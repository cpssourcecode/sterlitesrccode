#!/bin/bash
#------------------------------------------------------------------------------
# Variable declarations
#------------------------------------------------------------------------------

#!/bin/bash
#------------------------------------------------------------------------------
# Variable declarations
#------------------------------------------------------------------------------

WL_INSTALL_DIR=$1
WL_DOMAIN_NAME=$2
PG=$3
BCC=$4
STG=$5
SSO=$6
AUX=$7

MACHINE_A_NAME=dev-machine
#MACHINE_B_NAME=Local-Machine2

WL_SERVER_DIR=wlserver
WLST_DIR=oracle_common/common/bin


echo "domainDirName='$WL_INSTALL_DIR/user_projects/domains/$WL_DOMAIN_NAME'" > create_ms.py
echo "readDomain(domainDirName)" >> create_ms.py
#echo  "$DATAWLOADER"
# Creating Managed Servers

echo "print('Creating Server - $PG on Port # 7003')" >> create_ms.py
#echo "try: " >> create_ms.py
#echo "  cd('Server/$PG1')" >> create_ms.py
#echo "  print '===> Server $PG1 already exists'" >> create_ms.py
#echo "  print '===> No action was performed'" >> create_ms.py
#echo "except: " >> create_ms.py
#echo "  pass" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('$PG', 'Server')" >> create_ms.py
echo "cd('/Server/$PG')" >> create_ms.py
echo "cmo.setWeblogicPluginEnabled(true)" >> create_ms.py
echo "set('ListenPort', 7003)" >> create_ms.py
echo "set('ListenAddress', 'server50047')" >> create_ms.py

echo "print('Creating Server - $BCC on Port # 7103')" >> create_ms.py
#echo "try: " >> create_ms.py
#echo "  cd('Server/$BCC')" >> create_ms.py
#echo "  print '===> Server $BCC already exists'" >> create_ms.py
#echo "  print '===> No action was performed'" >> create_ms.py
#echo "except: " >> create_ms.py
#echo "  pass" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('$BCC', 'Server')" >> create_ms.py
echo "cd('/Server/$BCC')" >> create_ms.py
echo "cmo.setWeblogicPluginEnabled(true)" >> create_ms.py
echo "set('ListenPort', 7103)" >> create_ms.py
echo "set('ListenAddress', 'server50047')" >> create_ms.py

echo "print('Creating Server - $STG on Port # 7203')" >> create_ms.py
#echo "try: " >> create_ms.py
#echo "  cd('Server/$AUX')" >> create_ms.py
#echo "  print '===> Server $STG already exists'" >> create_ms.py
#echo "  print '===> No action was performed'" >> create_ms.py
#echo "except: " >> create_ms.py
#echo "  pass" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('$STG', 'Server')" >> create_ms.py
echo "cd('/Server/$STG')" >> create_ms.py
echo "cmo.setWeblogicPluginEnabled(true)" >> create_ms.py
echo "set('ListenPort',7203)" >> create_ms.py
echo "set('ListenAddress', 'server50047')" >> create_ms.py

echo "print('Creating Server - $SSO on Port # 7303')" >> create_ms.py
#echo "try: " >> create_ms.py
#echo "  cd('Server/$SSO')" >> create_ms.py
#echo "  print '===> Server $STG already exists'" >> create_ms.py
#echo "  print '===> No action was performed'" >> create_ms.py
#echo "except: " >> create_ms.py
#echo "  pass" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('$SSO', 'Server')" >> create_ms.py
echo "cd('/Server/$SSO')" >> create_ms.py
echo "cmo.setWeblogicPluginEnabled(true)" >> create_ms.py
echo "set('ListenPort',7303)" >> create_ms.py
echo "set('ListenAddress', 'server50047')" >> create_ms.py

echo "print('Creating Server - $AUX on Port # 7403')" >> create_ms.py
#echo "try: " >> create_ms.py
#echo "  cd('Server/$STG')" >> create_ms.py
#echo "  print '===> Server $STG already exists'" >> create_ms.py
#echo "  print '===> No action was performed'" >> create_ms.py
#echo "except: " >> create_ms.py
#echo "  pass" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('$AUX', 'Server')" >> create_ms.py
echo "cd('/Server/$AUX')" >> create_ms.py
echo "cmo.setWeblogicPluginEnabled(true)" >> create_ms.py
echo "set('ListenPort',7403)" >> create_ms.py
echo "set('ListenAddress', 'server50047')" >> create_ms.py


echo "print('Creating Cluster - pageCluster and assigning server $PG to it')" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('pageCluster', 'Cluster')" >> create_ms.py
echo "assign('Server', '$PG','Cluster','pageCluster')" >> create_ms.py
echo "cd('Clusters/pageCluster')" >> create_ms.py
echo "set('ClusterMessagingMode', 'unicast')" >> create_ms.py
echo "set('ClusterAddress','$MACHINE_A_NAME:10002')" >> create_ms.py

echo "print('Creating Cluster - bccCluster and assigning server $BCC to it')" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('bccCluster', 'Cluster')" >> create_ms.py
echo "assign('Server', '$BCC','Cluster','bccCluster')" >> create_ms.py
echo "cd('Clusters/bccCluster')" >> create_ms.py
echo "set('ClusterMessagingMode', 'unicast')" >> create_ms.py
echo "set('ClusterAddress','$MACHINE_A_NAME:10011')" >> create_ms.py

echo "print('Creating Cluster - stagingCluster and assigning server $STG to it')" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('stagingCluster', 'Cluster')" >> create_ms.py
echo "assign('Server', '$STG','Cluster','stagingCluster')" >> create_ms.py
echo "cd('Clusters/stagingCluster')" >> create_ms.py
echo "set('ClusterMessagingMode', 'unicast')" >> create_ms.py
echo "set('ClusterAddress','$MACHINE_A_NAME:10021')" >> create_ms.py

echo "print('Creating Cluster - ssoCluster and assigning server $SSO to it')" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('ssoCluster', 'Cluster')" >> create_ms.py
echo "assign('Server', '$SSO','Cluster','ssoCluster')" >> create_ms.py
echo "cd('Clusters/ssoCluster')" >> create_ms.py
echo "set('ClusterMessagingMode', 'unicast')" >> create_ms.py
echo "set('ClusterAddress','$MACHINE_A_NAME:10031')" >> create_ms.py

echo "print('Creating Cluster - auxCluster and assigning server $AUX to it')" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('auxCluster', 'Cluster')" >> create_ms.py
echo "assign('Server', '$AUX','Cluster','auxCluster')" >> create_ms.py
echo "cd('Clusters/auxCluster')" >> create_ms.py
echo "set('ClusterMessagingMode', 'unicast')" >> create_ms.py
echo "set('ClusterAddress','$MACHINE_A_NAME:10060')" >> create_ms.py


echo "print('Creating Machine - $MACHINE_A_NAME and adding $PG,$BCC,$STG,$SSO,$AUX')" >> create_ms.py
#echo "try: " >> create_ms.py
#echo "  cd('Machines/$MACHINE_A_NAME')" >> create_ms.py
#echo "  print '===> Machine $MACHINE_A_NAME already exists'" >> create_ms.py
#echo "  print '===> No action was performed'" >> create_ms.py
#echo "except: " >> create_ms.py
#echo "  pass" >> create_ms.py
echo "cd('/')" >> create_ms.py
echo "create('$MACHINE_A_NAME', 'Machine')" >> create_ms.py
echo "assign('Server', '$PG,$BCC,$STG,$SSO,$AUX','Machine','$MACHINE_A_NAME')" >> create_ms.py
echo "cd('Machines/' + '$MACHINE_A_NAME/')" >> create_ms.py
echo "create('$MACHINE_A_NAME', 'NodeManager')" >> create_ms.py
echo "cd('NodeManager/' + '$MACHINE_A_NAME')" >> create_ms.py
echo "set('NMType', 'plain')" >> create_ms.py
echo "set('ListenAddress', 'server50047')" >> create_ms.py
echo "set('DebugEnabled', 'false')" >> create_ms.py


# updating the changes
echo "print('Finalizing the changes')" >> create_ms.py

echo "updateDomain()" >> create_ms.py
echo "closeDomain()" >> create_ms.py
# Exiting
echo "print('Exiting...')" >> create_ms.py
echo "exit()" >> create_ms.py

$WL_INSTALL_DIR/$WLST_DIR/commEnv.sh
$WL_INSTALL_DIR/$WLST_DIR/wlst.sh create_ms.py
#./create_managed_servers_dev.sh /apps/Oracle/Middleware/Oracle_Home cpdomain DEVpageServer1 DEVbcc DEVstagingPreview DEVsso DEVauxServer1
