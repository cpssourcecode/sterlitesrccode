#!/bin/bash
#------------------------------------------------------------------------------
# Variable declarations
#------------------------------------------------------------------------------
INSTALL_USER=$1
INSTALL_GROUP=$2
WL_DOMAIN_NAME_PREFIX=$3
WL_DOMAIN_ADMIN_PASSWORD=$4
WL_DOMAIN_ADMIN_LISTENPORT=$5
WL_INSTALL_DIR=$6
WL_SERVER_DIR=wlserver
WLST_DIR=oracle_common/common/bin
WL_DOMAINS_DIR=user_projects/domains
WL_DOMAIN_NAME=$WL_DOMAIN_NAME_PREFIX'domain'
WL_DOMAIN_TEMPLATES=common/templates/wls/wls.jar

#-------------------------------------------------------------------------------------------------------------------------
#Create Weblogic domain
#-------------------------------------------------------------------------------------------------------------------------
if [ -d $WL_INSTALL_DIR/$WL_DOMAINS_DIR/$WL_DOMAIN_NAME ]; then
    echo "Weblogic domain already exists..Exiting"
else
    echo "Creating Weblogic domain.."
    export CONFIG_JVM_ARGS=-Djava.security.egd=file:/dev/./urandom

    #cd $WL_INSTALL_DIR/$WL_SERVER_DIR/$WLST_DIR
    cd $WL_INSTALL_DIR/$WLST_DIR
    mkdir temp
    cd temp

    echo "readTemplate('$WL_INSTALL_DIR/$WL_SERVER_DIR/$WL_DOMAIN_TEMPLATES')" > create_domain.py
    echo "cd('Security/base_domain/User/weblogic')" >> create_domain.py
    echo "cmo.setPassword('$WL_DOMAIN_ADMIN_PASSWORD')" >> create_domain.py
#    echo "setOption('ServerStartMode', 'dev')" >> create_domain.py
    echo "cd('/')" >> create_domain.py
    echo "cd('Servers/AdminServer')" >> create_domain.py
    echo "set('ListenPort', $WL_DOMAIN_ADMIN_LISTENPORT)" >> create_domain.py
    
    echo "cd('/')" >> create_domain.py

    echo "setOption('OverwriteDomain', 'true')" >> create_domain.py
    echo "writeDomain('$WL_INSTALL_DIR/$WL_DOMAINS_DIR/$WL_DOMAIN_NAME')" >> create_domain.py
    echo "closeTemplate()" >> create_domain.py
    echo "exit()" >> create_domain.py
        
    cat create_domain.py
    cd ..
        
    ./commEnv.sh
    ./wlst.sh temp/create_domain.py
    rm -rf temp
        
    cd $WL_INSTALL_DIR
    chown -R $INSTALL_USER:$INSTALL_GROUP user_projects
    cd $WL_DOMAINS_DIR/$WL_DOMAIN_NAME
    
    echo "DOMAIN_HOME=$WL_INSTALL_DIR/$WL_DOMAINS_DIR/$WL_DOMAIN_NAME" > startWebLogic.sh
    echo "export JAVA_OPTIONS=\"-Djava.security.egd=file:/dev/./urandom \"" >> startWebLogic.sh
    echo "nohup \${DOMAIN_HOME}/bin/startWebLogic.sh \$* > \${DOMAIN_HOME}/weblogic.out 2>&1 &" >> startWebLogic.sh
    echo "Weblogic domain created."

fi

# ./create_weblogic_domain_dev.sh dynuser 600 cps weblogic123 7001 /apps/Oracle/Middleware/Oracle_Home 
