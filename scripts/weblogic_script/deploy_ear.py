wlServer = sys.argv[1]
wls_username=sys.argv[2]
wls_password=sys.argv[3]
app_name1=sys.argv[4]
app_name2=sys.argv[5]
app_name3=sys.argv[6]
app_name4=sys.argv[7]
EAR_Path=sys.argv[8]

while True:
 try:
  connect(wls_username,wls_password,'t3://'+wlServer+':7001')
  edit()
  startEdit()
  break

 except:
  sleep(60)

print "Starting DEPLOYING EAR"

try:
    print "Starting DEPLOYING Prod EAR"
    deploy(appName= app_name1, path=''+EAR_Path+app_name1+'.ear', targets='pageCluster')
    print "Starting DEPLOYING CPSPub EAR"
    deploy(appName= app_name2 , path=''+EAR_Path+app_name2+'.ear', targets='bccCluster')
    print "Starting DEPLOYING CPSStage EAR"
    deploy(appName= app_name3 , path=''+EAR_Path+app_name3+'.ear', targets='stagingCluster')
    print "Starting DEPLOYING CPSAux EAR"
    deploy(appName= app_name4 , path=''+EAR_Path+app_name4+'.ear', targets='auxCluster')
    save()
    activate()          
    print "~~~~DEPLOY CPSProd,CPSPub,CPSStage,CPSAux SUCCESFULL~~~~"

except:
    print "Unexpected error:", sys.exc_info()[0]
    undo('true','y')
    stopEdit('y')
    print "~~~~COMMAND FAILED~~~~"
    raise
