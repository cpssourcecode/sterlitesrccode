#!/bin/bash
#------------------------------------------------------------------------------
# Variable declarations
#------------------------------------------------------------------------------

WL_INSTALL_DIR=$1
WL_DOMAIN_NAME=$2
WL_ADMIN_USERNAME=$3
WL_ADMIN_PASSWORD=$4
WL_ADMIN_URL=$5
WL_DATASOURCE_URL=$6
WL_DATASOURCE_NAME=$7
WL_DATASOURCE_JNDI=$8
WL_DATABASE_USER=$9
WL_DATABASE_PASSWORD=${10}
WL_DATASOURCE_DRIVER_CLASS=${11}
WL_TARGETS=${12}

WL_SERVER_DIR=wlserver
WLST_DIR=oracle_common/common/bin


echo "domainDirName='$WL_INSTALL_DIR/user_projects/domains/$WL_DOMAIN_NAME'" > create_ds.py
echo "adminURL='$WL_ADMIN_URL'" >> create_ds.py
echo "adminUserName='$WL_ADMIN_USERNAME'" >> create_ds.py
echo "adminPassword='$WL_ADMIN_PASSWORD'" >> create_ds.py
     
echo "dsName='$WL_DATASOURCE_NAME'" >> create_ds.py
echo "dsJNDI='$WL_DATASOURCE_JNDI'" >> create_ds.py
echo "dsDriverName='$WL_DATASOURCE_DRIVER_CLASS'" >> create_ds.py
echo "dsURL='$WL_DATASOURCE_URL'" >> create_ds.py
echo "dsUserName='$WL_DATABASE_USER'" >> create_ds.py
echo "dsPassword='$WL_DATABASE_PASSWORD'" >> create_ds.py
     
echo "dsTestQuery='SQL SELECT * FROM DUAL'" >> create_ds.py

echo "readDomain(domainDirName)" >> create_ds.py
#echo "try: " >> create_ds.py
#echo "  cd('/JDBCSystemResource/' + dsName + '/JdbcResource/'+ dsName)" >> create_ds.py
#echo "  print '===> JDBCSystemResource \"' +dsName+'\" already exists'" >> create_ds.py
#echo "  print '===> No action was performed'" >> create_ds.py
#echo "  exit()" >> create_ds.py
#echo "except: " >> create_ds.py
#echo "  pass" >> create_ds.py
echo "cd('/')" >> create_ds.py
echo "create(dsName, 'JDBCSystemResource')" >> create_ds.py
echo "cd('JDBCSystemResource/' + dsName + '/JdbcResource/'+ dsName)" >> create_ds.py
echo "create('myJdbcDriverParams','JDBCDriverParams')" >> create_ds.py
echo "cd('JDBCDriverParams/NO_NAME_0')" >> create_ds.py
echo "set('DriverName', dsDriverName)" >> create_ds.py
echo "set('URL', dsURL)" >> create_ds.py
echo "set('PasswordEncrypted', dsPassword)" >> create_ds.py
echo "set('UseXADataSourceInterface', 'true')" >> create_ds.py
echo "create('myProps','Properties')" >> create_ds.py
echo "cd('Properties/NO_NAME_0')" >> create_ds.py
echo "create('user', 'Property')" >> create_ds.py
echo "cd('Property/user')" >> create_ds.py
echo "cmo.setValue(dsUserName)" >> create_ds.py

echo "cd('/JDBCSystemResource/' + dsName + '/JdbcResource/' + dsName)" >> create_ds.py
echo "create('myJdbcDataSourceParams','JDBCDataSourceParams')" >> create_ds.py
echo "cd('JDBCDataSourceParams/NO_NAME_0')" >> create_ds.py
echo "set('KeepConnAfterGlobalTx',false)" >> create_ds.py
echo "set('KeepConnAfterLocalTx',false)" >> create_ds.py
echo "set('JNDIName', java.lang.String(dsJNDI))" >> create_ds.py

echo "cd('/JDBCSystemResource/' + dsName + '/JdbcResource/'+ dsName)" >> create_ds.py
echo "create('myJdbcConnectionPoolParams','JDBCConnectionPoolParams')" >> create_ds.py
echo "cd('JDBCConnectionPoolParams/NO_NAME_0')" >> create_ds.py
echo "set('TestTableName', dsTestQuery)" >> create_ds.py

echo "cd('/')" >> create_ds.py

###mapping datasource with targeter start
STR=$WL_TARGETS  #String with names
IFS=',' read -ra NAMES <<< "$STR"    #Convert string to array

#Print all names from array
for i in "${NAMES[@]}"; do
    echo "assign('JDBCSystemResource', dsName, 'Target', '$i')" >> create_ds.py
done

echo "updateDomain()" >> create_ds.py
echo "closeDomain()" >> create_ds.py
echo "exit()" >> create_ds.py

###mapping datasource with targeter finished

$WL_INSTALL_DIR/$WLST_DIR/commEnv.sh
$WL_INSTALL_DIR/$WLST_DIR/wlst.sh create_ds.py

#rm -rf create_ds.py

