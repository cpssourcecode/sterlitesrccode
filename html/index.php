<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-xs-7">
                <h1>CPS HTML <small>Don't forget fo clear your cache.</small></h1>
                
                <style>
                    .list-unstyled li .text-muted {
                        color: #DBDBDB; top: 2px; position: relative;
                    }
                </style>
                <br>
                <ul class="list-unstyled list-inline">
                    <li><a class="btn btn-link btn-nopadding" href="email-templates.php">Emails templates</a></li>
                    <li><a class="btn btn-link btn-nopadding" href="modals-preview.php">Modals Preview Page</a></li>
                    <!-- <a class="btn btn-link btn-nopadding" href="email.php">Send email template →</a></li> -->
                </ul>
            </div>
            <div class="col-lg-5 col-xs-5">
                    <h2>Email testing service</h2>
                    <p>Enter recipient email and click «Send».</p>
                    <form action="sendmail.php" method="post">
                        <input class="form-control" style="width: 200px; display: inline-block;" type="text" name="email" placeholder="Recipient Email"/>
                        <input class="btn btn-default btn-lg" type="submit" name="submit" value="Send" />
                    </form>
            </div>
        </div>
        <div class="row">
            <hr>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <ul class="list-unstyled">
                    <li><a class="btn btn-link btn-nopadding"   href="home.php">Home</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('home.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="home-logged-in.php">Home Logged In</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('home-logged-in.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="contact-us.php">Contact Us</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('contact-us.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="search-results.php">Search Results</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('search-results.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="packing-slips.php">Packing Slips</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('packing-slips.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="view-invoices.php">View Invoices</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('view-invoices.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="request-material-test-report.php">Request Material Test Report</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('request-material-test-report.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="manage-invoices.php">Manage Invoices</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('manage-invoices.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="pending-order-requests.php">Pending Order Requests</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('pending-order-requests.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="pending-order-approvals.php">Pending Order Approvals</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('pending-order-approvals.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="order-history.php">Order History</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('order-history.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="view-order-detail.php">View Order Detail</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('view-order-detail.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="material-lists.php">Material Lists</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('material-lists.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="material-lists-detail.php">Material List Detail</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('material-lists-detail.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="no-material-lists.php">No Material Lists</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('no-material-lists.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="view-statements.php">View Statements</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('view-statements.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="my-account.php">My Account</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('my-account.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="my-account-admin-access.php">My Account Admin Access</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('my-account-admin-access.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="product-listing.php">Product Listing</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('product-listing.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="pdp.php">PDP</a> <!-- <small class="text-muted">Not finished</small> -->
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('pdp.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="view-address.php">View Address</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('view-address.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="view-profile.php">View Profile</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('view-profile.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="manage-users.php">Manage Users</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('manage-users.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="manage-companies.php">Manage Companies</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('manage-companies.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="add-customer-admin.php">Add Customer Admin</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('add-customer-admin.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="add-user.php">Add User</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('add-user.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="edit-user.php">Edit User</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('edit-user.php')); ?>
                        </small>
                    </li>
                    
                </ul>
            </div>
            <div class="col-xs-6">
                <ul class="list-unstyled">
                    <li><a class="btn btn-link btn-nopadding"   href="category-landing-page.php">Category Landing Page</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('category-landing-page.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="cart.php">Cart</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('cart.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="cart-empty.php">Cart Empty</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('cart-empty.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="review.php">Review</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('review.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="confirmation.php">Confirmation</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('confirmation.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="locations.php">Locations</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('locations.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="locations-detail.php">Locations Detail</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('locations-detail.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="order-pad.php">Order Pad</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('order-pad.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="case-studies.php">Case Studies</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('case-studies.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="product-information.php">Product Information</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('product-information.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="case-studies-detail.php">Case Studies Detail</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('case-studies-detail.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="regional-managers.php">Regional Managers</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('regional-managers.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="404-page.php">404 Error Page</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('404-page.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="faqs.php">FAQs</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('faqs.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="video-library.php">Video Library</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('video-library.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="news-events.php">News & Events</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('news-events.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="site-map.php">Site Map</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('site-map.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="price-list.php">Price Lists</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('price-list.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="testimonials.php">Testimonials</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('testimonials.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="affiliations.php">Affiliations</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('affiliations.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="saved-carts.php">Saved Carts</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('saved-carts.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="services-with-video.php">Two Columns Template (Services with Video)</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('services-with-video.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="our-business.php">Our Business (Template 1 with Table on Content and Rich Text)</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('our-business.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="table-of-content-cartridge.php">Table of Content Cartridge</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('table-of-content-cartridge.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="messages-alerts.php">Messages/Alerts</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('messages-alerts.php')); ?>
                        </small>
                    </li>
                    <li><a class="btn btn-link btn-nopadding"   href="maintenance.php">Maintenance Page</a>
                        <small class="text-muted">&nbsp;&nbsp;
                            <?php echo "Modified " . date("F d Y H:i", filectime('maintenance.php')); ?>
                        </small>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>
</body>

</html>

