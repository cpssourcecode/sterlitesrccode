<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>News & Events Page - CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-sm-8">
            <ol class="breadcrumb">
                <li><a href="#">Our Business</a></li>
                <li class="active">News & Events</li>
            </ol>
        </div>
        <div class="col-sm-4 page-actions">
            <ul class="list-inline">
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-envelope-o"></i><span>Email Page</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-print"></i><span>Print Page</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12">
            <h1>News & Events</h1>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="sidebar-menu">
                        <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                        <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                            <li><a href="#">Our Business</a></li>
                            <li><a href="#">Locations</a></li>
                            <li class="active"><a href="#">News and Events</a></li>
                            <li><a href="#">Case Studies</a></li>
                            <li><a href="#">Testimonials</a></li>
                            <li><a href="#">Affiliations</a></li>
                            <li><a href="#">Regional Managers</a></li>
                            <li><a href="#">Price Lists</a></li>
                        </ul>
                    </div>
                    <div class="sidebar-block hidden-xs hidden-xxs">
                        <h4>Upcoming Events</h4>
                        <div class="sidebar-events-list">
                            <div class="item">
                                <h5>Event Name</h5>
                                <strong>03/30/2016</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam.</p>
                                <a class="more-link" href="#">Learn more</a>
                            </div>
                            <div class="item">
                                <h5>Event Name</h5>
                                <strong>08/15/2016</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam.</p>
                                <a class="more-link" href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <article class="news-article">
                        <header>
                            <div class="article-date">09/30/2015</div>
                            <h3><a href="#">ASA Economic Advisor: September 2015</a></h3>
                        </header>
                        <div class="article-data">
                            <div class="article-thumbnail">
                                <div class="thumbnail-pic">
                                    <img src="assets/images/ASA_Advixor_thumbnail-1.png" alt=""/>
                                </div>
                                <div class="btn btn-warning btn-block">View</div>
                            </div>
                            <div class="article-preview">
                                <div class="article-pic-preview"><img src="assets/images/ASA_Advisor_2.jpg" alt=""/></div>
                                <p>
                                    The <strong>ASA Advisor</strong> is a monthly economic report produced by the American Supply
                                    Association (ASA), the premier trade association serving the PVF and PHCP industry.
                                    It provides macro-economic analysis of the PVF and PHCP industry at a national and
                                    regional level as well as reports on how the U.S. economy may impact the different
                                    industry segments served by ASA members.
                                </p>
                                <p>
                                    Each month, ASA members receive an updated three-year forecast, and, as a proud
                                    member of ASA, Columbia Pipe & Supply is pleased to share this information with
                                    you, our valued partners.
                                </p>
                            </div>
                        </div>
                    </article>

                    <article class="news-article">
                        <header>
                            <div class="article-date">09/21/2015</div>
                            <h3><a href="#">PVF E-news: MRC Global-Phillips 66 announcement; <br> MORSCO acquisition</a></h3>
                            <h6>Sponsored by <strong><a href="#">ASA Industrial Piping Division</a></strong></h6>
                        </header>
                        <div class="article-data">
                            <div class="article-preview">
                                <div class="article-pic-preview"><img src="assets/images/PVF_Enews-1.JPG" alt=""/></div>
                                <div class="links-block">
                                    <h4>PVF Latest News</h4>
                                    <ul>
                                        <li><a href="#">MRC Global announces Q2 financials </a></li>
                                        <li><a href="#">Affiliated Distributors Q2 results</a></li>
                                        <li><a href="#">Distribution NOW DC opening</a></li>
                                        <li><a href="#">Why gas prices jumped so quickly in the Midwest (via USA Today)</a></li>
                                        <li><a href="#">Powell Valves news</a></li>
                                        <li><a href="#">ASA announces lifetime achievement award recipient</a></li>
                                        <li><a href="#">2015 top 50 PVF distributor rankings</a></li>
                                    </ul>
                                </div>
                                <div class="links-block">
                                    <h4>Supply House Times Opinions</h4>
                                    <ul>
                                        <li class="margin-b10">Morris Beschloss <br>
                                            <a href="#">Eliminating the oil embargo</a>
                                        </li>
                                        <li class="margin-b10">Rick Fantham (ASA President) <br>
                                            <a href="#">The industry is built on relationships</a>
                                        </li>
                                        <li class="margin-b10">Dirk Beveridge <br>
                                            <a href="#">Disruption is all about changing the rules</a>
                                        </li>
                                        <li class="margin-b10">Mike Miazga <br>
                                            <a href="#">Adapting to change</a>
                                        </li>
                                        <li class="margin-b10">Ricky Bryant/Melanie Felladore <br>
                                            <a href="#">Useful safety apps</a>
                                        </li>
                                    </ul>
                                </div>
                                <h4>PVF Outlook</h4>
                                <div class="article-thumbnail">
                                    <div class="thumbnail-pic">
                                        <img src="assets/images/PVFSummer15_Cover_Hi-Res-Resized.jpg" alt=""/>
                                    </div>
                                    <div class="btn btn-warning btn-block">View</div>
                                </div>
                                <div class="article-thumbnail-desc">
                                    Summer 2015 edition
                                </div>
                            </div>
                        </div>
                    </article>

                    <article class="news-article">
                        <header>
                            <div class="article-date">09/30/2015</div>
                            <h3><a href="#">ASA Economic Advisor: September 2015</a></h3>
                        </header>
                        <div class="article-data">
                            <div class="article-thumbnail">
                                <div class="thumbnail-pic">
                                    <img src="assets/images/ASA_Advixor_thumbnail-1.png" alt=""/>
                                </div>
                                <div class="btn btn-warning btn-block">View</div>
                            </div>
                            <div class="article-preview">
                                <div class="article-pic-preview"><img src="assets/images/ASA_Advisor_2.jpg" alt=""/></div>
                                <p>
                                    The <strong>ASA Advisor</strong> is a monthly economic report produced by the American Supply
                                    Association (ASA), the premier trade association serving the PVF and PHCP industry.
                                    It provides macro-economic analysis of the PVF and PHCP industry at a national and
                                    regional level as well as reports on how the U.S. economy may impact the different
                                    industry segments served by ASA members.
                                </p>
                                <p>
                                    Each month, ASA members receive an updated three-year forecast, and, as a proud
                                    member of ASA, Columbia Pipe & Supply is pleased to share this information with
                                    you, our valued partners.
                                </p>
                            </div>
                        </div>
                    </article>
                    <div class="sidebar-block visible-xs visible-xxs">
                        <h4>Upcoming Events</h4>
                        <div class="sidebar-events-list">
                            <div class="item">
                                <h5>Event Name</h5>
                                <strong>03/30/2016</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam.</p>
                                <a class="more-link" href="#">Learn more</a>
                            </div>
                            <div class="item">
                                <h5>Event Name</h5>
                                <strong>08/15/2016</strong>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam.</p>
                                <a class="more-link" href="#">Learn more</a>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <div class="pagination">
                            <nav>
                                <ul class="pagination pagination-lg">
                                    <li class="prev">
                                        <a href="#" aria-label="Previous">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><span>…</span></li>
                                    <li class="next">
                                        <a href="#" aria-label="Next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>

</html>