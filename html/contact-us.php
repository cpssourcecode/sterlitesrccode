<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
    <!-- header end -->
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>Contact Us</h1>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-4 col-xs-4 col-xxs-12">
                        
                        <h3>Your Sales Team</h3>
                        <p>
                            900 Industrial Avenue <br>
                            Rock Falls, Illinois 61071
                        </p>
                        <p>
                            <strong>Phone</strong> <br>
                            (815) 625-6692
                        </p>
                        <p>
                            <strong>Fax</strong> <br>
                            (000) 000-0000
                        </p>
                        <p>
                            <strong>Normal Business Hours</strong> <br>
                            M-F: 8 a.m. — 5 p.m. <br>
                            Sat: 9 a.m. — 3 p.m. <br>
                            Sun: Closed
                        </p>
                        <p><strong>24/7 Emergency Service Available</strong></p>
                        <hr>
                        <h3>Corporate Office</h3>
                        <p>
                            <address>
                                1120 West Pershing Road <br>
                                Chicago, IL 60609 <br>
                                Phone: 773-927-6600 <br>
                                Fax: 773-927-8415 <br>
                            </address>
                        </p>
                        <hr>
                        <h3>Remittance</h3>
                        <p>
                            <address>
                                <b>Remit Payments to:</b><br>
                                Chicago, IL 60609 <br>
                                Phone: 773-927-6600 <br>
                                Fax: 773-927-8415 <br>
                            </address>
                        </p>
                    </div>
                    <div class="col-sm-8 col-xs-8 col-xxs-12">
                        <div class="well well-gray">
                            <form>
                                <div class="form-group">
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                              </span>
                                            </button>
                                        Please, fill in <a class="error-anchor" href="#name-anchor"><span>Name</span></a> and <a class="error-anchor" href="#company-anchor"><span>Your Compay Name</span></a> field
                                    </div>
                                </div>
                                <div class="form-group has-error">
                                    <input id="name-anchor" type="text" class="form-control input-lg" placeholder="*Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" placeholder="*Email Address">
                                </div>
                                <div class="form-group has-error">
                                    <input id="company-anchor" type="text" class="form-control input-lg" value="My Company Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" value="Account Number">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" placeholder="*Phone Number">
                                </div>
                                <div class="form-group">
                                    <select name="" id="" class="form-control input-lg bootstrap-select">
                                        <option value="">*Subject</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p class="form-control-static">*Preferred Method of Contact</p>
                                    <label class="radio-custom radio-inline" data-initialize="radio" id="myCustomRadioLabel5">
                                      <input class="sr-only"  checked="checked" name="radioEx2" type="radio" value="option1"> Email
                                    </label>
                                    <label class="radio-custom radio-inline" data-initialize="radio" id="myCustomRadioLabel6">
                                      <input class="sr-only" name="radioEx2" type="radio" value="option2"> Phone
                                    </label>
                                </div>
                                <div class="form-group">
                                    <textarea name="" id="" cols="5" rows="7" class="form-control" placeholder="Message (500 Characters Max)"></textarea>
                                </div>
                                <div class="form-group">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="btn btn-warning btn-lg">Submit</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn btn-default btn-lg">Cancel</a>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
</dsp:page>
