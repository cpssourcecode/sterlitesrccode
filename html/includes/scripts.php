    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/javascripts/jquery-1.11.3.min.js"></script>
    <script src="assets/javascripts/jquery-migrate-1.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/javascripts/bootstrap.js"></script>
    <!--<script src="assets/javascripts/bootstrap/collapse.js"></script>-->
    <script src="assets/javascripts/bootstrap/tooltip.js"></script>
    <script src="assets/javascripts/bootstrap/popover.js"></script>
    <script src="assets/javascripts/bootstrap/alert.js"></script>
    <script src="assets/javascripts/bootstrap/tab.js"></script>
    <script src="assets/javascripts/bootstrap/button.js"></script>

    <script src="assets/javascripts/bootstrap-notify.min.js"></script>

    <script src="assets/javascripts/bootstrap-select/bootstrap-select.js"></script>
    <!-- // <script src='assets/javascripts/bootstrap/dropdown.js'></script> -->
    <script src="assets/javascripts/bootstrap-hover-dropdown.min.js"></script>
    <!-- fuel ux -->
    <script src="assets/javascripts/fuelux/checkbox.js"></script>
    <script src="assets/javascripts/fuelux/radio.js"></script>
    <!-- owl -->
    <script src='assets/javascripts/owl/owl.carousel.min.js'></script>
    <!-- custom scroll -->
    <script src='assets/javascripts/owl/owl.carousel.min.js'></script>
    <!-- dropdown-enchancements -->
    <script src='assets/javascripts/jquery.scrollbar.min.js'></script>
    <!-- Placeholders for old browsers -->
    <script src='assets/javascripts/jquery-placeholder-gh-pages/jquery.placeholder.min.js'></script>
    
    <script src='assets/javascripts/lightbox-master/dist/ekko-lightbox.min.js'></script>
    <!-- <script src='assets/javascripts/jquery.fancybox.js'></script> -->
    <!-- <script src='assets/javascripts/jquery.fancybox-thumbs.js'></script> -->
    <!-- // <script src='assets/javascripts/owl/owl.support.modernizr.js'></script> -->
    <!-- fileinput -->
    <!-- // <script src='assets/javascripts/bootstrap-fileinput/fileinput.min.js'></script> -->
    <script src='assets/javascripts/jasny-bootstrap/fileinput.js'></script>
    <script src='assets/javascripts/jasny-bootstrap/inputmask.js'></script>

    <script src='assets/javascripts/jquery.mobile.custom/jquery.mobile.custom.min.js'></script>
    <!-- custom -->
    <script src="assets/javascripts/prototype.js"></script>