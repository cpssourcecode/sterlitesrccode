<!-- Find Invoice Modal -->
<div class="modal fade" id="findInvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-search">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Find an Invoice</h2>
                <div class="row">
                    <div class="col-xs-8 col-sm-10">
                        <input type="text" class="form-control input-sm" placeholder="Enter Order # or P.O. #">
                    </div>
                    <div class="col-xs-4 col-sm-2">
                        <a href="#" class="btn btn-warning btn-block">Go</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Find Packing Slip -->
<div class="modal fade" id="findPackingSlip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-search">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Find a Packing Slip</h2>
                <div class="row">
                    <div class="col-xs-8 col-sm-10">
                        <input type="text" class="form-control input-sm" placeholder="Enter Order # or P.O. #">
                    </div>
                    <div class="col-xs-4 col-sm-2">
                        <a href="#" class="btn btn-warning btn-block">Go</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Find Order -->
<div class="modal fade" id="findOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-search">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Find an Order</h2>
                <div class="row">
                    <div class="col-xs-8 col-sm-10">
                        <input type="text" class="form-control input-sm" placeholder="Enter Order # or P.O. #">
                    </div>
                    <div class="col-xs-4 col-sm-2">
                        <a href="#" class="btn btn-warning btn-block">Go</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Request a Quote -->
<div class="modal fade" id="requestQuote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-power">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-power"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Request a Quote</h2>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 hidden-xs hidden-xxs hide-to-1024">
                        <p>To request a quote, upload a document in one of the folloiwng formats: .doc, .pdf, .exl.</p>
                        <div class="form-group">
                            <!--input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1"-->
                            <label class="custom-file-upload">
                                <span class="btn btn-lg btn-default">Choose File</span>
                                <mark style="width: 77%;">No file chosen</mark>
                                <input type="file" multiple data-overwrite-initial="false" data-min-file-count="1">
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12">
                        <textarea name="" id="" cols="30" rows="6" class="form-control" placeholder="Message"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left two-mobile">
                    <li>
                        <a href="#" class="btn btn-lg btn-warning btn-lg">Submit</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Log In -->
<div class="modal fade" id="logIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Log In to Your Account</h2>
                <form>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg autofocus-input" id="exampleInputEmail1" placeholder="Email" autofocus>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <div class="checkbox checked" id="myCheckbox">
                            <label class="checkbox-custom checked" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="" checked>
                                <span class="checkbox-label">Remember Me</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p>Enter the following characters exactly how they appear.</p>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6 col-xxs-12">
                                <div class="captcha">
                                    <div class="image">
                                        <img src="http://placehold.it/187x44" alt="">
                                    </div>
                                    <button class="reload"></button>
                                    <button class="play"></button>
                                </div>
                            </div>
                            <div class="col-xs-6 col-xxs-12">
                                <input type="text" class="form-control input-lg" placeholder="Enter captcha code">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="button" id="myButton1" autocomplete="off" class="btn btn-warning btn-huge btn-block" data-loading-text="Loading…">Log In</button>
                        <script>
                        $('#myButton1').on('click', function() {
                            var $btn = $(this).button('loading')
                                // business logic...
                            $btn.button('reset')
                        })
                        </script>
                    </div>
                    <div class="form-group">
                        <p><a class="text-link close-this-modal" href="#" data-toggle="modal" data-target="#forgotPassword"><span>Forgot Password</span></a></p>
                        <p>Don't have a login? <a class="text-link" href="#" data-toggle="modal" data-target="#requestAnAccount"><span>Request an account</span></a>.</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Request An Account -->
<div class="modal fade" id="requestAnAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Create New Account</h2>
                <p>Enter the email associated with this account and the temporary password that was sent to you. </p>
                <form>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg autofocus-input" id="exampleInputEmail1" placeholder="Your Email" autofocus value="your@email.com">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Enter Your Temporary Password">
                    </div>
                    <div class="form-group">
                        <br>
                        <h4>Enter Your New Passowrd</h4>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Enter New Password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Re-enter New Password">
                    </div>
                    <div class="form-group">
                        <div class="checkbox checked" id="myCheckbox">
                            <label class="checkbox-custom checked" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="" checked>
                                <span class="checkbox-label">Remember Me</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning btn-huge btn-block">Log In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Activate New Account -->
<div class="modal fade" id="activateNewAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Activate New Account</h2>
                <p>Enter the email associated with this account and the temporary password that was sent to you. </p>
                <form>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg autofocus-input" id="exampleInputEmail1" placeholder="Your Email" autofocus value="joshjones@companyname.com">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Enter Your Temporary Password">
                    </div>
                    <div class="form-group">
                        <br>
                        <h4>Enter Your New Passowrd</h4>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Enter New Password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Re-enter New Password">
                    </div>
                    <div class="form-group">
                        <div class="checkbox checked" id="myCheckbox">
                            <label class="checkbox-custom checked" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="" checked>
                                <span class="checkbox-label">Remember Me</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning btn-huge btn-block">Log In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Forgot Password -->
<div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Forgot Password</h2>
                <form>
                    <div class="form-group">
                        <p>Enter the email you use to login and we will send a temporary password to the email address on file. </p>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg" id="exampleInputPassword1" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning btn-huge btn-block" data-toggle="modal" data-target="#resetPassword">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Reset Password -->
<div class="modal fade" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Reset Your Password</h2>
                <form>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPa" placeholder="Enter New Password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPa" placeholder="Re-enter New Password">
                    </div>
                    <div class="form-group">
                        <div class="checkbox checked" id="myCheckbox">
                            <label class="checkbox-custom checked" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="" checked>
                                <span class="checkbox-label">Remember Me</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning btn-huge btn-block">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Reset Password (design from emails templates) -->
<div class="modal fade" id="resetPassword2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Reset Password</h2>
                <form>
                    <div class="form-group">
                        <input type="email" class="form-control input-lg autofocus-input" id="exampleInputEmail1" placeholder="Your Email" autofocus value="joshjones@companyname.com">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Enter Your Temporary Password">
                    </div>
                    <div class="form-group">
                        <br>
                        <h4>Enter Your New Passowrd</h4>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Enter New Password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Re-enter New Password">
                    </div>
                    <div class="form-group">
                        <div class="checkbox checked" id="myCheckbox">
                            <label class="checkbox-custom checked" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="" checked>
                                <span class="checkbox-label">Remember Me</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning btn-huge btn-block">Log In</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Select a ship to address -->
<div class="modal fade" id="selectShipAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-paragraph">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-paragraph"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Select a Ship To Address</h2>
                <form>
                    <div class="form-group">
                        <p><strong>For this session</strong>, confirm your default Ship To Address or choose an alternate Ship To Address from the list below. </p>
                        <p>Your Default Ship To Address</p>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="radio checked">
                                    <label class="radio-custom checked" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option1" checked> <strong>NW Hospital — 17th Floor, Imaging <br>Address Line 2 <br>Address Line 3</strong>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-warning btn-block btn-lg">Confirm</button>
                            </div>
                        </div>
                    </div>
                    <hr class="huge">
                    <div class="form-group">
                        <p><strong>Select an Alternate Ship To Address for this Session</strong></p>
                        <p><span class="glyphicon glyphicon-alert text-danger"></span> Changing your Ship to Address may affect prices and availablity of items in your cart.</p>
                        <div class="row smaller">
                            <div class="col-xs-6 col-xxs-12">
                                <input type="text" class="form-control input-lg mb-xxs" placeholder="Search for Ship To Address">
                            </div>
                            <div class="col-xs-3 col-xxs-6">
                                <a href="#" class="btn btn-block btn-warning btn-lg">Search</a>
                            </div>
                            <div class="col-xs-3 col-xxs-6">
                                <a href="#" class="btn btn-block btn-default btn-lg">Reset</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="scrollable">
                            <div class="head">
                                <span>Select</span>
                                <span>Ship To</span>
                            </div>
                            <div class="content scrollbar-inner">
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option2"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio checked">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option3"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option4"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option5"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option6"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option7"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option8"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option9"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option10"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option11"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option12"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option13"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option14"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option15"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <ul class="list-inline two-mobile">
                    <li>
                        <a href="#" class="btn btn-warning btn-lg">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default btn-lg">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Change a ship to address -->
<div class="modal fade" id="changeShipAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-paragraph">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-paragraph"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Change Ship To Address</h2>
                <form>
                    <div class="form-group">
                        <p>Your Default Ship To Address</p>
                    </div>
                    <div class="form-group">
                        <div class="radio checked">
                            <label class="radio-custom checked" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option1" checked> NW Hospital — 17th Floor, Imaging
                                <br>Address Line 2
                                <br>Address Line 3
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <p>Select an Alternate Ship To Address for this Session</p>
                        <div class="error-comment">
                            <i class="fa fa-exclamation-triangle"></i>
                            <p>Changing your Ship to Address may affect prices and availablity of items in your cart.
                            </p>
                        </div>
                        <div class="row smaller">
                            <div class="col-xs-6 col-xxs-12">
                                <input type="text" class="form-control input-lg mb-xxs" placeholder="Search for Ship To Address">
                            </div>
                            <div class="col-xs-3 col-xxs-6">
                                <a href="#" class="btn btn-block btn-warning btn-lg btn-nopadding">Search</a>
                            </div>
                            <div class="col-xs-3 col-xxs-6">
                                <a href="#" class="btn btn-block btn-default btn-lg btn-nopadding">Reset</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="scrollable">
                            <div class="head">
                                <span>Select</span>
                                <span>Ship To</span>
                            </div>
                            <div class="content scrollbar-inner">
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option2"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio checked">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option3"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option4"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option5"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option6"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option7"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option8"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option9"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option10"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <ul class="list-inline one-mobile">
                    <li>
                        <a href="#" class="btn btn-warning btn-lg pull-left">Confirm</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Change Pickup Location -->
<div class="modal fade" id="changePickupLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-location">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-location"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Change Pickup Location</h2>
                <form>
                    <div class="form-group">
                        <p>Your Default or Current Pickup Location</p>
                    </div>
                    <div class="form-group">
                        <div class="radio checked">
                            <label class="radio-custom checked" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option1" checked> <strong>NW Hospital — 17th Floor, Imaging
                                <br>Address Line 2
                                <br>Address Line 3</strong>
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <p>Select an alternate Pickup Location for this session.</p>
                        <div class="error-comment">
                            <i class="fa fa-exclamation-triangle"></i>
                            <p>Changing your Pickup Location may affect prices and availablity of items in your cart.</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="scrollable">
                            <div class="head">
                                <span>Select</span>
                                <span>Ship To</span>
                            </div>
                            <div class="content scrollbar-inner">
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option2"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio checked">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option3"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option4"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option5"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option6"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option7"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option8"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option9"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option10"> NW Hospital — 17th Floor, Imaging
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-warning btn-lg pull-left">Confirm</a>
            </div>
        </div>
    </div>
</div>
<!-- Add Ship to Address -->
<div class="modal fade" id="addShipToAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-paragraph">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-paragraph"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h3 class="modal-title">Add Ship To Address(es)</h3>
                <form>
                    <div class="form-group">
                        <p>Add Ship To Address(es) for this User. Be sure to set one Ship To Address as the default.</p>
                    </div>
                    <div class="form-group">
                        <div class="row smaller">
                            <div class="col-sm-6">
                                <input type="text" class="form-control input-lg mb-sm" placeholder="Search for Ship To Address">
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <a href="#" class="btn btn-block btn-warning btn-lg">Search</a>
                            </div>
                            <div class="col-sm-3 col-xs-6">
                                <a href="#" class="btn btn-block btn-default btn-lg">Reset</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="scrollable">
                            <div class="head">
                                <div class="row">
                                    <div class="col-sm-2 col-xs-2">
                                        <div class="checkbox" id="myCheckbox">
                                            <label class="checkbox-custom" data-initialize="checkbox">
                                                <input class="sr-only" type="checkbox" value="">
                                                <span class="checkbox-label">Select</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-9 col-xs-9">
                                        <span>Ship To</span>
                                    </div>
                                </div>
                            </div>
                            <div class="content scrollbar-inner">
                                <div class="col-lg-12 grid">
                                    <div class="row item">
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <address>
                                                NW Hospital — 17th Floor, Imaging
                                                <br> Address Line 2
                                                <br> Address Line 3
                                            </address>
                                        </div>
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="radio set-default">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                    <input class="sr-only" name="radioEx1" type="radio" value="option1"> Default
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row item">
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <address>
                                                NW Hospital — 17th Floor, Imaging
                                                <br> Address Line 2
                                                <br> Address Line 3
                                            </address>
                                        </div>
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="radio set-default">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                    <input class="sr-only" name="radioEx1" type="radio" value="option1"> Default
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row item">
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <address>
                                                NW Hospital — 17th Floor, Imaging
                                                <br> Address Line 2
                                                <br> Address Line 3
                                            </address>
                                        </div>
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="radio set-default">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                    <input class="sr-only" name="radioEx1" type="radio" value="option1"> Default
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row item">
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <address>
                                                NW Hospital — 17th Floor, Imaging
                                                <br> Address Line 2
                                                <br> Address Line 3
                                            </address>
                                        </div>
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="radio set-default">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                    <input class="sr-only" name="radioEx1" type="radio" value="option1"> Default
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row item">
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <address>
                                                NW Hospital — 17th Floor, Imaging
                                                <br> Address Line 2
                                                <br> Address Line 3
                                            </address>
                                        </div>
                                        <div class="col-sm-2 col-xs-2">
                                            <div class="radio set-default">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                    <input class="sr-only" name="radioEx1" type="radio" value="option1"> Default
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="#" class="btn btn-warning btn-lg">Add</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Check Price and Availability -->
<div class="modal fade" id="myCheckPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-write-1">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-write-1"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Import/Upload List</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <p>To add items to the Material List, upload a spreadsheet in .exl format.</p>
                    </div>
                    <div class="col-lg-12">
                        <!-- <div class="kv-main">
                            <form enctype="multipart/form-data">
                                <div class="form-group">
                                    <input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1">
                                </div>
                            </form>
                        </div> -->
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <span class="input-group-addon btn btn-lg btn-default btn-file">
                                <input type="file" name="...">
                                <span class="fileinput-new">Choose File</span>
                            <span class="fileinput-exists">Change</span>
                            </span>
                            <!-- <a href="#" class="input-group-addon btn btn-default input-lg fileinput-exists" data-dismiss="fileinput">Remove</a> -->
                            <div class="form-control input-lg" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-6 col-xxs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Upload</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-xxs-12">
                        <span class="modal-footer-noreload"><span class="text-muted glyphicon glyphicon-download-alt"></span> <a href="#" class="gray-noreload"><span>Download Spreadsheet Template</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Import Users -->
<div class="modal fade" id="importUsers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Import User(s)</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <p>To add User(s), upload a spreadsheet in .exl format. </p>
                    </div>
                    <div class="col-lg-12">
                        <div class="kv-main">
                            <form enctype="multipart/form-data">
                                <div class="form-group">
                                    <!--input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1"-->
                                    <label class="custom-file-upload">
                                        <span class="btn btn-lg btn-default">Choose File</span>
                                        <mark>No file chosen (1MB Max)</mark>
                                        <input type="file" multiple data-overwrite-initial="false" data-min-file-count="1">
                                    </label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-6 col-xxs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Upload</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-6 col-xxs-12">
                        <span class="modal-footer-noreload"><span class="text-muted glyphicon glyphicon-download-alt"></span> <a href="#" class="gray-noreload"><span>Download Spreadsheet Template</span></a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Export Users -->
<div class="modal fade" id="exportUsers" tabindex="-1" role="dialog" aria-labelledby="exportUsers" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-admin">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-admin"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Export Users to a CSV File</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <p>To export Users, click the Export button. Save the CSV file to a location of your choice.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul class="list-inline two-mobile">
                    <li>
                        <a href="file.csv" class="btn btn-lg btn-warning" download>Export</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Reinvite User Confirmation -->
<div class="modal fade" id="reinviteUserModal" tabindex="-1" role="dialog" aria-labelledby="reinviteUserModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-tick">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-tick"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Reinvite User Confirmation</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <p>An email with temporary User ID and Password information has been sent to this User.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-6 text-left">
                        <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Okay</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Check Availability -->
<div class="modal fade" id="myModalCheck" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-tick">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-tick"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Check Availability</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="media media-product">
                            <div class="media-left">
                                <div class="image">
                                    <a href="#">
                                        <img class="media-object" src="assets/images/pipe.png" alt="...">
                                    </a>
                                </div>
                            </div>
                            <div class="media-body ov-visible">
                                <h3 class="media-heading"><a href="#"><span>Std Blk, Galv PE&T&C, XH Blk PE</span></a></h3>
                                <p><span>Item #   Part #   UPC   Customer Alias</span>
                                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima nisi odio modi quidem quo dolorem quia minus consectetur cupiditate! Animi ipsa enim dolor, debitis sapiente aliquid cupiditate quae minus error.</span></p>
                                <br>
                            </div>
                        </div>
                        <div class="check-availability-actions">
                                    <div class="check-availability">
                                         <!-- data-collapse="#collapseHaveItShipped" -->
                                        <div class="radio checked">
                                            <label class="radio-custom" data-initialize="radio">
                                                <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="option2" id="collapseHaveItShipped"> Have it Shipped
                                            </label>
                                        </div>
                                        <!--  data-collapse="#collapsePickItUp" -->
                                        <div class="radio">
                                            <label class="radio-custom" data-initialize="radio">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1" id="collapsePickItUp"> Pick it Up
                                            </label>
                                        </div>
                                    </div>
                                    <form class="form-inline form-check-availability">
                                        <!-- <div class="form-group">
                                                <input type="text" class="form-control input-lg text-center" id="" placeholder="Qty" value="1">
                                            </div> -->
                                        <ul class="list-inline">
                                            <li>
                                                <span class="button-check-availability">
                                                        <button class="btn btn-warning btn-lg" type="button">Check Availability</button>
                                                    </span>
                                            </li>
                                            <li>
                                                <button class="btn btn-default btn-lg" data-dismiss="modal">Cancel</button>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <br>
                        <p>
                            <span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
                            <strong class="text-danger">IMPORTANT:</strong>&nbsp;<strong>ONLY ONE</strong> delivery method (<strong>Have it Shipped or Pick it Up</strong>) can be selected for all items in a shopping cart.
                        </p>
                    </div>
                </div>
                <div class="row collapse-check-availability">
                    <div class="col-lg-12 modal-check-collapse collapse" id="collapseHaveItShipped" aria-labelledby="headingOne">
                        <hr>
                        <h3>Have it Shipped</h3>
                        <br>
                        <p>
                            <span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
                            <strong class="text-danger">IMPORTANT:</strong>&nbsp;Quantity added to your cart can <strong>EXCEED</strong> quantity available shown. Exseeded quantities will be placed on backorder. You can contact your Sales Team for information about backorder items.
                        </p>
                        <p>Qty Available: <strong>25</strong></p>
                        <div class="row smaller">
                            <div class="col-xs-2 col-xxs-5">
                                <input type="text" placeholder="Qty" class="form-control text-center">
                            </div>
                            <div class="col-xs-5 col-xxs-7">
                                <a href="#" class="btn btn-lg btn-block btn-warning">Add to Cart</a>
                            </div>
                        </div>
                        <!-- <br> -->
                        <!-- <p><small>Disclaimer copy</small></p> -->
                    </div>
                    <div class="col-lg-12 modal-check-collapse collapse" id="collapsePickItUp" aria-labelledby="headingTwo">
                        <hr>
                        <form>
                            <div class="form-group">
                                <h4>Pick it Up</h4>
                                <!-- <br> -->
                                <p>
                                    <span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
                                    <strong class="text-danger">IMPORTANT:</strong>&nbsp;Quantity added to your cart can <strong>EXCEED</strong> quantity available shown. Exseeded quantities will be placed on backorder. You can contact your Sales Team for information about backorder items.
                                </p>
                                <h5>Your Default Pickup Location</h5>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 grid">
                                    <div class="row item pickup-location head">
                                        <div class="radio">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                                <input class="sr-only" name="radioEx2" type="radio" value="option1">
                                                <div class="col-sm-3 col-xs-3 col-xxs-4">
                                                    <strong>Qty Available</strong>
                                                    <br> 18
                                                </div>
                                                <div class="col-sm-3 col-xs-5 col-xxs-5">
                                                    <strong>Location Info Main</strong>
                                                    <br>
                                                    <address>
                                                        NW Hospital — 17th Floor, Imaging
                                                        <br> Address Line 2
                                                        <br> Address Line 3
                                                    </address>
                                                </div>
                                                <div class="col-sm-2 col-xs-4 col-xxs-3">
                                                    <strong>2.5 Miles</strong>
                                                    <br> <a href="#">Map it</a>
                                                </div>
                                                <div class="col-sm-4 col-xs-12 col-xxs-12 col-xs-ml-30">
                                                    <ul class="list-inline pull-right">
                                                        <li>
                                                            <input type="text" class="form-control input-sm text-center" placeholder="Qty">
                                                        </li>
                                                        <li>
                                                            <button class="btn btn-warning" type="button">Add to Cart</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr>
                                </div>
                                <h5>Other Pickup Location</h5>
                                <div class="pickup-location-grid">
                                    <div class="select">
                                        <strong>Select</strong>
                                    </div>
                                    <div class="qty">
                                        <strong>Qty Availabe</strong>
                                    </div>
                                    <div class="locations">
                                        <strong>Locations</strong>
                                    </div>
                                    <div class="distance">
                                        <strong>Distance/Map</strong>
                                    </div>
                                </div>
                                <div class="scrollable">
                                    <div class="content scrollbar-inner">
                                        <div class="radio">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                                <input class="sr-only" name="radioEx2" type="radio" value="option1">
                                                <div class="qty">
                                                    25
                                                </div>
                                                <div class="locations">
                                                    <strong>Locations</strong> NW Hospital — 17th Floor, Imaging
                                                    <br> Address Line 2
                                                    <br> Address Line 3
                                                </div>
                                                <div class="distance">
                                                    10 Miles
                                                    <br><a href="#">Map It</a>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <input type="text" class="form-control input-sm" placeholder="Qty">
                                                        </li>
                                                        <li>
                                                            <button class="btn btn-warning" type="button">Add to Cart</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="list-inline sm-actions">
                                                    <li>
                                                        <input type="text" class="form-control input-sm" placeholder="Qty">
                                                    </li>
                                                    <li>
                                                        <button class="btn btn-warning" type="button">Add to Cart</button>
                                                    </li>
                                                </ul>
                                            </label>
                                        </div>
                                        <div class="radio disabled">
                                            <label class="radio-custom disabled" data-initialize="radio" id="myCustomRadioLabel2">
                                                <input class="sr-only" disabled="disabled" name="radioEx2" type="radio" value="option1">
                                                <div class="qty">
                                                    25
                                                </div>
                                                <div class="locations">
                                                    <strong>Locations</strong> NW Hospital — 17th Floor, Imaging
                                                    <br> Address Line 2
                                                    <br> Address Line 3
                                                </div>
                                                <div class="distance">
                                                    10 Miles
                                                    <br><span>Map It</span>
                                                    <ul class="list-inline">
                                                        <li>
                                                            <input type="text" class="form-control input-sm disabled" disabled="disabled" placeholder="Qty">
                                                        </li>
                                                        <li>
                                                            <button class="btn btn-warning disabled" disabled="disabled" type="button">Add to Cart</button>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <ul class="list-inline sm-actions">
                                                    <li>
                                                        <input type="text" class="form-control input-sm disabled" disabled="disabled" placeholder="Qty">
                                                    </li>
                                                    <li>
                                                        <button class="btn btn-warning disabled" disabled="disabled" type="button">Add to Cart</button>
                                                    </li>
                                                </ul>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add to Material List -->
<div class="modal fade" id="addToMaterialList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-add-playlist">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-add-playlist"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body" id="add-to-material-list-body1" style="display:none">
                <h2 class="modal-title">Add to Material List</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group col-xs-8">
                                <select name="" id="sareList" class="bootstrap-select" data-width="100%" title='Select Existing Material List'>
                                    <option value="">List Name #2</option>
                                    <option value="">List Name #3</option>
                                    <option value="">List Name #5</option>
                                </select>
                            </div>
                            <div class="col-xs-4">
                                <a href="#" class="btn btn-block btn-lg btn-warning">Add to List</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <a class="btn create-new-list-btn" role="button" data-toggle="collapse" href="#collapseNewMaterial" aria-expanded="false" aria-controls="collapseExample" data-parent="#accordion">
                                    Create New Material List
                                </a>
                                <div class="create-new-list-collapse collapse" id="collapseNewMaterial" role="tabpanel">
                                    <hr>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter New Material List Name (50 Characters Max)" maxlength="50">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="" cols="4" rows="5" placeholder="Enter Optional List Description (125 Characters Max)" maxlength="125"></textarea>
                                    </div>
                                    <ul class="list-inline pull-left create-list-actions-modal">
                                        <li>
                                            <a href="#" class="btn btn-warning btn-lg">Create New List</a>
                                        </li>
                                        <li>
                                            <a class="btn btn-default btn-lg create-new-list-btn" role="button" data-toggle="collapse" href="#collapseNewMaterial" aria-expanded="false" aria-controls="collapseExample">Cancel</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body" id="add-to-material-list-body2" style="display:block">
                <h2 class="modal-title">10 Items Added to Material List <strong><a href="material-lists-detail.php">My New List</a></strong></h2>
                <div class="row scrollable">
                    <div class="col-lg-12">
                        <div class="content content-larger scrollbar-inner">
                            <div class="media media-product">
                                <div class="media-left">
                                    <div class="image">
                                        <a href="#">
                                            <img class="media-object" src="assets/images/pipe.png" alt="...">
                                        </a>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h3 class="media-heading"><a href="#"><span>Std Blk, Galv PE&T&C, XH Blk PE</span></a></h3>
                                    <p>Item #
                                        <br> Part #
                                        <br> UPC
                                        <br> Customer Alias</p>
                                    <p>Qty: 1</p>
                                </div>
                            </div>
                            <?php for ($i=0; $i < 10; $i++) { 
                                echo "
                                    <div class='media media-product'>
                                        <div class='media-left'>
                                            <div class='image'>
                                                <a href='#''>
                                                    <img class='media-object' src='assets/images/pipe.png' alt='...'>
                                                </a>
                                            </div>
                                        </div>
                                        <div class='media-body'>
                                            <h3 class='media-heading'><a href='#'><span>Std Blk, Galv PE&T&C, XH Blk PE</span></a></h3>
                                            <p>Item # <br>
                                               Part # <br>
                                               UPC <br>
                                               Customer Alias</p>
                                            <p>Qty: 1</p>
                                        </div>
                                    </div>
                                ";
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="#" class="btn btn-default btn-lg" data-dismiss="modal">Close</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="addToMaterialListSelect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-add-playlist">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-add-playlist"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body" id="add-to-material-list-body1">
                <h2 class="modal-title">Add to Material List</h2>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="form-group col-xs-8 col-xxs-12">
                                <select name="" id="sareList" class="bootstrap-select" data-width="100%" title='Select Existing Material List'>
                                    <option value="">List Name #2</option>
                                    <option value="">List Name #3</option>
                                    <option value="">List Name #5</option>
                                </select>
                            </div>
                            <div class="col-xs-4 col-xxs-12">
                                <a href="#collapseNewMaterial" class="btn btn-block btn-lg btn-warning">Add to List</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <a class="btn create-new-list-btn btn-link gray-noreload btn-nopadding" role="button" data-toggle="collapse" href="#collapseNewMaterial" aria-expanded="false" aria-controls="collapseNewMaterial" data-parent="#accordion">
                                    <span>Create New Material List</span>
                                </a>
                                <div class="create-new-list-collapse collapse" id="collapseNewMaterial">
                                    <hr>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Enter New Material List Name (50 Characters Max)" maxlength="50">
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="" cols="4" rows="5" placeholder="Enter Optional List Description (125 Characters Max)" maxlength="125"></textarea>
                                    </div>
                                    <ul class="list-inline pull-left create-list-actions-modal">
                                        <li>
                                            <a href="#" class="btn btn-warning btn-lg">Create New List</a>
                                        </li>
                                        <li>
                                            <a class="btn btn-default btn-lg cancel-create-new-list-btn" role="button" data-toggle="collapse" href="#collapseNewMaterial" aria-expanded="false" aria-controls="collapseExample">Cancel</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="#" class="btn btn-default btn-lg" data-dismiss="modal">Close</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Added to material List -->
<div class="modal fade" id="addedToMaterialList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-add-playlist">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-add-playlist"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body" id="add-to-material-list-body2" style="">
                <h2 class="modal-title">
                    2 Items Added to Material List
                </h2>
                <div class="media media-product">
                            <div class="media-left">
                                <div class="image">
                                    <a href="#">
                                        <!-- <img class="media-object" src="assets/images/pipe.png" alt="..."> -->
                                        <img class="media-object" src="https://images.tradeservice.com/5Q7ANIVTFBR62EKJ/THUMBNAILS/DIR100037/ANVILIE00057_85_PE_001TH.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="media-body">
                                <!-- <h3 class="media-heading"><a href="#"><span>Std Blk, Galv PE&amp;T&amp;C, XH Blk PE</span></a></h3> -->
                                <!-- <p>Item #
                                    <br> Part #
                                    <br> UPC
                                    <br> Customer Alias</p>
                                <p>Qty: 1</p> -->
                                <p>Added to Material List: <strong>test</strong></p>
                                <h3>
                                    <a href="#">BN6712 43566 654654 Pipe</a>
                                </h3>
                                <p>
                                    Mfr. # 0330062209
                                    <br> Item # 1605
                                    <br> UPC 690291836619
                                    <br>
                                </p>
                                <p>Qty: 2</p>
                                <a class="btn btn-nopadding btn-underline" href="#">
                                    <span>View Material List</span>
                                </a>
                            </div>
                        </div>
            </div>
            <div class="modal-footer">
                        <a href="#" class="btn btn-default btn-block btn-lg visible-xs" data-dismiss="modal">Close</a>
            </div>
            
        </div>
    </div>
</div>
<!-- Edit Name and Description -->
<div class="modal fade" id="myModalEditDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-write-1">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-write-1"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Edit Name and Description</h2>
                <div class="row">
                    <div class="form">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control input-lg" value="NW Hospital job #2">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea class="form-control input-lg" name="" id="" cols="4" rows="5">This is a job is for NW Hospital and is managed by John Smith and his team. We can add more detail but not over 125 characters for best fit in the space.</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Save Edits</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Create New Material List -->
<div class="modal fade" id="myModalListNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-write-1">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-write-1"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Create New Material List</h2>
                <div class="row">
                    <div class="form">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" class="form-control input-lg" placeholder="Enter New Material List Title (50 Characters Max)" value="">
                                <br>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea class="form-control input-lg" placeholder="Enter Optional Material List Description (125 Characters Max)" name="" id="" cols="4" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline create-list-actions-modal">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Create New Material List</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- reject Order Request -->
<div class="modal fade" id="rejectOrderRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Reject Order Request</h2>
                <div class="row">
                    <div class="form">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <p>Are you shure you want to reject this Order Request?</p><br>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <textarea class="form-control input-lg" placeholder="Optional Message (125 Characters Max)" name="" id="" cols="4" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline create-list-actions-modal">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Confirm</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Share list -->
<div class="modal fade" id="myModalShareList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-write-1">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-write-1"></use>
                    </svg>
                    <!-- <span class="glyphicon glyphicon-th-list"></span> -->
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Share Material List</h2>
                <div class="row">
                    <div class="form">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <select name="" id="sareList" class="form-control input-lg bootstrap-select mb-sm">
                                    <option value="">Share with</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <div class="row smaller">
                                    <div class="col-xs-6 col-xxs-6">
                                        <a href="#" class="btn btn-lg btn-warning btn-block btn-nopadding">Share</a>
                                    </div>
                                    <div class="col-xs-6 col-xxs-6">
                                        <a href="#" class="btn btn-lg btn-default btn-block btn-nopadding" data-dismiss="modal">Cancel</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Share</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<!-- Delete Material List -->
<div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Delete Material List</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <span>Are you sure you want to delete this Material List?</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Proceed</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- No List Found -->
<div class="modal fade" id="noResults" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">No Material Lists Found</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <!-- <p>Are you sure you want to delete this Material List?</p> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline one-mobile">
                            <!-- <li>
                <a href="#" class="btn btn-lg btn-warning">Proceed</a>
              </li> -->
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Close</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- confirm Modal on View Address Page -->
<div class="modal fade" id="addressConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-tick">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-tick"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Success</h2>
                <p>Your default Ship to Address has been changed.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-lg btn-default btn-block" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!--  Current Statement Sent -->
<div class="modal fade" id="requestStatement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <i class="fa fa-file-text-o"></i>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Current Statement Sent</h2>
                <p>A copy of your current statement will be sent to <a href="#">johnsmith@cps.com</a>.</p>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-lg btn-default btn-lg pull-left" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
<!--  Order Pad Modal-->
<div class="modal fade" id="orderPadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-power">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-power"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body modal-import-tab">
                <h2 class="modal-title">Order Pad</h2>
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="importTabs">
                        <li role="presentation" class="active"><a href="#manualEntry" aria-controls="manualEntry" role="tab" data-toggle="tab">Manual Entry</a></li>
                        <li role="presentation"><a href="#importUpload" aria-controls="importUpload" role="tab" data-toggle="tab">Import/Upload List</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="manualEntry">
                            <table class="table table-striped table-order-pad">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" class="form-control" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" class="form-control" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" class="form-control" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" class="form-control" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" class="form-control" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" class="form-control" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-sm-7 col-xs-7">
                                        <ul class="list-inline two-mobile">
                                            <li>
                                                <a href="#" class="btn btn-lg btn-warning btn-lg">Confirm</a>
                                            </li>
                                            <li>
                                                <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-5 col-xs-5 text-right">
                                        <ul class="list-inline">
                                            <li class="more-order-pad">
                                                <button class="btn btn-link gray-noreload btn-nopadding"><i class="fa fa-plus"></i> <span>Add More Items</span></button>
                                            </li>
                                            <!-- <li class="download-order-pad">
                                                <button class="btn btn-link gray-noreload btn-nopadding"><i class="fa fa-download"></i> <span>Download Spreadsheet Template</span></button>
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="importUpload">
                            <br>
                            <div class="kv-main">
                                <form enctype="multipart/form-data">
                                    <!-- <div class="form-group">
                                        <input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1">
                                    </div> -->
                                    <div class="form-group">
                                        <!--input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1"-->
                                        <label class="custom-file-upload">
                                            <span class="btn btn-lg btn-default">Choose File</span>
                                            <mark>No file chosen</mark>
                                            <input type="file" multiple data-overwrite-initial="false" data-min-file-count="1">
                                        </label>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-sm-7 col-xs-7 col-xxs-12">
                                        <ul class="list-inline two-mobile">
                                            <li>
                                                <a href="#" class="btn btn-lg btn-warning btn-lg">Upload</a>
                                            </li>
                                            <li>
                                                <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-5 col-xs-5 col-xxs-12 text-right">
                                        <ul class="list-inline">
                                            <!-- <li class="more-order-pad">
                                                <button class="btn btn-link gray-noreload btn-nopadding"><i class="fa fa-plus"></i> <span>Add More Items</span></button>
                                            </li> -->
                                            <li class="download-order-pad">
                                                <button class="btn btn-link gray-noreload btn-nopadding"><i class="fa fa-download"></i> <span>Download Spreadsheet Template</span></button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  View All Manufacturer Option Modal -->
<div class="modal fade" id="viewAllManufacturerOptions" tabindex="-1" role="dialog" aria-labelledby="viewAllManufacturerOption" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-box">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-box"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Manufacturers</h2>
                <nav>
                    <ul class="pagination pagination-abc">
                        <li class="active"><a href="#">A</a></li>
                        <li><a href="#">B</a></li>
                        <li><a href="#">C</a></li>
                        <li><a href="#">D</a></li>
                        <li><a href="#">E</a></li>
                        <li><a href="#">F</a></li>
                        <li><a href="#">G</a></li>
                        <li><a href="#">H</a></li>
                        <li><a href="#">I</a></li>
                        <li><a href="#">J</a></li>
                        <li><a href="#">K</a></li>
                        <li><a href="#">L</a></li>
                        <li><a href="#">M</a></li>
                        <li><a href="#">N</a></li>
                        <li><a href="#">O</a></li>
                        <li><a href="#">P</a></li>
                        <li><a href="#">Q</a></li>
                        <li><a href="#">R</a></li>
                        <li><a href="#">S</a></li>
                        <li><a href="#">T</a></li>
                        <li><a href="#">U</a></li>
                        <li><a href="#">V</a></li>
                        <li><a href="#">W</a></li>
                        <li><a href="#">X</a></li>
                        <li><a href="#">Y</a></li>
                        <li><a href="#">Z</a></li>
                    </ul>
                </nav>
                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Manufacturer</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left two-mobile">
                    <li>
                        <a href="#" class="btn btn-lg btn-warning btn-lg">Select</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Close</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Duplicate Matches Found -->
<div class="modal fade" id="duplicateMatches" tabindex="-1" role="dialog" aria-labelledby="duplicateMatches" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Duplicate Matches Found</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>The items in each row share the same Item # and Alias #. Select the one item in each row you wish to add to your cart. </p>
                    </div>
                </div>
                <div class="dublicate-list form-group">
                    <div class="scrollable">
                        <div class="content scrollbar-inner">
                            <div class="col-lg-12 grid">
                                <div class="row item">
                                    <div class="col-md-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                </div>
                                <div class="row item">
                                    <div class="col-md-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                </div>
                                <div class="row item">
                                    <div class="col-md-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <a href="#" class="btn btn-lg btn-warning">Add Selected to Cart</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Close</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Shipping, Tax, and Other Charges -->
<div class="modal fade" id="infoShipTaxOther" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-cercle-7">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-cercle-7"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Shipping, Tax, and Other Charges</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>An Order Acknowledgment with shipping, tax, and other potential charges will be emailed to you once your order has been completed. Or, you may go to the Order History page and select the "Acknowledgment PDF" from the list.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline one-mobile">
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Okay</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Remove Selected From Cart -->
<div class="modal fade" id="removeFromCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Remove Selected From Cart</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>Are you sure you want to remove the selected items from your cart?
                            <br>This cannot be undone. </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <button class="btn btn-lg btn-warning btn-lg">Confirm</button>
                            </li>
                            <li>
                                <button class="btn btn-lg btn-default" data-dismiss="modal">Cancel</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Driving Directions -->
<div class="modal fade" id="drivingDirections" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-location">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-location"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Driving Directions</h2>
                <div class="setDirections">
                    <form class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" id="adressStart" placeholder="Start Address">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control input-lg" id="adressDestination" placeholder="Destination Address" value="900 Industrial Avenue, Rock Falls, Illinois 61071">
                            </div>
                            <ul class="list-inline">
                                <li>
                                    <button class="mapIt btn btn-lg btn-warning btn-lg">Map It</button>
                                </li>
                                <li>
                                    <button class="btn btn-lg btn-default" data-dismiss="modal">Cancel</button>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="showDirections hidden">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" class="toAddress"><i class="fa fa-arrow-left"></i> Edit Start Address</a>
                        </div>
                        <div class="col-xs-12">
                            <div class="address-list">
                                <img src="assets/images/directions1-3.png" alt="" />
                            </div>
                            <div class="direction-map">
                                <img src="assets/images/map5-3.png" alt="" />
                            </div>
                            <ul class="list-inline">
                                <li>
                                    <button type="submit" class="btn btn-lg btn-warning btn-lg">Print</button>
                                </li>
                                <li>
                                    <button class="btn btn-lg btn-default" data-dismiss="modal">Cancel</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Clear All -->
<div class="modal fade" id="clearOrderPad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Clear All</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>Are you sure you want to clear all entries?</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline">
                            <li>
                                <button class="btn btn-lg btn-warning btn-lg">Confirm</button>
                            </li>
                            <li>
                                <button class="btn btn-lg btn-default" data-dismiss="modal">Cancel</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- All or Some Items Not Added to Cart Dublicate Matches-->
<div class="modal fade duplicate-matches" id="itemsNotAddedtoCart" tabindex="-1" role="dialog" aria-labelledby="itemsNotAddedtoCart" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">All or Some Items Not Added to Cart</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>The following items were <strong>NOT</strong> added ot your cart because you entered an invalid Item # or Alias # or these items are no longer available:</p>
                        <p>012345
                            <br> 012345
                            <br/> 012345
                        </p>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-xs-12">
                        <h3>Duplicate Matches Found</h3>
                        <p>The following items in each row share the same Item # and Alias #. Select one item in each row to add to your cart. </p>
                        <br/>
                    </div>
                </div>
                <div class="dublicate-list form-group">
                    <div class="scrollable">
                        <div class="content scrollbar-inner">
                            <div class="col-lg-12 grid">
                                <div class="row item">
                                    <div class="col-xs-6 col-xxs-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                    <div class="col-xs-6 col-xxs-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                </div>
                                <div class="row item">
                                    <div class="col-xs-6 col-xxs-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                    <div class="col-xs-6 col-xxs-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                </div>
                                <div class="row item">
                                    <div class="col-xs-6 col-xxs-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                    <div class="col-xs-6 col-xxs-6">
                                        <div class="radio set-default">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Select
                                                <span class="img-label">
                                                        <img src="assets/images/water-hitters.png" alt="">
                                                    </span>
                                            </label>
                                        </div>
                                        <h5>TD BLK, GALV PE& T&C, XH BLK PE</h5>
                                        <div class="price"><span>$000.00</span>/unit/pack info</div>
                                        <p>
                                            Mfr. #
                                            <br> Item # 012345
                                            <br> UPC
                                            <br> Alias
                                            <br>
                                        </p>
                                        <p>Brief description taking up oly about 2-3 lines of copy to keep the integrity of the space. </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <a href="#" class="call-alert btn btn-lg btn-warning" data-dismiss="modal">Add Selected to Cart</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-lg btn-default" data-toggle="modal" data-target="#displayErrorMsg">Close</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- All or Some Items Not Added to Cart -->
<div class="modal fade" id="itemsAddtoCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">All or Some Items Not Added to Cart</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>The following items were NOT added ot your cart because you entered an invalid Item # or Alias # or these items are no longer available:</p>
                        <p>012345
                            <br/> 012345
                            <br/> 012345
                        </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline one-mobile">
                            <li>
                                <button class="call-alert btn btn-lg btn-default" data-dismiss="modal">Okay</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Move to Active Cart -->
<div class="modal fade" id="movetoActiveCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Move to Active Cart</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>Moving a Saved Cart to the Active Cart will update pricing. Do you want to continue?</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <button class="btn btn-lg btn-warning" data-dismiss="modal">Confirm and Go to Cart</button>
                            </li>
                            <li>
                                <button class="btn btn-lg btn-default" data-dismiss="modal">Cancel</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Delete Saved Cart -->
<div class="modal fade" id="deleteSavedCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Delete Saved Cart</h2>
                <div class="row">
                    <div class="col-sm-12">
                        <p>Are you sure you want to delete this Saved Cart? </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-left">
                        <ul class="list-inline two-mobile">
                            <li>
                                <button class="btn btn-lg btn-warning" data-dismiss="modal">Confirm</button>
                            </li>
                            <li>
                                <button class="btn btn-lg btn-default" data-dismiss="modal">Cancel</button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Resubmit Order Request -->
<div class="modal fade" id="resubmitOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Resubmit Order Request</h2>
                <p>Are you sure you want to resubmit this Order Request?</p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left two-mobile">
                    <li>
                        <a href="#" class="btn btn-lg btn-warning btn-lg">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Edit Order Request -->
<div class="modal fade" id="editOrderRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-tick">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-tick"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Edit Order Request</h2>
                <p>A NEW Cart will be created with all items and details from this Order Request. You can make edits to this Cart and submit your order. </p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="cart.php" class="btn btn-lg btn-warning btn-lg">Confirm and Go to Cart</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Delete Order Request -->
<div class="modal fade" id="deleteRequest" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Delete Order Request</h2>
                <p>Are you sure you want to delete this Order Request? </p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left two-mobile">
                    <li>
                        <a href="cart.php" class="btn btn-lg btn-warning btn-lg">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- View Order Modal -->
<div class="modal fade" id="viewOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <div class="row smaller equalizeWebOrder">
                    <div class="col-xs-8 col-xxs-12">
                        <div class="well well-gray well-view-order-detail equalItem">
                            <div class="row">
                                <div class="col-sm-12">
                                    <big><strong>Web Confirmation Order # 00123456</strong></big>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-xxs-12">
                                    <h4>Ship to Address</h4>
                                    <address>
                                        NW Hospital — 17th Floor, Imaging
                                        <br> Address Line 2
                                        <br> Address Line 3
                                        <br>
                                    </address>
                                </div>
                                <div class="col-sm-6 col-xs-6 col-xxs-12">
                                    <h4>Billing Address</h4>
                                    <address>
                                        <p>Company ABC
                                            <br> 454 State Street
                                            <br> Chicago IL, 60630</p>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-xxs-12">
                        <div class="well well-view-order-detail-summary equalItem">
                            <h4>Summary</h4>
                            <p>PO#: <strong>00123</strong>
                                <br> # Items: <strong>5</strong>
                                <br> Subtotal: <strong>$2,443.00</strong></p>
                        </div>
                    </div>
                </div>
                <div class="row smaller">
                    <div class="col-sm-12">
                        <div class="material-items-list items-ordered-modal preload-cover">
                            <div class="header">
                                <div class="check">
                                    &nbsp;
                                </div>
                                <div class="name">
                                    <span class="title"><strong>Description</strong></span>
                                </div>
                                <div class="item-number">
                                    <span class="title"><strong>Item # <i class="fa fa-sort"></i></strong></span>
                                </div>
                                <div class="qty">
                                    <span class="title"><strong>Qty</strong></span>
                                </div>
                                <div class="price">
                                    <span class="title"><strong>Unit Price <i class="fa fa-sort"></i></strong></span>
                                </div>
                                <div class="alias">
                                    <span class="title"><strong>Total Price</strong></span>
                                </div>
                                <div class="availability">
                                    <span class="title"><strong>Availability</strong></span>
                                </div>
                            </div>
                            <div class="content-list scrollbar-inner">
                                <div class="content">
                                    <div class="check fake">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </div>
                                    <div class="name">
                                        <a class="product-title" href="#"><small>Name of product</small></a>
                                    </div>
                                    <div class="item-number">
                                        <span class="caption">Item #</span>4356346
                                    </div>
                                    <div class="qty">
                                        <span class="caption">Qty</span>3
                                    </div>
                                    <div class="price">
                                        <span class="caption">Unit Price</span>$1230.60
                                    </div>
                                    <div class="alias">
                                        <span class="caption">Total Price</span>$535
                                    </div>
                                    <div class="availability">
                                        <span class="caption">Availability</span>25
                                    </div>
                                </div>
                                <?php 
                                for ($i=0; $i < 10; $i++) { 
                                     echo "
                                            <div class='content'>
                                                <div class='check fake'>
                                                    <img src='assets/images/water-hitters.png' alt=''>
                                                </div>
                                                <div class='name'>
                                                    <a class='product-title' href='#''><small>Name of product</small></a>
                                                </div>
                                                <div class='item-number'>
                                                    <span class='caption'>Item #</span>4356346
                                                </div>
                                                <div class='qty'>
                                                    <span class='caption'>Qty</span>3
                                                </div>
                                                <div class='price'>
                                                    <span class='caption'>Unit Price</span>$1230.60
                                                </div>
                                                <div class='alias'>
                                                    <span class='caption'>Total Price</span>$535
                                                </div>
                                                <div class='availability'>
                                                    <span class='caption'>Availability</span>25
                                                </div>
                                            </div>
                                     ";
                                 } ?>
                            </div>
                            <div class="preload-bg"></div>
                            <div class="preload-spinner">
                                <i class="fa fa-spinner fa-pulse"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left one-mobile">
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Close</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- approve selected pending orders -->
<div class="modal fade" id="approveSelectedPendingOrders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Approve Selected Order Requests</h2>
                <p>Are you sure you want to approve selected order requests? </p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="" class="btn btn-lg btn-warning btn-lg">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- approve selected pending order -->
<div class="modal fade" id="approveSelectedPendingOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Approve Selected Order</h2>
                <p>Are you sure you want to approve this order request? </p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="" class="btn btn-lg btn-warning btn-lg">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- reject order request -->
<div class="modal fade" id="rejectOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Reject Order Request</h2>
                <p>Are you sure you want to reject this Order Request?</p>
                <textarea name="" id="" cols="10" rows="5" class="form-control" placeholder="Optional Message (125 Characters Max)"></textarea>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="" class="btn btn-lg btn-warning btn-lg">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- reject selected pending orders -->
<div class="modal fade" id="rejectSelectedPendingOrders" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Reject Selected Order Requests</h2>
                <p>Are you sure you want to reject selected order requests? </p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="" class="btn btn-lg btn-warning btn-lg">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- access customer account -->
<div class="modal fade" id="accessCustomerAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Access Customer Account</h2>
                <p>Are you sure you want to access this customer's account?
                    <br>Changes you make may impact this customer's account.</p>
            </div>
            <div class="modal-footer">
                <div class="clearfix">
                    <ul class="list-inline pull-left clearfix">
                        <li>
                            <a href="my-account-admin-access.php" class="btn btn-lg btn-warning btn-lg">Continue</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                        </li>
                    </ul>
                </div>
                <div class="footer-checkbox">
                    <div class="checkbox" id="myCheckbox">
                        <label class="checkbox-custom" data-initialize="checkbox">
                            <input class="sr-only" type="checkbox" value="">
                            <span class="checkbox-label">Don't show this message again</span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #2A-  Message Modal -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Alert Messsage Header</h2>
                <p>Description.</p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Okay</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- #2B — Confirm Selection Modal -->
<div class="modal fade" id="confirmSelectionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <!-- <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg> -->
                    <span class="glyphicon glyphicon-ok"></span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Confirm Message Header</h2>
                <p>Description.</p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="#" class="btn btn-lg btn-warning btn-lg" data-dismiss="modal">Confirm</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Permission Denied -->
<div class="modal fade" id="permissionDenied" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                    <!-- <span class="glyphicon glyphicon-ok"></span> -->
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Permission Denied</h2>
                <p>Sorry, you do not currently have privileges for this action. Contact the Administrator for more information.</p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Okay</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Share Page -->
<div class="modal fade" id="sharePage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-massage">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-massage"></use>
                    </svg>
                    <!-- <span class="glyphicon glyphicon-ok"></span> -->
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <h2 class="modal-title">Email this Page</h2>
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-lg-12">
                            <textarea class="form-control" cols="3" rows="4" placeholder="Recipient Email Address: (separate miltiple recipient email addresses with commas)"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4"><input type="text" class="form-control mb-sm" placeholder="Your First Name"></div>
                        <div class="col-sm-4"><input type="text" class="form-control mb-sm" placeholder="Your Last Name"></div>
                        <div class="col-sm-4 has-error">
                            <input type="text" class="form-control" placeholder="Your Email Address">
                            <small class="text-danger">Please Enter Valid Email</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <textarea class="form-control" cols="3" rows="4" placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div class="checkbox" id="myCheckbox">
                              <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label">Email me a copy</span>
                              </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <ul class="list-inline two-mobile pull-left">
                    <li>
                        <a href="#" class="btn btn-lg btn-warning btn-lg" data-dismiss="modal">Email this Page</a>
                    </li>
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- #displayErrorMsg -->
<!-- Display error on All or Some Items Not Added to Cart modal -->
<div class="modal fade" id="displayErrorMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-missing">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
                    </svg>
                    <!-- <span class="glyphicon glyphicon-ok"></span> -->
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
            <div class="modal-body">
                <!-- <h2 class="modal-title">Err</h2> -->
                <p>Please Select One Item in each row to add to your Material List.</p>
            </div>
            <div class="modal-footer">
                <ul class="list-inline pull-left">
                    <!-- <li>
                        <a href="#" class="btn btn-lg btn-warning btn-lg" data-dismiss="modal">Email this Page</a>
                    </li> -->
                    <li>
                        <a href="#" class="btn btn-lg btn-default btn-block" data-dismiss="modal">Ok</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>