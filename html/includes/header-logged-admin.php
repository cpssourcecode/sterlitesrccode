<?php include 'assets/images/subway-icons/icons-svgdefs.svg'; ?>

<header class="admin-access">
        <div class="container container-panel">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="context">
                            <p><i class="fa fa fa-exclamation-triangle"></i> <strong>You are Accessing Customer Account:</strong> ABC Company, Inc. Account # 0123456</p>
                        </div>
                        <a href="my-account.php" class="btn btn-default btn-sm">Stop Access</a>
                    </div>
                </div>
            </div>
        </div>
    <div class="login-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-right">
                    <div class="head-info">Welcome: <a href="#">Robert, Allied Mechanical</a></div>
                    <div class="head-info with-dropdown"><span class="hidden-xs text">| </span> <span class="text">Ship To:</span>
                        <div class="btn-group login-box-select">
                            <a data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="text">NW Hospital — 17th Floor, Imaging</span> <span class="caret"></span></a>
                            <ul class="dropdown-menu header-ship">
                                <button class="close"><i class="fa fa-close"></i></button>
                                <li class="current">
                                    <span>Current Ship To Address</span>
                                    <p>
                                        <span class="addr-title">
                                            NW Hosp — 17th Floor, Imaging
                                        </span>
                                        <span class="addr1">
                                            Address Line 1
                                        </span>
                                        <span class="addr2">
                                            Address LIne 2
                                        </span>
                                    </p>
                                </li>
                                <li class="divider"></li>
                                <li class="current">
                                    <span>Select New Ship To Address for this Session</span>
                                </li>
                                <li>
                                    <label>
                                        <span class="addr-title">NW Hosp — 17th Floor, Imaging</span>
                                        <span class="addr1">Address Line 1</span>
                                        <span class="addr2">Address Line 2</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <span class="addr-title">NW Hosp — 17th Floor, Imaging More Larger text</span>
                                        <span class="addr1">Address Line 12</span>
                                        <span class="addr2">Address Line 22</span>
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <span class="addr-title">NW Hosp — 17th Floor</span>
                                        <span class="addr1">Address Line 13</span>
                                        <span class="addr2">Address Line 23</span>
                                    </label>
                                </li>
                                <li><a href="#"><span>View All</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="head-info"><span class="hidden-xs">|</span> Sales Team: <a href="#">800-000-0000</a></div>
                    <a href="home.php" class="log-btn pull-right"><span>Log Out</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="brand-box">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 logo-box">
                    <a href="index.php" class="logo"><img src="assets/images/logo.png" alt=""></a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 user-actions-box">
                    <ul class="list-unstyled pull-right">
                        <li class="text-right hidden-xs">
                        <a class="link" href="#">Our Business</a>
                        <span class="line">|</span>
                        <a class="link" href="#" data-toggle="modal" data-target="#selectShipAddress">Locations</a>
                        <span class="line">|</span>
                        <a class="link" href="#">Contact</a>
                        </li>
                        <li class="text-right dropdowns">
                            <div class="dropdown">
                                <a class="btn btn-default btn-lg btn-services" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Services
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-services pull-right" role="menu" aria-labelledby="dLabel">
                                    <li><a href="#">Custom Product Service and Support</a></li>
                                    <li><a href="#">Inventory Solutions</a></li>
                                    <li><a href="#">Kitting</a></li>
                                    <li><a href="#">Standardisation</a></li>
                                    <li><a href="#">CAPS Valve Automation</a></li>
                                    <li><a href="#">Inter-Branch Trans-fer System - Stock Solutions</a></li>
                                </ul>
                            </div>
                            <div class="dropdown account">
                                <a class="btn btn-default btn-lg btn-services" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    My Account
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-services pull-right" role="menu" aria-labelledby="dLabel">
                                    <li><a href="#"><strong>My Account Overview</strong> <span class="glyphicon glyphicon-chevron-right text-warning"></span></a></li>
                                    <div class="row dropdown-grid dropdown-account">
                                        <div class="col-sm-6 col-xs-12">
                                                <ul>
                                                    <li><p><strong>Manage Orders</strong></p></li>
                                                    <li><a href="#">Order History</a></li>
                                                    <li><a href="#">Order Requests <span class="text-danger">(5)</span></a></li>
                                                    <li><a href="#">Packing Slips</a></li>
                                                    <li><p><strong>Admin Tools</strong></p></li>
                                                    <!-- <li><a href="#">Company Settings</a></li> -->
                                                    <li><a href="#">Manage Users</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <ul>
                                                    <li><p><strong>Billing and Payment</strong></p></li>
                                                    <li><a href="#">View Invoices</a></li>
                                                    <li><a href="#">View Statements</a></li>
                                                    <li><p><strong>Lists</strong></p></li>
                                                    <li><a href="#">My Material Lists</a></li>
                                                    <li><a href="#">Saved Carts <span class="text-danger">(3)</span></a></li>
                                                    <li><p><strong>My Profile</strong></p></li>
                                                    <!-- <li><a href="#">My Material Lists</a></li> -->
                                                    <li><a href="#">View Addresses</a></li>
                                                    <li><a href="view-profile.php">Edit Profile</a></li>
                                                </ul>
                                            </div>
                                    </div>
                                </ul>
                            </div>
                            <div class="dropdown">
                                <a class="btn btn-default btn-lg btn-services pull-righ" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Quick Tools
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-services pull-left" role="menu" aria-labelledby="dLabel">
                                    <li><a href="#" data-toggle="modal" data-target="#orderPadModal">Order Pad</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#findOrder">Find an Order</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#findInvoice">Find an Invoice</a></li>
                                    <li><a href="#">Check Price and Availability</a></li>
                                    <li><a href="#">My Material Lists</a></li>
                                    <li class="hidden-xs hidden-xxs"><a href="#" data-toggle="modal" data-target="#requestQuote">Request Quote</a></li>
                                    <li><a href="#" data-toggle="modal" data-target="#findPackingSlip">Find Packing Slip</a></li>
                                    <li><a href="#">Find Material Test Report </a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-inverse">
        <div class="container">
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse navbar-menu">
                <ul class="nav navbar-nav navbar-menu">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle mainmenu-trigger" data-toggle="dropdown" role="button" aria-expanded="false"><span class="visible-xs glyphicon glyphicon-align-justify"></span><span class="hidden-xs">Products</span> <span class="caret hidden-xs"></span></a>
                        <ul class="dropdown-menu mainmenu-dropdown" role="menu">
                            <li class="dropdown-submenu dropdown-hover-trigger dropdown-hover">
                                <a href="#" tabindex="0" data-toggle="dropdown" aria-expanded="false">Carbon Steel Pipe</a>
                                <ul class="dropdown-menu dropdown-second-level">
                                    <li><a href="#">Domestic Seamless Pipe 2-12</a></li>
                                    <li><a href="#">Domestic OD SMLS 14-24 Pipe</a></li>
                                    <li><a href="#">Pressure Pipe A106 & Global</a></li>
                                    <li><a href="#">Fusion Bond Pipe</a></li>
                                    <li><a href="#">Kottler HSC Items</a></li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu dropdown-hover-trigger dropdown-hover">
                                <a href="#" tabindex="0" data-toggle="dropdown" aria-expanded="false">SS Pipe & Fitting</a>
                                <ul class="dropdown-menu dropdown-second-level">
                                    <li><a href="#">Domestic Seamless Pipe 2-12</a></li>
                                    <li><a href="#">Domestic OD SMLS 14-24 Pipe</a></li>
                                    <li><a href="#">Pressure Pipe A106 & Global</a></li>
                                    <li><a href="#">Fusion Bond Pipe</a></li>
                                    <li><a href="#">Kottler HSC Items</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Plastic Pipe & Fittings</a></li>
                            <li><a href="#">Soli Pipe & Fittings</a></li>
                            <li><a href="#">Fittings & Nipples</a></li>
                            <li><a href="#">Hangers/Rood/Strut</a></li>
                            <li><a href="#">Weld Fittings & Flanges</a></li>
                            <li><a href="#">Valves</a></li>
                            <li><a href="#">Plumbing Fixtures</a></li>
                            <li><a href="#">Plumbing Brass</a></li>
                            <li><a href="#">Water Heters</a></li>
                            <li><a href="#">Pumps</a></li>
                            <li><a href="#">Hydronic Equipment</a></li>
                            <li><a href="#">Hydronic Misc.</a></li>
                            <li><a href="#">HVAC Equipement</a></li>
                            <li><a href="#">MVAC Assoc. Products</a></li>
                            <li><a href="#">Mainteance Supplies</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control input-lg typeahead-main" placeholder="Search by keyword, item #" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <!-- Test implementation on html, remove data attributes for your backend implementation -->
                        <ul class="dropdown-menu dropdown-typeahead">
                            <li><span class="title">Category Suggestions:</span></li>
                            <li><a href="#">Buttwed <strong>Pipe</strong></a></li>
                            <li><a href="#">Buttwed <strong>Pipe</strong> Domestic</a></li>
                            <li><a href="#">Buttwed <strong>Pipe</strong> Global</a></li>
                            <li role="separator" class="divider"></li>
                            <li><span class="title">Items Suggestions:</span></li>
                            <li><a href="#">Stainless Steel Reducer, Concentric, 1 inch × 1/2 inch, Sched</a></li>
                            <li><a href="#">Stainless Steel Reducer, Concentric, 1 inch × 1/2 inch, Sched</a></li>
                            <li><a href="#">Stainless Steel Reducer, Concentric, 1 inch × 1/2 inch, Sched</a></li>
                        </ul>
                    </div>
                    <button type="submit" class="btn btn-warning btn-lg"><i class="glyphicon glyphicon-search"></i></button>
                </form>
                <ul class="nav navbar-nav navbar-right navbar-cart">
                    <li><a href="#"><span class="hidden-xs">Cart</span> <span class="glyphicon glyphicon-shopping-cart text-warning"></span> (48)</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
</header>
