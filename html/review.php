<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Cart</a></li>
                        <li class="active">Review</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions hidden-print">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12 hidden-print">
                    <div class="carts-steps">
                        <a href="#" class="step">
                            <span class="step-number">1.</span>
                            <span class="step-label">Cart</span>
                        </a>
                        <a href="#" class="step is-active">
                            <span class="step-number">2.</span>
                            <span class="step-label">Review</span>
                        </a>
                        <a href="#" class="step">
                            <span class="step-number">3.</span>
                            <span class="step-label">Confirmation</span>
                        </a>
                    </div>
                </div>
                <div class="cart-page review-step">
                    <div class="col-xs-12">
                        <div class="important-message panel-group">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <div class="nolink">
                                            <i class="fa fa-exclamation-triangle"></i>
                                            This order must be submitted for approval.
                                        </div>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="cart-info col-xs-12">
                        <div class="col-md-9 col-sm-8 col-xs-8 col-xxs-12 ship-info">
                            <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                <h4>Ship To Address</h4>
                                <p>NW Hospital — 17th Floor, Imaging <br>
                                   Address Line 2<br>
                                   Address Line 3<br>
                                    <br>
                                </p>
                                <div class="checkbox hidden-print" id="myCheckbox">
                                    <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label">Include Materials Test Report(s) with shipment for applicable items.</span>
                                    </label>
                                </div>
                                <br>
                                <div class="error-comment hidden-print">
                                    <i class="fa fa-exclamation-triangle"></i>
                                    <p><em>Orders placed after 3pm are not guaranteed to be processed the same
                                           business day.</em></p>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                <h4>Billing Address</h4>
                                <p>
                                    Company ABC <br>
                                    454 State Street <br>
                                    Chicago IL <br>
                                    60630 <br>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 col-xs-4 col-xxs-12 summary">
                            <a href="#" class="btn btn-warning btn-lg btn-block hidden-print">Place Order</a>
                            <!--<a href="#" class="btn btn-warning btn-lg btn-block">Submit for Approval</a>-->
                            <div class="summary-block">
                                <h4>Summary</h4>
                                <span>PO #:</span> <strong class="po-value">001239043564642355323d325423</strong> <br>
                                # Items: <strong>5</strong> <br>
                                Subtotal: <strong>$2,443.00</strong>
                                <div class="info-comment">
                                    <span class="glyphicon glyphicon-alert text-danger"></span>
                                    <p><em>Information on shipping, tax, and other charges is not available until you complete your order.
                                        <a href="#infoShipTaxOther" data-toggle="modal"><strong>Read More</strong></a>.</em></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <br>
                        <div class="well well-review-transparent">
                                <span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
                                <strong class="text-danger">IMPORTANT:</strong>&nbsp;<strong>When checking availability</strong> quantity added to your cart can <strong>EXCEED</strong> quantity shown in the <strong>Availability</strong> column below. Exceeded quantities wil be placed on backorder. You can contact your Sales Team for information about backordered items.
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="cart-items-list">
                            <div class="header">
                                <div class="col check">

                                </div>
                                <div class="col desc">
                                    <span class="title"><strong>Description</strong></span>
                                </div>
                                <div class="col item-number">
                                    <span class="title"><strong>Item#</strong> <i class="fa fa-sort"></i></span>
                                </div>
                                <div class="col alias">
                                    <span class="title"><strong>Alias</strong> <i class="fa fa-sort"></i></span>
                                </div>
                                <div class="col qty">
                                    <span class="title"><strong>Qty</strong></span>
                                </div>
                                <div class="col u-price">
                                    <span class="title"><strong>Unit Price</strong> <i class="fa fa-sort"></i></span>
                                </div>
                                <div class="col t-price">
                                    <span class="title"><strong>Total Price</strong></span>
                                </div>
                                <div class="col availability">
                                    <span class="title"><strong>Availability</strong></span>
                                </div>
                            </div>
                            <div class="content-list">
                                <div class="content">
                                    <div class="col check">
                                        <div class="checkbox image">
                                            <span class="checkbox-label">
                                                <img src="assets/images/water-hitters.png" alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col desc">
                                        <a class="product-title pl0" href="#">Name of product</a>
                                    </div>
                                    <div class="col item-number">
                                        <span class="caption">Item#</span>000000
                                    </div>
                                    <div class="col alias">
                                        <span class="caption">Alias</span>000000
                                    </div>
                                    <div class="col qty">
                                        <span class="caption">Qty</span><input type="text" class="form-control" value="1" disabled>
                                    </div>
                                    <div class="col u-price">
                                        <span class="caption">Unit Price</span>$349.00
                                    </div>
                                    <div class="col t-price">
                                        <span class="caption">Total Price</span>$349.00
                                    </div>
                                    <div class="col availability">
                                        <span class="caption">Availability</span>25
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                    <div class="subtotal col-md-4 col-xs-12 pull-right text-right">
                        <h4>Subtotal: <strong class="text-success">$2,443.00</strong></h4>
                        <a href="#" class="btn btn-warning btn-lg hidden-print">Place Order</a>
                        <!--<a href="#" class="btn btn-warning btn-lg btn-block">Submit for Approval</a>-->
                    </div>

                </div>
            </div>
        </div>
        <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>