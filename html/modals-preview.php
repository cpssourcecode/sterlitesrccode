<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Modals Preview — CPS</title>
        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link href="assets/stylesheets/styles.css" rel="stylesheet">
        <link href="assets/stylesheets/common.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php include 'assets/images/subway-icons/icons-svgdefs.svg'; ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1><a href="index.php">←</a>&nbsp;Modals Preview Page</h1>
                    <hr>
                </div>
                
                <div class="col-xs-6">
                    <ul class="list-unstyled">
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#findInvoice">
                            Find Invoice Modal
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#findPackingSlip">
                            Find Packing Slip
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#findOrder">
                            Find Order
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#requestQuote">
                            Request a Quote
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#logIn">
                            Log In
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#requestAnAccount">
                            Request An Account
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#activateNewAccount">
                            Activate New Account
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#accessCustomerAccount">
                            Access Customer Account
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#forgotPassword">
                            Forgot Password
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#resetPassword">
                            Reset Password
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#resetPassword2">
                            Reset Password
                            </button>
                        (from emails templates)</li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#selectShipAddress">
                            Select a ship to address
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#changeShipAddress">
                            Change a ship to address
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#changePickupLocation">
                            Change Pickup Location
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#addShipToAddress">
                            Add Ship to Address
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#myCheckPrice">
                            Check Price and Availability
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#importUsers">
                            Import Users
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#exportUsers">
                            Export Users
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#reinviteUserModal">
                            Reinvite User Confirmation
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#myModalCheck">
                            Check Availability
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#addToMaterialList">
                            Add to Material List
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#addToMaterialListSelect">
                                Add to Material List (Select Box)
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#addedToMaterialList">
                                Added to Material List
                            </button>
                        </li>
                        
                    </ul>
                </div>
                <div class="col-xs-6">
                    <ul class="list-instyled">
                        
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#viewOrder">
                            View Order Modal
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#myModalEditDescription">
                            Edit Name and Description
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#myModalListNew">
                            Create New Material List
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#myModalShareList">
                            Share list
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#myModalDelete">
                            Delete Material List
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#noResults">
                            No List Found
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#addressConfirm">
                            Confirm Modal on View Address Page
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#requestStatement">
                            Current Statement Sent
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#orderPadModal">
                            Order Pad Modal
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#viewAllManufacturerOptions">
                            View All Manufacturer Option
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#duplicateMatches">
                            Duplicate Matches Found
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#infoShipTaxOther">
                            Shipping, Tax, and Other Charges
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#removeFromCart">
                            Remove Selected From Cart
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#drivingDirections">
                            Driving Directions
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#clearOrderPad">
                            Clear All
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#itemsNotAddedtoCart">
                            All or Some Items Not Added to Cart Dublicate Matches
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#itemsAddtoCart">
                            All or Some Items Not Added to Cart
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#movetoActiveCart">
                            Move to Active Cart
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#deleteSavedCart">
                            Delete Saved Cart
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#permissionDenied">
                            Permission Denied
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#rejectOrderRequest">
                            Reject Order Request
                            </button>
                        </li>
                        <li>
                            <button type="button" class="btn btn-link btn-nopadding" data-toggle="modal" data-target="#sharePage">
                                Share Page
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php include 'includes/scripts.php'; ?>
    </body>
    <?php include 'includes/modals.php'; ?>
</html>