<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
    <!-- header end -->
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                    <li><a href="#">Carbon Steel Pipe</a></li>
                    <li><a href="#">Buttweld Pipe Domestic</a></li>
                    <li class="active">Std Blk Galv Pe& T&c Xh Blk Pe</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions hidden-print">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <div class="row pdp-product">
                    <div class="col-xs-12 col-xxs-12 visible-xxs visible-xs">
                        <h1>STD BLK, GALV PE& T&C, XH BLK PE</h1>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-xxs-12 image-block">
                        <div class="product-image">
                            <div class="image">
                                    <img src="https://images.tradeservice.com/5Q7ANIVTFBR62EKJ/PRODUCTIMAGES/DIR100074/MOEINC_WB_PP_104538.jpg" alt="">
                                    <!-- <img alt="" src="assets/images/steel.jpg"> -->
                                <!-- <a href="assets/images/steel.jpg" title="STD BLK, GALV PE& T&C, XH BLK PE"> -->
                                <!-- </a> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 col-xxs-12 info-block">
                        <div class="product-info">
                            <h1 class="hidden-xs hidden-xxs">STD BLK, GALV PE& T&C, XH BLK PE</h1>
                            <div class="material-list detail pdp">
                                <div class="row">
                                    <div class="col-sm-6 col-xs-4 col-xxs-5 info-block">
                                        <big>Mfr. #</big>
                                        <big>Part #</big>
                                        <big>UPC</big>
                                        <big>Customer Alias</big>
                                    </div>
                                    <div class="col-sm-6 col-xs-8 col-xxs-7 actions">
                                        <div class="list-group">
                                            <div class="price">
                                                <big><span class="text-success">$00.00</span>/unit</big>
                                                <button class="info-button hidden-print" role="button" data-trigger="hover" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="Price shown in your account specific price, including all contrasted prices or discounts."><i class="fa fa-info-circle"></i></button>
                                            </div>
                                            <div class="qty hidden-print">
                                                <form class="form-inline">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-lg" id="pdpqty" placeholder="Qty" value="1">
                                                    </div>
                                                    <button type="submit" class="btn btn-warning btn-lg">Add to Cart</button>
                                                </form>
                                            </div>
                                            <a href="#myModalCheck" class="list-group-item hidden-print" data-toggle="modal"><i class="fa fa-check"></i> Check Availability</a>
                                            <a href="#addToMaterialList" class="list-group-item hidden-print" data-toggle="modal"><i class="fa fa-plus-square"></i> Add to Material List</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="pdp-tabs-information">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#poverview" aria-controls="poverview" role="tab" data-toggle="tab">Overview</a></li>
                        <li role="presentation"><a href="#pspec" aria-controls="pspec" role="tab" data-toggle="tab">Specifications</a></li>
                        <li role="presentation"><a href="#pdownload" aria-controls="pdownload" role="tab" data-toggle="tab"><span class="hide">Download</span> Docs & Guides</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content pdp-tabcontent">
                        <div role="tabpanel" class="tab-pane fade in active" id="poverview">
                            <h4 class="visible-print">Overview</h4>
                            <article>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, eum, ducimus. Nesciunt dolorum odio cum voluptatem, perspiciatis ea. Atque magni ducimus sunt quibusdam recusandae blanditiis commodi cumque quae ex tempora. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste omnis eius consequuntur magni pariatur sunt saepe illo quibusdam ipsum maxime molestias error, vitae amet corrupti nulla voluptatem minima blanditiis cumque.</p>
                                <ul>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit;</li>
                                    <li>Et distinctio voluptas;</li>
                                    <li>Minima, quos, ullam odit iure explicabo adipisci.</li>
                                </ul>
                            </article>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="pspec">
                            <h4 class="visible-print">Specifications</h4>
                            <table class="table table-striped table-specification">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Body Material</div>
                                                        <div class="col-xs-6"><strong>Brass</strong></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Lorem ipsum dolor sit amet</div>
                                                        <div class="col-xs-6"><strong>Lorem ipsum dolor sit amet elitdolor Lorem ipsum dolor sit amet elitdolor</strong></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Flow Rate</div>
                                                        <div class="col-xs-6"><strong>3.2 gpm</strong></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Lorem ipsum dolor sit amet</div>
                                                        <div class="col-xs-6"><strong>Lorem ipsum dolor sit</strong></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Lorem ipsum dolor sit</div>
                                                        <div class="col-xs-6"><strong>Lorem ipsum dolor</strong></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Lorem ipsum dolor sit amet</div>
                                                        <div class="col-xs-6"><strong>Lorem ipsum dolor sit</strong></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Lorem ipsum dolor sit</div>
                                                        <div class="col-xs-6"><strong>Lorem ipsum dolor</strong></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12 specification-item">
                                                    <div class="row">
                                                        <div class="col-xs-6">Lorem ipsum dolor sit amet</div>
                                                        <div class="col-xs-6"><strong>Lorem ipsum dolor sit</strong></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="pdownload">
                            <h4 class="visible-print">Download Docs & Guides</h4>
                            <table class="table table-striped table-specification table-spec-noborder">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor Sit Amet</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor Sit Amet</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor Sit Amet</span></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor Sit Amet dolos sit amet</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor</span></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae minima nisi</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor</span></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor Sit Amet dolos sit amet</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem ipsum dolor sit amet</span></a>
                                                </div>
                                                <div class="col-sm-3 col-xs-6 col-xxs-12 specification-item">
                                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Lorem Ipsum Dolor</span></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 hidden-print">
                <h3>You May Be Interested In</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="owl-full-width">
                            <div class="item">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/pipe.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">After Sales Service</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="thumbnail-content">
                                                    <a href="#">
                                                        <h4 class="visible-xs">After Sales Service</h4>
                                                    </a>
                                                    <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil adipisicing elit. Enim facere nihil</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row counter floated-bottom">
                                            <div class="col-sm-12 col-xs-12 col-sm-offset-0 col-xs-offset-0">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12">
                                                        <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                    </div>
                                                    <div class="col-sm-5 col-xs-4 qty-field">
                                                        <input type="text" class="form-control input-sm text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php for ($i=0; $i < 6; $i++) { 
                                    echo "
                                        <div class='item'>
                                    <div class='thumbnail'>
                                        <div class='row'>
                                            <div class='col-sm-12 col-xs-12'>
                                                <a href='#'>
                                                    <div class='image'>
                                                        <img src='assets/images/pipe.png' alt=''>
                                                    </div>
                                                    <h4 class='hidden-xs'>After Sales Service</h4>
                                                </a>
                                            </div>
                                            <div class='col-sm-12 col-xs-12'>
                                                <div class='thumbnail-content'>
                                                    <a href='#'>
                                                        <h4 class='visible-xs'>After Sales Service</h4>
                                                    </a>
                                                    <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='row counter floated-bottom'>
                                            <div class='col-sm-12 col-xs-12 col-sm-offset-0 col-xs-offset-0'>
                                                <div class='row'>
                                                    <div class='col-sm-12 col-xs-12'>
                                                        <big><span class='text-success'>$54.60</span><span>/unit</span></big>
                                                    </div>
                                                    <div class='col-sm-5 col-xs-4 qty-field'>
                                                        <input type='text' class='form-control input-sm text-center' placeholder='Qty' value='1'>
                                                    </div>
                                                    <div class='col-sm-7 col-xs-8'>
                                                        <a href='#' class='btn btn-warning btn-block'>Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    ";
                                } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
