<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
    <!-- header end -->
    
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                  <li><a href="#">My Account</a></li>
                  <li class="active">My Material Lists</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>My Material Lists</h1>
            </div>
            <div class="col-xs-12">
                <div class="well well-gray">
                    <h4>Search Material Lists</h4>
                    <form class="row">
                        <div class="col-md-4 col-sm-12">
                            <a href="#" class="btn btn-lg btn-warning btn-block mb-sm mb-xs mb-xxs">Create New Material List</a>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="row smaller">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control input-lg mb-sm mb-xs mb-xxs" placeholder="Material List Name or Keyword">
                                </div>
                                <div class="col-xs-6 col-xxs-12">
                                    <a href="#" class="btn btn-lg btn-warning btn-block mb-sm mb-xs mb-xxs">Search</a>
                                </div>
                                <div class="col-xs-6 col-xxs-12">
                                    <a href="#" class="btn btn-lg btn-default btn-block">Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12">
                <h3 class="text-muted">No Material Lists created</h3>
                <!-- <a href="#" class="btn btn-lg btn-warning">Create New Material List</a> -->
            </div>
        </div>
    </div>
    <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>

<?php include 'includes/modals.php'; ?>