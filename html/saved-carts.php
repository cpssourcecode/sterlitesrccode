<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Saved Carts Page - CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-sm-8">
            <ol class="breadcrumb">
                <li><a href="#">Account Overview</a></li>
                <li class="active">Saved Carts</li>
            </ol>
        </div>
        <div class="col-sm-4 page-actions">
            <ul class="list-inline">
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-envelope-o"></i><span>Email Page</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-print"></i><span>Print Page</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12">
            <h1>Saved Carts</h1>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-mobile">
                        <thead>
                        <tr>
                            <th class="col-xs-2"><strong>Saved Date</strong> <i class="fa fa-sort"></i></th>
                            <th class="col-xs-4"><strong>Ship To Address</strong> <i class="fa fa-sort"></i></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="caption">Saved Date</span>08/10/15
                                </td>
                                <td>
                                    <span class="caption">Ship To Address</span>NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2<br>
                                    Address Line 3
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                        <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Move to Active Cart</a></li>
                                            <li><a href="#viewOrder" data-toggle="modal">View</a></li>
                                            <li><a href="#deleteSavedCart" data-toggle="modal">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Saved Date</span>08/10/15
                                </td>
                                <td>
                                    <span class="caption">Ship To Address</span>NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2<br>
                                    Address Line 3
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                        <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Move to Active Cart</a></li>
                                            <li><a href="#viewOrder" data-toggle="modal">View</a></li>
                                            <li><a href="#deleteSavedCart" data-toggle="modal">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Saved Date</span>08/04/15
                                </td>
                                <td>
                                    <span class="caption">Ship To Address</span>NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2<br>
                                    Address Line 3
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                        <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Move to Active Cart</a></li>
                                            <li><a href="#viewOrder" data-toggle="modal">View</a></li>
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Saved Date</span>08/02/15
                                </td>
                                <td>
                                    <span class="caption">Ship To Address</span>NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2<br>
                                    Address Line 3
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                        <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Move to Active Cart</a></li>
                                            <li><a href="#viewOrder" data-toggle="modal">View</a></li>
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Saved Date</span>08/01/15
                                </td>
                                <td>
                                    <span class="caption">Ship To Address</span>NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2<br>
                                    Address Line 3
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                        <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Move to Active Cart</a></li>
                                            <li><a href="#viewOrder" data-toggle="modal">View</a></li>
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Saved Date</span>08/10/15
                                </td>
                                <td>
                                    <span class="caption">Ship To Address</span>NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2<br>
                                    Address Line 3
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                        <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Move to Active Cart</a></li>
                                            <li><a href="#viewOrder" data-toggle="modal">View</a></li>
                                            <li><a href="#movetoActiveCart" data-toggle="modal">Delete</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="col-xs-12">
                    <div class="text-center">
                        <div class="pagination">
                            <nav>
                                <ul class="pagination pagination-lg">
                                    <li class="prev">
                                        <a href="#" aria-label="Previous">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><span>…</span></li>
                                    <li class="next">
                                        <a href="#" aria-label="Next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
<?php include 'includes/modals.php'; ?>
</body>

</html>