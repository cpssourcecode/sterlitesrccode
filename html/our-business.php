<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Our Business</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Our Business</h1>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="sidebar-menu">
                            <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                                <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                                    <li class="active"><a href="#">Our Business</a></li>
                                    <li><a href="#">Locations</a></li>
                                    <li><a href="#">News and Events</a></li>
                                    <li><a href="#">Case Studies</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">Affiliations</a></li>
                                    <li><a href="#">Regional Managers</a></li>
                                    <li><a href="#">Price Lists</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <article>
                                <p>Since 1935, Columbia Pipe & Supply Co. has consistently delivered a wide range of high quality products and SERVICE plus SOLUTIONS. Since we are an independent company, our sales teams have the flexibility to focus their expertise on our customers’ unique needs without the bureaucratic limitations of a large corporation.</p>
                            <p>With more than $40 million inventory, including over 30,000 individual SKUs, stocked in close to one million square feet of warehouse, Columbia Pipe is able to connect our global supply of product with our local markets. We have 20 mutually-supporting locations in Illinois, Indiana, Michigan, Wisconsin and Minnesota, so wherever you are—we are close-by.</p>
                            <h4>Our Customers</h4>
                            <p>Our customers are our most important asset. We have over 10,000 local, regional, and national customers. Our people know our customers and their businesses well, and our partnerships thrive on individual relationships at the local level.</p>
                            <h4>Contractors Served</h4>
                            <ul>
                                <li>Fabricators</li>
                                <li>HVAC</li>
                                <li>Hydronic / Heating</li>
                                <li>Mechanical</li>
                                <li>Plumbing</li>
                                <li>Process Piping</li>
                            </ul>
                            <h4>Industries Served</h4>
                            <ul>
                                <li>Agricultural Processing</li>
                                <li>Automotive</li>
                                <li>Biotechnology</li>
                                <li>Chemical</li>
                                <li>Food & Beverage</li>
                                <li>Medical</li>
                                <li>OEM</li>
                                <li>Petrochemical</li>
                                <li>Pharmaceutical</li>
                                <li>Power Generation</li>
                                <li>Primary Metals</li>
                                <li>Pulp & Paper</li>
                                <li>Wastewater</li>
                                <li>Wet & Dry Grain Milling</li>
                            </ul>
                            <h4>Institutional Customers Served</h4>
                            <ul>
                                <li>K-12</li>
                                <li>Universities</li>
                                <li>Government</li>
                                <li>Health Care</li>
                                <li>Commercial Real Estate</li>
                                <li>Property Management</li>
                            </ul>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
