<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li><a href="manage-companies.php">Manage Companies</a></li>
                        <li class="active">Add Customer Admin</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Add Customer Admin</h1>
                </div>
                <div class="col-xs-12">
                    <div class="well well-gray well-invoices">
                        <h4>User Information</h4>
                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="alert alert-success">Sample Success Message
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                              </span>
                                            </button>
                                            </div>
                                            <div class="alert alert-danger">Sample Error Message
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                              </span>
                                            </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-xs-12"><input type="text" class="form-control xs-form-margin" placeholder="First Name"></div>
                                        <div class="col-sm-6 col-xs-12"><input type="text" class="form-control" placeholder="Last Name"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input type="email" class="form-control" placeholder="Email Address (will be User's login ID)">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <input type="phone" class="form-control" placeholder="Phone">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-xs-12">
                                            <a href="#" class="btn btn-lg btn-warning btn-block btn-invoices-search xs-form-margin">Add Customer Admin</a>
                                        </div>
                                        <div class="col-sm-4 col-xs-12">
                                            <a href="#" class="btn btn-lg btn-default btn-block btn-invoices-search">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
