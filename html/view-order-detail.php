<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Order History</a></li>
                        <li class="active">View Order Detail</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions hidden-print">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>View Order Detail</h1>
                </div>
                <div class="col-xs-12">
                    <div class="material-list detail">
                        <div class="row">
                            <div class="col-sm-12 info-block">
                                <h4 class="title">Order Number: 3242378</h4>
                                <div class="info">
                                    <div class="box">
                                        <strong>Ordered On</strong>
                                        <span>01/2/2015</span>
                                    </div>
                                    <div class="box">
                                        <strong># of Items</strong>
                                        <span>25</span>
                                    </div>
                                    <div class="box">
                                        <strong>Ship to Address</strong>
                                        <span>
                                    NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2 <br>
                                    Address Line 3
                                </span>
                                    </div>
                                    <div class="box">
                                        <strong>Total</strong>
                                        <span>
                                    Subtotal: $2,444.00 <br>
                                    Shipping: $49.00 <br>
                                    Tax: $243.00
                                </span>
                                        <br>
                                        <br>
                                        <strong>Total Amount</strong>
                                        <span>
                                    $2,735.00
                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 hidden-print">
                    <ul class="list-inline order-action-button-list">
                        <li>
                            <a class="btn btn-warning btn-lg" href="#">Add Selected to Cart</a>
                        </li>
                        <li>
                            <a class="btn btn-warning btn-lg" href="#">Add Selected to Material List</a>
                        </li>
                    </ul>
                    <br>
                </div>
                <div class="col-xs-12">
                    <div class="material-items-list order-detail-list items-order-mobile">
                        <div class="header">
                            <div class="check">
                                <div class="checkbox hidden-print" id="select-item-material">
                                    <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label"><strong>Select All</strong></span>
                                    </label>
                                </div>
                                <div class="print-image">&nbsp;</div>  <!-- For padding on print -->
                            </div>
                            <div class="name">
                                <span class="title"><strong>Description <i class="fa fa-sort"></i></strong></span>
                            </div>
                            <div class="item-number">
                                <span class="title"><strong>Item# <i class="fa fa-sort"></i></strong></span>
                            </div>
                            <div class="qty">
                                <span class="title"><strong>Quantity</strong></span>
                            </div>
                            <div class="price">
                                <span class="title"><strong>Price</strong> <i class="fa fa-sort"></i></span>
                            </div>
                            <div class="total">
                                <span class="title"><strong>Total</strong></span>
                            </div>
                            <div class="status">
                                <span class="title"><strong>Status</strong></span>
                            </div>
                        </div>
                        <div class="content-list scrollbar-inner">
                            <div class="content">
                                <div class="check">
                                    <div class="checkbox image" id="select-item-material-check">
                                        <label class="checkbox-custom hidden-print" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                            <span class="checkbox-label">
                                                <img src="assets/images/water-hitters.png" alt="">
                                            </span>
                                        </label>
                                    </div>
                                    <div class="print-image">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="name">
                                    <a class="product-title" href="#">Name of product</a>
                                </div>
                                <div class="item-number">
                                    <div class="caption">Item #</div>4356346
                                </div>
                                <div class="qty">
                                    <div class="caption">Qty</div>1
                                </div>
                                <div class="price">
                                    <div class="caption">Price</div>$123.60
                                </div>
                                <div class="total">
                                    <div class="caption">Total</div>$134.60
                                </div>
                                <div class="status">
                                    <div class="caption">Status</div><span class="label label-success">Received</span>
                                </div>
                            </div>
                            <div class="content disabled">
                                <div class="check">
                                    <div class="checkbox image" id="select-item-material-check">
                                        <button class="question-tooltip hidden-print" role="button" data-trigger="hover" data-toggle="popover" data-trigger="focus" data-placement="right" data-content="This item is not available for online order.">
                                            <span class="glyphicon glyphicon-question-sign"></span>
                                        </button>
                                        <label class="checkbox-custom hidden-print" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                            <span class="checkbox-label">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </span>
                                        </label>
                                    </div>
                                    <div class="print-image">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="name">
                                    <a class="product-title" href="#">Name of product</a>
                                </div>
                                <div class="item-number">
                                    <div class="caption">Item #</div>4356346
                                </div>
                                <div class="qty">
                                    <div class="caption">Qty</div>1
                                </div>
                                <div class="price">
                                    <div class="caption">Price</div>$123.60
                                </div>
                                <div class="total">
                                    <div class="caption">Total</div>$134.60
                                </div>
                                <div class="status">
                                    <div class="caption">Status</div><span class="label label-warning">Processing</span>
                                </div>
                            </div>
                            <div class="content">
                                <div class="check">
                                    <div class="checkbox image" id="select-item-material-check">
                                        <label class="checkbox-custom hidden-print" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                            <span class="checkbox-label">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </span>
                                        </label>
                                    </div>
                                    <div class="print-image">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="name">
                                    <a class="product-title" href="#">Name of product</a>
                                </div>
                                <div class="item-number">
                                    <div class="caption">Item #</div>4356346
                                </div>
                                <div class="qty">
                                    <div class="caption">Qty</div>1
                                </div>
                                <div class="price">
                                    <div class="caption">Price</div>$123.60
                                </div>
                                <div class="total">
                                    <div class="caption">Total</div>$134.60
                                </div>
                                <div class="status">
                                    <div class="caption">Status</div><span class="label label-warning-dark">Shipped</span>
                                </div>
                            </div>
                            <div class="content">
                                <div class="check">
                                    <div class="checkbox image" id="select-item-material-check">
                                        <label class="checkbox-custom hidden-print" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                            <span class="checkbox-label">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </span>
                                        </label>
                                    </div>
                                    <div class="print-image">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="name">
                                    <a class="product-title" href="#">Name of product</a>
                                </div>
                                <div class="item-number">
                                    <div class="caption">Item #</div>4356346
                                </div>
                                <div class="qty">
                                    <div class="caption">Qty</div>1
                                </div>
                                <div class="price">
                                    <div class="caption">Price</div>$123.60
                                </div>
                                <div class="total">
                                    <div class="caption">Total</div>$134.60
                                </div>
                                <div class="status">
                                    <div class="caption">Status</div><span class="label label-default">Delivered</span>
                                </div>
                            </div>
                            <div class="content">
                                <div class="check">
                                    <div class="checkbox image" id="select-item-material-check">
                                        <label class="checkbox-custom hidden-print" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                            <span class="checkbox-label">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </span>
                                        </label>
                                    </div>
                                    <div class="print-image">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="name">
                                    <a class="product-title" href="#">Name of product</a>
                                </div>
                                <div class="item-number">
                                    <div class="caption">Item #</div>4356346
                                </div>
                                <div class="qty">
                                    <div class="caption">Qty</div>1
                                </div>
                                <div class="price">
                                    <div class="caption">Price</div>$123.60
                                </div>
                                <div class="total">
                                    <div class="caption">Total</div>$134.60
                                </div>
                                <div class="status">
                                    <div class="caption">Status</div><span class="label label-primary">Invoiced</span>
                                </div>
                            </div>
                            <div class="content">
                                <div class="check">
                                    <div class="checkbox image" id="select-item-material-check">
                                        <label class="checkbox-custom hidden-print" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                            <span class="checkbox-label">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </span>
                                        </label>
                                    </div>
                                    <div class="print-image">
                                        <img src="assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="name">
                                    <a class="product-title" href="#">Name of product</a>
                                </div>
                                <div class="item-number">
                                    <div class="caption">Item #</div>4356346
                                </div>
                                <div class="qty">
                                    <div class="caption">Qty</div>1
                                </div>
                                <div class="price">
                                    <div class="caption">Price</div>$123.60
                                </div>
                                <div class="total">
                                    <div class="caption">Total</div>$134.60
                                </div>
                                <div class="status">
                                    <div class="caption">Status</div><span class="label label-primary">Invoiced</span>
                                </div>
                            </div>
                            <?php 
                            for ($i=0; $i < 10; $i++) { 
                                echo "<div class='content'>
                                <div class='check'>
                                    <div class='checkbox image' id='select-item-material-check'>
                                        <label class='checkbox-custom hidden-print' data-initialize='checkbox'>
                                            <input class='sr-only' type='checkbox' value=''>
                                            <span class='checkbox-label'>
                                        <img src='assets/images/water-hitters.png' alt=''>
                                    </span>
                                        </label>
                                    </div>
                                    <div class='print-image'>
                                        <img src='assets/images/water-hitters.png' alt=''>
                                    </div>
                                </div>
                                <div class='name'>
                                    <a class='product-title' href='#'>Name of product</a>
                                </div>
                                <div class='item-number'>
                                    <div class='caption'>Item #</div>4356346
                                </div>
                                <div class='qty'>
                                    <div class='caption'>Qty</div>1
                                </div>
                                <div class='price'>
                                    <div class='caption'>Price</div>$123.60
                                </div>
                                <div class='total'>
                                    <div class='caption'>Total</div>$134.60
                                </div>
                                <div class='status'>
                                    <div class='caption'>Status</div><span class='label label-primary'>Invoiced</span>
                                </div>
                            </div>";
                            }
                             ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center hidden-print">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <li class="prev">
                                <a href="#" aria-label="Previous">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>…</span></li>
                            <li class="next">
                                <a href="#" aria-label="Next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
