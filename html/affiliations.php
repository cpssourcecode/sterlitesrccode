<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Affiliations Page — CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-sm-8">
            <ol class="breadcrumb">
                <li class="active">Affiliations</li>
            </ol>
        </div>
        <div class="col-sm-4 page-actions">
            <ul class="list-inline">
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-envelope-o"></i><span>Email Page</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-print"></i><span>Print Page</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12">
            <h1>Affiliations</h1>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-sm-3 col-xs-12">
                    <div class="sidebar-menu">
                        <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                        <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                            <li><a href="#">Our Business</a></li>
                            <li><a href="#">Locations</a></li>
                            <li><a href="#">News and Events</a></li>
                            <li><a href="#">Case Studies</a></li>
                            <li><a href="#">Testimonials</a></li>
                            <li class="active"><a href="#">Affiliations</a></li>
                            <li><a href="#">Regional Managers</a></li>
                            <li><a href="#">Price Lists</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <div class="affiliation-list">
                        <article>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. </p>
                        </article>
                        <div class="affiliation-item">
                            <div class="pic-holder">
                                <img src="assets/images/AFD.png" alt=""/>
                            </div>
                            <h4>AD — Affiliated Distributors</h4>
                            <a href="#">Visit site</a>
                        </div>
                        <div class="affiliation-item">
                            <div class="pic-holder">
                                <img src="assets/images/images.png" alt=""/>
                            </div>
                            <h4>ASA — American Supply Association</h4>
                            <a href="#">Visit site</a>
                        </div>
                        <div class="affiliation-item">
                            <div class="pic-holder">
                                <img src="assets/images/aspe-logo166Ht_0.png" alt=""/>
                            </div>
                            <h4>ASPE — American Society of Plumbing Engineers</h4>
                            <a href="#">Visit site</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>

</html>