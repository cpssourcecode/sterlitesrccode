<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li class="active">Manage Companies</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Manage Companies</h1>
                </div>
                <div class="col-xs-12">
                    <p>To access a customer account, locate the desired customer in the list below and click "Access" in the same row. <strong>Use caution while accessing a customer's account</strong>; changes you make and settings you save will affect this customer's account and may impact their ability to conduct business or make purchases.</p>
                    <p><strong>To stop accessing</strong> a customer's account once you have access, click the "Stop Access" button at the top of the browser window. </p>
                </div>
                <div class="col-xs-12">
                    <div class="well well-gray well-invoices">
                        <h4>Search Companies</h4>
                        <form class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-5 col-sm-7">
                                        <div class="row smaller">
                                            <div class="col-sm-12 col-xs-12 mb-sm">
                                                <input type="text" class="form-control input-lg" placeholder="Company Name or Account #">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-4">
                                        <div class="row smaller">
                                            <div class="col-sm-6 col-xs-6">
                                                <a href="#" class="btn btn-lg btn-warning btn-block btn-invoices-search">Search</a>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <a href="#" class="btn btn-lg btn-default btn-block btn-invoices-search">Reset</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12 text-center">
                    <nav class="companies-nav">
                      <ul class="pagination">
                        <li><a href="#">A</a></li>
                        <li><a href="#">B</a></li>
                        <li><a href="#">C</a></li>
                        <li><a href="#">D</a></li>
                        <li><a href="#">E</a></li>
                        <li><a href="#">G</a></li>
                        <li><a href="#">H</a></li>
                        <li><a href="#">I</a></li>
                        <li><a href="#">J</a></li>
                        <li><a href="#">K</a></li>
                        <li><a href="#">L</a></li>
                        <li><a href="#">M</a></li>
                        <li><a href="#">N</a></li>
                        <li><a href="#">O</a></li>
                        <li><a href="#">P</a></li>
                        <li><a href="#">Q</a></li>
                        <li><a href="#">R</a></li>
                        <li><a href="#">S</a></li>
                        <li><a href="#">T</a></li>
                        <li><a href="#">V</a></li>
                        <li><a href="#">W</a></li>
                        <li><a href="#">X</a></li>
                        <li><a href="#">Y</a></li>
                        <li><a href="#">Z</a></li>
                      </ul>
                    </nav>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><strong>Company</strong> <i class="fa fa-sort"></i></th>
                                <th><strong>Account #</strong> <i class="fa fa-sort"></i></th>
                                <th colspan="2"><strong>Address</strong></strong> <i class="fa fa-sort"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="#" class="table-icon-link"><span>Company Name</span></a>
                                </td>
                                <td>
                                    894358436
                                </td>
                                <td>
                                    Street <br>
                                    City, State, Zip
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#accessCustomerAccount" class="table-icon-link"><span>Access</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#" class="table-icon-link"><span>Company Name</span></a>
                                </td>
                                <td>
                                    894358436
                                </td>
                                <td>
                                    Street <br>
                                    City, State, Zip
                                </td>
                                <td>
                                    <a href="add-customer-admin.php" class="table-icon-link"><span>Set Up</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#" class="table-icon-link"><span>Company Name</span></a>
                                </td>
                                <td>
                                    894358436
                                </td>
                                <td>
                                    Street <br>
                                    City, State, Zip
                                </td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#accessCustomerAccount" class="table-icon-link"><span>Access</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#" class="table-icon-link"><span>Company Name</span></a>
                                </td>
                                <td>
                                    894358436
                                </td>
                                <td>
                                    Street <br>
                                    City, State, Zip
                                </td>
                                <td>
                                    <a href="add-customer-admin.php" class="table-icon-link"><span>Set Up</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12 text-center">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <li class="prev">
                                <a href="#" aria-label="Previous">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>…</span></li>
                            <li class="next">
                                <a href="#" aria-label="Next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
