<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Services</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Inter-Branch Transfer/Cross Dock System</h1>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="sidebar-menu">
                                <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                                <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                                    <li><a href="#">After Sales Service</a></li>
                                    <li><a href="#">CAPS Valve Automation</a></li>
                                    <li><a href="#">Columbia Express™</a></li>
                                    <li><a href="#">Rapid Delivery</a></li>
                                    <li><a href="#">Commercial Plan & Spec</a></li>
                                    <li><a href="#">E-Commerce</a></li>
                                    <li><a href="#">HVAC</a></li>
                                    <li class="active"><a href="#">Inter-Branch Transfer / Cross Dock System</a></li>
                                    <li><a href="#">Kitting</a></li>
                                    <li><a href="#">Nationwide and Regional</a></li>
                                    <li><a href="#">Distribution Services</a></li>
                                    <li><a href="#">The Pipe Works™</a></li>
                                    <li><a href="#">Custom Fabrication</a></li>
                                    <li><a href="#">Product and Business </a></li>
                                    <li><a href="#">Expertise</a></li>
                                    <li><a href="#">Product Training</a></li>
                                    <li><a href="#">Rolling Warehouse™</a></li>
                                    <li><a href="#">$mart$tock™ — Inventory </a></li>
                                    <li><a href="#">Management </a></li>
                                    <li><a href="#">Standardization</a></li>
                                    <li><a href="#">Supplier Reviews</a></li>
                                </ul>
                            </div>
                            <div class="sidebar-menu hidden-xs hidden-xxs">
                                <ul class="sidebar-menu-container">
                                    <li><a href="#">Request More Information about [...]</a></li>
                                </ul>
                                <br>
                                <ul class="sidebar-menu-container">
                                    <li><a class="iconic" href="#"><i class="fa fa-file-text-o"></i>Download More Information about [...]</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <article>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem.</p>
                                <p>If it’s not in stock near you, it will be—by tomorrow.</p>
                                <iframe class="emmbed-youtube" id="ytplayer" type="text/html" width="100%" height="400px" src="https://www.youtube.com/embed/M7lc1UVf-VE" frameborder="0" allowfullscreen></iframe>
                                <br>
                                <br>
                                <p>Columbia Pipe’s unique Inter-Branch Transfer System supplies you with full access to the complete inventory of all of our locations throughout the Midwest. If what you need isn’t in stock at your location, we’ll make sure it is by the next day. From the smallest fitting to 24” pipe, from HVAC to plumbing supplies or valve actuation products, Inter-Branch Transfer ensures fast delivery of over 30,000 SKUs. Just contact your local Columbia Pipe representative to get the job done.</p>
                                <ul>
                                    <li>Next-day access to over $40 million of inventory</li>
                                    <li>Transfer trucks run daily / nightly</li>
                                    <li>Pipe, Valves, Fittings, Plumbing, Hydronic Equipment, and HVAC</li>
                                </ul>
                                <p><a href="#">Request More Information</a></p>
                            </article>
                            <div class="sidebar-menu visible-xs visible-xxs">
                                <ul class="sidebar-menu-container">
                                    <li><a href="#">Request More Information about [...]</a></li>
                                </ul>
                                <br>
                                <ul class="sidebar-menu-container">
                                    <li><a class="iconic" href="#"><i class="fa fa-file-text-o"></i>Download More Information about [...]</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
