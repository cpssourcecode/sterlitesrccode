<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Cart</a></li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <div class="carts-steps">
                        <a href="#" class="step is-active">
                            <span class="step-number">1.</span>
                            <span class="step-label">Cart</span>
                        </a>
                        <a href="#" class="step">
                            <span class="step-number">2.</span>
                            <span class="step-label">Review</span>
                        </a>
                        <a href="#" class="step">
                            <span class="step-number">3.</span>
                            <span class="step-label">Confirmation</span>
                        </a>
                    </div>
                </div>
                <div class="cart-page">
                    <div class="cart-info col-xs-12">
                        <div class="col-md-9 ship-info">
                            <div class="col-md-6">
                                <h4>Delivery method</h4>
                                <form class="row">
                                    <div class="col-lg-12">
                                        <div class="radio radio-toggle-delivery-method checked">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel3">
                                                <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="delivery-method-shipped"> Have it Shipped
                                            </label>
                                        </div>
                                        <div class="radio radio-toggle-delivery-method">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel4">
                                                <input class="sr-only" name="radioEx1" type="radio" value="delivery-method-pick-up"> Pick it Up
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="row delivery-method-shipped-form" style="display: block;">
                                            <div class="col-xs-12">
                                                <p><strong>Ship To Address:</strong></p>
                                                <p>NW Hospital - 17th Floor, Imaging <br>
                                                   Address Line 2<br>
                                                   Address Line 3<br>
                                                    <br>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row delivery-method-pick-up-form" style="display: none;">
                                            <div class="col-xs-12">
                                                <p><strong>Your Default Pickup Location:</strong></p>
                                                <p>1235 Address Ln.,<br>
                                                   Address Line 2<br>
                                                   Address Line 3<br>
                                                   Chicago, IL 60626
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <a href="#changeShipAddress" data-toggle="modal">Change Ship To Address</a>
                                <div class="error-comment">
                                    <i class="fa fa-exclamation-triangle"></i>
                                    <p>Changing your Ship to Address may affect prices and availablity of items in your
                                       cart.</p>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <h4>Billing address</h4>
                                <p>
                                    Company ABC <br>
                                    454 State Street <br>
                                    Chicago IL <br>
                                    60630 <br>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-3 summary">
                            <a class="btn btn-warning btn-lg btn-block disabled">Proceed to Checkout</a>
                            <div class="summary-block">
                                <h4>*Apply PO#</h4>
                                <div id="applyPO" class="row smaller">
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control input-lg" placeholder="PO#" maxlength="10">
                                    </div>
                                    <div class="col-xs-5">
                                        <button class="btn btn-default btn-lg btn-block btn-nopadding">Apply</button>
                                    </div>
                                </div>
                                <hr/>
                                <h4>Summary</h4>
                                # Items: <strong>0</strong> <br>
                                Subtotal: <strong>$0.00</strong>
                                <div class="info-comment">
                                    <i class="fa fa-info-circle"></i>
                                    <p><em>Information on shipping, tax, and other charges is not available until you complete your order.
                                        <a href="#infoShipTaxOther" data-toggle="modal"><strong>Read More</strong></a>.</em></p>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-xs-12">
                        <div class="important-message panel-group" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#importantMsg" aria-expanded="true" aria-controls="importantMsg">
                                            <i class="fa fa-exclamation-triangle"></i>
                                            Important Messages About Item in Your Cart.
                                        </a>
                                    </h4>
                                </div>
                                <div id="importantMsg" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <strong>Columbia Pipe no longer offers these items:</strong>
                                        <ul>
                                            <li>Std Blk Galv Pe& T&c Xh Blk</li>
                                            <li>Std Blk Galv Pe& T&c Xh Blk</li>
                                            <li>Std Blk Galv Pe& T&c Xh Blk</li>
                                        </ul>
                                        <a href="#">Show More</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="well well-gray quick-add">
                            <form class="row">
                                <div class="col-md-2 col-xs-12">
                                    <h4>Quick Add</h4>
                                </div>
                                <div class="form-group col-md-3 col-xs-6">
                                    <input type="text" class="form-control input-lg" placeholder="Item# or Alias">
                                </div>
                                <div class="form-group col-md-2 col-xs-6">
                                    <input type="text" class="form-control input-lg" placeholder="Qty">
                                </div>
                                <div class="form-group col-md-2 col-xs-12">
                                    <a class="btn btn-lg btn-default btn-block btn-invoices-search" href="#duplicateMatches" data-toggle="modal">Add to Cart</a>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <ul class="list-inline cart-products-actions">
                            <li>
                                <a href="#" class="btn btn-lg btn-default disabled">Check Availability of Selected</a>
                            </li>
                            <li>
                                <a href="#addToMaterialList" data-toggle="modal" class="btn btn-lg btn-default disabled">Add Selected to Material List</a>
                            </li>
                            <li>
                                <a href="#removeFromCart" data-toggle="modal" class="btn btn-lg btn-default disabled">Remove Selected</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-xs-12">
                        <div class="cart-items-list">
                            <div class="header">
                                <div class="col check">
                                    <div class="checkbox" id="select-item-material">
                                        <label class="checkbox-custom" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                            <span class="checkbox-label"><strong>Select All</strong></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col desc">
                                    <span class="title"><strong>Description</strong></span>
                                </div>
                                <div class="col item-number">
                                    <span class="title"><strong>Item#</strong> <i class="fa fa-sort"></i></span>
                                </div>
                                <div class="col alias">
                                    <span class="title"><strong>Alias</strong> <i class="fa fa-sort"></i></span>
                                </div>
                                <div class="col qty">
                                    <span class="title"><strong>Qty</strong></span>
                                </div>
                                <div class="col u-price">
                                    <span class="title"><strong>Unit Price</strong> <i class="fa fa-sort"></i></span>
                                </div>
                                <div class="col t-price">
                                    <span class="title"><strong>Total Price</strong></span>
                                </div>
                                <div class="col availability">
                                    <span class="title"><strong>Availability</strong></span>
                                </div>
                            </div>
                            <div class="content-list">
                                <div class="content empty-cart">
                                    Your Cart is Empty
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="subtotal col-md-4 col-xs-12 pull-right text-right">
                        <h4>Subtotal: <strong class="text-success">$0.00</strong></h4>
                        <a class="btn btn-warning btn-lg disabled">Proceed to Checkout</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>