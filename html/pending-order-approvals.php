<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Account Overview</a></li>
                        <li class="active">Pending Order Approvals</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Pending Order Approvals</h1>
                </div>
                <div class="col-xs-12">
                    <br>
                    <ul class="list-inline order-action-button-list half">
                        <li>
                            <a href="#" data-toggle="modal" data-target="#approveSelectedPendingOrders" class="btn btn-warning btn-lg">Approve Selected</a>
                        </li>
                        <li>
                            <a href="#" data-toggle="modal" data-target="#rejectSelectedPendingOrders" class="btn btn-default btn-lg">Reject Selected</a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-mobile orders">
                        <thead>
                            <tr>
                                <th class="check">
                                    <strong>
                                        <div class="checkbox" id="">
                                            <label class="checkbox-custom" data-initialize="checkbox">
                                                <span class="checkbox-label"><strong>All</strong></span>
                                            <input class="sr-only" type="checkbox" value="">
                                            </label>
                                        </div>
                                    </strong>
                                </th>
                                <th class="check check-mobile">
                                    <strong>
                                        <div class="checkbox" id="">
                                            <label class="checkbox-custom" data-initialize="checkbox">
                                            <input class="sr-only" type="checkbox" value="">
                                                <span class="checkbox-label"><strong>Select All</strong></span>
                                            </label>
                                        </div>
                                    </strong>
                                </th>
                                <th><strong>Requestor</strong> <i class="fa fa-sort"></i></th>
                                <th><strong>Request Date</strong> <i class="fa fa-sort"></i></th>
                                <th><strong>Spending Limit</strong> </th>
                                <th><strong>Spend Request</strong> </th>
                                <th></th>
                            </tr>
                        </thead>
                <tbody>
                    <tr>
                        <td class="check">
                            <div class="checkbox single" id="select-item-material">
                                <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label"></span>
                                </label>
                            </div>
                        </td>
                        <td>
                            <span class="caption">
                                Requester
                            </span>
                            <div class="checkbox single mobile" id="select-item-material">
                                    <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label"><strong>Konstantin Konstantinopolsky</strong></span>
                                    </label>
                            </div>
                                <span class="hidden-mobile">Konstantin Konstantinopolsky</span>
                        </td>
                        <td>
                            <span class="caption">Request Date</span>12/12/15
                        </td>
                        <td>
                            <span class="caption">Spending Limit</span>$3500.40
                        </td>
                        <td>
                            <span class="caption">Spend Request</span>$3500.40
                        </td>
                        <td>
                            <div class="dropdown">
                                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                    <li><a data-toggle="modal" data-target="#viewOrder" href="">View</a></li>
                                    <li><a data-toggle="modal" data-target="#approveSelectedPendingOrder" href="">Approve</a></li>
                                    <li><a data-toggle="modal" data-target="#rejectOrder" href="">Reject</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="check">
                            <div class="checkbox single" id="select-item-material">
                                <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label"></span>
                                </label>
                            </div>
                        </td>
                        <td>
                            <span class="caption">
                                Requester
                            </span>
                            <div class="checkbox single mobile" id="select-item-material">
                                    <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label"><strong>Konstantin Konstantinopolsky</strong></span>
                                    </label>
                            </div>
                                <span class="hidden-mobile">Konstantin Konstantinopolsky</span>
                        </td>
                        <td>
                            <span class="caption">Request Date</span>12/12/15
                        </td>
                        <td>
                            <span class="caption">Spending Limit</span>$3500.40
                        </td>
                        <td>
                            <span class="caption">Spend Request</span>$3500.40
                        </td>
                        <td>
                            <div class="dropdown">
                                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                    <li><a data-toggle="modal" data-target="#viewOrder" href="">View</a></li>
                                    <li><a data-toggle="modal" data-target="#approveSelectedPendingOrder" href="">Approve</a></li>
                                    <li><a data-toggle="modal" data-target="#rejectOrder" href="">Reject</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="check">
                            <div class="checkbox single" id="select-item-material">
                                <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label"></span>
                                </label>
                            </div>
                        </td>
                        <td>
                            <span class="caption">
                                Requester
                            </span>
                            <div class="checkbox single mobile" id="select-item-material">
                                    <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label"><strong>Konstantin Konstantinopolsky</strong></span>
                                    </label>
                            </div>
                                <span class="hidden-mobile">Konstantin Konstantinopolsky</span>
                        </td>
                        <td>
                            <span class="caption">Request Date</span>12/12/15
                        </td>
                        <td>
                            <span class="caption">Spending Limit</span>$3500.40
                        </td>
                        <td>
                            <span class="caption">Spend Request</span>$3500.40
                        </td>
                        <td>
                            <div class="dropdown">
                                <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>Action</span></a>
                                <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
                                    <li><a data-toggle="modal" data-target="#viewOrder" href="">View</a></li>
                                    <li><a data-toggle="modal" data-target="#approveSelectedPendingOrder" href="">Approve</a></li>
                                    <li><a data-toggle="modal" data-target="#rejectOrder" href="">Reject</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                </tbody>
                </table>
            </div>
            <div class="col-xs-12 text-center">
                <nav>
                    <ul class="pagination pagination-lg">
                        <li class="prev">
                            <a href="#" aria-label="Previous">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><span>…</span></li>
                        <li class="next">
                            <a href="#" aria-label="Next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
