<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Product Listing</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions hidden-print">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Buttweld Pipe/Buttweld/Pipe/PlugsBlack</h1>
                </div>
                <div class="col-md-3 col-xs-12 hidden-print">
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-6 col-xxs-12">
                            <h4>Narrow Results:</h4>
                            <div class="well well-gray well-sm">
                                <form class="form-inline form-search">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-lg" id="inputSearch" placeholder="Search Within Results">
                                    </div>
                                    <!-- <button type="submit" class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-search"></span></button> -->
                                    <button type="submit" class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-search"></span></button>
                                </form>
                            </div>
                            <div class="fassets-selection">
                                <div class="title">
                                    <div class="caption"><h4>Selected options</h4></div>
                                </div>
                                <ul class="list-unstyled">
                                    <li><span>Value:</span><a href="#"><span>Parameter with more larger text</span></a><button><i class="fa fa-times"></i></button></li>
                                    <li><span>Value 2:</span><a href="#"><span>Parameter 2</span></a><button><i class="fa fa-times"></i></button></li>
                                </ul>
                                <div class="clear-all"><button><span>Clear selection</span><!-- <i class="fa fa-times-circle-o"></i> --></button></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-6 col-xs-6 col-xxs-12">
                            <div class="accordion-filter panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default panel-fassets">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                              Manufacturer
                                            </a>
                                          </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="checkbox" id="myCheckbox1">
                                              <label class="checkbox-custom" data-initialize="checkbox">
                                                <input class="sr-only" type="checkbox" value="">
                                                <span class="checkbox-label">Manufacturer 1</span>
                                              </label>
                                            </div>
                                            <div class="checkbox" id="myCheckbox2">
                                              <label class="checkbox-custom" data-initialize="checkbox">
                                                <input class="sr-only" type="checkbox" value="">
                                                <span class="checkbox-label">Manufacturer 2</span>
                                              </label>
                                            </div>
                                            <div class="checkbox" id="myCheckbox3">
                                              <label class="checkbox-custom" data-initialize="checkbox">
                                                <input class="sr-only" type="checkbox" value="">
                                                <span class="checkbox-label">Manufacturer 3</span>
                                              </label>
                                            </div>
                                            <a href="#viewAllManufacturerOptions" data-toggle="modal" class="link">View All</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default panel-fassets">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                              Parameter 1
                                            </a>
                                          </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <div class="radio">
                                              <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1">
                                                Radio 1
                                              </label>
                                            </div>

                                            <div class="radio">
                                              <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                                <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="option2">
                                                Radio 2
                                              </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- <div class="well well-gray well-sm">
                        <button class="form-control input-lg">
                            Manufacturer
                            <span class="glyphicon glyphicon-plus-sign"></span>
                        </button>
                    </div> -->
                    
                    
                </div>
                <div class="col-md-9 col-xs-12">
                    <h4 class="hidden-print">Sort Results:</h4>
                    <div class="well well-gray well-sm hidden-print">
                        <div class="row">
                            <div class="col-md-3 col-sm-4">
                                <select name="" id="" class="form-control bootstrap-select input-lg">
                                    <option value="">Sort By</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="pagination-nav">
                        <div class="count">
                            <span class="text-block">Showing Results: <strong>10</strong> of <strong>33</strong></span>
                            <span class="text-block hidden-print">Results Per Page: <strong><a href="#">10</a></strong><a href="#">20</a><a href="#">30</a></span>
                        </div>
                        <div class="pagination hidden-print">
                            <nav>
                                <ul class="pagination pagination-lg">
                                    <li class="prev">
                                        <a href="#" aria-label="Previous">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a onclick="$('.preload-cover').toggleClass('loader-active');" href="#">2</a></li>
                                    <li><a onclick="$('.preload-cover').toggleClass('loader-active');" href="#">3</a></li>
                                    <li><a onclick="$('.preload-cover').toggleClass('loader-active');" href="#">4</a></li>
                                    <li><span>…</span></li>
                                    <li class="next">
                                        <a href="#" aria-label="Next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="preload-cover loadr-active">

                        <div class="material-list">
                            <div class="row">
                                <div class="col-sm-8 info-block search-list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" src="assets/images/MOEINCE00843_WB_248_PP_002_1225B.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="#"><span>STD BLK, GALV PE& T&C, XH BLK PE</span></a></h4>
                                            <div class="info">
                                                <p>Mfr. #</p>
                                                <p>Part #</p>
                                                <p>UPC</p>
                                                <p>Customer Alias</p>
                                                <p>Lorem ipsum dolor</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 actions">
                                    <div class="list-group">
                                        <div class="price">
                                            <big><span class="text-success">$00.00</span>/unit</big>
                                            <button class="info-button" role="button" data-trigger="hover" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="Price shown in your account specific price, including all contrasted prices or discounts."><i class="fa fa-info-circle"></i></button>
                                        </div>
                                        <div class="qty">
                                            <form class="form-inline">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg" id="pdpqty" placeholder="Qty" value="1">
                                                </div>
                                                <button type="submit" class="btn btn-warning btn-lg">Add to Cart</button>
                                            </form>
                                        </div>
                                        <a href="#myModalCheck" class="list-group-item" data-toggle="modal"><svg class="icon icon-tick"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-tick"></use></svg> Check Availability</a>
                                        <a href="#addToMaterialList" class="list-group-item" data-toggle="modal"><svg class="icon icon-add-playlist"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-add-playlist"></use></svg> Add to Material List</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="material-list">
                            <div class="row">
                                <div class="col-sm-8 info-block search-list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" src="assets/images/WHEATYE00006_2_PP_001TH.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="#"><span>STD BLK, GALV PE& T&C, XH BLK PE</span></a></h4>
                                            <div class="info">
                                                <p>Mfr. #</p>
                                                <p>Part #</p>
                                                <p>UPC</p>
                                                <p>Customer Alias</p>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 actions">
                                    <div class="list-group">
                                        <div class="price">
                                            <big><span class="text-success">$00.00</span>/unit</big>
                                            <button class="info-button" role="button" data-trigger="hover" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="Price shown in your account specific price, including all contrasted prices or discounts."><i class="fa fa-info-circle"></i></button>
                                        </div>
                                        <div class="qty">
                                            <form class="form-inline">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg" id="pdpqty" placeholder="Qty" value="1">
                                                </div>
                                                <button type="submit" class="btn btn-warning btn-lg">Add to Cart</button>
                                            </form>
                                        </div>
                                        <a href="#myModalCheck" class="list-group-item" data-toggle="modal"><svg class="icon icon-tick"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-tick"></use></svg> Check Availability</a>
                                        <a href="#addToMaterialList" class="list-group-item" data-toggle="modal"><svg class="icon icon-add-playlist"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-add-playlist"></use></svg> Add to Material List</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="material-list">
                            <div class="row">
                                <div class="col-sm-8 info-block search-list">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object" src="assets/images/steel.jpg" alt="...">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="#"><span>STD BLK, GALV PE& T&C, XH BLK PE</span></a></h4>
                                            <div class="info">
                                                <p>Mfr. #</p>
                                                <p>Part #</p>
                                                <p>UPC</p>
                                                <p>Customer Alias</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam porro, tenetur distinctio excepturi minus quas dolorum sequi voluptatum quisquam aut praesentium odit laborum perspiciatis, perferendis, quam deserunt. Veniam, ex dignissimos.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 actions">
                                    <div class="list-group">
                                        <div class="price">
                                            <big><span class="text-success">$00.00</span>/unit</big>
                                            <button class="info-button" role="button" data-trigger="hover" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="Price shown in your account specific price, including all contrasted prices or discounts."><i class="fa fa-info-circle"></i></button>
                                        </div>
                                        <div class="qty">
                                            <form class="form-inline">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-lg" id="pdpqty" placeholder="Qty" value="1">
                                                </div>
                                                <button type="submit" class="btn btn-warning btn-lg">Add to Cart</button>
                                            </form>
                                        </div>
                                        <a href="#myModalCheck" class="list-group-item" data-toggle="modal"><svg class="icon icon-tick"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-tick"></use></svg> Check Availability</a>
                                        <a href="#addToMaterialList" class="list-group-item" data-toggle="modal"><svg class="icon icon-add-playlist"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-add-playlist"></use></svg> Add to Material List</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="preload-bg"></div>
                        <div class="preload-spinner">
                            <i class="fa fa-spinner fa-pulse"></i>
                        </div>
                    </div>
                    <div class="text-right hidden-print">
                        <div class="pagination">
                            <nav>
                                <ul class="pagination pagination-lg">
                                    <li class="prev">
                                        <a href="#" aria-label="Previous">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><span>…</span></li>
                                    <li class="next">
                                        <a href="#" aria-label="Next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
