<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
    <!-- header end -->
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                    <li><a href="#">My Account</a></li>
                    <li class="active">View Address</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>View Addresses</h1>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-4">
                        <h4>Default Ship To Address</h4>
                        <p>NW Hospital - 17th Floor, Imaging <br>Address Line 2 <br>Address Line 3</p>

                        <p>Need to add a new Ship To Address?
                            <br>Call 000-000-0000 or email
                            <br><a href="mailto:">person@your-company.com</a>.</p>
                    </div>
                    <div class="col-sm-4">
                        <h4>Billing Address</h4>
                        <p>
                            Company ABC <br>
                            454 State Street <br>
                            Chicago IL <br>
                            60630 <br>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <h4>Change Default Ship To Address</h4>
                <p>To change your default Ship To Address, select an address from the list of Ship To Addresses below and click “Confirm”.</p>
                <div class="well well-gray">
                    <h4>Search Ship To Addresses</h4>
                    <form class="row">
                        <div class="col-md-12">
                            <div class="row smaller">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control input-lg" placeholder="Search for Ship To Address">
                                    <br>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <a href="#" class="btn btn-lg btn-warning btn-block">Search</a>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <a href="#" class="btn btn-lg btn-default btn-block">Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="scrollable">
                    <div class="head">
                        <span>Select</span>
                        <span>Ship To</span>
                    </div>
                    <div class="content scrollbar-inner">

                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option2"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio checked">
                            <label class="radio-custom checked" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option3" checked> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option4"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option5"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option6"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option7"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option8"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option9"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                        <div class="radio">
                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                <input class="sr-only" name="radioEx1" type="radio" value="option10"> NW Hospital — 17th Floor, Imaging
                            </label>
                        </div>
                    </div>
                </div>
                <br>
                <a href="#" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#addressConfirm">Confirm</a>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
