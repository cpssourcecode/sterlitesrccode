<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Regional Managers</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Regional Managers</h1>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="sidebar-menu">
                                <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                                <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                                    <li><a href="#">Our Business</a></li>
                                    <li><a href="#">Locations</a></li>
                                    <li><a href="#">News and Events</a></li>
                                    <li><a href="#">Case Studies</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">Affiliations</a></li>
                                    <li class="active"><a href="#">Regional Managers</a></li>
                                    <li><a href="#">Price Lists</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="row managers-section">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 col-xxs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-xxs-5">
                                            <div class="image">
                                                <img src="assets/images/people/david-blake.png" alt="" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-xxs-7 visible-xxs">
                                            <h2>David Blake</h2>
                                            <h3>HVAC Sales Manager</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 col-xxs-12">
                                    <h2 class="hidden-xxs">David Blake</h2>
                                    <h3 class="hidden-xxs">HVAC Sales Manager</h3>
                                    <address>
                                        <strong>Location</strong>
                                        <p>Rockford, IL</p>
                                        <strong>Email</strong>
                                        <p><a href="#">cps-contact@columbiapipe.com</a></p>
                                        <strong>Phone</strong>
                                        <p>(773) 927-6600</p>
                                    </address>
                                    <article class="collapse-description">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero reprehenderit, adipisci quaerat labore doloremque eaque voluptatum in consequatur? Tenetur voluptatum inventore harum, ratione totam aliquid expedita unde eos atque dolor.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae consequuntur beatae quis quidem, minima ea quibusdam odit ex, veritatis omnis qui magnam dolorem dolores ab dignissimos fugit earum inventore commodi? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quae, vero a nesciunt error sequi! Fugiat sit culpa libero unde architecto magni soluta. Odit quo, eos ducimus vel commodi esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat eligendi voluptates consequuntur hic, neque, a quae itaque deleniti iste reiciendis exercitationem. Eligendi, illum, perspiciatis. Doloremque expedita deleniti libero nostrum, sed. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius corporis dolorum possimus, dolor id itaque rem sunt minus. Similique placeat odit mollitia doloribus, nam perspiciatis soluta delectus consequuntur provident, rerum.</p>
                                        <a class="more" href="#"><span>More</span></a>
                                    </article>
                                </div>
                            </div>
                            <div class="row managers-section">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 col-xxs-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-xxs-5">
                                            <div class="image">
                                                <img src="assets/images/people/russ-christian.jpg" alt="" class="img-responsive">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-xxs-7 visible-xxs">
                                            <h2>Russ Christian</h2>
                                            <h3>Regional Manager — Eastern Michigan</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-8 col-xxs-12">
                                    <h2 class="hidden-xxs">Russ Christian</h2>
                                    <h3 class="hidden-xxs">Regional Manager — Eastern Michigan</h3>
                                    <address>
                                        <strong>Location</strong>
                                        <p>Eastern Michigan</p>
                                        <strong>Email</strong>
                                        <p><a href="#">cps-contact@columbiapipe.com</a></p>
                                        <strong>Phone</strong>
                                        <p>(773) 927-6600</p>
                                    </address>
                                    <article class="collapse-description">
                                        <p>Russ comes to Columbia Pipe & Supply Co. with over 20 years of experience in the Industrial pipe, valves and fittings distribution business. The son of a mechanical contractor, he worked closely with plumbers, pipefitters and welders Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero reprehenderit, adipisci quaerat labore doloremque eaque voluptatum in consequatur? Tenetur voluptatum inventore harum, ratione totam aliquid expedita unde eos atque dolor.  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae consequuntur beatae quis quidem, minima ea quibusdam odit ex, veritatis omnis qui magnam dolorem dolores ab dignissimos fugit earum inventore commodi? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quae, vero a nesciunt error sequi! Fugiat sit culpa libero unde architecto magni soluta. Odit quo, eos ducimus vel commodi esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat eligendi voluptates consequuntur hic, neque, a quae itaque deleniti iste reiciendis exercitationem. Eligendi, illum, perspiciatis. Doloremque expedita deleniti libero nostrum, sed. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius corporis dolorum possimus, dolor id itaque rem sunt minus. Similique placeat odit mollitia doloribus, nam perspiciatis soluta delectus consequuntur provident, rerum.</p>
                                        <a class="more" href="#"><span>More</span></a>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
