<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
    <!-- header end -->
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                    <li><a href="my-account.php">My Account</a></li>
                    <li class="active">Edit Profile</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>Edit Profile</h1>
            </div>
            <div class="col-xs-12">
                <div class="material-list detail profile">
                    <div class="row">
                        <div class="col-sm-12 info-block profile-block">
                            <h4 class="title">Personal Information</h4>
                            <div class="info">
                                <div>
                                    <strong>Name</strong>
                                    <span>Frank Jones</span>
                                </div>
                                <div>
                                    <strong>Title</strong>
                                    <span>Purchasing Manager</span>
                                </div>
                            </div>
                            <button class="btn btn-default btn-lg profile-button" type="button" data-toggle="collapse" data-target="#collapseExample0" aria-expanded="false" aria-controls="collapseExample0">Edit</button>
                        </div>
                        <div class="col-sm-12 collapse" id="collapseExample0">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-9 col-md-7">
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                                </span>
                                            </button>
                                            Personal information changed successfully
                                        </div>
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                                </span>
                                            </button>
                                            Please, fill in <a class="error-anchor" href="#job-anchor"><span>Job</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-7 col-sm-9">
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-6">
                                                <input type="text" class="form-control input-lg" placeholder="" value="Frank">
                                                <p class="help-block">First Name</p>
                                            </div>
                                            <div class="col-sm-6 col-xs-6">
                                                <input type="text" class="form-control input-lg" placeholder="" value="Jones">
                                                <p class="help-block">Last Name</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group has-error">
                                    <div class="col-md-7 col-sm-9">
                                        <input type="text" id="job-anchor" class="form-control input-lg" placeholder="" value="Job Title">
                                        <br>
                                    </div>
                                    <div class="col-md-5 col-sm-12">
                                        <ul class="list-inline list-inline-buttons two">
                                            <li>
                                                <a href="#" class="btn btn-warning btn-lg">Save</a>
                                            </li>
                                            <li>
                                                <button class="btn btn-default btn-lg profile-edit-button" type="button" data-toggle="collapse" data-target="#collapseExample0" aria-expanded="false" aria-controls="collapseExample0">Cancel</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 info-block profile-block">
                            <h4 class="title">Contact Information</h4>
                            <div class="info">
                                <div>
                                    <strong>Email</strong>
                                    <span>frank@company-name.com</span>
                                </div>
                                <div>
                                    <strong>Mobile Phone</strong>
                                    <span>000-000-0000</span>
                                </div>
                                <div>
                                    <strong>Direct Phone</strong>
                                    <span>000-000-0000</span>
                                </div>
                                <div>
                                    <strong>Company Address</strong>
                                    <span>1234 Company Drive, Suite C, Chicago, IL 60605</span>
                                </div>
                            </div>
                            <button class="btn btn-default btn-lg profile-button" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Edit</button>
                        </div>
                        <div class="col-sm-12 collapse" id="collapseExample">
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    <p>Editing your Contact Information <strong>DOES NOT</strong> change or affect Bill To or Ship To information or settings and <strong>DOES NOT</strong> your login ID.</p>
                                </div>
                                <div class="col-sm-9 col-md-7 col-xs-12">
                                    <!-- <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                            </span>
                                        </button>
                                        Data changed successfully
                                    </div>
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                            </span>
                                        </button>
                                        Please, fill in <a class="error-anchor" href="#email-anchor"><span>Email</span></a>, <a class="error-anchor" href="#mobile-anchor"><span>Mobile Phone</span></a>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6 col-xxs-12">
                                            <div class="form-horizontal">
                                                <div class="form-group has-error">
                                                    <div class="col-xs-12">
                                                        <input id="email-anchor" type="email" class="form-control input-lg" placeholder="Alternate Email Address" value="Alternate Email Address">
                                                    </div>
                                                </div>
                                                <div class="form-group has-error">
                                                    <div class="col-xs-12">
                                                        <input id="mobile-anchor" type="text" class="form-control input-lg" placeholder="Mobile Phone" value="Mobile Phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control input-lg" placeholder="Direct Phone" value="Direct Phone">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control input-lg" placeholder="Company Address 1" value="Company Address 1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6 col-xxs-12">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control input-lg" placeholder="Company Address 2" value="Company Address 2">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control input-lg" placeholder="City" value="City">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <select class="form-control input-lg bootstrap-select" data-live-search="true" name="" id="">
                                                            <option value="AL">Alabama</option>
                                                            <option value="AK">Alaska</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="AR">Arkansas</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
                                                            <option value="CT">Connecticut</option>
                                                            <option value="DE">Delaware</option>
                                                            <option value="DC">District Of Columbia</option>
                                                            <option value="FL">Florida</option>
                                                            <option value="GA">Georgia</option>
                                                            <option value="HI">Hawaii</option>
                                                            <option value="ID">Idaho</option>
                                                            <option value="IL">Illinois</option>
                                                            <option value="IN">Indiana</option>
                                                            <option value="IA">Iowa</option>
                                                            <option value="KS">Kansas</option>
                                                            <option value="KY">Kentucky</option>
                                                            <option value="LA">Louisiana</option>
                                                            <option value="ME">Maine</option>
                                                            <option value="MD">Maryland</option>
                                                            <option value="MA">Massachusetts</option>
                                                            <option value="MI">Michigan</option>
                                                            <option value="MN">Minnesota</option>
                                                            <option value="MS">Mississippi</option>
                                                            <option value="MO">Missouri</option>
                                                            <option value="MT">Montana</option>
                                                            <option value="NE">Nebraska</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="NH">New Hampshire</option>
                                                            <option value="NJ">New Jersey</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="NY">New York</option>
                                                            <option value="NC">North Carolina</option>
                                                            <option value="ND">North Dakota</option>
                                                            <option value="OH">Ohio</option>
                                                            <option value="OK">Oklahoma</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="PA">Pennsylvania</option>
                                                            <option value="RI">Rhode Island</option>
                                                            <option value="SC">South Carolina</option>
                                                            <option value="SD">South Dakota</option>
                                                            <option value="TN">Tennessee</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="UT">Utah</option>
                                                            <option value="VT">Vermont</option>
                                                            <option value="VA">Virginia</option>
                                                            <option value="WA">Washington</option>
                                                            <option value="WV">West Virginia</option>
                                                            <option value="WI">Wisconsin</option>
                                                            <option value="WY">Wyoming</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-12">
                                                        <input type="number" class="form-control input-lg" placeholder="ZIP" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-5 col-sm-12">
                                        <ul class="list-inline list-inline-buttons two">
                                            <li>
                                                <a href="#" class="btn btn-warning btn-lg">Save</a>
                                            </li>
                                            <li>
                                                <button class="btn btn-default btn-lg profile-edit-button" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Cancel</button>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 info-block profile-block">
                            <h4 class="title">Login / Security Information</h4>
                            <div class="info">
                                <div>
                                    <strong>User ID</strong>
                                    <span>frank@company-name.com</span>
                                </div>
                                <div>
                                    <strong>Password</strong>
                                    <span>************</span>
                                </div>
                                <!-- <div>
                                    <strong>Security Question</strong>
                                    <span>Place of Birth</span>
                                </div>
                                <div>
                                    <strong>Answer</strong>
                                    <span>************</span>
                                </div> -->
                            </div>
                            <button class="btn btn-default btn-lg profile-button" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3">Edit</button>
                        </div>
                        <div class="col-sm-12 col-xs-8 col-xxs-12 collapse" id="collapseExample3">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-7 col-sm-9 col-xxs-12">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="email" class="form-control input-lg" placeholder="Old Password" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12 form-mobile-hint">
                                                <input type="text" class="form-control form-new-pass-focusable input-lg" placeholder="New Password" value="" data-toggle="popover //use data-attr “popover” to implement popover functionality" data-html="true" data-trigger="focus" data-content="
                                                    <p class='caption'>Password rules</p>
                                                    <ul class='popover-list'>
                                                        <li>Minimum of six and maximum of 15 characters;</li>
                                                        <li>Contain at least one uppercase alphabetic character;</li>
                                                        <li>Contain at least one numeric character;</li>
                                                        <li>Contain at least one special character (examples: !, @, #, $, %, &, *, _).</li>
                                                    </ul>
                                                    ">
                                                <div class="dropdown dropdown-hint visible-xs">
                                                    <button class="btn btn-default btn-lg" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-info-circle"></i>
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                        <p>Password rules</p>
                                                        <button class="btn btn-default btn-lg btn-close" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                        <ul class="rules-list">
                                                            <li>Minimum of six and maximum of 15 characters;</li>
                                                            <li>Contain at least one uppercase alphabetic character;</li>
                                                            <li>Contain at least one numeric character;</li>
                                                            <li>Contain at least one special character (examples: !, @, #, $, %, &, *, _).</li>
                                                        </ul>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="text" class="form-control form-new-pass-focusable input-lg" placeholder="Confirm New Password" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-9 col-xxs-12 hidden-xs">
                                        <div class="well well-password-hint-inline highlight">
                                            <p>Password rules</p>
                                            <ul class="rules-list">
                                                <li>Minimum of six and maximum of 15 characters;</li>
                                                <li>Contain at least one uppercase alphabetic character;</li>
                                                <li>Contain at least one numeric character;</li>
                                                <li>Contain at least one special character (examples: !, @, #, $, %, &, *, _).</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-5 col-sm-12">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <ul class="list-inline list-inline-buttons two">
                                                    <li>
                                                        <a href="#" class="btn btn-warning btn-lg">Save</a>
                                                    </li>
                                                    <li>
                                                        <button class="btn btn-default btn-lg profile-edit-button" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3">Cancel</button>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <div class="col-md-7 col-sm-9">
                                        <input type="text" class="form-control input-lg" placeholder="Security Question" value="">
                                    </div>
                                </div> -->
                                <!--<div class="form-group">-->
                                <!-- <div class="col-md-7 col-sm-9">
                                        <input type="text" class="form-control input-lg" placeholder="Security Question Answer" value="">
                                        <br>
                                    </div> -->
                                <!--<div class="col-md-5 col-sm-12">-->
                                <!--<ul class="list-inline">-->
                                <!--<li>-->
                                <!--<a href="#" class="btn btn-warning btn-lg">Save</a>-->
                                <!--</li>-->
                                <!--<li>-->
                                <!--<button class="btn btn-default btn-lg profile-button" type="button" data-toggle="collapse" data-target="#collapseExample3" aria-expanded="false" aria-controls="collapseExample3">Cancel</button>-->
                                <!--</li>-->
                                <!--</ul>-->
                                <!--</div>-->
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
