<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Category</a></li>
                        <li class="active">Carbon Steel Pipe</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Carbon Steel Pipe</h1>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4>Narrow Your Results</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="well well-gray well-sm well-first">
                                <form class="form-inline form-search">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-search-small input-lg" id="inputSearch" placeholder="Search Within Results">
                                    </div>
                                    <button type="submit" class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-search"></span></button>
                                </form>
                            </div>
                            <div class="accordion-filter panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default panel-fassets">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                      Manufacturer
                                    </a>
                                  </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="checkbox" id="myCheckbox1">
                                      <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label">Manufacturer 1</span>
                                      </label>
                                    </div>
                                    <div class="checkbox" id="myCheckbox2">
                                      <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label">Manufacturer 2</span>
                                      </label>
                                    </div>
                                    <div class="checkbox" id="myCheckbox3">
                                      <label class="checkbox-custom" data-initialize="checkbox">
                                        <input class="sr-only" type="checkbox" value="">
                                        <span class="checkbox-label">Manufacturer 3</span>
                                      </label>
                                    </div>
                                    <a href="#viewAllManufacturerOptions" data-toggle="modal" class="link">View All</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default panel-fassets">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                      Parameter 1
                                    </a>
                                  </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="radio">
                                      <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="option1">
                                        Radio 1
                                      </label>
                                    </div>

                                    <div class="radio checked">
                                      <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                        <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="option2">
                                        Radio 2
                                      </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse, neque assumenda. Officia eius nisi mollitia blanditiis vero eveniet aliquid iusto numquam qui enim, repellat ipsam corrupti dolores velit ex dicta?</p>
                                    <big><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero molestias nihil consequuntur error maiores voluptates aliquid, sint suscipit, iusto autem odio. Minima voluptates hic provident voluptatibus architecto, velit inventore laborum.</p></big>
                                </div>
                            </div>
                            <div class="row equalize-category">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">ERW 2-24 Pipe</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Domestic Seamless Pipe 2012</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Domestic OD SMLS 14-24 Pipe</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Presssure Pipe A106 & Global</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Fusion Bond Pipe</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Kottler HSC Items</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Cut to Order Pipe</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Marinette Marine</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Consignment</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/kitting.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">Consignment withmorelargertextinoneline</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12 visible-xs">
                                                <a href="#">
                                                    <h4 class="root-mobile-title">Buttwed Pipe</h4>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
