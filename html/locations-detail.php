<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Locations</a></li>
                        <li class="active">Rock Falls, IL</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Rock Falls, IL</h1>
                </div>
                <div class="locations-page col-xs-12">
                    <div class="row locations-row">
                        <div class="map-detail col-lg-4 col-md-6 col-xs-5 col-xxs-12">
                            <img class="img-responsive" src="http://wifamilyaction.org/wp-content/uploads/2014/11/appleton-wi.jpg" alt="">
                        </div>
                        <div class="locations-detail col-lg-4 col-md-6 col-xs-7 col-xxs-12">
                            <!-- text wich will never be used -->
                            The &laquo;Rock Falls&raquo; building has housed pipe, valves and fittings supply houses since it&nbsp;was built in&nbsp;1963. Columbia Pipe acquired the facility in&nbsp;1996 when we&nbsp;bought Mott Bros Co. Our warehouse contains pipe, valves, and fittings, plumbing, and HVAC products, and we&nbsp;deliver up&nbsp;to&nbsp;100 miles in&nbsp;any direction. The veteran staff averages 18&nbsp;years per person in&nbsp;the industry.
                            <p>The Rock Falls building has housed pipe, valves and fittings supply houses since it&nbsp;was built in&nbsp;1963. Columbia Pipe acquired the facility in&nbsp;1996 when we&nbsp;bought Mott Bros Co. Our warehouse contains pipe, valves, and fittings, plumbing, and HVAC products, and we&nbsp;deliver up&nbsp;to&nbsp;100 miles in&nbsp;any direction. The veteran staff averages 18&nbsp;years per person in&nbsp;the industry.
                                <ul>
                                    <li><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                                </ul>
                            </p>
                        </div>
                        <div class="col-lg-4 col-md-12 col-xs-12 col-xxs-12 locations-info">
                            <div class="block-part">
                                <strong>Address</strong>
                                <p>900 Industrial Avenue
                                    <br> Rock Falls, Illinois 61071</p>
                                <div class="form-group">
                                    <a href="#drivingDirections" data-toggle="modal"><i class="fa fa-map-marker"></i> Give Driving Directions</a>
                                </div>
                            </div>
                            <div class="block-part">
                                <strong>Phone</strong>
                                <p>(815) 625-6692</p>
                            </div>
                            <div class="block-part">
                                <strong>Normal Business Hours</strong>
                                <p>Sales Office: M-F 0:00 am — 0:00 pm
                                    <br> Will Call: M-F 0:00 am — 0:00 pm</p>
                                <p><strong>24/7 Emergency Service Available</strong></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
