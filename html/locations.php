<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-sm-8">
            <ol class="breadcrumb">
                <li class="active">Locations</li>
            </ol>
        </div>
        <div class="col-sm-4 page-actions">
            <ul class="list-inline">
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-envelope-o"></i><span>Email Page</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-print"></i><span>Print Page</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12">
            <h1>Locations</h1>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="accordion-filter panel-grouppanel-group locations" id="accordion-filter" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default panel-fassets">
                    <div class="panel-heading" role="tab" id="heading1">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-filter" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                Chicago Metro
                            </a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><strong>Rockford, IL</strong></a>
                                    <p>
                                        5730 Columbia Pkwy <br>
                                        Rockford, IL 61108 <br>
                                        (815) 229-3300
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Rock Falls, IL</strong></a>
                                    <p>
                                        900 Industrial Avenue <br>
                                        Rock Falls, Illinois 61071 <br>
                                        (815) 625-6692
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Woodstock, IL</strong></a>
                                    <p>
                                        1000 Courtaulds Drive <br>
                                        Woodstock, Illinois 60098 <br>
                                        (815) 334-0575
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-fassets">
                    <div class="panel-heading" role="tab" id="heading2">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-filter" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                Nothern Indiana
                            </a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><strong>Rockford, IL</strong></a>
                                    <p>
                                        5730 Columbia Pkwy <br>
                                        Rockford, IL 61108 <br>
                                        (815) 229-3300
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Rock Falls, IL</strong></a>
                                    <p>
                                        900 Industrial Avenue <br>
                                        Rock Falls, Illinois 61071 <br>
                                        (815) 625-6692
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Woodstock, IL</strong></a>
                                    <p>
                                        1000 Courtaulds Drive <br>
                                        Woodstock, Illinois 60098 <br>
                                        (815) 334-0575
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-fassets">
                    <div class="panel-heading" role="tab" id="heading3">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-filter" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                NW Illinois
                            </a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><strong>Rockford, IL</strong></a>
                                    <p>
                                        5730 Columbia Pkwy <br>
                                        Rockford, IL 61108 <br>
                                        (815) 229-3300
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Rock Falls, IL</strong></a>
                                    <p>
                                        900 Industrial Avenue <br>
                                        Rock Falls, Illinois 61071 <br>
                                        (815) 625-6692
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Woodstock, IL</strong></a>
                                    <p>
                                        1000 Courtaulds Drive <br>
                                        Woodstock, Illinois 60098 <br>
                                        (815) 334-0575
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-fassets">
                    <div class="panel-heading" role="tab" id="heading4">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-filter" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
                                Central Illinois
                            </a>
                        </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><strong>Rockford, IL</strong></a>
                                    <p>
                                        5730 Columbia Pkwy <br>
                                        Rockford, IL 61108 <br>
                                        (815) 229-3300
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Rock Falls, IL</strong></a>
                                    <p>
                                        900 Industrial Avenue <br>
                                        Rock Falls, Illinois 61071 <br>
                                        (815) 625-6692
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Woodstock, IL</strong></a>
                                    <p>
                                        1000 Courtaulds Drive <br>
                                        Woodstock, Illinois 60098 <br>
                                        (815) 334-0575
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-fassets">
                    <div class="panel-heading" role="tab" id="heading5">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-filter" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
                                Wisconsin
                            </a>
                        </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><strong>Rockford, IL</strong></a>
                                    <p>
                                        5730 Columbia Pkwy <br>
                                        Rockford, IL 61108 <br>
                                        (815) 229-3300
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Rock Falls, IL</strong></a>
                                    <p>
                                        900 Industrial Avenue <br>
                                        Rock Falls, Illinois 61071 <br>
                                        (815) 625-6692
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Woodstock, IL</strong></a>
                                    <p>
                                        1000 Courtaulds Drive <br>
                                        Woodstock, Illinois 60098 <br>
                                        (815) 334-0575
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-fassets">
                    <div class="panel-heading" role="tab" id="heading6">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-filter" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
                                Michigan
                            </a>
                        </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><strong>Rockford, IL</strong></a>
                                    <p>
                                        5730 Columbia Pkwy <br>
                                        Rockford, IL 61108 <br>
                                        (815) 229-3300
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Rock Falls, IL</strong></a>
                                    <p>
                                        900 Industrial Avenue <br>
                                        Rock Falls, Illinois 61071 <br>
                                        (815) 625-6692
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Woodstock, IL</strong></a>
                                    <p>
                                        1000 Courtaulds Drive <br>
                                        Woodstock, Illinois 60098 <br>
                                        (815) 334-0575
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default panel-fassets">
                    <div class="panel-heading" role="tab" id="heading7">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion-filter" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
                                Minnesota
                            </a>
                        </h4>
                    </div>
                    <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="#"><strong>Rockford, IL</strong></a>
                                    <p>
                                        5730 Columbia Pkwy <br>
                                        Rockford, IL 61108 <br>
                                        (815) 229-3300
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Rock Falls, IL</strong></a>
                                    <p>
                                        900 Industrial Avenue <br>
                                        Rock Falls, Illinois 61071 <br>
                                        (815) 625-6692
                                    </p>
                                </li>
                                <li class="list-group-item">
                                    <a href="#"><strong>Woodstock, IL</strong></a>
                                    <p>
                                        1000 Courtaulds Drive <br>
                                        Woodstock, Illinois 60098 <br>
                                        (815) 334-0575
                                    </p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="locationsMap" class="col-md-9 col-xs-12">
            <img src="assets/images/map2.png" class="img-responsive" style="width: 100%;" alt=""/>
        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>