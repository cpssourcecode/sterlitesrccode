<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged-home.php'; ?>
        <!-- header end -->
        <div class="owl-home hide hidden-xs">
            <div>
                <div class="image" style="background: url(assets/images/home-slider/slide-bg.png);">
                    <!-- <img src="assets/images/slide-bg.png" alt=""> -->
                </div>
                <div class="lead-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 col-sm-offset-3">
                                <h2>Big Headline 1</h2>
                                <p class="lead">Smaller sub-copy that promotes something for viewer to take action on.</p>
                            </div>
                            <div class="col-sm-4 col-xs-12 text-right">
                                <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="image" style="background: url(assets/images/home-slider/black-and-white-sky-construction-bridge.jpg)">
                    <!-- <img src="assets/images/home-slider/black-and-white-sky-construction-bridge.jpg" alt=""> -->
                </div>
                <div class="lead-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 col-sm-offset-3">
                                <h2>Big Headline 2</h2>
                                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis pariatur numquam quasi nemo.</p>
                            </div>
                            <div class="col-sm-4 col-xs-12 text-right">
                                <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div class="image" style="background: url(assets/images/home-slider/landmark-bridge-metal-architecture.jpg)">
                    <!-- <img src="assets/images/home-slider/landmark-bridge-metal-architecture.jpg" alt=""> -->
                </div>
                <div class="lead-block">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5 col-xs-12 col-sm-offset-3">
                                <h2>Big Headline 3</h2>
                                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt quos perspiciatis suscipit saepe laudantium vel nihil possimus temporibus ducimus.</p>
                            </div>
                            <div class="col-sm-4 col-xs-12 text-right">
                                <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container main-content">
            <div class="row">
                <!-- <div class="col-sm-3 hidden-xs">
                    <div class="well">
                        <h3>Our Customers Love Us</h3>
                        <blockquote>
                            <p>Their service is fantastic. Our branch manager is knowledgeable and has great technical expertise.”</p>
                            <footer>Someone famous in
                                <cite title="Source Title">Source Title</cite>
                            </footer>
                        </blockquote>
                        <a class="testimonial-detail" href="#"><span>Read More testimonials</span> <span class="glyphicon glyphicon-menu-right"></span></a>
                    </div>
                </div> -->
                <div class="col-sm-9 col-sm-offset-3">
                    <h3 class="page-title centered-mobile">Explore Popular Products</h3>
                    <div class="row equalize">
                        <div class="col-xs-4 col-xxs-12">
                            <div class="thumbnail">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="image">
                                            <img src="assets/images/pipe.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="thumbnail-content">
                                            <h4>Pipe</h4>
                                            <ul class="list-unstyled">
                                                <li><a href="#">Carbon Steel Pipe</a></li>
                                                <li><a href="#">Carbon Steel</a></li>
                                                <li><a href="#">Soil Pipe & Fittings</a></li>
                                                <li><a href="#">SS Pipe</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="floated-bottom">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#findInvoice">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-xxs-12">
                            <div class="thumbnail">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="image">
                                            <img src="assets/images/water-hitters.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="thumbnail-content">
                                            <h4>Water Heaters</h4>
                                            <ul class="list-unstyled">
                                                <li><a href="#">Carbon Steel Pipe</a></li>
                                                <li><a href="#">Carbon Steel</a></li>
                                                <li><a href="#">Soil Pipe & Fittings</a></li>
                                                <li><a href="#">SS Pipe</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="floated-bottom">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#findInvoice">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-xxs-12">
                            <div class="thumbnail">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="image">
                                            <img src="assets/images/plumbing.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="thumbnail-content">
                                            <h4>Plumbing</h4>
                                            <ul class="list-unstyled">
                                                <li><a href="#">Carbon Steel Pipe</a></li>
                                                <li><a href="#">Carbon Steel</a></li>
                                                <li><a href="#">Soil Pipe & Fittings</a></li>
                                                <li><a href="#">SS Pipe</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="floated-bottom">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#findInvoice">View All</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3 class="centered-mobile">Services</h3>
                    <div class="row equalize">
                        <div class="col-xs-4 col-xxs-12">
                            <div class="thumbnail">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <a href="#">
                                            <div class="image">
                                                <img src="assets/images/kitting.png" alt="">
                                            </div>
                                            <h4 class="hidden-xs">Kitting</h4>
                                        </a>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="thumbmail-content no-qty">
                                            <h4 class="visible-xs">Kitting</h4>
                                            <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil, id totam sint dolor molestiae natus minus dolores consectetur pariatur eligendi</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="floated-bottom">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="#" class="btn btn-warning">Learn More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-xxs-12">
                            <div class="thumbnail">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <a href="#">
                                            <div class="image">
                                                <img src="assets/images/standartization.png" alt="">
                                            </div>
                                            <h4 class="hidden-xs">Standartisation</h4>
                                        </a>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="thumbnail-content no-qty">
                                            <h4 class="visible-xs">Standartisation</h4>
                                            <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil, id totam sint dolor molestiae natus minus dolores consectetur pariatur eligendi, iure, accusamus quisquam dolorum accusantium. Rerum illo, sapiente?</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="floated-bottom">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="#" class="btn btn-warning">Learn More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-xxs-12">
                            <div class="thumbnail">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <a href="#">
                                            <div class="image">
                                                <img src="assets/images/after-sales-service.png" alt="">
                                            </div>
                                            <h4 class="hidden-xs">After Sales Service</h4>
                                        </a>
                                    </div>
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="thumbnail-content no-qty">
                                            <h4 class="visible-xs">After Sales Service</h4>
                                            <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="floated-bottom">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="#" class="btn btn-warning">Learn More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <h3 class="centered-mobile">You May Be Interested In</h3>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="owl-thumbnails">
                                <div class="item">
                                    <div class="thumbnail">
                                        <div class="row">
                                            <div class="col-sm-12 col-xs-12">
                                                <a href="#">
                                                    <div class="image">
                                                        <img src="assets/images/pipe.png" alt="">
                                                    </div>
                                                    <h4 class="hidden-xs">After Sales Service</h4>
                                                </a>
                                            </div>
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="thumbnail-content">
                                                    <a href="#">
                                                        <h4 class="visible-xs">After Sales Service</h4>
                                                    </a>
                                                    <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil adipisicing elit. Enim facere nihil</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row counter floated-bottom">
                                            <div class="col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-sm-12 col-xs-12">
                                                        <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                    </div>
                                                    <div class="col-sm-5 col-xs-4 qty-field">
                                                        <input type="text" class="form-control input-sm text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php for ($i=0; $i < 6; $i++) { 
                                    echo "
                                        <div class='item'>
                                    <div class='thumbnail'>
                                        <div class='row'>
                                            <div class='col-sm-12 col-xs-12'>
                                                <a href='#'>
                                                    <div class='image'>
                                                        <img src='assets/images/pipe.png' alt=''>
                                                    </div>
                                                    <h4 class='hidden-xs'>After Sales Service</h4>
                                                </a>
                                            </div>
                                            <div class='col-sm-12 col-xs-12'>
                                                <div class='thumbnail-content'>
                                                    <a href='#'>
                                                        <h4 class='visible-xs'>After Sales Service</h4>
                                                    </a>
                                                    <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='row counter floated-bottom'>
                                            <div class='col-sm-12 col-xs-12'>
                                                <div class='row'>
                                                    <div class='col-sm-12 col-xs-12'>
                                                        <big><span class='text-success'>$54.60</span><span>/unit</span></big>
                                                    </div>
                                                    <div class='col-sm-5 col-xs-4 qty-field'>
                                                        <input type='text' class='form-control input-sm text-center' placeholder='Qty' value='1'>
                                                    </div>
                                                    <div class='col-sm-7 col-xs-8'>
                                                        <a href='#' class='btn btn-warning btn-block'>Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    ";
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="well visible-xs centered-mobile">
                                            <h3>Our Customers Love Us</h3>
                                            <blockquote>
                                                <p>Their service is fantastic. Our branch manager is knowledgeable and has great technical expertise.”</p>
                                                <footer>Someone famous in
                                                    <cite title="Source Title">Source Title</cite>
                                                </footer>
                                            </blockquote>
                                            <a class="testimonial-detail" href="#"><span>Read More testimonials</span> <span class="glyphicon glyphicon-menu-right"></span></a>
                                        </div>
                                        <div class="thumbnail thumbnail-testimonial visible-xs">
                                            <div id="carousel-example-generic" class="carousel slide testimonial" data-ride="carousel">
                                                <!-- Indicators -->
                                                <ol class="carousel-indicators">
                                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                                </ol>
                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active">
                                                        <div class="carousel-caption">
                                                            <a href="#">
                                                                <h3>
                                                    <span>Customizable Headline</span>
                                                </h3>
                                                                <div class="image">
                                                                    <img src="assets/images/after-sales-service.png" alt="">
                                                                </div>
                                                            </a>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae illum sapiente cupiditate suscipit. Consectetur dolorem quae, quaerat, blanditiis recusandae, culpa animi assumenda eligendi qui voluptatum eius. Enim repellat, velit fugiat.</p>
                                                            <a href="#" class="read-more"><span>Learn More</span><i class="fa fa-angle-right"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="carousel-caption">
                                                            <a href="#">
                                                                <h3>
                                                    <span>Testimonial Title 2</span>
                                                </h3>
                                                                <div class="image">
                                                                    <img src="assets/images/after-sales-service.png" alt="">
                                                                </div>
                                                            </a>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint sed, est doloribus ipsum velit itaque, deleniti corporis numquam, dolores nihil id soluta mollitia quas. Perferendis dolorum facere, et saepe alias!</p>
                                                            <a href="#" class="read-more"><span>Learn More</span><i class="fa fa-angle-right"></i></a>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="carousel-caption">
                                                            <a href="#">
                                                                <h3>
                                                    <span>Testimonial Title 3</span>
                                                </h3>
                                                                <div class="image">
                                                                    <img src="assets/images/after-sales-service.png" alt="">
                                                                </div>
                                                            </a>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa eaque quisquam, iste eligendi cumque excepturi doloremque ducimus illo fugiat placeat veritatis temporibus repellendus aperiam! Nemo, minima molestiae dolorum dolores quis.</p>
                                                            <a href="#" class="read-more"><span>Learn More</span><i class="fa fa-angle-right"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
