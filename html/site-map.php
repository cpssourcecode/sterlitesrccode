<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Site Map page - CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-xs-12">
            <h1>Site Map</h1>
            <div class="site-map">
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <h4>Products</h4>
                        <ul>
                            <li><a href="#">Carbon Steel Pipe</a></li>
                            <li><a href="#">SS Pipe & Fittings</a></li>
                            <li><a href="#">Soil Pipe Fittings</a></li>
                            <li><a href="#">Fittings and Nipples</a></li>
                            <li><a href="#">Hangers/Rod/Strut</a></li>
                            <li><a href="#">Weld Fittings & Flanges</a></li>
                            <li><a href="#">Valves</a></li>
                            <li><a href="#">Plumbing Fixtures</a></li>
                            <li><a href="#">Plumbing Brass</a></li>
                            <li><a href="#">Water Heaters</a></li>
                            <li><a href="#">Pumps</a></li>
                            <li><a href="#">Hydrongic Equipment</a></li>
                            <li><a href="#">Hydronic Misc</a></li>
                            <li><a href="#">HVAC Equipment</a></li>
                            <li><a href="#">HVAC Assoc. Products</a></li>
                            <li><a href="#">Maintenance Supplies</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <h4>My Account</h4>
                        <ul>
                            <li><a href="#">My Account Overview</a></li>
                        </ul>
                        <h4>Service</h4>
                        <ul>
                            <li><a href="#">After Sales Service</a></li>
                            <li><a href="#">CAPS Valve Automation</a></li>
                            <li><a href="#">Columbia Express™ - Rapid Delivery</a></li>
                            <li><a href="#">Commercial Plan & Spec</a></li>
                            <li><a href="#">E-Commerce</a></li>
                            <li><a href="#">HVA</a></li>
                            <li><a href="#">C Inter-Branch Transfer/Cross Dock System</a></li>
                            <li><a href="#">Kitting</a></li>
                            <li><a href="#">Nationwide and Regional Distribution Services</a></li>
                            <li><a href="#">The Pipe Works™</a></li>
                            <li><a href="#">Custom Fabrication</a></li>
                            <li><a href="#">Product and Business Expertise</a></li>
                            <li><a href="#">Product Training</a></li>
                            <li><a href="#">Rolling Warehouse™</a></li>
                            <li><a href="#">$mart$tock™ – Inventory Management</a></li>
                            <li><a href="#">Standardization</a></li>
                            <li><a href="#">Supplier Reviews</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <h4>Our Business</h4>
                        <ul>
                            <li><a href="#">Our Business</a></li>
                            <li><a href="#">Locations</a></li>
                            <li><a href="#">News and Events</a></li>
                            <li><a href="#">Affiliations</a></li>
                            <li><a href="#">Price Sheets</a></li>
                        </ul>
                        <h4>Support</h4>
                        <ul>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">FAQs</a></li>
                            <li><a href="#">Video Library</a></li>
                            <li><a href="#">Site Map</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>

</html>