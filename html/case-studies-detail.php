<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Case Studies</a></li>
                        <li class="active">Advance Mechanical Systems</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Advance Mechanical Systems</h1>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="sidebar-menu">
                                <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                                <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                                    <li><a href="#">Our Business</a></li>
                                    <li><a href="#">Locations</a></li>
                                    <li><a href="#">News and Events</a></li>
                                    <li class="active"><a href="#">Case Studies</a></li>
                                    <li><a href="#">Testimonials</a></li>
                                    <li><a href="#">Affiliations</a></li>
                                    <li><a href="#">Regional Managers</a></li>
                                    <li><a href="#">Price Lists</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="row">
                                <div class="col-xs-12">
                                    <article class="case-study-detail">
                                        <img class="img-responsive" src="assets/images/aspe-logo166Ht_0.png" alt="">
                                        <h2>Problem</h2>
                                        <p>A change order from a general contractor doing a round-the-clock stadium renovation on a Saturday night resulted in a mechanical contractor not having the required materials to perform a scheduled Sunday morning, with two tradesmen, on double time for a 12 hour shift each.</p>
                                        <h2>Solution</h2>
                                        <p>Our customer placed a Sunday 4:00 am emergency call to Columbia Pipe & Supply Co. that was answered on the third ring, the branch was opened within 30 minutes, all required materials were in-stock, picked and packed, loaded onto the contractor’s truck that was back to the job site by 5:40 a.m.</p>
                                        <h2>Value</h2>
                                        <p>If the contractor was not able to get the overnight change order material, the situation would have pushed all trades schedules back and possibly delay the opening of the start of the season. It would have totaled 30 man hours behind on the plumbing side which has a trickledown effect. In fact, it would have pushed the entire schedule for all trades back a couple of days and possibly delay the Chicago’s north side major league baseball team opener.</p>
                                        <h2>Testimonial</h2>
                                        <blockquote>
                                            <p>“Columbia Pipe came through in a big way. Actually, in an incredible way! Not only was the phone answered at 4:00 a.m. by Shawn Ramos on the third ring, but Columbia Pipe had everything we needed in stock to complete the change order. If we were not able to get the material, that would have pushed all trades back and possibly delay the start of the season for major league baseball on the north side. Try to wrap your head around that, Shawn, you could of actually played a role ensuring the Chicago’s north side major league baseball team opener! Columbia Pipe is awesome!”</p>
                                            <footer>Tony Laciak, Advance Mechanical Systems Inc.</footer>
                                        </blockquote>
                                    </article>
                                </div>
                            </div>
                            <div class="text-center hidden-print">
                                <div class="pagination">
                                    <nav>
                                        <ul class="pagination pagination-lg">
                                            <li class="prev">
                                                <a href="#" aria-label="Previous">
                                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                                </a>
                                            </li>
                                            <li class="active"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><span>…</span></li>
                                            <li class="next">
                                                <a href="#" aria-label="Next">
                                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
