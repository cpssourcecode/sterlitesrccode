<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li class="active">View Statements</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>View Statements</h1>
                </div>
                <div class="col-xs-12">
                    <div class="well well-gray">
                        <a href="#" class="btn btn-lg btn-warning" data-toggle="modal" data-target="#requestStatement">Request a Current Statement</a>
                        <br>
                        <br>
                        <span>To View Statements older then one year, <a href="#">contact your Sales Team</a>.</span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><strong>Month</strong></th>
                                <th colspan="2"><strong>Year</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>April</th>
                                <td>2015</td>
                                <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Statement</span></a></td>
                            </tr>
                            <tr>
                                <th>March</th>
                                <td>2015</td>
                                <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Statement</span></a></td>
                            </tr>
                            <tr>
                                <th>February</th>
                                <td>2015</td>
                                <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Statement</span></a></td>
                            </tr>
                            <tr>
                                <th>January</th>
                                <td>2015</td>
                                <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Statement</span></a></td>
                            </tr>
                            <tr>
                                <th>December</th>
                                <td>2014</td>
                                <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Statement</span></a></td>
                            </tr>
                            <tr>
                                <th>November</th>
                                <td>2014</td>
                                <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Statement</span></a></td>
                            </tr>
                            <tr>
                                <th>October</th>
                                <td>2014</td>
                                <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Statement</span></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
        <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
