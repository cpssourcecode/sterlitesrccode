<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Manage Users</a></li>
                        <li class="active">Add User</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1>Add User</h1>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="well well-gray manage-users">
                        <h4>Status</h4>
                        <form class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="radio checked">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                        <input class="sr-only" name="radioEx1" type="radio" value="nospending"> Active
                                    </label>
                                </div>
                                <div class="radio">
                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                        <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="defaultspending"> Deactivated
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row default-spending-form" style="display: none;">
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control input-lg" placeholder="$">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr class="row">
                                <h4>User Information</h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-horizontal">
                                        <div class="row form-group">
                                            <div class="col-md-6 col-xs-6">
                                                <input type="text" class="form-control form-error" placeholder="*First Name">
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                <input type="text" class="form-control" placeholder="*Last Name">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12 col-xs-12">
                                                <input type="text" class="form-control" placeholder="*Email Address (will be User's login ID)">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-12 col-xs-12">
                                                <input type="text" class="form-control" placeholder="*Phone">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr class="row">
                                <h4>Assign User to a Company</h4>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-horizontal">
                                <div class="row form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <select name="" id="" class="form-control">
                                            <option value="">*Company Name</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr class="row">
                                <h4>*Role(s)</h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="checkbox" id="myCheckbox">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">Admin</span>
                                  </label>
                                </div>
                                <div class="checkbox" id="myCheckbox">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">Approver</span>
                                  </label>
                                </div>
                                <div class="checkbox" id="myCheckbox">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">Buyer</span>
                                  </label>
                                </div>
                                <div class="checkbox" id="myCheckbox">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">Finance</span>
                                  </label>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr class="row">
                                <h4>*Spending Limit and Frequency</h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                <div class="form-group">
                                    <p>Entering a spending limit and frequency below will overide default spending limit and frequency settings for <strong>THIS USER ONLY</strong></p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-6 col-xs-6">
                                        <div class="input-group">
                                          <div class="input-group-addon">$</div>
                                          <input type="text" class="form-control" placeholder="">
                                        </div>
                                        <!-- <input type="text" class="form-control" placeholder="$"> -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="radio checked">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel3">
                                                <input class="sr-only" name="radioEx2" type="radio" value="perorder"> Per Order
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel4">
                                                <input class="sr-only" name="radioEx2" type="radio" value="perday"> Per Day
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel5">
                                                <input class="sr-only" name="radioEx2" type="radio" value="perweek"> Per Week
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel6">
                                                <input class="sr-only" name="radioEx2" type="radio" value="permonth"> Per Month
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <hr class="row">
                                <h4>*Ship To Address(es)</h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <p>Click «Add Ship to Address(es)» below to add Ship to Address(es) and to assign a default Ship to Address for this User. If you need to add a NEW Ship to Address, contact your Sales Team. </p>
                                <strong>*Default</strong>
                                <address>
                                    NW Hospital — 17th Floor, Imaging <br>
                                    Address Line 2 <br>
                                    Address Line 3
                                </address>
                                <button type="button" data-toggle="modal" data-target="#addShipToAddress" class="btn btn-default add-ship"><i class="fa fa-plus"></i> Add Ship To Address(es)</button>
                                <div class="user-addresses">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <address>
                                                NW Hospital — 17th Floor, Imaging <br>
                                                Address Line 2 <br>
                                                Address Line 3
                                            </address>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="radio">
                                              <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx11" type="radio" value="option11">
                                                Make default
                                              </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <a href="#" class="gray-noreload"><span>Remove</span></a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <address>
                                                NW Hospital — 17th Floor, Imaging <br>
                                                Address Line 2 <br>
                                                Address Line 3
                                            </address>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="radio">
                                              <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx11" type="radio" value="option21">
                                                Make default
                                              </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <a href="#" class="gray-noreload"><span>Remove</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="row well-gray form-group">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 well-gray">
                                <ul class="list-inline">
                                    <li>
                                        <a href="#" class="btn btn-warning btn-lg">Save and Add User</a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn btn-default btn-lg">Cancel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
