<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Testimonials Page — CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-sm-8">
            <ol class="breadcrumb">
                <li class="active">Testimonials</li>
            </ol>
        </div>
        <div class="col-sm-4 page-actions">
            <ul class="list-inline">
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-envelope-o"></i><span>Email Page</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-print"></i><span>Print Page</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12">
            <h1>Testimonials</h1>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <div class="sidebar-menu">
                        <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                        <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                            <li><a href="#">Our Business</a></li>
                            <li><a href="#">Locations</a></li>
                            <li><a href="#">News and Events</a></li>
                            <li><a href="#">Case Studies</a></li>
                            <li class="active"><a href="#">Testimonials</a></li>
                            <li><a href="#">Affiliations</a></li>
                            <li><a href="#">Regional Managers</a></li>
                            <li><a href="#">Price Lists</a></li>
                        </ul>
                    </div>
                    <div class="testimonial-container-desktop">
                        <!-- <div class="sidebar-block">
                            <h4>Submit a Testimonial</h4>
                            <div class="sidebar-form">
                                <form action="">
                                    <div class="form-group">
                                        <input type="text" name class="form-control" placeholder="*First Name" required/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="*Last Name" required/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="*Company" required/>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" name="testimonialsMsg" id="testimonialsMsg" cols="6"
                                                  rows="5" maxlength="250" placeholder="*Testimonial Message (Max 250 Characters)" required></textarea>
                                    </div>
                                    <div class="form-group text-center">
                                        <strong>Rate Us (optional)</strong>
                                        <div class="star-rating">
                                            <label for="star-rating-1"><i class="fa fa-star-o"></i></label>
                                            <label for="star-rating-2"><i class="fa fa-star-o"></i></label>
                                            <label for="star-rating-3"><i class="fa fa-star-o"></i></label>
                                            <label for="star-rating-4"><i class="fa fa-star-o"></i></label>
                                            <input type="radio" name="star-rating" id="star-rating-1" class="hidden rating-input" value="1" />
                                            <input type="radio" name="star-rating" id="star-rating-2" class="hidden rating-input" value="2" />
                                            <input type="radio" name="star-rating" id="star-rating-3" class="hidden rating-input" value="3" />
                                            <input type="radio" name="star-rating" id="star-rating-4" class="hidden rating-input" value="4" />
                                        </div>
                                    </div>


                                    <button type="submit" class="btn btn-default btn-block btn-lg"> Submit</button>
                                </form>
                            </div>
                        </div> -->
                    </div>
                    
                </div>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <article>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat.</p><br>
                    </article>
                    <div class="testimonial-list">
                        <div class="testimonial-item collapsed-testimonial">
                            <q class="testimonial-text">Their service is fantastic. Our branch manager is knowledgeable and has great technical expertise. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia vero recusandae dicta non illo consequuntur quod. Incidunt expedita, quia distinctio veritatis necessitatibus similique culpa eum officiis vitae minima nostrum, ipsum. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi praesentium molestiae ipsa, sed officiis illo atque eum illum, quibusdam dolores expedita aspernatur necessitatibus dolorem modi veritatis. Commodi laborum, enim dolorem?</q>
                            <div class="testimonial-author">Carl Ellison, Allied Mechanical</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text collapse-description">
                                <div class="quote">
                                    <div>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero reprehenderit, adipisci quaerat labore doloremque eaque voluptatum in consequatur? Tenetur voluptatum inventore harum, ratione totam aliquid expedita unde eos atque dolor. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae consequuntur beatae quis quidem, minima ea quibusdam odit ex, veritatis omnis qui magnam dolorem dolores ab dignissimos fugit earum inventore commodi?
                                    </div>
                                    <div>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero reprehenderit, adipisci quaerat labore doloremque eaque voluptatum in consequatur? Tenetur voluptatum inventore harum, ratione totam aliquid expedita unde eos atque dolor. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae consequuntur beatae quis quidem, minima ea quibusdam odit ex, veritatis omnis qui magnam dolorem dolores ab dignissimos fugit earum inventore commodi?
                                    </div>
                                    <div>
                                         Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore quae, vero a nesciunt error sequi! Fugiat sit culpa libero unde architecto magni soluta. Odit quo, eos ducimus vel commodi esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat eligendi voluptates consequuntur hic, neque, a quae itaque deleniti iste reiciendis exercitationem. Eligendi, illum, perspiciatis. Doloremque expedita deleniti libero nostrum, sed. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius corporis dolorum possimus, dolor id itaque rem sunt minus. Similique placeat odit mollitia doloribus, nam perspiciatis soluta delectus consequuntur provident, rerum.
                                    </div>
                                </div>
                                <a class="more" href="#"><span>More</span></a>
                            </q>

                            <!-- <q class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</q> -->
                            <div class="testimonial-author">Tom Jones, ACME Building Supply</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text collapse-description">
                                <div class="quote">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.
                                </div>
                                <a class="more" href="#"><span>More</span></a>
                            </q>
                            <div class="testimonial-author">Tom Jones, ACME Building Supply</div>
                            <!-- <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div> -->
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text">Their service is fantastic. Our branch manager is knowledgeable and has great technical expertise.</q>
                            <div class="testimonial-author">Carl Ellison, Allied Mechanical</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</q>
                            <div class="testimonial-author">Tom Jones, ACME Building Supply</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</q>
                            <div class="testimonial-author">Tom Jones, ACME Building Supply</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text">Their service is fantastic. Our branch manager is knowledgeable and has great technical expertise.</q>
                            <div class="testimonial-author">Carl Ellison, Allied Mechanical</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</q>
                            <div class="testimonial-author">Tom Jones, ACME Building Supply</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</q>
                            <div class="testimonial-author">Tom Jones, ACME Building Supply</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                        <div class="testimonial-item">
                            <q class="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.</q>
                            <div class="testimonial-author">Tom Jones, ACME Building Supply</div>
                            <div class="testimonial-rating"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i></div>
                            <a href="#"><svg class="icon icon-file-13"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-file-13"></use></svg> View Case Study</a>
                        </div>
                    </div>

                    <div class="text-center">
                        <div class="pagination">
                            <nav>
                                <ul class="pagination pagination-lg">
                                    <li class="prev">
                                        <a href="#" aria-label="Previous">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                        </a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><span>…</span></li>
                                    <li class="next">
                                        <a href="#" aria-label="Next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="testimonial-container-mobile">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>

</html>