<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li class="active">View Invoices</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>View Invoices</h1>
                </div>
                <div class="col-xs-12">
                    <div class="well well-gray well-invoices">
                        <h4>Search Incoices</h4>
                        <ul class="description">
                            <li>Searching by Web Confirmation #, Sales Order #, or PO # will return ALL Packing Slip History records (no date limit);</li>
                            <li>Searching by Ship to Address will return Packing Slip History records up to 120 days old;</li>
                            <li>Searching by Date Range will return records up to 30, 60, 90, or 120 days old.</li>
                        </ul>
                        <form class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="row smaller">
                                            <div class="col-md-7 col-sm-3 col-xs-4 col-xxs-6">
                                                <div class="radio">
                                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                        <input class="sr-only" name="radioEx1" type="radio" value="option0"> Web Confirmation #
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                        <input class="sr-only" name="radioEx1" type="radio" value="option1"> Order #
                                                    </label>
                                                </div>
                                                <div class="radio checked">
                                                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                                        <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="option2"> PO #
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-sm-3 col-xs-3 col-xxs-6">
                                                <input type="text" class="form-control input-lg xs-form-margin" placeholder="#">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12">
                                        <div class="row smaller">
                                            <div class="col-md-5 col-sm-4 col-xs-5 col-xxs-12">
                                                <select name="" id="shipTo" class="bootstrap-select">
                                                    <option value="" disabled selected data-hidden="true">Ship to Address</option>
                                                    <option value="1" title="address1">NW Hosp — 17th Floor, Imaging (Current)</option>
                                                    <option value="2" title="address2">PR Hosp — 8th Floor</option>
                                                </select>
                                            </div>
                                            <div class="col-md-3 col-sm-3 col-xs-3 col-xxs-12">
                                                <select name="" id="dateRange" class="bootstrap-select" >
                                                    <option value="" disabled selected data-hidden="true">Date Range</option>
                                                    <option value="">30 days</option>
                                                    <option value="">60 days</option>
                                                    <option value="">90 days</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2 col-xs-2 col-xxs-6">
                                                <a href="#" class="btn btn-lg btn-warning btn-block btn-invoices-search">Search</a>
                                            </div>
                                            <div class="col-sm-2 col-xs-2 col-xxs-6">
                                                <a href="#" class="btn btn-lg btn-default btn-block btn-invoices-search">Reset</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-mobile table-invoices">
                        <thead>
                            <tr>
                                <th><strong>Invoice</strong> <i class="fa fa-sort"></i></th>
                                <th><strong>Invoice Date</strong> <i class="fa fa-sort"></i></th>
                                <th><strong>Ship To</strong></th>
                                <th><strong>Amount</strong></th>
                                <th colspan="2"><strong>Status</strong></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="caption">Invoice</span><a href="#" class="table-icon-link"><span>3534534</span></a>
                                </td>
                                <td>
                                    <span class="caption">Invoice Date</span>01/13/2015
                                </td>
                                <td>
                                    <span class="caption">Ship To</span>My Name
                                </td>
                                <td>
                                    <span class="caption">Amount</span>$450.60
                                </td>
                                <td>
                                    <span class="caption">Status</span><span class="label label-success">Paid</span>
                                </td>
                                <td>
                                    <span class="caption">&nbsp;</span><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Invoice</span><a href="#" class="table-icon-link"><span>3534534</span></a>
                                </td>
                                <td>
                                    <span class="caption">Invoice Date</span>01/13/2015
                                </td>
                                <td>
                                    <span class="caption">Ship To</span>My Name
                                </td>
                                <td>
                                    <span class="caption">Amount</span>$450.60
                                </td>
                                <td>
                                    <span class="caption">Status</span><span class="label label-danger">Unpaid</span>
                                </td>
                                <td>
                                    <span class="caption">&nbsp;</span><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Invoice</span><a href="#" class="table-icon-link"><span>3534534</span></a>
                                </td>
                                <td>
                                    <span class="caption">Invoice Date</span>01/13/2015
                                </td>
                                <td>
                                    <span class="caption">Ship To</span>My Name
                                </td>
                                <td>
                                    <span class="caption">Amount</span>$450.60
                                </td>
                                <td>
                                    <span class="caption">Status</span><a href="#" class="table-icon-link"><span>Get Status</span></a>
                                </td>
                                <td>
                                    <span class="caption">&nbsp;</span><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12 text-center">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <li class="prev">
                                <a href="#" aria-label="Previous">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>…</span></li>
                            <li class="next">
                                <a href="#" aria-label="Next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
