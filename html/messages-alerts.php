<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="inde.php">Home</a></li>
                        <li class="active">Messages, Alerts</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Messages, Alerts</h1>
                </div>
                <div class="col-xs-12">
                    <h2>#1A — Page level error</h2>
                    <article>
                        <p>Display at the top of the page when a field level error message occurs. </p>
                    </article>
                    <div class="alert alert-danger alert-alt">
                        <span>Global page level error message here.</span>
                    </div>
                    <form action="" class="form">
                        <div class="form-group">
                            <span class="help-block text-danger">Enter a valid email address</span>
                            <div class="has-error">
                                <input type="text" class="form-control" placeholder="Email Address (will be User's login ID)">
                            </div>
                        </div>
                    </form>
                    <h2>#1C — Field level error</h2>
                    <article>
                        <p>It's not so simple as it looks, so I decided to use bootstrap <a href="http://getbootstrap.com/javascript/#popovers">popover plugin</a>.</p>
                    </article>
                    <p>First example, list of buttons</p>
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="btn btn-warning btn-lg warn-popover" role="button" data-class="warn" data-placement="top" data-trigger="click" data-content="Please select at least one item before adding to cart">Popover at the top</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-warning btn-lg warn-popover" role="button" data-class="warn" data-placement="top" data-trigger="click" data-content="Please select at least one item before adding to cart">Popover at the top</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-warning btn-lg warn-popover" role="button" data-class="warn" data-placement="top" data-trigger="click" data-content="Please select at least one item before adding to cart">Popover at the top</a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-warning btn-lg warn-popover" role="button" data-class="warn" data-placement="bottom" data-trigger="click" data-content="Please select at least one item before adding to cart">Popover at the bottom</a>
                        </li>
                    </ul>
                    <hr>
                    <ul class="list-inline">
                        <li>
                            <a class="btn btn-default" data-toggle="modal" data-target="#messageModal" href="#">#2A — Message Modal</a>
                        </li>
                        <li>
                            <a class="btn btn-default" data-toggle="modal" data-target="#confirmSelectionModal" href="#">#2B — Confirm Selection Modal</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
