<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-sm-8">
            <ol class="breadcrumb">
                <li><a href="#">Cart</a></li>
                <li class="active">Confirmation</li>
            </ol>
        </div>
        <div class="col-sm-4 page-actions hidden-print">
            <ul class="list-inline">
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-envelope-o"></i><span>Email Page</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-print"></i><span>Print Page</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12 hidden-print">
            <div class="carts-steps">
                <a href="#" class="step">
                    <span class="step-number">1.</span>
                    <span class="step-label">Cart</span>
                </a>
                <a href="#" class="step">
                    <span class="step-number">2.</span>
                    <span class="step-label">Review</span>
                </a>
                <a href="#" class="step is-active">
                    <span class="step-number">3.</span>
                    <span class="step-label">Confirmation</span>
                </a>
            </div>
        </div>
        <div class="cart-page">
            <div class="col-xs-12">
                <h1>Thank You</h1>
                <!-- Titles don't need a dot -->
                <p>
                    Your order has been placed. An Order Acknowledgment with shipping, tax, and other potential
                    charges will be emailed to you once your order has been completed. Or, you may go to the Order
                    History page in this website and select the "Acknowledgment PDF" from the list. Please note it
                    can take up to 15 minutes for an acknowledgement to generate.</p>
            </div>
            <div class="col-xs-12">
                <div class="well well-gray">
                    <h4>Web Order Confirmation Number: 012345</h4>
                    <table>
                        <tr>
                            <td>
                                <strong>Ordered On</strong>
                                <p>01/2/2015</p>
                            </td>
                            <td>
                                <strong>PO#</strong>
                                <p>0012312323123231232312323123231232312323123231232312323123</p>
                            </td>
                            <td>
                                <strong># of Items</strong>
                                <p>5</p>
                            </td>
                            <td>
                                <strong>Ship to Address</strong>
                                <p>
                                    NW Hospital - 17th Floor, Imaging <br>
                                    Address Line 2 <br>
                                    Address Line 3
                                </p>
                            </td>
                            <td>
                                <strong>Total Amount</strong>
                                <p>02,735.00</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group hidden-print">
                    <a href="#" class="btn btn-warning btn-lg">View Order Detail</a>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>