<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li class="active">Manage Users</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Manage Users</h1>
                </div>
                <div class="manage-users">
                    <div class="col-xs-12">
                        <div class="well well-gray">
                            <ul class="list-inline manage-users-actions">
                                <li>
                                    <a href="#" class="btn btn-warning btn-lg">Add User</a>
                                </li>
                                <li>
                                    <a href="#importUsers" data-toggle="modal" class="btn btn-warning btn-lg">Import Users</a>
                                </li>
                                <li>
                                    <a href="#exportUsers" data-toggle="modal" class="btn btn-default btn-lg">Export Users</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="well well-gray">
                            <h4>Default Spending Limit and Frequency for All Users</h4>
                            <div class="row">
                                <div class="col-lg-12 col-sm-8 col-xs-12">
                                    <p>You can set a default spending limit and frequency for <strong>ALL USERS</strong>. You can also choose to set an individual User's spending limit and frequency via the Edit User page. </p>
                                </div>
                            </div>
                            <form class="row">
                                <div class="col-lg-12">
                                    <div class="radio checked radio-toggle-sefault-spending">
                                        <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                            <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="nospending"> No spending limit for all Users
                                        </label>
                                    </div>
                                    <div class="radio radio-toggle-sefault-spending">
                                        <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                            <input class="sr-only" name="radioEx1" type="radio" value="defaultspending">Default spending limit for all Users
                                        </label>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row default-spending-form" style="display: none;">
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <input type="text" class="form-control input-lg" placeholder="$">
                                            <div class="radio">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel20">
                                                    <input class="sr-only" name="radioEx2" type="radio" value="perorder"> Per Order
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel21">
                                                    <input class="sr-only" name="radioEx2" type="radio" value="perday"> Per Day
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel22">
                                                    <input class="sr-only" name="radioEx2" type="radio" value="perweek"> Per Week
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel23">
                                                    <input class="sr-only" name="radioEx2" type="radio" value="permonth"> Per Month
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    <br>
                                        <div class="col-md-3 col-sm-4 col-xs-12">
                                            <button class="btn btn-default btn-block btn-lg">Save</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="well well-gray">
                            <form class="row">
                                <div class="col-md-12">
                                    <h4>Search Users</h4>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-8">
                                            <div class="row smaller">
                                                <div class="col-sm-7 col-xs-6 col-xxs-12">
                                                    <input type="text" class="form-control input-lg" placeholder="Name or Email">
                                                    <br>
                                                </div>
                                                <div class="col-sm-5 col-xs-6 col-xxs-12 mb-sm">
                                                    <select name="" id="" class="bootstrap-select form-control input-lg">
                                                        <option value="">Role</option>
                                                        <option value="">Role 2</option>
                                                    </select>
                                                </div>
                                                <!-- <div class="col-sm-4 col-xs-6 col-xxs-12 mb-sm">
                                                    <input type="text" class="form-control input-lg" placeholder="Company Name">
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="row smaller">
                                                <div class="col-sm-6 col-xs-6">
                                                    <a href="#" class="btn btn-lg btn-warning btn-block btn-invoices-search">Search</a>
                                                </div>
                                                <div class="col-sm-6 col-xs-6">
                                                    <a href="#" class="btn btn-lg btn-default btn-block btn-invoices-search">Clear</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <table class="table table-striped table-mobile">
                            <thead>
                                <tr>
                                    <th><strong>First Name</strong> <i class="fa fa-sort"></i></th>
                                    <th><strong>Last Name</strong> <i class="fa fa-sort"></i></th>
                                    <th><strong>Email Address</strong></th>
                                    <th><strong>Role</strong> <i class="fa fa-sort"></i></th>
                                    <th><strong>Company Name</strong> <i class="fa fa-sort"></i></th>
                                    <th><strong>Spend Limit</strong> <i class="fa fa-sort"></i></th>
                                    <th colspan="2"><strong>Status</strong> <i class="fa fa-sort"></i></th>
                                </tr>
                            </thead>
                            <tbody class="">
                                <tr>
                                    <td>
                                         <span class="caption">First Name</span>John
                                    </td>
                                    <td>
                                        <span class="caption">Last Name</span>Smith
                                    </td>
                                    <td>
                                        <span class="caption">Email Address</span><a href="#" class="table-icon-link"><span>johnsmith@mail.com</span></a>
                                    </td>
                                    <td>
                                        <span class="caption">Role</span>Buyer
                                    </td>
                                    <td>
                                        <span class="caption">Company Name</span>ACME Corporation
                                    </td>
                                    <td>
                                        <span class="caption">Spending Limit</span>$2000
                                    </td>
                                    <td>
                                        <span class="caption">Status</span>Active
                                    </td>
                                    <td>
                                        <a href="#" class="table-icon-link"><span>Edit User</span></a>
                                    </td>
                                </tr>
                                <?php for ($i=0; $i < 10; $i++) {
                                    echo "
                                    <tr>
                                    <td>
                                        <span class='caption'>First Name</span>John
                                    </td>
                                    <td>
                                        <span class='caption'>Last Name</span>Smith
                                    </td>
                                    <td>
                                        <span class='caption'>Email Address</span><a href='#' class='table-icon-link'><span>johnsmith@mail.com</span></a>
                                    </td>
                                    <td>
                                        <span class='caption'>Role</span>Buyer
                                    </td>
                                    <td>
                                        <span class='caption'>Company Name</span>ACME Corporation
                                    </td>
                                    <td>
                                        <span class='caption'>Spending Limit</span>$2000
                                    </td>
                                    <td>
                                        <span class='caption'>Status</span>Active
                                    </td>
                                    <td>
                                        <a href='#' class='table-icon-link'><span>Edit User</span></a>
                                    </td>
                                </tr>
                                    ";
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12 text-center">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <li class="prev">
                                <a href="#" aria-label="Previous">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>…</span></li>
                            <li class="next">
                                <a href="#" aria-label="Next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
