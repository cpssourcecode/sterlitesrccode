<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
    <!-- header end -->
    
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                  <li class="active">My Account Overview</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>My Account Overview</h1>
            </div>
            <div class="col-xs-12">
                <div class="my-account-links">
                    <div class="item">
                        <h4>Manage Orders</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Order History</a></li>
                            <li><a class="nowrap" href="#">Order Requests <span class="text-danger">(52222)</span></a></li>
                            <li><a href="#">Packing Slips</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <h4>Billing & Payment</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">View Invoices</a></li>
                            <li><a href="#">View Statements</a></li>
                        </ul>
                    </div>
                    <div class="item hide">
                        <h4>Admin Tools</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Manage Users</a></li>
                            <!-- <li><a href="#">Company Settings</a></li> -->
                        </ul>
                    </div>
                    <div class="item">
                        <h4>Lists</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">My Material Lists</a></li>
                            <li><a href="#">Saved Carts <span class="text-danger">(5)</span></a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <h4>My Profile</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">View Addresses</a></li>
                            <li><a href="#">Edit Profile</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div>
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#rinvoices" aria-controls="rinvoices" role="tab" data-toggle="tab">Recent Invoices</a></li>
                    <li role="presentation"><a href="#rorders" aria-controls="rorders" role="tab" data-toggle="tab">Recent Orders</a></li>
                  </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="rinvoices">
                        <table class="table table-striped table-mobile">
                          <thead>
                            <tr>
                              <th><strong>Invoice #</strong></th>
                              <th><strong>Date</strong> <i class="fa fa-sort"></i></th>
                              <th><strong>Ship To</strong></th>
                              <th><strong>Amount</strong></th>
                              <th colspan="2"><strong>Status</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><span class="caption">Invoice # </span>343432</td>
                              <td><span class="caption">Date </span>01/13/2015</td>
                              <td><span class="caption">Ship To</span>Name</td>
                              <td><span class="caption">Amount </span>$498.45</td>
                              <td><span class="caption">Status </span><span class="label label-success">Received</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                            <tr>
                              <td><span class="caption">Invoice # </span>343432</td>
                              <td><span class="caption">Date </span>01/13/2015</td>
                              <td><span class="caption">Ship To</span>Name</td>
                              <td><span class="caption">Amount </span>$498.45</td>
                              <td><span class="caption">Status </span><span class="label label-gray">Processing</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                            <tr>
                              <td><span class="caption">Invoice # </span>343432</td>
                              <td><span class="caption">Date </span>01/13/2015</td>
                              <td><span class="caption">Ship To</span>Name</td>
                              <td><span class="caption">Amount </span>$498.45</td>
                              <td><span class="caption">Status </span><span class="label label-warning">Shipped</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                            <tr>
                              <td><span class="caption">Invoice # </span>343432</td>
                              <td><span class="caption">Date </span>01/13/2015</td>
                              <td><span class="caption">Ship To</span>Name</td>
                              <td><span class="caption">Amount </span>$498.45</td>
                              <td><span class="caption">Status </span><span class="label label-dark">Delivered</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                            <tr>
                              <td><span class="caption">Invoice # </span>343432</td>
                              <td><span class="caption">Date </span>01/13/2015</td>
                              <td><span class="caption">Ship To</span>Name</td>
                              <td><span class="caption">Amount </span>$498.45</td>
                              <td><span class="caption">Status </span><span class="label label-primary">Invoiced</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                          </tbody>
                        </table>
                        <a href="#" class="btn btn-warning btn-lg">Search Invoices</a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="rorders">
                        <table class="table table-striped table-mobile">
                          <thead>
                            <tr>
                              <th><strong>Order Number</strong></th>
                              <th><strong>Date Placed</strong> <i class="fa fa-sort"></i></th>
                              <th><strong>Total</strong></th>
                              <th><strong>Ship To</strong></th>
                              <th><strong>Status</strong>  <i class="fa fa-sort"></i></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><span class="caption">Order Number</span>243432</td>
                              <td><span class="caption">Date Placed</span>01/13/2015</td>
                              <td><span class="caption">Total</span>$498.45</td>
                              <td><span class="caption">Ship To</span>John Smith</td>
                              <td><span class="caption">Status</span><span class="label label-danger">Pending Approval</span></td>
                            </tr>
                            <tr>
                              <td><span class="caption">Order Number</span>243432</td>
                              <td><span class="caption">Date Placed</span>01/13/2015</td>
                              <td><span class="caption">Total</span>$498.45</td>
                              <td><span class="caption">Ship To</span>John Smith</td>
                              <td><span class="caption">Status</span>Shipped</td>
                            </tr>
                          </tbody>
                        </table>
                        
                        <a href="#" class="btn btn-warning btn-lg">Search Orders</a>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>

<?php include 'includes/modals.php'; ?>