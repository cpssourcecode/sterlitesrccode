<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>FAQs - CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
    <!-- header end -->
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Video Library</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>Video Library</h1>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-3 col-xs-12">
                        <div class="sidebar-menu">
                            <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                            </button>
                            <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                                <li><a href="#">Product Information</a></li>
                                <li><a href="#">FAQs</a></li>
                                <li class="active"><a href="#">Video Library</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <article>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, maiores. Officiis cum in perferendis, ipsam, quae laborum magnam quidem commodi temporibus quasi facere recusandae velit libero, distinctio deleniti aspernatur odio. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae consequatur dolorum eligendi fugit quas natus dolor nostrum perspiciatis fugiat cumque rerum, eum, minima hic incidunt reiciendis, modi sed, eveniet rem.
                                    </p>
                                    <h3>Aquatherm</h3>
                                </article>
                            </div>
                        </div>
                        <div class="row equalize-category">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-xxs-12">
                                <div class="thumbnail case-study">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="http://www.youtube.com/embed/6wL1RD_bS3M?autoplay=1" data-toggle="lightbox">
                                                <div class="image video-thumb">
                                                    <img src="http://img.youtube.com/vi/6wL1RD_bS3M/mqdefault.jpg" alt="">
                                                    <div class="video-overlay">
                                                        <div class="image">
                                                            <img src="assets/images/icons/video-play-thumb.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="hidden-xs">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p class="hidden-xs"><strong>6:41 min</strong></p>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 visible-xs">
                                            <a href="http://www.youtube.com/embed/L9szn1QQfas?autoplay=1" data-toggle="lightbox">
                                                <h4 class="root-mobile-title">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p><strong>6:41 min</strong></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta harum et exercitationem at quas sequi delectus voluptatem molestiae voluptate. Aliquam ab provident laborum nisi numquam itaque omnis delectus, corporis nemo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-xxs-12">
                                <div class="thumbnail case-study">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="http://www.youtube.com/embed/6wL1RD_bS3M?autoplay=1" data-toggle="lightbox">
                                                <div class="image video-thumb">
                                                    <img src="http://img.youtube.com/vi/6wL1RD_bS3M/mqdefault.jpg" alt="">
                                                    <div class="video-overlay">
                                                        <div class="image">
                                                            <img src="assets/images/icons/video-play-thumb.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="hidden-xs">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p class="hidden-xs"><strong>6:41 min</strong></p>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 visible-xs">
                                            <a href="http://www.youtube.com/embed/L9szn1QQfas?autoplay=1" data-toggle="lightbox">
                                                <h4 class="root-mobile-title">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p><strong>6:41 min</strong></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta harum et exercitationem.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-xxs-12">
                                <div class="thumbnail case-study">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="http://www.youtube.com/embed/6wL1RD_bS3M?autoplay=1" data-toggle="lightbox">
                                                <div class="image video-thumb">
                                                    <img src="http://img.youtube.com/vi/6wL1RD_bS3M/mqdefault.jpg" alt="">
                                                    <div class="video-overlay">
                                                        <div class="image">
                                                            <img src="assets/images/icons/video-play-thumb.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="hidden-xs">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p class="hidden-xs"><strong>6:41 min</strong></p>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 visible-xs">
                                            <a href="http://www.youtube.com/embed/L9szn1QQfas?autoplay=1" data-toggle="lightbox">
                                                <h4 class="root-mobile-title">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p><strong>6:41 min</strong></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta harum et exercitationem at quas sequi delectus voluptatem molestiae voluptate. Aliquam ab provident laborum nisi numquam itaque omnis delectus, corporis nemo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3>Dixon Sanitary</h3>
                        <div class="row equalize-category">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-xxs-12">
                                <div class="thumbnail case-study">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                            <a href="http://www.youtube.com/embed/6wL1RD_bS3M?autoplay=1" class="various fancybox.iframe">
                                                <div class="image video-thumb">
                                                    <img src="http://img.youtube.com/vi/6wL1RD_bS3M/mqdefault.jpg" alt="">
                                                    <div class="video-overlay">
                                                        <div class="image">
                                                            <img src="assets/images/icons/video-play-thumb.svg" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <h4 class="hidden-xs">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p class="hidden-xs"><strong>6:41 min</strong></p>
                                        </div>
                                        <div class="col-sm-12 col-xs-12 visible-xs">
                                            <a href="http://www.youtube.com/embed/L9szn1QQfas?autoplay=1" class="various fancybox.iframe">
                                                <h4 class="root-mobile-title">Polypropylene Heat Fusion Demo — Aquatherm</h4>
                                            </a>
                                            <p><strong>6:41 min</strong></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 text-block">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta harum et exercitationem at quas sequi delectus voluptatem molestiae voluptate. Aliquam ab provident laborum nisi numquam itaque omnis delectus, corporis nemo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center hidden-print">
                            <div class="pagination">
                                <nav>
                                    <ul class="pagination pagination-lg">
                                        <li class="prev">
                                            <a href="#" aria-label="Previous">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                            </a>
                                        </li>
                                        <li class="active"><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><span>…</span></li>
                                        <li class="next">
                                            <a href="#" aria-label="Next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <?php include 'includes/footer.php'; ?>
    <?php include 'includes/scripts.php'; ?>
</body>

</html>
