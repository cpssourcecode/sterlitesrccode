function isElementExist(selector) {
    return !!$(selector).size();
}

$(document).ready(function() {
    $('.dropdown-hover').dropdownHover();
    // $('.scrollbar-inner').scrollbar();
    $('.scrollbar-inner').scrollbar({
        ignoreMobile: false,
        ignoreOverlay: true
    });
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
});

$('.warn-popover').on('shown.bs.popover', function() {
    $('.popover.warn').children('button.close').on('click', function(event) {
        event.preventDefault();
        $('.warn-popover').popover('hide');
    });
})


$('input, textarea').placeholder({ customClass: 'my-placeholder' });


if (isElementExist('.phone')) {
    $('.phone').inputmask({
            mask: '(999) 999-9999'
        })
        // $(".phone").mask("(999) 999-9999");
        //$("#alternate-anchor").mask("(999) 999-9999"); 
}

$("#forgotPassword").click(function() {
    $("#logIn").modal('hide');
});

$('#logIn').on('shown.bs.modal', function() {
    $('.autofocus-input').focus();
    console.log("focus on input field on modal launch"); //UI improvements
})

$(function() {
    $('[data-toggle="popover"]').popover();

    $('.warn-popover').popover({
        template: '<div class="popover warn" role="tooltip"><div class=""></div><h3 class="popover-title"></h3><div class="popover-content"></div><button class=close><span class="glyphicon glyphicon-remove"></span></button></div>'

    });

});



$(".error-anchor").click(function(event) {
    var anchorValue = $(this).attr("href");
    $("input" + anchorValue).focus();
    $("input" + anchorValue).addClass('animated').addClass('pulse');
    setTimeout(function() {
        $("input" + anchorValue).removeClass('animated').removeClass('pulse');
    }, 1500);
    // console.log(anchorValue);
    event.preventDefault(event);
});

$(document).ready(function() {
    $('.owl-home').removeClass('hide');
    $('.owl-home').owlCarousel({
        items: 1,
        itemsCustom: false,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [980, 1],
        itemsTablet: [768, 1],
        itemsTabletSmall: false,
        itemsMobile: [479, 1],
        singleItem: false,
        itemsScaleUp: false,
        loop: true,
        // animateOut: 'fadeOut',
        // animateIn: 'fadeIn',
        autoplay: true,
        autoplayTimeout: 7000,
        dots: false
    });

});

$(document).ready(function() {
    $(".owl-thumbnails").owlCarousel({
        items: 3,
        itemsCustom: false,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [980, 3],
        itemsTablet: [768, 3],
        itemsTabletSmall: false,
        itemsMobile: [480, 1],
        singleItem: false,
        itemsScaleUp: false,
        // autoHeight : true,
        // Navigation
        navigation: true,
        navigationText: ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
        rewindNav: false,
        scrollPerPage: false,
        //Pagination
        pagination: true,
        paginationNumbers: true,
        //Autoplay
        autoplay: true,
        autoplayTimeout: 5000,
    });

    $(".owl-full-width").owlCarousel({
        items: 4,
        itemsCustom: false,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [980, 3],
        itemsTablet: [768, 3],
        itemsTabletSmall: false,
        itemsMobile: [480, 1],
        singleItem: false,
        itemsScaleUp: false,
        // Navigation
        navigation: true,
        navigationText: ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"],
        rewindNav: false,
        scrollPerPage: false,
        //Pagination
        pagination: true,
        paginationNumbers: true,
        //Autoplay
        autoplay: true,
        autoplayTimeout: 5000,
    });

    $(".owl-testimonials").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true
    });
});

$(window).on('load resize', function() {

    var windowWidth = $(window).width();
    var testimonialsForm = '<div class="sidebar-block"><h4>Submit a Testimonial</h4><div class="sidebar-form"><form action=""><div class="form-group"><input type="text" name class="form-control" placeholder="*First Name" required/></div><div class="form-group"><input type="text" class="form-control" placeholder="*Last Name" required/></div><div class="form-group"><input type="text" class="form-control" placeholder="*Company" required/></div><div class="form-group"><textarea class="form-control" name="testimonialsMsg" id="testimonialsMsg" cols="6" rows="5" maxlength="250" placeholder="*Testimonial Message (Max 250 Characters)" required></textarea></div><button type="submit" class="btn btn-default btn-block btn-lg"> Submit</button></form></div></div>';
    // if ($('.testimonial-container-desktop').html()) {
    //     testimonialsForm = $('.testimonial-container-desktop').html();
    // } else {
    //     testimonialsForm = $('.testimonials-container-mobile').html();
    // };

    if (windowWidth <= 768) {
        $('.testimonial-container-desktop').html('');
        $('.testimonial-container-mobile').html('');
        $('.testimonial-container-mobile').append(testimonialsForm);
    } else {
        $('.mainmenu-trigger').children('.dropdown-menu').children('.nav-tabs').children('li').removeClass('active');
        $('.mainmenu-trigger').children('.dropdown-menu').children('.nav-tabs').children('li:first-child').addClass('active');
        $('.mainmenu-trigger').children('.dropdown-menu').children('.tab-content').children('.tab-pane').removeClass('active');
        $('.mainmenu-trigger').children('.dropdown-menu').children('.tab-content').children('#products').addClass('in active');
        $('.testimonial-container-mobile').html('');
        $('.testimonial-container-desktop').html('');
        $('.testimonial-container-desktop').append(testimonialsForm);;
    };
    // fassets fix
    if (windowWidth < 992 && windowWidth > 480) {
        // setTimeout(function() {}, 500);
        $('.fassets-selection-container').children('.accordion-fassets')
            .css('margin-top', -($('.fassets-selection-container').children('.selected-fassets').outerHeight() - 15));
    } else {
        $('.fassets-selection-container').children('.accordion-fassets')
            .css('');
    };

    var row = $('.equalize');
    $.each(row, function() {
        var maxh = 0;
        $.each($(this).find('div[class^="col-"] .thumbnail'), function() {
            if ($(this).height() > maxh) {
                maxh = $(this).height();
            }
        });
        $.each($(this).find('div[class^="col-"] .thumbnail'), function() {
            $(this).height(maxh);
        });
    });

    var equalRowCategory = $('.equalize-category');
    $.each(equalRowCategory, function() {
        var maxhEqCat = 0;
        $.each($(this).find('div[class^="col-"] .thumbnail'), function() {
            $(this).removeAttr("style");
            if ($(this).height() > maxhEqCat) {
                maxhEqCat = $(this).height();
            }
        });
        $.each($(this).find('div[class^="col-"] .thumbnail'), function() {
            $(this).height(maxhEqCat);
        });
    });

    var equalRowCarousel = $('.owl-thumbnails');
    $.each(equalRowCarousel, function() {
        var maxhEThumb = 0;
        $.each($(this).find('.owl-wrapper-outer .owl-wrapper .owl-item .item .thumbnail'), function() {
            if ($(this).height() > maxhEThumb) {
                maxhEThumb = $(this).height();
            }
        });
        $.each($(this).find('.owl-wrapper-outer .owl-wrapper .owl-item .item .thumbnail'), function() {
            if ($(window).width() <= 768) {
                $(this).css('height', 'auto');
            } else {
                $(this).height(maxhEThumb);
            };

        });
    });

    var equalRowCarouselFull = $('.owl-full-width');
    $.each(equalRowCarouselFull, function() {
        var maxhEThumb = 0;
        $.each($(this).find('.owl-wrapper-outer .owl-wrapper .owl-item .item .thumbnail'), function() {
            if ($(this).height() > maxhEThumb) {
                maxhEThumb = $(this).height();
            }
        });
        $.each($(this).find('.owl-wrapper-outer .owl-wrapper .owl-item .item .thumbnail'), function() {
            if ($(window).width() <= 768) {
                $(this).css('height', 'auto');
            } else {
                $(this).height(maxhEThumb);
            };

        });
    });

    $('#viewOrder').on('shown.bs.modal', function () {
        calcEqualWebOrderModal();
    });
    calcEqualWebOrderModal();
    function calcEqualWebOrderModal() {
        var equalizeModalWebOrder = $('.equalizeWebOrder');
        $.each(equalizeModalWebOrder, function () {
            var maxhEThumb = 0;
            $.each($(this).find('div .equalItem'), function () {
                if ($(this).height() > maxhEThumb) {
                    maxhEThumb = $(this).height();
                }
            });
            $.each($(this).find('div .equalItem'), function () {
                if ($(window).width() <= 768) {
                    $(this).css('height', 'auto');
                } else {
                    $(this).height(maxhEThumb);
                }
                ;

            });
        });
    }



    function mobileMainmenuTrigger() {
        $(".mainmenu-trigger").children('.dropdown-menu').css({
            width: windowWidth,
            display: ''
        });
        $('.mainmenu-trigger').children('.dropdown-toggle').removeAttr('data-toggle', 'dropdown');
        $(".dropdown-hover-trigger").removeClass('dropdown-hover');
        $('.mainmenu-trigger').children('.dropdown-toggle').off('click');
        $('.mainmenu-trigger').children('.dropdown-toggle').click(function(event) {
            $('.mainmenu-trigger').toggleClass('open');
        });
        $(".dropdown-hover-trigger").removeClass('dropdown-hover');
    }

    if ($('.mainmenu-trigger').hasClass('home')) {
        console.log("home");
        // window.matchMedia('(max-width: 768px)').matches
        // windowWidth <= 768
        if (window.matchMedia('(max-width: 768px)').matches) {
            mobileMainmenuTrigger();
        } else {
            $(".mainmenu-trigger").children('.dropdown-menu').css({
                width: '100%',
                display: 'block'
            });
            $(".dropdown-hover-trigger").addClass('dropdown-hover');
        };
    } else {
        console.log("inner");
        if (window.matchMedia('(max-width: 768px)').matches) {
            mobileMainmenuTrigger();
        } else {
            $('.dropdown-toggle').dropdown();
            $('.mainmenu-trigger').children('.dropdown-menu').css('width', '100%');
            $('.mainmenu-trigger').children('.dropdown-toggle').attr('data-toggle', 'dropdown');
            $(".dropdown-hover-trigger").addClass('dropdown-hover');
        };
    };

    if (window.matchMedia('(max-width: 768px)').matches) {
        $('.sidebar-collapsed')
            .addClass('collapse')
            .removeAttr('style');
        // $(".mainmenu-trigger").children('.dropdown-menu').css({
        // width: windowWidth,
        // display: ''
        // });
        // $(".mainmenu-dropdown-home").parent('.dropdown-menu').removeAttr('style');
        
        // $('.well-view-order-detail-summary').css('height', 'auto');
        // $('.mainmenu-trigger').children('.dropdown-toggle').click(function(event) {
        // event.preventDefault();
        // $(this).parent('.dropdown').toggleClass('open');
        // if ($(this).parent('.dropdown').hasClass('open')) {
        //   $(this).parent('.dropdown').removeClass('open');
        // } else {
        //   $(this).parent('.dropdown').addClass('open');
        // };
        // });
    } else {
        $('.sidebar-collapsed')
            .removeClass('collapse')
            .css('height', 'auto');
        // $(".mainmenu-dropdown-home").parent('.dropdown-menu').css('display', 'block');
        $(".dropdown-hover-trigger").addClass('dropdown-hover');
        // $('.well-view-order-detail-summary').css('height', $('.well-view-order-detail').outerHeight());
    };

    if (window.matchMedia('(max-width: 480px)').matches) {
        $('.equalize').find('div[class^="col-"] .thumbnail').css('height', 'auto');
    };

    if (windowWidth <= 480) {
        $('.dropdown-table-actions').width(windowWidth - 50);
        // console.log($('.dropdown-table-actions').offset().left);
    } else {
        $('.dropdown-table-actions').removeAttr('style');
    };
});



// $(".close-this-modal").click(function() {
//     console.log("test");
// });

$('#select-item-material-check input').on('change', function() {
    if ($(this).checkbox('isChecked')) {
        // console.log("cheked");
        $('.content-list').find('.content').removeClass('selected');
        // $('#select-item-material-check input').checkbox('uncheck');
        $(this).closest('.content').addClass('selected');
        $(this).checkbox('check');

    } else {

    };
});

$('#select-item-material-check-all input').on('change', function() {
    if ($(this).checkbox('isChecked')) {
        console.log("cheked-all");
        // $('.content-list').find('.content').removeClass('selected');
        $('#select-item-material-check input').checkbox('check');
        // $(this).closest('.content').addClass('selected');
        // $(this).checkbox('check');

    } else {
        $('#select-item-material-check input').checkbox('uncheck');
    };
});

$('.collapsed-testimonial').children('.testimonial-text').click(function(event) {
    event.preventDefault();
    // var thisTestimonialHeight = $(this).height();
    $(this).toggleClass('open');
});

$('.scrollbar-scrollable input').on('change', function() {
    if ($(this).checkbox('isChecked')) {
        console.log("cheked");
        $('.scrollbar-inner').find('.radio').removeClass('checked');
        $('.scrollbar-scrollable input').checkbox('uncheck');
        $(this).closest('.scrollbar-scrollable').addClass('checked');
        $(this).checkbox('check');

    } else {};
});


// $(document).ready(function() {

//     $(".fancybox").fancybox({
//         caption: {
//             type: 'outside'
//         },
//         openEffect: 'elastic',
//         closeEffect: 'elastic',
//         nextEffect: 'elastic',
//         prevEffect: 'elastic'
//     });

//     $(".various").fancybox({
//         maxWidth: 800,
//         maxHeight: 600,
//         fitToView: false,
//         width: '70%',
//         height: '70%',
//         padding: 0,
//         autoSize: false,
//         closeClick: false,
//         openEffect: 'elastic',
//         closeEffect: 'elastic',
//         nextEffect: 'elastic',
//         prevEffect: 'elastic'
//     });

// });

// $( ".typeahead-main" ).keypress(function() {
//   $(this).parent().addClass('open');
// });

$(".header-ship li label").click(function() {
    $(".login-box-select .btn .text").text($(this).children('.addr-title').text());
    $(".header-ship .current p .addr-title").text($(this).children('.addr-title').text());
    $(".header-ship .current p .addr1").text($(this).children('.addr1').text());
    $(".header-ship .current p .addr2").text($(this).children('.addr2').text());
    // addrTemp = $(this).children('.addr-title').text();
    // console.log(addrTemp);
});

$(".header-ship .close").click(function() {
    $(this).parent().parent().removeClass('open');
});

$(".radio-toggle-sefault-spending").click(function() {
    if ($(this).children('label').children('input').val() == 'defaultspending') {
        $(".default-spending-form").show('200');
    } else {
        $(".default-spending-form").hide('200');
    };
});

$(".radio-toggle-delivery-method").click(function() {
    if ($(this).children('label').children('input').val() == 'delivery-method-pick-up') {
        $(".delivery-method-pick-up-form").show();
        $(".delivery-method-shipped-form").hide();
    }
    if ($(this).children('label').children('input').val() == 'delivery-method-shipped') {
        $(".delivery-method-shipped-form").show();
        $(".delivery-method-pick-up-form").hide();
    }
});

$('.collapse-description').on('click', '.more', function(event) {
  event.preventDefault();
  var parentCover = $(this).parent('.collapse-description'), newHeight = 0;

  parentCover.children().each(function(){newHeight += $(this).height();});

  parentCover.toggleClass('full');

  if ( parentCover.hasClass('full') ) {
      parentCover.css('height', newHeight);
      $(this).children('span').text('Less');
  } else {
      parentCover.css('height', '');
      $(this).children('span').text('More');
  }
  $(this).blur();
});

$(document).ready(function() {
    $('.show-title').on('click', function() {
        open($('.title-picker'));
    });
    $('.title-picker').on('change', function() {
        var title = $(this).find('option:selected').attr('title');
        $('.show-title').text(title);
    });

    checkAvailability();
});

// function open(elem) {
//     if (document.createEvent) {
//         var e = document.createEvent("MouseEvents");
//         e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
//         elem[0].dispatchEvent(e);
//     } else if (element.fireEvent) {
//         elem[0].fireEvent("onmousedown");
//     }
// }


/* Custom File Input START */

$(function() {
    var file_api = (window.File && window.FileReader && window.FileList && window.Blob) ? true : false;

    $('.custom-file-upload').each(function(i, o) {
        var wrapper = $(o),
            inp = wrapper.find('input'),
            btn = wrapper.find('.btn'),
            lbl = wrapper.find('mark');

        // Crutches for :focus style
        inp.focus(function() {
            wrapper.addClass('focus');
        }).blur(function() {
            wrapper.removeClass('focus');
        });

        inp.change(function() {
            var file_name;
            if (file_api && inp[0].files[0])
                file_name = inp[0].files[0].name;
            else
                file_name = inp.val().replace("C:\\fakepath\\", '');

            if (!file_name.length)
                return;

            if (lbl.is(":visible")) {
                lbl.text(file_name);
            }
        }).change();
    });
});
$(window).resize(function() {
    $(".custom-file-upload input").triggerHandler("change");
});

/* Custom File Input END*/

//
function checkAvailability() {
    var btn = $('.button-check-availability button');
    $(btn).on('click', function(){
        if ($('.check-availability').children('.radio').children('label').children('input#collapseHaveItShipped').prop('checked')) {
            $('.collapse-check-availability').children('#collapseHaveItShipped').collapse('show');
            $('.collapse-check-availability').children('#collapsePickItUp').collapse('hide');
        } else {
            $('.collapse-check-availability').children('#collapseHaveItShipped').collapse('hide');
            $('.collapse-check-availability').children('#collapsePickItUp').collapse('show');
        };
    });
    // $('.check-availability .radio').on('change', function() {

    //     var id = $(this).attr('data-collapse');

    //     //console.log('click radio');
    //     //console.log('id='+id);
    //     btn.attr('data-target', id);
    //     //console.log(btn.data());
    // });
    // btn.on('click', function() {
    //     var activeCollapse = btn.attr('data-target');
    //     $(this).closest('.modal').animate({
    //         scrollTop: $(window).scrollTop() + 400
    //     });
    //     $('.modal-check-collapse').each(function() {
    //         if ($(this).hasClass('in') && !($(this).is(activeCollapse))) {
    //             $(this).removeClass('in');
    //         }
    //     });
    // });

}


// Apply PO# (from input to text)
$(document).ready(function() {


    function applyPO() {
        $('#applyPO').find('.form-control').on('keyup', function(event) {
            event.preventDefault();
            var cleredValue = $(this).val().replace(/\s/g, "");
            $(this).val(cleredValue);
        });
    }


    $('.well-password-hint-inline').removeClass('highlight');

    $('.bootstrap-select').selectpicker({
        style: 'form-control',
        size: 13
    });

    $('.create-new-list-btn').on('click', function() {
        $(this).hide();
        $(this).parents('.modal').find('.modal-footer').hide();
        $('.create-new-list-collapse').collapse('toggle');
        if ($('.create-new-list-collapse').hasClass('in')) {
            $(this).parents('.modal').find('.modal-footer').show();
        };
    });

    $('.cancel-create-new-list-btn').on('click', function() {
        $('.create-new-list-btn').show();
        $(this).parents('.modal').find('.modal-footer').show();
        $('.create-new-list-collapse').collapse('toggle');
        if ($('.create-new-list-collapse').hasClass('in')) {
            $(this).parents('.modal').find('.modal-footer').show();
        }
    });


    //switch content in Driving Directions Modal
    var $drivingDirections = $('#drivingDirections');
    $drivingDirections.find('.mapIt').click(function(e) {
        e.preventDefault();
        $drivingDirections.find(".setDirections").addClass('hidden');
        $drivingDirections.find(".showDirections").removeClass('hidden');
    });
    $drivingDirections.find('.toAddress').click(function(e) {
        e.preventDefault();
        $drivingDirections.find(".showDirections").addClass('hidden');
        $drivingDirections.find(".setDirections").removeClass('hidden');
    });



});

applyPO();

function applyPO() {
    var trigger = true;
    $('#applyPO').find('button').on('click', function() {
        if (trigger) {
            if ($('#applyPO').find('input')[0].value != '') {
                $('#applyPO').find('button').text('Edit');
                $('#applyPO').find('input').replaceWith('<span class="po">' + $('#applyPO').find('input')[0].value + '</span>');
                trigger = false;
            }
        } else {
            $('#applyPO').find('button').text('Apply');
            $('#applyPO').find('span.po').replaceWith('<input type="text" class="form-control input-lg" placeholder="PO#" maxlength="30" value="' + $('#applyPO').find('span.po').text() + '">');
            trigger = true;
        }

    });
}


function clearQuickOrder() {
    var products = document.getElementsByName("quickproductCart");
    var quantities = document.getElementsByName("quickqtyCart");
    for (i = 0; i < products.length; i++) {
        products[i].value = "";
    }
    for (i = 0; i < quantities.length; i++) {
        quantities[i].value = "";
    }
}

function duplicateAddToCart() {
    var productchoices = document.getElementsByName('dupeproduct');
    //var quantities = [];
    var quantitychoices = document.getElementsByName('dupeqty');
    var pclength = productchoices.length;
    var itemIds = "";
    var qtyStr = "";
    var count = 0;
    var checkedcount = 0;
    for (var i = 0; i < pclength; i++) {
        if (productchoices[i].checked) {
            checkedcount++;
        }
    }
    for (var i = 0; i < pclength; i++) {
        if (productchoices[i].checked) {
            var elementid = productchoices[i].value; //product id
            var qty = quantitychoices[i].value; // qty for product
            itemIds = itemIds.concat(elementid);
            qtyStr = qtyStr.concat(qty);
            if (++count < checkedcount) {
                itemIds = itemIds.concat(",");
                qtyStr = qtyStr.concat(",");
            }
        }

    }
    $("#dupeSkuIdsList").val(itemIds);
    $("#dupeQtyList").val(qtyStr);
    $("#quickDupeAddToCart").submit();
}

function quickOrderAddToCart() {
    //var errors = document.getElementsByClassName("message message-red");
    //for(i=0, iarr=0; i<errors.length; i++){
    //  att = errors[i].getAttribute("name");
    //  if(att != null){
    //                  att.hide();
    //                  }
    //  }
    var products = document.getElementsByName("quickproduct");
    var quantities = document.getElementsByName("quickqty");
    var itemIds = "";
    var qtyStr = "";
    var transactionStr = "";
    var count = 0;
    for (i = 0; i < products.length; i++) {
        var elementid = products[i].value; //product id
        var qty = quantities[i].value; // qty for product
        //if ($("#"+elementid).is(":checked")) {
        if (!!elementid && elementid != "Item # or Aias") {
            //----
            //($("#"+elementid).is(":checked").each(function(index,el){
            itemIds = itemIds.concat(elementid);
            qtyStr = qtyStr.concat(qty);

            //if( $("#pdpaccqty"+elementid+tabName).val() > 0 ) {
            //  qtyStr = qtyStr.concat($("#pdpaccqty"+elementid+tabName).val());
            //} else {
            //  qtyStr = qtyStr.concat("1");
            //}

            if (++count < products.length) {
                itemIds = itemIds.concat(",");
                qtyStr = qtyStr.concat(",");
            }
        }

    }
    //window.alert(itemIds); window.alert(qtyStr); window.alert(transactionStr);
    $("#addSkuIdsList").val(itemIds);
    $("#addQtyList").val(qtyStr);
    $("#quickAddToCart").submit();
}



//Input value cleaner
$(document).ready(

    function() {

        $(".input-clear").on('click', function(event) {
            event.preventDefault();
            $(this).prev('input').val('');
        });

        var $orderPadItems = $('.order-pad-item');
        $('.add-order-items').on('click', function() {
            var newItems = $orderPadItems.clone(true);
            newItems.find('input').val('');
            newItems.appendTo('.order-pad-items');
            // var newItems2 = $orderPadItems.clone(true);
            // window.alert(newItems2.find('input').val(''));
            return false;
        });


        //Add to Cart Success Message
        $('.call-alert').on('click', function() {
            $.notify({
                message: '<span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Item(s) Added to Cart'
            }, {
                type: 'cps',
                delay: 2000,
                mouse_over: null,
                offset: 0,
                placement: {
                    from: "top",
                    align: "center"
                }
            });
        });


        //Edit Profile Collapse
        $('.profile-button').on('click', function() {
            $(this).hide();
            $(this).prev('.info').slideUp();
            // console.log($(this).parent('.info-block').parent('.row').find('.collapse'));
            // if ($(this).parent('.info-block').parent('.row').find('.collapse').hasClass('in')) {

            //     $(this).parent('.info-block').css('min-height', 'auto');
            // } else {
            //     $(this).parent('.info-block').css('min-height', '250px');

            // };
        });
        $('.profile-edit-button').on('click', function() {
            var $profileBlock = $(this).parents('.collapse').prev();
            $profileBlock.find('.profile-button').show();
            $profileBlock.find('.info').slideDown();
        });

        // Star Rating
        $('.star-rating').each(function() {
            var $that = $(this);
            $(this).on('click', 'label', function(e) {
                $(this).siblings('label').removeClass('active');
                $(this).addClass('active');
            });
        });

        // Highlight password hint on profile edit page
        $('.form-new-pass-focusable').focus(function() {
            $('.well-password-hint-inline')
                .addClass('highlight');
        });

        $('.form-new-pass-focusable').blur(function() {
            $('.well-password-hint-inline')
                .removeClass('highlight');
        });

        // Move main carousl text to the right on the open mainmenu
        // $('.mainmenu-dropdown').prev('a').click(function() {
        //     console.log($(this));
        //     var thisMenuWidth = $('.mainmenu-dropdown').width();
        //     setTimeout(function() {
        //         if ($('.mainmenu-dropdown').prev('a').parent('li').hasClass('open')) {
        //             $('.lead-block').children('.container').css('padding-left', thisMenuWidth + 40);
        //         } else {
        //             $('.lead-block').children('.container').css('padding-left', '15px');
        //         };
        //     }, 500);
        // });

        $('.mainmenu-dropdown').parent('.mainmenu-trigger').on('show.bs.dropdown', function() {
            var thisMenuWidth = $('.mainmenu-dropdown').width();
            $('.lead-block').children('.container').css('padding-left', thisMenuWidth + 40);
            setTimeout(function() {}, 500);
        })
        $('.mainmenu-dropdown').parent('.mainmenu-trigger').on('hide.bs.dropdown', function() {
            var thisMenuWidth = $('.mainmenu-dropdown').width();
            $('.lead-block').children('.container').css('padding-left', '15px');
        })


        /*
            $('.dropdown-typeahead li a').click(function() {
                $('.typeahead-main').val($(this).html());
                $('.typeahead-main').val(function (i, old) {
                     return old
                         .replace('<b>', '')
                         .replace('</b>', '')
                         .replace('&nbsp;', '')
                         .replace('  ', '');
                });
            });

            function clearSearhBar(i, old) {
                // var inputValue = $(".typeahead-main").val();
                // inputValue.replace('<b>', '');    
                // console.log(inputValue);
                return old
                     // $(this)
                         .replace('<b>', '')
                         .replace('</b>', '')
                         .replace('  ', '');
            }
        */

    });

function addedToCartHangerShow() {
    $.notify({
        message: '<span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> Item(s) Added to Cart'
    }, {
        type: 'cps',
        delay: 2000,
        mouse_over: null,
        offset: 0,
        placement: {
            from: "top",
            align: "center"
        }
    });
}


/* Custom File Input START */

$(function() {
    var file_api = (window.File && window.FileReader && window.FileList && window.Blob) ? true : false;

    $('.custom-file-upload').each(function(i, o) {
        var wrapper = $(o),
            inp = wrapper.find('input'),
            btn = wrapper.find('.btn'),
            lbl = wrapper.find('mark');

        // Crutches for :focus style
        inp.focus(function() {
            wrapper.addClass('focus');
        }).blur(function() {
            wrapper.removeClass('focus');
        });

        inp.change(function() {
            var file_name;
            if (file_api && inp[0].files[0])
                file_name = inp[0].files[0].name;
            else
                file_name = inp.val().replace("C:\\fakepath\\", '');

            if (!file_name.length)
                return;

            if (lbl.is(":visible")) {
                lbl.text(file_name);
            }
        }).change();
    });
});

$(window).resize(function() {
    $(".custom-file-upload input").triggerHandler("change");

    // alert("resize triggered");
});

function changeHomeHeight(){
    $(".main-content").removeAttr("style");
    var menuHeight = $("#products").innerHeight();
    var bannerHeight = $(".owl-home").innerHeight();
    var mainContentHeight = $(".main-content").innerHeight();
    if(menuHeight > bannerHeight + mainContentHeight){
        var newMainHeight = menuHeight - bannerHeight;
        $(".main-content").attr("style", "height: " + newMainHeight + "px" );
    }
}

$(document).ready(function() {
    $(window).on('load resize', function() {
        changeHomeHeight();
    });
    $('#carousel-example-generic').on('slid.bs.carousel', function (e) {
        changeHomeHeight();
    });

    $(window).on("orientationchange",function(){
        if ($('.user-actions-box').find('.dropdown').hasClass('open')) {
            $('.user-actions-box').find('.dropdown').removeClass('open')
        };
        console.log("orientation changed");
        // alert("orientation changed");
    });

    $('#collapsePickItUp').on('shown.bs.collapse', function () {
        $('#myModalCheck').modal('handleUpdate');
        // alert("collapse");
    });

    $('#collapseHaveItShipped').on('shown.bs.collapse', function () {
        $('#myModalCheck').modal('handleUpdate');
        // alert("collapse");
    });

    // $(".modal").on( "scrollstart", function( event ) {
        // console.log("scroll start");
    // } );


    if (isElementExist('.search-list')) {
        $.each($('.search-list'), function(index, val) {
            $('.media .media-left a', val).each(function() {
                // console.log($(this).children('img').width() +'×'+ $(this).children('img').height());
                if($(this).children('img').height() > $(this).children('img').width()) {
                    $(this).children('img').css('height', '100%');
                    $(this).children('img').css('width', 'auto');
                }
            });
        });
    };

    if (isElementExist('.content')) {
    $.each($('.content'), function(index, val) {
        $('.check .checkbox .checkbox-custom .checkbox-label img', val).each(function() {
            // $(this).on('load', function() {
                console.log($(this).height());
                console.log($(this).width());
                if ($(this).height() > $(this).width()) {
                    var thisHeight = ( $(this).height() / ($(this).parent('.checkbox-label').width() / $(this).width()) );
                    $(this).css('width', 'auto');
                    $(this).css('height', thisHeight);
                    $(this).css('height', 'auto');
                    // $(this).css('max-height', 'auto');
                }
            // });
        });
    });
}

    
    
});
/* Custom File Input END*/
