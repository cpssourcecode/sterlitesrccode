<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Price Lists Page - CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<?php include 'includes/header-logged.php'; ?>
<!-- header end -->
<div class="container main-content">
    <div class="row">
        <div class="col-sm-8">
            <ol class="breadcrumb">
                <li class="active">Price Lists </li>
            </ol>
        </div>
        <div class="col-sm-4 page-actions">
            <ul class="list-inline">
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-envelope-o"></i><span>Email Page</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="action">
                        <i class="fa fa-print"></i><span>Print Page</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-xs-12">
            <h1>Price Lists</h1>
        </div>
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="sidebar-menu">
                        <button class="btn btn-default collapse-sidebar-btn" type="button" data-toggle="collapse" data-target="#collapseSidebar" aria-expanded="false" aria-controls="collapseSidebar">
                                  <glyphicon class="glyphicon glyphicon-menu-hamburger"></glyphicon>
                                </button>
                        <ul class="sidebar-menu-container sidebar-collapsed collapse" id="collapseSidebar">
                            <li><a href="#">Our Business</a></li>
                            <li><a href="#">Locations</a></li>
                            <li><a href="#">News and Events</a></li>
                            <li><a href="#">Case Studies</a></li>
                            <li><a href="#">Testimonials</a></li>
                            <li><a href="#">Affiliations</a></li>
                            <li><a href="#">Regional Managers</a></li>
                            <li class="active"><a href="#">Price Lists</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-xs-12">
                    <div class="well well-gray">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 mb-sm">
                                <select name="recentPriceListUpd" id="recentPriceListUpd" class="form-control bootstrap-select" onchange="getUrl();">
                                    <option value="" disabled selected data-hidden="true">Recent Price List Updates</option>
                                    <option value="http://google.com">2015 September 1</option>
                                    <option value="2">2015 August 15</option>
                                    <option value="3">2015 August 1</option>
                                    <option value="4">2015 June 15</option>
                                    <option value="5">2015 June 1</option>
                                    <option value="6">2015 May 15</option>
                                    <option value="7">2015 May 1</option>
                                    <option value="8">2015 April 15</option>
                                    <option value="9">2015 April 1</option>
                                    <option value="10">2015 March 1</option>
                                    <option value="11">2015 February 15</option>
                                </select>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <select name="priceBook" id="priceBook" class="form-control bootstrap-select">
                                    <option value="" disabled selected data-hidden="true">Price Book by Section</option>
                                    <option value="1">Section 1</option>
                                    <option value="2">Section 2</option>
                                    <option value="3">Section 3</option>
                                    <option value="4">Section 4</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <article>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. Aliquam erat volutpat. Donec placerat nisl magna, et faucibus arcu condimentum sed.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at porttitor sem. 
                        </p>
                    </article>
                    <h4>Get Automatic Price List Alerts</h4>
                    <p>Sign up for Columbia Pipe Price List e-mail updates. We will send an automatic email notification
                       everytime we have an update to our price list. We will not share your email with anyone.</p>

                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <form action="">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your name"/>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email"/>
                                </div>
                                <button class="btn btn-warning btn-lg">Sumbit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- main container end -->
<?php include 'includes/footer.php'; ?>
<?php include 'includes/scripts.php'; ?>
</body>
<script>
    function getUrl() {
        var url = $('#recentPriceListUpd').val();
        console.log(url);
        window.open(url, '_blank');
    }
</script>
</html>