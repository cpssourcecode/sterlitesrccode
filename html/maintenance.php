<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                
                <div class="col-xs-12">
                    <h1>We'll be back soon.</h1>
                </div>
                <div class="col-xs-12">
                    <h2>Our apologies. The Columbia Pipe website is <strong>temporary</strong> unavailable due to <strong>scheduled maintenace</strong>.</h2>
                    <p>We will be performing maintenace on columbiapipe.com on [Day], [Date] between [time frame] Central time. During the time, columbiapipe.com will be unavailable. We hope this won't be too much of an inconvenience as we work to perform necessary upgrades as quickly as possible.</p>
                    <p><strong>Be sure to check back soon.</strong> In the meantime, here are some resources you may be interested in.</p>
                    <ul>
                        <li>&#8226; Item 1</li>
                        <li>&#8226; Item 2</li>
                        <li>&#8226; Item 3</li>
                        <li>&#8226; Item 4</li>
                        <li>&#8226; Item 5</li>
                    </ul>
                    <p>Sincerely, <br>Your Columbia Pipe Online Team</p>
                </div>

            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
