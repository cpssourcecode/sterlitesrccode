<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>EMAILS</title>
</head>
<body>

<!-- Added New User Confirmation (Welcome Msg) -->
<div id="welcomeMsg" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse; font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name >.</p>
                                <br>
                                <p>Welcome to Columbia Pipe & Supply Co.
                                   Your user id is: "Users Email Address"</p>

                                <p>A Temporary password would be sent to you very shortly...</p>

                                <p>Once you have set your password, We suggest you keep your password confidential and
                                   select a password that is easy to remember. For security purpose, Columbia Pipe does
                                   not have access to your password.. <br>
                                   Therefore we are unable to give passwords over telephone or remind you of your
                                   password. If you need assistance, Please contact our customer service at
                                   1-000-000-0000 Monday to Friday 8 am to 8 pm CST .</p>
                                <br>
                                <p>We look forward to seeing you online soon.</p>
                                <p><i>Sincerely, <br>
                                   Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Temporary Password -->
<div id="temporaryPassword" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse; font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name >.</p>
                                <br>
                                <h3>Temporary Password</h3>

                                <p>Your have been assigned a temporary password : xxxxxxxx . Yoiu must reset your password within 72 hours.
                                   Please click on the link below to set you New Password. <br>
                                    <a href="http://www.columbiapipe.com/xxxxxxxxx">www.columbiapipe.com/xxxxxxxxx</a></p>

                                <p>Note If you are unable to clikck on the above link, Please copy and paste the link in your web browser.<br>
                                   if you need assistance, please contact customer service at 1-XXX-XXX-XXXX Monday to Friday 8 am to 6 pm CST.</p>
                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Password reset Link Sent -->
<div id="passwordResetLink" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse; font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name >.</p>
                                <br>
                                <h3>Reset Your Password</h3>

                                <p>To reset your password, click this link. <br>
                                    <a href="https://columbiapipe.com/">https://columbiapipe.com/…</a></p>

                                <p>Please note:<br>
                                   For security purposes, this link will expire 72 hours from the time it was sent.</p>

                                <p>If you cannot access this link, copy and paste the entire URL into your browser.</p>
                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Password Changed Confirmation -->
<div id="passwordResetConform" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name >.</p>
                                <br>
                                <h3>Password Changed Confirmation </h3>

                                <p>You have successfully updated your Columbia Pipe password.</p>

                                <p>If you received this message in error, please contact Columbia Pipe immediately at:
                                   1-XXX-XXX-XXXX, Monday-Friday 8am-9pm CST.</p>
                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Admin Added New User -->
<div id="adminAddedNewUser" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello Account Admin.</p>
                                <br>
                                <h3>New Member Added</h3>

                                <p>New User < First Name > < Last Name > < Email id > has been added to your Account Group.</p>

                                <p>If you received this message in error, please contact Columbia Pipe immediately at: 1-XXX-XXX-XXXX, Monday-Friday 8am-9pm CST.</p>
                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Order Approval Needed -->
<div id="orderApproval" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello </p>
                                <br>
                                <h3>Order Approval Needed</h3>

                                <p>User < First Name > < Last Name > has submitted an Columbiapipe.com order with web confirmation #__________for your approval.</p>
                                <p>To view and approve this order to Columbia Pipe for processing, Click here http://www.abc123.com/</p>

                                <p>Thank you, </p>
                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Order Confirmation -->
<div id="orderConfirm" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name ></p>
                                <br>
                                <p>Welcome to Columbia Pipe & Supply Co. <br>
                                   Your user id is: "Users Email Address" <br>
                                   To begin using the website, Please click set up my password (Note: setup my password
                                   is in blue ink and underlined)</p>

                                <p>Once you have set your password, We suggest you keep your password confidential and
                                   select a password that is easy to remember. For security purpose, Columbia Pipe
                                   does not have access to your password.. <br>
                                   Therefore we are unable to give passwords over telephone or remind you of your password.
                                   If you need assistance, Please contact our customer service at 1-000-000-0000 Monday to Friday 8 am to 8 pm CST .</p>

                                <br>
                                <p>We look forward to seeing you online soon</p>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- User Profile Updated -->
<div id="userProfileUpdated" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name ></p>
                                <br>
                                <h3>Your Profile has been updated</h3>

                                <p>Your user profile has been updated. If you need to verify or make further updates you can visit
                                    <a href="#">My Account</a> section on our website.</p>
                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Share Page  -->
<div id="sharePage" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name ></p>
                                <br>
                                <p>You have been shared a Web Page from Columbia Pipe <br>
                                   Click http;//columbipipe.com/ <br>
                                   You can also copy past the above link in your web browser.</p>

                                <br>
                                <p>We look forward to seeing you there.</p>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Shared - Material List -->
<div id="sharedMaterialList" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name ></p>
                                <br>

                                <p><b>A Columbia Pipe Material List has been Shared with you</b></p>

                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Items left in Cart -->
<div id="itemsleftInCart" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name ></p>
                                <br>
                                <h3>Items Pending in Cart for Check Out.</h3>

                                <p>Thank you for visiting Columbia. We noticecd that you still have items left in your cart.
                                   Please visit your shopping <a href="#">CART</a> to checkout and complete your order.</p>

                                <br>
                                <p>We look forward to seeing you online soon</p>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Order Approved -->
<div id="orderApproved" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name ></p>
                                <br>
                                <h3>Web Order ________ has been approved</h3>

                                <p>Your web order is approved and has been sent to CPS for processing.</p>

                                <br>
                                <p>Thank you for your business.<br>
                                <i>Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- OOrder Rejected -->
<div id="orderRejected" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello < First_Name > < Last_Name ></p>
                                <br>
                                <h3>Web Order ________ has been rejected</h3>

                                <p>Your web order has been rejected for the following reason</p>
                                <ul>
                                    <li>Over Spending Liimit</li>
                                </ul>
                                <p>Comments: <br>

                                </p>

                                <br>
                                <p>Thank you for your business.<br>
                                    <i>Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- CPS Admin/CPS Super User de-activates Customer User/Admin -->
<div id="adminDe-activatesUser" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello Admin,</p>
                                <br>
                                <h3>Columbia Piple has De-activated User < First Name >  < Last Name >.</h3>

                                <p>We regret to inform you that this User has been de-activated. If you need more information about this de-activation , Pleas contact Columbia Pipe
                                   at: 1-XXX-XXX-XXXX, Monday-Friday 8am-9pm CST.</p>


                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->

<!-- Reject Order Request Email  -->
<div id="rejectOrderRequest" bgcolor="#FAFAFA" lang="EN-US" link="blue" vlink="purple">
    <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background: #fafafa; border-collapse: collapse;
    font-family: Verdana, Geneva, Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.34em;">
        <tr>
            <td width="100%" valign="top">
                <div align="center" style="padding: 10px 0 20px;">
                    <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="740" style="border-collapse:collapse">
                        <tr bgcolor="#333333">
                            <td style="width: 50%; padding: 10px;"><img src="http://s7.postimg.org/86dxda7sr/email_logo.png" alt=""/></td>
                            <td style="width: 50%; text-align: right; padding: 10px; color: white; font-weight: bold;">Questions or Need Help? 773-927-6600</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px 30px 20px;">
                                <p>Hello Admin,</p>
                                <br>
                                <h3>Your Web Order #___________ has been rejected</h3>

                                <p>Approver < First Name > < Last Name > has rejected your Web Order.</p>
                                <p>Reason for Rejection entered by Approver: <i>Display any message enter by User in the "Reject Order Request" Modal.</i></p>

                                <br>
                                <p><i>Sincerely, <br>
                                      Your Columbia Pipe Online Team.</i></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
<!-- END-->


</body>
</html>