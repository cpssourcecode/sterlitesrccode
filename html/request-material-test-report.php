<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="/"><span>Home</span></a></li>
                        <li class="active"><span>Request Material Test Report</span></li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Request Material Test Report</h1>
                </div>
                <div class="col-xs-12">
                    <p>To retrieve a Material Test Report (MTR) on one or more items, enter up to six heat numbers below. <strong>NOTE</strong>: If you're having trouble finding a MTR, please <a href="contact-us.php">contact your Sales Team</a>.</p>
                </div>
                <div class="col-xs-12">
                    <form class="order-pad-table well well-gray nopadding">
                        <div class="order-pad-items row">
                            <div class="order-pad-item form-group col-md-4 col-sm-6 col-xs-12">
                                <div class="col-xs-12">
                                    <input type="text" class="form-control input-lg state-danger" placeholder="Item # or Aias" value="0123-45678 NOT FOUND">
                                    <button class="input-clear">
                                        <!-- <svg class="icon icon-crpss form-control-feedback">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-crpss"></use>
                                        </svg> -->
                                    </button>
                                </div>
                            </div>
                            <?php 
                                    for ($i=0; $i < 5; $i++) { 
                                           echo "<div class='order-pad-item form-group col-md-4 col-sm-6 col-xs-12'>
                                    <div class='col-xs-12'>
                                        <input type='text' class='form-control input-lg' placeholder='Heat #'>
                                        <button class='input-clear'>
                                            
                                        </button>
                                    </div>
                                </div>";
                                        }    
                                 ?>
                        </div>
                        <br>
                        <ul class="list-inline">
                            <li>
                                <a class="btn btn-warning btn-lg">Submit</a>
                            </li>
                            <li>
                                <a href="#clearOrderPad" data-toggle="modal" class="btn btn-default btn-lg">Clear</a>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="col-xs-12">
                    <div class="form-group">
                        <div class="scrollable scrollable-alt">
                            <div class="head">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="checkbox" id="myCheckbox">
                                            <label class="checkbox-custom" data-initialize="checkbox">
                                                <input class="sr-only" type="checkbox" value="">
                                                <span class="checkbox-label">Select All</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="content scrollbar-inner">
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="request-material">
                                            <div class="checkbox" id="myCheckbox">
                                                <label class="checkbox-custom" data-initialize="checkbox">
                                                    <input class="sr-only" type="checkbox" value="">
                                                    <span class="checkbox-label"></span>
                                                </label>
                                            </div>
                                            <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Heat #</span></a>
                                        </div>
                                    </div>
                                    <?php for ($i=0; $i < 20; $i++) { 
                                        echo "
                                            <div class='item'>
                                                <div class='request-material'>
                                                    <div class='checkbox' id='myCheckbox'>
                                                        <label class='checkbox-custom' data-initialize='checkbox'>
                                                            <input class='sr-only' type='checkbox' value=''>
                                                            <span class='checkbox-label'></span>
                                                        </label>
                                                    </div>
                                                    <a href='#' class='table-icon-link'><i class='fa fa-file-text-o'></i><span>Heat #</span></a>
                                                </div>
                                            </div>
                                        ";
                                    } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                <a href="#clearOrderPad" data-toggle="modal" class="btn btn-lg btn-default">Delete Selected</a>
                </div>
                <div class="col-xs-12 text-center">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <li class="prev">
                                <a href="#" aria-label="Previous">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>…</span></li>
                            <li class="next">
                                <a href="#" aria-label="Next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
