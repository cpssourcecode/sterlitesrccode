<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">My Account</a></li>
                        <li class="active">Order History</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Order History</h1>
                </div>
                <div class="col-xs-12">
                    <div class="well well-gray well-invoices">
                        <h4>Search Orders</h4>
                        <form class="row">
                            <div class="col-md-12">
                                <ul class="description">
                                    <li>Searching by Web Confirmation #, Sales Order #, or PO # will return ALL Order History records (no date limit). </li>
                                    <li>Searching by Ship to Address will return Order History records up to 120 days old. </li>
                                    <li>Searching by Date Range will return records up to  30, 60, 90, or 120 days old.</li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row smaller">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="row smaller">
                                                    <div class="col-md-7 col-sm-3 col-xs-6">
                                                        <div class="radio">
                                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                                <input class="sr-only" name="radioEx1" type="radio" value="option1"> Web Confirmation #
                                                            </label>
                                                        </div>
                                                        <div class="radio checked">
                                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                                                <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="option2"> Sales Order #
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel3">
                                                                <input class="sr-only" name="radioEx1" type="radio" value="option2"> PO #
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-3 col-xs-6">
                                                        <input type="text" class="form-control mb-xxs input-lg" placeholder="#">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="row smaller">
                                                    <div class="col-md-5 col-sm-5 col-xs-5 col-xxs-12">
                                                        <select name="" id="" class="form-control mb-xxs input-lg bootstrap-select">
                                                            <option value="">Ship to</option>
                                                            <option value="">NW Hosp — 17th Floor, Imaging (Current)</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-3 col-xxs-12">
                                                        <select name="" id="" class="form-control mb-xxs input-lg bootstrap-select">
                                                            <option value="">Date Range</option>
                                                            <option value="">30 days</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 col-xxs-12">
                                                        <div class="row smaller">
                                                            <div class="col-sm-6 col-xs-6 col-xxs-6">
                                                                <a href="#" class="btn btn-lg btn-warning btn-block btn-invoices-search">Search</a>
                                                            </div>
                                                            <div class="col-sm-6 col-xs-6 col-xxs-6">
                                                                <a href="#" class="btn btn-lg btn-default btn-block btn-invoices-search">Reset</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-xs-12">
                    <table class="table table-striped table-mobile">
                        <thead>
                            <tr>
                                <th><strong>Order Date</strong> <i class="fa fa-sort"></i></th>
                                <th><strong>Order</strong> <i class="fa fa-sort"></i></th>
                                <th><strong>PO#</strong> <i class="fa fa-sort"></i></th>
                                <th colspan="3"><strong>Amount</strong> <i class="fa fa-sort"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="caption">Order Date</span>01/13/2015
                                </td>
                                <td>
                                    <span class="caption">Order</span><a href="#" class="table-icon-link"><span>3534534</span></a>
                                </td>
                                <td>
                                    <span class="caption">PO #</span>547567
                                </td>
                                <td>
                                    <span class="caption">Amount</span>$450.30
                                </td>
                                <td class="dropdown">
                                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>View Details</span></a>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#">Test Dropdown</a></li>
                                    </ul>
                                </td>
                                <td>
                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Acknowledgement PDF</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Order Date</span>01/13/2015
                                </td>
                                <td>
                                    <span class="caption">Order</span><a href="#" class="table-icon-link"><span>3534534</span></a>
                                </td>
                                <td>
                                    <span class="caption">PO #</span>547567
                                </td>
                                <td>
                                    <span class="caption">Amount</span>$450.30
                                </td>
                                <td class="dropdown">
                                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>View Details</span></a>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#">Test Dropdown</a></li>
                                    </ul>
                                </td>
                                <td>
                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Acknowledgement PDF</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Order Date</span>01/13/2015
                                </td>
                                <td>
                                    <span class="caption">Order</span><a href="#" class="table-icon-link"><span>3534534</span></a>
                                </td>
                                <td>
                                    <span class="caption">PO #</span>547567
                                </td>
                                <td>
                                    <span class="caption">Amount</span>$450.30
                                </td>
                                <td class="dropdown">
                                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>View Details</span></a>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#">Test Dropdown</a></li>
                                    </ul>
                                </td>
                                <td>
                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Acknowledgement PDF</span></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="caption">Order Date</span>01/13/2015
                                </td>
                                <td>
                                    <span class="caption">Order</span><a href="#" class="table-icon-link"><span>3534534</span></a>
                                </td>
                                <td>
                                    <span class="caption">PO #</span>547567
                                </td>
                                <td>
                                    <span class="caption">Amount</span>$450.30
                                </td>
                                <td class="dropdown">
                                    <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="table-icon-link"><span>View Details</span></a>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#">Test Dropdown</a></li>
                                    </ul>
                                </td>
                                <td>
                                    <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>Acknowledgement PDF</span></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12 text-center">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <li class="prev">
                                <a href="#" aria-label="Previous">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                            </li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>…</span></li>
                            <li class="next">
                                <a href="#" aria-label="Next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
