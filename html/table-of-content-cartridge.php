<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Table of Content Cartridge</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Table of Content Cartridges Page</h1>
                </div>
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="sidebar-menu">
                                <ul class="sidebar-menu-container">
                                    <li class="active"><a href="#">Aside Link Active</a></li>
                                    <li><a href="#">Aside Link</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                    <h3>Nav Pills</h3>
                                    <ul class="nav nav-pills nav-stacked">
                                        <li><a href="#">Link</a></li>
                                        <li><a href="#">Link</a></li>
                                        <li><a href="#">Link</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                    <h3>Unstyled List</h3>
                                    <ul class="list-unstyled">
                                        <li><a href="#">Link</a></li>
                                        <li><a href="#">Link</a></li>
                                        <li><a href="#">Link</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="">
                                        <h2>Rich Text Cartridge (link)</h2>
                                    </a>
                                    <article>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, eum. Provident animi ipsum quae quidem aspernatur repudiandae ex est, repellendus doloremque iusto omnis asperiores ipsam deserunt numquam tenetur excepturi maiores. Lorem ipsum dolor sit amet, consectetur adipisicing elit. At voluptates similique dicta culpa rem perferendis ex tempora minima, incidunt exercitationem beatae iste, accusantium quia suscipit rerum ea, facere, ipsa excepturi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, placeat. Itaque deserunt, quibusdam fuga quidem facere dolore iure consectetur sint. Aspernatur quo, iste aliquam praesentium, accusantium nulla qui ducimus magnam.</p>
                                    </article>
                                    <article>
                                        <p><img align="left" style="padding: 5px;" src="assets/images/after-sales-service.png" alt="">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam, eum. Provident animi ipsum quae quidem aspernatur repudiandae ex est, repellendus doloremque iusto omnis asperiores ipsam deserunt numquam tenetur excepturi maiores. Lorem ipsum dolor sit amet, consectetur adipisicing elit. At voluptates similique dicta culpa rem perferendis ex tempora minima, incidunt exercitationem beatae iste, accusantium quia suscipit rerum ea, facere, ipsa excepturi. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic, placeat. Itaque deserunt, quibusdam fuga quidem facere dolore iure consectetur sint. Aspernatur quo, iste aliquam praesentium, accusantium nulla qui ducimus magnam. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus accusantium fugiat at doloribus porro eveniet, enim inventore, itaque consequuntur est culpa quaerat mollitia veritatis temporibus commodi nesciunt cum perferendis reiciendis.</p>
                                    </article>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2>PDF Cartridge</h2>
                                </div>
                                <div class="col-lg-4 col-md-3 col-sm-4 col-xs-6">
                                    <a href="#" class="thumbnail">
                                      <img src="assets/images/PVFSummer15_Cover_Hi-Res-Resized.jpg" alt="...">
                                      <span>PDF File Download With Thumbmail</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2>Promo Banner Cartridge</h2>
                                </div>
                                <div class="col-lg-12">
                                    <div class="media">
                                      <div class="media-left">
                                        <a href="#">
                                          <img class="media-object" style="width: 180px;" src="assets/images/after-sales-service.png" alt="...">
                                        </a>
                                      </div>
                                      <div class="media-body">
                                        <h4 class="media-heading"><a href="#">Promo Banner Cartridge Title</a></h4>
                                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea doloribus, quos delectus odio accusamus nostrum adipisci! Adipisci at molestias odit, suscipit, consequuntur eos quo dolorem minus facilis eligendi fugiat iure.</span>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
