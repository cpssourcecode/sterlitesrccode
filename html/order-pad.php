<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <link href="assets/stylesheets/common.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <?php include 'includes/header-logged.php'; ?>
        <!-- header end -->
        <div class="container main-content">
            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb">
                        <li class=""><a href="#">Home</a></li>
                        <li class="active">Order Pad</li>
                    </ol>
                </div>
                <div class="col-sm-4 page-actions">
                    <ul class="list-inline">
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-envelope-o"></i><span>Email Page</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="action">
                                <i class="fa fa-print"></i><span>Print Page</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-12">
                    <h1>Order Pad</h1>
                </div>
                <div class="col-xs-12">
                    <div class="well well-gray">
                        <h4>Import/Upload List</h4>
                        <form class="row">
                            <div class="col-md-5 col-sm-9 col-xs-12 col-xxs-12">
                                <div class="form-group">
                                    <!--input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1"-->
                                    <label class="custom-file-upload">
                                        <span class="btn btn-lg btn-default">Choose File</span>
                                        <mark>No file chosen (1MB Max)</mark>
                                        <input type="file" multiple data-overwrite-initial="false" data-min-file-count="1">
                                    </label>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 col-xxs-12">
                                <a href="#itemsAddtoCart" data-toggle="modal" class="btn btn-lg btn-warning">Add To Cart</a>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 col-xxs-12">
                                <span class="modal-footer-noreload"><span class="text-muted glyphicon glyphicon-download-alt"></span> <a href="#" class="gray-noreload"><span>Download Spreadsheet Template</span></a></span>
                            </div>
                        </form>
                    </div>

                    <form class="order-pad-table well well-gray">
                        <div class="order-pad-items row">
                            <div class="order-pad-item form-group col-md-4 col-sm-4 col-xs-4">
                                <div class="col-xs-8">
                                    <input type="text" class="form-control input-lg" placeholder="Item # or Aias">
                                    <!-- <svg class="input-clear icon icon-crpss form-control-feedback"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-crpss"></use></svg> -->
                                    <button class='input-clear'>
                                    </button>
                                </div>
                                <div class="col-xs-4"><input type="text" min="0" class="form-control input-lg" placeholder="Qty"></div>
                            </div>
                            <div class="order-pad-item form-group col-md-4 col-sm-4 col-xs-4">
                                <div class="col-xs-8">
                                    <input type="text" class="form-control input-lg" placeholder="Item # or Aias">
                                    <button class='input-clear'>
                                    </button>
                                </div>
                                <div class="col-xs-4"><input type="text" min="0" class="form-control input-lg" placeholder="Qty"></div>
                            </div>

                            <div class="order-pad-item form-group col-md-4 col-sm-4 col-xs-4">
                                <div class="col-xs-8">
                                    <input type="text" class="form-control input-lg" placeholder="Item # or Aias">
                                    <button class='input-clear'>
                                    </button>
                                </div>
                                <div class="col-xs-4"><input type="text" min="0" class="form-control input-lg" placeholder="Qty"></div>
                            </div>
                            <div class="order-pad-item form-group col-md-4 col-sm-4 col-xs-4">
                                <div class="col-xs-8">
                                    <input type="text" class="form-control input-lg" placeholder="Item # or Aias">
                                    <button class='input-clear'>
                                    </button>
                                </div>
                                <div class="col-xs-4"><input type="text" min="0" class="form-control input-lg" placeholder="Qty"></div>
                            </div>
                            <div class="order-pad-item form-group col-md-4 col-sm-4 col-xs-4">
                                <div class="col-xs-8">
                                    <input type="text" class="form-control input-lg" placeholder="Item # or Aias">
                                    <button class='input-clear'>
                                    </button>
                                </div>
                                <div class="col-xs-4"><input type="text" min="0" class="form-control input-lg" placeholder="Qty"></div>
                            </div>
                            <div class="order-pad-item form-group col-md-4 col-sm-4 col-xs-4">
                                <div class="col-xs-8">
                                    <input type="text" class="form-control input-lg" placeholder="Item # or Aias">
                                    <button class='input-clear'>
                                    </button>
                                </div>
                                <div class="col-xs-4"><input type="text" min="0" class="form-control input-lg" placeholder="Qty"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <a href="#" class="add-order-items"><i class="fa fa-plus"></i> Add More Items</a>
                        </div>

                        <ul class="list-inline">
                            <li>
                                <a href="#itemsNotAddedtoCart" data-toggle="modal" class="btn btn-warning btn-lg">Add to Cart</a>
                            </li>
                            <li>
                                <a href="#clearOrderPad" data-toggle="modal" class="btn btn-default btn-lg">Clear All</a>
                            </li>
                        </ul>
                    </form>

                </div>
            </div>
        </div>
        <!-- main container end -->
        <?php include 'includes/footer.php'; ?>
            <?php include 'includes/scripts.php'; ?>
</body>

</html>
<?php include 'includes/modals.php'; ?>
