Setting the index configuration for an application

1. copy local index-config.json to the /apps/endeca/apps/CPS/config/index_config under VM
2. navigate to /apps/endeca/apps/CPS/control
3. run command
./index_config_cmd.sh set-config -o all -f /apps/endeca/apps/CPS/config/index_config/index-config.json