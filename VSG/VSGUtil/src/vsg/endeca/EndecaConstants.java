package vsg.endeca;

/**
 * Contains constants necessary for working with Endeca
 */
public interface EndecaConstants {

	//query constants
	public static final char EQUALS_MARK = '=';
	public static final char QUESTION_MARK = '?';
	public static final char AMPERSAND = '&';

	//url parameter names
	String BASIC_NAVIGATION_PARAM = "N";
	String OFFSET_PARAM = "No";
	String RECORDS_PER_PAGE_PARAM = "Nrpp";
	String SORTING_PARAM = "Ns";
	String SEARCH_PARAM = "Ntt";
	String RECORD_FILTER_PARAM = "Nr";
	String RANGE_FILTER_PARAM = "Nf";

	/**
	 * The refinement is selected property name.
	 */
	String REFINEMENT_IS_SELECTED_PROPERTY_NAME = "selected";

	//indexing properties and dimensions
	String INDEX_PRICE_PROPERTY_NAME_TEMPLATE = "product.price.role-{0}.region-{1}";
	String INDEX_PRICE_PROPERTY_NAME_WITHOUT_REGION_TEMPLATE = "product.price.role-{0}";
	String CATEGORY_DIMENSION_NAME = "product.category";
	String CATEGORY_DIMENSION_ID = "10011";
	String INDEX_PRODUCT_LINK_PROPERTY_NAME = "product.seoUrl";
	String INDEX_CATEGORY_IS_LEAF_PROPERTY_NAME = "category.isLeaf";
	String INDEX_CATEGORY_REPOSITORY_ID_PROPERTY_NAME = "category.repositoryId";
	String INDEX_BUSINESS_ROLE_ID_PROPERTY_NAME = "businessRole.repositoryId";
	String INDEX_BUSINESS_REGION_ID_PROPERTY_NAME = "businessRegion.repositoryId";
	String INDEX_CATEGORY_DESCRIPTION_PROPERTY_NAME = "category.description";
	String CATEGORY_NAV_SATTE_PROPERTY = "categoryNavState";
	String DIMENSION_VALUE_UNIQUE_ID_PROPERTY_NAME = "DGraph.Spec";
	String DIMENSION_VALUE_DGRAPH_AGGR_BINS_PROPERTY_NAME = "DGraph.AggrBins";
	String DIMENSION_VALUE_DGRAPH_BINS_PROPERTY_NAME = "DGraph.Bins";
	String ALIAS_PROPERTY_NAME = "product.prodAlias.";
	String STOCKING_TYPE_PROPERTY_NAME = "product.stockingType";

	String INDEX_BOOLEAN_PROPERTY_TRUE_VALUE_AS_STRING = "1";

	//request parameters and attributes
	String REDIRECT_PARAM_SINGLE_PRODUCT = "endeca_redirect_single_product";
	String REDIRECT_PARAM_EMPTY_RESULTS = "endeca_redirect_empty_results";
	String REFINEMENT_CRUMBS_REQUEST_ATTR_NAME = "refinementCrumbs";
	String SEARCH_CRUMBS_REQUEST_ATTR_NAME = "searchCrumbs";
	String FIRST_REFINEMENT_REQUEST_ATTR_NAME = "firstRefinement";
	String REMOVE_ALL_ACTION_REQUEST_ATTR_NAME = "removeAllAction";
	String INVOKE_ASSEMBLER_FAILED_ATTR_NAME = "invokeAssemblerFailed";

	//ootb
	String ASSEMBLER_CONTENT_COLLECTION_PARAM = "assemblerContentCollection";
	String ASSEMBLER_RULE_LIMIT_PARAM = "assemblerRuleLimit";
	String ROOT_CONTENT_ITEM_ATTRIBUTE = "rootContentItem";

	//endeca managed pages urls
	String SEARCH_PAGE_URL = "/search";
	String CATEGORY_LANDING_LISTING_PAGE_URL = "/category";
	String EMPTY_RESULTS_PAGE_URL = "/catalog/zero-results.jsp";

	String SEARCH_KEY_ALL = "All";

	String ROOT_DIM_VAL_ID = "0";

	String ERROR_404_PAGE_URL = "/error404.jsp";

	Integer DEFAULT_RECS_PER_PAGE_VALUE = 24;

	//rollup keys
	String PRODUCT_REPOSITORY_ID_ROLLUP_KEY = "product.repositoryId";

	//refinement menu
	String REFINEMENT_MENU_SORT_DYNRANK = "DYNRANK";
	String REFINEMENT_MENU_SORT_STATIC = "STATIC";
	String REFINEMENT_MENU_SORT_DEFAULT = "DEFAULT";
	String REFINEMENT_MENU_DYNAMIC_CONFIG_PARAMETER = "Nrmc";
	String REFINEMENT_MENU_SHOW_MORE_PARAMETER = "showMoreIds";
	String REFINEMENT_MENU_SHOW_MORE_LINK = "showMoreLink";
	String REFINEMENT_MENU_NUMBER_TO_DISPLAY = "refNumberToDisplay";
	String REFINEMENT_MENU_SPLIT_BY_FIRST_LETTER = "splitByFirstLetter";
	String REFINEMENT_MENU_SPLITTED_REFINEMENTS = "splittedRefinements";

	//seo
	String CATEGORY_SEO_SPECIFIC_URL_ELEMENT = "/_/";

	//response content item params
	String RESPONSE_CONTENT_ITEM_ERROR_INDICATOR = "@error";
	String RESPONSE_CONTENT_ITEM_REDIRECT_PARAMETER = "endeca:redirect";
	String RESPONSE_CONTENT_ITEM_REDIRECT_LINK = "link";

	//guided nav snapshots constants
	String FILTERS_KEY_DELIMETER = "+";

	//refinements
	String REFINEMENT_DIMENSION_ID_PROP_NAME = "dimensionId";
	String REFINEMENT_DIMVAL_ID_PROP_NAME = "dimValId";

	String LISTING_ALL_PAGE_URI = "/category";

	public static final int INDEX_NONE_LEVEL = 0;
	public static final int INDEX_PARTIAL_LEVEL = 1;
	public static final int INDEX_BASELINE_LEVEL = 2;
}
