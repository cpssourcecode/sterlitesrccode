package vsg.form;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * Request processor for form handler. Process request within
 * RepeatRequestMonitor and transaction.
 * 
 * @author Andy Porter
 */
public abstract class TransactionalRequestHandler {

	/** The Request unique key. */
	private String mRequestUniqueKey;

	/**
	 * Instantiates a new transactional request handler.
	 * 
	 * @param pRequestUniqueKey the request unique key
	 */
	public TransactionalRequestHandler(final String pRequestUniqueKey) {
		setRequestUniqueKey(pRequestUniqueKey);
	}

	/**
	 * Processes request, does necessary actions.
	 * 
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public abstract void doHandle() throws ServletException, IOException;

	/**
	 * Process request in transaction and with RepeatRequestMonitor.
	 * 
	 * @param pFormHandler the form
	 * @param pRequest the http request
	 * @param pResponse the http response
	 * @return true, if request is processed successful, otherwise false
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean handleInTransaction(final IFormHandler pFormHandler, final DynamoHttpServletRequest pRequest,
			final DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		RepeatingRequestMonitor rrm = pFormHandler.getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(getRequestUniqueKey()))) {
			FormHandlerTools formHandlerTools = pFormHandler.getFormHandlerTools();
			TransactionManager tm = pFormHandler.getDefaultTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean isRollback = false;
			try {
				try {
					td.begin(tm);
					doHandle();
				} catch (Exception e) {
					isRollback = true;
					formHandlerTools.addGeneralError(pFormHandler, e);
				} finally {
					pFormHandler.completeRequest();
					td.end(isRollback);
				}
				return pFormHandler.checkFormRedirect(pFormHandler.getSuccessUrl(), pFormHandler.getErrorUrl(),
						pRequest, pResponse);
			} catch (TransactionDemarcationException e) {
				formHandlerTools.addGeneralError(pFormHandler, e);
				return pFormHandler.checkFormRedirect(pFormHandler.getSuccessUrl(), pFormHandler.getErrorUrl(),
						pRequest, pResponse);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(getRequestUniqueKey());
				}
			}
		} else {
			return false;
		}
	}

	/**
	 * Gets the request unique key.
	 * 
	 * @return the request unique key
	 */
	public String getRequestUniqueKey() {
		return mRequestUniqueKey;
	}

	/**
	 * Sets the request unique key.
	 * 
	 * @param requestUniqueKey the new request unique key
	 */
	public void setRequestUniqueKey(final String requestUniqueKey) {
		mRequestUniqueKey = requestUniqueKey;
	}

}