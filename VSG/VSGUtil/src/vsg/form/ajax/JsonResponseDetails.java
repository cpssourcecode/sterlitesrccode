package vsg.form.ajax;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import vsg.form.IFormHandler;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;

/**
 * Contains response details in JSON format.
 *
 * @author Andy Porter
 */
public class JsonResponseDetails {

	/** The Constant CODE. */
	public static final String CODE = "code";

	/** The Constant SUCCESS_URL. */
	public static final String SUCCESS_URL = "successUrl";

	/** The Constant ERROR_URL. */
	public static final String ERROR_URL = "errorUrl";

	/** The Constant ERR_PROPERTIES. */
	public static final String ERR_PROPERTIES = "errProperties";

	/** The Constant ERR_MESSAGES. */
	public static final String ERR_MESSAGES = "errMessages";

	/** The Content type. */
	private ContentType mContentType = ContentType.APP_JSON_CHARSET;

	/** The Code. */
	private ResponseDetailsCode mCode = ResponseDetailsCode.UNDEFINED;

	/** The Success url. */
	private String mSuccessUrl = "";

	/** The Error url. */
	private String mErrorUrl = "";

	/** The Error properties. */
	private Set<String> mErrorProperties = new HashSet<String>();

	/** The Error messages. */
	private List<String> mErrorMessages = new ArrayList<String>();

	/** The m form response details. */
	private Map<String,Object> mFormResponseDetails = new LinkedHashMap<String,Object>();

	/**
	 * Enum represents all possible response codes.
	 *
	 * @author Andy Porter
	 */
	public enum ResponseDetailsCode {

		/** The undefined response code. */
		UNDEFINED,
		/** The success response code. */
		SUCCESS,
		/** The error response code. */
		ERROR,
		/** The redirect response code. */
		REDIRECT,
		/** The skip response code. */
		SKIP;

		@Override
		public String toString() {
			return super.toString().toLowerCase();
		}
	}

	/**
	 * Instantiates a new json response details.
	 */
	private JsonResponseDetails() {

	}

	/**
	 * Gets the json object.
	 *
	 * @return the json
	 * @throws JSONException the jSON exception
	 */
	public JSONObject getJSON() throws JSONException {
		JSONObject json = new JSONObject();

		json.put(CODE, mCode.toString());
		json.put(SUCCESS_URL, mSuccessUrl);
		json.put(ERROR_URL, mErrorUrl);
		json.put(ERR_PROPERTIES, new JSONArray(mErrorProperties));
		json.put(ERR_MESSAGES, new JSONArray(mErrorMessages));

		Map<String,Object> responseDetails = getFormResponseDetails();
		if ( null!= responseDetails && !responseDetails.isEmpty() ) {
			for(String paramKey : responseDetails.keySet()) {
				json.put(paramKey, responseDetails.get(paramKey));
			}
		}

		return json;
	}

	/**
	 * Creates the JsonResponseDetails object.
	 *
	 * @return the json response details
	 */
	public static JsonResponseDetails create() {
		return new JsonResponseDetails();
	}

	/**
	 * Creates the JsonResponseDetails object from the form.
	 *
	 * @param pFormHandler the form
	 * @return the json response details
	 */
	public static JsonResponseDetails create(IFormHandler pFormHandler) {
		return createJsonResponseDetails(pFormHandler);
	}

	/**
	 * Creates the json response details.
	 *
	 * @param pFormHandler the form
	 * @return the json response details
	 */
	private static JsonResponseDetails createJsonResponseDetails(IFormHandler pFormHandler) {
		JsonResponseDetails jsonReponseDetails = new JsonResponseDetails();
		if (pFormHandler.getFormError()) {
			jsonReponseDetails = createErrorJsonResponseDetails(pFormHandler);
		} else {
			jsonReponseDetails = createSuccessJsonResponseDetails(pFormHandler);
		}
		jsonReponseDetails.setFormResponseDetails(pFormHandler.getResponseDetails());
		jsonReponseDetails.setErrorUrl(pFormHandler.getErrorUrl());
		jsonReponseDetails.setSuccessUrl(pFormHandler.getSuccessUrl());
		return jsonReponseDetails;
	}

	/**
	 * Creates the error json response details.
	 *
	 * @param pFormHandler the form
	 * @return the json response details
	 */
	private static JsonResponseDetails createErrorJsonResponseDetails(IFormHandler pFormHandler) {
		JsonResponseDetails jsonReponseDetails = new JsonResponseDetails();
		jsonReponseDetails.setCode(ResponseDetailsCode.ERROR);
		for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException dropletException = (DropletException) pFormHandler.getFormExceptions().get(i);
			addDropletExceptionDetails(jsonReponseDetails, dropletException);
		}
		return jsonReponseDetails;
	}

	/**
	 * Creates the success json response details.
	 *
	 * @param pFormHandler the form
	 * @return the json response details
	 */
	private static JsonResponseDetails createSuccessJsonResponseDetails(IFormHandler pFormHandler) {
		JsonResponseDetails jsonReponseDetails = new JsonResponseDetails();
		jsonReponseDetails.setCode(ResponseDetailsCode.SUCCESS);
		return jsonReponseDetails;
	}

	/**
	 * Adds the droplet exception details in the json.
	 *
	 * @param jsonReponseDetails the json reponse details
	 * @param dropletException the droplet exception
	 */
	private static void addDropletExceptionDetails(JsonResponseDetails jsonReponseDetails,
			DropletException dropletException) {
		String errorMessage = dropletException.getMessage();
		jsonReponseDetails.addErrorMessage(errorMessage);
		if (dropletException instanceof DropletFormException) {
			DropletFormException dropletFormException = (DropletFormException) dropletException;
			String errorProperty = dropletFormException.getPropertyPath();
			jsonReponseDetails.addErrorProperty(errorProperty);
		}
	}

	/**
	 * Gets the content type.
	 *
	 * @return the content type
	 */
	public ContentType getContentType() {
		return mContentType;
	}

	/**
	 * Sets the code.
	 *
	 * @param pCode the new code
	 */
	public void setCode(ResponseDetailsCode pCode) {
		mCode = pCode;
	}

	/**
	 * Sets the success url.
	 *
	 * @param pSuccessUrl the new success url
	 */
	public void setSuccessUrl(String pSuccessUrl) {
		mSuccessUrl = pSuccessUrl;
	}

	/**
	 * Sets the error url.
	 *
	 * @param pErrorUrl the new error url
	 */
	public void setErrorUrl(String pErrorUrl) {
		mErrorUrl = pErrorUrl;
	}

	/**
	 * Adds the error property.
	 *
	 * @param pErrorProperty the error property
	 */
	public void addErrorProperty(String pErrorProperty) {
		mErrorProperties.add(pErrorProperty);
	}

	/**
	 * Adds the error message.
	 *
	 * @param pErrorMessage the error message
	 */
	public void addErrorMessage(String pErrorMessage) {
		mErrorMessages.add(pErrorMessage);
	}

	/**
	 * Gets the form response details.
	 *
	 * @return the form response details
	 */
	public Map<String,Object> getFormResponseDetails() {
		return mFormResponseDetails;
	}

	/**
	 * Sets the form response details.
	 *
	 * @param mFormResponseDetails the m form response details
	 */
	public void setFormResponseDetails(Map<String,Object> mFormResponseDetails) {
		this.mFormResponseDetails = mFormResponseDetails;
	}

}
