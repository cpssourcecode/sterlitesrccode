package vsg.form.ajax;

/**
 * The Enum which contains all supported content types of http request/response.
 * 
 * @author Andy Porter
 */
public enum ContentType {

	/** The application json content type. */
	APP_JSON("application/json"),

	/** The app json charset. */
	APP_JSON_CHARSET("application/json;charset=UTF-8"),

	/** The text json content type. */
	TEXT_JSON("text/json");

	/** The Value. */
	private String mValue;

	/**
	 * Instantiates a new content type.
	 * 
	 * @param pValue the value
	 */
	ContentType(String pValue) {
		mValue = pValue;
	}

	@Override
	public String toString() {
		return mValue;
	}
}