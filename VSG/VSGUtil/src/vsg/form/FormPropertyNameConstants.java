package vsg.form;

/**
 * Contains all form property names.
 *
 * @author Andy Porter
 */
public interface FormPropertyNameConstants {

	/** The id property name. */
	String ID = "id";

	/** The login property name. */
	String LOGIN = "login";

	/** The password property name. */
	String PSWD = "password";

	/** The remember me property name. */
	String REMEMBER_ME="rememberMe";

	/** The confirm password property name. */
	String CONFIRM_PSWD = "confirmPassword";

	/** The email property name. */
	String EMAIL = "email";

	/** The confirm email property name. */
	String CONFIRM_EMAIL = "confirmEmail";

	/** The first name property name. */
	String FIRST_NAME = "firstName";

	/** The last name property name. */
	String LAST_NAME = "lastName";

	/** The last name property name. */
	String NAME = "name";

	/** The entity name property name. */
	String ENTITY_NAME = "entityName";

	/** The account number property name. */
	String ACCOUNT_NUMBER = "accountNumber";

	/** The chefs customer property name. */
	String CHEFS_CUSTOMER = "cwCustomer";

	/** The address1 property name. */
	String HOME_ADDRESS1 = "homeAddress.address1";

	/** The address2 property name. */
	String HOME_ADDRESS2 = "homeAddress.address2";

	/** The city property name. */
	String CITY = "homeAddress.city";

	/** The state property name. */
	String STATE = "homeAddress.state";

	/** The zip code property name. */
	String ZIP_CODE = "homeAddress.postalCode";

	/** The phone number property name. */
	String PHONE_NUMBER = "homeAddress.phoneNumber";

	/** The role name property name. */
	String ROLE_NAME = "roleName";

	/** The region name. */
	String REGION_NAME = "regionName";

	/** The quote region name. */
	String QUOTE_REGION_NAME = "regionLocationName";

	/** The recover password date. */
	String RECOVER_PSWD_DATE = "recoverPasswordDate";

	/** The generated password. */
	String GENERATED_PSWD = "generatedPassword";

	/** The quantity. */
	String QUANTITY = "quantity";

	String NOTES = "notes";

	String RECEPIENTS = "recepients";

	String RECEPIENTS_LIST = "recepientsList";

	String COPY = "copy";

	String TRUE = "true";

	String FALSE = "false";

	String EMAIL_MESSAGE = "emailMessage";

	/** The event name of giftlist */
	String EVENT_NAME = "eventName";

	/** The description of giftlist */
	String DESCRIPTION = "description";

	/** The giftlist */
	String GIFTLIST = "favorites";

	/** The giftlist id */
	String GIFTLIST_ID = "giftlistId";

	/** for Career page */
	String SUBJECT = "subject";
	String MESSAGE = "message";
	String RESUME = "resume";

	/** The company location */
	String COMPANY = "company";

	/** The office location */
	String OFFICE_LOCATION = "officeLocation";

	/** The display name properties */
	String PROPERTY_EVENT_NAME = "eventName";

	/** The job title. */
	String JOB_TITLE = "jobTitle";

	/** The other location info */
	String OTHER_LOCATION_INFO = "otherLocationInfo";

	/** The other */
	String OTHER = "Other";

	/** The other subject*/
	String OTHER_SUBJECT = "otherSubject";

}
