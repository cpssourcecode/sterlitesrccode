package vsg.form;

import java.io.IOException;
import java.util.Dictionary;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;

import atg.droplet.DropletException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * Contains all methods supported by application forms
 *
 * @author Andy Porter
 */
public interface IFormHandler  extends ITransactionalForm{

	/**
	 * Gets the form value.
	 *
	 * @return the value
	 */
	Dictionary<Object, Object> getValue();

	/**
	 * Gets the edited form value.
	 *
	 * @return the edits the value
	 */
	Map<Object, Object> getEditValue();

	/**
	 * Gets the response details form value.
	 *
	 * @return the edits the value
	 */
	Map<String, Object> getResponseDetails();

	/**
	 * Gets the property value by property name.
	 *
	 * @param pName the name
	 * @return the value property
	 */
	Object getValueProperty(String pName);

	/**
	 * Gets the form error.
	 *
	 * @return the form error
	 */
	boolean getFormError();

	/**
	 * Gets the form exceptions.
	 *
	 * @return the form exceptions
	 */
	Vector<Object> getFormExceptions();

	/**
	 * Adds the form exception.
	 *
	 * @param e the e
	 */
	void addFormException(DropletException e);

	/**
	 * Check form redirect.
	 *
	 * @param pSuccessURL the success url
	 * @param pFailureURL the failure url
	 * @param pRequest the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	boolean checkFormRedirect (String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException ;

	/**
	 * Gets the success url.
	 *
	 * @return the success url
	 */
	String getSuccessUrl();

	/**
	 * Gets the error url.
	 *
	 * @return the error url
	 */
	String getErrorUrl();


	/**
	 * Gets the form messages tools.
	 *
	 * @return the form messages tools
	 */
	FormHandlerTools getFormHandlerTools();

	/**
	 * Checks if form request is completed. Only when request is completed, checkFormRedirect method can add Ajax response
	 *
	 * @return true, if form request is completed
	 */
	boolean isRequestCompleted();

	/**
	 * Complete form request.
	 */
	void completeRequest();

}
