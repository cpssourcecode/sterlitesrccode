package vsg.form;

import javax.transaction.TransactionManager;

import atg.commerce.util.RepeatingRequestMonitor;

/**
 * Contains methods necessary for processing forms in transactions
 *
 * @author Andy Porter
 */
public interface ITransactionalForm {

	/**
	 * Gets the repeating request monitor.
	 *
	 * @return the repeating request monitor
	 */
	RepeatingRequestMonitor getRepeatingRequestMonitor();
	
	/**
	 * Gets the default transaction manager.
	 *
	 * @return the default transaction manager
	 */
	TransactionManager getDefaultTransactionManager();
}
