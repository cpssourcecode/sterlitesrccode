package vsg.form;

import java.io.IOException;

import javax.servlet.ServletException;

import vsg.util.ajax.AjaxUtils;
import vsg.messages.IMessagesTools;
import vsg.messages.MessageKey;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.json.JSONException;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletResponse;

/**
 * Tools which contains all basic methods which are used in Base form handlers.
 * It is error handling, success messages handling and Ajax form redirection.
 * 
 * @author Andy Porter
 */
public class FormHandlerTools extends GenericService {

	/** The Messages tools. */
	private IMessagesTools mMessagesTools;

	/**
	 * Implements checkFormRedirect logic for ajax request
	 *
	 * @param pFormHandler the form handler
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public boolean checkAjaxFormRedirect(final IFormHandler pFormHandler, final DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (pFormHandler.isRequestCompleted()) {
			try {
				AjaxUtils.addAjaxResponse(pFormHandler, pResponse);
				return false;
			} catch (JSONException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
		return true;
	}

	/**
	 * Adds the general form exception.
	 * 
	 * @param pFormHandler the form
	 * @param pException the exception
	 */
	public void addGeneralError(final IFormHandler pFormHandler, final Exception pException) {
		addErrorMessage(pFormHandler, MessageKey.GEN_ERR_SYSTEM);
		if (isLoggingError()) {
			logError(pException);
		}
	}

	/**
	 * Adds the form exception.
	 * 
	 * @param pFormHandler the form
	 * @param pMessageKey the message key
	 * @param pParams the parameters, if the message is parameterized
	 */
	public void addErrorMessage(final IFormHandler pFormHandler, final MessageKey pMessageKey, final Object... pParams) {
		String errorMessageText = getMessagesTools().getMessage(pMessageKey, pParams);
		pFormHandler.addFormException(new DropletException(errorMessageText));
	}

	/**
	 * Adds the form exception.
	 * 
	 * @param pFormHandler the form
	 * @param pMessageKey the message key
	 * @param pPropertyName the property name
	 * @param pParams the parameters, if the message is parameterized
	 */
	public void addErrorMessage(final IFormHandler pFormHandler, final MessageKey pMessageKey,
			final String pPropertyName, final Object... pParams) {
		String errorMessageText = getMessagesTools().getMessage(pMessageKey, pParams);
		pFormHandler.addFormException(new DropletFormException(errorMessageText, pPropertyName));
	}

	/**
	 * Adds the form success message.
	 * 
	 * @param pFormHandler the form handler
	 * @param pMessageKey the message key
	 * @param pParams the params
	 */
	public void addSuccessMessage(final IFormHandler pFormHandler, final MessageKey pMessageKey,
			final Object... pParams) {
		String message = getMessagesTools().getMessage(pMessageKey, pParams);
		pFormHandler.getResponseDetails().put(ResponseConstants.SUCCESS_MSG, message);
	}

	/**
	 * Gets the messages tools.
	 * 
	 * @return the messages tools
	 */
	public IMessagesTools getMessagesTools() {
		return mMessagesTools;
	}

	/**
	 * Sets the messages tools.
	 * 
	 * @param pMessagesTools the new messages tools
	 */
	public void setMessagesTools(final IMessagesTools pMessagesTools) {
		mMessagesTools = pMessagesTools;
	}

}
