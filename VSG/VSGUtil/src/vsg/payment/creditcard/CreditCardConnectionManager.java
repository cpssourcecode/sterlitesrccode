package vsg.payment.creditcard;

import atg.commerce.order.Order;
import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.CreditCardStatus;

/**
 * CreditCardConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * 
 * @author Tim Harshbarger
 *
 */
public interface CreditCardConnectionManager {
		
	public abstract CreditCardStatus requestAuthorization(Order pOrder);

	public abstract CreditCardStatus requestCapture(CreditCardInfo pCreditCardInfo, 
    		CreditCardStatus pCreditCardStatus);
    
    public abstract CreditCardStatus requestCredit(CreditCardInfo pCreditCardInfo, 
    		CreditCardStatus pCreditCardStatus);
    
    public abstract CreditCardStatus requestAuthorizationReversal(CreditCardInfo pCreditCardInfo, 
    		CreditCardStatus pCreditCardStatus);

    public abstract ConnectionManagerTools getConnectionManagerTools();

}
