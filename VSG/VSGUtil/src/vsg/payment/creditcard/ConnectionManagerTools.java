package vsg.payment.creditcard;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupManager;
import atg.commerce.order.PaymentGroupRelationship;
import atg.commerce.order.RelationshipTypes;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingTools;
import atg.core.util.NumberFormat;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.payment.Message;
import atg.payment.creditcard.CreditCardStatus;
import atg.payment.creditcard.CreditCardStatusImpl;
import atg.repository.Repository;
import atg.userprofiling.PropertyManager;

/**
 * ConnectionManagerTools
 * 
 * <h4>Description</h4>  Tools class for a connection manager 
 * integration.
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 * 
 */
public class ConnectionManagerTools extends GenericService {

	/**
	 * Added so can get it on request scope component
	 */
	private final static String COMPONENT_NAME = "/vsg/payment/creditCard/ConnectionManagerTools";
	
	/** Constants - Move to common constants class? */
	public static final String PAYMENT_GROUP_TYPE_CREDIT_CARD = "creditCard";
	public static final String PAYMENT_GROUP_TYPE_GIFT_CERTIFICATE = "giftCertificate";
	public static final String PAYMENT_GROUP_TYPE_STORE_CREDIT = "storeCredit";
	
	/**
     * Default Profile Type property value
     */
    private String mDefaultProfileType;
	
    /**
     * access to the profileRepository 
     */
    private Repository mProfileRepository;

	/**
	 * Payment Group Manager property
	 */
	PaymentGroupManager mPaymentGroupManager;

    /**
	 * property manager
	 */
	private PropertyManager mPropertyManager;

	/**
     * Default filler email address to send
     * to CyberSource
     */
    private String mDefaultRequestEmail;

	/**
	 * the PricingTools object which helps this calculator calculate money
	 * figures'
	 */
	private PricingTools mPricingTools;

	/**
	 * @return
	 */
	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	/**
	 * @param pProfileRepository
	 */
	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}

    /**
     * @return the defaultProfileType
     */
    public String getDefaultProfileType(){
        return mDefaultProfileType;
    }

    /**
     * @param pDefaultProfileType
     */
    public void setDefaultProfileType(String pDefaultProfileType){
        mDefaultProfileType = pDefaultProfileType;
    }

	/**
	 * Set the PaymentGroupManager property.
	 * 
	 * @param pPaymentGroupManager
	 *            a <code>PaymentGroupManager</code> value
	 */
	public void setPaymentGroupManager(PaymentGroupManager pPaymentGroupManager) {
		mPaymentGroupManager = pPaymentGroupManager;
	}

	/**
	 * Return the PaymentGroupManager property.
	 * 
	 * @return a <code>PaymentGroupManager</code> value
	 */
	public PaymentGroupManager getPaymentGroupManager() {
		return mPaymentGroupManager;
	}

    /**
     * return propertyManager property
     * 
     * @return
     */
    public PropertyManager getPropertyManager(){
        return mPropertyManager;
    }

    /**
     * set the propertyManager property
     * 
     * @param pPropertyManager
     */
    public void setPropertyManager(PropertyManager pPropertyManager){
        mPropertyManager = pPropertyManager;
    }

    /**
     * Returns the default email value to send 
     * requests to Cyber Source
     * @return
     */
    public String getDefaultRequestEmail(){
    	return mDefaultRequestEmail;
    }
    
    /**
     * Sets the default email value for sending
     * requests to Cyber Source
     * @param pDefaultRequestEmail
     */
    public void setDefaultRequestEmail(String pDefaultRequestEmail){
    	mDefaultRequestEmail = pDefaultRequestEmail;
    }
    
	/**
	 * the PricingTools object which helps this calculator calculate money
	 * figures
	 * 
	 * @beaninfo description: the PricingTools object which helps this
	 *           calculator calculate money figures
	 * @param pPricingTools
	 *            new value to set
	 */
	public void setPricingTools(PricingTools pPricingTools) {
		mPricingTools = pPricingTools;
	}

	/**
	 * the PricingTools object which helps this calculator calculate money
	 * figures
	 * 
	 * @beaninfo description: the PricingTools object which helps this
	 *           calculator calculate money figures
	 * @return property PricingTools
	 */
	public PricingTools getPricingTools() {
		return mPricingTools;
	}
	
	/**
	 * This method will look a subscriptionId on the order's credit card 
	 * payment group to be used as reference for credit card information
	 * with the integration service.
	 * 
	 * @param pOrder
	 * @return
	 */
	public String getOrderCreditCardSubscriptionId(Order pOrder, String pSubscriptionIdProperty, String pDefaultSubscriptionId) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".getOrderCreditCardSubscriptionId")
					.append(" - start")
					.toString());

		// get credit card payment group
		CreditCard creditCard = getCreditCard(pOrder);
		if (creditCard != null && !StringUtils.isBlank(pSubscriptionIdProperty)){
			// get subscriptionId from credit card payment group
			String subscriptionId = (String) creditCard
					.getPropertyValue(pSubscriptionIdProperty);
			// check that subscriptionId is not blank and there is an incoming default subscription that
			// the order credit card subscriptionId is not equal to
			if (!StringUtils.isBlank(subscriptionId) 
					&& !subscriptionId.equals(pDefaultSubscriptionId)) {
				if (isLoggingDebug()){
					logDebug(new StringBuilder().append(
							"Return subscriptionId:").append(
							subscriptionId).append(" from Order Credit Card Payment Group:")
							.append(creditCard.getId())
							.append("\n")
							.append(COMPONENT_NAME)
							.append(".getCreditCardSubscriptionId")
							.append(" - exit")
							.toString());
				}
				return subscriptionId;
			}
		} else {
			if (isLoggingDebug())
				logDebug(new StringBuilder().append(COMPONENT_NAME)
						.append(".getOrderCreditCardSubscriptionId -")
						.append("Credit card payment group is not present or invalide incoming subscriptionIdProperty:")
						.append(pSubscriptionIdProperty)
						.toString());
		}
		if (isLoggingDebug())
			logDebug(new StringBuilder()
				.append(COMPONENT_NAME)
				.append(".getCreditCardSubscriptionId")
				.append(" - no subscriptionId - exit")
				.toString());
		return null;
	}

	/**
	 * Returns the CreditCard payment group for the order. This is a utility
	 * method is used to easily get the credit card payment group. This method
	 * returns the first payment group of type CreditCard. If multiple credit
	 * cards are supported, this method needs to be updated.
	 * 
	 * This code was liberated from StoreOrderTools
	 * 
	 * @param pOrder
	 *            - order to get credit card from
	 * @return credit card for this order
	 */
	public CreditCard getCreditCard(Order pOrder) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".getCreditCard")
					.append(" - start")
					.toString());
		if (pOrder == null) {
			if (isLoggingDebug())
				logDebug("Null order passed to getCreditCard method, return null.");
			return null;
		}

		List paymentGroups = pOrder.getPaymentGroups();
		if (paymentGroups == null) {
			if (isLoggingDebug())
				logDebug("Order has null list of payment groups.");
			return null;
		}

		int numPayGroups = paymentGroups.size();
		if (numPayGroups == 0) {
			if (isLoggingWarning())
				logWarning("No payment group on this order!");
			return null;
		}
		
		// We are only supporting a single credit card payment group.
		// Return the first one found.
		Iterator paymentIterator = paymentGroups.iterator();
		while (paymentIterator.hasNext()) {
			PaymentGroup pg = (PaymentGroup) paymentIterator.next();
			if (pg instanceof CreditCard) {
				if (isLoggingDebug())
					logDebug(new StringBuilder().append(COMPONENT_NAME)
							.append(".getCreditCard")
							.append(" - returning credit card:")
							.append(pg.getId()).append(" - exit").toString());
				return (CreditCard) pg;
			}
		}
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".getCreditCard")
					.append(" - returning no credit card - exit")
					.toString());
		return null;
	}
	
    /**
     * Helper method to get the shipping method from the first hardgood
     * shipping group.
     * 
     * @param pOrder
     * @return
     */
	public HardgoodShippingGroup getFirstHardgoodShippingGroup(Order pOrder) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".getFirstHardgoodShippingGroup")
					.append(" - start")
					.toString());
		Collection shippingGroups = pOrder.getShippingGroups();
		Iterator shippingGrouperator = shippingGroups.iterator();
		ShippingGroup sg = null;
		while (shippingGrouperator.hasNext()) {
			sg = (ShippingGroup) shippingGrouperator.next();
			if (sg instanceof HardgoodShippingGroup) {
				if (isLoggingDebug())
					logDebug(new StringBuilder().append(COMPONENT_NAME)
							.append(".getFirstHardgoodShippingGroup")
							.append(" - exit")
							.toString());
				return (HardgoodShippingGroup) sg;
			}
		}
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".getFirstHardgoodShippingGroup")
					.append(" - exit")
					.toString());
		return null;
	}    

	/**
	 * This will calculate the actual amount remaining on the order before sending out
	 * to the payment processor.  It will take the initial order total and reduce it by 
	 * any gift certificates or store credits applied.
	 * 
	 * Conversion requirement:
	 * if ImpliedDecimal flag is true, convert to an implied decimal value
	 * 
	 * Implied Decimal including those currencies that are a zero exponent. For example,
	 * both $100.00 (an exponent of 2) and 100 Yen (an exponent of 0) should be sent as
	 * <Amount>10000</Amount>. 
	 * 
	 * @param pOrder
	 * @param pImpliedDecimal
	 * @return
	 */
	public double getOrderTotal(Order pOrder, boolean pImpliedDecimal) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".processTotals")
					.append(" - start")
					.toString());

		double initialTotal = 0.0;
		try {
			// convert order total to BigDecimal and round with pricing tools
			//initialTotal = getPricingTools().round(pOrder.getPriceInfo().getTotal());			
			initialTotal = pOrder.getPriceInfo().getTotal();
			if (isLoggingDebug())
				logDebug(new StringBuilder().append("Initial un-rounded order total:")
						.append(initialTotal)
						.toString());
			
			List<PaymentGroupRelationship> relationships = pOrder.getPaymentGroupRelationships();
			for (PaymentGroupRelationship relationship : relationships){
				// if payment group is gift certificate or store credit, subtract amount from total
				if(relationship.getPaymentGroup()!=null &&
						(relationship.getPaymentGroup().getPaymentGroupClassType().equals(PAYMENT_GROUP_TYPE_GIFT_CERTIFICATE) 
						|| relationship.getPaymentGroup().getPaymentGroupClassType().equals(PAYMENT_GROUP_TYPE_STORE_CREDIT))) {
					// check if relationship type is ORDERAMOUNT which is 401, which means
					// the payment group has part of the order amount to take off the 
					// overall total to get down to what actually needs to be charged
					// on the credit card
					if (relationship.getRelationshipType() == RelationshipTypes.ORDERAMOUNT){
						if (isLoggingDebug())
							logDebug(new StringBuilder().append("Subtracting:")
									.append(relationship.getAmount())
									.append(" from intial total:")
									.append(initialTotal)
									.toString());
						// round amount on payment group to BigDecimal with pricing tools
						//double paymentGroupAmount = getPricingTools().round(relationship.getAmount());			

						// subtract payment group amount from grand total
						//grandTotal-= paymentGroupAmount;
						initialTotal-= relationship.getAmount();
					}
				}
			}
			if (isLoggingDebug())
				logDebug(new StringBuilder().append("Initial order total after deductions:")
						.append(initialTotal).toString());
		}catch (Exception ex){
			logError(ex);
		}
		if (isLoggingDebug())
			logDebug(new StringBuilder().append("Before exit, initialTotal:")
					.append(initialTotal)
					.append(" will be rounded by the OOB tools.")
					.toString());

		if (pImpliedDecimal){
			if (isLoggingDebug())
				logDebug("Converting initial total to be implied decimal value.");
			// amount is multiplied by 100.0 to remove cents
			initialTotal*=100.0;
			if (isLoggingDebug())
				logDebug(new StringBuilder().append("Converted initialTotal * 100.0:")
						.append(initialTotal)
						.toString());			
		}
		// round value with OOB tools
		double finalTotal = getPricingTools().round(initialTotal);
		
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".processTotals")
					.append(" - exit")
					.toString());
		return finalTotal;
	}			
	
	/**
     * @return a CreditCardStatus object with the error message
     * @beaninfo hidden: false
     **/
    public CreditCardStatus processError(String pKey, String pMessage, String pResourceBundle) {
        return processError(pKey, pMessage, 0, pResourceBundle);
    }
    
    /**
     * This error condition should return failure in the pipeline
     * Pulls error messaging based on the key from the resource 
     * bundle. 
     * 
     * @param pKey
     * @param pMessage
     * @param totald
     * @return a CreditCardStatus object with the error message
     * and transaction success as false
     */
    public CreditCardStatus processError(String pKey, String pMessage, double totald, String pResourceBundle) {
        if (isLoggingDebug()){
        	logDebug(new StringBuilder().append(COMPONENT_NAME)
        			.append(".processError")
        			.append(" - start")
        			.append("\nLooking for error message in resource bundle:")
        			.append(pResourceBundle)
        			.append(" with key:")
        			.append(pKey)
        			.toString());
        }
        String error;        
        // check if incoming message
        Message message = new Message(pResourceBundle);
        if (pMessage != null)
            error = message.format(pKey, pMessage);
        else
            error = message.getString(pKey);
        if (isLoggingError())
            logError(new StringBuilder().append(COMPONENT_NAME).append(".processError - adding error to CreditCardStatus: ")
            		.append(error).toString());
        
        CreditCardStatusImpl creditCardStatus = new CreditCardStatusImpl();
        creditCardStatus.setErrorMessage(error);
        creditCardStatus.setTransactionSuccess(false);
        if (totald != 0)
        	creditCardStatus.setAmount(totald);
        if (isLoggingDebug())
        	logDebug(new StringBuilder().append(COMPONENT_NAME)
        			.append(".processError")
        			.append(" - exit")
        			.toString());
        return creditCardStatus;
    } 
    
    /**
     * @param pKey
     * @return
     */
    public String getMessage(String pKey, String pResourceBundle){
        if (isLoggingDebug()){
        	logDebug(new StringBuilder().append(COMPONENT_NAME)
        			.append(".getMessage")
        			.append(" - start")
        			.append("\nLooking for message in resource bundle:")
        			.append(pResourceBundle)
        			.append(" with key:")
        			.append(pKey)
        			.toString());
        }
        Message messageResource = new Message(pResourceBundle);
        String message = messageResource.getString(pKey);
        if (isLoggingDebug())
        	logDebug(new StringBuilder().append(COMPONENT_NAME)
        			.append(".getMessage")
        			.append(" - exit")
        			.toString());
        return message;    	
    }
    
	/**
	 * @param header
	 * @param map
	 */
	public void displayMap(String header, java.util.Map map) {
		vlogDebug(header);

		StringBuilder output = new StringBuilder();
		output.append("\n");
		if (map != null && !map.isEmpty()) {
			java.util.Iterator iter = map.keySet().iterator();
			String key, val;
			while (iter.hasNext()) {
				key = (String) iter.next();
				val = (String) map.get(key);
				/*if (key.equals(FIELD_ACCOUNT_NUMBER)){
					// mask the credit card on the display as to not actually have CC#s in log files
					String creditCardNumber = NumberFormat.formatCreditCardNumber(val, "X", 4, 4);
					dest.append(key + "=" + creditCardNumber + "\n");															
				} else if (key.equals(FIELD_EXPIRATION_DATE)){
					dest.append(key + "=EXPIRATION DATE MASK\n");
				} else if (key.equals(FIELD_CARD_SECURITY_VALUE)){
					dest.append(key + "=CVV MASK\n");										
				} else {*/
					// else display value from map
				output.append(key + "=" + val + "\n");					
				//}
			}
		}

		vlogDebug(output.toString());
	}
}
