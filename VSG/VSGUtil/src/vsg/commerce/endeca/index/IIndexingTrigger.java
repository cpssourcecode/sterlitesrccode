package vsg.commerce.endeca.index;

/**
 * This class used to trigger indexing.
 * 
 * @author Kate Koshman
 */
public interface IIndexingTrigger {
	
	/** The Constant INDEX_NONE_LEVEL. */
	int INDEX_NONE_LEVEL = 0;
	
	/** The Constant INDEX_PARTIAL_LEVEL. */
	int INDEX_PARTIAL_LEVEL = 1;
	
	/** The Constant INDEX_BASELINE_LEVEL. */
	int INDEX_BASELINE_LEVEL = 2;
	
	/**
	 * Trigger indexing.
	 * 
	 * @param pIndexingRequester the indexing requester, not required
	 * @param pIndexingLev the indexing level (1 for partial, 2 for baseline)
	 */
	void triggerIndexing(String pIndexingRequester, int pIndexingLev);
	
	/**
	 * Trigger indexing.
	 * 
	 * @param pIndexingRequester the indexing requester, not required
	 * @param pBaselineIndex the baseline index flag
	 */
	void triggerIndexing(String pIndexingRequester, boolean pBaselineIndex);
}
