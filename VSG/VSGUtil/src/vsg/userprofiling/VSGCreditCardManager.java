package vsg.userprofiling;

import static vsg.constants.VSGConstants.DEFAULT_CARD;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.transaction.TransactionManager;

import vsg.constants.VSGConstants;
import vsg.userprofiling.address.VSGAddressTools;
import vsg.userprofiling.creditcard.ICreditCardManager;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.profile.CommercePropertyManager;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextException;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

/**
 * This class used to create, update and delete credit cards.
 * 
 * @author Kate Koshman, osednev
 * 
 */
public class VSGCreditCardManager extends GenericService implements ICreditCardManager {
	
	/** The Card number template. */
	private static final String CARD_NUMBER_TEMPLATE = "XXXXXXXXXXXXXXXXXXXXXXXXXXXX";
	
	/** The count of credit card number digits to save. */
	private int mNumberDigitsToSave = 4;
	
	/** The Profile tools. */
	private CommerceProfileTools mProfileTools;
	
	/** The Transaction manager for credit cards operations. */
	private TransactionManager mTransactionManager;
	
	/** The credit card map property name. */
	protected String mCreditCardPropertyName = "";
	
	/** The credit card item descriptor billing address property name. */
	protected String mCreditCardItemDescriptorBillingAddressPropertyName = "";
	
	/** The billing address property name. */
	protected String mBillingAddressPropertyName = "";
	
	/**
	 * Creates the credit card. Add it to profiles credit cards.
	 *
	 * @param pProfile the profile
	 * @param pNewCard the map with properties of new credit card
	 * @param pCardName the credit card nickname. Will be generated if not
	 * provided.
	 * @param pAddress the billing address
	 * @return the new credit card nickname
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 * @throws SiteContextException 
	 */
	@SuppressWarnings({ "rawtypes" })
	public String createCreditCard(RepositoryItem pProfile, Map pNewCard, String pCardName, Object pAddress)
			throws CommerceException, RepositoryException, SiteContextException {
		
		cutCreditCardNumber(pNewCard);
		final CommerceProfileTools ptools = getProfileTools();
		try {
			return ptools.createProfileCreditCard(pProfile, pNewCard, pCardName, pAddress);
		} catch (IntrospectionException | PropertyNotFoundException e) {
			throw new CommerceException(e);
		}
	}
	
	/**
	 * Cut credit card number to 4 digits.
	 * 
	 * @param pCreditCard the credit card map
	 */
	@SuppressWarnings({ "rawtypes" })
	protected void cutCreditCardNumber(Map pCreditCard) {
		if (null != pCreditCard) {
			setCreditCardNumber(pCreditCard, maskNumber(getCreditCardNumber(pCreditCard)));
		}
	}
	
	/**
	 * Gets the credit card number.
	 *
	 * @param pCreditCard the credit card
	 * @return the credit card number
	 */
	@SuppressWarnings("rawtypes")
	public String getCreditCardNumber(Map pCreditCard) {
		return (String)pCreditCard.get(VSGConstants.CC_NUMBER);
	}
	
	/**
	 * Sets the credit card number.
	 *
	 * @param pCreditCard the credit card
	 * @param pNumber the number
	 */
	@SuppressWarnings("unchecked")
	public void setCreditCardNumber(Map pCreditCard, String pNumber) {
		pCreditCard.put(VSGConstants.CC_NUMBER, pNumber);
	}
	
	/**
	 * sets profile email
	 * @param pCreditCard - {@link Map}
	 * @param pProfile - {@link RepositoryItem}
	 */
	@SuppressWarnings("unchecked")
	public void setProfileEmailAddress(Map pCreditCard, RepositoryItem pProfile) {
		pCreditCard.put(VSGConstants.EMAIL, pProfile.getPropertyValue(VSGConstants.EMAIL));
	}
	
	/**
	 * Masked Number.
	 *
	 * @param pNumber Number
	 * @return masked number
	 */
	public String maskNumber(String pNumber) {
		String result;
		if (StringUtils.isNotBlank(pNumber)) {
			final int numberLength = pNumber.length();
			if (numberLength > mNumberDigitsToSave) {
				final StringBuilder maksedNumber = new StringBuilder();
				maksedNumber.append(CARD_NUMBER_TEMPLATE.substring(0, numberLength - mNumberDigitsToSave));
				maksedNumber.append(pNumber.substring(numberLength - mNumberDigitsToSave));
				result = maksedNumber.toString();
			} else {
				result = pNumber;
			}
		} else {
			result = "";
		}
		return result;
	}
	
	/**
	 * Gets the credit card by nickname. Delegate to ProfileTools.
	 * 
	 * @param pCreditCardNickname the credit card nickname
	 * @param pProfile the profile
	 * @return the credit card by nickname
	 */
	public RepositoryItem getCreditCardByNickname(String pCreditCardNickname, RepositoryItem pProfile) {
		return getProfileTools().getCreditCardByNickname(pCreditCardNickname, pProfile);
	}
	
	/**
	 * Find card.
	 *
	 * @param pCard the card
	 * @return the map
	 * @throws SiteContextException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map findCard(final RepositoryItem pCard) throws SiteContextException {
		Map result = new HashMap();
		if (null != pCard) {
			result.put("repositoryId", pCard.getRepositoryId());
			result.put("expirationMonth", pCard.getPropertyValue("expirationMonth"));
			result.put("expirationYear", pCard.getPropertyValue("expirationYear"));
			result.put("creditCardType", pCard.getPropertyValue("creditCardType"));
			RepositoryItem adr = (RepositoryItem)pCard.getPropertyValue("billingAddress");
			Map baddr = new HashMap();
			baddr.put("firstName", adr.getPropertyValue("firstName"));
			baddr.put("lastName", adr.getPropertyValue("lastName"));
			result.put("billingAddress", baddr);
			result.put("creditCardNumber", pCard.getPropertyValue("creditCardNumber"));
		}
		return result;
	}
	
	/**
	 * Update profile credit card.
	 *
	 * @param pCardToUpdate the card to update
	 * @param pProfile the profile
	 * @param pCreditCard the updated credit card
	 * @param pNewCCNickname the new credit card nickname
	 * @param pBillingAddress the billing address
	 * @param pBillingAddressClassName the billing address class name
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 * @throws SiteContextException 
	 */
	@SuppressWarnings("rawtypes")
	public void updateProfileCreditCard(RepositoryItem pCardToUpdate, RepositoryItem pProfile, Map pCreditCard,
			String pNewCCNickname, Map pBillingAddress, String pBillingAddressClassName) throws CommerceException,
			RepositoryException, SiteContextException {
		
		setCreditCardNumber(pCreditCard, maskNumber(getCreditCardNumber(pCreditCard)));
		try {
			getProfileTools().updateProfileCreditCard(pCardToUpdate, pProfile, pCreditCard, pNewCCNickname,
					pBillingAddress, pBillingAddressClassName);
		} catch (RepositoryException e) {
			throw e;
		} catch (Exception e) {
			throw new CommerceException(e);
		}
	}
	
	/**
	 * Update address for profile credit card.
	 *
	 * @param pCreditCard the credit card
	 * @param pBillingAddress the billing address values
	 * @param pBillingAddressClassName the billing address class name
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	public void updateAddressForProfileCreditCard(RepositoryItem pCreditCard, Map pBillingAddress,
			String pBillingAddressClassName) throws CommerceException, RepositoryException {
		
		CommercePropertyManager cpmgr = (CommercePropertyManager)getProfileTools().getPropertyManager();
		
		if (pCreditCard != null) {
			String ccAddressProperty = cpmgr.getBillingAddressPropertyName();
			MutableRepositoryItem billingAddress = (MutableRepositoryItem)pCreditCard
					.getPropertyValue(ccAddressProperty);
			
			if (pBillingAddress != null) {
				try {
					Address address = (Address)Class.forName(pBillingAddressClassName).newInstance();
					VSGAddressTools.copyAddress(billingAddress, address);
					VSGAddressTools.copyObject(pBillingAddress, address, null);
					getProfileTools().updateProfileRepositoryAddress(billingAddress, address);
					((MutableRepository)billingAddress.getRepository()).updateItem(billingAddress);
				} catch (RepositoryException e) {
					throw e;
				} catch (Exception e) {
					throw new CommerceException(e);
				}
			}
		}
	}
	
	/**
	 * Checks if credit card changed.
	 * 
	 * @param pOldCard the old card
	 * @param pNewCard the new card
	 * @return true, if some credit card property was changed.
	 */
	@SuppressWarnings("rawtypes")
	protected boolean creditCardChanged(RepositoryItem pOldCard, Map pNewCard) {
		boolean changed = false;
		CommercePropertyManager cpmgr = (CommercePropertyManager)getProfileTools().getPropertyManager();
		String[] cardProperties = cpmgr.getShallowCreditCardPropertyNames();
		for (int i = 0; i < cardProperties.length; ++i) {
			Object oldValue = pOldCard.getPropertyValue(cardProperties[i]);
			Object newValue = pNewCard.get(cardProperties[i]);
			if (newValue != null && !newValue.equals(oldValue)) {
				changed = true;
				break;
			}
		}
		return changed;
	}
	
	/**
	 * Gets the unique credit card nickname.
	 *
	 * @param pCreditCard the credit card
	 * @param pProfile the profile
	 * @param pNewNickname the new nickname
	 * @return the unique credit card nickname
	 */
	public String getUniqueCreditCardNickname(Object pCreditCard, RepositoryItem pProfile, String pNewNickname) {
		Collection nicknames = null;
		Map nicknamesMap = getUsersCreditCardMap(pProfile);
		if (nicknamesMap == null)
			nicknames = new ArrayList();
		else {
			nicknames = nicknamesMap.keySet();
		}
		return getProfileTools().getUniqueCreditCardNickname(pCreditCard, nicknames, pNewNickname);
	}
	
	/**
	 * Gets the users credit card map.
	 *
	 * @param pProfile the profile
	 * @return the users credit card map
	 */
	protected Map<String, RepositoryItem> getUsersCreditCardMap(RepositoryItem pProfile) {
		return ((Map<String, RepositoryItem>)pProfile.getPropertyValue(mCreditCardPropertyName));
	}
	
	/**
	 * Delete credit card.
	 *
	 * @param profile the profile
	 * @param targetCard the target card nickname
	 * @throws SiteContextException 
	 */
	public void deleteCreditCard(Profile profile, String targetCard) throws SiteContextException {
		final CommercePropertyManager pm = (CommercePropertyManager)getProfileTools().getPropertyManager();
		final Map creditCards = (Map)profile.getPropertyValue(pm.getCreditCardPropertyName());
		final RepositoryItem creditCard = (RepositoryItem)creditCards.get(targetCard);
		if (null != creditCard) {
			final String cardId = creditCard.getRepositoryId();
			if (StringUtils.isNotBlank(cardId)) {
				final RepositoryItem defaultCard = (RepositoryItem)profile.getPropertyValue(DEFAULT_CARD);
				if (null != defaultCard && cardId.equals(defaultCard.getRepositoryId())) {
					profile.setPropertyValue(DEFAULT_CARD, null);
				}
			}
		}
		creditCards.remove(targetCard);
	}
	
	/**
	 * Read credit card.
	 *
	 * @param pCreditCard the credit card
	 * @return the map
	 * @throws SiteContextException 
	 */
	public Map<String, Object> getCreditCardPropertiesForDisplay(final RepositoryItem pCreditCard) throws SiteContextException {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("cardNumber", (String)pCreditCard.getPropertyValue("creditCardNumber"));
		result.put("cardType", (String)pCreditCard.getPropertyValue("creditCardType"));
		result.put("cardExpMonth", (String)pCreditCard.getPropertyValue("expirationMonth"));
		result.put("cardExpYear", (String)pCreditCard.getPropertyValue("expirationYear"));
		//Address
		RepositoryItem billingAddress = (RepositoryItem) pCreditCard.getPropertyValue(VSGConstants.BILLING_ADDRESS);
		result.put("address1", (String)billingAddress.getPropertyValue(VSGConstants.ADDRESS1));
		result.put("address2", (String)billingAddress.getPropertyValue(VSGConstants.ADDRESS2));
		result.put("address3", (String)billingAddress.getPropertyValue(VSGConstants.ADDRESS3));
		result.put("city", (String)billingAddress.getPropertyValue(VSGConstants.CITY));
		result.put("state", (String)billingAddress.getPropertyValue(VSGConstants.STATE));
		result.put("postalCode", (String)billingAddress.getPropertyValue(VSGConstants.POSTAL_CODE));
		return result;
	}
	
	/**
	 * define variables.
	 *
	 * @throws ServiceException when errors
	 */
	@Override
	public void doStartService() throws ServiceException {
		final CommerceProfileTools tools = getProfileTools();
		if (null == tools) {
			throw new ServiceException("Profile tools should be set");
		} else {
			final CommercePropertyManager pm = (CommercePropertyManager)tools.getPropertyManager();
			mCreditCardPropertyName = pm.getCreditCardPropertyName();
			mCreditCardItemDescriptorBillingAddressPropertyName = pm
					.getCreditCardItemDescriptorBillingAddressPropertyName();
			mBillingAddressPropertyName = pm.getBillingAddressPropertyName();
		}
	}
	
	/**
	 * Gets the transaction manager.
	 *
	 * @return the transaction manager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}
	
	/**
	 * Sets the transaction manager.
	 *
	 * @param pTransactionManager the new transaction manager
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
	
	/**
	 * Gets the profile tools.
	 *
	 * @return the profile tools
	 */
	public CommerceProfileTools getProfileTools() {
		return mProfileTools;
	}
	
	/**
	 * Sets the profile tools.
	 *
	 * @param pProfileTools the new profile tools
	 */
	public void setProfileTools(CommerceProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	
}
