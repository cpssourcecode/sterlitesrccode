package vsg.userprofiling.creditcard;

import java.util.Map;

import atg.commerce.CommerceException;
import atg.multisite.SiteContextException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

/**
 * The ICreditCardManager provides interface for create, retrieve, update and
 * delete operations with profile credit cards.
 * 
 * @author Kate Koshman
 */
public interface ICreditCardManager {
	
	/**
	 * Gets the unique credit card nickname.
	 *
	 * @param pCreditCard the credit card
	 * @param pProfile the profile
	 * @param pNewNickname the new nickname proposal
	 * @return the unique credit card nickname
	 */
	String getUniqueCreditCardNickname(Object pCreditCard, RepositoryItem pProfile, String pNewNickname);
	
	/**
	 * Creates the profile credit card.
	 *
	 * @param pProfile the profile
	 * @param pNewCard the new card
	 * @param pCardNickname the card nickname
	 * @param pAddress the address
	 * @return the nickname of created credit card
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 * @throws SiteContextException 
	 */
	String createCreditCard(RepositoryItem pProfile, Map<String, Object> pNewCard, String pCardNickname,
			Object pAddress) throws CommerceException, RepositoryException, SiteContextException;
	
	/**
	 * Gets the credit card by nickname.
	 *
	 * @param pCreditCardNickname the credit card nickname
	 * @param pProfile the profile
	 * @return the credit card repository item
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	RepositoryItem getCreditCardByNickname(String pCreditCardNickname, RepositoryItem pProfile)
			throws CommerceException, RepositoryException;
	
	/**
	 * Update profile credit card.
	 *
	 * @param pCardToUpdate the card to update
	 * @param pProfile the profile
	 * @param pUpdatedCard the updated card
	 * @param pNewCCNickname the new cc nickname
	 * @param pBillingAddress the billing address
	 * @param pBillingAddressClassName the billing address class name
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 * @throws SiteContextException 
	 */
	void updateProfileCreditCard(RepositoryItem pCardToUpdate, RepositoryItem pProfile, Map pUpdatedCard,
			String pNewCCNickname, Map pBillingAddress, String pBillingAddressClassName) throws CommerceException,
			RepositoryException, SiteContextException;
	
	/**
	 * Update address for profile credit card.
	 *
	 * @param pCreditCard the credit card
	 * @param pBillingAddress the billing address
	 * @param pBillingAddressClassName the billing address class name
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 */
	void updateAddressForProfileCreditCard(RepositoryItem pCreditCard, Map pBillingAddress,
			String pBillingAddressClassName) throws CommerceException, RepositoryException;
	
	/**
	 * Delete profile credit card.
	 *
	 * @param pProfile the profile
	 * @param pNickname the nickname of credit card
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 * @throws SiteContextException 
	 */
	void deleteCreditCard(Profile pProfile, String pNickname) throws CommerceException, RepositoryException, SiteContextException;
	
	/**
	 * Gets the credit card properties for display.
	 *
	 * @param pCreditCard the credit card
	 * @return the map with credit card properties for display
	 * @throws CommerceException the commerce exception
	 * @throws RepositoryException the repository exception
	 * @throws SiteContextException 
	 */
	Map<String, Object> getCreditCardPropertiesForDisplay(RepositoryItem pCreditCard) throws CommerceException,
			RepositoryException, SiteContextException;
}
