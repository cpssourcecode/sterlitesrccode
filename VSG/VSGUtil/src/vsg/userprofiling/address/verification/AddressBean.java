package vsg.userprofiling.address.verification;

import java.beans.IntrospectionException;

import vsg.userprofiling.address.VSGAddressTools;

/**
 * AddressBean.
 *
 * @author Kate Koshman
 */
public class AddressBean {
	
	/** The Company name. */
	private String mCompanyName;
	
	/** The First name. */
	private String mFirstName;
	
	/** The Last name. */
	private String mLastName;
	
	/** The Country. */
	private String mCountry;
	
	/** The State. */
	private String mState;
	
	/** The County. */
	private String mCounty;
	
	/** The City. */
	private String mCity;
	
	/** The Address1. */
	private String mAddress1;
	
	/** The Address2. */
	private String mAddress2;
	
	/** The Address3. */
	private String mAddress3;
	
	/** The Postal code. */
	private String mPostalCode;
	
	/**
	 * Instantiates a new address bean.
	 */
	public AddressBean() {
	}
	
	/**
	 * Instantiates a new address bean based on pAddressToCopy.
	 *
	 * @param pAddressToCopy the address to copy
	 */
	public AddressBean(AddressBean pAddressToCopy) {
		this();
		mCompanyName = pAddressToCopy.getCompanyName();
		mFirstName = pAddressToCopy.getFirstName();
		mLastName = pAddressToCopy.getLastName();
		mCountry = pAddressToCopy.getCountry();
		mState = pAddressToCopy.getState();
		mCounty = pAddressToCopy.getCounty();
		mCity = pAddressToCopy.getCity();
		mAddress1 = pAddressToCopy.getAddress1();
		mAddress2 = pAddressToCopy.getAddress2();
		mAddress3 = pAddressToCopy.getAddress3();
		mPostalCode = pAddressToCopy.getPostalCode();
	}
	
	/**
	 * String presentation of AddressBean
	 * 
	 * @return string presentation of AddressBean
	 */
	@Override
	public String toString() {
		return "AddressBean [companyName=" + mCompanyName + ", firstName=" + mFirstName + ", lastName=" + mLastName
				+ ", country=" + mCountry + ", state=" + mState + ", county=" + mCounty + ", city=" + mCity
				+ ", address1=" + mAddress1 + ", address2=" + mAddress2 + ", address3=" + mAddress3
				+ ", postalCode=" + mPostalCode + "]";
	}
	


	/**
	 * Hash code.
	 *
	 * @return the int
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mAddress1 == null) ? 0 : mAddress1.hashCode());
		result = prime * result + ((mAddress2 == null) ? 0 : mAddress2.hashCode());
		result = prime * result + ((mAddress3 == null) ? 0 : mAddress3.hashCode());
		result = prime * result + ((mCity == null) ? 0 : mCity.hashCode());
		result = prime * result + ((mCompanyName == null) ? 0 : mCompanyName.hashCode());
		result = prime * result + ((mCountry == null) ? 0 : mCountry.hashCode());
		result = prime * result + ((mCounty == null) ? 0 : mCounty.hashCode());
		result = prime * result + ((mFirstName == null) ? 0 : mFirstName.hashCode());
		result = prime * result + ((mLastName == null) ? 0 : mLastName.hashCode());
		result = prime * result + ((mPostalCode == null) ? 0 : mPostalCode.hashCode());
		result = prime * result + ((mState == null) ? 0 : mState.hashCode());
		return result;
	}

	/**
	 * Checks whether this AddressBean equals <code>pOtherAddressBean</code>
	 * 
	 * @param pOtherAddressBean other address bean
	 * @return true, if AddressBean equals <code>pOtherAddressBean</code>
	 */
	@Override
	public boolean equals(Object pOtherAddressBean) {
		boolean result;
		if (this == pOtherAddressBean) {
			result = true;
		} else if (pOtherAddressBean == null) {
			result = false;
		} else if (getClass() != pOtherAddressBean.getClass()) {
			result = false;
		} else {
			try {
				result = VSGAddressTools.compareObjects(this, pOtherAddressBean, null);
			} catch (IntrospectionException e) {
				result = false;
			}
		}
		return result;
	}

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public String getCompanyName() {
		return mCompanyName;
	}
	
	/**
	 * Sets the company name.
	 *
	 * @param pCompany the new company name
	 */
	public void setCompanyName(final String pCompany) {
		mCompanyName = pCompany;
	}
	
	/**
	 * Gets the first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return mFirstName;
	}
	
	/**
	 * Sets the first name.
	 *
	 * @param pFirstName the new first name
	 */
	public void setFirstName(final String pFirstName) {
		mFirstName = pFirstName;
	}
	
	/**
	 * Gets the last name.
	 *
	 * @return the last name
	 */
	public String getLastName() {
		return mLastName;
	}
	
	/**
	 * Sets the last name.
	 *
	 * @param pLastName the new last name
	 */
	public void setLastName(final String pLastName) {
		mLastName = pLastName;
	}
	
	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return mCountry;
	}
	
	/**
	 * Sets the country.
	 *
	 * @param pCountry the new country
	 */
	public void setCountry(final String pCountry) {
		mCountry = pCountry;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return mState;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param pState the new state
	 */
	public void setState(final String pState) {
		mState = pState;
	}
	
	/**
	 * Gets the county.
	 *
	 * @return the county
	 */
	public String getCounty() {
		return mCounty;
	}
	
	/**
	 * Sets the county.
	 *
	 * @param pCounty the new county
	 */
	public void setCounty(final String pCounty) {
		mCounty = pCounty;
	}
	
	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return mCity;
	}
	
	/**
	 * Sets the city.
	 *
	 * @param pCity the new city
	 */
	public void setCity(final String pCity) {
		mCity = pCity;
	}
	
	/**
	 * Gets the address1.
	 *
	 * @return the address1
	 */
	public String getAddress1() {
		return mAddress1;
	}
	
	/**
	 * Sets the address1.
	 *
	 * @param pAddress1 the new address1
	 */
	public void setAddress1(final String pAddress1) {
		mAddress1 = pAddress1;
	}
	
	/**
	 * Gets the address2.
	 *
	 * @return the address2
	 */
	public String getAddress2() {
		return mAddress2;
	}
	
	/**
	 * Sets the address2.
	 *
	 * @param pAddress2 the new address2
	 */
	public void setAddress2(final String pAddress2) {
		mAddress2 = pAddress2;
	}
	
	/**
	 * Gets the address3.
	 *
	 * @return the address3
	 */
	public String getAddress3() {
		return mAddress3;
	}
	
	/**
	 * Sets the address3.
	 *
	 * @param pAddress3 the new address3
	 */
	public void setAddress3(final String pAddress3) {
		mAddress3 = pAddress3;
	}
	
	/**
	 * Gets the postal code.
	 *
	 * @return the postal code
	 */
	public String getPostalCode() {
		return mPostalCode;
	}
	
	/**
	 * Sets the postal code.
	 *
	 * @param pPostalCode the new postal code
	 */
	public void setPostalCode(final String pPostalCode) {
		mPostalCode = pPostalCode;
	}
	
}
