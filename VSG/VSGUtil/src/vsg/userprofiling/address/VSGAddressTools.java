package vsg.userprofiling.address;

import java.beans.IntrospectionException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import vsg.constants.VSGConstants;
import vsg.core.util.DataUtils;
import vsg.core.util.VSGContactInfo;
import vsg.messages.MessagesRepositoryConstants;
import vsg.userprofiling.address.verification.AddressBean;
import atg.adapter.gsa.GSARepository;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.json.JSONObject;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.ServletUtil;
import atg.userprofiling.address.AddressTools;

/**
 * The Class VSGAddressTools.
 */
public class VSGAddressTools extends AddressTools {
	private static final String QUERY_JDE_ADDRESS = "jdeAddressNumber = ?0";
	private static final String QUERY_CI_ID = "id = ?0";
	private static final String CONTACT_INFO = "contactInfo";
	
	/** The message repository. */
	private static Repository mProfileRepository = (Repository) ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/ProfileAdapterRepository");
	
	/**
	 * Gets the address from dictionary.
	 * 
	 * @param pDictionary the dictionary
	 * @return the address from dictionary
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static Address getAddressFromDictionary(final Dictionary pDictionary) throws IntrospectionException {
		return getAddressFromDictionary(pDictionary, null);
	}

	/**
	 * Gets the address from dictionary.
	 *
	 * @param pDictionary the dictionary
	 * @param pExcluded the excluded
	 * @return the address from dictionary
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static Address getAddressFromDictionary(final Dictionary pDictionary, final Set<String> pExcluded)
			throws IntrospectionException {
		final Address result = new VSGContactInfo();

		if (null != pDictionary) {
			copyObject(pDictionary, result, pExcluded);
		}

		return result;
	}

	/**
	 * Gets the address from map.
	 *
	 * @param pMap the map
	 * @return the address from map
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static Address getAddressFromMap(final Map pMap) throws IntrospectionException {
		return getAddressFromMap(pMap, null);
	}

	/**
	 * Gets the address from map.
	 *
	 * @param pMap the map
	 * @param pExcluded the excluded
	 * @return the address from map
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static Address getAddressFromMap(final Map pMap, final Set<String> pExcluded)
			throws IntrospectionException {
		final Address result = new VSGContactInfo();

		if (null != pMap) {
			copyObject(pMap, result, pExcluded);
		}

		return result;
	}

	/**
	 * Gets the address bean from map.
	 * 
	 * @param pAddress the address
	 * @return the address bean from dictionary
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static AddressBean getAddressBeanFromMap(final Map pAddress) throws IntrospectionException {
		return getAddressBeanFromMap(pAddress, null);
	}

	/**
	 * Gets the address bean from map.
	 *
	 * @param pAddress the address
	 * @param pExcluded the excluded
	 * @return the address bean from map
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static AddressBean getAddressBeanFromMap(final Map pAddress, final Set<String> pExcluded)
			throws IntrospectionException {
		final AddressBean result = new AddressBean();

		if (null != pAddress) {
			copyObject(pAddress, result, null);
		}

		return result;
	}

	/**
	 * Gets the address bean from dictionary.
	 *
	 * @param pAddress the address
	 * @return the address bean from dictionary
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static AddressBean getAddressBeanFromDictionary(final Dictionary pAddress) throws IntrospectionException {
		final AddressBean result = new AddressBean();

		if (null != pAddress) {
			copyObject(pAddress, result, null);
		}

		return result;
	}

	/**
	 * Gets the address bean from dictionary.
	 *
	 * @param pAddress the address
	 * @param pKeyPrefix the key prefix
	 * @return the address bean from dictionary
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static AddressBean getAddressBeanFromDictionary(final Dictionary pAddress, final String pKeyPrefix)
			throws IntrospectionException {
		if (StringUtils.isBlank(pKeyPrefix)) {
			return getAddressBeanFromDictionary(pAddress);
		} else {
			final AddressBean bean = new AddressBean();
			bean.setAddress1((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.ADDRESS1)));
			bean.setAddress2((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.ADDRESS2)));
			bean.setAddress3((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.ADDRESS3)));
			bean.setCity((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.CITY)));
			bean.setCompanyName((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.COMPANY_NAME)));
			bean.setCounty((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.COUNTY)));
			bean.setCountry((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.COUNTRY)));
			bean.setFirstName((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.FIRST_NAME)));
			bean.setLastName((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.LAST_NAME)));
			bean.setPostalCode((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.POSTAL_CODE)));
			bean.setState((String) pAddress.get(DataUtils.camelConcat(pKeyPrefix, VSGConstants.STATE)));
			return bean;
		}
	}

	/**
	 * Gets the repository address from dictionary.
	 * 
	 * @param pDictionary the dictionary
	 * @param pProfileAdapter the profile adapter
	 * @return the repository address from dictionary
	 * @throws IntrospectionException the introspection exception
	 * @throws RepositoryException the repository exception
	 */
	@SuppressWarnings("rawtypes")
	public static RepositoryItem getRepositoryAddressFromDictionary(final Dictionary pDictionary,
			final Repository pProfileAdapter) throws IntrospectionException, RepositoryException {
		RepositoryItem result = null;

		if (null != pDictionary && null != pProfileAdapter) {
			final GSARepository profileAdapter = (GSARepository) pProfileAdapter;
			result = profileAdapter.createItem(VSGConstants.CONTACT_INFO);
			Set<String> excl = new HashSet<String>();
			excl.add(VSGConstants.ID_CAPS);
			copyObject(pDictionary, result, excl);
			profileAdapter.addItem((MutableRepositoryItem) result);
		}

		return result;
	}

	/**
	 * Gets the jSON from address bean.
	 * 
	 * @param pAddressBean the address bean
	 * @return the jSON from address bean
	 * @throws IntrospectionException the introspection exception
	 */
	public static JSONObject getJSONFromAddressBean(final AddressBean pAddressBean) throws IntrospectionException {
		JSONObject result = null;
		if (pAddressBean != null) {
			final Map<String, Object> addressMap = new HashMap<String, Object>();
			copyObject(pAddressBean, addressMap, null);
			result = new JSONObject(addressMap);
		}
		return result;
	}

	/**
	 * Adds dictionary values to map
	 * 
	 * @param pDictionary dictionary
	 * @param pMap map
	 * @param <K> key object type
	 * @param <V> value object type
	 * @return map result
	 */
	public static <K, V> Map<K, V> addDictionaryToMap(final Dictionary<K, V> pDictionary, final Map<K, V> pMap) {
		for (final Enumeration<K> keys = pDictionary.keys(); keys.hasMoreElements();) {
			final K key = keys.nextElement();
			pMap.put(key, pDictionary.get(key));
		}
		return pMap;
	}

	/**
	 * Creates the address map from specified object with address.
	 *
	 * @param pAddress the address
	 * @return the map with address properties
	 * @throws IntrospectionException the introspection exception
	 */
	@SuppressWarnings("rawtypes")
	public static Map createAddressMap(Object pAddress) throws IntrospectionException {
		Map addressMap = new HashMap();
		copyObject(pAddress, addressMap, null);
		return addressMap;
	}

	/**
	 * Checks is address from repositry equal to address object
	 *
	 * @param pAddress the address
	 * @param pAddressItem the repository address (contactInfo item)
	 * @return is addresses equal
	 */
	public static boolean isAddressEqual(Address pAddress, RepositoryItem pAddressItem) {

		final String address1 = convertNullToEmpty((String) pAddressItem.getPropertyValue(VSGConstants.ADDRESS1));
		final String address2 = convertNullToEmpty((String) pAddressItem.getPropertyValue(VSGConstants.ADDRESS2));
		final String address3 = convertNullToEmpty((String) pAddressItem.getPropertyValue(VSGConstants.ADDRESS3));
		final String country = convertNullToEmpty((String) pAddressItem.getPropertyValue(VSGConstants.COUNTRY));
		final String city = convertNullToEmpty((String) pAddressItem.getPropertyValue(VSGConstants.CITY));
		final String state = convertNullToEmpty((String) pAddressItem.getPropertyValue(VSGConstants.STATE));
		final String postalCode = convertNullToEmpty((String) pAddressItem.getPropertyValue(VSGConstants.POSTAL_CODE));

		return convertNullToEmpty(pAddress.getAddress1()).equalsIgnoreCase(address1)
				&& convertNullToEmpty(pAddress.getAddress2()).equalsIgnoreCase(address2)
				&& convertNullToEmpty(pAddress.getAddress3()).equalsIgnoreCase(address3)
				&& convertNullToEmpty(pAddress.getCountry()).equalsIgnoreCase(country)
				&& convertNullToEmpty(pAddress.getCity()).equalsIgnoreCase(city)
				&& convertNullToEmpty(pAddress.getState()).equalsIgnoreCase(state)
				&& convertNullToEmpty(pAddress.getPostalCode()).equalsIgnoreCase(postalCode);
	}

	/**
	 * Checks string value. If value is null returns empty string value, else default value
	 *
	 * @param value
	 * @return value
	 */
	private static String convertNullToEmpty(final String value) {
		return value == null ? "" : value;
	}

	/*private String getUniqueAddressName(Map<String, RepositoryItem> pAllAddresses, String pAddressName) {

		String result = null;
		boolean uniqueNicknameFound = false;
		while(!uniqueNicknameFound) {
			if (pAllAddresses.get(pAddressName) != null) {
				pAddressName = mProfileTools.generateUniqueNickname(pAddressName);
			} else {
				result = pAddressName;
				uniqueNicknameFound = true;
			}
		}
		return result;
	}*/

	/**
	 *
	 * @param pRepositoryAddress the mutable repository address (contactInfo item)
	 * @param pAddress the address object
	 * @param pValues the map of additional values
	 * @return the updated repository address
	 * @throws RepositoryException
	 */
	public static MutableRepositoryItem setRepositoryAddressProperties(final MutableRepositoryItem pRepositoryAddress,
					final Address pAddress, final Map<String, String> pValues) throws RepositoryException {
		MutableRepositoryItem result;
		if (pAddress == null) {
			result = pRepositoryAddress;
		} else {
//			pRepositoryAddress.setPropertyValue(VSGConstants.ADDRESS1, pAddress.getAddress1());
//			pRepositoryAddress.setPropertyValue(VSGConstants.ADDRESS2, pAddress.getAddress2());
//			pRepositoryAddress.setPropertyValue(VSGConstants.ADDRESS3, pAddress.getAddress3());
//			pRepositoryAddress.setPropertyValue(VSGConstants.COUNTRY, pAddress.getCountry());
//			pRepositoryAddress.setPropertyValue(VSGConstants.CITY, pAddress.getCity());
//			pRepositoryAddress.setPropertyValue(VSGConstants.STATE, pAddress.getState());
//			pRepositoryAddress.setPropertyValue(VSGConstants.POSTAL_CODE, pAddress.getPostalCode());
			
			try {
				copyObject(pAddress, pRepositoryAddress, null);
			} catch (IntrospectionException e) {
				throw new RepositoryException(e);
			}
			for (Map.Entry<String, String> e : pValues.entrySet()) {
				String propertyName = e.getKey();
				String propertyValue = e.getValue();

				if (StringUtils.isNotBlank(propertyValue)) {
					pRepositoryAddress.setPropertyValue(propertyName, propertyValue);
				}
			}

			result = pRepositoryAddress;
		}
		return result;
	}

	public static RepositoryItem getShippingNameByShipTo(String pShipTo) {
		RepositoryItem result = null;
		try {
			RepositoryView view = getProfileRepository().getView(CONTACT_INFO);
			RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_JDE_ADDRESS);
			RepositoryItem[] items = statement.executeQuery(view, new Object[] { pShipTo });
			if (items != null && items.length > 0) {
				result = items[0];
			} else {
				statement = RqlStatement.parseRqlStatement(QUERY_CI_ID);
				items = statement.executeQuery(view, new Object[] { pShipTo });
				if (items != null && items.length > 0) {
					result = items[0];
				}
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	private static Repository getProfileRepository() {
		return mProfileRepository;
	}

	private void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}

}
