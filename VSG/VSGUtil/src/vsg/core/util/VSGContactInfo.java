package vsg.core.util;

import static vsg.core.util.DataUtils.areEquals;

import java.util.Date;
import java.util.Objects;

//import vsg.constants.VSGRepositoriesConstants;
import atg.core.util.ContactInfo;

/**
 * ContactInfo extension
 * 
 * @author osednev
 * 
 */
public class VSGContactInfo extends ContactInfo {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -4921109322398211474L;
	
	/** phoneNumberAlt property */
	private String mPhoneNumberAlt = null;
	
	/** phoneNumberAltExt property */
	private String mPhoneNumberAltExt = null;
	
	/** phoneNumberExt property */
	private String mPhoneNumberExt = null;
	
	/** courtesy property */
	private String mCourtesy = null;
	
	/** creation date property */
	private Date mCreationDate = null;
	
	/** verification status property */
//	private String mVerificationStatus = VSGRepositoriesConstants.VERIFICATION_UNKNOWN;
	
	/** The Avs codes property. */
	private String mAvsCodes = null;
	
	/** The Address type property. */
	private String mAddressType;
	
	/** That Accessorial Code property */
	private String mAccessorialCode;
	
	/** The limited access flag */
	private Boolean mLimitedAccess;
	
	/** The pre notification flag */
	private Boolean mPreNotification;
	
	/**
	 * Gets the phone number alt.
	 *
	 * @return the phone number alt
	 */
	public String getPhoneNumberAlt() {
		return mPhoneNumberAlt;
	}
	
	/**
	 * Sets the phone number alt.
	 *
	 * @param mPhoneNumberAlt the new phone number alt
	 */
	public void setPhoneNumberAlt(String mPhoneNumberAlt) {
		this.mPhoneNumberAlt = mPhoneNumberAlt;
	}
	
	/**
	 * Gets the phone number alt ext.
	 *
	 * @return the phone number alt ext
	 */
	public String getPhoneNumberAltExt() {
		return mPhoneNumberAltExt;
	}
	
	/**
	 * Sets the phone number alt ext.
	 *
	 * @param mPhoneNumberAltExt the new phone number alt ext
	 */
	public void setPhoneNumberAltExt(String mPhoneNumberAltExt) {
		this.mPhoneNumberAltExt = mPhoneNumberAltExt;
	}
	
	/**
	 * Gets the phone number ext.
	 *
	 * @return the phone number ext
	 */
	public String getPhoneNumberExt() {
		return mPhoneNumberExt;
	}
	
	/**
	 * Sets the phone number ext.
	 *
	 * @param mPhoneNumberExt the new phone number ext
	 */
	public void setPhoneNumberExt(String mPhoneNumberExt) {
		this.mPhoneNumberExt = mPhoneNumberExt;
	}
	
	/**
	 * Gets the courtesy.
	 *
	 * @return the courtesy
	 */
	public String getCourtesy() {
		return mCourtesy;
	}
	
	/**
	 * Sets the courtesy.
	 *
	 * @param mCourtesy the new courtesy
	 */
	public void setCourtesy(String mCourtesy) {
		this.mCourtesy = mCourtesy;
	}
	
	/**
	 * Gets the creation date.
	 *
	 * @return the creation date
	 */
	public Date getCreationDate() {
		return mCreationDate;
	}
	
	/**
	 * Sets the creation date.
	 *
	 * @param pCreationDate the new creation date
	 */
	public void setCreationDate(Date pCreationDate) {
		mCreationDate = pCreationDate;
	}
	
	/**
	 * Gets the verification status.
	 *
	 * @return the verification status
	 */
//	public String getVerificationStatus() {
//		return mVerificationStatus;
//	}
	
	/**
	 * Sets the verification status.
	 *
	 * @param pVerificationStatus the new verification status
	 */
//	public void setVerificationStatus(String pVerificationStatus) {
//		mVerificationStatus = pVerificationStatus;
//	}
	
	/**
	 * @return the avsCodes
	 */
	public String getAvsCodes() {
		return mAvsCodes;
	}
	
	/**
	 * @param pAvsCodes the avsCodes to set
	 */
	public void setAvsCodes(String pAvsCodes) {
		mAvsCodes = pAvsCodes;
	}
	
	/**
	 * @return the addressType
	 */
	public String getAddressType() {
		return mAddressType;
	}
	
	/**
	 * @param pAddressType the addressType to set
	 */
	public void setAddressType(String pAddressType) {
		mAddressType = pAddressType;
	}
	
	public String getAccessorialCode() {
		return mAccessorialCode;
	}

	public void setAccessorialCode(String pAccessorialCode) {
		mAccessorialCode = pAccessorialCode;
	}

	public Boolean getLimitedAccess() {
		return mLimitedAccess;
	}

	public void setLimitedAccess(Boolean pLimitedAccess) {
		mLimitedAccess = pLimitedAccess;
	}

	public Boolean getPreNotification() {
		return mPreNotification;
	}

	public void setPreNotification(Boolean pPreNotification) {
		mPreNotification = pPreNotification;
	}

	/**
	 * Equals.
	 *
	 * @param pAddress the address
	 * @return true, if equals
	 * @see atg.core.util.Address#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object pAddress) {
		boolean equals;
		if (super.equals(pAddress) && pAddress instanceof VSGContactInfo) {
			VSGContactInfo other = (VSGContactInfo)pAddress;
			equals = areEquals(getJobTitle(), other.getJobTitle()) && areEquals(getCourtesy(), other.getCourtesy())
					&& areEquals(getCompanyName(), other.getCompanyName())
					&& areEquals(getPhoneNumber(), other.getPhoneNumber())
					&& areEquals(getFaxNumber(), other.getFaxNumber()) && areEquals(getEmail(), other.getEmail())
					&& areEquals(getPhoneNumberAlt(), other.getPhoneNumberAlt())
					&& areEquals(getPhoneNumberExt(), other.getPhoneNumberExt())
					&& areEquals(getPhoneNumberAltExt(), other.getPhoneNumberAltExt());
		} else {
			equals = false;
		}
		return equals;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), mPhoneNumberAlt, mPhoneNumberAltExt, mPhoneNumberExt, mCourtesy);
	}
}