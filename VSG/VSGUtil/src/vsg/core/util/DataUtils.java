package vsg.core.util;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.core.util.StringUtils;


/**
 * The Class DataUtils.
 * 
 * @author Vasili
 */
public final class DataUtils {

	/**
	 * Instantiates a new records utils.
	 */
	private DataUtils() {
	}

	/**
	 * Gets the string value.
	 *
	 * @param pDictionary the dictionary
	 * @param pName the name
	 * @return the string value
	 */
	@SuppressWarnings("rawtypes")
	public static String getStringValue(final Dictionary pDictionary, final String pName) {
		String result;
		try {
			result = (String) pDictionary.get(pName);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	/**
	 * Gets the boolean value.
	 *
	 * @param pEditValues the edit values
	 * @param pName the name
	 * @return the boolean value
	 */
	@SuppressWarnings("rawtypes")
	public static Boolean getBoolean(final Dictionary pEditValues, final String pName) {
		Boolean result;
		try {
			result = Boolean.valueOf((String) pEditValues.get(pName));
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	/**
	 * Gets the boolean value.
	 *
	 * @param pEditValues the edit values
	 * @param pName the name
	 * @return the boolean value
	 */
	public static Boolean getBoolean(final Map<Object, Object> pEditValues, final String pName) {
		Boolean result;
		try {
			result = Boolean.valueOf((String) pEditValues.get(pName));
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	/**
	 * Map params.
	 * 
	 * @param pParams
	 *            the params
	 * @return the map
	 */
	public static Map<String, String> mapParams(String pParams) {
		Map<String, String> result;
		if (null == pParams) {
			result = null;
		} else {
			String[] params = pParams.split("&");
			if (null == params || 0 == params.length) {
				result = null;
			} else {
				result = new TreeMap<String, String>();
				for (String param : params) {
					String[] properties = param.split("=");
					if (null != properties && 1 < properties.length) {
						result.put(properties[0], properties[1]);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Compare two long values.
	 * 
	 * @param l1
	 *            the long 1
	 * @param l2
	 *            the long 2
	 * @return the -1, 0 or 1
	 * @see java.util.Comparator
	 */
	public static int compare(long l1, long l2) {
		return l1 < l2 ? -1 : (l1 == l2 ? 0 : 1);
	}

	/**
	 * Get parameterized message found exact by key.
	 * 
	 * @param pMessage
	 *            the message
	 * @param pParams
	 *            parameters map. It is expected that parameters map will have
	 *            Map (PARAM_key1=value1; PARAM_key2=value2, ... )
	 * @return message.
	 */
	public static String paramsMessage(final String pMessage, final Map<String, ?> pParams) {

		String result;
		if (null == pParams || pParams.isEmpty()) {
			result = pMessage;
		} else {
			List<String> searchList = new ArrayList<String>(pParams.size());
			List<String> replacementList = new ArrayList<String>(pParams.size());
			for (java.util.Map.Entry<String, ?> entry : pParams.entrySet()) {
				String key = entry.getKey();
				if (null != key && key.startsWith("PARAM_")) {
					searchList.add(getSearchParam(key));
					replacementList.add(getReplacementValue(entry.getValue()));
				}
			}
			result = (0 == searchList.size()) ? pMessage : org.apache.commons.lang3.StringUtils.replaceEach(pMessage,
					searchList.toArray(new String[searchList.size()]),
					replacementList.toArray(new String[searchList.size()]));
		}
		return result;
	}

	/**
	 * Gets the search param.
	 * 
	 * @param pKey
	 *            the key
	 * @return the search param
	 */
	private static String getSearchParam(String pKey) {
		return new StringBuilder("%").append(pKey).append("%").toString();
	}

	/**
	 * Gets the replacement value.
	 * 
	 * @param pValue
	 *            the value
	 * @return the replacement value
	 */
	private static String getReplacementValue(Object pValue) {
		return (null == pValue) ? "" : pValue.toString();
	}

	/**
	 * Escape message.
	 * 
	 * @param pMessage
	 *            the message
	 * @return escape
	 */
	public static String escapeMessage(String pMessage) {
		return org.apache.commons.lang3.StringEscapeUtils.escapeXml(pMessage);
	}
	
	/**
	 * Convert dictionary to map.
	 *
	 * @param <K> the key type
	 * @param <V> the value type
	 * @param pDictonary the dictonary
	 * @return the map
	 * @throws IntrospectionException the introspection exception
	 */
	public static <K,V> Map<K, V> convertDictionaryToMap(Dictionary<K, V> pDictonary) throws IntrospectionException {
		Map<K, V> map = new HashMap<K, V>();
		if (pDictonary != null && !pDictonary.isEmpty()) {
			for (Enumeration<K> keys = pDictonary.keys(); keys.hasMoreElements();) {
				K key = keys.nextElement();
				map.put(key, pDictonary.get(key));
			}
		}
		return map;
	}
	
	/**
	 * Trim dictionary.
	 * 
	 * @param pValue
	 *            the value
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void trimDictionary(Dictionary pValue) {
		if (null != pValue && !pValue.isEmpty()) {
			Object key;
			Object value;
			Enumeration keys = pValue.keys();
			while (keys.hasMoreElements()) {
				key = keys.nextElement();
				value = pValue.get(key);
				if (value instanceof String) {
					String newValue = ((String) value).trim();
					if (!newValue.equals(value)) {
						pValue.put(key, newValue);
					}
				} else {
					if (value instanceof Dictionary) {
						trimDictionary((Dictionary) value);
					}
				}
			}
		}
	}
	
	/**
	 * Trim map.
	 *
	 * @param pValue the value
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void trimMap(Map pValue) {
		if (null != pValue && !pValue.isEmpty()) {
			for (Object entryObj : pValue.entrySet()) {
				Map.Entry entry = (Entry)entryObj;
				Object key = entry.getKey();
				Object value = entry.getValue();
				if (value instanceof String) {
					String newValue = ((String)value).trim();
					if (!newValue.equals(value)) {
						pValue.put(key, newValue);
					}
				} else if (value instanceof Dictionary) {
					trimDictionary((Dictionary)value);
				} else if (value instanceof Map) {
					trimMap((Map)value);
				}
			}
		}
	}
	
	/**
	 * Concatenate two parts to produce result in camel case notation. If part isn't empty, it should be presented
	 * in camel case notation.
	 *
	 * @param pFirstPart the first part
	 * @param pSecondPart the second part
	 * @return the concatenated string in camel case notation
	 */
	public static String camelConcat(String pFirstPart, String pSecondPart) {
		if (StringUtils.isBlank(pFirstPart)) {
			return pSecondPart;
		} else if (StringUtils.isBlank(pSecondPart)) {
			return pFirstPart;
		} else {
			StringBuilder sb = new StringBuilder(pFirstPart);
			sb.append(Character.toUpperCase(pSecondPart.charAt(0))).append(pSecondPart.substring(1));
			return sb.toString();
		}
	}

	/**
	 * Replace search list with replacement list
	 *
	 * @param text input string
	 * @param searchList search list array
	 * @param replacementList replacement list array
	 * @return string after replacing
	 */
	public static String replaceEach(String text, String[] searchList, String[] replacementList) {
		return org.apache.commons.lang3.StringUtils.replaceEach(text, searchList, replacementList);
	}
	
	/**
	 * Checks whether objects are equals.
	 *
	 * @param pFirstObject the first object
	 * @param pSecondObject the second object
	 * @return true, if both object are null or <code>pFirstObject.equals(pSecondObject)</code>
	 */
	public static boolean areEquals(Object pFirstObject, Object pSecondObject) {
		if (pFirstObject == null) {
			return pSecondObject == null;
		} else {
			return pFirstObject.equals(pSecondObject);
		}
	}


	/**
	 * processTime
	 *
	 * @param pTime long
	 * @return String
	 */
	public static String processTime(long pTime) {
		StringBuilder timeValue = new StringBuilder();
		if (pTime > 60000) {
			// scale is in the mintues
			timeValue.append(pTime / 60000).append(" minute").append((pTime / 60000 > 1) ? "s" : "");
			if (pTime % 60000 > 0) {
				timeValue.append(" ").append((pTime % 60000) / 1000).append(" second").append(((pTime % 60000) / 1000 > 1) ? "s" : "");
			}
		} else {
			// scale is in the seconds
			timeValue.append(pTime / 1000).append(" second").append((pTime / 1000 > 1) ? "s" : "");
		}
		return timeValue.toString();
	}

}
