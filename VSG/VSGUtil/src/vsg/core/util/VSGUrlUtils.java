package vsg.core.util;

import vsg.constants.VSGConstants;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;

/**
 * The VSGUrlUtils provides help methods for work with URL.
 * 
 * @author Kate Koshman
 */
public class VSGUrlUtils {
	
	/**
	 * Builds the absolute url from relative. Request is used as base of url.
	 *
	 * @param pRequest the request
	 * @param pRelativeUri the relative uri
	 * @return the absolute url
	 */
	public static String buildAbsoluteUrl(DynamoHttpServletRequest pRequest, String pRelativeUri) {
		StringBuffer url = new StringBuffer();
		String scheme = pRequest.getScheme();
		int port = pRequest.getServerPort();
		url.append(scheme);
		url.append("://");
		url.append(pRequest.getServerName());
		if (((scheme.equals("http")) && (port != 80)) || ((scheme.equals("https")) && (port != 443))) {
			url.append(':');
			url.append(pRequest.getServerPort());
		}
		url.append(pRelativeUri);
		return url.toString();
	}
	
	/**
	 * Add parameter to URL.
	 *
	 * @param pUrl URL to update
	 * @param pParamName name of parameter to add
	 * @param pParamValue parameter value
	 * @return updated URL
	 */
	public static String addParameter(String pUrl, String pParamName, String pParamValue) {
		String result = null;
		if (pUrl != null && !StringUtils.isBlank(pParamName) && pParamValue != null) {
			String paramFirstPart = pParamName + VSGConstants.EQUALS_MARK;
			int indexOfQuestionMark = pUrl.indexOf(VSGConstants.QUESTION_MARK);
			String delimiter;
			if (indexOfQuestionMark == -1) {
				// "/brand/sharlot.html" - no parameters, add '?' for first
				delimiter = Character.toString(VSGConstants.QUESTION_MARK);
			} else if (indexOfQuestionMark == pUrl.length() - 1) {
				// "/brand/sharlot.html?" - just append parameter without
				// delimiter.
				delimiter = "";
			} else {
				// "/brand/sharlot.html?view=listing" - add one more parameter
				// with '&' delimiter
				delimiter = Character.toString(VSGConstants.AMPERSAND);
			}
			result = new StringBuffer(pUrl).append(delimiter).append(paramFirstPart).append(pParamValue).toString();
		}
		return result;
	}

	/**
	 * Remove parameter from URL.
	 *
	 * @param pUrl URL to update
	 * @param pParamName parameter name to remove
	 * @return updated URL
	 */
	public static String removeParam(String pUrl, String pParamName) {
		String result = pUrl;
		if (pUrl != null && !StringUtils.isBlank(pParamName)) {
			String paramPart = pParamName + VSGConstants.EQUALS_MARK;
			if (pUrl.contains(paramPart)) {
				int start = pUrl.indexOf(paramPart);
				int end = pUrl.indexOf(VSGConstants.AMPERSAND, start);
				if (end < 0) {
					end = pUrl.length();
					/*
					 * boolean paramPlacedAfterQuestionMark = start != 0 &&
					 * VSGConstants.QUESTION_MARK == pUrl.charAt(start - 1); if
					 * (paramPlacedAfterQuestionMark) { start--; }
					 */
				} else {
					end++;
				}
				result = new StringBuffer(pUrl).replace(start, end, "").toString();
			}
		}
		return result;
	}

	/**
	 * Gets the param.
	 *
	 * @param pUrl the url
	 * @param pParamName the param name
	 * @return the param
	 */
	public static String getParam(String pUrl, String pParamName) {
		String result = null;
		if (pUrl != null && !StringUtils.isBlank(pParamName)) {
			String paramPart = pParamName + VSGConstants.EQUALS_MARK;
			if (pUrl.contains(paramPart)) {
				int start = pUrl.indexOf(paramPart) + paramPart.length();
				int end = pUrl.indexOf(VSGConstants.AMPERSAND, start) - 1;
				if (end < 0) {
					end = pUrl.length();
				} else {
					end++;
				}
				result = new StringBuffer(pUrl).substring(start, end).toString();
			}
		}
		return result;
	}

	/**
	 * Update or add parameter value to URL.
	 *
	 * @param pUrl URL to update
	 * @param pParamName updated parameter name
	 * @param pParamValue new parameter value
	 * @return updated URL
	 */
	public static String updateParam(String pUrl, String pParamName, String pParamValue) {
		String result = null;
		if (pUrl != null && !StringUtils.isBlank(pParamName) && pParamValue != null) {
			String paramFirstPart = pParamName + VSGConstants.EQUALS_MARK;
			if (pUrl.contains(paramFirstPart)) {
				int start = pUrl.indexOf(paramFirstPart);
				int end = pUrl.indexOf(VSGConstants.AMPERSAND, start);
				if (end < 0) {
					end = pUrl.length();
				}
				result = new StringBuffer(pUrl).replace(start, end, paramFirstPart + pParamValue).toString();
			} else {
				result = addParameter(pUrl, pParamName, pParamValue);
			}
		}
		return result;
	}
}
