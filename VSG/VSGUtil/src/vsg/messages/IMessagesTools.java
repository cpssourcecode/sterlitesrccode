package vsg.messages;

import java.util.Locale;

/**
 * The Interface IMessagesTools. Retrieves user message
 * 
 * @author Andy Porter
 * 
 */
public interface IMessagesTools {

	/**
	 * Gets the message text by message key
	 * 
	 * @param pLookUpKey the look up key
	 * @param pParams the params
	 * @return the message
	 */
	String getMessage(MessageKey pLookUpKey, Object... pParams);

	/**
	 * Gets the message text by message key and locale
	 * 
	 * @param pLookUpKey the look up key
	 * @param pLocale the locale
	 * @param pParams the params
	 * @return the message
	 */
	String getMessage(MessageKey pLookUpKey, Locale pLocale, Object... pParams);

}
