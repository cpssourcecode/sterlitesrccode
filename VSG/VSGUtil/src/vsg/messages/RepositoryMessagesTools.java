package vsg.messages;

import java.text.MessageFormat;
import java.util.Locale;

import javax.transaction.TransactionManager;

import atg.dtm.TransactionDemarcation;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

/**
 * The Class RepositoryMessagesTools implements IMessagesTools.
 * RepositoryMessagesTools gets user messages from message repository.
 * 
 * @author Andy Porter
 */
public class RepositoryMessagesTools extends GenericService implements IMessagesTools {

	/** The Constant EMPTY. */
	private static final String EMPTY = "";

	/** The Constant QUERY_KEY_MESSAGE. */
//	private static final String QUERY_KEY_MESSAGE = "msgKey = ?0";
	private static final String QUERY_KEY_MESSAGE = "keyValue = ?0";

	/** The message repository. */
	private Repository mMessagesRepository;

	/** The transaction manager. */
	private TransactionManager mTransactionManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see vsg.messages.IMessagesTools#getMessage(vsg.messages.MessageKey,
	 * java.lang.Object[])
	 */
	@Override
	public String getMessage(MessageKey pMessageKey, Object... pParams) {
		return getMessage(pMessageKey, null, pParams);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see vsg.messages.IMessagesTools#getMessage(vsg.messages.MessageKey,
	 * java.util.Locale, java.lang.Object[])
	 */
	@Override
	public String getMessage(MessageKey pMessageKey, Locale pLocale, Object... pParams) {
		String userMessage = EMPTY;
		if (pMessageKey != null && !EMPTY.equals(pMessageKey.getValue())) {
			if (pParams != null && pParams.length > 0) {
				userMessage = getFormattedMessageText(pMessageKey, pParams);
			} else {
				userMessage = getMessageTextInTransaction(pMessageKey);
			}
		}
		return userMessage;
	}

	/**
	 * Gets the formatted message text.
	 * 
	 * @param pMessageKey the message key
	 * @param pParams the params
	 * @return the formatted message text
	 */
	private String getFormattedMessageText(MessageKey pMessageKey, Object[] pParams) {
		String messageText = getMessageTextInTransaction(pMessageKey);
		String formattedMessageText = MessageFormat.format(messageText, pParams);
		return formattedMessageText;
	}

	/**
	 * Gets the message text from the repository by message key.
	 * 
	 * @param pMessageKey the look up key
	 * @return the key message text
	 */
	private String getMessageTextInTransaction(MessageKey pMessageKey) {
		String messageKeyValue = pMessageKey.getValue();
		String result = messageKeyValue;
		TransactionDemarcation td = null;
		try {
			td = new TransactionDemarcation();
			td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
			result = getMessageText(messageKeyValue);
		} catch (Exception e) {
			vlogError(e, "Repository error occurs.");
		} finally {
			if (td != null) {
				try {
					td.end(false);
				} catch (Exception e) {
					if(isLoggingError()){
						logError(e);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Gets the message text.
	 * 
	 * @param pMessageKey the message key value
	 * @return the message text
	 * @throws RepositoryException the repository exception
	 */
	private String getMessageText(String pMessageKey) throws RepositoryException {
		String result = pMessageKey;
		RepositoryView view = getMessagesRepository().getView(MessagesRepositoryConstants.ITEM_DESCR_USER_MESSAGE);
		RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_KEY_MESSAGE); 
		vlogDebug("Message Repository query: msgKey = {0}", pMessageKey);
		RepositoryItem[] items = statement.executeQuery(view, new Object[] { pMessageKey });
		if (items != null && items.length > 0) {
			result = (String) items[0].getPropertyValue(MessagesRepositoryConstants.PROPERTY_MSG_TEXT);
			vlogDebug("Message Repository query result: {0}", result);
		} else {
			vlogDebug("Message Repository query result empty.");
		}
		return result;
	}
	
	/**
	 * @param pMessageKey
	 * @return message value
	 * @throws RepositoryException
	 */
	public String getMessageValue(String pMessageKey) throws RepositoryException {
        String result = pMessageKey;
        RepositoryView view = getMessagesRepository().getView(MessagesRepositoryConstants.ITEM_DESCR_USER_MESSAGE);
        RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_KEY_MESSAGE); 
        vlogDebug("Message Repository query: msgKey = {0}", pMessageKey);
        RepositoryItem[] items = statement.executeQuery(view, new Object[] { pMessageKey });
        if (items != null && items.length > 0) {
            result = (String) items[0].getPropertyValue(MessagesRepositoryConstants.PROPERTY_MESSAGE);
            vlogDebug("Message Repository query result: {0}", result);
        } else {
            vlogDebug("Message Repository query result empty.");
        }
        return result;
    }
	/**
	 * Gets the message repository.
	 * 
	 * @return the message repository
	 */
	public Repository getMessagesRepository() {
		return mMessagesRepository;
	}

	/**
	 * Sets the message repository.
	 * 
	 * @param pMessagesRepository the new message repository
	 */
	public void setMessagesRepository(Repository pMessagesRepository) {
		mMessagesRepository = pMessagesRepository;
	}

	/**
	 * Sets the transaction manager.
	 * 
	 * @param pTransactionManager the new transaction manager
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	/**
	 * Get TransactionManager.
	 * 
	 * @return TransactionManager.
	 */
	protected TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

}
