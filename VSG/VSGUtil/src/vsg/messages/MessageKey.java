package vsg.messages;

/**
 * This class contains all user messages keys.
 *
 * @author Andy Porter
 */
public class MessageKey {

	/** The Constant UP_ERR_GENERAL_SYSTEM. */
	public static final MessageKey GEN_ERR_SYSTEM = new MessageKey("Error100");

	/** The Constant GEN_WARN_SESSION_EXP. */
	public static final MessageKey GEN_WARN_SESSION_EXP = new MessageKey("Warning101");

	/** The Constant UP_ERR_LOGIN_EMPTY. */
	public static final MessageKey UP_ERR_LOGIN_EMPTY = new MessageKey("Error301");

	/** The Constant UP_ERR_LOGIN_INVALID. */
	public static final MessageKey UP_ERR_LOGIN_INVALID = new MessageKey("Error302");

	/** The Constant UP_ERR_PSWD_EMPTY. */
	public static final MessageKey UP_ERR_PSWD_EMPTY = new MessageKey("Error303");

	/** The Constant UP_ERR_PSWD_INVALID. */
	public static final MessageKey UP_ERR_PSWD_INVALID = new MessageKey("Error304");

	/** The Constant UP_ERR_FIRST_NAME_EMPTY. */
	public static final MessageKey UP_ERR_FIRST_NAME_EMPTY = new MessageKey("Error305");

	/** The Constant UP_ERR_FIRST_NAME_INVALID. */
	public static final MessageKey UP_ERR_FIRST_NAME_INVALID = new MessageKey("Error306");

	/** The Constant UP_ERR_LAST_NAME_EMPTY. */
	public static final MessageKey UP_ERR_LAST_NAME_EMPTY = new MessageKey("Error307");

	/** The Constant UP_ERR_LAST_NAME_INVALID. */
	public static final MessageKey UP_ERR_LAST_NAME_INVALID = new MessageKey("Error308");

	/** The Constant UP_ERR_ADDRESS_EMPTY. */
	public static final MessageKey UP_ERR_ADDRESS_EMPTY = new MessageKey("Error309");

	/** The Constant UP_ERR_ADDRESS_INVALID. */
	public static final MessageKey UP_ERR_ADDRESS_INVALID = new MessageKey("Error310");

	/** The Constant UP_ERR_CITY_EMPTY. */
	public static final MessageKey UP_ERR_CITY_EMPTY = new MessageKey("Error311");

	/** The Constant UP_ERR_CITY_INVALID. */
	public static final MessageKey UP_ERR_CITY_INVALID = new MessageKey("Error312");

	/** The Constant UP_ERR_STATE_EMPTY. */
	public static final MessageKey UP_ERR_STATE_EMPTY = new MessageKey("Error313");

	/** The Constant UP_ERR_STATE_INVALID. */
	public static final MessageKey UP_ERR_STATE_INVALID = new MessageKey("Error314");

	/** The Constant UP_ERR_ZIP_CODE_EMPTY. */
	public static final MessageKey UP_ERR_ZIP_CODE_EMPTY = new MessageKey("Error315");

	/** The Constant UP_ERR_ZIP_CODE_INVALID. */
	public static final MessageKey UP_ERR_ZIP_CODE_INVALID = new MessageKey("Error316");

	/** The Constant UP_ERR_ACCOUNT_NUMBER_EMPTY. */
	public static final MessageKey UP_ERR_ACCOUNT_NUMBER_EMPTY = new MessageKey("Error317");

	/** The Constant UP_ERR_ACCOUNT_NUMBER_INVALID. */
	public static final MessageKey UP_ERR_ACCOUNT_NUMBER_INVALID = new MessageKey("Error318");

	/** The Constant UP_ERR_ENTITY_NAME_EMPTY. */
	public static final MessageKey UP_ERR_ENTITY_NAME_EMPTY = new MessageKey("Error319");

	/** The Constant UP_ERR_ENTITY_NAME_INVALID. */
	public static final MessageKey UP_ERR_ENTITY_NAME_INVALID = new MessageKey("Error320");

	/** The Constant UP_ERR_PHONE_NUMBER_EMPTY. */
	public static final MessageKey UP_ERR_PHONE_NUMBER_EMPTY = new MessageKey("Error321");

	/** The Constant UP_ERR_PHONE_NUMBER_INVALID. */
	public static final MessageKey UP_ERR_PHONE_NUMBER_INVALID = new MessageKey("Error322");

	/** The Constant UP_ERR_EMAIL_PSWD_EMPTY. */
	public static final MessageKey UP_ERR_EMAIL_EMPTY = new MessageKey("Error323");

	/** The Constant UP_ERR_EMAIL_PSWD_INVALID. */
	public static final MessageKey UP_ERR_EMAIL_INVALID = new MessageKey("Error324");

	/** The Constant UP_ERR_CONFIRM_EMAIL_DOES_NOT_MATCH. */
	public static final MessageKey UP_ERR_CONFIRM_EMAIL_DOES_NOT_MATCH = new MessageKey("Error325");

	/** The Constant UP_ERR_CONFIRM_PSWD_DOES_NOT_MATCH. */
	public static final MessageKey UP_ERR_CONFIRM_PSWD_DOES_NOT_MATCH = new MessageKey("Error326");

	/** The Constant UP_ERR_ACCOUNT_WITH_THIS_EMAIL_ALREADY_EXISTS. */
	public static final MessageKey UP_ERR_ACCOUNT_WITH_THIS_EMAIL_ALREADY_EXISTS = new MessageKey("Error327");

	/** The Constant UP_ERR_NAME_EMPTY. */
	public static final MessageKey UP_ERR_NAME_EMPTY = new MessageKey("Error328");

	/** The Constant UP_ERR_NAME_INVALID. */
	public static final MessageKey UP_ERR_NAME_INVALID = new MessageKey("Error329");

	/** The Constant UP_ERR_NAME_INVALID. */
	public static final MessageKey UP_ERR_NOTE_INVALID = new MessageKey("Error330");

	/** The Constant UP_ERR_EMAIL_RECEPIENTS_EMPTY. */
	public static final MessageKey UP_ERR_EMAIL_RECEPIENTS_EMPTY = new MessageKey("Error331");

	/** The Constant UP_ERR_EMAIL_RECEPIENTS_INVALID. */
	public static final MessageKey UP_ERR_EMAIL_RECEPIENTS_INVALID = new MessageKey("Error332");

	/** The Constant UP_ERR_ACCOUNT_USED_BY_CSR. */
	public static final MessageKey UP_ERR_ACCOUNT_USED_BY_CSR = new MessageKey("ErrorAccountUsed");

	// Forgot Password error
	/** The Constant UP_ERROR_FP_NON_EXIST_EMAIL. */
	public static final MessageKey UP_ERROR_FP_NON_EXIST_EMAIL = new MessageKey("Error339");

	/** The Constant UP_ERROR_USER_NOT_EXIST. */
	public static final MessageKey UP_ERROR_USER_NOT_EXIST = new MessageKey("Error350");

	/** The Constant UP_ERROR_PSWD_ALREADY_CHANGED. */
	public static final MessageKey UP_ERROR_PSWD_ALREADY_CHANGED = new MessageKey("Error351");

	/** The Constant UP_ERROR_PROCESSING_FORGOT_PSWD. */
	public static final MessageKey UP_ERROR_PROCESSING_FORGOT_PSWD = new MessageKey("Error352");

	/** The Constant UP_ERROR_RESET_LINK_EXPIRE. */
	public static final MessageKey UP_ERROR_RESET_LINK_EXPIRE = new MessageKey("Error356");

	/** The Constant UP_ERROR_RESET_LINK_INVALID. */
	public static final MessageKey UP_ERROR_RESET_LINK_INVALID = new MessageKey("Error357");

	/** The Constant UP_ERR_JOB_TITLE_EMPTY. */
	public static final MessageKey UP_ERR_JOB_TITLE_EMPTY = new MessageKey("Error358");

	/** The Constant UP_ERR_JOB_TITLE_INVALID. */
	public static final MessageKey UP_ERR_JOB_TITLE_INVALID = new MessageKey("Error359");

	/** The Constant GIFTLIST_ERR_NAME_EMPTY. */
	public static final MessageKey GIFTLIST_ERR_NAME_EMPTY = new MessageKey("Error360");

	/** The Constant GIFTLIST_ERR_NAME_INVALID. */
	public static final MessageKey GIFTLIST_ERR_NAME_INVALID = new MessageKey("Error361");

	/** The Constant GIFTLIST_NAME_EXISTS. */
	public static final MessageKey GIFTLIST_NAME_EXISTS_ERR = new MessageKey("Error362");

	/** The Constant GIFTLIST_DESCRIPTION_INVALID. */
	public static final MessageKey GIFTLIST_DESCRIPTION_INVALID_ERR = new MessageKey("Error363");

	/** The Constant EMAIL_MESSAGE_ERROR_EMPTY. */
	public static final MessageKey EMAIL_MESSAGE_ERROR_EMPTY = new MessageKey("Error370");

	/** The Constant GIFTLIST_UNDEFINED_ERR. */
	public static final MessageKey GIFTLIST_UNDEFINED_ERR = new MessageKey("Error364");

	/** The Constant GIFTLIST_QTY_ERROR. */
	public static final MessageKey GIFTLIST_QTY_ERROR = new MessageKey("Error365");

	/** The Constant GIFTLIST_ADDING_ERROR. */
	public static final MessageKey GIFTLIST_ADDING_ERROR = new MessageKey("Error366");

	/** The Constant GIFTLIST_DELETED. */
	public static final MessageKey GIFTLIST_DELETED_SUCCESS = new MessageKey("Success311");

	/** The Constant GIFTLIST_ITEM_ADDED_SUCCESS. */
	public static final MessageKey GIFTLIST_ITEM_ADDED_SUCCESS = new MessageKey("Success312");

	/** The Constant UP_SUCCESS_FORGOT_PSWD. */
	public static final MessageKey UP_SUCCESS_FORGOT_PSWD = new MessageKey("Success300");

	/** The Constant UP_SUCCESS_CHANGE_PSWD. */
	public static final MessageKey UP_SUCCESS_CHANGE_PSWD = new MessageKey("Success301");

	/** The Constant UP_SUCCESS_UPDATE_PROFILE. */
	public static final MessageKey UP_SUCCESS_UPDATE_PROFILE = new MessageKey("Success302");

	/** The Constant UP_SUCCESS_SHARE_CART. */
	public static final MessageKey UP_SUCCESS_SHARE_CART = new MessageKey("Success303");

	/** The Constant UP_SUCCESS_SHARE_INGREDIENT_LIST. */
	public static final MessageKey UP_SUCCESS_SHARE_INGREDIENT_LIST = new MessageKey("Success304");

	/** The Constant UP_SUCCESS_SHARE_INGREDIENT_LIST. */
	public static final MessageKey UP_SUCCESS_QUOTE_REQUEST = new MessageKey("Success305");

	/** The Constant UP_SUCCESS_SHARE_FAVORITES_LIST. */
	public static final MessageKey UP_SUCCESS_SHARE_FAVORITES_LIST = new MessageKey("Success306");

	/** The Constant GIFTLIST_CREATED. */
	public static final MessageKey GIFTLIST_CREATED_SUCCESS = new MessageKey("Success310");

	/** The Constant UP_SUCCESS_CONTACT_US. */
	public static final MessageKey UP_SUCCESS_CONTACT_US = new MessageKey("Success313");

	/** The Constant UP_ROLE_SWITCH_WARNING. */
	public static final MessageKey UP_ROLE_SWITCH_WARNING = new MessageKey("Warning300");

	/** The Constant PDP_MIN_QTY_ERROR. */
	public static final MessageKey PDP_MIN_QTY_ERROR = new MessageKey("Error401");

	/** The Constant PDP_MAX_QTY_ERROR. */
	public static final MessageKey PDP_MAX_QTY_ERROR = new MessageKey("Error402");

	/** The Constant PDP_QTY_INVALID. */
	public static final MessageKey PDP_QTY_INVALID = new MessageKey("Error403");

	/** The Constant PDP_OUT_OF_STOCK. */
	public static final MessageKey PDP_OUT_OF_STOCK = new MessageKey("Error420");

	/** The Constant PDP_LOW_STOCK. */
	public static final MessageKey PDP_LOW_STOCK = new MessageKey("Error421");

	/** The Constant PDP_ADDED_TO_CART. */
	public static final MessageKey PDP_SUCCESS_ADDED_TO_CART = new MessageKey("Success401");

	/** The Constant USER_SUBSCRIBED_INTER_ERROR. */
	public static final MessageKey USER_SUBSCRIBED_INTER_ERROR = new MessageKey("Error502");

	/** The Constant USER_SUBSCRIBED. */
	public static final MessageKey USER_SUBSCRIBED = new MessageKey("Success503");

	/** The Constant USER_ALREADY_SUBSCRIBED. */
	public static final MessageKey USER_ALREADY_SUBSCRIBED = new MessageKey("Error501");

	/** The Constant GIFTLIST_NAME_EXISTS. */
	public static final MessageKey GIFTLIST_NAME_EXISTS = new MessageKey("Error362");

	/** The Constant GIFTLIST_DESCRIPTION_INVALID. */
	public static final MessageKey GIFTLIST_DESCRIPTION_INVALID = new MessageKey("Error363");

	/** The Constant GIFTLIST_DELETED. */
	public static final MessageKey GIFTLIST_DELETED = new MessageKey("Success311");

	/**  When user not input subject. */
	public static final MessageKey SUBJECT_ERROR_EMPTY = new MessageKey("Error408");

	/**  Subject is too large. */
	public static final MessageKey SUBJECT_TOO_LARGE = new MessageKey("Error409");

	/**  user not wrote cover letter. */
	public static final MessageKey MESSAGE_ERROR_EMPTY = new MessageKey("Error410");

	/**  user input too large message. */
	public static final MessageKey MESSAGE_TOO_LARGE = new MessageKey("Error407");

	/**  user not attached resume file. */
	public static final MessageKey RESUME_FILE_NOT_ATTACHED = new MessageKey("Error404");

	/** resume file not at .doc, .docx or .pdf */
	public static final MessageKey RESUME_FILE_INCORRECT_FORMAT = new MessageKey("Error405");

	/** region name is empty */
	public static final MessageKey QUOTE_REGION_NAME_EMPTY = new MessageKey("Error701");

	/** region name is not valid */
	public static final MessageKey QUOTE_REGION_NAME_NOT_VALID = new MessageKey("Error702");

	/**  resume successful sent. */
	public static final MessageKey RESUME_SUCCESS_SENT = new MessageKey("Success402");

	/** The Constant SC_ITEM_REMOVED. */
	public static final MessageKey SC_ITEM_REMOVED = new MessageKey("Warning601");

	/** The Constant SC_ITEM_QTY_DESCREASED. */
	public static final MessageKey SC_ITEM_QTY_DESCREASED = new MessageKey("Warning602");

	/** The Constant ERR_SC_ITEMS_DONOT_EXIST. */
	public static final MessageKey ERR_SC_ITEMS_DONOT_EXIST = new MessageKey("err_sc_items_donot_exist");

	/** The message key value. */
	private final String mValue;

	/**
	 * Instantiates a new message key.
	 *
	 * @param pValue
	 *            the value
	 */
	public MessageKey(final String pValue) {
		mValue = pValue;
	}

	/**
	 * Gets the message key string value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return mValue;
	}

}
