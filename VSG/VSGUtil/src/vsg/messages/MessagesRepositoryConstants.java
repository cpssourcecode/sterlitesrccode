package vsg.messages;

/**
 * Class contains constants necessary for working with MessagesRepository
 *
 * @author Andy Porter
 */
public interface MessagesRepositoryConstants {
	
	/** The item descriptor userMessage. */
	String ITEM_DESCR_USER_MESSAGE="userMessage";
	String ITEM_DESCR_PRODUCT_SPECIFIC_MESSAGE="productSpecificMessage";

	/** The item property msgText. */
	String PROPERTY_MSG_TEXT="msgText";
	String PROPERTY_MESSAGE="message";

	String ERR_SC_ITEMS_DONOT_EXIST = "err_sc_items_donot_exist";

	String CHECK_AVAILABILITY_ERROR_MESSAGE = "checkAvailabilityErrorMessage";
}
