package vsg.constants;

/**
 * The VSGRepositoriesConstants contains item descriptor names, property names,
 * enumerated property values.
 *
 * @author Kate Koshman
 */
public interface VSGRepositoriesConstants {
	
	// Organization
	public static final String ORG_PRICE_GROUP = "priceGroupId";
	public static final String ORG_SEGMENT_GROUP = "marketSegment";
	
	// Order
	public static final String ORDER = "order";
	public static final String ORDER_STATUS = "state";
	public static final String ORDER_ORGANIZATION_ID = "organizationId";
	public static final String ORDER_ESTIMATED_ZIP = "estimatedZip";
	public static final String ORDER_ESTIMATED_SHIIPING_COST = "estimatedShippingCost";
	public static final String ORDER_ESTIMATED_SHIPPING_DATE = "estimatedShippingDate";
	public static final String ORDER_ESTIMATED_TAX = "estimatedTax";
	public static final String ORDER_RECIPIENT = "recipient";
	public static final String ORDER_CHECKOUT_STEP = "checkoutStep";
	public static final String SHIPPING_ADDRESS_SKIPPED = "shippingAddressSkipped";
	public static final String SHIPPING_RATE = "shippingRate";
	// public static final String ORDER_ESTIMATED_SHIPPING_COST =
	// "estimatedShippingCost";
	// public static final String ORDER_ESTIMATED_DELIVERY_DATE =
	// "estimatedDeliveryDate";
	public static final String CI_CUSTOMIZED_ITEM_DESCR = "customizedCommerceItem";
	public static final String CI_CUSTOMIZATIONS = "customizations";
	public static final String ORDER_PROFILE_ID = "profileId";
	public static final String ORDER_SUBMITTED_DATE = "submittedDate";
	public static final String ORDER_CHARGE_CODES = "chargeCodes";

	// Address
	public static final String ADDR_FIRST_NAME = "firstName";
	public static final String ADDR_LAST_NAME = "lastName";
	public static final String ADDR_COMPANY_NAME = "companyName";
	public static final String ADDR_ADDRESS1 = "address1";
	public static final String ADDR_ADDRESS2 = "address2";
	public static final String ADDR_ADDRESS3 = "address3";
	public static final String ADDR_COUNTRY = "country";
	public static final String ADDR_COUNTY = "county";
	public static final String ADDR_CITY = "city";
	public static final String ADDR_STATE = "state";
	public static final String ADDR_POSTAL_CODE = "postalCode";
	public static final String VERIFICATION_STATUS = "verificationStatus";
	public static final String VERIFICATION_UNKNOWN = "unknown";
	public static final String VERIFICATION_VALID = "valid";
	public static final String VERIFICATION_INVALID = "invalid";
	public static final String VERIFICATION_UNSUPPORTED = "unsupported";

	// Credit card
	public static final String CC_SUBSCRIPTION_ID = "subscriptionId";
	public static final String CC_TYPE_AMERICAN_EXPRESS = "americanExpress";

	// user
	public static final String USER_EMAIL_PROP = "email";
	public static final String USER_ITEM_DESC = "user";
	public static final String USER_DEFAULT_ADDRESS_PROP = "shippingAddress";
	public static final String USER_ALL_ADDRESSES_PROP = "allSecondaryAddresses";
	public static final String USER_ADDRESSES_PROP = "secondaryAddresses";
	public static final String USER_ORGANIZATION_ADDRESSES_PROP = "parentOrganization.secondaryAddresses";
	public static final String USER_ORGANIZATION = "parentOrganization";
	public static final String USER_CREDIT_CARDS = "creditCards";
	public static final String USER_DEFAULT_CARD = "defaultCreditCard";
	public static final String USER_IS_GENERATED_PSWD = "generatedPassword";
	public static final String USER_LAST_PSWD_UPDATE = "lastPasswordUpdate";
	public static final String USER_EXTERNAL_ID = "externalContactId";

	// Gift item
	public static final String GITEM_TYPE = "itemType";
	public static final String GITEM_TYPE_DEFAULT = "default";
	public static final String GITEM_TYPE_CUSTOMIZED = "customized";

	// SKU
	public static final String SKU_SHIPWEIGHT_LBS_PROP = "weight";
	
	// Category
	public static final String CATEGORY_FIXED_PARENT_CATS = "fixedParentCategories";
	public static final String PARENT_CATEGORY = "parentCategory";
	public static final String ENABLE_CUT_SIZES= "enableCutSizes";

	// Product attributes
	public static final String PRODUCT_ATTRIBUTES = "productAttributes";
	public static final String LISTING_ATTRIBUTES = "listingAttributes";
	public static final String COMPARE_ATTRIBUTES = "compareAttributes";
	public static final String SPECIFICATION_ATTRIBUTES = "specificationAttributes";
	public static final String PRODUCT_ATTR_SITE_ID = "siteId";
	public static final String PRODUCT_ATTR_ATTRIBUTES = "attributes";
	
	// Location
	public static final String LOCATION_EXT_ID = "externalLocationId";

	public static final String CUTSIZE_ID = "id";
	public static final String CUTSIZE_QUANTITY = "quantity";
	public static final String CUTSIZE_WIDTH = "width";
	public static final String CUTSIZE_LENGTH = "length";
	
	// Contact Us
	public static final String CONTACT_US_TOPIC = "topic";
	public static final String CONTACT_UD_SUBTOPIC = "subtopic";
	public static final String CONTACT_UD_SUBTOPICS = "subtopics";
	
	// Pricing
	public static final String PRICE_GROUP_ITEM_DESC = "priceGroup";
	public static final String PRICE_GROUP_MODIFIER = "bracketModifier";
	public static final String PRICE_GROUP_BASE = "bracketBase";
	public static final String PRICE_GROUP_DISCOUNT= "discountPercent";
}
