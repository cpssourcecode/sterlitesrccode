package vsg.constants;

/**
 * VSG constants utility class
 * 
 * @author osednev
 * 
 */
public class VSGConstants {

	public static final String SLASH = "/";
	public static final String EMPTY = "";
	public static final String DASH = "-";
	public static final String SPACE = " ";
	public static final String DOT = ".";
	public static final String COMMA = ",";
	public static final String EQUALS_OP = "=";
	public static final char EQUALS_MARK = '=';
	public static final char QUESTION_MARK = '?';
	public static final char AMPERSAND = '&';

	public static final String US = "US";
	public static final String CA = "CA";
	public static final String OTHER = "Other";

	public static final String ID_CAPS = "ID";

	public static final String TRUE = "true";
	public static final String FALSE = "false";

	public static final String USER_TYPE = "userType";
	public static final String LOGIN = "login";
	public static final String GUEST_LOGIN = "guestLogin";
	public static final String NAME = "name";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String EMAIL = "email";
	public static final String CONFIRM_EMAIL = "confirmEmail";
	public static final String CURRENT_EMAIL = "currentEmail";
	public static final String PSWD = "password";
	public static final String CONFIRM_PSWD = "confirmPassword";
	public static final String SECURITY_QUESTION = "securityQuestion";
	public static final String SECURITY_ANSWER = "securityAnswer";
	public static final String SECURITY_QUESTION_ANSWER = "securityQuestionAnswer";
	public static final String IS_SEND_EMAIL = "isSendEmail";
	public static final String PROMO_EMAIL = "promoEmailSignUp";
	public static final String ROLE = "role";
	public static final String COMPANY = "company";
	public static final String DISPLAY_NAME = "displayName";
	public static final String COMPANY_NAME = "companyName";
	public static final String CREATION_DATE = "creationDate";
	public static final String OWNER_ID = "ownerId";
	public static final String EXTERNAL_ID = "externalId";
	public static final String ACCOUNT_ID = "accountId";
	public static final String EXTERNAL_CONTACT_ID = "externalContactId";
	public static final String TITLE = "title";
	public static final String COURTESY_TITLE = "courtesy";
	public static final String JOB_TITLE = "jobTitle";
	public static final String ADDRESS1 = "address1";
	public static final String ADDRESS2 = "address2";
	public static final String ADDRESS3 = "address3";
	public static final String COUNTRY = "country";
	public static final String COUNTY = "county";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String PROVINCE = "province";
	public static final String POSTAL_CODE = "postalCode";
	public static final String PHONE_NUMBER = "phoneNumber";
	public static final String PHONE_NUMBER_EXT = "phoneNumberExt";
	public static final String ALTERNATE_PHONE_NUMBER = "phoneNumberAlt";
	public static final String ALTERNATE_PHONE_NUMBER_EXT = "phoneNumberAltExt";
	public static final String FAX = "faxNumber";
	public static final String FAX_EXT = "faxNumberExt";
	public static final String AVS_CODES = "avsCodes";
	public static final String ADDRESS_TYPE = "addressType";
	public static final String ADDRESS_ACCRL_CODE = "accessorialCode";
	public static final String DEFAULT_ADDRESS_FIELD = "defaultAddress";
	public static final String ORGANIZATION_ADDRESS_FIELD = "organizationAddress";
	public static final String ADDRESS_TYPE_FIELD = "addressBookType";
	public static final String ADDRESS_NICKNAME = "addressNickname";
	public static final String ADDRESS_NOT_SELECTED = "notSelected";
	public static final String ADDRESS_USER = "myAddresses";
	public static final String ADDRESS_ORGANIZATION = "orgAddresses";
	public static final String ADDRESS_LIM = "LIM";
	public static final String ADDRESS_PRE_NOTIFICATION = "MNC";
	public static final String TERMS_AND_CONDITIONS = "termsAndConditions";
	public static final String IGNORE_GENERATED_DATE = "ignoreGeneratedPasswordDate";
	public static final String CATEGORIES = "categories";
	public static final String INDUSTRIES = "industries";
	public static final String CONTRACT_TERMS = "contractTerms";
	public static final String ORDER_PRICE_LIMIT = "orderPriceLimit";

	public static final String NAME_CAPITAL = "Name";
	public static final String FIRST_NAME_CAPITAL = "FirstName";
	public static final String LAST_NAME_CAPITAL = "LastName";
	public static final String EMAIL_CAPITAL = "Email";
	public static final String COMPANY_CAPITAL = "Company";
	public static final String COMPANY_NAME_CAPITAL = "CompanyName";
	public static final String TITLE_CAPITAL = "Title";
	public static final String JOB_TITLE_CAPITAL = "JobTitle";
	public static final String ADDRESS1_CAPITAL = "Address1";
	public static final String ADDRESS2_CAPITAL = "Address2";
	public static final String ADDRESS3_CAPITAL = "Address3";
	public static final String COUNTRY_CAPITAL = "Country";
	public static final String CITY_CAPITAL = "City";
	public static final String STATE_CAPITAL = "State";
	public static final String PROVINCE_CAPITAL = "Province";
	public static final String POSTAL_CODE_CAPITAL = "PostalCode";
	public static final String PHONE_NUMBER_CAPITAL = "PhoneNumber";
	public static final String PHONE_NUMBER_EXT_CAPITAL = "PhoneNumberExt";
	public static final String FAX_CAPITAL = "Fax";
	public static final String FAX_EXT_CAPITAL = "FaxExt";
	public static final String ACCOUNT_NUMBER_CAPITAL = "AccountNumber";
	public static final String MANAGER_CAPITAL = "Manager";
	public static final String CONTACT_NAME_CAPITAL = "ContactName";

	public static final String HOME_ADDRESS = "homeAddress";
	//public static final String ADDRESS_TYPE = "addressType";
	public static final String ADDRESS_TYPE_SHIPPING = "shipping";
	public static final String ADDRESS_TYPE_BILLING = "billing";
	public static final String SECONDARY_ADDRESSES = "secondaryAddresses";
	public static final String SHIPPING_ADDRESSES = "shippingAddresses";
	public static final String CREDIT_APP = "creditApp";
	public static final String CREDIT_LIMIT = "creditLimit";
	public static final String CURRENT_CREDIT_BALANCE = "currentCreditBalance";
	public static final String BUSINESS_NAME = "businessName";
	public static final String LEGAL_BUSINESS_NAME = "legalBusinessName";
	public static final String OWNER_NAME = "ownerName";
	public static final String TRADE_NAME = "tradeName";
	public static final String OWNERSHIP = "ownership";
	public static final String COMPANY_TYPE = "companyType";
	public static final String STOCK_SYMBOL = "stockSymbol";
	public static final String DB_NUM = "dbNum";
	public static final String BUSINESS_PHONE = "businessPhone";
	public static final String BUSINESS_PHONE_EXT = "businessPhoneExt";
	public static final String BUSINESS_FAX = "businessFax";
	public static final String BUSINESS_EMAIL = "businessEmail";
	public static final String CREDIT_LINE = "creditLine";
	public static final String CREDIT_REQUESTED = "creditRequested";
	public static final String ESTIMATED_SALES = "estimatedSales";
	public static final String ESTIMATED_ANNUAL_SALES = "estimatedAnnualSales";
	public static final String MONTHLY_PURCHASE = "monthlyPurchase";
	public static final String DATE_ESTABLISHED = "companyEstablished";
	public static final String NUMBER_EMPLOYEES = "numberEmployees";
	public static final String ACCOUNTANT = "accountant";
	public static final String PAYABLE_CONTACT_NAME = "payableContactName";
	public static final String PAYABLE_EMAIL = "payableEmail";
	public static final String PAYABLE_PHONE_NUMBER = "payablePhoneNumber";
	public static final String PAYABLE_PHONE_NUMBER_EXT = "payablePhoneNumberExt";
	public static final String BANK_REFERENCE_FILE = "bankReferencesFile";
	public static final String BANK_REFERENCE_UPLOAD = "bankReferencesUpload";
	public static final String HOME_ADDRESS_STATES_HIDDEN = "homeAddressStatesHidden";
	public static final String STATE_HIDDEN = "stateHidden";
	public static final String LAST_VSITED_LISTING_PAGE = "lastVisitedListingPage";
	
	public static final String TRADE_REFERENCES_FILE = "tradeReferencesFile";
	public static final String TRADE_REFERENCES_UPLOAD = "tradeReferencesUpload";

	public static final String SUPPLIER_REFERENCE_FILE = "supplierReferenceFile";

	public static final String SESSION_LOCKED = "timeLocked";
	public static final String CAPTCHA = "captcha";
	public static final String GENERATED_PSWD = "generatedPassword";
	public static final String ID = "id";
	public static final String ACCOUNT_NUMBER = "accountNumber";
	public static final String CREDIT_APP_ADDRESSES = "creditAppAddresses";
	public static final String COMPANY_ADDRESS = "company_address";
	public static final String PARTNER_1_CONTACT = "partner_1_contact";
	public static final String PARTNER_2_CONTACT = "partner_2_contact";
	public static final String PARTNER_3_CONTACT = "partner_3_contact";
	public static final String PARTNER_4_CONTACT = "partner_4_contact";
	public static final String ACCOUNTS_PAYABLE_CONTACT = "accounts_payable_contact";
	public static final String BANK_REFERENCE_1 = "bank_reference_1";
	public static final String BANK_REFERENCE_2 = "bank_reference_2";
	public static final String TRADE_REFERENCE_1 = "trade_reference_1";
	public static final String TRADE_REFERENCE_2 = "trade_reference_2";
	public static final String TRADE_REFERENCE_3 = "trade_reference_3";

	public static final String FIRST = "first";
	public static final String LAST = "last";
	public static final String EXT = "ext.";

	public static final String PAYMENT_PREFERENCE = "prefPayment";
	public static final String ROUTING_NUMBER = "routingNum";
	public static final String PO_NUMBER = "poNum";
	public static final String BUYER_NAME = "buyerName";
	public static final String DEFAULT_CARD = "defaultCreditCard";
	public static final String PREFERED_PRODUCT = "preferredProduct";

	public static final String CREATE = "create";
	public static final String UPDATE = "update";
	public static final String DAY = "day";
	public static final String MONTH = "month";
	public static final String YEAR = "year";
	public static final String DATE_OF_BIRTH = "dateOfBirth";
	public static final String PROFILE_ID = "profileId";
	
	public static final String BRONTO_NAME = "brontoName";

	
	// role
	public static final String ROLE_NAME_PROP = "name";
	public static final String ROLE_ID_PROP = "id";

	public static final String PROMOTION = "promotion";

	public static final String VALIDATE_USER = "validateUser";
	public static final String VALIDATE = "validate";

	public static final String COOKIE_ORDER = "PFOrder";

	public static final String CC_TYPE = "creditCardType";
	public static final String CC_NUMBER = "creditCardNumber";
	public static final String CC_EXP_MONTH = "expirationMonth";
	public static final String CC_EXP_YEAR = "expirationYear";

	public static final String CC_CUSTOMER_PROF_NUMBER = "customerProfileNumber";
	public static final String CC_TRANSACTION_ID = "transactionId";

	public static final String NICKNAME = "nickname";
	public static final String NICKNAME_CURRENT = "nicknameCurrent";
	public static final String BILLING_ADDRESS = "billingAddress";
	public static final String SHIPPING_ADDRESS = "shippingAddress";
	public static final String SHIPPING_METHOD = "shippingMethod";
	public static final String PAYMENT_METHOD = "paymentMethod";
	public static final String DEF_CREDIT_CARD = "defaultCreditCard";
	public static final String RETURN_REQUEST = "returnRequest";
	public static final String RECOVER_PSWD_DATE = "recoverPasswordDate";
	public static final String LOCATION_ID = "locationId";
	public static final String DROP_BUTTON = "drop-button";

	public static final String IS_REMEMBER = "isRemember";
	public static final String REMEMBER_ME = "rememberMe";
	public static final String CONTACT_INFO = "contactInfo";
	public static final String ORGANIZATION = "organization";
	public static final String PARENT_ORG = "parentOrganization";
	public static final String CHILD_ORGANIZATIONS = "childOrganizations";
	public static final String RECIPIENT_INFO = "recipientInfo";
	public static final String NICKNAME_REFERENCE_INFO = "address_nickname";
	public static final String BILL_ADDRESS_TYPE = "addressType";
	public static final String DERIVED_BILLING_ADDRESS = "derivedBillingAddress";
	
	

	public static final String ROLES = "roles";
	public static final String ROLE_PRPTY = "role";
	public static final String ROLES_PRPTY = "roles";
	public static final String ROLE_NAME = "roleName";
	public static final String ROLE_ACCOUNT_ADMIN = "accountAdmin";
	public static final String ROLE_BUYER = "buyer";
	public static final String ROLE_SALES = "sales";
	public static final String ROLE_TECH = "tech";
	public static final String REQUESTED_ROLE = "requestedRole";
	public static final String MEMBERS = "members";

	public static final String REFERENCE = "reference";
	public static final String TRADE = "trade";
	public static final String BANK = "bank";
	public static final String PARTNER = "partner";

	public static final String ONE = "1";
	public static final String TWO = "2";
	public static final String THREE = "3";

	public static final String HAS_ORG_KNOW_ID = "hasOrgKnowId";
	public static final String HAS_ORG_KNOW_ID_NO_ADMIN = "hasOrgKnowIdNoAdmin";
	public static final String HAS_ORG_DONT_KNOW_ID = "hasOrgDontKnowId";
	public static final String NO_ORG = "noOrg";
	public static final String REGISTER_TYPE = "registerType";
	public static final String NEED_CREDIT_APP = "needCreditApp";
	public static final String IS_TAX_EXEMPT = "isTaxExempt";
	public static final String TAX_EXEMPT_FILE = "taxExemptFile";
	public static final String TAX_EXEMPT_FILES = "taxExemptFiles";
	public static final String TAX_EXEMPT_FILE_NAMES = "taxExemptFileNames";
	public static final String TAX_EXEMPT = "taxExempt";
	public static final String MULTIPLE_USERS = "multipleUsers";
	public static final String EXISTING_ORGANIZATION = "existingOrganization";
	public static final String ORG_ID = "orgId";
	public static final String ACTION = "action";
	public static final String ALLOW = "allow";

	public static final String USE_BANK_REF_FILE = "useBankRefFile";
	public static final String USE_TRADE_REF_FILE = "useTradeRefFile";
	public static final String BANK_REF_FILE = "bankRefFile";
	public static final String TRADE_REF_FILE = "tradeRefFile";

	public static final String USER_STATUS = "userStatus";
	public static final String APPROVED = "Approved";
	public static final String PENDING_ADMIN_APPROVAL = "Pending Admin Approval";
	public static final String DEACTIVATED = "Deactivated";

	public static final String AUTO_LOGIN = "autoLogin";
	public static final String SECURITY_STATUS = "securityStatus";

	public static final String ORG_STATUS = "orgStatus";
	public static final String PENDING_CSR_APPROVAL = "Pending CSR Approval";

	public static final String SAVE_ADDRESS_FIELD = "saveAddress";
	public static final String SHIPPING_ADDRESS_NICKNAME = "shippingAddressNickname";
	public static final String ADDRESS_SELECTED = "addressSelected";


	// roles
	public static final String USER_ROLE = "roleName";
	public static final String ACCOUNT_ADMIN = "accountAdmin";
	public static final String BUYER = "buyer";
	
	//cc
	public static final String CREDIT_CARD_ID = "creditCardId";
	public static final String CREDIT_CARD = "credit-card";
	public static final String CREDIT_CARDS = "creditCards";
	public static final String ADMIN_USER_ORG_ID = "adminUserOrgId";
	public static final String TEMPORARY_PSWD = "Temp0rary_Pa$$";
	public static final String UPDATE_PROFILE_ID = "updateProfileId";
	public static final String DATA_IMPORT_FILE = "importFile";

	// orders
	public static final String ORDER_STATUS = "state";
	public static final String ORGANIZATION_ID = "organizationId";
	
	// Endeca constants
	public static final String OFFSET_PARAM = "No";
	public static final String RECORDS_PER_PAGE_PARAM = "Nrpp";
	public static final String SORTING_PARAM = "Ns";
	public static final String RECORD_FILTER_PARAM = "Nr";
	public static final String NAVIGATION_PARAM = "N";
	public static final String SECONDARY_SEARCH_PARAM = "Ssp";
	public static final String SINGLE_PRODUCT_REDIRECT_PARAM = "__SINGLE_PRODUCT_REDIRECT__";
	public static final String REDIRECT_PARAM_EMPTY_RESULTS = "__REDIRECT_EMPTY_RESULTS__";
	public static final String INDEX_PRODUCT_URL = "product.seoUrl";
	public static final String INDEX_PRODUCT_ID = "product.repositoryId";

	// Queries constants
	public static final String QUERY_LOOKUP_ACCOUNT = "[ExternalSystemId]='?0' AND [PrimaryBillToPostalCode]='?1'";
	public static final String QUERY_LOOKUP_ACCOUNT_NAME_ZIP = "[AccountName]='?0' AND [PrimaryBillToPostalCode]='?1'";
	public static final String QUERY_LOOKUP_ACCOUNT_PHONE_ZIP = "[MainPhone]='?0' AND [PrimaryBillToPostalCode]='?1'";
	public static final String QUERY_LOOKUP_ACCOUNT_EMAIL_PHONE = "[OwnerEmailAddress]='?0' OR [MainPhone]='?1'";
	public static final String QUERY_LOOKUP_UNIQUE_ACCOUNT = "[AccountName]~='?0' AND [PrimaryBillToCity]~='?1'";
	public static final String QUERY_LOOKUP_UNIQUE_ACCOUNT_BY_ID = "[Id]~='?0'";
	public static final String QUERY_LOOKUP_CONTACT = "[Id]='?0'";
	public static final String QUERY_LOOKUP_CONTACT_BY_EMAIL = "[ContactEmail]='?0'";
	public static final String QUERY_LOOKUP_CONTACT_BY_NAME_EMAIL = "[ContactFirstName]='?0'"
			+ "AND [ContactLastName]='?1' "
			+ "AND [ContactEmail]='?2'";
	public static final String QUERY_WRITE_ACCOUNTS = "externalId IS NULL AND name != \"Root Organization\"";
	public static final String QUERY_LOOKUP_ADDRESS = "[Id]='?0'";
	public static final String QUERY_LOOKUP_ACCOUNT_BY_ID = "[Id]='?0'";
	public static final String QUERY_LOOKUP_ADDRESSES = "[AccountId]='?0'";
	public static final String QUERY_LOOKUP_ACCOUNT_BY_ADDRESS = "[PrimaryBillToCity]~='?0' "
			+ "AND [PrimaryBillToCountry]~='?1' "
			+ "AND [PrimaryBillToPostalCode]~='?2' "
			+ "AND [PrimaryBillToState]~='?3' "
			+ "AND [PrimaryBillToStreetAddress]~='?4'";
	
	// Countries codes
	public final static String CANADA_INTERNAL_CODE = "CA";

	// Credit card sorting
	public static final String PROFILE = "profile";
	public static final String PROFILE_CREDIT_CARDS = "creditCards";
	public static final String DEFAULT_CREDIT_CARD = "defaultCreditCard";
	public static final String FIRST_CREDIT_CARD_ID = "firstCreditCardId";
	public static final String SORTED_PROPERTY_NAME = "sortedPropertyName";
	public static final String SORTED_PROPERTY_TYPE = "sortedPropertyType";
	public static final String SORTED_PROPERTY = "creationDate";
	public static final String COMPARATOR_DATE   = "date";
	public static final String COMPARATOR_STRING = "string";

	// Contact Us
	public static final String TOPIC = "topic";
	public static final String SUBTOPIC = "subtopic";
	public static final String TOPIC_ID = "topicId";
	public static final String SUBTOPIC_ID = "subtopicId";
	public static final String MESSAGE = "message";
	public static final String EMAIL_ATTACHMENTS = "emailAttachments";
	public static final String CONTACT_US = "contactUs";

	public static final String CSR_FLAG = "csrFlag";
	public static final String CSR_IMPERSONATE_DATE = "csrImpersonateDate";
	
	// PayPal
	public static final String PAY_PAL = "payPal";
	public static final String PAY_HANDOF_PAL = "payPalHandoffURL";
	
	// Order
	public static final String ORDER_SPEC_SHIPPING_ERROR = "ORDER_SPEC_SHIPPING_ERROR";
	public static final String ORDER_SPEC_TAX_ERROR = "ORDER_SPEC_TAX_ERROR";
	public static final String SEARCH_PARAMETERS = "Search Parameters";

	public static final String REDIRECT_URL = "redirectUrl";


	// Endeca JSON
	public static final String MAIN_CONTENT = "mainContent";

	public static final String MAP_KEY_VALUE_DELIMITER = "\u00B6";      //      http://unicode-table.com/en/#00B6   ¶
	public static final String MAP_PAIR_DELIMITER = "\u00A7";           //      http://unicode-table.com/en/#00A7   §

}
