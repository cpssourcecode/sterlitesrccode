package vsg.constants;

/**
 * VSG validation constants utility class
 * 
 * @author osednev
 * 
 */
public interface VSGMessageConstants {
	public static final String ACCOUNT_NUMBER = "Account #";
	public static final String FIRST_NAME = "First Name";
	public static final String LAST_NAME = "Last Name";
	public static final String ADDRESS_NAME = "Address Name";
	public static final String COMPANY_NAME = "Company Name";
	public static final String ADDRESS1 = "Street Address";
	public static final String ADDRESS2 = "Street Address 2";
	public static final String ADDRESS3 = "Street Address 3";
	public static final String CITY = "City";
	public static final String COUNTRY = "Country";
	public static final String STATE = "State";
	public static final String POSTAL_CODE = "Zip/Postal Code";
	public static final String PHONE_NUMBER = "Phone Number";
	public static final String ALTERNATE_PHONE_NUMBER = "Alternate Phone Number";
	public static final String FAX = "Fax";
	public static final String COURTESY_TITLE = "Courtesy Title";
	public static final String JOB_TITLE = "Job Title";
	public static final String TERMS_AND_CONDITIONS = "Terms & Conditions";
	public static final String TRADE_NAME = "Trade Name";
	public static final String BUSINESS_NAME = "Business Name";
	public static final String CONTRACT_TERMS = "Contract Terms";
	public static final String ORDER_PRICE_LIMIT = "Price Limit";

	public static final String PARTNER = "Partner";
	public static final String PARTNER_NAME = "Partner Name";
	public static final String PARTNER_TITLE = "Partner Title";

	public static final String BANK = "Bank";
	public static final String BANK_NAME = "Bank Name";
	public static final String BANK_CONTACT_NAME = "Bank Title";

	public static final String REFERENCE = "Trade Reference";
	public static final String REFERENCE_NAME = "Trade Reference Vendor Name";
	public static final String REFERENCE_ACCOUNT_NUMBER = "Trade Reference Account#";

	public static final String CREDIT_APP = "Credit Application";
	public static final String CREDIT_LIMIT = "Credit Limit";
	public static final String CURRENT_CREDIT_BALANCE = "Current Credit Balance";
	public static final String LEGAL_BUSINESS_NAME = "Legal Business Name";
	public static final String OWNER_NAME = "Owner Name";
	public static final String OWNERSHIP = "Ownership";
	public static final String COMPANY_TYPE = "Company Type";
	public static final String STOCK_SYMBOL = "Stock Symbol";
	public static final String DB_NUM = "DB Num";
	public static final String BUSINESS_PHONE = "Business Phone";
	public static final String BUSINESS_PHONE_EXT = "Business Phone Ext";
	public static final String BUSINESS_FAX = "Business Fax";
	public static final String BUSINESS_EMAIL = "Business Email";
	public static final String CREDIT_LINE = "Credit Line";
	public static final String CREDIT_REQUESTED = "Credit Requested";
	public static final String ESTIMATED_SALES = "Estimated Sales";
	public static final String ESTIMATED_ANNUAL_SALES = "Estimated Annual Sales";
	public static final String MONTHLY_PURCHASE = "Monthly Purchase";
	public static final String DATE_ESTABLISHED = "Company Established";
	public static final String NUMBER_EMPLOYEES = "Number Employees";
	public static final String ACCOUNTANT = "Accountant";
	public static final String PAYABLE_CONTACT_NAME = "Payable Contact Name";
	public static final String PAYABLE_EMAIL = "Payable Email";
	public static final String PAYABLE_PHONE_NUMBER = "Payable Phone Number";
	public static final String PAYABLE_PHONE_NUMBER_EXT = "Payable Phone Number Ext";
	public static final String BANK_REFERENCE_FILE = "Bank References File";
	public static final String BANK_REFERENCE_UPLOAD = "Bank References Upload";
	public static final String MONTH = "Month";
	public static final String DAY = "Day";
	public static final String YEAR = "Year";
	public static final String CC_NAME = "Card Name";
	public static final String SECURITY_QUESTION = "Security Question";
	public static final String SECURITY_ANSWER = "Security Answer";

	// Please enter your {0}.
	public static final String E_EMPTY_FIELD_DEFAULT = "E_EMPTY_FIELD_DEFAULT";// [emptyFieldDefault]
	// Please enter a valid {0}.
	public static final String E_INVALID_FIELD_DEFAULT = "E_INVALID_FIELD_DEFAULT";// [invalidFieldDefault]
	// Please enter email address/password.
	public static final String E_EMPTY_EMAIL_PSWD = "E_EMPTY_EMAIL_PASSWORD";// [emptyEmailPassword]
	public static final String E_EMPTY_EMAIL = "E_EMPTY_EMAIL";// [emptyEmail]
	public static final String E_EMPTY_PSWD = "E_EMPTY_PWD";// [emptyPassword]
	// Incorrect email address/password.
	public static final String E_NO_USER = "E_NO_USER";// [invalidEmailPassword]
	public static final String E_BAD_AUTH = "E_BAD_AUTH";// [invalidEmailPassword]2
	public static final String E_BAD_EMAIL = "E_BAD_EMAIL";// [invalidEmail]
	public static final String E_BAD_PSWD = "E_BAD_PWD";// [invalidPassword]
	public static final String E_ATTEMPT = "E_ATTEMPT";
	// The email address and confirm email address entered do not match. Please
	// try again.
	public static final String E_BAD_ACCOUNT_NUM = "E_BAD_ACCOUNT_NUM";// [invalidFieldDefault]1
	public static final String E_EMPTY_ACCOUNT_NUM = "E_EMPTY_ACCOUNT_NUM";// [emptyFieldDefault]1
	public static final String E_EMPTY_ACCOUNT_ZIP = "E_EMPTY_ACCOUNT_ZIP";// [emptyFieldDefault]2
	public static final String E_BAD_ACCOUNT_ZIP = "E_BAD_ACCOUNT_ZIP";// [invalidFieldDefault]2
	public static final String E_NO_ACCOUNT = "E_NO_ACCOUNT";
	public static final String E_NOT_AVAILABLE_EMAIL = "E_NOT_AVAILABLE_EMAIL";
	public static final String E_REGISTER_EMAIL = "E_REGISTER_EMAIL";
	public static final String E_CONFIRM_EMAILS_DO_NOT_MATCH = "E_CONFIRM_EMAILS_DO_NOT_MATCH";// [confirmEmailsDoNotMatch]
	// Your password and confirm password entered do not match. Please try
	// again.
	public static final String E_CONFIRM_PSWD_DO_NOT_MATCH = "E_CONFIRM_PASSWORDS_DO_NOT_MATCH";// [confirmPasswordsDoNotMatch]
	// Please agree to the Terms & Condition by checking the box.
	public static final String E_TERMS_OF_USE = "E_TERMS_OF_USE";// [termsOfUse]
	// The information you entered does not match our records. Please try again.
	public static final String E_ORGANIZATION_NOT_FOUND = "E_ORGANIZATION_NOT_FOUND";// [organizationNotFound]
	// User with specified email address already exists.
	public static final String E_USER_ALREADY_EXISTS = "E_USER_ALREADY_EXISTS";// [userAlreadyExists]
	// Non multi-user organization.
	public static final String E_SINGLE_USER_ORGANIZATION = "E_SINGLE_USER_ORGANIZATION";// [singleUserOrg]
	// Internal error
	public static final String E_INTERNAL_ERROR = "E_INTERNAL_ERROR";// [internalError]
	// Email was not found
	public static final String E_EMAIL_NOT_FOUND = "E_EMAIL_NOT_FOUND";// [emailNotFound]
	// Wrong answer
	public static final String E_WRONG_ANSWER = "E_WRONG_ANSWER";// [wrongAnswer]
	// Bad answer
	public static final String E_BAD_ANSWER = "E_BAD_ANSWER";// [badAnswer]
	// Deactivated account
	public static final String E_DEACTIVATED_ACCOUNT = "E_DEACTIVATED_ACCOUNT";// [deactivatedAccount]
	// Exceeded attempts
	public static final String E_EXCEEDED_ATTEMPTS = "E_EXCEEDED_ATTEMPTS";// [exceededAttempts]
	// Not approved account
	public static final String E_NOT_APPROVED_ACCOUNT = "E_NOT_APPROVED_ACCOUNT";// [notApprovedAccount]
	//This profile was impersonated by CSR user
	public static final String E_CSR_IMPERSONATE_ACCOUNT = "E_CSR_IMPERSONATE_ACCOUNT";// [csrImpersonateAccount]

	// Change pwd link expire
	public static final String E_CHANGE_PSWD_LINK_EXPIRE = "E_CHANGE_PWD_LINK_EXPIRE";// [changePWDLinkExpire]
	// Change pwd link already used
	public static final String E_CHANGE_PSWD_LINK_ALREADY_USED = "E_CHANGE_PWD_LINK_ALREADY_USED";// [changePWDLinkAlreadyUsed]
	// Change pwd email success
	public static final String S_CHANGE_PSWD_EMAIL_SUCCESS = "S_CHANGE_PWD_EMAIL_SUCCESS";// [changePWDEmailSuccess]
	// Change pwd email error
	public static final String E_CHANGE_PSWD_EMAIL_ERROR = "E_CHANGE_PWD_EMAIL_ERROR";// [changePWDEmailError]
	// Change pwd invalid link
	public static final String E_CHANGE_PSWD_LINK_INVALID = "E_CHANGE_PWD_LINK_INVALID";// [changePWDLinkInvalid]
	// Change pwd reset success
	public static final String S_CHANGE_PSWD_RESET_SUCCESS = "S_CHANGE_PWD_RESET_SUCCESS";// [changePWDResetSuccess]
	// Tax exempt empty file
	public static final String E_TAX_EXEMPT_EMPTY_FILE = "E_TAX_EXEMPT_EMPTY_FILE";// [taxExemptEmptyFile]
	// Address is invalid, please confirm you want to continue with entered
	// address.
	public static final String E_CONFIRM_CONTINUE_WITH_INVALID_ADDRESS = "E_CONFIRM_CONTINUE_WITH_INVALID_ADDRESS";// [confirmContinueWithInvalidAddress]
	// We found several addresses similar to the address you entered. Would you
	// like to use one of these?
	public static final String E_SEE_ADDRESS_SUGGESTIONS = "E_SEE_ADDRESS_SUGGESTIONS";//[seeAddressSuggestions]
	// Address verification failed. Please try again later.
	public static final String E_AVS_FAILED = "E_AVS_FAILED";//[avsFailed]
	// Bank empty file
	public static final String E_BANK_EMPTY_FILE = "E_BANK_EMPTY_FILE";//[bankEmptyFile]
	// Trade empty file
	public static final String E_TRADE_EMPTY_FILE = "E_TRADE_EMPTY_FILE";//[tradeEmptyFile]
	// A similar address already added.
	public static final String E_DUPLICATE_ADDRESS = "E_DUPLICATE_ADDRESS";//[duplicateAddress]
	// You have an address with the same address name already.
	public static final String E_DUPLICATE_ADDRESS_NAME = "E_DUPLICATE_ADDRESS_NAME";//[duplicateAddressName]
	// You are trying to update non-existing address. Please refresh the page.
	public static final String E_ADDRESS_DOESNT_EXIST = "E_ADDRESS_DOESNT_EXIST";//[addressDoesntExist]
	// You haven`t rights to manage organization addresses.
	public static final String E_UPDATE_ORG_ADDRESS_ACCESS = "E_UPDATE_ORG_ADDRESS_ACCESS";//[updateOrgAddressAccessViolated]
	// Please select the address type
	public static final String E_SELECT_ADDRESS_TYPE = "E_SELECT_ADDRESS_TYPE";//[selectAddressType]
	// Please enter if your address has limited access
	public static final String E_SELECT_ADDRESS_LIMIT_ACCESS = "E_SELECT_ADDRESS_LIMIT_ACCESS";
	// You don`t belong to any organization, so you cannot manage organization
	// addresses.
	public static final String E_ADDR_UPD_USER_HAS_NO_ORG = "E_ADDR_UPD_USER_HAS_NO_ORG";//[addrUpdateUserHasNoOrg]
	// Some error occurs during address procession. Please try again later.
	public static final String E_ADDRESS_GENERAL_ERROR = "E_ADDRESS_GENERAL_ERROR";//[addressGeneralError]
	// You have too few addresses to delete one.
	public static final String E_TOO_FEW_ADDRESSES_TO_DELETE = "E_TOO_FEW_ADDRESSES_TO_DELETE";//[tooFewAddressesToDelete]
	// You are trying to update foreign address.
	public static final String E_UPDATE_FOREIGN_ADDRESS = "E_UPDATE_FOREIGN_ADDRESS";//[updateForeignAddress]
	public static final String E_CREATE_ADDRESS_SUCCESS = "E_CREATE_ADDRESS_SUCCESS";//[createAddressSuccess]
	public static final String E_UPDATE_ADDRESS_SUCCESS = "E_UPDATE_ADDRESS_SUCCESS";//[updateAddressSuccess]
	public static final String E_DELETE_ADDRESS_SUCCESS = "E_DELETE_ADDRESS_SUCCESS";//[deleteAddressSuccess]
	public static final String E_DEFAULT_ADDRESS_SUCCESS = "E_DEFAULT_ADDRESS_SUCCESS";//[defaultAddressSuccess]
	// Payment type is empty
	public static final String E_CC_PAYMENT_TYPE_EMPTY = "E_CC_PAYMENT_TYPE_EMPTY";//[ccPaymentTypeEmpty]
	// Please enter your Credit Card number.
	public static final String E_CC_NUMBER_EMPTY = "E_CC_NUMBER_EMPTY";//[ccNumberEmpty]
	// Please enter a valid credit card number.
	public static final String E_CC_NUMBER_INVALID = "E_CC_NUMBER_INVALID";//[ccNumberInvalid]
	// Please enter your credit card's expiration date.
	public static final String E_CC_DATE_EMPTY = "E_CC_DATE_EMPTY";//[ccDateEmpty]
	// This card has expired. Please update the expiration date.
	public static final String E_CC_DATE_INVALID = "E_CC_DATE_INVALID";//[ccDateEmpty]
	// Please enter the Cardholder's Name as it appears on the card.
	public static final String E_CC_NAME_INVALID = "E_CC_NAME_INVALID";//[ccNameInvalid]
	// The system could not find credit card. Please try again.
	public static final String E_CC_CARD_NOT_FOUND = "E_CC_CARD_NOT_FOUND";//[ccCardNotFound]
	public static final String S_CC_DEFAULT_SUCCESS = "S_CC_DEFAULT_SUCCESS";//[ccDefaultSuccess]
	public static final String S_CC_DELETE_SUCCESS = "S_CC_DELETE_SUCCESS";//[ccDeleteSuccess]
	public static final String S_CC_UPDATE_SUCCESS = "S_CC_UPDATE_SUCCESS";//[ccUpdateSuccess]
	public static final String S_CC_CREATE_SUCCESS = "S_CC_CREATE_SUCCESS";//[ccCreateSuccess]
	public static final String E_CC_DUPLICATE = "E_CC_DUPLICATE";//[ccDuplicate]
	public static final String E_CC_UPDATE_DUPLICATE = "E_CC_UPDATE_DUPLICATE";//[ccUpdateDuplicate]
	public static final String E_CC_DOESNT_EXIST = "E_CC_DOESNT_EXIST";
	// profile updated
	public static final String S_PROFILE_UPDATED = "S_PROFILE_UPDATED";//[profileUpdated]
	// organization updated
	public static final String S_ORGANZIATION_UPDATED = "S_ORGANZIATION_UPDATED";//[orgUpdated]
	// The Constant EMPTY_USER_ROLE.
	public static final String E_EMPTY_USER_ROLE = "E_EMPTY_USER_ROLE";//[emptyUserRole]
	public static final String S_CREATE_USER_SUCCESS = "S_CREATE_USER_SUCCESS";//[createUserSuccess]
	public static final String S_USER_EMAIL_SUCCESS = "S_USER_EMAIL_SUCCESS";//[usrEmailSuccess]
	public static final String E_USER_EMAIL_ERROR = "E_USER_EMAIL_ERROR";//[usrEmailError]
	public static final String S_UPDATE_USER_SUCCESS = "S_UPDATE_USER_SUCCESS";//[updateUserSuccess]
	public static final String E_FINISH_REG_LINK_INVALID = "E_FINISH_REG_LINK_INVALID";//[finishRegLinkInvalid]
	public static final String E_FINISH_REG_LINK_ALREADY_USED = "E_FINISH_REG_LINK_ALREADY_USED";//[finishRegLinkAlreadyUsed]
	public static final String E_FINISH_REG_LINK_EXPIRE = "E_FINISH_REG_LINK_EXPIRE";//[finishRegLinkExpired]
	public static final String S_DEACTIVATE_USER_SUCCESS = "S_DEACTIVATE_USER_SUCCESS";//[deactivateUserProfileSuccess]
	public static final String S_APPROVE_USER_SUCCESS = "S_APPROVE_USER_SUCCESS";//[approveUserProfileSuccess]
	public static final String S_DECLINE_USER_SUCCESS = "S_DECLINE_USER_SUCCESS";//[declineUserProfileSuccess]
	public static final String E_IMPORT_FILE_MISSING = "E_IMPORT_FILE_MISSING";//[importFileMissing]
	public static final String S_IMPORT_FILE_SUCCESSFULL = "S_IMPORT_FILE_SUCCESSFULL";//[importSuccessfull]
	public static final String E_IMPORT_FILE_WITH_ERRORS = "E_IMPORT_FILE_WITH_ERRORS";//[importFileWithErrors]
	public static final String E_INACTIVE_USER = "E_INACTIVE_USER";// [userAlreadyExistsDeactivated]
	public static final String E_IMPORT_NO_ACCESS = "E_IMPORT_NO_ACCESS";//[importNoAccess]
	public static final String E_IMPORT_NO_ORGANIZATION = "E_IMPORT_NO_ORGANIZATION";//[importNoOrganization]

	// Estimated Shipping
	public static final String E_EMPTY_ZIP = "E_EMPTY_ZIP";
	public static final String E_BAD_ZIP = "E_BAD_ZIP";
	public static final String E_NO_ESTIMATE_SHIPPING = "E_NO_ESTIMATE_SHIPPING";
	public static final String E_NO_ESTIMATE_TAX = "E_NO_ESTIMATE_TAX";

	// Shopping Cart
	public static final String SC_ERROR_REMOVE_ITEM = "SC_ERROR_REMOVE_ITEM";
	public static final String SC_SUCCESS_REMOVE_ITEM = "SC_SUCCESS_REMOVE_ITEM";
	public static final String S_CART_ITEM_UPDATED = "S_CART_ITEM_UPDATED";
	public static final String S_CART_CUSTOMS_UPDATED = "S_CART_CUSTOMS_UPDATED";
	public static final String E_EMPTY_QUANTITY = "E_EMPTY_QUANTITY";
	public static final String E_NEGATIVE_QUANTITY = "E_NEGATIVE_QUANTITY";
	public static final String E_BAD_QUANTITY = "E_BAD_QUANTITY";
	public static final String E_NO_EXPRESS_CHECKOUT = "E_NO_EXPRESS_CHECKOUT";
	public static final String I_DELIVERY_DATE = "I_DELIVERY_DATE";
	public static final String E_REQUEST_QUOTE_NEED = "E_REQUEST_QUOTE_NEED";
	public static final String E_EMPTY_WIDTH = "E_EMPTY_WIDTH";
	public static final String E_BAD_WIDTH = "E_BAD_WIDTH";
	public static final String E_EMPTY_LENDTH = "E_EMPTY_LENDTH";
	public static final String E_BAD_LENDTH = "E_BAD_LENDTH";
	public static final String E_ORDER_UPDATE_ERROR = "E_ORDER_UPDATE_ERROR";
	public static final String E_CONCURRENT_UPDATE = "E_CONCURRENT_UPDATE";
	public static final String E_ITEMS_WAS_REMOVED_FROM_ORDER = "E_ITEMS_WAS_REMOVED_FROM_ORDER";
	public static final String E_SC_NO_ITEMS_SELECTED = "E_SC_NO_ITEMS_SELECTED";
	
	//checkout shipping
	public static final String E_INVALID_ORDER_VERSION = "E_INVALID_ORDER_VERSION";
	public static final String E_CS_SHIPPING_ADDRESS_FAIL = "E_CS_SHIPPING_ADDRESS_FAIL";
	public static final String E_CS_SHIPPING_METHOD_FAIL = "E_CS_SHIPPING_METHOD_FAIL";
	public static final String E_ADDRESS_CREATE_FAIL = "E_ADDRESS_CREATE_FAIL";
	public static final String E_EMPTY_COMPANY_NAME = "E_EMPTY_COMPANY_NAME";
	public static final String E_BAD_COMPANY_NAME = "E_BAD_COMPANY_NAME";
	public static final String E_EMPTY_STREET = "E_EMPTY_STREET";
	public static final String E_BAD_STREET = "E_BAD_STREET";
	public static final String E_EMPTY_CITY = "E_EMPTY_CITY";
	public static final String E_BAD_CITY = "E_BAD_CITY";
	public static final String E_EMPTY_STATE = "E_EMPTY_STATE";
	public static final String E_BAD_STATE = "E_BAD_STATE";
	public static final String E_BAD_ADDRESS_NAME = "E_BAD_ADDRESS_NAME";
	public static final String E_EMPTY_FIRST_NAME = "E_EMPTY_FIRST_NAME";
	public static final String E_BAD_FIRST_NAME = "E_BAD_FIRST_NAME";
	public static final String E_EMPTY_LAST_NAME = "E_EMPTY_LAST_NAME";
	public static final String E_BAD_LAST_NAME = "E_BAD_LAST_NAME";
	public static final String E_EMPTY_PHONE = "E_EMPTY_PHONE";
	public static final String E_BAD_PHONE = "E_BAD_PHONE";
	public static final String E_BAD_TAX_AREA = "E_BAD_TAX_AREA";
	public static final String E_EXIST_ADDRESS = "E_EXIST_ADDRESS";
	public static final String E_NO_ADDRESSES = "E_NO_ADDRESSES";
	public static final String E_NO_STORES = "E_NO_STORES";
	public static final String I_DELIVERY_DATE_SHORT = "I_DELIVERY_DATE_SHORT";
	public static final String E_SHIPPING_INSTRUCTIONS_LENGTH = "E_SHIPPING_INSTRUCTIONS_LENGTH";
	public static final String E_DELIVERY_APPOINTMENT_NOT_SELECTED = "E_DELIVERY_APPOINTMENT_NOT_SELECTED";
	public static final String E_RECIPIENT_NOT_FILLED = "E_RECIPIENT_NOT_FILLED";
	
	// Checkout Billing Credit Card
	public static final String E_INVALID_CC_SUBSCRIPTION_ID = "E_INVALID_CC_SUBSCRIPTION_ID";
	public static final String E_BAD_CVV = "E_BAD_CVV";
	public static final String E_EMPTY_CVV = "E_EMPTY_CVV";

	//order review
	public static final String E_NO_ACCEPT_TERMS = "E_NO_ACCEPT_TERMS";
	public static final String E_NO_BILL_ACCOUNT = "E_NO_BILL_ACCOUNT";
	public static final String I_CVV_TOOLTIP = "I_CVV_TOOLTIP";
	public static final String I_ACCEPT_TREMS = "I_ACCEPT_TREMS";
	
	// Comparison
	public static final String I_NO_ITEMS_TO_COMPARE = "I_NO_ITEMS_TO_COMPARE";
	public static final String E_TOO_FEW_PRODUCTS_TO_COMPARE = "E_TOO_FEW_PRODUCTS_TO_COMPARE";
	public static final String E_TOO_MANY_PRODUCTS_TO_COMPARE = "E_TOO_MANY_PRODUCTS_TO_COMPARE";
	
	// Inventory
	public static final String E_ORDER_ITEMS_ARENT_AVAILABLE = "E_ORDER_ITEMS_ARENT_AVAILABLE";
	public static final String E_ISPU_LOCATION_ISNT_AVAILABLE = "E_ISPU_LOCATION_ISNT_AVAILABLE";
	
	// Contact Us
	public static final String S_CONTACTUS_SUBMITTED = "CONTACTUS_SUBMITTED";
	public static final String E_EMPTY_TOPIC = "E_EMPTY_TOPIC";
	public static final String E_EMPTY_SUBTOPIC = "E_EMPTY_SUBTOPIC";
	public static final String E_EMPTY_MESSAGE = "E_EMPTY_MESSAGE";
	public static final String E_BAD_MESSAGE = "E_BAD_MESSAGE";
	
	// Project List
	public static final String E_NO_PROJECT_LISTS = "E_NO_PROJECT_LISTS";//There are no project lists.
	public static final String E_PRJL_EMPTY = "E_PRJL_EMPTY";//There are no items.
	public static final String E_INVALID_PRJL_NAME = "E_INVALID_PRJL_NAME";//Invalid Project Name.
	public static final String E_EMPTY_PRJL_NAME = "E_EMPTY_PRJL_NAME";//Empty Project Name
	public static final String E_PRJL_NAME_MAX_LENGTH = "E_PRJL_NAME_MAX_LENGTH";//Project List Name is too long.
	public static final String E_DUPLICATE_PRJL_NAME = "E_DUPLICATE_PRJL_NAME";//Project List with the same name already exist.
	public static final String E_INVALID_PRJL_PO_NUMBER = "E_INVALID_PRJL_PO_NUMBER";//Invalid value for Project List PO#
	public static final String E_PRJL_PO_NUMBER_MAX_LENGTH = "E_PRJL_PO_NUMBER_MAX_LENGTH";//PO# List Name is too long.
	public static final String E_INVALID_PRJL_NOTES = "E_INVALID_PRJL_NOTES";//Invalid value for Project List Notes
	public static final String E_PRJL_NOTES_MAX_LENGTH = "E_PRJL_NOTES_MAX_LENGTH";//Project List Notes is too long.
	public static final String E_PROJECT_LIST_NOT_EXIST = "E_PROJECT_LIST_NOT_EXIST";//You have no access to this Project List
	public static final String S_CREATE_PRJL_SUCCESS = "S_CREATE_PRJL_SUCCESS";//Project List was created.
	public static final String S_UPDATE_PRJL_SUCCESS = "S_UPDATE_PRJL_SUCCESS";//Project List was updated.
	public static final String E_INVALID_ADD_ITEM_QTY = "E_INVALID_ADD_ITEM_QTY";//Invalid value for quantity.
	public static final String S_ADD_ITEM_TO_PRJL_SUCCESS = "S_ADD_ITEM_TO_PRJL_SUCCESS";//Product was added to Project List
	public static final String S_ADD_ITEM_TO_PRJL_SUCCESS_INFO = "S_ADD_ITEM_TO_PRJL_SUCCESS_INFO";//Item {0} {1} was added to Project List {2}.
	public static final String E_PRJL_ACCES = "E_PRJL_ACCES";//You have no access to this Project List
	public static final String S_DELETE_PRJL_SUCCESS = "S_DELETE_PRJL_SUCCESS";//Project List was removed.
	public static final String I_DELETE_PRJL_CONFIRM = "I_DELETE_PRJL_CONFIRM";//Are you sure want to delete this Project List.
	public static final String E_PRJL_TRANSIENT_USER = "E_PRJL_TRANSIENT_USER";//Please login before.
	public static final String E_PRJL_PRODUCT_ALREADY_EXIST = "E_PRJL_PRODUCT_ALREADY_EXIST";//Item {0} {1} is already added to {2}.
	public static final String E_PRJL_NOT_SELECTED = "E_PRJL_NOT_SELECTED";//Please choose Project List or create a new one.
	public static final String S_PRJL_CI_ADD_SUCCESS = "S_PRJL_CI_ADD_SUCCESS";//Items was added to Project List.
	public static final String E_PRJL_ITEM_QTY_LESS_MIN = "E_PRJL_ITEM_QTY_LESS_MIN";//Quantity is too little.
	public static final String E_PRJL_ITEM_QTY_UP_MAX = "E_PRJL_ITEM_QTY_UP_MAX";//Quantity is too big.
	public static final String E_PRJL_ITEM_WAS_REMOVED = "E_PRJL_ITEM_WAS_REMOVED";//Item was removed from your Project List.
	public static final String S_PRJL_ITEMS_REMOVED = "S_PRJL_ITEMS_REMOVED";//Items was removed from your Project List.
	public static final String E_PRJL_NO_ITEMS_SELECTED = "E_PRJL_NO_ITEMS_SELECTED";//Please select items before.
	public static final String S_PRJL_ITEM_WAS_ADDET_TO_SC = "S_PRJL_ITEM_WAS_ADDET_TO_SC";//Items was added to cart.
	public static final String E_PRJL_REQUEST_QUOTE_NEED = "E_PRJL_REQUEST_QUOTE_NEED";//At least one item in your Project List is a quote only item. In order to proceed with the quote, please add the item(s) to the cart.
	public static final String S_PRJL_ITEM_REMOVED = "S_PRJL_ITEM_REMOVED";//Item was removed from your Project List.
	
	// PDP
	public static final String E_CUTSIZE_NO_ITEM_TO_ADD = "E_CUTSIZE_NO_ITEM_TO_ADD";//Please enter any cut size.
	
	//Express checkout
	public static final String E_EC_TRANSIENT_USER = "E_TRANSIENT_USER";// Please login before
	public static final String E_EC_SHIPPING_ADDRESS = "E_EC_SHIPPING_ADDRESS";// You express shipping address not valid.
	public static final String E_EC_SHIPPING_METHOD = "E_EC_SHIPPING_METHOD";// You express shipping method not valid.
	public static final String E_EC_PAYMENT_METHOD = "E_EC_PAYMENT_METHOD";// You express payment method not valid.
	public static final String E_EC_EXPRESS_SETTINGS_NOT_SET = "E_EXPRESS_SETTINGS_NOT_SET";// Your express checkout settings not set.
	
	//Cust items
	public static final String E_SELECT_CUST_ITEM_TO_REMOVE = "E_SELECT_CUST_ITEM_TO_REMOVE";// Please select line which should be removed.
	public static final String E_SC_REMOVE_LAST_CUT_SIZE_LINE = "E_SC_REMOVE_LAST_CUT_SIZE_LINE";
	public static final String E_PRJL_REMOVE_LAST_CUT_SIZE_LINE = "E_PRJL_REMOVE_LAST_CUT_SIZE_LINE";
	public static final String S_CUSTOMIZED_LINE_WAS_REMOVED = "S_CUSTOMIZED_LINE_WAS_REMOVED";
	public static final String E_SELECTED_CUST_ITEM_WAS_REMOVED = "E_SELECTED_CUST_ITEM_WAS_REMOVED";// Selected line was already removed.
	
	//Packing List
	public static final String E_SEARCH_PACKINGSLIP = "err_search_packingslip";//Select One Option to Search Packing Slips. 
	//Invoices
	public static final String E_SEARCH_INVOICE =  "err_search_invoice"; //Select One Option to Search Invoices. 
	//Orders
	public static final String E_SEARCH_ORDER =  "err_search_order"; //Select One Option to Search for Order.
}