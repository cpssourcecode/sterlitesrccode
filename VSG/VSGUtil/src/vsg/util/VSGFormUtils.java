package vsg.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.core.util.*;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.droplet.GenericFormHandler;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;


public class VSGFormUtils extends GenericService {

	private static final String VSG_REG_EXP = "vsg.util.RegularExpressions";
	private static final String JSON_FIELD_ERROR = "error";
	private static final String JSON_FIELD_ERRORS = "errors";
	private static final String JSON_FIELD_PROPERTIES = "properties";
	private static final String JSON_FIELD_ERROR_CODES = "errorCodes";
	private static final String CONTENT_TYPE_APP_JSON = "application/json";
	final ResourceBundle regularExpBundle = ResourceBundle.getBundle(VSG_REG_EXP);

	private List<String> rules = new ArrayList<String>();

	/*
	 * Form Validation Method
	 *
	 * This method takes in a map of <keys, values> for all of the
	 * fields within the form being validated. Will loop through all
	 * of the keys in the map and validate the values against the regExp
	 * matched to the key. If fails, add errorCode of matched key to return
	 * and display in the FormHandler.
	 *
	 * @return
	 */
	public Map<String, Object> validateForm(Map<String, String> values) {
		final LinkedHashMap<String, Object> errorCodes = new LinkedHashMap<String, Object>();

		//parse out rules
		List<String> validationRules = getRules();
		LinkedHashMap<String, String> expressionRules = new LinkedHashMap<String, String>();
		LinkedHashMap<String, String> errorRules = new LinkedHashMap<String, String>();
		for (String rule : validationRules) {
			//For each rule, parse out the parts and add it to the map
			String mappedRule = rule.substring(0, rule.length());
			int closingKeyChar = mappedRule.indexOf(":");
			String key = mappedRule.substring(0, closingKeyChar);
			String exp = mappedRule.substring(closingKeyChar + 1, mappedRule.indexOf("|"));
			String errCode = mappedRule.substring(mappedRule.indexOf("|") + 1, rule.length());
			vlogDebug("\n\tKey = " + key + "\n\tExpression = " + exp + "\n\tErrorCode = " + errCode);

			// Add expressions and error codes to their own linked hash maps
			expressionRules.put(key, exp);
			errorRules.put(key, errCode);
		}

		// run validation for values
		for (Entry<String, String> value : values.entrySet()) {
			//Match the value to the correct key to get the
			//corresponding regular expression and errorCode
			String valueKey = value.getKey();
			String fieldValue = value.getValue();
			String regExp = expressionRules.get(valueKey);
			String errorCode = errorRules.get(valueKey);
			vlogDebug("\n\tValue Key = " + valueKey + "\n\tFieldValue = " + fieldValue + "\n\tRegExp = " + regExp +
					"\n\tErrorCode = " + errorCode);

			// Collect errorCodes to return
			Map<String, Object> errors = validateFormField(valueKey, fieldValue, regExp, errorCode);
			vlogDebug("Error being added: " + errors);
			errorCodes.put(errors.entrySet().iterator().next().getKey(), errors.entrySet().iterator().next().getValue());
		}

		// return errorCodes to display
		vlogDebug("Returning error codes: " + errorCodes);
		return errorCodes;
	}

	public Map<String, Object> validateFormField(String key, String pValue, String pRegExp, String errorCode) {
		final LinkedHashMap<String, Object> errorCodes = new LinkedHashMap<String, Object>();
		final Set<String> emailErrorCode = new HashSet<String>();
		String regExp = ResourceUtils.getMsgResource(pRegExp, VSG_REG_EXP, regularExpBundle);

		Pattern p = Pattern.compile(regExp);
		Matcher m = p.matcher(pValue);

		if (!m.matches()) {
			emailErrorCode.add(errorCode);
			vlogDebug("Validation failed, add errorCode");
			// Currently setting property paths to null, can be
			// modified to accept property path in future implementation
			errorCodes.put(key, emailErrorCode);
		}

		return errorCodes;
	}

	public List<String> getRules() {
		return rules;
	}

	public void setRules(List<String> rules) {
		this.rules = rules;
	}

	/**
	 * Check if ajax request.
	 *
	 * @param request - request.
	 * @return true if ajax request, otherwise false.
	 */
	public static boolean isAjaxRequest(DynamoHttpServletRequest request) {
		String acceptHeader = request.getHeader("Accept");

		return (null != acceptHeader) && (acceptHeader.contains("text/json") || acceptHeader.contains(CONTENT_TYPE_APP_JSON));
	}

	/**
	 * Adding Success Json response.
	 *
	 * @param pResponse
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxSuccessResponse(DynamoHttpServletResponse pResponse)
			throws IOException, JSONException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put(JSON_FIELD_ERROR, "false");

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Adding Success Json response.
	 *
	 * @param pResponse
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxOkResponse(DynamoHttpServletResponse pResponse, String pOk)
			throws IOException, JSONException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put(JSON_FIELD_ERROR, "false");
		if (null != pOk) {
			responseJson.put("ok", pOk);
		}

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Adding Success Json response.
	 *
	 * @param pResponse pResponse
	 * @throws IOException IOException
	 * @throws JSONException
	 */
	public static void addLoginAjaxSuccessResponse(DynamoHttpServletResponse pResponse, String successURL)
			throws IOException, JSONException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put(JSON_FIELD_ERROR, "false");
		responseJson.put("successURL", successURL);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Add Errors to response.
	 *
	 * @param pFormHandler pFormHandler
	 * @param pResponse pResponse
	 * @throws IOException IOException
	 * @throws JSONException JSONException
	 */
	public static void addLoginAjaxErrorResponse(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse)
			throws IOException, JSONException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put(JSON_FIELD_ERROR, "true");

		JSONArray errorArray = new JSONArray();

		//JSONObject errorArray[] = new JSONObject[getFormExceptions().size()];
		JSONArray errorPropertiesArray = new JSONArray(); // new JSONObject[getFormExceptions().size()];

		for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException error = (DropletException) pFormHandler.getFormExceptions().get(i);
			errorArray.add(error.getMessage());

			if (error instanceof DropletFormException) {
				DropletFormException exc = (DropletFormException) error;
				errorPropertiesArray.add(exc.getPropertyPath());
			}
		}

		responseJson.put(JSON_FIELD_ERRORS, errorArray);
		responseJson.put(JSON_FIELD_PROPERTIES, errorPropertiesArray);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Add Errors to response.
	 *
	 * @param pFormHandler
	 * @param pResponse
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxErrorResponse(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse, Set<String> propertyPath)
			throws IOException, JSONException {

		JSONObject responseJson = ajaxErrorResponse(pFormHandler, pResponse, propertyPath);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	private static JSONObject ajaxErrorResponse(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse, Set<String> propertyPath) throws JSONException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson = new JSONObject();
		responseJson.put(JSON_FIELD_ERROR, "true");

		JSONArray errorArray = new JSONArray();

		//JSONObject errorArray[] = new JSONObject[getFormExceptions().size()];
		JSONArray errorPropertiesArray = new JSONArray(); // new JSONObject[getFormExceptions().size()];

		for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException error = (DropletException) pFormHandler.getFormExceptions().get(i);
			errorArray.add(error.getMessage());
		}

		for (String property : propertyPath) {
			errorPropertiesArray.add(property);
		}
		/*Iterator<String> it = propertyPath.keySet().iterator();
		while (it.hasNext()) {
			errorPropertiesArray.add(it.next());
		}*/

		responseJson.put(JSON_FIELD_ERRORS, errorArray);
		responseJson.put(JSON_FIELD_PROPERTIES, errorPropertiesArray);
		return responseJson;
	}

	/**
	 * Add Errors to response.
	 *
	 * @param pFormHandler
	 * @param pResponse
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxErrorResponseGiftList(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse, Set<String> propertyPath, String pGiftListId)
			throws IOException, JSONException {

		JSONObject responseJson = ajaxErrorResponse(pFormHandler, pResponse, propertyPath);
		responseJson.put("giftListId", pGiftListId);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Add Redirect URL to response.
	 *
	 * @param pFormHandler
	 * @param pResponse
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxRedirectResponse(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse, String pRedirectURL)
			throws IOException, JSONException {

		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson = new JSONObject();
		responseJson.put("redirect", pRedirectURL);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Add Errors to response.
	 *
	 * @param pFormHandler
	 * @param pResponse
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxErrorResponseMap(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse, Map<String, String> propertyPath)
			throws IOException, JSONException {

		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson = new JSONObject();
		responseJson.put(JSON_FIELD_ERROR, "true");
		JSONArray errorArray = new JSONArray();

		//JSONObject errorArray[] = new JSONObject[getFormExceptions().size()];
		JSONArray errorPropertiesArray = new JSONArray(); // new JSONObject[getFormExceptions().size()];

		for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException error = (DropletException) pFormHandler.getFormExceptions().get(i);
			errorArray.add(error.getMessage());
		}

		/*  for(String property: propertyPath){
		errorPropertiesArray.add(property);
		}*/
		Iterator<String> it = propertyPath.keySet().iterator();
		while (it.hasNext()) {
			errorPropertiesArray.add(it.next());
		}

		responseJson.put(JSON_FIELD_ERRORS, errorArray);
		responseJson.put(JSON_FIELD_PROPERTIES, errorPropertiesArray);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Add Errors to response.
	 *
	 * @param pFormHandler pFormHandler
	 * @param pResponse pResponse
	 * @throws IOException IOException
	 * @throws JSONException JSONException
	 */
	public static void addAjaxErrorResponse(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse)
			throws IOException, JSONException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put(JSON_FIELD_ERROR, "true");

		JSONArray errorArray = new JSONArray();

		//JSONObject errorArray[] = new JSONObject[getFormExceptions().size()];
		JSONArray errorPropertiesArray = new JSONArray(); // new JSONObject[getFormExceptions().size()];

		JSONArray errorCodesArray = new JSONArray(); // new JSONObject[getFormExceptions().size()];

		for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException error = (DropletException) pFormHandler.getFormExceptions().get(i);
			errorArray.add(error.getMessage());

			if (error instanceof DropletFormException) {
				DropletFormException exc = (DropletFormException) error;
				errorPropertiesArray.add(exc.getPropertyPath());
				errorCodesArray.add(exc.getErrorCode());
			}
		}

		responseJson.put(JSON_FIELD_ERRORS, errorArray);
		responseJson.put(JSON_FIELD_PROPERTIES, errorPropertiesArray);
		responseJson.put(JSON_FIELD_ERROR_CODES, errorCodesArray);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Adds results to the json object response
	 *
	 * @param pResponse
	 * @param pResults
	 * @throws IOException
	 * @throws JSONException
	 * @throws PropertyNotFoundException
	 */
	public static void addResultsAjaxSuccessResponse(DynamoHttpServletResponse pResponse, RepositoryItem[] pResults)
			throws IOException, JSONException, PropertyNotFoundException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);
		JSONObject responseJson = new JSONObject();
		responseJson.put(JSON_FIELD_ERROR, "false");
		JSONArray responseJsonArray = new JSONArray();
		if (null != pResults) {
			for (RepositoryItem item : pResults) {
				responseJsonArray.add(DynamicBeans.getPropertyValueAsString(item, "id"));
			}
		}
		responseJson.put("results", responseJsonArray);
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Write in out JSON response.
	 *
	 * @param formHandler - from hahdler.
	 * @param pResponse   - response.
	 * @throws IOException - when servlet response exception.
	 */
	public static void responseAsJSON(GenericFormHandler formHandler, DynamoHttpServletResponse pResponse)
			throws IOException {

		responseAsJSON(formHandler, pResponse, "");
	}

	/**
	 * Write in out JSON response.
	 *
	 * @param formHandler - from hahdler.
	 * @param pResponse   - response.
	 * @throws IOException - when servlet response exception.
	 */
	public static void responseAsJSON(GenericFormHandler formHandler, DynamoHttpServletResponse pResponse, String ok)
			throws IOException {

		String responseMessage = "";
		try {
			JSONObject responseJson = errorToJson(formHandler, ok);
			responseMessage = responseJson.toString();
		} catch (JSONException e) {
			if (formHandler.isLoggingError()) {
				formHandler.logError(e);
			}
		}
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);
		PrintWriter out = pResponse.getWriter();
		out.print(responseMessage);
		out.flush();
	}

	/**
	 * Copy form error into the JSON response object.
	 *
	 * @param formHandler - form handler.
	 * @return - JSON response object.
	 * @throws JSONException - when JSON exceptions occur.
	 */

	@SuppressWarnings("rawtypes")
	public static JSONObject errorToJson(GenericFormHandler formHandler, String ok)
			throws JSONException {

		JSONObject responseJson = new JSONObject();
		JSONArray errorArray = new JSONArray();
		JSONArray propertiesArray = new JSONArray();
		if (formHandler.getFormError()) {
			Vector formExceptions = formHandler.getFormExceptions();
			if (null != formExceptions) {
				for (Object formException : formExceptions) {
					formExceptionProcess(formException, errorArray, propertiesArray);
				}
			}
		}
		if (0 < errorArray.size()) {
			responseJson.put(JSON_FIELD_ERRORS, errorArray);
			responseJson.put(JSON_FIELD_PROPERTIES, propertiesArray);
		} else {
			responseJson.put("ok", ok);
		}
		return responseJson;
	}

	/**
	 * Process form exception. Find error message and property path.
	 *
	 * @param formException form exception.
	 * @param outErrors     errors array
	 * @param outProperties properties path array
	 */
	public static void formExceptionProcess(Object formException, JSONArray outErrors, JSONArray outProperties) {

		if (formException instanceof DropletException) {
			DropletException exception = (DropletException) formException;
			String errorMessage = exception.getMessage();
			if (null != errorMessage && !outErrors.contains(errorMessage)) {
				outErrors.add(errorMessage);
			}
			if (formException instanceof DropletFormException) {
				DropletFormException exc = (DropletFormException) formException;
				String propertyName = exc.getPropertyName();
				if (null != propertyName && !outProperties.contains(propertyName)) {
					outProperties.add(propertyName);
				}
			}
		}
	}

	public static void propertyAsJSON(GenericFormHandler formHandler, DynamoHttpServletResponse pResponse, Map<String, String> properties)
			throws JSONException, IOException {

		JSONObject responseJson = new JSONObject();
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);
		for (String key : properties.keySet()) {
			responseJson.put(key, properties.get(key));
		}

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson.toString());
		out.flush();
	}

	public static void ajaxJSONResponse(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse, Map<String, Object> jsonData) throws IOException, JSONException {
		pResponse.setContentType(CONTENT_TYPE_APP_JSON);

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put(JSON_FIELD_ERROR, Boolean.toString(pFormHandler.getFormError()));

        if(jsonData != null && !jsonData.isEmpty()) {
            for(Map.Entry<String, Object> entry : jsonData.entrySet()) {
                responseJson.put(entry.getKey(), entry.getValue());
            }
        }

        if (pFormHandler.getFormError() && pFormHandler.getFormExceptions() != null) {
            JSONArray outErrorArray = new JSONArray();
            JSONArray outPropertiesArray = new JSONArray();

            for(Object exception : pFormHandler.getFormExceptions()) {
                if (exception instanceof DropletException) {
                    DropletException dropletException = (DropletException) exception;
                    String errorMessage = dropletException.getMessage();
                    if(atg.core.util.StringUtils.isNotBlank(errorMessage) && !outErrorArray.contains(errorMessage)) {
                        outErrorArray.add(errorMessage);
                    }
                    if (exception instanceof DropletFormException) {
                        DropletFormException formException = (DropletFormException) exception;
                        String propertyName = formException.getPropertyName();
                        if(atg.core.util.StringUtils.isNotBlank(propertyName) && !outPropertiesArray.contains(propertyName)) {
                            outPropertiesArray.add(propertyName);
                        }
                    }
                }
            }
            responseJson.put(JSON_FIELD_ERRORS, outErrorArray);
            responseJson.put(JSON_FIELD_PROPERTIES, outPropertiesArray);
        }

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

}