package vsg.util;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import atg.core.i18n.LayeredResourceBundle;

public class VSGMessageUtils {
	
	
	public static Map<Locale, Map<String, Object>> sBundles = new ConcurrentHashMap();
	
	
//	public static void generateFormExeption(Object pMappingHolder, String pLookUpKey, String pMappingHolderName){
//		
//		
//		
//	}

    public static String getMessageText(ResourceBundle mExceptionBundle, String pLookUpKey){
    	String message = pLookUpKey;
    	message = mExceptionBundle.getString(pLookUpKey);
    	return message;
	}
	
	public static Object getResourceBundle(String pBaseName, Locale pLocale){
		Object bundle = null;
		Map bundleNames = (Map)sBundles.get(pLocale);
    	if (bundleNames == null)
    	{
    		bundle = LayeredResourceBundle.getBundle(pBaseName, pLocale);
    	 	bundleNames = new ConcurrentHashMap();
    	 	bundleNames.put(pBaseName, bundle);
    	 	sBundles.put(pLocale, bundleNames);
    	}else{
    		bundle = (ResourceBundle)bundleNames.get(pBaseName);
	    	if (bundle == null)
	    	{
	    		bundle = LayeredResourceBundle.getBundle(pBaseName, pLocale);
	    		bundleNames.put(pBaseName, bundle);
	    	}
    	}
    	return bundle;
	}

}