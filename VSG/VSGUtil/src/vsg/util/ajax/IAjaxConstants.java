package vsg.util.ajax;

/**
 * @author Dmitry Golubev
 */
public interface IAjaxConstants {
	/** The Constant A_JSON. */
	String A_JSON = "application/json";

	/** The Constant T_JSON. */
	String T_JSON = "text/json";

	/** The Constant ACCEPT. */
	String ACCEPT = "Accept";

	/** The Constant ERROR. */
	String ERROR = "error";
	
	/** The Constant SKIP. */
	String SKIP = "skip";
	
	/** The Constant SUCCESS. */
	String SUCCESS = "success";

	/** The Constant ERROR_URL. */
	String ERROR_URL = "errorUrl";
	
	/** The Constant SUCCESS_URL. */
	String SUCCESS_URL = "successUrl";

	/** The Constant SUCCESS_MSG_INFO. */
	String SUCCESS_MSG_INFO = "successMsgInfo";

	/** The Constant SUCCESS_MSG_ERROR. */
	String SUCCESS_MSG_ERROR = "successMsgError";

	/** The Constant CODE. */
	String CODE = "code";

	/** The Constant ERRORS. */
	String ERRORS = "errors";
	
	/** The Constant PARAM. */
	String PARAM = "param";

	/** The Constant PROPERTIES. */
	String PROPERTIES = "properties";

	/** The Constant ORDER_COOKIE. */
	String ORDER_COOKIE = "orderCookie";
	
}
