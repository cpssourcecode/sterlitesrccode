package vsg.util.ajax;

import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.droplet.GenericFormHandler;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.form.IFormHandler;
import vsg.form.ajax.JsonResponseDetails;
import vsg.form.ajax.JsonResponseDetails.ResponseDetailsCode;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

/**
 * Helper for form handlers to work with ajax requests responses.
 * 
 * addAjaxResponse
 * 
 * @author Dmitry Golubev
 *
 */
public final class AjaxUtils implements IAjaxConstants {
	static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(AjaxUtils.class);

	/**
	 * Check if ajax response has been written
	 * 
	 * @param pRequest - request.
	 * @param pResponse - response.
	 * @return true if has been written, false otherwise
	 */
	public static boolean isAjaxResponse(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		return isAjaxRequest(pRequest) && A_JSON.equals(pResponse.getContentType());
	}
	
	/**
	 * Check if ajax request.
	 * 
	 * @param pRequest - request.
	 * @return true if ajax request, otherwise false.
	 */
	public static boolean isAjaxRequest(DynamoHttpServletRequest pRequest) {
		String aheader = pRequest.getHeader(ACCEPT);
		return (null != aheader && (aheader.contains(T_JSON) || aheader.contains(A_JSON)));
	}
	
	/**
	 * Adding Json response.
	 *
	 * @param pFormHandler the form handler
	 * @param pResponse the response
	 * @param pParams params for response
	 * @throws java.io.IOException Signals that an I/O exception has occurred.
	 * @throws atg.json.JSONException the JSON exception
	 */
	public static void addAjaxResponse(GenericFormHandler pFormHandler,
			DynamoHttpServletResponse pResponse, Map<String, Object> pParams) {

		try {
			pResponse.setContentType(A_JSON);
			JSONObject responseJson = new JSONObject();
			if(pParams != null) {
				for(String paramKey : pParams.keySet()) {
					responseJson.put(paramKey, pParams.get(paramKey));
				}
			}
			if ( pFormHandler.getFormError() ) {
				responseJson.put(CODE, ERROR);
				addErrorsDetails(pFormHandler, responseJson);
			} else {
				responseJson.put(CODE, SUCCESS);
			}
			PrintWriter out = pResponse.getWriter();
			out.print(responseJson);
			out.flush();
		} catch (Exception e) {
			if (mLogging != null) {
				mLogging.logError(e);
			}
		}
	}




	/**
	 * Adds the errors details.
	 *
	 * @param pFormHandler the form handler
	 * @param pResponseJson the response json
	 * @throws java.io.IOException Signals that an I/O exception has occurred.
	 * @throws atg.json.JSONException the JSON exception
	 */
	private static void addErrorsDetails(GenericFormHandler pFormHandler,
			JSONObject pResponseJson) throws IOException, JSONException {

		JSONArray errorArray = new JSONArray();
		JSONArray errorPropertiesArray = new JSONArray();
		for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException error = (DropletException) pFormHandler.getFormExceptions().get(i);
			errorArray.add(error.getMessage());
			if ( error instanceof DropletFormException ) {
				DropletFormException exc = (DropletFormException) error;
				String propertyPath = exc.getPropertyPath();
				if ( !errorPropertiesArray.contains(propertyPath) ) {
					errorPropertiesArray.add(propertyPath);
				}
			}
		}
		pResponseJson.put(ERRORS, errorArray);
		pResponseJson.put(PROPERTIES, errorPropertiesArray);
	}


	//********************************************************************************************************************************

		/**
	 * Adds the skip ajax response.
	 *
	 * @param pResponse the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JSONException the jSON exception
	 */
	public static void addSkipAjaxResponse(final DynamoHttpServletResponse pResponse) throws IOException, JSONException {
		JsonResponseDetails jsonReponseDetails = JsonResponseDetails.create();
		jsonReponseDetails.setCode(ResponseDetailsCode.SKIP);
		addAjaxResponse(pResponse, jsonReponseDetails);
	}

	/**
	 * Adds the redirect ajax response.
	 *
	 * @param pResponse the response
	 * @param pRedirectUrl the redirect url
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JSONException the jSON exception
	 */
	public static void addRedirectAjaxResponse(final DynamoHttpServletResponse pResponse, final String pRedirectUrl)
			throws IOException, JSONException {
		JsonResponseDetails jsonReponseDetails = JsonResponseDetails.create();
		jsonReponseDetails.setCode(ResponseDetailsCode.REDIRECT);
		jsonReponseDetails.setSuccessUrl(pRedirectUrl);
		addAjaxResponse(pResponse, jsonReponseDetails);
	}

	/**
	 * Adds the ajax response.
	 *
	 * @param pFormHandler the form
	 * @param pResponse the response
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws JSONException the jSON exception
	 */
	public static void addAjaxResponse(final IFormHandler pFormHandler, final DynamoHttpServletResponse pResponse)
			throws IOException, JSONException {
		JsonResponseDetails jsonReponseDetails = JsonResponseDetails.create(pFormHandler);
		addAjaxResponse(pResponse, jsonReponseDetails);
	}

	/**
	 * Adds the ajax response.
	 *
	 * @param pResponse the response
	 * @param pJsonResponseDetails the json response details
	 * @throws JSONException the jSON exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private static void addAjaxResponse(final DynamoHttpServletResponse pResponse, final JsonResponseDetails pJsonResponseDetails)
			throws JSONException, IOException {
		pResponse.setContentType(pJsonResponseDetails.getContentType().toString());
		JSONObject responseJson = pJsonResponseDetails.getJSON();
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

}
