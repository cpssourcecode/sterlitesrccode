/*
 * 
 */
package vsg.util;

import static vsg.constants.VSGConstants.MAIN_CONTENT;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.Action;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.RecordAction;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.endeca.infront.cartridge.model.UrlAction;

import atg.core.util.NumberTable;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextException;
import atg.repository.RepositoryItem;
import vsg.common.message.IMessageUtils;
import vsg.constants.VSGConstants;
import vsg.constants.VSGMessageConstants;
import vsg.core.util.DataUtils;
import vsg.core.util.VSGUrlUtils;
import vsg.userprofiling.VSGCreditCardManager;

/**
 * The Class TagUtils implements tag library functions.
 */
public final class VSGTagUtils {

	/**
	 * Private constructor to disallow instantiate class.
	 */
	private VSGTagUtils() {
	}

	/**
	 * Get message found exact by key.
	 * 
	 * @param pMessages - message utils.
	 * @param pKey key value.
	 * @return message.
	 */
	public static String keyMessage(final IMessageUtils pMessages, final String pKey) {
		return (null == pMessages) ? "" : pMessages.getMessage(pKey);
	}

	/**
	 * Get format message found exact by key.
	 * 
	 * @param pMessages - message utils.
	 * @param pKey key value.
	 * @param pParams parameters map. It is expected that parameters map will have key_0=value0 key_1=value1 ... note:
	 * valueN could not be null, use empty string.
	 * 
	 * @return message.
	 */
	public static String formatMessage(final IMessageUtils pMessages, final String pKey,
			final Map<String, String> pParams) {

		String result;
		if (null == pMessages) {
			result = "";
		} else {
			final ArrayList<String> values = new ArrayList<String>();
			if (null != pKey && null != pParams) {
				for (int index = 0; index < Integer.MAX_VALUE; index++) {
					final String value = pParams.get(new StringBuilder(pKey).append("_").append(index).toString());
					if (null == value) {
						break;
					} else {
						values.add(value);
					}
				}
			}
			result = MessageFormat.format(pMessages.getMessage(pKey), values.toArray());
		}
		return result;
	}

	/**
	 * Get format message found exact by key.
	 * 
	 * @param pMessages - message utils.
	 * @param pKey key value.
	 * @param pParams parameters map. It is expected that parameters map will have %key%=value %key%=value ...
	 * 
	 * @return message.
	 */
	public static String prepareMessage(final IMessageUtils pMessages, final String pKey,
			final Map<String, String> pParams) {

		return (null == pMessages) ? "" : DataUtils.paramsMessage(pMessages.getMessage(pKey), pParams);
	}
	
	/**
	 * Gets content of estimated delivery date block.
	 *
	 * @param pMsgUtils the message utils
	 * @param pEstDeliveryDate the estimated delivery date as string
	 * @return the content of estimated delivery date block
	 */
	public static String getEstDeliveryDateBlock(final IMessageUtils pMsgUtils, final String pEstDeliveryDate) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("PARAM_DATE", pEstDeliveryDate);
		return prepareMessage(pMsgUtils, VSGMessageConstants.I_DELIVERY_DATE, params);
	}
	
	/**
	 * Gets content of estimated delivery date block.
	 *
	 * @param pMsgUtils the message utils
	 * @param pEstDeliveryDate the estimated delivery date as string
	 * @return the content of estimated delivery date block
	 */
	public static String getEstDeliveryDateShort(final IMessageUtils pMsgUtils, final String pEstDeliveryDate) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("PARAM_DATE", pEstDeliveryDate);
		return prepareMessage(pMsgUtils, VSGMessageConstants.I_DELIVERY_DATE_SHORT, params);
	}

	/**
	 * Sets hours, minutes, seconds and milliseconds of current date to zero.
	 * 
	 * @param cal calendar
	 */
	private static void resetTime(Calendar cal) {
		cal.set(Calendar.HOUR, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * Find map value.
	 * 
	 * @param pCardTypes the card types
	 * @param pCardType the card type
	 * @return the string
	 */
	public static String findMapValue(final Map<String, String> pCardTypes, String pCardType) {
		String result;
		if (null == pCardTypes) {
			result = pCardType;
		} else {
			result = pCardTypes.get(pCardType);
		}
		return (null == result) ? pCardType : result;
	}

	/**
	 * Params message.
	 * 
	 * @param pMessage the message
	 * @param pParams the params
	 * @return the string
	 */
	public static String paramsMessage(final String pMessage, final Map<String, String> pParams) {
		return DataUtils.paramsMessage(pMessage, pParams);
	}

	/**
	 * Number pages.
	 * 
	 * @param pRecords the records
	 * @param pRecordsPerPage the records per page
	 * @return the integer
	 */
	public static Integer numberPages(Integer pRecords, Integer pRecordsPerPage) {
		int modulo = pRecords % pRecordsPerPage;
		int result = Math.round((pRecords - modulo) / (float)pRecordsPerPage);
		return NumberTable.getInteger((0 == modulo) ? result : result + 1);
	}

	/**
	 * Current page.
	 * 
	 * @param pStartRecord
	 *            the start record
	 * @param pRecordsPerPage
	 *            the records per page
	 * @return the integer
	 */
	public static Integer currentPage(Integer pStartRecord, Integer pRecordsPerPage) {
		return NumberTable.getInteger(Math.round(pStartRecord / (float)pRecordsPerPage) + 1);
	}

	/**
	 * Rows number.
	 * 
	 * @param pRecords
	 *            the records
	 * @param pRowNumber
	 *            the row number
	 * @return the integer
	 */
	public static Integer rowsNumber(Integer pRecords, Integer pRowNumber) {
		return Math.round(pRecords / (float)pRowNumber);
	}

	/**
	 * Round number to nearest integer.
	 * 
	 * @param pNumber
	 *            the number
	 * @return the integer
	 */
	public static Integer roundNumber(String pNumber) {
		int result = 0;
		if (!StringUtils.isBlank(pNumber)) {
			Double newNumber = Double.valueOf(pNumber);
			result = (int) Math.round(newNumber);
		}
		return result;
	}

	/**
	 * Round number to the largest less or equals integer. Just remove
	 * fractional part.
	 * 
	 * @param pNumber
	 *            to round
	 * @return the largest less or equals integer
	 */
	public static Integer floorNumber(String pNumber) {
		int result = 0;
		if (!StringUtils.isBlank(pNumber)) {
			Double doubleNumber = Double.valueOf(pNumber);
			result = (int) Math.floor(doubleNumber);
		}
		return result;
	}

	/**
	 * Used to escape String that will be used in a javascript contexts.
	 *
	 * @param unescapedStr - string to be escaped
	 * @return string properly escaped to be used in a javascript context
	 */
	public static String escapeJSParam(String unescapedStr) {
		String escapedStr = unescapedStr.replaceAll("\\\\", "\\\\\\\\");
		escapedStr = escapedStr.replaceAll("\"", "\\\\\"");
		escapedStr = escapedStr.replaceAll("\'", "\\\\'");
		return escapedStr;
	}

	/**
	 * Phone number.
	 *
	 * @param pNumber the number
	 * @param pCountry the country
	 * @return the string
	 */
	public static String phoneNumber(final String pNumber, final String pCountry) {
		final StringBuilder result = new StringBuilder();
		if ( StringUtils.isNotBlank(pNumber) ) {
			final int j = pNumber.length();
			for ( int k = 0; k < j; k++ ) {
				char c = pNumber.charAt(k);
				if ( Character.isDigit(c) ) {
					result.append(c);
					if ( 10 <= result.length() ) {
						break;
					}
				}
			}
		}
		return result.toString();
	}

	/**
	 * Phone extension.
	 *
	 * @param pNumber the number
	 * @param pCountry the country
	 * @return the string
	 */
	public static String phoneExtension(final String pNumber,final String pCountry) {
		final StringBuilder result = new StringBuilder();
		if ( StringUtils.isNotBlank(pNumber) ) {
			final int j = pNumber.length();
			int n = 0;
			for ( int k = 0; k < j; k++ ) {
				char c = pNumber.charAt(k);
				if ( Character.isDigit(c) ) {
					n = n + 1;
					if ( 10 < n ) {
						result.append(c);
					}
				}
			}
		}
		return result.toString();
	}

	/** The countries. */
	private static final String[] COUNTRIES = {
	"", "Please select a Country",
	"US", "United States",
	"CA", "Canada",
	"MX", "Mexico",
	"AF", "Afghanistan",
	"AL", "Albania",
	"DZ", "Algeria",
	"AS", "American Samoa",
	"AD", "Andorra",
	"AO", "Angola",
	"AI", "Anguilla",
	"AQ", "Antarctica",
	"AG", "Antigua and Barbuda",
	"AR", "Argentina",
	"AM", "Armenia",
	"AW", "Aruba",
	"AU", "Australia",
	"AT", "Austria",
	"AZ", "Azerbaijan",
	"AP", "Azores",
	"BS", "Bahamas",
	"BH", "Bahrain",
	"BD", "Bangladesh",
	"BB", "Barbados",
	"BY", "Belarus",
	"BE", "Belgium",
	"BZ", "Belize",
	"BJ", "Benin",
	"BM", "Bermuda",
	"BT", "Bhutan",
	"BO", "Bolivia",
	"BA", "Bosnia And Herzegowina",
	"XB", "Bosnia-Herzegovina",
	"BW", "Botswana",
	"BV", "Bouvet Island",
	"BR", "Brazil",
	"IO", "British Indian Ocean Territory",
	"VG", "British Virgin Islands",
	"BN", "Brunei Darussalam",
	"BG", "Bulgaria",
	"BF", "Burkina Faso",
	"BI", "Burundi",
	"KH", "Cambodia",
	"CM", "Cameroon",
	"CV", "Cape Verde",
	"KY", "Cayman Islands",
	"CF", "Central African Republic",
	"TD", "Chad",
	"CL", "Chile",
	"CN", "China",
	"CX", "Christmas Island",
	"CC", "Cocos (Keeling) Islands",
	"CO", "Colombia",
	"KM", "Comoros",
	"CG", "Congo",
	"CK", "Cook Islands",
	"XE", "Corsica",
	"CR", "Costa Rica",
	"HR", "Croatia",
	"CU", "Cuba",
	"CY", "Cyprus",
	"CZ", "Czech Republic",
	"CI", "Cote d Ivoire (Ivory Coast)",
	"DK", "Denmark",
	"DJ", "Djibouti",
	"DM", "Dominica",
	"DO", "Dominican Republic",
	"TP", "East Timor",
	"EC", "Ecuador",
	"EG", "Egypt",
	"SV", "El Salvador",
	"GQ", "Equatorial Guinea",
	"ER", "Eritrea",
	"EE", "Estonia",
	"ET", "Ethiopia",
	"FK", "Falkland Islands (Malvinas)",
	"FO", "Faroe Islands",
	"FJ", "Fiji",
	"FI", "Finland",
	"FR", "France (Includes Monaco)",
	"GF", "French Guiana",
	"PF", "French Polynesia",
	"TA", "French Polynesia (Tahiti)",
	"TF", "French Southern Territories",
	"GA", "Gabon",
	"GM", "Gambia",
	"GE", "Georgia",
	"DE", "Germany",
	"GH", "Ghana",
	"GI", "Gibraltar",
	"GR", "Greece",
	"GL", "Greenland",
	"GD", "Grenada",
	"GP", "Guadeloupe",
	"GU", "Guam",
	"GT", "Guatemala",
	"GN", "Guinea",
	"GW", "Guinea-Bissau",
	"GY", "Guyana",
	"HT", "Haiti",
	"HM", "Heard And Mc Donald Islands",
	"HN", "Honduras",
	"HK", "Hong Kong",
	"HU", "Hungary",
	"IS", "Iceland",
	"IN", "India",
	"ID", "Indonesia",
	"IR", "Iran",
	"IQ", "Iraq",
	"IE", "Ireland",
	"EI", "Ireland (Eire)",
	"IL", "Israel",
	"IT", "Italy",
	"JM", "Jamaica",
	"JP", "Japan",
	"JO", "Jordan",
	"KZ", "Kazakhstan",
	"KE", "Kenya",
	"KI", "Kiribati",
	"KW", "Kuwait",
	"KG", "Kyrgyzstan",
	"LA", "Laos",
	"LV", "Latvia",
	"LB", "Lebanon",
	"LS", "Lesotho",
	"LR", "Liberia",
	"LY", "Libya",
	"LI", "Liechtenstein",
	"LT", "Lithuania",
	"LU", "Luxembourg",
	"MO", "Macao",
	"MK", "Macedonia",
	"MG", "Madagascar",
	"MW", "Malawi",
	"MY", "Malaysia",
	"MV", "Maldives",
	"ML", "Mali",
	"MT", "Malta",
	"MH", "Marshall Islands",
	"MQ", "Martinique",
	"MR", "Mauritania",
	"MU", "Mauritius",
	"YT", "Mayotte",
	"FM", "Micronesia Federated States Of",
	"MD", "Moldova Republic Of",
	"MC", "Monaco",
	"MN", "Mongolia",
	"ME", "Madeira Islands",
	"MS", "Montserrat",
	"MA", "Morocco",
	"MZ", "Mozambique",
	"MM", "Myanmar (Burma)",
	"NA", "Namibia",
	"NR", "Nauru",
	"NP", "Nepal",
	"NL", "Netherlands",
	"AN", "Netherlands Antilles",
	"NC", "New Caledonia",
	"NZ", "New Zealand",
	"NI", "Nicaragua",
	"NE", "Niger",
	"NG", "Nigeria",
	"NU", "Niue",
	"NF", "Norfolk Island",
	"KP", "Korea Democratic People Repub",
	"MP", "Northern Mariana Islands",
	"NO", "Norway",
	"OM", "Oman",
	"PK", "Pakistan",
	"PW", "Palau",
	"PS", "Palestinian Territory Occupied",
	"PA", "Panama",
	"PG", "Papua New Guinea",
	"PY", "Paraguay",
	"PE", "Peru",
	"PH", "Philippines",
	"PN", "Pitcairn",
	"PL", "Poland",
	"PT", "Portugal",
	"PR", "Puerto Rico",
	"QA", "Qatar",
	"RE", "Reunion",
	"RO", "Romania",
	"RU", "Russian Federation",
	"RW", "Rwanda",
	"SH", "St. Helena",
	"KN", "Saint Kitts And Nevis",
	"LC", "St. Lucia",
	"PM", "St. Pierre and Miquelon",
	"VC", "St. Vincent and the Grenadines",
	"WS", "Western Samoa",
	"SM", "San Marino",
	"ST", "Sao Tome and Principe",
	"SA", "Saudi Arabia",
	"SN", "Senegal",
	"XS", "Serbia-Montenegro",
	"SC", "Seychelles",
	"SL", "Sierra Leone",
	"SG", "Singapore",
	"SK", "Slovak Republic",
	"SI", "Slovenia",
	"SB", "Solomon Islands",
	"SO", "Somalia",
	"ZA", "South Africa",
	"GS", "South Georgia And The South Sand",
	"KR", "South Korea",
	"ES", "Spain",
	"LK", "Sri Lanka",
	"NV", "St. Christopher and Nevis",
	"SD", "Sudan",
	"SR", "Suriname",
	"SJ", "Svalbard And Jan Mayen Islands",
	"SZ", "Swaziland",
	"SE", "Sweden",
	"CH", "Switzerland",
	"SY", "Syrian Arab Republic",
	"TW", "Taiwan",
	"TJ", "Tajikistan",
	"TZ", "Tanzania",
	"TH", "Thailand",
	"CD", "Congo The Democratic Republic",
	"TG", "Togo",
	"TK", "Tokelau",
	"TO", "Tonga",
	"TT", "Trinidad and Tobago",
	"XU", "Tristan da Cunha",
	"TN", "Tunisia",
	"TR", "Turkey",
	"TM", "Turkmenistan",
	"TC", "Turks and Caicos Islands",
	"TV", "Tuvalu",
	"VI", "Virgin Islands (U.S.)",
	"UG", "Uganda",
	"UA", "Ukraine",
	"AE", "United Arab Emirates",
	"UK", "United Kingdom",
	"UM", "United States Minor Outlying Isl",
	"UY", "Uruguay",
	"UZ", "Uzbekistan",
	"VU", "Vanuatu",
	"VA", "Holy See (Vatican City State)",
	"XV", "Vatican City",
	"VE", "Venezuela",
	"VN", "Vietnam",
	"WF", "Wallis and Furuna Islands",
	"EH", "Western Sahara",
	"YE", "Yemen",
	"YU", "Yugoslavia",
	"ZR", "Zaire",
	"ZM", "Zambia",
	"ZW", "Zimbabwe",
	};

	/** The countries. */
	private static final String[] COUNTRIES_CUTTED_LIST = {
			//"", "Please select a Country",
			"US", "United States",
	};

	/**
	 * Countries list.
	 *
	 * @param pCountry the country
	 * @return the string
	 */
	public static String countriesList(String pCountry) {
		return countriesList(pCountry, COUNTRIES_CUTTED_LIST);
	}
	
	/**
	 * Countries all list.
	 *
	 * @param pCountry the country
	 * @return the string
	 */
	public static String countriesAllList(String pCountry) {
		return countriesList(pCountry, COUNTRIES);
	}
	
	/**
	 * Countries list.
	 *
	 * @param pCountry the country
	 * @param pCountries the countries
	 * @return the string
	 */
	protected static String countriesList(String pCountry, String[] pCountries) {
		String resultString = null;
		
		if (null != pCountries) {
			final StringBuilder result = new StringBuilder();
			String country = (null == pCountry) ? "" : pCountry.trim().toUpperCase();
			if ( "USA".equals(country) ) {
				country = "US";
			}
//			int k = COUNTRIES.length / 2;
			for ( int j = 0; j < pCountries.length; j = j + 2 ) {
				if ( pCountries[j].equals(country) ) {
					result.append("<option value=\"").append(pCountries[j])
						.append("\" selected>").append(pCountries[j+1]).append("</option>");
				} else {
					result.append("<option value=\"").append(pCountries[j]).append("\">")
						.append(pCountries[j+1]).append("</option>");
				}
			}
			resultString = result.toString();
		}
		
		return resultString;
	}

	/**
	 * Gets the credit card.
	 *
	 * @param pCardManager the card manager
	 * @param pCard the card
	 * @return the credit card
	 * @throws SiteContextException 
	 */
	@SuppressWarnings("rawtypes")
	public static Map getCreditCard(final VSGCreditCardManager pCardManager, final RepositoryItem pCard) throws SiteContextException {
		final Map result;
		if ( null == pCardManager ) {
			result = new HashMap();
		} else {
			result = pCardManager.findCard(pCard);
		}
		return result;
	}

	/**
	 * Gets the first role.
	 *
	 * @param pProfile the profile
	 * @return the first role
	 */
	public static String getFirstRole(final RepositoryItem pProfile) {
		String roleName = null;
		Set roles = (Set) pProfile.getPropertyValue("roles");
		if ( null != roles ) {
			Iterator iter = roles.iterator();
			if ( iter.hasNext() ) {
				RepositoryItem role = (RepositoryItem) iter.next();
				roleName = (String) role.getPropertyValue("name");
			}
		}
		return roleName;
	}

	/**
	 * Gets the card type.
	 *
	 * @param pCardCode the card code
	 * @return the card type
	 */
	public static String getCardType(final String pCardCode) {
		String returnString;
		if ( "001".equals(pCardCode) ) {
			returnString = "Visa";
		} else if ( "002".equals(pCardCode) ) {
			returnString = "Master Card";
		} else if ( "003".equals(pCardCode) ) {
			returnString = "American Express";
		} else if ( "004".equals(pCardCode) ) {
			returnString = "Discover";
		} else {
			returnString = "";
		}
		return returnString;
	}
	
	/**
	 * Gets the url for action.
	 *
	 * @param pAction the action
	 * @return the url for action
	 */
	/*public static String getUrlForAction(Action pAction) {
		StringBuilder href = new StringBuilder();
		if (pAction instanceof NavigationAction) {
			NavigationAction navAction = (NavigationAction)pAction;
			String path = navAction.getContentPath();
			if ("/search".equals(path)) {
				href.append(path);
			}
			if (StringUtils.isEmpty(navAction.getNavigationState())) {
				//href.append(navAction.getSiteRootPath()).append(navAction.getContentPath());
				href.append(VSGConstants.QUESTION_MARK);
			} else {
				href.append(navAction.getNavigationState());
			}
		}
		return href.toString();
	}*/
	public static String getUrlForAction(Action action) {
		String href = "";
		if (action instanceof NavigationAction) {
			NavigationAction navAction = (NavigationAction) action;
			if (navAction.getNavigationState() != null){
				href += navAction.getNavigationState();
			}
		} else if (action instanceof RecordAction) {
			RecordAction recAction = (RecordAction) action;
			if (recAction.getContentPath() != null){
				href += recAction.getContentPath();
			}
			if (recAction.getRecordState() != null){
				href += recAction.getRecordState();
			}
		} else if (action instanceof UrlAction) {
			href = ((UrlAction) action).getUrl();
		}
		return changeUrlToLower(href);
	}
	
	public static String changeUrlToLower(String url) {
		String result = "";
		if (StringUtils.isNotBlank(url)) {
			if (url.contains("?")) {
				result = url.substring(0, url.indexOf("?")).toLowerCase() + url.substring(url.indexOf("?"));
			} else {
				result = url.toLowerCase();
			}
		}
		if (!result.equals("")) {
			return result;
		}
		return url;
	}
	
	/**
	 * Retrieves pagination action
	 * 
	 * @param actionTemplate a NavigationAction that is used to construct paging actions
	 * @param pageNum current page number 1-based
	 * @param recordsPerPage number of records per page
	 * @param defaultRecordsPerPage default value for number results per page
	 * @return paging action for given arguments
	 */
	public static NavigationAction getPaginationAction(NavigationAction actionTemplate, int pageNum,
			int recordsPerPage, int defaultRecordsPerPage) {
		
		NavigationAction pagingControl = new NavigationAction();
		pagingControl.setSiteRootPath(actionTemplate.getSiteRootPath());
		pagingControl.setContentPath(actionTemplate.getContentPath());
		pagingControl.setSiteState(actionTemplate.getSiteState());
		pagingControl.setLabel(Integer.toString(pageNum));
		
		String navState = actionTemplate.getNavigationState();
		if (pageNum > 1) {
			int offset = (pageNum - 1) * recordsPerPage;
			navState = VSGUrlUtils.updateParam(navState, VSGConstants.OFFSET_PARAM, Integer.toString(offset));
		} else {
			navState = VSGUrlUtils.removeParam(navState, VSGConstants.OFFSET_PARAM);
		}
		
		if (recordsPerPage != defaultRecordsPerPage) {
			navState = VSGUrlUtils.updateParam(navState, VSGConstants.RECORDS_PER_PAGE_PARAM, Integer.toString(recordsPerPage));
		} else {
			navState = VSGUrlUtils.removeParam(navState, VSGConstants.RECORDS_PER_PAGE_PARAM);
		}
		
		pagingControl.setNavigationState(navState);
		return pagingControl;
	}
	
	/**
	 * get crumbs dimval ids
	 * @param pRefined - crumbs
	 * @return string of separated ids
	 */
	public final static String getCrumbsDimValIds(List<RefinementBreadcrumb> pRefined, Boolean isForMultiSelect) {
		StringBuilder result = new StringBuilder();
		for ( RefinementBreadcrumb b : pRefined ) {
			if(b.getProperties() != null && !b.getProperties().isEmpty()){
				String id = b.getProperties().get("dimValId");
				if (!StringUtils.isEmpty(id)){
					if(isForMultiSelect && b.isMultiSelect() || !(isForMultiSelect && b.isMultiSelect())){
						result.append(id).append("###");
					} 
					
				}
			}
		}
		return result.toString();
	}
	
	/**
	 * Get multi refinements
	 * 
	 * @param pRefined refined.
	 * @param pRefinements refinements
	 * @param pDimensionName dimension name
	 * @return multi refinements
	 */
	public final static List<Refinement> getMultiRefinements(List<RefinementBreadcrumb> pRefined,
			List<Refinement> pRefinements, String pDimensionName) {
		
		List<Refinement> result;
		try {
			result = new ArrayList<Refinement>();
			for ( Refinement ref: pRefinements ) {
				ref.getProperties().put("selected", "false");
				result.add(ref);
			}
			if ( null != pDimensionName ) {
				for ( RefinementBreadcrumb b : pRefined ) {
					if ( pDimensionName.equals(b.getDimensionName()) ) {
						Refinement ref = new Refinement();
						ref.setNavigationState(b.getRemoveAction().getNavigationState());
						ref.setLabel(b.getLabel());
						ref.setCount(b.getCount());
						Map<String, String> properties = ref.getProperties();
						if ( null == properties ) {
							properties = new HashMap<String, String>();
							ref.setProperties(properties);
						}
						properties.put("selected", "true");
						if(b.getProperties() != null && !b.getProperties().isEmpty()){
							for(Map.Entry<String, String> entry : b.getProperties().entrySet()){
								properties.put(entry.getKey(), entry.getValue());
							}
						}
						result.add(ref);
					}
				}
			}
			Collections.sort(result, new Comparator<Refinement>(){
				public int compare(final Refinement lhs, final Refinement rhs) {
					return lhs.getLabel().compareTo(rhs.getLabel());
				}
			});
		} catch ( Exception e ) { result = pRefinements; }
		return result;
	}

	/**
	 * Contains tag.
	 *
	 * @param tags the tags
	 * @param tag the tag
	 * @return true, if successful
	 */
	public static boolean containsTag(Set tags, String tag) {
		boolean result = false;
		if ( null != tag && null != tags && !tags.isEmpty() ) {
//			for (Iterator iter = tags.iterator(); iter.hasNext(); ) {
//				try {
//					RepositoryItem item = (RepositoryItem) iter.next();
//					if ( tag.equals(item.getPropertyValue("tag")) ) {
//						result = true;
//						break;
//					}
//				} catch (Exception e) {
//					result = false;
//				}
//			}
			result=tags.contains(tag);
		}
		return result;
	}

	/**
	 * Gets the results tabs.
	 *
	 * @param pContentItem the content item
	 * @return the results tabs
	 */
	public static ContentItem makeResultsTabs(final ContentItem pContentItem) {
		if ( null != pContentItem ) {
			final Object mainContent = pContentItem.get("mainContent");
			if ( mainContent instanceof List ) {
				final List mainContentList = (List) mainContent;
				final List<ContentItem> results = new ArrayList<>();
				for ( int j = 0; j < mainContentList.size(); j++ ) {
					ContentItem mainContentItem = (ContentItem) mainContentList.get(j);
					if ( ("MainResultsList".equals(mainContentItem.getType()) 
							|| "MainResources".equals(mainContentItem.getType())) 
							&& !"carousel".equals(mainContentItem.get("viewType")) ) {
						mainContentItem.put("href", "resultsList" + j);
						results.add(mainContentItem);
					}
				}
				if ( 1 < results.size() ) {
					pContentItem.put("mainContent", createResults(results, mainContentList));
				} else {
					pContentItem.put("mainContent", mainContent);
				}
			}
		}
		return pContentItem;
	}
	
	/**
	 * Accumulate items. If <code>pContainer</code> is not null, adds
	 * <code>pItemsToAdd</code> to it and return <code>pContainer</code>.
	 * Otherwise create new collection
	 *
	 * @param pContainer the container
	 * @param pItemsToAdd the items to add
	 * @return the collection
	 */
	public static Collection accumulateItems(Collection pContainer, Collection pItemsToAdd) {
		Collection resultCollection;
		if (pContainer == null) {
			resultCollection = new LinkedHashSet<>(pItemsToAdd);
		} else {
			resultCollection = pContainer;
			if (pItemsToAdd != null) {
				resultCollection.addAll(pItemsToAdd);
			}
		}
		return resultCollection;
	}
	
	/**
	 * Creates the results.
	 *
	 * @param results the results
	 * @param mainContentList the main content list
	 * @return the list
	 */
	private static List<Object> createResults(final List<ContentItem> results, final List mainContentList) {
		final List<Object> mainWithTabs = new ArrayList<Object>();
		ContentItem firstResult = results.get(0);
		for ( Object mainContentItemObject: mainContentList ) {
			if ( null != firstResult && firstResult == mainContentItemObject ) {
				ContentItem tabs = new BasicContentItem("MainTabs");
				List<Object[]> tabNames = new ArrayList<Object[]>();
				for ( ContentItem contentItem: results ) {
					tabNames.add(new Object[]{contentItem.get("href"), contentItem.get("name")});
					contentItem.put("inside", "true");
				}
				tabs.put("tabs", tabNames);
				tabs.put("tabContent", results);
				mainWithTabs.add(tabs);
				firstResult = null;
			} else if ( !results.contains(mainContentItemObject)) {
				mainWithTabs.add(mainContentItemObject);
			}
		}
		return mainWithTabs;
	}
	
	public static String seoId(final RepositoryItem pItem) {
		return (null == pItem) ? "" : seoName((String)pItem.getPropertyValue("displayName")); 
	}
	
	public static String seoName(final String pValue) {
		return StringUtils.isBlank(pValue) ? "" : pValue.replaceAll(" ", "-").replaceAll("/", "-").replaceAll("\\?", "-")
				.replaceAll("\"", "").replaceAll("\\.", "-").replaceAll("_", "-").replaceAll(",", "").replaceAll("#", "-");
	}

	public static String prodLink(final String pId, final String pDisplayName) {
		return new StringBuilder("/product/").append(pId).toString(); 
	}


	/**
	 * returns mainContent item sub item
	 *
	 * @param pContentItem
	 * @param pKey
	 * @return
	 */

	public static ContentItem getMainContentSubItem(ContentItem pContentItem, String pKey) {
		ContentItem result = null;
		List mainContent = (List) pContentItem.get(MAIN_CONTENT);
		for (Object oContentItem : mainContent) {
			if (oContentItem instanceof ContentItem) {
				ContentItem contentItem = (ContentItem) oContentItem;
				if (pKey.equals(contentItem.getType())) {
					return contentItem;
				}
			}
		}
		return result;
	}


	/**
	 * Generate meta tag value.
	 *
	 * @param pContentItem    Endeca content item
	 * @param pKey            key for meta value on category item
	 * @param pDefaultMetaKey key for default content meta tag value
	 * @return Boolean
	 */
	public static String generateMeta(ContentItem pContentItem, String pKey, String pDefaultMetaKey) {
		String meta = (String) pContentItem.get(pDefaultMetaKey);
		RefinementBreadcrumb firstRefinement = getFirstRefinement(pContentItem);
		if (firstRefinement != null) {
			String aMeta = (String) firstRefinement.getProperties().get("category." + pKey);
			if (aMeta != null) {
				meta = aMeta;
			}
		}
		return meta;
	}

	/**
	 * Parse content item and return RefinementBreadcrumb Endeca object.
	 *
	 * @param pContentItem Endeca content item
	 * @return RefinementBreadcrumb
	 */
	private static RefinementBreadcrumb getFirstRefinement(ContentItem pContentItem) {
		RefinementBreadcrumb firstRefinement = null;
		List mainContent = (List) pContentItem.get("mainContent");
		for (Object obj : mainContent) {
			ContentItem item = (ContentItem) obj;
			if ("Breadcrumbs".equals(item.getType())) {
				List itemList = (List) item.get("refinementCrumbs");
				if (itemList != null && itemList.size() > 0) {
					firstRefinement = (RefinementBreadcrumb) itemList.get(0);
				}
				break;
			}
		}
		return firstRefinement;
	}
	
	/**
	 * Parse navigation state and return N parameter for browse redirect.
	 * 
	 * 
	 */
	/**
	 * Parse navigation state and return N parameter for browse redirect.
	 * @param pUrl url to parse
	 * @param pParam param name to return
	 * @return param value
	 */
	public static String  getParamByNameFromUrl(String pUrl, String pParam){
		String result = null;
		if (StringUtils.isNotBlank(pUrl)&& StringUtils.isNotBlank(pParam)){
			result = VSGUrlUtils.getParam(pUrl, pParam);
		}
		return result;
	}
	
	/**
	 * Updates the param with <b> tags. 
	 * @param pText to search and update the param
	 * @param pParam to look for 
	 * @return updated sting with <b> tags
	 */
	
	public static String boldText(String pText, String pParam){
		StringBuilder result = new StringBuilder();
			if ( StringUtils.isBlank(pText) || StringUtils.isBlank(pParam) ) {
			   result.append(pText);
			  } else {
				String text = pText.toLowerCase();
				String param = pParam.toLowerCase();
			   String srch = (param.endsWith("*")) ? param.substring(0, param.length()-1) : param;
			   int i = 0;
			   int j = srch.length();
			   int k = text.indexOf(srch, 0);
			   while ( 0 <= k ) {
			    result.append(pText.substring(i, k));
			    result.append("<b>");
			    result.append(pText.substring(k, k + j));
			    result.append("</b>");
			    i = k + j;
			    k = text.indexOf(srch, i);
			   }
			   if ( i < pText.length() ) {
			    result.append(pText.substring(i));
			   }
			  }
			  return result.toString();
	}

	/**
	 * Encode Java Script characters.
	 * @return encoded string
	 */
	
	public static String escapeEcmaScript(final Object pParam) {
		String param = ( null == pParam ) ? "" : pParam.toString();
		return StringEscapeUtils.escapeEcmaScript(param);
	}

	/**
	 * Encode HTML characters.
	 * @return encoded string
	 */
	public static String getEscapeHtmlString(final Object pParam) {
		String result;
		String param = ( null == pParam ) ? "" : pParam.toString();

		if ( StringUtils.isBlank(param) ) {
			result = param;
		} else {
			result = StringEscapeUtils.escapeHtml4(StringEscapeUtils.unescapeHtml4(param));
			if ( !StringUtils.isBlank(result) ) {
				result = result.replaceAll("\\(", "&#40;");
				result = result.replaceAll("\\)", "&#41;");
				result = result.replaceAll("\\{", "&#123;");
				result = result.replaceAll("\\}", "&#125;");
			}
		}
		return result;
	}

	public static String formatName(String name) {
		if (StringUtils.isNotBlank(name)){
			name = name.replaceAll("/", " / ");
			name = name.replaceAll(",", ", ");
		}
		return name;
	}

	public static String formatLink(String pUrl) {
		String url = "";
		if (StringUtils.isNotBlank(pUrl)) {
			int index = pUrl.indexOf("?");
			if (index > 0) {
				url = pUrl.substring(0, pUrl.indexOf("?"));
			}
		}
		return url;
	}
	
	public static String contactInfoAddress(final RepositoryItem pRepositoryItem) {
		final String result;
		if ( null == pRepositoryItem ) {
			result = "";
		} else {
			final StringBuilder sb = new StringBuilder();
			String del = "<br/>";
			append(sb, (String) pRepositoryItem.getPropertyValue("address1"), del);
			append(sb, (String) pRepositoryItem.getPropertyValue("address2"), del);
			int address = append(sb, (String) pRepositoryItem.getPropertyValue("address3"), del);
			int city = append(sb, (String) pRepositoryItem.getPropertyValue("city"), del);
			if ( address < sb.length() ) {
				del = ",&nbsp;";
			}
			append(sb, (String) pRepositoryItem.getPropertyValue("state"), del);
			if ( city < sb.length() ) {
				del = "&nbsp;";
			}
			String zip;
			try {
				zip = (String) pRepositoryItem.getPropertyValue("postalCode");
			} catch (Exception e1) {
				try {
					zip = (String) pRepositoryItem.getPropertyValue("zip");
				} catch (Exception e2) {
					zip = "";
				}
			}
			append(sb, zip, del);
			result = sb.toString();
		}
		return result;
	}

	public static String addTimeMeridiem(String hours){
        String hoursMerid;
        if (StringUtils.isNotBlank(hours)){
            hoursMerid = hours.replace(" ", "");
            hoursMerid = hoursMerid.replace("-", " AM - ") + " PM";
        } else {
            hoursMerid = "";
        }
        return hoursMerid;
    }
	
	private static int append(final StringBuilder sb, final String value, final String del) {
		if ( StringUtils.isNotBlank(value) ) {
			if ( null != del && 0 < sb.length() ) {
				sb.append(del);
			}
			sb.append(value);
		}
		return sb.length();
	}

	/**
	 * Join a collection into string by separator
	 * @param pIterable an iterable
	 * @param separator a separator
	 * @return result string
	 */
	public static String join(Iterable pIterable, String separator) {
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for(Object element : pIterable){
			if(first){
				first = false;
			} else {
				result.append(separator);
			}

			result.append(String.valueOf(element));
		}

		return result.toString();
	}
	
    public static String encodeURI(String value) throws UnsupportedEncodingException {
        return URLEncoder.encode(value, "UTF-8").replace("+", "%20").replace("%21", "!").replace("%27", "'").replace("%28", "(").replace("%29", ")")
                        .replace("%7E", "~");
    }

}
