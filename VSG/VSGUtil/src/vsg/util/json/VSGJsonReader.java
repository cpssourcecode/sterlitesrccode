package vsg.util.json;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;

import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

public class VSGJsonReader {

    private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(VSGJsonReader.class);

    public static JSONObject readJsonFromUrl(String pUrl) throws IOException, JSONException {
        // "http://ip.jsontest.com"
        String jsonText = IOUtils.toString(new URL(pUrl), Charset.forName("UTF-8"));
        JSONObject json = new JSONObject(jsonText);
        return json;
    }

    public static JSONArray readJsonArrayFromUrl(String pUrl) throws IOException, JSONException {
        String jsonText = IOUtils.toString(new URL(pUrl), Charset.forName("UTF-8"));
        JSONArray jsonArray = new JSONArray(jsonText);
        return jsonArray;
    }

    public static JSONArray readJsonArrayFromUrlNew(String pUrl) throws IOException, JSONException {

        String jsonText = null;
        try (AsyncHttpClient asyncHttpClient = new AsyncHttpClient();) {
            final Request request = createRequest(pUrl);
            Future<Response> future = asyncHttpClient.executeRequest(request);
            Response response = future.get();
            String responseBody = response.getResponseBody();
            jsonText = responseBody;

        } catch (IOException e) {
            mLogging.logError(e);
        } catch (InterruptedException e) {
            mLogging.logError(e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            mLogging.logError(e);
        }

        JSONArray jsonArray = null;
        if (null != jsonText) {
            jsonArray = new JSONArray(jsonText);
        }
        return jsonArray;
    }

    public static JSONObject readJsonResponseFromUrl(String pUrl, JsonObject input) throws IOException, JSONException {

        String jsonText = null;
        try (AsyncHttpClient asyncHttpClient = new AsyncHttpClient();) {
            final Request request = createPOSTRequest(pUrl, input);
            mLogging.logDebug("Request == " + request);
            Future<Response> future = asyncHttpClient.executeRequest(request);
            Response response = null;
            try {
                response = future.get(10000, TimeUnit.MILLISECONDS);
                mLogging.logDebug("response status code :: " + response.getStatusCode());
                mLogging.logDebug("response status text :: " + response.getStatusText());
                mLogging.logDebug("response headers  :: " + response.getHeaders());
                mLogging.logDebug("response content type :: " + response.getContentType());
            } catch (TimeoutException e) {
                mLogging.logError(e);
            }

            String responseBody = response.getResponseBody();
            jsonText = responseBody;
            // mLogging.logDebug("jsontext == " + jsonText);

        } catch (IOException e) {
            mLogging.logError(e);
        } catch (InterruptedException e) {
            mLogging.logError(e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            mLogging.logError(e);
        }

        JSONObject jsonObject = null;
        if (null != jsonText) {
            jsonObject = new JSONObject(jsonText);
        }
        return jsonObject;
    }

    public static JsonObject readJsonObjectFromUrl(String pUrl, JsonObject pJsonObject, int pTimeOut, int pSoTimeout) throws Exception {

        HttpParams httpParams = new BasicHttpParams();
        if (pTimeOut > 0) {
            HttpConnectionParams.setConnectionTimeout(httpParams, pTimeOut);
        }
        if (pSoTimeout > 0) {
            HttpConnectionParams.setSoTimeout(httpParams, pSoTimeout);
        }

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);

        HttpPost request = new HttpPost(pUrl);
        StringEntity params = new StringEntity(pJsonObject.toString());
        request.addHeader("content-type", "application/json");
        request.addHeader("accept", "application/json");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);
        if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
        }

        try (JsonReader reader = Json.createReader(new InputStreamReader(response.getEntity().getContent()));) {
            return reader.readObject();
        }
    }

    // public static JsonObject readJsonObjectFromUrl(String pUrl, JsonObject pJsonObject) throws Exception {
    //
    // CloseableHttpClient httpClient = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
    // HttpPost request = new HttpPost(pUrl);
    // StringEntity params = new StringEntity(pJsonObject.toString());
    // request.addHeader("content-type", "text/json");
    // request.addHeader("accept", "application/json");
    // request.setEntity(params);
    // CloseableHttpResponse response = httpClient.execute(request);
    // if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
    // throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
    // }
    //
    // JsonReader reader = Json.createReader(new InputStreamReader((response.getEntity().getContent())));
    // return reader.readObject();
    //
    // }

    /**
     * Test URL = http://172.18.0.171:8080/drp/statement/for/100551?startDate=1437475415467&endDate=1469097815467
     *
     * @param pUrl
     *            URL
     * @return Request
     */
    private static Request createRequest(String pUrl) {
        String body = "";
        byte[] requestbody = body.getBytes();

        final Request request = new RequestBuilder("GET").setUrl(pUrl).setContentLength(requestbody.length).setBody(requestbody)
                        .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8").addHeader("Accept-Encoding", "gzip,deflate")
                        .build();
        return request;
    }

    private static Request createPOSTRequest(String pUrl, JsonObject pJsonObject) {
        mLogging.logDebug("request body == " + pJsonObject.toString());
        final Request request = new RequestBuilder("POST").setUrl(pUrl).setHeader("Content-Type", "application/json").setBody(pJsonObject.toString()).build();
        return request;
    }

}
