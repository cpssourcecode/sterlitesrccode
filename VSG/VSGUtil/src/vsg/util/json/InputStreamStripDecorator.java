package vsg.util.json;

/**
 * @author Andy Porter
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class InputStreamStripDecorator extends InputStream {

	private static Set<Character> sSkipChars;

	static {
		sSkipChars = new HashSet<Character>();
//		sSkipChars.add(' ');
		sSkipChars.add('\r');
		sSkipChars.add('\n');
		sSkipChars.add('\t');

	}

	private InputStream mIS;

	public InputStreamStripDecorator(InputStream pIS) {
		mIS = pIS;
	}

	@Override
	public int read() throws IOException {
		int ch;
		do {
			ch = mIS.read();
		} while (sSkipChars.contains((char) ch) && ch != -1);
		return ch;
	}

}
