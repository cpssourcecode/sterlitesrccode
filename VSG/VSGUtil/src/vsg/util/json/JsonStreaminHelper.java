package vsg.util.json;

/**
 * @author Andy Porter
 */

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JsonStreaminHelper {

	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(JsonStreaminHelper.class);

	public static void main(String[] args) throws IOException {
		InputStreamStripDecorator dec = new InputStreamStripDecorator(new FileInputStream(new File("d:\\Tasks\\CPS\\160712\\CPS-2370\\160720\\Local_debug\\2\\CPS_Catalog_Export_152507192016.json")));
		gotoToken(dec, "\"items\": [");
		String item;
		int i = 0;
		int k = 0;
		while ((item = nextItem(dec)) != null) {
			if (mLogging.isLoggingDebug()) {
				mLogging.logDebug("CATALOG: " + item);
			}
			i++;
			k++;
			if (i == 10) {
				i = 0;
				if (mLogging.isLoggingDebug()) {
					mLogging.logDebug("k: " + k);
				}

			}
		}

		dec.close();
	}

	public static String nextItem(InputStream pIS) throws IOException {
		boolean isOpen = false;
		boolean isInString = false;
		StringBuffer itemBuf = new StringBuffer();
		int ch;
		int prevCh = -1;
		int prevPrevCh = -1;
		int level = 0;
		while ((ch = pIS.read()) != -1) {
			switch (ch) {
				case '"':
					if (!(isInString && ((char) prevCh == '\\')&& ((char) prevPrevCh != '\\') ) ) {
						isInString = !isInString;
						if (mLogging.isLoggingDebug()) {
							if (isInString) {
								mLogging.logDebug("isInString: prevCh:" + String.valueOf((char) prevCh) + ", ch:" + String.valueOf((char) ch));
							} else {
								mLogging.logDebug("OutString: prevCh:" + String.valueOf((char) prevCh) + ", ch:" + String.valueOf((char) ch));
							}
						}

					} else {
						if (mLogging.isLoggingDebug()) {
							mLogging.logDebug("NO switch isInString:" + isInString + ", prevCh:" + String.valueOf((char) prevCh) + ", ch:" + String.valueOf((char) ch));
							mLogging.logDebug(itemBuf.toString());
						}
					}
					break;
				case '{':
					if (!isInString) {
						level++;
						if (mLogging.isLoggingDebug()) {
							mLogging.logDebug("level UP:" + level + "  prevCh:" + String.valueOf((char) prevCh) + ", ch:" + String.valueOf((char) ch));
						}
					}
					if (!isOpen) {
						isOpen = true;
					}
					break;
				case '}':
					if (!isInString) {
						level--;
						if (mLogging.isLoggingDebug()) {
							mLogging.logDebug("level down:" + level + "  prevCh:" + String.valueOf((char) prevCh) + ", ch:" + String.valueOf((char) ch));
						}
					}
					break;
			}
			if (isOpen) {
				itemBuf.append((char) ch);
				if (level == 0) {
					break;
				}
			}
			prevPrevCh = prevCh;
			prevCh = ch;
		}

		return itemBuf.length() == 0 ? null : itemBuf.toString();
	}


	public static boolean gotoToken(InputStream pIS, String pToken) throws IOException {
		boolean isFinded = false;
		char[] buf = new char[pToken.length()];
		init(buf);
		int ch;
		while ((ch = pIS.read()) != -1) {
			shift(buf, (char) ch);
			if (match(buf, pToken)) {
				isFinded = true;
				break;
			}
		}
		return isFinded;
	}

	private static void init(char[] pBuf) {
		for (int i = 0; i < pBuf.length; i++) {
			pBuf[i] = '~';
		}
	}

	private static void shift(char[] pBuf, char pVal) {
		int i = 1;
		for (; i < pBuf.length; i++) {
			pBuf[i - 1] = pBuf[i];
		}
		pBuf[i - 1] = pVal;
	}

	private static boolean match(char[] pBuf, String pAnchor) {
		return pAnchor.equals(String.valueOf(pBuf));
	}

}