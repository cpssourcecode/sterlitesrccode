package vsg.util;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import atg.core.util.ResourceUtils;
import atg.nucleus.GenericService;

/**
 * Class for form fields validation based on rules.
 * Rules could be set as
 * rules=address_address1:address1RegExp|Error1028,\
 *	address_address2:address2RegExp|Error1029,\
 *	address_city:cityRegExp|Error1030,\
 *	address_postalCode:zipcodeRegExp|Error1032,\
 *	address_phoneNumber:phoneRegExp|Error1033
 * 
 * address_address1:address1RegExp|Error1028
 * FORM_FIELD_NAME:REGEXP_PROPERTY|ERROR_PROPERTY
 * FORM_FIELD_NAME - form field name for validation.
 * REGEXP_PROPERTY - Property name {@see vsg.util.RegularExpressions.properties} that value is RegExp to match with field name.
 * ERROR_PROPERTY - Property name from file used by calling form that value is Error Message to show when field value does not matches RegExp.
 * 
 * Order of validation will be same as order of rules.
 * 
 * @author v.ivus
 *
 */
public class VSGValidator extends GenericService {
	
	/**
	 * Regular Expressions property file.
	 */
	static final private String VSG_REG_EXP = "vsg.util.RegularExpressions";
	
	/**
	 * Regular Expressions resource bundle.
	 */
	final private ResourceBundle regularExpBundle = ResourceBundle.getBundle(VSG_REG_EXP);

	/**
	 * Parsed rules.
	 */
	final private Map<String, RuleItem> mRuleItems = new LinkedHashMap<String, RuleItem>();

	/**
	 * Validation rules property.
	 */
	private List<String> mRules = new ArrayList<String>();

	/**
	 * Returns validation rules property.
	 */
	public List<String> getRules() {
		return mRules;
	}

	/**
	 * Set validation rules property.
	 */
	public void setRules(List<String> rules) {
		mRules = rules;
		parseRules();
	}

	/**
	 * Form Validation Method
	 * 
	 * This method for each rule field name find entity in pValues map of <keys:fieldName, values:fieldValue>.
	 * If such entity found, validate field value to match rule RegExp.
	 * 
	 * @param pValues map of <keys:fieldName, values:fieldValue>.
	 * @param pStopOnFirstError stop validation loop when first error if true, when false process all rules.
	 * @return If validation success empty map. When validation fails map of errorCode as key, field name as value.
	 **/
	public Map<String, String> validateForm(Map<String, Object> pValues, boolean pStopOnFirstError) {
		final Map<String, String> errorCodes = new LinkedHashMap<String, String>();
		for ( Entry<String, RuleItem> rule: mRuleItems.entrySet() ) {
			String key = rule.getKey();
			Object value = pValues.get(key);
			if ( value instanceof String ) {
				RuleItem ruleItem = rule.getValue();
				String regExp = ruleItem.getExpression();
				String errorCode = ruleItem.getCode();
				if ( isLoggingDebug() ) {
					logDebug(new StringBuilder(256).append("\n\tValue Key = ").append(key).append("\n\tFieldValue = ")
							.append((String) value).append("\n\tRegExp) = ").append(regExp)
							.append("\n\tErrorCode = ").append(errorCode).toString());
				}
				if ( validateFormField((String) value, regExp) ) {
					if ( isLoggingDebug() ) {
						logDebug("OK.");
					}
				} else {
					errorCodes.put(key, errorCode);
					if ( pStopOnFirstError ) {
						break;
					}
					if ( isLoggingDebug() ) {
						logDebug("Error.");
					}
				}
			}
		}
		return errorCodes;
	}

	/**
	 * Form Validation Method
	 * 
	 * This method for each rule field name find entity in pValues map of <keys:fieldName, values:fieldValue>.
	 * If such entity found, validate field value to match rule RegExp.
	 * Stop validation loop at first error.
	 * 
	 * @param pValues map of <keys:fieldName, values:fieldValue>.
	 * @return If validation success empty map. When validation fails map of errorCode as key, field name as value.
	 **/
	public Map<String, String> validateForm(Map<String, Object> pValues) {
		return validateForm(pValues, true);
	}

	/**
	 * Form Validation Method
	 * 
	 * This method for each rule field name find entity in pValues map of <keys:fieldName, values:fieldValue>.
	 * If such entity found, validate field value to match rule RegExp.
	 * 
	 * @param pValues map of <keys:fieldName, values:fieldValue>.
	 * @param pStopOnFirstError stop validation loop when first error if true, when false process all rules.
	 * @param pFormErrors object implements addError method to add errors.
	 **/
	public void validateForm(Map<String, Object> pValues, boolean pStopOnFirstError, VSGFormErrors pFormErrors) {
		for ( Entry<String, RuleItem> rule: mRuleItems.entrySet() ) {
			String key = rule.getKey();
			Object value = pValues.get(key);
			if ( value instanceof String ) {
				RuleItem ruleItem = rule.getValue();
				String regExp = ruleItem.getExpression();
				String errorCode = ruleItem.getCode();
				if ( isLoggingDebug() ) {
					logDebug(new StringBuilder(256).append("\n\tValue Key = ").append(key).append("\n\tFieldValue = ")
							.append((String) value).append("\n\tRegExp) = ").append(regExp)
							.append("\n\tErrorCode = ").append(errorCode).toString());
				}
				if ( validateFormField((String) value, regExp) ) {
					if ( isLoggingDebug() ) {
						logDebug("OK.");
					}
				} else {
					pFormErrors.addError("[" + errorCode + "]", key);
					if ( pStopOnFirstError ) {
						break;
					}
					if ( isLoggingDebug() ) {
						logDebug("Error.");
					}
				}
			}
		}
	}

	/**
	 * Form Validation Method
	 * 
	 * This method for each rule field name find entity in pValues map of <keys:fieldName, values:fieldValue>.
	 * If such entity found, validate field value to match rule RegExp.
	 * Stop validation loop at first error.
	 * 
	 * @param pValues map of <keys:fieldName, values:fieldValue>.
	 * @param pFormErrors object implements addError method to add errors.
	 **/
	public void validateForm(Map<String, Object> pValues, VSGFormErrors pFormErrors) {
		validateForm(pValues, true, pFormErrors);
	}
	
	/**
	 * Check if value matched to expression.
	 * 
	 * @param pValue value to validate.
	 * @param pRegExp expression property name to match.
	 * @return true if value matched to expression, when does not matched false.
	 */
	private boolean validateFormField(String pValue, String pRegExp) {
		String regExp = ResourceUtils.getMsgResource(pRegExp, VSG_REG_EXP, regularExpBundle);
		return validateValue(pValue, regExp);
	}
	
	/**
	 * Check if value matched to expression.
	 * 
	 * @param pValue value to validate.
	 * @param pRegExp expression to match.
	 * @return true if value matched to expression, when does not matched false.
	 */
	static public boolean validateValue(String pValue, String pRegExp) {
		return validatePattern(pValue, Pattern.compile(pRegExp));
	}
	
	/**
	 * Check if value matched to pattern.
	 * 
	 * @param pValue value to validate.
	 * @param pRegExp pattern to match.
	 * @return true if value matched to pattern, when does not matched false.
	 */
	static public boolean validatePattern(String pValue, Pattern pRegExp) {
		return pRegExp.matcher(pValue).matches();
	}

	/**
	 * Parse rules.
	 */
	private void parseRules () {
		List<String> validationRules = getRules();
		mRuleItems.clear();
		for (String rule : validationRules) {
			//For each rule, parse out the parts and add it to the map
			String mappedRule = rule.substring(0, rule.length());
			int closingKeyChar = mappedRule.indexOf(":");
			String key = "";
			String exp = "";
			String errCode = "";
			if ( -1 < closingKeyChar ) {
				key = mappedRule.substring(0, closingKeyChar);
				exp = mappedRule.substring(closingKeyChar + 1, mappedRule.indexOf("|"));
				errCode = mappedRule.substring(mappedRule.indexOf("|") + 1, rule.length());
				mRuleItems.put(key, new RuleItem(exp, errCode));
			}
			if ( isLoggingDebug() ) {
				logDebug(new StringBuilder(256).append("\n\tKey = ").append(key)
						.append("\n\tExpression = ").append(exp)
						.append("\n\tErrorCode = ").append(errCode).toString());
			}
		}
	}
	
	/**
	 * To store parsed rule item.
	 */
	private class RuleItem {
		
		private String mExpression;
		
		private String mCode;
		
		public RuleItem(String pExpression, String pCode) {
			mExpression = pExpression;
			mCode = pCode;
		}
		
		public String getExpression() {
			return mExpression;
		}
	
		public String getCode() {
			return mCode;
		}
	}

}
