package vsg.util;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.client.AsyncHttpClientConfig.Builder;
import com.ning.http.client.AsyncHttpProvider;
import com.ning.http.client.providers.netty.NettyAsyncHttpProvider;
import com.ning.http.client.providers.netty.NettyAsyncHttpProviderConfig;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

public class AsyncHttpClientUtils extends GenericService {

	private AsyncHttpClient asyncHttpClient;
	private boolean mAllowPoolingConnection;
	
	public AsyncHttpClient getAsyncHttpClient() {
		if (asyncHttpClient == null) {
			synchronized (this) {
				Builder builder = new AsyncHttpClientConfig.Builder();
				if(isAllowPoolingConnection()) {
					builder.setAllowPoolingConnection(true);
				}
				NettyAsyncHttpProviderConfig config = new NettyAsyncHttpProviderConfig();

				AsyncHttpProvider provider = new NettyAsyncHttpProvider(builder.build());
				asyncHttpClient = new AsyncHttpClient(provider, builder.build());
			}
		}
		return asyncHttpClient;
	}

	@Override
	public void doStopService() throws ServiceException {
		super.doStopService();
		synchronized (this) {
			asyncHttpClient = null;
		}
	}
	
	public boolean isAllowPoolingConnection() {
		return mAllowPoolingConnection;
	}

	public void setAllowPoolingConnection(boolean pAllowPoolingConnection) {
		mAllowPoolingConnection = pAllowPoolingConnection;
	}

}