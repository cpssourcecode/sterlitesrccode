package vsg.util;

/**
 * Interface to use as input value for methods needs to add form errors.
 * 
 * @author v.ivus
 *
 */
public interface VSGFormErrors {
	
	public void addError(String pErrorCode, String pPath);
}
