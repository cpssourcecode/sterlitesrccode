package vsg.util;

public class VSGConstants {
		
	public final static  String FIRST_NAME = "firstName";
	public final static  String LAST_NAME = "lastName";
	public final static  String EMAIL = "email";
	public final static  String CONFIRM_EMAIL = "confirmEmail";
	public final static  String LOGIN = "login";
	public final static  String CONFIRM_LOGIN = "confirmLogin";
	public final static  String PSWD = "password";
	public final static  String CONFIRM_PSWD = "confirmPassword";
	public final static  String SECURITY_ANS = "securityAns";
	public final static  String TERMS = "terms";
	public final static  String DAYTIME_PH_NUM = "daytimeTelephoneNumber";
	public final static  String CUSTOMER_SERVICE = "customerService";
	public final static  String SL_FLAG = "slFlag";
	public final static  String ACCOUNT_TYPE = "accountType";
	public final static  String PARENT_ORG = "parentOrganization";
	public final static  String PROFILE_ID = "profileId";
	public final static  String CREATE_NEW ="createNew";
	public final static  String LOGIN_URL ="loginSuccessURL";
	public final static  String USER_ID = "user_id";
	public final static  String CITY_PRPTY = "city";
	public final static  String COUNTY_PRPTY = "county";
	public final static  String COUNTRY_PRPTY = "country";
	public final static  String POSTALCODE_PRPTY = "postalCode";
	public final static  String STATE_PRPTY = "state";
	public final static  String PROMO_SIGNUP_PRPTY = "promoEmailSignUp";
	public final static  String PROMO_HTML_PRPTY = "promoEmailHTML";
	public final static  String PROMO_TEXT_PRPTY = "promoEmailText";
	
	public final static  String SUCCESS_URL = "successURL";
	public final static  String CREATE_SUCCESS_URL = "createSuccessURL";
	
	public final static  String SHIP_FIRSTNAME = "shippingAddress.firstName";
	public final static  String SHIP_LASTNAME = "shippingAddress.lastName";
	public final static  String SHIP_COMPANY = "shippingAddress.companyName";
	public final static  String SHIP_ADDR1 = "shippingAddress.address1";
	public final static  String SHIP_ADDR2 = "shippingAddress.address2";
	public final static  String SHIP_CITY = "shippingAddress.city";
	public final static  String SHIP_COUNTY = "shippingAddress.county";
	public final static  String SHIP_COUNTRY = "shippingAddress.country";
	public final static  String SHIP_POSTALCODE = "shippingAddress.postalCode";
	public final static  String SHIP_STATE = "shippingAddress.state";
	
	public final static  String BILL_FIRSTNAME = "billingAddress.firstName";
	public final static  String BILL_LASTNAME = "billingAddress.lastName";
	public final static  String BILL_COMPANY = "billingAddress.companyName";
	public final static  String BILL_ADDR1 = "billingAddress.address1";
	public final static  String BILL_ADDR2 = "billingAddress.address2";
	public final static  String BILL_CITY = "billingAddress.city";
	public final static  String BILL_COUNTY = "billingAddress.county";
	public final static  String BILL_COUNTRY = "billingAddress.country";
	public final static  String BILL_POSTALCODE = "billingAddress.postalCode";
	public final static  String BILL_STATE = "billingAddress.state";
	
	public final static  String HOME_ADDR1 = "homeAddress.address1";
	public final static  String HOME_ADDR2 = "homeAddress.address2";
	public final static  String HOME_CITY = "homeAddress.city";
	public final static  String HOME_COUNTY = "homeAddress.county";
	public final static  String HOME_COUNTRY = "homeAddress.country";
	public final static  String HOME_POSTALCODE = "homeAddress.postalCode";
	public final static  String HOME_STATE = "homeAddress.state";
	
	public final static  String EDIT_ADDR1 = "address1";
	public final static  String EDIT_ADDR2 = "address2";
	public final static  String EDIT_CITY = "city";
	public final static  String EDIT_COUNTY = "county";
	public final static  String EDIT_COUNTRY = "country";
	public final static  String EDIT_POSTALCODE = "postalCode";
	public final static  String EDIT_STATE = "state";
	
	public final static  String AVS_DPV_CODED = "dpvCoded";
	public final static  String AVS_ERR_CODE = "errorCode";
	public final static  String AVS_ERR_MSG = "errorMessage";
	
	public final static  String VSG_LOGIN_PREFIX = "vsg_";
	public final static  String WIZ_LOGIN_PREFIX= "wiz_";
	
	public final static  String ADDRESS_TYPE_HOME = "homeAddress";
	public final static  String ADDRESS_TYPE_BILLING = "billingAddress";
	public final static  String ADDRESS_TYPE_SHIPPING = "shippingAddress";
	public final static  String ADDRESS_TYPE_SECONDARY = "secondaryAddress";	
	
	public final static String LINK_TO_SITE = "linkToSite";
	public final static String RECOVER_PSWD_LINK = "/account/account-password-link.jsp";
}