package vsg.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * This class is responsible for containing method which are required for basic string
 * operation which is not available for ATG String Util.
 * @author root
 *
 */
public class StringUtils
{
	private static StringUtils mStringUtils = null;
	private static Object obj = new Object();

	/**
	 * Private method for singleton class.
	 */
	private StringUtils()
	{

	}

	/**
	 * This method returns only one instance of StringUtil class.
	 * @return
	 */
	public static StringUtils getInstance()
	{
		synchronized(obj)
		{
			if(mStringUtils == null)
			{
				mStringUtils = new StringUtils();
			}

			return mStringUtils;
		}
	}

	/**
	 * This method create a formatted message for single object parameter from the resource bundle and key passed as an argument.
	 * @param pBundle
	 * @param pKey
	 * @param pArguments
	 * @return
	 */
	public static String createMessageForSingleArgument(ResourceBundle pBundle , String pKey , Object[] pArguments )
	{
		String formattedMessage = null;
		String message = getMessageFromBundle(pBundle, pKey);
		if(message != null)
		{
			MessageFormat.format(message, pArguments);
		}
		return formattedMessage;
	}

	/**
	 * This method returns a message from ResourceBundle.
	 * @param pBundle
	 * @param key
	 * @return
	 */
	public static String getMessageFromBundle(ResourceBundle pBundle , String key)
	{
		String message = pBundle.getString(key);
		return message;
	}

}
