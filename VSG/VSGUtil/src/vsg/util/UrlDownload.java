package vsg.util;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class UrlDownload {

	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(UrlDownload.class);

	public static byte[] getFile(String pUrl, String pContentType) throws IOException {
        InputStream is = null;
		byte[] fileBytes = null;
		URL url = null;
		try {
			url = new URL(pUrl);
			URLConnection urlConn = url.openConnection();
			if (!urlConn.getContentType().equalsIgnoreCase(pContentType)) {
				mLogging.logError("FAILED.\n[Sorry. This is not a requested content type.]" + pContentType);
			} else {
				is = url.openStream();
				fileBytes = IOUtils.toByteArray(is);
			}
		} catch (MalformedURLException e) {
			mLogging.logError(e);
		} catch (IOException e) {
			mLogging.logError(e);
		} finally {
			if (is != null) {
				is.close();
			}
		}
		return  fileBytes;
	}

}
