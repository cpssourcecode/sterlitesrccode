package vsg.util;

import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;

public class VSGSiteUtils extends GenericService {

	static final public String PROTOCOL = "http://";
	
	static public String getSiteUrl(String pDefaultValue) {
		String siteUrl = pDefaultValue;
		Site currentSite = SiteContextManager.getCurrentSite();
		if ( null != currentSite ) {
			siteUrl = PROTOCOL + (String) currentSite.getPropertyValue("productionURL");
		}
		return siteUrl;
	}

}
