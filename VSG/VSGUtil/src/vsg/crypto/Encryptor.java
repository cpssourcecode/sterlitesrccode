package vsg.crypto;


public interface Encryptor {

	/**
	 * Class version string.
	 */
  
	//-------------------------------------
	// Methods.
	//-------------------------------------

	/**
	 * Sets the <code>pValue</code> property. This must get called before any
	 * encryption operations take place.
	 *
	 * @param pValue The <code>byte[]</code> to set as the key property.
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public void acceptEncryptKey(byte[] pValue) throws EncryptorException;

	/**
	 * Sets the <code>key</code> property. This must get called before any
	 * encryption operations take place.
	 *
	 * @param pValue The <code>byte[]</code> to set as the key property.
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public void acceptDecryptKey(byte[] pValue) throws EncryptorException;

	/**
	 * Encrypts <code>pValue</code> string.
	 *
	 * @param pValue String to encrypt.
	 * @return Encrypted string.
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public String encrypt(String pValue) throws EncryptorException;

	/**
	 * Encrypts <code>pValue</code> byte[].
	 *
	 * @param pValue byte[] array to encrypt.
	 * @return encrypted byte[] array
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public byte[] encrypt(byte[] pValue) throws EncryptorException;

	/**
	 * Decrypts encrypted <code>pValue</code> string.
	 *
	 * @param pValue encrypted String
	 * @return decrypted String
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public String decrypt(String pValue) throws EncryptorException;

	/**
	 * Decrypts encrypted <code>pValue</code> byte[].
	 *
	 * @param pValue encrypted array of byte[]
	 * @return decrypted array of byte[]
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public byte[] decrypt(byte[] pValue) throws EncryptorException;
}
