package vsg.crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 * @author VSG
 */
public class AESEncryptor {

	/**
	 * Crypto algorithm name.
	 */
	private static final String ALGORITHM = "AES";

	/**
	 * 128 bit key string.
	 */
	private static final String KEY = "Bar12345Bar12345";

	/**
	 * 16 bytes init vector.
	 */
	private static final String INIT_VECTOR = "RandomInitVector";

	/**
	 * Cipher.
	 */
	private static final String CIPHER = "AES/CBC/PKCS5PADDING";

	/**
	 * Encoding.
	 */
	private static final String ENCODING = "UTF-8";

	/**
	 * Key string.
	 */
	private byte[] mKeyString = null;

	/**
	 * Key specification.
	 */
	private transient SecretKeySpec mKeySpec = null;

	/**
	 * Encrypt cypher.
	 */
	private transient Cipher mEncryptCypher = null;

	/**
	 * Decrypt cypher.
	 */
	private transient Cipher mDecryptCypher = null;

	/**
	 * Init vector.
	 */
	private transient IvParameterSpec mIvParameterSpec = null;

	/**
	 * Encrypt byte array.
	 * 
	 * @param pValue
	 *            the value
	 * @return encode byte array value
	 * @throws EncryptorException
	 *             the encryptor exception
	 */
	public byte[] encrypt(final byte[] pValue) throws EncryptorException {
		try {
			byte[] encrypted = mEncryptCypher.doFinal(pValue);
			return Base64.encodeBase64(encrypted);
		} catch (IllegalBlockSizeException ibse) {
			throw new EncryptorException(ibse);
		} catch (BadPaddingException bpe) {
			throw new EncryptorException(bpe);
		}
	}

	/**
	 * Decrypt byte array.
	 * 
	 * @param pValue
	 *            the value
	 * @return decode byte array
	 * @throws EncryptorException
	 *             the encryptor exception
	 */
	public final byte[] decrypt(final byte[] pValue) throws EncryptorException {
		try {
			return mDecryptCypher.doFinal(Base64.decodeBase64(pValue));
		} catch (IllegalBlockSizeException ibse) {
			throw new EncryptorException(ibse);
		} catch (BadPaddingException bpe) {
			throw new EncryptorException(bpe);
		}
	}

	/**
	 * Initialize AESEncryptor
	 * 
	 * @throws EncryptorException
	 *             the encryptor exception
	 */
	public final void initialization() throws EncryptorException {
		try {
			if (mKeyString == null) {
				mKeyString = KEY.getBytes(ENCODING);
			}
			mIvParameterSpec = new IvParameterSpec(INIT_VECTOR.getBytes(ENCODING));
			mKeySpec = new SecretKeySpec(mKeyString, ALGORITHM);
			mEncryptCypher = Cipher.getInstance(CIPHER);
			mEncryptCypher.init(Cipher.ENCRYPT_MODE, mKeySpec, mIvParameterSpec);
			mDecryptCypher = Cipher.getInstance(CIPHER);
			mDecryptCypher.init(Cipher.DECRYPT_MODE, mKeySpec, mIvParameterSpec);
		} catch (NoSuchAlgorithmException nsae) {
			throw new EncryptorException(nsae);
		} catch (NoSuchPaddingException nspe) {
			throw new EncryptorException(nspe);
		} catch (InvalidKeyException nske) {
			throw new EncryptorException(nske);
		} catch (UnsupportedEncodingException usee) {
			throw new EncryptorException(usee);
		} catch (InvalidAlgorithmParameterException iape) {
			throw new EncryptorException(iape);
		}
	}
}