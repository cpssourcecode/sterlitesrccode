package vsg.crypto;

public class VSGDESEncryptor extends DESEncryptor {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * This method initializes the DES encryptor.
	 * @throws EncryptorException
	 */
	public void initializeEncryptor() throws EncryptorException{
		
		super.doInit();
	}
/**
 * This method encrypts the parameter.
 * @param pValue
 * @return
 * @throws EncryptorException
 */
	public byte[] doEncryptParam(byte[] pValue) throws EncryptorException{
		return super.doEncrypt(pValue);
	}
	/**
	 * This method decrypts the parameters
	 * @param pValue
	 * @return
	 * @throws EncryptorException
	 */
	public byte[]doDecryptParam(byte[]pValue) throws EncryptorException{
		return super.doDecrypt(pValue);
	}

	/**
	 * 
	 */
	@Override
	public byte[] decodeToByteArray(String pValue) throws EncryptorException {
		return super.decodeToByteArray(pValue);
	}
/**
 * 
 */
	@Override
	public String encodeToString(byte[] pValue) throws EncryptorException {
		return super.encodeToString(pValue);
	}
}