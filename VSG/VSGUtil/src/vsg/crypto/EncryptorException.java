package vsg.crypto;

import atg.core.exception.ContainerException;


public class EncryptorException extends ContainerException
{
	//-------------------------------------
	// Constants
	//-------------------------------------

	//-------------------------------------
	// Member Variables
	//-------------------------------------

	//-------------------------------------
	// Properties
	//-------------------------------------
    
	//-------------------------------------
	// Constructors
	//-------------------------------------

	/**
	 * Constructs a new EncryptorException.
	 **/
	public EncryptorException() {
		super();
	}

	/**
	 * Constructs a new EncryptorException with the given 
	 * explanation.
	 *
	 * @param pStr String that describes exception
	 * */
	public EncryptorException(String pStr) {
		super(pStr);
	}

	/**
	 * Constructs a new EncryptorException.
	 * @param pSourceException the initial exception which was the root
	 * cause of the problem
	 **/
	public EncryptorException(Throwable pSourceException) {
		super(pSourceException);
	}

	/**
	 * Constructs a new EncryptorException with the given 
	 * explanation.
	 * @param pStr String that describes exception
	 *
	 * @param pSourceException the initial exception which was the root
	 * cause of the problem
	 **/
	public EncryptorException(String pStr, Throwable pSourceException) {
		super(pStr, pSourceException);
	}
} // end of class
