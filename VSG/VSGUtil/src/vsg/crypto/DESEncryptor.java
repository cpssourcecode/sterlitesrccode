package vsg.crypto;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.CharacterDecoder;
import sun.misc.CharacterEncoder;
import sun.misc.UCDecoder;
import sun.misc.UCEncoder;

/**
 * @author Alex M
 */
public class DESEncryptor extends AbstractEncryptor {
	/**
	 * serial version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Crypto algorithm name.
	 */
	private static final String ALGORITHM = "DES";

	/**
	 * 24 byte key string.
	 */
	private static final String KEY = "J9c&W;!n";

	/** The character encoder. */
	private transient static CharacterEncoder mEncoder = new UCEncoder();

	/** The character decoder. */
	private transient static CharacterDecoder mDecoder = new UCDecoder();

	/**
	 * Enable or disable flag for encryptor' clients
	 * */
	private boolean mEnabled = true;

	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param pEnabled
	 *            the new enabled flag
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * Key string.
	 */
	private byte[] mKeyString = null;

	/**
	 * Key specification.
	 */
	private transient DESKeySpec mKeySpec = null;

	/**
	 * Secret key.
	 */
	private transient SecretKey mKey = null;

	/**
	 * Encrypt cypher.
	 */
	private transient javax.crypto.Cipher mEncryptCypher = null;

	/**
	 * Decrypt cypher.
	 */
	private transient javax.crypto.Cipher mDecryptCypher = null;

	/**
	 * Instantiates a new DES encryptor.
	 */
	public DESEncryptor() {
		super();
	}

	/**
	 * This is two way encryption, so encrypt/decrypt keys are the same.
	 * @param pValue - key
	 * @throws EncryptorException if encryption error occurs
	 */
	@Override
	protected final void doAcceptEncryptKey(byte[] pValue)
			throws EncryptorException {
		acceptKey(pValue);
	}

	/**
	 * This is two way encryption, so encrypt/decrypt keys are the same.
	 * @param pValue - key
	 * @throws EncryptorException if encryption error occurs
	 */
	@Override
	protected final void doAcceptDecryptKey(byte[] pValue)
			throws EncryptorException {
		acceptKey(pValue);
	}

	/**
	 * Initialize the KeySpec.
	 *
	 * @param pValue new key
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	private void acceptKey(byte[] pValue) throws EncryptorException {
		mKeyString = pValue;
	}

	/**
	 * Initialize DESEncrytor.
	 *
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public final void initialize() throws EncryptorException {
		try {
			if (mKeyString == null) {
				mKeyString = KEY.getBytes();
			}

			mKeySpec = new DESKeySpec(mKeyString);
			mKey = new SecretKeySpec(mKeySpec.getKey(), ALGORITHM);
			mEncryptCypher = javax.crypto.Cipher.getInstance(ALGORITHM);
			mEncryptCypher.init(javax.crypto.Cipher.ENCRYPT_MODE, mKey);
			mDecryptCypher = javax.crypto.Cipher.getInstance(ALGORITHM);
			mDecryptCypher.init(javax.crypto.Cipher.DECRYPT_MODE, mKey);
		} catch (NoSuchAlgorithmException nsae) {
			throw new EncryptorException(nsae);
		} catch (NoSuchPaddingException nspe) {
			throw new EncryptorException(nspe);
		} catch (InvalidKeyException nske) {
			throw new EncryptorException(nske);
		}
	}

	/**
	 * Performs encryption of array of bytes.
	 *
	 * @param pValue array of bytes to encrypt
	 * @return encrypted array of bytes
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public byte[] encryptParam(byte[] pValue) throws EncryptorException {
		try {
			return mEncryptCypher.doFinal(pValue);
		} catch (IllegalBlockSizeException ibse) {
			throw new EncryptorException(ibse);
		} catch (BadPaddingException bpe) {
			throw new EncryptorException(bpe);
		}
	}

	/**
	 * Performs decription of array of bytes.
	 *
	 * @param pValue decrypt array of bytes
	 * @return decrypted array of bytes
	 * @throws EncryptorException This exception indicates that a severe error
	 * occured while performing a cryptograpy operation.
	 */
	public byte[] decryptParam(byte[] pValue) throws EncryptorException {
		try {
			return mDecryptCypher.doFinal(pValue);
		} catch (IllegalBlockSizeException ibse) {
			throw new EncryptorException(ibse);
		} catch (BadPaddingException bpe) {
			throw new EncryptorException(bpe);
		}
	}

	/**
	 * Once encrypted, string data may no longer be a string because the
	 * encrypted data is binary and may contain null characters, thus it may
	 * need to be encoded using a encoder such as Base64, UUEncode (ASCII only)
	 * or UCEncode(ASCII independent).
	 * @param pValue Value to encode
	 * @throws EncryptorException This exception indicates that an error
	 * occured while performing a cryptograpy operation.
	 * @return Encoded data
	 */
	@Override
	public String encodeToString(byte[] pValue) throws EncryptorException {
		return mEncoder.encode(pValue);
	}

	/**
	 * Decode to byte array.
	 *
	 * @param pValue - value to decode
	 * @return byte array
	 * @throws EncryptorException if encryption error occurs
	 */
	@Override
	public byte[] decodeToByteArray(String pValue) throws EncryptorException {
		try {
			return mDecoder.decodeBuffer(pValue);
		} catch (IOException ioe) {
			throw new EncryptorException("Failed to decode byte array: ", ioe);
		}
	}

	@Override
	protected void doInit() throws EncryptorException {
		initialize();
	}

	@Override
	protected byte[] doEncrypt(byte[] pValue) throws EncryptorException {
		return encryptParam(pValue);
	}

	@Override
	protected byte[] doDecrypt(byte[] pValue) throws EncryptorException {
		return decryptParam(pValue);
	}
}
