package vsg.droplet;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.adapter.gsa.GSAItem;
import atg.commerce.catalog.comparison.ProductComparisonList;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.commerce.catalog.custom.CustomCatalogTools;

/**
 *
 */
public class ComparisonDroplet extends DynamoServlet {

  /**
   * Items parameter name.
   */
  public static final ParameterName ITEMS_PARAM = ParameterName.getParameterName("items");

  /**
   * Property name parameter name.
   */
  public static final ParameterName FEATURE_LIST_PARAM = ParameterName.getParameterName("featureList");
  
  /**
   * Property value parameter name.
   */
  public static final ParameterName FEATURE_NAME_PARAM = ParameterName.getParameterName("featureName");

  /**
   * Property value parameter name.
   */
  public static final ParameterName FEATURE_TYPE_PARAM = ParameterName.getParameterName("featureType");
  
  /**
   * Property value parameter name.
   */
  public static final ParameterName FEATURE_VALUE_PARAM = ParameterName.getParameterName("featureValue");
  
  /**
   * Product name.
   */
  public static final String OOB_DISPLAY_NAME = "displayName";
  
  /**
   * Values parameter name.
   */
  public static final String VALUES_PARAM = "values";

  /**
   * Output parameter name.
   */
  public static final String OUTPUT_OPARAM = "output";

  /**
   * Empty parameter name.
   */
  public static final String EMPTY_OPARAM = "empty";

  /**
   * Element parameter name.
   */
  public static final String ELEMENT = "element";
  
  /**
   * Error parameter name.
   */
  public static final String ERROR_OPARAM = "error";

  /**
   * Not Available Text
   */
  public static final String NOT_AVAILABLE = "Not Available";

  /**
   * Id property name.
   */
  public static final String ID = "Id";

  /**
   * Price property name.
   */
  public static final String PRICE = "Price";

  /**
   * Item # property name.
   */
  public static final String ITEM_NUMBER = "Item #";
  

  /**
   * Indexes
   */
  	public static final int ITEM_NUMBER_NDX = 0;
  	public static final int PRICE_NDX = 1;
  
  
  	private PriceListManager mPriceListManager;
  	 
  	private String mProfilePriceListPropertyName;
  	 
  	private CustomCatalogTools mCatalogTools;
  	
	public PriceListManager getPriceListManager() {
	return mPriceListManager;
	}
	
	public void setPriceListManager(PriceListManager pPriceListManager) {
		this.mPriceListManager = pPriceListManager;
	}

	  public void setProfilePriceListPropertyName(String pProfilePriceListPropertyName)
	  {
	    this.mProfilePriceListPropertyName = pProfilePriceListPropertyName;
	  }

	  public String getProfilePriceListPropertyName()
	  {
	    return this.mProfilePriceListPropertyName;
	  }
	
	    public CustomCatalogTools getCatalogTools() {
	        return mCatalogTools;
	    }

	    public void setCatalogTools(CustomCatalogTools pCatalogTools) {
	        mCatalogTools = pCatalogTools;
	    }
	  
	public void service(DynamoHttpServletRequest pRequest,DynamoHttpServletResponse pResponse) throws ServletException,IOException {
		
		if (isLoggingDebug()) {
			logDebug("ComparisonDroplet:droplet - start");
		}
		
		LinkedList<ArrayList<String>> outputList = new LinkedList<ArrayList<String>>();
		
		String renderParam;
		
		try {
			String propertyFeatureList = pRequest.getParameter(FEATURE_LIST_PARAM);
			String propertyFeatureType = pRequest.getParameter(FEATURE_TYPE_PARAM);
			String propertyFeatureName = pRequest.getParameter(FEATURE_NAME_PARAM);
			String propertyFeatureValue = pRequest.getParameter(FEATURE_VALUE_PARAM);
			List<ProductComparisonList.Entry> items = (List) pRequest.getObjectParameter(ITEMS_PARAM);
			
			Map<String,ArrayList<String>> features = new HashMap<String,ArrayList<String>>();
			
			//init Item #
			ArrayList<String> myal = new ArrayList<String>();
			myal.add(String.valueOf(ITEM_NUMBER_NDX));
			myal.add(ITEM_NUMBER);
			outputList.add(ITEM_NUMBER_NDX,myal);
			
			//init Price
			myal = new ArrayList<String>();
			myal.add(String.valueOf(PRICE_NDX));
			myal.add(PRICE);
			outputList.add(PRICE_NDX,myal);
			
			
			int prodCount = 0;
			
			// For each product
			for (ProductComparisonList.Entry object : items) {
			
				if (isLoggingDebug()) { logDebug(new StringBuilder().append("Working on: <").append(object.getProduct().getPropertyValue(OOB_DISPLAY_NAME)).append(">").toString()); }
				
				// get the list of features
				List<GSAItem> productFeatures = (List<GSAItem>) (object.getProduct().getPropertyValue(propertyFeatureList));
			
				prodCount+=1;
				int featureCount = 0;
				
				// add the Item # 
				if (isLoggingDebug()) { logDebug(new StringBuilder().append("Have ItemId: <").append(object.getProduct().getPropertyValue(ID)).append(">").toString()); }
				outputList.get(ITEM_NUMBER_NDX).add(object.getProduct().getPropertyValue(ID).toString());
			
				setPriceValue(object, outputList);

				
				// if it has features
				if (productFeatures != null ) {
					if (isLoggingDebug()) { logDebug(new StringBuilder().append("Have feature list: <").append(productFeatures.toString()).append(">").toString()); }
					
					
					// for each feature
					for (GSAItem feature : productFeatures) {
						
						
						if ( propertyFeatureType != null ) {
							if (isLoggingDebug()) { logDebug(new StringBuilder().append("feature : <").append(((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName)).append("> <").append(feature.getPropertyValue(propertyFeatureValue)).append(">").toString()); }
						} else {
							if (isLoggingDebug()) { logDebug(new StringBuilder().append("feature : <").append(feature.getPropertyValue(propertyFeatureName)).append("> <").append(feature.getPropertyValue(propertyFeatureValue)).append(">").toString()); }
						}
						
						featureCount+=1;
						
						// add the feature to the map  
						if ( (propertyFeatureType != null && features.containsKey(((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName)))
								|| (propertyFeatureType == null && features.containsKey(feature.getPropertyValue(propertyFeatureName))) ) {
						//	if (isLoggingDebug()) { logDebug(new StringBuilder().append("Contains Key: <").append(feature.getPropertyValue(propertyFeatureName)).append(">").toString()); }
							
							if ( propertyFeatureType != null ) {
								if (isLoggingDebug()) { logDebug(new StringBuilder().append("Contains Key: <").append(((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName)).append(">").toString()); }
								myal = features.remove( ((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName) );
								myal.add(feature.getPropertyValue(propertyFeatureValue).toString());
								
								features.put(((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName).toString(),myal);
							} else {
								if (isLoggingDebug()) { logDebug(new StringBuilder().append("Contains Key: <").append(feature.getPropertyValue(propertyFeatureName)).append(">").toString()); }
								myal = features.remove(feature.getPropertyValue(propertyFeatureName));
								myal.add(feature.getPropertyValue(propertyFeatureValue).toString());
								
								features.put(feature.getPropertyValue(propertyFeatureName).toString(),myal);
							}
							
						} else {
							
							if ( propertyFeatureType != null ) {
								if (isLoggingDebug()) { logDebug(new StringBuilder().append("Does Not Contain Key: <").append(((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName)).append(">").toString()); }
							} else {
								if (isLoggingDebug()) { logDebug(new StringBuilder().append("Does Not Contain Key: <").append(feature.getPropertyValue(propertyFeatureName)).append(">").toString()); }
							}
							
							
							myal = new ArrayList<String>();
							
							myal.add(String.valueOf(featureCount));
							
							if ( propertyFeatureType != null ) {
								myal.add(((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName).toString());
							} else {
								myal.add(feature.getPropertyValue(propertyFeatureName).toString());
							}
							
							
							if ( isLoggingDebug() ) {
								logDebug(new StringBuffer()
												.append("Product Count: <")
												.append(prodCount)
												.append(">")
												.toString());
							}
							//add blank elements for previously added.
							for ( int i = 1 ; i < prodCount ; i++ ) {
								if ( isLoggingDebug() ) {
									logDebug(new StringBuffer()
													.append("i: <")
													.append(i)
													.append("> added, <")
													.append(NOT_AVAILABLE)
													.append(">")
													.toString());
								}
								myal.add(NOT_AVAILABLE);
							}
							
							
							
							if ( propertyFeatureType != null ) {
								myal.add(feature.getPropertyValue(propertyFeatureValue).toString());
								features.put(((GSAItem)feature.getPropertyValue(propertyFeatureType)).getPropertyValue(propertyFeatureName).toString(), myal);
							} else {
								myal.add(feature.getPropertyValue(propertyFeatureValue).toString() );
								features.put(feature.getPropertyValue(propertyFeatureName).toString(), myal);
							}
							
						}
					}
				}
			}
			
			//check to ensure that all arrays match the number of products and move to output
			ArrayList<String>[] newOutput = (ArrayList<String>[]) new ArrayList<?>[features.size()];
			Set<String> myKeys = features.keySet();
			
			for ( String key : myKeys) {
				if (isLoggingDebug()) { logDebug(new StringBuilder().append("For Key: <").append(key).append(">").toString()); }
				while (features.get(key).size() < prodCount+2) {
					if (isLoggingDebug()) { logDebug(new StringBuilder().append("adding value: <").append(NOT_AVAILABLE).append(">").toString()); }
					features.get(key).add(NOT_AVAILABLE);
				}
				int index = Integer.parseInt( features.get(key).get(0) );
				newOutput[index-1] = (ArrayList<String>)features.get(key);
			}
		
			outputList.addAll(Arrays.asList(newOutput));
			
			if (isLoggingDebug()) { logDebug(new StringBuilder().append("Final output: <").append(outputList.toString()).append(">").toString()); }

			pRequest.setParameter(ELEMENT, outputList);
			renderParam = OUTPUT_OPARAM;
		
			pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);

		} catch (Exception exc) {
			vlogError(exc, "Error");
			pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
		}
		if (isLoggingDebug()) {
			logDebug("ComparisonDroplet:droplet - end");
		}
	}
	
	/**
	 * Set's price values to the output list
	 * 
	 * @param pObject
	 * @param pOutputList
	 */
	protected void setPriceValue(ProductComparisonList.Entry pObject, LinkedList<ArrayList<String>> pOutputList){
		// add the Price
		if (isLoggingDebug()) { logDebug(new StringBuilder().append("Have Price: <").append("????").append(">").toString()); }
		
        List childSKUs = (List)pObject.getProduct().getPropertyValue("childSKUs");
        if(childSKUs != null){
                RepositoryItem sku = (RepositoryItem)childSKUs.get(0);
                try{
					RepositoryItem defaultPriceList = getPriceListManager().getDefaultPriceList();
					RepositoryItem price = null;
					if ( defaultPriceList != null ) {
						price = getPriceListManager().getPrice(defaultPriceList, pObject.getProduct(), sku);
					}
					
					RepositoryItem defaultSalePriceList = getPriceListManager().getDefaultSalePriceList();
					RepositoryItem forSalePrice = null;
					if ( defaultSalePriceList != null ) {
						forSalePrice = getPriceListManager().getPrice(defaultSalePriceList, pObject.getProduct(), sku);
					}

                    Double salePrice = 0.0;
                    if(forSalePrice != null) {
                    	if(isLoggingDebug()) {
		                    logDebug(new StringBuffer().append("salePrice returned: <").append(forSalePrice).append(">").toString());
		                }
                    	
						salePrice = (Double) forSalePrice.getPropertyValue(getPriceListManager().getListPricePropertyName());

						if(isLoggingDebug()) {
		                    logDebug(new StringBuffer().append("sale price returned: <").append(salePrice).append(">").toString());
		                }

                    } else {
                    	if(isLoggingDebug()) {
		                    logDebug(new StringBuffer().append("salePrice was null").toString());
		                }
                    }
                    
                    if(price != null) {
                    	
                    	if(isLoggingDebug()) {
		                    logDebug(new StringBuffer().append("price returned: <").append(price).append(">").toString());
		                }
                    	
						Double listPrice = (Double) price.getPropertyValue(getPriceListManager().getListPricePropertyName());

						if(isLoggingDebug()) {
		                    logDebug(new StringBuffer().append("list price returned: <").append(listPrice).append(">").toString());
		                }
						
						NumberFormat formatter = NumberFormat.getCurrencyInstance();

						if (forSalePrice != null) {
							pOutputList.get(PRICE_NDX).add(new StringBuffer().append("<del>").append(formatter.format(listPrice)).append("</del> ")
															 				.append("<ins>").append(formatter.format(salePrice)).append("</ins>").toString());
						} else {
							pOutputList.get(PRICE_NDX).add(new StringBuffer().append(formatter.format(listPrice)).toString());
						}

                    } else {
                    	if(isLoggingDebug()) {
		                    logDebug(new StringBuffer().append("price was null").toString());
		                }
                    }
                }
                catch(Exception e) {
					vlogError(e, "Error");
                }
        } else {
        	pOutputList.get(PRICE_NDX).add("Not Available");	
		}
		
	}
}
