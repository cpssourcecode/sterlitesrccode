package vsg.notifications;

import atg.commerce.profile.CommercePropertyManager;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.service.email.EmailEvent;
import atg.service.email.SMTPEmailSender;

/**
 * InternalNoficationService
 *
 * <h4>Description</h4> This service sends email from the application to
 * internal users
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class InternalNotificationService extends GenericService {
	// ~ Instance fields
	// --------------------------------------------------------------------------------------------------------------------
	/**
	 * component name for debugging
	 */
	private final static String COMPONENT_NAME = "/vsg/notifications/InternalNotificationService";

	private String mSubject;
	private String mBody;
	// private EmailService mEmailService;
	private SMTPEmailSender mSMTPEmailSender;

	// ~ Constructors
	// -----------------------------------------------------------------------------------------------------------------------

	/**
	 * Default constructor.
	 */
	public InternalNotificationService() {
	}

	// ~ Methods
	// ----------------------------------------------------------------------------------------------------------------------------

	/**
	 * This service starts by...
	 *
	 * @throws ServiceException
	 * @throws atg.nucleus.ServiceException
	 */
	public void doStartService() throws ServiceException {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(" - Service Started")
					.toString());
		super.doStartService();
	}

	/**
	 * When this service stops we should...
	 *
	 * @throws ServiceException
	 */
	public void doStopService() throws ServiceException {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(" - Service Stopped")
					.toString());
		super.doStopService();
	}

	/**
	 *
	 *
	 * @return return
	 */
	public SMTPEmailSender getSMTPEmailSender() {
		return mSMTPEmailSender;
	}

	/**
	 *
	 *
	 * @param pSMTPEmailSender
	 *            param
	 */
	public void setSMTPEmailSender(SMTPEmailSender pSMTPEmailSender) {
		mSMTPEmailSender = pSMTPEmailSender;
	}

	/**
	 * @return the Subject
	 */
	public String getSubject() {
		return mSubject;
	}

	/**
	 * @param pSubject
	 *            the Subject to set
	 */
	public void setSubject(String pSubject) {
		mSubject = pSubject;
	}

	/**
	 * @return the Body
	 */
	public String getBody() {
		return mBody;
	}

	/**
	 * @param pBody
	 *            the Body to set
	 */
	public void setBody(String pBody) {
		mBody = pBody;
	}

	/**
	 * @return the EmailService
	 */
	/*
	 * public EmailService getEmailService() { return mEmailService; }
	 */

	/**
	 * @param pEmailService
	 *            the EmailService to set
	 */
	/*
	 * public void setEmailService(EmailService pEmailService) { mEmailService =
	 * pEmailService; }
	 */

	/**
	 * Creates a new internal notification
	 *
	 * @param pEmailAddress
	 * @param pEmailTemplate
	 */
	public void createNotification(String pEmailAddress, String pEmailTemplate) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".createNotification")
					.append(" - start")
					.toString());

		try {
			CommercePropertyManager cpm = new CommercePropertyManager();

			// #######################################################
			// #######################################################
			// #######################################################
			// #######################################################
			// Hacked this up so it will work, need to convert it to the
			// JMS design used by other messages
			// HashMap map = new HashMap();
			// map.put(cpm.getEmailAddressPropertyName(), pEmailAddress);
			if (isLoggingDebug())
				logDebug(new StringBuilder().append("Sending notification to:")
						.append(pEmailAddress).toString());

			EmailEvent em = new EmailEvent();
			em.setRecipient(pEmailAddress);
			em.setSubject(getSubject());

			String userinfo = "";

			em.setBody(getBody());

			// TO DO Notification
			SMTPEmailSender smtp = getSMTPEmailSender();
			smtp.sendEmailEvent(em);

			// ****************************************
			if (isLoggingDebug())
				logDebug("shot internal email");
		} catch (atg.service.email.EmailException eex) {
			logError(new StringBuilder()
					.append("Error sending internal email: ").append(eex)
					.toString());
		} catch (Exception e) {
			vlogError(e, "Error");
		}

		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".createNotification")
					.append(" - exit")
					.toString());
	}
}
