package vsg.notifications;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.commerce.profile.CommercePropertyManager;
import atg.nucleus.GenericService;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfo;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;


/**
 * Email Service
 * 
 * <h4>Description</h4> 
 * This service sends emails based on the template in the properties file
 * 
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 * 
 */

public class EmailService extends GenericService {
   //~ Instance fields --------------------------------------------------------------------------------------------------------------------
	/**
	 * component name for debugging
	 */
	private final static String COMPONENT_NAME = "/vsg/notifications/EmailService";
   // ------------------------------
   // Properties
   private String mConfirmEmailFrom;
   private String mConfirmEmailReplyTo;
   private String mConfirmEmailName;
   private String mConfirmEmailSubject;
   private String mConfirmEmailCc;
   private String mConfirmEmailBcc;

   // email template information
   private TemplateEmailInfoImpl mDefaultTemplateEmailInfo;

   // email template
   private String mEmailTemplate;

   // email template sender
   private TemplateEmailSender mTemplateEmailSender;

   //~ Methods ----------------------------------------------------------------------------------------------------------------------------

   /**
    * Sends out email using TemplateEmailSender
    *
    * @param map
    *            Hashmap containing values for params to be rendered in the
    *            email email address comes in as an entry in the map
    * @param subject
    *            the subject for this email
    */
   public void sendEmail(Map map, String subject) throws TemplateEmailException {
       if (isLoggingDebug())
           logDebug(new StringBuilder().append(COMPONENT_NAME).append(".sendEmail")
        		   .append(" - start")
        		   .toString());

       CommercePropertyManager cpm = new CommercePropertyManager();

       if (isLoggingDebug())
           logDebug(new StringBuilder().append("Preparing email for recipients: ")
        		   .append(map.get(cpm.getEmailAddressPropertyName()))
        		   .toString());

       // create the template email info
       TemplateEmailInfo emailInfo = createTemplateEmailInfo(map, subject);

       // create the list of recipients and add the recipient email
       List<String> recipients = new ArrayList<String>();
       recipients.add((String) map.get(cpm.getEmailAddressPropertyName()));

       if (isLoggingDebug())
           logDebug(new StringBuilder().append("Sending email with emailInfo=")
        		   .append(emailInfo)
        		   .append(" and recipients= ")
        		   .append(recipients)
        		   .toString());

       // send the email
       getTemplateEmailSender().sendEmailMessage(emailInfo, recipients, true, // separate email thread
           true // persist email
       );

       if (isLoggingDebug()) {
           logDebug(new StringBuilder().append(COMPONENT_NAME).append(".sendEmail")
        		   .append(" - exit")
        		   .toString());
       }
   }

   /**
    * Sends out email using TemplateEmailSender
    * @param map
    *          Hashmap containing values for params to be rendered in the email
    *                                         email address comes in as an entry in the map
    */
   public void sendEmail(Map map) throws TemplateEmailException {
       sendEmail(map, null);
   }

   /**
    * Creates and returns the TemplateEmailInfo to use for sending password
    * reminder email.
    * @param pMap
    *          Hashmap containing keys and values to be rendered in the email
    *
    */
   protected TemplateEmailInfo createTemplateEmailInfo(Map pMap, String pSubject) {
       if (isLoggingDebug())
           logDebug(new StringBuilder().append(COMPONENT_NAME).append(".createTemplateEmailInfo")
        		   .append(" - start")
        		   .toString());

       // copy default template email info
       TemplateEmailInfoImpl emailInfo = (TemplateEmailInfoImpl) getDefaultTemplateEmailInfo().copy();

       // set the mailing information
       emailInfo.setMailingName(getConfirmEmailName());
       emailInfo.setMessageReplyTo(getConfirmEmailReplyTo());
       emailInfo.setMessageFrom(getConfirmEmailFrom());
       emailInfo.setMessageCc(getConfirmEmailCc());
       emailInfo.setMessageBcc(getConfirmEmailBcc());

       // set the mailing subject
       String subject = pSubject;

       if (subject == null) {
           subject = getConfirmEmailSubject();
       }

       emailInfo.setMessageSubject(subject);

       // set the template url 
       String templateUrl = getEmailTemplate();
       emailInfo.setTemplateURL(templateUrl);

       // add parameters
       emailInfo.setTemplateParameters(pMap);

       if (isLoggingDebug())
           logDebug(new StringBuilder().append(COMPONENT_NAME).append(".createTemplateEmailInfo")
        		   .append(" - exit")
        		   .toString());
       return emailInfo;
   }

   /**
    *
    *
    * @param pConfirmEmailFrom param
    */
   public void setConfirmEmailFrom(String pConfirmEmailFrom) {
       mConfirmEmailFrom = pConfirmEmailFrom;
   }

   /**
    *
    *
    * @return return
    */
   public String getConfirmEmailFrom() {
       return mConfirmEmailFrom;
   }

   /**
    *
    *
    * @param pConfirmEmailReplyTo param
    */
   public void setConfirmEmailReplyTo(String pConfirmEmailReplyTo) {
       mConfirmEmailReplyTo = pConfirmEmailReplyTo;
   }

   /**
    *
    *
    * @return return
    */
   public String getConfirmEmailReplyTo() {
       return mConfirmEmailReplyTo;
   }

   /**
    *
    *
    * @param pConfirmEmailName param
    */
   public void setConfirmEmailName(String pConfirmEmailName) {
       mConfirmEmailName = pConfirmEmailName;
   }

   /**
    *
    *
    * @return return
    */
   public String getConfirmEmailName() {
       return mConfirmEmailName;
   }

   /**
    *
    *
    * @param pConfirmEmailSubject param
    */
   public void setConfirmEmailSubject(String pConfirmEmailSubject) {
       mConfirmEmailSubject = pConfirmEmailSubject;
   }

   /**
    *
    *
    * @return return
    */
   public String getConfirmEmailSubject() {
       return mConfirmEmailSubject;
   }

   /**
    *
    *
    * @param pConfirmEmailCc param
    */
   public void setConfirmEmailCc(String pConfirmEmailCc) {
       mConfirmEmailCc = pConfirmEmailCc;
   }

   /**
    *
    *
    * @return return
    */
   public String getConfirmEmailCc() {
       return mConfirmEmailCc;
   }

   /**
    *
    *
    * @param pConfirmEmailBcc param
    */
   public void setConfirmEmailBcc(String pConfirmEmailBcc) {
       mConfirmEmailBcc = pConfirmEmailBcc;
   }

   /**
    *
    *
    * @return return
    */
   public String getConfirmEmailBcc() {
       return mConfirmEmailBcc;
   }

   /**
    *
    *
    * @return return
    */
   public TemplateEmailInfoImpl getDefaultTemplateEmailInfo() {
       return mDefaultTemplateEmailInfo;
   }

   /**
    *
    *
    * @param pTemplateEmailInfo param
    */
   public void setDefaultTemplateEmailInfo(TemplateEmailInfoImpl pTemplateEmailInfo) {
       mDefaultTemplateEmailInfo = pTemplateEmailInfo;
   }

   /**
    *
    *
    * @return return
    */
   public String getEmailTemplate() {
       return mEmailTemplate;
   }

   /**
    *
    *
    * @param pEmailTemplate param
    */
   public void setEmailTemplate(String pEmailTemplate) {
       mEmailTemplate = pEmailTemplate;
   }

   /**
    *
    *
    * @return return
    */
   public TemplateEmailSender getTemplateEmailSender() {
       return mTemplateEmailSender;
   }

   /**
    *
    *
    * @param pTemplateEmailSender param
    */
   public void setTemplateEmailSender(TemplateEmailSender pTemplateEmailSender) {
       mTemplateEmailSender = pTemplateEmailSender;
   }
}
