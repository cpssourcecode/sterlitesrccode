package vsg.integration.export.mapping;

/**
 * @author Dmitry Golubev
 */
public class PropertyMappingElement {
	/**
	 * repository
	 */
	private String mRepositoryName;
	/**
	 * item descriptor
	 */
	private String mItemDescriptor;
	/**
	 * repository name
	 */
	private String mRepositoryPath;
	/**
	 * static value
	 */
	private String mStaticValue;
	/**
	 * inout flag
	 */
	private String mInout;
	/**
	 * wsdl name
	 */
	private String mWsdlPath;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * empty constructor
	 */
	public PropertyMappingElement() {
	}

	public String getRepositoryName() {
		return mRepositoryName;
	}

	public void setRepositoryName(String pRepositoryName) {
		mRepositoryName = pRepositoryName;
	}

	public String getRepositoryPath() {
		return mRepositoryPath;
	}

	public void setRepositoryPath(String pRepositoryPath) {
		mRepositoryPath = pRepositoryPath;
	}

	public String getWsdlPath() {
		return mWsdlPath;
	}

	public void setWsdlPath(String pWsdlPath) {
		mWsdlPath = pWsdlPath;
	}

	public String getStaticValue() {
		return mStaticValue;
	}

	public void setStaticValue(String pStaticValue) {
		mStaticValue = pStaticValue;
	}

	public String getItemDescriptor() {
		return mItemDescriptor;
	}

	public void setItemDescriptor(String pItemDescriptor) {
		mItemDescriptor = pItemDescriptor;
	}

	public String getInout() {
		return mInout;
	}

	public void setInout(String pInout) {
		mInout = pInout;
	}
}
