package vsg.integration.export.mapping;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class PropertyMappingInfo extends GenericService {
	/**
	 * wsdl url
	 */
	private String mWsdlUrl;
	/**
	 * wsdl operation
	 */
	private String mWsdlOperation;
	/**
	 * list of property mapping elements
	 */
	private List<PropertyMappingElement> mPropertiesMappings = new LinkedList<PropertyMappingElement>();

	//------------------------------------------------------------------------------------------------------------------

	public List<PropertyMappingElement> getPropertiesMapping() {
		return mPropertiesMappings;
	}

	public void setPropertiesMapping(List<PropertyMappingElement> pPropertiesMappings) {
		mPropertiesMappings = pPropertiesMappings;
	}

	public String getWsdlUrl() {
		return mWsdlUrl;
	}

	public void setWsdlUrl(String pWsdlUrl) {
		mWsdlUrl = pWsdlUrl;
	}

	public String getWsdlOperation() {
		return mWsdlOperation;
	}

	public void setWsdlOperation(String pWsdlOperation) {
		mWsdlOperation = pWsdlOperation;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * add property mapping element
	 *
	 * @param pElement property mapping element
	 */
	public void addPropertyMapping(PropertyMappingElement pElement) {
		if (pElement != null) {
			mPropertiesMappings.add(pElement);
		}
	}

	/**
	 * clear all property mapping elements
	 */
	public void resetMappings() {
		mPropertiesMappings.clear();
	}

	/**
	 * find element by wsdl path
	 *
	 * @param pWsdlPath wsdl path
	 * @return PropertyMappingElement with matched wsdl path
	 */
	public PropertyMappingElement findElementByWsdlPath(String pWsdlPath) {
		if (mPropertiesMappings != null && !StringUtils.isBlank(pWsdlPath)) {
			for (PropertyMappingElement element : mPropertiesMappings) {
				if (pWsdlPath.equals(element.getWsdlPath())) {
					return element;
				}
			}
		}
		return null;
	}

}
