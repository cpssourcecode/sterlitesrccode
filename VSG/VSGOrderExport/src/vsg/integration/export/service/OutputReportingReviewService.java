package vsg.integration.export.service;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.integration.export.ExportConfig;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.service.email.EmailSenderService;
import vsg.integration.export.service.ftp.IFtpUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class OutputReportingReviewService.
 *
 * @author "Vladimir Tretyakevich"
 */
public class OutputReportingReviewService extends GenericService implements IRepositoryConstants {

	private static final String FTP_CLIENT_UTIL = "/vsg/integration/export/service/ftp/FtpUtil";
	private static final String SFTP_CLIENT_UTIL = "/vsg/integration/export/service/ftp/SFtpUtil";
	private static final String DEFAULT_DATE_FORMAT = "YYYYMMDDhhmmss";

	/**
	 * export models repository.
	 */
	private MutableRepository mExportRepository;

	/**
	 * The ftp client util.
	 */
	private IFtpUtil mIFtpUtil;

	/**
	 * The local test file name.
	 */
	private String mLocalTestFileName;

	/**
	 * The remote test file name.
	 */
	private String mRemoteTestFileName;

	/**
	 * The email sender service.
	 */
	private EmailSenderService mEmailSenderService;

	// ------------------------------------------------------------------------------------------------------------------

	public MutableRepository getExportRepository() {
		return mExportRepository;
	}

	public void setExportRepository(MutableRepository pExportRepository) {
		mExportRepository = pExportRepository;
	}

	public IFtpUtil getClientUtil(String pServerType) {
		IFtpUtil client = null;
		if ("SFTP".equals(pServerType)) {
			client = (IFtpUtil) Nucleus.getGlobalNucleus().resolveName(SFTP_CLIENT_UTIL);
		} else {
			client = (IFtpUtil) Nucleus.getGlobalNucleus().resolveName(FTP_CLIENT_UTIL);
		}
		return client;
	}

	public void setIFtpUtil(IFtpUtil pIFtpUtil) {
		mIFtpUtil = pIFtpUtil;
	}

	public IFtpUtil getIFtpUtil() {
		return mIFtpUtil;
	}

	public String getLocalTestFileName() {
		return mLocalTestFileName;
	}

	public void setLocalTestFileName(String pLocalTestFileName) {
		mLocalTestFileName = pLocalTestFileName;
	}

	public String getRemoteTestFileName() {
		return mRemoteTestFileName;
	}

	public void setRemoteTestFileName(String mRemoteTestFileName) {
		this.mRemoteTestFileName = mRemoteTestFileName;
	}

	public EmailSenderService getEmailSenderService() {
		return mEmailSenderService;
	}

	public void setEmailSenderService(EmailSenderService pEmailSenderService) {
		mEmailSenderService = pEmailSenderService;
	}

	// ------------------------------------------------------------------------------------------------------------------

	/**
	 * Test ftp.
	 *
	 * @param exportConfig the export config
	 * @param pRequest
	 * @param pValue       the value
	 * @return true, if successful
	 */
	public boolean testFtp(ExportConfig exportConfig) {
		boolean result = false;
		Map<String, String> map = getTestConnectionDetails(exportConfig);
		result = getClientUtil(map.get("serverType")).uploadFile(map);
		if (result) {
			getClientUtil(map.get("serverType")).removeFile(map);
		}
		return result;
	}

	/**
	 * Test email.
	 *
	 * @param pRequest
	 * @param exportConfig the export config
	 * @return true, if successful
	 */
	public boolean testEmail(ExportConfig pExportConfig) {
		return getEmailSenderService().sendTestEmail(pExportConfig.getRepositoryId(), null);
	}

	/**
	 * Gets the test connection details.
	 *
	 * @param pExportConfig the export config
	 * @return the test connection details
	 */
	private Map<String, String> getTestConnectionDetails(ExportConfig pExportConfig) {
		Map<String, String> details = new HashMap<String, String>();
		details.putAll(getConnectionDetails(pExportConfig));
		details.put("localFileName", getLocalTestFileName());
		details.put("remoteFileName", appendTimestamp(getRemoteTestFileName(), pExportConfig));
		return details;
	}

	/**
	 * Append timestamp.
	 *
	 * @param pRemoteTestFileName the remote test file name
	 * @param pExportConfig
	 * @return the string
	 */
	private String appendTimestamp(String pRemoteTestFileName, ExportConfig pExportConfig) {
		String result = null;
		RepositoryItem riExport = null;
		try {
			riExport = getExportRepository().getItem(pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG);
		} catch (RepositoryException e) {
			logError(e);
		}
		String dateFormat = null;
		if (riExport != null) {
			dateFormat = (String) riExport.getPropertyValue(PRP_REP_CONF_TIME_FORMAT);
		}
		if (StringUtils.isBlank(dateFormat)) {
			dateFormat = DEFAULT_DATE_FORMAT;
		}
		Date date = new Date();
		String addition = new SimpleDateFormat(dateFormat).format(date);
		addition = "_" + addition;

		if (pRemoteTestFileName.contains(".")) {
			result = pRemoteTestFileName.substring(0, pRemoteTestFileName.lastIndexOf('.')) + addition
					+ pRemoteTestFileName.substring(pRemoteTestFileName.lastIndexOf('.'));

		} else {
			result = result + addition;
		}
		vlogDebug("Result file name: " + result);
		return StringUtils.isBlank(result) ? pRemoteTestFileName : result;
	}

	/**
	 * Gets the connection details.
	 *
	 * @param pExportConfig the export config
	 * @return the connection details
	 */
	protected Map<String, String> getConnectionDetails(ExportConfig pExportConfig) {
		RepositoryItem riExport = null;
		Map<String, String> details = null;
		try {
			riExport = getExportRepository().getItem(pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG);
		} catch (RepositoryException e) {
			logError(e);
		}
		if (riExport != null) {
			RepositoryItem riReportConfig = (RepositoryItem) riExport.getPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG);
			RepositoryItem riFtpConfig = (RepositoryItem) riReportConfig.getPropertyValue(PRP_REP_CONF_FTP_CONF);

			details = new HashMap<String, String>();
			details.put("serverType", (Integer) riFtpConfig.getPropertyValue(PRP_FTP_CONF_PROTOCOL) == 0 ? "FTP" : "SFTP");
			String port = getPort(riFtpConfig);
			if (!StringUtils.isBlank(port)) {
				details.put("port", port);
			}
			String host = getHost(riFtpConfig);
			if (!StringUtils.isBlank(host)) {
				details.put("server", host);
			}
			details.put("userName", (String) riFtpConfig.getPropertyValue(PRP_FTP_CONF_USER_NAME));
			details.put("password", (String) riFtpConfig.getPropertyValue(PRP_FTP_CONF_PASS));
			// details.put("fileType", riFtpConfig.getPropertyValue());
			// details.put("transferMode", riFtpConfig.getPropertyValue());
			details.put("outServerDir", (String) riFtpConfig.getPropertyValue(PRP_FTP_CONF_OUT_DIR_PATH));
			details.put("remoteFileName", appendTimestamp((String) riFtpConfig.getPropertyValue(PRP_REP_CONF_FILE_NAME), pExportConfig));
		}

		return details;
	}

	/**
	 * Gets the host.
	 *
	 * @param pRiFtpConfig the ri ftp config
	 * @return the host
	 */
	private String getHost(RepositoryItem pRiFtpConfig) {
		String result = null;
		if (pRiFtpConfig != null) {
			String host = (String) pRiFtpConfig.getPropertyValue(PRP_FTP_CONF_HOST_NAME);
			if (!host.contains(":")) {
				result = host;
			} else {
				if (org.apache.commons.lang3.StringUtils.countMatches(host, ":") > 1) {
					result = host.substring(0, host.lastIndexOf(":"));
				}
			}
		}
		vlogDebug(result, "parsed host");
		return result;
	}

	/**
	 * Gets the port.
	 *
	 * @param pRiFtpConfig the ri ftp config
	 * @return the port
	 */
	private String getPort(RepositoryItem pRiFtpConfig) {
		String result = null;
		if (pRiFtpConfig != null) {
			String host = (String) pRiFtpConfig.getPropertyValue(PRP_FTP_CONF_HOST_NAME);
			if (!host.contains(":")) {
				result = host;
			} else {
				if (org.apache.commons.lang3.StringUtils.countMatches(host, ":") > 1) {
					String port = host.substring(host.lastIndexOf(":"));
					if (port.length() <= 5) {
						result = port;
					}
				}
			}
		}
		vlogDebug(result, "parsed port");
		return result;
	}
}