package vsg.integration.export.service.ftp;

import com.jcraft.jsch.UserInfo;

public class SFtpUserInfo implements UserInfo {

	private String mPassword;
	private String mPassphrase;

	@Override
	public String getPassphrase() {
		return mPassphrase;
	}

	public void setPassphrase(String pPassphrase) {
		mPassphrase = pPassphrase;
	}

	@Override
	public String getPassword() {
		return mPassword;
	}

	public void setPassword(String pPassword) {
		mPassword = pPassword;
	}

	@Override
	public boolean promptPassword(String message) {
		return true;
	}

	@Override
	public boolean promptPassphrase(String message) {
		return true;
	}

	@Override
	public boolean promptYesNo(String message) {
		return true;
	}

	@Override
	public void showMessage(String message) {
	}
}
