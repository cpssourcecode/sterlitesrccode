package vsg.integration.export.service.ftp;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.SocketException;
import java.util.Map;

/**
 * The Class FtpUtil.
 *
 * @author "Vladimir Tretyakevich"
 */
public class FtpUtil extends GenericService implements IFtpUtil {

	/*
	 * (non-Javadoc)
	 * @see vsg.integration.export.service.ftp.IFtpUtil#uploadFile(java.util.Map)
	 */
	@Override
	public boolean uploadFile(Map<String, String> pConnectionDetails) {
		synchronized (this) {
			if (pConnectionDetails == null) {
				return false;
			}

			boolean result = false;

			FTPClient ftpClient = getFtpClient(pConnectionDetails);
			try {
				if (connectToServer(ftpClient, pConnectionDetails)) {
					if (loginToServer(ftpClient, pConnectionDetails)) {
						configConnection(ftpClient, pConnectionDetails);
						checkWorkingDirectory(ftpClient, pConnectionDetails);
						result = uploadFile(ftpClient, pConnectionDetails);
					}
				}
			} catch (Exception e) {
				vlogError(e, "Unable to upload file");
			} finally {
				try {
					logout(ftpClient);
					disconnect(ftpClient);
				} catch (IOException e) {
					vlogError("Exception logut from server: " + e);
				}
			}
			return result;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see vsg.integration.export.service.ftp.IFtpUtil#removeFile(java.util.Map)
	 */
	@Override
	public boolean removeFile(Map<String, String> pConnectionDetails) {
		synchronized (this) {
			if (pConnectionDetails == null) {
				return false;
			}

			boolean result = false;

			FTPClient ftpClient = getFtpClient(pConnectionDetails);
			try {
				if (connectToServer(ftpClient, pConnectionDetails)) {
					if (loginToServer(ftpClient, pConnectionDetails)) {
						configConnection(ftpClient, pConnectionDetails);
						checkWorkingDirectory(ftpClient, pConnectionDetails);
						result = removeFile(ftpClient, pConnectionDetails);
					}
				}
			} catch (Exception e) {
				vlogError(e, "Unable to upload file");
			} finally {
				try {
					logout(ftpClient);
					disconnect(ftpClient);
				} catch (IOException e) {
					vlogError("Exception logut from server: " + e);
				}
			}
			return result;
		}
	}

	/**
	 * Gets the ftp client.
	 *
	 * @param pConnectionDetails the connection details
	 * @return the ftp client
	 */
	private FTPClient getFtpClient(Map<String, String> pConnectionDetails) {
		String ftpType = pConnectionDetails.get("serverType");
		FTPClient ftpClient = initializeFTPClient(ftpType);
		// suppress login details
		ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));
		return ftpClient;
	}

	/**
	 * Logout.
	 *
	 * @param pFtpClient the ftp client
	 * @throws IOException
	 */
	protected void logout(FTPClient pFtpClient) throws IOException {
		pFtpClient.noop();
		pFtpClient.logout();
	}

	/**
	 * Disconnect.
	 *
	 * @param pFtpClient the ftp client
	 * @throws IOException
	 */
	protected void disconnect(FTPClient pFtpClient) throws IOException {
		pFtpClient.disconnect();
	}

	/**
	 * Check working directory.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @throws IOException
	 */
	protected void checkWorkingDirectory(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws IOException {
		String workingDir = pConnectionDetails.get("outServerDir");
		if (!StringUtils.isBlank(workingDir)) {
			if (!workingDir.equals(pFtpClient.printWorkingDirectory())) {
				pFtpClient.changeWorkingDirectory(workingDir);
				vlogDebug("Working dirctory has been changed");
			}
		} else {
			vlogError("Exception while checking working dirctory: outServerDir param is blank");
		}
	}

	/**
	 * Login to server.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @throws IOException
	 */
	protected boolean loginToServer(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws IOException {
		boolean result = false;
		result = pFtpClient.login(pConnectionDetails.get("userName"), pConnectionDetails.get("password"));
		if (!result) {
			pFtpClient.logout(); // to avoid bad server session
		}
		return result;
	}

	/**
	 * Initialize ftp client.
	 *
	 * @param ftpType the ftp type
	 * @return the FTP client
	 */
	protected FTPClient initializeFTPClient(String ftpType) {
		FTPClient result = null;
		vlogDebug("Server type: {0}", ftpType);
		if ("FTPS".equals(ftpType)) {
			FTPSClient ftps;
			String protocol = "true";
			if (protocol.equals("true")) {
				ftps = new FTPSClient(true);
			} else if (protocol.equals("false")) {
				ftps = new FTPSClient(false);
			} else {
				String prot[] = protocol.split(",");
				if (prot.length == 1) { // Just protocol
					ftps = new FTPSClient(protocol);
				} else { // protocol,true|false
					ftps = new FTPSClient(prot[0], Boolean.parseBoolean(prot[1]));
				}
			}
			result = ftps;
			// if ("all".equals(trustmgr)) {
			// ftps.setTrustManager(TrustManagerUtils.getAcceptAllTrustManager());
			// } else if ("valid".equals(trustmgr)) {
			// ftps.setTrustManager(TrustManagerUtils.getValidateServerCertificateTrustManager());
			// } else if ("none".equals(trustmgr)) {
			// ftps.setTrustManager(null);
			// }
		} else if ("FTP".equals(ftpType)) {
			result = new FTPClient();
			// ftp with proxy
			// if (proxyHost != null) {
			// ftp = new FTPHTTPClient(proxyHost, proxyPort, proxyUser,
			// proxyPassword);
			// }
		} else {
			result = new FTPClient();
			vlogDebug("Server type was not provided and is set to default: FTP");
		}
		return result;
	}

	/**
	 * Connect to server.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @throws IOException
	 * @throws SocketException
	 */
	protected boolean connectToServer(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws SocketException, IOException {
		Integer port = (Integer) Integer.parseInt(pConnectionDetails.get("port"));
		String server = pConnectionDetails.get("server");

		return connectToServer(pFtpClient, port, server);
	}

	public boolean connectToServer(FTPClient pFtpClient, Integer pPort, String pServer) throws IOException {
		int reply;
		vlogDebug("Connecting to " + pServer + " on " + ((pPort != null && pPort > 0) ? pPort : pFtpClient.getDefaultPort()));

		if (pPort != null && pPort > 0) {
			pFtpClient.connect(pServer, pPort);
		} else {
			pFtpClient.connect(pServer);
		}
		vlogDebug("Connected to " + pServer + " on " + ((pPort != null && pPort > 0) ? pPort : pFtpClient.getDefaultPort()));

		// reply should be success
		reply = pFtpClient.getReplyCode();

		if (!FTPReply.isPositiveCompletion(reply)) {
			pFtpClient.disconnect(); // if not positive response should reconnect
			vlogError("FTP server refused connection.");
			return false;
		} else {
			vlogError("FTP server connection success.");
			return true;
		}
	}

	/**
	 * Config connection.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @throws IOException
	 */
	protected void configConnection(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws IOException {
		if (isLoggingDebug()) {
			logDebug("Remote system is " + pFtpClient.getSystemType());
		}

		if ("binary".equals(pConnectionDetails.get("fileType"))) {
			pFtpClient.setFileType(FTP.BINARY_FILE_TYPE);
		} else {
			// should be by default
			pFtpClient.setFileType(FTP.ASCII_FILE_TYPE);
		}

		// Use passive mode as default because most of us are
		// behind firewall these days.
		if ("localActive".equals(pConnectionDetails.get("transferMode"))) {
			pFtpClient.enterLocalActiveMode();
		} else {
			pFtpClient.enterLocalPassiveMode();
		}

		// pFtpClient.setUseEPSVwithIPv4(true);
	}

	/**
	 * Upload file.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @return true, if successful
	 * @throws IOException
	 */
	protected boolean uploadFile(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws IOException {
		boolean result = false;
		InputStream input = null;
		try {
			input = new FileInputStream(pConnectionDetails.get("localFileName"));
			if (input != null) {
				result = pFtpClient.storeFile(pConnectionDetails.get("remoteFileName"), input);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					vlogError(e, "Error");
				}
			}
		}
		return result;
	}

	/**
	 * Download file.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @return true, if successful
	 * @throws IOException
	 */
	protected boolean downloadFile(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws IOException {
		boolean result = false;
		OutputStream output = null;
		try {
			output = new FileOutputStream(pConnectionDetails.get("localFileName"));
			result = pFtpClient.retrieveFile(pConnectionDetails.get("remoteFileName"), output);
		} catch (IOException e) {
			throw e;
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					vlogError(e, "Error");
				}
			}
		}
		return result;
	}

	/**
	 * Removes the file.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @return true, if successful
	 * @throws IOException
	 */
	protected boolean removeFile(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws IOException {
		return pFtpClient.deleteFile(pConnectionDetails.get("remoteFileName"));
	}

	/**
	 * List files.
	 *
	 * @param pFtpClient         the ftp client
	 * @param pConnectionDetails the connection details
	 * @throws IOException
	 */
	protected void listFiles(FTPClient pFtpClient, Map<String, String> pConnectionDetails) throws IOException {
		for (FTPFile f : pFtpClient.listFiles(pConnectionDetails.get("outServerDir"))) {
			vlogDebug(f.getRawListing());
			vlogDebug(f.toFormattedString());
		}
	}

}