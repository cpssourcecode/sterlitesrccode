package vsg.integration.export.service.ftp;

import java.util.Map;

/**
 * The Interface IFtpUtil.
 *
 * @author "Vladimir Tretyakevich"
 */
public interface IFtpUtil {

	/**
	 * Upload file.
	 *
	 * @param pConnectionDetails the connection details map
	 * @return true, if successful
	 */
	boolean uploadFile(Map<String, String> pConnectionDetails);

	/**
	 * Removes the file.
	 *
	 * @param pConnectionDetails the connection details map
	 * @return true, if successful
	 */
	boolean removeFile(Map<String, String> pConnectionDetails);
}
