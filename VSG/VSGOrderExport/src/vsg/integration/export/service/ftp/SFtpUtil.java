package vsg.integration.export.service.ftp;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.UserInfo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

/**
 * The Class SFtpUtil.
 *
 * @author "Vladimir Tretyakevich"
 */
public class SFtpUtil extends GenericService implements IFtpUtil {

	/*
	 * (non-Javadoc)
	 * @see vsg.integration.export.service.ftp.IFtpUtil#uploadFile(java.util.Map)
	 */
	@Override
	public boolean uploadFile(Map<String, String> pConnectionDetails) {
		synchronized (this) {
			if (pConnectionDetails == null) {
				return false;
			}
			boolean result = false;

			Session session = null;
			ChannelSftp channelSftp = null;
			try {
				session = getSession(pConnectionDetails);
				channelSftp = getSftpChannel(session);
				checkWorkingDirectory(channelSftp, pConnectionDetails);
				uploadFile(channelSftp, pConnectionDetails);
				result = true;
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			} finally {
				if (session != null && session.isConnected()) {
					if (channelSftp != null) {
						channelSftp.disconnect();
					}
					session.disconnect();
				}
			}

			return result;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see vsg.integration.export.service.ftp.IFtpUtil#removeFile(java.util.Map)
	 */
	@Override
	public boolean removeFile(Map<String, String> pConnectionDetails) {
		synchronized (this) {
			if (pConnectionDetails == null) {
				return false;
			}
			boolean result = false;

			Session session = null;
			ChannelSftp channelSftp = null;
			try {
				session = getSession(pConnectionDetails);
				channelSftp = getSftpChannel(session);
				checkWorkingDirectory(channelSftp, pConnectionDetails);
				removeFile(channelSftp, pConnectionDetails);
				result = true;
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			} finally {
				if (session != null && session.isConnected()) {
					if (channelSftp != null) {
						channelSftp.disconnect();
					}
					session.disconnect();
				}
			}

			return result;
		}
	}

	/**
	 * Upload file.
	 *
	 * @param channelSftp        the channel sftp
	 * @param pConnectionDetails the connection details
	 * @throws SftpException the sftp exception
	 * @throws IOException   Signals that an I/O exception has occurred.
	 */
	private void uploadFile(ChannelSftp channelSftp, Map<String, String> pConnectionDetails) throws SftpException, IOException {
		InputStream input = null;
		try {
			input = new FileInputStream(pConnectionDetails.get("localFileName"));
			if (input != null) {
				channelSftp.put(input, pConnectionDetails.get("remoteFileName"));
			}
		} catch (SftpException e) {
			throw e;
		} catch (FileNotFoundException e) {
			throw e;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					vlogError(e, "Error");
				}
			}
		}
	}

	/**
	 * Download file.
	 *
	 * @param pSftpChannel       the sftp channel
	 * @param pConnectionDetails the connection details
	 * @return true, if successful
	 * @throws SftpException the sftp exception
	 * @throws IOException   Signals that an I/O exception has occurred.
	 */
	protected boolean downloadFile(ChannelSftp pSftpChannel, Map<String, String> pConnectionDetails) throws SftpException, IOException {
		boolean result = false;
		OutputStream output = null;
		try {
			output = new FileOutputStream(pConnectionDetails.get("localFileName"));
			pSftpChannel.get(pConnectionDetails.get("remoteFileName"), output);
		} catch (IOException e) {
			throw e;
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					vlogError(e, "Error");
				}
			}
		}
		return result;
	}

	/**
	 * Gets the session.
	 *
	 * @param pConnectionDetails the connection details
	 * @return the session
	 * @throws NumberFormatException the number format exception
	 * @throws JSchException         the j sch exception
	 */
	private Session getSession(Map<String, String> pConnectionDetails) throws JSchException {
		JSch jsch = new JSch();
		int port = 0;
		if (!StringUtils.isBlank(pConnectionDetails.get("port"))) {
			try {
				port = Integer.parseInt(pConnectionDetails.get("port"));
			} catch (NumberFormatException e) {
				vlogError(e, "Error");
			}
		}
		Session session = null;
		if (port != 0) {
			session = jsch.getSession(pConnectionDetails.get("userName"), pConnectionDetails.get("server"), port);
		} else {
			session = jsch.getSession(pConnectionDetails.get("userName"), pConnectionDetails.get("server"));
		}
		UserInfo ui = initUserInfo(pConnectionDetails);
		session.setUserInfo(ui);
		// int timeout = Integer.parseInt(pConnectionDetails.get("timeout"));
		// session.setTimeout(timeout);
		session.connect();

		return session;
	}

	/**
	 * Inits the user info.
	 *
	 * @param pConnectionDetails
	 * @return the user info
	 */
	private UserInfo initUserInfo(Map<String, String> pConnectionDetails) {
		SFtpUserInfo ui = new SFtpUserInfo();
		ui.setPassword(pConnectionDetails.get("password"));
		ui.setPassphrase(pConnectionDetails.get("passphrase"));
		return ui;
	}

	/**
	 * Gets the sftp channel.
	 *
	 * @param session the session
	 * @return the sftp channel
	 * @throws JSchException the j sch exception
	 */
	private ChannelSftp getSftpChannel(Session session) throws JSchException {
		Channel channel = session.openChannel("sftp");
		channel.connect();
		return (ChannelSftp) channel;
	}

	/**
	 * Check working directory.
	 *
	 * @param pChannel           the channel
	 * @param pConnectionDetails the connection details
	 * @throws SftpException the sftp exception
	 */
	protected void checkWorkingDirectory(ChannelSftp pChannel, Map<String, String> pConnectionDetails) throws SftpException {
		String workingDir = pConnectionDetails.get("outServerDir");
		if (!StringUtils.isBlank(workingDir)) {
			if (!workingDir.equals(pChannel.lpwd())) {
				pChannel.cd(workingDir);
				vlogDebug("Working dirctory has been changed");
			}
		} else {
			vlogError("Exception while checking working dirctory: outServerDir param is blank");
		}
	}

	/**
	 * Removes the file.
	 *
	 * @param pSftpChannel       the sftp channel
	 * @param pConnectionDetails the connection details
	 * @throws SftpException the sftp exception
	 */
	private void removeFile(ChannelSftp pSftpChannel, Map<String, String> pConnectionDetails) throws SftpException {
		pSftpChannel.rm(pConnectionDetails.get("remoteFileName"));
	}

	/**
	 * List files.
	 *
	 * @param pSftpChannel       the sftp channel
	 * @param pConnectionDetails the connection details
	 * @throws SftpException the sftp exception
	 */
	protected void listFiles(ChannelSftp pSftpChannel, Map<String, String> pConnectionDetails) throws SftpException {
		for (Object f : pSftpChannel.ls(pConnectionDetails.get("outServerDir"))) {
			vlogDebug(f.toString());
		}
	}

}