package vsg.integration.export.service;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.util.ObjectStringFormatter;
import vsg.integration.export.util.ObjectXMLStringFormatter;

import java.util.Collection;

/**
 * Repository item preview service
 */
public class RepositoryItemPreviewService extends GenericService implements IRepositoryConstants {

	/**
	 * relation delimeter
	 */
	private static final String RELATION_DELIMETER = ".";

	/**
	 * Object to string formatter
	 */
	private ObjectStringFormatter mFormatter;
	/**
	 * Object to xml string formatter
	 */
	private ObjectXMLStringFormatter mXmlFormatter;

	//------------------------------------------------------------------------------------------------------------------

	public ObjectStringFormatter getFormatter() {
		return mFormatter;
	}

	public void setFormatter(ObjectStringFormatter pFormatter) {
		mFormatter = pFormatter;
	}

	public ObjectXMLStringFormatter getXmlFormatter() {
		return mXmlFormatter;
	}

	public void setXmlFormatter(ObjectXMLStringFormatter pXmlFormatter) {
		mXmlFormatter = pXmlFormatter;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pItem    repository item
	 * @param pColumns array of properties to retrieve from repository item
	 * @return repository as row by defined columns
	 */
	public String[] getRepositoryItemRow(RepositoryItem pItem, String[] pColumns) {
		return getRepositoryItemRow(pItem, pColumns, false);
	}

	/**
	 * @param pItem    repository item
	 * @param pColumns array of properties to retrieve from repository item
	 * @param pIsXml   use xml to string format
	 * @return repository as row by defined columns
	 */
	public String[] getRepositoryItemRow(RepositoryItem pItem, String[] pColumns, boolean pIsXml) {
		String[] row = new String[pColumns.length];
		for (int i = 0; i < pColumns.length; i++) {
			row[i] = getRepositoryItemValue(pItem, pColumns[i], pIsXml);
		}
		return row;
	}

	/**
	 * @param pItem     repository item
	 * @param pProperty property to retrieve
	 * @return repository item property value
	 */
	public String getRepositoryItemValue(RepositoryItem pItem, String pProperty) {
		return getRepositoryItemValue(pItem, pProperty, false);
	}

	/**
	 * @param pItem     repository item
	 * @param pProperty property to retrieve
	 * @param pIsXml    use xml to string format
	 * @return repository item property value
	 */
	public String getRepositoryItemValue(RepositoryItem pItem, String pProperty, boolean pIsXml) {
		if (pProperty.contains(RELATION_DELIMETER)) {
			return getRepositoryItemRelationValue(pItem, pProperty);
		} else {
			return getObjectToStringValue(pItem.getPropertyValue(pProperty), pIsXml);
		}
	}

	/**
	 * @param pValue value to format to string
	 * @param pIsXml use xml to string format
	 * @return repository item property value
	 */
	public String getObjectToStringValue(Object pValue, boolean pIsXml) {
		if (pValue == null) {
			return null;
		}
		if (pIsXml) {
			return getXmlFormatter().formatToString(pValue);
		} else {
			return getFormatter().formatToString(pValue);
		}
	}

	/**
	 * @param pItem     repository item
	 * @param pProperty property to retrieve
	 * @return repository item relation property value
	 */
	private String getRepositoryItemRelationValue(RepositoryItem pItem, String pProperty) {
		int delimeterIndex = pProperty.indexOf(RELATION_DELIMETER);
		String relationName = pProperty.substring(0, delimeterIndex);
		Object relationValue = pItem.getPropertyValue(relationName);
		if (relationValue == null) {
			return null;
		} else {
			if (!relationValue.getClass().isAssignableFrom(Collection.class)) {
				return getRepositoryItemValue((RepositoryItem) relationValue, pProperty.substring(delimeterIndex + 1));
			} else {
				vlogError("Try to get relation value through not repository item chain");
				return null;
			}
		}
	}

}
