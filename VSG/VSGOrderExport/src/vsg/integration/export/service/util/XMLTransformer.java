package vsg.integration.export.service.util;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;

/**
 * @author Dmitry Golubev
 */
public class XMLTransformer {

	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(XMLTransformer.class);

	/**
	 * transformer for string to xml view
	 */
	private static Transformer mTransformer;

	/**
	 * @return initialized transformer
	 * @throws javax.xml.transform.TransformerConfigurationException if error occurs
	 */
	public static Transformer getTransformer() throws TransformerConfigurationException {
		if (mTransformer == null) {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			mTransformer = transformerFactory.newTransformer();
		}
		return mTransformer;
	}

	/**
	 * @param pMessage soap message
	 * @return string preview of soap message
	 */
	public static String getSOAPMessageAsString(SOAPMessage pMessage) {
		try {
			Source sourceContent = pMessage.getSOAPPart().getContent();
			StreamResult result = new StreamResult(new ByteArrayOutputStream());
			Transformer transformer = XMLTransformer.getTransformer();
			transformer.transform(sourceContent, result);
			return new String(((ByteArrayOutputStream) result.getOutputStream()).toByteArray());
		} catch (Exception e) {
			mLogging.logError(e);
		}
		return null;
	}

}
