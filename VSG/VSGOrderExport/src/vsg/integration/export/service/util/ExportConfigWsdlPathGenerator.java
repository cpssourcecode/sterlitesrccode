package vsg.integration.export.service.util;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import vsg.integration.export.FileSettingsInfo;
import vsg.integration.export.util.FileUtil;

public class ExportConfigWsdlPathGenerator extends GenericService {

	/**
	 * file settings info
	 */
	private FileSettingsInfo mFileSettingsInfo;

	//------------------------------------------------------------------------------------------------------------------

	public FileSettingsInfo getFileSettingsInfo() {
		return mFileSettingsInfo;
	}

	public void setFileSettingsInfo(FileSettingsInfo pFileSettingsInfo) {
		this.mFileSettingsInfo = pFileSettingsInfo;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * generate wsdl path according to which parameter is provided
	 *
	 * @param pWsdlUri    - uri parameter
	 * @param pUploadPath - path parameter
	 * @return wsdl path
	 */
	public String generateWsdlPath(String pExportConfigId, String pWsdlUri, String pUploadPath) {
		String wsdlPath = null;
		if (!StringUtils.isBlank(pUploadPath)) {
			wsdlPath = getFileSettingsInfo().getWsdlPath() + "/" + pExportConfigId + "/" + pUploadPath;
		} else if (!StringUtils.isBlank(pWsdlUri)) {
			if (!FileUtil.isFileByUriExist(pWsdlUri)) {
				vlogError("WSDL is not accessible: {0} - {1}", pExportConfigId, pWsdlUri);
			} else {
				wsdlPath = pWsdlUri;
			}
		} else {
			vlogError("WSDL is not defined for export configuration: {0}", pExportConfigId);
		}
		return wsdlPath;
	}
}
