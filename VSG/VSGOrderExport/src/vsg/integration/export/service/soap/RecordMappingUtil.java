package vsg.integration.export.service.soap;

import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.Type;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class RecordMappingUtil implements IRepositoryConstants {
	/**
	 * @param pType               wsdl type
	 * @param pRepositoryMappings repository mappings to filter
	 * @return filterd set of mappings according to wsdl type
	 */
	public static Set<RepositoryItem> getRepositoryMappingsByType(Type pType,
																  Set<RepositoryItem> pRepositoryMappings) {
		Set<RepositoryItem> acceptableMappings = new HashSet<RepositoryItem>();
		for (RepositoryItem repositoryMapping : pRepositoryMappings) {
			String wsdlPath = (String) repositoryMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
			if (wsdlPath.startsWith(pType.getFullPath())) {
				acceptableMappings.add(repositoryMapping);
			}
		}
		return acceptableMappings;
	}

	/**
	 * @param pExportConfigItem export config repository item
	 * @param pOperation        wsdl operation
	 * @param pUseInputType     use input type (true), otherwise - output (false)
	 * @return set of record mapping, related to soap message response
	 */
	public static Set<RepositoryItem> getMappingsByType(RepositoryItem pExportConfigItem,
														Operation pOperation,
														boolean pUseInputType) {
		Set<RepositoryItem> recordMappings = (Set<RepositoryItem>) pExportConfigItem.getPropertyValue
				(PRP_EXPORT_RECORD_MAPPINGS);
		if (recordMappings == null)
			return null;
		Type type = null;
		if(pOperation != null) {
			if (pUseInputType && pOperation.getMessageInput() != null) {
				type = pOperation.getMessageInput().getType();
			} else if(pOperation.getMessageOutput() != null){
				type = pOperation.getMessageOutput().getType();
			}
		}
		if (type == null) return null;
		String prefix = pOperation.getName() + "." + type.getName() + ".";
		Set<RepositoryItem> recordMappingForCurrentOperation = new HashSet<RepositoryItem>();
		for (RepositoryItem recordMapping : recordMappings) {
			String wsdlPath = (String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
			// wsdl path should be null (WSDLMappingValidator makes validation) but otherwise
			if (wsdlPath != null && wsdlPath.startsWith(prefix)) {
				recordMappingForCurrentOperation.add(recordMapping);
			}
		}
		return recordMappingForCurrentOperation;
	}

}
