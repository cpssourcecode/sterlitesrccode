package vsg.integration.export.service.soap;

import atg.beans.DynamicPropertyDescriptor;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import org.w3c.dom.Node;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.ISOAPConstants;
import vsg.integration.export.parse.wsdl.DomUtil;
import vsg.integration.export.parse.wsdl.analyzer.TargetNamespaceUtil;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.TargetNamespace;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.service.ErrorTracker;
import vsg.integration.export.service.RepositoryItemPreviewService;
import vsg.integration.export.service.soap.util.RepositoryItemInfo;
import vsg.integration.export.service.soap.util.SOAPElementInfo;
import vsg.integration.export.util.bean.TransactionInfo;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.util.*;

/**
 * @author Dmitry Golubev
 */
public class SOAPRequestBuilder extends ErrorTracker implements IRepositoryConstants, ISOAPConstants {
	/**
	 * Repository Item Preview Service
	 */
	private RepositoryItemPreviewService mPreviewService;
	/**
	 * wsdl type targetnamespace prefix
	 */
	private String mNameSpacePrefix = null;
	/**
	 * SOAPCallInfo object
	 */
	private SOAPCallInfo mSOAPCallInfo;
	/**
	 * current wsdl model
	 */
	private WSDLModel mWSDLModel;
	/**
	 * current operation
	 */
	private Operation mOperation;
	/**
	 * repository path - wsdl path
	 */
	private Map<String, Set<String>> mRepositoryPathWsdlPathLevels = new HashMap<String, Set<String>>();
	/**
	 * repository - SOAPElementInfos
	 */
	private Map<RepositoryItem, Set<SOAPElementInfo>> mRepositoryItemSOAPElementInfos = new HashMap<>();
	/**
	 * SOAP message envelope
	 */
	private SOAPEnvelope mEnvelope;
	/**
	 * set of added target namespace prefixes
	 */
	private Set<String> mAddedToEnvelopeTargetNamespaces = new HashSet<>();

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pRepositoryModel repository model
	 * @param pWsdlModel       wsdl model
	 * @param pSOAPCallInfo    SOAPCallInfo object
	 * @return SOAPMessage object
	 * @throws Exception if error occures
	 */
	public SOAPMessage createSOAPMessage(RepositoryItem pRepositoryModel,
										 WSDLModel pWsdlModel,
										 SOAPCallInfo pSOAPCallInfo) throws Exception {
		if (false) {
			String xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:com=\"http://com.columbiapipe.e1.bssv.JP554200/\" xmlns:java=\"java:com.columbiapipe.e1.bssv.JP554200.valueobject\" xmlns:java1=\"java:oracle.e1.bssv.JP420000.valueobject\" xmlns:java2=\"java:oracle.e1.bssv.util.J0100010.valueobject\" xmlns:java3=\"java:oracle.e1.bssv.util.J4100010.valueobject\">\n" +
					"   <soapenv:Header>\n" +
					"                <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
					"         <wsse:UsernameToken>\n" +
					"            <wsse:Username>bssvdvatg1</wsse:Username>\n" +
					"            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">bsv#852!#l</wsse:Password>\n" +
					"         </wsse:UsernameToken>\n" +
					"      </wsse:Security>\n" +
					"   </soapenv:Header>\n" +
					"     <soapenv:Body>\n" +
					"      <com:processSalesOrderElement>\n" +
					"         <java:Header>\n" +
					"            <java:Processing>\n" +
					"               <java1:ActionType>A</java1:ActionType>\n" +
					"            </java:Processing>\n" +
					"            <java:SalesOrderKey>\n" +
					"               <java1:DocumentCompany>00001</java1:DocumentCompany>\n" +
					"            </java:SalesOrderKey>\n" +
					"            <java:BusinessUnit>100</java:BusinessUnit>\n" +
					"            <java:CustomerPO>8274</java:CustomerPO>\n" +
					"            <java:SoldTo>\n" +
					"               <java1:Customer>\n" +
					"                  <java2:EntityId>113553</java2:EntityId>\n" +
					"               </java1:Customer>\n" +
					"            </java:SoldTo>\n" +
					"            <java:ShipTo>\n" +
					"               <java1:Customer>\n" +
					"                  <java2:EntityId>153654</java2:EntityId>\n" +
					"               </java1:Customer>\n" +
					"            </java:ShipTo>\n" +
					"            <java:Detail>\n" +
					"               <java1:Processing>\n" +
					"                  <java1:ActionType>A</java1:ActionType>\n" +
					"               </java1:Processing>\n" +
					"               <java1:Product>\n" +
					"                  <java1:Item>\n" +
					"                     <java3:ItemId>500500</java3:ItemId>\n" +
					"                  </java1:Item>\n" +
					"               </java1:Product>\n" +
					"               <java1:QuantityOrdered>1.0</java1:QuantityOrdered>\n" +
					"            </java:Detail>\n" +
					"            <java:HeaderCP>\n" +
					"               <java:WebOrderNumber>9876544</java:WebOrderNumber>\n" +
					"               <java:WebOrderType>S6</java:WebOrderType>\n" +
					"               <java:StopCode>MTR</java:StopCode>\n" +
					"               <java:CustomerEmail>customer@gmail.com</java:CustomerEmail>\n" +
					"            </java:HeaderCP>\n" +
					"         </java:Header>\n" +
					"      </com:processSalesOrderElement>\n" +
					"   </soapenv:Body>\n" +
					"</soapenv:Envelope>";
			MessageFactory factory = MessageFactory.newInstance();
			SOAPMessage message = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8"))));
			return message;
		}

		synchronized (this) {
			setSOAPCallInfo(pSOAPCallInfo);
			setWSDLModel(pWsdlModel);


/*
			if(pWsdlModel.getSOAPAddressLocation()!=null && pWsdlModel.getSOAPAddressLocation().contains
					("Customers/customer_client_ep")) {
				String operationName = getOperationName(pRepositoryModel);
				Operation operation = pWsdlModel.getOperation(operationName);
				setOperation(operation);
				Type operationType = operation.getMessageInput().getType();
				return getTestRequest(operationType);
			}
*/

//			System.setProperty("javax.xml.soap.MessageFactory", "oracle.j2ee.ws.saaj.soap.MessageFactoryImpl");
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage soapMessage = messageFactory.createMessage();

			initSOAPMessageHeader(pRepositoryModel, soapMessage);
			initSOAPMessageContent(pRepositoryModel, soapMessage);
			addWSPolicyInfo(soapMessage);

			soapMessage.saveChanges();

			return soapMessage;
		}
	}

	/**
	 * init soap message header
	 *
	 * @param pExportConfigItem export model
	 * @param pSoapMessage      soap message to init
	 */
	private void initSOAPMessageHeader(RepositoryItem pExportConfigItem, SOAPMessage pSoapMessage) {
		String operationAction = getOperationName(pExportConfigItem);
		if (!StringUtils.isBlank(operationAction)) {
			MimeHeaders headers = pSoapMessage.getMimeHeaders();
			headers.addHeader("SOAPAction", operationAction);
		}
	}

	/**
	 * this is a temp solution to test
	 * need to rework and provide mapping
	 *
	 * @param pSOAPMessage soap message to update with policy settings
	 */
	private void addWSPolicyInfo(SOAPMessage pSOAPMessage) throws SOAPException {
/*
<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
			   xmlns="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
	<wsse:UsernameToken>
		<wsse:Username>bssvdvatg1</wsse:Username>
		<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">bsv#852!#l</wsse:Password>
	</wsse:UsernameToken>
</wsse:Security>

*/

		String policyPrefix = "policy";
		TargetNamespace targetNamespace = TargetNamespaceUtil.createTargetNamespace(
				"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd",
				policyPrefix,
				getWSDLModel()
		);
		getEnvelope().setAttribute(
				"xmlns:" + targetNamespace.getPrefix(),
				targetNamespace.getNamespace()
		);

		SOAPElement security = addSOAPElementChild(pSOAPMessage.getSOAPHeader(), "Security", policyPrefix);
		SOAPElement usernameToken = addSOAPElementChild(security, "UsernameToken", policyPrefix);
		SOAPElement username = addSOAPElementChild(usernameToken, "Username", policyPrefix);
		username.setValue("bssvdvatg1");
		SOAPElement password = addSOAPElementChild(usernameToken, "Password", policyPrefix);
		password.setValue("bsv#852!#l");
		password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
	}

	/**
	 * init soap message content
	 *
	 * @param pRepositoryModel export model
	 * @param pSoapMessage     soap message to init
	 */
	private void initSOAPMessageContent(RepositoryItem pRepositoryModel, SOAPMessage pSoapMessage)
			throws SOAPException {

		String operationName = getOperationName(pRepositoryModel);
		Operation operation = getWSDLModel().getOperation(operationName);
		setOperation(operation);
		Type operationType = operation.getMessageInput().getType();

		SOAPPart soapPart = pSoapMessage.getSOAPPart();
		setEnvelope(soapPart.getEnvelope());
		SOAPBody soapBody = getEnvelope().getBody();
		setNameSpacePrefix("nms");
		String namespace = getWSDLModel().getTargetNamespace();
		Type inputType = getWSDLModel().getOperation(operationName).getMessageInput().getType();
/*
		try {
			TargetNamespace targetNamespace = inputType.getTargetNamespace();
			setNameSpacePrefix(targetNamespace.getPrefix());
			if(FileUtil.isFileByUriExist(targetNamespace.getSchemaLocation())) {
				namespace = targetNamespace.getNamespace();
			}
		} catch (Exception e) {
			//
		}
*/

		if (getNameSpacePrefix() == null) {
			setNameSpacePrefix("trgtnms");
		}
		getEnvelope().addNamespaceDeclaration(getNameSpacePrefix(), namespace);
		boolean addOperationType = addOperationNode(operationType);

		SOAPElement operationElement = null;
		if (addOperationType) {
			operationElement = addSOAPElementChild(soapBody, inputType.getName(), getNameSpacePrefix());
			if (getWSDLModel().getTargetNamespaceSoapEncoding() != null) {
				// envelope.setAttribute( "xmlns:env", "http://schemas.xmlsoap.org/soap/envelope/");
				operationElement.setAttribute("env:encodingStyle",
						getWSDLModel().getTargetNamespaceSoapEncoding().getNamespace()
				);
			}
		}

		SOAPElement topElement = operationElement == null ? soapBody : operationElement;
		if (getWSDLModel().getTargetNamespaceSoapEncoding() != null) {
			topElement.setAttribute(
					"xmlns:" + getWSDLModel().getTargetNamespaceSoapEncoding().getPrefix()
					, getWSDLModel().getTargetNamespaceSoapEncoding().getNamespace());
		}
		addSOAPMessageArguments(topElement, operationType);
	}

	/**
	 * @param pOperationType operation type
	 * @return true if operation type node should be added to request
	 */
	private boolean addOperationNode(Type pOperationType) {
		if (pOperationType.getType() != NodeType.COMPLEX) {
			return false;
		}
		boolean allElement = false;
		// check all sub types for TYPE
		for (Type subTypes : pOperationType.getSubTypes()) {
			if (subTypes.getType() == NodeType.COMPLEX) {
				allElement = true;
				break;
			}
		}
		return allElement;
	}

	/**
	 * build operation arguments
	 *
	 * @param pTopElement    soap operation element or soap body element
	 * @param pOperationType operation type
	 */
	protected void addSOAPMessageArguments(SOAPElement pTopElement, Type pOperationType) throws SOAPException {
		Type wsdlType = getTypeForRepositoryItemStart(pOperationType);
/*
		if(wsdlType.getTargetNamespace()!=null) {
			getEnvelope().setAttribute(
					"xmlns:" + getOperation().getName(),
					wsdlType.getTargetNamespace().getNamespace());
		}
*/
/*
		for(TargetNamespace targetNamespace : getWSDLModel().getTargetNamespaces()) {
			if(
					targetNamespace.getPrefix()!=null */
/*&&
					getWSDLModel().getTargetNamespacesPrefixesFromDefinition().contains(targetNamespace.getPrefix())*//*

			) {
				getEnvelope().setAttribute(
						"xmlns:"+targetNamespace.getPrefix(),
						targetNamespace.getNamespace());
			}
		}
*/
		int numberOfExportedItems = 0;
		try {
			mRepositoryItemSOAPElementInfos.clear();
			getTransactionInfo().setExportedItemsIds(new LinkedList<String>());
			for (RepositoryItem itemForExport : getSOAPCallInfo().getItemsForExport()) {
				String repositoryPath = itemForExport.getItemDescriptor().getItemDescriptorName();
				SOAPElementInfo typeSoapElementInfo = createChild(pTopElement, wsdlType, itemForExport);
/*
				if(wsdlType.getTargetNamespace()!=null) {
					typeSoapElementInfo.getSOAPElement().setAttribute(
						"xmlns:"+pOperationType.getName(),
						wsdlType.getTargetNamespace().getNamespace());
				}
*/
				RepositoryItemInfo repositoryItemInfo = new RepositoryItemInfo(itemForExport, repositoryPath);

				initSOAPElementWithRepositoryItem(repositoryItemInfo, typeSoapElementInfo);
				initSOAPElementWithStaticValues(typeSoapElementInfo);

				getSOAPCallInfo().getItemsExported()[numberOfExportedItems] = itemForExport;
				getTransactionInfo().getExportedItemsIds().add(itemForExport.getRepositoryId());
				numberOfExportedItems++;
				if (wsdlType.getMaxOccurs() != -1) {
					if (wsdlType.getMaxOccurs() == numberOfExportedItems) {
						return;
					}
				}
			}
		} catch (RepositoryException e) {
			trackError(e, "Unable to get repository values.");
		} finally {
			removeExportedItems();
		}
	}

	private void removeExportedItems() {
		RepositoryItem[] itemsForExport = getSOAPCallInfo().getItemsForExport();
		RepositoryItem[] itemsExported = getSOAPCallInfo().getItemsExported();
		Map<String, RepositoryItem> itemsForExportMap = new HashMap<String, RepositoryItem>();

		for (int i = 0; i < itemsForExport.length; i++) {
			RepositoryItem itemForExport = itemsForExport[i];
			if (null != itemForExport) {
				itemsForExportMap.put(itemForExport.getRepositoryId(), itemForExport);
			}
		}

		for (int i = 0; i < itemsExported.length; i++) {
			RepositoryItem itemExported = itemsExported[i];
			if (null != itemExported) {
				itemsForExportMap.remove(itemExported.getRepositoryId());
			}
		}

		RepositoryItem[] itemsForExportNew = new RepositoryItem[itemsForExportMap.size()];
		int i = 0;
		for (String key : itemsForExportMap.keySet()) {
			RepositoryItem item = itemsForExportMap.get(key);
			itemsForExportNew[i] = item;
			i++;
		}

		getSOAPCallInfo().setItemsForExport(itemsForExportNew);
	}


	/**
	 * we register all enterance for repository path
	 * <p/>
	 * process.InputParameters.P_LINE_ADJ_TBL.P_LINE_ADJ_TBL_ITEM.LINE_ID
	 * process.InputParameters.P_LINE_TBL.P_LINE_TBL_ITEM.LINE_ID
	 *
	 * @param pRepositoryPath repository path
	 * @param pWsdlPathes     wsdl pathes
	 */
	private void registerRepositoryPathToWsdlPath(String pRepositoryPath, Set<String> pWsdlPathes) {
		Set<String> wsdlPathes = mRepositoryPathWsdlPathLevels.get(pRepositoryPath);
		if (wsdlPathes == null) {
			wsdlPathes = new HashSet<String>();
			mRepositoryPathWsdlPathLevels.put(pRepositoryPath, wsdlPathes);
		}
		if (!wsdlPathes.equals(pWsdlPathes)) {
			for (String pWsdlPath : pWsdlPathes) {
				wsdlPathes.add(pWsdlPath);
			}
		}
	}

	/**
	 * we should check if soap element is under repository item
	 * if not - check item descriptor of owner
	 * if it differs - it's OK, otherwise - same repo item node
	 *
	 * @param pSOAPElementInfo SOAPElementInfo to check
	 * @param pRepositoryItem  repository item to check
	 * @return true if SOAPElement is linked to
	 */
	private boolean isSOAPElementUnderRepositoryItem(SOAPElementInfo pSOAPElementInfo, RepositoryItem pRepositoryItem) {
		Set<SOAPElementInfo> repositoryItemSOAPElements = mRepositoryItemSOAPElementInfos.get(pRepositoryItem);
		boolean isUnderThatElement = repositoryItemSOAPElements != null &&
				repositoryItemSOAPElements.contains(pSOAPElementInfo);
		if (isUnderThatElement) {
			return true;
		}
		for (RepositoryItem repositoryItem : mRepositoryItemSOAPElementInfos.keySet()) {
			repositoryItemSOAPElements = mRepositoryItemSOAPElementInfos.get(repositoryItem);
			if (repositoryItemSOAPElements.contains(pSOAPElementInfo)) {
				// check item descriptor differ - it's OK
				try {
					return !repositoryItem.getItemDescriptor().equals(pRepositoryItem.getItemDescriptor());
				} catch (RepositoryException e) {
					vlogError(e, "Unable to compare item descriptors");
				}
			}
		}
		return false;
	}

	/**
	 * @param pSOAPElementInfo SOAPElementInfo to check
	 * @return true if SOAPElement is linked
	 */
	private boolean isSOAPElementInfoRegistered(SOAPElementInfo pSOAPElementInfo) {
		for (Set<SOAPElementInfo> soapElementInfos : mRepositoryItemSOAPElementInfos.values()) {
			for (SOAPElementInfo soapElementInfo : soapElementInfos) {
				if (soapElementInfo.getSOAPElement().equals(pSOAPElementInfo.getSOAPElement())) {
					return true;
				}
			}

/*
			if(soapElementInfos.contains(pSOAPElementInfo)) {
				return true;
			}
*/
		}
		return false;
	}

	/**
	 * find soap element or create suitable item
	 *
	 * @param pRepositoryItemInfo    repository item info
	 * @param pParentSOAPElementInfo SOAPElement info to find sub child
	 * @param pWsdlPath              wsdl path to find SOAPElement
	 * @return SOAPElementInfo to add property value
	 * @throws SOAPException if error occurs
	 */
	private SOAPElementInfo getSOAPElementByWsdlPath(RepositoryItemInfo pRepositoryItemInfo,
													 SOAPElementInfo pParentSOAPElementInfo,
													 String pWsdlPath)
			throws SOAPException {
		if (pWsdlPath.contains(DELIMITER)) {

			RepositoryItem itemForCheck = pRepositoryItemInfo.getParentRepositoryItem() != null ?
					pRepositoryItemInfo.getParentRepositoryItem() : pRepositoryItemInfo.getRepositoryItem();
			String[] wsdlPathParts = pWsdlPath.split("\\" + DELIMITER);
			int index = 0;
			SOAPElementInfo soapElementInfo = null;
			SOAPElement parentSOAPElement = pParentSOAPElementInfo.getSOAPElement();
			Type parentWSDLType = pParentSOAPElementInfo.getWsdlType();
			for (String wsdlPathPart : wsdlPathParts) {

				if (index == wsdlPathParts.length - 1) {
					break;
				}

				Type wsdlType = parentWSDLType.getSubType(wsdlPathPart);

				Set<Node> possibleElements = DomUtil.getNodesByName(parentSOAPElement.getChildNodes(), wsdlPathPart);
				SOAPElement possibleElement = null;
				if (possibleElements != null) {
					for (Node node : possibleElements) {
						if (node instanceof SOAPElement) {
							SOAPElement currentPossibleElement = (SOAPElement) node;
							SOAPElementInfo toCheckIfRegistered = new SOAPElementInfo(currentPossibleElement, wsdlType);
							if (isSOAPElementUnderRepositoryItem(toCheckIfRegistered, itemForCheck)) {
								possibleElement = currentPossibleElement;
								break;
							}
						}
					}
				}

//				SOAPElement possibleElement = (SOAPElement) DomUtil.getNodeByName(
//						parentSOAPElement.getChildNodes(),
//						wsdlPathPart);

				if (possibleElement != null) {
					soapElementInfo = new SOAPElementInfo(possibleElement, wsdlType);
					soapElementInfo.setSOAPElement(possibleElement);
				} else {
					soapElementInfo = new SOAPElementInfo(
							createChild(parentSOAPElement, wsdlType), wsdlType
					);
					registerRepositoryItemSOAPElement(itemForCheck, soapElementInfo);
				}

				parentSOAPElement = soapElementInfo.getSOAPElement();
				parentWSDLType = soapElementInfo.getWsdlType();
				index++;
			}
			return soapElementInfo;
			// return checkSOAPElementInfo(soapElementInfo, pRepositoryItemInfo);
		} else {
			return pParentSOAPElementInfo;
		}
	}

	/**
	 * checks if SOAPElement is linked to repository item
	 *
	 * @param pSOAPElementInfo    SOAPElementInfo with SOAPElement to check
	 * @param pRepositoryItemInfo repository item info to check
	 * @return checked and reinitialized SOAPElementInfo
	 * @throws SOAPException if error occures
	 */
	private SOAPElementInfo checkSOAPElementInfo(SOAPElementInfo pSOAPElementInfo,
												 RepositoryItemInfo pRepositoryItemInfo)
			throws SOAPException {
		if (pRepositoryItemInfo == null) {
			return pSOAPElementInfo;
		}
		RepositoryItem itemForCheck = pRepositoryItemInfo.getParentRepositoryItem() != null ?
				pRepositoryItemInfo.getParentRepositoryItem() : pRepositoryItemInfo.getRepositoryItem();
		if (!isSOAPElementInfoRegistered(pSOAPElementInfo)) {
			registerRepositoryItemSOAPElement(itemForCheck, pSOAPElementInfo);
		} else {
			if (!isSOAPElementUnderRepositoryItem(pSOAPElementInfo, itemForCheck)) {
				SOAPElementInfo possibleElement = getSOAPElementInfo(itemForCheck,
						pSOAPElementInfo.getWsdlType());
				if (possibleElement != null) {
					return possibleElement;
				}

				// we should duplicate last node
				SOAPElement soapElement = pSOAPElementInfo.getSOAPElement();
				return createChild(
						soapElement.getParentElement(),
						pSOAPElementInfo.getWsdlType(),
						itemForCheck
				);
			}
		}
		return pSOAPElementInfo;
	}

	/**
	 * @param pRepositoryItem repository item
	 * @param pWsdlType       wsdl type
	 * @return linked SOAPElementInfo
	 */
	private SOAPElementInfo getSOAPElementInfo(RepositoryItem pRepositoryItem, Type pWsdlType) {
		Set<SOAPElementInfo> elementInfos = mRepositoryItemSOAPElementInfos.get(pRepositoryItem);
		if (elementInfos != null) {
			for (SOAPElementInfo soapElementInfo : elementInfos) {
				if (soapElementInfo.getWsdlType().getFullPath().equals(pWsdlType.getFullPath())) {
					return soapElementInfo;
				}
			}
		}
		return null;
	}

	/**
	 * register repository item to SOAPElementInfo
	 *
	 * @param pRepositoryItem  repository item
	 * @param pSOAPElementInfo SOAPElementInfo
	 */
	private void registerRepositoryItemSOAPElement(RepositoryItem pRepositoryItem, SOAPElementInfo pSOAPElementInfo) {
		Set<SOAPElementInfo> elementInfos = mRepositoryItemSOAPElementInfos.get(pRepositoryItem);
		if (elementInfos == null) {
			elementInfos = new TreeSet<SOAPElementInfo>();
			mRepositoryItemSOAPElementInfos.put(pRepositoryItem, elementInfos);
		}
		elementInfos.add(pSOAPElementInfo);
	}

	/**
	 * add repository item properties to SOAPElement
	 *
	 * @param pRepositoryItemInfo repository item info
	 * @param pSOAPElementInfo    soap element info
	 * @throws RepositoryException if error occures
	 * @throws SOAPException       if error occures
	 */
	private void initSOAPElementWithRepositoryItem(RepositoryItemInfo pRepositoryItemInfo,
												   SOAPElementInfo pSOAPElementInfo)
			throws RepositoryException, SOAPException {
		RepositoryItemDescriptor itemDescriptor = pRepositoryItemInfo.getRepositoryItem().getItemDescriptor();
		for (DynamicPropertyDescriptor propertyDescriptor : itemDescriptor.getPropertyDescriptors()) {

			String repositoryPath = pRepositoryItemInfo.getRepositoryPath() + DELIMITER + propertyDescriptor.getName();

			PropertyValueInfo propertyValueInfo = new PropertyValueInfo(propertyDescriptor);
			propertyValueInfo.analyzeRepositoryPathMappings(repositoryPath, getSOAPCallInfo().getRepositoryMappings());

			if (!propertyValueInfo.isUnderMapping()) {
				continue;
			}
			Object value = pRepositoryItemInfo.getRepositoryItem().getPropertyValue(propertyDescriptor.getName());
			propertyValueInfo.setProperyValue(value);
			registerRepositoryPathToWsdlPath(repositoryPath, propertyValueInfo.getWsdlPathes());

			if (value == null) {
				continue;
			}

			switch (propertyValueInfo.getType()) {
				case SIMPLE:
					addSOAPElementSimpleProperty(pSOAPElementInfo, pRepositoryItemInfo, propertyValueInfo);
					break;
				case REPOSITORY_ITEM:
					initSOAPElementWithRepositoryItem(
							new RepositoryItemInfo(
									(RepositoryItem) value,
									pRepositoryItemInfo.getRepositoryItem(),
									repositoryPath),
							pSOAPElementInfo
					);

					break;
				case COLLECTION_OF_REPOSITORY_ITEMS:
					for (RepositoryItem subItem : (Collection<RepositoryItem>) value) {
						initSOAPElementWithRepositoryItem(
								new RepositoryItemInfo(subItem, repositoryPath),
								pSOAPElementInfo
						);
					}
					break;
			}
		}
	}

	/**
	 * analyze formed request and add static value nodes
	 *
	 * @param pSOAPElementInfo top soap element
	 * @throws SOAPException if error occures
	 */
	private void initSOAPElementWithStaticValues(SOAPElementInfo pSOAPElementInfo) throws SOAPException {
		for (RepositoryItem mapping : getSOAPCallInfo().getRepositoryMappings()) {
			String staticValue = (String) mapping.getPropertyValue(PRP_RECORD_MAPPING_STATIC_VALUE);
			String wsdlPath = (String) mapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
			if (!StringUtils.isBlank(staticValue)) {

				String leftWsdlPath = null;
				String leftWsdlProperty = null;
				if (wsdlPath.length() > pSOAPElementInfo.getWsdlType().getFullPath().length()) {
					leftWsdlPath = wsdlPath.substring(
							pSOAPElementInfo.getWsdlType().getFullPath().length() + 1
					);
					leftWsdlProperty = wsdlPath.substring(wsdlPath.lastIndexOf(DELIMITER) + 1, wsdlPath.length());
				}
				if (leftWsdlPath == null) {
					continue;
				}
				String[] wsdlPathParts = leftWsdlPath.split("\\" + DELIMITER);
				List<SOAPElementInfo> nodesForStaticValues = new ArrayList<SOAPElementInfo>();
				formSOAPElementsForStaticValues(
						pSOAPElementInfo.getSOAPElement(),
						pSOAPElementInfo.getWsdlType(),
						wsdlPathParts,
						0,
						nodesForStaticValues
				);
				for (SOAPElementInfo nodeForStaticValue : nodesForStaticValues) {
					SOAPElementInfo propertySoapElementInfo = createChild(
							nodeForStaticValue.getSOAPElement(),
							nodeForStaticValue.getWsdlType().getSubType(leftWsdlProperty),
							null
					);
					propertySoapElementInfo.getSOAPElement().setValue(
							getStaticValue(staticValue)
					);
				}
			}
		}
	}

	private void formSOAPElementsForStaticValues(SOAPElement pSOAPElement,
												 Type pType,
												 String[] pWsdlNodeNames,
												 int pCurrentIndex,
												 List<SOAPElementInfo> pResultList) throws SOAPException {
		if (pCurrentIndex == pWsdlNodeNames.length - 1) {
			pResultList.add(
					new SOAPElementInfo(pSOAPElement, pType)
			);
			return;
		}

		Set<org.w3c.dom.Node> subElements = DomUtil.getNodesByNameFromDirectChildren(
				pSOAPElement.getChildNodes(),
				pWsdlNodeNames[pCurrentIndex]
		);
		if (subElements.isEmpty()) {
			Type subType = pType.getSubType(pWsdlNodeNames[pCurrentIndex]);
			SOAPElement subElement = createChild(
					pSOAPElement,
					subType
			);
			formSOAPElementsForStaticValues(
					subElement,
					subType,
					pWsdlNodeNames,
					pCurrentIndex + 1,
					pResultList
			);
		} else {
			Type subType = pType.getSubType(pWsdlNodeNames[pCurrentIndex]);
			for (org.w3c.dom.Node subElement : subElements) {
				formSOAPElementsForStaticValues(
						(SOAPElement) subElement,
						subType,
						pWsdlNodeNames,
						pCurrentIndex + 1,
						pResultList
				);
			}
		}
	}

	/**
	 * add SOAPElement based on value of RepositoryItem type
	 *
	 * @param pSOAPElementInfo    SOAPElement info
	 * @param pRepositoryItemInfo repository item info
	 * @param pPropertyValueInfo  property value info
	 * @throws SOAPException if error occures
	 */
	private void addSOAPElementSimpleProperty(SOAPElementInfo pSOAPElementInfo,
											  RepositoryItemInfo pRepositoryItemInfo,
											  PropertyValueInfo pPropertyValueInfo)
			throws SOAPException {
		if (!pPropertyValueInfo.getWsdlPathes().isEmpty()) {
			for (String propertyWSDLPath : pPropertyValueInfo.getWsdlPathes()) {
				if (propertyWSDLPath.startsWith(pSOAPElementInfo.getWsdlType().getFullPath())) {
					String leftWsdlPath = "";
					if (propertyWSDLPath.length() > pSOAPElementInfo.getWsdlType().getFullPath().length()) {
						leftWsdlPath = propertyWSDLPath.substring(
								pSOAPElementInfo.getWsdlType().getFullPath().length() + 1
						);
					}
					String leftWsdlProperty = propertyWSDLPath.substring(
							propertyWSDLPath.lastIndexOf(DELIMITER) + 1, propertyWSDLPath.length());

					SOAPElementInfo soapElementInfo = getSOAPElementByWsdlPath(
							pRepositoryItemInfo,
							pSOAPElementInfo,
							leftWsdlPath
					);

					if (soapElementInfo != null) {
						if (!StringUtils.isBlank(leftWsdlPath)) {
							SOAPElementInfo propertySoapElementInfo = createChild(
									soapElementInfo.getSOAPElement(),
									soapElementInfo.getWsdlType().getSubType(leftWsdlProperty),
									pRepositoryItemInfo.getRepositoryItem()
							);
							propertySoapElementInfo.getSOAPElement().setValue(
									getPreviewService().getObjectToStringValue(
											pPropertyValueInfo.getProperyValue(), true
									)
							);
						} else {
							soapElementInfo.getSOAPElement().setValue(
									getPreviewService().getObjectToStringValue(
											pPropertyValueInfo.getProperyValue(), true
									)
							);
						}
					}
				} else {
					// mismapping
					if(isLoggingDebug()){
						logDebug("mismapping");
					}
				}
			}
		}
	}

	/**
	 * create and register SOAPElement with its wsdl path
	 *
	 * @param pParentElement  parent SOAPElement  @return created child SOAPElement
	 * @param pWSDLType       WSDL type
	 * @param pRepositoryItem repository item to link with SOAPElement
	 * @throws SOAPException if error occurs
	 */
	private SOAPElementInfo createChild(SOAPElement pParentElement,
										Type pWSDLType,
										RepositoryItem pRepositoryItem)
			throws SOAPException {
		SOAPElement soapElement = createSubElement(pParentElement, pWSDLType);
		analyzeComplexSOAPElement(soapElement, pWSDLType);
		SOAPElementInfo soapElementInfo = new SOAPElementInfo(soapElement, pWSDLType);
		if (pRepositoryItem != null) {
			registerRepositoryItemSOAPElement(pRepositoryItem, soapElementInfo);
		}
		return soapElementInfo;
	}

	/**
	 * create SOAPElement with its wsdl path
	 *
	 * @param pParentElement parent SOAPElement  @return created child SOAPElement
	 * @param pWSDLType      WSDL type
	 * @throws SOAPException if error occurs
	 */
	private SOAPElement createChild(SOAPElement pParentElement, Type pWSDLType)
			throws SOAPException {
		SOAPElement soapElement = createSubElement(pParentElement, pWSDLType);
		analyzeComplexSOAPElement(soapElement, pWSDLType);
		return soapElement;
	}

	/**
	 * create sub SOAPElement accordint to Type and target namespace
	 *
	 * @param pSOAPElement parent soap element
	 * @param pSubType     sub type to create sub soap element
	 * @return sub element
	 * @throws SOAPException if error occures
	 */
	private SOAPElement createSubElement(SOAPElement pSOAPElement, Type pSubType) throws SOAPException {
		SOAPElement soapElement;
		if (pSubType.getTargetNamespace() == null) {
			soapElement = addSOAPElementChild(
					pSOAPElement,
					pSubType.getName()
			);
		} else {
			if (pSubType.getTargetNamespace().getPrefix() == null || pSubType.getTargetNamespace().isMain()) {
				soapElement = addSOAPElementChild(
						pSOAPElement,
						pSubType.getName()
				);
			} else {
				soapElement = addSOAPElementChild(
						pSOAPElement,
						pSubType.getName(),
						pSubType.getTargetNamespace().getPrefix()
				);
			}
		}
		return soapElement;
	}


	/**
	 * @param pOperationType message operation input type
	 * @return wsdl type to start SOAPElement for each repository item
	 */
	private Type getTypeForRepositoryItemStart(Type pOperationType) {
		Type type = pOperationType;
		if (pOperationType.getSubTypes().size() == 1) {
			if (pOperationType.getSubTypes().get(0).getType() == NodeType.COMPLEX) {
				type = pOperationType.getSubTypes().get(0);
			}
		}
		return type;
	}

	/**
	 * init soap element
	 *
	 * @param pSOAPElement        soap element
	 * @param pType               wsdl type
	 * @param pItemForExport      repository items for export
	 * @param pFirstLevel         is type of first level
	 * @param pPrefixPathToRemove prefix to remove from repository path
	 * @throws SOAPException if error occures
	 */
	private void initSoapElementByType(SOAPElement pSOAPElement, Type pType,
									   RepositoryItem pItemForExport, boolean pFirstLevel,
									   String pPrefixPathToRemove) throws SOAPException {
		if (!isTypeInMappings(pType)) { // no need to create empty nodes
			return;
		}
		switch (pType.getType()) {
			case COMPLEX:
				if (pFirstLevel) {
					initComplexParameterFirstLevel(pSOAPElement, pType, pItemForExport);
				} else {
					initComplexParameter(pSOAPElement, pType, pItemForExport, pPrefixPathToRemove);
				}
				break;
			case ELEMENT:
				initElementParameter(pSOAPElement, pType, pItemForExport, pPrefixPathToRemove);
				break;
			case SIMPLE:
				initSimpleParameter(pSOAPElement, pType, pItemForExport, pPrefixPathToRemove);
				break;
		}
	}


	/**
	 * @param pSOAPElement        soap element to add child
	 * @param pType               wsdl type
	 * @param pItemForExport      items for export
	 * @param pPrefixPathToRemove prefix to remove from repository path
	 * @throws SOAPException if error occurs
	 */
	private void initElementParameter(SOAPElement pSOAPElement, Type pType,
									  RepositoryItem pItemForExport, String pPrefixPathToRemove)
			throws SOAPException {
		RepositoryItem propertyMapping = getPropertyMappingByType(pType);
		if (propertyMapping != null) {
			String value;
/*
			String staticValue = (String) propertyMapping.getPropertyValue(PRP_RECORD_MAPPING_STATIC_VALUE);
			if(!StringUtils.isBlank(staticValue)) {
				value = staticValue;
			} else {
*/
			value = getValueByRepositoryPath(
					pItemForExport,
					(String) propertyMapping.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH),
					pPrefixPathToRemove
			);
/*
			}
*/
			if (value != null) {
				SOAPElement soapBodyElemInput = createSubElement(pSOAPElement, pType);
				soapBodyElemInput.addTextNode(value);
			}
		} else {
			// no repository mapping is defined
			if(isLoggingDebug()){
				logDebug("no repository mapping is defined");
			}
		}
	}

	/**
	 * init soap element according to message type
	 *
	 * @param pSOAPElement        operation soap element to fill
	 * @param pType               message type
	 * @param pItemForExport      item for export
	 * @param pPrefixPathToRemove prefix to remove from repository path
	 * @throws SOAPException if error occurs
	 */
	private void initComplexParameter(SOAPElement pSOAPElement, Type pType,
									  RepositoryItem pItemForExport, String pPrefixPathToRemove)
			throws SOAPException {
		Set<RepositoryItem> filteredMappings = RecordMappingUtil.getRepositoryMappingsByType(pType,
				getSOAPCallInfo().getRepositoryMappings());
		String lessRepositoryPath = getLessRepositoryPath(filteredMappings);

		ComplexTypeSubRelationInfo subCollection = getFirstCollectionByPath(pItemForExport, lessRepositoryPath);

		if (subCollection != null && subCollection.getCollection() != null) {
			int index = 0;
			for (Object subElement : subCollection.getCollection()) {
				SOAPElement typeElement = createSubElement(pSOAPElement, pType);
				analyzeComplexSOAPElement(typeElement, pType);
				for (Type subType : pType.getSubTypes()) {
					initSoapElementByType(typeElement, subType, (RepositoryItem) subElement, false,
							subCollection.getPrefixPathToRemove());
				}
				if (pType.getMaxOccurs() != -1) {
					if (index == pType.getMaxOccurs()) {
						break;
					}
				}
				index++;
			}
		}
	}

	/**
	 * @param pItem repository item
	 * @param pPath
	 * @return
	 */
	private ComplexTypeSubRelationInfo getFirstCollectionByPath(RepositoryItem pItem, String pPath) {
		String[] properties = split(pPath);
		for (int i = 1; i < properties.length; i++) {
			String mPrefixPath = properties[0] + ".";
			String property = properties[i];
			Object propertyValue = pItem.getPropertyValue(property);
			if (propertyValue == null) {
				return null;
			} else {
				if (propertyValue instanceof Collection) {
					Collection collection = (Collection) propertyValue;
					Iterator iterator = collection.iterator();
					if (iterator.hasNext() && iterator.next() instanceof RepositoryItem) {
						ComplexTypeSubRelationInfo subRelations = new ComplexTypeSubRelationInfo();
						subRelations.setCollection(collection);
						subRelations.setPrefixPathToRemove(mPrefixPath + property);
						return subRelations;
					}
				}
			}
			mPrefixPath += property + ".";
		}
		return null;
	}

	/**
	 * init soap element according to message type
	 *
	 * @param pSOAPElement   operation soap element to fill
	 * @param pType          message type
	 * @param pItemForExport item for export
	 * @throws SOAPException if error occurs
	 */
	private void initComplexParameterFirstLevel(SOAPElement pSOAPElement, Type pType,
												RepositoryItem pItemForExport)
			throws SOAPException {
		SOAPElement typeElement = createSubElement(pSOAPElement, pType);
		analyzeComplexSOAPElement(typeElement, pType);
		for (Type subType : pType.getSubTypes()) {
			initSoapElementByType(typeElement, subType, pItemForExport, false, null);
		}
	}

	private void initSimpleParameter(SOAPElement pSOAPElement, Type pType,
									 RepositoryItem pItemForExport,
									 String pPrefixPathToRemove) {

		throw new RuntimeException("SIMPLE TYPE IS NOT IMPLEMENTED!.");
	}


	/**
	 * /**
	 *
	 * @param pRepositoryMappings set of repository mappings
	 * @return less repository path from set of repository mappings
	 */
	private String getLessRepositoryPath(Set<RepositoryItem> pRepositoryMappings) {
		String testPath = null;
		for (RepositoryItem repositoryMapping : pRepositoryMappings) {
			String repositoryPath = (String) repositoryMapping.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH);
			if (testPath == null) {
				testPath = repositoryPath;
			} else {
				if (testPath.length() > repositoryPath.length()) {
					testPath = repositoryPath;
				}
			}
		}
		return testPath;
	}

	/**
	 * @param pType wsdl type
	 * @return true if type is under repository mappings
	 */
	private boolean isTypeInMappings(Type pType) {
		for (RepositoryItem repositoryMapping : getSOAPCallInfo().getRepositoryMappings()) {
			String wsdlPath = (String) repositoryMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
			if (wsdlPath.startsWith(pType.getFullPath())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param pType wsdl type
	 * @return property mapping if will be found by type, otherwise - null
	 */
	private RepositoryItem getPropertyMappingByType(Type pType) {
		for (RepositoryItem repositoryMapping : getSOAPCallInfo().getRepositoryMappings()) {
			String wsdlPath = (String) repositoryMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
			if (wsdlPath.endsWith(pType.getFullPath())) {
				return repositoryMapping;
			}
		}
		return null;
	}

	/**
	 * @param pRepositoryItem     repository item
	 * @param pRepositoryPath     repository path  @return value by path from repository item
	 * @param pPrefixPathToRemove prefix to remove from repository path
	 */
	private String getValueByRepositoryPath(RepositoryItem pRepositoryItem, String pRepositoryPath,
											String pPrefixPathToRemove) {
		String path;
		if (StringUtils.isBlank(pPrefixPathToRemove)) {
			path = pRepositoryPath.substring(pRepositoryPath.indexOf(".") + 1);
		} else {
			path = pRepositoryPath.replace(pPrefixPathToRemove + ".", "");
			// path = pRepositoryPath.substring(pRepositoryPath.indexOf(".") + 1);
		}
		return getPreviewService().getRepositoryItemValue(pRepositoryItem, path, true);
	}

	/**
	 * @param pStr string to split
	 * @return array of elements, delimeter by '.'
	 */
	private String[] split(String pStr) {
		return pStr.split("\\.");
	}

	/**
	 * @param pExportConfigItem export config repository item
	 * @return operation name
	 */
	private String getOperationName(RepositoryItem pExportConfigItem) {
		try {
			RepositoryItem outputSettings = (RepositoryItem) pExportConfigItem.getPropertyValue(
					PRP_EXPORT_OUTPUT_FORMAT);
			return (String) outputSettings.getPropertyValue(PRP_OFC_WSDL_OPERATION);
		} catch (Exception e) {
			trackError(e, "Unable to get operaton from stored export configuration.");
		}
		return null;
	}

	private void analyzeComplexSOAPElement(SOAPElement pSOAPElement, Type pTypeOfArrayElement) {
		// soapenc:arrayType="vsg:VsgTestOrderItem[]"
		if (pTypeOfArrayElement.getType() == NodeType.COMPLEX) {
			if (pTypeOfArrayElement.getSubTypes().size() == 1) {
				Type subType = pTypeOfArrayElement.getSubTypes().get(0);
				if (subType.getType() == NodeType.COMPLEX && subType.isArrayElement()) {
					String attributeName = "arrayType";
					if (getWSDLModel().getTargetNamespaceSoapEncoding() != null) {
						attributeName = getWSDLModel().getTargetNamespaceSoapEncoding().getPrefix() + ":" + attributeName;
					}
					pSOAPElement.setAttribute(attributeName, getOperation().getName() + ":" + subType.getName() + "[]");
				}
			}
		}
	}

	/**
	 * @param pStaticValue static value to check
	 * @return static value with replaced {RANDOM} with time in mills
	 */
	private String getStaticValue(String pStaticValue) {
		if (pStaticValue.contains(RANDOM)) {
			return pStaticValue.replaceAll(RANDOM, String.valueOf(new Date().getTime()));
		}
		return pStaticValue;
	}

	/**
	 * @param pSOAPElement soap element
	 * @param pChildName   child name
	 * @throws SOAPException if error occurs
	 */
	private SOAPElement addSOAPElementChild(SOAPElement pSOAPElement, String pChildName) throws SOAPException {
		SOAPElement child = pSOAPElement.addChildElement(pChildName);
		// logDebug(child.toString());
		return child;
	}

	/**
	 * @param pSOAPElement soap element
	 * @param pChildName   child name
	 * @param pPrefix      prefix
	 * @throws SOAPException if error occurs
	 */
	private SOAPElement addSOAPElementChild(SOAPElement pSOAPElement, String pChildName, String pPrefix) throws
			SOAPException {
		TargetNamespace targetNamespace = getWSDLModel().getTargetNamespaceByPrefix(pPrefix);
		SOAPElement child;
		if (targetNamespace == null) {
			child = pSOAPElement.addChildElement(
					pChildName,
					pPrefix
			);
		} else {
			child = pSOAPElement.addChildElement(
					pChildName,
					pPrefix,
					targetNamespace.getNamespace()
			);
		}
		// logDebug(child.toString());
		return child;
/*
		SOAPElement soapElement = addSOAPElementChild(pSOAPElement, pChildName);
		QName prefixAttribute = new QName("xmlns:" + pPrefix);

		soapElement.addAttribute(prefixAttribute, getWSDLModel().getTargetNamespaceByPrefix(pPrefix).getNamespace());
		return soapElement;
*/
	}

	//------------------------------------------------------------------------------------------------------------------

	public RepositoryItemPreviewService getPreviewService() {
		return mPreviewService;
	}

	public void setPreviewService(RepositoryItemPreviewService pPreviewService) {
		mPreviewService = pPreviewService;
	}

	public String getNameSpacePrefix() {
		return mNameSpacePrefix;
	}

	public void setNameSpacePrefix(String pNameSpacePrefix) {
		mNameSpacePrefix = pNameSpacePrefix;
	}

	public SOAPCallInfo getSOAPCallInfo() {
		return mSOAPCallInfo;
	}

	public void setSOAPCallInfo(SOAPCallInfo pSOAPCallInfo) {
		mSOAPCallInfo = pSOAPCallInfo;
	}

	public TransactionInfo getTransactionInfo() {
		return getSOAPCallInfo().getTransactionInfo();
	}

	public WSDLModel getWSDLModel() {
		return mWSDLModel;
	}

	public void setWSDLModel(WSDLModel pWSDLModel) {
		mWSDLModel = pWSDLModel;
	}

	public Operation getOperation() {
		return mOperation;
	}

	public void setOperation(Operation pOperation) {
		mOperation = pOperation;
	}

	public SOAPEnvelope getEnvelope() {
		return mEnvelope;
	}

	public void setEnvelope(SOAPEnvelope pEnvelope) {
		mEnvelope = pEnvelope;
	}

	public Set<String> getAddedToEnvelopeTargetNamespaces() {
		return mAddedToEnvelopeTargetNamespaces;
	}

	public void setAddedToEnvelopeTargetNamespaces(Set<String> pAddedToEnvelopeTargetNamespaces) {
		mAddedToEnvelopeTargetNamespaces = pAddedToEnvelopeTargetNamespaces;
	}
}
