package vsg.integration.export.service.soap.util;

import vsg.integration.export.parse.wsdl.bean.Type;

import javax.xml.soap.SOAPElement;

/**
 * @author Dmitry Golubev
 */
public class ParentSOAPElementInfo {
	/**
	 * parent SOAPElement
	 */
	private SOAPElement mParentSOAPElement;
	/**
	 * parent wsdl type
	 */
	private Type mParentWSDLType;

	public SOAPElement getParentSOAPElement() {
		return mParentSOAPElement;
	}

	public void setParentSOAPElement(SOAPElement pParentSOAPElement) {
		mParentSOAPElement = pParentSOAPElement;
	}

	public Type getParentWSDLType() {
		return mParentWSDLType;
	}

	public void setParentWSDLType(Type pParentWSDLType) {
		mParentWSDLType = pParentWSDLType;
	}
}
