package vsg.integration.export.service.soap.util;

import vsg.integration.export.parse.wsdl.bean.Type;

import javax.xml.soap.SOAPElement;
import java.util.Objects;

/**
 * @author Dmitry Golubev
 */
public class SOAPElementInfo implements Comparable {
	/**
	 * current SOAPElement
	 */
	private SOAPElement mSOAPElement;
	/**
	 * current wsdl type
	 */
	private Type mWsdlType;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pSource source for SOAPElementInfo
	 */
	public SOAPElementInfo(SOAPElementInfo pSource) {
		setSOAPElement(pSource.getSOAPElement());
		setWsdlType(pSource.getWsdlType());
	}

	/**
	 * constructor
	 *
	 * @param pSOAPElement soap element
	 * @param pWsdlType    wsdl type
	 */
	public SOAPElementInfo(SOAPElement pSOAPElement, Type pWsdlType) {
		mSOAPElement = pSOAPElement;
		mWsdlType = pWsdlType;
	}

	//------------------------------------------------------------------------------------------------------------------

	public SOAPElement getSOAPElement() {
		return mSOAPElement;
	}

	public void setSOAPElement(SOAPElement pSOAPElement) {
		mSOAPElement = pSOAPElement;
	}

	public Type getWsdlType() {
		return mWsdlType;
	}

	public void setWsdlType(Type pWsdlType) {
		mWsdlType = pWsdlType;
	}

	// override


	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof SOAPElementInfo)) {
			return false;
		}
		SOAPElementInfo checkObj = (SOAPElementInfo) obj;
		if (checkObj.getSOAPElement().equals(getSOAPElement()) &&
				checkObj.getWsdlType().getFullPath().equals(getWsdlType().getFullPath())) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(mSOAPElement, mWsdlType.getFullPath());
	}

	@Override
	public int compareTo(Object obj) {
		if (obj == null || !(obj instanceof SOAPElementInfo)) {
			return -1;
		}
		SOAPElementInfo checkObj = (SOAPElementInfo) obj;
		if (checkObj.getSOAPElement().equals(getSOAPElement()) &&
				checkObj.getWsdlType().getFullPath().equals(getWsdlType().getFullPath())) {
			return 0;
		}
		return -1;
	}
}
