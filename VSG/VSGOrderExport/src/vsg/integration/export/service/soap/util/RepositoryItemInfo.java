package vsg.integration.export.service.soap.util;

import atg.repository.RepositoryItem;

/**
 * @author Dmitry Golubev
 */
public class RepositoryItemInfo {
	/**
	 * repository item
	 */
	private RepositoryItem mRepositoryItem;
	/**
	 * parent repository item (single-to-single) relation
	 */
	private RepositoryItem mParentRepositoryItem;
	/**
	 * repository item path
	 */
	private String mRepositoryPath;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pRepositoryItem repository item
	 * @param pRepositoryPath repository item path
	 */
	public RepositoryItemInfo(RepositoryItem pRepositoryItem, String pRepositoryPath) {
		mRepositoryItem = pRepositoryItem;
		mRepositoryPath = pRepositoryPath;
	}

	/**
	 * constructor
	 *
	 * @param pRepositoryItem       repository item
	 * @param pParentRepositoryItem parent repository item (single-to-single) relation
	 * @param pRepositoryPath       repository item path
	 */
	public RepositoryItemInfo(RepositoryItem pRepositoryItem,
							  RepositoryItem pParentRepositoryItem,
							  String pRepositoryPath) {
		mRepositoryItem = pRepositoryItem;
		mParentRepositoryItem = pParentRepositoryItem;
		mRepositoryPath = pRepositoryPath;
	}

	//------------------------------------------------------------------------------------------------------------------


	public RepositoryItem getRepositoryItem() {
		return mRepositoryItem;
	}

	public String getRepositoryPath() {
		return mRepositoryPath;
	}

	public RepositoryItem getParentRepositoryItem() {
		return mParentRepositoryItem;
	}
}
