package vsg.integration.export.service.soap;

import atg.repository.RepositoryItem;

import java.util.Collection;

/**
 * @author Dmitry Golubev
 */
public class ComplexTypeSubRelationInfo {
	/**
	 * prefix path to remove to get correct name
	 */
	private String mPrefixPathToRemove;
	/**
	 * sub items
	 */
	private Collection<RepositoryItem> mCollection;

	//------------------------------------------------------------------------------------------------------------------

	public String getPrefixPathToRemove() {
		return mPrefixPathToRemove;
	}

	public void setPrefixPathToRemove(String pPrefixPathToRemove) {
		mPrefixPathToRemove = pPrefixPathToRemove;
	}

	public Collection<RepositoryItem> getCollection() {
		return mCollection;
	}

	public void setCollection(Collection<RepositoryItem> pCollection) {
		mCollection = pCollection;
	}
}
