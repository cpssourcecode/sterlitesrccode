package vsg.integration.export.service.soap;

import atg.repository.RepositoryItem;
import vsg.integration.export.util.bean.TransactionInfo;

import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class SOAPCallInfo {
	/**
	 * repository items for export
	 */
	private RepositoryItem[] mItemsForExport = null;
	/**
	 * exported repository items
	 */
	private RepositoryItem[] mItemsExported = null;
	/**
	 * current set of repository mappings
	 */
	private Set<RepositoryItem> mRepositoryMappings = null;
	/**
	 * Transaction info
	 */
	private TransactionInfo mTransactionInfo;

	//------------------------------------------------------------------------------------------------------------------

	public RepositoryItem[] getItemsForExport() {
		return mItemsForExport;
	}

	public void setItemsForExport(RepositoryItem[] pItemsForExport) {
		mItemsForExport = pItemsForExport;
	}

	public RepositoryItem[] getItemsExported() {
		return mItemsExported;
	}

	public void setItemsExported(RepositoryItem[] pItemsExported) {
		mItemsExported = pItemsExported;
	}

	public Set<RepositoryItem> getRepositoryMappings() {
		return mRepositoryMappings;
	}

	public void setRepositoryMappings(Set<RepositoryItem> pRepositoryMappings) {
		mRepositoryMappings = pRepositoryMappings;
	}

	public TransactionInfo getTransactionInfo() {
		return mTransactionInfo;
	}

	public void setTransactionInfo(TransactionInfo pTransactionInfo) {
		mTransactionInfo = pTransactionInfo;
	}

}
