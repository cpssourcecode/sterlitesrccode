package vsg.integration.export.service.soap;

import atg.beans.DynamicPropertyDescriptor;
import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class PropertyValueInfo implements IRepositoryConstants {
	public enum PropertyValueInfoType {
		SIMPLE,
		REPOSITORY_ITEM,
		COLLECTION_OF_REPOSITORY_ITEMS
	}

	/**
	 * property value
	 */
	private Object mProperyValue;
	/**
	 * property name
	 */
	private DynamicPropertyDescriptor mPropertyDescriptor;
	/**
	 * propery is under property mapping
	 */
	private boolean mUnderMapping = false;
	/**
	 * propery is direct mapped
	 */
	private boolean mDirectMapped = false;
	/**
	 * repository path
	 */
	private String mRepositoryPath = null;
	/**
	 * property value type
	 */
	private PropertyValueInfoType mType = PropertyValueInfoType.SIMPLE;
	/**
	 * wsdl pathes
	 */
	private Set<String> mWsdlPathes = Collections.emptySet();

	//------------------------------------------------------------------------------------------------------------------

	public Object getProperyValue() {
		return mProperyValue;
	}

	public PropertyValueInfoType getType() {
		return mType;
	}

	public boolean isUnderMapping() {
		return mUnderMapping;
	}

	public boolean isDirectMapped() {
		return mDirectMapped;
	}

	public Set<String> getWsdlPathes() {
		return mWsdlPathes;
	}

	public String getRepositoryPath() {
		return mRepositoryPath;
	}

	public DynamicPropertyDescriptor getPropertyDescriptor() {
		return mPropertyDescriptor;
	}

	public String getPropertyName() {
		return getPropertyDescriptor() != null ? getPropertyDescriptor().getName() : null;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pPropertyValue     property value
	 * @param propertyDescriptor property descriptor
	 */
	public PropertyValueInfo(Object pPropertyValue, DynamicPropertyDescriptor propertyDescriptor) {
		this(propertyDescriptor);
		mProperyValue = pPropertyValue;
	}

	/**
	 * constructor
	 *
	 * @param propertyDescriptor property descriptor
	 */
	public PropertyValueInfo(DynamicPropertyDescriptor propertyDescriptor) {
		mPropertyDescriptor = propertyDescriptor;

		Class<?> propertyType = propertyDescriptor.getPropertyType();
		Class<?> componentType = propertyDescriptor.getComponentPropertyType();

		if (propertyType.isAssignableFrom(RepositoryItem.class)) {
			mType = PropertyValueInfoType.REPOSITORY_ITEM;
		} else {
			if (Collection.class.isAssignableFrom(propertyType) &&
					componentType != null && componentType.isAssignableFrom(RepositoryItem.class)) {
				mType = PropertyValueInfoType.COLLECTION_OF_REPOSITORY_ITEMS;
			}
		}

	}

	/**
	 * check if property is under mappings
	 *
	 * @param pRepositoryPath     repository path
	 * @param pRepositoryMappings repository property mappings
	 */
	public void analyzeRepositoryPathMappings(String pRepositoryPath, Set<RepositoryItem> pRepositoryMappings) {
		mRepositoryPath = pRepositoryPath;
		for (RepositoryItem mappimg : pRepositoryMappings) {
			String repositoryPath = (String) mappimg.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH);
			if (repositoryPath != null) {
				if (!mUnderMapping) {
					mUnderMapping = repositoryPath.startsWith(pRepositoryPath);
				}
				if (!mDirectMapped) {
					mDirectMapped = repositoryPath.equals(pRepositoryPath);
				}
			}
		}
		if (mDirectMapped) {
			mWsdlPathes = getPropertyMappedWsdlPath(pRepositoryPath, pRepositoryMappings, true);
		} else if (mUnderMapping) {
			mWsdlPathes = getPropertyMappedWsdlPath(pRepositoryPath, pRepositoryMappings, false);
		}
	}

	/**
	 * @param pRepositoryPath     repository path
	 * @param pRepositoryMappings repository property mappings
	 * @return wsdl pathes by repository path
	 */
	private Set<String> getPropertyMappedWsdlPath(String pRepositoryPath, Set<RepositoryItem> pRepositoryMappings,
												  boolean pFullMatch) {
		Set<String> wsdlPathes = new HashSet<String>();
		for (RepositoryItem recordMapping : pRepositoryMappings) {
			Object repositoryPath = recordMapping.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH);
			if (repositoryPath != null) {
				if ((pFullMatch && repositoryPath.equals(pRepositoryPath)) ||
						(!pFullMatch && ((String) repositoryPath).startsWith(pRepositoryPath))) {
					wsdlPathes.add((String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH));
				}
			}
		}
		return wsdlPathes;
	}

	public void setProperyValue(Object pProperyValue) {
		mProperyValue = pProperyValue;
	}
}
