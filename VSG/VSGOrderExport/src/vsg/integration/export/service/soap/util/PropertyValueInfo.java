package vsg.integration.export.service.soap.util;

import atg.beans.DynamicPropertyDescriptor;
import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class PropertyValueInfo implements IRepositoryConstants {
	public enum PropertyValueInfoType {
		SIMPLE,
		REPOSITORY_ITEM,
		COLLECTION_OF_REPOSITORY_ITEMS
	}

	/**
	 * property value
	 */
	private Object mProperyValue;
	/**
	 * propery is under property mapping
	 */
	private boolean mUnderMapping = false;
	/**
	 * repository path
	 */
	private String mRepositoryPath = null;
	/**
	 * property value type
	 */
	private PropertyValueInfoType mType = PropertyValueInfoType.SIMPLE;
	/**
	 * wsdl pathes
	 */
	private Set<String> mWsdlPathes = null;


	//------------------------------------------------------------------------------------------------------------------

	public Object getProperyValue() {
		return mProperyValue;
	}

	public PropertyValueInfoType getType() {
		return mType;
	}

	public boolean isUnderMapping() {
		return mUnderMapping;
	}

	public Set<String> getWsdlPathes() {
		return mWsdlPathes;
	}

	public String getRepositoryPath() {
		return mRepositoryPath;
	}

	//------------------------------------------------------------------------------------------------------------------

	public PropertyValueInfo(Object pPropertyValue, DynamicPropertyDescriptor propertyDescriptor) {
		mProperyValue = pPropertyValue;

		Class<?> propertyType = propertyDescriptor.getPropertyType();
		Class<?> componentType = propertyDescriptor.getComponentPropertyType();

		if (propertyType.isAssignableFrom(RepositoryItem.class)) {
			mType = PropertyValueInfoType.REPOSITORY_ITEM;
		} else {
			if (Collection.class.isAssignableFrom(propertyType) &&
					componentType != null && componentType.isAssignableFrom(RepositoryItem.class)) {
				mType = PropertyValueInfoType.COLLECTION_OF_REPOSITORY_ITEMS;
			}
		}

	}

	/**
	 * @param pRepositoryPath     repository path
	 * @param pRepositoryMappings repository property mappings
	 * @return wsdl pathes by repository path
	 */
	private Set<String> getPropertyMappedWsdlPath(String pRepositoryPath, Set<RepositoryItem> pRepositoryMappings) {
		Set<String> wsdlPathes = new HashSet<String>();
		for (RepositoryItem recordMapping : pRepositoryMappings) {
			Object repositoryPath = recordMapping.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH);
			if (repositoryPath != null && repositoryPath.equals(pRepositoryPath)) {
				wsdlPathes.add((String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH));
			}
		}
		return wsdlPathes;
	}

}
