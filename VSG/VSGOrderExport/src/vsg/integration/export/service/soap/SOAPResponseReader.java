package vsg.integration.export.service.soap;

import atg.adapter.gsa.GSAItemDescriptor;
import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.beans.DynamicPropertyDescriptor;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import org.w3c.dom.Node;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.parse.wsdl.DomConstants;
import vsg.integration.export.parse.wsdl.DomUtil;
import vsg.integration.export.parse.wsdl.bean.Message;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.service.ErrorTracker;
import vsg.integration.export.service.soap.util.RepositoryItemInfo;
import vsg.integration.export.service.soap.util.SOAPElementInfo;
import vsg.integration.export.util.ObjectXMLStringFormatter;
import vsg.integration.export.util.bean.TransactionInfo;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.beans.IntrospectionException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class SOAPResponseReader extends ErrorTracker implements IRepositoryConstants, DomConstants {
	/**
	 * object to string formatter
	 */
	private ObjectXMLStringFormatter mFormatter;
	/**
	 * current mappings for wsdl response
	 */
	private Set<RepositoryItem> mRecordMappings = null;
	/**
	 * properties delimeter
	 */
	private final static String DELIMETER = ".";
	/**
	 * SOAPBody soapBody
	 */
	private SOAPBody mSoapBody;

	/**
	 * Set<String> mandatoryFields
	 */
	private Set<String> mMandatoryFields = new HashSet<String>();

	/**
	 * boolean testFailure
	 */
	private boolean mTestFailure;

	//------------------------------------------------------------------------------------------------------------------

	public ObjectXMLStringFormatter getFormatter() {
		return mFormatter;
	}

	public void setFormatter(ObjectXMLStringFormatter pFormatter) {
		mFormatter = pFormatter;
	}

	public Set<RepositoryItem> getRecordMappings() {
		return mRecordMappings;
	}

	public void setRecordMappings(Set<RepositoryItem> pRecordMappings) {
		mRecordMappings = pRecordMappings;
	}

	public SOAPBody getSoapBody() {
		return mSoapBody;
	}

	public void setSoapBody(SOAPBody pSoapBody) {
		mSoapBody = pSoapBody;
	}

	public Set<String> getMandatoryFields() {
		return mMandatoryFields;
	}

	public void setMandatoryFields(Set<String> pMandatoryFields) {
		mMandatoryFields = pMandatoryFields;
	}

	public boolean isTestFailure() {
		return mTestFailure;
	}

	public void setTestFailure(boolean pTestFailure) {
		mTestFailure = pTestFailure;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * analyze soap message and make necessary updates
	 *
	 * @param pMessage soap message
	 */
	public void readSOAPMessage(TransactionInfo pTransactionInfo,
								SOAPMessage pMessage,
								Operation pOperation,
								RepositoryItem pExportConfigItem,
								RepositoryItem[] pItemsForUpdate) throws SOAPException {
		synchronized (this) {
			setTransactionInfo(pTransactionInfo);
			setSoapBody(pMessage.getSOAPBody());

			setRecordMappings(RecordMappingUtil.getMappingsByType(pExportConfigItem, pOperation, false));
			if (getRecordMappings() == null || getRecordMappings().isEmpty()) {
				return;
			}

			setMandatoryFields((Set<String>)pExportConfigItem.getPropertyValue(PRP_EXPORT_MANDATORY_FIELDS));

			Type outputType = pOperation.getMessageOutput().getType();
			Type itemProcessType = getResponseTopType(pOperation.getMessageOutput());

			Node outputNode = null;
			if (outputType.equals(itemProcessType)) {
				Node childNode = DomUtil.getNodeByName(getSoapBody().getChildNodes(), outputType.getName());
				if (childNode != null) {
					outputNode = childNode.getParentNode();
				}
			} else {
				outputNode = DomUtil.getNodeByName(getSoapBody().getChildNodes(), outputType.getName());
			}

			if (outputNode == null) {
				trackError("Response is malformed. Unable to parse.");
				return;
			}
			processReadSOAPMessage(outputNode, pItemsForUpdate, itemProcessType);
		}
	}

	/**
	 * read soap message and initialize repository items
	 *
	 * @param pOutputNode      start output node
	 * @param pItemsForUpdate  array of items for update
	 * @param pItemProcessType item process type
	 */
	private void processReadSOAPMessage(Node pOutputNode, RepositoryItem[] pItemsForUpdate, Type pItemProcessType) {
		Set<Node> itemNodes = DomUtil.getNodesByName(pOutputNode.getChildNodes(), pItemProcessType.getName());
		Iterator<Node> itemNodesIter = itemNodes.iterator();
		try {
			for (RepositoryItem itemForUpdate : pItemsForUpdate) {
				if (!itemNodesIter.hasNext()) {
					return;
				}
				RepositoryItemInfo repositoryItemInfo = new RepositoryItemInfo(itemForUpdate,
						itemForUpdate.getItemDescriptor().getItemDescriptorName());
				SOAPElementInfo soapElementInfo = new SOAPElementInfo(
						(SOAPElement) itemNodesIter.next(),
						pItemProcessType // outputType
				);
				checkSOAPElementForHref(soapElementInfo);
				initRepositoryItem(repositoryItemInfo, soapElementInfo);
				Repository repository = repositoryItemInfo.getRepositoryItem().getRepository();
				if (!repositoryItemInfo.getRepositoryItem().isTransient()) {
					((MutableRepository) repository).updateItem(
							(MutableRepositoryItem) repositoryItemInfo.getRepositoryItem()
					);
				}

				// check for mandatory fields in response, if value is empty or absent - response is bad
				for (String propertyName : getMandatoryFields() ) {
					Object value = repositoryItemInfo.getRepositoryItem().getPropertyValue(propertyName);
					if (value == null || (value instanceof String && StringUtils.isBlank((String)value)) || isTestFailure()) {
						trackError("Bad response.");
						break;
					}
				}

			}
		} catch (Exception e) {
			trackError(e, "Unable to process response callback. Please check log.");
		}
	}

	/**
	 * checks for href and finds suitable
	 *
	 * @param pSOAPElementInfo SOAPElementInfo to check
	 */
	private void checkSOAPElementForHref(SOAPElementInfo pSOAPElementInfo) {
		String attributeHref = DomUtil.getAttributeValue(pSOAPElementInfo.getSOAPElement(), ATTRIBUTE_HREF);
		if (!StringUtils.isBlank(attributeHref)) {
			Set<Node> hrefNodes = DomUtil.getNodesByNameAndAttribute(
					getSoapBody().getChildNodes(),
					pSOAPElementInfo.getWsdlType().getTypeName(),
					ATTRIBUTE_ID,
					attributeHref);
			if (hrefNodes != null && !hrefNodes.isEmpty()) {
				pSOAPElementInfo.setSOAPElement((SOAPElement) hrefNodes.iterator().next());
			}
		}
	}


	/**
	 * @param pMessage output message
	 * @return repository item process type
	 */
	private Type getResponseTopType(Message pMessage) {
		Type messageType = pMessage.getType();
		if (messageType.getType() == NodeType.COMPLEX) {
			// we will check sub types, if there is only one and it is complex, so we guess,
			// that there is sub type for complex update
			List<Type> subTypes = messageType.getSubTypes();
			if (subTypes.size() == 1) {
				if (subTypes.get(0).getType().equals(NodeType.COMPLEX)) {
					return subTypes.get(0);
				}
			}
		}
		return messageType;
	}

	/**
	 * @param pSOAPElement  SOAPElement
	 * @param pLeftWsdlPath left wsdl path
	 * @return String value of wsdl path
	 */
	private String getValue(SOAPElement pSOAPElement, String pLeftWsdlPath) {
		String[] wsdlPathes = pLeftWsdlPath.split("\\" + DELIMETER);
		int index = 0;
		Node currentNode = pSOAPElement;
		for (String wsdlPath : wsdlPathes) {
			currentNode = DomUtil.getNodeByName(currentNode.getChildNodes(), wsdlPath);
			if (currentNode == null) {
				return null;
			}
		}
		return DomUtil.getNodeValue(currentNode);
	}

	/**
	 * retrieve child SOAPElementInfo by specified wsdl path
	 *
	 * @param pSOAPElementInfo parent SOAPElementInfo
	 * @param pWsdlPath        wsdl path
	 * @return child SOAPElementInfo
	 */
	private SOAPElementInfo getChildSOAPElementInfo(SOAPElementInfo pSOAPElementInfo, String pWsdlPath) {
		String leftWsdlPath = pWsdlPath.substring(pSOAPElementInfo.getWsdlType().getFullPath().length() + 1);
		String[] pathes = leftWsdlPath.split("\\" + DELIMETER);
		if (pathes.length == 1) {
			return pSOAPElementInfo;
		}
		SOAPElementInfo childSOAPElementInfo = new SOAPElementInfo(pSOAPElementInfo);
		for (int index = 0; index < pathes.length - 1; index++) {
			String path = pathes[index];
			Type subType = childSOAPElementInfo.getWsdlType().getSubType(path);
			SOAPElement subElement = (SOAPElement) DomUtil.getNodeByName(
					childSOAPElementInfo.getSOAPElement().getChildNodes(),
					path);
			if (subType == null || subElement == null) {
				return null;
			}
			childSOAPElementInfo.setSOAPElement(subElement);
			childSOAPElementInfo.setWsdlType(subType);
			checkSOAPElementForHref(childSOAPElementInfo);
		}
		return childSOAPElementInfo;
	}

	/**
	 * @param pRepositoryItemInfo repository item info used for item for update
	 * @param pSOAPElementInfo    soap element info used to get response value
	 */
	private void initRepositoryItem(RepositoryItemInfo pRepositoryItemInfo,
									SOAPElementInfo pSOAPElementInfo)
			throws RepositoryException, IntrospectionException {
		RepositoryItemDescriptor itemDescriptor = pRepositoryItemInfo.getRepositoryItem().getItemDescriptor();
		DynamicPropertyDescriptor[] propertyDescriptors = itemDescriptor.getPropertyDescriptors();
		for (DynamicPropertyDescriptor propertyDescriptor : propertyDescriptors) {
			PropertyValueInfo propertyValueInfo = new PropertyValueInfo(propertyDescriptor);
			String repositoryPath = pRepositoryItemInfo.getRepositoryPath() + DELIMETER + propertyDescriptor.getName();
			propertyValueInfo.analyzeRepositoryPathMappings(repositoryPath, getRecordMappings());

			if (!propertyValueInfo.isUnderMapping()) {
				continue;
			}

			switch (propertyValueInfo.getType()) {
				case SIMPLE:
					if (propertyValueInfo.isDirectMapped()) {
						processSOAPElementSimplePropery(
								pSOAPElementInfo,
								pRepositoryItemInfo,
								propertyValueInfo);
					}
					break;
				case REPOSITORY_ITEM:
					String wsdlPath = propertyValueInfo.getWsdlPathes().iterator().next();
					SOAPElementInfo soapElementInfo = getChildSOAPElementInfo(pSOAPElementInfo, wsdlPath);

					if (soapElementInfo != null) {
						processSOAPElementSubRelation(
								soapElementInfo,
								pRepositoryItemInfo,
								propertyValueInfo);
					} else {
						vlogDebug("Unable to find child SOAPElement info by path: {0}", wsdlPath);
					}
					break;
				case COLLECTION_OF_REPOSITORY_ITEMS:
					processSOAPElementSubCollection(pSOAPElementInfo, pRepositoryItemInfo, propertyValueInfo);
					break;
			}


		}
	}

	/**
	 * process sub collection relation element property read from response
	 *
	 * @param pSOAPElementInfo    begin soap element to analyze tree below
	 * @param pRepositoryItemInfo repository item info
	 * @param pPropertyValueInfo  property value info
	 */
	private void processSOAPElementSubCollection(SOAPElementInfo pSOAPElementInfo,
												 RepositoryItemInfo pRepositoryItemInfo,
												 PropertyValueInfo pPropertyValueInfo) throws IntrospectionException, RepositoryException {
		String collectionElementName = pSOAPElementInfo.getSOAPElement().getElementName().getLocalName();
		Set<Node> sameNodes = DomUtil.getNodesByName(
				pSOAPElementInfo.getSOAPElement().getParentElement().getChildNodes(),
				collectionElementName
		);

		Collection<RepositoryItem> subCollection = (Collection<RepositoryItem>) pRepositoryItemInfo.getRepositoryItem()
				.getPropertyValue(pPropertyValueInfo.getPropertyName());
		if (subCollection == null) {
			subCollection = createSubCollection(pRepositoryItemInfo, pPropertyValueInfo);
		}

		if (subCollection != null) {
			Iterator iterator = subCollection.iterator();
			for (Node sameNode : sameNodes) {
				RepositoryItem itemToInit = null;
				if (!iterator.hasNext()) {
					itemToInit = createItem(pRepositoryItemInfo, pPropertyValueInfo);
					subCollection.add(itemToInit);
				} else {
					itemToInit = (RepositoryItem) iterator.next();
				}
				RepositoryItemInfo repositoryItemInfo = new RepositoryItemInfo(
						itemToInit,
						pRepositoryItemInfo.getRepositoryItem(),
						pRepositoryItemInfo.getRepositoryPath() + DELIMETER + pPropertyValueInfo.getPropertyName()
				);
				SOAPElementInfo soapElementInfo = new SOAPElementInfo(pSOAPElementInfo);
				soapElementInfo.setSOAPElement((SOAPElement) sameNode);
				checkSOAPElementForHref(soapElementInfo);
				initRepositoryItem(repositoryItemInfo, soapElementInfo);
			}
		}
	}

	/**
	 * create sub item element by correct property descriptor
	 *
	 * @param pRepositoryItemInfo repository item info
	 * @param pPropertyValueInfo  property item info
	 * @return created sub item
	 */
	private RepositoryItem createItem(RepositoryItemInfo pRepositoryItemInfo, PropertyValueInfo pPropertyValueInfo) {
		String itemDescriptorName = null;

		try {
			switch (pPropertyValueInfo.getType()) {
				case REPOSITORY_ITEM:
					itemDescriptorName = ((GSAPropertyDescriptor) pPropertyValueInfo.getPropertyDescriptor())
							.getPropertyItemDescriptor().getItemDescriptorName();
					break;
				case COLLECTION_OF_REPOSITORY_ITEMS:
					itemDescriptorName = ((GSAItemDescriptor) pPropertyValueInfo.getPropertyDescriptor()
							.getComponentPropertyBeanInfo()).getItemDescriptorName();
					break;
			}
		} catch (IntrospectionException e) {
			trackError("Unable to init sub item: " + e.toString());
			return null;
		}

		Repository repository = pRepositoryItemInfo.getRepositoryItem().getRepository();
		try {
			RepositoryItem item = ((MutableRepository) repository).createItem(itemDescriptorName);
			initItemWithDefaultValues(item);
			((MutableRepository) repository).addItem((MutableRepositoryItem) item);
			return item;
		} catch (Exception e) {
			trackError("Unable to create sub item: " + e.toString());
			return null;
		}
	}

	/**
	 * init item's properties with default values
	 *
	 * @param pRepositoryItem repository item
	 * @throws RepositoryException    if error occures
	 * @throws IllegalAccessException if error occures
	 * @throws InstantiationException if error occures
	 */
	private void initItemWithDefaultValues(RepositoryItem pRepositoryItem)
			throws RepositoryException, IllegalAccessException, InstantiationException {
		for (DynamicPropertyDescriptor descriptor : pRepositoryItem.getItemDescriptor().getPropertyDescriptors()) {
			if (descriptor.isRequired() && pRepositoryItem.getPropertyValue(descriptor.getName()) == null) {
				Class propertyType = descriptor.getPropertyType();
				Object propertyValue;
				if (String.class.isAssignableFrom(propertyType)) {
					propertyValue = pRepositoryItem.getRepositoryId() + " default";
				} else {
					propertyValue = propertyType.newInstance(); // ?
				}
				((MutableRepositoryItem) pRepositoryItem).setPropertyValue(
						descriptor.getName(),
						propertyValue
				);
			}
		}
	}

	/**
	 * create sub collection
	 *
	 * @param pRepositoryItemInfo repository item info
	 * @param pPropertyValueInfo  property value info
	 * @return sub collection
	 */
	private Collection<RepositoryItem> createSubCollection(RepositoryItemInfo pRepositoryItemInfo,
														   PropertyValueInfo pPropertyValueInfo) {
		Class collectionClass = pPropertyValueInfo.getPropertyDescriptor().getPropertyType();
		Collection subChildren = null;
		try {
			subChildren = (Collection) collectionClass.newInstance();
		} catch (Exception e) {
			// unable to instantiate class object
			return null;
		}
		((MutableRepositoryItem) pRepositoryItemInfo.getRepositoryItem()).setPropertyValue(
				pPropertyValueInfo.getPropertyName(), subChildren
		);
		return subChildren;
	}

	/**
	 * process sub relation element property read from response
	 *
	 * @param pSOAPElementInfo    begin soap element to analyze tree below
	 * @param pRepositoryItemInfo repository item info
	 * @param pPropertyValueInfo  property value info
	 */
	private void processSOAPElementSubRelation(SOAPElementInfo pSOAPElementInfo,
											   RepositoryItemInfo pRepositoryItemInfo,
											   PropertyValueInfo pPropertyValueInfo)
			throws IntrospectionException, RepositoryException {
		// check if repository item contains susch sub relation

		RepositoryItem subRelation = (RepositoryItem) pRepositoryItemInfo.getRepositoryItem().getPropertyValue(
				pPropertyValueInfo.getPropertyName()
		);

		if (subRelation == null) {
			subRelation = createItem(pRepositoryItemInfo, pPropertyValueInfo);
			((MutableRepositoryItem) pRepositoryItemInfo.getRepositoryItem()).setPropertyValue(
					pPropertyValueInfo.getPropertyName(), subRelation
			);
		}

		if (subRelation != null) {
			pRepositoryItemInfo = new RepositoryItemInfo(
					subRelation,
					pRepositoryItemInfo.getRepositoryItem(),
					pRepositoryItemInfo.getRepositoryPath() + DELIMETER + pPropertyValueInfo.getPropertyName());
			initRepositoryItem(pRepositoryItemInfo, pSOAPElementInfo);
		}
	}


	/**
	 * process simple property read from response
	 *
	 * @param pSOAPElementInfo    begin soap element to analyze tree below
	 * @param pRepositoryItemInfo repository item info
	 * @param pPropertyValueInfo  property value info
	 */
	private void processSOAPElementSimplePropery(SOAPElementInfo pSOAPElementInfo,
												 RepositoryItemInfo pRepositoryItemInfo,
												 PropertyValueInfo pPropertyValueInfo) {
		for (String wsdlPath : pPropertyValueInfo.getWsdlPathes()) {
			if (wsdlPath.startsWith(pSOAPElementInfo.getWsdlType().getFullPath())) {
				String leftWsdlPath = wsdlPath.substring(pSOAPElementInfo.getWsdlType().getFullPath().length() + 1);
				String wsdlPathValue = getValue(pSOAPElementInfo.getSOAPElement(), leftWsdlPath);
				String propName = pPropertyValueInfo.getPropertyName();
				((MutableRepositoryItem) pRepositoryItemInfo.getRepositoryItem()).setPropertyValue(
						propName,
						getFormatter().formatToObject(
								wsdlPathValue,
								pPropertyValueInfo.getPropertyDescriptor().getPropertyType()
						)
				);
				break; // we analyze only one mapping for property
			}
		}
	}

	/**
	 * init repository item with value from
	 * @param pItemForUpdate repository item for update
	 * @param pProperyName property name
	 * @param pNodeValue node value
	 */
/*
	private void initRepositoryItem(RepositoryItem pItemForUpdate, String pProperyName, String pNodeValue) {
		RepositoryItemDescriptor itemDescriptor = null;
		try {
			itemDescriptor = pItemForUpdate.getItemDescriptor();
			DynamicPropertyDescriptor propDescriptpr = itemDescriptor.getPropertyDescriptor(pProperyName);
			Class propertyType = propDescriptpr.getPropertyType();
			Object formattedValue = getFormatter().formatToObject(pNodeValue, propertyType);
			((MutableRepositoryItem)pItemForUpdate).setPropertyValue(pProperyName, formattedValue);
		} catch (RepositoryException e) {
			String msg = "Unable to set property {"+pProperyName+"}";
			if(itemDescriptor!=null) {
				msg += " for item {"+itemDescriptor.getItemDescriptorName()+":"+pItemForUpdate.getRepositoryId()+"}";
			} else {
				msg += " for item {"+pItemForUpdate.getRepositoryId()+"}";
			}
			msg +=" with value {"+pNodeValue+"}";
			trackError(e, msg);
		}
	}
*/

	/**
	 * @param pWsdlPath wsdl path
	 * @param pRecordMappings record mappings
	 * @return repository path
	 */
/*
	private String getRepositoryPath(String pWsdlPath, Set<RepositoryItem> pRecordMappings) {
		for(RepositoryItem recordMapping : pRecordMappings) {
			String recordMappingWsdlPath = (String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
			if(pWsdlPath.equals( recordMappingWsdlPath )) {
				return (String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH);
			}
		}
		return null;
	}
*/
}
