package vsg.integration.export.service;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import vsg.integration.export.ExportConfig;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.handler.BaseFormHandler;
import vsg.integration.export.service.validator.OutputReportingValidator;

import java.util.Map;

/**
 * The Class OutputReportingService.
 *
 * @author "Vladimir Tretyakevich"
 */
public class OutputReportingService extends GenericService implements IRepositoryConstants {

	/**
	 * The export repository.
	 */
	private MutableRepository mExportRepository;

	/**
	 * The reporting validator.
	 */
	private OutputReportingValidator mValidator;
	//------------------------------------------------------------------------------------------------------------------

	public MutableRepository getExportRepository() {
		return mExportRepository;
	}

	public void setExportRepository(MutableRepository pExportRepository) {
		mExportRepository = pExportRepository;
	}

	public OutputReportingValidator getValidator() {
		return mValidator;
	}

	public void setValidator(OutputReportingValidator pValidator) {
		mValidator = pValidator;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Checks if is valid input.
	 *
	 * @param pValue the values map
	 * @return true, if is valid input
	 */
	public boolean isValidInput(Map<String, String> pValue, BaseFormHandler pForm) {
		boolean result = false;
		result = getValidator().validateOptions(pValue, pForm);
		if (result && isUseEmail(pValue)) {
			result = getValidator().validateEmail(pValue, pForm);
		}
		if (result && isUseFTP(pValue)) {
			result = getValidator().validateFtp(pValue, pForm);
		}
		return result;
	}

	/**
	 * Checks if is use ftp.
	 *
	 * @param pValue the value
	 * @return true, if is use ftp
	 */
	private boolean isUseFTP(Map<String, String> pValue) {
		return pValue.get(USE_FTP) != null && pValue.get(USE_FTP).equals("true");
	}

	/**
	 * Checks if is use email.
	 *
	 * @param pValue the value
	 * @return true, if is use email
	 */
	private boolean isUseEmail(Map<String, String> pValue) {
		return pValue.get(USE_EMAIL) != null && pValue.get(USE_EMAIL).equals("true");
	}

	/**
	 * Creates the output report config item.
	 *
	 * @param pExportConfig the export config
	 * @param pValue        the value
	 * @throws RepositoryException the repository exception
	 */
	public void createOutputReportConfigItem(ExportConfig pExportConfig, Map<String, String> pValue) throws RepositoryException {
		MutableRepository mr = getExportRepository();
		MutableRepositoryItem exportConfig = findRepositoryItem(mr, pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG);
		MutableRepositoryItem reportConfig = null;
		if (exportConfig != null) {
			reportConfig = (MutableRepositoryItem) exportConfig.getPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG);
			if (reportConfig == null) {
//				reportConfig = findRepositoryItem(mr, reportConfigId, ID_OUT_REPORT_CONFIG);
				reportConfig = createRepositoryItem(mr, ID_OUT_REPORT_CONFIG);
			} else {
				vlogDebug("item exists:{0}", reportConfig.getRepositoryId());
			}
		} else {
			vlogError("exportconfig item repository id is null");
		}

		if (reportConfig != null) {
			updateReportConfigProperties(reportConfig, pValue);
			updateItem(mr, reportConfig);
			exportConfig.setPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG, reportConfig);
			updateItem(mr, exportConfig);
		}
	}

	/**
	 * Update report config item properties.
	 *
	 * @param reportConfig the report config
	 * @param pValue       the value
	 */
	private void updateReportConfigProperties(MutableRepositoryItem reportConfig, Map<String, String> pValue) {
		reportConfig.setPropertyValue(PRP_REP_CONF_SFREQ, getIntegerValue(pValue.get(PRP_REP_CONF_SFREQ)));
		reportConfig.setPropertyValue(PRP_REP_CONF_EFREQ, getIntegerValue(pValue.get(PRP_REP_CONF_EFREQ)));
		reportConfig.setPropertyValue(PRP_REP_CONF_FILE_NAME, pValue.get(PRP_REP_CONF_FILE_NAME));
		reportConfig.setPropertyValue(PRP_REP_CONF_TIME_FORMAT, pValue.get(PRP_REP_CONF_TIME_FORMAT));
		reportConfig.setPropertyValue(PRP_REP_CONF_STORE_PATH, pValue.get(PRP_REP_CONF_STORE_PATH));
		reportConfig.setPropertyValue(PRP_REP_CONF_ARCHIVE_PATH, pValue.get(PRP_REP_CONF_ARCHIVE_PATH));
		reportConfig.setPropertyValue(PRP_REP_CONF_FORMAT, getIntegerValue(pValue.get(PRP_REP_CONF_FORMAT)));

		reportConfig.setPropertyValue(PRP_REP_CONF_ENABLE_SUCCESS, getBooleanValue(pValue.get(PRP_REP_CONF_ENABLE_SUCCESS)));
		reportConfig.setPropertyValue(PRP_REP_CONF_ENABLE_ERROR, getBooleanValue(pValue.get(PRP_REP_CONF_ENABLE_ERROR)));

		MutableRepository mr = getExportRepository();
		updateEmailConfig(pValue, reportConfig, mr);
		updateFtpConfig(pValue, reportConfig, mr);
	}

	/**
	 * Update ftp config.
	 *
	 * @param pValue        the value
	 * @param pReportConfig the report config
	 * @param pRepository   the repository
	 */
	private void updateFtpConfig(Map<String, String> pValue, MutableRepositoryItem pReportConfig, MutableRepository pRepository) {
		boolean useFtp = isUseFTP(pValue);
		if (!useFtp) {
			MutableRepositoryItem ftpDel = (MutableRepositoryItem) (pReportConfig.getPropertyValue(PRP_REP_CONF_FTP_CONF));
			if (ftpDel != null) {
				removeRepositoryItemProperty(pRepository, pReportConfig, PRP_REP_CONF_FTP_CONF, ID_FTP_DELIVERY_CONFIG, ftpDel.getRepositoryId());
			}
		} else {
			MutableRepositoryItem ftpDel = (MutableRepositoryItem) (pReportConfig.getPropertyValue(PRP_REP_CONF_FTP_CONF));
			if (ftpDel == null) {
				ftpDel = createRepositoryItem(pRepository, ID_FTP_DELIVERY_CONFIG);
			}
			ftpDel.setPropertyValue(PRP_FTP_CONF_PORT, pValue.get(PRP_FTP_CONF_PORT));
			ftpDel.setPropertyValue(PRP_FTP_CONF_PROTOCOL, getIntegerValue(pValue.get(PRP_FTP_CONF_PROTOCOL)));
			ftpDel.setPropertyValue(PRP_FTP_CONF_HOST_NAME, pValue.get(PRP_FTP_CONF_HOST_NAME));
			ftpDel.setPropertyValue(PRP_FTP_CONF_OUT_DIR_PATH, pValue.get(PRP_FTP_CONF_OUT_DIR_PATH));
			ftpDel.setPropertyValue(PRP_FTP_CONF_USER_NAME, pValue.get(PRP_FTP_CONF_USER_NAME));
			ftpDel.setPropertyValue(PRP_FTP_CONF_PASS, pValue.get(PRP_FTP_CONF_PASS));
			ftpDel.setPropertyValue(PRP_FTP_CONF_ENC_KEY_PATH, pValue.get(PRP_FTP_CONF_ENC_KEY_PATH));
			ftpDel.setPropertyValue(PRP_FTP_CONF_ENC_PHRASE, pValue.get(PRP_FTP_CONF_ENC_PHRASE));
			updateItem(pRepository, ftpDel);
			pReportConfig.setPropertyValue(PRP_REP_CONF_FTP_CONF, ftpDel);
		}
	}

	/**
	 * Update email config.
	 *
	 * @param pValue         the value
	 * @param pRreportConfig the report config
	 * @param pRepository    the repository
	 */
	private void updateEmailConfig(Map<String, String> pValue, MutableRepositoryItem pRreportConfig, MutableRepository pRepository) {
		boolean useEmail = isUseEmail(pValue);
		if (!useEmail) {
			MutableRepositoryItem mailDel = (MutableRepositoryItem) (pRreportConfig.getPropertyValue(PRP_REP_CONF_MAIL_CONF));
			if (mailDel != null) {
				removeRepositoryItemProperty(pRepository, pRreportConfig, PRP_REP_CONF_MAIL_CONF, ID_EMAIL_DELIVERY_CONFIG, mailDel.getRepositoryId());
			}
		} else {
			MutableRepositoryItem mailDel = (MutableRepositoryItem) (pRreportConfig.getPropertyValue(PRP_REP_CONF_MAIL_CONF));
			if (mailDel == null) {
				mailDel = createRepositoryItem(pRepository, ID_EMAIL_DELIVERY_CONFIG);
			}
			mailDel.setPropertyValue(PRP_EMAIL_CONF_SUBJ, pValue.get(PRP_EMAIL_CONF_SUBJ));
			mailDel.setPropertyValue(PRP_EMAIL_CONF_SENDER, pValue.get(PRP_EMAIL_CONF_SENDER));
			mailDel.setPropertyValue(PRP_EMAIL_CONF_RECIPIENTS, pValue.get(PRP_EMAIL_CONF_RECIPIENTS));
			updateItem(pRepository, mailDel);
			pRreportConfig.setPropertyValue(PRP_REP_CONF_MAIL_CONF, mailDel);
		}
	}

	/**
	 * Creates the repository item.
	 *
	 * @param pRepository     the repository
	 * @param pItemDescriptor the item descriptor
	 * @return the mutable repository item
	 */
	protected MutableRepositoryItem createRepositoryItem(MutableRepository pRepository, String pItemDescriptor) {
		MutableRepositoryItem result = null;
		try {
			result = pRepository.createItem(pItemDescriptor);
			pRepository.addItem(result);
		} catch (RepositoryException e) {
			vlogError("Exception while creating item {0}: {1}", pItemDescriptor, e);
		}
		return result;
	}

	/**
	 * Removes the repository item property.
	 *
	 * @param pRepository           the repository
	 * @param pRemoveFromItem       the remove from item
	 * @param pRemovePropName       the remove prop name
	 * @param pRemoveItemDescriptor the remove item descriptor
	 * @param pRemoveItemId         the remove item id
	 */
	protected void removeRepositoryItemProperty(MutableRepository pRepository, MutableRepositoryItem pRemoveFromItem,
												String pRemovePropName, String pRemoveItemDescriptor, String pRemoveItemId) {
		pRemoveFromItem.setPropertyValue(pRemovePropName, null);
		updateItem(pRepository, pRemoveFromItem);
		try {
			pRepository.removeItem(pRemoveItemId, pRemoveItemDescriptor);
		} catch (RepositoryException e) {
			vlogError("Error while removing item {0}: {1}", pRemoveItemDescriptor, e);
		}
	}

	/**
	 * Update item.
	 *
	 * @param pRepository the repository
	 * @param pItem       the item
	 */
	protected void updateItem(MutableRepository pRepository, MutableRepositoryItem pItem) {
		try {
			pRepository.updateItem(pItem);
		} catch (RepositoryException e) {
			vlogError("Exception while updating of type {0}: {1}", pItem.getItemDisplayName(), e);
		}
	}

	/**
	 * Gets the integer value.
	 *
	 * @param pStringValue the string value
	 * @return the integer value
	 */
	protected Integer getIntegerValue(String pStringValue) {
		Integer result = null;
		if (!StringUtils.isBlank(pStringValue)) {
			try {
				result = Integer.parseInt(pStringValue);
			} catch (NumberFormatException e) {
				vlogError("invalid integer value: {0}. Exception: {1}", pStringValue, e);
			}
		}
		return result;
	}

	/**
	 * Gets the boolean value.
	 *
	 * @param pStringValue the string value
	 * @return the boolean value
	 */
	protected Boolean getBooleanValue(String pStringValue) {
		Boolean result = false;
		if (!StringUtils.isBlank(pStringValue)) {
			result = Boolean.valueOf(pStringValue);
		}
		return result;
	}

	/**
	 * Finds repository item.
	 *
	 * @param pRepository     the repository
	 * @param pRepositoryId   the repository id
	 * @param pItemDescriptor the item descriptor
	 * @return the mutable repository item
	 * @throws RepositoryException the repository exception
	 */
	protected MutableRepositoryItem findRepositoryItem(MutableRepository pRepository, String pRepositoryId, String pItemDescriptor) throws RepositoryException {
		MutableRepositoryItem rItem = null;
		if (pRepository != null && !StringUtils.isBlank(pRepositoryId) && !StringUtils.isBlank(pItemDescriptor)) {
			rItem = pRepository.getItemForUpdate(pRepositoryId, pItemDescriptor);
		} else {
			vlogError("check parameters");
		}
		return rItem;
	}

	/**
	 * Checks if could create items.
	 *
	 * @return true, if allowed
	 */
	private boolean isCreateItems() {
		return false;
	}

}