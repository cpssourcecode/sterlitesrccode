package vsg.integration.export.service;

import atg.nucleus.GenericService;
import vsg.integration.export.util.bean.TransactionInfo;

/**
 * @author Dmitry Golubev
 */
public class ErrorTracker extends GenericService {
	/**
	 * transaction info
	 */
	private TransactionInfo mTransactionInfo;

	public TransactionInfo getTransactionInfo() {
		return mTransactionInfo;
	}

	public void setTransactionInfo(TransactionInfo pTransactionInfo) {
		mTransactionInfo = pTransactionInfo;
	}

	/**
	 * log error message and throw exception
	 *
	 * @param pErrorMsg error message
	 */
	public void trackError(String pErrorMsg) {
		trackError(null, pErrorMsg);
	}

	/**
	 * log error message and throw exception
	 *
	 * @param e         excepion
	 * @param pErrorMsg error message
	 */
	public void trackError(Exception e, String pErrorMsg) {
		if (getTransactionInfo() != null) {
			getTransactionInfo().setStatus(TransactionInfo.FAIL);
			if (e != null) {
				getTransactionInfo().setFailReason(pErrorMsg + " - " + e.toString());
			} else {
				getTransactionInfo().setFailReason(pErrorMsg);
			}
		}
		if (e != null) {
			vlogError(e, pErrorMsg);
		} else {
			vlogError(pErrorMsg);
		}
	}
}
