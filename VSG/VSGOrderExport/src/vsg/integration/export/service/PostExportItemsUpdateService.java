package vsg.integration.export.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.integration.export.util.TransactionProcessor;
import vsg.integration.export.util.TransactionProcessorCommand;

/**
 * @author Dmitry Golubev
 */
public class PostExportItemsUpdateService extends GenericService {

	/**
	 * repositories item descriptors for rql execute
	 */
	private Map<String, Repository> mSupportedItemDescriptors = new HashMap<String, Repository>();

	/**
	 * transaction manager
	 */
	private TransactionProcessor mTransactionProcessor;

	//------------------------------------------------------------------------------------------------------------------

	public TransactionProcessor getTransactionProcessor() {
		return mTransactionProcessor;
	}

	public void setTransactionProcessor(TransactionProcessor pTransactionProcessor) {
		mTransactionProcessor = pTransactionProcessor;
	}

	public Map<String, Repository> getSupportedItemDescriptors() {
		return mSupportedItemDescriptors;
	}

	public void setSupportedItemDescriptors(Map<String, String> pSupportedItemDescriptors) {
		mSupportedItemDescriptors.clear();
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		for (String itemDescriptor : pSupportedItemDescriptors.keySet()) {
			String repositoryName = pSupportedItemDescriptors.get(itemDescriptor);
			Repository repository = (Repository) nucleus.resolveName(repositoryName);
			mSupportedItemDescriptors.put(itemDescriptor, repository);
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	public void updateExportedItems(final List<String> pItemIds, final String pItemDescriptor) {
		getTransactionProcessor().process(
				new TransactionProcessorCommand() {
					@Override
					public void processInTransaction() {
						if (pItemIds != null) {
							MutableRepository repository = (MutableRepository) getSupportedItemDescriptors().get(pItemDescriptor);
							for (String itemId : pItemIds) {
								try {
									MutableRepositoryItem item = repository.getItemForUpdate(itemId, pItemDescriptor);
									item.setPropertyValue("isExported", true);
									item.setPropertyValue("jdeSuccess", true);
									repository.updateItem(item);
								} catch (RepositoryException e) {
									vlogError("{0}:{1} - failed to update isExported flag", pItemDescriptor, itemId);
								}
							}
						}
					}

					@Override
					public void processError() {
						vlogError("unable to mark exported items");
					}

					@Override
					public Object getSyncObject() {
						return PostExportItemsUpdateService.this;
					}
				}
		);

	}

	/**
	 * @param pItemDescriptor item descriptor name to check
	 * @return true if item descriptor is supported for mark as exported
	 */
	private boolean isItemDescriptorSupported(String pItemDescriptor) {
		return getSupportedItemDescriptors().containsKey(pItemDescriptor);
	}
	
	/**
	 * 
	 * @param pItemsForExport
	 * @param pItemDescriptor
	 */
	public void updateExportedItems(RepositoryItem[] pItemsForExport, String pItemDescriptor) {
		if (pItemDescriptor.equals("order")) {
			vlogDebug("Item Descriptor {0}", pItemDescriptor);
			try {
				MutableRepository orderRepository = (MutableRepository) getSupportedItemDescriptors().get(pItemDescriptor);
				MutableRepositoryItem orderItem = orderRepository.getItemForUpdate(pItemsForExport[0].getRepositoryId(),pItemDescriptor);
				vlogDebug("Repository Item : {0}", orderItem);
				orderItem.setPropertyValue("jdeSuccess", false);
				orderRepository.updateItem(orderItem);
			} catch (RepositoryException e) {
				vlogError("{0}:{1} - failed to update jdeSuccess flag", pItemDescriptor, pItemsForExport[0].getRepositoryId());
				e.printStackTrace();
			}
		}
	}

}
