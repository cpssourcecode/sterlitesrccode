package vsg.integration.export.service.email;

import atg.nucleus.GenericService;

/**
 * The Class EmailConfig.
 *
 * @author "Vladimir Tretyakevich"
 */
public class EmailConfig extends GenericService {

	/**
	 * The send cc.
	 */
	private String[] mSendCc;

	/**
	 * The email body.
	 */
	private String mEmailBody;

	/**
	 * The body is html.
	 */
	private boolean mBodyIsHtml;

	/**
	 * Gets the email body.
	 *
	 * @return the email body
	 */
	public String getEmailBody() {
		return mEmailBody;
	}

	/**
	 * Sets the email body.
	 *
	 * @param pEmailBody the new email body
	 */
	public void setEmailBody(String pEmailBody) {
		mEmailBody = pEmailBody;
	}

	/**
	 * Checks if is body is html.
	 *
	 * @return true, if is body is html
	 */
	public boolean isBodyIsHtml() {
		return mBodyIsHtml;
	}

	/**
	 * Sets the body is html.
	 *
	 * @param pBodyIsHtml the new body is html
	 */
	public void setBodyIsHtml(boolean pBodyIsHtml) {
		mBodyIsHtml = pBodyIsHtml;
	}

	/**
	 * Gets the send cc.
	 *
	 * @return the send cc
	 */
	public String[] getSendCc() {
		return mSendCc;
	}

	/**
	 * Sets the send cc.
	 *
	 * @param pSendCc the new send cc
	 */
	public void setSendCc(String[] pSendCc) {
		mSendCc = pSendCc;
	}
}