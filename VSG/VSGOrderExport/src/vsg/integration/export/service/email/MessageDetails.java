package vsg.integration.export.service.email;

import java.io.File;


/**
 * The Class MessageDetails.
 *
 * @author "Vladimir Tretyakevich"
 */
public class MessageDetails {

	/**
	 * Instantiates a new message details.
	 */
	public MessageDetails() {
	}

	/**
	 * Instantiates a new message details.
	 *
	 * @param pEmailConfig the email config
	 */
	public MessageDetails(EmailConfig pEmailConfig) {
		setSendCc(pEmailConfig.getSendCc());
		setEmailBody(pEmailConfig.getEmailBody());
		setBodyIsHtml(pEmailConfig.isBodyIsHtml());
	}

	/**
	 * The send to.
	 */
	private String[] mSendTo;

	/**
	 * The send cc.
	 */
	private String[] mSendCc;

	/**
	 * The send from.
	 */
	private String mSendFrom;

	/**
	 * The subject.
	 */
	private String mSubject;

	/**
	 * The email body.
	 */
	private String mEmailBody;

	/**
	 * The body is html.
	 */
	private boolean mBodyIsHtml;

	private File mAttachment;

	/**
	 * Gets the send to.
	 *
	 * @return the send to
	 */
	public String[] getSendTo() {
		return mSendTo;
	}

	/**
	 * Sets the send to.
	 *
	 * @param pSendTo the new send to
	 */
	public void setSendTo(String[] pSendTo) {
		mSendTo = pSendTo;
	}

	/**
	 * Gets the send from.
	 *
	 * @return the send from
	 */
	public String getSendFrom() {
		return mSendFrom;
	}

	/**
	 * Sets the send from.
	 *
	 * @param pSendFrom the new send from
	 */
	public void setSendFrom(String pSendFrom) {
		mSendFrom = pSendFrom;
	}

	/**
	 * Gets the subject.
	 *
	 * @return the subject
	 */
	public String getSubject() {
		return mSubject;
	}

	/**
	 * Sets the subject.
	 *
	 * @param pSubject the new subject
	 */
	public void setSubject(String pSubject) {
		mSubject = pSubject;
	}

	/**
	 * Gets the email body.
	 *
	 * @return the email body
	 */
	public String getEmailBody() {
		return mEmailBody;
	}

	/**
	 * Sets the email body.
	 *
	 * @param pEmailBody the new email body
	 */
	public void setEmailBody(String pEmailBody) {
		mEmailBody = pEmailBody;
	}

	/**
	 * Checks if is body is html.
	 *
	 * @return true, if is body is html
	 */
	public boolean isBodyIsHtml() {
		return mBodyIsHtml;
	}

	/**
	 * Sets the body is html.
	 *
	 * @param pBodyIsHtml the new body is html
	 */
	public void setBodyIsHtml(boolean pBodyIsHtml) {
		mBodyIsHtml = pBodyIsHtml;
	}

	/**
	 * Gets the send cc.
	 *
	 * @return the send cc
	 */
	public String[] getSendCc() {
		return mSendCc;
	}

	/**
	 * Sets the send cc.
	 *
	 * @param pSendCc the new send cc
	 */
	public void setSendCc(String[] pSendCc) {
		mSendCc = pSendCc;
	}

	public File getAttachment() {
		return mAttachment;
	}

	public void setAttachment(File pAttachment) {
		mAttachment = pAttachment;
	}
}