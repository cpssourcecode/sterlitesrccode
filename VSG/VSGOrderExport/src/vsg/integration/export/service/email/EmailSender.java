package vsg.integration.export.service.email;

import atg.nucleus.GenericService;
import atg.service.email.EmailException;
import atg.service.email.MimeMessageUtils;
import atg.service.email.SMTPEmailSender;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Properties;

/**
 * The Class EmailSender.
 *
 * @author "Vladimir Tretyakevich"
 */
public class EmailSender extends GenericService {
/*
	*/
/**
	 * The host.
	 *//*

	private String mHost;

	*/
/**
	 * The email server.
	 *//*

	private String mEmailServer;
*/

	/**
	 * email sender
	 */
	private SMTPEmailSender mSender;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Send email.
	 *
	 * @param pMessageDetails the message details
	 * @return true, if successful
	 */
	public boolean sendEmail(MessageDetails pMessageDetails) {
		synchronized (this) {
			Properties properties = new Properties();
/*
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.host", getSender().getEmailHandlerHostName());
			properties.put("mail.smtp.port", getSender().getEmailHandlerPort());

			Session session = Session.getInstance(properties,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(
									getSender().getDefaultFrom(),
									getSender().getPassword()
							);
						}
					});
*/

			boolean isSend = false;
			try {
				MimeMessage message = (MimeMessage) MimeMessageUtils.createMessage();
				initMessageHeader(pMessageDetails, message);
				initMessageBody(pMessageDetails, message);
				getSender().sendEmailMessage(message);
				isSend = true;
			} catch (MessagingException | EmailException mExc) {
				vlogError(mExc, "Unable to send email");
			}
			return isSend;

/*
			boolean result = false;
			if (pMessageDetails == null) {
				return result;
			}
			final String from = pMessageDetails.getSendFrom();
			Properties properties = new Properties();
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.host", "smtp.gmail.com");
			properties.put("mail.smtp.port", "587");

			*/
/*Session session = Session.getDefaultInstance(properties);*//*

			Session session = Session.getInstance(properties,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(from, "zxASqw!2");
						}
					});

			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(from));
				initMessageHeader(pMessageDetails, message);
				initMessageBody(pMessageDetails, message);
				Transport.send(message);
				vlogDebug("Email sent successfully");
				result = true;
			} catch (MessagingException e) {
				vlogError(e, "Message was not send");
			}
			return result;
*/
		}
	}

	/**
	 * Inits the message header.
	 *
	 * @param pMessageDetails the message details
	 * @param pMessage        the message
	 * @throws MessagingException the messaging exception
	 */
	private void initMessageHeader(MessageDetails pMessageDetails, MimeMessage pMessage) throws MessagingException {

		if (pMessageDetails.getSendFrom() != null) {
			//pMessage.setSender(new InternetAddress(pMessageDetails.getSendFrom()));
			pMessage.setFrom(new InternetAddress(pMessageDetails.getSendFrom()));
		}
		if (pMessageDetails.getSendTo() != null) {
			for (String toAddress : pMessageDetails.getSendTo()) {
				pMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
			}
		}
		if (pMessageDetails.getSendCc() != null) {
			for (String ccAddress : pMessageDetails.getSendCc()) {
				pMessage.addRecipient(Message.RecipientType.CC, new InternetAddress(ccAddress));
			}
		}
		pMessage.setSubject(pMessageDetails.getSubject());

	}

	/**
	 * Inits the message body.
	 *
	 * @param pMessageDetails the message details
	 * @param message         the message
	 * @throws MessagingException the messaging exception
	 */
	private void initMessageBody(MessageDetails pMessageDetails, MimeMessage message) throws MessagingException {
		BodyPart messageBodyPart = new MimeBodyPart();
		if (pMessageDetails.isBodyIsHtml()) {
			messageBodyPart.setContent(pMessageDetails.getEmailBody(), "text/html");
		} else {
			messageBodyPart.setText(pMessageDetails.getEmailBody());
		}
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		File attachement = pMessageDetails.getAttachment();
		if (attachement != null) {
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(attachement);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(attachement.getName());
			multipart.addBodyPart(messageBodyPart);
		}
		message.setContent(multipart);
	}

	//------------------------------------------------------------------------------------------------------------------


/*
	public String getHost() {
		return mHost;
	}

	public void setHost(String pHost) {
		mHost = pHost;
	}

	public String getEmailServer() {
		return mEmailServer;
	}

	public void setEmailServer(String pEmailServer) {
		mEmailServer = pEmailServer;
	}
*/

	public SMTPEmailSender getSender() {
		return mSender;
	}

	public void setSender(SMTPEmailSender pSender) {
		mSender = pSender;
	}
}