package vsg.integration.export.service.email;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;

import java.io.File;

/**
 * The Class EmailSenderService.
 *
 * @author "Vladimir Tretyakevich"
 */
public class EmailSenderService extends GenericService implements IRepositoryConstants {

	/**
	 * The m export repository.
	 */
	private MutableRepository mExportRepository;

	/**
	 * The m email sender.
	 */
	private EmailSender mEmailSender;

	/**
	 * The m test email config.
	 */
	private EmailConfig mTestEmailConfig;

	/**
	 * The m success email config.
	 */
	private EmailConfig mSuccessEmailConfig;

	/**
	 * The m error email config.
	 */
	private EmailConfig mErrorEmailConfig;

	/**
	 * Send test email.
	 *
	 * @param pExportConfigItem the export config item
	 * @param pAttachment       the attachment
	 * @return true, if successful
	 */
	public boolean sendTestEmail(RepositoryItem pExportConfigItem, File pAttachment) {
		return sendEmail(pExportConfigItem, pAttachment, getTestEmailConfig());
	}

	/**
	 * Send success email.
	 *
	 * @param pExportConfigItem the export config item
	 * @param pAttachment       the attachment
	 * @return true, if successful
	 */
	public boolean sendSuccessEmail(RepositoryItem pExportConfigItem, File pAttachment) {
		return sendEmail(pExportConfigItem, pAttachment, getSuccessEmailConfig());
	}

	/**
	 * Send error email.
	 *
	 * @param pExportConfigItem the export config item
	 * @param pAttachment       the attachment
	 * @return true, if successful
	 */
	public boolean sendErrorEmail(RepositoryItem pExportConfigItem, File pAttachment) {
		return sendEmail(pExportConfigItem, pAttachment, getErrorEmailConfig());
	}

	/**
	 * Send test email.
	 *
	 * @param pExportItemRepositoryId the export item repository id
	 * @param pAttachment             the attachment
	 * @return true, if successful
	 */
	public boolean sendTestEmail(String pExportItemRepositoryId, File pAttachment) {
		RepositoryItem exportItem = getExportRepositoryItem(pExportItemRepositoryId);
		return sendEmailWDeliveryConfig(getEmailDeliveryConfigItem(exportItem), pAttachment, getTestEmailConfig());
	}

	/**
	 * Send success email.
	 *
	 * @param pExportItemRepositoryId the export item repository id
	 * @param pAttachment             the attachment
	 * @return true, if successful
	 */
	public boolean sendSuccessEmail(String pExportItemRepositoryId, File pAttachment) {
		RepositoryItem exportItem = getExportRepositoryItem(pExportItemRepositoryId);
		return sendEmailWDeliveryConfig(getEmailDeliveryConfigItem(exportItem), pAttachment, getSuccessEmailConfig());
	}

	/**
	 * Send error email.
	 *
	 * @param pExportItemRepositoryId the export item repository id
	 * @param pAttachment             the attachment
	 * @return true, if successful
	 */
	public boolean sendErrorEmail(String pExportItemRepositoryId, File pAttachment) {
		RepositoryItem exportItem = getExportRepositoryItem(pExportItemRepositoryId);
		return sendEmailWDeliveryConfig(getEmailDeliveryConfigItem(exportItem), pAttachment, getErrorEmailConfig());
	}

	/**
	 * Send email using export item's repository id.
	 *
	 * @param pExportItemRepositoryId the export item repository id
	 * @param pAttachment             the attachment
	 * @param pEmailConfig            the email config
	 * @return true, if successful
	 */
	protected boolean sendEmail(String pExportItemRepositoryId, File pAttachment, EmailConfig pEmailConfig) {
		RepositoryItem exportItem = getExportRepositoryItem(pExportItemRepositoryId);
		return sendEmailWDeliveryConfig(getEmailDeliveryConfigItem(exportItem), pAttachment, pEmailConfig);
	}

	/**
	 * Send email using export repository item.
	 *
	 * @param pExportItem  the export item
	 * @param pAttachment  the attachment
	 * @param pEmailConfig the email config
	 * @return true, if successful
	 */
	protected boolean sendEmail(RepositoryItem pExportItem, File pAttachment, EmailConfig pEmailConfig) {
		return sendEmailWDeliveryConfig(getEmailDeliveryConfigItem(pExportItem), pAttachment, pEmailConfig);
	}

	/**
	 * Send email using email delivery config repository item.
	 *
	 * @param pEmailDeliveryItem the email delivery item
	 * @param pAttachment        the attachment
	 * @param pEmailConfig       the email config
	 * @return true, if successful
	 */
	private boolean sendEmailWDeliveryConfig(RepositoryItem pEmailDeliveryItem, File pAttachment, EmailConfig pEmailConfig) {
		boolean result = false;
		if (pEmailDeliveryItem != null) {
			MessageDetails md = new MessageDetails(pEmailConfig);
			md.setSendFrom((String) pEmailDeliveryItem.getPropertyValue(PRP_EMAIL_CONF_SENDER));
			md.setSubject((String) pEmailDeliveryItem.getPropertyValue(PRP_EMAIL_CONF_SUBJ));
			md.setSendTo(getRecipients(pEmailDeliveryItem));
			if (pAttachment != null) {
				md.setAttachment(pAttachment);
			}
			result = getEmailSender().sendEmail(md);
		}
		return result;
	}

	/**
	 * Gets the export repository item.
	 *
	 * @param pExportItemRepositoryId the export item repository id
	 * @return the export repository item
	 */
	private RepositoryItem getExportRepositoryItem(String pExportItemRepositoryId) {
		RepositoryItem result = null;
		if (!StringUtils.isBlank(pExportItemRepositoryId)) {
			try {
				result = getExportRepository().getItem(pExportItemRepositoryId, ID_EXPORT_CONFIG);
			} catch (RepositoryException e) {
				logError(e);
			}
		}
		return result;
	}

	/**
	 * Gets the email delivery config repository item.
	 *
	 * @param pExportItem the export item
	 * @return the email delivery config item
	 */
	private RepositoryItem getEmailDeliveryConfigItem(RepositoryItem pExportItem) {
		RepositoryItem result = null;
		if (pExportItem != null) {
			RepositoryItem reportConfigItem = (RepositoryItem) pExportItem.getPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG);
			if (reportConfigItem != null) {
				result = (RepositoryItem) reportConfigItem.getPropertyValue(PRP_REP_CONF_MAIL_CONF);
			}
		}
		return result;
	}

	/**
	 * Gets the recipients.
	 *
	 * @param riEmailConf the ri email conf
	 * @return the recipients
	 */
	private String[] getRecipients(RepositoryItem riEmailConf) {
		String[] result = null;
		String recipients = (String) riEmailConf.getPropertyValue(PRP_EMAIL_CONF_RECIPIENTS);
		if (!StringUtils.isBlank(recipients) && recipients.contains(",")) {
			result = recipients.split(",");
		} else {
			result = new String[]{recipients};
		}
		return result;
	}

	public MutableRepository getExportRepository() {
		return mExportRepository;
	}

	public void setExportRepository(MutableRepository pExportRepository) {
		mExportRepository = pExportRepository;
	}

	public EmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(EmailSender mEmailSender) {
		this.mEmailSender = mEmailSender;
	}

	public EmailConfig getTestEmailConfig() {
		return mTestEmailConfig;
	}

	public void setTestEmailConfig(EmailConfig mTestEmailConfig) {
		this.mTestEmailConfig = mTestEmailConfig;
	}

	public EmailConfig getSuccessEmailConfig() {
		return mSuccessEmailConfig;
	}

	public void setSuccessEmailConfig(EmailConfig mSuccessEmailConfig) {
		this.mSuccessEmailConfig = mSuccessEmailConfig;
	}

	public EmailConfig getErrorEmailConfig() {
		return mErrorEmailConfig;
	}

	public void setErrorEmailConfig(EmailConfig mErrorEmailConfig) {
		this.mErrorEmailConfig = mErrorEmailConfig;
	}
}