package vsg.integration.export.service.upload;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import vsg.integration.export.service.ftp.SFtpUserInfo;

/**
 * SFTP Uploader
 */
public class SftpUploader extends BaseUploader implements IUploader {

	@Override
	public boolean uploadFile(File pFile, RepositoryItem pExportConfig) {
		synchronized (this) {
			if (pExportConfig == null || pFile == null) {
				return false;
			}
			boolean result = false;

			Session session = null;
			ChannelSftp channelSftp = null;
			RepositoryItem ftpConfig = obtainFtpConfig(pExportConfig);

			try {
				if (ftpConfig != null) {
					session = getSession(ftpConfig, pExportConfig.getRepositoryId());
					channelSftp = getSftpChannel(session);
					checkWorkingDirectory(channelSftp, ftpConfig);
					uploadFile(channelSftp, ftpConfig, pFile);
					result = true;
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					vlogError(e, "Unable to upload file");
				}
			} finally {
				if (null != session && session.isConnected()) {
					if (null != channelSftp) {
						channelSftp.disconnect();
					}
					session.disconnect();
				}
			}

			return result;
		}
	}

	@Override
	public boolean removeFile(File pFile, RepositoryItem pExportConfig) {
		synchronized (this) {
			if (pExportConfig == null || pFile == null) {
				return false;
			}
			boolean result = false;

			Session session = null;
			ChannelSftp channelSftp = null;
			RepositoryItem ftpConfig = obtainFtpConfig(pExportConfig);

			try {
				if (ftpConfig != null) {
					session = getSession(ftpConfig, pExportConfig.getRepositoryId());
					channelSftp = getSftpChannel(session);
					checkWorkingDirectory(channelSftp, ftpConfig);
					removeFile(channelSftp, ftpConfig, pFile);
					result = true;
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					vlogError(e, "Unable to upload file");
				}
			} finally {
				if (null != session && session.isConnected()) {
					if (null != channelSftp) {
						channelSftp.disconnect();
					}
					session.disconnect();
				}
			}

			return result;
		}
	}

	/**
	 * Upload file.
	 *
	 * @param channelSftp the channel sftp
	 * @param pFtpConfig  the connection details
	 * @param pFile       the connection details
	 * @throws SftpException the sftp exception
	 * @throws IOException   Signals that an I/O exception has occurred.
	 */
	private void uploadFile(ChannelSftp channelSftp, RepositoryItem pFtpConfig, File pFile) throws SftpException, IOException {
		InputStream input = null;
		try {
			input = new FileInputStream(pFile.getAbsolutePath());
			if (input != null) {
				channelSftp.put(input, generateRemoteFileName(pFtpConfig, pFile));
			}
		} catch (SftpException | FileNotFoundException e) {
			throw e;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					vlogError(e, "Error");
				}
			}
		}
	}

	/**
	 * Gets the session.
	 *
	 * @param pFtpConfig      the connection details
	 * @param pExportConfigId the connection details
	 * @return the session
	 * @throws NumberFormatException the number format exception
	 * @throws JSchException         the j sch exception
	 */
	private Session getSession(RepositoryItem pFtpConfig, String pExportConfigId) throws JSchException {
		JSch jsch = new JSch();
		Integer port = obtainPort(pFtpConfig);
		Session session = null;

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		String password = (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_PASS);
		String keyName = (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_ENC_KEY_PATH);
		String keyPath = getFileSettingsInfo().getSftpEncKeyPath() + "/" + pExportConfigId + "/" + keyName;
		String pathPhrase = (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_ENC_PHRASE);
		if (keyName != null && !StringUtils.isBlank(keyPath)) {
			if (pathPhrase != null && !StringUtils.isBlank(pathPhrase)) {
				vlogDebug("keyfile path and passphrase are not empty. use this settings to connect");
				jsch.addIdentity(keyPath, (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_ENC_PHRASE));
			} else {
				vlogDebug("keyfile path is not empty, passphrase is empty. use this settings to connect");
				jsch.addIdentity(keyPath);
			}
		}
		if (port != null) {
			session = jsch.getSession((String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_USER_NAME),
					(String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_HOST_NAME), port);
		} else {
			session = jsch.getSession((String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_USER_NAME),
					(String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_HOST_NAME));
		}
		session.setConfig(config);
		if (password != null && !StringUtils.isBlank(password)) {
			vlogDebug("password is not empty. use this settings to connect");
			SFtpUserInfo ui = new SFtpUserInfo();
			ui.setPassword(password);
			session.setUserInfo(ui);
		}
		// int timeout = Integer.parseInt(pConnectionDetails.get("timeout"));
		// session.setTimeout(timeout);
		session.connect();
		return session;
	}

	/**
	 * Gets the sftp channel.
	 *
	 * @param session the session
	 * @return the sftp channel
	 * @throws JSchException the j sch exception
	 */
	private ChannelSftp getSftpChannel(Session session) throws JSchException {
		Channel channel = session.openChannel("sftp");
		channel.connect();
		return (ChannelSftp) channel;
	}

	/**
	 * Check working directory.
	 *
	 * @param pChannel   the channel
	 * @param pFtpConfig the connection details
	 * @throws SftpException the sftp exception
	 */
	protected void checkWorkingDirectory(ChannelSftp pChannel, RepositoryItem pFtpConfig) throws SftpException {
		String workingDir = (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_OUT_DIR_PATH);
		if (!StringUtils.isBlank(workingDir)) {
			if (!workingDir.equals(pChannel.lpwd())) {
				pChannel.cd(workingDir);
				vlogDebug("Working dirctory has been changed");
			}
		} else {
			vlogError("Exception while checking working dirctory: outServerDir param is blank");
		}
	}

	/**
	 * Removes the file.
	 *
	 * @param pSftpChannel the sftp channel
	 * @param pFtpConfig   the connection details
	 * @param pFile        File
	 * @throws SftpException the sftp exception
	 */
	private void removeFile(ChannelSftp pSftpChannel, RepositoryItem pFtpConfig, File pFile) throws SftpException {
		pSftpChannel.rm(generateRemoteFileName(pFtpConfig, pFile));
	}

}