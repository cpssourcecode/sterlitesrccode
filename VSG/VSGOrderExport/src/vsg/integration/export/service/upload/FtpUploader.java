package vsg.integration.export.service.upload;

import org.apache.commons.net.ftp.FTPClient;


/**
 * The Class FtpUploader.
 */
public class FtpUploader extends BaseFtpUploader implements IUploader {

	protected FTPClient initializeFTPClient() {
		vlogDebug("Server type: FTP");
		return new FTPClient();
	}

}