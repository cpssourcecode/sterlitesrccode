package vsg.integration.export.service.upload;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import vsg.integration.export.FileSettingsInfo;
import vsg.integration.export.IRepositoryConstants;

import java.io.File;

/**
 * Base Uploader
 */
public abstract class BaseUploader extends GenericService implements IUploader, IRepositoryConstants {

	/**
	 * file settings info
	 */
	private FileSettingsInfo mFileSettingsInfo;

	public FileSettingsInfo getFileSettingsInfo() {
		return mFileSettingsInfo;
	}

	public void setFileSettingsInfo(FileSettingsInfo mFileSettingsInfo) {
		this.mFileSettingsInfo = mFileSettingsInfo;
	}

	/**
	 * get ftp delivery config from export config
	 *
	 * @param pExportConfig
	 * @return ftp config
	 */
	public RepositoryItem obtainFtpConfig(RepositoryItem pExportConfig) {

		RepositoryItem ftpDeliveryConfig = null;
		if (pExportConfig != null) {
			RepositoryItem reportConfig = (RepositoryItem) pExportConfig.getPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG);
			if (reportConfig != null) {
				ftpDeliveryConfig = (RepositoryItem) reportConfig.getPropertyValue(ID_FTP_DELIVERY_CONFIG);
			}
		}
		return ftpDeliveryConfig;
	}

	/**
	 * get port from ftp delivery config
	 *
	 * @param pFtpConfig - ftp config
	 * @return port
	 */
	public int obtainPort(RepositoryItem pFtpConfig) {
		String portValue = (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_PORT);
		Integer port = null;
		if (portValue != null) {
			port = (Integer) Integer.parseInt(portValue);
		}
		return port;
	}

	/**
	 * generate file name to upload
	 *
	 * @param pFtpConfig - ftp config
	 * @param pFile      - file
	 * @return file name needed for upload
	 */
	public String generateRemoteFileName(RepositoryItem pFtpConfig, File pFile) {
		StringBuilder name = new StringBuilder();
		name.append(pFtpConfig.getPropertyValue(PRP_FTP_CONF_OUT_DIR_PATH));
		if (!name.toString().endsWith("/")) {
			name.append("/");
		}
		name.append(pFile.getName());
		return name.toString();
	}
}
