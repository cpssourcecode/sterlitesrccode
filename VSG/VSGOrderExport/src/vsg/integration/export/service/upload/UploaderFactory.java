package vsg.integration.export.service.upload;

import atg.nucleus.GenericService;

/**
 * A factory for getting FtpUploader objects based on server type.
 */
public class UploaderFactory extends GenericService {

	/**
	 * ftp uploader
	 */
	private FtpUploader mFtpUploader;

	/**
	 * sftp uploader
	 */
	private FtpsUploader mFtpsUploader;

	/**
	 * The sftp uploader.
	 */
	private SftpUploader mSftpUplaoder;

	/**
	 * Gets the ftp upload.
	 *
	 * @param pUplaoderType the uploader type
	 * @return the ftp client
	 */
	public IUploader getUploader(int pUplaoderType) {
		if (pUplaoderType == 0) {
			return getFtpUplaoder();
		} else if (pUplaoderType == 1) {
			return getFtpsUploader();
		} else if (pUplaoderType == 2 || pUplaoderType == 3) {
			return getSftpUplaoder();
		} else {
			throw new RuntimeException("Unsupportable type of server to upload file");
		}
	}

	public FtpUploader getFtpUplaoder() {
		return mFtpUploader;
	}

	public void setFtpUplaoder(FtpUploader pFtpUplaoder) {
		mFtpUploader = pFtpUplaoder;
	}

	public FtpsUploader getFtpsUploader() {
		return mFtpsUploader;
	}

	public void setFtpsUploader(FtpsUploader pFtpsUplaoder) {
		mFtpsUploader = pFtpsUplaoder;
	}

	public SftpUploader getSftpUplaoder() {
		return mSftpUplaoder;
	}

	public void setSftpUplaoder(SftpUploader pSftpUplaoder) {
		this.mSftpUplaoder = pSftpUplaoder;
	}

}
