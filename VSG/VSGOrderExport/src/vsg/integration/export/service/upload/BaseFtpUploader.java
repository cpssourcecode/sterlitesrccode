package vsg.integration.export.service.upload;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import vsg.integration.export.service.ftp.FtpUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.SocketException;

/**
 * Base uploader for FTP and FTPS
 */
public abstract class BaseFtpUploader extends BaseUploader implements IUploader {

	/**
	 * binary constant
	 */
	public static final String BINARY_FILE_TYPE = "binary";
	/**
	 * ascii constant
	 */
	public static final String ASCII_FILE_TYPE = "ascii";

	private FtpUtil mFtpUtil;

	@Override
	public boolean uploadFile(File pFile, RepositoryItem pExportConfig) {
		synchronized (this) {
			vlogDebug("Start uploading file");
			if (pExportConfig == null || pFile == null) {
				vlogDebug("Can't upload cause Export Config or File is empty");
				return false;
			}
			boolean result = false;
			FTPClient ftpClient = getFtpClient();
			RepositoryItem ftpConfig = obtainFtpConfig(pExportConfig);

			try {
				if (ftpConfig != null && connectToServer(ftpClient, ftpConfig)) {
					if (loginToServer(ftpClient, ftpConfig)) {
						configConnection(ftpClient, pFile);
						checkWorkingDirectory(ftpClient, ftpConfig);
						result = uploadFile(ftpClient, ftpConfig, pFile);
					}
				}
			} catch (Exception e) {
				vlogError(e, "Unable to upload file");
			} finally {
				try {
					logout(ftpClient);
					disconnect(ftpClient);
				} catch (IOException e) {
					vlogError("Exception logut from server: " + e);
				}
			}
			return result;
		}
	}

	@Override
	public boolean removeFile(File pFile, RepositoryItem pExportConfig) {
		synchronized (this) {
			vlogDebug("Start removing file");
			if (pExportConfig == null || pFile == null) {
				vlogDebug("Can't remove cause Export Config or File is empty");
				return false;
			}
			boolean result = false;
			FTPClient ftpClient = getFtpClient();
			RepositoryItem ftpConfig = obtainFtpConfig(pExportConfig);

			try {
				if (connectToServer(ftpClient, ftpConfig)) {
					if (loginToServer(ftpClient, ftpConfig)) {
						configConnection(ftpClient, pFile);
						checkWorkingDirectory(ftpClient, ftpConfig);
						result = removeFile(ftpClient, ftpConfig, pFile);
					}
				}
			} catch (Exception e) {
				vlogError(e, "Unable to remove file");
			} finally {
				try {
					logout(ftpClient);
					disconnect(ftpClient);
				} catch (IOException e) {
					vlogError("Exception logut from server: " + e);
				}
			}
			return result;
		}
	}

	/**
	 * Gets the ftp client.
	 *
	 * @return the ftp client
	 */
	protected FTPClient getFtpClient() {
		FTPClient ftpClient = initializeFTPClient();
		ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out), true));
		return ftpClient;
	}

	/**
	 * Initialize ftp client.
	 *
	 * @return the FTP client
	 */
	protected abstract FTPClient initializeFTPClient();

	/**
	 * Connect to server.
	 *
	 * @param pFtpClient the ftp client
	 * @param pFtpConfig the connection details
	 * @throws IOException
	 * @throws SocketException
	 */
	protected boolean connectToServer(FTPClient pFtpClient, RepositoryItem pFtpConfig) throws SocketException, IOException {
		int reply;

		Integer port = obtainPort(pFtpConfig);
		String server = (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_HOST_NAME);

		return getFtpUtil().connectToServer(pFtpClient, port, server);
	}


	/**
	 * Login to server.
	 *
	 * @param pFtpClient the ftp client
	 * @param pFtpConfig the connection details
	 * @throws IOException
	 */
	protected boolean loginToServer(FTPClient pFtpClient, RepositoryItem pFtpConfig) throws IOException {
		boolean result = false;
		vlogDebug("Login to server...");
		result = pFtpClient.login((String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_USER_NAME),
				(String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_PASS));
		if (!result) {
			vlogDebug("Login failed");
			pFtpClient.logout(); // to avoid bad server session
		}
		return result;
	}

	/**
	 * Config connection.
	 *
	 * @param pFtpClient the ftp client
	 * @param pFile      File
	 * @throws IOException
	 */
	protected void configConnection(FTPClient pFtpClient, File pFile) throws IOException {

		if (isLoggingDebug()) {
			logDebug("Remote system is " + pFtpClient.getSystemType());
		}
		pFtpClient.setFileType(selectFileType(pFile));

		// Use passive mode as default because most of us are
		// behind firewall these days.
		//if ("localActive".equals(pConnectionDetails.get("transferMode"))) {
		//	pFtpClient.enterLocalActiveMode();
		//} else {
		pFtpClient.enterLocalPassiveMode();
		//}

		// pFtpClient.setUseEPSVwithIPv4(true);
	}

	public int selectFileType(File pFile) {
		int fileType = FTP.ASCII_FILE_TYPE;
		if (pFile != null && "pdf".equals(FilenameUtils.getExtension(pFile.getAbsolutePath()))) {
			fileType = FTP.BINARY_FILE_TYPE;
			vlogDebug("Binary file type selected");
		} else {
			vlogDebug("ASCII file type selected");
		}
		return fileType;
	}

	/**
	 * Check working directory.
	 *
	 * @param pFtpClient the ftp client
	 * @param pFtpConfig the connection details
	 * @throws IOException
	 */
	protected void checkWorkingDirectory(FTPClient pFtpClient, RepositoryItem pFtpConfig) throws IOException {
		String workingDir = (String) pFtpConfig.getPropertyValue(PRP_FTP_CONF_OUT_DIR_PATH);
		if (!StringUtils.isBlank(workingDir)) {
			if (!workingDir.equals(pFtpClient.printWorkingDirectory())) {
				pFtpClient.changeWorkingDirectory(workingDir);
				vlogDebug("Working dirctory has been changed");
			}
		} else {
			vlogError("Exception while checking working dirctory: workingDir param is blank");
		}
	}

	/**
	 * Logout.
	 *
	 * @param pFtpClient the ftp client
	 * @throws IOException
	 */
	protected void logout(FTPClient pFtpClient) throws IOException {
		pFtpClient.noop();
		pFtpClient.logout();
	}

	/**
	 * Disconnect.
	 *
	 * @param pFtpClient the ftp client
	 * @throws IOException
	 */
	protected void disconnect(FTPClient pFtpClient) throws IOException {
		pFtpClient.disconnect();
	}

	/**
	 * Upload file.
	 *
	 * @param pFtpClient the ftp client
	 * @param pFtpConfig the connection details
	 * @return true, if successful
	 * @throws IOException
	 */
	protected boolean uploadFile(FTPClient pFtpClient, RepositoryItem pFtpConfig, File pFile) throws IOException {
		boolean result = false;
		InputStream input = null;
		try {
			input = new FileInputStream(pFile.getAbsolutePath());
			if (input != null) {
				result = pFtpClient.storeFile(generateRemoteFileName(pFtpConfig, pFile), input);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					vlogError(e, "Error");
				}
			}
		}
		return result;
	}

	/**
	 * Removes the file.
	 *
	 * @param pFtpClient the ftp client
	 * @param pFtpConfig the connection details
	 * @return true, if successful
	 * @throws IOException
	 */
	protected boolean removeFile(FTPClient pFtpClient, RepositoryItem pFtpConfig, File pFile) throws IOException {
		return pFtpClient.deleteFile(generateRemoteFileName(pFtpConfig, pFile));
	}

	public FtpUtil getFtpUtil() {
		return mFtpUtil;
	}

	public void setFtpUtil(FtpUtil pFtpUtil) {
		mFtpUtil = pFtpUtil;
	}
}
