package vsg.integration.export.service.upload;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;

/**
 * The Class FtpsUploader.
 */
public class FtpsUploader extends BaseFtpUploader implements IUploader {

	protected FTPClient initializeFTPClient() {
		FTPClient result = null;
		vlogDebug("Server type: FTPS");

		FTPSClient ftps;
		String protocol = "true";
		if (protocol.equals("true")) {
			ftps = new FTPSClient(true);
		} else if (protocol.equals("false")) {
			ftps = new FTPSClient(false);
		} else {
			String prot[] = protocol.split(",");
			if (prot.length == 1) { // Just protocol
				ftps = new FTPSClient(protocol);
			} else { // protocol,true|false
				ftps = new FTPSClient(prot[0], Boolean.parseBoolean(prot[1]));
			}
		}
		result = ftps;
		// if ("all".equals(trustmgr)) {
		// ftps.setTrustManager(TrustManagerUtils.getAcceptAllTrustManager());
		// } else if ("valid".equals(trustmgr)) {
		// ftps.setTrustManager(TrustManagerUtils.getValidateServerCertificateTrustManager());
		// } else if ("none".equals(trustmgr)) {
		// ftps.setTrustManager(null);
		// }

		return result;
	}
}