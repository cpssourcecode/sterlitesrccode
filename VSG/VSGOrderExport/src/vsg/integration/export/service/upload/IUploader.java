package vsg.integration.export.service.upload;

import atg.repository.RepositoryItem;

import java.io.File;

/**
 * The Interface Uploader.
 */
public interface IUploader {

	/**
	 * Upload file
	 *
	 * @param pFile         - file
	 * @param pExportConfig - export config
	 * @return true, if successful
	 */
	boolean uploadFile(File pFile, RepositoryItem pExportConfig);

	/**
	 * Remove file
	 *
	 * @param pFile - file
	 * @return true, if successful
	 */
	boolean removeFile(File pFile, RepositoryItem pExportConfig);
}
