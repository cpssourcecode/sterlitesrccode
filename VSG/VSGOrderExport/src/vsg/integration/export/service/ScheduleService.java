package vsg.integration.export.service;

import atg.nucleus.GenericService;
import atg.nucleus.PropertyValueParseException;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.lockmanager.ClientLockManager;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.SchedulePropertyValueParser;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.IScheduleConstants;
import vsg.integration.export.job.TransactionErrorRepJob;
import vsg.integration.export.job.TransactionSuccessfulRepJob;
import vsg.integration.export.job.WebserviceJob;
import vsg.integration.export.job.base.VSGReportJob;
import vsg.integration.export.job.base.VSGScheduledJob;
import vsg.integration.export.report.service.ReportService;

import java.util.HashMap;
import java.util.Map;

/**
 * service to run scheduled jobs
 */
public class ScheduleService extends GenericService implements IRepositoryConstants, IScheduleConstants {

	/**
	 * Scheduler
	 */
	private Scheduler mScheduler;

	/**
	 * lock manager
	 */
	private ClientLockManager mLockManager;
	/**
	 * report service
	 */
	private ReportService mReportService;

	/**
	 * rql query for quering all Export Configs
	 */
	private static final String QUERY = "ALL";

	/**
	 * export repository
	 */
	private MutableRepository mRepository;

	/**
	 * map of job names (key) and job ids (value)
	 */
	private Map<String, Integer> mRegisteredJobs = new HashMap<String, Integer>();

	/**
	 * schedule values
	 */
	private static Map<String, String> mScheduleValues;

	/**
	 * repository service
	 */
	private ExportRepositoryService mRepositoryService;

	/**
	 * /vsg/integration/export/service/ExportWSService
	 */
	private ExportWSService mExportWSService;

	//------------------------------------------------------------------------------------------------------------------

	@Override
	public void doStartService() throws ServiceException {
		initJobs();
		super.doStartService();
	}

	/**
	 * initializes jobs
	 */
	public void initJobs() {
		vlogDebug("Initialize of export schedules");
		RepositoryItem[] exportConfigs = getRepositoryService().queryExportItems(QUERY, new Object[0]);
		if (null != exportConfigs) {
			for (RepositoryItem exportConfiguration : exportConfigs) {
				vlogDebug("Initialize schedule for export configuration: {0}", exportConfiguration.getRepositoryId());
				RepositoryItem outputFormatConfig = (RepositoryItem) exportConfiguration.getPropertyValue(ID_OUTPUT_FORMAT_CONFIG);
/*
				RepositoryItem transferOptionsConfig = (RepositoryItem) item.getPropertyValue(ID_TRANSFER_OPTIONS_CONFIG);
*/
				RepositoryItem outputReportConfig = (RepositoryItem) exportConfiguration.getPropertyValue(ID_OUT_REPORT_CONFIG);
				if (null != outputFormatConfig) {
					WebserviceJob webserviceJob = WebserviceJob.instance(
							exportConfiguration.getRepositoryId(),
							(String) outputFormatConfig.getPropertyValue(PRP_TOC_SCHEDULE),
							getExportWSService()
					);
					registerJob(exportConfiguration.getRepositoryId(), webserviceJob);
				}
				if (null != outputReportConfig) {
					Integer errorFreqkey = (Integer) outputReportConfig.getPropertyValue(PRP_REP_CONF_EFREQ);
					Integer successFreqkey = (Integer) outputReportConfig.getPropertyValue(PRP_REP_CONF_SFREQ);

					Boolean enableError = (Boolean) outputReportConfig.getPropertyValue(PRP_REP_CONF_ENABLE_ERROR);
					Boolean enableSuccess = (Boolean) outputReportConfig.getPropertyValue(PRP_REP_CONF_ENABLE_SUCCESS);

					if (enableError && errorFreqkey != null) {
						TransactionErrorRepJob transactionErrorRepJob = new TransactionErrorRepJob(exportConfiguration.getRepositoryId(),
								getScheduleValues().get(errorFreqkey.toString()));
						registerJob(exportConfiguration.getRepositoryId(), transactionErrorRepJob);
					}
					if (enableSuccess && successFreqkey != null) {
						TransactionSuccessfulRepJob transactionSuccessfulRepJob = new TransactionSuccessfulRepJob(exportConfiguration.getRepositoryId(),
								getScheduleValues().get(successFreqkey.toString()));
						registerJob(exportConfiguration.getRepositoryId(), transactionSuccessfulRepJob);
					}
				}
			}
		} else {
			vlogError("No configurations are defined to schedule");
		}
	}

	/**
	 * registers new or updates old scheduled job
	 * @param pExportConfigurationId export configuration id
	 * @param pJob scheduled job
	 */
	public synchronized void registerJob(String pExportConfigurationId, VSGScheduledJob pJob) {
		pJob.setClientLockManager(getLockManager());
		if (pJob instanceof VSGReportJob) {
			((VSGReportJob) pJob).setReportService(getReportService());
		}
		String jobKey = pJob.getJobName();
		Schedule schedule = initializeSchedule(pJob.getScheduleString(), pJob.getDefaultSchedule());
		if (schedule == null) {
			vlogError("Schedule is not initialized.");
			return;
		}

		if (isAlreadyRegistered(jobKey)) {
			getScheduler().removeScheduledJob(getRegisteredJobs().get(jobKey));
		}

		ScheduledJob job = new ScheduledJob(jobKey, jobKey, jobKey, schedule, pJob, ScheduledJob.REUSED_THREAD);
		int jobId = getScheduler().addScheduledJob(job);
		getRegisteredJobs().put(jobKey, jobId);
	}

	/**
	 * initializes schedule by property value. use default in error case
	 *
	 * @param pValue - property value
	 * @return initialized schedule
	 */
	private Schedule initializeSchedule(String pValue, String pDefaultSchedule) {

		SchedulePropertyValueParser mParser = new SchedulePropertyValueParser();
		Schedule schedule = null;
		try {
			schedule = (Schedule) mParser.parsePropertyValue(pValue, Schedule.class);
		} catch (PropertyValueParseException e) {
			vlogError("Unable to initialize schedule by specified schedule value: " + pValue);
		}

		if (schedule == null) {
			try {
				schedule = (Schedule) mParser.parsePropertyValue(pDefaultSchedule, Schedule.class);
			} catch (PropertyValueParseException e) {
				vlogError("Unable to initialize schedule by default schedule value: " + pDefaultSchedule);
			}
		}

		return schedule;
	}

	/**
	 * check if job is already registered
	 *
	 * @param jobKey key of the job
	 * @return true, if job with provided key is already registered, false othrwise
	 */
	private boolean isAlreadyRegistered(String jobKey) {
		boolean flag = true;
		if (getRegisteredJobs().get(jobKey) == null) {
			flag = false;
		}
		return flag;
	}

	/**
	 * deletes jobs by config id
	 *
	 * @param pConfigId key of the job
	 */
	public void deleteConfigJobs(String pConfigId) {
		try {
			RepositoryItem item = getRepository().getItem(pConfigId, ID_EXPORT_CONFIG);
			if (null != item) {
				deleteJobByKey(WEBSERVICE_JOB_NAME + item.getRepositoryId());
				deleteJobByKey(XML_TRANSFER_JOB_NAME + item.getRepositoryId());
				deleteJobByKey(TR_SUCCESS_REP_JOB_NAME + item.getRepositoryId());
				deleteJobByKey(TR_ERROR_REP_JOB_NAME + item.getRepositoryId());
			}
		} catch (RepositoryException e) {
			vlogError("error while querying export config");
		}
	}

	/**
	 * deleted job by job key
	 *
	 * @param pJobKey - job key
	 */
	private void deleteJobByKey(String pJobKey) {
		if (getRegisteredJobs().get(pJobKey) != null) {
			getScheduler().removeScheduledJob(getRegisteredJobs().get(pJobKey));
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	public ExportRepositoryService getRepositoryService() {
		return mRepositoryService;
	}

	public void setRepositoryService(ExportRepositoryService pRrepositoryService) {
		this.mRepositoryService = pRrepositoryService;
	}

	public Scheduler getScheduler() {
		return mScheduler;
	}

	public void setScheduler(Scheduler pScheduler) {
		this.mScheduler = pScheduler;
	}

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		this.mRepository = pRepository;
	}

	public Map<String, String> getScheduleValues() {
		return mScheduleValues;
	}

	public void setScheduleValues(Map<String, String> pScheduleValues) {
		ScheduleService.mScheduleValues = pScheduleValues;
	}

	public Map<String, Integer> getRegisteredJobs() {
		return mRegisteredJobs;
	}

	public void setRegisteredJobs(Map<String, Integer> pRegisteredJobs) {
		this.mRegisteredJobs = pRegisteredJobs;
	}

	public ClientLockManager getLockManager() {
		return mLockManager;
	}

	public void setLockManager(ClientLockManager pLockManager) {
		this.mLockManager = pLockManager;
	}

	public ReportService getReportService() {
		return mReportService;
	}

	public void setReportService(ReportService pReportService) {
		mReportService = pReportService;
	}

	public ExportWSService getExportWSService() {
		return mExportWSService;
	}

	public void setExportWSService(ExportWSService pExportWSService) {
		mExportWSService = pExportWSService;
	}
}
