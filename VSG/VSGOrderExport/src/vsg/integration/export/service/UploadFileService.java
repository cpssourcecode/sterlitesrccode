package vsg.integration.export.service;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import vsg.integration.export.FileSettingsInfo;
import vsg.integration.export.IRepositoryConstants;

import java.io.File;

/**
 * Upload File Service
 */
public class UploadFileService extends GenericService implements IRepositoryConstants {

	/**
	 * File Settings Info
	 */
	private FileSettingsInfo mFileSettingsInfo;

	public FileSettingsInfo getFileSettingsInfo() {
		return mFileSettingsInfo;
	}

	public void setFileSettingsInfo(FileSettingsInfo pFileSettingsInfo) {
		this.mFileSettingsInfo = pFileSettingsInfo;
	}

	/**
	 * delete all unnecessary files of export config
	 *
	 * @param pExportConfig - export config
	 */
	public void deleteUnnecessaryFiles(RepositoryItem pExportConfig) {
		if (pExportConfig != null) {
			deleteUnnecessaryWsdls(pExportConfig);
			deleteUnnecessaryXsds(pExportConfig);
			deleteUnnecessaryKeys(pExportConfig);
			deleteUnnecessaryEncKeys(pExportConfig);
			deleteUnnecessarySftpEncKeys(pExportConfig);
		} else {
			vlogDebug("No Export Config");
		}

	}

	/**
	 * delete all unnecessary wsdls of output format config
	 *
	 * @param pOutputFormatConfig - output format config
	 */
	public void deleteUnnecessaryWsdls(RepositoryItem pExportConfig) {
		RepositoryItem outputFormatConfig = (RepositoryItem) pExportConfig.getPropertyValue(ID_OUTPUT_FORMAT_CONFIG);
		if (outputFormatConfig != null) {
			String folderPath = getFileSettingsInfo().getWsdlPath() + "/" + pExportConfig.getRepositoryId();
			deleteFiles(folderPath, (String) outputFormatConfig.getPropertyValue(PRP_OFC_UPLOAD_WSDL));
		} else {
			vlogDebug("Transfer Options Config is empty. Dont need to delete files");
		}

	}

	/**
	 * delete all unnecessary xsds of transfer options config
	 *
	 * @param pTransferOptions - transfer options config
	 */
	public void deleteUnnecessaryXsds(RepositoryItem pExportConfig) {
		RepositoryItem transferOptions = (RepositoryItem) pExportConfig.getPropertyValue(ID_TRANSFER_OPTIONS_CONFIG);
		if (transferOptions != null) {
			String folderPath = getFileSettingsInfo().getXsdPath() + "/" + pExportConfig.getRepositoryId();
			deleteFiles(folderPath, (String) transferOptions.getPropertyValue(PRP_TOC_UPLOAD_XSD));
		} else {
			vlogDebug("Transfer Options Config is empty. Dont need to delete files");
		}
	}

	/**
	 * delete all unnecessary keys of transfer options config
	 *
	 * @param pTransferOptions - transfer options config
	 */
	public void deleteUnnecessaryKeys(RepositoryItem pExportConfig) {
		RepositoryItem transferOptions = (RepositoryItem) pExportConfig.getPropertyValue(ID_TRANSFER_OPTIONS_CONFIG);
		if (transferOptions != null) {
			String folderPath = getFileSettingsInfo().getTransferOptionsKeyPath() + "/" + pExportConfig.getRepositoryId();
			deleteFiles(folderPath, (String) transferOptions.getPropertyValue(PRP_TOC_KEY));
		} else {
			vlogDebug("Transfer Options Config is empty. Dont need to delete files");
		}
	}

	/**
	 * delete all unnecessary encription keys of transfer options config
	 *
	 * @param pTransferOptions - transfer options config
	 */
	public void deleteUnnecessaryEncKeys(RepositoryItem pExportConfig) {
		RepositoryItem transferOptions = (RepositoryItem) pExportConfig.getPropertyValue(ID_TRANSFER_OPTIONS_CONFIG);
		if (transferOptions != null) {
			String folderPath = getFileSettingsInfo().getTransferOptionsEncKeyPath() + "/" + pExportConfig.getRepositoryId();
			deleteFiles(folderPath, (String) transferOptions.getPropertyValue(PRP_TOC_ENCRIPTION_KEY));
		} else {
			vlogDebug("Transfer Options Config is empty. Dont need to delete files");
		}
	}

	/**
	 * delete all unnecessary encription keys of ftp delivery config
	 *
	 * @param pFtpDeliveryConfig
	 */
	public void deleteUnnecessarySftpEncKeys(RepositoryItem pExportConfig) {
		RepositoryItem reportConfig = (RepositoryItem) pExportConfig.getPropertyValue(ID_OUT_REPORT_CONFIG);
		RepositoryItem ftpDeliveryConfig = null;
		if (reportConfig != null) {
			ftpDeliveryConfig = (RepositoryItem) reportConfig.getPropertyValue(ID_FTP_DELIVERY_CONFIG);
			if (ftpDeliveryConfig != null) {
				String folderPath = getFileSettingsInfo().getSftpEncKeyPath() + "/" + pExportConfig.getRepositoryId();
				deleteFiles(folderPath, (String) ftpDeliveryConfig.getPropertyValue(PRP_FTP_CONF_ENC_KEY_PATH));
			} else {
				vlogDebug("Ftp Delivery Config is empty. Dont need to delete files");
			}
		} else {
			vlogDebug("Report Config is empty. Dont need to delete files");
		}
	}

	/**
	 * delete files from folder except file with provided name.
	 *
	 * @param pPath        - folder path
	 * @param pExcludeFile - file name that shouldn't be deleted
	 */
	private void deleteFiles(String pPath, String pExcludeFile) {
		File folder = new File(pPath);
		File[] listOfFiles = folder.listFiles();
		if (listOfFiles != null) {
			for (File file : listOfFiles) {
				if (!file.getName().equals(pExcludeFile)) {
					vlogDebug("Deleting " + file.getName() + " from " + pPath);
					file.delete();
				}
			}
		}
	}
}
