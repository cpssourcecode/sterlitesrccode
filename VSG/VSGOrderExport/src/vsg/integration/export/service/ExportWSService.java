package vsg.integration.export.service;

import atg.core.util.StringUtils;
import atg.repository.*;
import atg.repository.rql.RqlStatement;
import org.apache.axis.soap.SOAPConnectionImpl;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.parse.wsdl.DomConstants;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.service.soap.SOAPCallInfo;
import vsg.integration.export.service.soap.SOAPRequestBuilder;
import vsg.integration.export.service.soap.SOAPResponseReader;
import vsg.integration.export.service.util.XMLTransformer;
import vsg.integration.export.service.validator.ValidatorsSuite;
import vsg.integration.export.util.bean.TransactionInfo;
import vsg.integration.export.util.bean.ValidationResult;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.util.Set;
import java.io.IOException;

/**
 * @author Dmitry Golubev
 */
public class ExportWSService extends ErrorTracker implements IRepositoryConstants, DomConstants {
	/**
	 * SOAP request builder
	 */
	private SOAPRequestBuilder mSOAPRequestBuilder = new SOAPRequestBuilder();
	/**
	 * SOAP response reader
	 */
	private SOAPResponseReader mSOAPResponseReader = new SOAPResponseReader();
	/**
	 * validator suite
	 */
	private ValidatorsSuite mValidatorsSuite;
	/**
	 * export repository service
	 */
	private ExportRepositoryService mExportRepositoryService;
	/**
	 * post export items update service
	 */
	private PostExportItemsUpdateService mPostExportItemsUpdateService;
	/**
	 * SOAPCallInfo
	 */
	private SOAPCallInfo mSOAPCallInfo;

	/**
	 * retryNumber
	 */
	private int mRetryNumber = 3;

	/**
	 * timeToWait
	 */
	private long mTimeToWait;

	/**
	 * boolean testFailure
	 */
	private boolean mTestFailure;

	/** Time out property
	 * time out value in milli seconds
	 */
	private int mTimeOut;

	//------------------------------------------------------------------------------------------------------------------

	public SOAPRequestBuilder getSOAPRequestBuilder() {
		return mSOAPRequestBuilder;
	}

	public void setSOAPRequestBuilder(SOAPRequestBuilder pSOAPRequestBuilder) {
		mSOAPRequestBuilder = pSOAPRequestBuilder;
	}

	public SOAPResponseReader getSOAPResponseReader() {
		return mSOAPResponseReader;
	}

	public void setSOAPResponseReader(SOAPResponseReader pSOAPResponseReader) {
		mSOAPResponseReader = pSOAPResponseReader;
	}

	public ValidatorsSuite getValidatorsSuite() {
		return mValidatorsSuite;
	}

	public void setValidatorsSuite(ValidatorsSuite pValidatorsSuite) {
		mValidatorsSuite = pValidatorsSuite;
	}

	public Repository getExportRepository() {
		return getExportRepositoryService().getExportRepository();
	}

	public ExportRepositoryService getExportRepositoryService() {
		return mExportRepositoryService;
	}

	public void setExportRepositoryService(ExportRepositoryService pExportRepositoryService) {
		mExportRepositoryService = pExportRepositoryService;
	}

	public SOAPCallInfo getSOAPCallInfo() {
		return mSOAPCallInfo;
	}

	public void setSOAPCallInfo(SOAPCallInfo pSOAPCallInfo) {
		mSOAPCallInfo = pSOAPCallInfo;
	}

	public PostExportItemsUpdateService getPostExportItemsUpdateService() {
		return mPostExportItemsUpdateService;
	}

	public void setPostExportItemsUpdateService(PostExportItemsUpdateService pPostExportItemsUpdateService) {
		mPostExportItemsUpdateService = pPostExportItemsUpdateService;
	}

	public int getRetryNumber() {
		return mRetryNumber;
	}

	public void setRetryNumber(int pRetryNumber) {
		mRetryNumber = pRetryNumber;
	}

	public long getTimeToWait() {
		return mTimeToWait;
	}

	public void setTimeToWait(long pTimeToWait) {
		mTimeToWait = pTimeToWait;
	}

	public boolean isTestFailure() {
		return mTestFailure;
	}

	public void setTestFailure(boolean pTestFailure) {
		mTestFailure = pTestFailure;
	}

	public int getTimeOut() {
		return mTimeOut;
	}

	public void setTimeOut(int pTimeOut) {
		mTimeOut = pTimeOut;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * make call to ws using export configuration
	 *
	 * @param pExportConfigId export configuration id
	 * @param pItemForExport  repository item for export
	 * @return transaction id
	 */
	public String invokeModel(String pExportConfigId, RepositoryItem pItemForExport) {
		return invokeModelCommon(pExportConfigId, new RepositoryItem[]{pItemForExport});
	}

	/**
	 * make call to ws using export configuration
	 *
	 * @param pExportConfigId export configuration id
	 * @param pItemsForExport repository items for export
	 * @return transaction id
	 */
	public String invokeModel(String pExportConfigId, RepositoryItem[] pItemsForExport) {
		return invokeModelCommon(pExportConfigId, pItemsForExport);
	}

	/**
	 * make call to ws using export configuration
	 *
	 * @param pExportConfigId export configuration id
	 * @return transaction id
	 */
	public String invokeModel(String pExportConfigId) {
 		return invokeForEach(pExportConfigId);
	}

	/**
	 * validate export config and invokeModelCommonWithoutConfValid() for each item
	 *
	 * @param pExportConfigId export configuration id
	 * @return transaction id
	*/
	private String invokeForEach(String pExportConfigId) {
		synchronized (this) {
			try {
				MutableRepositoryItem exportJobTime = ((MutableRepository) getExportRepository()).createItem(ID_EXPORT_JOB_TIME);
				((MutableRepository) getExportRepository()).addItem(exportJobTime);

				RepositoryItem exportConfigItem = getExportRepository().getItem(pExportConfigId, ID_EXPORT_CONFIG);
				ValidationResult validationResult = getValidatorsSuite().getWSDLModelValidator().validateModel(exportConfigItem);
				WSDLModel wsdlModel = (WSDLModel) validationResult.getAttachment();
				if (validationResult.isSuccess()) {
					validationResult = getValidatorsSuite().getWSDLMappingValidator().isMappingValid(exportConfigItem);
				}
				if (validationResult.isSuccess()) {
					RepositoryItem[] pItemsForExport = getRepositoryItems(exportConfigItem);
					StringBuilder result = new StringBuilder();
					if (pItemsForExport != null) {
						for (int i = 0; i < pItemsForExport.length; i++) {
							result.append(invokeModelCommon(exportConfigItem, wsdlModel, pItemsForExport[i]));
							result.append(" ");
						}
					}
					return result.toString();
				} else {
					trackError(validationResult.getFailReason());
				}
			} catch (Exception e) {
				trackError(e, "Unable to perform export for export configuration '" + pExportConfigId + "'. Please check log.");
			}
		}
		return null;
	}


	/**
	 * make call for item
	 *
	 * @param exportConfigItem export configuration item
	 * @param wsdlModel wsdl model
	 * @param itemForExport item for export
	 * @return transaction id
	 */
	private String invokeModelCommon(RepositoryItem exportConfigItem, WSDLModel wsdlModel, RepositoryItem itemForExport) {
		synchronized (this) {
			setSOAPCallInfo(new SOAPCallInfo());
			setTransactionInfo(new TransactionInfo());
			getSOAPCallInfo().setTransactionInfo(getTransactionInfo());
			try {
				RepositoryItem[] pItemsForExport = {itemForExport};
				return exportModel(exportConfigItem, wsdlModel, pItemsForExport);
			} catch (Exception e) {
				trackError(e, "Unable to perform export for export configuration '" + exportConfigItem + "'. Please check log.");
			}
		}
		return null;
	}



	/**
	 * make call to ws using export configuration and specified items for export
	 *
	 * @param pExportConfigId export configuration id
	 * @param pItemsForExport repository items for export
	 * @return transaction id
	 */
	private String invokeModelCommon(String pExportConfigId, RepositoryItem[] pItemsForExport) {
		synchronized (this) {
			setSOAPCallInfo(new SOAPCallInfo());
			setTransactionInfo(new TransactionInfo());
			getSOAPCallInfo().setTransactionInfo(getTransactionInfo());
			try {
				RepositoryItem exportConfigItem = getExportRepository().getItem(pExportConfigId, ID_EXPORT_CONFIG);
				ValidationResult validationResult = getValidatorsSuite().getWSDLModelValidator().validateModel(exportConfigItem);
				WSDLModel wsdlModel = (WSDLModel) validationResult.getAttachment();
				if (validationResult.isSuccess()) {
					validationResult = getValidatorsSuite().getWSDLMappingValidator().isMappingValid(exportConfigItem);
				}
				if (validationResult.isSuccess()) {
					if (pItemsForExport == null) {
						pItemsForExport = getRepositoryItems(exportConfigItem);
					}
					return exportModel(exportConfigItem, wsdlModel, pItemsForExport);
				} else {
					trackError(validationResult.getFailReason());
				}
			} catch (Exception e) {
				trackError(e, "Unable to perform export for export configuration '" + pExportConfigId + "'. Please check log.");
			}
		}
		return null;
	}

	/**
	 * export model to ws
	 *
	 * @param pExportConfigItem export configuration
	 * @param pWSDLModel        wsdl model
	 * @param pItemsForExport   items for export
	 * @return transaction id
	 */
	private String exportModel(RepositoryItem pExportConfigItem,
							   WSDLModel pWSDLModel,
							   RepositoryItem[] pItemsForExport) {
		try {
			getSOAPCallInfo().setItemsForExport(pItemsForExport);
			getSOAPCallInfo().setRepositoryMappings(
					(Set<RepositoryItem>) pExportConfigItem.getPropertyValue(PRP_EXPORT_RECORD_MAPPINGS)
			);

			execute(pExportConfigItem, pWSDLModel, pItemsForExport);
/*
			if (getSOAPCallInfo().getItemsForExport() != null) {
				getSOAPCallInfo().setItemsExported(
						new RepositoryItem[getSOAPCallInfo().getItemsForExport().length]
				);
				makeCall(pExportConfigItem, pWSDLModel);
			}
*/
		} catch (Exception e) {
			trackError(e, "Unable to export model.");
		}
		Boolean markAsExported = (Boolean) pExportConfigItem.getPropertyValue(PRP_EXPORT_MARK_AS_EXPORTED);
		if (markAsExported) {
			getPostExportItemsUpdateService().updateExportedItems(
					getTransactionInfo().getExportedItemsIds(),
					(String) pExportConfigItem.getPropertyValue(PRP_EXPORT_ITEM_DESCRIPTOR)
			);
		}
		return getExportRepositoryService().storeTransaction(pExportConfigItem, getTransactionInfo());

	}


	private void execute(RepositoryItem pExportConfigItem, WSDLModel pWSDLModel, RepositoryItem[] pItemsForExport) {
		if (getSOAPCallInfo().getItemsForExport() != null) {
			if (getSOAPCallInfo().getItemsExported() == null) { // only 1st step
				getSOAPCallInfo().setItemsExported(
						new RepositoryItem[getSOAPCallInfo().getItemsForExport().length]
				);
			}
			makeCall(pExportConfigItem, pWSDLModel, pItemsForExport);
		}
		if (getSOAPCallInfo().getItemsForExport() != null && getSOAPCallInfo().getItemsForExport().length > 0) {
			execute(pExportConfigItem, pWSDLModel, pItemsForExport);
		}
	}

	/**
	 * builds SOAP request and validates it
	 *
	 * @param pExportConfigItem export configuration
	 * @param pWSDLModel        wsdl model
	 * @return SOAPMessage
	 */
	private SOAPMessage createSOAPMessage(RepositoryItem pExportConfigItem, WSDLModel pWSDLModel) {
		try {
			SOAPMessage request = getSOAPRequestBuilder().createSOAPMessage(
					pExportConfigItem,
					pWSDLModel,
					getSOAPCallInfo()
			);
/*
			ValidationResult validationResult = getValidatorsSuite().getSOAPMessageValidator().
					validateSOAPMessageToWsdl(request, pWSDLModel, this);
*/
			return request;
/*
			if(validationResult.isSuccess()) {
				return request;
			} else {
				trackError("SOAP request failed due to xsd validation: " + validationResult.getFailReason());
				return null;
			}
*/
		} catch (Exception e) {
			trackError(e, "Unable to build SOAPMessage request.");
			logError(e);
			return null;
		}
	}

	/**
	 * make call to ws
	 *
	 * @param pExportConfigItem export configuration
	 * @param pWsdlModel        wsdl model
	 */
	private void makeCall(RepositoryItem pExportConfigItem, WSDLModel pWsdlModel, RepositoryItem[] pItemsForExport) {
		SOAPMessage soapRequest = createSOAPMessage(pExportConfigItem, pWsdlModel);
		if (soapRequest == null) {
			return;
		}
		getTransactionInfo().setRequest(XMLTransformer.getSOAPMessageAsString(soapRequest));

		SOAPMessage soapResponse = null;

		RetryStrategy retry = new RetryStrategy(getRetryNumber(), getTimeToWait());

		vlogDebug("Making {0} attempt", retry.mNumberOfRetries + 1 - retry.mNumberOfTriesLeft);
		int count = 1;
		while (retry.shouldRetry()) {
			vlogDebug("Number of attempt {0}",count);
			// need to clear status from previous failure if happened
			if (getTransactionInfo() != null && TransactionInfo.FAIL == getTransactionInfo().getStatus()) {
				getTransactionInfo().setStatus(TransactionInfo.SUCCESS);
				getTransactionInfo().setFailReason(null);
			}
			
			if(isLoggingDebug()) {
				try {
					soapRequest.writeTo(System.out);
				} catch (SOAPException | IOException e) {
					e.printStackTrace();
				}
			}

			soapResponse = getResponse(pExportConfigItem, pWsdlModel, soapRequest);
			if (soapResponse == null) {
				retry.errorOccurred();
				count++;
			} else {
				retry.abort();
			}

		}

		if (soapResponse == null || isTestFailure()) {
			getPostExportItemsUpdateService().updateExportedItems(pItemsForExport, (String) pExportConfigItem.getPropertyValue(PRP_EXPORT_ITEM_DESCRIPTOR));
			getTransactionInfo().getExportedItemsIds().clear();
		}

		if (isTestFailure()) {
			trackError("Test response failure.");
		} else if (soapResponse != null) {
			if(isLoggingDebug()) {
				try {
					soapResponse.writeTo(System.out);
				} catch (SOAPException | IOException e) {
					e.printStackTrace();
				}
			}
			analyzeSOAPResponse(soapResponse, pWsdlModel, pExportConfigItem);
			getTransactionInfo().setResponse(XMLTransformer.getSOAPMessageAsString(soapResponse));
		}

	}

	/**
	 * get response by request call
	 *
	 * @param pExportConfigItem export configuration
	 * @param pWsdlModel        wsdl model
	 * @param pSoapRequest      soap request
	 * @return soap response
	 */
	private SOAPMessage getResponse(RepositoryItem pExportConfigItem, WSDLModel pWsdlModel, SOAPMessage pSoapRequest) {
		SOAPConnection connection = null;
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			connection = soapConnectionFactory.createConnection();
			if (connection instanceof SOAPConnectionImpl) {
				((SOAPConnectionImpl) connection).setTimeout(getTimeOut());
			}
			RepositoryItem outputConfig = (RepositoryItem) pExportConfigItem.getPropertyValue(PRP_EXPORT_OUTPUT_FORMAT);
			//boolean useUri = outputConfig != null && outputConfig.getPropertyValue(PRP_OFC_WSDL_URL) != null;
			String endpointUrl = null;
			if (outputConfig != null) {
				endpointUrl = (String) outputConfig.getPropertyValue(PRP_OFC_ENDPOINT_URL);
				vlogDebug("Output config endpointUrl:{0}", endpointUrl);
			}
			if (StringUtils.isBlank(endpointUrl)) {
				endpointUrl = pWsdlModel.getSOAPAddressLocation();
			}
			vlogDebug("Actual getResponse() WS URL:{0}", endpointUrl);
			return connection.call(pSoapRequest, endpointUrl);

/*
			if (useUri) {
				return connection.call(pSoapRequest, pWsdlModel.getWsdl());
			} else {
				return connection.call(pSoapRequest, pWsdlModel.getSOAPAddressLocation());
			}
*/
		} catch (Exception e) {
			trackError(e, "Unable to call ws.");
			return null;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SOAPException e) {
					vlogError(e, "Unable to close connection.");
				}
			}
		}
	}

	/**
	 * analyze and parse soap response
	 *
	 * @param pSOAPMessage      soap response
	 * @param pWsdlModel        wsdl model
	 * @param pExportConfigItem export config repository item
	 */
	private boolean analyzeSOAPResponse(SOAPMessage pSOAPMessage,
										WSDLModel pWsdlModel,
										RepositoryItem pExportConfigItem) {
		String operationName = getOperationName(pExportConfigItem);
		if (operationName == null) {
			trackError("No operation is specified.");
			return false;
		}
		Operation wsdlOperation = pWsdlModel.getOperation(operationName);

		try {
			getSOAPResponseReader().readSOAPMessage(
					getTransactionInfo(),
					pSOAPMessage,
					wsdlOperation,
					pExportConfigItem,
					getSOAPCallInfo().getItemsExported()
			);


			return true;
		} catch (Exception e) {
			trackError(e, pExportConfigItem.getRepositoryId()+": unable to read and apply response.");
			return false;
		}
	}

	/**
	 * find repository items for export
	 *
	 * @param pModel model
	 * @return array of items for export
	 * @throws atg.repository.RepositoryException if error occurs
	 */
	private RepositoryItem[] getRepositoryItems(RepositoryItem pModel) throws RepositoryException {

		String rql = (String) pModel.getPropertyValue(PRP_EXPORT_RQL);
		String repositoryName = (String) pModel.getPropertyValue(PRP_EXPORT_REPOSITORY_NAME);
		String itemDescriptor = (String) pModel.getPropertyValue(PRP_EXPORT_ITEM_DESCRIPTOR);

		Repository exportRepository = (Repository) getNucleus().resolveName(repositoryName);

		RepositoryView view = exportRepository.getView(itemDescriptor);
		RqlStatement rqlStatement = RqlStatement.parseRqlStatement(rql);

		return rqlStatement.executeQuery(view, null);

	}


	/**
	 * @param pExportConfigItem export config repository item
	 * @return operation name
	 */
	private String getOperationName(RepositoryItem pExportConfigItem) {
		try {
			RepositoryItem outputSettings = (RepositoryItem) pExportConfigItem.getPropertyValue(
					PRP_EXPORT_OUTPUT_FORMAT);
			return (String) outputSettings.getPropertyValue(PRP_OFC_WSDL_OPERATION);
		} catch (Exception e) {
			trackError(e, "Unable to get operaton from stored export configuration.");
		}
		return null;
	}

	static class RetryStrategy {

		public static final int DEFAULT_RETRIES = 3;
		public static final long DEFAULT_WAIT_TIME_IN_MILLI = 2000;

		private int mNumberOfRetries;
		private int mNumberOfTriesLeft;
		private long mTimeToWait;

		public RetryStrategy() {
			this(DEFAULT_RETRIES, DEFAULT_WAIT_TIME_IN_MILLI);
		}

		public RetryStrategy(int pNumberOfRetries, long pTimeToWait) {
			mNumberOfRetries = pNumberOfRetries;
			mNumberOfTriesLeft = mNumberOfRetries;
			mTimeToWait = pTimeToWait;
		}

		public void setNumberOfTriesLeft(int pNumberOfTriesLeft) {
			mNumberOfTriesLeft = pNumberOfTriesLeft;
		}

		/**
		 * @return true if there are tries left
		 */
		public boolean shouldRetry() {
			return mNumberOfTriesLeft > 0;
		}

		public void errorOccurred() {
			mNumberOfTriesLeft--;
			waitUntilNextTry();
		}

		public void abort() {
			setNumberOfTriesLeft(0);
		}

//		public void errorOccurred() throws Exception {
//			mNumberOfTriesLeft--;
//			if (!shouldRetry()) {
//				throw new Exception("Retry Failed: Total " + mNumberOfRetries
//						+ " attempts made at interval " + getTimeToWait()
//						+ "ms");
//			}
//			waitUntilNextTry();
//		}

		public long getTimeToWait() {
			return mTimeToWait;
		}

		private void waitUntilNextTry() {
			try {
				Thread.sleep(getTimeToWait());
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

}
