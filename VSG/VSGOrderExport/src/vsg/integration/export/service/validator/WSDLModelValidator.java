package vsg.integration.export.service.validator;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.service.util.ExportConfigWsdlPathGenerator;
import vsg.integration.export.util.FileUtil;
import vsg.integration.export.util.bean.ValidationResult;

import java.io.File;

/**
 * @author Dmitry Golubev
 */
public class WSDLModelValidator extends GenericService implements IRepositoryConstants {
	/**
	 * wsdl parser
	 */
	private WSDLParser mWSDLParser;
	/**
	 * common service
	 */
	private ExportConfigWsdlPathGenerator mExportConfigWsdlPathGenerator;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfigWsdlPathGenerator getExportConfigWsdlPathGenerator() {
		return mExportConfigWsdlPathGenerator;
	}

	public void setExportConfigWsdlPathGenerator(ExportConfigWsdlPathGenerator pExportConfigWsdlPathGenerator) {
		this.mExportConfigWsdlPathGenerator = pExportConfigWsdlPathGenerator;
	}

	public WSDLParser getWSDLParser() {
		return mWSDLParser;
	}

	public void setWSDLParser(WSDLParser pWSDLParser) {
		mWSDLParser = pWSDLParser;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * validates loaded repository item and checks if wsdl model is builded on accessible wsdl
	 *
	 * @param pExportConfigItem repository item
	 * @return builded wsdl model
	 */
	public ValidationResult validateModel(RepositoryItem pExportConfigItem) {
		ValidationResult validationResult = new ValidationResult();
		if (pExportConfigItem == null) {
			validationResult.setFailReason("Export model is not loaded.");
		} else {
			RepositoryItem outputFormatSettings = (RepositoryItem) pExportConfigItem.getPropertyValue
					(PRP_EXPORT_OUTPUT_FORMAT);
			WSDLModel wsdlModel = null;
			String wsdlUri = null;
			String wsdlUpload = null;
			if (outputFormatSettings != null) {
				wsdlUri = (String) outputFormatSettings.getPropertyValue(PRP_OFC_WSDL_URL);
				wsdlUpload = (String) outputFormatSettings.getPropertyValue(PRP_OFC_UPLOAD_WSDL);
			}
			if (!StringUtils.isBlank(wsdlUri) && !FileUtil.isFileByUriExist(wsdlUri)) {
				validationResult.setFailReason("Wsdl url is not available: " + wsdlUri);
			}
			if (!StringUtils.isBlank(wsdlUpload) && !new File(wsdlUpload).exists()) {
				validationResult.setFailReason("Local Wsdl file is not available: " + wsdlUpload);
			}
			if (validationResult.isSuccess()) {
				wsdlModel = getWSDLParser().analyze(getExportConfigWsdlPathGenerator().generateWsdlPath(
						pExportConfigItem.getRepositoryId(),
						wsdlUri,
						wsdlUpload));
			}
			if (wsdlModel == null) {
				validationResult.setFailReason("Wsdl model is not loaded.");
			}
			validationResult.setAttachment(wsdlModel);
		}
		return validationResult;
	}

}
