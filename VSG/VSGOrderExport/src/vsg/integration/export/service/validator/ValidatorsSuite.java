package vsg.integration.export.service.validator;

import atg.nucleus.GenericService;

/**
 * @author Dmitry Golubev
 */
public class ValidatorsSuite extends GenericService {
/*
	*/
/**
 * export config to wsdl model validator
 *//*

	private ExportConfigWsdlModelValidator mExportConfigWsdlModelValidator;
*/
	/**
	 * SOAPMessage validator
	 */
	private SOAPMessageValidator mSOAPMessageValidator;
	/**
	 * wsdl mapping validator
	 */
	private WSDLMappingValidator mWSDLMappingValidator;
	/**
	 * wsdl model validator
	 */
	private WSDLModelValidator mWSDLModelValidator;

	//------------------------------------------------------------------------------------------------------------------

/*
	@Deprecated
	public ExportConfigWsdlModelValidator getExportConfigWsdlModelValidator() {
		return mExportConfigWsdlModelValidator;
	}

	public void setExportConfigWsdlModelValidator(ExportConfigWsdlModelValidator pExportConfigWsdlModelValidator) {
		mExportConfigWsdlModelValidator = pExportConfigWsdlModelValidator;
	}
*/

	public SOAPMessageValidator getSOAPMessageValidator() {
		return mSOAPMessageValidator;
	}

	public void setSOAPMessageValidator(SOAPMessageValidator pSOAPMessageValidator) {
		mSOAPMessageValidator = pSOAPMessageValidator;
	}

	public WSDLMappingValidator getWSDLMappingValidator() {
		return mWSDLMappingValidator;
	}

	public void setWSDLMappingValidator(WSDLMappingValidator pWSDLMappingValidator) {
		mWSDLMappingValidator = pWSDLMappingValidator;
	}

	public WSDLModelValidator getWSDLModelValidator() {
		return mWSDLModelValidator;
	}

	public void setWSDLModelValidator(WSDLModelValidator pWSDLModelValidator) {
		mWSDLModelValidator = pWSDLModelValidator;
	}
}
