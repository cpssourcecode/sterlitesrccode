package vsg.integration.export.service.validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * @author Dmitry Golubev
 */
public class SOAPMessageErrorHandler implements ErrorHandler {

	/**
	 * array of exception codes, which not should be thrown
	 */
	private String[] mExcludeErrors = new String[]{
			"cvc-complex-type.2.4.a", // Invalid content was found starting with element 'property'
			"cvc-complex-type.2.4.b"  // The content of element '...' is not complete.
	};

	/**
	 * check if exception should be ignored
	 *
	 * @param pException saxparseexception
	 * @return true if if exception should be ignored
	 */
	private boolean isExceptionToExclude(SAXParseException pException) {
		for (String excludeError : mExcludeErrors) {
			if (pException.getMessage() != null && pException.getMessage().contains(excludeError)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void warning(SAXParseException pException) throws SAXException {
		throw pException;
	}

	@Override
	public void error(SAXParseException pException) throws SAXException {
		if (!isExceptionToExclude(pException)) {
			throw pException;
		}
	}

	@Override
	public void fatalError(SAXParseException pException) throws SAXException {
		throw pException;
	}
}
