package vsg.integration.export.service.validator;

import java.util.regex.Pattern;


/**
 * The Interface IValidatorConstants.
 *
 * @author "Vladimir Tretyakevich"
 */
public interface IValidatorConstants {

	/**
	 * The Constant MASK_SIMPLE_FILE_NAME.
	 */
	Pattern MASK_SIMPLE_FILE_NAME = Pattern.compile("[a-zA-Z0-9_\\-\\.]+");

	/**
	 * The Constant MASK_UNIX_FILE_NAME.
	 */
	Pattern MASK_UNIX_FILE_NAME = Pattern.compile("[\\w,\\s-]+\\.[A-Za-z]{0,10}");

	/**
	 * The Constant MASK_WIN_FILE_NAME.
	 */
	Pattern MASK_WIN_FILE_NAME = Pattern.compile("(?!(?:CON|PRN|AUX|NUL|COM[1-9]|LPT[1-9])(?:\\.[^.]*)?$)[^<>:\"/\\\\|?*\\x00-\\x1F]*[^<>:\"/\\\\|?*\\x00-\\x1F\\ .]",
			Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.COMMENTS);

//	^['"]?(?:\/[^\/]+)*['"]?$
//	^([a-zA-Z]:)?(\\[^<>:"/\\|?*]+)+\\?$
	/**
	 * The Constant MASK_DIRECTORY.
	 */
	Pattern MASK_DIRECTORY = Pattern.compile("[\\\\/0-9a-zA-Z\\-_ :]+");

	/**
	 * The Constant MASK_EMAIL_SUBJ.
	 */
	Pattern MASK_EMAIL_SUBJ = Pattern.compile("(?!\\s*$).+");

	/**
	 * The Constant MASK_EMAIL.
	 */
	Pattern MASK_EMAIL = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");

	/**
	 * The Constant MASK_HOST.
	 */
	Pattern MASK_HOST = Pattern.compile("(?!\\s*$).+");

	/**
	 * The Constant MASK_PORT.
	 */
	Pattern MASK_PORT = Pattern.compile("(6553[0-5]|655[0-2]\\d|65[0-4]\\d\\d|6[0-4]\\d{3}|[1-5]\\d{4}|[1-9]\\d{0,3}|0)");

	/**
	 * The Constant MASK_USER_NAME.
	 */
	Pattern MASK_USER_NAME = Pattern.compile(".+");

	/**
	 * The Constant MASK_PSWD.
	 */
	Pattern MASK_PSWD = Pattern.compile(".+");

	/**
	 * The Constant MASK_ENC_KEY.
	 */
	Pattern MASK_ENC_KEY = Pattern.compile(".+");

	/**
	 * The Constant MASK_TIMEOUT.
	 */
	Pattern MASK_TIMEOUT = Pattern.compile("[0-9]*+");
}
