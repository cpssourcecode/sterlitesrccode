package vsg.integration.export.service.validator.util;

/**
 * @author Dmitry Golubev
 */
public class PropertyMappingIteratorValue {
	/**
	 * wsdl path
	 */
	private String mWsdlPath;
	/**
	 * repository path
	 */
	private String mRepositoryPath;
	/**
	 * static value
	 */
	private String mStaticValue;
	/**
	 * static value
	 */
	private String mInout;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pRepositoryPath repository path
	 * @param pWsdlPath       wsdl path
	 * @param pStaticValue    static value
	 * @param pInout          inout value
	 */
	public PropertyMappingIteratorValue(String pRepositoryPath, String pWsdlPath, String pStaticValue, String pInout) {
		mRepositoryPath = pRepositoryPath;
		mWsdlPath = pWsdlPath;
		mStaticValue = pStaticValue;
		mInout = pInout;
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getWsdlPath() {
		return mWsdlPath;
	}

	public void setWsdlPath(String pWsdlPath) {
		mWsdlPath = pWsdlPath;
	}

	public String getRepositoryPath() {
		return mRepositoryPath;
	}

	public void setRepositoryPath(String pRepositoryPath) {
		mRepositoryPath = pRepositoryPath;
	}

	public String getStaticValue() {
		return mStaticValue;
	}

	public void setStaticValue(String pStaticValue) {
		mStaticValue = pStaticValue;
	}

	public String getInout() {
		return mInout;
	}

	public void setInout(String pInout) {
		mInout = pInout;
	}
}
