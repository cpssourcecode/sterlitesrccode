package vsg.integration.export.service.validator;

// import org.apache.axis2.saaj.SOAPBodyImpl;

import atg.nucleus.GenericService;
import vsg.integration.export.parse.wsdl.DomUtil;
import vsg.integration.export.parse.wsdl.bean.TargetNamespace;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.service.ErrorTracker;
import vsg.integration.export.util.FileUtil;
import vsg.integration.export.util.bean.ValidationResult;

import javax.xml.XMLConstants;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class SOAPMessageValidator extends GenericService {
	/**
	 * set of uri, which should be excluded by xsd validation
	 */
	private String[] mExcludedUriForXsd = new String[]{
			"http://schemas.xmlsoap.org/wsdl/mime/", // Cannot resolve the name 'wsdl:tExtensibilityElement' to a(n) 'type definition' component.
			"http://schemas.xmlsoap.org/wsdl/http/", // Cannot resolve the name 'wsdl:tExtensibilityElement' to a(n) 'type definition' component.
			"http://schemas.xmlsoap.org/wsdl/soap/", // Cannot resolve the name 'wsdl:tExtensibilityElement' to a(n) 'type definition' component.
			"http://schemas.xmlsoap.org/wsdl/soap12/", // html document
//			"http://schemas.xmlsoap.org/soap/encoding/", // html document
			"http://www.w3.org/2001/XMLSchema" // Unable to retrieve schemas from document: s4s-elt-character: Non-whitespace characters are not allowed in schema elements other than xs:appinfo and xs:documentation. Saw XML Schema.
	};

	/**
	 * @param pSOAPMessage  soap message
	 * @param pWsdlModel    wsdl model
	 * @param pErrorTracker error tracker
	 * @return validation result of soap message check according to wsdl
	 */
	public ValidationResult validateSOAPMessageToWsdl(SOAPMessage pSOAPMessage,
													  WSDLModel pWsdlModel,
													  ErrorTracker pErrorTracker) {
		try {
			Schema schema = getSchemaFromSOAPMessage(pSOAPMessage, pWsdlModel, pErrorTracker);
			// Schema schema = getSchemaFromSOAPMessageQualified(pSOAPMessage, pWsdlModel, pErrorTracker);
			if (schema == null) {
				return new ValidationResult(false, "Unable to retrieve schemas to check SOAP Message");
			}
			Validator validator = schema.newValidator();
			validator.setErrorHandler(new SOAPMessageErrorHandler());
			validator.validate(pSOAPMessage.getSOAPPart().getContent());
		} catch (Exception e) {
			pErrorTracker.trackError(e, "Unable to validate soap message. Please check log.");
			return new ValidationResult(false, e.getMessage());
		}
		return ValidationResult.successValidation();
	}

	/**
	 * retrieve schemas from document
	 *
	 * @param pWsdlModel    wsdl model
	 * @param pSOAPMessage  soap message
	 * @param pErrorTracker error tracker
	 * @return computed schema from document
	 */
	private Schema getSchemaFromSOAPMessage(SOAPMessage pSOAPMessage,
											WSDLModel pWsdlModel,
											ErrorTracker pErrorTracker) {
		try {
			List<Source> sourcesList = new LinkedList<Source>();
			sourcesList.add(getSoapSource(pSOAPMessage));
			for (TargetNamespace targetNamespace : pWsdlModel.getTargetNamespaces()) {
				analyzeTargetNamespace(targetNamespace, pWsdlModel, sourcesList);
			}
			for (TargetNamespace targetNamespace : pWsdlModel.getCommonTargetNamespaces()) {
				analyzeTargetNamespace(targetNamespace, pWsdlModel, sourcesList);
			}
			SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			return factory.newSchema(sourcesList.toArray(new Source[sourcesList.size()]));
		} catch (Exception e) {
			vlogError(e.toString());
			// pErrorTracker.trackError(e, "Unable to retrieve schemas from document: "+e.getMessage());
			// pErrorTracker.trackError("Unable to retrieve schemas from document: "+e.getMessage());
			return null;
		}
	}

	/**
	 * check for target namespace state and add to sources for schema
	 *
	 * @param pTargetNamespace current document namespace
	 * @param pWsdlModel       wsdl model
	 * @param pSourceList      list of sources for future schema for validation form
	 */
	private void analyzeTargetNamespace(TargetNamespace pTargetNamespace, WSDLModel pWsdlModel,
										List<Source> pSourceList) {
		Source source = null;
		if (pTargetNamespace.getSchemaLocation() == null) {
			if (isUriNotExcluded(pTargetNamespace.getNamespace()) &&
					FileUtil.isFileByUriExist(pTargetNamespace.getNamespace())) {
				source = new StreamSource(pTargetNamespace.getNamespace());
			}
		} else {
			if (FileUtil.isLink(pTargetNamespace.getSchemaLocation())) {
				if (FileUtil.isFileByUriExist(pTargetNamespace.getSchemaLocation())) {
					source = new StreamSource(pTargetNamespace.getSchemaLocation());
				}
			} else {
				source = new StreamSource(
						FileUtil.getFile(pWsdlModel.getWsdlDocument(), pTargetNamespace.getSchemaLocation())
				);
			}
		}
		if (source == null) {
			return;
		}
		for (Source sourceFromList : pSourceList) {
			if (sourceFromList.getSystemId().equals(source.getSystemId())) {
				return;
			}
		}
		pSourceList.add(source);
	}

	/**
	 * @param pUri uri to check
	 * @return true if uri is in exclude list
	 */
	private boolean isUriNotExcluded(String pUri) {
		if (pUri == null) {
			return true;
		}
		for (String excludeUri : mExcludedUriForXsd) {
			if (excludeUri.equals(pUri)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param pSOAPMessage soap message
	 * @return return namespace source from soap message
	 */
	private Source getSoapSource(SOAPMessage pSOAPMessage) {
		SOAPBody bodyNode = (SOAPBody) DomUtil.getNodeByName(
				pSOAPMessage.getSOAPPart().getChildNodes(), "Body"
		);
		String soapXsdUri = bodyNode.getNamespaceURI();
		return new StreamSource(soapXsdUri);

	}

/*
	private Schema getSchemaFromSOAPMessageQualified(SOAPMessage pSOAPMessage,
	                                                        WSDLModel pWsdlModel,
	                                                        ErrorTracker pErrorTracker) {
		try {
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

			SOAPBodyImpl bodyNode = (SOAPBodyImpl) DomUtil.getNodeByName(
					pSOAPMessage.getSOAPPart().getChildNodes(), "Body"
			);
			String soapXsdUri = bodyNode.getNamespaceURI();

			String W3C_XSD_TOP_ELEMENT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
			W3C_XSD_TOP_ELEMENT += "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" " +
					"elementFormDefault=\"qualified\">\n";
			W3C_XSD_TOP_ELEMENT += "<xs:import namespace=\"" + soapXsdUri + "\" schemaLocation=\"" + soapXsdUri + "\"/>\n";
			for (TargetNamespace targetNamespace : pWsdlModel.getTargetNamespaces()) {
				W3C_XSD_TOP_ELEMENT +=
						"<xs:import namespace=\""+targetNamespace.getNamespace()+"\" "+
						"schemaLocation=\"" + targetNamespace.getSchemaLocation()+"\"/>\n";
			}
			W3C_XSD_TOP_ELEMENT += "</xs:schema>";

			return schemaFactory.newSchema(new StreamSource(new StringReader(W3C_XSD_TOP_ELEMENT), "xsdTop"));
		} catch (Exception e) {
			pErrorTracker.trackError(e, "Unable to retrieve schemas from document: "+e.getMessage());
			return null;
		}

	}
*/
}
