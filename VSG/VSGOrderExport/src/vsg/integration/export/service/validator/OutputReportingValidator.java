package vsg.integration.export.service.validator;

import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.nucleus.GenericService;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.handler.BaseFormHandler;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * The Class OutputReportingValidator.
 *
 * @author "Vladimir Tretyakevich"
 */
public class OutputReportingValidator extends GenericService implements IValidatorConstants, IRepositoryConstants {

	/**
	 * Validate options.
	 *
	 * @param pValue the value
	 * @return true, if successful
	 */
	public boolean validateOptions(Map<String, String> pValue, BaseFormHandler pForm) {
		boolean result = true;
		// getIntegerValue(pValue.get(PRP_REP_CONF_SFREQ));
		// getIntegerValue(pValue.get(PRP_REP_CONF_EFREQ));
		if (!MASK_SIMPLE_FILE_NAME.matcher(pValue.get(PRP_REP_CONF_FILE_NAME)).matches()) {
			pForm.addFormException(new DropletException("Please check input file name"));
			result = false;
		}
		try {
			new SimpleDateFormat(pValue.get(PRP_REP_CONF_TIME_FORMAT));
		} catch (Exception e) {
			vlogError(e, "bad date format");
			pForm.addFormException(new DropletException("Please check input date format"));
			result = false;
		}
		result = validateDirectoryPath(pValue, pForm, PRP_REP_CONF_STORE_PATH, "Output Server Directory") && result;
		result = validateDirectoryPath(pValue, pForm, PRP_REP_CONF_ARCHIVE_PATH, "Archive Server Directory") && result;

		return result;
	}

	/**
	 * Validate directory path.
	 *
	 * @param pValue        the value
	 * @param pForm         the form
	 * @param pPropertyName the property name
	 * @return true, if successful
	 */
	private boolean validateDirectoryPath(Map<String, String> pValue, BaseFormHandler pForm, String pPropertyName,
										  String pPathName) {
		boolean result = true;
		if (!MASK_DIRECTORY.matcher(pValue.get(pPropertyName)).matches()) {
			pForm.addFormException(new DropletException("Please check '" + pPathName + "' path."));
			result = false;
		} else {
			File f = new File(pValue.get(pPropertyName));
			if (!f.exists() || !f.isDirectory()) {
				pForm.addFormException(
						new DropletException(
								"Please check '" + pPathName + "'. It is not exists or is a file path."));
				result = false;
			}
		}
		return result;
	}

	/**
	 * Validate email.
	 *
	 * @param pValue the value
	 * @return true, if successful
	 */
	public boolean validateEmail(Map<String, String> pValue, BaseFormHandler pForm) {
		boolean result = true;
		if (!MASK_EMAIL_SUBJ.matcher(pValue.get(PRP_EMAIL_CONF_SUBJ)).matches()) {
			pForm.addFormException(new DropletException("Please check input email subject"));
			result = false;
		}
/*
		if (!MASK_EMAIL.matcher(pValue.get(PRP_EMAIL_CONF_SENDER)).matches()) {
			pForm.addFormException(new DropletException("Please check input sender email address"));
			result = false;
		}
*/
		if (!validateEmails(pValue.get(PRP_EMAIL_CONF_RECIPIENTS))) {
			pForm.addFormException(new DropletException("Please check input recipients email addresses"));
			result = false;
		}
		return result;
	}

	/**
	 * Validate emails.
	 *
	 * @param pEmails the emails
	 * @return true, if successful
	 */
	private boolean validateEmails(String pEmails) {
		boolean result = false;
		if (!StringUtils.isBlank(pEmails)) {
			if (pEmails.contains(",")) {
				for (String mail : pEmails.split(",")) {
					result = MASK_EMAIL.matcher(mail).matches();
					if (!result) {
						break;
					}
				}
			} else {
				result = MASK_EMAIL.matcher(pEmails).matches();
			}
		}
		return result;
	}

	/**
	 * Validate ftp.
	 *
	 * @param pValue the value
	 * @return true, if successful
	 */
	public boolean validateFtp(Map<String, String> pValue, BaseFormHandler pForm) {
		boolean result = true;
		if (!MASK_HOST.matcher(pValue.get(PRP_FTP_CONF_HOST_NAME)).matches()) {
			pForm.addFormException(new DropletException("Please check input host name"));
			result = false;
		}
		if (!MASK_PORT.matcher(pValue.get(PRP_FTP_CONF_PORT)).matches()) {
			pForm.addFormException(new DropletException("Please check input port"));
			result = false;
		}
		if (!MASK_DIRECTORY.matcher(pValue.get(PRP_FTP_CONF_OUT_DIR_PATH)).matches()) {
			pForm.addFormException(new DropletException("Please check input server out dir path"));
			result = false;
		}
		if (!MASK_USER_NAME.matcher(pValue.get(PRP_FTP_CONF_USER_NAME)).matches()) {
			pForm.addFormException(new DropletException("Please check input user name"));
			result = false;
		}

		if (!StringUtils.isBlank(pValue.get(PRP_FTP_CONF_PASS)) && !StringUtils.isBlank(pValue.get(FTP_CONF_PASS_CONFIRM))) {
			if (!pValue.get(FTP_CONF_PASS_CONFIRM).equals(pValue.get(PRP_FTP_CONF_PASS))) {
				pForm.addFormException(new DropletException("Please check input passwords are different"));
				result = false;
			}
		} else if (!StringUtils.isBlank(pValue.get(PRP_FTP_CONF_ENC_KEY_PATH))) {
			if (!MASK_ENC_KEY.matcher(pValue.get(PRP_FTP_CONF_ENC_KEY_PATH)).matches()) {
				pForm.addFormException(new DropletException("Please check input encryption key path"));
				result = false;
			}
		} else {
			pForm.addFormException(new DropletException("Please fill password or keyfile path and passphrase (for FTPS)"));
			result = false;
		}
		return result;
	}

}
