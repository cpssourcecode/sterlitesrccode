package vsg.integration.export.service.validator.util;

import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.mapping.PropertyMappingElement;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class PropertyMappingIterator implements IRepositoryConstants {
	/**
	 * record mappings iterator
	 */
	private Iterator<RepositoryItem> mRecordsMappingsIterator;
	/**
	 * properties mapping
	 */
	private List<PropertyMappingElement> mPropertiesMappings;
	/**
	 * property mapping index
	 */
	private int mIndex = 0;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pRecordsMappings set of stored property mappings
	 */
	public PropertyMappingIterator(Set<RepositoryItem> pRecordsMappings) {
		if (pRecordsMappings != null) {
			mRecordsMappingsIterator = pRecordsMappings.iterator();
		}
	}

	/**
	 * constructor
	 *
	 * @param pPropertiesMappings list of request property mappings
	 */
	public PropertyMappingIterator(List<PropertyMappingElement> pPropertiesMappings) {
		mPropertiesMappings = pPropertiesMappings;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @return true if there is more element for iterate
	 */
	public boolean hasNext() {
		if (mRecordsMappingsIterator != null) {
			return mRecordsMappingsIterator.hasNext();
		} else if (mPropertiesMappings != null) {
			return mIndex < mPropertiesMappings.size();
		}
		return false;
	}

	/**
	 * @return wsdl path from collection of initial data
	 */
	public PropertyMappingIteratorValue next() {
		if (mRecordsMappingsIterator != null) {
			RepositoryItem recordMapping = mRecordsMappingsIterator.next();
			return new PropertyMappingIteratorValue(
					(String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH),
					(String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH),
					(String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_STATIC_VALUE),
					(String) recordMapping.getPropertyValue(PRP_RECORD_MAPPING_INOUT)
			);

		} else if (mPropertiesMappings != null) {
			PropertyMappingElement element = mPropertiesMappings.get(mIndex);
			PropertyMappingIteratorValue result = new PropertyMappingIteratorValue(
					element.getRepositoryPath(),
					element.getWsdlPath(),
					element.getStaticValue(),
					element.getInout()
			);
			mIndex++;
			return result;
		}
		return null;
	}
}
