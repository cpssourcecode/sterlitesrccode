package vsg.integration.export.service.validator;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.util.bean.ValidationResult;

import java.util.Set;

/**
 * @author Dmitry Golubev
 */
@Deprecated
public class ExportConfigWsdlModelValidator extends GenericService implements IRepositoryConstants {

	/**
	 * wsdl mapping validator
	 */
	private WSDLMappingValidator mWSDLMappingValidator;

	//------------------------------------------------------------------------------------------------------------------

	public WSDLMappingValidator getWSDLMappingValidator() {
		return mWSDLMappingValidator;
	}

	public void setWSDLMappingValidator(WSDLMappingValidator pWSDLMappingValidator) {
		mWSDLMappingValidator = pWSDLMappingValidator;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * validate stored model info and current wsdl model
	 *
	 * @param pRepositoryConfig repository item for model
	 * @param pWsdlModel        wsdl model
	 * @return true if conformity is correct
	 */
	//public ValidationResult validateModelConformity(RepositoryItem pRepositoryConfig, WSDLModel pWsdlModel, WSDLParser pWsdlParser) {
	private ValidationResult validateModelConformity(RepositoryItem pRepositoryConfig, WSDLModel pWsdlModel,
													 WSDLParser pWsdlParser) {
		ValidationResult currentResult = new ValidationResult();
		if (validateModelOperation(pRepositoryConfig, pWsdlModel, currentResult)) {
			validateModelPropertyMappings(pRepositoryConfig, pWsdlModel, currentResult, pWsdlParser);
		}
		return currentResult;
	}

	/**
	 * validate operation wsdl-repository
	 *
	 * @param pRepositoryConfig repository item for model
	 * @param pModel            wsdl model
	 * @param pCurrentResult    validation result
	 * @return true if conformity is correct
	 */
	private boolean validateModelOperation(RepositoryItem pRepositoryConfig, WSDLModel pModel,
										   ValidationResult pCurrentResult) {
		RepositoryItem outputSettings = (RepositoryItem) pRepositoryConfig.getPropertyValue(
				PRP_EXPORT_OUTPUT_FORMAT);
		if (outputSettings == null) {
			pCurrentResult.setSuccess(false);
			pCurrentResult.setFailReason("Output settings are not initialized for export config: '" + pModel.getWsdl() + "'");
			return false;
		}
		String wsdlOperation = (String) outputSettings.getPropertyValue(PRP_OFC_WSDL_OPERATION);
		String wsdlOperationAction = (String) outputSettings.getPropertyValue(PRP_OFC_WSDL_OPERATION_ACTION);

		Operation modelOperation = pModel.getOperation(wsdlOperation);
		if (modelOperation == null) {
			pCurrentResult.setSuccess(false);
			pCurrentResult.setFailReason("Unable to retrive operation '" + wsdlOperation + "' by wsdl '" + pModel.getWsdl() + "'");
			return false;
		}
		if (!StringUtils.isBlank(wsdlOperationAction) && !StringUtils.isBlank(modelOperation.getAction()) &&
				!wsdlOperationAction.equals(modelOperation.getAction())) {
			pCurrentResult.setSuccess(false);
			pCurrentResult.setFailReason("Operation '" + wsdlOperation + "' action '" +
					modelOperation.getAction() + "' is not as defined in wsdl '" + pModel.getWsdl() + "'");
			return false;
		}
		return true;
	}


	/**
	 * validate stored model info and current wsdl model
	 *
	 * @param pRepositoryConfig repository item for model
	 * @param pWsdlModel        wsdl model
	 * @param pCurrentResult    validation result
	 * @return true if conformity is correct
	 */
	private boolean validateModelPropertyMappings(RepositoryItem pRepositoryConfig, WSDLModel pWsdlModel,
												  ValidationResult pCurrentResult, WSDLParser pWsdlParser) {
		Set<RepositoryItem> repositoryMappings = (Set<RepositoryItem>) pRepositoryConfig.getPropertyValue(PRP_EXPORT_RECORD_MAPPINGS);

		for (RepositoryItem repositoryMapping : repositoryMappings) {
			String wsdlPath = (String) repositoryMapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
			if (StringUtils.isBlank(wsdlPath)) {
				pCurrentResult.setSuccess(false);
				pCurrentResult.setFailReason("Repository property mapping '" + repositoryMapping.getRepositoryId() + "' has no wsdl path. ");
				return false;
			}
			String repositoryPath = (String) repositoryMapping.getPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH);
			if (StringUtils.isBlank(repositoryPath)) {
				pCurrentResult.setSuccess(false);
				pCurrentResult.setFailReason("Repository property mapping '" + repositoryMapping.getRepositoryId() +
						"' has no repository path. ");
				return false;
			}
			if (!isWsdlPathValid(wsdlPath, pWsdlModel, pCurrentResult)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * validate that specified wsdl path is correct according to wsdl model
	 *
	 * @param pWsdlPath      wsdl path of repository property mapping
	 * @param pWsdlModel     wsdl model
	 * @param pCurrentResult validation result
	 * @return true if OK
	 */
	private boolean isWsdlPathValid(String pWsdlPath, WSDLModel pWsdlModel, ValidationResult pCurrentResult) {
		String[] wsdlPathArgs = pWsdlPath.split("\\.");
		Operation operation = pWsdlModel.getOperation(wsdlPathArgs[0]);
		if (operation == null) {
			pCurrentResult.setSuccess(false);
			pCurrentResult.setFailReason("No operation '" + wsdlPathArgs[0] + "' in wsdl according to repositoy mapping");
			return false;
		} else {
			if (operation.getMessageInput() == null || operation.getMessageInput().getType() == null) {
				pCurrentResult.setSuccess(false);
				pCurrentResult.setFailReason("No operation input message is specified for operation '" + wsdlPathArgs[0] + "' in wsdl");
				return false;
			} else {
				if (!isWsdlPathValidWithType(wsdlPathArgs, operation.getMessageInput().getType(), 1)) {
					pCurrentResult.setSuccess(false);
					pCurrentResult.setFailReason("Specified wsdl path '" + pWsdlPath + "' does not match operation '" +
							operation.getName() + "' type tree from in wsdl");
					return false;
				} else {
					return true;
				}
			}
		}
	}

	/**
	 * @param pWsdlPathArgs splitted wsdl path
	 * @param pWsdlType     current wsdl type
	 * @param pIndex        index of wsdl path (level of types depth)
	 * @return true if all wsdl path compares to type from wsdl model
	 */
	private boolean isWsdlPathValidWithType(String[] pWsdlPathArgs, Type pWsdlType, int pIndex) {
		if (pWsdlType == null) {
			return false;
		}
		if (pIndex == 1) { // check for first type
			if (!pWsdlType.getName().equals(pWsdlPathArgs[pIndex])) {
				return false;
			}
		}
		if (pWsdlPathArgs.length - 1 == pIndex) { // last element
			return true;
		} else {
			String subChildTypeName = pWsdlPathArgs[pIndex + 1];
			return isWsdlPathValidWithType(pWsdlPathArgs, pWsdlType.getSubType(subChildTypeName), pIndex + 1);
		}
	}
}
