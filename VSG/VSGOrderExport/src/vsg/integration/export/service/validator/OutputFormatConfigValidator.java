package vsg.integration.export.service.validator;

import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.nucleus.GenericService;
import atg.nucleus.PropertyValueParseException;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.SchedulePropertyValueParser;
import vsg.integration.export.ExportConfig;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.handler.BaseFormHandler;
import vsg.integration.export.util.FileUtil;

import java.util.Map;

/**
 * service to help configure Order Output Format
 */
public class OutputFormatConfigValidator extends GenericService implements IRepositoryConstants, IValidatorConstants {

	/**
	 * @param pScheduleString schedule string
	 * @return true if schedule string is valid
	 */
	public boolean isValidSchedule(String pScheduleString) {
		if (StringUtils.isEmpty(pScheduleString)) { // no schedule - right schedule
			return true;
		}
		SchedulePropertyValueParser mParser = new SchedulePropertyValueParser();
		boolean flag = true;
		try {
			mParser.parsePropertyValue(pScheduleString, Schedule.class);
		} catch (PropertyValueParseException e1) {
			flag = false;
		}
		return flag;
	}

	public boolean isValidExportConfig(GenericFormHandler pHandler, ExportConfig pExportConfig) {
		boolean flag = true;
		if (pExportConfig == null || StringUtils.isBlank(pExportConfig.getRepositoryId())) {
			pHandler.addFormException(new DropletException("Validation of Export Config failed"));
			flag = false;
		}
		return flag;
	}

	/**
	 * validate export config
	 *
	 * @param pHandler      - handler
	 * @param pExportConfig - export config
	 * @param pValue        - map of values to validate
	 * @return true if values are valid
	 */
	public boolean isValidOututFormatConfig(BaseFormHandler pHandler, Map<String, String> pValue) {
		boolean result = true;


		if (!isValidSchedule(pValue.get(PRP_OFC_SCHEDULE))) {
			pHandler.addFormException(new DropletException("Output Format Config schedule is incorrect"));
			result = false;
		}
		if (!MASK_TIMEOUT.matcher(pValue.get(PRP_OFC_CONNECTION_TIMEOUT)).matches()) {
			pHandler.addFormException(new DropletException("Output Format Config co nnection timeout is incorrect"));
			result = false;
		}

		if (StringUtils.isBlank(pValue.get(PRP_OFC_WSDL_URL)) && StringUtils.isBlank(pValue.get(PRP_OFC_UPLOAD_WSDL))) {
			pHandler.addFormException(new DropletException("Please, upload wsdl or provide wsdl URL"));
			result = false;
		} else if (!StringUtils.isBlank(pValue.get(PRP_OFC_WSDL_URL)) && !FileUtil.isFileByUriExist(pValue.get(PRP_OFC_WSDL_URL))) {
			pHandler.addFormException(new DropletException("Cannot access file by provided URL"));
			result = false;
		}

		return result;
	}

	public boolean isValidTransferOptionsConfig(BaseFormHandler pHandler, Map<String, String> pValue) {
		boolean result = true;

		if (!isValidSchedule(pValue.get(PRP_TOC_SCHEDULE_TRANSFER))) {
			pHandler.addFormException(new DropletException("Transfer Options Config schedule is incorrect"));
			result = false;
		}

		if (StringUtils.isBlank(pValue.get(PRP_TOC_PSWD)) || StringUtils.isBlank(pValue.get(PRP_TOC_CONFIRM_PSWD)) ||
				!pValue.get(PRP_TOC_PSWD).equals(pValue.get(PRP_TOC_CONFIRM_PSWD))) {
			pHandler.addFormException(new DropletException("Please check passwords you enter"));
			result = false;
		}

		return result;
	}

}
