package vsg.integration.export.service.validator;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import org.apache.commons.lang3.StringUtils;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.mapping.PropertyMappingInfo;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.parse.wsdl.bean.Message;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.service.util.ExportConfigWsdlPathGenerator;
import vsg.integration.export.service.validator.util.PropertyMappingIterator;
import vsg.integration.export.service.validator.util.PropertyMappingIteratorValue;
import vsg.integration.export.util.bean.ValidationResult;

import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class WSDLMappingValidator extends GenericService implements IRepositoryConstants {
	/**
	 * wsdl parser
	 */
	private WSDLParser mWSDLParser;
	/**
	 * common service
	 */
	private ExportConfigWsdlPathGenerator mExportConfigWsdlPathGenerator;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfigWsdlPathGenerator getExportConfigWsdlPathGenerator() {
		return mExportConfigWsdlPathGenerator;
	}

	public void setExportConfigWsdlPathGenerator(ExportConfigWsdlPathGenerator pExportConfigWsdlPathGenerator) {
		this.mExportConfigWsdlPathGenerator = pExportConfigWsdlPathGenerator;
	}

	public WSDLParser getWSDLParser() {
		return mWSDLParser;
	}

	public void setWSDLParser(WSDLParser pWSDLParser) {
		mWSDLParser = pWSDLParser;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * validate stoted export config property mapping according to wsdl model
	 *
	 * @param pExportConfigItem export configuration repository item
	 * @return validation result of property mapping check
	 */
	public ValidationResult isMappingValid(RepositoryItem pExportConfigItem) {
		RepositoryItem outputFormat = (RepositoryItem) pExportConfigItem.getPropertyValue(PRP_EXPORT_OUTPUT_FORMAT);
		if (outputFormat == null) {
			return ValidationResult.errorValidation("Output format setting is not specified");
		}
		String wsdlPath = getExportConfigWsdlPathGenerator().generateWsdlPath(
				pExportConfigItem.getRepositoryId(),
				(String) outputFormat.getPropertyValue(PRP_OFC_WSDL_URL),
				(String) outputFormat.getPropertyValue(PRP_OFC_UPLOAD_WSDL)
		);
		String wsdlOperation = (String) outputFormat.getPropertyValue(PRP_OFC_WSDL_OPERATION);
		if (StringUtils.isBlank(wsdlPath) || StringUtils.isBlank(wsdlOperation)) {
			return ValidationResult.errorValidation(
					pExportConfigItem.getRepositoryId()+": ws uri and operation are specified with errors."
			);
		}

		WSDLModel wsdlModel = getWSDLParser().analyze(wsdlPath);
		Set<RepositoryItem> recordsMappings = (Set<RepositoryItem>) pExportConfigItem.getPropertyValue
				(PRP_EXPORT_RECORD_MAPPINGS);
		return checkRecordMappings(wsdlModel.getOperation(wsdlOperation), new PropertyMappingIterator(recordsMappings));
	}

	/**
	 * validate property mapping according to wsdl model
	 *
	 * @param pPropertyMappingInfo property mapping info
	 * @return validation result of property mapping check
	 */
	public ValidationResult isMappingValid(PropertyMappingInfo pPropertyMappingInfo) {

		String wsdlUri = pPropertyMappingInfo.getWsdlUrl();
		String wsdlOperation = pPropertyMappingInfo.getWsdlOperation();
		if (StringUtils.isBlank(wsdlUri) || StringUtils.isBlank(wsdlOperation)) {
			return ValidationResult.errorValidation("WS uri and operation are specified with errors.");
		}

		WSDLModel wsdlModel = getWSDLParser().analyze(wsdlUri);
		return checkRecordMappings(wsdlModel.getOperation(wsdlOperation),
				new PropertyMappingIterator(pPropertyMappingInfo.getPropertiesMapping()));
	}

	/**
	 * @param pOperation               wsdl operation
	 * @param pPropertyMappingIterator property mappings
	 * @return validation result of stored mapping check
	 */
	private ValidationResult checkRecordMappings(Operation pOperation, PropertyMappingIterator pPropertyMappingIterator) {
		boolean isInputMappingRequired = isInputMappingRequired(pOperation);
		boolean isOutputMappingRequired = isOutputMappingRequired(pOperation);

		if ((!isInputMappingRequired && !isOutputMappingRequired) && pPropertyMappingIterator.hasNext()) {
			return ValidationResult.errorValidation("Mapping is not requeired. But defined.");
		}
		if ((isInputMappingRequired || isOutputMappingRequired) && !pPropertyMappingIterator.hasNext()) {
			return ValidationResult.errorValidation("No mapping is defined for selected operation.");
		}

		String inputPrefix = isInputMappingRequired && pOperation.getMessageInput() != null && pOperation.getMessageInput().getType() != null
				? pOperation.getName() + "." + pOperation.getMessageInput().getType().getName() : null;
		String outputPrefix = isOutputMappingRequired && pOperation.getMessageOutput() != null && pOperation.getMessageOutput().getType() != null
				? pOperation.getName() + "." + pOperation.getMessageOutput().getType().getName() : null;

		while (pPropertyMappingIterator.hasNext()) {
			ValidationResult result = checkRecordMapping(pPropertyMappingIterator.next(), inputPrefix, outputPrefix);
			if (result != null) {
				return result;
			}
		}
		return ValidationResult.successValidation();
	}

	/**
	 * @param pIteratorValue iterator
	 * @param pInputPrefix   input type prefix
	 * @param pOutputPrefix  output type prefix
	 * @return validation result on error, otherwise null
	 */
	private ValidationResult checkRecordMapping(PropertyMappingIteratorValue pIteratorValue,
												String pInputPrefix, String pOutputPrefix) {
		if (StringUtils.isBlank(pIteratorValue.getWsdlPath())) {
			return ValidationResult.errorValidation("Stored empty wsdl path in export config property " +
					"mapping.");
		}
		if (StringUtils.isBlank(pIteratorValue.getRepositoryPath()) &&
				StringUtils.isBlank(pIteratorValue.getStaticValue())) {
			return ValidationResult.errorValidation("Stored empty repository path in export config property " +
					"mapping.");
		}
		if (!isWsdlPathMappedCorrect(pIteratorValue.getWsdlPath(), pInputPrefix, pOutputPrefix)) {
			return ValidationResult.errorValidation("Other operation mapping: " + pIteratorValue.getWsdlPath());
		}
		return null;
	}

	/**
	 * @param pWsdlPath         wsdl path
	 * @param pInputTypePrefix  input type prefix
	 * @param pOutputTypePrefix output type prefix
	 * @return true if wsdl path starts with input type of output type prefix
	 */
	private boolean isWsdlPathMappedCorrect(String pWsdlPath, String pInputTypePrefix, String pOutputTypePrefix) {
		return
				(pInputTypePrefix != null && pWsdlPath.startsWith(pInputTypePrefix)) ||
						(pOutputTypePrefix != null && pWsdlPath.startsWith(pOutputTypePrefix));
	}

	/**
	 * check if mapping for of input message is required
	 *
	 * @param pOperation - operation
	 * @return true if parameter is required
	 */
	public boolean isInputMappingRequired(Operation pOperation) {
		boolean flag = true;
		if (pOperation != null) {
			Message messageInput = pOperation.getMessageInput();
			if (messageInput != null) {
				return typeRequiresRecordMapping(messageInput.getType());
			}
		}
		return flag;
	}

	/**
	 * check if mapping for of input message is required
	 *
	 * @param pOperation - operation
	 * @return true if parameter is required
	 */
	public boolean isOutputMappingRequired(Operation pOperation) {
		boolean flag = true;
		if (pOperation != null) {
			Message messageOutput = pOperation.getMessageOutput();
			if (messageOutput != null) {
				return typeRequiresRecordMapping(messageOutput.getType());
			}
		}
		return flag;
	}

	/**
	 * @param type wsdl type
	 * @return true if type requeires mapping
	 */
	private boolean typeRequiresRecordMapping(Type type) {
		if (type == null || (type.getType().equals(NodeType.COMPLEX) && type.getSubTypes().isEmpty())) {
			return false;
		}
		return true;
	}


}
