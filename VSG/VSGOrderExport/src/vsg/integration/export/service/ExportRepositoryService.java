package vsg.integration.export.service;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import vsg.integration.export.ExportConfig;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.mapping.PropertyMappingElement;
import vsg.integration.export.mapping.PropertyMappingInfo;
import vsg.integration.export.repository.SupportedRepositories;
import vsg.integration.export.util.bean.TransactionInfo;

import javax.transaction.Status;
import javax.transaction.TransactionManager;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class ExportRepositoryService extends GenericService implements IRepositoryConstants {
	/**
	 * export models repository
	 */
	private Repository mExportRepository;
	/**
	 * supported repositories
	 */
	private SupportedRepositories mSupportedRepositories;
	/**
	 * transaction manager
	 */
	private TransactionManager mTransactionManager;

	//------------------------------------------------------------------------------------------------------------------

	public Repository getExportRepository() {
		return mExportRepository;
	}

	public void setExportRepository(Repository pExportRepository) {
		mExportRepository = pExportRepository;
	}

	public SupportedRepositories getSupportedRepositories() {
		return mSupportedRepositories;
	}

	public void setSupportedRepositories(SupportedRepositories pSupportedRepositories) {
		mSupportedRepositories = pSupportedRepositories;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * creates repository item
	 *
	 * @throws RepositoryException if creation fails
	 */
	public String createExportItem() throws RepositoryException {
		MutableRepositoryItem export = ((MutableRepository) getExportRepository()).createItem(ID_EXPORT_CONFIG);
		Date date = new java.util.Date();
		export.setPropertyValue(PRP_EXPORT_DATE_CREATE, new Timestamp(date.getTime()));
		String repositoryId = export.getRepositoryId();
		((MutableRepository) getExportRepository()).addItem(export);
		return repositoryId;
	}

	/**
	 * deletes repository item
	 *
	 * @param pId - config id
	 * @throws RepositoryException if creation fails
	 */
	public void deleteExportItem(String pId) throws RepositoryException {
		TransactionDemarcation td = new TransactionDemarcation();
		boolean isFailed = false;
		try {
			td.begin(getTransactionManager());
			RepositoryItem exportConfiguration = getExportRepository().getItem(pId, ID_EXPORT_CONFIG);
			RepositoryItem outputFormat = (RepositoryItem) exportConfiguration.getPropertyValue(PRP_EXPORT_OUTPUT_FORMAT);
			RepositoryItem reportConfig = (RepositoryItem) exportConfiguration.getPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG);
			RepositoryItem transferOptions = (RepositoryItem) exportConfiguration.getPropertyValue(PRP_EXPORT_TRANSFER_OPTIONS);

			if (outputFormat != null) {
				((MutableRepository) getExportRepository()).removeItem(outputFormat.getRepositoryId(), ID_OUTPUT_FORMAT_CONFIG);
			}
			if (reportConfig != null) {
				RepositoryItem emailConfig = (RepositoryItem) reportConfig.getPropertyValue(PRP_REP_CONF_MAIL_CONF);
				RepositoryItem ftpConfig = (RepositoryItem) reportConfig.getPropertyValue(PRP_REP_CONF_FTP_CONF);

				if (emailConfig != null) {
					((MutableRepository) getExportRepository()).removeItem(emailConfig.getRepositoryId(), ID_EMAIL_DELIVERY_CONFIG);
				}
				if (ftpConfig != null) {
					((MutableRepository) getExportRepository()).removeItem(ftpConfig.getRepositoryId(), ID_FTP_DELIVERY_CONFIG);
				}
				((MutableRepository) getExportRepository()).removeItem(reportConfig.getRepositoryId(), ID_OUT_REPORT_CONFIG);
			}
			if (transferOptions != null) {
				((MutableRepository) getExportRepository()).removeItem(transferOptions.getRepositoryId(), ID_TRANSFER_OPTIONS_CONFIG);
			}

			((MutableRepository) getExportRepository()).removeItem(pId, ID_EXPORT_CONFIG);
		} catch (Exception e) {
			isFailed = true;
			throw new IllegalStateException(e);
		} finally {
			transactionEnd(td, isFailed);
		}
	}


	/**
	 * @param pDemarcation transaction demacration
	 * @param pIsfailed    is transaction fialed
	 */
	private void transactionEnd(TransactionDemarcation pDemarcation, boolean pIsfailed) {
		try {
			if (pDemarcation.getTransaction() != null) {
				int status = pDemarcation.getTransaction().getStatus();
				if (status == Status.STATUS_ACTIVE) {
					pDemarcation.end(pIsfailed);
				} else {
					pDemarcation.end(true);
				}
			}
		} catch (Exception e) {
			vlogDebug(e, "Error in transaction");
		}
	}

	/**
	 * queries export configs from repository
	 *
	 * @param pQuery  query
	 * @param pParams params for query
	 * @return array of repository items
	 */
	public RepositoryItem[] queryExportItems(String pQuery, Object[] pParams) {
		RepositoryItem[] items = null;
		try {
			RepositoryView view = getExportRepository().getView(ID_EXPORT_CONFIG);
			RqlStatement statement = RqlStatement.parseRqlStatement(pQuery);
			items = statement.executeQuery(view, pParams);
		} catch (RepositoryException e) {
			vlogDebug(e, "Repository exception during quering items");
		}
		return items;
	}

	/**
	 * updates repository item initial settings
	 *
	 * @param pExportConfig - export configuration
	 * @throws RepositoryException if update fails
	 */
	public void storeInitialConfigurationSettings(ExportConfig pExportConfig, Map<String,
			String> pValue) throws RepositoryException {
		MutableRepositoryItem export = ((MutableRepository) getExportRepository()).getItemForUpdate(
				pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG
		);

		export.setPropertyValue(PRP_EXPORT_NAME, pValue.get(PRP_EXPORT_NAME));
		export.setPropertyValue(PRP_EXPORT_DESCRIPTION, pValue.get(PRP_EXPORT_DESCRIPTION));
		export.setPropertyValue(PRP_EXPORT_AUTHOR, pValue.get(PRP_EXPORT_AUTHOR));

		((MutableRepository) getExportRepository()).updateItem(export);
	}

	/**
	 * updates repository item
	 *
	 * @param pExportConfig - model
	 * @throws RepositoryException if creation fails
	 */
	public void storeOutputFormatSettings(ExportConfig pExportConfig, Map<String, String> pValue) throws RepositoryException {
		MutableRepositoryItem export = ((MutableRepository) getExportRepository()).getItemForUpdate(pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG);
		MutableRepositoryItem outputFormatConfig = (MutableRepositoryItem) export.getPropertyValue(ID_OUTPUT_FORMAT_CONFIG);
		boolean isCreated = false;
		if (outputFormatConfig == null) {
			outputFormatConfig = ((MutableRepository) getExportRepository()).createItem(ID_OUTPUT_FORMAT_CONFIG);
			isCreated = true;
		}
		outputFormatConfig.setPropertyValue(PRP_OFC_WS_SEQURITY, pValue.get(PRP_OFC_WS_SEQURITY));
		outputFormatConfig.setPropertyValue(PRP_OFC_SCHEDULE, pValue.get(PRP_OFC_SCHEDULE));
		outputFormatConfig.setPropertyValue(PRP_OFC_USE_SCHEDULE, Boolean.parseBoolean(pValue.get(PRP_OFC_USE_SCHEDULE)));
		outputFormatConfig.setPropertyValue(PRP_OFC_ENDPOINT_URL, pValue.get(PRP_OFC_ENDPOINT_URL));
		outputFormatConfig.setPropertyValue(PRP_OFC_CONNECTION_TIMEOUT, pValue.get(PRP_OFC_CONNECTION_TIMEOUT));
		outputFormatConfig.setPropertyValue(PRP_OFC_WSDL_URL, pValue.get(PRP_OFC_WSDL_URL));
		outputFormatConfig.setPropertyValue(PRP_OFC_UPLOAD_WSDL, pValue.get(PRP_OFC_UPLOAD_WSDL));
		if (isCreated) {
			((MutableRepository) getExportRepository()).addItem(outputFormatConfig);
		} else {
			((MutableRepository) getExportRepository()).updateItem(outputFormatConfig);
		}
		export.setPropertyValue(ID_OUTPUT_FORMAT_CONFIG, outputFormatConfig);

		((MutableRepository) getExportRepository()).updateItem(export);
	}

	/**
	 * stores property mapping settings to db
	 *
	 * @param pExportConfigId export config id
	 * @param pRql            rql
	 * @param pItemDescriptor item descriptor name
	 * @param pRepository     repository
	 * @return true if saved, false - otherwise
	 */
	public boolean storeQuerySettings(String pExportConfigId,
									  String pRql,
									  String pItemDescriptor,
									  boolean pMarkItemsAsExported,
									  Repository
											  pRepository) {
		try {
			MutableRepositoryItem repositoryItemExport = (MutableRepositoryItem)
					getExportRepository().getItem(pExportConfigId, ID_EXPORT_CONFIG);
			repositoryItemExport.setPropertyValue(PRP_EXPORT_RQL, pRql);
			repositoryItemExport.setPropertyValue(PRP_EXPORT_REPOSITORY_NAME,
					((GSARepository) pRepository).getAbsoluteName());
			repositoryItemExport.setPropertyValue(PRP_EXPORT_MARK_AS_EXPORTED, pMarkItemsAsExported);
			repositoryItemExport.setPropertyValue(PRP_EXPORT_ITEM_DESCRIPTOR, pItemDescriptor);
			((MutableRepository) getExportRepository()).updateItem(repositoryItemExport);
		} catch (Exception e) {
			vlogError(e, "Unable to store query settings.");
			return false;
		}
		return true;
	}

	/**
	 * stores property mapping settings to db
	 *
	 * @param pExportConfig        export model
	 * @param pPropertyMappingInfo property mapping info
	 * @param pWsdlOperation       wsdl operation
	 * @param pWsdlOperationAction wsdl operation action
	 * @return true if saved, false - otherwise
	 */
	public boolean storePropertyMappingSettings(ExportConfig pExportConfig, PropertyMappingInfo pPropertyMappingInfo,
												String pWsdlOperation, String pWsdlOperationAction, String pMandatoryField) {
		try {
			MutableRepositoryItem exportConfigItem = (MutableRepositoryItem)
					getExportRepository().getItem(pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG);

			Set<RepositoryItem> recordMappings = new HashSet<RepositoryItem>();
			if (pPropertyMappingInfo != null && pPropertyMappingInfo.getPropertiesMapping() != null) {
				for (PropertyMappingElement element : pPropertyMappingInfo.getPropertiesMapping()) {
					recordMappings.add(createPropertyMappingItem(element, exportConfigItem));
				}
			}
			exportConfigItem.setPropertyValue(PRP_EXPORT_RECORD_MAPPINGS, recordMappings);

			if (StringUtils.isBlank(pMandatoryField)) {
				exportConfigItem.setPropertyValue(PRP_EXPORT_MANDATORY_FIELDS, null);
			} else {
				Set<String> fields = (Set<String>)exportConfigItem.getPropertyValue(PRP_EXPORT_MANDATORY_FIELDS);
				fields.clear();
				fields.add(pMandatoryField);
			}

			((MutableRepository) getExportRepository()).updateItem(exportConfigItem);

			MutableRepositoryItem outputFormatSettings = (MutableRepositoryItem) exportConfigItem.getPropertyValue
					(PRP_EXPORT_OUTPUT_FORMAT);
			outputFormatSettings.setPropertyValue(PRP_OFC_WSDL_OPERATION, pWsdlOperation);
			outputFormatSettings.setPropertyValue(PRP_OFC_WSDL_OPERATION_ACTION, pWsdlOperationAction);
			((MutableRepository) getExportRepository()).updateItem(outputFormatSettings);

		} catch (Exception e) {
			vlogError(e, "Unable to store export property mapping.");
			return false;
		}

		return true;
	}

	/**
	 * check if transfer options is not used - delete it
	 *
	 * @param pExportConfig export config to get id
	 * @param pValue        param map
	 * @throws RepositoryException if error occurres
	 */
	public void checkTransferOptionsUse(ExportConfig pExportConfig, Map<String, String> pValue)
			throws RepositoryException {
		if ("false".equals(pValue.get("useXmlTransfer"))) {
			MutableRepositoryItem export = ((MutableRepository) getExportRepository()).getItemForUpdate(
					pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG);
			export.setPropertyValue(PRP_EXPORT_TRANSFER_OPTIONS, null);
			((MutableRepository) getExportRepository()).updateItem(export);
		}
	}

	/**
	 * updates transfer options repository item
	 *
	 * @param pExportConfig - model
	 * @throws RepositoryException if creation fails
	 */
	public void storeTransferOptions(ExportConfig pExportConfig, Map<String, String> pValue) throws RepositoryException {
		MutableRepositoryItem export = ((MutableRepository) getExportRepository()).getItemForUpdate(pExportConfig.getRepositoryId(), ID_EXPORT_CONFIG);
		MutableRepositoryItem transferOptionsConfig = (MutableRepositoryItem) export.getPropertyValue(ID_TRANSFER_OPTIONS_CONFIG);
		boolean isCreated = false;
		if (transferOptionsConfig == null) {
			transferOptionsConfig = ((MutableRepository) getExportRepository()).createItem(ID_TRANSFER_OPTIONS_CONFIG);
			isCreated = true;
		}
		transferOptionsConfig.setPropertyValue(PRP_TOC_CONFIRM_PSWD, pValue.get(PRP_TOC_CONFIRM_PSWD));
		transferOptionsConfig.setPropertyValue(PRP_TOC_CONTROL_FILE, pValue.get(PRP_TOC_CONTROL_FILE));
		transferOptionsConfig.setPropertyValue(PRP_TOC_ENCRIPTION_KEY, pValue.get(PRP_TOC_ENCRIPTION_KEY));
		transferOptionsConfig.setPropertyValue(PRP_TOC_ENCRIPTION_PHRASE, pValue.get(PRP_TOC_ENCRIPTION_PHRASE));
		transferOptionsConfig.setPropertyValue(PRP_TOC_FILE_NAME, pValue.get(PRP_TOC_FILE_NAME));
		transferOptionsConfig.setPropertyValue(PRP_TOC_KEY, pValue.get(PRP_TOC_KEY));
		transferOptionsConfig.setPropertyValue(PRP_TOC_OUTPUT_DIRECTORY, pValue.get(PRP_TOC_OUTPUT_DIRECTORY));
		transferOptionsConfig.setPropertyValue(PRP_TOC_PSWD, pValue.get(PRP_TOC_PSWD));
		transferOptionsConfig.setPropertyValue(PRP_TOC_PREFIX, pValue.get(PRP_TOC_PREFIX));
		transferOptionsConfig.setPropertyValue(PRP_TOC_PROTOCOL, pValue.get(PRP_TOC_PROTOCOL));
		transferOptionsConfig.setPropertyValue(PRP_TOC_SCHEDULE, pValue.get(PRP_TOC_SCHEDULE_TRANSFER));
		transferOptionsConfig.setPropertyValue(PRP_TOC_SINGLE_PER_FILE, pValue.get(PRP_TOC_SINGLE_PER_FILE));
		transferOptionsConfig.setPropertyValue(PRP_TOC_SUFIX, pValue.get(PRP_TOC_SUFIX));
		transferOptionsConfig.setPropertyValue(PRP_TOC_TIME_STEP_FORMAT, pValue.get(PRP_TOC_TIME_STEP_FORMAT));
		transferOptionsConfig.setPropertyValue(PRP_TOC_UPLOAD_XSD, pValue.get(PRP_TOC_UPLOAD_XSD));
		transferOptionsConfig.setPropertyValue(PRP_TOC_URL_IP, pValue.get(PRP_TOC_URL_IP));
		transferOptionsConfig.setPropertyValue(PRP_TOC_USER_NAME, pValue.get(PRP_TOC_USER_NAME));
		if (isCreated) {
			((MutableRepository) getExportRepository()).addItem(transferOptionsConfig);
		} else {
			((MutableRepository) getExportRepository()).updateItem(transferOptionsConfig);
		}
		export.setPropertyValue(ID_TRANSFER_OPTIONS_CONFIG, transferOptionsConfig);
		((MutableRepository) getExportRepository()).updateItem(export);
	}

	/**
	 * create RepositoryItem for property mapping
	 *
	 * @param pElement          PropertyMappingElement object
	 * @param pExportConfigItem export config item
	 * @return initialized property mapping repository item
	 * @throws RepositoryException if error occurs
	 */
	private RepositoryItem createPropertyMappingItem(PropertyMappingElement pElement, RepositoryItem pExportConfigItem)
			throws RepositoryException {
		MutableRepositoryItem recordMapping = ((MutableRepository) getExportRepository()).createItem(ID_RECORD_MAPPING);

		recordMapping.setPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH, pElement.getWsdlPath());
		recordMapping.setPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_PATH, pElement.getRepositoryPath());
		recordMapping.setPropertyValue(PRP_RECORD_MAPPING_REPOSITORY_NAME, pElement.getRepositoryName());
		recordMapping.setPropertyValue(PRP_RECORD_MAPPING_STATIC_VALUE, pElement.getStaticValue());
		recordMapping.setPropertyValue(PRP_RECORD_MAPPING_INOUT, pElement.getInout());

		// recordMapping.setPropertyValue(PRP_RECORD_MAPPING_EXPORT_CONFIG, pExportConfigItem);

		// ((MutableRepository) getExportRepository()).addItem(recordMapping);

		return recordMapping;
	}

	/**
	 * store transaction info to db
	 *
	 * @param pExportConfigItem export config
	 * @param pTransactionInfo  transaction info
	 * @return transaction id
	 */
	public String storeTransaction(RepositoryItem pExportConfigItem, TransactionInfo pTransactionInfo) {
		try {
			MutableRepositoryItem transaction = ((MutableRepository) getExportRepository()).createItem(ID_TRANSACTION);

			transaction.setPropertyValue(PRP_TRANSACTION_DATE_CREATE, new Date());
			transaction.setPropertyValue(PRP_TRANSACTION_EXPORT_CONFIG, pExportConfigItem);
			transaction.setPropertyValue(PRP_TRANSACTION_REPOSITORY_PATH,
					pExportConfigItem.getPropertyValue(PRP_EXPORT_REPOSITORY_NAME));

			transaction.setPropertyValue(PRP_TRANSACTION_REQUEST, pTransactionInfo.getRequest());
			transaction.setPropertyValue(PRP_TRANSACTION_RESPONSE, pTransactionInfo.getResponse());
			transaction.setPropertyValue(PRP_TRANSACTION_FAIL_REASON, pTransactionInfo.getFailReason());
			transaction.setPropertyValue(PRP_TRANSACTION_STATUS, pTransactionInfo.getStatus());
			transaction.setPropertyValue(PRP_TRANSACTION_REPOSITORY_IDS, pTransactionInfo.getExportedItemsIds());

			return ((MutableRepository) getExportRepository()).addItem(transaction).getRepositoryId();
		} catch (Exception e) {
			vlogError(e, "Unable to store transaction.");
		}
		return null;
	}
}
