package vsg.integration.export.job;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import vsg.integration.export.job.base.VSGScheduledJob;
import vsg.integration.export.service.ExportWSService;

public class WebserviceJob extends VSGScheduledJob {
	/**
	 * export ws service
	 */
	private ExportWSService mExportWSService;

	//------------------------------------------------------------------------------------------------------------------

	public ExportWSService getExportWSService() {
		return mExportWSService;
	}

	public void setExportWSService(ExportWSService pExportWSService) {
		mExportWSService = pExportWSService;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * default schedule string
	 */
	private static final String DEFAULT_SCHEDULE = "every 15 minutes";

	/**
	 * private constructor to encapsulate create logic in one place
	 * @param pExportConfigId export configuration id
	 * @param pScheduleString schedule definition
	 */
	private WebserviceJob(String pExportConfigId, String pScheduleString) {
		super(pExportConfigId, pScheduleString, WEBSERVICE_JOB_NAME + " [" + pExportConfigId + "]", DEFAULT_SCHEDULE);
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pExportConfigId export configuration id
	 * @param pScheduleString schedule definition
	 * @param pExportWSService export ws service
	 * @return webservice job
	 */
	public static WebserviceJob instance(
			String pExportConfigId,
			String pScheduleString,
			ExportWSService pExportWSService) {
		WebserviceJob webserviceJob = new WebserviceJob( pExportConfigId, pScheduleString );
		webserviceJob.setExportWSService(pExportWSService);
		return webserviceJob;
	}

	//------------------------------------------------------------------------------------------------------------------


	/**
	 * performs scheduled task
	 * @param pScheduler scheduler
	 * @param pScheduledJob schedule job
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler,
								ScheduledJob pScheduledJob) {
		vlogDebug("Web Service Job: start");
		String transactionId = getExportWSService().invokeModel(getExportConfigId());
		vlogDebug("Export configuration {0}, transaction id {1}", getExportConfigId(), transactionId);
		vlogDebug("Web Service Job: end");
	}

}
