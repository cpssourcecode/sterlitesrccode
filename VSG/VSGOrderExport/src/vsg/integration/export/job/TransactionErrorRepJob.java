package vsg.integration.export.job;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import vsg.integration.export.job.base.VSGReportJob;
import vsg.integration.export.report.service.ReportResult;

public class TransactionErrorRepJob extends VSGReportJob {

	/**
	 * default schedule string
	 */
	private static final String DEFAULT_SCHEDULE = "every 15 minutes";

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pExportConfigId export config id
	 * @param pScheduleString schedule
	 */
	public TransactionErrorRepJob(String pExportConfigId, String pScheduleString) {
		super(pExportConfigId, pScheduleString, TR_ERROR_REP_JOB_NAME + pExportConfigId, DEFAULT_SCHEDULE);
	}

	@Override
	public void doScheduledTask(Scheduler paramScheduler,
								ScheduledJob paramScheduledJob) {
		ReportResult result = getReportService().performGenerateReportError(getExportConfigId());
		if (result.isSuccess()) {
			vlogDebug("Generate report processed without errors.");
		} else {
			vlogError("Generate report processed with errors. Check log above.");
		}
	}
}
