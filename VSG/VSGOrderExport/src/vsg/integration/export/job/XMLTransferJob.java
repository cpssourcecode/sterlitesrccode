package vsg.integration.export.job;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import vsg.integration.export.job.base.VSGScheduledJob;

public class XMLTransferJob extends VSGScheduledJob {

	/**
	 * default schedule string
	 */
	private static final String DEFAULT_SCHEDULE = "every 15 minutes";

	public XMLTransferJob(String pExportConfigId, String pScheduleString) {
		super(pExportConfigId, pScheduleString, XML_TRANSFER_JOB_NAME + pExportConfigId, DEFAULT_SCHEDULE);
	}

	@Override
	public void doScheduledTask(Scheduler paramScheduler, ScheduledJob paramScheduledJob) {
		vlogDebug(paramScheduledJob.getJobName());
	}
}
