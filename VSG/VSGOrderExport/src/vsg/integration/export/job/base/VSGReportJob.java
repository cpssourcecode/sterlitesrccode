package vsg.integration.export.job.base;

import vsg.integration.export.report.service.ReportService;

/**
 * @author Dmitry Golubev
 */
public abstract class VSGReportJob extends VSGScheduledJob {
	/**
	 * report service
	 */
	private ReportService mReportService;

	//------------------------------------------------------------------------------------------------------------------

	public ReportService getReportService() {
		return mReportService;
	}

	public void setReportService(ReportService pReportService) {
		mReportService = pReportService;
	}

	//------------------------------------------------------------------------------------------------------------------

	public VSGReportJob(String pExportConfigId, String pScheduleString, String pJobName, String pDefaultSchedule) {
		super(pExportConfigId, pScheduleString, pJobName, pDefaultSchedule);
	}

	/**
	 * test call of scheduled job
	 */
	public void performTaskOnce() {
		doScheduledTask(null, null);
	}
}
