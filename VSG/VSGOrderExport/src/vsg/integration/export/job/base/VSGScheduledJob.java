package vsg.integration.export.job.base;

import atg.service.scheduler.SingletonSchedulableService;
import vsg.integration.export.IScheduleConstants;

/**
 * VSG Scheduled job
 */
public abstract class VSGScheduledJob extends SingletonSchedulableService implements IScheduleConstants {

	/**
	 * export config id
	 */
	private String mExportConfigId;
	/**
	 * job name
	 */
	private String mJobName;
	/**
	 * default schedule
	 */
	private String mDefaultSchedule;

	/**
	 * schedule string
	 */
	private String mScheduleString;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 * @param pExportConfigId export configuration id
	 * @param pScheduleString schedule definition
	 * @param pJobName job name
	 * @param pDefaultSchedule default schedule definition
	 */
	public VSGScheduledJob(String pExportConfigId, String pScheduleString, String pJobName, String pDefaultSchedule) {
		setExportConfigId(pExportConfigId);
		setJobName(pJobName);
		setDefaultSchedule(pDefaultSchedule);
		setScheduleString(pScheduleString);
		setLockName(getJobName());
		setLockTimeOut(2000);
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getExportConfigId() {
		return mExportConfigId;
	}

	public void setExportConfigId(String pExportConfigId) {
		this.mExportConfigId = pExportConfigId;
	}

	public String getJobName() {
		return mJobName;
	}

	public void setJobName(String pJobName) {
		this.mJobName = pJobName;
	}

	public String getDefaultSchedule() {
		return mDefaultSchedule;
	}

	public void setDefaultSchedule(String pDefaultSchedule) {
		this.mDefaultSchedule = pDefaultSchedule;
	}

	public String getScheduleString() {
		return mScheduleString;
	}

	public void setScheduleString(String pScheduleString) {
		this.mScheduleString = pScheduleString;
	}

}
