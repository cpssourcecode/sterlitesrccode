package vsg.integration.export.handler;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.UploadedFile;

import javax.servlet.ServletException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Dmitry Golubev
 */
public class FileUploadHandler extends BaseFormHandler {

	/**
	 * property: UploadDirectory - where we will put the uploaded file
	 */
	private String mUploadDirectory;

	//------------------------------------------------------------------------------------------------------------------

	public void setUploadDirectory(String pUploadDirectory) {
		mUploadDirectory = pUploadDirectory;
	}

	public String getUploadDirectory() {
		return mUploadDirectory;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * This method is called when the form above is submitted.  This code makes
	 * sure that it has an appropriate object and then pass it along for further
	 * processing.
	 *
	 * @param pFileObject either an UploadedFile or an UploadedFile[]
	 */
	public void setUploadProperty(Object pFileObject) {
		if (pFileObject == null) {
			vlogError("**** ERROR: FileUploadDroplet received a NULL file.");
			return;
		}

		if (pFileObject instanceof UploadedFile[]) {
			vlogDebug("Reading in UploadedFile[]");
			readUpFiles((UploadedFile[]) pFileObject);
		} else if (pFileObject instanceof UploadedFile) {
			readUpFiles(new UploadedFile[]{(UploadedFile) pFileObject});
		} else {
			vlogError("**** ERROR: FileUploadDroplet received an Object which is "
					+ "neither an UploadedFile or an UploadedFile[].");
		}
	}

	/**
	 * Returns property UploadProperty
	 */
	public Object getUploadProperty() {
		// return null since we don't need to maintain a
		// reference to the original uploaded file(s)
		return null;
	}

	/**
	 * Here you can access the data in the uploaded file(s). You
	 * should get the data from the uploaded file before the
	 * request is complete.  If the file is large, it is stored as a temporary
	 * file on disk, and this file is removed when the request is complete.
	 *
	 * @param pFiles the uploaded file(s)
	 */
	void readUpFiles(UploadedFile[] pFiles) {
		for (UploadedFile uploadedFile : pFiles) {
			if (uploadedFile.getFileSize() <= 0) {
				vlogError(" FileUploadDroplet Cannot upload - file has length 0: " + uploadedFile.getFilename());
				return;
			}
			writeFileToLocalDir(uploadedFile);

		}
	}

	/**
	 * writes upload file to local dir
	 *
	 * @param pUploadedFile upload file to store
	 */
	private void writeFileToLocalDir(UploadedFile pUploadedFile) {
		String otherSeparator = "/";
		if ("/".equals(File.separator)) otherSeparator = "\\";
		String convertedClientFilePath = StringUtils.replace(pUploadedFile.getFilename(), otherSeparator, File.separator);
		String fileName = convertedClientFilePath.substring(convertedClientFilePath.lastIndexOf(File.separator) + 1);

		File localFile = new File(getUploadDirectory() + File.separator + fileName);

		FileOutputStream fos = null;
		try {
			byte[] fileData = pUploadedFile.toByteArray();
			vlogDebug(pUploadedFile.getFilename() + " - " + pUploadedFile.getFileSize() + " bytes long.");
			fos = new FileOutputStream(localFile);
			fos.write(fileData);
			fos.flush();
		} catch (IOException e) {
			vlogError(e, "FileUploadDroplet failed");
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException exc) {
					vlogError(exc, "Error");
				}
			}
		}
	}

	/**
	 * test handle method
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException                    if error occurs
	 * @throws javax.servlet.ServletException if error occurs
	 */
	public boolean handleTestCall(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		vlogDebug("handleTestCall");
		return checkFormRedirect(pRequest, pResponse);
	}

}
