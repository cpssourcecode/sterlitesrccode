package vsg.integration.export.handler;

import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.nucleus.Nucleus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.repository.SupportedRepositories;
import vsg.integration.export.service.ExportRepositoryService;
import vsg.integration.export.service.RepositoryItemPreviewService;
import vsg.integration.export.util.ObjectStringFormatter;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class QueryFormHandler extends BaseFormHandler implements IRepositoryConstants {
	/**
	 * relation delimeter
	 */
	private static final String RELATION_DELIMETER = ".";
	/**
	 * supported repositories info
	 */
	private SupportedRepositories mSupportedRepositories;
	/**
	 * Object to string formatter
	 */
	private ObjectStringFormatter mFormatter;
	/**
	 * test result column properties
	 */
	private String[] mTestResultColumns;
	/**
	 * test result column titles
	 */
	private String[] mTestResultColumnsTitles;
	/**
	 * test result columns
	 */
	private List<String[]> mTestResult;
	/**
	 * export repository service
	 */
	private ExportRepositoryService mExportRepositoryService;

	/**
	 * Repository Item Preview Service
	 */
	private RepositoryItemPreviewService mPreviewService;

	//------------------------------------------------------------------------------------------------------------------

	public SupportedRepositories getSupportedRepositories() {
		return mSupportedRepositories;
	}

	public void setSupportedRepositories(SupportedRepositories pSupportedRepositories) {
		mSupportedRepositories = pSupportedRepositories;
	}

	public ObjectStringFormatter getFormatter() {
		return mFormatter;
	}

	public void setFormatter(ObjectStringFormatter pFormatter) {
		mFormatter = pFormatter;
	}

	public List<String[]> getTestResult() {
		return mTestResult;
	}

	public void setTestResult(List<String[]> pTestResult) {
		mTestResult = pTestResult;
	}

	public String[] getTestResultColumnsTitles() {
		return mTestResultColumnsTitles;
	}

	public void setTestResultColumnsTitles(String[] pTestResultColumnsTitles) {
		mTestResultColumnsTitles = pTestResultColumnsTitles;
	}

	public String[] getTestResultColumns() {
		return mTestResultColumns;
	}

	public void setTestResultColumns(String[] pTestResultColumns) {
		mTestResultColumns = pTestResultColumns;
	}

	public ExportRepositoryService getExportRepositoryService() {
		return mExportRepositoryService;
	}

	public void setExportRepositoryService(ExportRepositoryService pExportRepositoryService) {
		mExportRepositoryService = pExportRepositoryService;
	}

	public RepositoryItemPreviewService getPreviewService() {
		return mPreviewService;
	}

	public void setPreviewService(RepositoryItemPreviewService pPreviewService) {
		mPreviewService = pPreviewService;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * test rql
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleTestRQL(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();
		resetTestResults();
		String paramRql = getValue().get(PRP_EXPORT_RQL);
		if (StringUtils.isBlank(paramRql)) {
			addFormException(new DropletException("Proposed RQL is empty."));
		}
		SelectedRepositoryInfo repositoryInfo = new SelectedRepositoryInfo(getValue().get(PRP_EXPORT_REPOSITORY_NAME));
		if (!getFormError()) {
			try {
				executeTestRQL(paramRql, repositoryInfo);
			} catch (Exception e) {
				vlogError(e, "Error in rql test.");
				addFormException(new DropletException("Unable to execute rql. Check log."));
			}
		}
		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * save rql to export model
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleSaveRQL(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();
		resetTestResults();
		String paramRql = getValue().get(PRP_EXPORT_RQL);
		if (StringUtils.isBlank(paramRql)) {
			addFormException(new DropletException("Proposed RQL is empty."));
		}
		SelectedRepositoryInfo repositoryInfo = new SelectedRepositoryInfo(getValue().get(PRP_EXPORT_REPOSITORY_NAME));
		if (!getFormError()) {
			if (isTestCallSuccess(paramRql, repositoryInfo)) {
				if (!getExportRepositoryService().storeQuerySettings(
						getExportConfig().getRepositoryId(),
						paramRql,
						repositoryInfo.getItemDescriptor(),
						Boolean.parseBoolean(getValue().get(PRP_EXPORT_MARK_AS_EXPORTED)),
						repositoryInfo.getRepository())) {
					addFormException(new DropletException("Unable to save settings. Check log."));
				}
			}
		}
		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * reset search
	 */
	private void resetTestResults() {
		setTestResult(null);
		setTestResultColumnsTitles(null);
		setTestResultColumns(null);
	}

	/**
	 * execute test rql and fill test results
	 *
	 * @param pRql            rql to execute
	 * @param pRepositoryInfo selected repository info
	 * @throws RepositoryException if error occurs
	 */
	private void executeTestRQL(String pRql, SelectedRepositoryInfo pRepositoryInfo) throws RepositoryException {
		RepositoryView view = pRepositoryInfo.getRepository().getView(pRepositoryInfo.getItemDescriptor());
		RqlStatement rqlStatement = RqlStatement.parseRqlStatement(pRql);

		RepositoryItem[] items = rqlStatement.executeQuery(view, null);
		if (items != null) {
			setTestResultColumnsTitles(
					getSupportedRepositories().getDefaultColumnsTitles(pRepositoryInfo.getItemDescriptor())
			);
			setTestResultColumns(
					getSupportedRepositories().getDefaultColumns(pRepositoryInfo.getItemDescriptor())
			);
			setTestResult(new ArrayList<String[]>());
			for (RepositoryItem repositoryItem : items) {
				getTestResult().add(getPreviewService().getRepositoryItemRow(repositoryItem, getTestResultColumns()));
			}
		}
	}

	/**
	 * @param pRql            rql for test
	 * @param pRepositoryInfo selected repository info
	 * @return rql is correct and can return more then 0 records
	 */
	private boolean isTestCallSuccess(String pRql, SelectedRepositoryInfo pRepositoryInfo) {
		try {
			RepositoryView view = pRepositoryInfo.getRepository().getView(pRepositoryInfo.getItemDescriptor());
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(pRql);
			int numberOfRecords = rqlStatement.executeCountQuery(view, null);
//			if (numberOfRecords == 0) {
///*
//				vlogError("No items will be returned for rql: {0}", pRql);
//				return false;
//*/
//			}
			return true;
		} catch (Exception e) {
			vlogError("Error in rql test: {0}", e.toString());
			addFormException(new DropletException("Error in rql test.", e));
		}
		return false;
	}


	class SelectedRepositoryInfo {
		/**
		 * repository
		 */
		private Repository mRepository;
		/**
		 * item descriptor name
		 */
		private String mItemDescriptor;

		public Repository getRepository() {
			return mRepository;
		}

		public void setRepository(Repository pRepository) {
			mRepository = pRepository;
		}

		public String getItemDescriptor() {
			return mItemDescriptor;
		}

		public void setItemDescriptor(String pItemDescriptor) {
			mItemDescriptor = pItemDescriptor;
		}

		SelectedRepositoryInfo(String pRepositoryRequestParam) {
			String[] repositoryParamParts = pRepositoryRequestParam.split("\\|");
			setItemDescriptor(repositoryParamParts[0]);
			setRepository(getRepository(repositoryParamParts[1]));
		}

		/**
		 * @return repository from request
		 */
		private Repository getRepository(String pRepositoryName) {
/*
			String paramRepository = getValue().get(PRP_EXPORT_REPOSITORY_NAME);
*/
			Repository repository = null;
			try {
				repository = (Repository) Nucleus.getGlobalNucleus().resolveName(pRepositoryName);
			} catch (Exception e) {
				vlogError(e, "Unable retrieve repository by specified name: " + pRepositoryName);
			}
			if (repository == null) {
				addFormException(new DropletException("Repository is not specified."));
			}
			return repository;
		}

	}
}
