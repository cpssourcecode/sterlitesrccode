package vsg.integration.export.handler;

import atg.repository.*;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.repository.SupportedRepositories;
import vsg.integration.export.service.RepositoryItemPreviewService;
import vsg.integration.export.util.bean.TableData;

import javax.servlet.ServletException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * handles Transactions (see /vsg/integration/export/repository/ExportRepository )
 */
public class TransactionFormHandler extends BaseFormHandler implements IRepositoryConstants {

	/**
	 * Input date format
	 */
	private static final String INPUT_DATE_FORMAT = "\\d{2}/\\d{2}/\\d{4}";

	/**
	 * Date format for parse
	 */
	private static final String PARSE_DATE_FORMAT = "MM/dd/yyyy";

	/**
	 * Export Repository
	 */
	private MutableRepository mRepository;

	/**
	 * Repository Item Preview Service
	 */
	private RepositoryItemPreviewService mPreviewService;

	/**
	 * supported repositories info
	 */
	private SupportedRepositories mSupportedRepositories;

	/**
	 * list of result tables
	 */
	private List<TableData> mResult;

	//------------------------------------------------------------------------------------------------------------------

	public List<TableData> getResult() {
		return mResult;
	}

	public void setResult(List<TableData> pResult) {
		mResult = pResult;
	}

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		mRepository = pRepository;
	}

	public RepositoryItemPreviewService getPreviewService() {
		return mPreviewService;
	}

	public void setPreviewService(RepositoryItemPreviewService pPreviewService) {
		mPreviewService = pPreviewService;
	}

	public SupportedRepositories getSupportedRepositories() {
		return mSupportedRepositories;
	}

	public void setSupportedRepositories(SupportedRepositories pSupportedRepositories) {
		mSupportedRepositories = pSupportedRepositories;
	}


	//------------------------------------------------------------------------------------------------------------------


	/**
	 * search for repository items by date range and transaction state
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleSearch(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		resetErrors();
		setResult(new ArrayList<TableData>());

		try {
			RepositoryView view = getRepository().getView(ID_TRANSACTION);
			Query query = generateQuery(view);
			SortDirectives sortDirectives = new SortDirectives();
			sortDirectives.addDirective(new SortDirective(PRP_TRANSACTION_DATE_CREATE, SortDirective.DIR_DESCENDING));
			QueryOptions queryOptions = new QueryOptions(-1, -1, sortDirectives, null);
			RepositoryItem[] items = view.executeQuery(query, queryOptions);
			generateResult(items);
		} catch (RepositoryException e) {
			vlogDebug("Repository exception during quering items");
		}

		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * generates result tables by transaction data
	 *
	 * @param pItems - transactions
	 * @throws RepositoryException
	 */
	private void generateResult(RepositoryItem[] pItems) throws RepositoryException {
		if (pItems != null) {
			for (RepositoryItem item : pItems) {
				String itemRepository = getPreviewService().getRepositoryItemValue(item, PRP_TRANSACTION_REPOSITORY_PATH);
				Repository repo = (Repository) resolveName(itemRepository);
				getResult().add(generateTable(repo, item));
			}
		}
	}

	/**
	 * generates table: first info row, columns titles, rows generated according to repository ids setted in transaction
	 *
	 * @param pRepo        repository established by repository path in transaction
	 * @param pTransaction - transaction
	 * @return table
	 * @throws RepositoryException
	 */
	private TableData generateTable(Repository pRepo, RepositoryItem pTransaction) throws RepositoryException {

		TableData table = generateTableInfo(pTransaction);
		List<String[]> rows = new ArrayList<String[]>();

		List<String> repositoryIds = (List<String>) pTransaction.getPropertyValue(PRP_TRANSACTION_REPOSITORY_IDS);

		RepositoryItem exportConfig = (RepositoryItem) pTransaction.getPropertyValue(PRP_TRANSACTION_EXPORT_CONFIG);
		if (exportConfig != null) {
			String itemDescriptor = (String) exportConfig.getPropertyValue(PRP_EXPORT_ITEM_DESCRIPTOR);

			if (table != null && repositoryIds != null) {
				for (String id : repositoryIds) {
					RepositoryItem currentItem = pRepo.getItem(id, itemDescriptor);
					String[] itemRow = getPreviewService().getRepositoryItemRow(
							currentItem,
							getSupportedRepositories().getDefaultColumns(itemDescriptor)
					);
					rows.add(itemRow);
				}
				table.setRows(rows);
			}
		}
		return table;
	}

	/**
	 * generates first two rows of table
	 *
	 * @param pTransaction - transaction
	 * @return first two rows of table
	 */
	private TableData generateTableInfo(RepositoryItem pTransaction) {
		TableData table = new TableData();

		RepositoryItem exportConfig = (RepositoryItem) pTransaction.getPropertyValue(PRP_TRANSACTION_EXPORT_CONFIG);
		if (exportConfig == null) {
			return table;
		}
		String itemDescriptor = (String) exportConfig.getPropertyValue(PRP_EXPORT_ITEM_DESCRIPTOR);


		int tableColumnNumber = 4;
		String[] columnsTitles = getSupportedRepositories().getDefaultColumnsTitles(itemDescriptor);

		if (getSupportedRepositories().getDefaultColumnsTitles(itemDescriptor).length > 4) {
			tableColumnNumber = columnsTitles.length;
		}
		table.setColunmsCount(tableColumnNumber);

		String[] firstRow = new String[tableColumnNumber];
		firstRow[0] = "Repository Id: " + pTransaction.getRepositoryId();
		firstRow[1] = "Creation Date: " + getPreviewService().getRepositoryItemValue(pTransaction, PRP_EXPORT_DATE_CREATE);

		int transactionStatus = (Integer) pTransaction.getPropertyValue(PRP_TRANSACTION_STATUS);
		if (transactionStatus == 1) {
			firstRow[2] = "Transaction Status: SUCCESS";
		} else if (transactionStatus == 0) {
			firstRow[2] = "Transaction Status: FAIL";
		}
		firstRow[3] = pTransaction.getRepositoryId();

		table.setPreTableInfo(firstRow);
		table.setColunmsTitles(columnsTitles);

		return table;

	}

	/**
	 * choose query according to query parameters
	 *
	 * @param pView - repository view
	 * @return query
	 * @throws RepositoryException
	 */
	private Query generateQuery(RepositoryView pView) throws RepositoryException {
		QueryBuilder queryBuilder = pView.getQueryBuilder();
		List<Query> addQueries = new ArrayList<Query>();

		try {
			if (getValue().get("dateFrom").matches(INPUT_DATE_FORMAT)) {
				Date dateFrom = new SimpleDateFormat(PARSE_DATE_FORMAT).parse(getValue().get("dateFrom"));
				addQueries.add(queryBuilder.createComparisonQuery(queryBuilder.createPropertyQueryExpression(PRP_EXPORT_DATE_CREATE),
						queryBuilder.createConstantQueryExpression(dateFrom),
						QueryBuilder.GREATER_THAN));
			}
			if (getValue().get("dateTo").matches(INPUT_DATE_FORMAT)) {
				Date dateTo = new SimpleDateFormat(PARSE_DATE_FORMAT).parse(getValue().get("dateTo"));
				addQueries.add(queryBuilder.createComparisonQuery(queryBuilder.createPropertyQueryExpression(PRP_EXPORT_DATE_CREATE),
						queryBuilder.createConstantQueryExpression(dateTo),
						QueryBuilder.LESS_THAN));
			}
		} catch (ParseException e) {
			vlogError("Cannot parse date. Query without date range");
		}

		if (getValue().get("trStatus").equals("1") || getValue().get("trStatus").equals("0")) {
			addQueries.add(queryBuilder.createComparisonQuery(queryBuilder.createPropertyQueryExpression(PRP_TRANSACTION_STATUS),
					queryBuilder.createConstantQueryExpression(getValue().get("trStatus")),
					QueryBuilder.EQUALS));
		}

		Query[] addQueriesArray = addQueries.toArray(new Query[addQueries.size()]);

		Query query;
		if (addQueriesArray != null && addQueriesArray.length == 0) {
			query = queryBuilder.createUnconstrainedQuery();
		} else {
			query = queryBuilder.createAndQuery(addQueriesArray);
		}
		return query;

	}

}
