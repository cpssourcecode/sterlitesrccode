package vsg.integration.export.handler;

import atg.droplet.DropletException;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.service.ExportRepositoryService;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * handles Order Output Format and ExportConfig session bean
 */
public class InitialConfiguratorFormHandler extends BaseFormHandler implements IRepositoryConstants {
	/**
	 * Export Repository Service
	 */
	private ExportRepositoryService mExportRepositoryService;

	//------------------------------------------------------------------------------------------------------------------

	public ExportRepositoryService getExportRepositoryService() {
		return mExportRepositoryService;
	}

	public void setExportRepositoryService(
			ExportRepositoryService pExportRepositoryService) {
		mExportRepositoryService = pExportRepositoryService;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * update order output format
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws java.io.IOException            if error occurs
	 * @throws javax.servlet.ServletException if error occurs
	 */
	public boolean handleUpdateInitial(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();

		try {

			getExportRepositoryService().storeInitialConfigurationSettings(getExportConfig(), getValue());

		} catch (RepositoryException e) {
			vlogError("Error while updating Export Initial Settings");
			addFormException(new DropletException("Error while updating Export Initial Settings: " + e.toString(), e));
		}

		return checkFormRedirect(pRequest, pResponse);
	}
}
