package vsg.integration.export.handler;

import atg.droplet.DropletException;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.service.ExportRepositoryService;
import vsg.integration.export.service.ScheduleService;
import vsg.integration.export.service.validator.OutputFormatConfigValidator;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * handles Model Initialization and Model session bean
 */
public class ConfigIinitializerFormHandler extends BaseFormHandler implements IRepositoryConstants {

	/**
	 * Export Repository
	 */
	private MutableRepository mRepository;

	/**
	 * Order Output Format Service
	 */
	private OutputFormatConfigValidator mOutputFormatConfigValidator;

	/**
	 * Export Repository Service
	 */
	private ExportRepositoryService mExportRepositoryService;

	/**
	 * Schedule Service
	 */
	private ScheduleService mScheduleService;

	//------------------------------------------------------------------------------------------------------------------

	public MutableRepository getRepository() {
		return mRepository;
	}

	public ScheduleService getScheduleService() {
		return mScheduleService;
	}

	public void setScheduleService(ScheduleService pScheduleService) {
		this.mScheduleService = pScheduleService;
	}

	public void setRepository(MutableRepository pRepository) {
		this.mRepository = pRepository;
	}

	public OutputFormatConfigValidator getOutputFormatConfigValidator() {
		return mOutputFormatConfigValidator;
	}

	public void setOutputFormatConfigValidator(
			OutputFormatConfigValidator pOutputFormatConfigValidator) {
		mOutputFormatConfigValidator = pOutputFormatConfigValidator;
	}

	public ExportRepositoryService getExportRepositoryService() {
		return mExportRepositoryService;
	}

	public void setExportRepositoryService(
			ExportRepositoryService pExportRepositoryService) {
		mExportRepositoryService = pExportRepositoryService;
	}

	//------------------------------------------------------------------------------------------------------------------


	/**
	 * sets existing model to session bean
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleUseExistingConfig(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();
		getExportConfig().setRepositoryId(getValue().get(PRP_EXPORT_REPOSITORY_ID));
		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * creates model
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleCreateConfig(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();

		try {
			String repositoryId = getExportRepositoryService().createExportItem();
			getExportConfig().setRepositoryId(repositoryId);
		} catch (RepositoryException e) {
			vlogError(e, "Error while Creating Export Config");
			addFormException(new DropletException("Unable to create export configuration. Please check log."));
		}

		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * deletes model
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleDeleteConfig(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();

		try {
			String deleteId = getValue().get("deleteId");
			getScheduleService().deleteConfigJobs(deleteId);
			getExportRepositoryService().deleteExportItem(deleteId);
			if (deleteId.equals(getExportConfig().getRepositoryId())) {
				getExportConfig().setRepositoryId(null);
			}
		} catch (RepositoryException e) {
			vlogError(e, "Error while Creating Export Config");
			addFormException(new DropletException("Unable to delete export configuration. Please check log."));
		}

		return checkFormRedirect(pRequest, pResponse);
	}
}
