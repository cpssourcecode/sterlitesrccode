package vsg.integration.export.handler;

import atg.droplet.DropletException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.service.ExportWSService;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class ModelExportFormHandler extends BaseFormHandler {
	/**
	 * model export service
	 */
	ExportWSService mExportWSService;

	//------------------------------------------------------------------------------------------------------------------

	public ExportWSService getExportWSService() {
		return mExportWSService;
	}

	public void setExportWSService(ExportWSService pExportWSService) {
		mExportWSService = pExportWSService;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * handle export model to ws
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false for ajax submit
	 * @throws java.io.IOException            if error occur
	 * @throws javax.servlet.ServletException if error occur
	 */
	public boolean handleExportModel(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		resetErrors();

		Map<String, Object> params = new HashMap<String, Object>();
		try {
			String transactionId = getExportWSService().invokeModel(getExportConfig().getRepositoryId());
			if (transactionId == null) {
				addFormException(new DropletException("Unable to export model. Please check log."));
			} else {
				params.put("transactionId", transactionId);
			}
		} catch (Exception e) {
			vlogError(e, "Unable to export model");
			addFormException(new DropletException("Unable to export model. Please check log."));
		}

		return checkFormRedirect(pRequest, pResponse, params);
	}
}
