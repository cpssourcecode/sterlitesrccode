package vsg.integration.export.handler;

import atg.droplet.DropletException;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.job.WebserviceJob;
import vsg.integration.export.job.XMLTransferJob;
import vsg.integration.export.service.ExportRepositoryService;
import vsg.integration.export.service.ExportWSService;
import vsg.integration.export.service.ScheduleService;
import vsg.integration.export.service.UploadFileService;
import vsg.integration.export.service.validator.OutputFormatConfigValidator;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * handles Order Output Format and ExportConfig session bean
 */
public class OutputFormatConfigFormHandler extends BaseFormHandler implements IRepositoryConstants {

	/**
	 * Output Format Config Service
	 */
	private OutputFormatConfigValidator mOutputFormatConfigService;

	/**
	 * Export Repository Service
	 */
	private ExportRepositoryService mExportRepositoryService;

	/**
	 * Schedule Service
	 */
	private ScheduleService mScheduleService;
	/**
	 * export ws service
	 */
	private ExportWSService mExportWSService;

	/**
	 * Upload File Service
	 */
	private UploadFileService mUploadFileService;

	//------------------------------------------------------------------------------------------------------------------

	public UploadFileService getUploadFileService() {
		return mUploadFileService;
	}

	public void setUploadFileService(UploadFileService pUploadFileService) {
		this.mUploadFileService = pUploadFileService;
	}

	public OutputFormatConfigValidator getOutputFormatConfigValidator() {
		return mOutputFormatConfigService;
	}

	public void setOutputFormatConfigValidator(
			OutputFormatConfigValidator pOutputFormatConfigService) {
		mOutputFormatConfigService = pOutputFormatConfigService;
	}

	public ExportRepositoryService getExportRepositoryService() {
		return mExportRepositoryService;
	}

	public void setExportRepositoryService(
			ExportRepositoryService pExportRepositoryService) {
		mExportRepositoryService = pExportRepositoryService;
	}

	public ScheduleService getScheduleService() {
		return mScheduleService;
	}

	public void setScheduleService(ScheduleService pScheduleService) {
		this.mScheduleService = pScheduleService;
	}

	public ExportWSService getExportWSService() {
		return mExportWSService;
	}

	public void setExportWSService(ExportWSService pExportWSService) {
		mExportWSService = pExportWSService;
	}
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * update order output format
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleUpdateOutputFormatConfig(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();

		try {

			if (getOutputFormatConfigValidator().isValidExportConfig(this, getExportConfig()) &&
					getOutputFormatConfigValidator().isValidOututFormatConfig(this, getValue())) {

				getExportRepositoryService().storeOutputFormatSettings(getExportConfig(), getValue());
				getExportRepositoryService().checkTransferOptionsUse(getExportConfig(), getValue());

				boolean createJob = Boolean.parseBoolean(getValue().get(PRP_OFC_USE_SCHEDULE));
				if (createJob) {
					WebserviceJob webserviceJob = WebserviceJob.instance(
							getExportConfig().getRepositoryId(),
							getValue().get(PRP_OFC_SCHEDULE),
							getExportWSService()
					);
					getScheduleService().registerJob(getExportConfig().getRepositoryId(), webserviceJob);
				}
				getUploadFileService().deleteUnnecessaryFiles(getRepository().getItem(getExportConfig().getRepositoryId(), ID_EXPORT_CONFIG));

			} else {
				if (isLoggingDebug()) {
					vlogDebug("Validation of export config failed");
				}
			}

		} catch (RepositoryException e) {
			vlogError("Error while updating Export Output Format");
			addFormException(new DropletException("Error while updating Output Format Config: " + e.toString(), e));
		}

		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * verify order output format
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleVerifyOutputFormatConfig(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();

		if (!getOutputFormatConfigValidator().isValidExportConfig(this, getExportConfig()) ||
				!getOutputFormatConfigValidator().isValidOututFormatConfig(this, getValue())) {
			if (isLoggingDebug()) {
				vlogDebug("Validation failed");
			}
		}
		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * configure transfer options
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleUpdateTransferOptions(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();

		try {

			if (getOutputFormatConfigValidator().isValidExportConfig(this, getExportConfig()) &&
					getOutputFormatConfigValidator().isValidTransferOptionsConfig(this, getValue())) {

				getExportRepositoryService().storeTransferOptions(getExportConfig(), getValue());
				XMLTransferJob xmlTransferJob = new XMLTransferJob(getExportConfig().getRepositoryId(),
						getValue().get(PRP_TOC_SCHEDULE_TRANSFER));
				getScheduleService().registerJob(getExportConfig().getRepositoryId(), xmlTransferJob);

			} else {
				if (isLoggingDebug()) {
					vlogDebug("Validation failed");
				}
			}

		} catch (RepositoryException e) {
			vlogError("Error while updating Export Output Format");
			addFormException(new DropletException("Error while updating Output Format Config: " + e.toString(), e));
		}

		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * verify transfer options
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return true - stay on that page, false - redirect on specified page
	 * @throws IOException      if error occurs
	 * @throws ServletException if error occurs
	 */
	public boolean handleVerifyTransferOptions(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();

		if (!getOutputFormatConfigValidator().isValidExportConfig(this, getExportConfig()) ||
				!getOutputFormatConfigValidator().isValidTransferOptionsConfig(this, getValue())) {
			if (isLoggingDebug()) {
				vlogDebug("Validation of Export Config failed, dont set");
			}
		}

		return checkFormRedirect(pRequest, pResponse);
	}

}
