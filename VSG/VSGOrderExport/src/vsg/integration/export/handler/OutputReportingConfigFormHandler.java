package vsg.integration.export.handler;

import atg.droplet.DropletException;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.job.TransactionErrorRepJob;
import vsg.integration.export.job.TransactionSuccessfulRepJob;
import vsg.integration.export.service.OutputReportingService;
import vsg.integration.export.service.ScheduleService;
import vsg.integration.export.service.UploadFileService;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * handles Order Output Format and Model session bean
 *
 * @author "Vladimir Tretyakevich"
 */
public class OutputReportingConfigFormHandler extends BaseFormHandler implements IRepositoryConstants {

	/**
	 * Output Reporting Service
	 */
	private OutputReportingService mOutputReportingService;

	/**
	 * Schedule Service
	 */
	private ScheduleService mScheduleService;

	/**
	 * Upload File Service
	 */
	private UploadFileService mUploadFileService;

	// ------------------------------------------------------------------------------------------------------------------

	public OutputReportingService getOutputReportingService() {
		return mOutputReportingService;
	}

	public void setOutputReportingService(OutputReportingService pOutputReportingService) {
		mOutputReportingService = pOutputReportingService;
	}

	public ScheduleService getScheduleService() {
		return mScheduleService;
	}

	public void setScheduleService(ScheduleService pScheduleService) {
		this.mScheduleService = pScheduleService;
	}

	public UploadFileService getUploadFileService() {
		return mUploadFileService;
	}

	public void setUploadFileService(UploadFileService pUploadFileService) {
		this.mUploadFileService = pUploadFileService;
	}

	// ------------------------------------------------------------------------------------------------------------------

	/**
	 * Handle create output report config.
	 *
	 * @param pRequest  the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws IOException      Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public boolean handleCreateOutputReportConfig(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		debugAllInputValues();

		resetErrors();
		try {
			if (getOutputReportingService().isValidInput(getValue(), this)) {
				getOutputReportingService().createOutputReportConfigItem(getExportConfig(), getValue());

				TransactionErrorRepJob transactionErrorRepJob = new TransactionErrorRepJob(getExportConfig().getRepositoryId(),
						getScheduleService().getScheduleValues().get(getValue().get(PRP_REP_CONF_EFREQ)));
				getScheduleService().registerJob(getExportConfig().getRepositoryId(), transactionErrorRepJob);

				TransactionSuccessfulRepJob transactionSuccessfulRepJob = new TransactionSuccessfulRepJob(getExportConfig().getRepositoryId(),
						getScheduleService().getScheduleValues().get(getValue().get(PRP_REP_CONF_SFREQ)));
				getScheduleService().registerJob(getExportConfig().getRepositoryId(), transactionSuccessfulRepJob);

				getUploadFileService().deleteUnnecessaryFiles(getRepository().getItem(getExportConfig().getRepositoryId(), ID_EXPORT_CONFIG));

			} else {
//				addFormException(new DropletException("Please check input values"));
				vlogDebug("Validation fails, don't create");
			}
		} catch (RepositoryException e) {
			vlogError("Error while Creating Output Reporting Configuration");
			addFormException(new DropletException("Error while Creating Output Reporting Configuration: " + e.toString(), e));
		}

		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * Validate output report config.
	 *
	 * @param pRequest  the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws IOException      Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public boolean handleVerifyOutputReportConfig(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		resetErrors();
		if (!getOutputReportingService().isValidInput(getValue(), this)) {
			vlogDebug("Validation fails, don't create");
		}
		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * Debug all input values.
	 */
	private void debugAllInputValues() {
		if (isLoggingDebug()) {
			StringBuilder sb = new StringBuilder();
			for (String key : getValue().keySet()) {
				sb.append(key).append(": ").append(getValue().get(key)).append("; ");
			}
			logDebug(sb.toString());
		}
	}

}
