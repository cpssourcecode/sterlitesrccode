package vsg.integration.export.handler;

import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.mapping.PropertyMappingElement;
import vsg.integration.export.mapping.PropertyMappingInfo;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.service.ExportRepositoryService;
import vsg.integration.export.service.util.ExportConfigWsdlPathGenerator;
import vsg.integration.export.service.validator.WSDLMappingValidator;
import vsg.integration.export.util.bean.ValidationResult;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class PropertyMappingFormHandler extends BaseFormHandler implements IRepositoryConstants {
	/**
	 * property mapping info
	 */
	private PropertyMappingInfo mPropertyMappingInfo;
	/**
	 * export repository service
	 */
	private ExportRepositoryService mExportRepositoryService;
	/**
	 * wsdl mapping validator
	 */
	private WSDLMappingValidator mWSDLMappingValidator;
	/**
	 * request prefix name for full path info
	 */
	private final static String typeFullPathPrefix = ".type.fullPath.";
	/**
	 * request prefix name for static value info
	 */
	private final static String typeStaticValuePrefix = ".type.staticValue.";
	/**
	 * request parameter 'wsdlUrl'
	 */
	private final static String PARAM_WSDL_URL = "wsdlUrl";
	/**
	 * request parameter 'exportConfigId'
	 */
	private final static String PARAM_EXPORT_CONFIG_ID = "exportConfigId";
	/**
	 * request parameter 'wsdlUpload'
	 */
	private final static String PARAM_WSDL_UPLAOD = "wsdlUplaod";
	/**
	 * request parameter 'wsdlOperation'
	 */
	private final static String PARAM_WSDL_OPERATION = "wsdlOperation";
	/**
	 * request parameter 'wsdlOperationAction'
	 */
	private final static String PARAM_WSDL_OPERATION_ACTION = "wsdlOperationAction";
	/**
	 * value property 'mandatoryField'
	 */
	private final static String PRPTY_MANDATORY_FIELD = "mandatoryField";
	/**
	 * wsdl parser
	 */
	private WSDLParser mWSDLParser;
	/**
	 * common service
	 */
	private ExportConfigWsdlPathGenerator mExportConfigWsdlPathGenerator;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfigWsdlPathGenerator getExportConfigWsdlPathGenerator() {
		return mExportConfigWsdlPathGenerator;
	}

	public void setExportConfigWsdlPathGenerator(ExportConfigWsdlPathGenerator pExportConfigWsdlPathGenerator) {
		this.mExportConfigWsdlPathGenerator = pExportConfigWsdlPathGenerator;
	}

	public PropertyMappingInfo getPropertyMappingInfo() {
		return mPropertyMappingInfo;
	}

	public void setPropertyMappingInfo(PropertyMappingInfo pPropertyMappingInfo) {
		mPropertyMappingInfo = pPropertyMappingInfo;
	}

	public ExportRepositoryService getExportRepositoryService() {
		return mExportRepositoryService;
	}

	public void setExportRepositoryService(ExportRepositoryService pExportRepositoryService) {
		mExportRepositoryService = pExportRepositoryService;
	}

	public WSDLParser getWSDLParser() {
		return mWSDLParser;
	}

	public void setWSDLParser(WSDLParser pWSDLParser) {
		mWSDLParser = pWSDLParser;
	}

	public WSDLMappingValidator getWSDLMappingValidator() {
		return mWSDLMappingValidator;
	}

	public void setWSDLMappingValidator(WSDLMappingValidator pWSDLMappingValidator) {
		mWSDLMappingValidator = pWSDLMappingValidator;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * handle save user selection between wsdl and repository
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return
	 * @throws IOException      if error occur
	 * @throws ServletException if error occur
	 */
	public boolean handleSaveSelection(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		resetErrors();
		String exportConfigId = pRequest.getParameter(PARAM_EXPORT_CONFIG_ID);
		String wsdlUrl = pRequest.getParameter(PARAM_WSDL_URL);
		String wsdlUplaod = pRequest.getParameter(PARAM_WSDL_UPLAOD);
		String wsdlOperation = pRequest.getParameter(PARAM_WSDL_OPERATION);
		if (StringUtils.isBlank(wsdlOperation)) {
			addFormException(new DropletException("Operation is not selected."));
		} else {
			String wsdlOperationAction = pRequest.getParameter(PARAM_WSDL_OPERATION_ACTION);
			getPropertyMappingInfo().setWsdlUrl(getExportConfigWsdlPathGenerator().generateWsdlPath(
					exportConfigId,
					wsdlUrl,
					wsdlUplaod));
			getPropertyMappingInfo().setWsdlOperation(wsdlOperation);
			initPropertyMappingSelection(pRequest);

			ValidationResult result = getWSDLMappingValidator().isMappingValid(getPropertyMappingInfo());
			if (!result.isSuccess()) {
				addFormException(new DropletException(result.getFailReason()));
			} else {
				String mandatoryField = getValue().get(PRPTY_MANDATORY_FIELD);
				if (!getExportRepositoryService().storePropertyMappingSettings(
						getExportConfig(), getPropertyMappingInfo(), wsdlOperation, wsdlOperationAction, mandatoryField
				)) {
					addFormException(new DropletException("Unable to store property mappings. Check log."));
				}
			}
		}
		return checkFormRedirect(pRequest, pResponse);
	}


	/**
	 * init property mappings from request
	 *
	 * @param pRequest request
	 */
	private void initPropertyMappingSelection(DynamoHttpServletRequest pRequest) {
		getPropertyMappingInfo().resetMappings();

		Set<String> paramNames = pRequest.getParameterMap().keySet();

		for (String param : paramNames) {
			if (param.contains(typeFullPathPrefix)) {
				String paramValue = pRequest.getParameter(param);
				if (StringUtils.isBlank(paramValue) || (paramValue.indexOf(":") == paramValue.length() - 1)) {
					continue;
				}
				int index = param.indexOf(typeFullPathPrefix);
				String inout = param.substring(0, index);
				param = param.substring(index + typeFullPathPrefix.length());
				getPropertyMappingInfo().addPropertyMapping(
						initElementRepositoryPath(paramValue, inout, param)
				);
			} else if (param.contains(typeStaticValuePrefix)) {
				String paramValue = pRequest.getParameter(param);
				if (StringUtils.isBlank(paramValue)) {
					continue;
				}
				int index = param.indexOf(typeStaticValuePrefix);
				String inout = param.substring(0, index);
				param = param.substring(index + typeStaticValuePrefix.length());
				getPropertyMappingInfo().addPropertyMapping(
						initElementStaticValue(paramValue, inout, param)
				);
			}
		}

		vlogDebug("Property mappings: " + getPropertyMappingInfo().getPropertiesMapping().size() + " elements inited");
	}

	/**
	 * init PropertyMappingElement from request param
	 *
	 * @param pRepositoryPath repository path
	 * @param pInout          inout
	 * @param pWsdlPath       wsdl path
	 * @return PropertyMappingElement
	 */
	private PropertyMappingElement initElementRepositoryPath(String pRepositoryPath, String pInout, String pWsdlPath) {
		PropertyMappingElement element = new PropertyMappingElement();
		try {
			// /atg/commerce/order/OrderRepository:order.commerceItems.quantity
			// int indexOf = pRequestParam.indexOf(":");
			int indexOf = pRepositoryPath.indexOf(":");

			element.setRepositoryName(pRepositoryPath.substring(0, indexOf)); // /atg/commerce/order/OrderRepository
			element.setRepositoryPath(pRepositoryPath.substring(indexOf + 1)); // order.commerceItems.quantity
			element.setInout(pInout);

			// CreateOrder.Order.AtgOrderId
			element.setWsdlPath(pWsdlPath);

		} catch (Exception e) {
			vlogError(e, "Unable to parse param '" + pRepositoryPath + "'.");
		}
		return element;
	}

	/**
	 * init PropertyMappingElement from request param
	 *
	 * @param pStaticValue input value
	 * @param pWsdlPath    wsdl path
	 * @return PropertyMappingElement
	 */
	private PropertyMappingElement initElementStaticValue(String pStaticValue, String pInout, String pWsdlPath) {
		PropertyMappingElement element = new PropertyMappingElement();
		try {
			// input value
			element.setStaticValue(pStaticValue);

			// CreateOrder.Order.AtgOrderId
			element.setWsdlPath(pWsdlPath);
			element.setInout(pInout);

		} catch (Exception e) {
			vlogError(e, "Unable to init with static value param '" + pStaticValue + "'.");
		}
		return element;
	}
}
