package vsg.integration.export.handler;

import atg.droplet.GenericFormHandler;
import atg.json.JSONException;
import atg.repository.MutableRepository;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.ExportConfig;
import vsg.integration.export.util.AjaxUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class BaseFormHandler extends GenericFormHandler {
	/**
	 * export model
	 */
	private ExportConfig mExportConfig;

	/**
	 * Map of values
	 */
	private Map<String, String> mValue = new HashMap<String, String>();

	private MutableRepository mRepository;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfig getExportConfig() {
		return mExportConfig;
	}

	public void setExportConfig(ExportConfig pExportConfig) {
		mExportConfig = pExportConfig;
	}

	public Map<String, String> getValue() {
		return mValue;
	}

	public void setValue(Map<String, String> pValue) {
		mValue = pValue;
	}

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		this.mRepository = pRepository;
	}

	//------------------------------------------------------------------------------------------------------------------


	/**
	 * makes simple redirect of using ajax according to request
	 *
	 * @param pSuccessURL - success url
	 * @param pFailureURL - error url
	 * @param pRequest    - request
	 * @param pResponse   - response
	 * @param pDetails    - details (success message here)
	 * @return false, if redirect was successful
	 * @throws IOException      - if cannot make checkFormRedirect
	 * @throws ServletException - if cannot make checkFormRedirect
	 */
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
									 DynamoHttpServletResponse pResponse, String pDetails) throws ServletException, IOException {
		if (AjaxUtils.isAjaxRequest(pRequest)) {
			try {
				AjaxUtils.addAjaxResponse(this, pResponse, pDetails, pSuccessURL, pFailureURL, null);
			} catch (JSONException e) {
				if (isLoggingError()) {
					if (isLoggingError()) logError(e);
				}
			}
			return false;
		} else {
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}
	}

	/**
	 * makes simple redirect of using ajax according to request
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @return false, if redirect was successful
	 * @throws IOException      - if cannot make checkFormRedirect
	 * @throws ServletException - if cannot make checkFormRedirect
	 */
	public boolean checkFormRedirect(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		return checkFormRedirect(pRequest, pResponse, null);
	}

	/**
	 * makes simple redirect of using ajax according to request
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @param pParams   - param map
	 * @return false, if redirect was successful
	 * @throws IOException      - if cannot make checkFormRedirect
	 * @throws ServletException - if cannot make checkFormRedirect
	 */
	public boolean checkFormRedirect(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
									 Map<String, Object> pParams)
			throws ServletException, IOException {
		if (AjaxUtils.isAjaxRequest(pRequest)) {
			try {
				AjaxUtils.addAjaxResponse(this, pResponse, pParams);
			} catch (JSONException e) {
				if (isLoggingError()) {
					if (isLoggingError()) logError(e);
				}
			}
			return false;
		} else {
			return super.checkFormRedirect("succes_url", "error_url", pRequest, pResponse);
		}
	}

	/**
	 * reset all form handler errors
	 */
	public void resetErrors() {
		getFormExceptions().clear();
	}

}
