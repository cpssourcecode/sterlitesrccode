package vsg.integration.export.handler;

import atg.droplet.DropletException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.service.OutputReportingReviewService;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * The Class OutputReportingReviewFormHandler.
 *
 * @author "Vladimir Tretyakevich"
 */
public class OutputReportingReviewFormHandler extends BaseFormHandler implements IRepositoryConstants {

	/**
	 * Export Repository.
	 */
	private OutputReportingReviewService mOutputReportingReviewService;

	// ------------------------------------------------------------------------------------------------------------------

	/**
	 * Gets the output reporting review service.
	 *
	 * @return the output reporting review service
	 */
	public OutputReportingReviewService getOutputReportingReviewService() {
		return mOutputReportingReviewService;
	}

	/**
	 * Sets the output reporting review service.
	 *
	 * @param pOutputReportingReviewService the new output reporting review service
	 */
	public void setOutputReportingReviewService(OutputReportingReviewService pOutputReportingReviewService) {
		mOutputReportingReviewService = pOutputReportingReviewService;
	}

	// ------------------------------------------------------------------------------------------------------------------

	/**
	 * Handle test ftp.
	 *
	 * @param pRequest  the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws IOException      Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public boolean handleTestFtp(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		resetErrors();
		try {
			if (!getOutputReportingReviewService().testFtp(getExportConfig())) {
				addFormException(new DropletException("Test server call failed \n"));
			}
		} catch (Exception e) {
			vlogError(e, "Exception while invoking test server call");
			addFormException(new DropletException("Exception while invoking test server call \n"));
		}
		return checkFormRedirect(pRequest, pResponse);
	}

	/**
	 * Handle test email.
	 *
	 * @param pRequest  the request
	 * @param pResponse the response
	 * @return true, if successful
	 * @throws IOException      Signals that an I/O exception has occurred.
	 * @throws ServletException the servlet exception
	 */
	public boolean handleTestEmail(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		resetErrors();
		try {
			if (!getOutputReportingReviewService().testEmail(getExportConfig())) {
				addFormException(new DropletException("Test email call failed \n"));
			}
		} catch (Exception e) {
			vlogError(e, "Exception while invoking test email call");
			addFormException(new DropletException("Exception while invoking test email call \n"));
		}
		return checkFormRedirect(pRequest, pResponse);
	}

}
