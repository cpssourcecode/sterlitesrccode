package vsg.integration.export.store;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import org.w3c.dom.Element;
import vsg.integration.export.service.util.XMLTransformer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Date;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class ExportConfigExporter extends ExportConfigStoreProcessor {
	/**
	 * export dir for file creation for further upload
	 */
	private String mExportDir;

	/**
	 * generated export file name
	 */
	private String mExportFileName = null;

	//------------------------------------------------------------------------------------------------------------------

	public String getExportDir() {
		return mExportDir;
	}

	public void setExportDir(String pExportDir) {
		mExportDir = pExportDir;
	}

	private String getExportFileName() {
		return mExportFileName;
	}

	private void setExportFileName(String pExportFileName) {
		mExportFileName = pExportFileName;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * call process
	 *
	 * @param pExportConfigId export config id
	 * @return processed value
	 */
	public String process(String pExportConfigId) {
		setExportConfigId(pExportConfigId);
		return super.process();
	}

	@Override
	protected void preProcess() {
		super.preProcess();
		File exportDir = new File(getExportDir());
		if (!new File(getExportDir()).exists()) {
			if (exportDir.mkdir()) {
				throw new IllegalStateException("Export dir does not exist.");
			}
		}
		if (StringUtils.isBlank(getExportConfigId())) {
			throw new IllegalStateException("Export configuration ID is not specified.");
		}
		try {
			setExportConfigItem(getExportRepository().getItem(getExportConfigId(), ID_EXPORT_CONFIG));
		} catch (RepositoryException e) {
			trackError(e, "Unable to load export config repository item by id {" + getExportConfigId() + "}.");
		}
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			setDocument(docBuilder.newDocument());
		} catch (ParserConfigurationException e) {
			trackError(e, "Unable to build xml-document.");
		}
	}

	@Override
	protected void postProcess() {
		super.postProcess();
		try {
			Transformer transformer = XMLTransformer.getTransformer();
			DOMSource source = new DOMSource(getDocument());
			String exportFileName = getExportDir() + "/export_configuration_" + getExportConfigId() + "_" + (new Date())
					.getTime() + ".xml";
			File exportFile = new File(exportFileName);
			StreamResult result = new StreamResult(exportFile);
			transformer.transform(source, result);
			setExportFileName(exportFileName);
		} catch (TransformerException e) {
			vlogError(e, "Unable to create xml-file.");
			throw new IllegalStateException("Unable to create xml-file: " + e.toString());
		}
	}

	@Override
	protected String getProcessReturnValue() {
		return getExportFileName();
	}

	@Override
	protected void processExportConfig() {
		try {
			setRootElement(getDocument().createElement(ID_EXPORT_CONFIG));
			getDocument().appendChild(getRootElement());

			appendXmlElement(getExportConfigItem(), PRP_EXPORT_NAME, getRootElement());
			appendXmlElement(getExportConfigItem(), PRP_EXPORT_AUTHOR, getRootElement());
			appendXmlElement(getExportConfigItem(), PRP_EXPORT_DESCRIPTION, getRootElement());

			appendXmlElement(getExportConfigItem(), PRP_EXPORT_RQL, getRootElement());
			appendXmlElement(getExportConfigItem(), PRP_EXPORT_REPOSITORY_NAME, getRootElement());
			appendXmlElement(getExportConfigItem(), PRP_EXPORT_ITEM_DESCRIPTOR, getRootElement());

		} catch (Exception e) {
			trackError(e, "Unable to build export config part of xml-file.");
		}
	}

	@Override
	protected void processRecordMapping() {
		try {
			Set<RepositoryItem> recordMappings = (Set<RepositoryItem>)
					getExportConfigItem().getPropertyValue(PRP_EXPORT_RECORD_MAPPINGS);
			if (recordMappings != null && recordMappings.size() > 0) {
				Element recordMappingsElement = getDocument().createElement(PRP_EXPORT_RECORD_MAPPINGS);
				getRootElement().appendChild(recordMappingsElement);
				for (RepositoryItem recordMapping : recordMappings) {
					Element recordMappingElement = getDocument().createElement(ID_RECORD_MAPPING);
					recordMappingsElement.appendChild(recordMappingElement);
					appendXmlElement(recordMapping, PRP_RECORD_MAPPING_REPOSITORY_NAME, recordMappingElement);
					appendXmlElement(recordMapping, PRP_RECORD_MAPPING_REPOSITORY_PATH, recordMappingElement);
					appendXmlElement(recordMapping, PRP_RECORD_MAPPING_STATIC_VALUE, recordMappingElement);
					appendXmlElement(recordMapping, PRP_RECORD_MAPPING_INOUT, recordMappingElement);
					appendXmlElement(recordMapping, PRP_RECORD_MAPPING_WSDL_PATH, recordMappingElement);
				}
			}
		} catch (Exception e) {
			trackError(e, "Unable to build record mappings part of xml-file.");
		}
	}

	@Override
	protected void processConfigReport() {
		try {
			RepositoryItem outputReportConfig = (RepositoryItem)
					getExportConfigItem().getPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG);
			if (outputReportConfig != null) {
				Element reportConfigElement = getDocument().createElement(PRP_EXPORT_OUT_REPORT_CONFIG);
				getRootElement().appendChild(reportConfigElement);

				appendXmlElement(outputReportConfig, PRP_REP_CONF_SFREQ, reportConfigElement);
				appendXmlElement(outputReportConfig, PRP_REP_CONF_EFREQ, reportConfigElement);
				appendXmlElement(outputReportConfig, PRP_REP_CONF_FILE_NAME, reportConfigElement);
				appendXmlElement(outputReportConfig, PRP_REP_CONF_TIME_FORMAT, reportConfigElement);
				appendXmlElement(outputReportConfig, PRP_REP_CONF_STORE_PATH, reportConfigElement);
				appendXmlElement(outputReportConfig, PRP_REP_CONF_ARCHIVE_PATH, reportConfigElement);

				processConfigReportEmailConfig(outputReportConfig, reportConfigElement);
				processConfigReportFtpConfig(outputReportConfig, reportConfigElement);
			}
		} catch (Exception e) {
			trackError(e, "Unable to build report config part of xml-file.");
		}
	}

	/**
	 * initalize email config for report config
	 *
	 * @param pOutputReportConfig  report config repository item
	 * @param pReportConfigElement report config xml element
	 */
	private void processConfigReportEmailConfig(RepositoryItem pOutputReportConfig, Element pReportConfigElement) {
		RepositoryItem emailConfigItem = (RepositoryItem)
				pOutputReportConfig.getPropertyValue(PRP_REP_CONF_MAIL_CONF);
		if (emailConfigItem != null) {
			Element emailConfigElement = getDocument().createElement(PRP_REP_CONF_MAIL_CONF);
			pReportConfigElement.appendChild(emailConfigElement);

			appendXmlElement(emailConfigItem, PRP_EMAIL_CONF_SUBJ, emailConfigElement);
			appendXmlElement(emailConfigItem, PRP_EMAIL_CONF_SENDER, emailConfigElement);
			appendXmlElement(emailConfigItem, PRP_EMAIL_CONF_RECIPIENTS, emailConfigElement);
		}
	}

	/**
	 * initalize ftp config for report config
	 *
	 * @param pOutputReportConfig  report config repository item
	 * @param pReportConfigElement report config xml element
	 */
	private void processConfigReportFtpConfig(RepositoryItem pOutputReportConfig, Element pReportConfigElement) {
		RepositoryItem ftpConfigItem = (RepositoryItem)
				pOutputReportConfig.getPropertyValue(PRP_REP_CONF_FTP_CONF);
		if (ftpConfigItem != null) {
			Element ftpConfigElement = getDocument().createElement(PRP_REP_CONF_FTP_CONF);
			pReportConfigElement.appendChild(ftpConfigElement);

			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_PROTOCOL, ftpConfigElement);
			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_KEY_FILE_NAME, ftpConfigElement);
			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_HOST_NAME, ftpConfigElement);
			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_OUT_DIR_PATH, ftpConfigElement);
			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_USER_NAME, ftpConfigElement);
			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_PASS, ftpConfigElement);
			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_ENC_KEY_PATH, ftpConfigElement);
			appendXmlElement(ftpConfigItem, PRP_FTP_CONF_ENC_PHRASE, ftpConfigElement);
		}
	}

	@Override
	protected void processConfigOutputFormat() {
		try {
			RepositoryItem outputFormatConfig = (RepositoryItem)
					getExportConfigItem().getPropertyValue(PRP_EXPORT_OUTPUT_FORMAT);
			if (outputFormatConfig != null) {
				Element outputFormatElement = getDocument().createElement(PRP_EXPORT_OUTPUT_FORMAT);
				getRootElement().appendChild(outputFormatElement);

				appendXmlElement(outputFormatConfig, PRP_OFC_WS_SEQURITY, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_UPLOAD_WSDL, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_WSDL_URL, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_WSDL_OPERATION, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_WSDL_OPERATION_ACTION, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_ENDPOINT_URL, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_CONNECTION_TIMEOUT, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_USE_SCHEDULE, outputFormatElement);
				appendXmlElement(outputFormatConfig, PRP_OFC_SCHEDULE, outputFormatElement);
			}
		} catch (Exception e) {
			trackError(e, "Unable to build output format config part of xml-file.");
		}
	}

	@Override
	protected void processConfigTransferOptions() {
		try {
			RepositoryItem transferOptionsConfig = (RepositoryItem)
					getExportConfigItem().getPropertyValue(PRP_EXPORT_TRANSFER_OPTIONS);
			if (transferOptionsConfig != null) {
				Element transferOptionsElement = getDocument().createElement(PRP_EXPORT_TRANSFER_OPTIONS);
				getRootElement().appendChild(transferOptionsElement);

				appendXmlElement(transferOptionsConfig, PRP_TOC_UPLOAD_XSD, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_CONTROL_FILE, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_OUTPUT_DIRECTORY, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_PROTOCOL, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_KEY, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_URL_IP, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_USER_NAME, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_CONFIRM_PSWD, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_PREFIX, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_FILE_NAME, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_TIME_STEP_FORMAT, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_SUFIX, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_ENCRIPTION_KEY, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_ENCRIPTION_PHRASE, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_SCHEDULE, transferOptionsElement);
				appendXmlElement(transferOptionsConfig, PRP_TOC_SINGLE_PER_FILE, transferOptionsElement);
			}
		} catch (Exception e) {
			trackError(e, "Unable to build transfer config part of xml-file.");
		}
	}
}
