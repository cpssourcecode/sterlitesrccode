package vsg.integration.export.store;

/**
 * @author Dmitry Golubev
 */
public interface IExportConfigStoreProcessor {
	/**
	 * process store functionality
	 *
	 * @return export config item id
	 */
	String process();

	/**
	 * process store functionality
	 *
	 * @param pExportConfigId export config id to use
	 * @return export config item id
	 */
	String process(String pExportConfigId);
}
