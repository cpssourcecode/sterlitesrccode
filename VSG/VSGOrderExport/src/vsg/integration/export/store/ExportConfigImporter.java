package vsg.integration.export.store;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import org.w3c.dom.Node;
import vsg.integration.export.parse.wsdl.DomUtil;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class ExportConfigImporter extends ExportConfigStoreProcessor {
	/**
	 * import dir for file upload for further parse and import
	 */
	private String mImportDir;
	/**
	 * current import file
	 */
	private String mImportFile;

	//------------------------------------------------------------------------------------------------------------------

	public String getImportDir() {
		return mImportDir;
	}

	public void setImportDir(String pImportDir) {
		mImportDir = pImportDir;
	}

	private String getImportFile() {
		return mImportFile;
	}

	private void setImportFile(String pImportFile) {
		mImportFile = pImportFile;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * call process
	 *
	 * @param pImportFileName export config id
	 */
	public void process(String pImportFileName) {
		setImportFile(pImportFileName);
		super.process();
	}

	@Override
	protected void preProcess() {
		super.preProcess();

		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true); // never forget this!
			DocumentBuilder builder = factory.newDocumentBuilder();
			setDocument(builder.parse(getImportFile()));
		} catch (Exception e) {
			vlogError("Unable to parse xml-document.");
			throw new IllegalStateException(e);
		}
	}

	@Override
	protected void postProcess() {
		super.postProcess();
		try {
			((MutableRepository) getExportRepository()).updateItem((MutableRepositoryItem) getExportConfigItem());
			setExportConfigId(getExportConfigItem().getRepositoryId());
		} catch (RepositoryException e) {
			vlogError("Unable to store inited export config repository item.");
			throw new IllegalStateException(e);
		}
	}

	@Override
	protected void processExportConfig() {
		try {
			Node exportConfigNode = DomUtil.getNodeByName(getDocument().getChildNodes(), ID_EXPORT_CONFIG);
			setRootElement(exportConfigNode);

			setExportConfigItem(createItem(ID_EXPORT_CONFIG));
			((MutableRepositoryItem) getExportConfigItem()).setPropertyValue(PRP_EXPORT_DATE_CREATE, new Date());

			initRepositoryItemByXmlElement(getExportConfigItem(), PRP_EXPORT_NAME, exportConfigNode);
			initRepositoryItemByXmlElement(getExportConfigItem(), PRP_EXPORT_AUTHOR, exportConfigNode);
			initRepositoryItemByXmlElement(getExportConfigItem(), PRP_EXPORT_DESCRIPTION, exportConfigNode);

			initRepositoryItemByXmlElement(getExportConfigItem(), PRP_EXPORT_RQL, exportConfigNode);
			initRepositoryItemByXmlElement(getExportConfigItem(), PRP_EXPORT_REPOSITORY_NAME, exportConfigNode);
			initRepositoryItemByXmlElement(getExportConfigItem(), PRP_EXPORT_ITEM_DESCRIPTOR, exportConfigNode);

			((MutableRepository) getExportRepository()).addItem((MutableRepositoryItem) getExportConfigItem());

		} catch (RepositoryException e) {
			vlogError("Unable to init export config from xml-file.");
			throw new IllegalStateException(e);
		}
	}

	@Override
	protected void processRecordMapping() {
		try {
			Node propertyMappingsNode = DomUtil.getNodeByName(getRootElement().getChildNodes(),
					PRP_EXPORT_RECORD_MAPPINGS);
			if (propertyMappingsNode != null) {
				Set<RepositoryItem> propertyMappingsSet = new HashSet<RepositoryItem>();
				for (int i = 0; i < propertyMappingsNode.getChildNodes().getLength(); i++) {
					Node propertyMappingNode = propertyMappingsNode.getChildNodes().item(i);

					RepositoryItem propertyMappingItem = createItem(ID_RECORD_MAPPING);
					initRepositoryItemByXmlElement(propertyMappingItem, PRP_RECORD_MAPPING_REPOSITORY_NAME, propertyMappingNode);
					initRepositoryItemByXmlElement(propertyMappingItem, PRP_RECORD_MAPPING_REPOSITORY_PATH, propertyMappingNode);
					initRepositoryItemByXmlElement(propertyMappingItem, PRP_RECORD_MAPPING_STATIC_VALUE, propertyMappingNode);
					initRepositoryItemByXmlElement(propertyMappingItem, PRP_RECORD_MAPPING_INOUT, propertyMappingNode);
					initRepositoryItemByXmlElement(propertyMappingItem, PRP_RECORD_MAPPING_WSDL_PATH, propertyMappingNode);

					((MutableRepository) getExportRepository()).addItem((MutableRepositoryItem) propertyMappingItem);

					propertyMappingsSet.add(propertyMappingItem);
				}

				((MutableRepositoryItem) getExportConfigItem()).setPropertyValue(PRP_EXPORT_RECORD_MAPPINGS, propertyMappingsSet);
			}

		} catch (RepositoryException e) {
			vlogError("Unable to init record mappings from xml-file.");
			throw new IllegalStateException(e);
		}
	}

	@Override
	protected void processConfigReport() {
		try {
			Node reportConfigNode = DomUtil.getNodeByName(getRootElement().getChildNodes(), PRP_EXPORT_OUT_REPORT_CONFIG);
			if (reportConfigNode != null) {
				RepositoryItem reportConfigItem = createItem(ID_OUT_REPORT_CONFIG);

				initRepositoryItemByXmlElement(reportConfigItem, PRP_REP_CONF_SFREQ, reportConfigNode);
				initRepositoryItemByXmlElement(reportConfigItem, PRP_REP_CONF_EFREQ, reportConfigNode);
				initRepositoryItemByXmlElement(reportConfigItem, PRP_REP_CONF_FILE_NAME, reportConfigNode);
				initRepositoryItemByXmlElement(reportConfigItem, PRP_REP_CONF_TIME_FORMAT, reportConfigNode);
				initRepositoryItemByXmlElement(reportConfigItem, PRP_REP_CONF_STORE_PATH, reportConfigNode);
				initRepositoryItemByXmlElement(reportConfigItem, PRP_REP_CONF_ARCHIVE_PATH, reportConfigNode);

				((MutableRepository) getExportRepository()).addItem((MutableRepositoryItem) reportConfigItem);

				((MutableRepositoryItem) getExportConfigItem()).setPropertyValue(PRP_EXPORT_OUT_REPORT_CONFIG,
						reportConfigItem);

				processConfigReportEmailConfig(reportConfigItem, reportConfigNode);
				processConfigReportFtpConfig(reportConfigItem, reportConfigNode);
			}
		} catch (RepositoryException e) {
			vlogError("Unable to init report config from xml-file.");
			throw new IllegalStateException(e);
		}
	}

	/**
	 * initalize email config for report config
	 *
	 * @param pReportConfigItem    report config repository item
	 * @param pReportConfigElement report config xml element
	 */
	private void processConfigReportEmailConfig(RepositoryItem pReportConfigItem, Node pReportConfigElement)
			throws RepositoryException {

		Node reportEmailConfigNode = DomUtil.getNodeByName(pReportConfigElement.getChildNodes(), PRP_REP_CONF_MAIL_CONF);
		if (reportEmailConfigNode != null) {
			RepositoryItem reportEmailConfigItem = createItem(ID_EMAIL_DELIVERY_CONFIG);

			initRepositoryItemByXmlElement(reportEmailConfigItem, PRP_EMAIL_CONF_SUBJ, reportEmailConfigNode);
			initRepositoryItemByXmlElement(reportEmailConfigItem, PRP_EMAIL_CONF_SENDER, reportEmailConfigNode);
			initRepositoryItemByXmlElement(reportEmailConfigItem, PRP_EMAIL_CONF_RECIPIENTS, reportEmailConfigNode);

			((MutableRepository) getExportRepository()).addItem((MutableRepositoryItem) reportEmailConfigItem);

			((MutableRepositoryItem) pReportConfigItem).setPropertyValue(PRP_REP_CONF_MAIL_CONF, reportEmailConfigItem);
		}
	}

	/**
	 * initalize ftp config for report config
	 *
	 * @param pReportConfigItem    report config repository item
	 * @param pReportConfigElement report config xml element
	 */
	private void processConfigReportFtpConfig(RepositoryItem pReportConfigItem, Node pReportConfigElement)
			throws RepositoryException {
		Node reportFtpConfigNode = DomUtil.getNodeByName(pReportConfigElement.getChildNodes(), PRP_REP_CONF_FTP_CONF);
		if (reportFtpConfigNode != null) {
			RepositoryItem reportFtpConfigItem = createItem(ID_FTP_DELIVERY_CONFIG);

			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_PROTOCOL, reportFtpConfigNode);
			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_KEY_FILE_NAME, reportFtpConfigNode);
			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_HOST_NAME, reportFtpConfigNode);
			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_OUT_DIR_PATH, reportFtpConfigNode);
			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_USER_NAME, reportFtpConfigNode);
			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_PASS, reportFtpConfigNode);
			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_ENC_KEY_PATH, reportFtpConfigNode);
			initRepositoryItemByXmlElement(reportFtpConfigItem, PRP_FTP_CONF_ENC_PHRASE, reportFtpConfigNode);

			((MutableRepository) getExportRepository()).addItem((MutableRepositoryItem) reportFtpConfigItem);

			((MutableRepositoryItem) pReportConfigItem).setPropertyValue(PRP_REP_CONF_FTP_CONF, reportFtpConfigItem);
		}
	}

	@Override
	protected void processConfigOutputFormat() {
		try {
			Node outputFormatNode = DomUtil.getNodeByName(getRootElement().getChildNodes(), PRP_EXPORT_OUTPUT_FORMAT);
			if (outputFormatNode != null) {
				RepositoryItem outputFormatItem = createItem(ID_OUTPUT_FORMAT_CONFIG);

				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_WS_SEQURITY, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_UPLOAD_WSDL, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_WSDL_URL, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_WSDL_OPERATION, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_WSDL_OPERATION_ACTION, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_ENDPOINT_URL, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_CONNECTION_TIMEOUT, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_USE_SCHEDULE, outputFormatNode);
				initRepositoryItemByXmlElement(outputFormatItem, PRP_OFC_SCHEDULE, outputFormatNode);

				((MutableRepository) getExportRepository()).addItem((MutableRepositoryItem) outputFormatItem);

				((MutableRepositoryItem) getExportConfigItem()).setPropertyValue(PRP_EXPORT_OUTPUT_FORMAT,
						outputFormatItem);
			}

		} catch (RepositoryException e) {
			vlogError("Unable to init output format config from xml-file.");
			throw new IllegalStateException(e);
		}
	}

	@Override
	protected void processConfigTransferOptions() {
		try {
			Node transferOptionsNode = DomUtil.getNodeByName(getRootElement().getChildNodes(), PRP_EXPORT_TRANSFER_OPTIONS);
			if (transferOptionsNode != null) {
				RepositoryItem transferOptionsItem = createItem(ID_TRANSFER_OPTIONS_CONFIG);

				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_UPLOAD_XSD, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_CONTROL_FILE, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_OUTPUT_DIRECTORY, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_PROTOCOL, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_KEY, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_URL_IP, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_USER_NAME, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_CONFIRM_PSWD, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_PREFIX, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_FILE_NAME, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_TIME_STEP_FORMAT, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_SUFIX, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_ENCRIPTION_KEY, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_ENCRIPTION_PHRASE, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_SCHEDULE, transferOptionsNode);
				initRepositoryItemByXmlElement(transferOptionsItem, PRP_TOC_SINGLE_PER_FILE, transferOptionsNode);

				((MutableRepository) getExportRepository()).addItem((MutableRepositoryItem) transferOptionsItem);

				((MutableRepositoryItem) getExportConfigItem()).setPropertyValue(PRP_EXPORT_TRANSFER_OPTIONS,
						transferOptionsItem);
			}

		} catch (RepositoryException e) {
			vlogError("Unable to init transfer config from xml-file.");
			throw new IllegalStateException(e);
		}
	}

	/**
	 * create item by specified item descriptor name
	 *
	 * @param pItemDescriptor item descriptor name
	 * @return repository item
	 * @throws RepositoryException if error occurs
	 */
	private RepositoryItem createItem(String pItemDescriptor) throws RepositoryException {
		return ((MutableRepository) getExportRepository()).createItem(pItemDescriptor);
	}
}
