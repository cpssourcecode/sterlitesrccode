package vsg.integration.export.store;

import atg.beans.DynamicPropertyDescriptor;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.nucleus.GenericService;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.parse.wsdl.DomUtil;
import vsg.integration.export.util.ObjectStringFormatter;

import javax.transaction.Status;
import javax.transaction.TransactionManager;

/**
 * @author Dmitry Golubev
 */
public abstract class ExportConfigStoreProcessor extends GenericService implements IRepositoryConstants {

	/**
	 * transaction manager
	 */
	private TransactionManager mTransactionManager;
	/**
	 * export repository
	 */
	private Repository mExportRepository;
	/**
	 * export config repository item
	 */
	private RepositoryItem mExportConfigItem;
	/**
	 * object to string formatter
	 */
	private ObjectStringFormatter mObjectStringFormatter;
	/**
	 * current document
	 */
	private Document mDocument;
	/**
	 * root xml element
	 */
	private Node mRootElement;

	/**
	 * export config id for export
	 */
	private String mExportConfigId;

	//------------------------------------------------------------------------------------------------------------------

	public String getExportConfigId() {
		return mExportConfigId;
	}

	public void setExportConfigId(String pExportConfigId) {
		mExportConfigId = pExportConfigId;
	}

	public Repository getExportRepository() {
		return mExportRepository;
	}

	public void setExportRepository(Repository pExportRepository) {
		mExportRepository = pExportRepository;
	}

	public RepositoryItem getExportConfigItem() {
		return mExportConfigItem;
	}

	public void setExportConfigItem(RepositoryItem pExportConfigItem) {
		mExportConfigItem = pExportConfigItem;
	}

	public ObjectStringFormatter getObjectStringFormatter() {
		return mObjectStringFormatter;
	}

	public void setObjectStringFormatter(ObjectStringFormatter pObjectStringFormatter) {
		mObjectStringFormatter = pObjectStringFormatter;
	}

	public Document getDocument() {
		return mDocument;
	}

	public void setDocument(Document pDocument) {
		mDocument = pDocument;
	}

	protected Node getRootElement() {
		return mRootElement;
	}

	protected void setRootElement(Node pRootElement) {
		mRootElement = pRootElement;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * pre process functionality
	 */
	protected void preProcess() {

	}

	/**
	 * post process functionality
	 */
	protected void postProcess() {

	}

	/**
	 * @return result value of process method
	 */
	protected String getProcessReturnValue() {
		return getExportConfigId();
	}

	/**
	 * process work with export config
	 *
	 * @return process return value
	 */
	protected final String process() {
		synchronized (this) {
			TransactionDemarcation td = new TransactionDemarcation();
			boolean isFailed = false;
			try {
				td.begin(getTransactionManager());
				preProcess();

				processExportConfig();
				processRecordMapping();
				processConfigReport();
				processConfigOutputFormat();
				processConfigTransferOptions();

				postProcess();
			} catch (Exception e) {
				isFailed = true;
				if(e instanceof RuntimeException) {
					throw (RuntimeException)e;
				} else {
					throw new IllegalStateException(e);
				}
			} finally {
				transactionEnd(td, isFailed);
			}
			return getProcessReturnValue();
		}
	}

	/**
	 * @param pDemarcation transaction demacration
	 * @param pIsfailed    is transaction fialed
	 */
	private void transactionEnd(TransactionDemarcation pDemarcation, boolean pIsfailed) {
		try {
			if (pDemarcation.getTransaction() != null) {
				int status = pDemarcation.getTransaction().getStatus();
				if (status == Status.STATUS_ACTIVE) {
					pDemarcation.end(pIsfailed);
				} else {
					pDemarcation.end(true);
				}
			}
		} catch (Exception e) {
			trackError(e, "Error in transaction");
		}
	}


	/**
	 * process export config
	 */
	protected abstract void processExportConfig();

	/**
	 * process export config record mapping
	 */
	protected abstract void processRecordMapping();

	/**
	 * process export report config
	 */
	protected abstract void processConfigReport();

	/**
	 * process export output format config
	 */
	protected abstract void processConfigOutputFormat();

	/**
	 * process export transfer options config
	 */
	protected abstract void processConfigTransferOptions();

	/**
	 * log error and throw store exception
	 *
	 * @param pException exception
	 * @param pMessage   error message
	 */
	protected void trackError(Exception pException, String pMessage) {
		vlogError(pException, pMessage);
		throw new IllegalStateException(pMessage + " " + pException.toString());
	}

	/**
	 * @param pValue object to transform to string
	 * @return string presentation of object
	 */
	protected String getValue(Object pValue) {
		return getObjectStringFormatter().formatToString(pValue);
	}

	/**
	 * append new element to document based on property from repository item
	 *
	 * @param pItem          repository item
	 * @param pProperty      propery to get
	 * @param pParentElement parent xml element
	 */
	protected void appendXmlElement(RepositoryItem pItem, String pProperty, Node pParentElement) {
		String rqlValue = getValue(pItem.getPropertyValue(pProperty));
		if (rqlValue != null) {
			Element rql = getDocument().createElement(pProperty);
			rql.appendChild(getDocument().createTextNode(rqlValue));
			pParentElement.appendChild(rql);
		}
	}

	/**
	 * initialize repositoru item with propery from xml
	 *
	 * @param pItem     repository item
	 * @param pProperty propery to get
	 * @param pXmlNode  xml element
	 */
	protected void initRepositoryItemByXmlElement(RepositoryItem pItem, String pProperty, Node pXmlNode)
			throws RepositoryException {
		Node propertyNode = DomUtil.getNodeByName(pXmlNode.getChildNodes(), pProperty);
		if (propertyNode != null && propertyNode.getChildNodes().getLength() > 0) {
			String value = propertyNode.getChildNodes().item(0).getNodeValue();
			if (!StringUtils.isBlank(value)) {
				DynamicPropertyDescriptor descriptor = pItem.getItemDescriptor().getPropertyDescriptor(pProperty);
				((MutableRepositoryItem) pItem).setPropertyValue(pProperty,
						getObjectStringFormatter().formatToObject(value, descriptor.getPropertyType()));
			}
		}
	}
}
