package vsg.integration.export.parse.wsdl.bean;

/**
 * allowed values:
 * <br>
 * <ol>
 * <li>ELEMENT</li>
 * <li>SIMPLE</li>
 * <li>COMPLEX</li>
 * </ol>
 *
 * @author Dmitry Golubev
 */
public enum NodeType {
	/**
	 * type element
	 */
	ELEMENT("ELEMENT"),
	/**
	 * type simple
	 */
	SIMPLE("SIMPLE"),
	/**
	 * type complex (may contain all other types)
	 */
	COMPLEX("COMPLEX");

	/**
	 * string type presentation
	 */
	private String mValue;

	/**
	 * constructor
	 *
	 * @param pValue string type presentation
	 */
	NodeType(String pValue) {
		mValue = pValue;
	}

	/**
	 * @return string type presentation
	 */
	@Override
	public String toString() {
		return mValue;
	}
}
