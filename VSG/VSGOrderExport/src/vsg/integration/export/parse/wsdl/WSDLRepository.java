package vsg.integration.export.parse.wsdl;

import atg.nucleus.GenericService;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class WSDLRepository extends GenericService {
	/**
	 * map with wsdl and its' model
	 */
	Map<String, WSDLModel> mWSDLModels = new HashMap<String, WSDLModel>();

	/**
	 * reset repository analyzed models
	 */
	public void resetState() {
		mWSDLModels.clear();
	}

	/**
	 * @param pWsdlUri wsdl uri
	 * @return true if current wsdl uri is parsed and registered
	 */
	public boolean isRegistered(String pWsdlUri) {
		return mWSDLModels.containsKey(pWsdlUri);
	}

	/**
	 * @param pWsdlUri wsdl uri
	 * @return parsed model by specified uri, other case - null
	 */
	public WSDLModel get(String pWsdlUri) {
		return mWSDLModels.get(pWsdlUri);
	}

	/**
	 * register wsdl model by provided wsdl uri
	 *
	 * @param pWsdlUri   wsdl uri
	 * @param pWSDLModel wsdl model
	 */
	public void register(String pWsdlUri, WSDLModel pWSDLModel) {
		mWSDLModels.put(pWsdlUri, pWSDLModel);
	}
}
