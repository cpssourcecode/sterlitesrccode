package vsg.integration.export.parse.wsdl.bean;

/**
 * for example:<br>
 * <b>wsdl-file part:</b>
 * <pre> {@code
 * <definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tns="http://example/"
 * xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://schemas.xmlsoap.org/wsdl/"
 * targetNamespace="http://example/" name="TestOrderWSService">
 * <types>
 * <xsd:schema>
 * <xsd:import namespace="http://example/" schemaLocation="http://localhost:8180/services/TestOrderWS?xsd=1"/>
 * </xsd:schema>
 * </types>
 * } </pre><br>
 * <b>xsd-file part:</b>
 * <pre> {@code <xs:schema version="1.0" targetNamespace="http://example/">
 * } </pre>
 * <br>
 * <b>result:</b>
 * <br>name: http://example/
 * <br>prefix: tns
 * <br>location: http://localhost:8180/services/TestOrderWS?xsd=1
 *
 * @author Dmitry Golubev
 */
public class TargetNamespace implements Cloneable {
	/**
	 * prefix
	 */
	private String mPrefix;
	/**
	 * name
	 */
	private String mNamespace;
	/**
	 * location
	 */
	private String mSchemaLocation;

	/**
	 * main flag
	 */
	private boolean mMain = false;

	//------------------------------------------------------------------------------------------------------------------

	public String getNamespace() {
		return mNamespace;
	}

	public void setNamespace(String pNamespace) {
		mNamespace = pNamespace;
	}

	public String getPrefix() {
		return mPrefix;
	}

	public void setPrefix(String pPrefix) {
		mPrefix = pPrefix;
	}

	public String getSchemaLocation() {
		return mSchemaLocation;
	}

	public void setSchemaLocation(String pSchemaLocation) {
		mSchemaLocation = pSchemaLocation;
	}

	public boolean isMain() {
		return mMain;
	}

	public void setMain(boolean pMain) {
		mMain = pMain;
	}


	//------------------------------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		String info = "";
		if (getPrefix() != null) {
			info += getPrefix() + ":";
		}
		info += getNamespace();
		return info;
	}
}
