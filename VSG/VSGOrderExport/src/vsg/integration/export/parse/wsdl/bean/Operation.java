package vsg.integration.export.parse.wsdl.bean;

/**
 * class for operation elements<br>
 * <pre> {@code <wsdl:operation name="...">
 *   <wsdl:input message="tns:.."/>
 *   <wsdl:output message="tns:..."/>
 * </wsdl:operation>
 * }
 * </pre>
 *
 * @author Dmitry Golubev
 */
public class Operation implements Cloneable {
	/**
	 * operation name
	 */
	private String mName;
	/**
	 * operation action
	 */
	private String mAction;
	/**
	 * operation message input
	 */
	private Message mMessageInput;
	/**
	 * operation message output
	 */
	private Message mMessageOutput;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 */
	public Operation() {
	}

	/**
	 * constructor
	 *
	 * @param pName          operation name
	 * @param pMessageInput  operation input action
	 * @param pMessageOutput operation output action
	 */
	public Operation(String pName, String pAction, Message pMessageInput, Message pMessageOutput) {
		mName = pName;
		mAction = pAction;
		mMessageInput = pMessageInput;
		mMessageOutput = pMessageOutput;
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getName() {
		return mName;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public Message getMessageInput() {
		return mMessageInput;
	}

	public void setMessageInput(Message pMessageInput) {
		mMessageInput = pMessageInput;
	}

	public Message getMessageOutput() {
		return mMessageOutput;
	}

	public void setMessageOutput(Message pMessageOutput) {
		mMessageOutput = pMessageOutput;
	}

	public String getAction() {
		return mAction;
	}

	public void setAction(String pAction) {
		mAction = pAction;
	}
}
