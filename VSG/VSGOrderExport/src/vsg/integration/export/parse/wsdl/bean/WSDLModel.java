package vsg.integration.export.parse.wsdl.bean;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import vsg.integration.export.util.comparator.ComparatorTargetNamespace;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Dmitry Golubev
 */
public class WSDLModel {
	/************************************** TO STORE ******************************************************************/
	/**
	 * wsdl
	 */
	private String mWsdl;
	/**
	 * soap address location
	 */
	private String mSOAPAddressLocation;
	/**
	 * targetNamespace for defined types
	 */
	private String mTargetNamespace;
	/**
	 * list of element messages
	 */
	private List<Message> mMessages = new ArrayList<Message>();
	/**
	 * list of element operations
	 */
	private List<Operation> mOperations = new ArrayList<Operation>();
	/**
	 * http://schemas.xmlsoap.org/soap/encoding/
	 */
	private TargetNamespace mTargetNamespaceSoapEncoding;

	/**************************************~TO STORE~******************************************************************/
	/**
	 * wsdl document
	 */
	private Document mWsdlDocument = null;
	/**
	 * map of types by their names
	 */
	private Set<Type> mWsdlTypes = new HashSet<Type>();
	// private HashMap<String, Type> mWsdlTypes = new HashMap<String, Type>();
	/**
	 * list of simple type nodes
	 */
	private List<Node> mNodesOfSimpleTypes = new ArrayList<Node>();
	/**
	 * list of complex type nodes
	 */
	private List<Node> mNodesOfComplexTypes = new ArrayList<Node>();
	/**
	 * list of complex type nodes with array
	 */
	private List<Node> mNodesOfComplexTypesWithArray = new ArrayList<Node>();
	/**
	 * list of element type nodes
	 */
	private List<Node> mNodesOfElementsTopLevel = new ArrayList<Node>();
	/**
	 * list of target namespaces
	 */
	/*private Set<TargetNamespace> mTargetNamespaces = new HashSet<TargetNamespace>();*/
	private Set<TargetNamespace> mTargetNamespaces = new TreeSet<TargetNamespace>(
			ComparatorTargetNamespace.instanse()
	);
	/**
	 * list of target namespaces prefixes defined under difinitions
	 */
	private Set<String> mTargetNamespacesPrefixesFromDefinition = new HashSet<>();
	/**
	 * list of target namespaces on main document
	 */
	/*private Set<TargetNamespace> mMainDocumentTargetNamespaces = new HashSet<TargetNamespace>();*/
	private Set<TargetNamespace> mMainDocumentTargetNamespaces = new TreeSet<TargetNamespace>(
			ComparatorTargetNamespace.instanse()
	);
	/**
	 * list of common target namespaces
	 */
/*
	private Set<TargetNamespace> mCommonTargetNamespaces = new HashSet<TargetNamespace>();
*/
	private Set<TargetNamespace> mCommonTargetNamespaces = new TreeSet<TargetNamespace>(
			ComparatorTargetNamespace.instanse()
	);

	public TargetNamespace getTargetNamespaceSoapEncoding() {
		return mTargetNamespaceSoapEncoding;
	}

	public void setTargetNamespaceSoapEncoding(TargetNamespace pTargetNamespaceSoapEncoding) {
		mTargetNamespaceSoapEncoding = pTargetNamespaceSoapEncoding;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pWsdl wsdl to analyze
	 */
	public WSDLModel(String pWsdl) {
		setWsdl(pWsdl);
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getWsdl() {
		return mWsdl;
	}

	public void setWsdl(String pWsdl) {
		mWsdl = pWsdl;
	}

	public String getSOAPAddressLocation() {
		return mSOAPAddressLocation;
	}

	public void setSOAPAddressLocation(String pSOAPAddressLocation) {
		mSOAPAddressLocation = pSOAPAddressLocation;
	}

	public String getTargetNamespace() {
		return mTargetNamespace;
	}

	public void setTargetNamespace(String pTargetNamespace) {
		mTargetNamespace = pTargetNamespace;
	}

	public List<Message> getMessages() {
		return mMessages;
	}

	void setMessages(List<Message> pMessages) {
		mMessages = pMessages;
	}

	public List<Operation> getOperations() {
		return mOperations;
	}

	void setOperations(List<Operation> pOperations) {
		mOperations = pOperations;
	}

	/**
	 * @param pOperationName operation name
	 * @return operation by specified name
	 */
	public Operation getOperation(String pOperationName) {
		if (getOperations() == null) {
			return null;
		}
		for (Operation operation : getOperations()) {
			if (operation.getName().equals(pOperationName)) {
				return operation;
			}
		}
		return null;
	}

	/**
	 * @param pMessageName message name
	 * @return message by specified name
	 */
	public Message getMessage(String pMessageName) {
		if (getMessages() == null) {
			return null;
		}
		for (Message message : getMessages()) {
			if (message.getName().equals(pMessageName)) {
				return message;
			}
		}
		return null;
	}

	/**
	 * add operation to model
	 *
	 * @param pOperation operation
	 */
	void addOperation(Operation pOperation) {
		mOperations.add(pOperation);
	}

	public Set<Type> getWsdlTypes() {
		return mWsdlTypes;
	}

	public List<Node> getNodesOfSimpleTypes() {
		return mNodesOfSimpleTypes;
	}

	void setNodesOfSimpleTypes(List<Node> pNodesOfSimpleTypes) {
		mNodesOfSimpleTypes = pNodesOfSimpleTypes;
	}

	public List<Node> getNodesOfComplexTypes() {
		return mNodesOfComplexTypes;
	}

	void setNodesOfComplexTypes(List<Node> pNodesOfComplexTypes) {
		mNodesOfComplexTypes = pNodesOfComplexTypes;
	}

	public List<Node> getNodesOfComplexTypesWithArray() {
		return mNodesOfComplexTypesWithArray;
	}

	public void setNodesOfComplexTypesWithArray(List<Node> pNodesOfComplexTypesWithArray) {
		mNodesOfComplexTypesWithArray = pNodesOfComplexTypesWithArray;
	}

	public List<Node> getNodesOfElementsTopLevel() {
		return mNodesOfElementsTopLevel;
	}

	void setNodesOfElementsTopLevel(List<Node> pNodesOfElementsTopLevel) {
		mNodesOfElementsTopLevel = pNodesOfElementsTopLevel;
	}

	public Set<TargetNamespace> getMainDocumentTargetNamespaces() {
		return mMainDocumentTargetNamespaces;
	}

	public void setMainDocumentTargetNamespaces(Set<TargetNamespace> pMainDocumentTargetNamespaces) {
		mMainDocumentTargetNamespaces = pMainDocumentTargetNamespaces;
	}

	public Set<TargetNamespace> getTargetNamespaces() {
		return mTargetNamespaces;
	}

	public TargetNamespace getTargetNamespace(String pNamespace) {
		if (StringUtils.isBlank(pNamespace)) {
			return null;
		}
		for (TargetNamespace targetNamespace : getTargetNamespaces()) {
			if (targetNamespace.getNamespace().endsWith(pNamespace)) {
				return targetNamespace;
			}
		}
		return null;
	}

	public void setTargetNamespaces(Set<TargetNamespace> pTargetNamespaces) {
		mTargetNamespaces = pTargetNamespaces;
	}

	public Set<TargetNamespace> getCommonTargetNamespaces() {
		return mCommonTargetNamespaces;
	}

	public Document getWsdlDocument() {
		return mWsdlDocument;
	}

	public void setWsdlDocument(Document pWsdlDocument) {
		mWsdlDocument = pWsdlDocument;
	}

	public Set<String> getTargetNamespacesPrefixesFromDefinition() {
		return mTargetNamespacesPrefixesFromDefinition;
	}

	public void setTargetNamespacesPrefixesFromDefinition(Set<String> pTargetNamespacesPrefixesFromDefinition) {
		mTargetNamespacesPrefixesFromDefinition = pTargetNamespacesPrefixesFromDefinition;
	}

	/**
	 * @param pPrefix namespace prefix
	 * @return namespace by its prefix
	 */
	public TargetNamespace getTargetNamespaceByPrefix(String pPrefix) {
		if (getTargetNamespaces() == null || StringUtils.isBlank(pPrefix)) {
			return null;
		}
		for (TargetNamespace targetNamespace : getTargetNamespaces()) {
			if (pPrefix.equals(targetNamespace.getPrefix())) {
				return targetNamespace;
			}
		}
		return null;
	}

	/**
	 * @param pPrefix prefix
	 * @return true if there is a target namespace with current prefix
	 */
	public boolean containsTargetNamespace(String pPrefix) {
		if (getTargetNamespaces() == null || StringUtils.isBlank(pPrefix)) {
			return false;
		}
		for (TargetNamespace targetNamespace : getTargetNamespaces()) {
			if (pPrefix.equals(targetNamespace.getPrefix())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param pPrefix prefix
	 * @return true if there is a common target namespace with current prefix
	 */
	public boolean containsCommonTargetNamespace(String pPrefix) {
		if (getCommonTargetNamespaces() == null || StringUtils.isBlank(pPrefix)) {
			return false;
		}
		for (TargetNamespace targetNamespace : getCommonTargetNamespaces()) {
			if (pPrefix.equals(targetNamespace.getPrefix())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param pNamespace namespace value
	 * @return namespace by its value
	 */
	public TargetNamespace getTargetNamespaceByNamespace(String pNamespace) {
		if (getTargetNamespaces() == null || pNamespace == null) {
			return null;
		}
		for (TargetNamespace targetNamespace : getTargetNamespaces()) {
			if (pNamespace.equals(targetNamespace.getNamespace())) {
				return targetNamespace;
			}
		}
		return null;
	}

	/**
	 * get 1st type with current name
	 *
	 * @param pName name of wsdl type
	 * @return wsdl type
	 */
	public Type getTypeByName(String pName) {
		if (atg.core.util.StringUtils.isBlank(pName)) {
			return null;
		}
		for (Type wsdlType : getWsdlTypes()) {
			if (wsdlType.getName().equals(pName)) {
				return wsdlType;
			}
		}
		return null;
	}

	/**
	 * get type with specified type
	 *
	 * @param pTypeName type name of wsdl type
	 * @return wsdl type
	 */
	public Type getTypeByTypeName(String pTypeName) {
		if (atg.core.util.StringUtils.isBlank(pTypeName)) {
			return null;
		}
		for (Type wsdlType : getWsdlTypes()) {
			if (wsdlType.getTypeName().equals(pTypeName)) {
				return wsdlType;
			}
		}
		return null;
	}

}
