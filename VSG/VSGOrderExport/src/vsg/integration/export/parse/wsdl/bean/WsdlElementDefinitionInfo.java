package vsg.integration.export.parse.wsdl.bean;

/**
 * @author Dmitry Golubev
 */
public class WsdlElementDefinitionInfo {
	/**
	 * name
	 */
	private String mName;
	/**
	 * target namespace
	 */
	private TargetNamespace mTargetNamespace;

	//------------------------------------------------------------------------------------------------------------------

	public String getName() {
		return mName;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public TargetNamespace getTargetNamespace() {
		return mTargetNamespace;
	}

	public void setTargetNamespace(TargetNamespace pTargetNamespace) {
		mTargetNamespace = pTargetNamespace;
	}
}
