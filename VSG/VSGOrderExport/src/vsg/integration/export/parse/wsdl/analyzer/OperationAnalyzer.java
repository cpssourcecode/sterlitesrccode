package vsg.integration.export.parse.wsdl.analyzer;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import vsg.integration.export.parse.wsdl.DomUtil;
import vsg.integration.export.parse.wsdl.bean.Message;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.parse.wsdl.bean.WsdlElementDefinitionInfo;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class OperationAnalyzer extends BaseDocumentAnalyzer {
	/**
	 * form list of messages
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @param pDocument  docuemnt to parse
	 */
	private void formListOfMessages(WSDLModel pWSDLModel, Document pDocument) {
		Node definitions = DomUtil.getNodeByName(pDocument.getChildNodes(), TYPE_DEFINITIONS);
		pWSDLModel.setTargetNamespace(DomUtil.getAttributeValue(definitions, ATTRIBUTE_TARGET_NAMESPACE));
		List<Node> messages = new ArrayList<Node>();
		DomUtil.formListOfNodesByName(definitions, messages, TYPE_MESSAGE);

		for (Node messageNode : messages) {
			Message message = new Message();
			message.setName(DomUtil.getAttributeValue(messageNode, ATTRIBUTE_NAME));

			Node partNode = DomUtil.getNodeByName(messageNode.getChildNodes(), TYPE_MESSAGE_PART);
			if (partNode != null) { // no any part
				String partElementAttr = DomUtil.getAttributeValue(partNode, ATTRIBUTE_PART_ELEMENT);
				if (partElementAttr == null) { // <wsdl:part name="ZIP" type="s:string"/> or <part name="in0" type="ns1:VsgTestOrder"/>
					String partElementName = DomUtil.getAttributeValue(partNode, ATTRIBUTE_PART_NAME);
					String partElementType = DomUtil.getAttributeValue(partNode, ATTRIBUTE_PART_TYPE);
					WsdlElementDefinitionInfo info = DomUtil.getRealTypeForLink(pWSDLModel, partElementType);
					Type messageType = null;
					if (info != null) {
						messageType = pWSDLModel.getTypeByName(info.getName());
					}
					if (messageType != null) {
						messageType = new Type(messageType);
						messageType.setName(partElementName);
						message.setType(messageType);
					} else {
						message.setType(new Type(partElementName, partElementType, NodeType.ELEMENT));
					}
				} else { // it's a reference to defined type <part name="parameters" element="tns:exportOrders"></part>
/*
					WsdlElementDefinitionInfo info = DomUtil.getRealTypeForLink(pWSDLModel, partElementAttr);
					Type messageType = pWSDLModel.getWsdlTypes().get(info.getName());
					if(messageType.getTargetNamespace() == null && info.getTargetNamespace()!=null) {
						messageType.setTargetNamespace(info.getTargetNamespace());
					}
					message.setType(messageType);
*/
					Type messageType = null;
					if (partElementAttr.contains(DELIMITER)) {
						String name = partElementAttr.split(DELIMITER)[1];
						messageType = pWSDLModel.getTypeByName(name);
					}
					if (messageType == null) {
						WsdlElementDefinitionInfo info = DomUtil.getRealTypeForLink(pWSDLModel, partElementAttr);
						if (info != null) {
							messageType = pWSDLModel.getTypeByName(info.getName());
							if (messageType.getTargetNamespace() == null) {
								messageType.setTargetNamespace(info.getTargetNamespace());
							}
						}
					}
					message.setType(messageType);
				}
			}

			pWSDLModel.getMessages().add(message);
		}
	}

	/**
	 * form list of operations
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @param pDocument  docuemnt to parse
	 */
	private void formListOfOperations(WSDLModel pWSDLModel, Document pDocument) {
		Node definitions = DomUtil.getNodeByName(pDocument.getChildNodes(), TYPE_DEFINITIONS);

		List<Node> portTypes = new LinkedList<Node>();
		DomUtil.formListOfNodesByName(definitions, portTypes, TYPE_PORTTYPE);

		List<Node> bindings = new LinkedList<Node>();
		DomUtil.formListOfNodesByName(definitions, bindings, TYPE_BINDING);
		List<Node> operationsFromBinding = new LinkedList<Node>();
		for (Node binding : bindings) {
			DomUtil.formListOfNodesByName(binding, operationsFromBinding, TYPE_OPERATION);
		}

		for (Node portType : portTypes) {
			List<Node> operations = new LinkedList<Node>();
			DomUtil.formListOfNodesByName(portType, operations, TYPE_OPERATION);
			for (Node operationNode : operations) {
				formOperations(pWSDLModel, operationNode, operationsFromBinding);
			}
		}
	}

	/**
	 * analyze operation wsdl node and forms operation record
	 *
	 * @param pWSDLModel             wsdl model
	 * @param pOperationNode         operation wsdl node
	 * @param pOperationsFromBinding operation nodes from binding elements
	 */
	private void formOperations(WSDLModel pWSDLModel, Node pOperationNode, List<Node> pOperationsFromBinding) {
		Operation operation = new Operation();
		operation.setName(DomUtil.getAttributeValue(pOperationNode, ATTRIBUTE_NAME));
		Node nodeInput = DomUtil.getNodeByName(pOperationNode.getChildNodes(), TYPE_OPERATION_INPUT);
		if (nodeInput != null) {
			operation.setMessageInput(getMessage(
							pWSDLModel,
							DomUtil.getRealTypeForLink(
									pWSDLModel, DomUtil.getAttributeValue(nodeInput, ATTRIBUTE_MESSAGE)
							).getName())
			);
		}
		Node nodeOutput = DomUtil.getNodeByName(pOperationNode.getChildNodes(), TYPE_OPERATION_OUTPUT);
		if (nodeOutput != null) {
			operation.setMessageOutput(getMessage(
							pWSDLModel,
							DomUtil.getRealTypeForLink(
									pWSDLModel, DomUtil.getAttributeValue(nodeOutput, ATTRIBUTE_MESSAGE)
							).getName())
			);
		}
		additionalOperationInit(operation, pOperationsFromBinding);
		pWSDLModel.getOperations().add(operation);
	}

	/**
	 * search operation action through binfing-operation nodes <pre> {@code
	 * <wsdl:binding name="..." type="tns:...">
	 *   <soap:binding transport="..."/>
	 *   <wsdl:operation name="...">
	 *     <soap:operation soapAction="..." style="document"/>
	 *     ...
	 * }
	 *
	 * @param pOperation             operation name to find
	 * @param pOperationsFromBinding operation nodes from binding elements
	 * @return operation action
	 */
	private void additionalOperationInit(Operation pOperation, List<Node> pOperationsFromBinding) {
		if (pOperationsFromBinding == null)
			return;
		for (Node operationBindingNode : pOperationsFromBinding) {
			String operationBindingName = DomUtil.getAttributeValue(operationBindingNode, ATTRIBUTE_NAME);
			if (pOperation.getName().equals(operationBindingName)) {
				// operation action
				Node soapOperation = DomUtil.getNodeByName(operationBindingNode.getChildNodes(), TYPE_OPERATION);
				if (soapOperation != null) {
					pOperation.setAction(DomUtil.getAttributeValue(soapOperation, ATTRIBUTE_SOAP_ACTION));
				}
				if (pOperation.getMessageInput() != null) {
					Node input = DomUtil.getNodeByName(operationBindingNode.getChildNodes(), TYPE_OPERATION_INPUT);
					if (input != null) {
						Node body = DomUtil.getNodeByName(input.getChildNodes(), TYPE_OPERATION_BODY);
						if (body != null) {
							pOperation.getMessageInput().setEncoding(
									DomUtil.getAttributeValue(body, ATTRIBUTE_ENDCODING_STYLE)
							);
						}
					}
				}
				if (pOperation.getMessageOutput() != null) {
					Node output = DomUtil.getNodeByName(operationBindingNode.getChildNodes(), TYPE_OPERATION_OUTPUT);
					if (output != null) {
						Node body = DomUtil.getNodeByName(output.getChildNodes(), TYPE_OPERATION_BODY);
						if (body != null) {
							pOperation.getMessageOutput().setEncoding(
									DomUtil.getAttributeValue(body, ATTRIBUTE_ENDCODING_STYLE)
							);
						}
					}
				}
			}
		}
	}

	/**
	 * search operation action through binfing-operation nodes <pre> {@code
	 * <wsdl:binding name="..." type="tns:...">
	 *   <soap:binding transport="..."/>
	 *   <wsdl:operation name="...">
	 *     <soap:operation soapAction="..." style="document"/>
	 *     ...
	 * }
	 *
	 * @param pOperationName         operation name to find
	 * @param pOperationsFromBinding operation nodes from binding elements
	 * @return operation action
	 */
	private String getOperationEncoding(String pOperationName, List<Node> pOperationsFromBinding) {
		if (pOperationsFromBinding == null)
			return null;
		for (Node operationBindingNode : pOperationsFromBinding) {
			String operationBindingName = DomUtil.getAttributeValue(operationBindingNode, ATTRIBUTE_NAME);
			if (pOperationName.equals(operationBindingName)) {
				Node soapOperation = DomUtil.getNodeByName(operationBindingNode.getChildNodes(), TYPE_OPERATION);
				if (soapOperation != null) {
					return DomUtil.getAttributeValue(soapOperation, ATTRIBUTE_SOAP_ACTION);
				}
			}
		}
		return null;
	}

	/**
	 * find message by its name
	 *
	 * @param pWSDLModel   wsdl model to fill
	 * @param pMessageName message name
	 * @return message if found by name
	 */
	private Message getMessage(WSDLModel pWSDLModel, String pMessageName) {
		if (pMessageName == null || pWSDLModel.getMessages() == null) {
			return null;
		}
		for (Message message : pWSDLModel.getMessages()) {
			if (pMessageName.equalsIgnoreCase(message.getName())) {
				return message;
			}
		}
		return null;
	}

	/**
	 * sort operations types and init full pathes
	 *
	 * @param pWSDLModel wsdl model to fill
	 */
	private void initOperationsTypes(WSDLModel pWSDLModel) {
		for (Operation operation : pWSDLModel.getOperations()) {
			if (operation.getMessageInput() != null && operation.getMessageInput().getType() != null) {
				getTypeUtil().sortTypes(operation.getMessageInput().getType().getSubTypes());
				getTypeUtil().initFullPath(operation.getMessageInput().getType(), null, operation.getName());
			}
			if (operation.getMessageOutput() != null && operation.getMessageOutput().getType() != null) {
				getTypeUtil().sortTypes(operation.getMessageOutput().getType().getSubTypes());
				getTypeUtil().initFullPath(operation.getMessageOutput().getType(), null, operation.getName());
			}
		}
	}

	/**
	 * analyze document for model init
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @throws IOException                  if error occurs
	 * @throws SAXException                 if error occurs
	 * @throws ParserConfigurationException if error occurs
	 */
	@Override
	public void analyzeDocument(WSDLModel pWSDLModel)
			throws IOException, SAXException, ParserConfigurationException {
		Document document = pWSDLModel.getWsdlDocument();
		formListOfMessages(pWSDLModel, document);
		formListOfOperations(pWSDLModel, document);
		initOperationsTypes(pWSDLModel);
	}
}
