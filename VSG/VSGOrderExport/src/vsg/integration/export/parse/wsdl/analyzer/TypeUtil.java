package vsg.integration.export.parse.wsdl.analyzer;

import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.util.comparator.ComparatorType;

import java.util.Collections;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class TypeUtil {
	/**
	 * sort types 1. element 2. simple 3. complex
	 */
	public void sortTypes(List<Type> pTypes) {
		if (pTypes != null) {
			Collections.sort(pTypes, ComparatorType.instanse());
			for (Type type : pTypes) {
				if (type.getType() == NodeType.COMPLEX && !type.getSubTypes().isEmpty()) {
					sortTypes(type.getSubTypes());
				}
			}
		}
	}

	/**
	 * init type full tree path
	 *
	 * @param pInitialPath initial path for type
	 * @param pType        type to analyze
	 * @param pTypeParent  parent type
	 */
	public void initFullPath(Type pType, Type pTypeParent, String pInitialPath) {
		if (pType.getParent() == null && pTypeParent != null) {
			pType.setParent(pTypeParent);
		}
		if (pType.getParent() == null) {
			if (pInitialPath == null) {
				pType.setFullPath(pType.getName());
			} else {
				pType.setFullPath(pInitialPath + "." + pType.getName());
			}
		} else {
			Type parent = pType.getParent();
			pType.setFullPath(parent.getFullPath() + "." + pType.getName());
		}
		for (Type subType : pType.getSubTypes()) {
			subType.setParent(pType); // ?
			initFullPath(subType, pType, pType.getFullPath());
		}
	}
}
