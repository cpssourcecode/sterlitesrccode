package vsg.integration.export.parse.wsdl.bean;

import atg.core.util.StringUtils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class Type {

	/**
	 * type name
	 */
	private String mName;
	/**
	 * type name value
	 */
	private String mTypeName;
	/**
	 * full path of type hierarchy
	 */
	private String mFullPath;
	/**
	 * min occurs in document
	 */
	private int mMinOccurs = 1;
	/**
	 * max occurs in document
	 */
	private int mMaxOccurs = 1;
	/**
	 * type enum
	 */
	private NodeType mType;
	/**
	 * parent type
	 */
	private Type mParent;
	/**
	 * list of sub types
	 */
	private List<Type> mSubTypes = new ArrayList<Type>();
	/**
	 * list of allowed values
	 */
	private List<String> mAllowedValues;
	/**
	 * target namespace
	 */
	private TargetNamespace mTargetNamespace;
	/**
	 * target namespace for children types
	 */
	private TargetNamespace mTargetNamespaceForChildTypes;
	/**
	 * flag for complex type of array sub elements
	 */
	private boolean mArrayElement = false;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pName     name
	 * @param pTypeName value type
	 * @param pTYPE     node type
	 */
	public Type(String pName, String pTypeName, NodeType pTYPE) {
		mName = pName;
		mTypeName = pTypeName;
		mType = pTYPE;
	}

	/**
	 * Copy constructor
	 * @param pType source object
	 */
	public Type(Type pType) {
		// copy primitives and immutable.
		mName = pType.mName;
		mTypeName = pType.mTypeName;
		mFullPath = pType.mFullPath;
		mMinOccurs = pType.mMinOccurs;
		mMaxOccurs = pType.mMaxOccurs;
		mType = pType.mType;
		mArrayElement = pType.mArrayElement;

		// deep copy
		mSubTypes = copySubTypes(pType.mSubTypes);

		// copy only reference
		mParent = pType.mParent;
		mAllowedValues = pType.mAllowedValues;
		mTargetNamespace = pType.mTargetNamespace;
		mTargetNamespaceForChildTypes = pType.mTargetNamespaceForChildTypes;
	}

	private List<Type> copySubTypes(List<Type> original){
		List<Type> result = new ArrayList<>();
		for(Type type : original){
			result.add(new Type(type));
		}

		return result;
	}

	/**
	 * add sub type
	 *
	 * @param pSubType sub type
	 */
	public void addSubType(Type pSubType) {
		if (mSubTypes == null) {
			mSubTypes = new LinkedList<Type>();
		}
		mSubTypes.add(pSubType);
	}

	private void setSubTypes(List<Type> pSubTypes) {
		mSubTypes = pSubTypes;
	}

	public Type getParent() {
		return mParent;
	}

	public void setParent(Type pParentType) {
		mParent = pParentType;
	}

	/**
	 * add allowed value
	 *
	 * @param pAllowedValue allowed value
	 */
	public void addAllowedValue(String pAllowedValue) {
		if (mAllowedValues == null) {
			mAllowedValues = new LinkedList<String>();
		}
		mAllowedValues.add(pAllowedValue);
	}

	public void setName(String pName) {
		mName = pName;
	}

	public String getName() {
		return mName;
	}

	public void setTypeName(String pTypeName) {
		mTypeName = pTypeName;
	}

	public String getTypeName() {
		return mTypeName;
	}

	public NodeType getType() {
		return mType;
	}

	public void setType(NodeType pType) {
		mType = pType;
	}

	public String getTypeAsString() {
		return mType.toString();
	}

	public List<String> getAllowedValues() {
		return mAllowedValues;
	}

	public List<Type> getSubTypes() {
		return mSubTypes;
	}

	public boolean isArrayElement() {
		return mArrayElement;
	}

	public void setArrayElement(boolean pArrayElement) {
		mArrayElement = pArrayElement;
	}

	/**
	 * @param pSubTypeName sub type name
	 * @return sub type with matched name
	 */
	public Type getSubType(String pSubTypeName) {
		if (getSubTypes() == null || StringUtils.isBlank(pSubTypeName)) {
			return null;
		}
		for (Type subType : getSubTypes()) {
			if (subType.getName().equals(pSubTypeName)) {
				return subType;
			}
		}
		return null;
	}

	public String getFullPath() {
		return mFullPath;
	}

	public void setFullPath(String pFullPath) {
		mFullPath = pFullPath;
	}

	public int getMinOccurs() {
		return mMinOccurs;
	}

	public void setMinOccurs(int pMinOccurs) {
		mMinOccurs = pMinOccurs;
	}

	public int getMaxOccurs() {
		return mMaxOccurs;
	}

	public void setMaxOccurs(int pMaxOccurs) {
		mMaxOccurs = pMaxOccurs;
	}

	public TargetNamespace getTargetNamespace() {
		return mTargetNamespace;
	}

	public void setTargetNamespace(TargetNamespace pTargetNamespace) {
		mTargetNamespace = pTargetNamespace;
	}

	public TargetNamespace getTargetNamespaceForChildTypes() {
		return mTargetNamespaceForChildTypes;
	}

	public void setTargetNamespaceForChildTypes(TargetNamespace pTargetNamespaceForChildTypes) {
		mTargetNamespaceForChildTypes = pTargetNamespaceForChildTypes;
	}

	/**
	 * @return string presentation of type
	 */
	@Override
	public String toString() {
		return " [" + getType() + "]: " + getName() + " <" + getTypeName() + "> - " + getFullPath();
	}
}
