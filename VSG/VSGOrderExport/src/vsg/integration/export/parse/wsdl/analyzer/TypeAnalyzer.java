package vsg.integration.export.parse.wsdl.analyzer;

import atg.core.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import vsg.integration.export.parse.wsdl.DomConstants;
import vsg.integration.export.parse.wsdl.DomUtil;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.TargetNamespace;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.util.FileUtil;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class TypeAnalyzer extends BaseDocumentAnalyzer {
	/**
	 * wsdl parser
	 */
	private WSDLParser mWSDLParser;
	/**
	 * temp name for type with array declaration
	 */
	private static final String TMP_NAME_FOR_ARRAY_SUB_TYPE = "attr_array";

	//------------------------------------------------------------------------------------------------------------------

	public TypeAnalyzer() {
	}

	/**
	 * for test wsdlParser
	 *
	 * @param pWSDLParser wsdl parser
	 */
	public TypeAnalyzer(WSDLParser pWSDLParser) {
		mWSDLParser = pWSDLParser;
	}

	public WSDLParser getWSDLParser() {
		return mWSDLParser;
	}

	public void setWSDLParser(WSDLParser pWSDLParser) {
		mWSDLParser = pWSDLParser;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * analyze
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @param pDocument  docuemnt to parse
	 * @throws ParserConfigurationException if error occurs
	 * @throws IOException                  if error occurs
	 * @throws SAXException                 if error occurs
	 */
	private void formListOfTypeNodes(WSDLModel pWSDLModel, Document pDocument)
			throws ParserConfigurationException, IOException, SAXException {
		List<Node> schemas = new ArrayList<Node>();
		DomUtil.formListOfNodesByName(pDocument, schemas, TYPE_SCHEMA);
		for (Node schema : schemas) {
			formListOfTypeNodes(pWSDLModel, schema);
		}
/*
		Node schema = DomUtil.getNodeByName( pDocument.getChildNodes(), TYPE_SCHEMA );
		formListOfTypeNodes(pWSDLModel, schema);
*/
	}

	/**
	 * analyze
	 *
	 * @param pWSDLModel                 wsdl model to fill
	 * @param pTopNodeOfTypesDeclaration top node to get types declaration
	 * @throws ParserConfigurationException if error occurs
	 * @throws IOException                  if error occurs
	 * @throws SAXException                 if error occurs
	 */
	private void formListOfTypeNodes(WSDLModel pWSDLModel, Node pTopNodeOfTypesDeclaration)
			throws ParserConfigurationException, IOException, SAXException {
		DomUtil.formListOfNodesByName(pTopNodeOfTypesDeclaration, pWSDLModel.getNodesOfComplexTypes(), TYPE_COMPLEX_TYPE,
				new String[]{TYPE_SIMPLE_TYPE, TYPE_ELEMENT}
		);
		DomUtil.formListOfNodesByName(pTopNodeOfTypesDeclaration, pWSDLModel.getNodesOfSimpleTypes(), TYPE_SIMPLE_TYPE,
				new String[]{TYPE_COMPLEX_TYPE, TYPE_ELEMENT}
		);
		DomUtil.formListOfNodesByName(pTopNodeOfTypesDeclaration, pWSDLModel.getNodesOfElementsTopLevel(), TYPE_ELEMENT,
				new String[]{TYPE_COMPLEX_TYPE, TYPE_SIMPLE_TYPE}
		);
	}

	/**
	 * analyze document for current prefix
	 *
	 * @param pWSDLModel        wsdl model
	 * @param pSchemaNodeOfXsd  schema node of xsd-file
	 * @param pSchemaLocation   schema location
	 * @param pAddToDefinitions flag to store prefix in the list definition-targetnamespaces
	 */
	private void initTargetNamespaceInfo(WSDLModel pWSDLModel, Node pSchemaNodeOfXsd, String pSchemaLocation, boolean pAddToDefinitions) {
		String namespace = DomUtil.getAttributeValue(pSchemaNodeOfXsd, ATTRIBUTE_TARGET_NAMESPACE);

		String prefix = null;
		Node xmlns = DomUtil.getAttributeByValue(pSchemaNodeOfXsd, ATTRIBUTE_XMLNS, namespace);
		if (xmlns != null && xmlns.getNodeName().contains(DELIMITER)) {
			prefix = xmlns.getNodeName().split(DELIMITER)[1];
		}

		if (pWSDLModel.containsTargetNamespace(prefix)) {
			return;
		}
		TargetNamespaceUtil.createTargetNamespace(namespace, prefix, pSchemaLocation, pWSDLModel);

		// check if any other namespaces are declared
		List<Node> xmlnsAttributes = DomUtil.getAttributesByName(pSchemaNodeOfXsd, ATTRIBUTE_XMLNS);
		for (Node xmlnsAttribute : xmlnsAttributes) {
			if (xmlnsAttribute.getNodeName() != null && xmlnsAttribute.getNodeName().contains(ATTRIBUTE_XMLNS + DELIMITER)) {
				String attrPrefix = xmlnsAttribute.getLocalName();
				String attrNamespace = xmlnsAttribute.getNodeValue();
				if (pWSDLModel.containsTargetNamespace(attrPrefix)) {
					continue;
				}
				if (pAddToDefinitions) {
					pWSDLModel.getTargetNamespacesPrefixesFromDefinition().add(xmlnsAttribute.getLocalName());
				}
				TargetNamespaceUtil.createTargetNamespace(
						attrNamespace,
						attrPrefix,
						pWSDLModel);
			}
		}
	}

	/**
	 * analyze for complex types and array declaration
	 *
	 * @param pTypes types to analyze
	 */
	private void checkTypes(WSDLModel pWSDLModel, Collection<Type> pTypes, Type pParent) {
		for (Type type : pTypes) {
			// check for complex types array input
			if (type.getName().equals(TMP_NAME_FOR_ARRAY_SUB_TYPE) && type.getType() == NodeType.ELEMENT) {
				Type alreadyInitedType = pWSDLModel.getTypeByName(type.getTypeName());
				//getRegisteredType(pWSDLModel, type .getTypeName());
				if (alreadyInitedType == null) {
					vlogError("Unable to retrieve sub type: " + type.getTypeName());
				} else {
					if (pParent != null) {
						pParent.getSubTypes().remove(type);
						type = new Type(alreadyInitedType);
						type.setArrayElement(true);
						pParent.addSubType(type);
					} else {
						pWSDLModel.getWsdlTypes().remove(type);
						type = new Type(alreadyInitedType);
						type.setArrayElement(true);
						pWSDLModel.getWsdlTypes().add(type);
					}
				}
			}
			checkTypes(pWSDLModel, type.getSubTypes(), type);
		}
	}

	/**
	 * form list of attributes
	 *
	 * @param pWSDLModel wsdl model to fill
	 */
	private void formListOfTypes(WSDLModel pWSDLModel) {
		for (Node node : pWSDLModel.getNodesOfSimpleTypes()) {
			analyzeNodeCreateType(pWSDLModel, node, null);
		}
		for (Node node : pWSDLModel.getNodesOfComplexTypes()) {
			analyzeNodeCreateType(pWSDLModel, node, null);
		}
		for (Node node : pWSDLModel.getNodesOfElementsTopLevel()) {
			analyzeNodeCreateType(pWSDLModel, node, null);
		}
		checkTypes(pWSDLModel, pWSDLModel.getWsdlTypes(), null);
	}

	/**
	 * forms list of targetnamespaces defined in node 'definitions'
	 *
	 * @param pWSDLModel wsdl model
	 * @param pDocument  document
	 */
	private void formListOfTargetNamespacesFromDefinition(WSDLModel pWSDLModel, Document pDocument) {
		Node definition = DomUtil.getNodeByName(pDocument.getChildNodes(), TYPE_DEFINITIONS);

//		get main target namespase
		List<Node> targetNamespaceList = DomUtil.getAttributesByName(definition, ATTRIBUTE_TARGET_NAMESPACE);
		Node targetNamespace = targetNamespaceList.get(0);
		String targetNamespaceValue = targetNamespace.getNodeValue();

		List<Node> xmlnsAttributes = DomUtil.getAttributesByName(definition, ATTRIBUTE_XMLNS + DELIMITER);

		for (Node xmlnsAttribute : xmlnsAttributes) {
			String prefix = xmlnsAttribute.getNodeName().split(DELIMITER)[1];
			if (pWSDLModel.containsTargetNamespace(prefix)) {
				continue;
			}
			pWSDLModel.getTargetNamespacesPrefixesFromDefinition().add(prefix);
			if (pWSDLModel.getTargetNamespaceByPrefix(prefix) != null) {
				continue;
			}
			String namespace = xmlnsAttribute.getNodeValue();
			if (null != namespace && namespace.equals(targetNamespaceValue)) {
				TargetNamespaceUtil.createTargetNamespace(namespace, prefix, null, pWSDLModel, true);
			} else {
				TargetNamespaceUtil.createTargetNamespace(namespace, prefix, null, pWSDLModel);
			}
		}
	}

	/**
	 * forms list of types from internal declared schemas
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @param pDocument  wsdl document
	 * @throws java.io.IOException                            if error occurs
	 * @throws org.xml.sax.SAXException                       if error occurs
	 * @throws javax.xml.parsers.ParserConfigurationException if error occurs
	 */
	private void formListOfTypeNodesFromInternalSchemas(WSDLModel pWSDLModel, Document pDocument)
			throws IOException, SAXException, ParserConfigurationException {
		List<Node> internalSchemas = new ArrayList<Node>();
		DomUtil.formListOfNodesByName(pDocument, internalSchemas, TYPE_SCHEMA);
		for (Node internalSchemaDeclaration : internalSchemas) {
			initTargetNamespaceInfo(
					pWSDLModel,
					internalSchemaDeclaration,
					"internalSchemaDeclaration",
					true);
			formListOfTypeNodes(pWSDLModel, internalSchemaDeclaration);
		}

	}

	/**
	 * forms list of types from external xsd file
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @param pDocument  wsdl document
	 * @throws java.io.IOException                            if error occurs
	 * @throws org.xml.sax.SAXException                       if error occurs
	 * @throws javax.xml.parsers.ParserConfigurationException if error occurs
	 */
	private void formListOfTypeNodesFromExternalXSD(WSDLModel pWSDLModel, Document pDocument)
			throws IOException, SAXException, ParserConfigurationException {
		List<Node> externalImports = new ArrayList<Node>();
		DomUtil.formListOfNodesByName(pDocument, externalImports, TYPE_IMPORT);
		for (Node externalImport : externalImports) {
			String schemaLocation = DomUtil.getAttributeValue(externalImport, DomConstants.ATTRIBUTE_SCHEMA_LOCATION);
			if (schemaLocation == null) {
				schemaLocation = DomUtil.getAttributeValue(externalImport, DomConstants.ATTRIBUTE_LOCATION);
			}

			if (schemaLocation == null) {
				vlogDebug("No schema location is defined for {0}", externalImport.getNamespaceURI());
				continue;
			}
			String xsdFile = FileUtil.getFile(pDocument, schemaLocation);
			if (xsdFile == null) {
				vlogError("{0} is not accessible", schemaLocation);
				continue;
			}
			Document xsdDocument = null;
			try {
				xsdDocument = getWSDLParser().getDocument(xsdFile);
			} catch (Exception e) {
				vlogError("Unable to build document for xsd file '" + xsdFile + "': " + e.toString());
				continue;
			}
			initTargetNamespaceInfo(
					pWSDLModel,
					DomUtil.getNodeByName(xsdDocument.getChildNodes(), TYPE_SCHEMA),
					schemaLocation,
					false);
			formListOfTypeNodes(pWSDLModel, xsdDocument);
		}
	}

	/**
	 * @param pNode wsdl node
	 * @return NodeType value
	 */
	private NodeType getNodeType(Node pNode, WSDLModel pWSDLModel) {
		NodeType nodeType = NodeType.ELEMENT;
		String nodeName = pNode.getNodeName();
		if (nodeName.endsWith(TYPE_ELEMENT)) {
			nodeType = NodeType.ELEMENT;
			// check if it has xlmns:type declaration
			String elementType = DomUtil.getAttributeValue(pNode, ATTRIBUTE_TYPE);
			if (!StringUtils.isBlank(elementType)) {
				if (elementType.contains(DELIMITER)) {
					String namespacePrefix = elementType.split(DELIMITER)[0];
					if (!pWSDLModel.containsCommonTargetNamespace(namespacePrefix)) {
						nodeType = NodeType.COMPLEX;
					}
				}
			}

		} else if (nodeName.endsWith(TYPE_COMPLEX_TYPE)) {
			nodeType = NodeType.COMPLEX;
		} else if (nodeName.endsWith(TYPE_SIMPLE_TYPE)) {
			nodeType = NodeType.SIMPLE;
		}
		return nodeType;
	}

	/**
	 * @param pWSDLModel  wsdl model to fill
	 * @param pNode       dom node to analyze
	 * @param pParentType parent node
	 */
	private Type analyzeNodeCreateType(WSDLModel pWSDLModel,
									   Node pNode,
									   Type pParentType) {
		String elementName = getName(pNode);

		NodeType nodeType = getNodeType(pNode, pWSDLModel);
		String elementTypeFull = getTypeValue(pNode, nodeType);
		String elementType = elementTypeFull;

		// check if type already registered - clone it
		// otherwise create new one

		Type registeredType = null;
		if (elementType.contains(DELIMITER)) {
			elementType = elementType.split(DELIMITER)[1];
			registeredType = pWSDLModel.getTypeByTypeName(elementType);
		}

		Type type;
		if (registeredType != null) {
			type = typeClone(registeredType, pNode, nodeType);
/*
			if(type.getType().equals(NodeType.ELEMENT) && pParentType!=null) {
				type.setTargetNamespace( pParentType.getTargetNamespace() );
			}
*/
		} else {
			// determine target namespace
			type = new Type(elementName, elementType, nodeType);
		}

		if (type != null) {
			type.setParent(pParentType);
			if (pParentType != null) {
				pParentType.addSubType(type);
			}
		}

		TargetNamespaceUtil.initTypeTargetNamespace(
				pWSDLModel,
				type,
				pParentType,
				pNode,
				elementTypeFull
		);
		if (registeredType == null) {
			type = additionalInit(pWSDLModel, pNode, type);
		}

		if (type != null) {
			pWSDLModel.getWsdlTypes().add(type);
			type = analyzeNodeOccures(type, pNode);
		}
		return type;
	}

	/**
	 * @param pNode node with type declaration
	 * @param pType type to clone
	 * @param pTYPE node type
	 * @return cloned type
	 */
	private Type typeClone(Type pType, Node pNode, NodeType pTYPE) {
		String name = getName(pNode);
		Type cloned = null;
		cloned = new Type(pType);
		cloned.setName(name);
/*
		if (pTYPE == NodeType.ELEMENT) {
			cloned.setTypeName(pNode.getNodeName());
		}
*/

		return cloned;
	}

	/**
	 * analyze node occurs in document tree
	 *
	 * @param pType model type of wsdl element
	 * @param pNode wsdl node
	 * @return modified type
	 */
	private Type analyzeNodeOccures(Type pType, Node pNode) {
		String minOccurs = DomUtil.getAttributeValue(pNode, ATTRIBUTE_MIN_OCCURS);
		String maxOccurs = DomUtil.getAttributeValue(pNode, ATTRIBUTE_MAX_OCCURS);
		if (!StringUtils.isBlank(maxOccurs)) {
			if (ATTRIBUTE_OCCURS_VALUE_UNBOUNDED.equals(maxOccurs)) {
				pType.setMaxOccurs(-1); // unbounded
			} else {
				try {
					pType.setMaxOccurs(Integer.parseInt(maxOccurs));
				} catch (Exception e) {
					if(isLoggingError()){
						logError(e);
					}
				}
			}
		}
		if (!StringUtils.isBlank(minOccurs)) {
			try {
				pType.setMinOccurs(Integer.parseInt(minOccurs));
			} catch (Exception e) {
				if(isLoggingError()){
					logError(e);
				}
			}
		}
		return pType;
	}


	/**
	 * additional check and attribute init
	 *
	 * @param pWSDLModel wsdl model to init
	 * @param pNode      current node
	 * @param pType      attribute
	 */
	private Type additionalInit(WSDLModel pWSDLModel, Node pNode, Type pType) {
		switch (pType.getType()) {
			case COMPLEX:
				return initComplexTypeSettings(pWSDLModel, pNode, pType);
			case SIMPLE:
				return initSimpleTypeSettings(pNode, pType);
		}
		return pType;
	}

	/**
	 * @param pNode document node to analyze
	 * @param pTYPE TYPE of node
	 * @return declared xml type of element
	 */
	private String getTypeValue(Node pNode, NodeType pTYPE) {
		if (pTYPE == NodeType.ELEMENT) {
			String type = DomUtil.getNodeTypeByAttribute(pNode);
			if (StringUtils.isBlank(type)) {
				Node restriction = DomUtil.getNodeByName(pNode.getChildNodes(), TYPE_RESTRICTION);
				if (restriction != null) {
					return DomUtil.getNodeValue(
							DomUtil.getAttributeByName(restriction, ATTRIBUTE_BASE)
					);
				}
			}
		}
		if (pTYPE == NodeType.SIMPLE) {
			return DomUtil.getAttributeValue(pNode, ATTRIBUTE_NAME);
		} else {
			String type = DomUtil.getNodeTypeByAttribute(pNode);
			if (type == null) {
				return DomUtil.getAttributeValue(pNode, ATTRIBUTE_NAME);
			}
			return type;
		}
	}

	/**
	 * @param pNode wsdl Node
	 * @return node name
	 */
	private String getName(Node pNode) {
		String name = DomUtil.getNodeNameByAttribute(pNode);
		if (name == null) {
			return pNode.getNodeName();
		}
		return name;
	}


	/**
	 * init simple attribute
	 *
	 * @param pNode node to analyze
	 * @param pType type to init
	 */
	private Type initSimpleTypeSettings(Node pNode, Type pType) {
		List<Node> childNodesWithEnumValues = new ArrayList<Node>();
		DomUtil.formListOfNodesByName(pNode, childNodesWithEnumValues, TYPE_ENUMERATION);
		for (Node node : childNodesWithEnumValues) {
			pType.addAllowedValue(DomUtil.getNodeValue(node));
		}
		return pType;
	}

	/**
	 * @param pWSDLModel wsdl model to init
	 * @param pType      node type
	 * @return complex node with satisfied type
	 */
	private Node getComplexNodeByType(WSDLModel pWSDLModel, String pType) {
		for (Node complexNode : pWSDLModel.getNodesOfComplexTypes()) {
			if (pType.equals(DomUtil.getNodeNameByAttribute(complexNode))) {
				return complexNode;
			}
		}
		return null;
	}

	/**
	 * init complex type with no child-nodes
	 *
	 * @param pWSDLModel wsdl model to init
	 * @param pNode      node to analyze
	 * @param pType      attribute to init
	 */
	private Type initComplexTypeSettingsWithoutChildNodes(WSDLModel pWSDLModel, Node pNode, Type pType) {
		String complexTypeNameToSearch = pType.getTypeName();
		if (complexTypeNameToSearch.contains(DELIMITER)) {
			complexTypeNameToSearch = complexTypeNameToSearch.split(DELIMITER)[1];
		}
		Node complexNode = getComplexNodeByType(pWSDLModel, complexTypeNameToSearch);
		if (complexNode != null) {
			if (complexNode.equals(pNode)) {
				// founded declaration of node is similar with processing node
				return pType;
			}
			Type createdType = analyzeNodeCreateType(pWSDLModel, complexNode, null);
			Type clonedType = new Type(createdType);
			if (pType.getParent() != null) {
				pType.getParent().getSubTypes().remove(pType);
			}
			clonedType.setName(pType.getName());
			clonedType.setTargetNamespace(pType.getTargetNamespace());
			clonedType.setTargetNamespaceForChildTypes(pType.getTargetNamespaceForChildTypes());
			clonedType.setParent(pType.getParent());
			if (clonedType.getParent() != null) {
				clonedType.getParent().addSubType(clonedType);
			}
			return clonedType;
		}
		return null;
	}

	/**
	 * init complex type with child-nodes
	 *
	 * @param pWSDLModel wsdl model to init
	 * @param pNode      node to analyze
	 * @param pType      attribute to init
	 */
	private Type initComplexTypeSettingsWithChildNodes(WSDLModel pWSDLModel, Node pNode, Type pType) {
		List<Node> childNodesElements = new ArrayList<>();
		DomUtil.formListOfNodesByName(pNode, childNodesElements, TYPE_ELEMENT);
		if (childNodesElements.isEmpty()) {
			// <attribute ref="soap-enc:arrayType" wsdl:arrayType="tns:VsgTestShippingGroup[]"/>
			Node attribute = DomUtil.getNodeByName(pNode.getChildNodes(), TYPE_ATTRIBUTE);
			if (attribute != null) {
				String arrayType = DomUtil.getAttributeValue(attribute, ATTRIBUTE_ARRAY_TYPE);
				if (arrayType != null && arrayType.contains("[]")) {
					arrayType = arrayType.substring(0, arrayType.length() - 2);
					Type arrayItemType = pWSDLModel.getTypeByName(arrayType);
					if (arrayItemType == null) {
						pType.addSubType(
								new Type(TMP_NAME_FOR_ARRAY_SUB_TYPE, arrayType, NodeType.ELEMENT)
						);
					}
				}
			}
		} else {
			for (Node childNode : childNodesElements) {
				Type childType = analyzeNodeCreateType(
						pWSDLModel,
						childNode,
						pType
				);
				childType.setName(DomUtil.getAttributeValue(childNode, ATTRIBUTE_NAME));
				// pType.addSubType(childType);
			}
		}
		return pType;
	}

	/**
	 * init complex attribute
	 *
	 * @param pWSDLModel wsdl model to init
	 * @param pNode      node to analyze
	 * @param pType      attribute to init
	 */
	private Type initComplexTypeSettings(WSDLModel pWSDLModel, Node pNode, Type pType) {
		if (pNode.getChildNodes().getLength() == 0) {
			return initComplexTypeSettingsWithoutChildNodes(pWSDLModel, pNode, pType);
		} else {
			return initComplexTypeSettingsWithChildNodes(pWSDLModel, pNode, pType);
		}
	}

	/**
	 * checks for all declared target namespaces and used prefixes both in xsd files and wsdl file
	 *
	 * @param pWSDLModel wsdl model
	 * @param pDocument  wsdl document
	 */
	private void checkDocumentDefinitions(WSDLModel pWSDLModel, Document pDocument) {
		Set<TargetNamespace> prevTargetNamespaces = new HashSet<TargetNamespace>();
		prevTargetNamespaces.addAll(pWSDLModel.getTargetNamespaces());

		Node definitions = DomUtil.getNodeByName(pDocument.getChildNodes(), TYPE_DEFINITIONS);
		checkDocumentOtherPrefixForTargetNamespace(pWSDLModel, definitions);
		checkDocumentDefinitionsTargetNamespace(pWSDLModel, definitions);
		// check for http://schemas.xmlsoap.org/soap/encoding/
		for (TargetNamespace targetNamespace : pWSDLModel.getTargetNamespaces()) {
			if ("http://schemas.xmlsoap.org/soap/encoding/".equals(targetNamespace.getNamespace())) {
				pWSDLModel.setTargetNamespaceSoapEncoding(targetNamespace);
				break;
			}
		}
		// add http://schemas.xmlsoap.org/soap/envelope/
		TargetNamespaceUtil.createTargetNamespace(
				"http://schemas.xmlsoap.org/soap/envelope/",
				"env",
				pWSDLModel);

		pWSDLModel.getMainDocumentTargetNamespaces().addAll(pWSDLModel.getTargetNamespaces());
		pWSDLModel.getMainDocumentTargetNamespaces().removeAll(prevTargetNamespaces);
	}


	/**
	 * checks for targetNamespace with different prefixes but the same schemalocation
	 *
	 * @param pWSDLModel   wsdl model
	 * @param pDefinitions definitions node of document
	 */
	private void checkDocumentOtherPrefixForTargetNamespace(WSDLModel pWSDLModel, Node pDefinitions) {
		List<Node> xmlnsAttributes = DomUtil.getAttributesByName(pDefinitions, ATTRIBUTE_XMLNS + DELIMITER);
		for (Node xmlnsAttribute : xmlnsAttributes) {
			String prefix = xmlnsAttribute.getNodeName().split(DELIMITER)[1];
			if (pWSDLModel.containsTargetNamespace(prefix)) {
				continue;
			}
			String namespace = xmlnsAttribute.getNodeValue();
			String schemaLocation = null;
			TargetNamespace modelTargetNamespace = pWSDLModel.getTargetNamespaceByNamespace(namespace);
			if (modelTargetNamespace != null && modelTargetNamespace.getSchemaLocation() != null) {
				schemaLocation = modelTargetNamespace.getSchemaLocation();
			}
			TargetNamespaceUtil.createTargetNamespace(
					namespace,
					prefix,
					schemaLocation,
					pWSDLModel
			);
		}
	}

	/**
	 * checks if targetNamespace is defined, not specified in schemas and is in list of xmlns: namespaces
	 *
	 * @param pWSDLModel   wsdl model
	 * @param pDefinitions definitions node of document
	 */
	private void checkDocumentDefinitionsTargetNamespace(WSDLModel pWSDLModel, Node pDefinitions) {
		// check document xsds targetnamespaces for changed name
		Node targetNamespaceNode = DomUtil.getAttributeByName(pDefinitions, ATTRIBUTE_TARGET_NAMESPACE);
		String namespace = targetNamespaceNode.getNodeValue();
		if (pWSDLModel.getTargetNamespaceByNamespace(targetNamespaceNode.getNodeValue()) != null) {
			return;
		}
		Node targetNamespaceNodeInDefinitions = DomUtil.getAttributeByValue(pDefinitions, ATTRIBUTE_XMLNS,
				namespace);
		if (targetNamespaceNodeInDefinitions != null) {
			String prefix = targetNamespaceNodeInDefinitions.getNodeName().split(DELIMITER)[1];
			if (pWSDLModel.containsTargetNamespace(prefix)) {
				return;
			}
			TargetNamespaceUtil.createTargetNamespace(
					namespace,
					prefix,
					pWSDLModel);
		}
	}

	/**
	 * checks defined target namespaces and set prefixes to them if they are not defined
	 *
	 * @param pWSDLModel wsdl model
	 */
	private void checkTargetNamespacePrefixes(WSDLModel pWSDLModel) {
		String prefixForEmpty = "java";
		int index = 0;
		for (TargetNamespace targetNamespace : pWSDLModel.getTargetNamespaces()) {
			if (StringUtils.isBlank(targetNamespace.getPrefix())) {
				targetNamespace.setPrefix(prefixForEmpty + index);
				index++;
			}
		}
	}

	/**
	 * analyze document for model init
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @throws IOException                  if error occurs
	 * @throws SAXException                 if error occurs
	 * @throws ParserConfigurationException if error occurs
	 */
	@Override
	public void analyzeDocument(WSDLModel pWSDLModel)
			throws IOException, SAXException, ParserConfigurationException {
		Document document = pWSDLModel.getWsdlDocument();
		formListOfTargetNamespacesFromDefinition(pWSDLModel, document);
		formListOfTypeNodesFromInternalSchemas(pWSDLModel, document);
		formListOfTypeNodesFromExternalXSD(pWSDLModel, document);
		checkDocumentDefinitions(pWSDLModel, document);
		formListOfTypeNodes(pWSDLModel, document);
		formListOfTypes(pWSDLModel);
		checkTargetNamespacePrefixes(pWSDLModel);
	}
}
