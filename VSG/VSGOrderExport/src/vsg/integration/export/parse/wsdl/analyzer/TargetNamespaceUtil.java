package vsg.integration.export.parse.wsdl.analyzer;

import atg.core.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import vsg.integration.export.parse.wsdl.DomConstants;
import vsg.integration.export.parse.wsdl.DomUtil;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.TargetNamespace;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;

/**
 * @author Dmitry Golubev
 */
public class TargetNamespaceUtil implements DomConstants {

	/**
	 * array of common target namespaces prefixes
	 */
	static String[] mCommonTargetNamespacePrefixes = new String[]{
			"http://schemas.xmlsoap.org",
			"http://www.w3.org/"
	};

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pNamespace      namespace
	 * @param pSchemaLocation schema location
	 * @param pPrefix         prefix
	 * @param pWSDLModel      wsdl model
	 * @param pMain flag if this targetNamespace market as targetNamespace in WSDL
	 * @return initialized target namespace
	 */
	public static TargetNamespace createTargetNamespace(String pNamespace, String pPrefix, String pSchemaLocation, WSDLModel pWSDLModel, boolean pMain) {
		TargetNamespace targetNamespace = createTargetNamespace(pNamespace, pPrefix, pSchemaLocation, pWSDLModel);
		targetNamespace.setMain(pMain);
		return targetNamespace;
	}

	/**
	 * @param pNamespace      namespace
	 * @param pSchemaLocation schema location
	 * @param pPrefix         prefix
	 * @param pWSDLModel      wsdl model
	 * @return initialized target namespace
	 */
	public static TargetNamespace createTargetNamespace(String pNamespace, String pPrefix, String pSchemaLocation, WSDLModel pWSDLModel) {
		TargetNamespace targetNamespace = new TargetNamespace();
		targetNamespace.setNamespace(pNamespace);
		targetNamespace.setSchemaLocation(pSchemaLocation);
		targetNamespace.setPrefix(pPrefix);

		if (isTargetNamespaceCommon(targetNamespace, pWSDLModel)) {
			pWSDLModel.getCommonTargetNamespaces().add(targetNamespace);
		} else {
			pWSDLModel.getTargetNamespaces().add(targetNamespace);
		}

		return targetNamespace;
	}

	/**
	 * @param pNamespace namespace
	 * @param pPrefix    prefix
	 * @param pWSDLModel wsdl model
	 * @return initialized target namespace
	 */
	public static TargetNamespace createTargetNamespace(String pNamespace, String pPrefix, WSDLModel pWSDLModel) {
		return createTargetNamespace(pNamespace, pPrefix, null, pWSDLModel);
	}

	/**
	 * check if declared namespace is common for wsdl and xsd forming
	 *
	 * @param pTargetNamespace taget namespace
	 * @param pWSDLModel       wsdl model
	 * @return true if target namespace is under common definitions
	 */
	public static boolean isTargetNamespaceCommon(TargetNamespace pTargetNamespace, WSDLModel pWSDLModel) {
		for (String commonPrefix : mCommonTargetNamespacePrefixes) {
			if (pTargetNamespace.getNamespace() != null) {
				if (pTargetNamespace.getNamespace().startsWith(commonPrefix)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * init type target namespace
	 *
	 * @param pWSDLModel  wsdl model
	 * @param pType       type to init
	 * @param pParentType parent type
	 * @param pNode       wsdl node
	 */
	private static void initTargetNamespace(WSDLModel pWSDLModel, Type pType, Type pParentType, Node pNode) {
		if (pParentType != null) {
			pType.setTargetNamespace(pParentType.getTargetNamespaceForChildTypes());
		} else {
			TargetNamespace targetNamespace = getTargetNamespaceByParentSchema(pWSDLModel, pNode);
			pType.setTargetNamespace(targetNamespace);
		}
	}

	/**
	 * init type target namespace
	 *
	 * @param pWSDLModel wsdl model
	 * @param pType      type to init
	 * @param pNode      wsdl node
	 */
	private static void initTargetNamespaceForChildTypes(WSDLModel pWSDLModel, Type pType, Node pNode, String
			pTypeValue) {
		if (!pType.getType().equals(NodeType.COMPLEX)) {
			return;
		}
		if (pTypeValue.contains(DELIMITER)) {
			String[] typeInfo = pTypeValue.split(DELIMITER);
			String prefix = typeInfo[0];
			String namespace = DomUtil.getAttributeValue(pNode, ATTRIBUTE_XMLNS + DELIMITER + prefix);
			if (namespace != null) {
				pType.setTargetNamespaceForChildTypes(pWSDLModel.getTargetNamespace(namespace));
			}
		}
		if (pType.getTargetNamespaceForChildTypes() == null) {
			pType.setTargetNamespaceForChildTypes(pType.getTargetNamespace());
		}
	}

	/**
	 * @param pParentType parent node type    @return related target namespace
	 * @param pWSDLModel  wsdl model
	 * @param pType       type to initialize
	 * @param pNode       node
	 * @param pTypeValue  node 'type' value
	 */
	public static void initTypeTargetNamespace(WSDLModel pWSDLModel, Type pType, Type pParentType, Node pNode, String
			pTypeValue) {
		pType.setTargetNamespace(null);
		pType.setTargetNamespaceForChildTypes(null);

		initTargetNamespace(pWSDLModel, pType, pParentType, pNode);
		initTargetNamespaceForChildTypes(pWSDLModel, pType, pNode, pTypeValue);

/*
		// check type prefix
		if(pTypeValue.contains(DELIMITER)) {
			// type="java1:ProcessSOHeaderProcessing" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"
			String[] typeInfo = pTypeValue.split(DELIMITER);
			String prefix = typeInfo[0];

			if(!pWSDLModel.containsCommonTargetNamespace(prefix)) {
				String namespace = DomUtil.getAttributeValue(pNode, ATTRIBUTE_XMLNS + DELIMITER + prefix);
				if(StringUtils.isBlank(namespace)) {
					if(pType.getType() == NodeType.COMPLEX) {
						pType.setTargetNamespaceForChildTypes( pType.getTargetNamespace() );
					}
				}
			}

			if(pParentType !=null) {
				pType.setTargetNamespace( pParentType.getTargetNamespaceForChildTypes() );
			}
			if(pType.getType() == NodeType.COMPLEX) {
				pType.setTargetNamespaceForChildTypes( pWSDLModel.getTargetNamespace( namespace ) );
			}

		} else {
			TargetNamespace targetNamespace = getTargetNamespaceByParentSchema(pWSDLModel, pNode);
			if(pParentType !=null) {
				pType.setTargetNamespace( pParentType.getTargetNamespace() );
				pType.setTargetNamespaceForChildTypes(targetNamespace);
			} else {
				pType.setTargetNamespace( targetNamespace );
			}
			pType.setTargetNamespaceForChildTypes( targetNamespace );
		}
*/

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
		TargetNamespace targetNamespace = DomUtil.getLinkedTargetNamespace(pTypeValue, pWSDLModel);
java:oracle.e1.bssv.JP420000.valueobject
		TargetNamespace result;
		if( ( result = DomUtil.getLinkedTargetNamespace(pTypeValue, pWSDLModel) ) == null ) {
			String typeValue = DomUtil.getAttributeValue(pNode, ATTRIBUTE_TYPE);
			if(!StringUtils.isBlank(typeValue) && typeValue.contains( DELIMITER )) { // we have defined
				String typePrefix = typeValue.split(DELIMITER)[0];
				if(pWSDLModel.containsCommonTargetNamespace(typePrefix)) {
					result = pParentType.getTargetNamespace();
				} else {
					*/
/**
 * process type="java1:ProcessSOHeaderProcessing" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"
 *//*

					result = getTargetNamespaceByComplexTypeDeclaration(pWSDLModel, pNode);
				}
			} else {
				result = getTargetNamespaceByParentSchema(pWSDLModel, pNode);
				if(result == null) {
					result = getTargetNamespaceByWsdlDefinitions(pWSDLModel, pNode);
				}
			}
		}
		return result;
*/
	}

	/**
	 * @param pWSDLModel wsdl model
	 * @param pTypeValue node 'type' value
	 * @return related common target namespace
	 */
	public static TargetNamespace getNodeCommonTargetNamespace(WSDLModel pWSDLModel, String pTypeValue) {
		return DomUtil.getLinkedCommonTargetNamespace(pTypeValue, pWSDLModel);
	}

	/**
	 * @param pWSDLModel wsdl model
	 * @param pNode      node
	 * @return target namespace by complex type and target namespace declaration
	 */
	private static TargetNamespace getTargetNamespaceByComplexTypeDeclaration(WSDLModel pWSDLModel, Node pNode) {
		TargetNamespace result = null;
		String attributeTypeValue = DomUtil.getAttributeValue(pNode, ATTRIBUTE_TYPE);

		String[] values = attributeTypeValue.split(DELIMITER);
		String prefix = values[0];
		// we have now ATTRIBUTE_XMLNS + DELIMITER + prefix
		// which is equal to xmlns:java1 from our sample
		// we can check if there is such a declaration as an attribute
		String namespace = DomUtil.getAttributeValue(pNode, ATTRIBUTE_XMLNS + DELIMITER + prefix);

		if (!StringUtils.isBlank(namespace)) {
			result = pWSDLModel.getTargetNamespace(namespace);
		} else {
			result = pWSDLModel.getTargetNamespaceByPrefix(prefix);
		}

		return result;
	}

	/**
	 * @param pWSDLModel wsdl model
	 * @param pNode      node to get target namespace
	 * @return target namespace from parent schema node
	 */
	private static TargetNamespace getTargetNamespaceByParentSchema(WSDLModel pWSDLModel, Node pNode) {
		if (pNode == null) {
			return null;
		}
		if (DomUtil.areEqual(pNode.getNodeName(), TYPE_SCHEMA)) {
			String targetNamespace = DomUtil.getAttributeValue(pNode, DomConstants.ATTRIBUTE_TARGET_NAMESPACE);
			if (targetNamespace == null) {
				return null;
			} else {
				return pWSDLModel.getTargetNamespaceByNamespace(targetNamespace);
			}
		} else if (pNode.getNodeName().equals(TYPE_TYPES)) {
			return null;
		} else {
			return getTargetNamespaceByParentSchema(pWSDLModel, pNode.getParentNode());
		}
	}

	/**
	 * @param pWSDLModel wsdl model
	 * @param pNode      node
	 * @return target namespace declaration from wsdl-file definition
	 */
	private static TargetNamespace getTargetNamespaceByWsdlDefinitions(WSDLModel pWSDLModel, Node pNode) {
		TargetNamespace result = null;
		Document document = pNode.getOwnerDocument();
		Node definitions = DomUtil.getNodeByName(document.getChildNodes(), TYPE_DEFINITIONS);
		if (definitions != null) {
			String targetNamespaceName = DomUtil.getAttributeValue(definitions, ATTRIBUTE_TARGET_NAMESPACE);
			if (!StringUtils.isBlank(targetNamespaceName)) {
				result = pWSDLModel.getTargetNamespace(targetNamespaceName);
			}
		}
		return result;
	}


}
