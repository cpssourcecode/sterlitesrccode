package vsg.integration.export.parse.wsdl.analyzer;

import atg.nucleus.GenericService;
import vsg.integration.export.parse.wsdl.DomConstants;


/**
 * @author Dmitry Golubev
 */
public abstract class BaseDocumentAnalyzer extends GenericService implements IDocumentAnalyzer, DomConstants {
	/**
	 * type sorter
	 */
	private TypeUtil mTypeUtil = new TypeUtil();

	//------------------------------------------------------------------------------------------------------------------

	public TypeUtil getTypeUtil() {
		return mTypeUtil;
	}

}
