package vsg.integration.export.parse.wsdl;

import atg.nucleus.GenericService;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import vsg.integration.export.parse.wsdl.analyzer.OperationAnalyzer;
import vsg.integration.export.parse.wsdl.analyzer.TypeAnalyzer;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author Dmitry Golubev
 */
public class WSDLParser extends GenericService implements DomConstants {
	/**
	 * wsdl attributes former
	 */
	private TypeAnalyzer mTypeAnalyzer;
	/**
	 * wsdl operations former
	 */
	private OperationAnalyzer mOperationAnalyzer;

	/**
	 * registry with parsed wsdl models
	 */
	private WSDLRepository mWSDLRepository;

	//------------------------------------------------------------------------------------------------------------------

	public TypeAnalyzer getTypeAnalyzer() {
		if (mTypeAnalyzer == null) { // for test
			mTypeAnalyzer = new TypeAnalyzer(this);
		}
		return mTypeAnalyzer;
	}

	public void setTypeAnalyzer(TypeAnalyzer pTypeAnalyzer) {
		mTypeAnalyzer = pTypeAnalyzer;
	}

	public OperationAnalyzer getOperationAnalyzer() {
		if (mOperationAnalyzer == null) { // for tests
			mOperationAnalyzer = new OperationAnalyzer();
		}
		return mOperationAnalyzer;
	}

	public void setOperationAnalyzer(OperationAnalyzer pOperationAnalyzer) {
		mOperationAnalyzer = pOperationAnalyzer;
	}

	public WSDLRepository getWSDLRepository() {
		if (mWSDLRepository == null) { // for tests
			mWSDLRepository = new WSDLRepository();
		}
		return mWSDLRepository;
	}

	public void setWSDLRepository(WSDLRepository pWSDLRepository) {
		mWSDLRepository = pWSDLRepository;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * build document model
	 *
	 * @param pWsdl wsdl to parse
	 * @return formed document model
	 * @throws ParserConfigurationException if parse error occurs
	 * @throws IOException                  if read error occurs
	 * @throws SAXException                 if parse error occurs
	 */
	public Document getDocument(String pWsdl) throws ParserConfigurationException, IOException, SAXException {

/*
		String TEST_URL = pWsdl;//"https://172.18.0.13:9087/PY900/SalesOrderManagerCP";
		URL url = new URL(TEST_URL);
		HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection();
		httpsCon.setHostnameVerifier(new HostnameVerifier()
		{
			public boolean verify(String hostname, SSLSession session)
			{
				return true;
			}
		});
		httpsCon.connect();
		InputStream is = httpsCon.getInputStream();
		int nread = 0;
		byte [] buf = new byte[8192];
		while ((nread = is.read(buf)) != -1)
		{
			logDebug(new String(buf));
			// System.out.write(buf, 0, nread);
		}
*/


		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true); // never forget this!
		factory.setIgnoringComments(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(pWsdl);
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * analyze wsdl
	 *
	 * @param pWsdlToAnalyze wsdl path
	 * @return wsdl model
	 */
	public WSDLModel analyze(String pWsdlToAnalyze) {
		WSDLModel model;
		if (getWSDLRepository().isRegistered(pWsdlToAnalyze)) {
			model = getWSDLRepository().get(pWsdlToAnalyze);
			vlogDebug("ExportConfig is retrieved from cache. {0}", pWsdlToAnalyze);
		} else {
			model = buildModel(pWsdlToAnalyze);
			if (model != null) {
				getWSDLRepository().register(pWsdlToAnalyze, model);
				vlogDebug("ExportConfig is created by parser. {0}", pWsdlToAnalyze);
			} else {
				vlogError("ExportConfig is not created. {0}", pWsdlToAnalyze);
			}
		}
		return model;
	}

	/**
	 * build model according to wsdl
	 *
	 * @param pWsdlToAnalyze wsdl path
	 * @return wsdl model
	 */
	private WSDLModel buildModel(String pWsdlToAnalyze) {
		try {
			vlogDebug(pWsdlToAnalyze + " to analyze");
			if (pWsdlToAnalyze == null) {
				return null;
			}
			WSDLModel model = new WSDLModel(pWsdlToAnalyze);
			model.setWsdlDocument(getDocument(pWsdlToAnalyze));
			getTypeAnalyzer().analyzeDocument(model);
			getOperationAnalyzer().analyzeDocument(model);
			initSOAPAddressLocation(model);
			return model;
		} catch (Exception e) {
			vlogError(e, "Unable to build wsdl model");
		}
		return null;
	}

	/**
	 * get SOAP address location from wsdl
	 *
	 * @param pWSDLModel wsdlm odel
	 */
	private void initSOAPAddressLocation(WSDLModel pWSDLModel) {
		Node service = DomUtil.getNodeByName(pWSDLModel.getWsdlDocument().getChildNodes(), TYPE_SERVICE);
		if (service != null) { // should be!
			Node serviceAddress = DomUtil.getNodeByName(service.getChildNodes(), TYPE_SERVICE_ADDRESS);
			if (serviceAddress != null) {
				pWSDLModel.setSOAPAddressLocation(
						DomUtil.getAttributeValue(serviceAddress, ATTRIBUTE_SERVICE_PORT_LOCATION)
				);
			}
		}
	}

}

