package vsg.integration.export.parse.wsdl;

/**
 * @author Dmitry Golubev
 */
public interface DomConstants {
	/**
	 * complex type of wsdl elements
	 */
	String TYPE_COMPLEX_TYPE = "complexType";
	/**
	 * simple type of wsdl elements
	 */
	String TYPE_SIMPLE_TYPE = "simpleType";
	/**
	 * element type of wsdl elements
	 */
	String TYPE_ELEMENT = "element";
	/**
	 * attribute type of wsdl elements
	 */
	String TYPE_ATTRIBUTE = "attribute";
	/**
	 * enumeration type of wsdl elements
	 */
	String TYPE_ENUMERATION = "enumeration";
	/**
	 * import node of wsdl
	 */
	String TYPE_IMPORT = "import";
	/**
	 * types node of wsdl
	 */
	String TYPE_TYPES = "types";
	/**
	 * schema node of wsdl
	 */
	String TYPE_SCHEMA = "schema";
	/**
	 * restriction node of wsdl
	 */
	String TYPE_RESTRICTION = "restriction";
	/**
	 * wsdl element attribute arrayType
	 */
	String ATTRIBUTE_ARRAY_TYPE = "arrayType";
	/**
	 * wsdl element attribute soapAction
	 */
	String ATTRIBUTE_SOAP_ACTION = "soapAction";
	/**
	 * wsdl element attribute encodingStyle
	 */
	String ATTRIBUTE_ENDCODING_STYLE = "encodingStyle";
	/**
	 * wsdl element attribute name
	 */
	String ATTRIBUTE_NAME = "name";
	/**
	 * wsdl element attribute base
	 */
	String ATTRIBUTE_BASE = "base";
	/**
	 * wsdl element attribute xmlns
	 */
	String ATTRIBUTE_XMLNS = "xmlns";
	/**
	 * wsdl element attribute type
	 */
	String ATTRIBUTE_TYPE = "type";
	/**
	 * wsdl element attribute value
	 */
	String ATTRIBUTE_VALUE = "value";
	/**
	 * wsdl element attribute message
	 */
	String ATTRIBUTE_MESSAGE = "message";
	/**
	 * wsdl element attribute location
	 */
	String ATTRIBUTE_LOCATION = "location";
	/**
	 * wsdl element attribute schemaLocation
	 */
	String ATTRIBUTE_SCHEMA_LOCATION = "schemaLocation";
	/**
	 * wsdl element attribute minOccurs
	 */
	String ATTRIBUTE_MIN_OCCURS = "minOccurs";
	/**
	 * wsdl element attribute maxOccurs
	 */
	String ATTRIBUTE_MAX_OCCURS = "maxOccurs";
	/**
	 * value for occurs attribute
	 */
	String ATTRIBUTE_OCCURS_VALUE_UNBOUNDED = "unbounded";
	/**
	 * wsdl element attribute href
	 */
	String ATTRIBUTE_HREF = "href";
	/**
	 * wsdl element attribute id
	 */
	String ATTRIBUTE_ID = "id";
	/**
	 * wsdl element attribute targetNamespace
	 */
	String ATTRIBUTE_TARGET_NAMESPACE = "targetNamespace";
	/**
	 * porttype wsdl-node name
	 */
	String TYPE_PORTTYPE = "portType";
	/**
	 * binding wsdl-node name
	 */
	String TYPE_BINDING = "binding";
	/**
	 * definitions wsdl-node name
	 */
	String TYPE_DEFINITIONS = "definitions";
	/**
	 * service wsdl-node name
	 */
	String TYPE_SERVICE = "service";
	/**
	 * operation wsdl-node name
	 */
	String TYPE_OPERATION = "operation";
	/**
	 * message type of wsdl elements
	 */
	String TYPE_MESSAGE = "message";
	/**
	 * part subelement of message type of wsdl elements
	 */
	String TYPE_MESSAGE_PART = "part";
	/**
	 * part subelement of message type of wsdl elements
	 */
	String ATTRIBUTE_PART_ELEMENT = "element";
	/**
	 * part name of subelement of message type of wsdl elements
	 */
	String ATTRIBUTE_PART_NAME = "name";
	/**
	 * part type of subelement of message type of wsdl elements
	 */
	String ATTRIBUTE_PART_TYPE = "type";
	/**
	 * input subelement of operation type of wsdl elements
	 */
	String TYPE_OPERATION_INPUT = "input";
	/**
	 * output subelement of operation type of wsdl elements
	 */
	String TYPE_OPERATION_OUTPUT = "output";
	/**
	 * body subelement of operation type of wsdl elements
	 */
	String TYPE_OPERATION_BODY = "body";
	/**
	 * address sub type of service type of wsdl elements
	 */
	String TYPE_SERVICE_ADDRESS = "address";
	/**
	 * location attribute of address type of wsdl elements
	 */
	String ATTRIBUTE_SERVICE_PORT_LOCATION = "location";
	/**
	 * href id value prefix
	 */
	String HREF_ID_PREFIX = "#";
	/**
	 * delimiter character
	 */
	String DELIMITER = ":";
}
