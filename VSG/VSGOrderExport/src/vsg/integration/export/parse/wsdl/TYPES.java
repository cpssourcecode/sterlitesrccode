package vsg.integration.export.parse.wsdl;

/**
 * @author Dmitry Golubev
 */
public enum TYPES {

	/**
	 * complex type of wsdl elements
	 */
	TYPE_COMPLEX_TYPE("complexType"),
	/**
	 * simple type of wsdl elements
	 */
	TYPE_SIMPLE_TYPE("simpleType"),
	/**
	 * element type of wsdl elements
	 */
	TYPE_ELEMENT("element"),
	/**
	 * enumeration type of wsdl elements
	 */
	TYPE_ENUMERATION("enumeration"),
	/**
	 * import node of wsdl
	 */
	TYPE_IMPORT("import"),
	/**
	 * porttype wsdl-node name
	 */
	TYPE_PORTTYPE("portType"),
	/**
	 * definitions wsdl-node name
	 */
	TYPE_DEFINITIONS("definitions"),
	/**
	 * operation wsdl-node name
	 */
	TYPE_OPERATION("operation"),
	/**
	 * message type of wsdl elements
	 */
	TYPE_MESSAGE("message"),
	/**
	 * part subelement of message type of wsdl elements
	 */
	TYPE_MESSAGE_PART("part"),
	/**
	 * part subelement of message type of wsdl elements
	 */
	TYPE_MESSAGE_PART_ELEMENT("element"),
	/**
	 * input subelement of operation type of wsdl elements
	 */
	TYPE_OPERATION_INPUT("input"),
	/**
	 * output subelement of operation type of wsdl elements
	 */
	TYPE_OPERATION_OUTPUT("output");

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * string presentation
	 */
	private String mValue;

	/**
	 * constructor
	 *
	 * @param pValue string presentation
	 */
	TYPES(String pValue) {
		mValue = pValue;
	}

}
