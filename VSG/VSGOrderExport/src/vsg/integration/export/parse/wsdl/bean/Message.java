package vsg.integration.export.parse.wsdl.bean;

/**
 * class for message elements<br>
 * <pre> {@code <wsdl:message name="...">
 *   <wsdl:part element="tns:..." name="parameters"/>
 * </wsdl:message>
 * }
 * </pre>
 *
 * @author Dmitry Golubev
 */
public class Message {
	/**
	 * message name
	 */
	private String mName;
	/**
	 * usage type
	 */
	private Type mType;
	/**
	 * encoding
	 */
	private String mEncoding;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 */
	public Message() {
	}

	/**
	 * constructor
	 *
	 * @param pName message name
	 * @param pType type
	 */
	public Message(String pName, Type pType) {
		mName = pName;
		mType = pType;
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getName() {
		return mName;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public Type getType() {
		return mType;
	}

	public void setType(Type pType) {
		mType = pType;
	}

	public String getEncoding() {
		return mEncoding;
	}

	public void setEncoding(String pEncoding) {
		mEncoding = pEncoding;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pTypeFullPath type full path
	 * @return analyze type and its sub-types for type full path match and return founded
	 */
	public Type getType(String pTypeFullPath) {
		return getType(getType(), pTypeFullPath);
	}

	/**
	 * @param pType         type
	 * @param pTypeFullPath type full path
	 * @return analyze type and its sub-types for type full path match and return founded
	 */
	private Type getType(Type pType, String pTypeFullPath) {
		if (pType == null) {
			return null;
		}
		if (isTypeAcceptable(pType, pTypeFullPath)) {
			return pType;
		} else {
			if (pType.getSubTypes() != null) {
				Type resultType = null;
				for (Type subType : pType.getSubTypes()) {
					resultType = getType(subType, pTypeFullPath);
					if (resultType != null) {
						return resultType;
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param pType         type to check
	 * @param pTypeFullPath type full path
	 * @return true if type has specified type name
	 */
	private boolean isTypeAcceptable(Type pType, String pTypeFullPath) {
		return pType.getFullPath().equals(pTypeFullPath);
	}

}
