package vsg.integration.export.parse.wsdl.bean;

/**
 * class for operation input/ouput elements<br>
 * <pre> {@code <wsdl:operation name="...">
 *   <wsdl:input message="tns:.."/>
 *   <wsdl:output message="tns:..."/>
 * </wsdl:operation>
 * }
 * </pre>
 *
 * @author Dmitry Golubev
 */
public class OperationAction {
	/**
	 * operation action name
	 */
	private String mName;
	/**
	 * type
	 */
	private Type mType;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 */
	public OperationAction() {
	}

	/**
	 * constructor
	 *
	 * @param pName name
	 * @param pType type
	 */
	public OperationAction(String pName, Type pType) {
		mName = pName;
		mType = pType;
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getName() {
		return mName;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public Type getType() {
		return mType;
	}

	public void setType(Type pType) {
		mType = pType;
	}
}
