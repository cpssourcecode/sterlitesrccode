package vsg.integration.export.parse.wsdl.analyzer;

import org.w3c.dom.Node;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.TargetNamespace;

/**
 * @author Dmitry Golubev
 */
public class NodeInfo {
	/**
	 * node
	 */
	Node mNode;
	/**
	 * node type
	 */
	private NodeType mNodeType = NodeType.ELEMENT;
	/**
	 * target namespace
	 */
	private TargetNamespace mTargetNamespace;

	//------------------------------------------------------------------------------------------------------------------

	public NodeInfo(Node pNode) {
		mNode = pNode;
	}

	public NodeInfo(Node pNode, NodeType pNodeType, TargetNamespace pTargetNamespace) {
		mNode = pNode;
		mNodeType = pNodeType;
		mTargetNamespace = pTargetNamespace;
	}

	//------------------------------------------------------------------------------------------------------------------

	public Node getNode() {
		return mNode;
	}

	public void setNode(Node pNode) {
		mNode = pNode;
	}

	public NodeType getNodeType() {
		return mNodeType;
	}

	public void setNodeType(NodeType pNodeType) {
		mNodeType = pNodeType;
	}

	public TargetNamespace getTargetNamespace() {
		return mTargetNamespace;
	}

	public void setTargetNamespace(TargetNamespace pTargetNamespace) {
		mTargetNamespace = pTargetNamespace;
	}
}
