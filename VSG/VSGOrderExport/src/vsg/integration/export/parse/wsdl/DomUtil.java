package vsg.integration.export.parse.wsdl;

import atg.core.util.StringUtils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import vsg.integration.export.parse.wsdl.bean.TargetNamespace;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.parse.wsdl.bean.WsdlElementDefinitionInfo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class DomUtil implements DomConstants {

	private static List<String> mSupportableTypes = new LinkedList<String>();

	static {
		mSupportableTypes.add(TYPE_PORTTYPE);
		mSupportableTypes.add(TYPE_BINDING);
		mSupportableTypes.add(TYPE_ELEMENT);
		mSupportableTypes.add(TYPE_COMPLEX_TYPE);
		mSupportableTypes.add(TYPE_SIMPLE_TYPE);
		mSupportableTypes.add(TYPE_ENUMERATION);
		mSupportableTypes.add(TYPE_IMPORT);
		mSupportableTypes.add(TYPE_MESSAGE);
		mSupportableTypes.add(TYPE_OPERATION);
		mSupportableTypes.add(TYPE_OPERATION_INPUT);
		mSupportableTypes.add(TYPE_OPERATION_OUTPUT);
		mSupportableTypes.add(TYPE_SCHEMA);
	}

/*
	public static boolean isAttributeOfSimleType(Type pType) {
		return areEqual(pType.getTypeName(), TYPE_SIMPLE_TYPE);
	}

	public static boolean isAttributeOfComplexType(Type pType) {
		return areEqual(pType.getTypeName(), TYPE_COMPLEX_TYPE);
	}

	public static boolean isAttributeOfElement(Type pType) {
		return areEqual(pType.getTypeName(), TYPE_ELEMENT);
	}
*/

	/**
	 * @param pType      attribute type
	 * @param pWSDLModel wsdl model
	 * @return type contains link to another type
	 */
	public static TargetNamespace getLinkedTargetNamespace(String pType, WSDLModel pWSDLModel) {
		if (StringUtils.isBlank(pType)) {
			return null;
		} else {
			for (TargetNamespace targetNamespace : pWSDLModel.getTargetNamespaces()) {
				if (pType.startsWith(targetNamespace.getPrefix() + DELIMITER)) {
					return targetNamespace;
				}
			}
		}
		return null;
	}

	/**
	 * @param pType      attribute type
	 * @param pWSDLModel wsdl model
	 * @return type contains link to another type
	 */
	public static TargetNamespace getLinkedCommonTargetNamespace(String pType, WSDLModel pWSDLModel) {
		if (StringUtils.isBlank(pType)) {
			return null;
		} else {
			for (TargetNamespace targetNamespace : pWSDLModel.getCommonTargetNamespaces()) {
				if (pType.startsWith(targetNamespace.getPrefix() + DELIMITER)) {
					return targetNamespace;
				}
			}
		}
		return null;
	}

	/**
	 * @param pWSDLModel wsdl model
	 * @param pType      attribute type
	 * @return realt type value without link prefix
	 */
	public static WsdlElementDefinitionInfo getRealTypeForLink(WSDLModel pWSDLModel, String pType) {
		try {
			for (TargetNamespace targetNamespace : pWSDLModel.getTargetNamespaces()) {
				if (pType.startsWith(targetNamespace.getPrefix() + DELIMITER)) {
					WsdlElementDefinitionInfo info = new WsdlElementDefinitionInfo();
					info.setTargetNamespace(targetNamespace);
					info.setName(pType.substring((targetNamespace.getPrefix() + DELIMITER).length()));
					return info;
				}
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	public static boolean isNodeAcceptable(Node pNode, String pNodeSearchName) {
		boolean initialCheck =
				mSupportableTypes.contains(pNodeSearchName) &&
						areEqual(pNode.getNodeName(), pNodeSearchName);
		boolean additionalCheck = isElementOfComplexType(pNode, pNodeSearchName);
		return initialCheck || additionalCheck;
	}

	private static boolean isElementOfComplexType(Node pNode, String pNodeSearchName) {
		if (areEqual(pNodeSearchName, TYPE_COMPLEX_TYPE) && areEqual(pNode.getNodeName(), TYPE_ELEMENT)) {
			Node checkComplexNode = getNodeByName(pNode.getChildNodes(), TYPE_COMPLEX_TYPE);
			if (checkComplexNode != null) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param pNodeList node list
	 * @param pNodeName search node name
	 * @return first found by name node
	 */
	public static Node getNodeByName(NodeList pNodeList, String pNodeName) {
		return getNodeByName(pNodeList, pNodeName, null);
	}

	/**
	 * analyze tree with all nodes on all level for search name coincidence
	 *
	 * @param pNodeList node list
	 * @param pNodeName node name to find
	 * @return found nodes of specified name
	 */
	public static Set<Node> getNodesByName(NodeList pNodeList, String pNodeName) {
		return getNodesByName(pNodeList, pNodeName, null);
	}

	/**
	 * analyze tree with all nodes on one level for search name coincidence
	 *
	 * @param pNodeList node list
	 * @param pNodeName node name to find
	 * @return found nodes of specified name
	 */
	public static Set<Node> getNodesByNameFromDirectChildren(NodeList pNodeList, String pNodeName) {
		return getNodesByNameFromDirectChildren(pNodeList, pNodeName, null);
	}

	/***
	 * @param pNode         xml node
	 * @param pExcludeNames exclude names
	 * @return true if node should be skipped from analyze
	 */
	private static boolean skipNode(Node pNode, String[] pExcludeNames) {
		return pNode.getNodeType() != Node.ELEMENT_NODE || skipNodeAnalyze(pNode.getLocalName(), pExcludeNames);
	}

	/**
	 * @param pNodeList     node list
	 * @param pNodeName     node name to find
	 * @param pExcludeNames exclude node names (no digging under node with specified ignore name)
	 * @return found node of specified name
	 */
	public static Node getNodeByName(NodeList pNodeList, String pNodeName, String[] pExcludeNames) {
		for (int i = 0; i < pNodeList.getLength(); i++) {
			Node node = pNodeList.item(i);
			if (skipNode(node, pExcludeNames)) {
				continue;
			}
			if (areEqual(pNodeName, node.getLocalName())) {
				return node;
			}
			if (node.getChildNodes() != null && node.getChildNodes().getLength() > 0) {
				Node nodeFromChildren = getNodeByName(node.getChildNodes(), pNodeName, pExcludeNames);
				if (nodeFromChildren != null) {
					return nodeFromChildren;
				}
			}
		}
		return null;
	}

	/**
	 * analyze tree with all nodes on all level for search name coincidence
	 *
	 * @param pNodeList     node list
	 * @param pNodeName     node name to find
	 * @param pExcludeNames exclude node names (no digging under node with specified ignore name)
	 * @return found nodes of specified name
	 */
	public static Set<Node> getNodesByName(NodeList pNodeList, String pNodeName, String[] pExcludeNames) {
		Set<Node> nodeSet = new HashSet<Node>();
		for (int i = 0; i < pNodeList.getLength(); i++) {
			Node node = pNodeList.item(i);
			if (skipNode(node, pExcludeNames)) {
				continue;
			}
			if (areEqual(pNodeName, node.getLocalName())) {
				nodeSet.add(node);
			}
			if (node.getChildNodes() != null && node.getChildNodes().getLength() > 0) {
				nodeSet.addAll(getNodesByName(node.getChildNodes(), pNodeName, pExcludeNames));
			}
		}
		return nodeSet;
	}

	/**
	 * analyze tree with all nodes on one level for search name coincidence
	 *
	 * @param pNodeList     node list
	 * @param pNodeName     node name to find
	 * @param pExcludeNames exclude node names (no digging under node with specified ignore name)
	 * @return found nodes of specified name
	 */
	public static Set<Node> getNodesByNameFromDirectChildren(NodeList pNodeList, String pNodeName, String[]
			pExcludeNames) {
		Set<Node> nodeSet = new HashSet<Node>();
		for (int i = 0; i < pNodeList.getLength(); i++) {
			Node node = pNodeList.item(i);
			if (skipNode(node, pExcludeNames)) {
				continue;
			}
			if (areEqual(pNodeName, node.getLocalName())) {
				nodeSet.add(node);
			}
		}
		return nodeSet;
	}

	/**
	 * analyze tree with all nodes on all level for search name coincidence
	 *
	 * @param pNodeList       node list
	 * @param pNodeName       node name to find
	 * @param pAttributeName  attribute name
	 * @param pAttributeValue attribute value
	 * @return found nodes of specified name and attribute value
	 */
	public static Set<Node> getNodesByNameAndAttribute(NodeList pNodeList,
													   String pNodeName,
													   String pAttributeName,
													   String pAttributeValue) {
		Set<Node> nodeSet = new HashSet<Node>();
		for (int i = 0; i < pNodeList.getLength(); i++) {
			Node node = pNodeList.item(i);
			if (areEqual(pNodeName, node.getLocalName())) {
				if (pAttributeValue.equals(HREF_ID_PREFIX + DomUtil.getAttributeValue(node, pAttributeName))) {
					nodeSet.add(node);
				}
			}
			if (node.getChildNodes() != null && node.getChildNodes().getLength() > 0) {
				nodeSet.addAll(getNodesByNameAndAttribute(
								node.getChildNodes(),
								pNodeName,
								pAttributeName,
								pAttributeValue)
				);
			}
		}
		return nodeSet;
	}

	public static void formListOfNodesByName(Node pParentNode, List<Node> pChildNodesToFill, String pNodeSearchName) {
		formListOfNodesByName(pParentNode, pChildNodesToFill, pNodeSearchName, null);
	}

	public static void formListOfNodesByName(Node pParentNode, List<Node> pListOfNodes, String pNodeSearchName,
											 String[] pExcludeNames) {
		NodeList childNodes = pParentNode.getChildNodes();
		if (childNodes != null) {
			for (int i = 0; i < childNodes.getLength(); i++) {
				Node childNode = childNodes.item(i);
				boolean isNodeSupportable = isNodeAcceptable(childNode, pNodeSearchName);
				if (isNodeSupportable) {
					pListOfNodes.add(childNode);
					continue;
				} else {
					if (skipNodeAnalyze(childNode.getLocalName(), pExcludeNames)) {
						continue;
					}
				}
				formListOfNodesByName(childNode, pListOfNodes, pNodeSearchName, pExcludeNames);
			}
		}
	}

	private static boolean skipNodeAnalyze(String pNodeName, String[] pExcludeNames) {
		if (pExcludeNames != null) {
			for (String excludeName : pExcludeNames) {
				if (areEqual(pNodeName, excludeName)) {
					return true;
				}
			}
		}
		return false;
	}

	public static boolean areEqual(String pStr1, String pStr2) {
		return pStr1 == null && pStr2 == null || pStr1 != null && pStr2 != null && pStr1.endsWith(pStr2);
	}

	/**
	 * @param pNode          parent xml node
	 * @param pAttributeName attribute name
	 * @return attribute node value
	 */
	public static String getAttributeValue(Node pNode, String pAttributeName) {
		return getAttributeValue(pNode, pAttributeName, false);
	}

	/**
	 * @param pNode          parent xml node
	 * @param pAttributeName attribute name
	 * @param pExactMatch    exact match for attribute name or contains
	 * @return attribute node value
	 */
	public static String getAttributeValue(Node pNode, String pAttributeName, boolean pExactMatch) {
		if (pNode.getAttributes() != null) {
			for (int i = 0; i < pNode.getAttributes().getLength(); i++) {
				Node attribute = pNode.getAttributes().item(i);
				if (attribute.getNodeName() != null) {
					if (pExactMatch && attribute.getNodeName().equals(pAttributeName)) {
						return attribute.getNodeValue();
					}
					if (!pExactMatch && attribute.getNodeName().contains(pAttributeName)) {
						return attribute.getNodeValue();
					}
				}
			}
		}
		return null;
	}

	/**
	 * @param pNode              parent xml node
	 * @param pAttributeNamePart part of attribute name
	 * @return attribute node founded by name
	 */
	public static Node getAttributeByName(Node pNode, String pAttributeNamePart) {
		if (pNode.getAttributes() != null) {
			for (int i = 0; i < pNode.getAttributes().getLength(); i++) {
				Node attribute = pNode.getAttributes().item(i);
				if (attribute.getNodeName() != null && attribute.getNodeName().contains(pAttributeNamePart)) {
					return attribute;
				}
			}
		}
		return null;
	}

	/**
	 * @param pNode              parent xml node
	 * @param pAttributeNamePart part of attribute name
	 * @return attribute node founded by name
	 */
	public static List<Node> getAttributesByName(Node pNode, String pAttributeNamePart) {
		List<Node> attributes = new LinkedList<Node>();
		if (pNode.getAttributes() != null) {
			for (int i = 0; i < pNode.getAttributes().getLength(); i++) {
				Node attribute = pNode.getAttributes().item(i);
				if (attribute.getNodeName() != null && attribute.getNodeName().contains(pAttributeNamePart)) {
					attributes.add(attribute);
				}
			}
		}
		return attributes;
	}

	/**
	 * @param pNode              parent xml node
	 * @param pAttributeNamePart part of attribute name
	 * @param pAttributeValue    attribute value
	 * @return attribute node founded by name
	 */
	public static Node getAttributeByValue(Node pNode, String pAttributeNamePart, String pAttributeValue) {
		if (pNode.getAttributes() != null) {
			for (int i = 0; i < pNode.getAttributes().getLength(); i++) {
				Node attribute = pNode.getAttributes().item(i);
				if (
						(attribute.getNodeValue() != null && attribute.getNodeValue().equals(pAttributeValue)) &&
								(attribute.getNodeName() != null && attribute.getNodeName().contains(pAttributeNamePart))
						) {
					return attribute;
				}
			}
		}
		return null;
	}

	public static String getNodeNameByAttribute(Node pNode) {
		return getAttributeValue(pNode, ATTRIBUTE_NAME);
	}

	public static String getNodeTypeByAttribute(Node pNode) {
		return getAttributeValue(pNode, ATTRIBUTE_TYPE, true);
	}

	/**
	 * @param pNode xml-document node
	 * @return value of xml-document node
	 */
	public static String getNodeValue(Node pNode) {
		if (pNode == null) {
			return null;
		}
		String nodeValue = pNode.getNodeValue();
		if (nodeValue == null) {
			nodeValue = getAttributeValue(pNode, ATTRIBUTE_VALUE);
		}
		if (nodeValue == null) {
			NodeList childNodes = pNode.getChildNodes();
			if (childNodes != null && childNodes.getLength() > 0) {
				nodeValue = childNodes.item(0).getNodeValue();
			}
		}
		return nodeValue;
	}
}
