package vsg.integration.export.parse.wsdl.analyzer;

import org.xml.sax.SAXException;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author Dmitry Golubev
 */
public interface IDocumentAnalyzer {

	/**
	 * analyze document for model init
	 *
	 * @param pWSDLModel wsdl model to fill
	 * @throws IOException                  if error occurs
	 * @throws SAXException                 if error occurs
	 * @throws ParserConfigurationException if error occurs
	 */
	void analyzeDocument(WSDLModel pWSDLModel)
			throws IOException, SAXException, ParserConfigurationException;

}
