package vsg.integration.export;

import atg.nucleus.GenericService;

/**
 * @author Dmitry Golubev
 */
public class ExportConfig extends GenericService {
	/**
	 * Id came from repository of existing model
	 */
	private String mRepositoryId;

	//------------------------------------------------------------------------------------------------------------------

	public String getRepositoryId() {
		return mRepositoryId;
	}

	public void setRepositoryId(String pRepositoryId) {
		mRepositoryId = pRepositoryId;
	}

}
