package vsg.integration.export;

/**
 * @author Dmitry Golubev
 */
public interface IRepositoryConstants {

	/**
	 * report item-descriptor order
	 */
	String ID_ORDER = "order";

	/**
	 * export item-descriptor name
	 */
	String ID_EXPORT_CONFIG = "exportConfig";
	/**
	 * exportJobTime item-descriptor name
	 */
	String ID_EXPORT_JOB_TIME = "exportJobTime";
	/**
	 * export item-descriptor property wsdlUri name
	 */
	String PRP_EXPORT_WSDL = "wsdlUri";
	/**
	 * export item-descriptor property markAsExported name
	 */
	String PRP_EXPORT_MARK_AS_EXPORTED = "markAsExported";
	/**
	 * export item-descriptor property dateCreate name
	 */
	String PRP_EXPORT_DATE_CREATE = "dateCreate";
	/**
	 * export item-descriptor property configName name
	 */
	String PRP_EXPORT_NAME = "configName";
	/**
	 * export item-descriptor property configDescription name
	 */
	String PRP_EXPORT_DESCRIPTION = "configDescription";
	/**
	 * export item-descriptor property configAuthor name
	 */
	String PRP_EXPORT_AUTHOR = "configAuthor";
	/**
	 * export item-descriptor property rql name
	 */
	String PRP_EXPORT_RQL = "rql";
	/**
	 * export item-descriptor property repositoryName name
	 */
	String PRP_EXPORT_REPOSITORY_NAME = "repositoryName";
	/**
	 * export item-descriptor property itemDescriptor name
	 */
	String PRP_EXPORT_ITEM_DESCRIPTOR = "itemDescriptor";
	/**
	 * export item-descriptor property recordMappings name
	 */
	String PRP_EXPORT_RECORD_MAPPINGS = "recordMappings";
	/**
	 * export item-descriptor property recordMappings name
	 */
	String PRP_EXPORT_MANDATORY_FIELDS = "mandatoryFields";
	/**
	 * recordMapping item-descriptor name
	 */
	String ID_RECORD_MAPPING = "recordMapping";
	/**
	 * recordMapping item-descriptor property wsdlPath name
	 */
	String PRP_RECORD_MAPPING_WSDL_PATH = "wsdlPath";
	/**
	 * recordMapping item-descriptor property staticValue name
	 */
	String PRP_RECORD_MAPPING_STATIC_VALUE = "staticValue";
	/**
	 * recordMapping item-descriptor property repositoryPath name
	 */
	String PRP_RECORD_MAPPING_ITEM_DESCRIPTOR = "itemDescriptor";
	/**
	 * recordMapping item-descriptor property repositoryPath name
	 */
	String PRP_RECORD_MAPPING_REPOSITORY_PATH = "repositoryPath";
	/**
	 * recordMapping item-descriptor property inout name
	 */
	String PRP_RECORD_MAPPING_INOUT = "inout";
	/**
	 * recordMapping item-descriptor property repositoryName name
	 */
	String PRP_RECORD_MAPPING_REPOSITORY_NAME = "repositoryName";
	/**
	 * recordMapping item-descriptor property exportConfig name
	 */
	String PRP_RECORD_MAPPING_EXPORT_CONFIG = "exportConfig";
	/**
	 * export item-descriptor property repositoryId name
	 */
	String PRP_EXPORT_REPOSITORY_ID = "repositoryId";
	/**
	 * transaction item-descriptor property repositoryId name
	 */
	String PRP_TRANSACTION_STATUS = "status";
	/**
	 * transaction item-descriptor property repositoryIds name
	 */
	String PRP_TRANSACTION_REPOSITORY_IDS = "repositoryIds";
	/**
	 * transaction item-descriptor property dateCreate name
	 */
	String PRP_TRANSACTION_DATE_CREATE = "dateCreate";
	/**
	 * transaction item-descriptor property repositoryPath name
	 */
	String PRP_TRANSACTION_REPOSITORY_PATH = "repositoryPath";
	/**
	 * transaction item-descriptor name
	 */
	String ID_TRANSACTION = "transaction";
	/**
	 * transactionForReport item-descriptor name
	 */
	String ID_TRANSACTION_FOR_REPORT = "transactionForReport";
	/**
	 * transaction item-descriptor property response name
	 */
	String PRP_TRANSACTION_RESPONSE = "response";
	/**
	 * transaction item-descriptor property failReason name
	 */
	String PRP_TRANSACTION_FAIL_REASON = "failReason";
	/**
	 * transaction item-descriptor property request name
	 */
	String PRP_TRANSACTION_REQUEST = "request";
	/**
	 * transaction item-descriptor property exportConfig name
	 */
	String PRP_TRANSACTION_EXPORT_CONFIG = "exportConfig";
	/**
	 * id property name
	 */
	public final static String ID = "id";
	/**
	 * Order Output Format item-descriptor name
	 */
	String ID_OUTPUT_FORMAT_CONFIG = "outputFormatConfig";
	/**
	 * Output Format Config item-descriptor property wsSecurity name
	 */
	String PRP_OFC_WS_SEQURITY = "wsSecurity";
	/**
	 * Output Format Config item-descriptor property schedule name
	 */
	String PRP_OFC_USE_SCHEDULE = "useSchedule";
	/**
	 * Output Format Config item-descriptor property schedule name
	 */
	String PRP_OFC_SCHEDULE = "schedule";
	/**
	 * Output Format Config item-descriptor property endpointUrl name
	 */
	String PRP_OFC_ENDPOINT_URL = "endpointUrl";
	/**
	 * Output Format Config item-descriptor property connectionTimeout name
	 */
	String PRP_OFC_CONNECTION_TIMEOUT = "connectionTimeout";
	/**
	 * Output Format Config item-descriptor property wsdlUrl name
	 */
	String PRP_OFC_WSDL_URL = "wsdlUrl";
	/**
	 * Output Format Config item-descriptor property uploadWsdl name
	 */
	String PRP_OFC_UPLOAD_WSDL = "uploadWsdl";
	/**
	 * Order Output Format item-descriptor property wsdlOperation name
	 */
	String PRP_OFC_WSDL_OPERATION = "wsdlOperation";
	/**
	 * Order Output Format item-descriptor property wsdlOperationAction name
	 */
	String PRP_OFC_WSDL_OPERATION_ACTION = "wsdlOperationAction";

	//--------------Repository-items property names--------------------------------

	/**
	 * The Constant ID_OUT_REPORT_CONFIG. item-descriptor name
	 */
	String ID_OUT_REPORT_CONFIG = "outputReportConfig";
	/**
	 * The Constant ID_EMAIL_DELIVERY_CONFIG. item-descriptor name
	 */
	String ID_EMAIL_DELIVERY_CONFIG = "emailDeliveryConfig";
	/**
	 * The Constant ID_FTP_DELIVERY_CONFIG. item-descriptor name
	 */
	String ID_FTP_DELIVERY_CONFIG = "ftpDeliveryConfig";
	/**
	 * exportConfig item-descriptor property outputFormatConfig.
	 */
	String PRP_EXPORT_OUTPUT_FORMAT = "outputFormatConfig";
	/**
	 * exportConfig item-descriptor property transferOptionsConfig.
	 */
	String PRP_EXPORT_TRANSFER_OPTIONS = "transferOptionsConfig";
	/**
	 * The Constant PRP_EXPORT_OUT_REPORT_CONFIG. exportConfig item-descriptor
	 * property.
	 */
	String PRP_EXPORT_OUT_REPORT_CONFIG = "outputReportConfig";

	/**
	 * The Constant PRP_REP_CONF_TYPE. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_TYPE = "type";
	/**
	 * The Constant PRP_REP_CONF_FREQ. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_FREQ = "frequency";
	/**
	 * The Constant PRP_REP_CONF_SFREQ. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_SFREQ = "sfrequency";
	/**
	 * The Constant PRP_REP_CONF_ENABLE_SUCCESS. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_ENABLE_SUCCESS = "enableSuccess";
	/**
	 * The Constant PRP_REP_CONF_FORMAT. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_FORMAT = "format";
	/**
	 * The Constant PRP_REP_CONF_EFREQ. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_EFREQ = "efrequency";
	/**
	 * The Constant PRP_REP_CONF_ENABLE_ERROR. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_ENABLE_ERROR = "enableError";
	/**
	 * The Constant PRP_REP_CONF_FILE_NAME. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_FILE_NAME = "fileName";
	/**
	 * The Constant PRP_REP_CONF_TIME_FORMAT. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_TIME_FORMAT = "timestampFormat";

	/**
	 * The Constant PRP_REP_CONF_STORE_PATH. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_STORE_PATH = "storeDirPath";
	/**
	 * The Constant PRP_REP_CONF_ARCHIVE_PATH. outputReportConfig
	 * item-descriptor property.
	 */
	String PRP_REP_CONF_ARCHIVE_PATH = "archiveDirPath";
	/**
	 * The Constant PRP_REP_CONF_MAIL_CONF. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_MAIL_CONF = "emailDeliveryConfig";
	/**
	 * The Constant PRP_REP_CONF_FFTP_CONF. outputReportConfig item-descriptor
	 * property.
	 */
	String PRP_REP_CONF_FTP_CONF = "ftpDeliveryConfig";
	/**
	 * The Constant PRP_EMAIL_CONF_SUBJ. emailDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_EMAIL_CONF_SUBJ = "subject";
	/**
	 * The Constant PRP_EMAIL_CONF_SENDER. emailDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_EMAIL_CONF_SENDER = "sender";
	/**
	 * The Constant PRP_EMAIL_CONF_RECIPIENTS. emailDeliveryConfig
	 * item-descriptor property.
	 */
	String PRP_EMAIL_CONF_RECIPIENTS = "recipients";
	/**
	 * The Constant PRP_FTP_CONF_PROTOCOL. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_PROTOCOL = "protocol";
	/**
	 * The Constant PRP_FTP_CONF_KEY_FILE_NAME. ftpDeliveryConfig
	 * item-descriptor property.
	 */
	String PRP_FTP_CONF_KEY_FILE_NAME = "keyFileName";
	/**
	 * The Constant PRP_FTP_CONF_HOST_NAME. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_HOST_NAME = "hostName";
	/**
	 * The Constant PRP_FTP_CONF_PORT. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_PORT = "port";
	/**
	 * The Constant PRP_FTP_CONF_OUT_DIR_PATH. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_OUT_DIR_PATH = "outDirPath";
	/**
	 * The Constant PRP_FTP_CONF__USER_NAME. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_USER_NAME = "userName";
	/**
	 * The Constant PRP_FTP_CONF_PASS. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_PASS = "password";
	/**
	 * The Constant PRP_FTP_CONF_ENC_KEY_PATH. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_ENC_KEY_PATH = "encKeyFilePath";
	/**
	 * The Constant PRP_FTP_CONF_ENC_PHRASE. ftpDeliveryConfig item-descriptor
	 * property.
	 */
	String PRP_FTP_CONF_ENC_PHRASE = "encPhrase";
	/**
	 * The Constant FTP_PASS_CONFIRM.
	 */
	String FTP_CONF_PASS_CONFIRM = "passwordConf";
	/**
	 * The Constant USE_EMAIL.
	 */
	String USE_EMAIL = "useEmail";
	/**
	 * The Constant USE_FTP.
	 **/
	String USE_FTP = "useFtp";


	//------------------------------------------------------------------------------------------------------------------
	//                                           TRANSFER OPTIONS CONFIG
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * transferOptionsConfig item-descriptor name
	 */
	String ID_TRANSFER_OPTIONS_CONFIG = "transferOptionsConfig";
	/**
	 * transferOptionsConfig item-descriptor property confirmPassword
	 */
	String PRP_TOC_CONFIRM_PSWD = "confirmPassword";
	/**
	 * transferOptionsConfig item-descriptor property controlFile
	 */
	String PRP_TOC_CONTROL_FILE = "controlFile";
	/**
	 * transferOptionsConfig item-descriptor property encriptionKey
	 */
	String PRP_TOC_ENCRIPTION_KEY = "encriptionKey";
	/**
	 * transferOptionsConfig item-descriptor property encriptionPhrase
	 */
	String PRP_TOC_ENCRIPTION_PHRASE = "encriptionPhrase";
	/**
	 * transferOptionsConfig item-descriptor property fileName
	 */
	String PRP_TOC_FILE_NAME = "fileName";
	/**
	 * transferOptionsConfig item-descriptor property key
	 */
	String PRP_TOC_KEY = "key";
	/**
	 * transferOptionsConfig item-descriptor property outputDirectory
	 */
	String PRP_TOC_OUTPUT_DIRECTORY = "outputDirectory";
	/**
	 * transferOptionsConfig item-descriptor property password
	 */
	String PRP_TOC_PSWD = "password";
	/**
	 * transferOptionsConfig item-descriptor property prefix
	 */
	String PRP_TOC_PREFIX = "prefix";
	/**
	 * transferOptionsConfig item-descriptor property protocol
	 */
	String PRP_TOC_PROTOCOL = "protocol";
	/**
	 * transferOptionsConfig item-descriptor property schedule
	 */
	String PRP_TOC_SCHEDULE = "schedule";
	/**
	 * transferOptionsConfig item-descriptor key for property schedule
	 */
	String PRP_TOC_SCHEDULE_TRANSFER = "scheduleTransfer";
	/**
	 * transferOptionsConfig item-descriptor property singlePerFile
	 */
	String PRP_TOC_SINGLE_PER_FILE = "singlePerFile";
	/**
	 * transferOptionsConfig item-descriptor property sufix
	 */
	String PRP_TOC_SUFIX = "sufix";
	/**
	 * transferOptionsConfig item-descriptor property timeStepFormat
	 */
	String PRP_TOC_TIME_STEP_FORMAT = "timeStepFormat";
	/**
	 * transferOptionsConfig item-descriptor property uploadXsd
	 */
	String PRP_TOC_UPLOAD_XSD = "uploadXsd";
	/**
	 * transferOptionsConfig item-descriptor property urlIp
	 */
	String PRP_TOC_URL_IP = "urlIp";
	/**
	 * transferOptionsConfig item-descriptor property userName
	 */
	String PRP_TOC_USER_NAME = "userName";


	//------------------------------------------------------------------------------------------------------------------
	//                                                REPORT
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * report item-descriptor name
	 */
	String ID_REPORT = "report";
	/**
	 * report item-descriptor property id
	 */
	String PRP_REPORT_ID = "id";
	/**
	 * report item-descriptor property exportConfig
	 */
	String PRP_REPORT_EXPORT_CONFIG = "exportConfig";
	/**
	 * report item-descriptor property dateCreate
	 */
	String PRP_REPORT_DATE_CREATE = "dateCreate";
	/**
	 * report item-descriptor property isEmailed
	 */
	String PRP_REPORT_IS_EMAILED = "isEmailed";
	/**
	 * report item-descriptor property isSuccess
	 */
	String PRP_REPORT_IS_SUCCESS = "isSuccess";
	/**
	 * report item-descriptor property failReason
	 */
	String PRP_REPORT_FAIL_REASON = "failReason";
	/**
	 * report item-descriptor property isFtpPlaced
	 */
	String PRP_REPORT_IS_FTP_PLACED = "isFtpPlaced";
	/**
	 * report item-descriptor property emailFailReason
	 */
	String PRP_REPORT_EMAIL_FAIL_REASON = "emailFailReason";
	/**
	 * report item-descriptor property ftpFailReason
	 */
	String PRP_REPORT_FTP_FAIL_REASON = "ftpFailReason";
	/**
	 * report item-descriptor property fileName
	 */
	String PRP_REPORT_FILE_NAME = "fileName";
	/**
	 * report item-descriptor property transactionIds
	 */
	String PRP_REPORT_TRANSACTION_IDS = "transactionIds";

}