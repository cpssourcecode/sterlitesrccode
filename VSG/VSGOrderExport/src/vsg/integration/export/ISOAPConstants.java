package vsg.integration.export;

/**
 * @author Dmitry Golubev
 */
public interface ISOAPConstants {
	/**
	 * properties delimeter
	 */
	String DELIMITER = ".";
	/**
	 * random flag for static value
	 */
	String RANDOM = "#random#";
}
