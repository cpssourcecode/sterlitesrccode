package vsg.integration.export.util;

import atg.dtm.TransactionDemarcation;
import atg.nucleus.GenericService;

import javax.transaction.Status;
import javax.transaction.TransactionManager;

/**
 * @author Dmitry Golubev
 */
public class TransactionProcessor extends GenericService {

	private TransactionManager mTransactionManager;

	//------------------------------------------------------------------------------------------------------------------

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * process command in transaction, if error occures - rollback will be performed
	 *
	 * @param pCommand command to execute
	 */
	public void process(TransactionProcessorCommand pCommand) {
		if (pCommand == null) {
			return;
		}
		synchronized (pCommand.getSyncObject()) {
			TransactionDemarcation td = new TransactionDemarcation();
			boolean isFailed = false;
			try {
				td.begin(getTransactionManager());
				pCommand.processInTransaction();
				pCommand.processPost();
			} catch (Exception e) {
				isFailed = true;
				pCommand.processError();
				vlogError(e, "Error during transaction process.");
			} finally {
				transactionEnd(td, isFailed);
			}
		}
	}

	/**
	 * @param pDemarcation transaction demacration
	 * @param pIsfailed    is transaction fialed
	 */
	protected void transactionEnd(TransactionDemarcation pDemarcation, boolean pIsfailed) {
		try {
			if (pDemarcation.getTransaction() != null) {
				int status = pDemarcation.getTransaction().getStatus();
				if (status == Status.STATUS_ACTIVE) {
					pDemarcation.end(pIsfailed);
				} else {
					pDemarcation.end(true);
				}
			}
		} catch (Exception e) {
			vlogError(e, "Error in transaction");
		}
	}

}
