package vsg.integration.export.util;

import javax.xml.datatype.DatatypeFactory;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Dmitry Golubev
 */
public class ObjectXMLStringFormatter extends ObjectStringFormatter {
	@Override
	public String formatToString(Object pObject) {
		if (pObject == null) {
			return null;
		}
		try {
			if (pObject.getClass().isAssignableFrom(Date.class) ||
					pObject.getClass().isAssignableFrom(Timestamp.class)) {
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.setTimeInMillis(((Date) pObject).getTime());
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar).toXMLFormat();
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to build string from object: " + e.toString());
		}
		return super.formatToString(pObject);
	}
}
