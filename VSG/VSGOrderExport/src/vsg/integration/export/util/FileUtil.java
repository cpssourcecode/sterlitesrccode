package vsg.integration.export.util;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystemNotFoundException;

/**
 * @author Dmitry Golubev
 */
public class FileUtil {

	/**
	 * retrive file
	 *
	 * @param pDocument       wsdl document
	 * @param pSchemaLocation considered schema location
	 * @return xsd file is available, otherwise - null
	 */
	public static String getFile(Document pDocument, String pSchemaLocation) {
		String xsdFile = null;
		if (FileUtil.isFileExist(pSchemaLocation)) {
			xsdFile = pSchemaLocation;
		} else {
			if (isLink(pSchemaLocation)) {
				if (FileUtil.isFileByUriExist(pSchemaLocation)) {
					xsdFile = pSchemaLocation;
				}
			} else {
				xsdFile = FileUtil.isFileWithDocumentNearExist(pSchemaLocation, pDocument);
			}
		}
		return xsdFile;
	}

	/**
	 * check if path is web link
	 *
	 * @param pPath test path
	 * @return true if it's web link
	 */
	public static boolean isLink(String pPath) {
		return pPath != null && pPath.startsWith("http");
	}

	/**
	 * check if file exist
	 *
	 * @param pFileName file name
	 * @return file exist
	 */
	public static boolean isFileExist(String pFileName) {
		try {
			File pFile = new File(pFileName);
			return pFile.exists();
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * check if file is avalaible online
	 *
	 * @param pUri file uri
	 * @return true if file is avalaible online
	 */
	public static boolean isFileByUriExist(String pUri) {
		if (StringUtils.isBlank(pUri)) {
			return false;
		} else {
			if (pUri.toLowerCase().startsWith("https")) {
				return ConnectionUtil.isHttpsConnectionAvailable(pUri);
			} else {
				return ConnectionUtil.isHttpConnectionAvailable(pUri);
			}
		}
	}

	/**
	 * check if file is placed near with wsdl file
	 *
	 * @param pFileName file to check
	 * @param pDocument wsdl document
	 * @return file path, otherwise - null
	 */
	public static String isFileWithDocumentNearExist(String pFileName, Document pDocument) {
		try {
			URI documentURI = new URI(pDocument.getDocumentURI());

			String path = documentURI.getPath().substring(0, documentURI.getPath().lastIndexOf("/"));
			URI fileURI = new URI(documentURI.getScheme() + "://" + documentURI.getAuthority() + path + "/" + pFileName);
			if (isFileByUriExist(fileURI.toString())) {
				return fileURI.toString();
			}
/*
			File documentFile = new File(documentURI);
			String documentParent = documentFile.getParent();
			File checkFile = new File(documentParent + "/" + pFileName);
			if (checkFile.exists()) {
				return checkFile.getPath();
			}
*/
		} catch (URISyntaxException e) {
			throw new FileSystemNotFoundException(pFileName + ": check isFileWithDocumentNearExist failed: " + e.toString());
		}
		return null;
	}
}
