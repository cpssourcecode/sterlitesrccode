package vsg.integration.export.util.comparator;

import vsg.integration.export.parse.wsdl.bean.TargetNamespace;

import java.util.Comparator;

/**
 * @author Dmitry Golubev
 */
public class ComparatorTargetNamespace extends AbstractStringComparator implements Comparator<TargetNamespace> {
	/**
	 * comporator instanse
	 */
	private static ComparatorTargetNamespace mInstanse = null;

	/**
	 * @return instanse of comparator
	 */
	public static ComparatorTargetNamespace instanse() {
		if (mInstanse == null) {
			mInstanse = new ComparatorTargetNamespace();
		}
		return mInstanse;
	}

	/**
	 * private constructor not to use
	 */
	private ComparatorTargetNamespace() {

	}

	/**
	 * compare two property info for sort by their location
	 *
	 * @param o1 target namespace info
	 * @param o2 target namespace info
	 * @return -1, 0, 1 check for compare method in Comparator class
	 */
	@Override
	public int compare(TargetNamespace o1, TargetNamespace o2) {
		if (o1 == null && o2 == null) {
			return 0;
		} else {
			if ((o1 != null && o2 == null) || (o1 == null && o2 != null)) {
				return -1;
			} else {
				return compareStrings(o1.getNamespace(), o2.getNamespace());
			}
		}
	}

}
