package vsg.integration.export.util.comparator;

import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Type;

import java.util.Comparator;

/**
 * @author Dmitry Golubev
 */
public class ComparatorWsdlType implements Comparator<Type> {
	/**
	 * comporator instanse
	 */
	private static ComparatorWsdlType mInstanse = null;

	/**
	 * @return instanse of comparator
	 */
	public static ComparatorWsdlType instanse() {
		if (mInstanse == null) {
			mInstanse = new ComparatorWsdlType();
		}
		return mInstanse;
	}

	/**
	 * private constructor not to use
	 */
	private ComparatorWsdlType() {

	}

	/**
	 * compare two types for sort by their TYPE
	 *
	 * @param o1 wsdl type
	 * @param o2 wsdl type
	 * @return -1, 0, 1 check for compare method in Comparator class
	 */
	@Override
	public int compare(Type o1, Type o2) {
		switch (o1.getType()) {
			case ELEMENT:
				if (o2.getType() == NodeType.ELEMENT) return 0;
				else return -1;
			case SIMPLE:
				if (o2.getType() == NodeType.COMPLEX) return -1;
				if (o2.getType() == NodeType.SIMPLE) return 0;
				else return 1; // Type.TYPE.ELEMENT
			case COMPLEX:
				if (o2.getType() == NodeType.COMPLEX) return 0;
				else return 1;
		}
		return 0;
	}
}