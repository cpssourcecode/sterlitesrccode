package vsg.integration.export.util.comparator;

import vsg.integration.export.repository.model.PropertyInfo;

import java.util.Comparator;

/**
 * @author Dmitry Golubev
 */
public class ComparatorPropertyInfo extends AbstractStringComparator implements Comparator<PropertyInfo> {
	/**
	 * comporator instanse
	 */
	private static ComparatorPropertyInfo mInstanse = null;

	/**
	 * @return instanse of comparator
	 */
	public static ComparatorPropertyInfo instanse() {
		if (mInstanse == null) {
			mInstanse = new ComparatorPropertyInfo();
		}
		return mInstanse;
	}

	/**
	 * private constructor not to use
	 */
	private ComparatorPropertyInfo() {

	}

	/**
	 * compare two property info for sort by their name
	 *
	 * @param o1 property info
	 * @param o2 property info
	 * @return -1, 0, 1 check for compare method in Comparator class
	 */
	@Override
	public int compare(PropertyInfo o1, PropertyInfo o2) {
		if (o1 == null && o2 == null) {
			return 0;
		} else {
			if ((o1 != null && o2 == null) || (o1 == null && o2 != null)) {
				return -1;
			} else {
				// return compareStrings(o1.getName(), o2.getName());
				return compareStrings(o1.getDisplayName(), o2.getDisplayName());
			}
		}
	}
}
