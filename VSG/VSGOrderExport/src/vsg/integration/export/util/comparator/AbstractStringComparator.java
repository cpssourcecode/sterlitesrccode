package vsg.integration.export.util.comparator;

/**
 * @author Dmitry Golubev
 */
public class AbstractStringComparator {
	/**
	 * compare two strings
	 *
	 * @param pStr1 string 1 to compare
	 * @param pStr2 string 1 to compare
	 * @return -1, 0, 1 check for compare method in Comparator class
	 */
	protected int compareStrings(String pStr1, String pStr2) {
		if (pStr1 == null && pStr2 == null) {
			return 0;
		} else {
			if (pStr1 == null && pStr2 != null) {
				return -1;
			} else {
				if (pStr1 != null && pStr2 == null) {
					return 1;
				} else {
					return pStr1.compareTo(pStr2);
				}
			}
		}
	}
}
