package vsg.integration.export.util.comparator;

import vsg.integration.export.repository.model.ItemDescriptorInfo;

import java.util.Comparator;

/**
 * @author Dmitry Golubev
 */
public class ComparatorItemDescriptorInfo extends AbstractStringComparator implements Comparator<ItemDescriptorInfo> {
	/**
	 * comporator instanse
	 */
	private static ComparatorItemDescriptorInfo mInstanse = null;

	/**
	 * @return instanse of comparator
	 */
	public static ComparatorItemDescriptorInfo instanse() {
		if (mInstanse == null) {
			mInstanse = new ComparatorItemDescriptorInfo();
		}
		return mInstanse;
	}

	/**
	 * private constructor not to use
	 */
	private ComparatorItemDescriptorInfo() {

	}

	/**
	 * compare two property info for sort by their name
	 *
	 * @param o1 property info
	 * @param o2 property info
	 * @return -1, 0, 1 check for compare method in Comparator class
	 */
	@Override
	public int compare(ItemDescriptorInfo o1, ItemDescriptorInfo o2) {
		if (o1 == null && o2 == null) {
			return 0;
		} else {
			if ((o1 != null && o2 == null) || (o1 == null && o2 != null)) {
				return -1;
			} else {
				return compareStrings(o1.getName(), o2.getName());
			}
		}
	}

}
