package vsg.integration.export.util.comparator;

import vsg.integration.export.parse.wsdl.bean.Type;

import java.util.Comparator;

/**
 * is used for proper sorting and display by type name (not its wsdl type name)
 *
 * @author Dmitry Golubev
 */
public class ComparatorType extends AbstractStringComparator implements Comparator<Type> {
	/**
	 * comporator instanse
	 */
	private static ComparatorType mInstanse = null;

	/**
	 * @return instanse of comparator
	 */
	public static ComparatorType instanse() {
		if (mInstanse == null) {
			mInstanse = new ComparatorType();
		}
		return mInstanse;
	}

	/**
	 * private constructor not to use
	 */
	private ComparatorType() {

	}

	/**
	 * compare two property info for sort by their location
	 *
	 * @param o1 target namespace info
	 * @param o2 target namespace info
	 * @return -1, 0, 1 check for compare method in Comparator class
	 */
	@Override
	public int compare(Type o1, Type o2) {
		int result = -2;
		if (o1 == null && o2 == null) {
			result = 0;
		} else {
			if ((o1 != null && o2 == null) || (o1 == null && o2 != null)) {
				result = -1;
			} else {
				if (o1.getType().equals(o2.getType())) {
					result = compareStrings(o1.getName(), o2.getName());
				} else {
					result = 0 - compareStrings(o1.getType().toString(), o2.getType().toString());
				}
			}
		}
		return result;
	}

}
