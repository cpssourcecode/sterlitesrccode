package vsg.integration.export.util.bean;

import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class TransactionInfo {
	/**
	 * success status flag
	 */
	public static final int SUCCESS = 1;
	/**
	 * error status flag
	 */
	public static final int FAIL = 0;
	/**
	 * request
	 */
	private String mRequest;
	/**
	 * response
	 */
	private String mResponse;
	/**
	 * status value (1 - success, 0 - error)
	 */
	private int mStatus = SUCCESS;
	/**
	 * fail reason
	 */
	private String mFailReason;
	/**
	 * set of exported items ids
	 */
	private List<String> mExportedItemsIds;

	//------------------------------------------------------------------------------------------------------------------

	public String getRequest() {
		return mRequest;
	}

	public void setRequest(String pRequest) {
		mRequest = pRequest;
	}

	public String getResponse() {
		return mResponse;
	}

	public void setResponse(String pResponse) {
		mResponse = pResponse;
	}

	public int getStatus() {
		return mStatus;
	}

	public void setStatus(int pStatus) {
		mStatus = pStatus;
	}

	public String getFailReason() {
		return mFailReason;
	}

	public void setFailReason(String pFailReason) {
		mFailReason = pFailReason;
	}

	public List<String> getExportedItemsIds() {
		return mExportedItemsIds;
	}

	public void setExportedItemsIds(List<String> pExportedItemsIds) {
		mExportedItemsIds = pExportedItemsIds;
	}
}
