package vsg.integration.export.util.bean;

import java.util.List;

/**
 * Bean contains table data
 */
public class TableData {

	/**
	 * number of columns
	 */
	private int mColunmsCount;

	/**
	 * table rows
	 */
	private List<String[]> mRows;

	/**
	 * additional info to show before table
	 */
	private String[] mPreTableInfo;

	/**
	 * additional info to show after table
	 */
	private String[] mPostTableInfo;

	/**
	 * columns titles
	 */
	private String[] mColunmsTitles;

	public int getColunmsCount() {
		return mColunmsCount;
	}

	public void setColunmsCount(int pColunmsCount) {
		mColunmsCount = pColunmsCount;
	}

	public List<String[]> getRows() {
		return mRows;
	}

	public void setRows(List<String[]> pRows) {
		mRows = pRows;
	}

	public String[] getPreTableInfo() {
		return mPreTableInfo;
	}

	public void setPreTableInfo(String[] pPreTableInfo) {
		mPreTableInfo = pPreTableInfo;
	}

	public String[] getPostTableInfo() {
		return mPostTableInfo;
	}

	public void setPostTableInfo(String[] pPostTableInfo) {
		mPostTableInfo = pPostTableInfo;
	}

	public String[] getColunmsTitles() {
		return mColunmsTitles;
	}

	public void setColunmsTitles(String[] pColunmsTitles) {
		mColunmsTitles = pColunmsTitles;
	}


}
