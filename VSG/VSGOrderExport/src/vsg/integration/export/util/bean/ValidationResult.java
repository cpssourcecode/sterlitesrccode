package vsg.integration.export.util.bean;

/**
 * @author Dmitry Golubev
 */
public class ValidationResult {
	/**
	 * success flag
	 */
	private boolean mSuccess = true;
	/**
	 * fail reason
	 */
	private String mFailReason;
	/**
	 * attachment for validation
	 */
	private Object mAttachment;
	/**
	 * default success validation result
	 */
	private static final ValidationResult mSussessResult = new ValidationResult();

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * default constructor
	 */
	public ValidationResult() {
	}

	/**
	 * constructor
	 *
	 * @param pSuccess    success flag
	 * @param pFailReason fail reason
	 */
	public ValidationResult(boolean pSuccess, String pFailReason) {
		mSuccess = pSuccess;
		mFailReason = pFailReason;
	}

	/**
	 * @return success validation result
	 */
	public static ValidationResult successValidation() {
		return mSussessResult;
	}

	/**
	 * @param pFailReason fail reason
	 * @return error validation result with fail reason
	 */
	public static ValidationResult errorValidation(String pFailReason) {
		return new ValidationResult(false, pFailReason);
	}

	//------------------------------------------------------------------------------------------------------------------

	public boolean isSuccess() {
		return mSuccess;
	}

	public void setSuccess(boolean pSuccess) {
		mSuccess = pSuccess;
	}

	public String getFailReason() {
		return mFailReason;
	}

	public void setFailReason(String pFailReason) {
		mFailReason = pFailReason;
		mSuccess = false;
	}

	public Object getAttachment() {
		return mAttachment;
	}

	public void setAttachment(Object pAttachment) {
		mAttachment = pAttachment;
	}
}
