package vsg.integration.export.util;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

/**
 * @author Dmitry Golubev
 */
public class ConnectionUtil {
	/**
	 * logger for static class
	 */
	static ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(ConnectionUtil.class);

	/**
	 * flag of initialized ssl verification skip
	 */
	static boolean mSSLVerificationDisabled = false;

	/**
	 * @param pHttpUri http uri
	 * @return true if uri is accessable
	 */
	public static boolean isHttpConnectionAvailable(String pHttpUri) {
		HttpURLConnection con = null;
		try {
			HttpURLConnection.setFollowRedirects(false);
			con = (HttpURLConnection) new URL(pHttpUri).openConnection();
			con.setReadTimeout(5000);
			con.setRequestMethod("GET");
			return con.getResponseCode() == HttpURLConnection.HTTP_OK;
		} catch (Exception e) {
			mLogger.logError(e);
			return false;
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
	}

	/**
	 * @param pHttpsUri http uri
	 * @return true if uri is accessable
	 */
	public static boolean isHttpsConnectionAvailable(String pHttpsUri) {
		HttpsURLConnection con = null;
		try {
			disableSSLVerification();

			HttpsURLConnection.setFollowRedirects(false);
			con = (HttpsURLConnection) new URL(pHttpsUri).openConnection();
			con.setReadTimeout(5000);
			con.setRequestMethod("GET");
			return con.getResponseCode() == HttpURLConnection.HTTP_OK;
		} catch (Exception e) {
			mLogger.logError(e);
			return false;
		} finally {
			if (con != null) {
				con.disconnect();
			}
		}
	}

	/**
	 * disable ssl verifacations
	 */
	private static void disableSSLVerification() {
		if (!mSSLVerificationDisabled) {
			try {
				// Create a trust manager that does not validate certificate chains
				TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					public void checkClientTrusted(X509Certificate[] certs, String authType) {
					}

					public void checkServerTrusted(X509Certificate[] certs, String authType) {
					}
				}
				};

				// Install the all-trusting trust manager
				SSLContext sc = SSLContext.getInstance("SSL");
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

				// Create all-trusting host name verifier
				HostnameVerifier allHostsValid = new HostnameVerifier() {
					public boolean verify(String hostname, SSLSession session) {
						return true;
					}
				};

				// Install the all-trusting host verifier
				HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			} catch (NoSuchAlgorithmException | KeyManagementException e) {
				mLogger.logError(
						"Unable to disable ssl verification. " +
								"Can cause errors on files retrieving under https connection", e
				);
			}
		}
		mSSLVerificationDisabled = true;
	}
}
