package vsg.integration.export.util;

import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.droplet.GenericFormHandler;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

/**
 * Helper for form handlers to work with ajax requests responses.
 * <p/>
 * addAjaxResponse
 *
 * @author Dmitry Golubev
 */
public final class AjaxUtils {

	/**
	 * The Constant A_JSON.
	 */
	public static final String A_JSON = "application/json";

	/**
	 * The Constant T_JSON.
	 */
	public static final String T_JSON = "text/json";

	/**
	 * The Constant ACCEPT.
	 */
	private static final String ACCEPT = "Accept";

	/**
	 * The Constant ERROR.
	 */
	private static final String ERROR = "error";

	/**
	 * The Constant SKIP.
	 */
	private static final String SKIP = "skip";

	/**
	 * The Constant SUCCESS.
	 */
	private static final String SUCCESS = "success";

	/**
	 * The Constant ERROR_URL.
	 */
	public static final String ERROR_URL = "errorUrl";

	/**
	 * The Constant SUCCESS_URL.
	 */
	public static final String SUCCESS_URL = "successUrl";

	/**
	 * The Constant SUCCESS_MSG_INFO.
	 */
	public static final String SUCCESS_MSG_INFO = "successMsgInfo";

	/**
	 * The Constant SUCCESS_MSG_ERROR.
	 */
	public static final String SUCCESS_MSG_ERROR = "successMsgError";

	/**
	 * The Constant CODE.
	 */
	private static final String CODE = "code";

	/**
	 * The Constant DETAILS.
	 */
	public static final String DETAILS = "details";

	/**
	 * The Constant PARAM.
	 */
	public static final String PARAM = "param";

	/**
	 * The Constant PROPERTIES.
	 */
	private static final String PROPERTIES = "properties";

	/**
	 * The Constant ORDER_COOKIE.
	 */
	public static final String ORDER_COOKIE = "orderCookie";

	/**
	 * The Constant ALL_ERRORS.
	 */
	public static final String ALL_ERRORS = "all_errors";

	/**
	 * Check if ajax response has been written
	 *
	 * @param pRequest  - request.
	 * @param pResponse - response.
	 * @return true if has been written, false otherwise
	 */
	public static boolean isAjaxResponse(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		return isAjaxRequest(pRequest) && A_JSON.equals(pResponse.getContentType());
	}

	/**
	 * Check if ajax request.
	 *
	 * @param pRequest - request.
	 * @return true if ajax request, otherwise false.
	 */
	public static boolean isAjaxRequest(DynamoHttpServletRequest pRequest) {
		String aheader = pRequest.getHeader(ACCEPT);
		return (null != aheader && (aheader.contains(T_JSON) || aheader.contains(A_JSON)));
	}

	/**
	 * Adding Skip Json response.
	 *
	 * @param pResponse the response
	 * @throws IOException   Signals that an I/O exception has occurred.
	 * @throws JSONException the JSON exception
	 */
	public static void addSkipAjaxResponse(DynamoHttpServletResponse pResponse)
			throws IOException, JSONException {

		pResponse.setContentType(A_JSON);
		JSONObject responseJson = new JSONObject();
		responseJson.put(CODE, SKIP);
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Adding Success Json response.
	 *
	 * @param pFormHandler the form handler
	 * @param pResponse    the response
	 * @param pDetails     the details
	 * @param pSuccessURL  the success url
	 * @param pFailureURL  the failure url
	 * @param pParam       the param
	 * @throws IOException   Signals that an I/O exception has occurred.
	 * @throws JSONException the JSON exception
	 */
	public static void addAjaxResponse(GenericFormHandler pFormHandler,
									   DynamoHttpServletResponse pResponse, String pDetails,
									   String pSuccessURL, String pFailureURL, Object pParam) throws IOException, JSONException {

		pResponse.setContentType(A_JSON);
		JSONObject responseJson = new JSONObject();
		if (pSuccessURL != null) {
			responseJson.put(SUCCESS_URL, pSuccessURL);
		}
		if (pFailureURL != null) {
			responseJson.put(ERROR_URL, pFailureURL);
		}
		if (pParam != null) {
			responseJson.put(PARAM, pParam);
		}
		if (pFormHandler.getFormError()) {
			responseJson.put(CODE, ERROR);
			responseJson.put(ALL_ERRORS, addErrors(pFormHandler));
			addErrorsDetails(pFormHandler, responseJson);
		} else {
			responseJson.put(CODE, SUCCESS);
			responseJson.put(DETAILS, pDetails);
		}
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	private static String addErrors(GenericFormHandler pFormHandler) {
		StringBuilder sb = new StringBuilder();
		Vector exceptions = pFormHandler.getFormExceptions();
		for (Object exc : exceptions) {
			sb.append(((DropletException) exc).getMessage());
			sb.append("<br>");
		}
		return sb.toString();
	}

	/**
	 * Adding Success Json response.
	 *
	 * @param pFormHandler the form handler
	 * @param pResponse    the response
	 * @param pParams      params for response
	 * @throws IOException   Signals that an I/O exception has occurred.
	 * @throws JSONException the JSON exception
	 */
	public static void addAjaxResponse(GenericFormHandler pFormHandler,
									   DynamoHttpServletResponse pResponse, Map<String, Object> pParams) throws IOException, JSONException {

		pResponse.setContentType(A_JSON);
		JSONObject responseJson = new JSONObject();
		if (pParams != null) {
			for (String paramKey : pParams.keySet()) {
				responseJson.put(paramKey, pParams.get(paramKey));
			}
		}
		if (pFormHandler.getFormError()) {
			responseJson.put(CODE, ERROR);
			responseJson.put(ALL_ERRORS, addErrors(pFormHandler));
			addErrorsDetails(pFormHandler, responseJson);
		} else {
			responseJson.put(CODE, SUCCESS);
		}
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Adding Success Json response.
	 *
	 * @param pFormHandler the form handler
	 * @param pResponse    the response
	 * @param pDetails     the details
	 * @param pCookie      the cookie
	 * @param pSuccessURL  the success url
	 * @param pFailureURL  the failure url
	 * @param pParam       the param
	 * @throws IOException   Signals that an I/O exception has occurred.
	 * @throws JSONException the JSON exception
	 */
	public static void addAjaxResponse(GenericFormHandler pFormHandler,
									   DynamoHttpServletResponse pResponse, String pDetails, String pCookie,
									   String pSuccessURL, String pFailureURL, Object pParam) throws IOException, JSONException {

		pResponse.setContentType(A_JSON);
		JSONObject responseJson = new JSONObject();
		responseJson.put(SUCCESS_URL, pSuccessURL);
		responseJson.put(ERROR_URL, pFailureURL);
		if (pParam != null) {
			responseJson.put(PARAM, pParam);
		}
		if (pFormHandler.getFormError()) {
			responseJson.put(CODE, ERROR);
			addErrorsDetails(pFormHandler, responseJson);
		} else {
			responseJson.put(ORDER_COOKIE, pCookie);
			responseJson.put(CODE, SUCCESS);
			responseJson.put(DETAILS, pDetails);
		}
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Adding Success Json response.
	 *
	 * @param pFormHandler     the form handler
	 * @param pResponse        the response
	 * @param pDetails         the details
	 * @param pCookie          the cookie
	 * @param pSuccessMsgInfo  the success msg info
	 * @param pSuccessMsgError the success msg error
	 * @param pSuccessURL      the success url
	 * @param pFailureURL      the failure url
	 * @param pParam           the param
	 * @throws IOException   Signals that an I/O exception has occurred.
	 * @throws JSONException the JSON exception
	 */
	public static void addAjaxResponse(GenericFormHandler pFormHandler,
									   DynamoHttpServletResponse pResponse, String pDetails, String pCookie,
									   String pSuccessMsgInfo, String pSuccessMsgError,
									   String pSuccessURL, String pFailureURL, Object pParam) throws IOException, JSONException {

		pResponse.setContentType(A_JSON);
		JSONObject responseJson = new JSONObject();
		responseJson.put(SUCCESS_URL, pSuccessURL);
		responseJson.put(ERROR_URL, pFailureURL);
		if (pParam != null) {
			responseJson.put(PARAM, pParam);
		}
		if (pFormHandler.getFormError()) {
			responseJson.put(CODE, ERROR);
			addErrorsDetails(pFormHandler, responseJson);
		} else {
			responseJson.put(ORDER_COOKIE, pCookie);
			responseJson.put(CODE, SUCCESS);
			responseJson.put(DETAILS, pDetails);
			responseJson.put(SUCCESS_MSG_INFO, pSuccessMsgInfo);
			responseJson.put(SUCCESS_MSG_ERROR, pSuccessMsgError);
		}
		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Adding Success Json responce with pSuccessState map.
	 *
	 * @param pResponse     the response
	 * @param pSuccessState output map
	 * @throws IOException   Signals that an I/O exception has occurred.
	 * @throws JSONException the JSON exception
	 */
	public static void addAjaxSuccessResponse(DynamoHttpServletResponse pResponse, Map<String, Object> pSuccessState)
			throws IOException, JSONException {
		pResponse.setContentType("application/json");

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put("error", "false");
		if (null != pSuccessState) {
			if (pSuccessState.isEmpty()) {
				responseJson.put("success", "true");
			} else {
				putMapToJson(pSuccessState, responseJson);
			}
		}

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/**
	 * Put map to JSON object.
	 *
	 * @param pErrorState  output map
	 * @param responseJson - JSON object
	 * @throws JSONException the JSON exception
	 */
	private static void putMapToJson(Map<String, Object> pErrorState,
									 JSONObject responseJson) throws JSONException {
		for (Entry<String, Object> entry : pErrorState.entrySet()) {
			Object value = entry.getValue();
			if (value instanceof List) {
				List list = (List) value;
				JSONArray array = new JSONArray();
				for (Object item : list) {
					if (item instanceof List) {
						JSONArray varray = new JSONArray();
						List l = (List) item;
						for (Object i : l) {
							varray.add(i);
						}
						array.add(varray);
					} else {
						array.add(item);
					}
				}
				responseJson.put(entry.getKey(), array);
			} else if (value instanceof Map) {
				Map<String, Object> map = (Map) value;
				JSONObject jsonMap = new JSONObject(map);
				responseJson.put(entry.getKey(), jsonMap);
			} else {
				responseJson.put(entry.getKey(), value);
			}
		}
	}

	/**
	 * Adds the errors details.
	 *
	 * @param pFormHandler  the form handler
	 * @param pResponseJson the response json
	 * @throws IOException   Signals that an I/O exception has occurred.
	 * @throws JSONException the JSON exception
	 */
	private static void addErrorsDetails(GenericFormHandler pFormHandler,
										 JSONObject pResponseJson) throws IOException, JSONException {

		JSONArray errorArray = new JSONArray();
		JSONArray errorPropertiesArray = new JSONArray();
		for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException error = (DropletException) pFormHandler.getFormExceptions().get(i);
			errorArray.add(error.getMessage());
			if (error instanceof DropletFormException) {
				DropletFormException exc = (DropletFormException) error;
				String propertyPath = exc.getPropertyPath();
				if (!errorPropertiesArray.contains(propertyPath)) {
					errorPropertiesArray.add(propertyPath);
				}
			}
		}
		pResponseJson.put(DETAILS, errorArray);
		pResponseJson.put(PROPERTIES, errorPropertiesArray);
	}

}
