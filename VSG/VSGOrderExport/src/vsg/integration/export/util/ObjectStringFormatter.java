package vsg.integration.export.util;

import atg.nucleus.GenericService;

import javax.xml.bind.DatatypeConverter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public class ObjectStringFormatter extends GenericService {

	/**
	 * Date format
	 */
	private String mOutFormatDate = "yyyy-MM-dd HH:mm";
	/**
	 * Double format
	 */
	private String mOutFormatDouble = "%.2f";
	/**
	 * Float format
	 */
	private String mOutFormatFloat = "%.2f";
	/**
	 * Date format
	 */
	private SimpleDateFormat mDateFormatter = null;

	//------------------------------------------------------------------------------------------------------------------

	public String getOutFormatDate() {
		return mOutFormatDate;
	}

	public void setOutFormatDate(String pOutFormatDate) {
		mOutFormatDate = pOutFormatDate;
		mDateFormatter = new SimpleDateFormat(mOutFormatDate);
	}

	private SimpleDateFormat getDateFormatter() {
		if (mDateFormatter == null) {
			mDateFormatter = new SimpleDateFormat(getOutFormatDate());
		}
		return mDateFormatter;
	}

	public String getOutFormatDouble() {
		return mOutFormatDouble;
	}

	public void setOutFormatDouble(String pOutFormatDouble) {
		mOutFormatDouble = pOutFormatDouble;
	}

	public String getOutFormatFloat() {
		return mOutFormatFloat;
	}

	public void setOutFormatFloat(String pOutFormatFloat) {
		mOutFormatFloat = pOutFormatFloat;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pObject object to string
	 * @return string value of object
	 */
	public String formatToString(Object pObject) {
		if (pObject == null) {
			return null;
		}
		try {
			if (pObject.getClass().isAssignableFrom(String.class)) {
				return (String) pObject;
			} else if (pObject.getClass().isAssignableFrom(Date.class) ||
					pObject.getClass().isAssignableFrom(Timestamp.class)) {
				return getDateFormatter().format((Date) pObject);
			}
			if (pObject.getClass().isAssignableFrom(Integer.class)) {
				return DatatypeConverter.printInt((Integer) pObject);
			} else if (pObject.getClass().isAssignableFrom(Double.class)) {
				return DatatypeConverter.printDouble((Double) pObject);
				// return String.format(getOutFormatDouble(), pObject);
			} else if (pObject.getClass().isAssignableFrom(Long.class)) {
				return DatatypeConverter.printLong((Long) pObject);
				// return pObject.toString();
			} else if (pObject.getClass().isAssignableFrom(Float.class)) {
				return DatatypeConverter.printFloat((Float) pObject);
				// return String.format(getOutFormatFloat(), pObject);
			} else if (pObject.getClass().isAssignableFrom(Boolean.class)) {
				return DatatypeConverter.printBoolean((Boolean) pObject);
			} else {
				throw new IllegalArgumentException("Unsuppored class: " + pObject.getClass());
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to build string from object: " + e.toString());
		}
	}

	/**
	 * @param pString string to object
	 * @param pClass  class of new object
	 * @return object value of string
	 */
	public Object formatToObject(String pString, Class pClass) {
		if (pString == null || pClass == null) {
			return null;
		}
		try {
			if (pClass.isAssignableFrom(String.class)) {
				return pString;
			} else if (pClass.isAssignableFrom(Integer.class)) {
				return DatatypeConverter.parseInt(pString);
			} else if (pClass.isAssignableFrom(Float.class)) {
				return DatatypeConverter.parseFloat(pString);
			} else if (pClass.isAssignableFrom(Double.class)) {
				return DatatypeConverter.parseDouble(pString);
			} else if (pClass.isAssignableFrom(Long.class)) {
				return DatatypeConverter.parseLong(pString);
			} else if (pClass.isAssignableFrom(Date.class)) {
				return DatatypeConverter.parseDate(pString);
			} else if (pClass.isAssignableFrom(Timestamp.class)) {
				return DatatypeConverter.parseDate(pString);
			} else if (pClass.isAssignableFrom(Boolean.class)) {
				return DatatypeConverter.parseBoolean(pString);
			} else {
				throw new IllegalArgumentException("Unsupported object class: " + pClass);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("Unable to parse string to object: " + e.toString());
		}
	}

}
