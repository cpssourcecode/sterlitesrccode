package vsg.integration.export.util;

/**
 * @author Dmitry Golubev
 */
public abstract class TransactionProcessorCommand {
	/**
	 * command to execute in transaction
	 */
	public abstract void processInTransaction();

	/**
	 * error catch process
	 */
	public void processError() {
	}

	/**
	 * error catch process
	 */
	public void processPost() {
	}

	/**
	 * @return sync object
	 */
	public abstract Object getSyncObject();
}
