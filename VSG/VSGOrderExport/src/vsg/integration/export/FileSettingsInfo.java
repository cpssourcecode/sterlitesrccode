package vsg.integration.export;

import atg.nucleus.GenericService;

public class FileSettingsInfo extends GenericService {
	/**
	 * wsdl file path
	 */
	private String mWsdlPath;
	/**
	 * xsd file path
	 */
	private String mXsdPath;
	/**
	 * transfer options key path
	 */
	private String mTransferOptionsKeyPath;
	/**
	 * transfer option encription key file path
	 */
	private String mTransferOptionsEncKeyPath;
	/**
	 * sftp encription key file path
	 */
	private String mSftpEncKeyPath;

	public String getWsdlPath() {
		return mWsdlPath;
	}

	public void setWsdlPath(String pWsdlPath) {
		this.mWsdlPath = pWsdlPath;
	}

	public String getXsdPath() {
		return mXsdPath;
	}

	public void setXsdPath(String pXsdPath) {
		this.mXsdPath = pXsdPath;
	}

	public String getTransferOptionsKeyPath() {
		return mTransferOptionsKeyPath;
	}

	public void setTransferOptionsKeyPath(String pTransferOptionsKeyPath) {
		this.mTransferOptionsKeyPath = pTransferOptionsKeyPath;
	}

	public String getTransferOptionsEncKeyPath() {
		return mTransferOptionsEncKeyPath;
	}

	public void setTransferOptionsEncKeyPath(String pTransferOptionsEncKeyPath) {
		this.mTransferOptionsEncKeyPath = pTransferOptionsEncKeyPath;
	}

	public String getSftpEncKeyPath() {
		return mSftpEncKeyPath;
	}

	public void setSftpEncKeyPath(String pSftpEncKeyPath) {
		this.mSftpEncKeyPath = pSftpEncKeyPath;
	}

}
