package vsg.integration.export.report.model;

/**
 * @author Dmitry Golubev
 */
public class LineItem implements IItem {

	/**
	 * array represents cells in table row
	 */
	private String[] mLineCells;

	public String[] getLineCells() {
		return mLineCells;
	}

	public void setLineCells(String[] pLineCells) {
		this.mLineCells = pLineCells;
	}

}
