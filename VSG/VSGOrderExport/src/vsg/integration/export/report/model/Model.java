package vsg.integration.export.report.model;

import atg.repository.RepositoryItem;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class Model {

	/**
	 * header
	 */
	private Header mHeader;
	/**
	 * footer
	 */
	private Footer mFooter;
	/**
	 * main contant
	 */
	private List<IItem> mItems = new LinkedList<IItem>();
	/**
	 * repository items
	 */
	private List<RepositoryItem> mRepositoryItems;

	//------------------------------------------------------------------------------------------------------------------

	public Header getHeader() {
		return mHeader;
	}

	public void setHeader(Header pHeader) {
		mHeader = pHeader;
	}

	public Footer getFooter() {
		return mFooter;
	}

	public void setFooter(Footer pFooter) {
		mFooter = pFooter;
	}

	public List<IItem> getItems() {
		return mItems;
	}

	public void setItems(List<IItem> pItems) {
		mItems = pItems;
	}

	public void addItem(IItem pItem) {
		mItems.add(pItem);
	}

	public List<RepositoryItem> getRepositoryItems() {
		return mRepositoryItems;
	}

	public void setRepositoryItems(List<RepositoryItem> pRepositoryItems) {
		mRepositoryItems = pRepositoryItems;
	}
}
