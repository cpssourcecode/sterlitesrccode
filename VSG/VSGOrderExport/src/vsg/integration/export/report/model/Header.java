package vsg.integration.export.report.model;

/**
 * @author Dmitry Golubev
 */
public class Header {

	/**
	 * transaction id
	 */
	private String mTransactionId;

	/**
	 * wsdl url
	 */
	private String mWsdl;

	/**
	 * report date
	 */
	private String mReportDate;

	public String getTransactionId() {
		return mTransactionId;
	}

	public void setTransactionId(String mTransactionId) {
		this.mTransactionId = mTransactionId;
	}

	public String getWsdl() {
		return mWsdl;
	}

	public void setWsdl(String mWsdl) {
		this.mWsdl = mWsdl;
	}

	public String getReportDate() {
		return mReportDate;
	}

	public void setReportDate(String mReportDate) {
		this.mReportDate = mReportDate;
	}

}