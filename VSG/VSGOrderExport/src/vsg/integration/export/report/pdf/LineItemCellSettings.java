package vsg.integration.export.report.pdf;

import com.lowagie.text.Rectangle;

import java.awt.*;

/**
 * @author Dmitry Golubev
 */
public class LineItemCellSettings extends CellSettings {
	/**
	 * LineItemCellSettings constructor
	 */
	public LineItemCellSettings() {
		super();
		initLineItemCellBorders();
	}

	/**
	 * LineItemCellSettings constructor
	 *
	 * @param pColspan - colspan
	 * @param pAlign   - align
	 */
	public LineItemCellSettings(int pColspan, int pAlign) {
		super(pColspan, pAlign);
		initLineItemCellBorders();
	}

	/**
	 * LineItemCellSettings constructor
	 *
	 * @param pAlign - align
	 */
	public LineItemCellSettings(int pAlign) {
		super(pAlign);
		initLineItemCellBorders();
	}

	/**
	 * LineItemCellSettings constructor
	 *
	 * @param pBackgroundColor - background color
	 */
	public LineItemCellSettings(Color pBackgroundColor) {
		super(pBackgroundColor);
		initLineItemCellBorders();
	}

	/**
	 * initializes cell borders of line item
	 */
	private void initLineItemCellBorders() {
		setBorder(BORDER_TOP, Rectangle.TOP);
		setBorder(BORDER_BOTTOM, Rectangle.BOTTOM);
	}
}
