package vsg.integration.export.report.pdf.builder;

import com.lowagie.text.pdf.PdfPTable;
import vsg.integration.export.report.model.Footer;
import vsg.integration.export.report.model.Model;

/**
 * @author Dmitry Golubev
 */
public class FooterTableBuilder extends CommonBuilder {
	/**
	 * FooterTableBuilder constructor
	 *
	 * @param pModel - model
	 */
	public FooterTableBuilder(Model pModel) {
		setModel(pModel);
	}

	/**
	 * builds footer table
	 *
	 * @return table
	 */
	public PdfPTable buildFooterTable() {
		Footer footer = getModel().getFooter();
		if (footer == null) {
			return null;
		}

		PdfPTable footerTable = new PdfPTable(1);
		footerTable.setTotalWidth(560);

		return footerTable;
	}
}
