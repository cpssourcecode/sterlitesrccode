package vsg.integration.export.report.pdf.builder;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import vsg.integration.export.report.format.FormatterFactory;
import vsg.integration.export.report.format.base.IRepositoryItemFormatter;
import vsg.integration.export.report.model.IItem;
import vsg.integration.export.report.model.LineItem;
import vsg.integration.export.report.model.Model;

import java.util.ArrayList;

/**
 * @author Dmitry Golubev
 */
public class LineItemsTableBuilder extends CommonBuilder {
	/**
	 * LineItemsTableBuilder
	 *
	 * @param pModel - model
	 */
	public LineItemsTableBuilder(Model pModel) {
		setModel(pModel);
	}

	/**
	 * builds main pdf table, represented by several tables, according to export result
	 *
	 * @return main table
	 */
	public PdfPTable buildLineItemsTable() {

		PdfPTable lineTable = new PdfPTable(1);
		lineTable.setWidthPercentage(100);
		lineTable.setSplitLate(false);

		ArrayList<String[]> tempTable = new ArrayList<String[]>();

		for (IItem item : getModel().getItems()) {
			if (item instanceof LineItem) {
				String[] rowCells = ((LineItem) item).getLineCells();

				if (rowCells != null) {
					tempTable.add(rowCells);
				} else {
					addTable(lineTable, tempTable);
					tempTable = new ArrayList<String[]>();
				}
			}
		}
		return lineTable;
	}

	/**
	 * finds max length of result portion to define columns number of table
	 *
	 * @param pTable - table
	 * @return columns number
	 */
	private int findMaximumLength(ArrayList<String[]> pTable) {
		int length = 0;
		for (String[] row : pTable) {
			if (row.length > length) {
				length = row.length;
			}
		}
		return length;
	}

	/**
	 * makes length of all arrays in result portion the same
	 *
	 * @param pLength - length of new result portion
	 * @param pTable  - unformatted table, represented by List
	 * @return - formatted table, represented by List (with rows with the same length)
	 */
	private ArrayList<String[]> unifyRowsLength(int pLength, ArrayList<String[]> pTable) {

		ArrayList<String[]> newTable = new ArrayList<String[]>();
		for (String[] row : pTable) {
			if (row.length == pLength) {
				newTable.add(row);
			} else {
				String[] newRow = new String[pLength];
				for (int i = 0; i < row.length; i++) {
					newRow[i] = row[i];
				}
				newTable.add(newRow);
			}
		}

		return newTable;
	}

	/**
	 * adds portion of result to main table
	 *
	 * @param pTable     - main table
	 * @param pTempTable - portion of result, represented by List
	 */
	private void addTable(PdfPTable pTable, ArrayList<String[]> pTempTable) {

		int tableLength = findMaximumLength(pTempTable);
		pTempTable = unifyRowsLength(tableLength, pTempTable);
		PdfPTable pdfTable = new PdfPTable(tableLength);
		pdfTable.setWidthPercentage(100);
		pdfTable.setSplitLate(false);
		initWidthes(pdfTable, tableLength);

		for (String[] row : pTempTable) {
			for (int i = 1; i <= row.length; i++) {
				addCell(row[i - 1], pdfTable, cellSettingsDefault);
			}
		}

		addTableAsCell(pTable, pdfTable, Rectangle.NO_BORDER);
		addEmptyRow(pTable);

	}

	/**
	 * initializes widths of columns according to number of that columns
	 *
	 * @param pPdfTable    - pdf table
	 * @param pTableLength - number of columns
	 */
	private void initWidthes(PdfPTable pPdfTable, int pTableLength) {
		if (pTableLength > 0) {
			int[] tableWidthes = new int[pTableLength];
			for (int i = 1; i <= pTableLength; i++) {
				tableWidthes[i - 1] = 100 / pTableLength;
			}
			initTableWidthes(pPdfTable, tableWidthes);
		}
	}

	/**
	 * adds empty pdf table to make whitespaces between tables
	 *
	 * @param pTable - table, after which whitespace should be inserted
	 */
	private void addEmptyRow(PdfPTable pTable) {
		PdfPCell emptyCell = new PdfPCell();
		emptyCell.setBorder(0);
		pTable.addCell(emptyCell);
	}

	public void buildLinerContent(Document pDocument, FormatterFactory pFormatterFactory) throws DocumentException {
		try {
			for (RepositoryItem item : getModel().getRepositoryItems()) {

				IRepositoryItemFormatter formatter = pFormatterFactory.getFormatter(
						item.getItemDescriptor().getItemDescriptorName()
				);

				String[][] itemInfo = formatter.formatItem(item);

				PdfPTable lineTable = new PdfPTable(2);
				lineTable.setWidths(new int[] {30, 70});
				// lineTable.setWidthPercentage(100);
				lineTable.setSplitLate(false);

				for(int i=0;i<itemInfo.length;i++) {
					String[] part_1, part_2 = null;
					part_1 = itemInfo[i];
					i++;
					if(i<itemInfo.length) {
						part_2 = itemInfo[i];
					}

					if(part_1!=null && part_2!=null) {
						for(int j=0;j<part_1.length;j++) {
							addCell(part_1[j], lineTable, cellSettingsDefault);
							addCell(part_2[j], lineTable, cellSettingsDefault);
						}
					}
				}

				pDocument.add( lineTable );
				pDocument.add(new Paragraph("\n"));
			}
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
	}

}
