package vsg.integration.export.report.pdf.builder;

import com.lowagie.text.pdf.PdfPTable;
import vsg.integration.export.report.model.Header;
import vsg.integration.export.report.model.Model;

/**
 * @author Dmitry Golubev
 */
public class HeaderTableBuilder extends CommonBuilder {
	/**
	 * HeaderTableBuilder constructor
	 *
	 * @param pModel - model
	 */
	public HeaderTableBuilder(Model pModel) {
		setModel(pModel);
	}

	/**
	 * builds header table
	 *
	 * @return - heder table
	 */
	public PdfPTable buildHeaderTable() {
		Header header = getModel().getHeader();
		if (header == null) {
			return null;
		}

		PdfPTable headerTable = new PdfPTable(1);
		headerTable.setTotalWidth(810);

		addTableAsCell(headerTable, getHeaderLine_01(header));
		addTableAsCell(headerTable, getHeaderLine_02(header));
		addTableAsCell(headerTable, getHeaderLine_03(header));

		return headerTable;
	}

	/**
	 * builds header first line
	 *
	 * @param pHeader - header
	 * @return first line table
	 */
	private PdfPTable getHeaderLine_01(Header pHeader) {
		PdfPTable tableLine_01 = new PdfPTable(2);
		initTableWidthes(tableLine_01, new int[]{50, 50});

		addCell("Report For Transaction: ", tableLine_01, cellSettingsNoBorder);

		addCell(pHeader.getTransactionId(), tableLine_01, cellSettingsNoBorder);

		return tableLine_01;
	}

	/**
	 * builds header second line
	 *
	 * @param pHeader - header
	 * @return second line table
	 */
	private PdfPTable getHeaderLine_02(Header pHeader) {
		PdfPTable tableLine_02 = new PdfPTable(2);
		initTableWidthes(tableLine_02, new int[]{50, 50});

		addCell("WSDL: ", tableLine_02, cellSettingsNoBorder);

		addCell(pHeader.getWsdl(), tableLine_02, cellSettingsNoBorder);

		return tableLine_02;
	}

	/**
	 * builds header third line
	 *
	 * @param pHeader - header
	 * @return third line table
	 */
	private PdfPTable getHeaderLine_03(Header pHeader) {
		PdfPTable tableLine_03 = new PdfPTable(2);
		initTableWidthes(tableLine_03, new int[]{50, 50});

		addCell("Report Date: ", tableLine_03, cellSettingsNoBorder);

		addCell(pHeader.getReportDate(), tableLine_03, cellSettingsNoBorder);

		return tableLine_03;
	}


}
