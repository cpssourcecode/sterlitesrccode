package vsg.integration.export.report.pdf.builder;

import atg.nucleus.GenericService;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import vsg.integration.export.report.model.Model;
import vsg.integration.export.report.pdf.CellSettings;

import java.awt.*;

/**
 * @author Dmitry Golubev
 */
public class CommonBuilder extends GenericService {
	/**
	 * font
	 */
	protected static final Font NORMAL_6 = FontFactory.getFont(FontFactory.COURIER, 6);
	/**
	 * font
	 */
	protected static final Font NORMAL_7 = FontFactory.getFont(FontFactory.COURIER, 7);
	/**
	 * font
	 */
	protected static final Font NORMAL_8 = FontFactory.getFont(FontFactory.COURIER, 8);
	/**
	 * color
	 */
	protected static final Color COLOR_HEADER = new Color(230, 230, 230);
	/**
	 * cell settings
	 */
	protected CellSettings cellSettingsDefault = new CellSettings();
	/**
	 * cell settings - align center
	 */
	protected CellSettings cellSettingsAlignCenter = new CellSettings(Element.ALIGN_CENTER);
	/**
	 * cell settings - no border
	 */
	CellSettings cellSettingsNoBorder = new CellSettings();
	/**
	 * cell settings for right column
	 */
	protected CellSettings settingsRightestColumn = new CellSettings();
	/**
	 * cell settings for left column
	 */
	protected CellSettings settingsLeftestColumn = new CellSettings();

	{
		settingsLeftestColumn.setBorder(CellSettings.BORDER_LEFT, Rectangle.NO_BORDER);
		settingsRightestColumn.setBorder(CellSettings.BORDER_RIGHT, Rectangle.NO_BORDER);
		cellSettingsNoBorder.setBorder(CellSettings.BORDER_LEFT, Rectangle.NO_BORDER);
		cellSettingsNoBorder.setBorder(CellSettings.BORDER_RIGHT, Rectangle.NO_BORDER);
		cellSettingsNoBorder.setBorder(CellSettings.BORDER_TOP, Rectangle.NO_BORDER);
		cellSettingsNoBorder.setBorder(CellSettings.BORDER_BOTTOM, Rectangle.NO_BORDER);
	}

	/**
	 * model
	 */
	private Model mModel;

	public Model getModel() {
		return mModel;
	}

	public void setModel(Model pModel) {
		mModel = pModel;
	}

	/**
	 * initializes widths for table
	 *
	 * @param pTable   - table
	 * @param pWidthes - widths
	 */
	protected void initTableWidthes(PdfPTable pTable, int[] pWidthes) {
		try {
			pTable.setWidths(pWidthes);
		} catch (DocumentException e) {
			vlogError(e, "Unable to set widthes");
		}
	}

	/**
	 * initializes widths for table
	 *
	 * @param pTable   - table
	 * @param pWidthes - widths
	 */
	protected void initTableWidthes(PdfPTable pTable, float[] pWidthes) {
		try {
			pTable.setWidths(pWidthes);
		} catch (DocumentException e) {
			vlogError(e, "Unable to set widthes");
		}
	}

	/**
	 * adds child table as a cell into parent table
	 *
	 * @param pTableOwner - parent table
	 * @param pTableChild - child table
	 */
	protected void addTableAsCell(PdfPTable pTableOwner, PdfPTable pTableChild) {
		PdfPCell cell = new PdfPCell(pTableChild);
		cell.setPadding(0);
		cell.setBorderWidth(0.1f);
		pTableOwner.addCell(cell);
	}

	/**
	 * adds child table as a cell into parent table
	 *
	 * @param pTableOwner - parent table
	 * @param pTableChild - child table
	 */
	protected void addTableAsCell(PdfPTable pTableOwner, PdfPTable pTableChild, int pBorder) {
		PdfPCell cell = new PdfPCell(pTableChild);
		cell.setPadding(0);
		cell.setBorder(pBorder);
		cell.setBorderWidth(0.1f);
		pTableOwner.addCell(cell);
	}

	/**
	 * adds string content into table cell
	 *
	 * @param pContent - content
	 * @param pTable   - table
	 * @return cell
	 */
	protected PdfPCell addCell(String pContent, PdfPTable pTable) {
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Phrase(pContent, NORMAL_8));
		cell.setPadding(1);
		cell.setPaddingBottom(3);
		cell.setBorderWidth(0.1f);
		pTable.addCell(cell);
		return cell;
	}

	/**
	 * initializes cell by content and cell settings
	 *
	 * @param pContent      - content
	 * @param pCellSettings - cell settings
	 * @return cell
	 */
	protected PdfPCell initCell(String pContent, CellSettings pCellSettings) {
		PdfPCell cell = new PdfPCell();
		cell.setPadding(1);
		cell.setPaddingBottom(3);
		if (pCellSettings.getFont() != null) {
			cell.setPhrase(new Phrase(pContent, pCellSettings.getFont()));
		} else {
			cell.setPhrase(new Phrase(pContent));
		}
		if (pCellSettings.getColspan() != -1) {
			cell.setColspan(pCellSettings.getColspan());
		}
		if (pCellSettings.getBackgroundColor() != null) {
			cell.setBackgroundColor(pCellSettings.getBackgroundColor());
		}
		if (pCellSettings.getAlign() != -1) {
			cell.setHorizontalAlignment(pCellSettings.getAlign());
		}
		if (pCellSettings.getPadding() != -1) {
			cell.setPadding(pCellSettings.getPadding());
		}
		// border settings
		if (pCellSettings.getBorder(CellSettings.BORDER_LEFT) == Rectangle.NO_BORDER) {
			cell.setBorderWidthLeft(0);
		} else {
			cell.setBorderWidthLeft(0.1f);
		}
		if (pCellSettings.getBorder(CellSettings.BORDER_RIGHT) == Rectangle.NO_BORDER) {
			cell.setBorderWidthRight(0);
		} else {
			cell.setBorderWidthRight(0.1f);
		}
		if (pCellSettings.getBorder(CellSettings.BORDER_TOP) == Rectangle.NO_BORDER) {
			cell.setBorderWidthTop(0);
		} else {
			cell.setBorderWidthTop(0.1f);
		}
		if (pCellSettings.getBorder(CellSettings.BORDER_BOTTOM) == Rectangle.NO_BORDER) {
			cell.setBorderWidthBottom(0);
		} else {
			cell.setBorderWidthBottom(0.1f);
		}
		return cell;
	}

	/**
	 * adds cell to table with provided content and cell settings
	 *
	 * @param pContent      -content
	 * @param pTable        - table
	 * @param pCellSettings - cell settings
	 */
	protected void addCell(String pContent, PdfPTable pTable, CellSettings pCellSettings) {
		pTable.addCell(initCell(pContent, pCellSettings));
	}

}
