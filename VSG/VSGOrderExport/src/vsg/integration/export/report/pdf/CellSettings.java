package vsg.integration.export.report.pdf;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Rectangle;

import java.awt.*;

/**
 * @author Dmitry Golubev
 */
public class CellSettings {

	/**
	 * left border value
	 */
	public static final int BORDER_LEFT = 0;
	/**
	 * right border value
	 */
	public static final int BORDER_RIGHT = 1;
	/**
	 * top border value
	 */
	public static final int BORDER_TOP = 2;
	/**
	 * bottom border value
	 */
	public static final int BORDER_BOTTOM = 3;
	/**
	 * font
	 */
	private Font mFont = FontFactory.getFont(FontFactory.COURIER, 8);
	/**
	 * colspan
	 */
	private int mColspan = -1;
	/**
	 * background color
	 */
	private Color mBackgroundColor = null;
	/**
	 * align
	 */
	private int mAlign = -1;
	/**
	 * cell borders
	 */
	private int[] mBorders = new int[]{
			Rectangle.UNDEFINED,
			Rectangle.UNDEFINED,
			Rectangle.UNDEFINED,
			Rectangle.UNDEFINED};
	/**
	 * padding
	 */
	private int mPadding = -1;

	/**
	 * CellSettings constructor
	 */
	public CellSettings() {

	}

	/**
	 * CellSettings constructor
	 *
	 * @param pAlign - align
	 */
	public CellSettings(int pAlign) {
		mAlign = pAlign;
	}

	/**
	 * CellSettings constructor
	 *
	 * @param pColspan - colspan
	 * @param pAlign   - aling
	 */
	public CellSettings(int pColspan, int pAlign) {
		mColspan = pColspan;
		mAlign = pAlign;
	}

	/**
	 * CellSettings constructor
	 *
	 * @param pBackgroundColor - background color
	 */
	public CellSettings(Color pBackgroundColor) {
		mBackgroundColor = pBackgroundColor;
	}

	/**
	 * CellSettings constructor
	 *
	 * @param pColspan         - colspan
	 * @param pBackgroundColor -background color
	 */
	public CellSettings(int pColspan, Color pBackgroundColor) {
		mColspan = pColspan;
		mBackgroundColor = pBackgroundColor;
	}

	/**
	 * CellSettings constructor
	 *
	 * @param pColspan         - colspan
	 * @param pAlign           - align
	 * @param pBackgroundColor - background color
	 */
	public CellSettings(int pColspan, int pAlign, Color pBackgroundColor) {
		mColspan = pColspan;
		mAlign = pAlign;
		mBackgroundColor = pBackgroundColor;
	}

	public void setBorder(int pBorderIndex, int pBorderType) {
		mBorders[pBorderIndex] = pBorderType;
	}

	public Font getFont() {
		return mFont;
	}

	public void setFont(Font pFont) {
		mFont = pFont;
	}

	public int getColspan() {
		return mColspan;
	}

	public void setColspan(int pColspan) {
		mColspan = pColspan;
	}

	public Color getBackgroundColor() {
		return mBackgroundColor;
	}

	public void setBackgroundColor(Color pBackgroundColor) {
		mBackgroundColor = pBackgroundColor;
	}

	public int getAlign() {
		return mAlign;
	}

	public void setAlign(int pAlign) {
		mAlign = pAlign;
	}

	public int[] getBorders() {
		return mBorders;
	}

	public void setBorders(int[] pBorders) {
		mBorders = pBorders;
	}

	public int getBorder(int pBorderIndex) {
		return mBorders[pBorderIndex];
	}

	public int getPadding() {
		return mPadding;
	}

	public void setPadding(int pPadding) {
		mPadding = pPadding;
	}
}
