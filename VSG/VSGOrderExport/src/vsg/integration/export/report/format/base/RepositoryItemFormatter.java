package vsg.integration.export.report.format.base;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import vsg.integration.export.service.RepositoryItemPreviewService;

/**
 * @author Dmitry Golubev
 */
public class RepositoryItemFormatter extends GenericService implements IRepositoryItemFormatter {
	/**
	 * repository item preview service
	 */
	private RepositoryItemPreviewService mPreviewService;
	/**
	 * properties for report preview
	 */
	private String[] mProperties;

	//------------------------------------------------------------------------------------------------------------------

	@Override
	public String[][] formatItem(RepositoryItem pItem) {
		if (pItem == null) {
			return null;
		}
		String[] itemPreview = getPreviewService().getRepositoryItemRow(pItem, getProperties());
		String[][] result = new String[1][];
		result[0] = itemPreview;
		return result;
	}

	//------------------------------------------------------------------------------------------------------------------

	public RepositoryItemPreviewService getPreviewService() {
		return mPreviewService;
	}

	public void setPreviewService(RepositoryItemPreviewService pPreviewService) {
		mPreviewService = pPreviewService;
	}

	@Override
	public String[] getProperties() {
		return mProperties;
	}

	public void setProperties(String[] pProperties) {
		mProperties = pProperties;
	}
}
