package vsg.integration.export.report.format;

import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import vsg.integration.export.report.format.base.IRepositoryItemFormatter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class FormatterFactory extends GenericService {

	/**
	 * map of repository item formatters
	 */
	private Map<String, IRepositoryItemFormatter> mMapOfFormatters = new HashMap<String, IRepositoryItemFormatter>();

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * init map of map of item descriptor to formatters
	 *
	 * @param pMapItemDescriptorFormatters map of repository to formatter
	 */
	public void setItemDescriptorFormatters(Map<String, String> pMapItemDescriptorFormatters) {
		Nucleus globalNuclues = Nucleus.getGlobalNucleus();
		for (String itemDescriptor : pMapItemDescriptorFormatters.keySet()) {
			IRepositoryItemFormatter formatter = (IRepositoryItemFormatter) globalNuclues.resolveName(
					pMapItemDescriptorFormatters.get(itemDescriptor));

			mMapOfFormatters.put(itemDescriptor, formatter);
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pItemDescriptorName item descriptor name
	 * @return repository item formatter by item descriptor name
	 */
	public IRepositoryItemFormatter getFormatter(String pItemDescriptorName) {
		IRepositoryItemFormatter result = mMapOfFormatters.get(pItemDescriptorName);
		if (result == null) {
			throw new RuntimeException("Unsupportable type of formatter: " + pItemDescriptorName);
		}
		return result;
	}
}
