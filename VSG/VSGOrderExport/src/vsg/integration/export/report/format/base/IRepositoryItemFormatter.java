package vsg.integration.export.report.format.base;

import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;

/**
 * @author Dmitry Golubev
 */
public interface IRepositoryItemFormatter extends IRepositoryConstants {
	/**
	 * format repository item to array of string
	 *
	 * @param pItem
	 * @return
	 */
	String[][] formatItem(RepositoryItem pItem);

	String[] getProperties();
}
