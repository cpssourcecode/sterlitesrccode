package vsg.integration.export.report.format;

import atg.nucleus.Nucleus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.integration.export.report.format.base.IRepositoryItemFormatter;
import vsg.integration.export.report.format.base.RepositoryItemFormatter;

import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class TransactionFormatter extends RepositoryItemFormatter {

	private FormatterFactory mFormatterFactory;

	private boolean mAppendRepositoryItemsInfo;

	//------------------------------------------------------------------------------------------------------------------

	public FormatterFactory getFormatterFactory() {
		return mFormatterFactory;
	}

	public void setFormatterFactory(FormatterFactory pFormatterFactory) {
		mFormatterFactory = pFormatterFactory;
	}

	public boolean isAppendRepositoryItemsInfo() {
		return mAppendRepositoryItemsInfo;
	}

	public void setAppendRepositoryItemsInfo(boolean pAppendRepositoryItemsInfo) {
		mAppendRepositoryItemsInfo = pAppendRepositoryItemsInfo;
	}

	//------------------------------------------------------------------------------------------------------------------

	@Override
	public String[][] formatItem(RepositoryItem pItem) {
		if (pItem == null) {
			return null;
		}

		List<String> repositoryIds = (List<String>) pItem.getPropertyValue(PRP_TRANSACTION_REPOSITORY_IDS);
		int numberOfRows = 2;

		if (isAppendRepositoryItemsInfo() && repositoryIds != null && repositoryIds.size() > 0) {
			numberOfRows += 1 + repositoryIds.size() + 1;
		}
		String[][] result = new String[numberOfRows][];
		result[0] = getProperties();

		String[] transactionPreview = getPreviewService().getRepositoryItemRow(pItem, getProperties());
		result[1] = transactionPreview;

		if (isAppendRepositoryItemsInfo()) {
			appendRepositoryItemsInfo(result, pItem, repositoryIds);
		}

		return result;
	}

	/**
	 * appends to
	 *
	 * @param pResult
	 * @param pTransactionItem transaction repository item
	 * @param pRepositoryIds   transaction repository ids
	 */
	private void appendRepositoryItemsInfo(String[][] pResult, RepositoryItem pTransactionItem,
										   List<String> pRepositoryIds) {
		if (pRepositoryIds == null || pRepositoryIds.size() == 0) {
			return;
		}

		try {
			RepositoryItem exportConfig = (RepositoryItem) pTransactionItem.getPropertyValue(PRP_TRANSACTION_EXPORT_CONFIG);
			String itemDescriptor = (String) exportConfig.getPropertyValue(PRP_EXPORT_ITEM_DESCRIPTOR);
			String repositoryPath = (String) pTransactionItem.getPropertyValue(PRP_TRANSACTION_REPOSITORY_PATH);
			Repository repository = (Repository) Nucleus.getGlobalNucleus().resolveName(repositoryPath);

			IRepositoryItemFormatter formatter = getFormatterFactory().getFormatter(itemDescriptor);

			pResult[2] = formatter.getProperties();
			int index = 3;
			for (String repositoryId : pRepositoryIds) {
				RepositoryItem exportedItem = repository.getItem(repositoryId, itemDescriptor);
				String[][] exportedItemPreview = formatter.formatItem(exportedItem);
				pResult[index] = exportedItemPreview[0];
				index++;
			}
		} catch (RepositoryException e) {
			vlogError(e, "Unable to add exported repository items info to report.");
		}
	}
}
