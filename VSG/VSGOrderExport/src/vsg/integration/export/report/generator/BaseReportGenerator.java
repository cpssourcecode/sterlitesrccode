package vsg.integration.export.report.generator;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.report.format.FormatterFactory;
import vsg.integration.export.report.format.base.IRepositoryItemFormatter;

import java.io.File;
import java.util.Calendar;

/**
 * @author Dmitry Golubev
 */
public abstract class BaseReportGenerator extends GenericService implements IReportGenerator, IRepositoryConstants {

	private static final String SUCCESS_POSTFIX = "-success";

	private static final String FAIL_POSTFIX = "-fail";

	/**
	 * file extension
	 */
	private String mExtension;
	/**
	 * formatter factory
	 */
	private FormatterFactory mFormatterFactory;

	//------------------------------------------------------------------------------------------------------------------


	public String getExtension() {
		return mExtension;
	}

	public void setExtension(String pExtension) {
		this.mExtension = pExtension;
	}

	public FormatterFactory getFormatterFactory() {
		return mFormatterFactory;
	}

	public void setFormatterFactory(FormatterFactory pFormatterFactory) {
		mFormatterFactory = pFormatterFactory;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * creates file
	 *
	 * @param pOutputReportConfig - report config
	 * @return true, if file was crated;
	 */
	public abstract File createFile(RepositoryItem pOutputReportConfig, boolean isSuccess);


	/**
	 * generates full path to file
	 *
	 * @param pOutputReportConfig - report config
	 * @return full path to file
	 */
	public String generatePath(RepositoryItem pOutputReportConfig, boolean isSuccess) {
		String path = null;
		if (null != pOutputReportConfig) {
			String storeDirPath = (String) pOutputReportConfig.getPropertyValue(PRP_REP_CONF_STORE_PATH);
			if (!storeDirPath.endsWith("/")) {
				storeDirPath += "/";
			}
			String fileName = (String) pOutputReportConfig.getPropertyValue(PRP_REP_CONF_FILE_NAME);
			long time = Calendar.getInstance().getTimeInMillis();
			if (!StringUtils.isBlank(storeDirPath) && !StringUtils.isBlank(fileName) && !StringUtils.isBlank(getExtension())) {
				if (isSuccess) {
					path = storeDirPath + fileName + time + SUCCESS_POSTFIX + getExtension();
				} else {
					path = storeDirPath + fileName + time + FAIL_POSTFIX + getExtension();
				}
			}
		}
		return path;
	}

	/**
	 * select formatter according to ited descriptor
	 *
	 * @param pItem - item
	 * @return formatter
	 */
	public IRepositoryItemFormatter selectFormatter(RepositoryItem pItem) {

		IRepositoryItemFormatter formatter = null;
		if (null != pItem) {
			try {
				formatter = getFormatterFactory().getFormatter(pItem.getItemDescriptor().getItemDescriptorName());
			} catch (RepositoryException e) {
				vlogError(e, "Unable to get formatter.");
			}
		}
		return formatter;
	}

	/**
	 * check transaction status
	 *
	 * @param pCode status code
	 * @return true, if transaction status is success
	 */
	public boolean isTransactionSuccess(int pCode) {
		switch (pCode) {
			case 0:
				return false;
			case 1:
				return true;
			default:
				return false;
		}
	}

}
