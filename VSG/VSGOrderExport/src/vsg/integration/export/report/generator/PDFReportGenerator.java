package vsg.integration.export.report.generator;

import atg.repository.RepositoryItem;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;
import vsg.integration.export.report.model.Footer;
import vsg.integration.export.report.model.Header;
import vsg.integration.export.report.model.LineItem;
import vsg.integration.export.report.model.Model;
import vsg.integration.export.report.pdf.ReportHeaderFooter;
import vsg.integration.export.report.pdf.builder.FooterTableBuilder;
import vsg.integration.export.report.pdf.builder.HeaderTableBuilder;
import vsg.integration.export.report.pdf.builder.LineItemsTableBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class PDFReportGenerator extends BaseReportGenerator {

	/**
	 * date format
	 */
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	/**
	 * model
	 */
	private Model mModel;


	public File generateReport(List<RepositoryItem> pItemsForExport, RepositoryItem pExportConfig) {
		setModel(new Model());
		File file = null;
		if (pItemsForExport != null && pExportConfig != null && pItemsForExport.size() > 0) {
			initModelHeader(pExportConfig);
			initModelFooter();
			boolean isSuccess = isTransactionSuccess((Integer) pItemsForExport.get(0).getPropertyValue(PRP_TRANSACTION_STATUS));
			getModel().setRepositoryItems(pItemsForExport);
/*
			for (RepositoryItem item : pItemsForExport) {
				IRepositoryItemFormatter formatter = selectFormatter(item);
				if (null != formatter) {
					String[][] result = formatter.formatItem(item);
					initModelItems(result);
				}
			}
*/
			RepositoryItem pOutputReportConfig = (RepositoryItem) pExportConfig.getPropertyValue(ID_OUT_REPORT_CONFIG);
			if (pOutputReportConfig != null) {
				file = createFile(pOutputReportConfig, isSuccess);
			} else {
				vlogDebug("Report Config is empty");
			}
		}
		return file;
	}

	@Override
	public File createFile(RepositoryItem pOutputReportConfig, boolean isSuccess) {

		File file = new File(generatePath(pOutputReportConfig, isSuccess));
		Document document = new Document(PageSize.A4.rotate(), 20, 15, 75, 40);
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

			FooterTableBuilder footerTableBuilder = new FooterTableBuilder(getModel());
			HeaderTableBuilder headerTableBuilder = new HeaderTableBuilder(getModel());
			LineItemsTableBuilder lineItemsTableBuilder = new LineItemsTableBuilder(getModel());

			ReportHeaderFooter headerFooter = new ReportHeaderFooter(
					headerTableBuilder.buildHeaderTable(),
					footerTableBuilder.buildFooterTable()
			);

			writer.setPageEvent(headerFooter);
			document.open();
			lineItemsTableBuilder.buildLinerContent(document, getFormatterFactory());

		} catch (Exception e) {
			vlogError(e, "Error document forming.");
		} finally {
			if (document.isOpen()) {
				document.close();
			}
		}
		return file;
	}

	/**
	 * initializes model main content by export result
	 *
	 * @param pResult - result
	 */
	private void initModelItems(String[][] pResult) {
		for (String[] lineCells : pResult) {

			LineItem lineItem = new LineItem();
			lineItem.setLineCells(lineCells);
			getModel().addItem(lineItem);
		}
	}

	/**
	 * initializes model header
	 *
	 * @param pExportConfig - export config
	 */
	private void initModelHeader(RepositoryItem pExportConfig) {
		Header header = new Header();

		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		header.setReportDate(sdf.format(cal.getTime()));

		RepositoryItem outputFormatConfig = (RepositoryItem) pExportConfig.getPropertyValue(PRP_EXPORT_OUTPUT_FORMAT);
		if (outputFormatConfig != null) {
			header.setWsdl((String) outputFormatConfig.getPropertyValue(PRP_OFC_WSDL_URL));
		}

		header.setTransactionId(pExportConfig.getRepositoryId());
		getModel().setHeader(header);

	}

	/**
	 * initializes model footer
	 *
	 */
	private void initModelFooter() {
		Footer footer = new Footer();
		getModel().setFooter(footer);
	}

	//------------------------------------------------------------------------------------------------------------------

	public Model getModel() {
		return mModel;
	}

	public void setModel(Model pModel) {
		mModel = pModel;
	}
}
