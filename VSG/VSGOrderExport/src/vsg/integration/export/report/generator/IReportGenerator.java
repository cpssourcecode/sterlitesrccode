package vsg.integration.export.report.generator;

import atg.repository.RepositoryItem;

import java.io.File;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public interface IReportGenerator {
	/**
	 * generates report
	 *
	 * @param pItemsForExport items for export
	 * @param pExportConfig   - export config
	 * @return generated file
	 */
	File generateReport(List<RepositoryItem> pItemsForExport, RepositoryItem pExportConfig);
}
