package vsg.integration.export.report.generator;

import atg.nucleus.GenericService;

/**
 * Generator Factory
 */
public class GeneratorFactory extends GenericService {

	/**
	 * CSV Generator
	 */
	private CSVReportGenerator mCsvReportGenerator;
	/**
	 * PDF Generator
	 */
	private PDFReportGenerator mPdfReportGenerator;

	public CSVReportGenerator getCsvReportGenerator() {
		return mCsvReportGenerator;
	}

	public void setCsvReportGenerator(CSVReportGenerator pCsvReportGenerator) {
		mCsvReportGenerator = pCsvReportGenerator;
	}

	public PDFReportGenerator getPdfReportGenerator() {
		return mPdfReportGenerator;
	}

	public void setPdfReportGenerator(PDFReportGenerator pPdfReportGenerator) {
		this.mPdfReportGenerator = pPdfReportGenerator;
	}


	/**
	 * @param pType format type. 0 for csv, 1 for pdf.
	 * @return generator by format type
	 */
	public IReportGenerator getReportGenerator(int pType) {

		switch (pType) {
			case 0:
				return getCsvReportGenerator();
			case 1:
				return getPdfReportGenerator();
			default:
				throw new RuntimeException("Unsupportable type of generator: " + pType + ". 0 and 1 are allowed.");
		}
	}

}
