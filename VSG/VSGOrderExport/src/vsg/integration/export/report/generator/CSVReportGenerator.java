package vsg.integration.export.report.generator;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import vsg.integration.export.report.format.base.IRepositoryItemFormatter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class CSVReportGenerator extends BaseReportGenerator {

	/**
	 * BufferedWriter
	 */
	protected BufferedWriter mWriter = null;
	/**
	 * delimiter property
	 */
	private String mDelimiter;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * generates report
	 *
	 * @param pItemsForExport items for export
	 * @param pExportConfigItem   - export config
	 * @return generated file
	 */
	public File generateReport(List<RepositoryItem> pItemsForExport, RepositoryItem pExportConfigItem) {
		File file = null;
		if (pItemsForExport != null && pExportConfigItem != null && pItemsForExport.size() > 0) {
			boolean isSuccess = isTransactionSuccess((Integer) pItemsForExport.get(0).getPropertyValue(PRP_TRANSACTION_STATUS));
			try {
				file = createFile((RepositoryItem) pExportConfigItem.getPropertyValue(ID_OUT_REPORT_CONFIG), isSuccess);
				if (file != null && mWriter != null) {
					for (RepositoryItem item : pItemsForExport) {
						IRepositoryItemFormatter formatter = selectFormatter(item);
						if (null != formatter) {
							String[][] result = formatter.formatItem(item);
							writeInFile(result);
						}
					}
				} else {
					vlogError("Writer is not initialized");
				}
			} catch (IOException e) {
				vlogError(e, "Unable to write report-file");
			} finally {
				endWritingFile();
			}
		}
		return file;
	}

	/**
	 * creates file
	 *
	 * @param pOutputReportConfig - report config
	 * @return true, if file was crated;
	 */
	@Override
	public File createFile(RepositoryItem pOutputReportConfig, boolean isSuccess) {
		File file = null;
		try {
			String path = generatePath(pOutputReportConfig, isSuccess);
			if (!StringUtils.isBlank(path)) {
				file = new File(path);
				mWriter = new BufferedWriter(new FileWriter(file));
			} else {
				vlogError("Export config: {0}, wrong file path: {1}", pOutputReportConfig.getRepositoryId(), path);
			}
		} catch (IOException e) {
			vlogError(e, "Unable to write report-file");
		}

		return file;
	}

	/**
	 * writes result in file
	 *
	 * @param pResult - result
	 * @return writer
	 * @throws IOException
	 */
	public BufferedWriter writeInFile(String[][] pResult) throws IOException {
		if (null != pResult) {
			for (String[] line : pResult) {
				StringBuilder stringLine = new StringBuilder();
				if (line != null) {
					for (int i = 0; i < line.length; i++) {
						stringLine.append(line[i]);
						if (i != line.length - 1) {
							stringLine.append(getDelimiter());
						}
					}
					mWriter.write(stringLine.toString());
					mWriter.newLine();
				}
			}
		} else {
			vlogDebug("Empty result array");
		}
		return mWriter;
	}

	/**
	 * end writing file
	 */
	public void endWritingFile() {
		try {
			if(mWriter!=null) {
				mWriter.close();
			}
		} catch (IOException e) {
			vlogError(e, "Unable to close report file");
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getDelimiter() {
		return mDelimiter;
	}

	public void setDelimiter(String pDelimiter) {
		this.mDelimiter = pDelimiter;
	}
}
