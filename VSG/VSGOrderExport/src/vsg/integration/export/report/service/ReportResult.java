package vsg.integration.export.report.service;

import atg.repository.RepositoryItem;

import java.io.File;

/**
 * @author Dmitry Golubev
 */
public class ReportResult {
	/**
	 * report file
	 */
	private File mReportFile = null;
	/**
	 * export config item
	 */
	private RepositoryItem mExportConfigItem = null;
	/**
	 * report item
	 */
	private RepositoryItem mReportItem = null;
	/**
	 * success
	 */
	private boolean mSuccess = true;
	/**
	 * fail reason
	 */
	private String mFailReason;

	//----------------------------------------------------------------------------------------------------------------


	public File getReportFile() {
		return mReportFile;
	}

	public void setReportFile(File pReportFile) {
		mReportFile = pReportFile;
	}

	public RepositoryItem getExportConfigItem() {
		return mExportConfigItem;
	}

	public void setExportConfigItem(RepositoryItem pExportConfigItem) {
		mExportConfigItem = pExportConfigItem;
	}

	public RepositoryItem getReportItem() {
		return mReportItem;
	}

	public void setReportItem(RepositoryItem pReportItem) {
		mReportItem = pReportItem;
	}

	public boolean isSuccess() {
		return mSuccess;
	}

	public void setSuccess(boolean pSuccess) {
		mSuccess = pSuccess;
	}

	public String getFailReason() {
		return mFailReason;
	}

	public void setFailReason(String pFailReason) {
		mFailReason = pFailReason;
	}
}
