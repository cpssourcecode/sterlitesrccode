package vsg.integration.export.report.service;

import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.report.generator.GeneratorFactory;
import vsg.integration.export.report.generator.IReportGenerator;
import vsg.integration.export.service.email.EmailSenderService;
import vsg.integration.export.service.upload.IUploader;
import vsg.integration.export.service.upload.UploaderFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class ReportService extends GenericService implements IRepositoryConstants {
	/**
	 * export repository
	 */
	private Repository mRepository;

	/**
	 * The email sender service.
	 */
	private EmailSenderService mEmailSenderService;
	/**
	 * rql for query available transactions with success status
	 */
	private static final String RQL_SUCCESS = "status=\"success\" and exportConfig.id=?0";
	/**
	 * rql for query available transactions with success status
	 */
	private static final String RQL_ERROR = "status=\"fail\" and exportConfig.id=?0";
	/**
	 * Generator Factory
	 */
	private GeneratorFactory mGeneratorFactory;

	/**
	 * current report file
	 */
	private ReportResult mCurrentResult = null;

	private UploaderFactory mUploaderFactory;

	//------------------------------------------------------------------------------------------------------------------

	public UploaderFactory getUploaderFactory() {
		return mUploaderFactory;
	}

	public void setUploaderFactory(UploaderFactory pUploaderFactory) {
		this.mUploaderFactory = pUploaderFactory;
	}

	public Repository getRepository() {
		return mRepository;
	}

	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	public GeneratorFactory getGeneratorFactory() {
		return mGeneratorFactory;
	}

	public void setGeneratorFactory(GeneratorFactory pGeneratorFactory) {
		this.mGeneratorFactory = pGeneratorFactory;
	}

	public EmailSenderService getEmailSenderService() {
		return mEmailSenderService;
	}

	public void setEmailSenderService(EmailSenderService mEmailSenderService) {
		this.mEmailSenderService = mEmailSenderService;
	}

	//------------------------------------------------------------------------------------------------------------------

	public void TESTCALL() {
		performGenerateReportSuccess("test123");
		//performGenerateReportError("t534500001");
	}

	/**
	 * generates report with success transactions for specified export configuration
	 *
	 * @param pExportConfigId export configuration id
	 * @return true if no erros occurres
	 */
	public ReportResult performGenerateReportSuccess(String pExportConfigId) {
		try {
			RepositoryItem exportConfig = getRepository().getItem(pExportConfigId, ID_EXPORT_CONFIG);
			if (exportConfig == null) {
				processEmptyExportConfig();
				return mCurrentResult;
			}
		} catch (RepositoryException e) {
			vlogError("Error while getting export config from repository");
		}
		performReporting(pExportConfigId, RQL_SUCCESS);
		if (mCurrentResult.getReportFile() != null) {
			sendByEmail(true);
			postPerformGenerateReport();
		}
		return mCurrentResult;
	}

	/**
	 * generates report with error transactions for specified export configuration
	 *
	 * @param pExportConfigId export configuration id
	 * @return true if no erros occurres
	 */
	public ReportResult performGenerateReportError(String pExportConfigId) {
		try {
			RepositoryItem exportConfig = getRepository().getItem(pExportConfigId, ID_EXPORT_CONFIG);
			if (exportConfig == null) {
				processEmptyExportConfig();
				return mCurrentResult;
			}
		} catch (RepositoryException e) {
			vlogError("Error while getting export config from repository");
		}
		performReporting(pExportConfigId, RQL_ERROR);
		if (mCurrentResult.getReportFile() != null) {
			sendByEmail(false);
			postPerformGenerateReport();
		}
		return mCurrentResult;
	}

	/**
	 * configure Report Result if Export Config is empty
	 */
	private void processEmptyExportConfig() {
		vlogError("Could not find export config");
		mCurrentResult = new ReportResult();
		mCurrentResult.setSuccess(false);
		mCurrentResult.setFailReason("Could not find export config");
	}

	/**
	 * post report update
	 */
	protected void postPerformGenerateReport() {
		((MutableRepositoryItem) mCurrentResult.getReportItem()).setPropertyValue(
				PRP_REPORT_IS_SUCCESS, mCurrentResult.isSuccess()
		);
		try {
			((MutableRepository) getRepository()).updateItem((MutableRepositoryItem) mCurrentResult.getReportItem());
		} catch (RepositoryException e) {
			vlogError(e, "Unable to store report options.");
		}
	}

	/**
	 * do reporting
	 *
	 * @param pExportConfigId export configuration id
	 * @param pRql            rql to find items for reporting
	 * @return true if no erros occurres
	 */
	private void performReporting(String pExportConfigId, String pRql) {
		mCurrentResult = new ReportResult();
		try {
			mCurrentResult.setExportConfigItem(getRepository().getItem(pExportConfigId, ID_EXPORT_CONFIG));
			if (mCurrentResult.getExportConfigItem() == null) {
				trackError("Unable to load export configuration by id: {" + pExportConfigId + "}.");
			} else {
				createReportItem();
				generateReportFile(pRql);
				uploadToFtp();
			}
		} catch (Exception e) {
			trackError(e);
		}
	}

	/**
	 * sends email with generated report
	 *
	 * @param mSuccessEmail success email
	 */
	protected void sendByEmail(boolean mSuccessEmail) {
		RepositoryItem reportConfig = (RepositoryItem) mCurrentResult.getExportConfigItem().getPropertyValue
				(PRP_EXPORT_OUT_REPORT_CONFIG);
		if (reportConfig != null) {
			RepositoryItem emailConfig = (RepositoryItem) reportConfig.getPropertyValue(PRP_REP_CONF_MAIL_CONF);
			if (emailConfig != null) {
				try {
					if (mSuccessEmail) {
						getEmailSenderService().sendSuccessEmail(
								mCurrentResult.getExportConfigItem(),
								mCurrentResult.getReportFile()
						);
					} else {
						getEmailSenderService().sendErrorEmail(
								mCurrentResult.getExportConfigItem(),
								mCurrentResult.getReportFile()
						);
					}
					((MutableRepositoryItem) mCurrentResult.getReportItem()).setPropertyValue(
							PRP_REPORT_IS_EMAILED, true
					);
				} catch (Exception e) {
					trackError(e);
					((MutableRepositoryItem) mCurrentResult.getReportItem()).setPropertyValue(
							PRP_REPORT_EMAIL_FAIL_REASON, e.toString());
				}
			}
		}
	}

	/**
	 * uploads generated report to ftp
	 */
	protected void uploadToFtp() {
		if (mCurrentResult.getReportFile() == null) {
			return;
		}
		RepositoryItem reportConfig = (RepositoryItem) mCurrentResult.getExportConfigItem().getPropertyValue
				(PRP_EXPORT_OUT_REPORT_CONFIG);
		if (reportConfig != null) {
			RepositoryItem ftpConfig = (RepositoryItem) reportConfig.getPropertyValue(PRP_REP_CONF_FTP_CONF);
			if (ftpConfig != null) {
				try {

					IUploader uploader = getUploaderFactory().getUploader((Integer) ftpConfig.getPropertyValue(PRP_FTP_CONF_PROTOCOL));
					uploader.uploadFile(mCurrentResult.getReportFile(), mCurrentResult.getExportConfigItem());

					((MutableRepositoryItem) mCurrentResult.getReportItem()).setPropertyValue(
							PRP_REPORT_IS_FTP_PLACED, true
					);
				} catch (Exception e) {
					trackError(e);
					((MutableRepositoryItem) mCurrentResult.getReportItem()).setPropertyValue(
							PRP_REPORT_FTP_FAIL_REASON, e.toString());
				}
			}
		}
	}

	/**
	 * @param pRql rql
	 */
	protected void generateReportFile(String pRql) {
		List<RepositoryItem> transactionsForExport = getItemsForExport(pRql,
				mCurrentResult.getExportConfigItem().getRepositoryId());
		if (transactionsForExport == null) {
			return;
		}

		if (isLoggingDebug()) {
			logDebug("Transactions: " + transactionsForExport);
		}

		addTransactionsForReport(transactionsForExport);
		generateFile(transactionsForExport);

		updateFileForReport();
	}

	/**
	 * update file property for report
	 */
	protected void updateFileForReport() {
		if (mCurrentResult.getReportFile() != null) {
			((MutableRepositoryItem) mCurrentResult.getReportItem()).setPropertyValue(
					PRP_REPORT_FILE_NAME, mCurrentResult.getReportFile().getAbsolutePath());
		}
	}

	/**
	 * add transactions to report
	 *
	 * @param pTransactionsForExport - transactions
	 */
	protected void addTransactionsForReport(List<RepositoryItem> pTransactionsForExport) {
		List<String> transactionIds = new LinkedList<String>();
		for (RepositoryItem transactionForReport : pTransactionsForExport) {
			transactionIds.add(transactionForReport.getRepositoryId());
		}
		((MutableRepositoryItem) mCurrentResult.getReportItem()).setPropertyValue(
				PRP_REPORT_TRANSACTION_IDS, transactionIds);
	}

	/**
	 * generate report file
	 *
	 * @param pTransactionsForReport array of transactions for report
	 */
	protected void generateFile(List<RepositoryItem> pTransactionsForReport) {
		RepositoryItem reportConfig = (RepositoryItem) mCurrentResult.getExportConfigItem().
				getPropertyValue(ID_OUT_REPORT_CONFIG);
		IReportGenerator generator = getGeneratorFactory().getReportGenerator(
				(Integer) reportConfig.getPropertyValue(PRP_REP_CONF_FORMAT)
		);
		File reportFile = generator.generateReport(pTransactionsForReport, mCurrentResult.getExportConfigItem());
		if (reportFile != null && reportFile.exists()) {
			mCurrentResult.setReportFile(reportFile);
		} else {
			mCurrentResult.setFailReason("Unable to create report-file");
			if (reportFile != null) {
				mCurrentResult.setFailReason(mCurrentResult.getFailReason() + ": "+reportFile.getAbsolutePath());
			}
			mCurrentResult.setSuccess(false);
		}
	}

	/**
	 * create and init report item
	 *
	 * @throws RepositoryException if error occurres
	 */
	protected void createReportItem() throws RepositoryException {
		RepositoryItem reportItem = ((MutableRepository) getRepository()).createItem(ID_REPORT);
		((MutableRepositoryItem) reportItem).setPropertyValue(PRP_REPORT_EXPORT_CONFIG,
				mCurrentResult.getExportConfigItem());
		((MutableRepositoryItem) reportItem).setPropertyValue(PRP_REPORT_DATE_CREATE, new Date());
		((MutableRepositoryItem) reportItem).setPropertyValue(PRP_REPORT_IS_EMAILED, false);
		((MutableRepositoryItem) reportItem).setPropertyValue(PRP_REPORT_IS_FTP_PLACED, false);
		((MutableRepository) getRepository()).addItem((MutableRepositoryItem) reportItem);
		mCurrentResult.setReportItem(reportItem);
	}

	/**
	 * @param pRql            rql to find available transactions for export
	 * @param pExportConfigId export config id
	 * @return available transactions for export
	 */
	protected List<RepositoryItem> getItemsForExport(String pRql, String pExportConfigId) {
		try {
			RepositoryView view = getRepository().getView(ID_TRANSACTION_FOR_REPORT);
			RqlStatement statement = RqlStatement.parseRqlStatement(pRql);
			RepositoryItem[] transactionsForReport = statement.executeQuery(view, new Object[]{pExportConfigId});
			List<RepositoryItem> items = new ArrayList<RepositoryItem>();
			if (transactionsForReport != null) {
				for (RepositoryItem transaction : transactionsForReport) {
					RepositoryItem item = getRepository().getItem(transaction.getRepositoryId(), ID_TRANSACTION);
					if (item != null) {
						items.add(item);
					}
				}
			}
			return items;
		} catch (RepositoryException e) {
			vlogError(e, "Unable to query transactions for report.");
		}
		return null;
	}

	/**
	 * track error
	 *
	 * @param pException report processing error tracking
	 */
	private void trackError(Exception pException) {
		vlogError(pException, "Error during file reporting.");
		mCurrentResult.setFailReason(pException.toString());
		mCurrentResult.setSuccess(false);
	}

	/**
	 * track error
	 *
	 * @param pError report processing error
	 */
	private void trackError(String pError) {
		vlogError(pError);
		mCurrentResult.setFailReason(pError);
		mCurrentResult.setSuccess(false);
	}
}
