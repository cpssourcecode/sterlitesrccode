package vsg.integration.export.report.service;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class ReportServiceForTest extends ReportService {

	/**
	 * override to prevent saving transactions for report
	 */
	protected void addTransactionsForReport(RepositoryItem[] pTransactionsForExport) {
	}

	/**
	 * override ot prevent upload to ftp
	 */
	@Override
	protected void uploadToFtp() {
	}

	/**
	 * override to prevent sending by email
	 */
	@Override
	protected void sendByEmail(boolean mSuccessEmail) {
	}

	/**
	 * override to prevent creating report item
	 */
	@Override
	protected void createReportItem() throws RepositoryException {
	}

	/**
	 * override to prevent updating file for report
	 */
	@Override
	protected void updateFileForReport() {
	}

	/**
	 * override to prevent updating report item
	 */
	@Override
	protected void postPerformGenerateReport() {
	}

	/**
	 * override to get transactions instead transactions for report
	 */
	@Override
	protected List<RepositoryItem> getItemsForExport(String pRql, String pExportConfigId) {
		pRql += " RANGE +10";
		try {
			RepositoryView view = getRepository().getView(ID_TRANSACTION);
			RqlStatement statement = RqlStatement.parseRqlStatement(pRql);
			RepositoryItem[] transactions = statement.executeQuery(view, new Object[]{pExportConfigId});
			List<RepositoryItem> items = null;
			if (transactions != null) {
				items = new ArrayList<RepositoryItem>(Arrays.asList(transactions));
			}
			return items;
		} catch (RepositoryException e) {
			vlogError(e, "Unable to query transactions for report.");
		}
		return null;
	}
}
