package vsg.integration.export.droplet;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.parse.wsdl.bean.Message;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;
import vsg.integration.export.service.util.ExportConfigWsdlPathGenerator;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author Dmitry Golubev
 */
public class WSDLModelInfoDroplet extends DynamoServlet implements IDropletConstants {
	/**
	 * input parameter repository name
	 */
	private static final String INPUT_WSDL = "wsdl";
	/**
	 * input parameter repository name
	 */
	private static final String INPUT_WSDL_UPLOAD = "wsdlUpload";
	/**
	 * input parameter repository name
	 */
	private static final String INPUT_EXPORT_CONFIG_ID = "exportConfigId";
	/**
	 * input parameter message name
	 */
	private static final String INPUT_MESSAGE_NAME = "messageName";
	/**
	 * input parameter type name
	 */
	private static final String INPUT_TYPE_FULL_PATH = "typeFullPath";
	/*
	 * output param model name
	 */
	private static final String OUTPUT_MODEL = "model";
	/*
	 * output param message name
	 */
	private static final String OUTPUT_MESSAGE = "message";
	/*
	 * output param type name
	 */
	private static final String OUTPUT_TYPE = "type";
	/**
	 * wsdl parser
	 */
	private WSDLParser mParser;
	/**
	 * common service
	 */
	private ExportConfigWsdlPathGenerator mExportConfigWsdlPathGenerator;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfigWsdlPathGenerator getExportConfigWsdlPathGenerator() {
		return mExportConfigWsdlPathGenerator;
	}

	public void setExportConfigWsdlPathGenerator(ExportConfigWsdlPathGenerator pExportConfigWsdlPathGenerator) {
		this.mExportConfigWsdlPathGenerator = pExportConfigWsdlPathGenerator;
	}

	public WSDLParser getParser() {
		return mParser;
	}

	public void setParser(WSDLParser pParser) {
		mParser = pParser;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws javax.servlet.ServletException - if cannot make output or empty oparam
	 * @throws java.io.IOException            - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
/*
		if(isLoggingDebug()) {
			logState("jvm state before");
		}
*/
		String exportConfigId = pRequest.getParameter(INPUT_EXPORT_CONFIG_ID);
		String inputParamWsdlUrl = pRequest.getParameter(INPUT_WSDL);
		String inputParamWsdlUpload = pRequest.getParameter(INPUT_WSDL_UPLOAD);
		String inputMessageName = pRequest.getParameter(INPUT_MESSAGE_NAME);
		String inputTypeFullPath = pRequest.getParameter(INPUT_TYPE_FULL_PATH);

		String wsdlPath = getExportConfigWsdlPathGenerator().generateWsdlPath(
				exportConfigId,
				inputParamWsdlUrl,
				inputParamWsdlUpload);

		if (StringUtils.isBlank(wsdlPath)) {
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		}

		WSDLModel model = getParser().analyze(wsdlPath);
		if (model == null) {
			vlogError("WSDLModel is not loaded: {0}", wsdlPath);
			pRequest.serviceLocalParameter(ERROR, pRequest, pResponse);
		} else {
			pRequest.setParameter(OUTPUT_MODEL, model);
			if (!StringUtils.isBlank(inputMessageName)) {
				Message message = model.getMessage(inputMessageName);
				if (message != null) {
					pRequest.setParameter(OUTPUT_MESSAGE, message);
					if (!StringUtils.isBlank(inputTypeFullPath)) {
						Type messageType = message.getType(inputTypeFullPath);
						if (messageType != null) {
							pRequest.setParameter(OUTPUT_TYPE, messageType);
						}
					}
				}
			}
			pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
/*
		if(isLoggingDebug()) {
			logState("jvm state after");
		}
*/
	}

	/**
	 * log jvm state
	 */
/*
	private void logState(String pMsg) {
		Runtime runtime = Runtime.getRuntime();

		int mb = 1024 * 1024;

		String used = String.valueOf((runtime.totalMemory() - runtime.freeMemory()) / mb);
		String free = String.valueOf(runtime.freeMemory() / mb);
		String total = String.valueOf(runtime.totalMemory() / mb);
		String max = String.valueOf(runtime.maxMemory() / mb);

		vlogDebug(pMsg + " Used Free Ttl Max:" + used + " " + free + " " + total + " " + max);
	}
*/
}
