package vsg.integration.export.droplet;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import org.xml.sax.InputSource;
import vsg.integration.export.IRepositoryConstants;

import javax.servlet.ServletException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Query transaction response from repository
 */
public class QueryTransactionResponseDroplet extends DynamoServlet implements IDropletConstants, IRepositoryConstants {

	/**
	 * export config repository
	 */
	private MutableRepository mRepository;

	//------------------------------------------------------------------------------------------------------------------

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		mRepository = pRepository;
	}


	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws javax.servlet.ServletException - if cannot make output or empty oparam
	 * @throws java.io.IOException            - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		try {
			RepositoryItem transaction = getRepository().getItem(pRequest.getParameter(TRANSACTION_ID), ID_TRANSACTION);
			if (transaction != null) {
				pRequest.setParameter(ID_TRANSACTION, transaction);
				pRequest.setParameter(PRP_TRANSACTION_REQUEST + "Div", PRP_TRANSACTION_REQUEST + transaction.getRepositoryId());
				pRequest.setParameter(PRP_TRANSACTION_RESPONSE + "Div", PRP_TRANSACTION_RESPONSE + transaction.getRepositoryId());
				pRequest.setParameter(
						PRP_TRANSACTION_REQUEST,
						formatTransactionResponse((String) transaction.getPropertyValue(PRP_TRANSACTION_REQUEST))
				);
				pRequest.setParameter(
						PRP_TRANSACTION_RESPONSE,
						formatTransactionResponse((String) transaction.getPropertyValue(PRP_TRANSACTION_RESPONSE))
				);
				pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
			} else {
				pRequest.serviceParameter(EMPTY, pRequest, pResponse);
			}
		} catch (RepositoryException e) {
			vlogDebug(e, "Unable to load transaction info.");
			pRequest.serviceParameter(ERROR, pRequest, pResponse);
		}

	}

	/**
	 * Formatting response (as xml)
	 *
	 * @param trResponse - transaction response
	 * @return formatted response
	 */
	private String formatTransactionResponse(String trResponse) {
		try {
			Transformer serializer = SAXTransformerFactory.newInstance()
					.newTransformer();
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			serializer.setOutputProperty("{http://xml.customer.org/xslt}indent-amount", "2");
			Source xmlSource = new SAXSource(new InputSource(new ByteArrayInputStream(trResponse.getBytes())));
			StreamResult res = new StreamResult(new ByteArrayOutputStream());
			serializer.transform(xmlSource, res);
			return new String(((ByteArrayOutputStream) res.getOutputStream()).toByteArray());
		} catch (Exception e) {
			return trResponse;
		}
	}

}
