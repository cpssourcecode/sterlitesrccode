package vsg.integration.export.droplet;

import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.integration.export.IRepositoryConstants;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class ExportConfigTypeMappedDroplet extends DynamoServlet implements IDropletConstants, IRepositoryConstants {
	/**
	 * input parameter export config id
	 */
	private static final String INPUT_EXPORT_CONFIG_ID = "exportConfigId";
	/**
	 * input parameter type name
	 */
	private static final String INPUT_TYPE_FULL_PATH = "typeFullPath";
	/*
	 * output param model name
	 */
	private static final String OUTPUT_MAPPED = "mapped";
	/**
	 * export repository
	 */
	private Repository mRepository;

	//------------------------------------------------------------------------------------------------------------------

	public Repository getRepository() {
		return mRepository;
	}

	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws ServletException - if cannot make output or empty oparam
	 * @throws IOException      - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		RepositoryItem exportConfig = getExportConfig(pRequest);
		if (exportConfig == null) {
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			return;
		}
		String inputTypeFullPath = pRequest.getParameter(INPUT_TYPE_FULL_PATH);
		if (StringUtils.isBlank(inputTypeFullPath)) {
			vlogError("Type full path is not specified.");
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			return;
		}

		pRequest.setParameter(OUTPUT_MAPPED, isTypeUnderMapping(exportConfig, inputTypeFullPath));
		pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
	}

	/**
	 * @param pExportConfig export config
	 * @param pTypeFullPath type full path
	 * @return true if type full path under mapping
	 */
	private boolean isTypeUnderMapping(RepositoryItem pExportConfig, String pTypeFullPath) {
		Set<RepositoryItem> mappings = (Set<RepositoryItem>) pExportConfig.getPropertyValue(PRP_EXPORT_RECORD_MAPPINGS);
		if (mappings != null) {
			for (RepositoryItem mapping : mappings) {
				String wsdlPath = (String) mapping.getPropertyValue(PRP_RECORD_MAPPING_WSDL_PATH);
				if (!StringUtils.isBlank(wsdlPath)) {
					if (wsdlPath.contains(pTypeFullPath)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * @param pRequest request
	 * @return loads export config repository item by request params
	 */
	private RepositoryItem getExportConfig(DynamoHttpServletRequest pRequest) {
		String inputParamExportConfigId = pRequest.getParameter(INPUT_EXPORT_CONFIG_ID);
		if (StringUtils.isBlank(inputParamExportConfigId)) {
			vlogError("No export config id is specified.");
			return null;
		}
		try {
			return getRepository().getItem(inputParamExportConfigId, ID_EXPORT_CONFIG);
		} catch (RepositoryException e) {
			vlogError("Unable to load export config item by id: {0}", inputParamExportConfigId);
		}
		return null;
	}

}
