package vsg.integration.export.droplet;

import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.integration.export.IRepositoryConstants;
import vsg.integration.export.store.ExportConfigExporter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Dmitry Golubev
 */
public class ExportConfigExportDroplet extends DynamoServlet implements IDropletConstants, IRepositoryConstants {
	/**
	 * export config exporter
	 */
	private ExportConfigExporter mExportConfigExporter;
	/**
	 * export repository
	 */
	private MutableRepository mRepository;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfigExporter getExportConfigExporter() {
		return mExportConfigExporter;
	}

	public void setExportConfigExporter(ExportConfigExporter pExportConfigExporter) {
		mExportConfigExporter = pExportConfigExporter;
	}

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		mRepository = pRepository;
	}
//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws ServletException - if cannot make output or empty oparam
	 * @throws IOException      - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		String exportConfigId = pRequest.getParameter("exportConfigId");
		if (StringUtils.isBlank(exportConfigId)) {
			vlogError("No id specified to export 'export config'.");
		} else {
			try {
				RepositoryItem exportConfig = getRepository().getItem(exportConfigId, ID_EXPORT_CONFIG);
				if (exportConfig == null) {
					vlogError("No export config is found by id {0}.", exportConfigId);
				} else {
					formExportResponse(pResponse,
							getExportConfigExporter().process(exportConfigId)
					);
					pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
				}
			} catch (RepositoryException e) {
				vlogError(e, "Unable to lookup info.");
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			}
		}
	}

	/**
	 * forms response to upload file
	 *
	 * @param pResponse       http response
	 * @param pExportFileName generated export file name
	 */
	private void formExportResponse(HttpServletResponse pResponse, String pExportFileName) {
		pResponse.setHeader("Content-disposition", "inline;filename=ExportConfiguration.xml");
		pResponse.setContentType("application/xml");
		try {
			OutputStream out = pResponse.getOutputStream();
			try (FileInputStream in = new FileInputStream(pExportFileName);) { // !!!
				byte[] buffer = new byte[4096];
				int length;
				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}
				in.close();
				out.flush();
			}
		} catch (IOException e) {
			vlogError(e, "Error");
		}
	}
}
