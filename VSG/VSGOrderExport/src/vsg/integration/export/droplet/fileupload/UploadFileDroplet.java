package vsg.integration.export.droplet.fileupload;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import vsg.integration.export.ExportConfig;
import vsg.integration.export.FileSettingsInfo;
import vsg.integration.export.droplet.IDropletConstants;

import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class UploadFileDroplet extends DynamoServlet implements IDropletConstants {
	/**
	 * input parameter exportConfig
	 */
	private final static String INPUT_EXPORT_CONFIG = "exportConfig";

	/**
	 * file params from request
	 */
	private final Map<String, FileItem> fFileParams = new LinkedHashMap<String, FileItem>();
	/**
	 * regular params from request
	 */
	private final Map<String, String> fRegularParams = new LinkedHashMap<String, String>();
	/**
	 * file settings info
	 */
	private FileSettingsInfo fileSettingsInfo;
	/**
	 * upload dir for files
	 */
	private String mUploadDir;

	//------------------------------------------------------------------------------------------------------------------

	public FileSettingsInfo getFileSettingsInfo() {
		return fileSettingsInfo;
	}

	public void setFileSettingsInfo(FileSettingsInfo fileSettingsInfo) {
		this.fileSettingsInfo = fileSettingsInfo;
	}

	public String getUploadDir() {
		return mUploadDir;
	}

	public void setUploadDir(String pUploadDir) {
		mUploadDir = pUploadDir;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws javax.servlet.ServletException - if cannot make output or empty oparam
	 * @throws java.io.IOException            - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		try {
			new File(getUploadDir()).mkdirs();
			ServletFileUpload upload = getFileUpload(getUploadDir());
			List<FileItem> fileItems = upload.parseRequest(pRequest);

			if (fileItems.size() == 0) {
				vlogError("No file items.");
				pRequest.setParameter(ERROR_MSG, "No files for import.");
				pRequest.serviceLocalParameter(ERROR, pRequest, pResponse);
			} else {
				convertToMaps(fileItems);
				pRequest.setParameter("regularParams", fRegularParams);
				pRequest.serviceLocalParameter(REGULAR_OPARAM, pRequest, pResponse);
			}
			ExportConfig exportConfig = (ExportConfig)pRequest.getObjectParameter(INPUT_EXPORT_CONFIG);
			if (exportConfig != null) {
				for (FileItem fileItem : fileItems) {
					String loadedFileName = uploadFileItem(fileItem, getUploadDir(), exportConfig.getRepositoryId());
					if (StringUtils.isBlank(loadedFileName)) {
						pRequest.setParameter(ERROR_MSG, "Unable upload file.");
						pRequest.serviceLocalParameter(ERROR, pRequest, pResponse);
					} else {
						pRequest.setParameter("fileName", loadedFileName);
						pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
					}
				}
			}
		} catch (Exception ex) {
			vlogError(ex, "Unable upload file");
			pRequest.setParameter(ERROR_MSG, "Unable upload file.");
			pRequest.serviceLocalParameter(ERROR, pRequest, pResponse);
		}
	}

	/**
	 * check if File Item is file upload field
	 *
	 * @param aFileItem - file item
	 * @return true if file item is file upload field
	 */
	private boolean isFileUploadField(FileItem aFileItem) {
		return !aFileItem.isFormField();
	}

	/**
	 * filling regular params map and file params map
	 *
	 * @param aFileItems
	 */
	private void convertToMaps(List<FileItem> aFileItems) {
		for (FileItem item : aFileItems) {
			if (isFileUploadField(item)) {
				fFileParams.put(item.getFieldName(), item);
			} else {
				fRegularParams.put(item.getFieldName(), item.getString());
			}
		}
	}

	/**
	 * initialize servletFileUpload
	 *
	 * @param pFilePath upload file path
	 * @return servletFileUpload
	 */
	private ServletFileUpload getFileUpload(String pFilePath) {
		int maxFileSize = 500 * 1024;
		int maxMemSize = 4 * 1024;

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(maxMemSize); // maximum size that will be stored in memory
		factory.setRepository(new File(pFilePath)); // Location to save data that is larger than maxMemSize.

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);
		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);

		return upload;
	}

	/**
	 * writes FileItem to file
	 *
	 * @param pFileItem  fileItem
	 * @param pUploadDir pUploadDir
	 * @throws Exception
	 */
	protected String uploadFileItem(FileItem pFileItem, String pUploadDir, String pExportConfigId) throws Exception {
		if (!pFileItem.isFormField()) {
			String fileName = pFileItem.getName();
			fileName = fileName.substring(0, fileName.lastIndexOf(".")) + new Date().getTime() +
					fileName.substring(fileName.lastIndexOf("."));
			return upload(pFileItem, pUploadDir, fileName);
		}
		return null;
	}

	/**
	 * upload file main method
	 *
	 * @param pFileItem  - file item
	 * @param pUploadDir - upload directory
	 * @param fileName   - file name
	 * @return file name
	 */
	protected String upload(FileItem pFileItem, String pUploadDir, String fileName) {
		File fileToSave = new File(pUploadDir + "/" + fileName);
		File directory = new File(pUploadDir);
		if (createUploadDirs(directory)) {
			if( saveFile(pFileItem, fileToSave) ) {
				return fileName;
			}
		}
		return null;
	}

	/**
	 * create directories
	 *
	 * @param pDirectory - final directory that should be created
	 * @return true if all directories were created
	 */
	protected boolean createUploadDirs(File pDirectory) {
		boolean flag = true;
		if (pDirectory.exists()) {
			vlogDebug("Directory for that config already exists");
		} else {
			if (!pDirectory.mkdirs()) {
				vlogError("Could not save create all necessary directories, Please, check permissions");
				flag = false;
			}
		}
		return flag;
	}

	/**
	 * save file
	 *
	 * @param pFileItem   - fileitem
	 * @param pFileToSave - file to save
	 */
	protected boolean saveFile(FileItem pFileItem, File pFileToSave) {
		if (pFileToSave.isDirectory()) {
			vlogError("Could not save '{0}', it's not a file", pFileToSave.getAbsolutePath());
			return false;
		} else {
			try {
				pFileItem.write(pFileToSave);
				postUpload(pFileToSave);
				vlogDebug("File '{0}' is uploaded", pFileToSave.getAbsolutePath());
			} catch (Exception e) {
				vlogError(e, "Unable save '{0}'", pFileToSave.getAbsolutePath());
				return false;
			}
		}
		return true;
	}

	/**
	 * post upload file processing
	 *
	 * @param pFile uploaded file to local dir
	 */
	protected void postUpload(File pFile) {

	}
}
