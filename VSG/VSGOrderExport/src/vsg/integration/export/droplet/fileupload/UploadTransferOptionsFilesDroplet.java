package vsg.integration.export.droplet.fileupload;

import org.apache.commons.fileupload.FileItem;

public class UploadTransferOptionsFilesDroplet extends UploadFileDroplet {

	@Override
	protected String uploadFileItem(FileItem pFileItem, String pUploadDir, String pExportConfigId) throws Exception {
		if (!pFileItem.isFormField()) {

			String fieldName = pFileItem.getFieldName();
			if ("file1".equals(fieldName)) {
				pUploadDir = getFileSettingsInfo().getXsdPath();
			} else if ("file2".equals(fieldName)) {
				pUploadDir = getFileSettingsInfo().getTransferOptionsKeyPath();
			} else if ("file3".equals(fieldName)) {
				pUploadDir = getFileSettingsInfo().getTransferOptionsEncKeyPath();
			}
			String fileName = pFileItem.getName();
			return upload(pFileItem, pUploadDir + "/" + pExportConfigId, fileName);
		}
		return null;
	}

}
