package vsg.integration.export.droplet.fileupload;

import vsg.integration.export.store.ExportConfigImporter;

import java.io.File;

/**
 * @author Dmitry Golubev
 */
public class ExportConfigImportUploadFileDroplet extends UploadFileDroplet {
	/**
	 * export config importer
	 */
	private ExportConfigImporter mImporter;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfigImporter getImporter() {
		return mImporter;
	}

	public void setImporter(ExportConfigImporter pImporter) {
		mImporter = pImporter;
	}

	//------------------------------------------------------------------------------------------------------------------

	@Override
	protected void postUpload(File pFile) {
		super.postUpload(pFile);

		getImporter().process(pFile.getAbsolutePath());
	}
}
