package vsg.integration.export.droplet.fileupload;

import org.apache.commons.fileupload.FileItem;

public class UploadFileFromFormDroplet extends UploadFileDroplet {

	@Override
	protected String uploadFileItem(FileItem pFileItem, String pUploadDir, String pExportConfigId) throws Exception {
		if (!pFileItem.isFormField()) {
			String fileName = pFileItem.getName();
			return upload(pFileItem, pUploadDir + "/" + pExportConfigId, fileName);
		}
		return null;
	}
}
