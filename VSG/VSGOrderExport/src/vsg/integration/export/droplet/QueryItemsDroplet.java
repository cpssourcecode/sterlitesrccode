package vsg.integration.export.droplet;

import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Query items from provided repository by item descriptor and rql query
 */
public class QueryItemsDroplet extends DynamoServlet implements IDropletConstants {

	/**
	 * repositoryItems oparam
	 */
	private final static String REPOSITORY_ITEMS = "repositoryItems";
	/**
	 * repository
	 */
	private MutableRepository mRepository;
	/**
	 * descriptor
	 */
	private String mDescriptor;
	/**
	 * rql query
	 */
	private String mQuery;

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		this.mRepository = pRepository;
	}

	public String getDescriptor() {
		return mDescriptor;
	}

	public void setDescriptor(String pDescriptor) {
		this.mDescriptor = pDescriptor;
	}

	public String getQuery() {
		return mQuery;
	}

	public void setQuery(String pQuery) {
		this.mQuery = pQuery;
	}

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws javax.servlet.ServletException - if cannot make output or empty oparam
	 * @throws java.io.IOException            - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		RepositoryItem[] items = null;
		try {
			RepositoryView view = getRepository().getView(getDescriptor());
			RqlStatement statement = RqlStatement.parseRqlStatement(getQuery());
			Object params[] = new Object[0];
			items = statement.executeQuery(view, params);
		} catch (RepositoryException e) {
			vlogDebug("Repository exception during quering items");
		}

		if (items == null || items.length == 0) {
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		} else {
			pRequest.setParameter(REPOSITORY_ITEMS, items);
			pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
	}

}
