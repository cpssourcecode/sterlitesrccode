package vsg.integration.export.droplet;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.integration.export.mapping.PropertyMappingElement;
import vsg.integration.export.mapping.PropertyMappingInfo;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author Dmitry Golubev
 */
public class PropertyMappingCheckDroplet extends DynamoServlet implements IDropletConstants {
	/**
	 * input parameter attributePath
	 */
	private final static String INPUT_ATTRIBUTE_PATH = "attributePath";
	/**
	 * input parameter propertyMappingInfo
	 */
	private final static String PROPERTY_MAPPING_INFO = "propertyMappingInfo";
	/**
	 * input parameter repositoryPath
	 */
	private final static String OUTPUT_REPOSITORY_PATH = "repositoryPath";
	/**
	 * input parameter staticValue
	 */
	private final static String OUTPUT_STATIC_VALUE = "staticValue";

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws javax.servlet.ServletException - if cannot make output or empty oparam
	 * @throws java.io.IOException            - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		String wsdlPath = pRequest.getParameter(INPUT_ATTRIBUTE_PATH);
		if (StringUtils.isBlank(wsdlPath)) {
			vlogError(INPUT_ATTRIBUTE_PATH + " is not defined.");
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		} else {
			PropertyMappingInfo propertyMappingInfo = (PropertyMappingInfo)pRequest.getObjectParameter(PROPERTY_MAPPING_INFO);
			if (propertyMappingInfo != null) {
				PropertyMappingElement mappingElement = propertyMappingInfo.findElementByWsdlPath(wsdlPath);
				if (mappingElement == null) {
					vlogDebug("No mapping is found for {0}", wsdlPath);
					pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
				} else {
					vlogDebug("Mapping for {0} is defined: {1}", wsdlPath, mappingElement.getRepositoryPath());
					pRequest.setParameter(OUTPUT_REPOSITORY_PATH, mappingElement.getRepositoryPath());
					pRequest.setParameter(OUTPUT_STATIC_VALUE, mappingElement.getStaticValue());
					pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
				}
			} else {
				vlogDebug("No propertyMappingInfo is defined for {0}", wsdlPath);
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			}
		}
	}
}
