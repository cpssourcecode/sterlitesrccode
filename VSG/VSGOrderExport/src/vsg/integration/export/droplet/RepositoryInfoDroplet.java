package vsg.integration.export.droplet;

import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.integration.export.repository.RepositoryModelBuilder;
import vsg.integration.export.repository.model.ItemDescriptorInfo;
import vsg.integration.export.repository.model.RepositoryModel;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class RepositoryInfoDroplet extends DynamoServlet implements IDropletConstants {
	/**
	 * input parameter repository name
	 */
	private static final String INPUT_REPOSITORY = "repository";
	/**
	 * input parameter item descriptor
	 */
	private static final String INPUT_ITEM_DESCRIPTOR_FULL_PATH = "itemDescriptorFullPath";
	/*
	 * output param model name
	 */
	private static final String OUTPUT_MODEL = "model";
	/*
	 * output param item descriptor info name
	 */
	private static final String OUTPUT_ITEM_DESCRIPTOR_INFO = "itemDescriptorInfo";

	/**
	 * map with repository name and its' model
	 */
	Map<String, RepositoryModel> mRepositoryModels = new HashMap<String, RepositoryModel>();

	/**
	 * repository model builder
	 */
	RepositoryModelBuilder mModelBuilder;

	//------------------------------------------------------------------------------------------------------------------

	public RepositoryModelBuilder getModelBuilder() {
		return mModelBuilder;
	}

	public void setModelBuilder(RepositoryModelBuilder pModelBuilder) {
		mModelBuilder = pModelBuilder;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * reset repository analyzed models
	 */
	public void resetState() {
		mRepositoryModels.clear();
	}

	/**
	 * validate repository name
	 *
	 * @param pRepositoryName repository name
	 * @return repository if is found
	 */
	private Repository getRepositoryByName(String pRepositoryName) {
		if (StringUtils.isBlank(pRepositoryName)) {
			vlogError("Repository is not defined.");
			return null;
		}
		Object repository = getNucleus().resolveName(pRepositoryName);
		if (repository == null) {
			vlogError("Repository is not found: {0}", pRepositoryName);
			return null;
		}
		if (!(repository instanceof Repository)) {
			vlogError("Specified component is not Repository: {0}", pRepositoryName);
			return null;
		} else {
			return (Repository) repository;
		}
	}

	/**
	 * get repository model
	 *
	 * @param pRepositoryName repository name
	 * @return repository model
	 */
	public RepositoryModel getRepositoryModel(String pRepositoryName) {
		Repository repository = getRepositoryByName(pRepositoryName);
		if (repository == null) {
			return null;
		}
		RepositoryModel model = mRepositoryModels.get(pRepositoryName);
		if (model == null) {
			model = getModelBuilder().getRepositoryModel(repository);
			mRepositoryModels.put(pRepositoryName, model);
		}
		return model;
	}

	/**
	 * retrieve item descriptor info object
	 *
	 * @param pItemDescriptorName item descriptor name
	 * @param pRepositoryName     repository name
	 * @return item descriptor info object
	 */
	public ItemDescriptorInfo getItemDescriptorInfo(String pItemDescriptorName, String pRepositoryName) {
		RepositoryModel model = getRepositoryModel(pRepositoryName);
		return model.getItemDescriptorInfoByName(pItemDescriptorName);
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws ServletException - if cannot make output or empty oparam
	 * @throws IOException      - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
/*
		if(isLoggingDebug()) {
			logState("jvm state before");
		}
*/
		String inputParamRepository = pRequest.getParameter(INPUT_REPOSITORY);
		String inputItemDescriptorFullPath = pRequest.getParameter(INPUT_ITEM_DESCRIPTOR_FULL_PATH);

		RepositoryModel model = getRepositoryModel(inputParamRepository);
		if (null == model) {
			vlogError("ExportConfig is not found: {0}", inputParamRepository);
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		} else {
			vlogDebug("ExportConfig is found: {0}", inputParamRepository);
			pRequest.setParameter(OUTPUT_MODEL, model);
			if (!StringUtils.isBlank(inputItemDescriptorFullPath)) {
				ItemDescriptorInfo itemDescriptorInfo =
						model.getItemDescriptorInfoByFullPath(inputItemDescriptorFullPath);
				if (null == itemDescriptorInfo) {
					vlogError("Item descriptor info is not found {0}.", inputItemDescriptorFullPath);
				} else {
					vlogDebug("Item descriptor is found: {0}", inputItemDescriptorFullPath);
					pRequest.setParameter(OUTPUT_ITEM_DESCRIPTOR_INFO, itemDescriptorInfo);
				}
			}
			pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
/*
		if(isLoggingDebug()) {
			logState("jvm state after");
		}
*/
	}

	/**
	 * log jvm state
	 */
/*
	private void logState(String pMsg) {
		Runtime runtime = Runtime.getRuntime();

		int mb = 1024 * 1024;

		String used = String.valueOf((runtime.totalMemory() - runtime.freeMemory()) / mb);
		String free = String.valueOf(runtime.freeMemory() / mb);
		String total = String.valueOf(runtime.totalMemory() / mb);
		String max = String.valueOf(runtime.maxMemory() / mb);

		//Print used memory
		vlogDebug(pMsg + " Used Free Ttl Max:" + used + " " + free + " " + total + " " + max);
	}
*/
}
