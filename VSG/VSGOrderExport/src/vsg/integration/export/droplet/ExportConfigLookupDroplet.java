package vsg.integration.export.droplet;

import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.integration.export.IRepositoryConstants;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * Loads export config by id
 * If specified property param, will load export config property and put to 'property' parameter
 *
 * @author Dmitry Golubev
 */
public class ExportConfigLookupDroplet extends DynamoServlet implements IDropletConstants, IRepositoryConstants {
	/**
	 * input param id
	 */
	public final static String INPUT_PARAM_ID = "id";
	/**
	 * input param property
	 */
	public final static String INPUT_PARAM_PROPERTY = "property";
	/**
	 * input param properties
	 */
	public final static String INPUT_PARAM_PROPERTIES = "properties";
	/**
	 * output param export config
	 */
	public final static String OUTPUT_PARAM_EXPORT_CONFIG = "exportConfig";
	/**
	 * export repository
	 */
	private MutableRepository mRepository;

	//------------------------------------------------------------------------------------------------------------------

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		mRepository = pRepository;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Droplet service method.
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws javax.servlet.ServletException - if cannot make output or empty oparam
	 * @throws java.io.IOException            - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		String exportConfigId = pRequest.getParameter(INPUT_PARAM_ID);
		if (StringUtils.isBlank(exportConfigId)) {
			vlogError("No id specified to load export config.");
		} else {
			try {
				RepositoryItem exportConfig = getRepository().getItem(exportConfigId, ID_EXPORT_CONFIG);
				if (exportConfig == null) {
					vlogError("No export config is found by id {0}.", exportConfigId);
				} else {
					pRequest.setParameter(OUTPUT_PARAM_EXPORT_CONFIG, exportConfig);

					initPropertyParam(pRequest, exportConfig);
					initPropertiesParam(pRequest, exportConfig);
					pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
				}
			} catch (RepositoryException e) {
				vlogError(e, "Unable to lookup info.");
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			}
		}
	}

	/**
	 * check and init request parameter 'property'
	 *
	 * @param pRequest      request
	 * @param pExportConfig repository item of loaded export config
	 */
	private void initPropertyParam(DynamoHttpServletRequest pRequest, RepositoryItem pExportConfig) {
		String exportConfigProperty = pRequest.getParameter(INPUT_PARAM_PROPERTY);
		if (!StringUtils.isBlank(exportConfigProperty)) {
			Object property = pExportConfig.getPropertyValue(exportConfigProperty);
			pRequest.setParameter(exportConfigProperty, property);
		}
	}

	/**
	 * check and init request parameter 'property'
	 *
	 * @param pRequest      request
	 * @param pExportConfig repository item of loaded export config
	 */
	private void initPropertiesParam(DynamoHttpServletRequest pRequest, RepositoryItem pExportConfig) {
		String exportConfigProperties = pRequest.getParameter(INPUT_PARAM_PROPERTIES);
		if (!StringUtils.isBlank(exportConfigProperties)) {
			String[] properties = exportConfigProperties.split(",");
			for (String property : properties) {
				pRequest.setParameter(property, pExportConfig.getPropertyValue(property));
			}
		}
	}
}
