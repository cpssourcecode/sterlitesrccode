package vsg.integration.export.droplet;

/**
 * @author Dmitry Golubev
 */
public interface IDropletConstants {
	/**
	 * output param empty name
	 */
	String EMPTY = "empty";
	/**
	 * output param error name
	 */
	String ERROR = "error";
	/**
	 * output param error msg name
	 */
	String ERROR_MSG = "errorMsg";
	/**
	 * output param output name
	 */
	String OUTPUT_OPARAM = "output";
	/**
	 * transactionId property name
	 */
	String TRANSACTION_ID = "transactionId";
	/**
	 * regular oparam
	 */
	String REGULAR_OPARAM = "regularOparam";
}
