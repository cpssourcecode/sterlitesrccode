package vsg.integration.export;

/**
 * Constants for schedule service
 */
public interface IScheduleConstants {
	/**
	 * FtpExportJob job name
	 */
	String XML_TRANSFER_JOB_NAME = "XMLTransferJob";
	/**
	 * TransactionErrorRepJob job name
	 */
	String TR_ERROR_REP_JOB_NAME = "TransactionErrorRepJob";
	/**
	 * TransactionSuccessfulRepJob job name
	 */
	String TR_SUCCESS_REP_JOB_NAME = "TransactionSuccessfulRepJob";
	/**
	 * WebserviceJob job name
	 */
	String WEBSERVICE_JOB_NAME = "WebserviceJob";
}
