package vsg.integration.export.repository;

import atg.adapter.gsa.GSAItemDescriptor;
import atg.adapter.gsa.GSARepository;
import atg.beans.DynamicPropertyDescriptor;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryPropertyDescriptor;
import vsg.integration.export.repository.model.BrokenItemDescriptorInfo;
import vsg.integration.export.repository.model.BrokenItemDescriptorInfoReason;
import vsg.integration.export.repository.model.ItemDescriptorInfo;
import vsg.integration.export.repository.model.PropertyInfo;
import vsg.integration.export.repository.model.RepositoryModel;

import java.util.HashMap;

/**
 * @author Dmitry Golubev
 */
public class RepositoryModelBuilder extends GenericService {
	/**
	 * test repository
	 */
	private Repository mRepository;
	/**
	 * map of analyzed item descirptors map
	 */
	private HashMap<String, ItemDescriptorInfo> mDescriptorNameMap = new HashMap<String, ItemDescriptorInfo>();
	/**
	 * map of analyzed item descirptors map
	 */
	private HashMap<String, ItemDescriptorInfo> mDescriptorFullPathMap = new HashMap<String, ItemDescriptorInfo>();

	//------------------------------------------------------------------------------------------------------------------

	public Repository getRepository() {
		return mRepository;
	}

	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	public HashMap<String, ItemDescriptorInfo> getDescriptorNameMap() {
		return mDescriptorNameMap;
	}

	public void setDescriptorNameMap(HashMap<String, ItemDescriptorInfo> pDescriptorNameMap) {
		mDescriptorNameMap = pDescriptorNameMap;
	}

	public HashMap<String, ItemDescriptorInfo> getDescriptorFullPathMap() {
		return mDescriptorFullPathMap;
	}

	public void setDescriptorFullPathMap(HashMap<String, ItemDescriptorInfo> pDescriptorFullPathMap) {
		mDescriptorFullPathMap = pDescriptorFullPathMap;
	}

	//------------------------------------------------------------------------------------------------------------------

	public void analyze() {
		RepositoryModel model = getRepositoryModel(getRepository());
		previewModel(model);
	}


	//------------------------------------------------------------------------------------------------------------------

	/**
	 * init repository model
	 *
	 * @param pRepository repository
	 * @return repository model
	 */
	public RepositoryModel getRepositoryModel(Repository pRepository) {
		if (pRepository == null) {
			vlogError("Repository is not defined.");
			return null;
		}
		resetState();
		RepositoryModel model = new RepositoryModel();
		try {
			for (String itemDescriptorName : pRepository.getItemDescriptorNames()) {
				ItemDescriptorInfo itemInfo = initItemDescirptorInfo(
						null,
						pRepository.getItemDescriptor(itemDescriptorName),
						null);
				model.addItemDescriptor(itemInfo);
			}
		} catch (RepositoryException e) {
			vlogError(e.toString());
		}
		vlogDebug("Repository model is built. {0}", ((GSARepository) pRepository).getInvalidationAbsoluteName());
		return model;
	}

	private int getItemDescriptorDepth(ItemDescriptorInfo pItemInfo, int currentDepth) {
		int currentMaxDepth = currentDepth;
		if (currentDepth > 100) {
			return currentDepth;
		}
		for (ItemDescriptorInfo itemDescriptorInfo : pItemInfo.getSubItemDescriptorInfos()) {
			int tempDepth = getItemDescriptorDepth(itemDescriptorInfo, currentDepth++);
			if (currentMaxDepth < tempDepth) {
				currentMaxDepth = tempDepth;
			}
		}
		return currentMaxDepth;
	}

	private void previewModel(RepositoryModel pModel) {
		vlogDebug("_____________________________________________________");
		vlogDebug("Mode depth:");
		for (ItemDescriptorInfo itemInfo : pModel.getItemDescriptorInfos()) {
			vlogDebug(itemInfo.getName() + " - " + getItemDescriptorDepth(itemInfo, 0));
		}
	}

	/**
	 * init item descriptor info
	 *
	 * @param pParentItemInfo     parent item descriptor info
	 * @param pItemDescriptor     repository item descriptor
	 * @param pPropertyDescriptor repository property descriptor
	 * @return item descriptor info
	 */
	private ItemDescriptorInfo initItemDescirptorInfo(ItemDescriptorInfo pParentItemInfo,
													  RepositoryItemDescriptor pItemDescriptor,
													  RepositoryPropertyDescriptor pPropertyDescriptor) {
		if (getDescriptorNameMap().containsKey(pItemDescriptor.getItemDescriptorName())) {
			// check for full path
			String fullPath = getPathItemDescriptor(pParentItemInfo, pItemDescriptor);
			if (fullPath != null && getDescriptorFullPathMap().containsKey(fullPath)) {
				return getDescriptorFullPathMap().get(fullPath);
			}
			return getClonedItemDescriptor(pParentItemInfo, pItemDescriptor, pPropertyDescriptor);
		} else {
			return createItemDescriptor(pParentItemInfo, pItemDescriptor, pPropertyDescriptor);
		}
	}

	/**
	 * get cloned item descriptor info
	 *
	 * @param pParentItemInfo     parent item descriptor info
	 * @param pItemDescriptor     repository item descriptor
	 * @param pPropertyDescriptor property descriptor
	 * @return cloned item descriptor info
	 */
	private ItemDescriptorInfo getClonedItemDescriptor(ItemDescriptorInfo pParentItemInfo,
													   RepositoryItemDescriptor pItemDescriptor,
													   RepositoryPropertyDescriptor pPropertyDescriptor) {
		ItemDescriptorInfo originalItemDescriptorInfo = getDescriptorNameMap().get(
				pItemDescriptor.getItemDescriptorName());

		ItemDescriptorInfo clonedItemDescriptorInfo = new ItemDescriptorInfo(originalItemDescriptorInfo);
		clonedItemDescriptorInfo.setName(
				pPropertyDescriptor == null ? pItemDescriptor.getItemDescriptorName() : pPropertyDescriptor.getName()
		);
		clonedItemDescriptorInfo.setPreviewName(
				getItemDescriptorPreviewName(pItemDescriptor, pPropertyDescriptor)
		);

		clonedItemDescriptorInfo.setPropertyType(
				pPropertyDescriptor == null ? pItemDescriptor.getClass() : pPropertyDescriptor.getPropertyType()
		);
		clonedItemDescriptorInfo.setComponentPropertyType(
				pPropertyDescriptor == null ? null : pPropertyDescriptor.getComponentPropertyType()
		);
		initParent(clonedItemDescriptorInfo, pParentItemInfo);
/*
		NOT ALLOWED - ORIGINAL WILL BE OVERWRITTEN
		getDescriptorNameMap().put(pItemDescriptor.getItemDescriptorName(), clonedItemDescriptorInfo);
*/
		getDescriptorFullPathMap().put(clonedItemDescriptorInfo.getFullPath(), clonedItemDescriptorInfo);

		return clonedItemDescriptorInfo;
	}

	/**
	 * create new one item descriptor info
	 *
	 * @param pParentItemInfo     parent item descriptor info
	 * @param pItemDescriptor     repository item descriptor
	 * @param pPropertyDescriptor property descriptor
	 * @return new item descriptor info
	 */
	private ItemDescriptorInfo createItemDescriptor(ItemDescriptorInfo pParentItemInfo,
													RepositoryItemDescriptor pItemDescriptor,
													RepositoryPropertyDescriptor pPropertyDescriptor) {

		ItemDescriptorInfo itemInfo = new ItemDescriptorInfo(
				getRepositoryName(pItemDescriptor),
				pPropertyDescriptor == null ? pItemDescriptor.getItemDescriptorName() : pPropertyDescriptor.getName(),
				getItemDescriptorPreviewName(pItemDescriptor, pPropertyDescriptor),
				pPropertyDescriptor == null ? pItemDescriptor.getClass() : pPropertyDescriptor.getPropertyType(),
				pPropertyDescriptor == null ? null : pPropertyDescriptor.getComponentPropertyType()
		);
		initParent(itemInfo, pParentItemInfo);

		getDescriptorNameMap().put(pItemDescriptor.getItemDescriptorName(), itemInfo);
		getDescriptorFullPathMap().put(itemInfo.getFullPath(), itemInfo);

		analyzePropertyDescriptors(itemInfo, pItemDescriptor);
		analyzeSuperTypesPropertyDescriptors(itemInfo, pItemDescriptor);
		return itemInfo;
	}

	/**
	 * @param pItemDescriptor     repository item descriptor
	 * @param pPropertyDescriptor repository property descriptor
	 * @return preview name
	 */
	private String getItemDescriptorPreviewName(RepositoryItemDescriptor pItemDescriptor,
												RepositoryPropertyDescriptor pPropertyDescriptor) {
		if (pPropertyDescriptor == null) {
			return pItemDescriptor.getItemDescriptorName();
		} else {
			String displayName = pPropertyDescriptor.getDisplayName();
			if (displayName != null && displayName.contains("(")) {
				return displayName;
			} else {
				return displayName + " (" + pPropertyDescriptor.getName() + ")";
			}
		}
	}

	/**
	 * analyze repository property descriptors
	 *
	 * @param pItemDescriptorInfo       item descriptor info
	 * @param pRepositoryItemDescriptor repository item descriptor info
	 */
	private void analyzePropertyDescriptors(ItemDescriptorInfo pItemDescriptorInfo,
											RepositoryItemDescriptor pRepositoryItemDescriptor) {
		for (DynamicPropertyDescriptor dpd : pRepositoryItemDescriptor.getPropertyDescriptors()) {
			RepositoryPropertyDescriptor propertyDescriptor = (RepositoryPropertyDescriptor) dpd;
			Class<?> componentType = propertyDescriptor.getComponentPropertyType();
			Class<?> propertyType = propertyDescriptor.getPropertyType();

			boolean isRelationShipChildren =
					(componentType != null && componentType.isAssignableFrom(RepositoryItem.class));
			boolean isRelationShipChild = (propertyType != null && propertyType.isAssignableFrom(RepositoryItem.class));

			if (isRelationShipChild || isRelationShipChildren) {
				RepositoryItemDescriptor itemDescriptor = getPepositoryItemDescriptor(propertyDescriptor);
				String itemDescriptorName = itemDescriptor.getItemDescriptorName();
				String brokenReason = isBrokenSubItem(pItemDescriptorInfo, getRepositoryName(itemDescriptor), itemDescriptorName);
				if (brokenReason != null) {
					addBrokenSubItemDescriptorInfo(pItemDescriptorInfo, pRepositoryItemDescriptor, propertyDescriptor, brokenReason);
				} else {
					addSubItemDescriptorInfo(pItemDescriptorInfo, propertyDescriptor);
				}
			} else {
				addPropertyInfo(pItemDescriptorInfo, propertyDescriptor);
			}
		}
	}

	/**
	 * check sub item descriptors and add its properties
	 *
	 * @param pItemDescriptorInfo       ItemDescriptorInfo
	 * @param pRepositoryItemDescriptor repository item descriptor
	 */
	private void analyzeSuperTypesPropertyDescriptors(ItemDescriptorInfo pItemDescriptorInfo,
													  RepositoryItemDescriptor pRepositoryItemDescriptor) {
		GSAItemDescriptor[] subItemDescriptors = ((GSAItemDescriptor) pRepositoryItemDescriptor).getSubTypeDescriptors();
		if (subItemDescriptors != null) {
			for (GSAItemDescriptor subItemDescriptor : subItemDescriptors) {
				String fullPath = getPathItemDescriptor(null, subItemDescriptor);
				ItemDescriptorInfo subItemDescriptorInfo = null;
				if (fullPath != null && getDescriptorFullPathMap().containsKey(fullPath)) {
					subItemDescriptorInfo = getDescriptorFullPathMap().get(fullPath);
				}
				if (subItemDescriptorInfo == null) {
					subItemDescriptorInfo = initItemDescirptorInfo(null, subItemDescriptor, null);
				}
				for (PropertyInfo propertyInfo : subItemDescriptorInfo.getProperties()) {
					if (!pItemDescriptorInfo.getProperties().contains(propertyInfo)) {
						PropertyInfo subItemPropertyInfo = new PropertyInfo(propertyInfo);
						subItemPropertyInfo.setDisplayName("[" + subItemDescriptor.getItemDescriptorName() + "] " +
								"" + subItemPropertyInfo.getDisplayName());
						pItemDescriptorInfo.getProperties().add(subItemPropertyInfo);
					}
				}
			}
		}
		vlogDebug("Bean descriptor: " + pRepositoryItemDescriptor.getBeanDescriptor());

	}

	/**
	 * add broken item descriptor info
	 *
	 * @param pParentItemInfo     item descriptor info
	 * @param pItemDescriptor     repository item descriptor
	 * @param pPropertyDescriptor repository property descriptor
	 * @param pReason             reason
	 */
	private void addBrokenSubItemDescriptorInfo(ItemDescriptorInfo pParentItemInfo,
												RepositoryItemDescriptor pItemDescriptor,
												RepositoryPropertyDescriptor pPropertyDescriptor,
												String pReason) {
		BrokenItemDescriptorInfo itemInfo = new BrokenItemDescriptorInfo(
				getRepositoryName(pItemDescriptor),
				pItemDescriptor.getItemDescriptorName(),
				getItemDescriptorPreviewName(pItemDescriptor, pPropertyDescriptor),
				pReason
		);
		pParentItemInfo.addBrokenItemDescriptorInfo(itemInfo);
	}

	/**
	 * add sub item descriptor info
	 *
	 * @param pParentItemInfo     item descriptor info
	 * @param pPropertyDescriptor repository property descriptor
	 */
	private void addSubItemDescriptorInfo(ItemDescriptorInfo pParentItemInfo,
										  RepositoryPropertyDescriptor pPropertyDescriptor) {
		RepositoryItemDescriptor itemDescriptor = getPepositoryItemDescriptor(pPropertyDescriptor);
		initItemDescirptorInfo(pParentItemInfo, itemDescriptor, pPropertyDescriptor);
	}

	/**
	 * @param pPropertyDescriptor property descriptor
	 * @return return repository item descriptor from property descriptor
	 */
	private RepositoryItemDescriptor getPepositoryItemDescriptor(RepositoryPropertyDescriptor pPropertyDescriptor) {
		return pPropertyDescriptor.getPropertyItemDescriptor() == null ?
				pPropertyDescriptor.getComponentItemDescriptor() :
				pPropertyDescriptor.getPropertyItemDescriptor();
	}

	/**
	 * @param pParentItemInfo     item descriptor info, which tries to retrieve child
	 * @param pRepositoryName     repository name
	 * @param pItemDescriptorName item descriptor name
	 * @return broken sub item reason
	 */
	private String isBrokenSubItem(ItemDescriptorInfo pParentItemInfo,
								   String pRepositoryName,
								   String pItemDescriptorName) {
		if (hasItemDescriptorInfoInParent(pParentItemInfo, pItemDescriptorName)) {

			return BrokenItemDescriptorInfoReason.REASON_CYCLE_RELATIONSHIP;
		}
		if (!pParentItemInfo.getRepositoryName().equals(pRepositoryName)) {
			return BrokenItemDescriptorInfoReason.REASON_ANOTHER_REPOSITORY;
		}
		return null;
	}

	/**
	 * add property info
	 *
	 * @param pParentItemInfo     item descriptor info
	 * @param pPropertyDescriptor repository property descriptor
	 */
	private void addPropertyInfo(ItemDescriptorInfo pParentItemInfo,
								 RepositoryPropertyDescriptor pPropertyDescriptor) {
		PropertyInfo propertyInfo = initPropertyInfo(pPropertyDescriptor);
		propertyInfo.setFullPath(getPathProperty(pParentItemInfo,
				pPropertyDescriptor.getName()));
		pParentItemInfo.addPropery(propertyInfo);
	}


	/**
	 * init property info
	 *
	 * @param pPropertyDescriptor property descriptor
	 * @return property info
	 */
	private PropertyInfo initPropertyInfo(RepositoryPropertyDescriptor pPropertyDescriptor) {
		String descriptorName = null;
		if (pPropertyDescriptor.getItemDescriptor() != null) {
			descriptorName = getRepositoryName(pPropertyDescriptor.getItemDescriptor());
		}
		return new PropertyInfo(
				descriptorName,
				pPropertyDescriptor.getDisplayName(),
				pPropertyDescriptor.getName(),
				pPropertyDescriptor.getShortDescription(),
				pPropertyDescriptor.getPropertyType(),
				pPropertyDescriptor.getComponentPropertyType());
	}

	/**
	 * return full path for property
	 *
	 * @param pItemInfo     item descriptor info
	 * @param pPropertyName property name
	 * @return full path in repository tree
	 */
	private String getPathProperty(ItemDescriptorInfo pItemInfo,
								   String pPropertyName) {
		return getPathItemDescriptor(pItemInfo) + "." + pPropertyName;
	}

	/**
	 * return parents path
	 *
	 * @param pParentItemInfo item descriptor info
	 * @param pItemDescriptor item descriptor info
	 * @return full parents path
	 */
	private String getPathItemDescriptor(ItemDescriptorInfo pParentItemInfo, RepositoryItemDescriptor pItemDescriptor) {
		if (pParentItemInfo != null) {
			return pParentItemInfo.getFullPath() + "." + pItemDescriptor.getItemDescriptorName();
		}
		return pItemDescriptor.getItemDescriptorName();
	}

	/**
	 * return parents path
	 *
	 * @param pItemInfo item descriptor info
	 * @return full parents path
	 */
	private String getPathItemDescriptor(ItemDescriptorInfo pItemInfo) {
		StringBuilder pathHolder = new StringBuilder();
		initPathItemDescriptor(pathHolder, pItemInfo);
		return pathHolder.toString();
	}

	/**
	 * retrieve parents path
	 *
	 * @param pBuilder  string builder to contains path
	 * @param pItemInfo pItemDescriptor
	 */
	private void initPathItemDescriptor(StringBuilder pBuilder, ItemDescriptorInfo pItemInfo) {
		if (pItemInfo.getLevel() != 0) {
			// pBuilder.insert(0, "."+pItemInfo.getParentPropertyName());
			pBuilder.insert(0, "." + pItemInfo.getName());
			initPathItemDescriptor(pBuilder, pItemInfo.getParent());
		} else {
			pBuilder.insert(0, pItemInfo.getName());
		}
	}

	/**
	 * init new parent
	 *
	 * @param pItemInfo       item descriptor info
	 * @param pParentItemInfo parent item descriptor info
	 */
	private void initParent(ItemDescriptorInfo pItemInfo, ItemDescriptorInfo pParentItemInfo) {
		pItemInfo.setParent(pParentItemInfo);
		if (pParentItemInfo != null) {
			pItemInfo.setLevel(pParentItemInfo.getLevel() + 1);
			pParentItemInfo.addSubItemDescriptor(pItemInfo);
		} else {
			pItemInfo.setLevel(0);
		}
		initFullPath(pItemInfo);
	}

	/**
	 * reinit parents hierarchy
	 *
	 * @param pItemInfo child item descirptor info
	 */
	private void initFullPath(ItemDescriptorInfo pItemInfo) {
		pItemInfo.setFullPath(getPathItemDescriptor(pItemInfo));

		for (PropertyInfo propertyInfo : pItemInfo.getProperties()) {
			propertyInfo.setFullPath(getPathProperty(pItemInfo, propertyInfo.getName()));
		}
		for (ItemDescriptorInfo subItemDescriptorInfo : pItemInfo.getSubItemDescriptorInfos()) {
			subItemDescriptorInfo.setParent(pItemInfo);
			initFullPath(subItemDescriptorInfo);
		}
	}

	/**
	 * check if item is in parent list to prevent cycle relationships
	 *
	 * @param pDescriptorInfo               item descriptor info
	 * @param pPossibleParentDescriptorName item descriptor info name to check for parent relationship
	 * @return 2nd arg is parent for 1st arg
	 */
	private boolean hasItemDescriptorInfoInParent(ItemDescriptorInfo pDescriptorInfo,
												  String pPossibleParentDescriptorName) {
		if (pDescriptorInfo.getParent() == null) {
			return false;
		}
		if (pDescriptorInfo.getParent().getName().equals(pPossibleParentDescriptorName)) {
			return true;
		} else {
			return hasItemDescriptorInfoInParent(pDescriptorInfo.getParent(), pPossibleParentDescriptorName);
		}
	}

	/**
	 * @param pItemDescriptor repository item descriptor name
	 * @return repository name
	 */
	public String getRepositoryName(RepositoryItemDescriptor pItemDescriptor) {
		return ((GSARepository) pItemDescriptor.getRepository()).getInvalidationAbsoluteName();
	}

	/**
	 * reset state for all analyzed repository items
	 */
	public void resetState() {
		mDescriptorFullPathMap.clear();
		mDescriptorNameMap.clear();
	}
}
