package vsg.integration.export.repository;

/**
 * @author Dmitry Golubev
 */
public class RepositoryInfo {
	/**
	 * repository preview name (f.e. UserProfiles Repositor)
	 */
	private String mRepositoryPreviewName;
	/**
	 * repository name (f.e. /atg/userprofiling/ProfileAdapterRepository)
	 */
	private String mRepositoryName;
	/**
	 * supported item descriptor name (f.e. user)
	 */
	private String mItemDescriptorName;

	public RepositoryInfo(String pRepositoryPreviewName, String pRepositoryName, String pItemDescriptorName) {
		mRepositoryPreviewName = pRepositoryPreviewName;
		mRepositoryName = pRepositoryName;
		mItemDescriptorName = pItemDescriptorName;
	}

	public String getRepositoryPreviewName() {
		return mRepositoryPreviewName;
	}

	public void setRepositoryPreviewName(String pRepositoryPreviewName) {
		mRepositoryPreviewName = pRepositoryPreviewName;
	}

	public String getRepositoryName() {
		return mRepositoryName;
	}

	public void setRepositoryName(String pRepositoryName) {
		mRepositoryName = pRepositoryName;
	}

	public String getItemDescriptorName() {
		return mItemDescriptorName;
	}

	public void setItemDescriptorName(String pItemDescriptorName) {
		mItemDescriptorName = pItemDescriptorName;
	}

	public String getRepositoryItemDescriptorValue() {
		return getItemDescriptorName() + "|" + getRepositoryName();
	}
}
