package vsg.integration.export.repository.model;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class RepositoryModel {
	/**
	 * list of item descriptors for repository
	 */
	private List<ItemDescriptorInfo> mItemDescriptorInfos = new LinkedList<ItemDescriptorInfo>();

	//------------------------------------------------------------------------------------------------------------------

	public List<ItemDescriptorInfo> getItemDescriptorInfos() {
		return mItemDescriptorInfos;
	}

	public void setItemDescriptorInfos(List<ItemDescriptorInfo> pItemDescriptorInfos) {
		mItemDescriptorInfos = pItemDescriptorInfos;
	}

	/**
	 * add item descriptor to model
	 *
	 * @param pItemDescriptorInfo item descriptor
	 */
	public void addItemDescriptor(ItemDescriptorInfo pItemDescriptorInfo) {
		mItemDescriptorInfos.add(pItemDescriptorInfo);
	}

	/**
	 * get item descriptor info by name from model
	 *
	 * @param pItemDescriptorName item descriptor name
	 * @return item descriptor info
	 */
	public ItemDescriptorInfo getItemDescriptorInfoByName(String pItemDescriptorName) {
		ItemDescriptorInfo result = null;
		for (ItemDescriptorInfo itemDescriptorInfo : getItemDescriptorInfos()) {
			result = getItemDescriptorInfoByName(itemDescriptorInfo, pItemDescriptorName);
			if (result != null) {
				return result;
			}
		}
		return result;
	}

	/**
	 * get item descriptor info by full path from model
	 *
	 * @param pItemDescriptorFullPath item descriptor full path
	 * @return item descriptor info
	 */
	public ItemDescriptorInfo getItemDescriptorInfoByFullPath(String pItemDescriptorFullPath) {
		ItemDescriptorInfo result = null;
		for (ItemDescriptorInfo itemDescriptorInfo : getItemDescriptorInfos()) {
			result = getItemDescriptorInfoByFullPath(itemDescriptorInfo, pItemDescriptorFullPath);
			if (result != null) {
				return result;
			}
		}
		return result;
	}

	/**
	 * get item descriptor info checking by name with subItems analyze
	 *
	 * @param pItemDescriptorInfo item descriptor info to check
	 * @param pItemDescriptorName item descriptor name
	 * @return item descriptor object
	 */
	private ItemDescriptorInfo getItemDescriptorInfoByName(ItemDescriptorInfo pItemDescriptorInfo,
														   String pItemDescriptorName) {
		if (pItemDescriptorName.equals(pItemDescriptorInfo.getName())) {
			return pItemDescriptorInfo;
		}
		for (ItemDescriptorInfo subItemDescriptor : pItemDescriptorInfo.getSubItemDescriptorInfos()) {
			ItemDescriptorInfo result = getItemDescriptorInfoByName(subItemDescriptor, pItemDescriptorName);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	/**
	 * get item descriptor info checking by name with subItems analyze
	 *
	 * @param pItemDescriptorInfo     item descriptor info to check
	 * @param pItemDescriptorFullPath item descriptor full path
	 * @return item descriptor object
	 */
	private ItemDescriptorInfo getItemDescriptorInfoByFullPath(ItemDescriptorInfo pItemDescriptorInfo,
															   String pItemDescriptorFullPath) {
		if (pItemDescriptorFullPath.equals(pItemDescriptorInfo.getFullPath())) {
			return pItemDescriptorInfo;
		}
		for (ItemDescriptorInfo subItemDescriptor : pItemDescriptorInfo.getSubItemDescriptorInfos()) {
			ItemDescriptorInfo result = getItemDescriptorInfoByFullPath(subItemDescriptor, pItemDescriptorFullPath);
			if (result != null) {
				return result;
			}
		}
		return null;
	}
}
