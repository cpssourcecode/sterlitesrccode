package vsg.integration.export.repository.model;

/**
 * @author Dmitry Golubev
 */
public class PropertyInfo {
	/**
	 * repository name
	 */
	private String mRepositoryName;
	/**
	 * property display name
	 */
	private String mDisplayName;
	/**
	 * property name
	 */
	private String mName;
	/**
	 * property short description
	 */
	private String mShortDescription;
	/**
	 * property class
	 */
	private Class mType;
	/**
	 * component type
	 */
	private Class mComponentType;
	/**
	 * full path to property in repository tree
	 */
	private String mFullPath;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pRepositoryName   repository name
	 * @param pDisplayName      property display name
	 * @param pName             property name
	 * @param pShortDescription short description
	 * @param pType             property class
	 * @param pComponentType    component class
	 */
	public PropertyInfo(String pRepositoryName,
						String pDisplayName,
						String pName,
						String pShortDescription,
						Class pType,
						Class pComponentType) {
		mRepositoryName = pRepositoryName;
		mDisplayName = pDisplayName;
		mName = pName;
		mShortDescription = pShortDescription;
		mType = pType;
		mComponentType = pComponentType;
	}

	/**
	 * a Copy constructor
	 * @param pOriginal an original object.
	 */
	public PropertyInfo(PropertyInfo pOriginal) {
		// copy primitives and immutables
		mRepositoryName = pOriginal.mRepositoryName;
		mDisplayName = pOriginal.mDisplayName;
		mName = pOriginal.mName;
		mShortDescription = pOriginal.mShortDescription;
		mType = pOriginal.mType;
		mComponentType = pOriginal.mComponentType;
		mFullPath = pOriginal.mFullPath;
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getRepositoryName() {
		return mRepositoryName;
	}

	public void setRepositoryName(String pRepositoryName) {
		mRepositoryName = pRepositoryName;
	}

	public String getDisplayName() {
		return mDisplayName;
	}

	public void setDisplayName(String pDisplayName) {
		mDisplayName = pDisplayName;
	}

	public String getName() {
		return mName;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public String getShortDescription() {
		return mShortDescription;
	}

	public void setShortDescription(String pShortDescription) {
		mShortDescription = pShortDescription;
	}

	public Class getType() {
		return mType;
	}

	public void setType(Class pType) {
		mType = pType;
	}

	public Class getComponentType() {
		return mComponentType;
	}

	public void setComponentType(Class pComponentType) {
		mComponentType = pComponentType;
	}

	public String getFullPath() {
		return mFullPath;
	}

	public void setFullPath(String pFullPath) {
		mFullPath = pFullPath;
	}

	@Override
	public String toString() {
		return getFullPath();
	}
}
