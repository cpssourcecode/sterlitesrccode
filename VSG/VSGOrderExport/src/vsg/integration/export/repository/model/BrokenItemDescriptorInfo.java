package vsg.integration.export.repository.model;

/**
 * @author Dmitry Golubev
 */
public class BrokenItemDescriptorInfo implements BrokenItemDescriptorInfoReason {
	/**
	 * repository name
	 */
	private String mRepositoryName;
	/**
	 * item descriptor name
	 */
	private String mName;
	/**
	 * preview name
	 */
	private String mPreviewName;
	/**
	 * reason
	 */
	private String mReason;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pName           name
	 * @param pRepositoryName repository
	 * @param pPreviewName    preview name
	 * @param pReason         reason
	 */
	public BrokenItemDescriptorInfo(String pName, String pRepositoryName, String pPreviewName, String pReason) {
		mName = pName;
		mRepositoryName = pRepositoryName;
		mPreviewName = pPreviewName;
		mReason = pReason;
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getRepositoryName() {
		return mRepositoryName;
	}

	public String getName() {
		return mName;
	}

	public String getPreviewName() {
		return mPreviewName;
	}

	public String getReason() {
		return mReason;
	}
}
