package vsg.integration.export.repository.model;

/**
 * @author Dmitry Golubev
 */
public interface BrokenItemDescriptorInfoReason {
	/**
	 * another repository reason
	 */
	String REASON_ANOTHER_REPOSITORY = "Another repository.";
	/**
	 * cycle relationship reason
	 */
	String REASON_CYCLE_RELATIONSHIP = "Cycle relationships.";
}
