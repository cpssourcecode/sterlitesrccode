package vsg.integration.export.repository.model;

import vsg.integration.export.util.comparator.ComparatorItemDescriptorInfo;
import vsg.integration.export.util.comparator.ComparatorPropertyInfo;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Dmitry Golubev
 */
public class ItemDescriptorInfo {
	/**
	 * repository name
	 */
	private String mRepositoryName;
	/**
	 * item descriptor name
	 */
	private String mName;
	/**
	 * item descriptor preview name
	 */
	private String mPreviewName;
	/**
	 * property class
	 */
	private Class mPropertyType;
	/**
	 * component propery type (if property is instanse of collection)
	 */
	private Class mComponentPropertyType;
	/**
	 * parent
	 */
	private ItemDescriptorInfo mParent;
	/**
	 * item descriptor level in repository tree
	 */
	private int mLevel;
	/**
	 * full path to property in repository tree
	 */
	private String mFullPath;
	/**
	 * list of properties
	 */
	private Set<PropertyInfo> mProperties = new TreeSet<PropertyInfo>(
			ComparatorPropertyInfo.instanse()
	);
	/**
	 * list of sub item descriptors
	 */
	private Set<ItemDescriptorInfo> mSubItemDescriptorInfos = new TreeSet<ItemDescriptorInfo>(
			ComparatorItemDescriptorInfo.instanse()
	);

	/**
	 * list of broken sub items:
	 * - another repository
	 * - cycle relationshipt
	 */
	private List<BrokenItemDescriptorInfo> mBrokenItemDescriptorInfos = new LinkedList<BrokenItemDescriptorInfo>();

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor with item descriptor name
	 *
	 * @param pRepositoryName        repository name
	 * @param pName                  item descriptor name
	 * @param pPreviewName           preview name
	 * @param pPropertyType          property type
	 * @param pComponentPropertyType component property type
	 */
	public ItemDescriptorInfo(String pRepositoryName, String pName, String pPreviewName,
							  Class pPropertyType, Class pComponentPropertyType) {
		mRepositoryName = pRepositoryName;
		mName = pName;
		mPreviewName = pPreviewName;
		mPropertyType = pPropertyType;
		mComponentPropertyType = pComponentPropertyType;
	}

	/**
	 * Copy constructor
	 * @param pOriginal an original object
	 */
	public ItemDescriptorInfo(ItemDescriptorInfo pOriginal) {
		// copy primitives and immutables.
		mRepositoryName = pOriginal.mRepositoryName;
		mName = pOriginal.mName;
		mPreviewName = pOriginal.mPreviewName;
		mPropertyType = pOriginal.mPropertyType;
		mComponentPropertyType = pOriginal.mComponentPropertyType;
		mLevel = pOriginal.mLevel;
		mFullPath = pOriginal.mFullPath;

		// deep copy
		mProperties = copyProperties(pOriginal.getProperties());
		mSubItemDescriptorInfos = copySubItemDescriptors(pOriginal.mSubItemDescriptorInfos);

		// copy reference only
		mParent = pOriginal.mParent;
		mBrokenItemDescriptorInfos = pOriginal.mBrokenItemDescriptorInfos;
	}

	private Set<PropertyInfo> copyProperties(Set<PropertyInfo> pOriginal){
		if (pOriginal == null){
			return null;
		}
		Set<PropertyInfo> result = new TreeSet<PropertyInfo>(ComparatorPropertyInfo.instanse());

		for (PropertyInfo propertyInfo : pOriginal) {
			result.add(new PropertyInfo(propertyInfo));
		}

		return result;
	}

	private Set<ItemDescriptorInfo> copySubItemDescriptors(Set<ItemDescriptorInfo> pOriginal){
		if (pOriginal == null){
			return null;
		}
		Set<ItemDescriptorInfo> result = new TreeSet<>(ComparatorItemDescriptorInfo.instanse());

		for (ItemDescriptorInfo subItemDescriptorInfo : pOriginal) {
			result.add(new ItemDescriptorInfo(subItemDescriptorInfo));
		}

		return result;
	}

	//------------------------------------------------------------------------------------------------------------------

	public String getRepositoryName() {
		return mRepositoryName;
	}

	public void setRepositoryName(String pRepositoryName) {
		mRepositoryName = pRepositoryName;
	}

	public String getName() {
		return mName;
	}

	public void setName(String pName) {
		mName = pName;
	}

	public Class getPropertyType() {
		return mPropertyType;
	}

	public void setPropertyType(Class pPropertyType) {
		mPropertyType = pPropertyType;
	}

	public Class getComponentPropertyType() {
		return mComponentPropertyType;
	}

	public void setComponentPropertyType(Class pComponentPropertyType) {
		mComponentPropertyType = pComponentPropertyType;
	}

	public Set<PropertyInfo> getProperties() {
		return mProperties;
	}

	public void setProperties(Set<PropertyInfo> pProperties) {
		mProperties = pProperties;
	}

	/**
	 * add new property
	 *
	 * @param pPropertyInfo property
	 */
	public void addPropery(PropertyInfo pPropertyInfo) {
		mProperties.add(pPropertyInfo);
	}

	public Set<ItemDescriptorInfo> getSubItemDescriptorInfos() {
		return mSubItemDescriptorInfos;
	}

	public void setSubItemDescriptorInfos(Set<ItemDescriptorInfo> pSubItemDescriptorInfos) {
		mSubItemDescriptorInfos = pSubItemDescriptorInfos;
	}

	/**
	 * add sub item descriptor
	 *
	 * @param pSubItemDescriptorInfo sub item descriptor
	 */
	public void addSubItemDescriptor(ItemDescriptorInfo pSubItemDescriptorInfo) {
		mSubItemDescriptorInfos.add(pSubItemDescriptorInfo);
	}

	public ItemDescriptorInfo getParent() {
		return mParent;
	}

	public void setParent(ItemDescriptorInfo pParent) {
		mParent = pParent;
	}

	public int getLevel() {
		return mLevel;
	}

	public void setLevel(int pLevel) {
		mLevel = pLevel;
	}

	public String getFullPath() {
		return mFullPath;
	}

	public void setFullPath(String pFullPath) {
		mFullPath = pFullPath;
	}

	public String getPreviewName() {
		return mPreviewName;
	}

	public void setPreviewName(String pPreviewName) {
		mPreviewName = pPreviewName;
	}

	public List<BrokenItemDescriptorInfo> getBrokenItemDescriptorInfos() {
		return mBrokenItemDescriptorInfos;
	}

	public void setBrokenItemDescriptorInfos(List<BrokenItemDescriptorInfo> pBrokenItemDescriptorInfos) {
		mBrokenItemDescriptorInfos = pBrokenItemDescriptorInfos;
	}

	/**
	 * add broken item descriptor info
	 *
	 * @param pBrokenItemDescriptorInfo sub item descriptor
	 */
	public void addBrokenItemDescriptorInfo(BrokenItemDescriptorInfo pBrokenItemDescriptorInfo) {
		mBrokenItemDescriptorInfos.add(pBrokenItemDescriptorInfo);
	}

	@Override
	public String toString() {
		return getFullPath();
	}
}
