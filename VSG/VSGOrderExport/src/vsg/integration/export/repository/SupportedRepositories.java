package vsg.integration.export.repository;

import atg.adapter.gsa.GSARepository;
import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class SupportedRepositories extends GenericService {
	/**
	 * list of supported repositories
	 */
	private List<Repository> mRepositories = new ArrayList<Repository>();
	/**
	 * repositories item descriptors for rql execute
	 */
	private Map<Repository, String[]> mRepositoriesViews = new HashMap<Repository, String[]>();
	/**
	 * repositories default colums to execute
	 */
	private Map<String, String[]> mViewDefaultColumns = new HashMap<String, String[]>();
	/**
	 * repositories default colums titles to execute
	 */
	private Map<String, String[]> mViewDefaultColumnsTitles = new HashMap<String, String[]>();

	//------------------------------------------------------------------------------------------------------------------

	public List<Repository> getRepositories() {
		return mRepositories;
	}

	public void setRepositories(List<String> pRepositories) {
		mRepositories.clear();
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		for (String repositoryName : pRepositories) {
			Repository repository = (Repository) nucleus.resolveName(repositoryName);
			mRepositories.add(repository);
		}
	}

	public Map<Repository, String[]> getRepositoriesViews() {
		return mRepositoriesViews;
	}

	public void setRepositoriesViews(Map<String, String> pRepositoriesViews) {
		mRepositoriesViews.clear();
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		for (String repositoryName : pRepositoriesViews.keySet()) {
			Repository repository = (Repository) nucleus.resolveName(repositoryName);
			mRepositoriesViews.put(repository, pRepositoriesViews.get(repositoryName).split("\\|"));
		}
	}

	/**
	 * @param pRepositoryName repository name
	 * @return repository default view
	 */
	public String[] getRepositoryDefaultView(String pRepositoryName) {
		for (Repository repository : getRepositoriesViews().keySet()) {
			if ((((GSARepository) repository).getInvalidationAbsoluteName()).equals(pRepositoryName)) {
				return getRepositoriesViews().get(repository);
			}
		}
		return null;
	}

	public Map<String, String[]> getViewDefaultColumns() {
		return mViewDefaultColumns;
	}

	public void setViewDefaultColumns(Map<String, String> pViewDefaultColumns) {
		mViewDefaultColumns.clear();
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		for (String viewName : pViewDefaultColumns.keySet()) {
			String columns = pViewDefaultColumns.get(viewName);
			if (columns != null) {
				mViewDefaultColumns.put(viewName, columns.split("\\|"));
			}
		}
	}

	public Map<String, String[]> getViewDefaultColumnsTitles() {
		return mViewDefaultColumnsTitles;
	}

	public void setViewDefaultColumnsTitles(Map<String, String> pViewDefaultColumnsTitles) {
		mViewDefaultColumnsTitles.clear();
		for (String viewName : pViewDefaultColumnsTitles.keySet()) {
			String columns = pViewDefaultColumnsTitles.get(viewName);
			if (columns != null) {
				mViewDefaultColumnsTitles.put(viewName, columns.split("\\|"));
			}
		}
	}

	/**
	 * @param pItemDescriptor item descriptor name
	 * @return repository deault columns
	 */
	public String[] getDefaultColumns(String pItemDescriptor) {
		return getViewDefaultColumns().get(pItemDescriptor);
	}

	/**
	 * @param pItemDescriptor item descriptor name
	 * @return repository deault columns titles
	 */
	public String[] getDefaultColumnsTitles(String pItemDescriptor) {
		return getViewDefaultColumnsTitles().get(pItemDescriptor);
	}

	/**
	 * @param pRepository repository
	 * @return repository deault view
	 */
	public String[] getDefaultView(Repository pRepository) {
		return getRepositoriesViews().get(pRepository);
	}

	/**
	 * @return supported repositories item-descriptors info
	 */
	public List<RepositoryInfo> getRepositoriesSelectionInfo() {
		List<RepositoryInfo> repositoriesSelectionInfo = new ArrayList<RepositoryInfo>();

		for (Repository repository : getRepositoriesViews().keySet()) {
			String[] supportedViews = getRepositoriesViews().get(repository);
			for (String supportedView : supportedViews) {
				repositoriesSelectionInfo.add(
						new RepositoryInfo(
								repository.getRepositoryName(),
								((GSARepository) repository).getInvalidationAbsoluteName(),
								supportedView
						)
				);
			}
		}

		return repositoriesSelectionInfo;
	}
}
