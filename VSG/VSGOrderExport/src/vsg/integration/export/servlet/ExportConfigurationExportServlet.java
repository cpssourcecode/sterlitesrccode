package vsg.integration.export.servlet;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import vsg.integration.export.store.ExportConfigExporter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Dmitry Golubev
 */
public class ExportConfigurationExportServlet extends HttpServlet {

	private ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(ExportConfigurationExportServlet.class);

	/**
	 * export config exporter
	 */
	ExportConfigExporter mExportConfigExporter;

	//------------------------------------------------------------------------------------------------------------------

	public ExportConfigExporter getExportConfigExporter() {
		return mExportConfigExporter;
	}

	public void setExportConfigExporter(ExportConfigExporter pExportConfigExporter) {
		mExportConfigExporter = pExportConfigExporter;
	}

	//------------------------------------------------------------------------------------------------------------------

	public void service(HttpServletRequest pRequest, HttpServletResponse pResponse) throws ServletException,
			IOException {
		String fileName = getExportConfigExporter().process(pRequest.getParameter("configId"));
		formExportResponse(pResponse, fileName);
	}

	/**
	 * forms response to upload file
	 *
	 * @param pResponse       http response
	 * @param pExportFileName generated export file name
	 */
	private void formExportResponse(HttpServletResponse pResponse, String pExportFileName) {
		pResponse.setHeader("Content-disposition", "inline;filename=ExportConfiguration.xml");
		pResponse.setContentType("application/xml");
		try {
			OutputStream out = pResponse.getOutputStream();
			try (FileInputStream in = new FileInputStream(pExportFileName);) {// !!!
				byte[] buffer = new byte[4096];
				int length;
				while ((length = in.read(buffer)) > 0) {
					out.write(buffer, 0, length);
				}
				in.close();
				out.flush();
			}
		} catch (IOException e) {
			mLogging.logError(e);
		}
	}

}
