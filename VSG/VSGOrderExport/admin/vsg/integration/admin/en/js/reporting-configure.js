$(document).ready(function () {

    if (!$('#ftpDelivery').is(":checked")) {
        $('#ftpConfig').hide();
    }
    $('#ftpDelivery').change(function () {
        if (this.checked) {
            $('#ftpConfig').fadeIn('slow');
        } else {
            $('#ftpConfig').fadeOut('slow');
        }
    });

    if (!$('#mailDelivery').is(":checked")) {
        $('#emailConfig').hide();
    }
    $('#mailDelivery').change(function () {
        if (this.checked) {
            $('#emailConfig').fadeIn('slow');
        } else {
            $('#emailConfig').fadeOut('slow');
        }
    });
    selectProtocol();
});

function selectProtocol() {
    if ($("#selProt_input").val() == 3) {
        $("#encPhraseRow").removeClass("hidden");
        $("#encKeyFilePathRow").removeClass("hidden");
        $("#passRow").addClass("hidden");
        $("#confPassRow").addClass("hidden");
        $("#pass_input").val("");
        $("#confPass_input").val("");
    } else {
        $("#encPhraseRow").addClass("hidden");
        $("#encKeyFilePathRow").addClass("hidden");
        $("#passRow").removeClass("hidden");
        $("#confPassRow").removeClass("hidden");
        $("#encPhrase_input").val("");
        $("#encKeyFilePath_input").val("");
    }
}

function copyValues() {
    if ($('#mailDelivery').is(":checked") == true) {
        $("#useEmail_update").val("true");
        $("#useEmail_input").val("true");
    } else {
        $("#useEmail_update").val("false");
        $("#useEmail_input").val("false");
    }

    if ($('#ftpDelivery').is(":checked") == true) {
        $("#useFtp_update").val("true");
        $("#useFtp_input").val("true");
    } else {
        $("#useFtp_update").val("false");
        $("#useFtp_input").val("false");
    }
}

function verifyReportingConfig(formId) {
    hideErrorMsg();
    blockUI();

	$("#enableSuccess_update").val($("#enableSuccess_input").is(":checked"));
	$("#enableError_update").val($("#enableError_input").is(":checked"));

    $("#sfrequency_update").val($("#sfrequency_input").val().trim());
    $("#efrequency_update").val($("#efrequency_input").val().trim());
    $("#format_update").val($("#format_input").val().trim());
    $("#fileName_update").val($("#fileName_input").val().trim());
    $("#stampFormat_update").val($("#stampFormat_input").val().trim());
    $("#outLocDir_update").val($("#outLocDir_input").val().trim());
    $("#archLocDir_update").val($("#archLocDir_input").val().trim());

    $("#emailSubj_update").val($("#emailSubj_input").val().trim());
    $("#emailFrom_update").val($("#emailFrom_input").val().trim());
    $("#emailTo_update").val($("#emailTo_input").val().trim());

    $("#selProt_update").val($("#selProt_input").val().trim());
    $("#url_update").val($("#url_input").val().trim());
    $("#port_update").val($("#port_input").val().trim());
    $("#outDir_update").val($("#outDir_input").val().trim());
    $("#name_update").val($("#name_input").val().trim());
    $("#pass_update").val($("#pass_input").val().trim());
    $("#confPass_update").val($("#confPass_input").val().trim());
    $("#uploadKeyFilePath_update").val($("#uploadKeyFilePath_input").val().trim());
    $("#encPhrase_update").val($("#encPhrase_input").val().trim());

    var options = {
        success: function (data) {
            unBlockUI();
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else if ('success' == data.code) {
                uploadFile(formId);
            }
        },
        error: function (data) {
            unBlockUI();
            showRequestError();
        },
        dataType: 'json'
    };

    var form = $("#verifyOutputReportConfigForm");
    form.ajaxForm(options);
    form.submit();

}

function createOutputReportConfig(formId) {
    hideErrorMsg();

    $("#sfrequency_update").val($("#sfrequency_input").val().trim());
    $("#efrequency_update").val($("#efrequency_input").val().trim());
    $("#format_update").val($("#format_input").val().trim());
    $("#fileName_update").val($("#fileName_input").val().trim());
    $("#stampFormat_update").val($("#stampFormat_input").val().trim());
    $("#outLocDir_update").val($("#outLocDir_input").val().trim());
    $("#archLocDir_update").val($("#archLocDir_input").val().trim());

    $("#useEmail_update").val($("#useEmail_input").val().trim());
    $("#emailSubj_update").val($("#emailSubj_input").val().trim());
/*
    $("#emailFrom_update").val($("#emailFrom_input").val().trim());
*/
    $("#emailTo_update").val($("#emailTo_input").val().trim());

    $("#useFtp_update").val($("#useFtp_input").val().trim());
    $("#selProt_update").val($("#selProt_input").val().trim());
    $("#url_update").val($("#url_input").val().trim());
    $("#port_update").val($("#port_input").val().trim());
    $("#outDir_update").val($("#outDir_input").val().trim());
    $("#name_update").val($("#name_input").val().trim());
    $("#pass_update").val($("#pass_input").val().trim());
    $("#confPass_update").val($("#confPass_input").val().trim());
    $("#uploadKeyFilePath_update").val($("#uploadKeyFilePath_input").val().trim());
    $("#encPhrase_update").val($("#encPhrase_input").val().trim());

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else if ('success' == data.code) {
                location.href = '../../reporting-review-settings.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };

    var form = $("#createOutputReportConfig");
    form.ajaxForm(options);
    form.submit();

}

function submitForm(formId, redirectUrl) {
    hideErrorMsg();

    var options = {
        success: function (data) {
            console.log('success');
            successResponse(data, redirectUrl);
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    hideErrorMsg();
    var form = $("#" + formId);
    form.ajaxForm(options);
    form.submit();
    return false;
}

function successResponse(data, redirectUrl, modalMessage) {
    if ('error' == data.code) {
        showErrorMsg(data.all_errors);
        window.scrollTo(0, 0);
    } else if ('success' == data.code) {
        if (redirectUrl != null) {
            location.href = redirectUrl;
        } else {
            console.log(data);
            if (modalMessage != null) {
                $("#modalContent").html(modalMessage);
                window.location.hash = '#openModal';
            }
        }
    }
}

function testFtp() {
    hideErrorMsg();

    $("#testFtpBut").hide();
    $("#ftpLoader").show();
    var successMessage = "Test FTP call was succesfull";
    var options = {
        success: function (data) {
            successResponse(data, null, successMessage);
            $("#ftpLoader").hide();
            $("#testFtpBut").show();
        },
        error: function (data) {
            showRequestError();
            $("#ftpLoader").hide();
            $("#testFtpBut").show();
        },
        dataType: 'json'
    };
    var form = $("#testFtpCall");
    form.ajaxForm(options);
    form.submit();
    return false;
}

function testEmail() {
    hideErrorMsg();

    $("#testEmailBut").hide();
    $("#emailLoader").show();
    var successMessage = "Test Email was sent succesfully";
    var options = {
        success: function (data) {
            successResponse(data, null, successMessage);
            $("#emailLoader").hide();
            $("#testEmailBut").show();
        },
        error: function (data) {
            showRequestError();
            $("#emailLoader").hide();
            $("#testEmailBut").show();
        },
        dataType: 'json'
    };
    var form = $("#testEmailCall");
    form.ajaxForm(options);
    form.submit();
    return false;
}

function generateReport(item, type) {
    hideErrorMsg();

    var configItemId = item.getAttribute("itemId");
    var repFormat = item.getAttribute("repFormat");
    var uri = 'gadgets/generateReport.jhtml?exportConfigId=' + configItemId + "&repFormat=" + repFormat + "&repType=" + type;
    window.open(uri, 'Download');
}
