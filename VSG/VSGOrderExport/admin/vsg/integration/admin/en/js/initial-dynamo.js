window.onload = function () {
    var expConfigTableRows = document.getElementById("export-config-table").getElementsByTagName("tr");
    for (var index = 0; index < expConfigTableRows.length; index++) {
        if (index % 2 == 1) {
            expConfigTableRows[index].className = 'evenrow';
        }
    }
};

function useExistingConfig(item) {
    hideErrorMsg();

    $("#repositoryId").val(item.getAttribute("itemId"));
    $("#repositoryDateCreate").val(item.getAttribute("dateCreate"));
    $("#repositoryWsdlUri").val(item.getAttribute("wsdlUri"));

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else {
                location.href = 'initial-configurator.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#useExistingFormat");
    form.ajaxForm(options);
    form.submit();
    return false;
}

function createExportConfig() {
    hideErrorMsg();
    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else {
                location.href = 'initial-configurator.jhtml?createMsg=true';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#createModel");
    form.ajaxForm(options);
    form.submit();
    return false;
}

function importExportConfig() {
    var uploadFileInput = $("#uploadFileInput");
    uploadFileInput.change(function () {
        $("#uploadFileSubmit").click();
    });
    uploadFileInput.click();
}

function createOutputFormatConfig() {
    hideErrorMsg();
    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg();
            } else {
                location.href = 'output-configure.jhtml?createMsg=true';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#createModel");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function createOutputReportingConfig() {
    hideErrorMsg();
    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg();
            } else {
                location.href = 'reporting-configure.jhtml?createMsg=true';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#createModel");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function deleteExportConfig(item) {
    hideErrorMsg();
    $("#deleteId").val(item.getAttribute("itemId"));

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg();
            } else {
                location.href = 'initial-dynamo.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#deleteModel");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function storeExportConfig(item) {
    hideErrorMsg();
    $("#configId").val(item.getAttribute("itemId"));

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg();
            } else {
                location.href = 'initial-dynamo.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#storeConfig");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function storeExportConfigNew(item) {
    var configItemId = item.getAttribute("itemId");
    var uri = 'gadgets/exportConfig-export.jhtml?exportConfigId=' + configItemId;
    window.open(uri, 'Download');
}

function get_full_url(url_path) {
    var loc = window.location;
    return loc.protocol + "//" + loc.host + url_path;
}
