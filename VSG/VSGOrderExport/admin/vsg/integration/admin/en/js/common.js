// browser things
$.browser.chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());

$(document).ajaxStop($.unblockUI);

if ($.browser.msie) {
    $('body').addClass('ie');
    if ($.browser.version == "6.0") {
        $('body').addClass('ie6');
    }
    if ($.browser.version == "7.0") {
        $('body').addClass('ie7');
    }
} else if ($.browser.mozilla) {
    $('body').addClass('ff');
} else if ($.browser.webkit) {
    $('body').addClass('wk');
    if ($.browser.chrome) {
        $('body').addClass('chrome');
    }
}

function getName(str, linkId, propertyId) {
    //$("#" + propertyId).val(str);
    if (str.lastIndexOf('\\')) {
        var i = str.lastIndexOf('\\') + 1;
    } else {
        var i = str.lastIndexOf('/') + 1;
    }
    var filename = str.slice(i);
//	var filename = str;
    $("#" + propertyId).val(filename);
    $("#" + propertyId + "_input").val(filename);
    var uploadedLink = document.getElementById(linkId);
    uploadedLink.innerHTML = filename;
}

function hideMessages() {
    hideErrorMsg();
    hideSuccessMsg();
}

function showErrorMsg(message) {
    $("#msgErrorText").html(message);
    $("#msgError").show();
}

function hideErrorMsg() {
    $("#msgError").hide();
}

function hideSuccessMsg() {
    $("#msgSuccess").hide();
}

function showSuccessMsg(message) {
    $("#msgSuccessText").html(message);
    $("#msgSuccess").show();
}

function uploadFile(formId) {
    $("#" + formId).click();
}

function blockUI() {
    $.blockUI({message: $('#domMessage')});
}

function unBlockUI() {
    $.unblockUI();
}

function showRequestError() {
    showErrorMsg("Error in request processing.");
}