function saveInitialConfigurator() {
    hideErrorMsg();

    /*
     $("#configDescription").val( $("textarea[name='configDescription']").val() )
     */

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else {
                location.href = 'output-configure.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#initialConfigurator");
    form.ajaxForm(options);
    form.submit();
    return false;

}