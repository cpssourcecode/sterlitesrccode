function headerItemsInit(numberOfItemsToHighlight) {
    // $("div.menu li").hide();
    var lisFromDiv = $("div.menu").find("li");
    var max = lisFromDiv.length > numberOfItemsToHighlight ? numberOfItemsToHighlight : lisFromDiv.length;
    for (var i = 0; i < max; i++) {
        lisFromDiv[i].style.fontWeight = "bold";
        lisFromDiv[i].style.display = "block";
    }
}