function reloadTestRQLResult() {
    $.ajax({
        url: "gadgets/selectquery/results.jhtml?show=true",
        cache: false,
        dataType: "html",
        success: function (data) {
            $("#selectquery-results").html(data);
        },
        error: function (data) {
            showRequestError();
        }
    });
}

function queryTest() {
    hideErrorMsg();

    blockUI();

    $("#testRQL").removeAttr("disabled");
    $("#saveRQL").attr("disabled", "true");
    $("#rql").val($("textarea[name='rql']").val())

    var options = {
        success: function (data) {
            unBlockUI();
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
                $("#selectquery-results").html("");
            } else {
                reloadTestRQLResult();
            }
        },
        error: function () {
            unBlockUI();
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#selectquery");
    form.ajaxForm(options);
    form.submit();
    return false;
}

function queryNext() {
    hideErrorMsg();

    $("#saveRQL").removeAttr("disabled");
    $("#testRQL").attr("disabled", "true");
    $("#value_markAsExported").val($("#markAsExported").is(":checked"));

    $("#rql").val($("textarea[name='rql']").val());

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else {
                location.href = 'output-property-mapping.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#selectquery");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function checkRepository(checkbox) {
    $("input[name='repository']").attr("checked", false);
    checkbox.checked = true;

    var repository = checkbox.value;

    $("#repositoryName").val(repository);

    /*
     var modelDiv = $("#model-descriptors");

     $.ajax({
     url: "gadgets/propertymapping/repository/repository-model.jhtml",
     cache: false,
     dataType: "html",
     data: {
     "repository": repository
     },
     success: function (data) {
     modelDiv.html(data);
     modelDiv.show();
     $("[name*='attribute.']").val("");
     },
     error: function (data) {
     alert('Error in response');
     }
     });
     */
}