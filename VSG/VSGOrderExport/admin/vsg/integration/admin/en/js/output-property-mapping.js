function openDescriptors(link) {
    var id = link.getAttribute("itemId");
    var path = link.getAttribute("itemPath");
    var itemActive = link.getAttribute("itemActive");
    var tableDiv = $("div[itemtable='" + path + "']");
    var display = tableDiv.css('display');

    if (display == 'none') {
        $.ajax({
            url: "gadgets/propertymapping/repository/itemdescriptor-info.jhtml",
            cache: false,
            dataType: "html",
            data: {
                "itemDescriptorFullPath": path,
                "itemActive": itemActive,
                "repository": $("input[name='repository']:checked").attr('repository')
            },
            success: function (data) {
                tableDiv.html(data);
                tableDiv.show();
            },
            error: function (data) {
                showRequestError();
            }
        });
    } else {
        tableDiv.hide();
    }
}

function openComplexType(link) {

    blockUI();

    var messageName = link.getAttribute("messageName");
    var typeFullPath = link.getAttribute("typeFullPath");
    var wsdl = link.getAttribute("wsdl");
    var wsdlUpload = link.getAttribute("wsdlUpload");
    var inout = link.getAttribute("inout");

    var tableDiv = $("div[typeFullPath='" + typeFullPath + "'][inout='" + inout + "']");
    var display = tableDiv.css('display');

    if (display == 'none') {
        if (tableDiv.text().trim().length > 0) {
            tableDiv.show();
            unBlockUI();
        } else {
            $.ajax({
                url: "gadgets/propertymapping/wsdl/type-complex.jhtml",
                cache: false,
                dataType: "html",
                data: {
                    "messageName": messageName,
                    "typeFullPath": typeFullPath,
                    "wsdl": wsdl,
                    "wsdlUpload": wsdlUpload,
                    "inout": inout
                },
                success: function (data) {
                    unBlockUI();
                    tableDiv.html(data);
                    tableDiv.show();
                },
                error: function (data) {
                    unBlockUI();
                    showRequestError();
                }
            });
        }
    } else {
        unBlockUI();
        tableDiv.hide();
    }
}

function openOperation(link) {
    var wsdlOperations = $("input[name='wsdlOperation']");
    var operationId = link.getAttribute("operationId");
    wsdlOperations.each(function () {
        if (this != link) {
            this.checked = false;
            $("input[name*='type." + operationId + ".']").val('');
            $("input[name*='type.fullPath." + operationId + ".']").val('');
            $("input[name*='type.staticValue." + operationId + ".']").val('');
            $("img[name*='type.clear." + operationId + ".']").hide();
            $("img[name*='type.edit." + operationId + ".']").hide();
        }
    });
    var tableDiv = $("div[id='" + operationId + "']");
    if (link.checked) {
        wsdlOperations.each(function () {
                var operationId = $(this).attr("operationId");
                $("div[id='" + operationId + "']").hide();
            }
        );
        $("#wsdlOperationAction").val(link.getAttribute("operationAction"));
        tableDiv.show();
    } else {
        tableDiv.hide();
    }
}

function checkRepository(checkbox) {
    $("input[name='repository']").attr("checked", false);
    checkbox.checked = true;

    var repository = checkbox.value;

    var modelDiv = $("#model-descriptors");

    $.ajax({
        url: "gadgets/propertymapping/repository/repository-model.jhtml",
        cache: false,
        dataType: "html",
        data: {
            "repository": repository
        },
        success: function (data) {
            modelDiv.html(data);
            modelDiv.show();
            $("[name*='attribute.']").val("");
        },
        error: function (data) {
            showRequestError();
        }
    });
}

var currentItemDescriptorPropery = null;
var currentItemDescriptorProperyRepository = null;

function selectItemDescriptorProperty(tdProperty) {
    var fullPath = $(tdProperty).attr("fullPath");
    var repositoryName = $(tdProperty).attr("repositoryName");

    if (fullPath == currentItemDescriptorPropery && currentItemDescriptorProperyRepository == repositoryName) {
        currentItemDescriptorPropery = null;
        currentItemDescriptorProperyRepository = null;
        $(tdProperty).css('background-color', '');
    } else {
        $("[name='item-descriptor-property']").css('background-color', '');
        $(tdProperty).css('background-color', '#E0ECF8');

        currentItemDescriptorPropery = fullPath;
        currentItemDescriptorProperyRepository = repositoryName;
    }
}

function selectModelInput(input) {
    if (currentItemDescriptorPropery != null && typeof currentItemDescriptorPropery != "undefined") {
        input.value = currentItemDescriptorPropery;

        var wsdlPath = input.getAttribute("name");
        var inout = input.getAttribute("inout");

        var length = inout.length + 1 + 5;
        // type. - 5

        wsdlPath = wsdlPath.substr(length, wsdlPath.length);
        $("img[name='" + inout + ".type.clear." + wsdlPath + "']").show();
        $("img[name='" + inout + ".type.edit." + wsdlPath + "']").hide();
        $("input[name='" + inout + ".type.fullPath." + wsdlPath + "']").val(currentItemDescriptorProperyRepository + ":" + currentItemDescriptorPropery);
        $("input[name='" + inout + ".type.staticValue." + wsdlPath + "']").val('');
    }
}

function openAllowedValues(href, id) {
    if (openDiv(id)) {
        href.innerHTML = '&ndash; Allowed values';
    } else {
        href.innerHTML = '+ Allowed values';
    }
}

function openDiv(id) {
    var display = document.getElementById(id).style.display;
    if (display == 'block') {
        document.getElementById(id).style.display = 'none';
        return false;
    } else {
        document.getElementById(id).style.display = 'block';
        return true;
    }
}

function saveSelection() {
    hideErrorMsg();

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else {
                location.href = 'output-review-settings.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#propertymapping");
    form.ajaxForm(options);
    form.submit();
    return false;
}

function clearParameterMapping(link) {
    var wsdlPath = link.getAttribute("name");
    var inout = link.getAttribute("inout");

    // type.clear. - 11
    var length = inout.length + 1 + 11;

    wsdlPath = wsdlPath.substr(length, wsdlPath.length);
    $("input[name='" + inout + ".type." + wsdlPath + "']").val('');
    $("img[name='" + inout + ".type.edit." + wsdlPath + "']").hide();
    $("input[name='" + inout + ".type.fullPath." + wsdlPath + "']").val('');
    $("input[name='" + inout + ".type.staticValue." + wsdlPath + "']").val('');
    link.style.display = 'none';
    // alert(wsdlPath);
}

function typeStaticValue(event, input) {
    var wsdlPath = input.getAttribute("name");
    var inout = input.getAttribute("inout");

    var length = inout.length + 1 + 5;
    // type. - 5
    wsdlPath = wsdlPath.substr(length, wsdlPath.length);
    $("img[name='" + inout + ".type.clear." + wsdlPath + "']").show();
    $("img[name='" + inout + ".type.edit." + wsdlPath + "']").show();

    $("input[name='" + inout + ".type.staticValue." + wsdlPath + "']").val(input.value);
}