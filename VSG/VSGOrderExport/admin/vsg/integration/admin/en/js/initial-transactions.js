/*
 $(document).ready(function(){
 $('.datepicker').mask("99/99/9999");
 });
 */

function searchTransactions() {
    hideErrorMsg();
    blockUI();

    $("#date_from_search").val($("#date_from").val());
    $("#date_to_search").val($("#date_to").val());
    $("#tr_status_search").val($("#tr_status").val());


    var options = {
        success: function (data) {
            unBlockUI();
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else {
                $("#transactions").load("gadgets/tables-data.jhtml");
            }
        },
        error: function (data) {
            unBlockUI();
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#searchTransactions");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function openRequestModal(item) {
    item.setAttribute("href", "#request" + item.getAttribute("transactionId"));
    item.click();
}

function openResponseModal(item) {
    item.setAttribute("href", "#response" + item.getAttribute("transactionId"));
    item.click();
}