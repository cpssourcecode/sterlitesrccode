function executeExport() {
    hideMessages();

    // $.blockUI();
    blockUI();

    var transactionInfoDiv = $("#transactionInfoDiv");
    transactionInfoDiv.html("");

    var options = {
        success: function (data) {
            unBlockUI();
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else {
                // showSuccessMsgDiv("Successfully exported.");
                var transactionId = data.transactionId;
                $.ajax({
                    url: "gadgets/output-review-transaction.jhtml?transactionId=" + transactionId,
                    cache: false
                }).done(function (html) {
                    transactionInfoDiv.html(html);
                    transactionInfoDiv.show();
                });
                // location.href = 'output-review-settings.jhtml';
            }
        },
        error: function (data) {
            unBlockUI();
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#exportModel");
    form.ajaxForm(options);
    form.submit();
    return false;
}