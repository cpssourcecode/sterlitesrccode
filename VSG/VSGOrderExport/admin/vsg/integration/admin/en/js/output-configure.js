$(document).ready(function () {
    var xmlTransferDiv = $('#xmlTransferDiv');
    var xmlTransferCheckbox = $('#xmlTransferCheckbox');
    var xmlTransferUseHid = $('#useXmlTransfer_hid');

    if (!xmlTransferCheckbox.is(":checked")) {
        xmlTransferDiv.hide();
        xmlTransferUseHid.val("false");
    } else {
        xmlTransferUseHid.val('true');
    }


/*

TEMP HIDE

    if ($("#preselectWsdl").val() == "wsdlUpload") {
        $("#check1").attr("checked", "");
    } else if ($("#preselectWsdl").val() == "wsdlUrl") {
        $("#check2").attr("checked", "");
    }
    checkElement();
*/

    xmlTransferCheckbox.change(function () {
        if (this.checked) {
            xmlTransferDiv.fadeIn('slow');
            xmlTransferUseHid.val("true");
        } else {
            xmlTransferUseHid.val("false");
            xmlTransferDiv.fadeOut('slow');
        }
    });
});

function checkElement() {
    var ele = document.getElementsByName('radiobutton');
    var i = ele.length;

    if (ele[1].checked) {
        $("#wsdlUrl_input").addClass("disabled");
        $("#uploadWsdlDiv").removeClass("disabled");
        $("#fileformlabelWsdl").removeClass("disabled");
        $("#uploadWsdl").removeAttr("disabled");
        $("#wsdlUrl_input").attr("disabled", "");
        $("#wsdlUrl_input").val("");
        $("#fileformlabelWsdl").text($("#uploadWsdl_preselect").val());
        $("#uploadWsdl_input").val($("#uploadWsdl_preselect").val());
        $("#uploadWsdl").val($("#uploadWsdl_preselect").val());
    } else if (ele[0].checked) {
        $("#wsdlUrl_input").removeClass("disabled");
        $("#uploadWsdlDiv").addClass("disabled");
        $("#fileformlabelWsdl").addClass("disabled");
        $("#uploadWsdl").attr("disabled", "");
        $("#wsdlUrl_input").removeAttr("disabled");
        $("#uploadWsdl").val("");
        $("#uploadWsdl_input").val("");
        $("#fileformlabelWsdl").text("");
        $("#wsdlUrl_input").val($("#wsdlUrl_preselect").val());
    }

}

function openFileDialog() {
    document.getElementById('upload').click();
    return false
}

function verifyOutputFormat(formId) {
    hideErrorMsg();
/*
TEMP HIDE

    $("#wsSecurity_update").val($("#wsSecurity_input").val().trim());
    $("#uploadWsdl_update").val($("#uploadWsdl").val().trim());
    $("#endpointUrl_update").val($("#endpointUrl_input").val().trim());
    $("#connectionTimeout_update").val($("#connectionTimeout_input").val().trim());
    $("#useSchedule_update").val($("#schedule_checkBox").attr('value'));
    $("#schedule_update").val($("#schedule_input").val().trim());
*/
    blockUI();

    $("#wsdlUrl_update").val($("#wsdlUrl_input").val().trim());

    var options = {
        success: function (data) {
            unBlockUI();
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else if ('success' == data.code) {
                uploadFile(formId);
            }
        },
        error: function (data) {
            unBlockUI();
            showRequestError();
        },
        dataType: 'json'
    };

    var form = $("#verifyOutputFormat");
    form.ajaxForm(options);
    form.submit();

}

function saveExportConfig() {
    hideErrorMsg();

    $("#wsSecurity_update").val($("#wsSecurity_input").val().trim());
    $("#wsdlUrl_update").val($("#wsdlUrl_input").val().trim());
    $("#uploadWsdl_update").val($("#uploadWsdl").val().trim());
    $("#endpointUrl_update").val($("#endpointUrl_input").val().trim());
    $("#connectionTimeout_update").val($("#connectionTimeout_input").val().trim());
    $("#useSchedule_update").val($("#schedule_checkBox").attr('value'));
    $("#schedule_update").val($("#schedule_input").val().trim());

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else if ('success' == data.code) {
                location.href = '../../output-selectquery.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };

    var form = $("#updateExportConfig");
    form.ajaxForm(options);
    form.submit();

}

function verifyTransferOptions(formId) {
    hideErrorMsg();

    $("#uploadXsd").val($("#uploadXsd_input").val().trim());
    $("#controlFile").val($("#controlFile_input").val().trim());
    $("#outputDirectory").val($("#outputDirectory_input").val().trim());
    $("#protocol").val($("#protocol_input").val().trim());
    $("#key").val($("#key_input").val().trim());
    $("#urlIp").val($("#urlIp_input").val().trim());
    $("#userName").val($("#userName_input").val().trim());
    $("#password").val($("#password_input").val().trim());
    $("#confirmPassword").val($("#confirmPassword_input").val().trim());
    $("#prefix").val($("#prefix_input").val().trim());
    $("#fileName").val($("#fileName_input").val().trim());
    $("#timeStepFormat").val($("#timeStepFormat_input").val().trim());
    $("#sufix").val($("#sufix_input").val().trim());
    $("#encriptionKey").val($("#encriptionKey_input").val().trim());
    $("#encriptionPhrase").val($("#encriptionPhrase_input").val().trim());
    $("#scheduleTransfer").val($("#scheduleTransfer_input").val().trim());
    $("#singleOrderPerFile").val($("#singleOrderPerFile_input").val().trim());

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else if ('success' == data.code) {
                uploadFile(formId);
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#verifyTransferOptions");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function saveTransferOptions() {
    hideErrorMsg();

    $("#uploadXsd_update").val($("#uploadXsd_input").val().trim());
    $("#controlFile_update").val($("#controlFile_input").val().trim());
    $("#outputDirectory_update").val($("#outputDirectory_input").val().trim());
    $("#protocol_update").val($("#protocol_input").val().trim());
    $("#key_update").val($("#key_input").val().trim());
    $("#urlIp_update").val($("#urlIp_input").val().trim());
    $("#userName_update").val($("#userName_input").val().trim());
    $("#password_update").val($("#password_input").val().trim());
    $("#confirmPassword_update").val($("#confirmPassword_input").val().trim());
    $("#prefix_update").val($("#prefix_input").val().trim());
    $("#fileName_update").val($("#fileName_input").val().trim());
    $("#timeStepFormat_update").val($("#timeStepFormat_input").val().trim());
    $("#sufix_update").val($("#sufix_input").val().trim());
    $("#encriptionKey_update").val($("#encriptionKey_input").val().trim());
    $("#encriptionPhrase_update").val($("#encriptionPhrase_input").val().trim());
    $("#scheduleTransfer_update").val($("#scheduleTransfer_input").val().trim());
    $("#singleOrderPerFile_update").val($("#singleOrderPerFile_input").val().trim());

    var options = {
        success: function (data) {
            if ('error' == data.code) {
                showErrorMsg(data.all_errors);
            } else if ('success' == data.code) {
                location.href = '../../output-selectquery.jhtml';
            }
        },
        error: function (data) {
            showRequestError();
        },
        dataType: 'json'
    };
    var form = $("#transferOptionsConfig");
    form.ajaxForm(options);
    form.submit();
    return false;

}

function toggleOrderTransferOptions() {
    $("#orderTransferOptionsDiv").toggleClass("hidden");
    $("#h1oto_1").toggleClass("hidden");
    $("#h1oto_2").toggleClass("hidden");
}

function useScheduleCheck() {
    var scheduleUseCheckbox = $("#schedule_checkBox");
    var schedule_input = $("#schedule_input");
    if (scheduleUseCheckbox.prop('checked')) {
        scheduleUseCheckbox.attr('value', 'true');
        schedule_input.removeClass("disabled");
        scheduleUseCheckbox.value = 'true';
    } else {
        scheduleUseCheckbox.attr('value', 'false');
        schedule_input.addClass("disabled");
        schedule_input.val('');
    }
    console.log('checked: ' + scheduleUseCheckbox.prop('checked'));
}

function checkScheduleCheck() {
    var scheduleUseCheckbox = $("#schedule_checkBox");
    if (scheduleUseCheckbox.attr('value') == 'true') {
        scheduleUseCheckbox.prop('checked', 'true');
    }
    useScheduleCheck();
}