package AtgWebBroker;

import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.parse.wsdl.bean.Message;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;

/**
 * @author Dmitry Golubev
 */
public class AtgWebBrokerTest {
	/**
	 * model of wsdl
	 */
	private static WSDLModel mWSDLModel;

	@BeforeClass
	public static void loadWSDLModel() {
		String wsdl = "D:/projects/CPS/svn/VSG/VSGOrderExport/testwsdl/AtgWebBroker.wsdl";
		WSDLParser parser = new WSDLParser();
		mWSDLModel = parser.analyze(wsdl);

		Assert.assertNotNull(mWSDLModel);
/*
		Assert.assertFalse(mWSDLModel.containsTargetNamespace("xs"));
		Assert.assertTrue(mWSDLModel.containsCommonTargetNamespace("xs"));
		Assert.assertTrue(mWSDLModel.getTargetNamespacesPrefixesFromDefinition().contains("xs"));
*/

/*
		mWSDLModel = parser.analyze(SalesOrderManagerCPTest.class.getResource("SalesOrderManagerCP.wsdl")
				.getFile());
*/
	}

	@Test
	public void test() {
		Assert.assertNotNull(mWSDLModel.getWsdl());
	}
}
