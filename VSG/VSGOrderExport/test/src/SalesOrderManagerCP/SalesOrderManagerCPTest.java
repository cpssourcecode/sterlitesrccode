package src.SalesOrderManagerCP;

import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import vsg.integration.export.parse.wsdl.bean.NodeType;
import vsg.integration.export.parse.wsdl.bean.Operation;
import vsg.integration.export.parse.wsdl.bean.Type;
import vsg.integration.export.parse.wsdl.bean.Message;
import vsg.integration.export.parse.wsdl.WSDLParser;
import vsg.integration.export.parse.wsdl.bean.WSDLModel;

import java.util.Iterator;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class SalesOrderManagerCPTest {
	/**
	 * model of wsdl
	 */
	private static WSDLModel mWSDLModel;

	private static String namespace_00 = "java:com.columbiapipe.e1.bssv.JP554200.valueobject";
	private static String namespace_01 = "java:oracle.e1.bssv.JP420000.valueobject";
	private static String namespace_02 = "java:oracle.e1.bssv.util.J0100010.valueobject";
	private static String namespace_03 = "java:oracle.e1.bssv.util.J4100010.valueobject";

	@BeforeClass
	public static void loadWSDLModel() {
		String wsdl = "D:/projects/CPS/svn/SalesOrderManagerCP.wsdl";
		WSDLParser parser = new WSDLParser();
		mWSDLModel = parser.analyze(wsdl);

		Assert.assertFalse(mWSDLModel.containsTargetNamespace("xs"));
		Assert.assertTrue(mWSDLModel.containsCommonTargetNamespace("xs"));
		Assert.assertTrue(mWSDLModel.getTargetNamespacesPrefixesFromDefinition().contains("xs"));

/*
		mWSDLModel = parser.analyze(SalesOrderManagerCPTest.class.getResource("SalesOrderManagerCP.wsdl")
				.getFile());
*/
	}

	@Test
	public void testMessages() {
		Assert.assertNotNull(mWSDLModel.getMessages());
		Assert.assertEquals(3, mWSDLModel.getMessages().size());
	}

	@Test
	public void testMessage_processSalesOrder() {
		Message message = mWSDLModel.getMessage("processSalesOrder");
		Assert.assertNotNull(message);

		Type messageType = mWSDLModel.getTypeByName("processSalesOrderElement");
		Assert.assertNotNull(messageType);
		Assert.assertEquals(NodeType.COMPLEX, messageType.getType());

		Assert.assertEquals(messageType, message.getType());
	}


	@Test
	public void testType_ProcessSalesOrder() {
		Type type_ProcessSalesOrder = mWSDLModel.getTypeByName("ProcessSalesOrder");
		Assert.assertNotNull(type_ProcessSalesOrder);
		Assert.assertNotNull(type_ProcessSalesOrder.getSubTypes());
		Assert.assertNotNull(type_ProcessSalesOrder.getTargetNamespace());
		Assert.assertEquals(namespace_00, type_ProcessSalesOrder.getTargetNamespace().getNamespace());
		Assert.assertEquals(NodeType.COMPLEX, type_ProcessSalesOrder.getType());
		Assert.assertEquals(1, type_ProcessSalesOrder.getSubTypes().size());

		Type type_Header = type_ProcessSalesOrder.getSubType("Header");
		Assert.assertNotNull(type_Header);
		Assert.assertEquals("Header", type_Header.getName());
		Assert.assertEquals("ProcessSOHeader", type_Header.getTypeName());
		Assert.assertEquals(NodeType.COMPLEX, type_Header.getType());
		Assert.assertNotNull(type_Header.getTargetNamespace());
		Assert.assertEquals(namespace_00, type_Header.getTargetNamespace().getNamespace());
		Assert.assertNotNull(type_Header.getTargetNamespaceForChildTypes());
		Assert.assertEquals(namespace_00, type_Header.getTargetNamespaceForChildTypes().getNamespace());

		Type type_DateOrdered = type_Header.getSubType("DateOrdered");
		Assert.assertNotNull(type_DateOrdered);
		Assert.assertEquals("DateOrdered", type_DateOrdered.getName());
		Assert.assertEquals("dateTime", type_DateOrdered.getTypeName());
		Assert.assertEquals(type_DateOrdered.getType(), NodeType.ELEMENT);
		Assert.assertNotNull(type_DateOrdered.getTargetNamespace());
		Assert.assertEquals(type_Header.getTargetNamespaceForChildTypes(), type_DateOrdered.getTargetNamespace());
		Assert.assertEquals(namespace_00, type_DateOrdered.getTargetNamespace().getNamespace());
		Assert.assertNull(type_DateOrdered.getTargetNamespaceForChildTypes());

		Type type_SoldTo = type_Header.getSubType("SoldTo");
		Assert.assertNotNull(type_SoldTo);
		Assert.assertEquals("SoldTo", type_SoldTo.getName());
		Assert.assertEquals("ProcessSOCustomer", type_SoldTo.getTypeName());
		Assert.assertEquals(NodeType.COMPLEX, type_SoldTo.getType());
		Assert.assertEquals(11, type_SoldTo.getSubTypes().size());
		Assert.assertNotNull(type_SoldTo.getTargetNamespace());
		Assert.assertEquals(namespace_00, type_SoldTo.getTargetNamespace().getNamespace());
		Assert.assertNotNull(type_SoldTo.getTargetNamespaceForChildTypes());
		Assert.assertEquals(namespace_01, type_SoldTo.getTargetNamespaceForChildTypes().getNamespace());

		Type type_Customer = type_SoldTo.getSubType("Customer");
		Assert.assertNotNull(type_Customer);
		Assert.assertEquals("Customer", type_Customer.getName());
		Assert.assertEquals("Entity", type_Customer.getTypeName()); // ?
		Assert.assertEquals(NodeType.COMPLEX, type_Customer.getType());
		Assert.assertEquals(3, type_Customer.getSubTypes().size());
		Assert.assertNotNull(type_Customer.getTargetNamespace());
		Assert.assertEquals(namespace_01, type_Customer.getTargetNamespace().getNamespace());
		Assert.assertNotNull(type_Customer.getTargetNamespaceForChildTypes());
		Assert.assertEquals(namespace_02, type_Customer.getTargetNamespaceForChildTypes().getNamespace());

		Type type_EntityId = type_Customer.getSubType("EntityId");
		Assert.assertNotNull(type_EntityId);
		Assert.assertEquals("EntityId", type_EntityId.getName());
		Assert.assertEquals("int", type_EntityId.getTypeName()); // ?
		Assert.assertEquals(NodeType.ELEMENT, type_EntityId.getType());
		Assert.assertEquals(0, type_EntityId.getSubTypes().size());
		Assert.assertNotNull(type_EntityId.getTargetNamespace());
		Assert.assertEquals(namespace_02, type_EntityId.getTargetNamespace().getNamespace());
		Assert.assertNull(type_EntityId.getTargetNamespaceForChildTypes());

	}

	@Test
	public void testType_ProcessSOHeader() {
		Type type_ProcessSOHeader = mWSDLModel.getTypeByName("ProcessSOHeader");
		Assert.assertNotNull(type_ProcessSOHeader);
		Assert.assertNotNull(type_ProcessSOHeader.getSubTypes());
		Assert.assertNotNull(type_ProcessSOHeader.getTargetNamespace());

		Assert.assertEquals(type_ProcessSOHeader.getType(), NodeType.COMPLEX);
		Assert.assertEquals(
				type_ProcessSOHeader.getTargetNamespace().getNamespace(),
				namespace_00
		);
		Assert.assertEquals(type_ProcessSOHeader.getSubTypes().size(), 27);

		// subtype: Processing
		Type type_Processing = type_ProcessSOHeader.getSubType("Processing");
		Assert.assertEquals(NodeType.COMPLEX, type_Processing.getType());
		Assert.assertEquals("Processing", type_Processing.getName());
		Assert.assertEquals("ProcessSOHeaderProcessing", type_Processing.getTypeName());
		Assert.assertEquals(2, type_Processing.getSubTypes().size());

		// subtype: BusinessUnit
		Type type_BusinessUnit = type_ProcessSOHeader.getSubType("BusinessUnit");
		Assert.assertEquals(type_BusinessUnit.getType(), NodeType.ELEMENT);
		Assert.assertEquals(type_BusinessUnit.getName(), "BusinessUnit");
		Assert.assertEquals(type_BusinessUnit.getTypeName(), "string");
		Assert.assertEquals(type_BusinessUnit.getSubTypes().size(), 0);
		Assert.assertNotNull(type_BusinessUnit.getTargetNamespace());
		Assert.assertEquals(type_BusinessUnit.getTargetNamespace(), type_ProcessSOHeader.getTargetNamespace());

		// subtype: Processing.ActionType
		Type type_Processing_ActionType = type_Processing.getSubType("ActionType");
		Assert.assertEquals(NodeType.ELEMENT, type_Processing_ActionType.getType());
		Assert.assertEquals("string", type_Processing_ActionType.getTypeName());
		Assert.assertEquals(namespace_01, type_Processing_ActionType.getTargetNamespace().getNamespace());

		// subtype: Processing.ProcessingVersion
		Type type_Processing_ProcessingVersion = type_Processing.getSubType("ProcessingVersion");
		Assert.assertEquals(type_Processing_ProcessingVersion.getType(), NodeType.ELEMENT);
		Assert.assertEquals(type_Processing_ProcessingVersion.getTypeName(), "string");

		// subtype: SalesOrderKey
		Type type_SalesOrderKey = type_ProcessSOHeader.getSubType("SalesOrderKey");
		Assert.assertEquals(type_SalesOrderKey.getType(), NodeType.COMPLEX);
		Assert.assertEquals(type_SalesOrderKey.getTypeName(), "SalesOrderKey");
		Assert.assertEquals(type_SalesOrderKey.getSubTypes().size(), 3);

		// subtype: SalesOrderKey.DocumentNumber
		Type type_SalesOrderKey_DocumentNumber = type_SalesOrderKey.getSubType("DocumentNumber");
		Assert.assertEquals(type_SalesOrderKey_DocumentNumber.getType(), NodeType.ELEMENT);
		Assert.assertEquals(type_SalesOrderKey_DocumentNumber.getTypeName(), "int");

		// subtype: SalesOrderKey.DocumentTypeCode
		Type type_SalesOrderKey_DocumentTypeCode = type_SalesOrderKey.getSubType("DocumentTypeCode");
		Assert.assertEquals(type_SalesOrderKey_DocumentTypeCode.getType(), NodeType.ELEMENT);
		Assert.assertEquals(type_SalesOrderKey_DocumentTypeCode.getTypeName(), "string");

		// subtype: SalesOrderKey.DocumentCompany
		Type type_SalesOrderKey_DocumentCompany = type_SalesOrderKey.getSubType("DocumentCompany");
		Assert.assertEquals(type_SalesOrderKey_DocumentCompany.getType(), NodeType.ELEMENT);
		Assert.assertEquals(type_SalesOrderKey_DocumentCompany.getTypeName(), "string");
	}


	@Test
	public void testOperations() {
		Assert.assertNotNull(mWSDLModel.getOperations());
		Assert.assertEquals(mWSDLModel.getOperations().size(), 1);
	}

	@Test
	public void testOperation_processSalesOrder_Tree() {
		Operation operation = mWSDLModel.getOperation("processSalesOrder");
		Message inputMessage = mWSDLModel.getMessage("processSalesOrder");
		Message outputMessage = mWSDLModel.getMessage("processSalesOrderResponse");

		Assert.assertNotNull(operation);
		Assert.assertNotNull(inputMessage);
		Assert.assertNotNull(outputMessage);

		Assert.assertEquals(operation.getMessageInput(), inputMessage);
		Assert.assertEquals(operation.getMessageOutput(), outputMessage);

		StringBuilder path = new StringBuilder("processSalesOrder");

		Type type_ProcessSalesOrder = operation.getMessageInput().getType();
		//Assert.assertEquals(null, type_ProcessSalesOrder.getParent());
		Assert.assertEquals("processSalesOrderElement", type_ProcessSalesOrder.getName());
		Assert.assertEquals("ProcessSalesOrder", type_ProcessSalesOrder.getTypeName());
		Assert.assertEquals(path.append(".processSalesOrderElement").toString(), type_ProcessSalesOrder.getFullPath());

		Type type_Header = type_ProcessSalesOrder.getSubType("Header");
		// Assert.assertEquals(type_ProcessSalesOrder, type_Header.getParent());
		Assert.assertEquals("Header", type_Header.getName());
		Assert.assertEquals("ProcessSOHeader", type_Header.getTypeName());
		Assert.assertEquals(path.append(".Header").toString(), type_Header.getFullPath());

		Type type_Detail = type_Header.getSubType("Detail");
		Assert.assertEquals("Detail", type_Detail.getName());
		Assert.assertEquals("ProcessSODetail", type_Detail.getTypeName());
		Assert.assertEquals(path.append(".Detail").toString(), type_Detail.getFullPath());


		Type type_Product = type_Detail.getSubType("Product");
		Assert.assertEquals("Product", type_Product.getName());
		Assert.assertEquals("ProcessSODetailProduct", type_Product.getTypeName());
		Assert.assertEquals(path.append(".Product").toString(), type_Product.getFullPath());

		Type type_Item = type_Product.getSubType("Item");
		Assert.assertEquals("Item", type_Item.getName());
		Assert.assertEquals("ItemGroupCustomer", type_Item.getTypeName());
		Assert.assertEquals(path.append(".Item").toString(), type_Item.getFullPath());

		Type type_ItemProduct = type_Item.getSubType("ItemProduct");
		Assert.assertEquals("ItemProduct", type_ItemProduct.getName());
		Assert.assertEquals("string", type_ItemProduct.getTypeName());
		Assert.assertEquals(path.append(".ItemProduct").toString(), type_ItemProduct.getFullPath());
	}

	@Test
	public void testOperation_processSalesOrder() {
		Operation operation = mWSDLModel.getOperation("processSalesOrder");
		Message inputMessage = mWSDLModel.getMessage("processSalesOrder");

		Assert.assertNotNull(operation);
		Assert.assertNotNull(inputMessage);

		Assert.assertEquals(operation.getMessageInput(), inputMessage);


		Type type_ProcessSalesOrder = inputMessage.getType();
		Type type_Header = type_ProcessSalesOrder.getSubTypes().iterator().next();

		List<Type> subTypes = type_Header.getSubTypes();
/*
			  <xs:element minOccurs="1" name="Processing" nillable="true" type="java1:ProcessSOHeaderProcessing" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"/>
              <xs:element minOccurs="1" name="SalesOrderKey" nillable="true" type="java1:SalesOrderKey" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"/>
              <xs:element minOccurs="1" name="SoldTo" nillable="true" type="java1:ProcessSOCustomer" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"/>
              <xs:element minOccurs="1" name="ShipTo" nillable="true" type="java1:ProcessSOCustomer" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"/>
              <xs:element minOccurs="1" name="Billing" nillable="true" type="java1:ProcessSOHeaderBilling" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"/>
              <xs:element minOccurs="1" name="DeliverTo" nillable="true" type="java1:Entity" xmlns:java1="java:oracle.e1.bssv.util.J0100010.valueobject"/>
              <xs:element minOccurs="1" name="InvoicedTo" nillable="true" type="java1:Entity" xmlns:java1="java:oracle.e1.bssv.util.J0100010.valueobject"/>
              <xs:element minOccurs="1" name="PaidBy" nillable="true" type="java1:Entity" xmlns:java1="java:oracle.e1.bssv.util.J0100010.valueobject"/>
              <xs:element minOccurs="1" name="ForwardedTo" nillable="true" type="java1:Entity" xmlns:java1="java:oracle.e1.bssv.util.J0100010.valueobject"/>
              <xs:element minOccurs="1" name="UserReservedData" nillable="true" type="java1:UserReservedData" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"/>
              <xs:element maxOccurs="unbounde"Detail" nillable="true" type="java1:ProcessSODetail" xmlns:java1="java:oracle.e1.bssv.JP420000.valueobject"/>
              <xs:element minOccurs="1" name="HeaderCP" nillable="true" type="java1:ProcessSOHeaderCP" xmlns:java1="java:com.columbiapipe.e1.bssv.JP554200.valueobject"/>

*/
		Iterator<Type> subTypesIter = subTypes.iterator();
		Type subType;

		subType = subTypesIter.next();
		Assert.assertEquals("AttachmentText", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("BusinessUnit", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("Company", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("CurrencyCodeTo", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("CustomerPO", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("DateCancel", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("DateOrdered", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("DateRequested", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("DateScheduledPick", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("HoldOrderCode", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("OrderTakenBy", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("OrderedBy", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("RateExchange", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("TimeRequested", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("TimeScheduledPick", subType.getName());

		subType = subTypesIter.next();
		Assert.assertEquals("Billing", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("DeliverTo", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("Detail", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("ForwardedTo", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("HeaderCP", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("InvoicedTo", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("PaidBy", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("Processing", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("SalesOrderKey", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("ShipTo", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("SoldTo", subType.getName());
		subType = subTypesIter.next();
		Assert.assertEquals("UserReservedData", subType.getName());
	}
}