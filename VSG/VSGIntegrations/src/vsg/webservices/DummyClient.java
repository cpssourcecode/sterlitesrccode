package vsg.webservices;

/**
 * DummyClient
 * 
 * <h4>Description</h4> This is the super class 
 * for a dummy client used for integrations.
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 *
 */
public class DummyClient extends WebServiceClient {

}
