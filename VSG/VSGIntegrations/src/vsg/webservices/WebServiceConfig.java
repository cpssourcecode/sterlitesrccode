package vsg.webservices;

import atg.nucleus.GenericService;

/**
 * WebServiceConfig
 * 
 * <h4>Description</h4> This class is the configuration 
 * structure to be extended by all web services for 
 * integration.
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 * 
 */
public class WebServiceConfig extends GenericService {

	/** user id property */
	private String mUserId;

	/** password property */
	private String mPassword;

	/** secret key property */
	private String mSecretKey;

	/** Web Service Enabled property
	 * flag used to determine if the web services are active or not
	 */
	private boolean mWebServiceEnabled;

	/** Service address property 
	 * ip address to be used for web service 
	 */
	private String mServiceAddress;

	/** Service Port Address property
	 * string that is used to set the BTS WS end point
	 */
	private String mServicePortAddress;

	/** Time out property 
	 * time out value in milli seconds
	 */
	private int mTimeOut = 60000;

	/** Binding Time Out property
	 * binding time out in milli seconds
	 */
	private int mSoTimeOut = 60000;

	/** Application Name property
	 * application name for the security bean
	 */
	private String mApplicationName;

	/**
	 * 
	 */
	public WebServiceConfig() {
	}

	/**
	 * The method looks up the interface code to see if the service is enabled or not
	 * 
	 * @return the pWebServiceEnabled
	 */
	public boolean getWebServiceEnabled() {
		return mWebServiceEnabled;
	}

	public void setWebServiceEnabled(boolean pWebServiceEnabled){
		mWebServiceEnabled = pWebServiceEnabled;
	}
	
	/**
	 * @return the ServiceAddress
	 */
	public String getServiceAddress() {
		return mServiceAddress;
	}

	/**
	 * @param pServiceAddress
	 *            the ServiceAddress to set
	 */
	public void setServiceAddress(String pServiceAddress) {
		mServiceAddress = pServiceAddress;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return mUserId;
	}

	/**
	 * @param pUserId
	 *            the userId to set
	 */
	public void setUserId(String pUserId) {
		mUserId = pUserId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return mPassword;
	}

	/**
	 * @param pPassword
	 *            the Password to set
	 */
	public void setPassword(String pPassword) {
		mPassword = pPassword;
	}

	/**
	 * @return the SecretKey
	 */
	public String getSecretKey() {
		return mSecretKey;
	}

	/**
	 * @param pSecretKey
	 *            the SecretKey to set
	 */
	public void setSecretKey(String pSecretKey) {
		mSecretKey = pSecretKey;
	}

	/**
	 * @return the TimeOut
	 */
	public int getTimeOut() {
		return mTimeOut;
	}

	/**
	 * @param pTimeOut
	 *            the TimeOut to set
	 */
	public void setTimeOut(int pTimeOut) {
		mTimeOut = pTimeOut;
	}

	/**
	 * @return the soTimeOut
	 */
	public int getSoTimeOut() {
		return mSoTimeOut;
	}

	/**
	 * @param pSoTimeOut
	 *            the SoTimeOut to set
	 */
	public void setSoTimeOut(int pSoTimeOut) {
		mSoTimeOut = pSoTimeOut;
	}

	/**
	 * @return the ApplicationName
	 */
	public String getApplicationName() {
		return mApplicationName;
	}

	/**
	 * @param pApplicationName
	 *            the ApplicationName to set
	 */
	public void setApplicationName(String pApplicationName) {
		mApplicationName = pApplicationName;
	}

	/**
	 * @return the ServicePortAddress
	 */
	public String getServicePortAddress() {
		return mServicePortAddress;
	}

	/**
	 * @param pServicePortAddress
	 *            the ServicePortAddress to set
	 */
	public void setServicePortAddress(String pServicePortAddress) {
		mServicePortAddress = pServicePortAddress;
	}

	private String mHistoryServiceAddress;

	public String getHistoryServiceAddress() {
		return mHistoryServiceAddress;
	}

	public void setHistoryServiceAddress(String pHistoryServiceAddress) {
		mHistoryServiceAddress = pHistoryServiceAddress;
	}

	private String mDocumentExtractionServiceAddress;

	public String getDocumentExtractionServiceAddress() {
		return mDocumentExtractionServiceAddress;
	}

	public void setDocumentExtractionServiceAddress(String pDocumentExtractionServiceAddress) {
		mDocumentExtractionServiceAddress = pDocumentExtractionServiceAddress;
	}

	private String mRemoteFormAddress;

	public void setRemoteFormAddress(String pRemoteFormAddress) {
		mRemoteFormAddress = pRemoteFormAddress;
	}

	public String getRemoteFormAddress() {
		return mRemoteFormAddress;
	}

}

