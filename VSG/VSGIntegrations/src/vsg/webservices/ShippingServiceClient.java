package vsg.webservices;

import java.util.Properties;

public class ShippingServiceClient extends WebServiceClient {

	private Properties mShippingMethodServiceCodesMap = null;
	private Properties mServiceCodesShippingMethodMap = null;

	public Properties getShippingMethodServiceCodesMap() {
		return mShippingMethodServiceCodesMap;
	}

	public void setShippingMethodServiceCodesMap(Properties pShippingMethodServiceCodesMap) {
		mShippingMethodServiceCodesMap = pShippingMethodServiceCodesMap;
	}


	public Properties getServiceCodesShippingMethodMap() {
		return mServiceCodesShippingMethodMap;
	}

	public void setServiceCodesShippingMethodMap(Properties pServiceCodesShippingMethodMap) {
		mServiceCodesShippingMethodMap = pServiceCodesShippingMethodMap;
	}

}
