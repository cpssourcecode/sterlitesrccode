package vsg.webservices;

import atg.nucleus.GenericService;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.llom.OMElementImpl;
import org.apache.ws.security.WSConstants;

/**
 * WebServiceClient
 * 
 * <h4>Description</h4> This class is the integration parent class
 * for all web services.
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 * 
 */
public class WebServiceClient extends GenericService {

	/** Web Service Config property */
	private WebServiceConfig mWebServiceConfig;
	
	/**
	 * Default constructor
	 */
	public WebServiceClient() {
	}

	/** Constructor
	 * 
	 * @param pWebServiceConfig
	 */
	public WebServiceClient(WebServiceConfig pWebServiceConfig) {
		setWebServiceConfig(pWebServiceConfig);
	}

	/**
	 * @return the webServicesConfig
	 */
	public WebServiceConfig getWebServiceConfig() {
		return this.mWebServiceConfig;
	}

	/**
	 * @param pWebServiceConfig
	 *            the webServiceConfig to set
	 */
	public void setWebServiceConfig(WebServiceConfig pWebServiceConfig) {
		mWebServiceConfig = pWebServiceConfig;
	}

	protected OMElement createSecurityHeader(String userName , String password) {
		OMFactory factory = OMAbstractFactory.getOMFactory();
    	OMNamespace wsseNS = factory.createOMNamespace(WSConstants.WSSE_NS, WSConstants.WSSE_PREFIX);
        OMElementImpl securityHeader;
        OMElementImpl usernameTokenElement;
        OMElementImpl usernameElement;
        OMElementImpl passwordElement;

        // create the Security header block
        securityHeader = new OMElementImpl("Security", wsseNS, factory);
        securityHeader.addAttribute("mustUnderstand", "1", null);

        // nest the UsernameToken in the Security header
        usernameTokenElement = new OMElementImpl(WSConstants.USERNAME_TOKEN_LN, wsseNS, securityHeader, factory);

        // nest the Username and Password elements
        usernameElement = new OMElementImpl(WSConstants.USERNAME_LN, wsseNS, usernameTokenElement, factory);
        usernameElement.setText(userName);

        passwordElement = new OMElementImpl(WSConstants.PASSWORD_LN, wsseNS, usernameTokenElement, factory);
        passwordElement.setText(password);
        passwordElement.addAttribute(WSConstants.PASSWORD_TYPE_ATTR, WSConstants.PASSWORD_TEXT, null);

        return securityHeader;
	}

}
