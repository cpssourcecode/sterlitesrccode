package vsg.order;

/**
 * @author Andy Porter
 */
public class VSGOrderInfo {
	private String mId;
	private String mType;
	private String mShipTo;
	private String mShipToName;
	private String mOrderNumber;
	private String mUrl;

	public void setId(String pId) {
		mId = pId;
	}

	public String getId() {
		return mId;
	}

	public void setType(String pType) {
		mType = pType;
	}

	public String getType() {
		return mType;
	}

	public void setShipTo(String pShipTo) {
		mShipTo = pShipTo;
	}

	public String getShipTo() {
		return mShipTo;
	}

	public void setShipToName(String pShipToName) {
		mShipToName = pShipToName;
	}

	public String getShipToName() {
		return mShipToName;
	}

	public void setOrderNumber(String pOrderNumber) {
		mOrderNumber = pOrderNumber;
	}

	public String getOrderNumber() {
		return mOrderNumber;
	}

	public void setUrl(String pUrl) {
		mUrl = pUrl;
	}

	public String getUrl() {
		return mUrl;
	}
}
