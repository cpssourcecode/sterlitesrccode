package vsg.exception;

/**
 * WebServiceException
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 * 
 */
public class WebServiceException extends Exception {

	/**
	 * Generated
	 */
	private static final long serialVersionUID = -7905779169691034812L;

	/**
	 * 
	 */
	public WebServiceException() {
	}

	/**
	 * @param pMessage
	 */
	public WebServiceException(String pMessage) {
		super(pMessage);
	}

	/**
	 * @param pCause
	 */
	public WebServiceException(Throwable pCause) {
		super(pCause);
	}

	/**
	 * @param pMessage
	 * @param pCause
	 */
	public WebServiceException(String pMessage, Throwable pCause) {
		super(pMessage, pCause);
	}

}
