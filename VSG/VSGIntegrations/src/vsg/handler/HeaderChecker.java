package vsg.handler;

import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;

import javax.xml.namespace.QName;
import java.util.HashSet;
import java.util.Set;

public class HeaderChecker extends AbstractHandler {

	public InvocationResponse invoke(MessageContext messageContext) throws AxisFault {

        Set<QName> understoodHeaders = new HashSet<QName>();
        SOAPEnvelope envelope = messageContext.getEnvelope();

		if (envelope.getHeader() != null) {
		understoodHeaders.add(envelope.getHeader().getQName());
			if (envelope.getHeader().getFirstElement() != null) {
				understoodHeaders.add(envelope.getHeader().getFirstElement().getQName());
			}
//			Iterator headerBlocks = envelope.getHeader().getHeadersToProcess((RolePlayer) messageContext.getConfigurationContext().getAxisConfiguration().getParameterValue("rolePlayer"));
//			while (headerBlocks.hasNext()) {
//				OMElementImpl headerBlock = (OMElementImpl) headerBlocks.next();
//				understoodHeaders.add(envelope.getHeader().getFirstElement().getQName());
//			}
        }
		messageContext.setProperty("client.UnderstoodHeaders",understoodHeaders);
		return InvocationResponse.CONTINUE;
	}

}
