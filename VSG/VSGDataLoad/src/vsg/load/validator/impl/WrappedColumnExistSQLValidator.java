package vsg.load.validator.impl;

import vsg.load.loader.Record;
import vsg.load.wrapper.IValueWrapper;

/**
 * The Class WrappedColumnExistSQLValidator is used to validate wrapped value instead initial field value.
 * 
 * @author Kate Koshman
 */
public class WrappedColumnExistSQLValidator extends ColumnExistSQLValidator {
	
	/** The Wrapper. */
	@SuppressWarnings("rawtypes")
	private IValueWrapper mWrapper;
	
	/* Overrides <code>isValid</code> to wrap value before validation via configured <code>wrapper</code>.
	 * @see vsg.load.validator.impl.ColumnExistSQLValidator#isValid(java.lang.Object, java.lang.String)
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {
		Object wrappedValue = getWrapper().getValue(pValue);
		return super.isValid(wrappedValue, pFieldName, pRecord);
	}

	@SuppressWarnings("rawtypes")
	public IValueWrapper getWrapper() {
		return mWrapper;
	}

	@SuppressWarnings("rawtypes")
	public void setWrapper(IValueWrapper pWrapper) {
		mWrapper = pWrapper;
	}
}
