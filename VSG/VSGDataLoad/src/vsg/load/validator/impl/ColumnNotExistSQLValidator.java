package vsg.load.validator.impl;

import vsg.load.loader.Record;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author Dmitry Golubev
 */
public class ColumnNotExistSQLValidator extends ColumnExistSQLValidator {
	/**
	 * check if value is valid
	 *
	 * @param pValue     value to be checked
	 * @param pFieldName field name
	 * @param pRecord    record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {
		if (pValue != null) {
			String recordId = (String) pValue;
			boolean exist = false;
			try (Connection connection = getConnection();
				 PreparedStatement ps = connection.prepareStatement(getSqlExistColumn());) {
				ps.setString(1, recordId);
				try (ResultSet rs = ps.executeQuery();) {
					exist = rs.next();
					rs.close();
					ps.close();
					closeConnection(connection);
				}
			} catch (Exception e) {
				getLoggerTools().logError(e);
			}
			if (exist) {
				setValidationMessage(getTableName() + "." + pFieldName + ": - found exist item [" +
						pRecord.getId() + ":" + pValue + "]");
			}
			return !exist;
		} else {
			return false;
		}
	}
}
