package vsg.load.validator.impl;

import vsg.load.loader.Record;
import vsg.load.tools.ILoggerTools;
import vsg.load.validator.IValidator;

/**
 * @author Dmitry Golubev
 */
public abstract class ValidatorImpl implements IValidator {
	/**
	 * validation message
	 */
	private String mValidationMessage;
	/**
	 * allow null flag
	 */
	private boolean mAllowNull = false;
	/**
	 * just skip failed on validation without error notifications
	 */
	private boolean mJustSkip = false;
	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return null is allowed
	 * @return null is allowed
	 */
	public boolean isAllowNull() {
		return mAllowNull;
	}

	/**
	 * init null is allowed
	 * @param pAllowNull null is allowed
	 */
	public void setAllowNull(boolean pAllowNull) {
		mAllowNull = pAllowNull;
	}

	/**
	 * init validation message
	 * @param pValidationMessage validation message
	 */
	public void setValidationMessage(String pValidationMessage) {
		mValidationMessage = pValidationMessage;
	}

	/**
	 * return validation message
	 * @return validation message
	 */
	@Override
	public String getValidationMessage() {
		return mValidationMessage;
	}

	/**
	 * return logger tools
	 * @return logger tools
	 */
	public ILoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	/**
	 * init logger tools
	 * @param pLoggerTools logger tools
	 */
	public void setLoggerTools(ILoggerTools pLoggerTools) {
		mLoggerTools = pLoggerTools;
	}

	public boolean isJustSkip() {
		return mJustSkip;
	}

	public void setJustSkip(boolean pJustSkip) {
		mJustSkip = pJustSkip;
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public boolean isValid(Object pValue, String pFieldName) {
		return isValid(pValue, pFieldName, null);
	}
	/**
	 * check if null input is error
	 * @param pFieldName field name
	 * @param pValue input value
	 * @return null input is error
	 */
	protected boolean isErrorNullInput(String pFieldName, Object pValue) {
		boolean isError = pValue == null && !isAllowNull();
		if(isError) {
			setValidationMessage("{"+pFieldName+"} - empty field value is not allowed.");
		}
		return isError;
	}
}
