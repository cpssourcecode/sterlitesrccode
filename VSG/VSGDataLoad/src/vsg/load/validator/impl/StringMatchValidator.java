package vsg.load.validator.impl;

import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public class StringMatchValidator extends StringValidator {

	/**
	 * allowed value
	 */
	private String mAllowedValue;

	// -----------------------------------------------------------------------------------------------------------------

	public String getAllowedValue() {
		return mAllowedValue;
	}

	public void setAllowedValue(String pAllowedValue) {
		mAllowedValue = pAllowedValue;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {
		String value = (String) pValue;
		boolean isValid = super.isValid(pValue, pFieldName, pRecord);
		if(isValid) {
			if(!getAllowedValue().equalsIgnoreCase(value)) {
				isValid = false;
				setValidationMessage("{"+pFieldName+": "+pValue+"} - does not match ["+ getAllowedValue() + "]");
			}
		}
		return isValid;
	}
}
