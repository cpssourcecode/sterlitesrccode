package vsg.load.validator.impl;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * @author Dmitry Golubev
 */
public class DateValidator extends ValidatorImpl {

	/**
	 * date pattern
	 */
	private String mDatePattern="MM/dd/yyyy kk:mm";

	/**
	 * SimpleDateFormat obj
	 */
	private SimpleDateFormat mDateFormat;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return date pattern
	 * @return date pattern
	 */
	public String getDatePattern() {
		return mDatePattern;
	}

	/**
	 * init date pattern
	 * @param pDatePattern date pattern
	 */
	public void setDatePattern(String pDatePattern) {
		mDatePattern = pDatePattern;
	}

	/**
	 * init date formatter if it's null
	 * @return date formatter
	 */
	public SimpleDateFormat getDateFormat() {
		if(mDateFormat == null) {
			mDateFormat = new SimpleDateFormat(getDatePattern());
		}
		return mDateFormat;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {

		if(isAllowNull() && StringUtils.isBlank((String) pValue)) {
			return true;
		} else {
			if(isErrorNullInput(pFieldName, pValue)) {
				return false;
			}
		}

		try {
			getDateFormat().parse((String) pValue);
		} catch (ParseException e) {
			setValidationMessage("{"+pFieldName+": "+pValue+"} - is not acceptable Date ["+mDatePattern+"]");
			return false;
		}

		return true;
	}
}
