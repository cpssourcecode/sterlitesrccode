package vsg.load.validator.impl;

import atg.core.util.StringUtils;
import vsg.load.loader.CommonSettings;
import vsg.load.loader.Record;

import java.io.UnsupportedEncodingException;

/**
 * @author Dmitry Golubev
 */
public class StringValidator extends ValidatorImpl {

	/**
	 * allowed max length
	 */
	private int mMaxLength = -1;
	/**
	 * allowed min length
	 */
	private int mMinLength = -1;
	/**
	 * common settings
	 */
	private CommonSettings mCommonSettings;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return max string length
	 * @return max string length
	 */
	public int getMaxLength() {
		return mMaxLength;
	}

	/**
	 * init max string length
	 * @param pMaxLength max string length
	 */
	public void setMaxLength(int pMaxLength) {
		mMaxLength = pMaxLength;
	}

	/**
	 * return min string length
	 * @return min string length
	 */
	public int getMinLength() {
		return mMinLength;
	}

	/**
	 * init min string length
	 * @param pMinLength min string length
	 */
	public void setMinLength(int pMinLength) {
		mMinLength = pMinLength;
	}

	/**
	 * common settings
	 * @return common settings
	 */
	public CommonSettings getCommonSettings() {
		return mCommonSettings;
	}

	/**
	 * init common settings
	 * @param pCommonSettings common settings
	 */
	public void setCommonSettings(CommonSettings pCommonSettings) {
		mCommonSettings = pCommonSettings;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {

		if(isAllowNull() && StringUtils.isBlank((String) pValue)) {
			return true;
		} else {
			if(isErrorNullInput(pFieldName, pValue)) {
				return false;
			}
		}

		String value = (String) pValue;
		boolean isValid = isValidByMaxLength(value) && isValidByMinLength(value);
		if(!isValid) {
			setValidationMessage("{"+pFieldName+": '"+pValue+"'} - length is not allowed ["+getMinLength()+" - "
					+getMaxLength() + "]");
		}
		return isValid;
	}

	/**
	 * check if string to be validated by max length
	 * @param pValue string to be validated by max length
	 * @return string is valid by max length
	 */
	private boolean isValidByMaxLength(String pValue) {
		int length;
		try {
			length = pValue.getBytes(getCommonSettings().getEncoding()).length;
		} catch (UnsupportedEncodingException e) {
			length = pValue.length();
		}
		return getMaxLength() < 0 || length <= getMaxLength();
	}

	/**
	 * check if string to be validated by min length
	 * @param pValue string to be validated by min length
	 * @return string is valid by min length
	 */
	private boolean isValidByMinLength(String pValue) {
		int length;
		try {
			length = pValue.getBytes(getCommonSettings().getEncoding()).length;
		} catch (UnsupportedEncodingException e) {
			length = pValue.length();
		}
		return getMinLength() < 0 || length >= getMinLength();
	}
}
