package vsg.load.validator.impl;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;

import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class EnumValidator extends ValidatorImpl {

	/**
	 * allowed values
	 */
	private List<String> mEnumValues;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return list of acceptable enum values
	 * @return list of acceptable enum values
	 */
	public List<String> getEnumValues() {
		return mEnumValues;
	}

	/**
	 * init list of acceptable enum values
	 * @param pEnumValues list of acceptable enum values
	 */
	public void setEnumValues(List<String> pEnumValues) {
		mEnumValues = pEnumValues;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {

		if(isAllowNull() && StringUtils.isBlank((String) pValue)) {
			return true;
		} else {
			if(isErrorNullInput(pFieldName, pValue)) {
				return false;
			}
		}

		if (getEnumValues() == null) {
			setValidationMessage("{"+pFieldName+"} no enum value to check is specified.");
			return false;
		}

		String value = (String)pValue;
		boolean isValid = getEnumValues().contains(value);
		if(!isValid) {
			setValidationMessage(getErrorMsg(pFieldName, pValue));
		}
		return isValid;
    }

	/**
	 * return formatted error validation message
	 * @param pFieldName field name
	 * @param pValue field value
	 * @return formatted error validation message
	 */
	private String getErrorMsg(String pFieldName, Object pValue) {
		StringBuilder msg = new StringBuilder();
		msg.append("{").append(pFieldName).append(":").append(pValue);
		msg.append("} specified value is not in allowed list [");
		for(String enumVal : getEnumValues()) {
			msg.append(enumVal).append(",");
		}
		msg.setCharAt(msg.length()-1, ']');
		return msg.toString();
	}
}
