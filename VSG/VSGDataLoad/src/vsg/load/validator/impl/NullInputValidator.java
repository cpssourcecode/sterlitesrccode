package vsg.load.validator.impl;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public class NullInputValidator extends ValidatorImpl {

	/**
	 * allow null flag
	 */
	private boolean mAllowNull = false;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init allow null input values
	 * @param pAllowNull allow null input values
	 */
	@Override
	public void setAllowNull(boolean pAllowNull) {
		mAllowNull = pAllowNull;
	}

	/**
	 * return allow null input values
	 * @return allow null input values
	 */
	@Override
	public boolean isAllowNull() {
		return mAllowNull;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {
		if (isAllowNull() && StringUtils.isBlank((String) pValue)) {
			return true;
		} else {
			if (isErrorNullInput(pFieldName, pValue)) {
				return false;
			}
		}
		return true;
	}
}
