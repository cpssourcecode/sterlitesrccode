package vsg.load.validator.impl;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public class DoubleValidator extends ValidatorImpl {

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {

		if(isAllowNull() && StringUtils.isBlank((String) pValue)) {
			return true;
		} else {
			if(isErrorNullInput(pFieldName, pValue)) {
				return false;
			}
		}

		try {
			Double.parseDouble((String) pValue);
		} catch (Exception e) {
			setValidationMessage("{"+pFieldName+": "+pValue+"} - is not Double");
			return false;
		}

		return true;
	}
}
