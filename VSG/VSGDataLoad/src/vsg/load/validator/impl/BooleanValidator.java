package vsg.load.validator.impl;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public class BooleanValidator extends ValidatorImpl {
	/**
	 * true value
	 */
	private String mTrueValue;
	/**
	 * false value
	 */
	private String mFalseValue;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init true value
	 * @param trueValue true value
	 */
	public void setTrueValue(String trueValue) {
		mTrueValue = trueValue;
	}

	/**
	 * return true value
	 * @return true value
	 */
	public String getTrueValue() {
		return mTrueValue;
	}

	/**
	 * init false value
	 * @param falseValue false value
	 */
	public void setFalseValue(String falseValue) {
		mFalseValue = falseValue;
	}

	/**
	 * return false value
	 * @return false value
	 */
	public String getFalseValue() {
		return mFalseValue;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {

		if(isAllowNull() && StringUtils.isBlank((String)pValue)) {
			return true;
		} else {
			if(isErrorNullInput(pFieldName, pValue)) {
				return false;
			}
		}

		String value = (String) pValue;
		boolean isValid = value.equalsIgnoreCase(getTrueValue()) || value.equalsIgnoreCase(getFalseValue());
		if(!isValid) {
			setValidationMessage("{"+pFieldName+": "+value+"} is not acceptable boolean: ["+getTrueValue()+", " +
					""+getFalseValue()+"]");
		}
		return isValid;
	}
}
