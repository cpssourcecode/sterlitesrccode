package vsg.load.validator.impl;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public class RegexpValidator extends ValidatorImpl {

	/**
	 * regexp to use for input value validation
	 */
	private String mRegexp;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return regexp to use for input value validation
	 * @return regexp to use for input value validation
	 */
	public String getRegexp() {
		return mRegexp;
	}

	/**
	 * init regexp to use for input value validation
	 * @param pRegexp regexp to use for input value validation
	 */
	public void setRegexp(String pRegexp) {
		mRegexp = pRegexp;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {

		if(isAllowNull() && StringUtils.isBlank((String) pValue)) {
			return true;
		} else {
			if(isErrorNullInput(pFieldName, pValue)) {
				return false;
			}
		}

		String value = (String) pValue;

		boolean isValid = value.matches(getRegexp());
		if(!isValid) {
			setValidationMessage("{"+pFieldName+": "+value+"} - regexp validation is failed");
		}
		return isValid;
	}
}
