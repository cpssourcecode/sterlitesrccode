package vsg.load.validator.impl;

import vsg.load.connection.ISQLConnector;
import vsg.load.loader.Record;
import vsg.load.loader.impl.SQLLoader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmitry Golubev
 */
public class ColumnExistSQLValidator extends ValidatorImpl {
	/**
	 * parent sql loader
	 */
	private SQLLoader mParentLoader;
	/**
	 * connector to search table
	 */
	private ISQLConnector mConnector;
	/**
	 * table name
	 */
	private String mTableName;

	/**
	 * table id name
	 */
	private String mTableColumnName;
	/**
	 * sql exist id
	 */
	protected String mSqlExistId;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return parent sql loader
	 *
	 * @return parent sql loader
	 */
	public SQLLoader getParentLoader() {
		return mParentLoader;
	}

	/**
	 * init parent sql loader
	 *
	 * @param pParentLoader parent sql loader
	 */
	public void setParentLoader(SQLLoader pParentLoader) {
		mParentLoader = pParentLoader;
	}

	/**
	 * return table name
	 *
	 * @return table name
	 */
	public String getTableName() {
		return mTableName;
	}

	/**
	 * init table name
	 *
	 * @param pTableName table name
	 */
	public void setTableName(String pTableName) {
		mTableName = pTableName;
	}

	/**
	 * return table id name
	 *
	 * @return table id name
	 */
	public String getTableColumnName() {
		return mTableColumnName;
	}

	/**
	 * init table id name
	 *
	 * @param pTableColumnName table id name
	 */
	public void setTableColumnName(String pTableColumnName) {
		mTableColumnName = pTableColumnName;
	}

	public ISQLConnector getConnector() {
		return mConnector;
	}

	public void setConnector(ISQLConnector pConnector) {
		mConnector = pConnector;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue     value to be checked
	 * @param pFieldName field name
	 * @param pRecord    record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {
		if (pValue != null) {
			String recordId = (String) pValue;
			boolean exist = false;
			try (Connection connection = getConnection();
				 PreparedStatement ps = connection.prepareStatement(getSqlExistColumn());) {
				ps.setString(1, recordId);
				try (ResultSet rs = ps.executeQuery();) {
					exist = rs.next();
					rs.close();
					ps.close();
					closeConnection(connection);
				}
			} catch (Exception e) {
				getLoggerTools().logError(e);
			}
			if (!exist) {
				setValidationMessage(getTableName() + "." + getTableColumnName() + " by " + pFieldName + ": - could not find item [" + recordId + "]");
			}
			return exist;
		} else {
			return false;
		}
	}

	/**
	 * initialize connection
	 *
	 * @return connection
	 */
	protected Connection getConnection() {
		if (getConnector() == null) {
			return getParentLoader().getConnection();
		} else {
			return getConnector().getConnection();
		}
	}

	/**
	 * close connection
	 *
	 * @param pConnection connection
	 * @throws SQLException if error occurs
	 */
	protected void closeConnection(Connection pConnection) throws SQLException {
		if (getConnector() != null) {
			pConnection.close();
		}
	}

	/**
	 * form sql exist
	 *
	 * @return sql exist
	 */
	public String getSqlExistColumn() {
		if (mSqlExistId == null) {
			mSqlExistId = "select * from " + getTableName() + " where " + getTableColumnName() + "=?";
		}
		return mSqlExistId;
	}
}
