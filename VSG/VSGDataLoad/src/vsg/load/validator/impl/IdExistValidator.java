package vsg.load.validator.impl;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.load.connection.impl.Connector;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public class IdExistValidator extends ValidatorImpl {
	/**
	 * repository connector
	 */
	private Connector mConnector;
	/**
	 * item descriptor
	 */
	private String mItemDescriptor;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return connector
	 * @return connector
	 */
	public Connector getConnector() {
		return mConnector;
	}

	/**
	 * init connector
	 * @param pConnector connector
	 */
	public void setConnector(Connector pConnector) {
		mConnector = pConnector;
	}

	/**
	 * return item descriptor
	 * @return item descriptor
	 */
	public String getItemDescriptor() {
		return mItemDescriptor;
	}

	/**
	 * init item descriptor
	 * @param pItemDescriptor item descriptor
	 */
	public void setItemDescriptor(String pItemDescriptor) {
		mItemDescriptor = pItemDescriptor;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record
	 * @return values is value
	 */
	@Override
	public boolean isValid(Object pValue, String pFieldName, Record pRecord) {
		if(pValue!=null) {
			String recordId = (String) pValue;
			boolean exist = false;
			try {
				RepositoryItem item = getConnector().getRepository().getItem(recordId, getItemDescriptor());
				exist = item != null;
			} catch (RepositoryException e) {
				getLoggerTools().logError(e);
			}
			if(!exist) {
				setValidationMessage("{"+pFieldName+":"+getItemDescriptor()+"} - could not find item ["+recordId+"]");
			}
			return exist;
		} else {
			return false;
		}
	}
}
