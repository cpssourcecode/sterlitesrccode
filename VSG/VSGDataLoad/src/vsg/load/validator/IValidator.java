package vsg.load.validator;

import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public interface IValidator {
	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @param pRecord record to validate
	 * @return values is valid
	 */
	boolean isValid(Object pValue, String pFieldName, Record pRecord);

	/**
	 * check if value is valid
	 *
	 * @param pValue value to be checked
	 * @param pFieldName field name
	 * @return values is valid
	 */
	boolean isValid(Object pValue, String pFieldName);
	/**
	 * return validation message
	 * @return validation message
	 */
	String getValidationMessage();

	/**
	 * @return true if record should be skipped by validation, without errors
	 */
	boolean isJustSkip();
}
