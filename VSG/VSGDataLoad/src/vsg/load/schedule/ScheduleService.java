package vsg.load.schedule;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;
import vsg.load.service.IImportService;

/**
 * @author Dmitry Golubev
 */
public class ScheduleService extends SingletonSchedulableService {
	/**
	 * mEnabled
	 */
	private boolean mEnabled = false;

	/**
	 * import service
	 */
	private IImportService mImportService;

	/**
	 * return import service to be scheduled
	 *
	 * @return import service to be scheduled
	 */
	public IImportService getImportService() {
		return mImportService;
	}

	/**
	 * init import service to be scheduled
	 *
	 * @param pImportService import service to be scheduled
	 */
	public void setImportService(IImportService pImportService) {
		mImportService = pImportService;
	}

	/**
	 * perform schedule task
	 *
	 * @param pScheduler    scheduler
	 * @param pScheduledJob scheduled job
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
		if (!isEnabled()) {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("performScheduledTask: ")
						.append(getJobName()).append(" is disabled. ").toString());
			}
			return;
		}

		synchronized (getImportService()) {
			getImportService().service(null);
		}
	}

	/**
	 * force run job once
	 */
	public void runJobOnce() {
		doScheduledTask(null, null);
	}


	/**
	 * Sets new mEnabled.
	 *
	 * @param pEnabled New value of mEnabled.
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * Gets mEnabled.
	 *
	 * @return Value of mEnabled.
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

}
