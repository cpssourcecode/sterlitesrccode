/**
 *
 */
package vsg.load.schedule;

import java.util.Calendar;


import atg.service.scheduler.SingletonSchedulableService;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import vsg.load.service.impl.ImportServiceMultiLoaders;

/**
 * @author Nviz-Kundan
 */

public class ProfileGenderScheduler extends SingletonSchedulableService {

	/**
	 * The m enabled.
	 */
	private boolean mEnabled = false;
	/**
	 * Flag for check Gender/Bday scheduer Call
	 */
	private boolean genFlag;
	/**
	 * Object of ImportServiceMultiLoaders
	 */
	private ImportServiceMultiLoaders mProfileService;

	/**
	 * @return the mProfileService
	 */
	public ImportServiceMultiLoaders getProfileService() {
		return mProfileService;
	}

	/**
	 * @param mProfileService the mProfileService to set
	 */
	public void setProfileService(ImportServiceMultiLoaders mProfileService) {
		this.mProfileService = mProfileService;
	}

	/**
	 * @return the genFlag
	 */
	public boolean isGenFlag() {
		return genFlag;
	}

	/**
	 * @param genFlag the genFlag to set
	 */
	public void setGenFlag(boolean genFlag) {
		this.genFlag = genFlag;
	}

	@Override
	public void doScheduledTask(Scheduler arg0, ScheduledJob arg1) {
		processImpService();
	}

	@Override
	public void performScheduledTask(Scheduler pScheduler, ScheduledJob pJob) {
		if (isEnabled()) {
			setGenFlag(true);
			super.performScheduledTask(pScheduler, pJob);
		} else {
			vlogWarning("ProfileGenderScheduler job not started, reason - disabled flag on.");
		}
	}

	/**
	 * Run job once.
	 */
	public void runJobOnce() {
		performScheduledTask(null, null);
	}

	/**
	 * Process GenderImport in profile.
	 *
	 * @return true, if successful
	 */
	protected boolean processImpService() {
		boolean result = false;
		try {
			getProfileService().service(null);
		} catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return result;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param mEnabled the new enabled
	 */
	public void setEnabled(boolean mEnabled) {
		this.mEnabled = mEnabled;
	}

}
