package vsg.load.service;

import java.util.List;

import atg.repository.Repository;
import vsg.load.info.StatisticsService;
import vsg.load.loader.ILoader;

/**
 * @author Dmitry Golubev
 */
public interface IImportService {
	/**
	 * No import action status
	 */
	public final String NO_ACTION_STATUS = "Idle";

	/**
	 * return loader
	 *
	 * @return loader
	 */
	public ILoader getLoader();

	/**
	 * return items repository for invalidation
	 *
	 * @return items repository
	 */
	public List<Repository> getRepositories();

	/**
	 * return cash invalidation required
	 *
	 * @return cash invalidation required
	 */
	public boolean isCashInvalidate();

	/**
	 * init cash invalidation required
	 *
	 * @param pCashInvalidationRequired cash invalidation required
	 */
	public void setCashInvalidate(boolean pCashInvalidationRequired);

	/**
	 * perform loader work and repository invalidation
	 *
	 * @return statistics for service's loader work
	 */
	public StatisticsService service(List<String> pInitialImportFiles);

    public StatisticsService service();

}
