package vsg.load.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import atg.adapter.gsa.GSARepository;
import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.Repository;
import vsg.commerce.endeca.index.IIndexingTrigger;
import vsg.load.info.StatisticsService;
import vsg.load.loader.ILoader;
import vsg.load.service.IImportService;
import vsg.load.tools.ICacheInvalidateTools;
import vsg.load.tools.ILoggerTools;

/**
 * @author Dmitry Golubev
 */
public class ImportService extends GenericService implements IImportService {

	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;
	/**
	 * cache invalidate on other servers tools
	 */
	private ICacheInvalidateTools mCacheInvalidateTools;
	/**
	 * loader
	 */
	private ILoader mLoader;
	/**
	 * items repository
	 */
	private List<Repository> mRepositories;
	/**
	 * cash invalidation required on other servers flag
	 */
	private boolean mCashInvalidateOnOtherServers = false;
	/**
	 * cash invalidation required flag
	 */
	private boolean mCashInvalidate = true;
	/**
	 * service name
	 */
	private String mServiceName;
	/**
	 * terminate import flag
	 */
	protected boolean mTerminate = false;

	/**
	 * The Indexing message trigger.
	 */
	private IIndexingTrigger mIndexingMessageTrigger;

	/**
	 * current load statistics
	 */
	protected StatisticsService mStatisticsService;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return service name
	 *
	 * @return service name
	 */
	public String getServiceName() {
		return mServiceName;
	}

	/**
	 * init service name
	 *
	 * @param pServiceName service name
	 */
	public void setServiceName(String pServiceName) {
		mServiceName = pServiceName;
	}

	/**
	 * return logger tools
	 *
	 * @return logger tools
	 */
	public ILoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	/**
	 * init logger tools
	 *
	 * @param pLoggerTools logger tools
	 */
	public void setLoggerTools(ILoggerTools pLoggerTools) {
		this.mLoggerTools = pLoggerTools;
	}

	/**
	 * return loader
	 *
	 * @return loader
	 */
	@Override
	public ILoader getLoader() {
		return mLoader;
	}

	/**
	 * init loader
	 *
	 * @param pLoader loader
	 */
	public void setLoader(ILoader pLoader) {
		mLoader = pLoader;
	}

	/**
	 * return cash invalidation required on other servers flag
	 *
	 * @return cash invalidation required on other servers flag
	 */
	public boolean isCashInvalidateOnOtherServers() {
		return mCashInvalidateOnOtherServers;
	}

	/**
	 * init cash invalidation required on other servers flag
	 *
	 * @param pCashInvalidateOnOtherServers cash invalidation required on other servers flag
	 */
	public void setCashInvalidateOnOtherServers(boolean pCashInvalidateOnOtherServers) {
		mCashInvalidateOnOtherServers = pCashInvalidateOnOtherServers;
	}

	/**
	 * return cash invalidation required
	 *
	 * @return cash invalidation required
	 */
	@Override
	public boolean isCashInvalidate() {
		return mCashInvalidate;
	}

	/**
	 * init cash invalidation required
	 *
	 * @param pCashInvalidate cash invalidation required
	 */
	@Override
	public void setCashInvalidate(boolean pCashInvalidate) {
		mCashInvalidate = pCashInvalidate;
	}

	/**
	 * return items repository
	 *
	 * @return items repository
	 */
	@Override
	public List<Repository> getRepositories() {
		return mRepositories;
	}

	/**
	 * init items repository
	 *
	 * @param pRepositories items repository
	 */
	public void setRepositories(List<String> pRepositories) {
		mRepositories = new ArrayList<Repository>();
		Nucleus nucleus = Nucleus.getGlobalNucleus();
		for (String repositoryName : pRepositories) {
			Repository repository = (Repository) nucleus.resolveName(repositoryName);
			mRepositories.add(repository);
		}
	}

	/**
	 * Gets the indexing message trigger.
	 *
	 * @return the indexing message trigger
	 */
	public IIndexingTrigger getIndexingMessageTrigger() {
		return mIndexingMessageTrigger;
	}

	/**
	 * Sets the indexing message trigger.
	 *
	 * @param pIndexingMessageTrigger the new indexing message trigger
	 */
	public void setIndexingMessageTrigger(IIndexingTrigger pIndexingMessageTrigger) {
		mIndexingMessageTrigger = pIndexingMessageTrigger;
	}

	/**
	 * return cache invalidate on other servers tools
	 *
	 * @return cache invalidate on other servers tools
	 */
	public ICacheInvalidateTools getCacheInvalidateTools() {
		return mCacheInvalidateTools;
	}

	/**
	 * init cache invalidate on other servers tools
	 *
	 * @param pCacheInvalidateTools cache invalidate on other servers tools
	 */
	public void setCacheInvalidateTools(ICacheInvalidateTools pCacheInvalidateTools) {
		mCacheInvalidateTools = pCacheInvalidateTools;
	}

	/**
	 * return import service status
	 *
	 * @return import service status
	 */
	public String getStatus() {
		return mStatisticsService != null ? mStatisticsService.getStatus() : NO_ACTION_STATUS;
	}

	public boolean isTerminate() {
		return mTerminate;
	}

	public void setTerminate(boolean pTerminate) {
		mTerminate = pTerminate;
	}

	// -----------------------------------------------------------------------------------------------------------------

    @Override
    public StatisticsService service() {
        return service(null);
    }

	/**
	 * perform loader work and repository invalidation
	 *
	 * @return statistics for service's loader work
	 */
	@Override
	public StatisticsService service(List<String> pInitialImportFiles ) {
		synchronized (this) {
			getLoggerTools().logDebug("Start service " + getServiceName());
			long startTime = Calendar.getInstance().getTimeInMillis();
			StatisticsService statistics = new StatisticsService();
			mStatisticsService = statistics;
			mStatisticsService.setInitialImportFiles(pInitialImportFiles);
			performService(statistics);


			invalidateCaches(statistics);
			if (checkIfRecordsModified(statistics)) {
				getLoggerTools().logDebug("Records modified");
				long endTime = Calendar.getInstance().getTimeInMillis();
				getLoggerTools().logDebug(getTime(getServiceName(), endTime - startTime));
				triggerIndexing(statistics);
			}
			mStatisticsService = null;
			return statistics;
		}
	}

	/**
	 * launch loader to work
	 *
	 * @param pStatistics statistics for service's loader work
	 */
	protected void performService(StatisticsService pStatistics) {
		setTerminate(false);
		getLoader().load(pStatistics);
	}

	/**
	 * invalidate caches if required
	 *
	 * @param pStatistics statistics for service's loader work
	 */
	protected void invalidateCaches(StatisticsService pStatistics) {
		if (isCashInvalidate()) {
			if (checkIfRecordsModified(pStatistics)) {
				mStatisticsService.setStatus(">> Cache invalidation <<");
				for (Repository repository : getRepositories()) {
					if (repository instanceof GSARepository) {
						((GSARepository) repository).invalidateCaches(true);
						postCacheInvalidate();
						if (isCashInvalidateOnOtherServers()) {
							getCacheInvalidateTools().invalidateOtherServersCache(
									((GSARepository) repository).getInvalidationAbsoluteName()
							);
							postCacheInvalidateOtherServers();
						}
						getLoggerTools().logDebug(((GSARepository) repository).getName() + " - caches are invalidated");
					}
				}
			}
		}
	}

	/**
	 * post action after cache invalidate
	 */
	protected void postCacheInvalidate() {
		//
	}

	/**
	 * post action after cache invalidate on other servers
	 */
	protected void postCacheInvalidateOtherServers() {
		//
	}

	/**
	 * check if records where modified
	 *
	 * @param pStatistics statistics for service's loader work
	 * @return records modified
	 */
	protected boolean checkIfRecordsModified(StatisticsService pStatistics) {
		return pStatistics.checkIfRecordsModified();
	}

	/**
	 * format time to string
	 *
	 * @param pMsg  additional message before time
	 * @param pTime time in mills
	 * @return formatted string
	 */
	protected String getTime(String pMsg, long pTime) {
		return String.format(pMsg + ": %d min %d sec.",
				TimeUnit.MILLISECONDS.toMinutes(pTime),
				TimeUnit.MILLISECONDS.toSeconds(pTime) -
						TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(pTime))
		);
	}

	/**
	 * terminate import with fail reason
	 */
	public void terminateImport() {
		setTerminate(true);
		getLoader().terminateImport();
	}

	/**
	 * Trigger indexing with level in <code>pStatistic</code>.
	 *
	 * @param pStatistics the statistics for loaders
	 */
	protected void triggerIndexing(StatisticsService pStatistics) {
		mStatisticsService.setStatus(">> Trigger indexing with level " + pStatistics.getIndexingLevel() + " <<");
		getLoggerTools().logDebug("Start triggerIndexing with level " + pStatistics.getIndexingLevel());
		IIndexingTrigger indexingTrigger = getIndexingMessageTrigger();
		if (indexingTrigger == null) {
			getLoggerTools().logError("No indexingTrigger configured.");
		} else {
			int indexingLevel = pStatistics.getIndexingLevel();
			String name = getServiceName();
			indexingTrigger.triggerIndexing(name, indexingLevel);
		}
	}
}
