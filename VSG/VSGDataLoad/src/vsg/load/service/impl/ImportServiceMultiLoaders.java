package vsg.load.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import atg.nucleus.Nucleus;
import vsg.load.info.StatisticsService;
import vsg.load.loader.ILoader;

/**
 * @author Dmitry Golubev
 */
public class ImportServiceMultiLoaders extends ImportService {
	/**
	 * sorted map of loaders
	 */
	private Map<String, ILoader> mLoaders;

	/**
	 * sorted map of Genloaders
	 */
	private Map<String, ILoader> mGenLoaders;


	private boolean mSaveAuditReport = false;

	private ILoader mAuditReportSaver;

	private boolean mFtpEnabled = false;


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return sorted map of loader to be launched
	 *
	 * @return sorted map of loader to be launched
	 */
	public Map<String, ILoader> getLoaders() {
		return mLoaders;
	}

	/**
	 * init sorted map of loader to be launched
	 *
	 * @param pLoaders sorted map of loader to be launched
	 */
	public void setLoaders(Map<String, String> pLoaders) {
		if (pLoaders == null) {
			mLoaders = null;
		} else {
			mLoaders = new TreeMap<String, ILoader>();
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			for (String fieldName : pLoaders.keySet()) {
				ILoader validator = (ILoader) nucleus.resolveName(pLoaders.get(fieldName));
				mLoaders.put(fieldName, validator);
			}
		}
	}


	protected String formatDate(Date pDate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
		String dateStr = format.format(pDate);
		return dateStr;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * launch loaders to work
	 *
	 * @param pStatistics statistics for service's loaders work
	 */
	@Override
	protected void performService(StatisticsService pStatistics) {
		setTerminate(false);
		for (String orderNumber : mLoaders.keySet()) {
			if (isTerminate()) {
				break;
			}
			ILoader loader = mLoaders.get(orderNumber);
			loader.load(pStatistics);
		}
		setTerminate(false);
	}

	/**
	 * terminate load with fail reason
	 */
	@Override
	public void terminateImport() {
		setTerminate(true);
		for (ILoader loader : getLoaders().values()) {
			loader.terminateImport();
		}
	}

	public boolean isSaveAuditReport() {
		return mSaveAuditReport;
	}

	public void setSaveAuditReport(boolean pSaveAuditReport) {
		mSaveAuditReport = pSaveAuditReport;
	}

	public ILoader getAuditReportSaver() {
		return mAuditReportSaver;
	}

	public void setAuditReportSaver(ILoader pAuditReportSaver) {
		mAuditReportSaver = pAuditReportSaver;
	}

	public boolean isFtpEnabled() {
		return mFtpEnabled;
	}

	public void setFtpEnabled(boolean pFtpEnabled) {
		mFtpEnabled = pFtpEnabled;
	}
}
