package vsg.load.email;

import vsg.notifications.EmailService;

/**
 * @author Dmitry Golubev
 */
public class EmailSettings {
	/**
	 * emailService property
	 */
	private EmailService mEmailService;

	/**
	 * email recipients
	 */
	private String mEmailRecipient = "dgolubev@go-vsg.com";

	//------------------------------------------------------------------------------------------------------------------


	public EmailService getEmailService() {
		return mEmailService;
	}

	public void setEmailService(EmailService pEmailService) {
		mEmailService = pEmailService;
	}

	public String getEmailRecipient() {
		return mEmailRecipient;
	}

	public void setEmailRecipient(String pEmailRecipient) {
		mEmailRecipient = pEmailRecipient;
	}
}
