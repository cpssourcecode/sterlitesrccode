package vsg.load.loader;

/**
 * @author Dmitry Golubev
 */
public interface IRepositoryLoader extends ILoader {
	/**
	 * return item descriptor name
	 * @return item descriptor name
	 */
	public String getItemDescriptor();
}
