package vsg.load.loader;

import vsg.load.info.StatisticsLoader;
import vsg.load.info.StatisticsService;
import vsg.load.tools.IImportTools;
import vsg.load.info.FieldsInfo;
import vsg.load.info.FileInfo;
import vsg.load.file.IReader;
import vsg.load.file.IWriter;
import vsg.load.tools.IParserTools;

/**
 * @author Dmitry Golubev
 */
public interface ILoader {
	/**
	 * top level record name (not child)
	 */
	public String THIS = "THIS";

	/**
	 * return import tools
	 *
	 * @return import tools
	 */
	public IImportTools getImportTools();

	/**
	 * return parse tools from common settings
	 *
	 * @return parse tools
	 */
	public IParserTools getParserTools();

	/**
	 * return fields info from import tools
	 *
	 * @return fields info
	 */
	public FieldsInfo getFieldsInfo();

	/**
	 * insert record
	 *
	 * @param pRecord record to insert
	 */
	public void insert(Record pRecord);

	/**
	 * update record
	 *
	 * @param pRecord record to update
	 */
	public void update(Record pRecord);

	/**
	 * delete record
	 *
	 * @param pRecord record to delete
	 */
	public void delete(Record pRecord);

	/**
	 * sends reader to insert records
	 *
	 * @param pReader import file reader
	 */
	public void insertRecords(IReader pReader);

	/**
	 * sends reader to update records
	 *
	 * @param pReader import file reader
	 */
	public void updateRecords(IReader pReader);

	/**
	 * sends reader to delete records
	 *
	 * @param pReader import file reader
	 */
	public void deleteRecords(IReader pReader);

	/**
	 * find all satisfied files and send them to import
	 *
	 * @param pStatisticsService service statistics
	 */
	public void load(StatisticsService pStatisticsService);

	/**
	 * perform load for file
	 *
	 * @param pFileInfo file info
	 */
	public void load(FileInfo pFileInfo);

	/**
	 * @return loader name
	 */
	public String getLoaderName();

	/**
	 * @return file info
	 */
	public FileInfo getFileInfo();

	/**
	 * return loader statistics
	 *
	 * @return loader statistics
	 */
	public StatisticsLoader getStatistics();

	/**
	 * create new one if it is needed and return import file reader
	 *
	 * @return import file reader
	 */
	public IReader getReader();

	/**
	 * init import file reader
	 *
	 * @param pReader import file reader
	 */
	public void setReader(IReader pReader);

	/**
	 * create new one if it is needed and return error file writer
	 *
	 * @return error file writer
	 */
	public IWriter getErrorWriter();

	/**
	 * create new one if it is needed and return Audit Report file writer
	 *
	 * @return AuditReport file writer
	 */
	public IWriter getAuditReportWriter(String pFileNamePrefix);

	/**
	 * create new one if it is needed and return Audit Report file writer
	 *
	 * @return AuditReport file writer
	 */
	public IWriter getAuditReportWriter(String pFileNamePrefix, String pFileType);

	/**
	 * init error file writer
	 *
	 * @param pAuditReportWriter error file writer
	 */
	public void setAuditReportWriter(IWriter pAuditReportWriter);

	/**
	 * init error file writer
	 *
	 * @param pErrorWriter error file writer
	 */
	public void setErrorWriter(IWriter pErrorWriter);

	/**
	 * terminate import
	 */
	public void terminateImport();
}
