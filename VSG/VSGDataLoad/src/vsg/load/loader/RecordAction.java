package vsg.load.loader;

/**
 * @author Dmitry Golubev
 */
public enum RecordAction {
	CREATE,
	UPDATE,
	DELETE
}
