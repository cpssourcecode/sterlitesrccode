package vsg.load.loader;

/**
 * @author Dmitry Golubev
 */
public class RecordValidationInfo {
	/**
	 * validated records
	 */
	private Record mRecord;
	/**
	 * validation message
	 */
	private String mValidationMsg;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 * @param pRecord validated record
	 * @param pValidationMsg validation message
	 */
	public RecordValidationInfo(Record pRecord, String pValidationMsg) {
		mRecord = pRecord;
		mValidationMsg = pValidationMsg;
	}

	/**
	 * return validated record
	 * @return validated record
	 */
	public Record getRecord() {
		return mRecord;
	}

	/**
	 * return validation message
	 * @return validation message
	 */
	public String getValidationMsg() {
		return mValidationMsg;
	}
}
