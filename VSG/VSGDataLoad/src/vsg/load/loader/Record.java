package vsg.load.loader;

import java.util.HashMap;
import java.util.Map;

import atg.nucleus.Nucleus;
import vsg.load.tools.ILoggerTools;

/**
 * @author Dmitry Golubev
 */
public class Record {
    /**
     * record id
     */
    private String mId;
    /**
     * flag to use record for import
     */
    private boolean mUseToImport = true;
    /**
     * proceeded values
     */
    private Map<String, Object> mValues;
    /**
     * initial values
     */
    private Map<String, String> mValuesInitial;
    /**
     * import index
     */
    private int mIndex;
    /**
     * sub records
     */
    private Map<String, Record> mSubRecords = new HashMap<String, Record>();
    /**
     * record name
     */
    private String mName = "";
    /**
     * parent record
     */
    private Record mParentRecord;

    // ------------------------------------------------------------------------------------------------------------------

    /**
     * constructor with specified id
     *
     * @param pId
     *            id
     */
    public Record(String pId) {
        mId = pId;
    }

    /**
     * return id
     *
     * @return id
     */
    public String getId() {
        return mId;
    }

    /**
     * init id
     *
     * @param pId
     *            id
     */
    public void setId(String pId) {
        mId = pId;
    }

    /**
     * return use record for import flag
     *
     * @return use record for import flag
     */
    public boolean isUseToImport() {
        return mUseToImport;
    }

    /**
     * init use record for import flag
     *
     * @param pUseToImport
     *            use record for import flag
     */
    public void setUseToImport(boolean pUseToImport) {
        mUseToImport = pUseToImport;
    }

    /**
     * return record name
     *
     * @return record name
     */
    public String getName() {
        return mName;
    }

    /**
     * init record name
     *
     * @param pName
     *            record name
     */
    public void setName(String pName) {
        mName = pName;
    }

    /**
     * init initial import values
     *
     * @param pValuesInitial
     *            initial import values
     */
    public void setValuesInitial(Map<String, String> pValuesInitial) {
        mValuesInitial = pValuesInitial;
    }

    /**
     * return initial import values
     *
     * @return initial import values
     */
    public Map<String, String> getValuesInitial() {
        return mValuesInitial;
    }

    /**
     * return processed value by field name
     *
     * @param pFieldName
     *            field name
     * @return processed value by field name
     */
    public Object getValue(String pFieldName) {
        return mValues.get(pFieldName);
    }

    /**
     * return processed values
     *
     * @return processed values
     */
    public Map<String, Object> getValues() {
        return mValues;
    }

    /**
     * init processed values
     *
     * @param pValues
     *            processed values
     */
    public void setValues(Map<String, Object> pValues) {
        mValues = pValues;
    }

    /**
     * return initial value by field name
     *
     * @param pFieldName
     *            field name
     * @return initial value by field name
     */
    public String getValueInitial(String pFieldName) {
        return mValuesInitial.get(pFieldName);
    }

    /**
     * return import index
     *
     * @return import index
     */
    public int getIndex() {
        return mIndex;
    }

    /**
     * init import index
     *
     * @param pIndex
     *            import index
     */
    public void setIndex(int pIndex) {
        mIndex = pIndex;
    }

    /**
     * return sub records
     *
     * @return sub records
     */
    public Map<String, Record> getSubRecords() {
        return mSubRecords;
    }

    /**
     * retrieve subRecord by name
     *
     * @param pSubRecordName
     *            sub record name
     * @return sub record with appropriate name
     */
    public Record getSubRecord(String pSubRecordName) {
        return mSubRecords.get(pSubRecordName);
    }

    /**
     * return parent record if is sub record
     *
     * @return parent record if is sub record
     */
    public Record getParentRecord() {
        return mParentRecord;
    }

    /**
     * init parent record if is sub record
     *
     * @param pParentRecord
     *            parent record if is sub record
     */
    public void setParentRecord(Record pParentRecord) {
        mParentRecord = pParentRecord;
    }

    // ------------------------------------------------------------------------------------------------------------------

    /**
     * return string presentation
     *
     * @return string presentation
     */
    @Override
    public String toString() {
        if ((getValuesInitial() == null || getValuesInitial().size() == 0) && (getValues() == null || getValues().size() == 0)) {
            return super.toString();
        }

        StringBuilder builder = new StringBuilder();
        builder.append("[");

        if (getValuesInitial() != null && !getValuesInitial().isEmpty()) {
            builder.append(" [");
            for (Object fieldVale : getValuesInitial().values()) {
                builder.append(fieldVale).append(",");
            }
            builder.setCharAt(builder.length() - 1, ']');
        }

        if (getValues() != null && !getValues().isEmpty()) {
            builder.append(" [");
            for (Object fieldVale : getValues().values()) {
                builder.append(fieldVale).append(",");
            }
            builder.setCharAt(builder.length() - 1, ']');
        }

        return builder.toString();
    }

    /**
     * override equals to do check by record ids
     *
     * @param obj
     *            object to compare
     * @return objects are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Record) {
            Record eqObj = (Record) obj;
            if (getId() != null && eqObj.getId() != null) {
                return getId().equals(eqObj.getId());
            } else {
                return super.equals(obj);
            }
        } else {
            return false;
        }
    }

    /**
     * override hashCode to do use hashCode of record id
     *
     * @return id hashCode
     */
    @Override
    public int hashCode() {
        if (getId() == null) {
            return super.hashCode();
        }
        return getId().hashCode();
    }

    /** The Logger tools. */
    private ILoggerTools mLoggerTools;

    /**
     * Gets the logger tools.
     *
     * @return the logger tools
     */
    protected ILoggerTools getLoggerTools() {
        if (mLoggerTools == null) {
            mLoggerTools = (ILoggerTools) Nucleus.getGlobalNucleus().resolveName("/vsg/load/tools/LoggerTools");
        }
        return mLoggerTools;
    }

    /**
     * Put field value. Trim field name and provide stack trace to investigate. Temporary solution.
     *
     * @param pFieldName
     *            the field name
     * @param pFieldValue
     *            the field value
     */
    public void putFieldValue(String pFieldName, Object pFieldValue) {
        String trimmedFieldName = pFieldName.trim();
        if (!pFieldName.equals(trimmedFieldName)) {
            getLoggerTools().logDebug("Stack trace for Record.putField value with params: '{0}' := '{1}'", new Object[] { pFieldName, pFieldValue });

            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                getLoggerTools().logDebug(ste.toString());
            }
        }
        getValues().put(trimmedFieldName, pFieldValue);
    }

    /**
     * Put initial field value. Trim field name and provide stack trace to investigate. Temporary solution.
     *
     * @param pFieldName
     *            the field name
     * @param pFieldValue
     *            the field value
     */
    public void putInitialFieldValue(String pFieldName, String pFieldValue) {
        String trimmedFieldName = pFieldName.trim();
        if (!pFieldName.equals(trimmedFieldName)) {
            getLoggerTools().logDebug("Stack trace for Record.putInitialFieldValue value with params: '{0}' := '{1}'",
                            new Object[] { pFieldName, pFieldValue });

            for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
                getLoggerTools().logDebug(ste.toString());
            }
        }
        getValuesInitial().put(trimmedFieldName, pFieldValue);
    }
}
