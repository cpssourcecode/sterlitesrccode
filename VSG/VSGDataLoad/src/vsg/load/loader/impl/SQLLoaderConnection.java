package vsg.load.loader.impl;

import java.sql.Connection;
import java.sql.SQLException;

import vsg.load.connection.ISQLConnector;
import vsg.load.loader.ISQLLoader;

/**
 * @author Dmitry Golubev
 */
public abstract class SQLLoaderConnection extends PostIndexingRequiredLoader implements ISQLLoader {
	/**
	 * db connection
	 */
	private Connection mConnection;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init db connection
	 * @param pConnection db connection
	 */
	protected void setConnection(Connection pConnection) {
		mConnection = pConnection;
	}

	/**
	 * return db connection
	 * @return db connection
	 */
	public Connection getConnection() {
		return mConnection;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * initialize db connection using connector<br/>
	 * connection is set to autoCommit=false
	 * @return success if connection is established
	 */
	protected boolean initConnection() {
		Connection connection = ((ISQLConnector)getConnector()).getConnection();
		if(connection == null) {
			setImportFailed(true);
			return false;
		}

		try {
			if (connection.getAutoCommit()) {
				connection.setAutoCommit(false);
			}
		} catch (SQLException e) {
			logError(e.toString());
		}
		setConnection(connection);
		return true;
	}

	/**
	 * commit connection
	 * @throws SQLException if error occurs
	 */
	protected void commit() throws SQLException {
		getConnection().commit();
	}

	/**
	 * rollback connection if there was any error during import
	 */
	protected void rollbackConnection() {
		if(getConnection()!=null) {
			try {
				getConnection().rollback();
			} catch (SQLException e) {
				logError(e);
			}
		}
	}

	/**
	 * close db connection
	 * @throws SQLException if error occurs
	 */
	protected void closeConnection() throws SQLException {
		if(getConnection()!=null) {
			getConnection().close();
		}
	}

	/**
	 * process exception base exceptionCatch call and connection rollback
	 * @param e exception
	 */
	@Override
	protected void exceptionCatch(Exception e) {
		super.exceptionCatch(e);
		rollbackConnection();
	}
}
