package vsg.load.loader.impl;

import java.util.Collection;
import java.util.HashMap;

import vsg.load.loader.IRepositoryLoader;
import vsg.load.loader.Record;
import vsg.load.loader.RecordValidationInfo;
import vsg.load.wrapper.IRepositoryValue;
import atg.adapter.gsa.GSAItem;
import atg.adapter.gsa.GSARepository;
import atg.adapter.version.CurrentVersionItem;
import atg.repository.ItemDescriptorImpl;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryPropertyDescriptor;

/**
 * @author Dmitry Golubev
 */
public class RepositoryLoader extends PostIndexingRequiredLoader implements IRepositoryLoader {
	/**
	 * item descriptor name
	 */
	private String mItemDescriptor;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return item descriptor name
	 * @return item descriptor name
	 */
	@Override
	public String getItemDescriptor() {
		return mItemDescriptor;
	}

	/**
	 * init item descriptor name
	 * @param pItemDescriptor item descriptor name
	 */
	public void setItemDescriptor(String pItemDescriptor) {
		mItemDescriptor = pItemDescriptor;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return repository from connector
	 * @return repository
	 */
	protected MutableRepository getRepository() {
		return (MutableRepository) getConnector().getRepository();
	}

	// ---------- INSERT ----------

	/**
	 * create repository item and put record to processed map of records
	 * @param pRecord record to be inserted
	 */
	@Override
	public void insert(Record pRecord) {
		try {
			HashMap<String, RepositoryItem> insertedItems = insert(pRecord, getRepository(), getItemDescriptor());
			postInsert(pRecord, insertedItems);
		} catch (Exception e) {
			getStatistics().addErrorRecord( new RecordValidationInfo(pRecord, e.getMessage()) );
			logError(e.toString());
		}
	}

	/**
	 * post insert record processing
	 * @param pRecord inserted record
	 * @param insertedItems inserted repository items
	 */
	protected void postInsert(Record pRecord, HashMap<String, RepositoryItem> insertedItems) {
		//
	}

	/**
	 * perform create repository item based on passed record
	 * @param pRecord record to be inserted
	 * @param pRepository items repository
	 * @return map of inserted records
	 * @throws Exception if error occurs
	 */
	protected HashMap<String, RepositoryItem> insert(Record pRecord, Repository pRepository) throws Exception {
		return insert(pRecord, pRepository, getItemDescriptor());
	}

	/**
	 * perform create repository item based on passed record
	 * @param pRecord record to be inserted
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor name
	 * @return map of inserted records
	 * @throws Exception if error occurs
	 */
	protected HashMap<String, RepositoryItem> insert(Record pRecord, Repository pRepository, String pItemDescriptor)
			throws Exception {
		HashMap<String, RepositoryItem> items = new HashMap<String, RepositoryItem>();
		RepositoryItem item = getRepositoryItem(pRecord, pRepository, pItemDescriptor);
		if(pRecord.isUseToImport() && insertCheckItem(pRecord, item)) {
			item = insertCreateItem(pRecord, pRepository, pItemDescriptor);
			initRepositoryItem(pRecord, pRepository, (MutableRepositoryItem) item);
			insertProceedItem(pRepository, item);
			items.put(pItemDescriptor, item);
			if(pRecord.getSubRecords()!=null) {
				for(String subRecordName : pRecord.getSubRecords().keySet()) {
					items.putAll(insert(pRecord.getSubRecords().get(subRecordName), pRepository, subRecordName));
				}
			}
		}
		return items;
	}

	/**
	 * get RepositoryItem for insert
	 * @param pRecord record to be inserted
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor name
	 * @return created repository item
	 * @throws RepositoryException if error occurs
	 */
	protected RepositoryItem insertCreateItem(Record pRecord, Repository pRepository, String pItemDescriptor)
			throws RepositoryException {
		return ((MutableRepository)pRepository).createItem(pRecord.getId(), pItemDescriptor);
	}

	/**
	 * process created repository item
	 * @param pRepository items repository
	 * @param pItem created repository item
	 * @throws RepositoryException if error occurs
	 */
	protected void insertProceedItem(Repository pRepository, RepositoryItem pItem) throws RepositoryException {
		((GSARepository)pRepository).addItemOnCommit((MutableRepositoryItem) pItem);
		getStatistics().incrementInsert();
	}

	/**
	 * check for insert record processing on repository item
	 * @param pRecord record to be inserted
	 * @param pItem created repository item
	 * @return validation passed
	 */
	protected boolean insertCheckItem(Record pRecord, RepositoryItem pItem) {
		if(pItem != null) {
			getStatistics().addErrorRecord( new RecordValidationInfo(pRecord, "Record already exist in DB") );
			return false;
		}
		return true;
	}

	// ---------- UPDATE ----------

	/**
	 * update repository item
	 * @param pRecord record to be updated
	 */
	@Override
	public void update(Record pRecord) {
		try {
			update(pRecord, getRepository(), getItemDescriptor());
		} catch (Exception e) {
			getStatistics().addErrorRecord( new RecordValidationInfo(pRecord, e.getMessage()) );
			logError(e.toString());
		}
	}

	/**
	 * perform update repository item based on passed record
	 * @param pRecord record to be updated
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor name
	 * @return map of updated records
	 * @throws Exception if error occurs
	 */
	protected HashMap<String, RepositoryItem> update(Record pRecord, Repository pRepository, String pItemDescriptor)
			throws Exception {
		HashMap<String, RepositoryItem> items = new HashMap<String, RepositoryItem>();
		RepositoryItem item = getRepositoryItem(pRecord, pRepository, pItemDescriptor);
		if(pRecord.isUseToImport() && updateCheckItem(pRecord, item)) {
			initRepositoryItem(pRecord, getRepository(), (MutableRepositoryItem) item);
			getRepository().updateItem((MutableRepositoryItem) item);
			getStatistics().incrementUpdate();
			items.put(pItemDescriptor, item);
			if(pRecord.getSubRecords()!=null) {
				for(String subRecordName : pRecord.getSubRecords().keySet()) {
					items.putAll(update(pRecord.getSubRecords().get(subRecordName), pRepository, subRecordName));
				}
			}
		}
		return items;
	}

	/**
	 * check for update record processing on repository item
	 * @param pRecord record to be updated
	 * @param pItem retrieved from repository item
	 * @return validation passed
	 */
	protected boolean updateCheckItem(Record pRecord, RepositoryItem pItem) {
		if(pItem == null) {
			getStatistics().addErrorRecord( new RecordValidationInfo(pRecord, "Record does not exist in DB") );
			return false;
		}
		return true;
	}

	// ---------- DELETE ----------

	/**
	 * delete repository item
	 * @param pRecord record to be deleted
	 */
	@Override
	public void delete(Record pRecord) {
		try {
			delete(pRecord, getRepository(), getItemDescriptor());
		} catch (Exception e) {
			getStatistics().addErrorRecord( new RecordValidationInfo(pRecord, e.getMessage()) );
			logError(e.toString());
		}
	}

	/**
	 * perform delete repository item based on passed record
	 * @param pRecord record to be delete
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor name
	 * @throws Exception if error occurs
	 */
	protected void delete(Record pRecord, Repository pRepository, String pItemDescriptor) throws Exception {
		RepositoryItem item = getRepositoryItem(pRecord, pRepository, pItemDescriptor);
		if(pRecord.isUseToImport() && updateCheckItem(pRecord, item)) {
			try {
				((MutableRepository)pRepository).removeItem(pRecord.getId(), pItemDescriptor);
			} catch (RepositoryException e) {
				getStatistics().addErrorRecord( new RecordValidationInfo(pRecord, "Unable to delete record from DB: " +
						""+e.toString())
				);
			}
			getStatistics().incrementDelete();
		}
		if(pRecord.getSubRecords()!=null) {
			for(String subRecordName : pRecord.getSubRecords().keySet()) {
				delete(pRecord.getSubRecords().get(subRecordName), pRepository, subRecordName);
			}
		}
	}

	// ---------- ITEM INIT ----------

	/**
	 * init repository item using record
	 * @param pRecord record for RepositoryItem initializing
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @throws Exception if error occurs
	 */
	protected void initRepositoryItem(Record pRecord, Repository pRepository, MutableRepositoryItem pItem)
			throws Exception {
		for(String fieldName : pRecord.getValues().keySet()) {
			if(fieldName.equals( getFieldsInfo().getFieldId() )) {
				continue;
			}
			initPropertyValue(pRepository, pItem, fieldName, pRecord.getValue(fieldName));
		}
	}

	/**
	 * init repository item with property value
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pProperty property name
	 * @param pPropertyValue property value
	 * @throws Exception if error occurs
	 */
	protected void initPropertyValue(Repository pRepository, MutableRepositoryItem pItem, String pProperty,
									 Object pPropertyValue) throws Exception {
		initItemPropertyValue(pRepository, pItem, pProperty, pPropertyValue);
	}

	/**
	 * repository item init with specified property value
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pProperty property name
	 * @param pPropertyValue property value
	 * @throws Exception if error occurs
	 */
	public void initItemPropertyValue(Repository pRepository, MutableRepositoryItem pItem, String pProperty,
									  Object pPropertyValue) throws Exception {
		if (pPropertyValue instanceof IRepositoryValue) {
			pPropertyValue = ((IRepositoryValue) pPropertyValue).getValue(pRepository);
		}
		setPropertyValue(pItem, pProperty, pPropertyValue);
	}

	/**
	 * update item property
	 * @param pItem catalog item
	 * @param pPropertyName property name
	 * @param pPropertyValue property value
	 */
	public void setPropertyValue(MutableRepositoryItem pItem, String pPropertyName,
								  Object pPropertyValue) {
		if(pItem instanceof CurrentVersionItem && pPropertyValue!=null && pPropertyValue instanceof Collection) {
			CurrentVersionItem currItem = (CurrentVersionItem) pItem;
			GSAItem gsaItem = currItem.getVersionItem(true, true);
			RepositoryItemDescriptor itemDescr = gsaItem.getItemDescriptor();
			RepositoryPropertyDescriptor p =
					((ItemDescriptorImpl) itemDescr).getRepositoryPropertyDescriptor(pPropertyName);
			p.setPropertyValue(gsaItem, pPropertyValue);
		} else {
			pItem.setPropertyValue( pPropertyName, pPropertyValue );
		}
	}

	/**
	 * return repository item based on record
	 * @param pRecord processing record
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor
	 * @return found repository item
	 * @throws RepositoryException if error occurs
	 */
	protected RepositoryItem getRepositoryItem(Record pRecord, Repository pRepository,
											   String pItemDescriptor) throws RepositoryException {
		return pRepository.getItem(pRecord.getId(), pItemDescriptor);
	}

}
