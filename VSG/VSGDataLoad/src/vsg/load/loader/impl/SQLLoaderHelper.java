package vsg.load.loader.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import vsg.load.info.FieldsInfo;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public class SQLLoaderHelper extends GenericService {
    /**
     * insert sql set former
     */
    private SQLSetFormer sqlInsertSetFormer;
    /**
     * update sql set former
     */
    private SQLSetFormer sqlUpdateSetFormer;
    /**
     * delete sql set former
     */
    private SQLSetFormer sqlDeleteSetFormer;

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * add batch to insert PreparedStatement
     *
     * @param pRecord
     *            record to be inserted
     * @param pStatementInsert
     *            statement for insert sql
     * @param pFieldsInfo
     *            fields info
     * @throws Exception
     *             if error occurs
     */
    public void addBatchInsert(Record pRecord, PreparedStatement pStatementInsert, FieldsInfo pFieldsInfo) throws Exception {
        int i = 1;
        initStatementObject(pStatementInsert, i, pRecord.getId());
        i++;
        String fieldIdName = pFieldsInfo.getFieldId(pRecord.getName());
        Map<String, String> fieldToField = null;
        if (!StringUtils.isBlank(pRecord.getName())) {
            fieldToField = pFieldsInfo.getFieldsToField(pRecord.getName());
        }

        for (String fieldName : pRecord.getValues().keySet()) {
            if (fieldName.equals(fieldIdName) /* || StringUtils.isBlank(getSqlField(pFieldsInfo, fieldName)) */) {
                continue;
            }
            if (!StringUtils.isBlank(pRecord.getName())) {
                if (fieldToField != null) {
                    String otherField = fieldToField.get(pRecord.getName() + "." + fieldName);
                    if (otherField != null && otherField.equals(fieldIdName)) {
                        continue;
                    }
                }
            }
            Object fieldValue = pRecord.getValue(fieldName);
            initStatementObject(pStatementInsert, i, fieldValue);
            i++;
        }
        vlogDebug("Adding Record :: {0} for {1} to batch for insert SQL :: {2}", pRecord, pRecord.getName(), pStatementInsert.toString());
        pStatementInsert.addBatch();
    }

    /**
     * add batch to update PreparedStatement
     *
     * @param pRecord
     *            record to be updated
     * @param pStatementUpdate
     *            statement for update sql
     * @param pFieldsInfo
     *            fields info
     * @throws Exception
     *             if error occurs
     */
    public void addBatchUpdate(Record pRecord, PreparedStatement pStatementUpdate, FieldsInfo pFieldsInfo) throws Exception {
        int i = 1;

        for (String fieldName : pRecord.getValues().keySet()) {
            if (fieldName.equals(pFieldsInfo.getFieldId(pRecord.getName())) /*
                                                                             * || StringUtils.isBlank(getSqlField(pFieldsInfo, fieldName))
                                                                             */
            ) {
                continue;
            }
            Object fieldValue = pRecord.getValue(fieldName);
            initStatementObject(pStatementUpdate, i, fieldValue);
            i++;
        }

        initStatementObject(pStatementUpdate, i, pRecord.getId());
        vlogDebug("Adding Record :: {0} for {1} to batch for update SQL :: {2}", pRecord, pRecord.getName(), pStatementUpdate.toString());
        pStatementUpdate.addBatch();
    }

    /**
     * add batch to delete PreparedStatement
     *
     * @param pRecord
     *            record to be deleted
     * @param pStatementDelete
     *            statement for delete sql
     * @throws Exception
     *             if error occurs
     */
    public void addBatchDelete(Record pRecord, PreparedStatement pStatementDelete) throws Exception {
        initStatementObject(pStatementDelete, 1, pRecord.getId());
        vlogDebug("Adding Record :: {0} for {1} to batch for delete SQL :: {2}", pRecord, pRecord.getName(), pStatementDelete.toString());
        pStatementDelete.addBatch();       
    }

    /**
     * return list of exist ids from current set of processing records
     *
     * @param pConnection
     *            db connection
     * @param pFieldsInfo
     *            fields info
     * @param pRecords
     *            recors for check
     * @return list of exist ids from current set of processing records
     * @throws SQLException
     *             if error occurs
     */
    public Set<String> getExistIds(Connection pConnection, FieldsInfo pFieldsInfo, Set<Record> pRecords) throws SQLException {
        Set<String> existRecordIds = new HashSet<String>();
        Set<Record> mainRecords = new HashSet<Record>();

        for (Record currentRecord : pRecords) {
            vlogDebug("Current Record ID {0} :: current record to string {1}", currentRecord.getId(),currentRecord.toString());
            if (currentRecord.getParentRecord() == null) { // main record
                mainRecords.add(currentRecord);
            }
        }

        if (mainRecords.size() > 0) {
            String sqlSelectAlreadyExist = formSqlSelectExistIds(pFieldsInfo, mainRecords.size());
            vlogDebug("sqlSelectAlreadyExist SQL Query {0}", sqlSelectAlreadyExist);
            // System.out.println("sqlSelectAlreadyExist :: " + sqlSelectAlreadyExist);
            // System.out.println("pRecords.size() :: " + pRecords.size());
            // System.out.println("mainRecords.size() :: " + mainRecords.size());
            // System.out.println("pRecords :: " + pRecords);
            // System.out.println("mainRecords :: " + mainRecords);

            try (PreparedStatement statement = pConnection.prepareStatement(sqlSelectAlreadyExist);) {
                int i = 1;
                for (Record record : mainRecords) {
                    initStatementObject(statement, i, record.getId());
                    i++;
                }
                try (ResultSet rs = statement.executeQuery();) {
                    while (rs.next()) {
                        existRecordIds.add(rs.getString(1));
                    }
                    return existRecordIds;
                }
            }
        }
        return existRecordIds;
    }

    /**
     * init statement with passed value
     *
     * @param pStatement
     *            prepared statement to be inited
     * @param pIndex
     *            column index
     * @param pValue
     *            column value
     * @throws SQLException
     *             if error occurs
     */
    private void initStatementObject(PreparedStatement pStatement, int pIndex, Object pValue) throws SQLException {
        if (pValue instanceof String) {
            pStatement.setString(pIndex, (String) pValue);
        } else if (pValue instanceof Boolean) {
            pStatement.setBoolean(pIndex, (Boolean) pValue);
        } else if (pValue instanceof Integer) {
            pStatement.setInt(pIndex, (Integer) pValue);
        } else if (pValue instanceof Double) {
            pStatement.setDouble(pIndex, (Double) pValue);
        } else if (pValue instanceof Date) {
            pStatement.setTimestamp(pIndex, new java.sql.Timestamp(((Date) pValue).getTime()));
        } else {
            pStatement.setObject(pIndex, pValue);
        }
    }

    /**
     * return sql for id exist selection
     *
     * @param pFieldsInfo
     *            fields info
     * @param pIdsNumber
     *            number of passed ids
     * @return sql for id exist selection
     */
    public String formSqlSelectExistIds(FieldsInfo pFieldsInfo, int pIdsNumber) {
        if (pFieldsInfo == null) {
            return null;
        }
        StringBuilder sql = new StringBuilder();
        String idSqlName = pFieldsInfo.getFieldsToSqlMap().get(pFieldsInfo.getFieldId());

        sql.append("SELECT ").append(idSqlName).append(" FROM ").append(pFieldsInfo.getTableName()).append(" WHERE ").append(idSqlName).append(" in (");

        for (int i = 0; i < pIdsNumber; i++) {
            sql.append("?,");
        }
        sql.setCharAt(sql.length() - 1, ')');
        return sql.toString();
    }

    /**
     * return sql for record insert
     *
     * @param pFieldsInfo
     *            fields info
     * @param pSubRecordName
     *            sub record name
     * @param pTableNam
     *            table name
     * @param pFieldList
     *            column names with values
     * @param pFieldSqlList
     *            column names with specified sql values
     * @return sql for record insert
     */
    private String formSqlInsert(FieldsInfo pFieldsInfo, String pSubRecordName, String pTableNam, Collection<String> pFieldList,
                    Collection<String> pFieldSqlList) {
        StringBuilder sql = new StringBuilder("INSERT INTO ").append(pTableNam).append(" (");
        StringBuilder sqlNames = new StringBuilder(getSqlIdName(pFieldsInfo, pSubRecordName)).append(",");
        StringBuilder sqlValues = new StringBuilder("?,");

        fillSqlPart(sqlNames, sqlValues, pFieldsInfo, pFieldList);

        if (pFieldSqlList != null) {
            for (String fieldName : pFieldSqlList) {
                String sqlFieldName = getSqlField(pFieldsInfo, fieldName);
                /* if(!StringUtils.isBlank(sqlFieldName)) { */
                sqlNames.append(getSqlField(pFieldsInfo, fieldName)).append(",");
                sqlValues.append(pFieldsInfo.getFieldsSql(pSubRecordName).get(fieldName)).append(",");
                /* } */
            }
        }

        sql.append(sqlNames.deleteCharAt(sqlNames.length() - 1)).append(") VALUES (");
        sql.append(sqlValues.deleteCharAt(sqlValues.length() - 1)).append(")");
        return sql.toString();
    }

    /**
     * return sql for record update
     *
     * @param pFieldsInfo
     *            fields info
     * @param pSubRecordName
     *            sub record name
     * @param pTableNam
     *            table name
     * @param pFieldList
     *            column names with values
     * @param pFieldSqlList
     *            column names with specified sql values
     * @return sql for record update
     */
    private String formSqlUpdate(FieldsInfo pFieldsInfo, String pSubRecordName, String pTableNam, Collection<String> pFieldList,
                    Collection<String> pFieldSqlList) {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(pTableNam).append(" SET ");
        for (String pFieldUpdate : pFieldList) {
            String sqlFieldName = getSqlField(pFieldsInfo, pFieldUpdate);
            /* if(!StringUtils.isBlank(sqlFieldName)) { */
            sql.append(sqlFieldName).append("=?,");
            /* } */
        }
        if (pFieldSqlList != null) {
            for (String fieldName : pFieldSqlList) {
                String sqlFieldName = getSqlField(pFieldsInfo, fieldName);
                /* if(!StringUtils.isBlank(sqlFieldName)) { */
                sql.append(sqlFieldName).append("=");
                sql.append(pFieldsInfo.getFieldsSql(pSubRecordName).get(fieldName)).append(",");
                /* } */
            }
        }

        sql.deleteCharAt(sql.length() - 1);
        sql.append(" WHERE ");
        formSqlWhereById(sql, pFieldsInfo, pSubRecordName);
        return sql.toString();
    }

    /**
     * return sql for record delete
     *
     * @param pFieldsInfo
     *            fields info
     * @param pSubRecordName
     *            sub record name
     * @param pTableNam
     *            table name
     * @return sql for record delete
     */
    private String formSqlDelete(FieldsInfo pFieldsInfo, String pSubRecordName, String pTableNam) {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM ").append(pTableNam).append(" WHERE ");
        formSqlWhereById(sql, pFieldsInfo, pSubRecordName);
        return sql.toString();
    }

    /**
     * appens to sql where by id condition
     *
     * @param pSql
     *            sql to be appended
     * @param pFieldsInfo
     *            fields info
     * @param pSubRecordName
     *            sub record name
     */
    private void formSqlWhereById(StringBuilder pSql, FieldsInfo pFieldsInfo, String pSubRecordName) {
        pSql.append(getSqlIdName(pFieldsInfo, pSubRecordName)).append("=?");
    }

    /**
     * return id field sql column name
     *
     * @param pFieldsInfo
     *            fields info
     * @param pSubRecordName
     *            sub record name
     * @return id field sql column name
     */
    private String getSqlIdName(FieldsInfo pFieldsInfo, String pSubRecordName) {
        if (!StringUtils.isBlank(pSubRecordName)) {
            if (pFieldsInfo.getSubRecordFieldIds() != null) {
                String specifiedSubRecordFieldId = pFieldsInfo.getSubRecordFieldIds().get(pSubRecordName);
                if (specifiedSubRecordFieldId != null) {
                    return getSqlField(pFieldsInfo, specifiedSubRecordFieldId);
                }
            }
            Map<String, String> fieldsToField = pFieldsInfo.getFieldsToField(pSubRecordName);
            if (fieldsToField != null && fieldsToField.containsValue(pFieldsInfo.getFieldId())) {
                for (String fieldName : fieldsToField.keySet()) {
                    if (pFieldsInfo.getFieldId().equals(fieldsToField.get(fieldName))) {
                        return getSqlField(pFieldsInfo, fieldName);
                    }
                }
            }
        }
        return getSqlField(pFieldsInfo, pFieldsInfo.getFieldId());
    }

    /**
     * return field sql column name
     *
     * @param pFieldsInfo
     *            fields info
     * @param pFieldName
     *            field name
     * @return field sql column name
     */
    private String getSqlField(FieldsInfo pFieldsInfo, String pFieldName) {
        return pFieldsInfo.getFieldsToSqlMap().get(pFieldName);
    }

    /**
     * return map of formed insert sql with record name as key
     *
     * @param pFieldsInfo
     *            fields info
     * @return map of formed insert sql
     */
    public Map<String, String> getSqlInserts(FieldsInfo pFieldsInfo) {
        if (pFieldsInfo == null) {
            return null;
        }
        if (sqlInsertSetFormer == null) {
            sqlInsertSetFormer = new SQLSetFormer() {
                @Override
                public String formSql(FieldsInfo pFieldsInfo, String pRecordName, String pTableName, Collection<String> pFieldList,
                                Collection<String> pFieldSqlList) {

                    String insertStmt = formSqlInsert(pFieldsInfo, pRecordName, pTableName, pFieldList, pFieldSqlList);
                    if (isLoggingDebug()) {
                        logDebug("insert stmt is -" + insertStmt);
                    }
                    return insertStmt;
                }
            };
        }
        return sqlInsertSetFormer.getSQLs(pFieldsInfo);
    }

    /**
     * return map of formed update sql with record name as key
     *
     * @param pFieldsInfo
     *            fields info
     * @return map of formed update sql
     */
    public Map<String, String> getSqlUpdates(FieldsInfo pFieldsInfo) {
        if (pFieldsInfo == null) {
            return null;
        }
        if (sqlUpdateSetFormer == null) {
            sqlUpdateSetFormer = new SQLSetFormer() {
                @Override
                public String formSql(FieldsInfo pFieldsInfo, String pRecordName, String pTableName, Collection<String> pFieldList,
                                Collection<String> pFieldSqlList) {

                    String updateStmt = formSqlUpdate(pFieldsInfo, pRecordName, pTableName, pFieldList, pFieldSqlList);
                    if (isLoggingDebug()) {
                        logDebug("update stmt is -" + updateStmt);
                    }
                    return updateStmt;
                }
            };
        }
        return sqlUpdateSetFormer.getSQLs(pFieldsInfo);
    }

    /**
     * return map of formed delete sql with record name as key
     *
     * @param pFieldsInfo
     *            fields info
     * @return map of formed delete sql
     */
    public Map<String, String> getSqlDeletes(FieldsInfo pFieldsInfo) {
        if (pFieldsInfo == null) {
            return null;
        }
        if (sqlDeleteSetFormer == null) {
            sqlDeleteSetFormer = new SQLSetFormer() {
                @Override
                public String formSql(FieldsInfo pFieldsInfo, String pRecordName, String pTableName, Collection<String> pFieldList,
                                Collection<String> pFieldSqlList) {
                    String deleteStmt = formSqlDelete(pFieldsInfo, pRecordName, pTableName);
                    if (isLoggingDebug()) {
                        logDebug("delete stmt is -" + deleteStmt);
                    }
                    return deleteStmt;
                }
            };
        }
        return sqlDeleteSetFormer.getSQLs(pFieldsInfo);
    }

    /**
     * form sqls parts with names and values
     *
     * @param pSqlNames
     *            sql part for name forming
     * @param pSqlValues
     *            sql part for value forming
     * @param pFieldsInfo
     *            fields info
     * @param pFieldNames
     *            fields name
     */
    private void fillSqlPart(StringBuilder pSqlNames, StringBuilder pSqlValues, FieldsInfo pFieldsInfo, Collection<String> pFieldNames) {
        for (String fieldName : pFieldNames) {
            String sqlFieldName = getSqlField(pFieldsInfo, fieldName);
            /* if(!StringUtils.isBlank(sqlFieldName)) { */
            pSqlNames.append(sqlFieldName).append(",");
            pSqlValues.append("?,");
            /* } */
        }
    }

    /*
     * ID,NAME, EMAIL,LOCATION, REVIEW_TITLE,REVIEW, REVIEW_PROS,REVIEW_CONS, RATING,IS_RECOMMENDED, STATE,REVIEW_TIME, IS_VERIF_BUYER,SITE_ID, PRODUCT_ID
     */
}
