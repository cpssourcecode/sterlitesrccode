package vsg.load.loader.impl;

import vsg.load.info.FileInfo;
import vsg.load.info.StatisticsLoader;

/**
 * 
 * @author Kate Koshman
 */
public abstract class PostIndexingRequiredLoader extends Loader {
	
	/** The Needed indexing level. */
	private int mNeededIndexingLevel = 0; //EndecaConstants.INDEX_NONE_LEVEL;

	
	/**
	 * Gets the needed indexing level.
	 * 
	 * @return the needed indexing level
	 */
	public int getNeededIndexingLevel() {
		return mNeededIndexingLevel;
	}

	/**
	 * Sets the needed indexing level.
	 * 
	 * @param pNeededIndexingLevel
	 *            the new needed indexing level
	 */
	public void setNeededIndexingLevel(int pNeededIndexingLevel) {
		mNeededIndexingLevel = pNeededIndexingLevel;
	}
	
	@Override
	public void load(FileInfo pFileInfo) {
		super.load(pFileInfo);
		updateIndexingLevel(getStatistics());
	}
	
	/**
	 * Update indexing level if we loaded something.
	 * 
	 * @param pStatistics
	 *            the statistics
	 */
	protected void updateIndexingLevel(StatisticsLoader pStatistics) {
		if (pStatistics.checkIfRecordsModified()) {
			pStatistics.setIndexingLevel(getNeededIndexingLevel());
		}
	}
}
