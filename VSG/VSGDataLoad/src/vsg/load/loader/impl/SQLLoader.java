package vsg.load.loader.impl;

import java.sql.BatchUpdateException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import vsg.load.loader.Record;
import vsg.load.loader.RecordAction;
import vsg.load.loader.RecordValidationInfo;

/**
 * @author Dmitry Golubev
 */
public class SQLLoader extends SQLLoaderConnection {
    /**
     * sql loader helper
     */
    private SQLLoaderHelper mHelper;
    /**
     * map of insert record-statements
     */
    private Map<String, PreparedStatement> mStatementInserts = new HashMap<String, PreparedStatement>();
    /**
     * map of update record-statements
     */
    private Map<String, PreparedStatement> mStatementUpdates = new HashMap<String, PreparedStatement>();
    /**
     * map of delete record-statements
     */
    private Map<String, PreparedStatement> mStatementDeletes = new HashMap<String, PreparedStatement>();
    /**
     * map of insert record-sql
     */
    private Map<String, String> mSqlInserts = null;
    /**
     * map of update record-sql
     */
    private Map<String, String> mSqlUpdates = null;
    /**
     * map of delete record-sql
     */
    private Map<String, String> mSqlDeletes = null;

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * return helper
     *
     * @return helper
     */
    public SQLLoaderHelper getHelper() {
        return mHelper;
    }

    /**
     * init helper
     *
     * @param pHelper
     *            helper
     */
    public void setHelper(SQLLoaderHelper pHelper) {
        mHelper = pHelper;
    }

    /**
     * return map of delete sqls
     *
     * @return map of delete sqls
     */
    public Map<String, String> getSqlDeletes() {
        if (mSqlDeletes == null) {
            mSqlDeletes = getHelper().getSqlDeletes(getFieldsInfo());
        }
        return mSqlDeletes;
    }

    /**
     * return map of update sqls
     *
     * @return map of update sqls
     */
    public Map<String, String> getSqlUpdates() {
        if (mSqlUpdates == null) {
            mSqlUpdates = getHelper().getSqlUpdates(getFieldsInfo());
        }
        return mSqlUpdates;
    }

    /**
     * return map of insert sqls
     *
     * @return map of insert sqls
     */
    public Map<String, String> getSqlInserts() {
        if (mSqlInserts == null) {
            mSqlInserts = getHelper().getSqlInserts(getFieldsInfo());
        }
        return mSqlInserts;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * insert record
     *
     * @param pRecord
     *            record to be inserted
     */
    @Override
    public void insert(Record pRecord) {
        addToCurrentSet(pRecord, RecordAction.CREATE);
    }

    /**
     * update record
     *
     * @param pRecord
     *            record to be updated
     */
    @Override
    public void update(Record pRecord) {
        addToCurrentSet(pRecord, RecordAction.UPDATE);
    }

    /**
     * delete record
     *
     * @param pRecord
     *            record to be deleted
     */
    @Override
    public void delete(Record pRecord) {
        addToCurrentSet(pRecord, RecordAction.DELETE);
    }

    /**
     * add new record to current set of proceeded records with duplicate check
     *
     * @param pRecord
     *            importing record
     * @param pRecordAction
     *            record action
     */
    private void addToCurrentSet(Record pRecord, RecordAction pRecordAction) {
        if (!getStatistics().addRecord(pRecord, pRecordAction)) { // add to current records to perform batch
            getStatistics().addErrorRecord(new RecordValidationInfo(pRecord, "Duplicate record in import file"));
        }
    }

    /**
     * fill batch on delete PreparedStatement with current set of proceeded records
     */
    private void fillBatchDelete() {
        try {
            for (Record pRecord : getStatistics().getRecordsForDelete()) {
                addBatchDelete(pRecord);
            }
            getStatistics().incrementDelete(getStatistics().getRecordsForDelete().size());
        } catch (Exception e) {
            exceptionCatch(e);
        }
    }

    /**
     * fill batch on insert PreparedStatement with current set of proceeded records
     */
    private void fillBatchInsert() {
        try {
            Set<String> existRecordIds = getExistRecordIds(RecordAction.CREATE);

            try {
                for (Record record : getStatistics().getRecordsForCreate()) {
                    if (existRecordIds.contains(record.getId())) {
                        getStatistics().addErrorRecord(new RecordValidationInfo(record, "Record already exist in DB"));
                    } else {
                        addBatchInsert(record);
                        getStatistics().incrementInsert();
                    }
                }
            } catch (Exception e) {
                getStatistics().incrementError();
                exceptionCatch(e);
            }
        } catch (Exception e) {
            exceptionCatch(e);
        }
    }

    /**
     * fill batch on update PreparedStatement with current set of proceeded records
     */
    private void fillBatchUpdate() {
        try {
            Set<String> existRecordIds = getExistRecordIds(RecordAction.UPDATE);

            try {
                for (Record record : getStatistics().getRecordsForUpdate()) {
                    if (!existRecordIds.contains(record.getId())) {
                        getStatistics().addErrorRecord(new RecordValidationInfo(record, "Record does not exist in DB"));
                    } else {
                        addBatchUpdate(record);
                        getStatistics().incrementUpdate();
                    }
                }
            } catch (Exception e) {
                getStatistics().incrementError();
                exceptionCatch(e);
            }
        } catch (Exception e) {
            exceptionCatch(e);
        }
    }

    /**
     * add record to delete statement batch
     *
     * @param pRecord
     *            record to be deleted
     * @throws Exception
     *             if error occurs
     */
    protected void addBatchDelete(Record pRecord) throws Exception {
        if (pRecord.isUseToImport()) {
            getHelper().addBatchDelete(pRecord, mStatementDeletes.get(getRecordSubName(pRecord)));
        }
        for (Record subRecord : pRecord.getSubRecords().values()) {
            addBatchDelete(subRecord);
        }
    }

    /**
     * add record to insert statement batch
     *
     * @param pRecord
     *            record to be inserted
     * @throws Exception
     *             if error occurs
     */
    protected void addBatchInsert(Record pRecord) throws Exception {
        if (pRecord.isUseToImport()) {
            getHelper().addBatchInsert(pRecord, mStatementInserts.get(getRecordSubName(pRecord)), getFieldsInfo());
        }
        for (Record subRecord : pRecord.getSubRecords().values()) {
            addBatchInsert(subRecord);
        }
    }

    /**
     * add record to update statement batch
     *
     * @param pRecord
     *            record to be updated
     * @throws Exception
     *             if error occurs
     */
    protected void addBatchUpdate(Record pRecord) throws Exception {
        if (pRecord.isUseToImport()) {
            getHelper().addBatchUpdate(pRecord, mStatementUpdates.get(getRecordSubName(pRecord)), getFieldsInfo());
        }
        for (Record subRecord : pRecord.getSubRecords().values()) {
            addBatchUpdate(subRecord);
        }
    }

    /**
     * execute batch on insert statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void executeInsertStatements() throws SQLException {
        try {
            PreparedStatement statement = mStatementInserts.get(THIS);
            int[] mainRecordBatchResult = statement.executeBatch();
            vlogDebug("Main Record Batch Result :: {0}", mainRecordBatchResult);
            if (getFieldsInfo().getSubRecordNames() != null) {
                for (String subRecordName : getFieldsInfo().getSubRecordNames()) {
                    statement = mStatementInserts.get(subRecordName);
                    int[] subRecordBatchResult = statement.executeBatch();
                    vlogDebug("{0} Sub Record Batch Result :: {1}", subRecordName, subRecordBatchResult);
                }
            }
        } catch (BatchUpdateException e) {
            int[] errUpdateResults = e.getUpdateCounts();
            vlogDebug("Error Batch Result :: {0}", errUpdateResults);
            throw e;
        }
    }

    /**
     * execute batch on update statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void executeUpdateStatements() throws SQLException {
        for (PreparedStatement statement : mStatementUpdates.values()) {
            statement.executeBatch();
        }
    }

    /**
     * execute batch on delete statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void executeDeleteStatements() throws SQLException {
        Set<PreparedStatement> statementsLastUse = new HashSet<PreparedStatement>();
        for (String recordName : mStatementDeletes.keySet()) {
            PreparedStatement statement = mStatementDeletes.get(recordName);
            if (getFieldsInfo().isRecordToProcessLast(recordName)) {
                statementsLastUse.add(statement);
            } else {
                statement.executeBatch();
            }
        }
        for (PreparedStatement statement : statementsLastUse) {
            statement.executeBatch();
        }
    }

    /**
     * init statements according to file import action
     *
     * @return successful state
     */
    protected boolean initStatements() {
        try {
            initDeleteStatements();
            initInsertStatements();
            initUpdateStatements();
            /*
             * switch (getFileInfo().getAction()) { case FileInfo.ACTION_DELETE: initDeleteStatements(); break; case FileInfo.ACTION_INSERT:
             * initInsertStatements(); break; case FileInfo.ACTION_UPDATE: initUpdateStatements(); break; }
             */
        } catch (SQLException e) {
            exceptionCatch(e);
            return false;
        }
        return true;
    }

    /**
     * init insert prepared statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void initInsertStatements() throws SQLException {
        mStatementInserts.clear();
        for (String key : getSqlInserts().keySet()) {
            String sql = getSqlInserts().get(key);
            mStatementInserts.put(key, getConnection().prepareStatement(sql));
        }
    }

    /**
     * init update prepared statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void initUpdateStatements() throws SQLException {
        mStatementUpdates.clear();
        for (String key : getSqlUpdates().keySet()) {
            String sql = getSqlUpdates().get(key);
            mStatementUpdates.put(key, getConnection().prepareStatement(sql));
        }
    }

    /**
     * init delete prepared statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void initDeleteStatements() throws SQLException {
        mStatementDeletes.clear();
        for (String key : getSqlDeletes().keySet()) {
            String sql = getSqlDeletes().get(key);
            mStatementDeletes.put(key, getConnection().prepareStatement(sql));
        }
    }

    /**
     * close insert prepared statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void closeInsertStatements() throws SQLException {
        for (PreparedStatement statement : mStatementInserts.values()) {
            statement.close();
        }
    }

    /**
     * close update prepared statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void closeUpdateStatements() throws SQLException {
        for (PreparedStatement statement : mStatementUpdates.values()) {
            statement.close();
        }
    }

    /**
     * close delete prepared statements
     *
     * @throws SQLException
     *             if error occurs
     */
    protected void closeDeleteStatements() throws SQLException {
        for (PreparedStatement statement : mStatementDeletes.values()) {
            statement.close();
        }
    }

    /**
     * open new transaction, open connection and init statements
     *
     * @param pDemarcation
     *            transaction demarcation
     * @throws TransactionDemarcationException
     *             could not open transaction
     */
    @Override
    protected void transactionStart(TransactionDemarcation pDemarcation) throws TransactionDemarcationException {
        initConnection();
        initStatements();
        super.transactionStart(pDemarcation);
    }

    /**
     * execute batches<br>
     * commit<br>
     * close statements<br>
     * close connection<br>
     * track error<br>
     * reset statistics<br>
     * end transaction with check for import failed state for rollback<br>
     *
     * @param pDemarcation
     *            transaction demarcation
     */
    @Override
    protected void transactionEnd(TransactionDemarcation pDemarcation) {
        try {
            fillBatchInsert();
            executeInsertStatements();

            fillBatchUpdate();
            executeUpdateStatements();

            fillBatchDelete();
            executeDeleteStatements();

            commit();

            closeDeleteStatements();
            closeUpdateStatements();
            closeInsertStatements();

            /*
             * switch (getFileInfo().getAction()) { case FileInfo.ACTION_DELETE: fillBatchDelete(); executeDeleteStatements(); commit();
             * closeDeleteStatements(); break; case FileInfo.ACTION_INSERT: fillBatchInsert(); executeInsertStatements(); commit(); closeInsertStatements();
             * break; case FileInfo.ACTION_UPDATE: fillBatchUpdate(); executeUpdateStatements(); commit(); closeUpdateStatements(); break; }
             */
            closeConnection();
            // trackError();
            getStatistics().resetRecordSets();
        } catch (SQLException e) {
            exceptionCatch(e);
        }
        super.transactionEnd(pDemarcation);
    }

    /**
     * return set of exist record ids
     *
     * @return set of exist record ids
     * @throws SQLException
     *             if error occurs
     * @param pRecordAction
     *            supposed records action
     */
    protected Set<String> getExistRecordIds(RecordAction pRecordAction) throws SQLException {
        Set<Record> recordSet = null;
        switch (pRecordAction) {
        case CREATE:
            recordSet = getStatistics().getRecordsForCreate();
            break;
        case UPDATE:
            recordSet = getStatistics().getRecordsForUpdate();
            break;
        }
        if (recordSet == null || recordSet.size() == 0) {
            return Collections.EMPTY_SET;
        }
        return getHelper().getExistIds(getConnection(), getFieldsInfo(), recordSet);
    }

}
