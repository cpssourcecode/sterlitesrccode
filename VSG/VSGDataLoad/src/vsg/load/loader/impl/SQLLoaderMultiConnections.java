package vsg.load.loader.impl;

import vsg.load.connection.ISQLConnector;
import vsg.load.loader.Record;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Dmitry Golubev
 */
public class SQLLoaderMultiConnections extends SQLLoader {
	/**
	 * set of connections
	 */
	private Set<Connection> mConnections = new HashSet<Connection>();
	/**
	 * map of insert record-statements for all connections
	 */
	private Map<String, Set<PreparedStatement>> mStatementInserts = new HashMap<String, Set<PreparedStatement>>();
	/**
	 * map of update record-statements for all connections
	 */
	private Map<String, Set<PreparedStatement>> mStatementUpdates = new HashMap<String, Set<PreparedStatement>>();
	/**
	 * map of delete record-statements for all connections
	 */
	private Map<String, Set<PreparedStatement>> mStatementDeletes = new HashMap<String, Set<PreparedStatement>>();

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return set of db connections
	 * @return set of db connections
	 */
	public Set<Connection> getConnections() {
		return mConnections;
	}

	/**
	 * return map of insert PreparedStatement's with record name as key
	 * @return map of insert PreparedStatement's
	 */
	private Map<String, Set<PreparedStatement>> getStatementInserts() {
		return mStatementInserts;
	}

	/**
	 * return map of update PreparedStatement's with record name as key
	 * @return map of insert PreparedStatement's
	 */
	private Map<String, Set<PreparedStatement>> getStatementUpdates() {
		return mStatementUpdates;
	}

	/**
	 * return map of delete PreparedStatement's with record name as key
	 * @return map of insert PreparedStatement's
	 */
	private Map<String, Set<PreparedStatement>> getStatementDeletes() {
		return mStatementDeletes;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * add batches for update PreparedStatement
	 * @param pRecord record to be used for update
	 * @throws Exception if error occurs
	 */
	@Override
	protected void addBatchUpdate(Record pRecord) throws Exception {
		for(PreparedStatement preparedStatement : getStatementUpdates().get(getRecordSubName(pRecord))) {
			getHelper().addBatchUpdate(pRecord, preparedStatement, getFieldsInfo());
		}
		for(Record subRecord : pRecord.getSubRecords().values()) {
			addBatchUpdate(subRecord);
		}
	}

	/**
	 * add batches for delete PreparedStatement
	 * @param pRecord record to be used for delete
	 * @throws Exception if error occurs
	 */
	@Override
	protected void addBatchDelete(Record pRecord) throws Exception {
		for(PreparedStatement preparedStatement : getStatementDeletes().get(getRecordSubName(pRecord))) {
			getHelper().addBatchDelete(pRecord, preparedStatement);
		}
		for(Record subRecord : pRecord.getSubRecords().values()) {
			addBatchDelete(subRecord);
		}
	}

	/**
	 * add batches for insert PreparedStatement
	 * @param pRecord record to be used for insert
	 * @throws Exception if error occurs
	 */
	@Override
	protected void addBatchInsert(Record pRecord) throws Exception {
		for(PreparedStatement preparedStatement : getStatementInserts().get(getRecordSubName(pRecord))) {
			getHelper().addBatchInsert(pRecord, preparedStatement, getFieldsInfo());
		}
		for(Record subRecord : pRecord.getSubRecords().values()) {
			addBatchInsert(subRecord);
		}
	}

	/**
	 * commit on all connection
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void commit() throws SQLException {
		for(Connection connection : getConnections()) {
			connection.commit();
		}
	}

	/**
	 * initialize db connections using connector<br/>
	 * connections are set to autoCommit=false
	 * @return success if connections are established
	 */
	@Override
	protected boolean initConnection() {
		getConnections().clear();
		List<Connection> connections = ((ISQLConnector)getConnector()).getConnections();
		if(connections == null) {
			setImportFailed(true);
			return false;
		}

		for(Connection connection : connections) {
			try {
				connection.setAutoCommit(false);
			} catch (SQLException e) {
				logError(e.toString());
			}
			getConnections().add(connection);
		}
		return true;
	}

	/**
	 * close db connections
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void closeConnection() throws SQLException {
		for(Connection connection : getConnections()) {
			connection.close();
		}
	}

	/**
	 * rollback connections if there was any error during import
	 */
	@Override
	protected void rollbackConnection() {
		for(Connection connection : getConnections()) {
			try {
				connection.rollback();
			} catch (SQLException e) {
				logError(e);
			}
		}
	}

	/**
	 * execute batch on insert statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void executeInsertStatements() throws SQLException {
		for(Set<PreparedStatement> statementSet : getStatementInserts().values()) {
			for(PreparedStatement statementInsert : statementSet) {
				statementInsert.executeBatch();
			}
		}
	}

	/**
	 * execute batch on update statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void executeUpdateStatements() throws SQLException {
		for(Set<PreparedStatement> statementSet : getStatementUpdates().values()) {
			for(PreparedStatement statementUpdate : statementSet) {
				statementUpdate.executeBatch();
			}
		}
	}

	/**
	 * execute batch on delete statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void executeDeleteStatements() throws SQLException {
		for(Set<PreparedStatement> statementSet : getStatementDeletes().values()) {
			for(PreparedStatement statementDelete : statementSet) {
				statementDelete.executeBatch();
			}
		}
	}

	/**
	 * close insert prepared statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void closeInsertStatements() throws SQLException {
		for(Set<PreparedStatement> statementSet : getStatementInserts().values()) {
			for(PreparedStatement statementInsert : statementSet) {
				statementInsert.close();
			}
		}
	}

	/**
	 * close update prepared statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void closeUpdateStatements() throws SQLException {
		for(Set<PreparedStatement> statementSet : getStatementUpdates().values()) {
			for(PreparedStatement statementUpdate : statementSet) {
				statementUpdate.close();
			}
		}
	}

	/**
	 * close delete prepared statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void closeDeleteStatements() throws SQLException {
		for(Set<PreparedStatement> statementSet : getStatementDeletes().values()) {
			for(PreparedStatement statementDelete :statementSet) {
				statementDelete.close();
			}
		}
	}

	/**
	 * init insert prepared statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void initInsertStatements() throws SQLException {
		getStatementInserts().clear();
		for(Connection connection : getConnections()) {
			for(String key : getSqlInserts().keySet()) {
				String sql = getSqlInserts().get(key);
				Set<PreparedStatement> statementSet = getStatementInserts().get(key);
				if(statementSet == null) {
					statementSet = new HashSet<PreparedStatement>();
					getStatementInserts().put( key, statementSet );
				}
				statementSet.add( connection.prepareStatement(sql) );
			}
		}
	}

	/**
	 * init update prepared statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void initUpdateStatements() throws SQLException {
		getStatementUpdates().clear();
		for(Connection connection : getConnections()) {
			for(String key : getSqlUpdates().keySet()) {
				String sql = getSqlUpdates().get(key);
				Set<PreparedStatement> statementSet = getStatementUpdates().get(key);
				if(statementSet == null) {
					statementSet = new HashSet<PreparedStatement>();
					getStatementUpdates().put( key, statementSet );
				}
				statementSet.add( connection.prepareStatement(sql) );
			}
		}
	}

	/**
	 * init delete prepared statements
	 * @throws SQLException if error occurs
	 */
	@Override
	protected void initDeleteStatements() throws SQLException {
		getStatementDeletes().clear();
		for(Connection connection : getConnections()) {
			for(String key : getSqlDeletes().keySet()) {
				String sql = getSqlDeletes().get(key);
				Set<PreparedStatement> statementSet = getStatementDeletes().get(key);
				if(statementSet == null) {
					statementSet = new HashSet<PreparedStatement>();
					getStatementDeletes().put( key, statementSet );
				}
				statementSet.add( connection.prepareStatement(sql) );
			}
		}
	}
}
