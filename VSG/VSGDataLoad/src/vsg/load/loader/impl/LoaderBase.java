package vsg.load.loader.impl;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import vsg.load.connection.IConnector;
import vsg.load.file.IReader;
import vsg.load.file.IWriter;
import vsg.load.file.csv.CSVBase;
import vsg.load.info.FieldsInfo;
import vsg.load.info.FileInfo;
import vsg.load.info.StatisticsLoader;
import vsg.load.info.StatisticsService;
import vsg.load.loader.CommonSettings;
import vsg.load.loader.ILoader;
import vsg.load.loader.Record;
import vsg.load.tools.IImportTools;
import vsg.load.tools.ILoggerTools;
import vsg.load.tools.IParserTools;
import vsg.load.tools.IReportingTools;

import java.io.File;
import java.util.concurrent.TimeUnit;

/**
 * @author Dmitry Golubev
 */
public abstract class LoaderBase extends GenericService implements ILoader {
	/**
	 * loader name
	 */
	private String mLoaderName;
	/**
	 * is import failed flag
	 */
	private boolean mImportFailed = false;
	/**
	 * import tools
	 */
	private IImportTools mImportTools;
	/**
	 * common settings
	 */
	private CommonSettings mCommonSettings;
	/**
	 * import file reader
	 */
	private IReader mReader;
	/**
	 * import error file writer
	 */
	IWriter mErrorWriter;
	/**
	 * import AuditReport file writer
	 */
	private IWriter mAuditReportWriter;
	/**
	 * repository connector
	 */
	private IConnector mConnector;
	/**
	 * loader statistics
	 */
	private StatisticsLoader mStatistics;
	/**
	 * service statistics
	 */
	private StatisticsService mStatisticsService;
	/**
	 * file info
	 */
	private FileInfo mFileInfo;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check if import failed
	 *
	 * @return import failed
	 */
	protected boolean isImportFailed() {
		return mImportFailed;
	}

	/**
	 * init import failed flag
	 *
	 * @param pImportFailed import failed flag
	 */
	protected void setImportFailed(boolean pImportFailed) {
		mImportFailed = pImportFailed;
		if (pImportFailed) {
			if (getStatistics() == null) {
				setStatistics(new StatisticsLoader(this));
			}
			getStatistics().setFailed();
		}
	}

	@Override
	public void terminateImport() {
		setImportFailed(true);
		getStatistics().setFailReason("Import terminated by user");
	}

	/**
	 * return loader name
	 *
	 * @return loader name
	 */
	public String getLoaderName() {
		return mLoaderName;
	}

	/**
	 * init loader name
	 *
	 * @param pLoaderName loader name
	 */
	public void setLoaderName(String pLoaderName) {
		mLoaderName = pLoaderName;
	}

	/**
	 * return common settings
	 *
	 * @return common settings
	 */
	public CommonSettings getCommonSettings() {
		return mCommonSettings;
	}

	/**
	 * init common settings
	 *
	 * @param pCommonSettings common settings
	 */
	public void setCommonSettings(CommonSettings pCommonSettings) {
		mCommonSettings = pCommonSettings;
	}

	/**
	 * return logger tools from common settings
	 *
	 * @return logger tools
	 */
	public ILoggerTools getLoggerTools() {
		return getCommonSettings().getLoggerTools();
	}

	/**
	 * return reporting tools
	 *
	 * @return reporting tools
	 */
	public IReportingTools getReportingTools() {
		return getCommonSettings().getReportingTools();
	}

	/**
	 * return import tools
	 *
	 * @return import tools
	 */
	@Override
	public IImportTools getImportTools() {
		return mImportTools;
	}

	/**
	 * init import tools
	 *
	 * @param pImportTools import tools
	 */
	public void setImportTools(IImportTools pImportTools) {
		mImportTools = pImportTools;
	}

	/**
	 * return parse tools from common settings
	 *
	 * @return parse tools
	 */
	@Override
	public IParserTools getParserTools() {
		return getCommonSettings().getParserTools();
	}

	/**
	 * return loader statistics
	 *
	 * @return loader statistics
	 */
	@Override
	public StatisticsLoader getStatistics() {
		return mStatistics;
	}

	/**
	 * init loader statistics
	 *
	 * @param pStatistics loader statistics
	 */
	public void setStatistics(StatisticsLoader pStatistics) {
		mStatistics = pStatistics;
	}

	/**
	 * return service statistics
	 *
	 * @return service statistics
	 */
	public StatisticsService getStatisticsService() {
		return mStatisticsService;
	}

	/**
	 * init service statistics
	 *
	 * @param pStatisticsService service statistics
	 */
	public void setStatisticsService(StatisticsService pStatisticsService) {
		mStatisticsService = pStatisticsService;
	}

	/**
	 * return fields info from import tools
	 *
	 * @return fields info
	 */
	@Override
	public FieldsInfo getFieldsInfo() {
		return getImportTools().getFieldsInfo();
	}

	/**
	 * create new one if it is needed and return import file reader
	 *
	 * @return import file reader
	 */
	@Override
	public IReader getReader() {
/*
		if (mReader == null) {
			setReader(getImportTools().getReader(getFileInfo().getFile()));
		}
*/
		return mReader;
	}

	/**
	 * init import file reader
	 *
	 * @param pReader import file reader
	 */
	@Override
	public void setReader(IReader pReader) {
		mReader = pReader;
	}

	/**
	 * create new one if it is needed and return error file writer
	 *
	 * @return error file writer
	 */
	@Override
	public IWriter getErrorWriter() {
		if (mErrorWriter == null) {
			getImportTools().checkErrorFileDir();
			File errorFile = new File(getImportTools().getErrorFileName(getFileInfo().getFile()));
			setErrorWriter(getImportTools().getWriter(errorFile));
		}
		return mErrorWriter;
	}

	/**
	 * create new one if it is needed and return audit report file writer
	 *
	 * @return error file writer
	 */
	@Override
	public IWriter getAuditReportWriter(String pFileNamePrefix) {
		if (mAuditReportWriter == null) {
			getImportTools().checkAuditReportFileDir();
			File auditReportFile = new File(getImportTools().getAuditReportFileName(pFileNamePrefix));
			setAuditReportWriter(getImportTools().getWriter(auditReportFile));
			((CSVBase) mAuditReportWriter).setQuoteChar(CSVBase.NO_QUOTE_CHARACTER);
			// set tab separator
			((CSVBase) mAuditReportWriter).setSeparator(getImportTools().getFileSeparator());
		}
		return mAuditReportWriter;
	}

	/**
	 * create new one if it is needed and return audit report file writer
	 *
	 * @return error file writer
	 */
	@Override
	public IWriter getAuditReportWriter(String pFileNamePrefix, String pFileType) {
		if (mAuditReportWriter == null) {
			getImportTools().checkAuditReportFileDir();
			File auditReportFile = new File(getImportTools().getAuditReportFileName(pFileNamePrefix, pFileType));
			setAuditReportWriter(getImportTools().getWriter(auditReportFile));
			((CSVBase) mAuditReportWriter).setQuoteChar(CSVBase.NO_QUOTE_CHARACTER);
			// set tab separator
			((CSVBase) mAuditReportWriter).setSeparator(getImportTools().getFileSeparator());
		}
		return mAuditReportWriter;
	}

	public void setAuditReportWriter(IWriter pAuditReportWriter) {
		mAuditReportWriter = pAuditReportWriter;
	}

	/**
	 * init error file writer
	 *
	 * @param pErrorWriter error file writer
	 */
	@Override
	public void setErrorWriter(IWriter pErrorWriter) {
		mErrorWriter = pErrorWriter;
	}

	/**
	 * return file info
	 *
	 * @return file info
	 */
	public FileInfo getFileInfo() {
		return mFileInfo;
	}

	/**
	 * init file info
	 *
	 * @param pFileInfo file info
	 */
	protected void setFileInfo(FileInfo pFileInfo) {
		mFileInfo = pFileInfo;
	}

	/**
	 * return connector
	 *
	 * @return connector
	 */
	public IConnector getConnector() {
		return mConnector;
	}

	/**
	 * init connector
	 *
	 * @param pConnector connector
	 */
	public void setConnector(IConnector pConnector) {
		mConnector = pConnector;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * return record name
	 *
	 * @param pRecord import record
	 * @return record name
	 */
	protected String getRecordSubName(Record pRecord) {
		if (StringUtils.isBlank(pRecord.getName())) {
			return "THIS";
		} else {
			return pRecord.getName();
		}
	}


	/**
	 * format time to string
	 *
	 * @param pMsg  additional message before time
	 * @param pTime time in mills
	 * @return formatted string
	 */
	public String getTime(String pMsg, long pTime) {
		return String.format(pMsg + ": %d min %d sec.",
				TimeUnit.MILLISECONDS.toMinutes(pTime),
				TimeUnit.MILLISECONDS.toSeconds(pTime) -
						TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(pTime))
		);
	}

	/**
	 * log info
	 *
	 * @param pMsg message to be logged
	 */
	public void logInfo(String pMsg) {
		if (getLoggerTools().isLoggingInfo()) {
			getLoggerTools().logInfo(getLoaderName() + ": " + pMsg);
		}
	}

	/**
	 * log debug
	 *
	 * @param pMsg message to be logged
	 */
	public void logDebug(String pMsg) {
		if (getLoggerTools().isLoggingDebug()) {
			getLoggerTools().logDebug(getLoaderName() + ": " + pMsg);
		}
	}

	/**
	 * log error
	 *
	 * @param pMsg message to be logged
	 */
	public void logError(String pMsg) {
		if (getLoggerTools().isLoggingError()) {
			getLoggerTools().logError(getLoaderName() + ": " + pMsg);
		}
	}

	/**
	 * log error
	 *
	 * @param pExc exception to be logged
	 */
	protected void logError(Exception pExc) {
		if (getLoggerTools().isLoggingError()) {
			getLoggerTools().logError(pExc);
		}
	}


}
