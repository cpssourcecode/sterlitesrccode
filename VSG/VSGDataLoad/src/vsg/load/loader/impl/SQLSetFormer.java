package vsg.load.loader.impl;

import atg.core.util.StringUtils;
import atg.schema.sql.LinkedHashSet;
import vsg.load.info.FieldsInfo;

import java.util.*;

/**
 * @author Dmitry Golubev
 */
public abstract class SQLSetFormer {
	/**
	 * main method for records sql forming
	 * @param pFieldsInfo fields info
	 * @return map of record name - sql
	 */
	public Map<String, String> getSQLs(FieldsInfo pFieldsInfo) {
		Map<String, String> recordsSql = new HashMap<String, String>();

		recordsSql.put("THIS", formSql(pFieldsInfo, "") );

		if(pFieldsInfo.getSubRecordTables()!=null) {
			for(String subRecordName : pFieldsInfo.getSubRecordTables().keySet()) {
				recordsSql.put( subRecordName, formSql(pFieldsInfo, subRecordName) );
			}
		}

		return recordsSql;
	}

	/**
	 * return formed sql
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 * @return formed sql
	 */
	private String formSql(FieldsInfo pFieldsInfo, String pRecordName) {
		String tableName;
		if(StringUtils.isBlank(pRecordName)) {
			tableName = pFieldsInfo.getTableName();
		} else {
			tableName = pFieldsInfo.getSubRecordTables().get( pRecordName );
		}

		Map<String, String> fieldsSql = pFieldsInfo.getFieldsSql(pRecordName);
		Collection<String> sqlFields = fieldsSql == null ? null : fieldsSql.keySet();

		return formSql(pFieldsInfo, pRecordName, tableName, getSqlUpdateFieldsList(pFieldsInfo, pRecordName),
				sqlFields);
	}

	/**
	 * return update fields list
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 * @return update fields list
	 */
	private List<String> getSqlUpdateFieldsList(FieldsInfo pFieldsInfo, String pRecordName) {
		Map<String, String> fieldsValue = pFieldsInfo.getFieldsValue(pRecordName);

		Set<String> fieldsUpdateSet = new LinkedHashSet();

		loopThroughFields(fieldsUpdateSet, pFieldsInfo.getSqlUpdateFieldsList(pRecordName), pFieldsInfo,
				pRecordName );
		initFieldToField(fieldsUpdateSet, pFieldsInfo, pRecordName);
		if(fieldsValue!=null) {
			loopThroughFields(fieldsUpdateSet, fieldsValue.keySet(), pFieldsInfo, pRecordName );
		}
		List<String> fieldsUpdateList = new ArrayList<String>();
		fieldsUpdateList.addAll( fieldsUpdateSet );
		return fieldsUpdateList;
	}

	/**
	 * fill update list with fields to fields
	 * @param pFieldsUpdateSet update fields set
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 */
	private void initFieldToField(Set<String> pFieldsUpdateSet, FieldsInfo pFieldsInfo, String pRecordName) {
		Map<String, String> fieldsToField = pFieldsInfo.getFieldsToField(pRecordName);
		if(fieldsToField!=null) {
			loopThroughFields(pFieldsUpdateSet, fieldsToField.keySet(), pFieldsInfo, pRecordName );
			if(!StringUtils.isBlank(pRecordName)) {
				for(String fieldName : fieldsToField.keySet()) {
					if(fieldsToField.get(fieldName).equals(pFieldsInfo.getFieldId(pRecordName))) {
						pFieldsUpdateSet.remove(getFieldName(fieldName, pRecordName));
					}
				}
			}
		}
	}

	/**
	 * fill update fields list
	 * @param pUpdateFieldNames update fields list
	 * @param fieldNames proposed new fields to add to update list
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 */
	private void loopThroughFields(Set<String> pUpdateFieldNames, Collection<String> fieldNames,
								   FieldsInfo pFieldsInfo, String pRecordName) {
		for( String fieldName : fieldNames) {
			String fieldNameRes = getFieldName(fieldName, pRecordName);
			if(fieldNameRes.equals(pFieldsInfo.getFieldId(pRecordName))) {
				continue;
			}
			pUpdateFieldNames.add(fieldNameRes);
		}
	}

	/**
	 * return field name according to record name
	 * @param pFieldName field name
	 * @param pRecordName record name
	 * @return field name according to record name
	 */
	private String getFieldName(String pFieldName, String pRecordName) {
		if(!StringUtils.isBlank(pRecordName)) {
			return pFieldName.replace(pRecordName+".", "");
		} else {
			return pFieldName;
		}
	}

	/**
	 * return formed sql
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 * @param pTableName table name
	 * @param pFieldList update fields list
	 * @param pFieldSqlList fields list with specified sql values
	 * @return formed sql
	 */
	public abstract String formSql(FieldsInfo pFieldsInfo, String pRecordName, String pTableName,
								   Collection<String> pFieldList, Collection<String> pFieldSqlList);
}
