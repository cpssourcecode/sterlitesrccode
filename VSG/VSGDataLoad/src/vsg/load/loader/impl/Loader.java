package vsg.load.loader.impl;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import vsg.load.file.IReader;
import vsg.load.info.FileInfo;
import vsg.load.info.StatisticsLoader;
import vsg.load.info.StatisticsService;
import vsg.load.info.ValidationResult;
import vsg.load.loader.ILoader;
import vsg.load.loader.Record;
import vsg.load.loader.RecordValidationInfo;
import vsg.load.tools.impl.LoaderFullFileAnalyzeTools;
import vsg.load.tools.impl.ValidationTools;

import javax.transaction.Status;
import javax.transaction.SystemException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author Dmitry Golubev
 */
public abstract class Loader extends LoaderBase implements ILoader {
	/**
	 * insert records reader
	 */
	private RecordImporter mReaderRecordInsert;
	/**
	 * update records reader
	 */
	private RecordImporter mReaderRecordUpdate;
	/**
	 * delete records reader
	 */
	private RecordImporter mReaderRecordDelete;
	/**
	 * loader limit per transaction
	 */
	private int mLoaderLimitPerTransaction = -1;
	/**
	 * number of lines to skip
	 */
	private int mLineToSkip = 0;
	/**
	 * loader full analyze tools
	 */
	private LoaderFullFileAnalyzeTools mFileAnalyzeTools = new LoaderFullFileAnalyzeTools();

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init number of lines to skip
	 *
	 * @param pLineToSkip number of lines to skip
	 */
	public void setLineToSkip(int pLineToSkip) {
		mLineToSkip = pLineToSkip;
	}

	/**
	 * return number of lines to skip
	 *
	 * @return number of lines to skip
	 */
	public int getLineToSkip() {
		return mLineToSkip;
	}

	/**
	 * creates new one if it's needed and return import reader, which is responsible for insert data
	 *
	 * @return import reader, which is responsible for insert data
	 */
	private RecordImporter getReaderRecordInsert() {
		if (mReaderRecordInsert == null) {
			mReaderRecordInsert = new RecordImporter() {
				/**
				 * process record during load
				 * @param pRecord record for import processing
				 * @throws Exception possible exception
				 */
				@Override
				public void importRecord(Record pRecord) throws Exception {
					insert(pRecord);
				}
			};
		}
		return mReaderRecordInsert;
	}

	/**
	 * creates new one if it's needed and return import reader, which is responsible for update data
	 *
	 * @return import reader, which is responsible for update data
	 */
	private RecordImporter getReaderRecordUpdate() {
		if (mReaderRecordUpdate == null) {
			mReaderRecordUpdate = new RecordImporter() {
				/**
				 * process record during load
				 * @param pRecord record for import processing
				 * @throws Exception possible exception
				 */
				@Override
				public void importRecord(Record pRecord) throws Exception {
					update(pRecord);
				}
			};
		}
		return mReaderRecordUpdate;
	}

	/**
	 * creates new one if it's needed and return import reader, which is responsible for delete data
	 *
	 * @return import reader, which is responsible for delete data
	 */
	private RecordImporter getReaderRecordDelete() {
		if (mReaderRecordDelete == null) {
			mReaderRecordDelete = new RecordImporter() {
				/**
				 * process record during load
				 * @param pRecord record for import processing
				 * @throws Exception possible exception
				 */
				@Override
				public void importRecord(Record pRecord) throws Exception {
					delete(pRecord);
				}
			};
		}
		return mReaderRecordDelete;
	}

	public int getLoaderLimitPerTransaction() {
		return mLoaderLimitPerTransaction;
	}

	public void setLoaderLimitPerTransaction(int pLoaderLimitPerTransaction) {
		mLoaderLimitPerTransaction = pLoaderLimitPerTransaction;
	}


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * pre load functionality
	 */
	protected void preLoad() {
		mFileAnalyzeTools.setCommonSettings(getCommonSettings());
		mFileAnalyzeTools.performCheck(this);
	}

	/**
	 * post load functionality
	 */
	protected void postLoad() {
/*
		if(getLoggerTools().isLoggingDebug()) {
			getLoggerTools().logDebug(getLoaderName()+" - end.");
		}
*/
	}

	/**
	 * find all satisfied files and send them to import
	 *
	 * @param pStatisticsService service statistics
	 */
	@Override
	public synchronized void load(StatisticsService pStatisticsService) {
		preLoad();
		setStatisticsService(pStatisticsService);
		List<FileInfo> fileInfoList = getImportTools().getFileList();
		if (fileInfoList.size() > 0) {
			logDebug("*******************************");
			logDebug(getLoaderName());
			logDebug("*******************************");

			if (checkLockFile()) {
				for (FileInfo fileInfo : fileInfoList) {
					logDebug(fileInfo.toString());
					pStatisticsService.setStatus(">> " + getLoaderName() + ": " + fileInfo.getFile().getName() + " <<");
					load(fileInfo);
					pStatisticsService.addLoaderStatistics(getStatistics());
					getImportTools().archiveFile(fileInfo.getFile());
				}
				releaseLockFile();
			}
		}
		postLoad();
	}


	/**
	 * check if lock file exist
	 *
	 * @return lock file exist
	 */
	private boolean checkLockFile() {
		return getImportTools().checkLockFileExist(getImportTools().getPathInfo().getInputDir());
	}

	/**
	 * release lock file
	 */
	private void releaseLockFile() {
		getImportTools().deleteLockFile(getImportTools().getPathInfo().getInputDir());
	}

	/**
	 * perform load for file
	 *
	 * @param pFileInfo file info
	 */
	@Override
	public void load(FileInfo pFileInfo) {
		setFileInfo(pFileInfo);

		setStatistics(new StatisticsLoader(this));
		setImportFailed(false);
		setReader(null);

		Date startTime = new Date();
		performAction(pFileInfo);
		Date endTime = new Date();
		getReportingTools().sendReport(this, startTime, endTime);
		getReportingTools().storeReport(this, startTime, endTime);
		getStatistics().setStartTime(startTime);
		getStatistics().setEndTime(endTime);

		logDebug(getTime("Duration time", endTime.getTime() - startTime.getTime()));
	}

	/**
	 * choose import type for received file
	 *
	 * @param pFileInfo file info
	 */
	private void performAction(FileInfo pFileInfo) {
		try {
			IReader iReader = null;
			try (FileInputStream inputStream = new FileInputStream(pFileInfo.getFile())) {
				setFileInfo(pFileInfo);
				iReader = getImportTools().getReader(getFileInfo().getFile(), inputStream);
				setReader(iReader);

				getReader().setSkipLinesNumber(getLineToSkip());
				if (pFileInfo.isFileToDelete()) {
					deleteRecords(getReader());
				} else if (pFileInfo.isFileToInsert()) {
					insertRecords(getReader());
				} else if (pFileInfo.isFileToUpdate()) {
					updateRecords(getReader());
				} else {
					logDebug("File is skipped. Action is not defined.");
				}
			} finally {
				if (null != iReader) {
					iReader.close();
				}
			}
		} catch (IOException ie) {
			if (getImportTools().getLoggerTools().isLoggingError()) {
				getImportTools().getLoggerTools().logError(ie);
			}
		}
	}

	/**
	 * sends reader to insert records
	 *
	 * @param pReader import file reader
	 */
	@Override
	public void insertRecords(IReader pReader) {
		getReaderRecordInsert().importRecords(pReader);
	}

	/**
	 * sends reader to update records
	 *
	 * @param pReader import file reader
	 */
	@Override
	public void updateRecords(IReader pReader) {
		getReaderRecordUpdate().importRecords(pReader);
	}

	/**
	 * sends reader to delete records
	 *
	 * @param pReader import file reader
	 */
	@Override
	public void deleteRecords(IReader pReader) {
		getReaderRecordDelete().importRecords(pReader);
	}

	/**
	 * add error records to error file
	 */
	protected void trackError() {
		try {
			for (RecordValidationInfo validationInfo : getStatistics().getErrorRecords()) {
				getErrorWriter().writeNext(getRecordInfoWithReason(
						validationInfo.getRecord(),
						validationInfo.getRecord().getIndex() + " line:",
						validationInfo.getValidationMsg()));
			}
			if (getStatistics().getErrorRecords().size() > 0) {
				getStatistics().setErrorFileName(getErrorWriter().getFile().getAbsolutePath());
			}
			getStatistics().getErrorRecords().clear();
		} catch (Exception e) {
			logError(e);
		}
	}

	/**
	 * format new error row with record, line index and reason
	 *
	 * @param pRecord error record
	 * @param pLine   line
	 * @param pReason fail reason
	 * @return new record info with specified line and reason
	 */
	private String[] getRecordInfoWithReason(Record pRecord, String pLine, String pReason) {
		Collection<String> recordInitialValues = pRecord.getValuesInitial().values();
		Iterator<String> valuesIterator = recordInitialValues.iterator();
		int infoLength = recordInitialValues.size();
		String[] error = new String[infoLength + 2];
		for (int i = 0; i < infoLength; i++) {
			error[i] = valuesIterator.next();
		}
		error[infoLength] = pLine;
		error[infoLength + 1] = pReason;
		return error;
	}

	/**
	 * pre import initial operations
	 */
	protected void preImportRecords() {
		setImportFailed(false);
		setErrorWriter(null);
		getFieldsInfo().init();
	}

	/**
	 * post import operations
	 */
	protected void postImportRecords() {

	}

	/**
	 * perform operations on import file end
	 */
	protected void importRecordsEnd() {
		trackError();
		getReader().close();
		if (mErrorWriter != null) {
			mErrorWriter.close();
		}
		logDebug(
				getStatistics().getStatisticsPresentation()
		);
	}

	/**
	 * open new transaction
	 *
	 * @param pDemarcation transaction demarcation
	 * @throws TransactionDemarcationException could not open transaction
	 */
	protected void transactionStart(TransactionDemarcation pDemarcation) throws TransactionDemarcationException {
		pDemarcation.begin(getCommonSettings().getTransactionManager());
	}

	/**
	 * commit transaction
	 *
	 * @param pDemarcation transaction demarcation
	 * @throws TransactionDemarcationException could not commit transaction
	 */
	protected void transactionCommit(TransactionDemarcation pDemarcation)
			throws TransactionDemarcationException {
		transactionEnd(pDemarcation);
		getStatisticsService().setStatus(">> " + getLoaderName() + ": " + getFileInfo().getFile().getName() +
				" - " + getStatistics().getCountProceeded() + " records.... <<");
		trackError();

		logDebug(getStatistics().getCountProceeded() + " records [" +
				getStatistics().getCountError() + " errors]....");

		if (!isImportFailed()) {
			transactionStart(pDemarcation);
		}
	}

	/**
	 * end transaction with check for import failed state for rollback
	 *
	 * @param pDemarcation transaction demarcation
	 */
	protected void transactionEnd(TransactionDemarcation pDemarcation) {
		try {
			if (pDemarcation.getTransaction() != null) {
				int status = -1;
				try {
					status = pDemarcation.getTransaction().getStatus();
				} catch (SystemException e) {
					exceptionCatch(e);
				}
				if (status == Status.STATUS_ACTIVE) {
					pDemarcation.end(isImportFailed());
				} else {
					pDemarcation.end(true);
				}
			}
		} catch (TransactionDemarcationException tde) {
			exceptionCatch(tde);
		}
	}

	/**
	 * record importer helper class
	 * implement base import logic
	 */
	class RecordImporter {
		/**
		 * perform read all records in transactions
		 *
		 * @param pReader import file reader
		 */
		public void importRecords(IReader pReader) {
			TransactionDemarcation td = new TransactionDemarcation();
			try {
				preImportRecords();
				transactionStart(td);
				String[] row;
				while ((row = pReader.readNext()) != null && !isImportFailed()) {
					if (!processRow(row)) {
						break;
					}
					if (isCommitRequired()) {
						transactionCommit(td);
					}
				}
			} catch (Exception e) {
				exceptionCatch(e);
			} finally {
				transactionEnd(td);
				importRecordsEnd();
				postImportRecords();
			}
		}

		/**
		 * proceed row with validation
		 *
		 * @param pRow new row from file
		 * @return record is proceed without error state
		 */
		private boolean processRow(String[] pRow) {
			if (pRow == null || pRow.length != getFieldsInfo().getFields().size()) {
				getStatistics().addErrorRecord(
						getEmptyOrNotWellFormedRecord(
								pRow,
								getStatistics().getCountProceeded())
				);
				return true;
			}
			Record record = getParserTools().getRecord(getFieldsInfo(), pRow, getStatistics().getCountProceeded());
			getStatistics().incrementProceeded();

			ValidationTools validationTools = getCommonSettings().getValidationTools();
			ValidationResult validationResult = validationTools.validateRecord(record, getFieldsInfo());
			if (validationResult.isValid()) { // validation is passed
				try {
					getParserTools().initRecord(record, getFieldsInfo());
					importRecord(record); // pass record to loader import
				} catch (Exception e) {
					exceptionCatch(e);
					return false;
				}
			} else {
				if (!validationResult.isSkip()) {
					getStatistics().addErrorRecord(new RecordValidationInfo(record, validationResult.getMsg()));
				} else {
					getStatistics().incrementSkipped();
				}
			}
			return true;
		}

		/**
		 * @param pRow   readed row from file
		 * @param pIndex row index
		 * @return fake record for broken row
		 */
		private RecordValidationInfo getEmptyOrNotWellFormedRecord(String[] pRow, int pIndex) {
			Record errorRecord = new Record("Broken row");
			errorRecord.setIndex(pIndex);
			errorRecord.setValuesInitial(new HashMap<String, String>());
			if (pRow != null) {
				for (int i = 0; i < pRow.length; i++) {
					errorRecord.putInitialFieldValue(String.valueOf(i), pRow[i]);
				}
			}
			String errorMsg = "";
			if (pRow == null) {
				errorMsg = "Cells row is not specified.";
			} else {
				errorMsg = "Cells row count is " + pRow.length + " instead of " + getFieldsInfo().getFields().size();
			}
			return new RecordValidationInfo(errorRecord, errorMsg);
		}

		/**
		 * process record during load
		 *
		 * @param pRecord record for import processing
		 * @throws Exception possible exception
		 */
		protected void importRecord(Record pRecord) throws Exception {

		}
	}

	/**
	 * check if commit required
	 *
	 * @return commit required
	 */
	protected boolean isCommitRequired() {
		if (getLoaderLimitPerTransaction() != -1) {
			return getStatistics().getCountProceeded() % getLoaderLimitPerTransaction() == 0;
		} else {
			return getStatistics().getCountProceeded() % getConnector().getLimitPerTransaction() == 0;
		}
	}

	/**
	 * process exception with stopping import and logging error
	 *
	 * @param e exception
	 */
	protected void exceptionCatch(Exception e) {
		setImportFailed(true);

		boolean inOurClass = false;
		String loaderClassName = Loader.class.getName();
		StringBuilder exceptionReason = new StringBuilder(e.toString()).append("\n");
		logError(e.toString());
		if (e.getStackTrace() != null) {
			for (StackTraceElement traceElement : e.getStackTrace()) {
				if (traceElement.getClassName().equals(loaderClassName)) {
					inOurClass = true;
				} else {
					if (inOurClass) {
						break;
					}
				}
				exceptionReason.append(traceElement.toString()).append("\n");
			}
		}

		getStatistics().setFailReason(exceptionReason.toString());
		logError(e);
	}

}
