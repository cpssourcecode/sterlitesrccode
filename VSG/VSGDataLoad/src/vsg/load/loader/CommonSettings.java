package vsg.load.loader;

import vsg.load.email.EmailSettings;
import vsg.load.tools.IReportingTools;
import vsg.load.tools.impl.LoggerTools;
import vsg.load.tools.impl.ParserTools;
import vsg.load.tools.impl.ValidationTools;

import javax.transaction.TransactionManager;

/**
 * @author Dmitry Golubev
 */
public class CommonSettings {
	/**
	 * transaction manager
	 */
	private TransactionManager mTransactionManager = null;
	/**
	 * validation tools
	 */
	private ValidationTools mValidationTools;
	/**
	 * logger tools
	 */
	private LoggerTools mLoggerTools;
	/**
	 * parse tools
	 */
	private ParserTools mParserTools;
	/**
	 * email settings
	 */
	private EmailSettings mEmailSettings;

	/**
	 * loader reporting
	 */
	private IReportingTools mReportingTools;

	/**
	 * File encoding to read import files
	 */
	private String mEncoding = "UTF8";

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return transaction manager
	 * @return transaction manager
	 */
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	/**
	 * init transaction manager
	 * @param pTransactionManager transaction manager
	 */
	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	/**
	 * return validation tools
	 * @return validation tools
	 */
	public ValidationTools getValidationTools() {
		return mValidationTools;
	}

	/**
	 * init validation tools
	 * @param pValidationTools validation tools
	 */
	public void setValidationTools(ValidationTools pValidationTools) {
		mValidationTools = pValidationTools;
	}

	/**
	 * return logger tools
	 * @return logger tools
	 */
	public LoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	/**
	 * init logger tools
	 * @param pLoggerTools logger tools
	 */
	public void setLoggerTools(LoggerTools pLoggerTools) {
		mLoggerTools = pLoggerTools;
	}

	/**
	 * return parse tools
	 * @return parse tools
	 */
	public ParserTools getParserTools() {
		return mParserTools;
	}

	/**
	 * init parse tools
	 * @param pParserTools parse tools
	 */
	public void setParserTools(ParserTools pParserTools) {
		mParserTools = pParserTools;
	}

	/**
	 * return email settings
	 * @return email settings
	 */
	public EmailSettings getEmailSettings() {
		return mEmailSettings;
	}

	/**
	 * init email settings component
	 * @param pEmailSettings email settings component
	 */
	public void setEmailSettings(EmailSettings pEmailSettings) {
		mEmailSettings = pEmailSettings;
	}

	/**
	 * return loader reporting
	 * @return loader reporting
	 */
	public IReportingTools getReportingTools() {
		return mReportingTools;
	}

	/**
	 * init loader reporting
	 * @param pReportingTools loader reporting
	 */
	public void setReportingTools(IReportingTools pReportingTools) {
		mReportingTools = pReportingTools;
	}

	/**
	 * retrieve import file encoding
	 * @return import file encoding
	 */
	public String getEncoding() {
		return mEncoding;
	}

	/**
	 * init import file encoding
	 * @param pEncoding import file encoding
	 */
	public void setEncoding(String pEncoding) {
		mEncoding = pEncoding;
	}
}
