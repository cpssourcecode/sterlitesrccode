package vsg.load.info;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import vsg.load.loader.Record;
import vsg.load.wrapper.IValueWrapper;
import vsg.load.validator.IValidator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class FieldsInfo {
	/**
	 * id field
	 */
	private String mFieldId;
	/**
	 * field is under subrecord map
	 */
	private final Map<String, Boolean> mFieldsIsSubRecordMap = new HashMap<String, Boolean>();
	/**
	 * fields update map by record name
	 */
	private final Map<String, List<String>> mFieldsUpdateMap = new HashMap<String, List<String>>();
	/**
	 * fields to sql map by record name
	 */
	private final Map<String, Map<String, String>> mFieldsSqlMap = new HashMap<String, Map<String, String>>();
	/**
	 * fields to fields map by record name
	 */
	private final Map<String, Map<String, String>> mFieldsToFieldMap = new HashMap<String, Map<String, String>>();
	/**
	 * fields to values map by record name
	 */
	private final Map<String, Map<String, String>> mFieldsValueMap = new HashMap<String, Map<String, String>>();
	/**
	 * list of all fields from import file
	 */
	private List<String> mFields;
	/**
	 * skip fields during sql forming, but left in record values
	 */
	private List<String> mFieldsSqlSkip;
	/**
	 * skip fields
	 */
	private List<String> mFieldsSkip;
	/**
	 * fields to sql map
	 */
	private Map<String, String> mFieldsToSqlMap;
	/**
	 * fields value map
	 */
	private Map<String, String> mFieldsValue;
	/**
	 * fields to fields map
	 */
	private Map<String, String> mFieldsToField;
	/**
	 * fields to wrapped fields map
	 */
	private List<String> mFieldsToFieldWrapped;
	/**
	 * fields sql map
	 */
	private Map<String, String> mFieldsSql;
	/**
	 * fields validators
	 */
	private Map<String, IValidator> mFieldsValidator;
	/**
	 * fields wrappers
	 */
	private Map<String, IValueWrapper> mFieldsValueWrappers;
	/**
	 * sub record names
	 */
	private List<String> mSubRecordNames;
	/**
	 * sub record names
	 */
	private List<String> mSubRecordLastUse;
	/**
	 * sub record tables
	 */
	private Map<String, String> mSubRecordTables;
	/**
	 * sub record field ids
	 */
	private Map<String, String> mSubRecordFieldIds;
	/**
	 * table name
	 */
	private String mTableName;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * field id index
	 */
	private int mFieldIdIndex = -1;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init
	 */
	public void init() {
		mFieldIdIndex = getFields().indexOf( getFieldId() );
	}

	/**
	 * return id field
	 * @return id field
	 */
	public String getFieldId() {
		return mFieldId;
	}

	/**
	 * return field id index
	 * @return field id index
	 */
	public int getFieldIdIndex() {
		return mFieldIdIndex;
	}

	/**
	 * return id field
	 * @param pSubRecordName subRecordName
	 * @return id field
	 */
	public String getFieldId(String pSubRecordName) {
		if(pSubRecordName == null) {
			return mFieldId;
		}
		if(getSubRecordFieldIds()==null) {
			return mFieldId;
		}
		String subRecordFieldId = getSubRecordFieldIds().get(pSubRecordName);
		if(subRecordFieldId != null) {
			return subRecordFieldId;
		}
		return mFieldId;
	}

	/**
	 * init id field
	 * @param pFieldId id field
	 */
	public void setFieldId(String pFieldId) {
		mFieldId = pFieldId;
	}

	/**
	 * return list of fields
	 * @return list of fields
	 */
	public List<String> getFields() {
		return mFields;
	}

	/**
	 * init list of fields
	 * @param pFields list of fields
	 */
	public void setFields(List<String> pFields) {
		mFields = pFields;
	}

	/**
	 * return list of fields to be skipped during sql forming
	 * @return list of fields to be skipped
	 */
	public List<String> getFieldsSqlSkip() {
		return mFieldsSqlSkip;
	}

	/**
	 * init list of fields to be skipped during sql forming
	 * @param pFieldsSqlSkip list of fields to be skipped
	 */
	public void setFieldsSqlSkip(List<String> pFieldsSqlSkip) {
		mFieldsSqlSkip = pFieldsSqlSkip;
	}

	/**
	 * return list of fields to be skipped
	 * @return list of fields to be skipped
	 */
	public List<String> getFieldsSkip() {
		return mFieldsSkip;
	}

	/**
	 * init list of fields to be skipped
	 * @param pFieldsSkip list of fields to be skipped
	 */
	public void setFieldsSkip(List<String> pFieldsSkip) {
		mFieldsSkip = pFieldsSkip;
	}

	/**
	 * return mapping of fields to db table columns
	 * @return mapping of fields to db table columns
	 */
	public Map<String, String> getFieldsToSqlMap() {
		return mFieldsToSqlMap;
	}

	/**
	 * init mapping of fields to db table columns
	 * @param pFieldsToSqlMap mapping of fields to db table columns
	 */
	public void setFieldsToSqlMap(Map<String, String> pFieldsToSqlMap) {
		mFieldsToSqlMap = pFieldsToSqlMap;
	}

	/**
	 * return fields values for current record level
	 * @param pSubRecordName sub record name
	 * @return fields values for current record level
	 */
	public Map<String, String> getFieldsValue(String pSubRecordName) {
		if(mFieldsValue == null) {
			return mFieldsValue;
		}
		Map<String, String> fieldsValues = mFieldsValueMap.get(pSubRecordName);
		if(fieldsValues == null) {
			fieldsValues =  new HashMap<String, String>();
			boolean isNotSubRecord = StringUtils.isBlank(pSubRecordName);
			for(String fieldKey : mFieldsValue.keySet()) {
				if(isNotSubRecord) {
					if(!isSubRecordField(fieldKey)) {
						fieldsValues.put(fieldKey, mFieldsValue.get(fieldKey));
					}
				} else {
					if(isSubRecordField(fieldKey, pSubRecordName)) {
						fieldsValues.put(fieldKey, mFieldsValue.get(fieldKey));
					}
				}
			}
			mFieldsValueMap.put(pSubRecordName, fieldsValues);
		}
		return fieldsValues;
	}

	/**
	 * init specified field values
	 * @param pFieldsValue specified field values
	 */
	public void setFieldsValue(Map<String, String> pFieldsValue) {
		mFieldsValue = pFieldsValue;
	}

	/**
	 * return fields to fields for current record level
	 * @param pSubRecordName sub record name
	 * @return fields to fields for current record level
	 */
	public Map<String, String> getFieldsToField(String pSubRecordName) {
		if(mFieldsToField == null) {
			return mFieldsToField;
		}
		Map<String, String> fieldsToField = mFieldsToFieldMap.get(pSubRecordName);
		if(fieldsToField == null) {
			fieldsToField =  new HashMap<String, String>();
			boolean isNotSubRecord = StringUtils.isBlank(pSubRecordName);
			for(String fieldKey : mFieldsToField.keySet()) {
				if(isNotSubRecord) {
					if(!isSubRecordField(fieldKey)) {
						fieldsToField.put(fieldKey, mFieldsToField.get(fieldKey));
					}
				} else {
					if(isSubRecordField(fieldKey, pSubRecordName)) {
						fieldsToField.put(fieldKey, mFieldsToField.get(fieldKey));
					}
				}
			}
			mFieldsToFieldMap.put(pSubRecordName, fieldsToField);
		}
		return fieldsToField;
	}

	/**
	 * init fields to fields for current record level
	 * @param pFieldsToField fields to fields mapping
	 */
	public void setFieldsToField(Map<String, String> pFieldsToField) {
		mFieldsToField = pFieldsToField;
	}

	public List<String> getFieldsToFieldWrapped() {
		return mFieldsToFieldWrapped;
	}

	public void setFieldsToFieldWrapped(List<String> pFieldsToFieldWrapped) {
		mFieldsToFieldWrapped = pFieldsToFieldWrapped;
	}

	/**
	 * return fields sql values for current record level
	 * @param pSubRecordName sub record name
	 * @return fields sql values for current record level
	 */
	public Map<String, String> getFieldsSql(String pSubRecordName) {
		if(mFieldsSql == null) {
			return mFieldsSql;
		}
		Map<String, String> fieldsSql = mFieldsSqlMap.get(pSubRecordName);
		if(fieldsSql == null) {
			fieldsSql =  new HashMap<String, String>();
			for(String fieldKey : mFieldsSql.keySet()) {
				final boolean isCorrectSubRecord = !StringUtils.isBlank(pSubRecordName) && isSubRecordField(fieldKey, pSubRecordName);
				final boolean isNotSubRecord = StringUtils.isBlank(pSubRecordName) && !isSubRecordField(fieldKey);
				if (isCorrectSubRecord || isNotSubRecord) {
					fieldsSql.put( fieldKey, mFieldsSql.get(fieldKey) );
				}
			}
			mFieldsSqlMap.put(pSubRecordName, fieldsSql);
		}
		return fieldsSql;
	}

	/**
	 * init fields sql values for current record level
	 * @param pFieldsSql fields to sql mapping
	 */
	public void setFieldsSql(Map<String, String> pFieldsSql) {
		mFieldsSql = pFieldsSql;
	}

	/**
	 * return table name
	 * @return table name
	 */
	public String getTableName() {
		return mTableName;
	}

	/**
	 * init table name
	 * @param pTableName table name
	 */
	public void setTableName(String pTableName) {
		mTableName = pTableName;
	}

	/**
	 * return sub record field ids
	 * @return sub record field ids
	 */
	public Map<String, String> getSubRecordFieldIds() {
		return mSubRecordFieldIds;
	}

	/**
	 * init sub record field ids
	 * @param pSubRecordFieldIds sub record field ids
	 */
	public void setSubRecordFieldIds(Map<String, String> pSubRecordFieldIds) {
		mSubRecordFieldIds = pSubRecordFieldIds;
	}

	/**
	 * return fields validators
	 * @return fields validators
	 */
	public Map<String, IValidator> getFieldsValidator() {
		return mFieldsValidator;
	}

	/**
	 * init fields validators
	 * @param pFieldsValidator fields validators
	 */
	public void setFieldsValidator(Map<String, String> pFieldsValidator) {
		if(pFieldsValidator == null) {
			mFieldsValidator = null;
		} else {
			mFieldsValidator = new HashMap<String, IValidator>();
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			for(String fieldName : pFieldsValidator.keySet()) {
				IValidator validator = (IValidator) nucleus.resolveName( pFieldsValidator.get(fieldName) );
				mFieldsValidator.put(fieldName, validator);
			}
		}
	}

	/**
	 * return fields wrappers
	 * @return fields wrappers
	 */
	public Map<String, IValueWrapper> getFieldsValueWrappers() {
		return mFieldsValueWrappers;
	}

	/**
	 * init fields wrappers
	 * @param pFieldsValueWrappers fields wrappers
	 */
	public void setFieldsValueWrappers(Map<String, String> pFieldsValueWrappers) {
		if(pFieldsValueWrappers == null) {
			mFieldsValueWrappers = null;
		} else {
			mFieldsValueWrappers = new HashMap<String, IValueWrapper>();
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			for(String fieldName : pFieldsValueWrappers.keySet()) {
				IValueWrapper valueWrapper = (IValueWrapper) nucleus.resolveName( pFieldsValueWrappers.get(fieldName));
				mFieldsValueWrappers.put(fieldName, valueWrapper);
			}
		}
	}

	/**
	 * return sub records names
	 * @return sub records names
	 */
	public List<String> getSubRecordNames() {
		return mSubRecordNames;
	}

	/**
	 * init sub records names
	 * @param pSubRecordNames sub records names
	 */
	public void setSubRecordNames(List<String> pSubRecordNames) {
		mSubRecordNames = pSubRecordNames;
	}

	/**
	 * return sub records tables names
	 * @return sub records tables names
	 */
	public Map<String, String> getSubRecordTables() {
		return mSubRecordTables;
	}

	/**
	 * init sub records tables names
	 * @param pSubRecordTables sub records tables names
	 */
	public void setSubRecordTables(Map<String, String> pSubRecordTables) {
		mSubRecordTables = pSubRecordTables;
	}

	public List<String> getSubRecordLastUse() {
		return mSubRecordLastUse;
	}

	public void setSubRecordLastUse(List<String> pSubRecordLastUse) {
		mSubRecordLastUse = pSubRecordLastUse;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return fields list to be used on record result values forming
	 * @param pSubRecordName sub record name
	 * @return fields list to be used on record result values forming
	 */
	public List<String> getSqlUpdateFieldsList(String pSubRecordName) {
		if(mFieldsUpdateMap.get(pSubRecordName) != null) {
			return mFieldsUpdateMap.get(pSubRecordName);
		} else {
			List<String> fields = new ArrayList<String>();
			for(String fieldName : getFields()) {
				if(isNotSkipField(pSubRecordName, fieldName)) {
					if(StringUtils.isBlank(pSubRecordName) && isSubRecordField(fieldName)) {
						continue;
					}
					if(!StringUtils.isBlank(pSubRecordName) && !isSubRecordField(fieldName, pSubRecordName)) {
						continue;
					}
					fields.add( fieldName );
				}
			}
			fields.remove(getFieldId(pSubRecordName));
			mFieldsUpdateMap.put(pSubRecordName, fields);
			return fields;
		}
	}

	/**
	 * check if should this field to be skipped during row analyzing
	 * @param pSubRecordName sub record name
	 * @param pFieldName field name
	 * @return should this field to be skipped during row analyzing
	 */
	private boolean isNotSkipField(String pSubRecordName, String pFieldName) {
		return ! ( getFieldsSkip() != null && getFieldsSkip().contains(pFieldName) ) &&
				! ( getFieldsSqlSkip() != null && getFieldsSqlSkip().contains(pFieldName) ) &&
				! ( getFieldsToField(pSubRecordName) != null &&
						getFieldsToField(pSubRecordName).containsKey(pFieldName) );
	}

	/**
	 * check if field refers to any sub record
	 * @param pFieldName field name
	 * @return field refers to any sub record
	 */
	public boolean isSubRecordField(String pFieldName) {
		Boolean isUnderSubRecord = mFieldsIsSubRecordMap.get(pFieldName);
		if(isUnderSubRecord != null) {
			return isUnderSubRecord;
		} else {
			Boolean result = Boolean.FALSE;
			if(getSubRecordNames() != null) {
				for(String subRecordName : getSubRecordNames()) {
					if(isSubRecordField(pFieldName, subRecordName)) {
						result = Boolean.TRUE;
					}
				}
			}
			mFieldsIsSubRecordMap.put(pFieldName, result);
			return result;
		}
	}

	/**
	 * check if field refers to specified sub record
	 * @param pFieldName field name
	 * @param pSubRecordName sub record name
	 * @return field refers to specified sub record
	 */
	public boolean isSubRecordField(String pFieldName, String pSubRecordName) {
		return !StringUtils.isBlank(pSubRecordName) && pFieldName.startsWith(pSubRecordName + ".");
	}

	/**
	 * @param pChildRecord child record to get from top parent record
	 * @return top parent record
	 */
	public Record getTopRecord(Record pChildRecord) {
		if(pChildRecord.getParentRecord() == null) {
			return pChildRecord;
		} else {
			return getTopRecord(pChildRecord.getParentRecord());
		}
	}

	/**
	 * @param pRecord record to check
	 * @return true if record is marked to use in the end
	 */
	public boolean isRecordToProcessLast(Record pRecord) {
		if( getSubRecordLastUse() == null) {
			return false;
		} else {
			return getSubRecordLastUse().contains( pRecord.getName() );
		}
	}

	/**
	 * @param pRecordName record name to check
	 * @return true if record is marked to use in the end
	 */
	public boolean isRecordToProcessLast(String pRecordName) {
		if( getSubRecordLastUse() == null) {
			return false;
		} else {
			return getSubRecordLastUse().contains( pRecordName );
		}
	}
}
