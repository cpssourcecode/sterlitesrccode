package vsg.load.info;

import java.io.File;

/**
 * @author Dmitry Golubev
 */
public class FileInfo {
	/**
	 * flag to insert
	 */
	public static final int ACTION_INSERT = 0;
	/**
	 * flag to update
	 */
	public static final int ACTION_UPDATE = 1;
	/**
	 * flag to delete
	 */
	public static final int ACTION_DELETE = 2;
	/**
	 * obtained file
	 */
	private File mFile;
	/**
	 * selected action
	 */
	private int mAction = -1;

	/**
	 * constructor for file info
	 * @param pFile processing file
	 * @param pAction action to be applied for current file
	 */
	public FileInfo(File pFile, int pAction) {
		mFile = pFile;
		mAction = pAction;
	}

	/**
	 * return processing file
	 * @return processing file
	 */
	public File getFile() {
		return mFile;
	}

	/**
	 * return action to be applied for current file
	 * @return action to be applied for current file
	 */
	public int getAction() {
		return mAction;
	}

	/**
	 * init action type
	 * @param pAction action type
	 */
	public void setAction(int pAction) {
		mAction = pAction;
	}

	/**
	 * return action to string presentation
	 * @return action to string presentation
	 */
	public String getActionToString() {
		switch (mAction) {
			case ACTION_DELETE: return "delete";
			case ACTION_INSERT: return "insert";
			case ACTION_UPDATE: return "update";
		}
		return "not_specified";
	}

	/**
	 * check if file will be used to insert new records
	 * @return file will be used to insert new records
	 */
	public boolean isFileToInsert() {
		return getAction() == FileInfo.ACTION_INSERT;
	}

	/**
	 * check if file will be used to update exist records
	 * @return file will be used to update exist records
	 */
	public boolean isFileToUpdate() {
		return getAction() == FileInfo.ACTION_UPDATE;
	}

	/**
	 * check if file will be used to delete exist records
	 * @return file will be used to delete exist records
	 */
	public boolean isFileToDelete() {
		return getAction() == FileInfo.ACTION_DELETE;
	}

	/**
	 * file info string presentation
	 * @return string presentation
	 */
	@Override
	public String toString() {
		if(getFile() == null) {
			return super.toString();
		}
		StringBuilder objToString = new StringBuilder();
		objToString.append("FILE: ").append(getFile().getName()).append("; ");
		objToString.append("ACTION: ").append(getActionToString());
		return objToString.toString();
	}
}
