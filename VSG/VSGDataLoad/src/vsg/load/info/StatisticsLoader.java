package vsg.load.info;

import vsg.load.loader.ILoader;
import vsg.load.loader.Record;
import vsg.load.loader.RecordAction;
import vsg.load.loader.RecordValidationInfo;
import vsg.load.tools.ILoggerTools;

import java.util.*;

/**
 * @author Dmitry Golubev, Kate Koshman
 */
public class StatisticsLoader {

	public static final String ACTION_CHANGE = "Change";
	public static final String ACTION_ADD = "Add";

	/**
	 * loader link
	 */
	private ILoader mLoader;
	/**
	 * load file name
	 */
	private String mFileName;
	/**
	 * insert count
	 */
	private int mCountInsert = 0;
	/**
	 * update count
	 */
	private int mCountUpdate = 0;
	/**
	 * update count
	 */
	private int mCountDelete = 0;
	/**
	 * error count
	 */
	private int mCountError = 0;
	/**
	 * proceeded count
	 */
	private int mCountProceeded = 0;
	/**
	 * skipped count
	 */
	private int mCountSkipped = 0;
	/**
	 * is load succeeded
	 */
	private boolean mSucceeded = true;
	/**
	 * fail reason
	 */
	private String mFailReason = null;
	/**
	 * The needed indexing level.
	 */
	private int mIndexingLevel = 0; //EndecaConstants.INDEX_NONE_LEVEL;
	/**
	 * records comparator
	 */
	private Comparator<RecordValidationInfo> recordsComparator = new Comparator<RecordValidationInfo>() {
		@Override
		public int compare(RecordValidationInfo o1, RecordValidationInfo o2) {
			return o1.getRecord().getIndex() - o2.getRecord().getIndex();
		}
	};
	/**
	 * set of processed records for create
	 */
	private Set<Record> mRecordsForCreate = new HashSet<Record>();
	/**
	 * set of processed records for update
	 */
	private Set<Record> mRecordsForUpdate = new HashSet<Record>();
	/**
	 * set of processed records for delete
	 */
	private Set<Record> mRecordsForDelete = new HashSet<Record>();

	/**
	 * errorFileName
	 */
	private String mErrorFileName = new String();

	/**
	 * set of processed error records
	 */
	private List<RecordValidationInfo> mErrorRecords = new ArrayList<RecordValidationInfo>();
	// private Set<RecordValidationInfo> mErrorRecords = new TreeSet<RecordValidationInfo>(recordsComparator);

	private List<ItemStatus> mItemStatuses = new ArrayList<ItemStatus>();


	private Date mStartTime;
	private Date mEndTime;


	/**
	 * load file name only
	 */
	private String mFileNameOnly;
	/**
	 * load file action
	 */
	private String mAction;


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 *
	 * @param pLoader loader link
	 */
	public StatisticsLoader(ILoader pLoader) {
		mLoader = pLoader;
		setFileName(pLoader.getFileInfo().toString());
		setFileNameOnly(pLoader.getFileInfo().getFile().getName());
		setAction(pLoader.getFileInfo().getActionToString());
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return count of inserted records
	 *
	 * @return count of inserted records
	 */
	public int getCountInsert() {
		return mCountInsert;
	}

	public String getFileName() {
		return mFileName;
	}

	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	/**
	 * return count of updated records
	 *
	 * @return count of updated records
	 */
	public int getCountUpdate() {
		return mCountUpdate;
	}

	/**
	 * return count of deleted records
	 *
	 * @return count of deleted records
	 */
	public int getCountDelete() {
		return mCountDelete;
	}

	/**
	 * @return count of error records
	 */
	public int getCountError() {
		return mCountError;
	}

	/**
	 * return count of proceeded records
	 *
	 * @return count of proceeded records
	 */
	public int getCountProceeded() {
		return mCountProceeded;
	}

	public int getCountSkipped() {
		return mCountSkipped;
	}

	/**
	 * increments count of inserted records
	 */
	public void incrementInsert() {
		mCountInsert++;
	}

	/**
	 * increments count of updated records
	 */
	public void incrementUpdate() {
		mCountUpdate++;
	}

	/**
	 * increments count of deleted records
	 */
	public void incrementDelete() {
		mCountDelete++;
	}

	/**
	 * increments count of deleted records
	 *
	 * @param pNumber number of deleted records
	 */
	public void incrementDelete(int pNumber) {
		mCountDelete += pNumber;
	}

	/**
	 * increments count of proceeded records
	 */
	public void incrementProceeded() {
		mCountProceeded++;
	}

	/**
	 * increments count of error records
	 */
	public void incrementError() {
		mCountError++;
	}

	/**
	 * increments count of skipped records
	 */
	public void incrementSkipped() {
		mCountSkipped++;
	}

	public Set<Record> getRecordsForCreate() {
		return mRecordsForCreate;
	}

	public void setRecordsForCreate(Set<Record> pRecordsForCreate) {
		mRecordsForCreate = pRecordsForCreate;
	}

	public Set<Record> getRecordsForUpdate() {
		return mRecordsForUpdate;
	}

	public void setRecordsForUpdate(Set<Record> pRecordsForUpdate) {
		mRecordsForUpdate = pRecordsForUpdate;
	}

	public Set<Record> getRecordsForDelete() {
		return mRecordsForDelete;
	}

	public void setRecordsForDelete(Set<Record> pRecordsForDelete) {
		mRecordsForDelete = pRecordsForDelete;
	}

	/**
	 * return current set of error records
	 *
	 * @return current set of error records
	 */
	public List<RecordValidationInfo> getErrorRecords() {
		return mErrorRecords;
	}

	/**
	 * return state of load
	 *
	 * @return state of load
	 */
	public boolean isSucceeded() {
		return mSucceeded;
	}

	/**
	 * set state of load to failed
	 */
	public void setFailed() {
		mSucceeded = false;
	}

	/**
	 * return fail reason
	 *
	 * @return fail reason
	 */
	public String getFailReason() {
		return mFailReason;
	}

	/**
	 * init fail reason
	 *
	 * @param pFailReason fail reason
	 */
	public void setFailReason(String pFailReason) {
		mFailReason = pFailReason;
	}

	/**
	 * Gets the indexing level.
	 *
	 * @return the indexing level
	 */
	public int getIndexingLevel() {
		return mIndexingLevel;
	}

	/**
	 * Sets the indexing level.
	 *
	 * @param pIndexingLevel the new indexing level
	 */
	public void setIndexingLevel(int pIndexingLevel) {
		mIndexingLevel = pIndexingLevel;
	}

	public ILoader getLoader() {
		return mLoader;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * clear current set of records
	 */
	public void resetRecordSets() {
		getRecordsForCreate().clear();
		getRecordsForUpdate().clear();
		getRecordsForDelete().clear();
	}

	/**
	 * check if record is added to current set of processing records
	 *
	 * @param pRecord       new record
	 * @param pRecordAction record action
	 * @return record is added to current set of processing records
	 */
	public boolean addRecord(Record pRecord, RecordAction pRecordAction) {
		if (pRecordAction == null)
			return false;
		switch (pRecordAction) {
			case CREATE:
				return getRecordsForCreate().add(pRecord);
			case UPDATE:
				return getRecordsForUpdate().add(pRecord);
			case DELETE:
				return getRecordsForDelete().add(pRecord);
			default:
				return false;
		}
	}

	/**
	 * add error record
	 *
	 * @param pRecord error record
	 */
	public void addErrorRecord(RecordValidationInfo pRecord) {
		incrementError();
		getErrorRecords().add(pRecord);
	}

	/**
	 * show statistics
	 *
	 * @return statistics string presentation
	 */
	public String getStatisticsPresentation() {
		StringBuilder sb = new StringBuilder();
		sb.append("Statistics for ").append(getLoader().getLoaderName()).append("\n");
		sb.append("file name: ").append(getFileName()).append("\n");
		sb.append("success: ").append(isSucceeded()).append("\n");
		if (!isSucceeded()) {
			sb.append("fail reason: ").append(getFailReason()).append("\n");
		}

		sb.append("total/delete/insert/update/skip/error : ");
		sb.append(getCountProceeded()).append("/");
		sb.append(getCountDelete()).append("/");
		sb.append(getCountInsert()).append("/");
		sb.append(getCountUpdate()).append("/");
		sb.append(getCountSkipped()).append("/");
		sb.append(getCountError()).append("\n");

/*
		sb.append("total count:  ").append(getCountProceeded()).append("\n");
		if(getLoader().getFileInfo().isFileToDelete()) {
			sb.append("delete count: ").append(getCountDelete()).append("\n");
		} else
		if(getLoader().getFileInfo().isFileToInsert()) {
			sb.append("insert count: ").append(getCountInsert()).append("\n");
		} else
		if(getLoader().getFileInfo().isFileToUpdate()) {
			sb.append("update count: ").append(getCountUpdate()).append("\n");
		}
		sb.append("skipped count:  ").append(getCountSkipped()).append("\n");
		sb.append("error count:  ").append(getCountError()).append("\n");
*/
		return sb.toString();
	}

	/**
	 * Check if records modified.
	 *
	 * @return true, if successful
	 */
	public boolean checkIfRecordsModified() {
		return getCountInsert() > 0 || getCountUpdate() > 0 || getCountDelete() > 0;
	}


	/**
	 * Sets new errorFileName.
	 *
	 * @param pErrorFileName New value of errorFileName.
	 */
	public void setErrorFileName(String pErrorFileName) {
		mErrorFileName = pErrorFileName;
	}

	/**
	 * Gets errorFileName.
	 *
	 * @return Value of errorFileName.
	 */
	public String getErrorFileName() {
		return mErrorFileName;
	}


	/**
	 * Gets mItemStatuses.
	 *
	 * @return Value of mItemStatuses.
	 */
	public List<ItemStatus> getItemStatuses() {
		return mItemStatuses;
	}

	/**
	 * Sets new mItemStatuses.
	 *
	 * @param pItemStatuses New value of mItemStatuses.
	 */
	public void setItemStatuses(List<ItemStatus> pItemStatuses) {
		mItemStatuses = pItemStatuses;
	}

	public Date getStartTime() {
		return mStartTime;
	}

	public void setStartTime(Date pStartTime) {
		mStartTime = pStartTime;
	}

	public Date getEndTime() {
		return mEndTime;
	}

	public void setEndTime(Date pEndTime) {
		mEndTime = pEndTime;
	}

	public String getFileNameOnly() {
		return mFileNameOnly;
	}

	public void setFileNameOnly(String pFileNameOnly) {
		mFileNameOnly = pFileNameOnly;
	}

	public String getAction() {
		return mAction;
	}

	public void setAction(String pAction) {
		mAction = pAction;
	}
}
