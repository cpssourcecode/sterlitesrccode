package vsg.load.info;

/**
 * @author Dmitry Golubev
 */
public class PathInfo {
	/**
	 * input dir
	 */
	private String mInputDir;
	/**
	 * archive dir
	 */
	private String mArchiveDir;
	/**
	 * error dir
	 */
	private String mErrorDir;
	/**
	 * flag dir
	 */
	private String mFullDir;
	/**
	 * flag dir
	 */
	private String mFullArchiveDir;

	/**
	 *  AuditReportDir
	 */
	private String mAuditReportDir;

	/**
	 * file prefix
	 */
	private String mFilePrefix;
	/**
	 * file ext
	 */
	private String mFileExt = ".csv";
	/**
	 * file postfix insert
	 */
	private String mPostfixInsert;
	/**
	 * file postfix delete
	 */
	private String mPostfixDelete;
	/**
	 * file postfix update
	 */
	private String mPostfixUpdate;
	/**
	 * file postfix full
	 */
	private String mPostfixFull;
	/**
	 * flag that file contains bean info (false - relations file)
	 */
	private boolean mBeanFile = true;
	/**
	 * lock file name
	 */
	private String mLockFile;
	
	private boolean sortByTime=false;

	/**
	 * Divider in import file name between prefix and data : CPS_Catalog_Export_-060215-.json
	 * default="-", in JSON file "_"
	 */
	private String mImportFileNameDivider = "-";


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return input dir for loaders
	 * @return input dir for loaders
	 */
	public String getInputDir() {
		return mInputDir;
	}

	/**
	 * init input dir for loaders
	 * @param pInputDir input dir for loaders
	 */
	public void setInputDir(String pInputDir) {
		mInputDir = pInputDir;
	}

	/**
	 * return archive dir for loaders
	 * @return archive dir for loaders
	 */
	public String getArchiveDir() {
		return mArchiveDir;
	}

	/**
	 * init archive dir for loaders
	 * @param pArchiveDir archive dir for loaders
	 */
	public void setArchiveDir(String pArchiveDir) {
		mArchiveDir = pArchiveDir;
	}

	/**
	 * return error dir for loaders
	 * @return error dir for loaders
	 */
	public String getErrorDir() {
		return mErrorDir;
	}

	/**
	 * init error dir for loaders
	 * @param pErrorDir error dir for loaders
	 */
	public void setErrorDir(String pErrorDir) {
		mErrorDir = pErrorDir;
	}

	/**
	 * return full dir for loaders
	 * @return full dir for loaders
	 */
	public String getFullDir() {
		return mFullDir;
	}

	/**
	 * init full dir for loaders
	 * @param pFullDir full dir for loaders
	 */
	public void setFullDir(String pFullDir) {
		mFullDir = pFullDir;
	}

	/**
	 * return full archive dir for loaders
	 * @return full archive dir for loaders
	 */
	public String getFullArchiveDir() {
		return mFullArchiveDir;
	}

	/**
	 * init full archive dir for loaders
	 * @param pFullArchiveDir full archive dir for loaders
	 */
	public void setFullArchiveDir(String pFullArchiveDir) {
		mFullArchiveDir = pFullArchiveDir;
	}

	/**
	 * return file prefix, which will be used to filter files
	 * @return file prefix, which will be used to filter files
	 */
	public String getFilePrefix() {
		return mFilePrefix;
	}

	/**
	 * init file prefix, which will be used to filter files
	 * @param pFilePrefix file prefix, which will be used to filter files
	 */
	public void setFilePrefix(String pFilePrefix) {
		mFilePrefix = pFilePrefix;
	}

	/**
	 * return file extension, which will be used to filter files
	 * @return file extension, which will be used to filter files
	 */
	public String getFileExt() {
		return mFileExt;
	}

	/**
	 * init file extension, which will be used to filter files
	 * @param pFileExt file extension, which will be used to filter files
	 */
	public void setFileExt(String pFileExt) {
		mFileExt = pFileExt;
	}

	/**
	 * set flag to insert data
	 * @return flag to insert data
	 */
	public String getPostfixInsert() {
		return mPostfixInsert;
	}

	/**
	 * init flag to insert data
	 * @param pPostfixInsert flag to insert data
	 */
	public void setPostfixInsert(String pPostfixInsert) {
		mPostfixInsert = pPostfixInsert;
	}

	/**
	 * return flag to delete data
	 * @return flag to delete data
	 */
	public String getPostfixDelete() {
		return mPostfixDelete;
	}

	/**
	 * init flag to delete data
	 * @param pPostfixDelete flag to delete data
	 */
	public void setPostfixDelete(String pPostfixDelete) {
		mPostfixDelete = pPostfixDelete;
	}

	/**
	 * return flag to update data
	 * @return flag to update data
	 */
	public String getPostfixUpdate() {
		return mPostfixUpdate;
	}

	/**
	 * init flag to update data
	 * @param pPostfixUpdate flag to update data
	 */
	public void setPostfixUpdate(String pPostfixUpdate) {
		mPostfixUpdate = pPostfixUpdate;
	}

	/**
	 * return flag to use full data to generate delta-files
	 * @return flag to use full data to generate delta-files
	 */
	public String getPostfixFull() {
		return mPostfixFull;
	}

	/**
	 * init flag to use full data to generate delta-files
	 * @param pPostfixFull flag to use full data to generate delta-files
	 */
	public void setPostfixFull(String pPostfixFull) {
		mPostfixFull = pPostfixFull;
	}

	/**
	 * return flag that file contains bean info (false - relations file)
	 * @return flag that file contains bean info (false - relations file)
	 */
	public boolean isBeanFile() {
		return mBeanFile;
	}

	/**
	 * init flag that file contains bean info (false - relations file)
	 * @param pBeanFile flag that file contains bean info (false - relations file)
	 */
	public void setBeanFile(boolean pBeanFile) {
		mBeanFile = pBeanFile;
	}


	/**
	 * retrieve lock file name
	 * @return lock file name
	 */
	public String getLockFile() {
		return mLockFile;
	}

	/**
	 * init lock file name
	 * @param pLockFile lock file name
	 */
	public void setLockFile(String pLockFile) {
		mLockFile = pLockFile;
	}


	public String getImportFileNameDivider() {
		return mImportFileNameDivider;
	}

	public void setImportFileNameDivider(String pImportFileNameDivider) {
		mImportFileNameDivider = pImportFileNameDivider;
	}


	public String getAuditReportDir() {
		return mAuditReportDir;
	}

	public void setAuditReportDir(String pAuditReportDir) {
		mAuditReportDir = pAuditReportDir;
	}

	public boolean isSortByTime() {
	    return sortByTime;
	}

	public void setSortByTime(boolean sortByTime) {
	    this.sortByTime = sortByTime;
	}
}
