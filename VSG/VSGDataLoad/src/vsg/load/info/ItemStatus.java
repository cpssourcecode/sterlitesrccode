package vsg.load.info;

/**
 * @author Andy Porter
 */
public class ItemStatus {
	private String mItemNumber = "";
	private String mAction = "";
	private String mOldST = "";
	private String mNewST = "";

	public ItemStatus(String pItemNumber, String pAction, String pOldST, String pNewST) {
		mItemNumber = pItemNumber;
		mAction = pAction;
		mOldST = pOldST;
		mNewST = pNewST;
	}

	public String getItemNumber() {
		return mItemNumber;
	}

	public void setItemNumber(String pItemNumber) {
		mItemNumber = pItemNumber;
	}

	public String getAction() {
		return mAction;
	}

	public void setAction(String pAction) {
		mAction = pAction;
	}

	public String getOldST() {
		return mOldST;
	}

	public void setOldST(String pOldST) {
		mOldST = pOldST;
	}

	public String getNewST() {
		return mNewST;
	}

	public void setNewST(String pNewST) {
		mNewST = pNewST;
	}
}
