package vsg.load.info;

import atg.nucleus.ValueFormatter;
import vsg.commerce.endeca.index.IIndexingTrigger;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class StatisticsService implements ValueFormatter {
	/**
	 * loaders statistics
	 */
	private List<StatisticsLoader> mLoaderStatistics = new ArrayList<StatisticsLoader>();

	/**
	 * current import status
	 */
	private String mStatus;

	private List<String> mInitialImportFiles;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return list of loaders statistics
	 *
	 * @return list of loaders statistics
	 */
	public List<StatisticsLoader> getLoaderStatistics() {
		return mLoaderStatistics;
	}

	/**
	 * init list of loaders statistics
	 *
	 * @param pStatisticsLoader list of loaders statistics
	 */
	public void addLoaderStatistics(StatisticsLoader pStatisticsLoader) {
		mLoaderStatistics.add(pStatisticsLoader);
	}

	/**
	 * return current import status
	 *
	 * @return current import status
	 */
	public String getStatus() {
		return mStatus;
	}

	/**
	 * init current import status
	 *
	 * @param pStatus current import status
	 */
	public void setStatus(String pStatus) {
		mStatus = pStatus;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Gets the indexing level needed according to loader statistic.
	 *
	 * @return the indexing level
	 */
	public int getIndexingLevel() {
		int indexingLevel = IIndexingTrigger.INDEX_NONE_LEVEL;
		for (StatisticsLoader singleStatistic : getLoaderStatistics()) {
			indexingLevel = mergeIndexingLevels(indexingLevel, singleStatistic.getIndexingLevel());
		}
		return indexingLevel;
	}

	/**
	 * Check if records modified.
	 *
	 * @return true, if successful
	 */
	public boolean checkIfRecordsModified() {
		boolean modified = false;
		for (StatisticsLoader statisticsLoader : getLoaderStatistics()) {
			if (statisticsLoader.checkIfRecordsModified()) {
				modified = true;
				break;
			}
		}
		return modified;
	}

	/**
	 * Merge indexing levels.
	 *
	 * @param pFirstIndexingLevel  the first indexing level
	 * @param pSecondIndexingLevel the second indexing level
	 * @return the int
	 */
	protected int mergeIndexingLevels(int pFirstIndexingLevel, int pSecondIndexingLevel) {
		return Math.max(pFirstIndexingLevel, pSecondIndexingLevel);
	}

	@Override
	public String formatValue() {
		StringBuilder sb = new StringBuilder();
		sb.append("Statistics:<br>");
		for (StatisticsLoader statisticsLoader : getLoaderStatistics()) {
			sb.append(statisticsLoader.getStatisticsPresentation().replaceAll("\\n", "<br>"));
			sb.append("<br>");
		}
		return sb.toString();
	}

	@Override
	public String formatLongValue() {
		return null;
	}


	/**
	 * Sets new pInitialImportFiles.
	 *
	 * @param pInitialImportFiles New value of pInitialImportFiles.
	 */
	public void setInitialImportFiles(List<String> pInitialImportFiles) {
		mInitialImportFiles = pInitialImportFiles;
	}

	/**
	 * Gets pInitialImportFiles.
	 *
	 * @return Value of pInitialImportFiles.
	 */
	public List<String> getInitialImportFiles() {
		return mInitialImportFiles;
	}


}
