package vsg.load.info;

/**
 * @author Dmitry Golubev
 */
public class ValidationResult {
	/**
	 * is valid flag
	 */
	private boolean mValid = false;
	/**
	 * skip validation errors notification flag
	 */
	private boolean mSkip = false;
	/**
	 * validation message
	 */
	private String mMsg = null;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * private constructor
	 */
	private ValidationResult() {
	}

	/**
	 * return success validation result
	 * @return success validation result
	 */
	public static ValidationResult validResult() {
		ValidationResult result = new ValidationResult();
		result.mValid = true;
		return result;
	}

	/**
	 * return fail validation result with reason message
	 * @param pValidationMsg validation message
	 * @return fail validation result
	 */
	public static ValidationResult notValidResult(String pValidationMsg) {
		ValidationResult result = new ValidationResult();
		result.mValid = false;
		result.mMsg = pValidationMsg;
		return result;
	}

	/**
	 * check if validation passed
	 * @return validation passed
	 */
	public boolean isValid() {
		return mValid;
	}

	/**
	 * return validation message
	 * @return validation message
	 */
	public String getMsg() {
		return mMsg;
	}

	public boolean isSkip() {
		return mSkip;
	}

	public void setSkip(boolean pSkip) {
		mSkip = pSkip;
	}
}
