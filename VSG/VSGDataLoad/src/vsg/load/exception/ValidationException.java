package vsg.load.exception;

/**
 * @author Dmitry Golubev
 */
public class ValidationException extends RuntimeException {
	/**
	 * constructor with validation message
	 * @param message validation message
	 */
	public ValidationException(String message) {
		super(message);
	}
}
