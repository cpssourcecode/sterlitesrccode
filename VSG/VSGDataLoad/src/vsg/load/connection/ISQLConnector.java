package vsg.load.connection;

import java.sql.Connection;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public interface ISQLConnector extends IConnector {
	/**
	 * return database connection if repository is not switchable
	 * @return database connection if repository is not switchable
	 */
	public Connection getConnection();
	/**
	 * return list of database connections if repository is switchable
	 * @return list of database connections if repository is switchable
	 */
	public List<Connection> getConnections();
	/**
	 * check if single connection repository
	 * @return result of check if single connection repository
	 */
	public boolean isSingleConnection();
}
