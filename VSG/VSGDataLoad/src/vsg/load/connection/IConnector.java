package vsg.load.connection;

import atg.repository.Repository;

/**
 * @author Dmitry Golubev
 */
public interface IConnector {
	/**
	 * return specified repository
	 * @return specified repository
	 */
	public Repository getRepository();

	/**
	 * return limit per one transaction
	 * @return limit per one transaction
	 */
	public int getLimitPerTransaction();
}
