package vsg.load.connection.impl;

import atg.repository.Repository;
import vsg.load.connection.IConnector;
import vsg.load.tools.ILoggerTools;

/**
 * @author Dmitry Golubev
 */
public class Connector implements IConnector {
	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;
	/**
	 * limit per one transaction
	 */
	private int mLimitPerTransaction = -1;
	/**
	 * repository
	 */
	private Repository mRepository;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * return logger tools
	 * @return logger tools
	 */
	public ILoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	/**
	 * init logger tools
	 * @param pLoggerTools logger tools
	 */
	public void setLoggerTools(ILoggerTools pLoggerTools) {
		this.mLoggerTools = pLoggerTools;
	}

	/**
	 * return limit per one transaction
	 * @return limit per one transaction
	 */
	@Override
	public int getLimitPerTransaction() {
		return mLimitPerTransaction;
	}

	/**
	 * init limit per one transaction
	 * @param pLimitPerTransaction limit per one transaction
	 */
	public void setLimitPerTransaction(int pLimitPerTransaction) {
		mLimitPerTransaction = pLimitPerTransaction;
	}

	/**
	 * return specified repository
	 * @return specified repository
	 */
	@Override
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * init item's repository
	 * @param pRepository item's repository
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}
}
