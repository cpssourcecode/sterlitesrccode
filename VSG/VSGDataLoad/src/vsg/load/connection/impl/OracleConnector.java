package vsg.load.connection.impl;

import atg.adapter.gsa.GSARepository;
import atg.service.jdbc.SwitchingDataSource;
import vsg.load.connection.ISQLConnector;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class OracleConnector extends Connector implements ISQLConnector {
// -----------------------------------------------------------------------------------------------------------------

	/**
	 * check is repository is from tests
	 * @return result of check is repository is from tests
	 */
	public boolean isTestRepository() {
		return "InitializingGSA".equals( getRepository().getClass().getSimpleName() );
	}

	/**
	 * check if single connection repository
	 * @return result of check if single connection repository
	 */
	@Override
	public boolean isSingleConnection() {
		return ! ( ((GSARepository)getRepository()).getDataSource() instanceof SwitchingDataSource );
	}

	/**
	 * return connection from dataSource
	 * @param pDataSource repository data source
	 * @return database connection
	 */
	private Connection getConnection(DataSource pDataSource) {
		try {
			return pDataSource.getConnection();
		} catch (SQLException e) {
			getLoggerTools().logError(e);
		}
		return null;
	}

	/**
	 * return database connection if repository is not switchable
	 * @return database connection if repository is not switchable
	 */
	@Override
	public Connection getConnection() {
		if(isTestRepository()) {
			return getConnection(((GSARepository)getRepository()).getDataSource());
		} else {
			if(!isSingleConnection()) {
				getLoggerTools().logError("Trying to get single connection on switchingDataSource.");
				return null;
			}
			return getConnection(((GSARepository)getRepository()).getDataSource());
		}
	}

	/**
	 * return list of database connections if repository is switchable
	 * @return list of database connections if repository is switchable
	 */
	@Override
	public List<Connection> getConnections() {
		if(isTestRepository()) {
			List<Connection> connections = new ArrayList<Connection>();
			connections.add( getConnection(((GSARepository)getRepository()).getDataSource()) );
			return connections;
		} else {
			if(isSingleConnection()) {
				getLoggerTools().logError("Trying to get multi connections on DataSource.");
				return null;
			}
			SwitchingDataSource switchingDataSource = (SwitchingDataSource) ((GSARepository)getRepository())
					.getDataSource();
			List<Connection> connections = new ArrayList<Connection>();
			for( Object dataSourceObj : switchingDataSource.getDataStores() ) {
				connections.add( getConnection((DataSource)dataSourceObj) );
			}
			return connections;
		}
	}
}
