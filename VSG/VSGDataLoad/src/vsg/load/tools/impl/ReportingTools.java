package vsg.load.tools.impl;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import vsg.load.email.EmailSettings;
import vsg.load.info.FileInfo;
import vsg.load.info.StatisticsLoader;
import vsg.load.loader.RecordValidationInfo;
import vsg.load.loader.impl.Loader;
import vsg.load.tools.IReportingTools;
import vsg.notifications.EmailService;

import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public class ReportingTools implements IReportingTools {

	/**
	 * logger tools
	 */
	private LoggerTools mLoggerTools;

	/**
	 * report repository
	 */
	private MutableRepository mReportRepository;

	/**
	 * server name
	 */
	private String mServerName;

	//------------------------------------------------------------------------------------------------------------------

	public LoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	public void setLoggerTools(LoggerTools pLoggerTools) {
		mLoggerTools = pLoggerTools;
	}

	public MutableRepository getReportRepository() {
		return mReportRepository;
	}

	public void setReportRepository(MutableRepository pReportRepository) {
		mReportRepository = pReportRepository;
	}

	public String getServerName() {
		return mServerName;
	}

	public void setServerName(String pServerName) {
		mServerName = pServerName;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * send loader result report
	 * @param pLoader loader
	 * @param pStartTime start time
	 * @param pEndTime end time
	 */
	@Override
	public void sendReport(Loader pLoader, Date pStartTime, Date pEndTime) {
		EmailSettings emailSettings = pLoader.getCommonSettings().getEmailSettings();
		EmailService emailService = emailSettings.getEmailService();
/*
		emailService.sendLoaderImport(
				emailSettings.getEmailRecipient(),
				getServerName(), pLoader.getLoaderName(),
				getEmail(pLoader, pStartTime, pEndTime)
		);
*/
	}
	/**
	 * send loader result report
	 * @param pLoader loader
	 * @param pFileName full-file name
	 * @param pMsg report message
	 */
	@Override
	public void sendReportFullFileAnalyze(Loader pLoader, String pFileName, String pMsg) {
		EmailSettings emailSettings = pLoader.getCommonSettings().getEmailSettings();
		EmailService emailService = emailSettings.getEmailService();
/*
		emailService.sendLoaderFullFileProcess(
				emailSettings.getEmailRecipient(),
				getServerName(),
				pLoader.getLoaderName(),
				pFileName,
				pMsg
		);
*/
	}

	/**
	 * form loader result report message
	 * @return email message
	 * @param pLoader loader
	 * @param pStartTime start time
	 * @param pEndTime end time
	 */
	private String getEmail(Loader pLoader, Date pStartTime, Date pEndTime) {
		StringBuilder builder = new StringBuilder();
		formMailFileInfo(pLoader.getFileInfo(), builder);
		builder.append("Start time: ").append(getTime(pStartTime)).append("<br>");
		builder.append(pLoader.getTime("Duration time", pEndTime.getTime() - pStartTime.getTime())).append("<br>");
		formMailStatistics(pLoader, builder);
		formMailErrorMsg(builder, pLoader.getStatistics());
		return builder.toString();
	}

	/**
	 * format date to string
	 * @param pDate date to string
	 * @return formatted date
	 */
	private String  getTime(Date pDate) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		return simpleDateFormat.format(pDate);
	}

	/**
	 * form statistics message
	 * @param pLoader loader
	 * @param pBuilder email message string builder
	 */
	private void formMailStatistics(Loader pLoader, StringBuilder pBuilder) {
		StatisticsLoader statistics = pLoader.getStatistics();
		FileInfo fileInfo = pLoader.getFileInfo();
		if(statistics.isSucceeded()) {
			pBuilder.append("<br>Load is successful.<br>");
		} else {
			pBuilder.append("<br>Load is failed.<br>");
			pBuilder.append("Fail reason: ").append(
					statistics.getFailReason().replaceAll("\\\n", "<br>")
			).append("<br>");
		}
		pBuilder.append("<br>Proceeded records: ").append(statistics.getCountProceeded()).append("<br>");
		if(fileInfo.isFileToDelete()) {
			pBuilder.append("Deleted records: ").append(statistics.getCountDelete()).append("<br>");
		} else
		if(fileInfo.isFileToInsert()) {
			pBuilder.append("Inserted count: ").append(statistics.getCountInsert()).append("<br>");
		} else
		if(fileInfo.isFileToUpdate()) {
			pBuilder.append("Updated count: ").append(statistics.getCountUpdate()).append("<br>");
		}
	}

	/**
	 * form file info message
	 * @param pFileInfo loader file info
	 * @param pBuilder email message string builder
	 */
	private void formMailFileInfo(FileInfo pFileInfo, StringBuilder pBuilder) {
		String hostName = "";
		try {
			hostName = java.net.InetAddress.getLocalHost().getHostAddress();
			hostName = " on "+hostName;
		} catch (UnknownHostException e) {
			if(getLoggerTools().isLoggingError()){
				getLoggerTools().logError(e);
			}
		}
		pBuilder.append("Import file ").append(hostName).append(":  ").
				append(pFileInfo.getFile().getName()).append("<br>");
		pBuilder.append("Action:  ");
		switch (pFileInfo.getAction()) {
			case FileInfo.ACTION_DELETE: pBuilder.append("DELETE."); break;
			case FileInfo.ACTION_INSERT: pBuilder.append("INSERT."); break;
			case FileInfo.ACTION_UPDATE: pBuilder.append("UPDATE."); break;
		}
		pBuilder.append("<br>");
		pBuilder.append("<br>");
	}

	/**
	 * form error message
	 * @param pBuilder email message string builder
	 * @param pStatisticsLoader loader statistics
	 */
	private void formMailErrorMsg(StringBuilder pBuilder, StatisticsLoader pStatisticsLoader) {
		if(pStatisticsLoader.getErrorRecords()!=null && pStatisticsLoader.getErrorRecords().size() > 0) {
			pBuilder.append("<br>Validation error count:  ").append(pStatisticsLoader.getCountError()).append("<br>");
			pBuilder.append("<br><table border='1' cellspacing='0' cellpadding='5'>");

			pBuilder.append("<tr><td>row</td><td>id</td><td>message</td>");

			for(RecordValidationInfo validationInfo : pStatisticsLoader.getErrorRecords()) {
				pBuilder.append("<tr><td>");
				pBuilder.append(validationInfo.getRecord().getIndex());
				pBuilder.append("</td><td>");
				pBuilder.append(validationInfo.getRecord().getId());
				pBuilder.append("</td><td>");
				pBuilder.append(validationInfo.getValidationMsg());
				pBuilder.append("</td></tr>");
			}
			pBuilder.append("</table>");
		}
	}

	/**
	 * store loader result to repository
	 * @param pLoader loader
	 * @param pStartTime start time
	 * @param pEndTime end time
	 */
	public void storeReport(Loader pLoader, Date pStartTime, Date pEndTime) {
		try {
			FileInfo fileInfo = pLoader.getFileInfo();
			StatisticsLoader statistics = pLoader.getStatistics();

			String fileName = fileInfo.getFile().getName();
			try {
				String hostName = java.net.InetAddress.getLocalHost().getHostAddress();
				fileName = hostName+": "+fileName;
			} catch (UnknownHostException e) {
				if(getLoggerTools().isLoggingError()){
					getLoggerTools().logError(e);
				}
			}

			MutableRepositoryItem report = getReportRepository().createItem("report");
			report.setPropertyValue("fileName", fileName);
			String fileAction = "";
			switch (fileInfo.getAction()) {
				case FileInfo.ACTION_DELETE: fileAction = "DELETE"; break;
				case FileInfo.ACTION_INSERT: fileAction = "INSERT"; break;
				case FileInfo.ACTION_UPDATE: fileAction = "UPDATE"; break;
			}
			report.setPropertyValue("fileAction", fileAction);
			report.setPropertyValue("startTime", pStartTime);
			report.setPropertyValue("duration", pEndTime.getTime() - pStartTime.getTime());
			report.setPropertyValue("durationString", pLoader.getTime("Duration time",
					pEndTime.getTime() - pStartTime.getTime()));
			report.setPropertyValue("isImported", statistics.isSucceeded());
			if(!statistics.isSucceeded()) {
				report.setPropertyValue("failReason", statistics.getFailReason());
			}

			initReportRecordsCount(report, statistics);
			report = (MutableRepositoryItem) getReportRepository().addItem(report);

			initReportErrors(report, statistics);

			getReportRepository().updateItem(report);
		} catch (RepositoryException e) {
			getLoggerTools().logError(e);
		}
	}

	/**
	 * init report validation errors
	 * @param pReportItem report item
	 * @param pStatistics loader statistics
	 * @throws RepositoryException if error occurs
	 */
	private void initReportRecordsCount(MutableRepositoryItem pReportItem, StatisticsLoader pStatistics) throws
			RepositoryException {
		int countImported = pStatistics.getCountDelete() + pStatistics.getCountInsert() + pStatistics.getCountUpdate();
		pReportItem.setPropertyValue("countProceed",  pStatistics.getCountProceeded());
		pReportItem.setPropertyValue("countImported", countImported );
		pReportItem.setPropertyValue("countError",    pStatistics.getCountError());
	}

	/**
	 * init report validation errors
	 * @param pReportItem report item
	 * @param pStatistics loader statistics
	 * @throws RepositoryException if error occurs
	 */
	private void initReportErrors(MutableRepositoryItem pReportItem, StatisticsLoader pStatistics) throws
			RepositoryException {
		// Set<MutableRepositoryItem> reportErrors = new HashSet<MutableRepositoryItem>();
		for(RecordValidationInfo validationInfo : pStatistics.getErrorRecords()) {
			MutableRepositoryItem reportError = getReportRepository().createItem("reportError");
			reportError.setPropertyValue("rowNumber", validationInfo.getRecord().getIndex());
			reportError.setPropertyValue("rowId",     validationInfo.getRecord().getId());
			reportError.setPropertyValue("message",   validationInfo.getValidationMsg());
			reportError.setPropertyValue("report",    pReportItem);
			// reportErrors.add(reportError);
			getReportRepository().addItem(reportError);
		}
		// pReportItem.setPropertyValue("reportErrors", reportErrors);
	}


}
