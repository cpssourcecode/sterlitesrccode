package vsg.load.tools.impl;

import atg.core.util.StringUtils;
import vsg.load.info.FieldsInfo;
import vsg.load.info.ValidationResult;
import vsg.load.loader.Record;
import vsg.load.tools.ILoggerTools;
import vsg.load.tools.IValidationTools;
import vsg.load.validator.IValidator;

import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class ValidationTools implements IValidationTools {

	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;

	/**
	 * return logger tools
	 * @return logger tools
	 */
	public ILoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	/**
	 * init logger tools
	 * @param pLoggerTools logger tools
	 */
	public void setLoggerTools(ILoggerTools pLoggerTools) {
		this.mLoggerTools = pLoggerTools;
	}

	/**
	 * return validation result
	 * @param pRecord record to be validated
	 * @param pFieldsInfo fields info
	 * @return validation result
	 */
	@Override
	public ValidationResult validateRecord(Record pRecord, FieldsInfo pFieldsInfo) {
		Map<String, IValidator> fieldsValidators = pFieldsInfo.getFieldsValidator();
		if(fieldsValidators!=null) {
			ValidationResult result = isValid(pFieldsInfo, pRecord, fieldsValidators);
			if(result!=null && !result.isValid()) {
				return result;
			}
			if(pRecord.getSubRecords()!=null) {
				for(Record subRecord : pRecord.getSubRecords().values()) {
					result = isValid(pFieldsInfo, subRecord, fieldsValidators);
					if(result!=null && !result.isValid()) {
						return result;
					}
				}
			}
		} else {
			return ValidationResult.validResult();
		}
		return ValidationResult.validResult();
	}

	/**
	 * check if record is valid
	 * @param pFieldsInfo fields info
	 * @param pRecord record to be validated
	 * @param pFieldsValidators declared fields validators
	 * @return  record is valid
	 */
	private ValidationResult isValid(FieldsInfo pFieldsInfo, Record pRecord, Map<String,
			IValidator> pFieldsValidators) {
		boolean isSubRecord = !StringUtils.isBlank(pRecord.getName());
		for(String fieldName : pFieldsValidators.keySet()) {
			String fieldNameFromInitial = fieldName;
			if(isSubRecord && pFieldsInfo.isSubRecordField(fieldName, pRecord.getName())) {
				fieldNameFromInitial = fieldName.replace(pRecord.getName()+".", "");
				if(!pRecord.getValuesInitial().containsKey( fieldNameFromInitial )) {
					continue;
				}
			} else {
				if(!pRecord.getValuesInitial().containsKey(fieldName)) {
					continue;
				}
			}
			IValidator validator = pFieldsValidators.get( fieldName );
			boolean validated = validator.isValid(pRecord.getValueInitial(fieldNameFromInitial), fieldName, pRecord);
			if(!validated) {
				ValidationResult result = ValidationResult.notValidResult(validator.getValidationMessage());
				result.setSkip( validator.isJustSkip() );
				return result;
			}
		}
		return null;
	}
}
