package vsg.load.tools.impl;

import atg.core.util.StringUtils;
import vsg.load.file.IReader;
import vsg.load.file.IWriter;
import vsg.load.file.csv.CSVBase;
import vsg.load.file.csv.CSVReader;
import vsg.load.file.csv.CSVWriter;
import vsg.load.info.FieldsInfo;
import vsg.load.info.FileInfo;
import vsg.load.info.PathInfo;
import vsg.load.loader.CommonSettings;
import vsg.load.tools.IImportTools;
import vsg.load.tools.ILoggerTools;

import java.io.*;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.regex.Pattern;


/**
 * @author Dmitry Golubev
 */
public class ImportTools implements IImportTools {
	/**
	 * error file prefix
	 */
	private static final String ERROR_FILE_PREFIX = "errors_";
	/**
	 * error file type
	 */
	private static final String ERROR_FILE_TYPE = ".csv";
	/**
	 * audit report file type
	 */
	private static final String AUDIT_REPORT_FILE_TYPE = ".json";
	/**
	 * audit report name
	 */
	private static final String AUDIT_REPORT = "Audit_Report_";
	/**
	 * file comparator
	 */
	private Comparator<File> mFileComparator;
	/**
	 * fields info
	 */
	private FieldsInfo mFieldsInfo;
	/**
	 * common settings for loaders
	 */
	private CommonSettings mCommonSettings;
	/**
	 * path info
	 */
	private PathInfo mPathInfo;
	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;
	/**
	 * file accept pattern
	 */
	private Pattern mFileAcceptPattern = null;
	/**
	 * full-file accept pattern
	 */
	private Pattern mFullFileAcceptPattern = null;
	/**
	 * file string data separator
	 */
	private char mFileSeparator = CSVBase.DEFAULT_SEPARATOR;
	/**
	 * flag that indicates to analyse data with quotes
	 */
	private boolean mUseQuotes = true;
	/**
	 * flag that indicates to archive processed file
	 */
	private boolean mArchiveFile = true;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return path info
	 *
	 * @return path info
	 */
	@Override
	public PathInfo getPathInfo() {
		return mPathInfo;
	}

	/**
	 * init path info
	 *
	 * @param pPathInfo path info
	 */
	public void setPathInfo(PathInfo pPathInfo) {
		mPathInfo = pPathInfo;
	}

	/**
	 * return fields info
	 *
	 * @return fields info
	 */
	@Override
	public FieldsInfo getFieldsInfo() {
		return mFieldsInfo;
	}

	/**
	 * init fields info
	 *
	 * @param pFieldsInfo fields info
	 */
	public void setFieldsInfo(FieldsInfo pFieldsInfo) {
		mFieldsInfo = pFieldsInfo;
	}

	/**
	 * return logger tools
	 *
	 * @return logger tools
	 */
	@Override
	public ILoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	/**
	 * init logger tools
	 *
	 * @param pLoggerTools logger tools
	 */
	public void setLoggerTools(ILoggerTools pLoggerTools) {
		mLoggerTools = pLoggerTools;
	}

	/**
	 * return common settings for loaders
	 *
	 * @return common settings for loaders
	 */
	public CommonSettings getCommonSettings() {
		return mCommonSettings;
	}

	/**
	 * init common settings for loaders
	 *
	 * @param pCommonSettings common settings for loaders
	 */
	public void setCommonSettings(CommonSettings pCommonSettings) {
		mCommonSettings = pCommonSettings;
	}

	/**
	 * return pattern to accept file for import
	 *
	 * @return pattern to accept file for import
	 */
	private Pattern getFileAcceptPattern() {
		String divider = getPathInfo().getImportFileNameDivider();
		String dividerBeforePostfix = getPathInfo().getImportFileNameDivider();
		if (StringUtils.isBlank(getPathInfo().getPostfixDelete()) && StringUtils.isBlank(getPathInfo().getPostfixUpdate())
				&& StringUtils.isBlank(getPathInfo().getPostfixInsert())) {
			dividerBeforePostfix = "";
		}

		if (mFileAcceptPattern == null) {
			String fileAcceptRegexp = "^" + getPathInfo().getFilePrefix() + divider + "?\\d+" + dividerBeforePostfix + "(" +
					(StringUtils.isBlank(getPathInfo().getPostfixDelete()) ? "" : (getPathInfo().getPostfixDelete() + "|")) +
					(StringUtils.isBlank(getPathInfo().getPostfixUpdate()) ? "" : (getPathInfo().getPostfixUpdate() + "|")) +
					(StringUtils.isBlank(getPathInfo().getPostfixInsert()) ? "" : (getPathInfo().getPostfixInsert())) +
					")" + getPathInfo().getFileExt();
			mFileAcceptPattern = Pattern.compile(fileAcceptRegexp.toLowerCase());
		}
		return mFileAcceptPattern;
	}

	/**
	 * return pattern to accept full file for import
	 *
	 * @return pattern to accept full file for import
	 */
	private Pattern getFullFileAcceptPattern() {
		if (mFullFileAcceptPattern == null) {
			String fileAcceptRegexp = "^" + getPathInfo().getFilePrefix() + "-?\\d+-(" +
					(StringUtils.isBlank(getPathInfo().getPostfixFull()) ? "" : (getPathInfo().getPostfixFull() + "|")) +
					")" + getPathInfo().getFileExt();
			mFullFileAcceptPattern = Pattern.compile(fileAcceptRegexp.toLowerCase());
		}
		return mFullFileAcceptPattern;
	}

	public char getFileSeparator() {
		return mFileSeparator;
	}

	public void setFileSeparator(char pFileSeparator) {
		mFileSeparator = pFileSeparator;
	}

	public boolean isUseQuotes() {
		return mUseQuotes;
	}

	public void setUseQuotes(boolean pUseQuotes) {
		mUseQuotes = pUseQuotes;
	}

	public boolean isArchiveFile() {
		return mArchiveFile;
	}

	public void setArchiveFile(boolean pArchiveFile) {
		mArchiveFile = pArchiveFile;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * reset file accept pattern
	 */
	public void resetFileAcceptPattern() {
		mFileAcceptPattern = null;
	}

	/**
	 * file info object
	 *
	 * @param pFile file to be imported
	 * @return file info object
	 */
	@Override
	public FileInfo getFileInfo(File pFile) {
		if (!StringUtils.isBlank(getPathInfo().getPostfixDelete()) &&
				pFile.getName().contains(getPathInfo().getPostfixDelete())) {
			return new FileInfo(pFile, FileInfo.ACTION_DELETE);
		} else if (!StringUtils.isBlank(getPathInfo().getPostfixInsert()) &&
				pFile.getName().contains(getPathInfo().getPostfixInsert())) {
			return new FileInfo(pFile, FileInfo.ACTION_INSERT);
		} else if (!StringUtils.isBlank(getPathInfo().getPostfixUpdate()) &&
				pFile.getName().contains(getPathInfo().getPostfixUpdate())) {
			return new FileInfo(pFile, FileInfo.ACTION_UPDATE);
		}

		if (getLoggerTools().isLoggingDebug()) {
			getLoggerTools().logDebug(
					"File " + pFile.getName() + " doesn't contain any specified action flag and is SKIPPED.");
		}

		return new FileInfo(pFile, -1);
	}

	/**
	 * return writer
	 *
	 * @param pFile file to be written
	 * @return writer
	 */
	@Override
	public IWriter getWriter(File pFile) {
		IWriter writer = null;
		try {
			writer = new CSVWriter(getLoggerTools());
			writer.setWriter(pFile, getCommonSettings().getEncoding());

		} catch (Exception ie) {
			if (getLoggerTools().isLoggingError()) {
				getLoggerTools().logError(ie);
			}
		}

		return writer;
	}

/*
	@Override
	public IReader getReader(File pFile) {
		IReader reader = null;

		try {
			FileInputStream inputStream = new FileInputStream(pFile);
			Reader fileReader;
			try {
				fileReader = new InputStreamReader(inputStream, getCommonSettings().getEncoding());
			} catch (UnsupportedEncodingException e) {
				fileReader = new InputStreamReader(inputStream);
			}
			reader = new CSVReader(fileReader, getLoggerTools());
			reader.setSeparator(getFileSeparator());
			if (!isUseQuotes()) {
				reader.setQuoteChar(CSVReader.NO_QUOTE_CHARACTER);
			}
			if (getLoggerTools().isLoggingDebug()) {
				String readerEncoding = ((InputStreamReader) fileReader).getEncoding();
				getLoggerTools().logDebug(pFile.getName() + " - ready for read [" + readerEncoding + "]");
			}
		} catch (IOException ie) {
			if (getLoggerTools().isLoggingError()) {
				getLoggerTools().logError(ie);
			}
		}


		return reader;
	}
*/

	/**
	 * return reader
	 *
	 * @param pFile file to be red
	 * @return reader
	 */
	@Override
	public IReader getReader(File pFile, FileInputStream pInputStream) {
		IReader reader = null;
/*
		try {
			FileInputStream inputStream = new FileInputStream(pFile);
*/
		Reader fileReader;
		try {
			fileReader = new InputStreamReader(pInputStream, getCommonSettings().getEncoding());
		} catch (UnsupportedEncodingException e) {
			fileReader = new InputStreamReader(pInputStream);
		}
		reader = new CSVReader(fileReader, getLoggerTools());
		reader.setSeparator(getFileSeparator());
		if (!isUseQuotes()) {
			reader.setQuoteChar(CSVReader.NO_QUOTE_CHARACTER);
		}
		if (getLoggerTools().isLoggingDebug()) {
			String readerEncoding = ((InputStreamReader) fileReader).getEncoding();
			getLoggerTools().logDebug(pFile.getName() + " - ready for read [" + readerEncoding + "]");
		}
/*
		} catch (IOException ie) {
			if (getLoggerTools().isLoggingError()) {
				getLoggerTools().logError(ie);
			}
		}
*/

		return reader;
	}

	/**
	 * move file to archive dir
	 *
	 * @param pFile file to be archived
	 * @return success state of archive
	 */
	@Override
	public boolean archiveFile(File pFile) {
		if (isArchiveFile()) {
			return moveFile(pFile, getPathInfo().getArchiveDir());
		} else {
			getLoggerTools().logDebug("File is not archived according to settings: [" + pFile.getName() + "]");
		}
		return true;
	}

	/**
	 * move file to archive dir
	 *
	 * @param pFile      file to be archived
	 * @param pTargetDir archiv dir
	 * @return success state of move
	 */
	@Override
	public boolean moveFile(File pFile, String pTargetDir) {
		checkFileDir(pTargetDir);
		File archiveDir = new File(pTargetDir);
		File archivedFile = new File(archiveDir, pFile.getName());
		if (archivedFile.exists()) {
			if (archivedFile.delete()) {
				getLoggerTools().logError(
						archivedFile.getName() + " already exist and will be deleted from dir " + pTargetDir);
			} else {
				getLoggerTools().logError(
						archivedFile.getName() + " is already in " + pTargetDir + " and cann't be deleted");
			}
		}
		boolean result = pFile.renameTo(archivedFile);
		if (getLoggerTools().isLoggingDebug()) {
			if (result) {
				getLoggerTools().logDebug(pFile.getName() + " is moved");
			} else {
				getLoggerTools().logError(pFile.getName() + " is not moved");
				if (pFile.delete()) {
					getLoggerTools().logError(pFile.getName() + " is deleted");
				} else {
					getLoggerTools().logError(pFile.getName() + " can't be deleted");
				}
			}
		}
		return result;
	}

	/**
	 * move file to error dir
	 *
	 * @param pFile file to be placed in error dir
	 * @return success state of move
	 */
	@Override
	public boolean errorFile(File pFile) {
		checkErrorFileDir();
		File errorDir = new File(getPathInfo().getErrorDir());
		File errorFile = new File(errorDir, pFile.getName());
		if (errorFile.exists()) {
			if (errorFile.delete()) {
				getLoggerTools().logError(
						errorFile.getName() + " already exist and will be deleted from error dir.");
			} else {
				getLoggerTools().logError(errorFile.getName() + " is already in error dir and cann't be deleted.");
			}
		}
		boolean result = pFile.renameTo(errorFile);
		if (getLoggerTools().isLoggingDebug()) {
			if (result) {
				getLoggerTools().logDebug(pFile.getName() + " is placed in error dir");
			} else {
				getLoggerTools().logError(pFile.getName() + " is not placed in error dir");
				if (pFile.delete()) {
					getLoggerTools().logError(pFile.getName() + " is deleted");
				} else {
					getLoggerTools().logError(pFile.getName() + " can't be deleted");
				}
			}
		}
		return result;
	}

	/**
	 * copy file
	 *
	 * @param pFile     file to copy
	 * @param pCopyFile destination file
	 * @return success state of copy
	 */
	@Override
	public boolean copyFile(File pFile, File pCopyFile) {
		try {
			if (!pCopyFile.exists()) {
				pCopyFile.createNewFile();
			} else {
				pCopyFile.delete();
				pCopyFile.createNewFile();
			}
			try (FileChannel in = new FileInputStream(pFile).getChannel();
				 FileChannel out = new FileOutputStream(pCopyFile).getChannel();) {
				out.transferFrom(in, 0, in.size());
				out.close();
				in.close();
			}
		} catch (Exception e) {
			if (getLoggerTools().isLoggingError()) {
				getLoggerTools().logError(pFile.getName() + " can't be copied: " + e.toString());
			}
			return false;
		}
		return true;
	}

	/**
	 * check if error dir exist
	 */
	@Override
	public void checkErrorFileDir() {
		checkFileDir(getPathInfo().getErrorDir());
	}

	/**
	 * check if audit report dir exist
	 */
	@Override
	public void checkAuditReportFileDir() {
		checkFileDir(getPathInfo().getAuditReportDir());
	}

	/**
	 * check if archive dir exist
	 */
	@Override
	public void checkArchiveFileDir() {
		checkFileDir(getPathInfo().getArchiveDir());
	}

	/**
	 * check if file dir exist
	 *
	 * @param pDirName dir name
	 */
	public void checkFileDir(String pDirName) {
		File destinationDirectory = new File(pDirName);
		if (!destinationDirectory.exists()) {
			if (!destinationDirectory.mkdirs()) {
				if (getLoggerTools().isLoggingError()) {
					getLoggerTools().logError("'" + pDirName + "' is not created.");
				}
			} else {
				if (getLoggerTools().isLoggingDebug()) {
					getLoggerTools().logDebug("'" + pDirName + "' is created.");
				}
			}
		}
	}

	/**
	 * return new error file name
	 *
	 * @param pFile importing file
	 * @return new error file name
	 */
	@Override
	public String getErrorFileName(File pFile) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
		String date = format.format(new java.util.Date());
		return getPathInfo().getErrorDir() + "/" + pFile.getName() + ERROR_FILE_PREFIX + date + ERROR_FILE_TYPE;
	}

	/**
	 * return new error file name
	 *
	 * @param pFileNamePrefix importing fileName
	 * @return new error file name
	 */
	@Override
	public String getAuditReportFileName(String pFileNamePrefix) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
		String date = format.format(new java.util.Date());
		String fileNamePrefix = pFileNamePrefix;
		if (StringUtils.isBlank(fileNamePrefix)) {
			fileNamePrefix = AUDIT_REPORT;
		}
		return getPathInfo().getAuditReportDir() + "/" + fileNamePrefix + date + AUDIT_REPORT_FILE_TYPE;
	}

	/**
	 * return new error file name
	 *
	 * @param pFileNamePrefix importing fileName
	 * @return new error file name
	 */
	@Override
	public String getAuditReportFileName(String pFileNamePrefix, String pFileType) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
		String date = format.format(new java.util.Date());
		String fileNamePrefix = pFileNamePrefix;
		if (StringUtils.isBlank(fileNamePrefix)) {
			fileNamePrefix = AUDIT_REPORT;
		}
		return getPathInfo().getAuditReportDir() + "/" + fileNamePrefix + date + pFileType;
	}

	/**
	 * return acceptable import file list
	 *
	 * @return acceptable import file list
	 */
	@Override
	public ArrayList<FileInfo> getFileList() {
		return getFileList(getPathInfo().getInputDir(), getFileAcceptPattern());
	}

	/**
	 * return acceptable import full-file list
	 *
	 * @return acceptable import full-file list
	 */
	@Override
	public File[] getFullFileList() {
		if (!StringUtils.isBlank(getPathInfo().getFullDir()) && !StringUtils.isBlank(getPathInfo().getPostfixFull())) {
			File[] filesFromFullDir = getFiles(new File(getPathInfo().getFullDir()), getFullFileAcceptPattern());
			if (filesFromFullDir != null && filesFromFullDir.length > 0) {
				Arrays.sort(filesFromFullDir, getFileComparator());
				return filesFromFullDir;
			}
		}
		return null;
	}

	/**
	 * return acceptable import file list
	 *
	 * @return acceptable import file list
	 */
	public File[] getFilesList() {
		if (!StringUtils.isBlank(getPathInfo().getInputDir())) {
			File[] filesFromFullDir = getFiles(new File(getPathInfo().getInputDir()), getFileAcceptPattern());
			if (filesFromFullDir != null && filesFromFullDir.length > 0) {
				Arrays.sort(filesFromFullDir, getFileComparator());
				return filesFromFullDir;
			}
		}
		return null;
	}

	/**
	 * return latest full-file from archive dir
	 *
	 * @return latest full-file from archive dir
	 */
	@Override
	public File getFullFileFromArchive() {
		File[] filesFromArchive = getFiles(new File(getPathInfo().getFullArchiveDir()), getFullFileAcceptPattern());
		if (filesFromArchive != null && filesFromArchive.length > 0) {
			Arrays.sort(filesFromArchive, getFileComparator());
			return filesFromArchive[filesFromArchive.length - 1];
		}
		return null;
	}

	/**
	 * return acceptable import file list
	 *
	 * @param pDirectory         dir to look up files for import
	 * @param pFileAcceptPattern file accept pattern
	 * @return acceptable import file list
	 */
	public ArrayList<FileInfo> getFileList(String pDirectory, Pattern pFileAcceptPattern) {
		ArrayList<FileInfo> fileList = new ArrayList<FileInfo>();
		if (!StringUtils.isBlank(pDirectory)) {
			File[] files = getFiles(new File(pDirectory), pFileAcceptPattern);
			if (files != null) {
				if (files.length > 0 && getLoggerTools().isLoggingDebug()) {
					getLoggerTools().logDebug("");
					getLoggerTools().logDebug("List of import file(s):");
				}
				for (File file : files) {
					if (file.isFile()) {
						FileInfo fileInfo = getFileInfo(file);
						if (getLoggerTools().isLoggingDebug()) {
							getLoggerTools().logDebug(fileInfo.toString());
						}
						fileList.add(fileInfo);
					}
				}
				if (files.length > 0 && getLoggerTools().isLoggingDebug()) {
					getLoggerTools().logDebug("");
				}
			}
		}
		return fileList;
	}

	/**
	 * check if lock file exist in specified dir
	 *
	 * @param pDir dir with imort file
	 * @return lock file exist
	 */
	@Override
	public boolean checkLockFileExist(String pDir) {
		if (StringUtils.isBlank(getPathInfo().getLockFile())) {
			if (getLoggerTools().isLoggingDebug()) {
				getLoggerTools().logDebug("Lock file is not specified. Perform load.");
			}
			return true;
		}
/*
		if (getLoggerTools().isLoggingDebug()) {
			getLoggerTools().logDebug( "Check if "+pDir+getPathInfo().getLockFile()+" exist." );
		}
*/
		boolean lockFileExist = new File(pDir + getPathInfo().getLockFile()).exists();
		if (getLoggerTools().isLoggingDebug()) {
			getLoggerTools().logDebug("Lock file " + pDir + getPathInfo().getLockFile() + ": " +
					(lockFileExist ? "FOUND" : "NOT FOUND"));
		}
		return lockFileExist;
	}

	/**
	 * delete lock file exist in specified dir
	 *
	 * @param pDir dir with imort file
	 */
	@Override
	public void deleteLockFile(String pDir) {
		if (isArchiveFile()) { // if we do not remove import file then we do not remove a lock file
			if (StringUtils.isBlank(getPathInfo().getLockFile())) {
				return;
			}
			if (!StringUtils.isBlank(pDir) && !otherFilesExists(pDir)) {
				String lockFile = pDir + getPathInfo().getLockFile();
				boolean isFileDeleted = false;
				try {
					isFileDeleted = new File(lockFile).delete();
				} catch (SecurityException e) {
					if (getLoggerTools().isLoggingError()) {
						getLoggerTools().logError(e.toString());
					}
				}
				// extra logging on delete file action
				if (isFileDeleted) {
					if (getLoggerTools().isLoggingDebug()) {
						getLoggerTools().logDebug("Lock file deleted");
					}
				} else {
					if (getLoggerTools().isLoggingError()) {
						getLoggerTools().logError("Lock file does not exist");
					}
				}
			}
		} else {
			getLoggerTools().logDebug("Lock file is not deleted according to settings isArchiveFile=false");
		}

	}


	/**
	 * Other files exists.
	 *
	 * @param pDir the dir
	 * @return true, if successful
	 */
	private boolean otherFilesExists(String pDir) {
		boolean result = false;
		if (!StringUtils.isBlank(pDir)) {
			File directory = new File(pDir);
			File[] listFiles = directory.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(getPathInfo().getFileExt());
				}
			});
			if (listFiles != null && listFiles.length > 0) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * return array of files from dir
	 *
	 * @param pDirectory         dir with import files
	 * @param pFileAcceptPattern file accept pattern
	 * @return array of files from dir
	 */
	private File[] getFiles(File pDirectory, final Pattern pFileAcceptPattern) {
		File[] files;
		if (!StringUtils.isBlank(getPathInfo().getFileExt())) {
			files = pDirectory.listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File pDir, String pFileName) {
					return pFileAcceptPattern.matcher(pFileName.toLowerCase()).matches();
					// return (pFileName.endsWith(getPathInfo().getFileExt()));
				}
			});
		} else {
			files = pDirectory.listFiles();
		}
		if (files != null) {
			Arrays.sort(files, getFileComparator());
		}
		return files;
	}

	/**
	 * return file comparator to sort them for import
	 *
	 * @return file comparator
	 */
	private Comparator<File> getFileComparator() {
		if (mFileComparator == null) {
			mFileComparator = new Comparator<File>() {
				@Override
				public int compare(File s1, File s2) {
					return compareFileTimestamps(
							getTimestampPart(s1.getName()),
							getTimestampPart(s2.getName()));
				}
			};
		}
		return mFileComparator;
	}

	/**
	 * return timestamp part of file name
	 *
	 * @param pFileName file name
	 * @return timestamp part of file name
	 */
	private String getTimestampPart(String pFileName) {

		int index1 = 0;
		int index2 = 0;
		if (StringUtils.isBlank(getPathInfo().getPostfixDelete()) && StringUtils.isBlank(getPathInfo().getPostfixUpdate())
				&& StringUtils.isBlank(getPathInfo().getPostfixInsert())) {
			index2 = pFileName.lastIndexOf(getPathInfo().getFileExt());
			index1 = pFileName.substring(0, index2).lastIndexOf(getPathInfo().getImportFileNameDivider());
		} else {
			//		int index2 = pFileName.lastIndexOf("-");
			index2 = pFileName.lastIndexOf(getPathInfo().getImportFileNameDivider());
			//		int index1 = pFileName.substring(0, index2).lastIndexOf("-");
			index1 = pFileName.substring(0, index2).lastIndexOf(getPathInfo().getImportFileNameDivider());

		}

		return pFileName.substring(index1 + 1, index2);
	}

	/**
	 * return compare result
	 *
	 * @param pTimestamp1 timestamp of file 1
	 * @param pTimestamp2 timestamp of file 2
	 * @return compare result
	 */
	private int compareFileTimestamps(String pTimestamp1, String pTimestamp2) {
		if (StringUtils.isBlank(pTimestamp1) || StringUtils.isBlank(pTimestamp2)) {
			return -1;
		}
        	if (getPathInfo().isSortByTime()) {
        	    DateFormat dateFormat = new SimpleDateFormat("hhmmMMddyyyy");
        	    try {
        		Date date1 = dateFormat.parse(pTimestamp1);
        		Date date2 = dateFormat.parse(pTimestamp2);
        		if (date1 == date2) {
        		    return 0;
        		} else {
        		    if (date1.after(date2)) {
        			return 1;
        		    } else {
        			return -1;
        		    }
        		}
        	    } catch (ParseException e) {
        		e.printStackTrace();
        	    }
        	}
		double num1 = Double.parseDouble(pTimestamp1);
		double num2 = Double.parseDouble(pTimestamp2);
		if (num1 == num2) {
			return 0;
		} else {
			if (num1 > num2) {
				return 1;
			} else {
				return -1;
			}
		}
	}
}
