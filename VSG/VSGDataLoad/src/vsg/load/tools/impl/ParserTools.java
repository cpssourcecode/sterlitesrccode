package vsg.load.tools.impl;

import atg.core.util.StringUtils;
import vsg.load.info.FieldsInfo;
import vsg.load.loader.Record;
import vsg.load.tools.IParserTools;
import vsg.load.wrapper.IRecordValueWrapper;
import vsg.load.wrapper.IValueWrapper;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class ParserTools implements IParserTools {

	/**
	 * possible null input value
	 */
	private static final String NULL = "NULL";

	/**
	 * return new record
	 * @param pFieldsInfo fields info
	 * @param pRow import row
	 * @return new record
	 */
	@Override
	public Record getRecord(FieldsInfo pFieldsInfo, String[] pRow) {
		return getRecord(pFieldsInfo, pRow, -1);
	}

	/**
	 * return new record
	 * @param pFieldsInfo fields info
	 * @param pRow import row
	 * @param pIndex import index
	 * @return new record
	 */
	@Override
	public Record getRecord(FieldsInfo pFieldsInfo, String[] pRow, int pIndex) {
		String recordId = getId(pFieldsInfo, pRow);
		Record record = createRecord(recordId, pIndex);
		return formRecord(record, pFieldsInfo, pRow);
	}

	/**
	 * return record object
	 * @param pRecordId record id
	 * @param pIndex import index
	 * @return record object
	 */
	private Record createRecord(String pRecordId, int pIndex) {
		Record record = new Record(pRecordId);
		record.setIndex( pIndex );
		record.setValues( new LinkedHashMap<String, Object>() );
		record.setValuesInitial(new LinkedHashMap<String, String>());
		return record;
	}

	/**
	 * init record
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 * @param pRow import row
	 * @return initialized record
	 */
	private Record formRecord(Record pRecord, FieldsInfo pFieldsInfo, String[] pRow) {
		if(pFieldsInfo.getSubRecordNames()!=null) {
			for(String subRecordName : pFieldsInfo.getSubRecordNames()) {
				initSubRecord(pRecord, pFieldsInfo, subRecordName);
			}
		}
		for(int i=0;i<pFieldsInfo.getFields().size();i++) {
			String fieldName = pFieldsInfo.getFields().get(i);
			if(isSkipField(pFieldsInfo, fieldName) ||
				isFieldToField(pFieldsInfo, pRecord.getName(), fieldName) ||
				isFieldToValue(pFieldsInfo, pRecord.getName(), fieldName))
			{
				continue;
			}

			String recordName = checkSubRecordField(pFieldsInfo, fieldName);
			if( recordName != null ) {
				Record subRecord = getSubRecord(pRecord, pFieldsInfo, recordName);
				if (subRecord != null) {
					String subRecordFieldName = getFieldName(fieldName, subRecord.getName());
					if (!StringUtils.isBlank(subRecord.getName())) {
						subRecord.putInitialFieldValue(subRecordFieldName, getRowCell(pRow, i));
					} else {
						if (isFieldToField(pFieldsInfo, subRecord.getName(), fieldName)) {
							initRecordFieldsToField(subRecord, pFieldsInfo);
						}
					}
				}
			} else {
				pRecord.putInitialFieldValue( fieldName, getRowCell(pRow, i) );
			}
		}
		return pRecord;
	}

	/**
	 * init record with processed values
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 * @return initialized record
	 */
	@Override
	public Record initRecord(Record pRecord, FieldsInfo pFieldsInfo) {
		for(String fieldName : pRecord.getValuesInitial().keySet()) {
			proceedRecordFieldInit(pRecord, pFieldsInfo, fieldName);
		}
		initRecordFieldsToField(pRecord, pFieldsInfo);
		String fieldIdName = pFieldsInfo.getFieldId(pRecord.getName());
		Map<String, String> fieldsValues = pFieldsInfo.getFieldsValue(pRecord.getName());
		if(fieldsValues!=null) {
			for(String fieldName : fieldsValues.keySet()) {
				String recordFieldName = getFieldName(fieldName, pRecord.getName());
				Object recordFieldValue = getFieldValueCheckedForWrapper(
						pRecord,
						pFieldsInfo,
						fieldName,
						fieldsValues.get(fieldName));
				pRecord.putFieldValue( recordFieldName, recordFieldValue);
				if( fieldIdName.equals(recordFieldName) ) {
					pRecord.setId((String) recordFieldValue);
				}
			}
		}
		initRecordSubRecord(pRecord, pFieldsInfo);
		return pRecord;
	}

	/**
	 * init record field to field values
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 */
	private void initRecordFieldsToField(Record pRecord, FieldsInfo pFieldsInfo) {
		Map<String, String> fieldToFields = pFieldsInfo.getFieldsToField(pRecord.getName());
		String fieldIdName = pFieldsInfo.getFieldId(pRecord.getName());
		if(fieldToFields!=null) {
			for(String fieldName : fieldToFields.keySet()) {
				if(isFieldOnProcess(pRecord, pFieldsInfo, fieldName)) {
					String otherFieldName = fieldToFields.get(fieldName);
					String recordFieldName = getFieldName(fieldName, pRecord.getName());
					Object recordFieldValue = null;
					if(pFieldsInfo.isSubRecordField(otherFieldName)) {
						String otherRecordName = otherFieldName.substring( 0, otherFieldName.indexOf(".") );
						Record otherRecord = pFieldsInfo.getTopRecord( pRecord ).getSubRecord(otherRecordName);
						otherFieldName = getFieldName(otherFieldName, otherRecordName);
						recordFieldValue = getFieldToFieldValue(otherRecord, pFieldsInfo, fieldName, otherFieldName);
					} else {
						recordFieldValue = getFieldToFieldValue(pRecord, pFieldsInfo, fieldName, otherFieldName);
					}
					pRecord.putFieldValue( recordFieldName, recordFieldValue );
					if( fieldIdName.equals(recordFieldName) ) {
						pRecord.setId((String) recordFieldValue);
					}
				}
			}
		}
	}

	/**
	 * retrieve field to field value
	 * @param pRecord record
	 * @param pFieldsInfo fields info
	 * @param pFieldName field name
	 * @param pOtherFieldName other field name
	 * @return other field value
	 */
	private Object getFieldToFieldValue(Record pRecord, FieldsInfo pFieldsInfo,
										String pFieldName, String pOtherFieldName ) {
		Object fieldValue = null;
		Map values = pRecord.getValuesInitial();
		if(pFieldsInfo.getFieldsToFieldWrapped()!=null && pFieldsInfo.getFieldsToFieldWrapped().contains(pFieldName)) {
			values = pRecord.getValues();
		}
		if(values.containsKey(pOtherFieldName)) {
			fieldValue = getFieldValueCheckedForWrapper(pRecord, pFieldsInfo, pFieldName, values.get(pOtherFieldName));
		} else {
			if(pRecord.getParentRecord()!=null) {
				fieldValue = getFieldToFieldValue(pRecord.getParentRecord(), pFieldsInfo, pFieldName, pOtherFieldName);
			}
		}
		return fieldValue;
	}


	/**
	 * return field should be proceeded
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 * @param pFieldName field name
	 * @return field should be proceeded
	 */
	private boolean isFieldOnProcess(Record pRecord, FieldsInfo pFieldsInfo, String pFieldName) {
		if(StringUtils.isBlank(pRecord.getName())){
			if( pFieldsInfo.isSubRecordField(pFieldName) ) {
				return false;
			}
		} else {
			if(!pFieldsInfo.isSubRecordField(pFieldName, pRecord.getName())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * init record sub records
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 */
	private void initRecordSubRecord(Record pRecord, FieldsInfo pFieldsInfo) {
		if(pRecord.getSubRecords()!=null) {
			for(String recordName : pRecord.getSubRecords().keySet()) {
				Record subRecord = getSubRecord(pRecord, pFieldsInfo, recordName);
				initRecord(subRecord, pFieldsInfo);
			}
		}
	}

	/**
	 * initialize proceeded field value
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 * @param pFieldName field name
	 */
	private void proceedRecordFieldInit(Record pRecord, FieldsInfo pFieldsInfo, String pFieldName) {
		String searchFieldName = pFieldName;
		if(!StringUtils.isBlank(pRecord.getName())) {
			searchFieldName = pRecord.getName()+"."+pFieldName;
		}

		String fieldValue = pRecord.getValueInitial( pFieldName );
		Object fieldValueWrapped = getFieldValueCheckedForWrapper(
				pRecord,
				pFieldsInfo,
				searchFieldName,
				fieldValue);
		pRecord.putFieldValue( pFieldName, fieldValueWrapped );
		String fieldIdName = pFieldsInfo.getFieldId(pRecord.getName());
		if( fieldIdName.equals(pFieldName) ) {
			pRecord.setId((String) fieldValueWrapped);
		}
	}

	/**
	 * get sub record
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 * @return sub record
	 */
	private Record getSubRecord(Record pRecord, FieldsInfo pFieldsInfo, String pRecordName) {
		Record subRecord = pRecord.getSubRecords().get( pRecordName );
		if(subRecord == null) {
			initSubRecord(pRecord, pFieldsInfo, pRecordName);
		}
		return subRecord;
	}

	/**
	 * init sub record
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 * @return sub record
	 */
	private Record initSubRecord(Record pRecord, FieldsInfo pFieldsInfo, String pRecordName) {
		Record subRecord;
		if(pFieldsInfo.getFields().contains(pFieldsInfo.getFieldId())) {
			subRecord = createRecord(pRecord.getId(), pRecord.getIndex());
		} else {
			subRecord = createRecord(getId(pFieldsInfo, null), pRecord.getIndex());
		}
		subRecord.setName(pRecordName);
		subRecord.setParentRecord(pRecord);
		pRecord.getSubRecords().put( pRecordName, subRecord );
		return subRecord;
	}

	/**
	 * return field name according to record name
	 * @param pFieldName field name
	 * @param pRecordName record name
	 * @return field name according to record name
	 */
	private String getFieldName(String pFieldName, String pRecordName) {
		if(!StringUtils.isBlank(pRecordName)) {
			return pFieldName.replace(pRecordName+".", "");
		} else {
			return pFieldName;
		}
	}

	/**
	 * check if field should be skipped
	 * @param pFieldsInfo fields info
	 * @param pFieldName field name
	 * @return skip field
	 */
	private boolean isSkipField(FieldsInfo pFieldsInfo, String pFieldName) {
		return pFieldsInfo.getFieldsSkip() != null && pFieldsInfo.getFieldsSkip().contains(pFieldName);
	}

	/**
	 * check if field is "field to field"
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 * @param pFieldName field name
	 * @return field is "field to field"
	 */
	private boolean isFieldToField(FieldsInfo pFieldsInfo, String pRecordName, String pFieldName) {
		return pFieldsInfo.getFieldsToField(pRecordName) != null &&
				pFieldsInfo.getFieldsToField(pRecordName).containsKey(pFieldName);
	}

	/**
	 * check if field is "field to value"
	 * @param pFieldsInfo fields info
	 * @param pRecordName record name
	 * @param pFieldName field name
	 * @return field is "field to value"
	 */
	private boolean isFieldToValue(FieldsInfo pFieldsInfo, String pRecordName, String pFieldName) {
		return pFieldsInfo.getFieldsValue(pRecordName) != null &&
				pFieldsInfo.getFieldsValue(pRecordName).containsKey(pFieldName);
	}

	/**
	 * return sub record owner
	 * @param pFieldsInfo fields info
	 * @param pFieldName field name
	 * @return sub record owner
	 */
	private String checkSubRecordField(FieldsInfo pFieldsInfo, String pFieldName) {
		if  (pFieldsInfo.getSubRecordNames() == null) {
			return null;
		}
		for(String subRecordName : pFieldsInfo.getSubRecordNames()) {
			if(pFieldName.startsWith( subRecordName +"." )) {
				return subRecordName;
			}
		}
		return null;
	}

	/**
	 * return id value
	 * @param pFieldsInfo fields info
	 * @param pRow import row
	 * @return id value
	 */
	private String getId(FieldsInfo pFieldsInfo, String[] pRow) {
		int idIndex = pFieldsInfo.getFieldIdIndex();
		String idValue = null;
		if(idIndex!=-1) {
			idValue = getRowCell(pRow, idIndex);
		} else {
			Map<String, String> fieldValues = pFieldsInfo.getFieldsValue(null);
			if(fieldValues!=null) {
				idValue = fieldValues.get( pFieldsInfo.getFieldId() );
			}
		}
		IValueWrapper fieldWrapper = getFieldWrapper(pFieldsInfo, pFieldsInfo.getFieldId());
		if(fieldWrapper == null) {
			return idValue;
		} else {
			return (String) fieldWrapper.getValue(idValue);
		}
	}

	/**
	 * return processed field value (wrapped if wrapper is declared for this field)
	 *
	 * @param pRecord
	 * @param pFieldsInfo fields info
	 * @param pFieldName field name
	 * @param pFieldValue field value
	 * @return processed field value
	 */
	private Object getFieldValueCheckedForWrapper(Record pRecord, FieldsInfo pFieldsInfo, String pFieldName, Object pFieldValue) {
		Map<String, IValueWrapper> fieldsValueWrappers = pFieldsInfo.getFieldsValueWrappers();
		if(fieldsValueWrappers!=null && fieldsValueWrappers.containsKey( pFieldName)) {
			IValueWrapper valueWrapper = fieldsValueWrappers.get(pFieldName);
			if(valueWrapper instanceof IRecordValueWrapper) {
				return ((IRecordValueWrapper)valueWrapper).getValue(pRecord);
			} else {
				return valueWrapper.getValue(pFieldValue);
			}
		} else {
			return pFieldValue;
		}
	}

	/**
	 * return field wrapper
	 * @param pFieldsInfo fields info
	 * @param pFieldName field name
	 * @return field wrapper
	 */
	private IValueWrapper getFieldWrapper(FieldsInfo pFieldsInfo, String pFieldName) {
		if(pFieldsInfo.getFieldsValueWrappers() == null) {
			return null;
		}
		return pFieldsInfo.getFieldsValueWrappers().get( pFieldName );
	}

	/**
	 * return row cell value
	 * @param pRow import row
	 * @param pIndex cell index
	 * @return row cell value
	 */
	private String getRowCell(String[] pRow, int pIndex) {
		if(NULL.equals(pRow[pIndex])) {
			return null;
		}
		return pRow[pIndex];
	}

}
