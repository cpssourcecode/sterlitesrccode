package vsg.load.tools.impl;

import vsg.load.tools.ICacheInvalidateTools;
import vsg.load.tools.ILoggerTools;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class CacheInvalidateTools implements ICacheInvalidateTools {
	/**
	 * list of servers, which repositories has to be invalidate
	 */
	private List<String> mServerNames;
	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;

	//------------------------------------------------------------------------------------------------------------------

	public List<String> getServerNames() {
		return mServerNames;
	}

	public void setServerNames(List<String> pServerNames) {
		mServerNames = pServerNames;
	}

	public ILoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	public void setLoggerTools(ILoggerTools pLoggerTools) {
		mLoggerTools = pLoggerTools;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * invalidate caches for specified repository
	 * @param pRepositoryName repository name
	 */
	@Override
	public void invalidateOtherServersCache(String pRepositoryName) {
/*
		if(getServerNames() == null || getServerNames().size() == 0) {
			if(getLoggerTools().isLoggingError()) {
				getLoggerTools().logError("CacheInvalidateTools: "+
						"Trying to invalidate caches for other servers without specifying them.");
			}
			return;
		}

		String invalidateCacheClass = "atg.adapter.gsa.invalidator.GSAInvalidatorClient";
		String invalidateCacheComponent = "/atg/dynamo/service/GSAInvalidatorService";
		String runClassString = "java -Datg.adapter.gsa.invalidator.uri=";
		String args = new StringBuffer(invalidateCacheComponent).append(" ")
				.append(invalidateCacheClass).append(" ")
				.append(pRepositoryName).toString();
		for (String server : getServerNames()) {
			String rmiUrl = new StringBuffer("rmi://").append(server.trim()).toString();
			String finalStringToExec = new StringBuffer(runClassString).append(rmiUrl).append(args).toString();

			if (getLoggerTools().isLoggingDebug()) {
				getLoggerTools().logDebug("Process exec string :" + finalStringToExec);
			}
			try {
				Runtime.getRuntime().exec(finalStringToExec);
			} catch (Exception e) {
				if(getLoggerTools().isLoggingError()) {
					getLoggerTools().logError(e.toString());
				}
			}
		}
*/
	}


	/**
	 * inventory servlet
	 */
	private String mInventoryServlet;
	/**
	 * price servlet
	 */
	private String mPriceServlet;


	public String getInventoryServlet() {
		return mInventoryServlet;
	}

	public void setInventoryServlet(String pInventoryServlet) {
		mInventoryServlet = pInventoryServlet;
	}

	public String getPriceServlet() {
		return mPriceServlet;
	}

	public void setPriceServlet(String pPriceServlet) {
		mPriceServlet = pPriceServlet;
	}

	/**
	 * invalidate inventory servlets
	 */
	public void invalidateInventory() {
		invalidateRepository(getServerNames(), getInventoryServlet());
	}

	/**
	 * invalidate price servlets
	 */
	public void invalidatePrice() {
		invalidateRepository(getServerNames(), getPriceServlet());
	}

	/**
	 * invalidate by servlets calling
	 * @param pServers servers
	 * @param pServlet servlet
	 */
	private void invalidateRepository(List<String> pServers, String pServlet) {
		for( String pageServerUrl : pServers ) {
			try {
				URL url = new URL("http://"+pageServerUrl+"/"+pServlet);
				if (getLoggerTools().isLoggingDebug()) {
					getLoggerTools().logDebug("invalidating on: " + pageServerUrl);
	            }
				HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
				int resp = urlConnection.getResponseCode();
				if (getLoggerTools().isLoggingDebug()) {
					getLoggerTools().logDebug("invalidate response: " + resp);
	            }
			} catch (IOException e) {
				getLoggerTools().logError(e.toString());
			}
		}
	}
}
