package vsg.load.tools.impl;

import atg.nucleus.GenericService;
import vsg.load.tools.ILoggerTools;

/**
 * @author Dmitry Golubev
 */
public class LoggerTools extends GenericService implements ILoggerTools {

	/**
	 * log info message with params
	 * @param pMsg message
	 * @param pParams params
	 */
	@Override
	public void logInfo(String pMsg, Object[] pParams) {
		super.vlogInfo(pMsg, pParams);
	}

	/**
	 * log debug message with params
	 * @param pMsg message
	 * @param pParams params
	 */
	@Override
	public void logDebug(String pMsg, Object[] pParams) {
		super.vlogDebug(pMsg, pParams);
	}

	/**
	 * log error exception
	 * @param pException exception
	 */
	@Override
	public void logError(Exception pException) {
		super.logError(pException);
	}

	/**
	 * log error message with params
	 * @param pMsg message
	 * @param pParams params
	 */
	@Override
	public void logError(String pMsg, Object[] pParams) {
		super.vlogError(pMsg, pParams);
	}

}
