package vsg.load.tools.impl;

import vsg.load.file.IReader;
import vsg.load.file.csv.CSVReader;
import vsg.load.file.csv.CSVWriter;
import vsg.load.info.PathInfo;
import vsg.load.loader.CommonSettings;
import vsg.load.loader.impl.Loader;
import vsg.load.tools.IImportTools;
import vsg.load.tools.IReportingTools;

import java.io.*;
import java.net.UnknownHostException;
import java.util.*;

/**
 * @author Dmitry Golubev
 */
public class LoaderFullFileAnalyzeTools {
	/**
	 * relation string delimeter
	 */
	private static final String DELIMETER = ",";
	/**
	 * list of new rows to import
	 */
	private List<String[]> mDeltaCreate = new LinkedList<String[]>();
	/**
	 * list of update rows to import
	 */
	private List<String[]> mDeltaUpdate = new LinkedList<String[]>();
	/**
	 * list of delete rows to import
	 */
	private List<String[]> mDeltaDelete = new LinkedList<String[]>();
	/**
	 * loader
	 */
	private Loader mLoader;
	/**
	 * analyze statisctics
	 */
	private AnalyzeStatistics mStatistics;

	/**
	 * loader common settings
	 */
	private CommonSettings mCommonSettings;

	//------------------------------------------------------------------------------------------------------------------

	public Loader getLoader() {
		return mLoader;
	}

	public void setLoader(Loader pLoader) {
		mLoader = pLoader;
	}

	public IImportTools getImportTools() {
		return mLoader.getImportTools();
	}

	public IReportingTools getReportingTools() {
		return mLoader.getReportingTools();
	}

	private List<String[]> getDeltaCreate() {
		return mDeltaCreate;
	}

	private List<String[]> getDeltaUpdate() {
		return mDeltaUpdate;
	}

	private List<String[]> getDeltaDelete() {
		return mDeltaDelete;
	}

	public CommonSettings getCommonSettings() {
		return mCommonSettings;
	}

	public void setCommonSettings(CommonSettings pCommonSettings) {
		mCommonSettings = pCommonSettings;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * analyze statistics
	 */
	private class AnalyzeStatistics {
		/**
		 * full-file
		 */
		File mFullFileName;
		/**
		 * is full-file just copied to input dir
		 */
		boolean mCopied = false;
		/**
		 * is full-file placed to archive dir
		 */
		boolean mArchived = false;
		/**
		 * is full-file placed to error dir
		 */
		boolean mErrored = false;
		/**
		 * is full-file successfully parsed
		 */
		boolean mParsed = false;
		/**
		 * is delta insert-file created in input dir
		 */
		boolean mWriteSuccessCreate = true;
		/**
		 * is delta update-file created in input dir
		 */
		boolean mWriteSuccessUpdate = true;
		/**
		 * is delta delete-file created in input dir
		 */
		boolean mWriteSuccessDelete = true;
		/**
		 * delta insert-file
		 */
		File mFileDeltaInsert = null;
		/**
		 * delta update-file
		 */
		File mFileDeltaUpdate = null;
		/**
		 * delta delete-file
		 */
		File mFileDeltaDelete = null;
		/**
		 * delta insert records number
		 */
		int mDeltaCountInsert = 0;
		/**
		 * delta update records number
		 */
		int mDeltaCountUpdate = 0;
		/**
		 * delta delete records number
		 */
		int mDeltaCountDelete = 0;
		/**
		 * list of exceptions during analyze
		 */
		List<String> mExceptions = new LinkedList<String>();
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * perform check for full-file and generate delta-files if full is found
	 *
	 * @param pLoader loader
	 */
	public void performCheck(Loader pLoader) {
		setLoader(pLoader);
		File[] fullFiles = getImportTools().getFullFileList();
		if (fullFiles != null) {
			if (checkLockFile()) {
				for (File fullFile : fullFiles) {
					reset();
					mStatistics = new AnalyzeStatistics();
					mStatistics.mFullFileName = fullFile;
					File fullFileFromArchive = getImportTools().getFullFileFromArchive();
					if (fullFileFromArchive == null) {
						mStatistics.mCopied = copyFullFileToInput(fullFile);
					} else {
						analyzeFiles(fullFileFromArchive, fullFile);
					}
					if (mStatistics.mExceptions.size() > 0) {
						mStatistics.mErrored = getImportTools().errorFile(fullFile);
					} else {
						mStatistics.mArchived = getImportTools().moveFile(fullFile,
								getImportTools().getPathInfo().getFullArchiveDir());
					}
					sendReport(fullFile);
					reset();
					if (mStatistics.mFileDeltaDelete != null || mStatistics.mFileDeltaInsert != null ||
							mStatistics.mFileDeltaUpdate != null) {
						copyLockFileToInput();
					}

					releaseLockFile();

				}
			}
		}
	}

	/**
	 * check if lock file exist
	 *
	 * @return lock file exist
	 */
	private boolean checkLockFile() {
		return getImportTools().checkLockFileExist(getImportTools().getPathInfo().getFullDir());
	}

	/**
	 * move lock file
	 */
	private void copyLockFileToInput() {
		getImportTools().copyFile(
				new File(getImportTools().getPathInfo().getFullDir() + getImportTools().getPathInfo().getLockFile()),
				new File(getImportTools().getPathInfo().getInputDir() + getImportTools().getPathInfo().getLockFile()));
	}

	/**
	 * release lock file
	 */
	private void releaseLockFile() {
		getImportTools().deleteLockFile(getImportTools().getPathInfo().getFullDir());
	}

	/**
	 * copy full-file to input dir and make it prefix CREATE
	 *
	 * @param pFullFile full-file to copy
	 * @return is filed copied
	 */
	private boolean copyFullFileToInput(File pFullFile) {
		PathInfo pathInfo = getImportTools().getPathInfo();
		String copyFileName = pFullFile.getName().replace(
				pathInfo.getPostfixFull(),
				pathInfo.getPostfixInsert());
		boolean result = getImportTools().copyFile(pFullFile, new File(pathInfo.getInputDir() + copyFileName));
		getImportTools().getLoggerTools().logDebug(
				"Full file " + pFullFile.getName() + " is copied to input as " + copyFileName
		);
		return result;
	}

	/**
	 * compare and analyze two files
	 *
	 * @param pOldFile old full-file
	 * @param pNewFile new full-file
	 */
	private void analyzeFiles(File pOldFile, File pNewFile) {
		getImportTools().getLoggerTools().logDebug(
				"full files analyze: " + pOldFile.getName() + " - " + pNewFile.getName());
		HashMap<String, String[]> oldData = retrieveDataFromFile(pOldFile);
		if (oldData != null) {
			analyzeData(oldData, pNewFile, getImportTools().getPathInfo().isBeanFile());
			writeDeltaFiles(pNewFile);
		}
		getImportTools().getLoggerTools().logDebug("full files analyze is complete");
	}

	/**
	 * forms list of delta data
	 *
	 * @param pOldData    old data
	 * @param pNewFile    new file
	 * @param pIsBeanFile is file contains bean info, not relations
	 */
	private void analyzeData(HashMap<String, String[]> pOldData, File pNewFile, boolean pIsBeanFile) {
		try {
			try (FileInputStream inputStream = new FileInputStream(pNewFile)) {

				CSVReader readerNewFile = (CSVReader) getImportTools().getReader(pNewFile, inputStream);
				try {
					if (readerNewFile != null) {
						readerNewFile.setSkipLinesNumber(getLoader().getLineToSkip());
						String[] row;
						while ((row = readerNewFile.readNext()) != null) {
							analyzeRow(row, pOldData, pIsBeanFile);
						}
						mDeltaDelete.addAll(pOldData.values());
					}
					mStatistics.mParsed = true;
				} catch (Exception e) {
					mStatistics.mParsed = false;
					exceptionLog("Error in files compare: ", e);
				} finally {
					if (readerNewFile != null) {
						readerNewFile.close();
					}
				}
				pOldData.clear();
			}
		} catch (IOException ie) {
			if (getImportTools().getLoggerTools().isLoggingError()) {
				getImportTools().getLoggerTools().logError(ie);
			}
		}
	}

	/**
	 * forms list of delta data
	 *
	 * @param row         row to analyze
	 * @param pOldData    old data
	 * @param pIsBeanFile is file contains bean info, not relations
	 */
	private void analyzeRow(String[] row, HashMap<String, String[]> pOldData, boolean pIsBeanFile) {
		String newKey = row[0];
		String[] oldDataRow = pOldData.get(newKey);
		if (oldDataRow == null) {
			mDeltaCreate.add(row);
		} else {
			pOldData.remove(newKey);
			if (!areRowsEqual(oldDataRow, row)) {
				if (pIsBeanFile) {
					mDeltaUpdate.add(row);
				} else {
					String[] deltas = getRelationDelta(oldDataRow[0], row[0]);
					if (!"".equals(deltas[0])) {
						mDeltaUpdate.add(new String[]{newKey, deltas[0]});
					}
					if (!"".equals(deltas[1])) {
						mDeltaDelete.add(new String[]{newKey, deltas[1]});
					}
				}
			}
		}
	}

	/**
	 * write delta files
	 *
	 * @param pFullFile original new file
	 */
	private void writeDeltaFiles(File pFullFile) {
		PathInfo pathInfo = getImportTools().getPathInfo();
		List<String[]> skipBlock = getImportFileSkipLines(pFullFile);
		if (getDeltaCreate().size() > 0) {
			mStatistics.mFileDeltaInsert = getDeltaFile(pFullFile, pathInfo.getPostfixInsert());
			mStatistics.mDeltaCountInsert = getDeltaCreate().size();
			mStatistics.mWriteSuccessCreate = writeDeltaFile(mStatistics.mFileDeltaInsert, getDeltaCreate(), skipBlock);
		}
		if (getDeltaUpdate().size() > 0) {
			mStatistics.mFileDeltaUpdate = getDeltaFile(pFullFile, pathInfo.getPostfixUpdate());
			mStatistics.mDeltaCountUpdate = getDeltaUpdate().size();
			mStatistics.mWriteSuccessUpdate = writeDeltaFile(mStatistics.mFileDeltaUpdate, getDeltaUpdate(), skipBlock);
		}
		if (getDeltaDelete().size() > 0) {
			mStatistics.mFileDeltaDelete = getDeltaFile(pFullFile, pathInfo.getPostfixDelete());
			mStatistics.mDeltaCountDelete = getDeltaDelete().size();
			mStatistics.mWriteSuccessDelete = writeDeltaFile(mStatistics.mFileDeltaDelete, getDeltaDelete(), skipBlock);
		}
	}

	/**
	 * get skip block from files
	 *
	 * @param pFullFile initial file
	 * @return skip block from files
	 */
	private List<String[]> getImportFileSkipLines(File pFullFile) {
		try {
			try (FileInputStream inputStream = new FileInputStream(pFullFile)) {
				if (getLoader().getLineToSkip() > 0) {
					IReader reader = getReader(pFullFile, inputStream);
					try {
						if (reader != null) {
							reader.setSkipLinesNumber(0);
							List<String[]> rows = new LinkedList<String[]>();
							for (int i = 0; i < getLoader().getLineToSkip(); i++) {
								String[] row = reader.readNext();
								if (row == null) {
									break;
								}
								rows.add(row);
							}
							return rows;
						}
					} catch (Exception e) {
						exceptionLog("Unable to read file " + pFullFile.getName() + ": ", e);
					} finally {
						if (reader != null) {
							reader.close();
						}
					}
				}
			}
		} catch (IOException ie) {
			if (getImportTools().getLoggerTools().isLoggingError()) {
				getImportTools().getLoggerTools().logError(ie);
			}
		}
		return null;
	}

	/**
	 * write delta file
	 *
	 * @param pFile      new file
	 * @param rows       delta rows
	 * @param pSkipBlock skip rows from initial-file
	 * @return is filed generated
	 */
	private boolean writeDeltaFile(File pFile, List<String[]> rows, List<String[]> pSkipBlock) {
		try {
			CSVWriter writer = (CSVWriter) getImportTools().getWriter(pFile);
/*
			CSVWriter writer = new CSVWriter(getLoader().getLoggerTools());
			writer.setWriter(pFile, getCommonSettings().getEncoding());
*/
			if (pSkipBlock != null) {
				for (String[] skipRow : pSkipBlock) {
					writer.writeNext(skipRow);
				}
			}
			for (String[] row : rows) {
				writer.writeNext(row);
			}
			writer.close();
		} catch (Exception e) {
			exceptionLog("Unable to write delta file " + pFile.getName() + ": ", e);
			return false;
		}
		return true;
	}

	/**
	 * create delta file
	 *
	 * @param pFullFile initial full-file
	 * @param pPostfix  postfix to use in name
	 * @return create delta file
	 */
	private File getDeltaFile(File pFullFile, String pPostfix) {
		PathInfo pathInfo = getImportTools().getPathInfo();
		String fileName = pFullFile.getName().replace("-" + pathInfo.getPostfixFull(), "-" + pPostfix);
		return new File(pathInfo.getInputDir() + fileName);
	}

	/**
	 * retrieve data from file
	 *
	 * @param pFile data-file
	 * @return map with data from file
	 */
	private HashMap<String, String[]> retrieveDataFromFile(File pFile) {
		try {
			try (FileInputStream inputStream = new FileInputStream(pFile)) {
				IReader reader = getReader(pFile, inputStream);
				try {
					if (reader != null) {
						HashMap<String, String[]> dataFromFile = new HashMap<String, String[]>();
						String[] row;
						while ((row = reader.readNext()) != null) {
							dataFromFile.put(row[0], row);
						}
						return dataFromFile;
					}
				} catch (Exception e) {
					exceptionLog("Unable to read file: " + pFile.getName(), e);
				} finally {
					if (reader != null) {
						reader.close();
					}
				}
			}
		} catch (IOException ie) {
			if (getImportTools().getLoggerTools().isLoggingError()) {
				getImportTools().getLoggerTools().logError(ie);
			}
		}
		return null;
	}

	/**
	 * return initialized reader
	 *
	 * @param pFile file to read
	 * @return reader
	 */
	private CSVReader getReader(File pFile, FileInputStream pInputStream) {
		CSVReader reader = (CSVReader) getImportTools().getReader(pFile, pInputStream);
		if (reader != null) {
			reader.setSkipLinesNumber(getLoader().getLineToSkip());
		}

		return reader;
	}

	/**
	 * reset lists
	 */
	private void reset() {
		mDeltaCreate.clear();
		mDeltaUpdate.clear();
		mDeltaDelete.clear();
	}

	/**
	 * compare strings arrays
	 *
	 * @param pRowOld string array 1
	 * @param pRowNew string array 1
	 * @return strings arrays are equal
	 */
	private boolean areRowsEqual(String[] pRowOld, String[] pRowNew) {
		for (int i = 0; i < pRowOld.length; i++) {
			if (!areStringEqual(pRowOld[i], pRowNew[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * compare strings
	 *
	 * @param pStr1 string1
	 * @param pStr2 string2
	 * @return strings are equal
	 */
	private boolean areStringEqual(String pStr1, String pStr2) {
		if (pStr1 == null && pStr2 == null) {
			return true;
		} else if (pStr1 != null && pStr2 == null) {
			return false;
		} else if (pStr1 == null/* && pStr2 != null*/) {
			return false;
		} else {
			return pStr1.equals(pStr2);
		}
	}

	/**
	 * forms delta relations strings
	 *
	 * @param pRelationsOld string of old relations
	 * @param pRelationsNew string of new relations
	 * @return array of relations: 0 - new relations, 1 - relations to delete
	 */
	private String[] getRelationDelta(String pRelationsOld, String pRelationsNew) {
		List<String> listOfOldRelations = Arrays.asList(pRelationsOld.split(DELIMETER));
		List<String> listOfNewRelations = Arrays.asList(pRelationsNew.split(DELIMETER));

		List<String> relationsAdd = new ArrayList<String>();
		relationsAdd.addAll(listOfNewRelations);
		relationsAdd.removeAll(listOfOldRelations);


		List<String> relationsRemove = new ArrayList<String>();
		relationsRemove.addAll(listOfOldRelations);
		relationsRemove.removeAll(listOfNewRelations);

		return new String[]{
				getStringFromArray(relationsAdd),
				getStringFromArray(relationsRemove)
		};
	}

	/**
	 * form string from list
	 *
	 * @param pList list from strings
	 * @return formed string with delimeters
	 */
	private String getStringFromArray(List<String> pList) {
		StringBuilder builder = new StringBuilder();
		Iterator<String> iterator = pList.iterator();
		while (iterator.hasNext()) {
			builder.append(iterator.next());
			if (iterator.hasNext()) {
				builder.append(DELIMETER);
			}
		}
		return builder.toString();
	}

	/**
	 * send report about file analyze
	 *
	 * @param pFile full-file
	 */
	public void sendReport(File pFile) {
		getReportingTools().sendReportFullFileAnalyze(getLoader(), pFile.getName(), formReportMsg());
	}

	/**
	 * forms report message
	 *
	 * @return report message
	 */
	private String formReportMsg() {
		StringBuilder builder = new StringBuilder();
		String hostName = "";
		try {
			hostName = " on " + java.net.InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			if (getImportTools().getLoggerTools().isLoggingError()) {
				getImportTools().getLoggerTools().logError(e);
			}
		}
		builder.append("Full file ").append(hostName).append(":  ").
				append(mStatistics.mFullFileName.getName()).append("<br>");
		builder.append("<br>Placed in archive: ").append(mStatistics.mArchived).append("<br>");
		builder.append("Placed in error: ").append(mStatistics.mErrored).append("<br>");
		if (mStatistics.mCopied) {
			builder.append("Copied: ").append(mStatistics.mCopied).append("<br>");
		} else {
			formReportMsgParseInfo(builder);
		}
		formReportMsgExceptions(builder);
		return builder.toString();
	}

	/**
	 * forms report message part abount parseing
	 *
	 * @param pBuilder message builder
	 */
	private void formReportMsgParseInfo(StringBuilder pBuilder) {
		pBuilder.append("Parsed: ").append(mStatistics.mParsed).append("<br>");
		if (mStatistics.mFileDeltaInsert != null || mStatistics.mFileDeltaUpdate != null ||
				mStatistics.mFileDeltaDelete != null) {
			pBuilder.append("<br>Delta files info.<br>");
			if (mStatistics.mFileDeltaInsert != null) {
				pBuilder.append("<br>Insert file: ").append(mStatistics.mFileDeltaInsert.getName()).append("<br>");
				pBuilder.append("Number of records: ").append(mStatistics.mDeltaCountInsert).append("<br>");
				pBuilder.append("Insert file created: ").append(mStatistics.mWriteSuccessCreate).append("<br>");
			}
			if (mStatistics.mFileDeltaUpdate != null) {
				pBuilder.append("<br>Update file: ").append(mStatistics.mFileDeltaUpdate.getName()).append("<br>");
				pBuilder.append("Number of records: ").append(mStatistics.mDeltaCountUpdate).append("<br>");
				pBuilder.append("Update file created: ").append(mStatistics.mWriteSuccessUpdate).append("<br>");
			}
			if (mStatistics.mFileDeltaDelete != null) {
				pBuilder.append("<br>Delete file name: ").append(mStatistics.mFileDeltaDelete.getName()).append("<br>");
				pBuilder.append("Number of records: ").append(mStatistics.mDeltaCountDelete).append("<br>");
				pBuilder.append("Delete file is created: ").append(mStatistics.mWriteSuccessDelete).append("<br>");
			}
		} else {
			pBuilder.append("<br>No delta files generated.<br>");
		}
		pBuilder.append("<br>");
	}

	/**
	 * forms report message part abount errors
	 *
	 * @param pBuilder message builder
	 */
	private void formReportMsgExceptions(StringBuilder pBuilder) {
		if (mStatistics.mExceptions != null && mStatistics.mExceptions.size() > 0) {
			pBuilder.append("ERRORS:").append("<br>");
			for (String exception : mStatistics.mExceptions) {
				pBuilder.append("- ").append(exception).append("<br>");
			}
		}
	}

	/**
	 * log exception
	 *
	 * @param pMsg message
	 * @param e    exception
	 */
	private void exceptionLog(String pMsg, Exception e) {
		boolean inOurClass = false;
		String finalClassName = LoaderFullFileAnalyzeTools.class.getName();
		StringBuilder exceptionReason = new StringBuilder(e.toString()).append("\n");
		if (e.getStackTrace() != null) {
			for (StackTraceElement traceElement : e.getStackTrace()) {
				if (traceElement.getClassName().equals(finalClassName)) {
					inOurClass = true;
				} else {
					if (inOurClass) {
						break;
					}
				}
				exceptionReason.append(traceElement.toString()).append("\n");
			}
		}

		String msg = pMsg + "\n" + exceptionReason;
		mStatistics.mExceptions.add(msg);
		if (getImportTools().getLoggerTools().isLoggingError()) {
			getImportTools().getLoggerTools().logError(msg);
		}
	}
}
