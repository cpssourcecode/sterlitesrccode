package vsg.load.tools;

import vsg.load.loader.impl.Loader;

import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public interface IReportingTools {

	/**
	 * send loader result report
	 * @param pLoader loader
	 * @param pStartTime start time
	 * @param pEndTime end time
	 */
	public void sendReport(Loader pLoader, Date pStartTime, Date pEndTime);

	/**
	 * send loader result report
	 * @param pLoader loader
	 * @param pFileName full-file name
	 * @param pMsg email msg
	 */
	public void sendReportFullFileAnalyze(Loader pLoader, String pFileName, String pMsg);

	/**
	 * store loader result to repository
	 * @param pLoader loader
	 * @param pStartTime start time
	 * @param pEndTime end time
	 */
	public void storeReport(Loader pLoader, Date pStartTime, Date pEndTime);
}
