package vsg.load.tools;

import vsg.load.file.IReader;
import vsg.load.file.IWriter;
import vsg.load.info.FieldsInfo;
import vsg.load.info.FileInfo;
import vsg.load.info.PathInfo;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * @author Dmitry Golubev
 */
public interface IImportTools {
	/**
	 * retrieve loggerTools
	 *
	 * @return loggerTools
	 */
	public ILoggerTools getLoggerTools();

	/**
	 * file info object
	 *
	 * @param pFile file to be imported
	 * @return file info object
	 */
	public FileInfo getFileInfo(File pFile);

	/**
	 * return path info
	 *
	 * @return path info
	 */
	public PathInfo getPathInfo();

	/**
	 * return fields info
	 *
	 * @return fields info
	 */
	public FieldsInfo getFieldsInfo();

	/**
	 * return acceptable import file list
	 *
	 * @return acceptable import file list
	 */
	public ArrayList<FileInfo> getFileList();

	/**
	 * return acceptable import full-file list
	 *
	 * @return acceptable import full-file list
	 */
	public File[] getFullFileList();

	/**
	 * return latest full-file from archive dir
	 *
	 * @return latest full-file from archive dir
	 */
	public File getFullFileFromArchive();

	/**
	 * return writer
	 *
	 * @param pFile file to be written
	 * @return writer
	 */
	public IWriter getWriter(File pFile);

	/**
	 * return reader
	 *
	 * @param pFile file to be red
	 * @return reader
	 */
/*
	public IReader getReader(File pFile);
*/

	/**
	 * return reader
	 *
	 * @param pFile file to be red
	 * @return reader
	 */
	public IReader getReader(File pFile, FileInputStream pInputStream);

	/**
	 * move file to archive dir
	 *
	 * @param pFile file to be archived
	 * @return success state of archive
	 */
	public boolean archiveFile(File pFile);

	/**
	 * move file to archive dir
	 *
	 * @param pFile      file to be archived
	 * @param pTargetDir target dir
	 * @return success state of archive
	 */

	public boolean moveFile(File pFile, String pTargetDir);

	/**
	 * move file to error dir
	 *
	 * @param pFile file to be placed in error dir
	 * @return success state of move
	 */
	public boolean errorFile(File pFile);

	/**
	 * copy file
	 *
	 * @param pFile     file to copy
	 * @param pCopyFile destination file
	 * @return success state of copy
	 */
	public boolean copyFile(File pFile, File pCopyFile);

	/**
	 * check if error dir exist
	 */
	public void checkErrorFileDir();

	/**
	 * check if audit report dir exist
	 */
	public void checkAuditReportFileDir();

	/**
	 * check if archive dir exist
	 */
	public void checkArchiveFileDir();

	/**
	 * return new error file name
	 *
	 * @param pFile importing file
	 * @return new error file name
	 */
	public String getErrorFileName(File pFile);

	/**
	 * return new AuditReport file name
	 *
	 * @param pFileNamePrefix importing file
	 * @return new AuditReport file name
	 */

	public String getAuditReportFileName(String pFileNamePrefix);

	public String getAuditReportFileName(String pFileNamePrefix, String pFileType);

	/**
	 * check if lock file exist in specified dir
	 *
	 * @param pDir dir with imort file
	 * @return lock file exist
	 */
	public boolean checkLockFileExist(String pDir);

	/**
	 * delete lock file exist in specified dir
	 *
	 * @param pDir dir with imort file
	 */
	public void deleteLockFile(String pDir);

	/**
	 * check if file dir exist
	 *
	 * @param pDirName dir name
	 */
	public void checkFileDir(String pDirName);

	/**
	 * return separator for csv file
	 *
	 * @return FileSeparator char
	 */
	public char getFileSeparator();

	/**
	 * set separator for csv file
	 *
	 * @param pFileSeparator FileSeparator char
	 */
	public void setFileSeparator(char pFileSeparator);
}