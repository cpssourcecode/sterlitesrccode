package vsg.load.tools;

import vsg.load.info.FieldsInfo;
import vsg.load.info.ValidationResult;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public interface IValidationTools {
	/**
	 * return validation result
	 * @param pRecord record to be validated
	 * @param pFieldsInfo fields info
	 * @return validation result
	 */
	public ValidationResult validateRecord(Record pRecord, FieldsInfo pFieldsInfo);
}
