package vsg.load.tools;

/**
 * @author Dmitry Golubev
 */
public interface ILoggerTools {
	/**
	 * check if is logging info enabled
	 * @return logging info enabled
	 */
	public boolean isLoggingInfo();
	/**
	 * check if is logging debug enabled
	 * @return logging debug enabled
	 */
	public boolean isLoggingDebug();
	/**
	 * check if is logging error enabled
	 * @return logging error enabled
	 */
	public boolean isLoggingError();
	/**
	 * log info message
	 * @param pMsg message
	 */
	public void logInfo(String pMsg);
	/**
	 * log info message with params
	 * @param pMsg message
	 * @param pParams params
	 */
	public void logInfo(String pMsg, Object[] pParams);
	/**
	 * log debug message
	 * @param pMsg message
	 */
	public void logDebug(String pMsg);
	/**
	 * log debug message with params
	 * @param pMsg message
	 * @param pParams params
	 */
	public void logDebug(String pMsg, Object[] pParams);
	/**
	 * log error message
	 * @param pMsg message
	 */
	public void logError(String pMsg);
	/**
	 * log error exception
	 * @param pException exception
	 */
	public void logError(Exception pException);
	/**
	 * log error prepared message
	 * @param pMsg message
	 * @param pParams params
	 */
	public void logError(String pMsg, Object[] pParams);
}
