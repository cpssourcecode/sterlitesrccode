package vsg.load.tools;

import vsg.load.info.FieldsInfo;
import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 */
public interface IParserTools {
	/**
	 * return new record
	 * @param pFieldsInfo fields info
	 * @param pRow import row
	 * @return new record
	 */
	public Record getRecord(FieldsInfo pFieldsInfo, String[] pRow);
	/**
	 * return new record
	 * @param pFieldsInfo fields info
	 * @param pRow import row
	 * @param pIndex import index
	 * @return new record
	 */
	public Record getRecord(FieldsInfo pFieldsInfo, String[] pRow, int pIndex);
	/**
	 * init record with processed values
	 * @param pRecord record to be initialized
	 * @param pFieldsInfo fields info
	 * @return initialized record
	 */
	public Record initRecord(Record pRecord, FieldsInfo pFieldsInfo);
}
