package vsg.load.file;

import java.io.Reader;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public interface IReader {
	/**
	 * init file reader
	 * @param pFileReader file reader
	 */
	public void setFileReader(Reader pFileReader);
	/**
	 * Reads the next line from the buffer and converts to a string array.
	 * @return a string array with each comma-separated element as a separate entry.
	 */
	public String[] readNext();
	/**
	 * Reads the next line from the file.
	 * @return the next line from the file without trailing newline
	 */
	public String readNextLine();
	/**
	 * Reads the entire file into a List with each element being a String[] of tokens.
	 * @return a List of String[], with each String[] representing a line of the file.
	 */
	public List<String[]> readAll();
	/**
	 * Closes the underlying reader.
	 */
	public void close();

	/**
	 * init number of lines to skip
	 * @param pSkipLinesNumber number of lines to skip
	 */
	public void setSkipLinesNumber(int pSkipLinesNumber);

	/**
	 * init separator char
	 * @param pChar separator char
	 */
	public void setSeparator(char pChar);

	/**
	 * init quote char
	 * @param pChar quote char
	 */
	public void setQuoteChar(char pChar);
}
