package vsg.load.file.csv;

/**
 * @author Dmitry Golubev
 */
public class CSVBase {

	/**
	 * The default separator to use if none is supplied to the constructor.
 	 */
	public static final char DEFAULT_SEPARATOR = ',';
	/**
	 * The default quote character to use if none is supplied to the constructor.
	 */
	public static final char DEFAULT_QUOTE_CHARACTER = '"';
	/**
	 * The character used for escaping quotes.
 	 */
	public static final char ESCAPE_CHARACTER = '"';
	/**
	 * The tab character used for separator.
 	 */
	public static final char TAB_CHARACTER = '\t';

	/**
	 * The separator constant to use when you wish to suppress all separators.
 	 */
	public static final char NO_SEPARATOR = '\u0000';

	/**
	 * 	The quote constant to use when you wish to suppress all quoting.
	 */
	public static final char NO_QUOTE_CHARACTER = '\u0000';
	/**
	 * The default line to start reading.
 	 */
	public static final int DEFAULT_SKIP_LINES = 0;
	/**
	 * separator char
	 */
	protected char mSeparator = DEFAULT_SEPARATOR;
	/**
	 * quote char
	 */
	protected char mQuoteChar = DEFAULT_QUOTE_CHARACTER;

	/**
	 * check if char is used as quote
	 * @param pChar test char
	 * @return result of check if char is used as quote
	 */
	boolean isQuote(char pChar) {
		return pChar == mQuoteChar;
	}

	/**
	 * check if char is used as special symbol
	 * @param pChar test char
	 * @return result check if char is used as special symbol
	 */
	boolean isSpecialSymbol(char pChar) {
		return pChar == 92; // symbol '\'
	}

	/**
	 * check if char is used as separator
	 * @param pChar test char
	 * @return result of check if char is used as separator
	 */
	boolean isSeparator(char pChar) {
		return pChar == mSeparator;
	}

	/**
	 * init quote char
	 * @param pChar quote char
	 */
	public void setQuoteChar(char pChar) {
		this.mQuoteChar = pChar;
	}

	/**
	 * init separator char
	 * @param pChar separator char
	 */
	public void setSeparator(char pChar) {
		mSeparator = pChar;
	}
}
