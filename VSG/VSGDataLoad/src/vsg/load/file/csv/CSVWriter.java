package vsg.load.file.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import vsg.load.file.IWriter;
import vsg.load.tools.ILoggerTools;

/**
 * @author Dmitry Golubev
 */
public class CSVWriter extends CSVBase implements IWriter {
    /**
     * java.io.Writer writer
     */
    // private Writer mRawWriter;
    /**
     * java.io.PrintWriter printWriter
     */
    protected PrintWriter mPrintWriter;
    /**
     * end line
     */
    private String mLineEnd;
    /**
     * logger tools
     */
    protected ILoggerTools mLoggerTools;

    private File mFile;

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Constructs CSVWriter. Writer has to be initialized via setWriter.
     */
    public CSVWriter() {
        this(null, null);
    }

    /**
     * Constructs CSVWriter. Writer has to be initialized via setWriter.
     *
     * @param pLoggerTools
     *            logger tools
     */
    public CSVWriter(ILoggerTools pLoggerTools) {
        this(null, pLoggerTools);
    }

    /**
     * Constructs CSVWriter using a comma for the separator.
     *
     * @param pWriter
     *            the writer to an underlying CSV source.
     */
    public CSVWriter(Writer pWriter) {
        this(pWriter, null);
    }

    /**
     * Constructs CSVWriter using a comma for the separator.
     *
     * @param pWriter
     *            the writer to an underlying CSV source.
     * @param pLoggerTools
     *            logger tools
     */
    public CSVWriter(Writer pWriter, ILoggerTools pLoggerTools) {
        setWriter(pWriter);
        mLoggerTools = pLoggerTools;
        this.mSeparator = DEFAULT_SEPARATOR;
        this.mQuoteChar = DEFAULT_QUOTE_CHARACTER;
        this.mLineEnd = "\r\n";
    }

    /**
     * init writer
     *
     * @param pWriter
     *            the writer to an underlying CSV source.
     */
    @Override
    public void setWriter(Writer pWriter) {
        if (pWriter != null) {
            // mRawWriter = pWriter;
            mPrintWriter = new PrintWriter(pWriter);
        } else {
            // mRawWriter = null;
            mPrintWriter = null;
        }
    }

    @Override
    public void setWriter(File pFile, String pEncoding) {
        if (pEncoding == null) {
            try {
                mPrintWriter = new PrintWriter(pFile);
            } catch (FileNotFoundException e) {
                if (mLoggerTools != null) {
                    mLoggerTools.logError(e);
                }
            }
        } else {
            try {
                mPrintWriter = new PrintWriter(pFile, pEncoding);
            } catch (Exception e) {
                pEncoding = null;
                if (mLoggerTools != null) {
                    mLoggerTools.logError(e);
                }
                try {
                    mPrintWriter = new PrintWriter(pFile);
                } catch (FileNotFoundException e1) {
                    if (mLoggerTools != null) {
                        mLoggerTools.logError(e);
                    }
                }
            }
        }
        if (mLoggerTools != null && mLoggerTools.isLoggingDebug()) {
            String readerEncoding = pEncoding == null ? "" : pEncoding;
            mLoggerTools.logDebug(pFile.getName() + " - ready for write [" + readerEncoding + "]");
        }
        setFile(pFile);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Writes the entire list to a CSV file. The list is assumed to be a String[]
     *
     * @param pAllLines
     *            a List of String[], with each String[] representing a line of the file.
     */
    @Override
    public void writeAll(List<String[]> pAllLines) {
        for (String[] nextLine : pAllLines) {
            writeNext(nextLine);
        }
    }

    /**
     * Writes the next line to the file.
     *
     * @param pNextLines
     *            a string array with each comma-separated element as a separate entry.
     */
    @Override
    public void writeNext(String[] pNextLines) {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        for (String nextLine : pNextLines) {
            appendSeparator(sb, i);
            if (nextLine == null) {
                continue;
            }
            appendQuoteChar(sb);
            appendNextLine(nextLine, sb);
            sb.append(" ");
            appendQuoteChar(sb);
            i++;
        }

        sb.append(mLineEnd);
        mPrintWriter.write(sb.toString());
    }

    /**
     * Writes the next line to the file.
     *
     * @param pNextLines
     *            a string array with each comma-separated element as a separate entry.
     */
    @Override
    public void writeAuditNext(String[] pNextLines) {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        for (String nextLine : pNextLines) {
            appendSeparator(sb, i);
            if (nextLine == null) {
                continue;
            }
            appendQuoteChar(sb);
            appendAuditNextLine(nextLine, sb);
            appendQuoteChar(sb);
            i++;
        }

        sb.append(mLineEnd);
        mPrintWriter.write(sb.toString());
    }

    /**
     * Writes the next line to the file.
     *
     * @param pNextLine
     *            a string with each comma-separated element as a separate entry.
     */
    @Override
    public void writeNext(String pNextLine) {
        StringBuilder sb = new StringBuilder();
        sb.append(pNextLine);
        sb.append(mLineEnd);
        mPrintWriter.write(sb.toString());
    }

    /**
     * add new line to buffer, which be written to file
     * same as appendNextLine but we dont want to escape the escape char
     *
     * @param pNextLine
     *            new line
     * @param pStringBuilder
     *            collector of lines to be written to file
     */
    protected void appendAuditNextLine(String pNextLine, StringBuilder pStringBuilder) {
        char[] nextElementChars = pNextLine.toCharArray();
        for (char nextChar : nextElementChars) {
            if (isQuote(nextChar)) {
                pStringBuilder.append(ESCAPE_CHARACTER).append(nextChar);
            } else {
                pStringBuilder.append(nextChar);
            }
        }
    }

    /**
     * add new line to buffer, which be written to file
     *
     * @param pNextLine
     *            new line
     * @param pStringBuilder
     *            collector of lines to be written to file
     */
    protected void appendNextLine(String pNextLine, StringBuilder pStringBuilder) {
        char[] nextElementChars = pNextLine.toCharArray();
        for (char nextChar : nextElementChars) {
            if (isQuote(nextChar)) {
                pStringBuilder.append(ESCAPE_CHARACTER).append(nextChar);
            } else if (nextChar == ESCAPE_CHARACTER) {
                pStringBuilder.append(ESCAPE_CHARACTER).append(nextChar);
            } else {
                pStringBuilder.append(nextChar);
            }
        }
    }
    /**
     * append separator
     *
     * @param pStringBuilder
     *            collector of lines to be written to file
     * @param pIndex
     *            current index
     */
    private void appendSeparator(StringBuilder pStringBuilder, int pIndex) {
        if (mSeparator != NO_SEPARATOR && pIndex != 0) {
            pStringBuilder.append(mSeparator);
        }
    }

    /**
     * append quote separator
     *
     * @param pStringBuilder
     *            collector of lines to be written to file
     */
    private void appendQuoteChar(StringBuilder pStringBuilder) {
        if (mQuoteChar != NO_QUOTE_CHARACTER) {
            pStringBuilder.append(mQuoteChar);
        }
    }

    /**
     * Close the underlying stream writer flushing any buffered content.
     */
    @Override
    public void close() {
        try {
            mPrintWriter.flush();
            mPrintWriter.close();
            // mRawWriter.close();
        } catch (Exception e) {
            if (mLoggerTools != null) {
                mLoggerTools.logError(e);
            }
        }
    }

    /**
     * Sets new mFile.
     *
     * @param pFile
     *            New value of mFile.
     */
    @Override
    public void setFile(File pFile) {
        mFile = pFile;
    }

    /**
     * Gets mFile.
     *
     * @return Value of mFile.
     */
    @Override
    public File getFile() {
        return mFile;
    }
}
