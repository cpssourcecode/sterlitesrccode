package vsg.load.file.csv;

import vsg.load.file.IReader;
import vsg.load.tools.ILoggerTools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class CSVReader extends CSVBase implements IReader {
	/**
	 * java.io.BufferedReader
	 */
	private BufferedReader mReader;
	/**
	 * has more chars flag
	 */
	private boolean mHasNext = true;
	/**
	 * number of skipped lines on initial read
	 */
	private int mSkipLinesNumber;
	/**
	 * skip line flag
	 */
	private boolean mLinesSkipped;

	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * default constructor<br>
	 * <b>no reader is initialized</b> - has to be set after
	 */
	public CSVReader() {
		this(null, null);
	}

	/**
	 * default constructor<br>
	 * <b>no reader is initialized</b> - has to be set after
	 * @param pLoggerTools logger tools
	 */
	public CSVReader(ILoggerTools pLoggerTools) {
		this(null, pLoggerTools);
	}

	/**
	 * constructor with file reader
	 * @param pReader file reader
	 */
	public CSVReader(Reader pReader) {
		this(pReader, null);
	}

	/**
	 * constructor with file reader
	 * @param pReader file reader
	 * @param pLoggerTools logger tools
	 */
	public CSVReader(Reader pReader, ILoggerTools pLoggerTools) {
		setFileReader(pReader);
		this.mLoggerTools = pLoggerTools;
		this.mSeparator = DEFAULT_SEPARATOR;
		this.mQuoteChar = DEFAULT_QUOTE_CHARACTER;
		this.mSkipLinesNumber = DEFAULT_SKIP_LINES;
	}

	/**
	 * init file reader
	 * @param pFileReader file reader
	 */
	@Override
	public void setFileReader(Reader pFileReader) {
		if(pFileReader == null) {
			this.mReader = null;
		} else {
			this.mReader = new BufferedReader( pFileReader );
			mHasNext = true;
		}
	}

	/**
	 * init number of lines to skip
	 * @param pSkipLinesNumber number of lines to skip
	 */
	@Override
	public void setSkipLinesNumber(int pSkipLinesNumber) {
		mSkipLinesNumber = pSkipLinesNumber;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * Reads the entire file into a List with each element being a String[] of tokens.
	 * @return a List of String[], with each String[] representing a line of the file.
	 */
	@Override
	public List<String[]> readAll() {
		List<String[]> allElements = new ArrayList<String[]>();
		while (mHasNext) {
			String[] nextLineAsTokens = readNext();
			if (nextLineAsTokens != null) {
				allElements.add(nextLineAsTokens);
			}
		}
		return allElements;
	}

	/**
	 * Reads the next line from the buffer and converts to a string array.
	 * @return a string array with each comma-separated element as a separate entry.
	 */
	@Override
	public String[] readNext() {
		if(mHasNext) {
			String[] result = parseLine(readNextLine());
			if(result!=null) {
				for(int i=0;i<result.length;i++) {
					if(result[i]!=null) {
						result[i] = result[i].trim();
					}
				}
			}
			return result;
		} else {
			return null;
		}
	}

	/**
	 * Reads the next line from the file.
	 * @return the next line from the file without trailing newline
	 */
	@Override
	public String readNextLine() {
		String nextLine;
		try {
			if (!mLinesSkipped) {
				for (int i = 0; i < mSkipLinesNumber; i++) {
					nextLine = mReader.readLine();
				}
				mLinesSkipped = true;
			}
			nextLine = mReader.readLine();
			if (nextLine == null) {
				mHasNext = false;
			}
		} catch (IOException e) {
			if(mLoggerTools!=null) {
				mLoggerTools.logError(e);
			}
			return null;
		}
		return mHasNext ? nextLine : null;
	}

	/**
	 * Parses an incoming String and returns an array of elements.
	 * @param pNextLine the string to parse
	 * @return the comma-token list of elements, or null if nextLine is null
	 */
	private String[] parseLine(String pNextLine) {
		if (pNextLine == null) {
			return null;
		}
		List<String> tokensOnThisLine = new ArrayList<String>();
		StringBuffer sb = new StringBuffer();
		processLine(pNextLine, false, sb, tokensOnThisLine);
		return tokensOnThisLine.toArray(new String[tokensOnThisLine.size()]);
	}

	/**
	 * process new line to tokens
	 * @param pNextLine new line to analyze
	 * @param pIsInQuote process of line in state of quotes
	 * @param pStringBuffer current set of processed chars
	 * @param pTokens list of generated string tokens from file row
	 */
	private void processLine(String pNextLine, boolean pIsInQuote, StringBuffer pStringBuffer, List<String> pTokens) {
		char[] lineChars = pNextLine.toCharArray();
		for (int i = 0; i < lineChars.length; i++) {
			boolean hasNext = hasNextChar(lineChars, i + 1);
			boolean isPrevSpecialSymbol = isPrevSpecialSymbol(lineChars, i);
			boolean isPrevQuoteSymbol = isPrevQuoteSymbol(lineChars, i);
/*
			boolean isNextQuoteSymbol = hasNext && isQuote(lineChars[i+1]);
			boolean isPrevSeparator= isPrevSeparatorSymbol(lineChars, i);
			boolean isNextSeparator = hasNext && isSeparator(lineChars[i+1]);
*/

			if(isQuote(lineChars[i])) {
				if(isPrevSpecialSymbol || isPrevQuoteSymbol) {
					if(processLineNotDelimeterQuoteSymbol(lineChars, i, pIsInQuote, hasNext, pStringBuffer, pTokens)) {
						continue;
					}
				} else {
					pIsInQuote = !pIsInQuote || processLineQuoteSymbol(lineChars, i, hasNext, pStringBuffer, pTokens);
				}
			} else {
				if(processLineNotDelimeterQuoteSymbol(lineChars, i, pIsInQuote, hasNext, pStringBuffer, pTokens)) {
					continue;
				}
			}
			if(!hasNext) {
				processLineNoMoreSymbols(pIsInQuote, pStringBuffer, pTokens);
			}
		}
		processLineEmpty(lineChars, pIsInQuote, pStringBuffer, pTokens);
	}

	/**
	 * process line in 'quote symbol found' state
	 * @param pChars current file row as char array
	 * @param pIndex current index of char array analyzing
	 * @param pHasNext flag if there are more chars to analyze
	 * @param pStringBuffer current set of processed chars
	 * @param pTokens list of generated string tokens from file row
	 * @return row is still analyzing, no new toked is added
	 */
	private boolean processLineQuoteSymbol(char[] pChars, int pIndex, boolean pHasNext, StringBuffer pStringBuffer,
									  List<String> pTokens) {
		if(pHasNext) {
			char nextChar = pChars[pIndex+1];
			if(isSeparator(nextChar)) {
				addToken( pTokens, pStringBuffer);
				return false;
			}
		} else {
			addToken(pTokens, pStringBuffer);
			return false;
		}
		return true;
	}

	/**
	 * process line in 'no quote symbol found' state
	 * @param pChars current file row as char array
	 * @param pIndex current index of char array analyzing
	 * @param pIsInQuote process of line in state of quotes
	 * @param pHasNext flag if there are more chars to analyze
	 * @param pStringBuffer current set of processed chars
	 * @param pTokens list of generated string tokens from file row
	 * @return separator is proceeded
	 */
	private boolean processLineNotDelimeterQuoteSymbol(char[] pChars, int pIndex, boolean pIsInQuote, boolean pHasNext,
													   StringBuffer pStringBuffer, List<String> pTokens) {
		char currentChar = pChars[pIndex];
		if(pIsInQuote) {
			pStringBuffer.append(currentChar);
		} else {
			if(isSeparator(currentChar)) {
				processLineSeparatorSymbol(pChars, pIndex, pHasNext, pStringBuffer, pTokens);
				return true;
			}
			pStringBuffer.append(currentChar);
		}
		return false;
	}

	/**
	 * process line with found separator
	 * @param pChars current file row as char array
	 * @param pIndex current index of char array analyzing
	 * @param pHasNext flag if there are more chars to analyze
	 * @param pStringBuffer current set of processed chars
	 * @param pTokens list of generated string tokens from file row
	 */
	private void processLineSeparatorSymbol(char[] pChars, int pIndex, boolean pHasNext, StringBuffer pStringBuffer,
								  List<String> pTokens) {
		if(isPrevSeparatorSymbol(pChars, pIndex)) {
			addToken( pTokens, pStringBuffer);
		} else {
			if(pStringBuffer.length() > 0) { // added without quotes
				addToken( pTokens, pStringBuffer);
			}
		}
		if(!pHasNext) {
			addToken(pTokens, pStringBuffer);
		}
	}

	/**
	 * process empty line passed to analyze
	 * @param pChars current file row as char array
	 * @param pIsInQuote process of line in state of quotes
	 * @param pStringBuffer current set of processed chars
	 * @param pTokens list of generated string tokens from file row
	 */
	private void processLineEmpty(char[] pChars, boolean pIsInQuote, StringBuffer pStringBuffer,
								  List<String> pTokens) {
		if(pChars.length == 0) {
			processLineNoMoreSymbols(pIsInQuote, pStringBuffer,
					pTokens);
		}
	}

	/**
	 * process ended line with check for 'quote symbol found'
	 * @param pIsInQuote process of line in state of quotes
	 * @param pStringBuffer current set of processed chars
	 * @param pTokens list of generated string tokens from file row
	 */
	private void processLineNoMoreSymbols(boolean pIsInQuote, StringBuffer pStringBuffer,
										  List<String> pTokens) {
		if(pIsInQuote) {
			pStringBuffer.append("\n");
			String nextLine = readNextLine();
			if(nextLine != null) {
				processLine(nextLine, true, pStringBuffer, pTokens);
			}
		} else {
			if(pStringBuffer.length() > 0) {
				addToken(pTokens, pStringBuffer);
			}
		}
	}

	/**
	 * check if previous symbol is separator
	 * @param pChars current file row as char array
	 * @param pIndex current index of char array analyzing
	 * @return check if previous symbols is separator
	 */
	private boolean isPrevSeparatorSymbol(char[] pChars, int pIndex) {
		return pIndex - 1 > 0 && isSeparator(pChars[pIndex - 1]);
	}

	/**
	 * check if previous symbol is quote
	 * @param pChars current file row as char array
	 * @param pIndex current index of char array analyzing
	 * @return check if previous symbols is separator
	 */
	private boolean isPrevQuoteSymbol(char[] pChars, int pIndex) {
		int numberOfQuoteSymbols = 0;
		pIndex --;
		boolean isSeparatorBefore = false;
		boolean isSpecialSymbolBefore = false;
		while(pIndex > 0) {
			if(isQuote( pChars[pIndex] )) {
				numberOfQuoteSymbols ++;
			} else {
				isSeparatorBefore = isSeparator(pChars[pIndex]);
				isSpecialSymbolBefore = isSpecialSymbol(pChars[pIndex]);
				break;
			}
			pIndex--;
		}
		return !isSeparatorBefore &&
				!isSpecialSymbolBefore &&
				!(numberOfQuoteSymbols == 0 || numberOfQuoteSymbols % 2 == 0);
	}

	/**
	 * check if previous symbol is special
	 * @param pChars current file row as char array
	 * @param pIndex current index of char array analyzing
	 * @return check if previous symbols is special
	 */
	private boolean isPrevSpecialSymbol(char[] pChars, int pIndex) {
		int numberOfSpecialSymbols = 0;
		pIndex --;
		while(pIndex > 0) {
			if(isSpecialSymbol( pChars[pIndex] )) {
				numberOfSpecialSymbols ++;
			} else {
				break;
			}
			pIndex--;
		}
		return !(numberOfSpecialSymbols == 0 || numberOfSpecialSymbols % 2 == 0);
	}

	/**
	 * add token to list of found tokens and clear current buffer
	 * @param pTokens list of generated string tokens from file row
	 * @param pStringBuffer current set of processed chars
	 */
	private void addToken(List<String> pTokens, StringBuffer pStringBuffer) {
		pTokens.add(pStringBuffer.toString());
		pStringBuffer.setLength(0); // clear content
	}

	/**
	 * check if there are more symbols
	 * @param pChars current file row as char array
	 * @param pIndex required length
	 * @return check if there are more symbols
	 */
	private boolean hasNextChar(char[] pChars, int pIndex) {
		return pChars.length > pIndex;
	}

	/**
	 * Closes the underlying reader.
	 */
	@Override
	public void close() {
		try {
			mReader.close();
		} catch (IOException e) {
			if(mLoggerTools!=null) {
				mLoggerTools.logError(e);
			}
		}
	}
}
