package vsg.load.file;

import java.io.File;
import java.io.Writer;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public interface IWriter {

    /**
     * init writer
     *
     * @param pWriter
     *            the writer to an underlying CSV source.
     */
    public void setWriter(Writer pWriter);

    /**
     * init writer
     *
     * @param pFile
     *            file to write
     * @param pEncoding
     *            write encoding
     */
    public void setWriter(File pFile, String pEncoding);

    /**
     * Writes the next line to the file.
     *
     * @param pNextLines
     *            a string array with each comma-separated element as a separate entry.
     */
    public void writeNext(String[] pNextLines);

    public void writeAuditNext(String[] pNextLines);

    /**
     * Writes the next line to the file.
     *
     * @param pNextLine
     *            a string with each comma-separated element as a separate entry.
     */
    public void writeNext(String pNextLine);

    /**
     * Writes the entire list to a CSV file. The list is assumed to be a String[]
     *
     * @param pAllLines
     *            a List of String[], with each String[] representing a line of the file.
     */
    public void writeAll(List<String[]> pAllLines);

    /**
     * Close the underlying stream writer flushing any buffered content.
     */
    public void close();

    public void setFile(File pFile);

    /**
     * Gets mFile.
     *
     * @return Value of mFile.
     */
    public File getFile();
}
