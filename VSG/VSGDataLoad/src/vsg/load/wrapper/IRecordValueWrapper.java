package vsg.load.wrapper;

import vsg.load.loader.Record;

/**
 * @author Dmitry Golubev
 * @param <WRAPPED_TYPE> template class
 */
public interface IRecordValueWrapper<WRAPPED_TYPE> {
	/**
	 * retrieve value from
	 * @param pRecord inited record
	 */
	public WRAPPED_TYPE getValue(Record pRecord);
}
