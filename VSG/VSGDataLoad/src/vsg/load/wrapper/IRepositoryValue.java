package vsg.load.wrapper;

import atg.repository.Repository;
import atg.repository.RepositoryException;

/**
 * @author Dmitry Golubev
 * @param <WRAPPED_TYPE> template class
 */
public interface IRepositoryValue<WRAPPED_TYPE> {
	/**
	 * return WRAPPED_TYPE object
	 * @param pRepository repository
	 * @return WRAPPED_TYPE object
	 * @throws RepositoryException if error occurs
	 */
	public WRAPPED_TYPE getValue(Repository pRepository) throws RepositoryException;
}
