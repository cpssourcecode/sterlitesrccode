package vsg.load.wrapper;

/**
 * @author Dmitry Golubev
 * @param <WRAPPED_TYPE> template class
 */
public interface IValueWrapper<WRAPPED_TYPE> {
	/**
	 * convert string to WRAPPED_TYPE
	 * @param pValue string WRAPPED_TYPE presentation
	 * @return WRAPPED_TYPE
	 */
	public WRAPPED_TYPE getValue(Object pValue);
}
