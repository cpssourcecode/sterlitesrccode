package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class UpperCaseWrapper extends ValueWrapperImpl {
	/**
	 * return upper case value
	 * @param pValueObj passed value
	 * @return upper case value
	 */
	@Override
	public String getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if ( !StringUtils.isBlank(pValue) ) {
			return StringUtils.toUpperCase(pValue);
		}
		return pValue;
	}
}
