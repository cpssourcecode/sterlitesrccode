package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class StringTrimWrapper extends ValueWrapperImpl {
	private int mMaxLength = -1;
	/**
	 * return lower case value
	 * @param pValueObj passed value
	 * @return lower case value
	 */
	@Override
	public String getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if (!StringUtils.isBlank(pValue) && mMaxLength != -1 && (pValue.length() > mMaxLength)) {
			return pValue.substring(0, mMaxLength);
		}
		return pValue;
	}

	public int getMaxLength() {
		return mMaxLength;
	}

	public void setMaxLength(int pMaxLength) {
		mMaxLength = pMaxLength;
	}
}