package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class PostfixWrapper extends ValueWrapperImpl {
	/**
	 * postfix
	 */
	private String mPostfix;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return postfix
	 * @return postfix
	 */
	public String getPostfix() {
		return mPostfix;
	}

	/**
	 * init postfix
	 * @param pPostfix postfix
	 */
	public void setPostfix(String pPostfix) {
		mPostfix = pPostfix;
	}


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return passed value + postfix
	 * @param pValueObj passed value
	 * @return passed value + postfix
	 */
	@Override
	public String getValue(Object pValueObj) {
		String result;
		String pValue = getTrimmedValue( (String) pValueObj );
		if ( !StringUtils.isBlank(pValue) ) {
			result = pValue + getPostfix();
		} else {
			result = pValue;
		}
		return result;
	}
}
