package vsg.load.wrapper.impl;

import vsg.load.connection.ISQLConnector;
import vsg.load.loader.impl.SQLLoader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Dmitry Golubev
 */
public class OtherColumnSQLWrapper extends ValueWrapperImpl {
	/**
	 * parent sql loader
	 */
	private SQLLoader mParentLoader;
	/**
	 * connector to search table
	 */
	private ISQLConnector mConnector;
	/**
	 * table name
	 */
	private String mTableName;

	/**
	 * table column name - filter
	 */
	private String mTableColumnName;

	/**
	 * table column name - project to be used as wrapped value
	 */
	private String mTableOtherColumnName;

	/**
	 * sql select other column
	 */
	protected String mSqlOtherColumn;

	/**
	 * allow null value from other column
	 */
	private boolean mAllowOtherNullValue = true;

	// -----------------------------------------------------------------------------------------------------------------

	public SQLLoader getParentLoader() {
		return mParentLoader;
	}

	public void setParentLoader(SQLLoader pParentLoader) {
		mParentLoader = pParentLoader;
	}

	public ISQLConnector getConnector() {
		return mConnector;
	}

	public void setConnector(ISQLConnector pConnector) {
		mConnector = pConnector;
	}

	public String getTableName() {
		return mTableName;
	}

	public void setTableName(String pTableName) {
		mTableName = pTableName;
	}

	public String getTableColumnName() {
		return mTableColumnName;
	}

	public void setTableColumnName(String pTableColumnName) {
		mTableColumnName = pTableColumnName;
	}

	public String getTableOtherColumnName() {
		return mTableOtherColumnName;
	}

	public void setTableOtherColumnName(String pTableOtherColumnName) {
		mTableOtherColumnName = pTableOtherColumnName;
	}

	public boolean isAllowOtherNullValue() {
		return mAllowOtherNullValue;
	}

	public void setAllowOtherNullValue(boolean pAllowOtherNullValue) {
		mAllowOtherNullValue = pAllowOtherNullValue;
	}

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	public Object getValue(Object pValueObj) {
		String pValue = getTrimmedValue((String) pValueObj);
		if (pValue != null) {
			boolean exist = false;
			Object otherColumnValue = null;
			try (Connection connection = getConnection();
				 PreparedStatement ps = connection.prepareStatement(getSqlExistColumn());) {
				ps.setString(1, pValue);
				try (ResultSet rs = ps.executeQuery();) {
					exist = rs.next();
					if (exist) {
						otherColumnValue = rs.getObject(1);
					}
					rs.close();
					ps.close();
					closeConnection(connection);
				}
			} catch (Exception e) {
				getLoggerTools().logError(e);
			}
			return otherColumnValue;
		} else {
			return null;
		}
	}

	/**
	 * initialize connection
	 *
	 * @return connection
	 */
	protected Connection getConnection() {
		if (getConnector() == null) {
			return getParentLoader().getConnection();
		} else {
			return getConnector().getConnection();
		}
	}

	/**
	 * close connection
	 *
	 * @param pConnection connection
	 * @throws SQLException if error occurs
	 */
	protected void closeConnection(Connection pConnection) throws SQLException {
		if (getConnector() != null) {
			pConnection.close();
		}
	}

	/**
	 * form sql exist
	 *
	 * @return sql exist
	 */
	public String getSqlExistColumn() {
		if (mSqlOtherColumn == null) {
			mSqlOtherColumn =
					"select " + getTableOtherColumnName() + " from " + getTableName() +
							" where " + getTableColumnName() + "=?";
			if (!isAllowOtherNullValue()) {
				mSqlOtherColumn += " and " + getTableOtherColumnName() + " is not null";
			}
		}
		return mSqlOtherColumn;
	}

}
