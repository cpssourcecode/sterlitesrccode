package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class NullInputWrapper extends ValueWrapperImpl<String> {

	/**
	 * return specified empty value if passed value is null
	 * @param pValueObj passed value to check for null
	 * @return wrapped value
	 */
	@Override
	public String getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}
		return pValue;
	}
}
