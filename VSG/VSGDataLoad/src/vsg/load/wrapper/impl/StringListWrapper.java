package vsg.load.wrapper.impl;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;

/**
 * 
 * @author VSG
 *
 */
public class StringListWrapper extends ValueWrapperImpl<List<String>> {

	/**
	 * separator
	 */
	private String mSeparator;

	/**
	 * split string by separator and convert to list of strings
	 */
	@Override
	public List<String> getValue(Object pValue) {
		List<String> result = new ArrayList<String>();

		if (pValue instanceof String) {
			String[] values = ((String) pValue).split(getSeparator());
			if(values.length > 0) {
				for(String val: values) {
					if(StringUtils.isNotBlank(val)) {
						result.add(val);
					}
				}
			}
		}
		
		return result.size() > 0 ? result : null;
	}

	public String getSeparator() {
		return mSeparator;
	}

	public void setSeparator(String pSeparator) {
		mSeparator = pSeparator;
	}

}
