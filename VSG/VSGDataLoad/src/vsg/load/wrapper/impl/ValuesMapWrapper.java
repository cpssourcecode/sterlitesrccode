package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

import java.util.HashMap;

/**
 * @author Dmitry Golubev
 */
public class ValuesMapWrapper extends ValueWrapperImpl<String> {

	/**
	 * map of string-string values
	 */
	private HashMap<String, String> mValues;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return map of string-string values
	 * @return map of string-string values
	 */
	public HashMap<String, String> getValues() {
		return mValues;
	}

	/**
	 * init map of string-string values
	 * @param pValues map of string-string values
	 */
	public void setValues(HashMap<String, String> pValues) {
		mValues = pValues;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * replace string with mapped string value
	 * @param pValueObj string input
	 * @return replaced string value
	 */
	@Override
	public String getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}

		if(getValues() == null || getValues().isEmpty()) {
			return getEmptyValue();
		}

		try {
			if(getValues().containsKey(pValue)) {
				return getValues().get(pValue);
			} else {
				if(!StringUtils.isBlank(getEmptyValue())) {
					return getEmptyValue();
				}
				return pValue;
			}
		} catch (Exception e) {
			logError("FAILED wrap ["+pValue+"] from values map ");
		}

		return getEmptyValue();
	}
}
