package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class DoubleWrapper extends ValueWrapperImpl<Double> {

	/**
	 * return empty value
	 * @return empty value
	 */
	@Override
	public Double getEmptyValue() {
		return 0.0;
	}

	/**
	 * convert string to double
	 * @param pValueObj string double presentation
	 * @return double
	 */
	@Override
	public Double getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(isAllowNull() && StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}
		try {
			return Double.parseDouble(pValue);
		} catch (Exception e) {
			logError("FAILED wrap ["+pValue+"] to double ");
		}
		return getEmptyValue();
	}
}
