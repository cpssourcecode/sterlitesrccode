package vsg.load.wrapper.impl;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import vsg.load.wrapper.IRepositoryValue;

/**
 * @author Dmitry Golubev
 */
public class RepositoryRepositoryValue implements IRepositoryValue<Object> {
	/**
	 * repository item id
	 */
	private String mRepositoryId;
	/**
	 * item descriptor name
	 */
	private String mItemDescriptor;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return repository id
	 * @return repository id
	 */
	public String getRepositoryId() {
		return mRepositoryId;
	}

	/**
	 * return item descriptor
	 * @return item descriptor
	 */
	public String getItemDescriptor() {
		return mItemDescriptor;
	}

	// -----------------------------------------------------------------------------------------------------------------


	/**
	 * constructor
	 * @param pRepositoryId repository id
	 * @param pItemDescriptor item descriptor
	 */
	public RepositoryRepositoryValue(String pRepositoryId, String pItemDescriptor) {
		mRepositoryId = pRepositoryId;
		mItemDescriptor = pItemDescriptor;
	}

	/**
	 * return repository item
	 * @param pRepository repository
	 * @return repository item
	 * @throws RepositoryException if error occurs
	 */
	@Override
	public Object getValue(Repository pRepository) throws RepositoryException {
		return pRepository.getItem( getRepositoryId(), getItemDescriptor() );
	}
}
