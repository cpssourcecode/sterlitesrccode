package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

import java.util.HashMap;

/**
 * @author Dmitry Golubev
 */
public class ValuesMapIntWrapper extends ValueWrapperImpl<Integer> {

	/**
	 * map of string-int values
	 */
	private HashMap<String, String> mValues;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return map of string-int values
	 * @return map of string-int values
	 */
	public HashMap<String, String> getValues() {
		return mValues;
	}

	/**
	 * init map of string-boolean values
	 * @param pValues map of string-boolean values
	 */
	public void setValues(HashMap<String, String> pValues) {
		mValues = pValues;
	}

// -----------------------------------------------------------------------------------------------------------------


	/**
	 * convert boolean from string
	 * @param pValueObj boolean-int string presentation
	 * @return boolean value
	 */
	@Override
	public Integer getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}

		if(getValues() == null || getValues().isEmpty()) {
			return getEmptyValue();
		}

		try {
			if(getValues().containsKey(pValue)) {
				return Integer.parseInt(getValues().get(pValue));
			} else {
				return getEmptyValue();
			}
		} catch (Exception e) {
			logError("FAILED wrap ["+pValue+"] from values map ");
		}

		return getEmptyValue();
	}

	@Override
	public Integer getEmptyValue() {
		Object superEmpty = super.getEmptyValue();
		return new Integer((String)superEmpty);
	}
}
