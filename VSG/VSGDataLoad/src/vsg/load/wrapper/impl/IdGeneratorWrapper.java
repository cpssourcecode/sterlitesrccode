package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;
import atg.service.idgen.IdGeneratorException;
import atg.service.idgen.SQLIdGenerator;

/**
 * @author Dmitry Golubev
 */
public class IdGeneratorWrapper extends ValueWrapperImpl<String> {

	/**
	 * id generator
	 */
	private SQLIdGenerator mIdGenerator;
	/**
	 * id space name
	 */
	private String mIdSpaceName;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return id generator
	 * @return id generator
	 */
	public SQLIdGenerator getIdGenerator() {
		return mIdGenerator;
	}

	/**
	 * init id generator
	 * @param pIdGenerator id generator
	 */
	public void setIdGenerator(SQLIdGenerator pIdGenerator) {
		mIdGenerator = pIdGenerator;
	}

	/**
	 * return id space name
	 * @return id space name
	 */
	public String getIdSpaceName() {
		return mIdSpaceName;
	}

	/**
	 * init id space name
	 * @param pIdSpaceName id space name
	 */
	public void setIdSpaceName(String pIdSpaceName) {
		mIdSpaceName = pIdSpaceName;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * get new id from id generator instead of passed value
	 * @param pValueObj fake value
	 * @return generated id
	 */
	@Override
	public String getValue(Object pValueObj) {
		try {
			if(StringUtils.isBlank(getIdSpaceName())) {
				return getIdGenerator().generateStringId();
			} else {
				return getIdGenerator().generateStringId(getIdSpaceName());
			}
		} catch (IdGeneratorException e) {
			throw new RuntimeException(e);
		}
	}
}
