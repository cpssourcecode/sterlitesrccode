package vsg.load.wrapper.impl;

/**
 * 
 * @author Mark Miller
 *
 */
public class EscapeSymbolsRemoverWrapper extends ValueWrapperImpl<String> {

	/**
	 * removes escape symbols
	 * 
	 * @param pValueObj - value
	 * @return string without escape symbols
	 */
	@Override
	public String getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );

		return pValue.replace( "\\\\", "\\" ).replace( "\\\"", "\"" );
		
	}
	
}
