package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public class DateWrapper extends ValueWrapperImpl<Date> {

	/**
	 * date pattern
	 */
	private String mDatePattern="MM/dd/yyyy kk:mm";

	/**
	 * SimpleDateFormat obj
	 */
	private SimpleDateFormat mDateFormat;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return date pattern
	 * @return date pattern
	 */
	public String getDatePattern() {
		return mDatePattern;
	}

	/**
	 * init date pattern
	 * @param pDatePattern date pattern
	 */
	public void setDatePattern(String pDatePattern) {
		mDatePattern = pDatePattern;
	}

	/**
	 * init date formatter if it's null
	 * @return date formatter
	 */
	public SimpleDateFormat getDateFormat() {
		if(mDateFormat == null) {
			mDateFormat = new SimpleDateFormat(getDatePattern());
		}
		return mDateFormat;
	}

// -----------------------------------------------------------------------------------------------------------------

	/**
	 * convert string to date
	 * @param pValueObj string date presentation
	 * @return date
	 */
	@Override
	public Date getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(isAllowNull() && StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}
		try {
			return getDateFormat().parse(pValue);
		} catch (Exception e) {
			logError("FAILED wrap ["+pValue+"] to date ");
		}
		return getEmptyValue();
	}
}
