package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class PrefixWrapper extends ValueWrapperImpl {
	/**
	 * prefix
	 */
	private String mPrefix;


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return prefix
	 * @return prefix
	 */
	public String getPrefix() {
		return mPrefix;
	}

	/**
	 * init prefix
	 * @param pPrefix prefix
	 */
	public void setPrefix(String pPrefix) {
		if(pPrefix == null) {
			pPrefix = "";
		}
		mPrefix = pPrefix;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return prefix + passed value
	 * @param pValueObj passed value
	 * @return prefix + passed value
	 */
	@Override
	public String getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if ( !StringUtils.isBlank(pValue) ) {
			return getPrefix() + pValue;
		} else {
			return pValue;
		}
	}
}
