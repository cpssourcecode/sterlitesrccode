package vsg.load.wrapper.impl;

import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public class CurrentDateWrapper extends ValueWrapperImpl<Date> {

	/**
	 * convert string to date
	 * @param pValueObj string date presentation
	 * @return date
	 */
	@Override
	public Date getValue(Object pValueObj) {
		return new Date();
	}
}
