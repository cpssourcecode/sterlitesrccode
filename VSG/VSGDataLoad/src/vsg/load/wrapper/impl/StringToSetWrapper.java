package vsg.load.wrapper.impl;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class StringToSetWrapper extends ValueWrapperImpl<Set<String>> {
	/**
	 * return set of strings
	 * @param pValueObj passed value
	 * @return set of strings
	 */
	@Override
	public Set<String> getValue(Object pValueObj) {
		Set<String> resultValues = new HashSet<String>();
		if(pValueObj!=null) {
			resultValues.add( getTrimmedValue( (String) pValueObj ) );
		}
		return resultValues;
	}
}
