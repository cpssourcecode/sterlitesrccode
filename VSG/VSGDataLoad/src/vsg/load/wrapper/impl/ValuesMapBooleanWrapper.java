package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

import java.util.HashMap;

/**
 * @author Dmitry Golubev
 */
public class ValuesMapBooleanWrapper extends ValueWrapperImpl<Boolean> {

	/**
	 * map of string-boolean values
	 */
	private HashMap<String, String> mValues;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return map of string-boolean values
	 * @return map of string-boolean values
	 */
	public HashMap<String, String> getValues() {
		return mValues;
	}

	/**
	 * init map of string-boolean values
	 * @param pValues map of string-boolean values
	 */
	public void setValues(HashMap<String, String> pValues) {
		mValues = pValues;
	}

	/**
	 * return empty value
	 * @return empty value
	 */
	@Override
	public Boolean getEmptyValue() {
		return false;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * convert boolean from string
	 * @param pValueObj boolean string presentation
	 * @return boolean value
	 */
	@Override
	public Boolean getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}

		if(getValues() == null || getValues().isEmpty() || !getValues().containsKey(pValue)) {
			return getEmptyValue();
		}

		try {
			return Boolean.valueOf( getValues().get(pValue) );
		} catch (Exception e) {
			logError("FAILED wrap ["+pValue+"] to boolean ");
		}

		return getEmptyValue();
	}
}
