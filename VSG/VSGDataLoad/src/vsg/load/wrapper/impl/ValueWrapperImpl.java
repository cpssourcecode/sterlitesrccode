package vsg.load.wrapper.impl;

import vsg.load.tools.ILoggerTools;
import vsg.load.wrapper.IValueWrapper;

/**
 * @author Dmitry Golubev
 * @param <WRAPPED_TYPE> template class
 */
public abstract class ValueWrapperImpl<WRAPPED_TYPE> implements IValueWrapper<WRAPPED_TYPE> {

	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;
	/**
	 * allowed null flag
	 */
	private boolean mAllowNull = false;
	/**
	 * empty value
	 */
	private WRAPPED_TYPE mEmptyValue = null;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return allow null
	 * @return allow null
	 */
	public boolean isAllowNull() {
		return mAllowNull;
	}

	/**
	 * init allow null
	 * @param pAllowNull  allow null
	 */
	public void setAllowNull(boolean pAllowNull) {
		mAllowNull = pAllowNull;
	}

	/**
	 * return logger tools
	 * @return logger tools
	 */
	public ILoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	/**
	 * init logger tools
	 * @param pLoggerTools logger tools
	 */
	public void setLoggerTools(ILoggerTools pLoggerTools) {
		mLoggerTools = pLoggerTools;
	}

	/**
	 * return empty value
	 * @return empty value
	 */
	public WRAPPED_TYPE getEmptyValue() {
		return mEmptyValue;
	}

	/**
	 * init empty value
	 * @param pEmptyValue empty value
	 */
	public void setEmptyValue(WRAPPED_TYPE pEmptyValue) {
		mEmptyValue = pEmptyValue;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * log error message
	 * @param pErr error message
	 */
	protected void logError(String pErr) {
		getLoggerTools().logError(pErr);
	}

	/**
	 * log error exception
	 * @param pErr error exception
	 */
	protected void logError(Exception pErr) {
		getLoggerTools().logError(pErr.toString());
	}

	/**
	 * @param pValue string to trim
	 * @return trimmed string
	 */
	protected String getTrimmedValue(String pValue) {
		if(pValue == null) {
			return null;
		} else {
			return pValue.trim();
		}
	}
}
