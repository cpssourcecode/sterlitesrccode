package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class IntegerWrapper extends ValueWrapperImpl<Integer> {

	/**
	 * return empty value
	 * @return empty value
	 */
	@Override
	public Integer getEmptyValue() {
		return 0;
	}

	/**
	 * convert string to integer
	 * @param pValueObj string integer presentation
	 * @return integer
	 */
	@Override
	public Integer getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(isAllowNull() && StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}

		try {
			return Integer.parseInt(pValue);
		} catch (Exception e) {
			logError("FAILED wrap ["+pValue+"] to integer ");
		}
		return getEmptyValue();
	}
}
