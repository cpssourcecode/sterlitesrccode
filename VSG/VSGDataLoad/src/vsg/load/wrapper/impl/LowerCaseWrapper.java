package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class LowerCaseWrapper extends ValueWrapperImpl {
	/**
	 * return lower case value
	 * @param pValueObj passed value
	 * @return lower case value
	 */
	@Override
	public String getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if ( !StringUtils.isBlank(pValue) ) {
			return StringUtils.toLowerCase(pValue);
		}
		return pValue;
	}
}
