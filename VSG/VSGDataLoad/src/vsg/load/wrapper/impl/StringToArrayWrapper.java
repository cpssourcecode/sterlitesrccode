package vsg.load.wrapper.impl;

/**
 * @author Dmitry Golubev
 */
public class StringToArrayWrapper extends ValueWrapperImpl<String[]> {
	/**
	 * return array of string
	 * @param pValueObj passed value
	 * @return array of string
	 */
	@Override
	public String[] getValue(Object pValueObj) {
		return new String[] { getTrimmedValue( (String) pValueObj ) };
	}
}
