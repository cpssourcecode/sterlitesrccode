package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

import static vsg.constants.VSGConstants.MAP_KEY_VALUE_DELIMITER;
import static vsg.constants.VSGConstants.MAP_PAIR_DELIMITER;

import java.util.*;


/**
 * @author Andy Porter
 */
public class StringsMapWrapper extends ValueWrapperImpl<Map<String, String>> {

	/**
	 * return map of strings
	 *
	 * @param pValueObj passed value
	 * @return map of strings
	 */
	@Override
	public Map<String, String> getValue(Object pValueObj) {
		String stringMap = null;
		Map<String, String> resultValues = null;
		if (pValueObj instanceof String) {
			stringMap = (String) pValueObj;
		}

		if (null != stringMap) {
			resultValues = new HashMap<String, String>();
			StringTokenizer st = new StringTokenizer(stringMap, MAP_PAIR_DELIMITER);
			while (st.hasMoreTokens()) {
				String keyValuePair = st.nextToken();
				if (!StringUtils.isBlank(keyValuePair)) {
					if (getLoggerTools().isLoggingDebug()) {
						getLoggerTools().logDebug("keyValuePair=" + keyValuePair);
					}

					StringTokenizer pairTokenize = new StringTokenizer(keyValuePair, MAP_KEY_VALUE_DELIMITER);
					String key = null;
					String value = null;
					if (pairTokenize.hasMoreTokens()) {
						key = pairTokenize.nextToken();
					}
					if (pairTokenize.hasMoreTokens()) {
						value = pairTokenize.nextToken();
					}
					if ((null != key) && (null != value)) {
						resultValues.put(key, value);
					}
				} else {
					getLoggerTools().logError("keyValuePair=blank!");
				}
			}

		}
		return resultValues;
	}
}
