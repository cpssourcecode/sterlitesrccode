package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;

/**
 * @author Dmitry Golubev
 */
public class RepositoryWrapper extends ValueWrapperImpl<RepositoryRepositoryValue> {

	/**
	 * item descriptor name
	 */
	private String mItemDescriptor;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return item descriptor
	 * @return item descriptor
	 */
	public String getItemDescriptor() {
		return mItemDescriptor;
	}

	/**
	 * item descriptor
	 * @param pItemDescriptor item descriptor
	 */
	public void setItemDescriptor(String pItemDescriptor) {
		mItemDescriptor = pItemDescriptor;
	}

	/**
	 * return empty value
	 * @return empty value
	 */
	@Override
	public RepositoryRepositoryValue getEmptyValue() {
		return null;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return repository wrapper
	 *
	 * @param pValueObj input value
	 * @return repository wrapper
	 */
	@Override
	public RepositoryRepositoryValue getValue(Object pValueObj) {
		String pValue = getTrimmedValue( (String) pValueObj );
		if(isAllowNull() && StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}

		try {
			return initWrappedValue(pValue);
		} catch (Exception e) {
			logError("FAILED wrap ["+pValue+"] to repository wrapper.");
		}
		return getEmptyValue();
	}

	/**
	 * create instance repository wrapper
	 *
	 * @param pValue input value
	 * @return repository wrapper
	 */
	protected RepositoryRepositoryValue initWrappedValue(String pValue) {
		return new RepositoryRepositoryValue(pValue, getItemDescriptor());
	}
}
