package vsg.commerce.endeca.index;

import vsg.endeca.EndecaConstants;
import atg.nucleus.GenericService;

/**
 * Default implementation of <code>void triggerIndexing(String pIndexingRequester, int pIndexingLev)</code> method
 * 
 * @author Kate Koshman
 */
public abstract class IndexingTriggerImpl extends GenericService implements IIndexingTrigger {
	
	/** The Triggering messages is enabled. */
	private boolean mEnabled = true;
	
	/**
	 * Checks if is enabled.
	 * 
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets the enabled.
	 * 
	 * @param pEnabled
	 *            the new enabled
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
	
	@Override
	public void triggerIndexing(String pIndexingRequester, int pIndexingLev) {
		triggerIndexing(pIndexingRequester, EndecaConstants.INDEX_BASELINE_LEVEL == pIndexingLev);
	}
}
