package vsg.commerce.endeca.index;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import atg.epub.search.indexing.SynchronizationInvokerProxy;
import atg.search.SearchAdminException;
import atg.search.core.RemoteSynchronizationInvokerService;

/**
 * This class use remote synchronization invoker to trigger indexing job.
 *
 * @author Kate Koshman
 */
public class SynchronizationInvokerTrigger extends IndexingTriggerImpl {

	private final static String PRODUCT_CATALOG_REPOSITORY = "/atg/commerce/catalog/ProductCatalog";
	private final static String PRODUCT_CATALOG_OUTPUT_CONFIG = "/atg/commerce/search/ProductCatalogOutputConfig";
	private final static String CATEGORY_TO_DIMENSION_OUTPUT_CONFIG = "/atg/commerce/endeca/index/CategoryToDimensionOutputConfig";


	/**
	 * The Remote synchronization invoker services.
	 */
	private RemoteSynchronizationInvokerService[] mRemoteSynchronizationInvokerServices;

	/**
	 * The CA target name - parameter to execute indexing job.
	 */
	private String mCATargetName = "Production";

	/**
	 * Gets the remote synchronization invoker services.
	 *
	 * @return the remote synchronization invoker services
	 */
	public RemoteSynchronizationInvokerService[] getRemoteSynchronizationInvokerServices() {
		return mRemoteSynchronizationInvokerServices;
	}

	/**
	 * Sets the remote synchronization invoker services.
	 *
	 * @param pRemoteSynchronizationInvokerServices the new remote synchronization invoker services
	 */
	public void setRemoteSynchronizationInvokerServices(
			RemoteSynchronizationInvokerService[] pRemoteSynchronizationInvokerServices) {
		mRemoteSynchronizationInvokerServices = pRemoteSynchronizationInvokerServices;
	}

	/**
	 * Gets the CA target name.
	 *
	 * @return the CA target name
	 */
	public String getCATargetName() {
		return mCATargetName;
	}

	/**
	 * Sets the CA target name.
	 *
	 * @param pCATargetName the new CA target name
	 */
	public void setCATargetName(String pCATargetName) {
		mCATargetName = pCATargetName;
	}


	@Override
	public void triggerIndexing(String pIndexingRequester, boolean pBaselineIndex) {
		if (isEnabled()) {
			SynchronizationInvokerProxy[] invokers = getSynchronizationInvokers();

			String indexingType = pBaselineIndex ? "bulk" : "incremental";
			Map<String, String> affectedItemTypes = new HashMap<String, String>();
			String types = pBaselineIndex ? "[catalog, category, product]" : "[product]";
			affectedItemTypes.put(PRODUCT_CATALOG_REPOSITORY, types);
			Set<String> bulkIndexingOCPaths = new HashSet<String>();
			Set<String> incrementalIndexingOCPaths = new HashSet<String>();
			if (pBaselineIndex) {
				bulkIndexingOCPaths.add(PRODUCT_CATALOG_OUTPUT_CONFIG);
				bulkIndexingOCPaths.add(CATEGORY_TO_DIMENSION_OUTPUT_CONFIG);
			} else {
				incrementalIndexingOCPaths.add(PRODUCT_CATALOG_OUTPUT_CONFIG);
			}
			Set<String> ancillaryIndexingOCPaths = new HashSet<String>();


			for (SynchronizationInvokerProxy invoker : invokers) {
				if (invoker != null) {
					try {
						invoker.onCADeploymentChanges(new HashSet<String>());
						invoker.executeIndexingJobsFor(getCATargetName(), indexingType, affectedItemTypes,
								bulkIndexingOCPaths, incrementalIndexingOCPaths, ancillaryIndexingOCPaths);
					} catch (SearchAdminException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		} else {
			vlogDebug("Triggerting indexing is not enabled.");
		}
	}

	/**
	 * Gets the synchronization invokers.
	 *
	 * @return the synchronization invokers
	 */
	protected SynchronizationInvokerProxy[] getSynchronizationInvokers() {
		RemoteSynchronizationInvokerService[] rsisArray = getRemoteSynchronizationInvokerServices();

		if (rsisArray == null) {
			return new SynchronizationInvokerProxy[0];
		}

		SynchronizationInvokerProxy[] result = new SynchronizationInvokerProxy[rsisArray.length];
		for (int i = 0; i < rsisArray.length; ++i) {
			RemoteSynchronizationInvokerService rsis = rsisArray[i];
			try {
				result[i] = proxySynchronizationInvoker(rsis);
			} catch (Exception ex) {
				vlogWarning("SynchronizationInvokerProxy was not instantiated: ", ex.getMessage());
			}
		}
		return result;
	}

	/**
	 * Proxy synchronization invoker.
	 *
	 * @param pRsis the remote synchronization invoker service
	 * @return the synchronization invoker proxy
	 */
	protected SynchronizationInvokerProxy proxySynchronizationInvoker(RemoteSynchronizationInvokerService pRsis) {
		return new SynchronizationInvokerProxy(pRsis);
	}

	/**
	 * Test trigger baseline index.
	 */
	public void testTriggerBaselineIndex() {
		triggerIndexing("testTriggerBaselineIndex", true);
	}

	/**
	 * Test trigger partial index.
	 */
	public void testTriggerPartialIndex() {
		triggerIndexing("testTriggerPartialIndex", false);
	}
}
