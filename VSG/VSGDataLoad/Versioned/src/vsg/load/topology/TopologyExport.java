package vsg.load.topology;

import atg.deployment.server.DeploymentServer;
import atg.nucleus.GenericService;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public class TopologyExport extends GenericService {
	/**
	 * path to export
	 */
	private String mPath = "/apps/load/";
	/**
	 * deployment server
	 */
	private DeploymentServer mDeploymentServer;

	//------------------------------------------------------------------------------------------------------------------

	public String getPath() {
		return mPath;
	}

	public void setPath(String pPath) {
		mPath = pPath;
	}

	public DeploymentServer getDeploymentServer() {
		return mDeploymentServer;
	}

	public void setDeploymentServer(DeploymentServer pDeploymentServer) {
		mDeploymentServer = pDeploymentServer;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * export toplogy to xml
	 */
	public void exportTopology() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HHmmss");
		File exportFile = new File(getPath()+"topology-"+sdf.format(new Date())+".xml");
		try {
			getDeploymentServer().exportTopologyToXML(new FileOutputStream(exportFile));
		} catch (Exception e) {
			logError(e);
		}
	}
}
