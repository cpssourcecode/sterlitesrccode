package vsg.load.deployment;

import atg.deployment.common.event.DeploymentEvent;
import atg.deployment.common.event.DeploymentEventListener;
import atg.deployment.server.DeploymentServer;
import atg.deployment.server.Target;
import vsg.load.loader.impl.ProjectRepositoryLoader;
import vsg.load.tools.impl.LoggerTools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Golubev
 */
public class ProjectDeploymentNotifier implements DeploymentEventListener {
	/**
	 * enable flag
	 */
	private boolean mEnabled = true;
	/**
	 * component name
	 */
	private static final String COMPONENT_NAME = "ProjectDeploymentNotifier";
	/**
	 * deployment is completed flag
	 */
	public static final int DEPLOYEMNT_COMPLETED = 0;
	/**
	 * deployment is failed flag
	 */
	public static final int DEPLOYEMNT_FAILED    = 1;
	/**
	 * deployment is canceled flag
	 */
	public static final int DEPLOYEMNT_CANCELED  = 2;
	/**
	 * deployment server
	 */
	private DeploymentServer mDeploymentServer;
	/**
	 * logger tools
	 */
	private LoggerTools mLoggerTools;
	/**
	 * loaders info
	 */
	private final LoadersSyncInfo mLoadersSyncInfo = new LoadersSyncInfo();

	/**
	 * this asset destination will be used as criterion to check if agent will be deployed
	 */
	private String mIncludeAssetDestination = "/atg/epub/file/ConfigFileSystem";

	//------------------------------------------------------------------------------------------------------------------

	public LoggerTools getLoggerTools() {
		return mLoggerTools;
	}

	public void setLoggerTools(LoggerTools pLoggerTools) {
		mLoggerTools = pLoggerTools;
	}

	public DeploymentServer getDeploymentServer() {
		return mDeploymentServer;
	}

	public void setDeploymentServer(DeploymentServer pDeploymentServer) {
		mDeploymentServer = pDeploymentServer;
	}

	private LoadersSyncInfo getSyncInfo() {
		return mLoadersSyncInfo;
	}

	public String getIncludeAssetDestination() {
		return mIncludeAssetDestination;
	}

	public void setIncludeAssetDestination(String pIncludeAssetDestination) {
		mIncludeAssetDestination = pIncludeAssetDestination;
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * register loader for notify
	 * @param pProjectId project identifier
	 * @param pLoader loader instance
	 */
	public void registerRequestForProjectDeployment(String pProjectId, ProjectRepositoryLoader pLoader) {
		synchronized (getSyncInfo()) {
			getSyncInfo().registerLoader(pProjectId, pLoader);
			if(!isEnabled()) {
				sendNotificationToLoader(pProjectId, DEPLOYEMNT_COMPLETED);
			}
		}
	}

	/**
	 * notify loader deployemnt with action
	 * @param pProjectId project identifier
	 * @param pAction action
	 */
	private void sendNotificationToLoader(String pProjectId, int pAction) {
		synchronized (getSyncInfo()) {
			ProjectRepositoryLoader loader = getSyncInfo().getLoader(pProjectId);
			if(loader!=null) {
				loader.notifyDeployFinished(pAction);
				getSyncInfo().unRegisterLoader(pProjectId);
			}
		}
	}

	/**
	 * notify loader deployemnt is completed
	 * @param pProjectId project identifier
	 * @param pTarget deployed target
	 */
	private void deploymentCompleted(String pProjectId, String pTarget) {
		if( getLoggerTools().isLoggingDebug() ) {
			getLoggerTools().logDebug(
					COMPONENT_NAME+": "+pProjectId+" project: COMPLETED on target: "+pTarget
			);
		}
		synchronized ( getSyncInfo() ) {
			getSyncInfo().unRegisterDeployedTarget( pProjectId, pTarget );
			if( getSyncInfo().isWaitingForOtherTargets( pProjectId ) ) {
				return;
			}
			sendNotificationToLoader(pProjectId, DEPLOYEMNT_COMPLETED);
		}
	}

	/**
	 * notify loader deployemnt is failed
	 * @param pProjectName project name
	 */
	private void deploymentFailed(String pProjectName) {
		if( getLoggerTools().isLoggingDebug() ) {
			getLoggerTools().logDebug(COMPONENT_NAME+": "+pProjectName+" deployment: FAILED.");
		}
		sendNotificationToLoader(pProjectName, DEPLOYEMNT_FAILED);
	}

	/**
	 * notify loader deployemnt is canceled
	 * @param pProjectName project name
	 */
	private void deploymentCancelled(String pProjectName) {
		if( getLoggerTools().isLoggingDebug() ) {
			getLoggerTools().logDebug(COMPONENT_NAME+": "+pProjectName+" deployment: CANCELED.");
		}
		sendNotificationToLoader(pProjectName, DEPLOYEMNT_CANCELED);
	}

	/**
	 * analyze recieved deployement event
	 * @param pDeploymentEvent recieved deployement event
	 */
	@Override
	public void deploymentEvent(DeploymentEvent pDeploymentEvent) {
		String registeredProjectId;
		StringBuilder projectIds = new StringBuilder();
		for(String deploymentProjectID : pDeploymentEvent.getDeploymentProjectIDs()) {
			projectIds.append(deploymentProjectID).append(", ");
		}
		getLoggerTools().logDebug(pDeploymentEvent.getTarget() + " " + projectIds + ": state - "+
				pDeploymentEvent.getNewState());

		if( ( registeredProjectId = isEventForRegisteredLoaders(pDeploymentEvent) ) != null ) {
			if(isDeploymentCompleted(pDeploymentEvent)) {
				deploymentCompleted(registeredProjectId, pDeploymentEvent.getTarget());
			}
			if(isDeploymentInErrorState(pDeploymentEvent)) {
				deploymentFailed(registeredProjectId);
			}
			if(isDeploymentCanceled(pDeploymentEvent)) {
				deploymentCancelled(registeredProjectId);
			}
		}
	}

	/**
	 * check if deployment event refers to registered for notify loaders
	 * @param pDeploymentEvent recieved deployement event
	 * @return deployment event refers to registered for notify loaders
	 */
	private String isEventForRegisteredLoaders(DeploymentEvent pDeploymentEvent) {
		boolean isIncremental = pDeploymentEvent.getDeploymentType() == DeploymentEvent.TYPE_INCREMENTAL;
		if ( isIncremental ) {
			synchronized (getSyncInfo()) {
				for(String deploymentProjectID : pDeploymentEvent.getDeploymentProjectIDs()) {
					if(getSyncInfo().isProjectRegistered(deploymentProjectID)) {
						return deploymentProjectID;
					}
				}
/*
				if(getSyncInfo().isProjectRegistered( deploymentProjectID )) {
					ProjectRepositorryLoader loader = getSyncInfo().getLoader( deploymentProjectID );
					getSyncInfo().reRegisterLoader(deploymentProjectID, deploymentID, loader);
					return deploymentID;
				}
				if(getSyncInfo().isProjectRegistered(deploymentID)) {
					return deploymentID;
				}
*/
			}
		}
		return null;
	}

	/**
	 * check if deployement get error state
	 * @param pDeploymentEvent recieved deployement event
	 * @return deployemnt is in error state
	 */
	private boolean isDeploymentInErrorState(DeploymentEvent pDeploymentEvent) {
		return pDeploymentEvent.getNewState() == DeploymentEvent.ERROR ||
				pDeploymentEvent.getNewState() == DeploymentEvent.ERROR_APPLY  ||
				pDeploymentEvent.getNewState() == DeploymentEvent.ERROR_APPLY_COMMITTED  ||
				pDeploymentEvent.getNewState() == DeploymentEvent.ERROR_PREPARE ;
	}

	/**
	 * A deployment has been finished successfully and now has a been deleted so that next deployment in
	 * the queue can proceed.
	 * @param pDeploymentEvent recieved deployement event
	 * @return deployment is successed
	 */
	private boolean isDeploymentCompleted(DeploymentEvent pDeploymentEvent) {
		return pDeploymentEvent.getOldState() == DeploymentEvent.DEPLOYMENT_COMPLETE &&
				pDeploymentEvent.getNewState() == DeploymentEvent.DEPLOYMENT_DELETED ;
	}

	/**
	 * A deployment has been canceled  and now has a been deleted so that next deployment in the queue can proceed.
	 * @param pDeploymentEvent recieved deployement event
	 * @return deployment is successed
	 */
	private boolean isDeploymentCanceled(DeploymentEvent pDeploymentEvent) {
		return pDeploymentEvent.getOldState() != DeploymentEvent.DEPLOYMENT_COMPLETE &&
				pDeploymentEvent.getNewState() == DeploymentEvent.DEPLOYMENT_DELETED ;
	}

	/**
	 * Help class to keep info about loaders to notify their deployment status
	 */
	private class LoadersSyncInfo {
		/**
		 * map of registerd loaders for deployment notify
		 */
		private Map<String, ProjectRepositoryLoader> mLoadesQueue = new HashMap<String, ProjectRepositoryLoader>();
		/**
		 * map of targets to be deployed
		 */
		private Map<String, List<String>> mProjectTargets = new HashMap<String, List<String>>();

		/**
		 * put loader info to notify queue
		 * @param pProjectName project
		 * @param pLoader loader
		 */
		public void registerLoader(String pProjectName, ProjectRepositoryLoader pLoader) {
			mLoadesQueue.put(pProjectName, pLoader);
			List<String> targets = new ArrayList<String>();
			for(Target target : getDeploymentServer().getTargets()) {
				targets.add(target.getName());
/*
				for(AgentRef agent : target.getAgents()) {
					String[] includeDestinations = agent.getIncludeAssetDestinations();
					if(includeDestinations!=null) {
						for(String includeDestination : includeDestinations) {
							if(includeDestination.equals(getIncludeAssetDestination())) {
								targets.add(target.getName());
								break;
							}
						}
					}
				}
*/
			}
			mProjectTargets.put(pProjectName, targets);
		}

		/**
		 * we re-register loader info to contain deployment id
		 * @param pProjectName project
		 * @param pDeploymentId deployment id
		 * @param pLoader loader
		 */
		public void reRegisterLoader(String pProjectName, String pDeploymentId, ProjectRepositoryLoader pLoader) {
/*
			mLoadesQueue.remove( pProjectName );
			mLoadesQueue.put( pDeploymentId, pLoader );
			List<String> targets = new ArrayList<String>();
			for(Target target : getDeploymentServer().getTargets()) {
				for(AgentRef agent : target.getAgents()) {
					String[] includeDestinations = agent.getIncludeAssetDestinations();
					if(includeDestinations!=null) {
						for(String includeDestination : includeDestinations) {
							if(includeDestination.equals(getIncludeAssetDestination())) {
								targets.add(target.getName());
								break;
							}
						}
					}
				}
			}
			mProjectTargets.put(pProjectName, targets);
*/
		}

		/**
		 * unregister project from sync info
		 * @param pProjectDeploymentId project deployment id
		 */
		public void unRegisterLoader(String pProjectDeploymentId) {
			mLoadesQueue.remove(pProjectDeploymentId);
			mProjectTargets.remove(pProjectDeploymentId);
		}

		/**
		 * retrieve loader by project identifier
		 * @param pProjectId project identifier
		 * @return loader if exists such in map
		 */
		public ProjectRepositoryLoader getLoader(String pProjectId) {
			return mLoadesQueue.get( pProjectId );
		}

		/**
		 * check if project should be deployed on other targets
		 * @param pProjectId project
		 * @return project should be deployed on other targets
		 */
		public boolean isWaitingForOtherTargets( String pProjectId ) {
			List<String> targets = mProjectTargets.get( pProjectId );
			if(getLoggerTools().isLoggingDebug() && targets!=null) {
				StringBuilder leftTargets = new StringBuilder();
				for(String target : targets) {
					leftTargets.append(target).append(". ");
				}
				getLoggerTools().logDebug("Left targets: ["+leftTargets+"]");
			}
			return targets!=null && targets.size() > 0;
		}

		/**
		 * unregister project deployed target
		 * @param pProjectId project identifier
		 * @param pTarget target
		 */
		public void unRegisterDeployedTarget( String pProjectId, String pTarget) {
			List<String> targets = mProjectTargets.get( pProjectId );
			if( targets!=null ) {
				targets.remove( pTarget );
			}
		}

		/**
		 * check if there is registered loader
		 * @param pProjectId loader project identifier
		 * @return is there registered loader
		 */
		public boolean isProjectRegistered(String pProjectId) {
			return mLoadesQueue.containsKey( pProjectId );
		}
	}
}
