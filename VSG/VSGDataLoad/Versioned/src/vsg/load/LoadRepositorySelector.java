package vsg.load;

/**
 * @author Dmitry Golubev
 */
public class LoadRepositorySelector {
	/**
	 * flag to use un versioned repositories in data load on versioned loaders
	 */
	private boolean mUseUnVersionedRepositories = true;

	public boolean isUseUnVersionedRepositories() {
		return mUseUnVersionedRepositories;
	}

	public void setUseUnVersionedRepositories(boolean pUseUnVersionedRepositories) {
		mUseUnVersionedRepositories = pUseUnVersionedRepositories;
	}
}
