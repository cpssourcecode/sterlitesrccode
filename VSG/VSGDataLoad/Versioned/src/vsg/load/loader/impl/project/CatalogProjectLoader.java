package vsg.load.loader.impl.project;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import vsg.load.loader.impl.helper.CatalogLoaderHelper;
import vsg.load.loader.impl.ProjectRepositoryLoader;

/**
 * @author Dmitry Golubev
 */
public class CatalogProjectLoader extends ProjectRepositoryLoader {
	/**
	 * catalog loader helper
	 */
	private CatalogLoaderHelper mHelper;

	/**
	 * return or init on first demand catalog loader helper object
	 * @return catalog loader helper
	 */
	private CatalogLoaderHelper getHelper() {
		if(mHelper == null) {
			mHelper = new CatalogLoaderHelper(getLoaderName(), getLoggerTools());
		}
		return mHelper;
	}

	/**
	 * add custom logic for root categories initialization
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pFieldName field name
	 * @param pFieldValue field value
	 * @throws Exception if exception occurs
	 */
	@Override
	protected void initPropertyValue(Repository pRepository, MutableRepositoryItem pItem, String pFieldName,
									 Object pFieldValue)
			throws Exception {
		getHelper().initPropertyValue(this, pRepository, pItem, pFieldName, pFieldValue);
	}
}
