package vsg.load.loader.impl;

import atg.deployment.common.event.DeploymentEvent;

/**
 * This interface used to escape indexing after deployment.
 *
 * @author Dmitry Golubev
 */
public interface IDeploymentSource {
	/**
	 * Checks if deployment initiated by this object.
	 *
	 * @param pEvent
	 *            the deployment event
	 * @return true, if deployment initiated by this object
	 */
	public boolean ownDeployment(DeploymentEvent pEvent);
}
