package vsg.load.loader.impl;

import atg.adapter.gsa.GSARepository;
import atg.repository.*;
import vsg.load.loader.Record;
import vsg.load.loader.RecordValidationInfo;

/**
 * @author Dmitry Golubev
 */
public class ProjectRelationsRepositoryLoader extends ProjectRepositoryLoader {

	/**
	 * check for insert record processing on repository item
	 * @param pRecord record to be inserted
	 * @param pItem created repository item
	 * @return validation passed
	 */
	@Override
	protected boolean insertCheckItem(Record pRecord, RepositoryItem pItem) {
		if(pItem == null) {
			getStatistics().addErrorRecord( new RecordValidationInfo(pRecord,
					"Record is not found in DB to init relationships"));
			return false;
		}
		return true;
	}

	/**
	 * get RepositoryItem for insert
	 * @param pRecord record to be inserted
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor name
	 * @return created repository item
	 * @throws RepositoryException if error occurs
	 */
	@Override
	protected RepositoryItem insertCreateItem(Record pRecord, Repository pRepository, String pItemDescriptor)
			throws RepositoryException {
		return ((GSARepository)pRepository).getItemForUpdate(pRecord.getId(), pItemDescriptor);
	}

	/**
	 * process created repository item
	 * @param pRepository items repository
	 * @param pItem created repository item
	 * @throws RepositoryException if error occurs
	 */
	@Override
	protected void insertProceedItem(Repository pRepository, RepositoryItem pItem) throws RepositoryException {
		((MutableRepository)pRepository).updateItem((MutableRepositoryItem) pItem);
		getStatistics().incrementInsert();
	}

	/**
	 * override delete to prevent delete item - only change relations
	 * @param pRecord record which relations should be delete
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor name
	 * @throws Exception if error occurs
	 */
	@Override
	protected void delete(Record pRecord, Repository pRepository, String pItemDescriptor) throws Exception {
		// super.delete(pRecord, pRepository, pItemDescriptor);
		RepositoryItem item = getRepositoryItem(pRecord, pRepository, pItemDescriptor);
		if(updateCheckItem(pRecord, item)) {
			initRepositoryItem(pRecord, getRepository(), (MutableRepositoryItem) item);
			getRepository().updateItem((MutableRepositoryItem) item);
			getStatistics().incrementDelete();
			if(pRecord.getSubRecords()!=null) {
				for(String subRecordName : pRecord.getSubRecords().keySet()) {
					delete(pRecord.getSubRecords().get(subRecordName), pRepository, subRecordName);
				}
			}
		}
	}
}
