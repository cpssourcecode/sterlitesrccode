package vsg.load.loader.impl;

import atg.adapter.gsa.GSARepository;
import atg.repository.*;
import vsg.load.loader.Record;
import vsg.load.loader.RecordValidationInfo;

/**
 * @author Dmitry Golubev
 */
public class WorkspaceRelationsRepositoryLoader extends WorkspaceRepositoryLoader {
	/**
	 * check for insert record processing on repository item
	 * @param pRecord record to be inserted
	 * @param pItem created repository item
	 * @return validation passed
	 */
	@Override
	protected boolean insertCheckItem(Record pRecord, RepositoryItem pItem) {
		if(pItem == null) {
			getStatistics().addErrorRecord( new RecordValidationInfo(pRecord,
					"Record is not found in DB to init relationships"));
			return false;
		}
		return true;
	}

	/**
	 * get RepositoryItem for insert
	 * @param pRecord record to be inserted
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor name
	 * @return created repository item
	 * @throws RepositoryException if error occurs
	 */
	@Override
	protected RepositoryItem insertCreateItem(Record pRecord, Repository pRepository, String pItemDescriptor)
			throws RepositoryException {
		return ((GSARepository)pRepository).getItemForUpdate(pRecord.getId(), pItemDescriptor);
	}

	/**
	 * process created repository item
	 * @param pRepository items repository
	 * @param pItem created repository item
	 * @throws RepositoryException if error occurs
	 */
	@Override
	protected void insertProceedItem(Repository pRepository, RepositoryItem pItem) throws RepositoryException {
		((MutableRepository)pRepository).updateItem((MutableRepositoryItem) pItem);
		getStatistics().incrementInsert();
	}

}
