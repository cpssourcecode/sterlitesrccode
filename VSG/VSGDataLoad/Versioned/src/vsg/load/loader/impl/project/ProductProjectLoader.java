package vsg.load.loader.impl.project;

import atg.repository.RepositoryItem;
import vsg.load.loader.impl.helper.ProductLoaderHelper;
import vsg.load.loader.Record;
import vsg.load.loader.impl.ProjectRepositoryLoader;

import java.util.HashMap;

/**
 * @author Dmitry Golubev
 */
public class ProductProjectLoader extends ProjectRepositoryLoader {

	/**
	 * catalog loader helper
	 */
	private ProductLoaderHelper mHelper;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return catalog loader helper
	 * @return catalog loader helper
	 */
	private ProductLoaderHelper getHelper() {
		if(mHelper == null) {
			mHelper = new ProductLoaderHelper(getLoaderName(), getLoggerTools());
		}
		return mHelper;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * insert product and sku record, for sub record sku id is init
	 * @param pRecord record to be inserted
	 */
	@Override
	public void insert(Record pRecord) {
		getHelper().modifyRecordOnInsert(pRecord);
		super.insert(pRecord);
	}

	/**
	 * create relationship between product and sku repository items
	 * @param pRecord record to be inserted
	 * @param pInsertedItems map of inserted intes
	 */
	@Override
	protected void postInsert(Record pRecord, HashMap<String, RepositoryItem> pInsertedItems) {
		getHelper().postInsert(this, pRecord, pInsertedItems);
	}
}
