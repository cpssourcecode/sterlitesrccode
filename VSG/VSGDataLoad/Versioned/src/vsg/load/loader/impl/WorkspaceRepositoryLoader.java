package vsg.load.loader.impl;

import atg.adapter.gsa.GSARepository;
import atg.adapter.version.VersionRepository;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.Nucleus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.search.indexing.IncrementalLoader;
import atg.versionmanager.VersionManager;
import atg.versionmanager.WorkingContext;
import atg.versionmanager.Workspace;
import atg.versionmanager.exceptions.VersionException;
import vsg.load.LoadRepositorySelector;
import vsg.load.connection.impl.Connector;
import vsg.load.loader.Record;
import vsg.load.loader.RecordValidationInfo;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class WorkspaceRepositoryLoader extends RepositoryLoader {
	/**
	 * flag pushed workspace
	 */
	private boolean mPushedWorkspace = false;
	/**
	 * workspace
	 */
	private Workspace mWorkspace;
	/**
	 * set of unversioned repositories
	 */
	private Set<Repository> mUnVersionedRepositories;
	/**
	 * flag to re-init workspace
	 */
	private boolean mReInitWorkspace;
	/**
	 * time start
	 */
	private long mTimeStart = 0;
	/**
	 * component to check if we need to load data to unversioned repositories
	 */
	private LoadRepositorySelector mLoadRepositorySelector;
	/**
	 * connector to prod
	 */
	private Connector mConnectorProd;

	// -----------------------------------------------------------------------------------------------------------------


	public LoadRepositorySelector getLoadRepositorySelector() {
		return mLoadRepositorySelector;
	}

	public void setLoadRepositorySelector(LoadRepositorySelector pLoadRepositorySelector) {
		mLoadRepositorySelector = pLoadRepositorySelector;
	}

	public Connector getConnectorProd() {
		return mConnectorProd;
	}

	public void setConnectorProd(Connector pConnectorProd) {
		mConnectorProd = pConnectorProd;
	}

	public Set<Repository> getUnVersionedRepositories() {
		return mUnVersionedRepositories;
	}

	/**
	 * init unversioned repositories with nucleus
	 * @param pUnVersionedRepositories unversioned repositories component paths
	 */
	public void setUnVersionedRepositories(Set pUnVersionedRepositories) {
		if(pUnVersionedRepositories == null) {
			mUnVersionedRepositories= null;
		} else {
			mUnVersionedRepositories = new HashSet<Repository>();
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			for(Object repositoryName : pUnVersionedRepositories) {
				Repository repository = (Repository) nucleus.resolveName( repositoryName.toString() );
				mUnVersionedRepositories.add(repository);
			}
		}
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * create repository item and put record to processed map of records<br/>
	 * if use unversioned repositories - will run create on those repositories
	 * @param pRecord record to be inserted
	 */
	@Override
	public void insert(Record pRecord) {
		super.insert(pRecord);
		if(getUnVersionedRepositories() != null && getLoadRepositorySelector().isUseUnVersionedRepositories()) {
			try {
				for(Repository unVersionedRepository : getUnVersionedRepositories()) {
					super.insert(pRecord, unVersionedRepository);
				}
			} catch (Exception e) {
				getStatistics().addErrorRecord( new RecordValidationInfo(pRecord, e.getMessage()) );
				logError(e.toString());
			}
		}
	}

	/**
	 * workspace loader is created only for initial load
	 * @param pRecord record to be updated
	 */
	@Override
	public void update(Record pRecord) {
		throw new RuntimeException("WorkspaceRepositoryLoader is used only once for initial file load.");
	}

	/**
	 * workspace loader is created only for initial load
	 * @param pRecord record to be deleted
	 */
	@Override
	public void delete(Record pRecord) {
		throw new RuntimeException("WorkspaceRepositoryLoader is used only once for initial file load.");
	}

	/**
	 * on transaction end pop workspace and flags to re-init it
	 * @param pDemarcation transaction demarcation
	 */
	@Override
	protected void transactionEnd(TransactionDemarcation pDemarcation) {
		super.transactionEnd(pDemarcation);
		if(!isImportFailed()) {
			if( getStatistics().getCountProceeded() % ( getConnector().getLimitPerTransaction()*10 ) == 0 ) {
				popDevelopment();
				mReInitWorkspace = true;
			}
		}
	}

	/**
	 * on transaction start create and push workspace
	 * @param pDemarcation transaction demarcation
	 * @throws TransactionDemarcationException if error occurs
	 */
	@Override
	protected void transactionStart(TransactionDemarcation pDemarcation) throws TransactionDemarcationException {
		if( mReInitWorkspace ) {
			try {
				pushDevelopment();
			} catch (Exception e) {
				exceptionCatch(e);
			}
			mReInitWorkspace = false;
		}

		super.transactionStart(pDemarcation);
	}

	/**
	 * extend pre-import records to disable IncrementalLoader
	 */
	@Override
	protected void preImportRecords() {
		super.preImportRecords();
		disableIncrementLoader();
		mWorkspace = null;
		mReInitWorkspace = true;
	}

	/**
	 * extend post-import to pop workspace
	 */
	@Override
	protected void postImportRecords() {
		super.postImportRecords();
		popDevelopment();
	}

	/**
	 * create new workspace and push it
	 * @throws RepositoryException if exception occurs
	 */
	private void pushDevelopment()
			throws RepositoryException {
		mPushedWorkspace = false;
		mWorkspace = getWorkspace("workspaceName " + Calendar.getInstance().getTimeInMillis());
		WorkingContext.pushDevelopmentLine(mWorkspace);
		mPushedWorkspace = true;
		mTimeStart = Calendar.getInstance().getTimeInMillis();
		invalidateCaches();
	}

	/**
	 * pop development
	 */
	private void popDevelopment() {
		checkInAll();
		long timeEnd = Calendar.getInstance().getTimeInMillis();
		getLoggerTools().logDebug(getTime("Workspace processing: ", timeEnd - mTimeStart));
	}

	/**
	 * call check in for workspace
	 */
	private void checkInAll() {
		if(!isImportFailed() && mPushedWorkspace) {
			try {
				if (mWorkspace.getAssets().size() > 0) {
					mWorkspace.checkInAll("import", "Commit " + getLoaderName());
				}
			} catch (Exception e) {
				exceptionCatch(e);
			} finally {
				WorkingContext.popDevelopmentLine();
			}
		}
	}

	/**
	 * create workspace
	 * @param pWorkspaceId workspace id
	 * @param pBranchId branch id
	 * @return created workspace
	 */
	private Workspace createWorkspace(String pWorkspaceId, String pBranchId) {
		if (StringUtils.isBlank(pWorkspaceId) || StringUtils.isBlank(pBranchId))
			throw new RuntimeException("Workspace and parent branch are required attributes");
		VersionManager vm = getVersionManager();
		atg.versionmanager.Branch parent = null;
		try {
			parent = vm.getBranchByName(pBranchId);
		} catch (VersionException ve) {
			throw new RuntimeException("Unable to create workspace: invalid parent branch: "+pBranchId, ve);
		}
		if (parent == null)
			throw new RuntimeException("Invalid parent branch: "+pBranchId);
		try {
			return vm.createWorkspace(pWorkspaceId, parent);
		} catch (VersionException ve) {
			throw new RuntimeException("Unable to create workspace: name already in use: "+pWorkspaceId, ve);
		}
	}

	/**
	 * get workspace by name
	 * @param pWorkspaceName workspace name
	 * @return workspace
	 */
	private Workspace getWorkspace(String pWorkspaceName) {
		VersionManager vm = getVersionManager();
		Workspace ws = null;
		try {
			ws = vm.getWorkspaceByName(pWorkspaceName);
		} catch (VersionException ve) {
			throw new RuntimeException("Unable to get workspace: "+pWorkspaceName, ve);
		}
		if (ws != null && ws.isCheckedIn())
			throw new RuntimeException("Attempted to use checked-in workspace: "+ws.getName());
		if (ws == null)
			ws = createWorkspace(pWorkspaceName, "main");
		return ws;
	}

	/**
	 * invalidate caches on repositories
	 */
	private void invalidateCaches() {
		( (GSARepository) getRepository()).invalidateCaches();
		if(getUnVersionedRepositories() != null && getLoadRepositorySelector().isUseUnVersionedRepositories()) {
			for(Repository unVersionedRepository : getUnVersionedRepositories()) {
				( (GSARepository)unVersionedRepository ).invalidateCaches();
			}
		}
	}

	/**
	 * get version manager from repository
	 * @return version repository
	 */
	private VersionManager getVersionManager() {
		VersionRepository repository = (VersionRepository) getConnector().getRepository();
		VersionManager vm = repository.getVersionManager();
		if (vm == null)
			throw new RuntimeException("VersionManager is not set for repository: "+repository.getName());
		return vm;
	}

	/**
	 * disable incremental loader
	 */
	private void disableIncrementLoader() {
		String incrementLoaderName = "/atg/search/repository/IncrementalLoader";
		IncrementalLoader loader = (IncrementalLoader) Nucleus.getGlobalNucleus().resolveName(incrementLoaderName);
		loader.setRecordChangeEvents(false);
	}
}
