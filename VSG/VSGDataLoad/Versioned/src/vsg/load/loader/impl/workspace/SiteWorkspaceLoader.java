package vsg.load.loader.impl.workspace;

import atg.adapter.gsa.GSARepository;
import atg.repository.*;
import vsg.load.loader.impl.helper.BaseCatalogLoaderHelper;
import vsg.load.connection.IConnector;
import vsg.load.loader.impl.WorkspaceRepositoryLoader;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Golubev
 */
public class SiteWorkspaceLoader extends WorkspaceRepositoryLoader {

	/**
	 * catalog loader helper
	 */
	private BaseCatalogLoaderHelper mHelper;

	/**
	 * return or init on first demand catalog loader helper object
	 * @return catalog loader helper
	 */
	private BaseCatalogLoaderHelper getHelper() {
		if(mHelper == null) {
			mHelper = new BaseCatalogLoaderHelper(getLoaderName(), getLoggerTools());
		}
		return mHelper;
	}

	/**
	 * ProductCatalog connector
	 */
	private IConnector mProductCatalogConnector;

	//------------------------------------------------------------------------------------------------------------------

	public IConnector getProductCatalogConnector() {
		return mProductCatalogConnector;
	}

	public void setProductCatalogConnector(IConnector pProductCatalogConnector) {
		mProductCatalogConnector = pProductCatalogConnector;
	}


	//------------------------------------------------------------------------------------------------------------------


	@Override
	protected void insertProceedItem(Repository pRepository, RepositoryItem pItem) throws RepositoryException {
		((MutableRepositoryItem)pItem).setPropertyValue("enabled", Boolean.TRUE);
		super.insertProceedItem(pRepository, pItem);
	}

	/**
	 * add custom logic for root categories initialization
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pFieldName field name
	 * @param pFieldValue field value
	 * @throws Exception if exception occurs
	 */
	@Override
	protected void initPropertyValue(Repository pRepository, MutableRepositoryItem pItem, String pFieldName,
									 Object pFieldValue)
			throws Exception {
		if("subCatalogs".equals(pFieldName)) {
			String[] catalogIds = ((String) pFieldValue).split("\\|");

			String catalogFolderId = "cf_"+pItem.getRepositoryId();
			MutableRepository productCatalog = (MutableRepository) getProductCatalogConnector().getRepository();
			MutableRepositoryItem catalogFolder = productCatalog.createItem(catalogFolderId, "catalogFolder");
			catalogFolder.setPropertyValue("name", "Folder for "+pItem.getPropertyValue("name"));

			Set<String> siteIds = new HashSet<String>();
			siteIds.add( pItem.getRepositoryId() );
			setPropertyValue(catalogFolder, "siteIds", siteIds);

			List<RepositoryItem> childItems = new ArrayList<RepositoryItem>();
			for(String catalogId : catalogIds) {
				childItems.add(productCatalog.getItem(catalogId, "catalog"));
			}
			setPropertyValue(catalogFolder, "childItems", childItems);

			((GSARepository)productCatalog).addItemOnCommit(catalogFolder);
			setPropertyValue(pItem, "defaultCatalog", childItems.get(0));
		} else {
			super.initPropertyValue(pRepository, pItem, pFieldName, pFieldValue);
		}

	}
}
