package vsg.load.loader.impl.workspace;

import atg.repository.*;
import vsg.load.loader.impl.helper.ProductLoaderHelper;
import vsg.load.loader.Record;
import vsg.load.loader.impl.WorkspaceRepositoryLoader;

import java.util.*;

/**
 * @author Dmitry Golubev
 */
public class ProductWorkspaceLoader extends WorkspaceRepositoryLoader {

	/**
	 * catalog loader helper
	 */
	private ProductLoaderHelper mHelper;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init if not initialized helper
	 * @return catalog loader helper
	 */
	private ProductLoaderHelper getHelper() {
		if(mHelper == null) {
			mHelper = new ProductLoaderHelper(getLoaderName(), getLoggerTools());
		}
		return mHelper;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * on workspace initial loader we consider that there is no data, so check is un necessary
	 * @param pRecord processing record
	 * @param pRepository items repository
	 * @param pItemDescriptor item descriptor
	 * @return null
	 * @throws RepositoryException if exception occures
	 */
	@Override
	protected RepositoryItem getRepositoryItem(Record pRecord, Repository pRepository,
											   String pItemDescriptor) throws RepositoryException {
		return null;
	}

	/**
	 * we consider that db is empty - no need to check
	 * @param pRecord record to be inserted
	 * @param pItem created repository item
	 * @return success flag to insert record
	 */
	@Override
	protected boolean insertCheckItem(Record pRecord, RepositoryItem pItem) {
		return true;
	}

	/**
	 * insert product and sku record, for sub record sku id is init
	 * @param pRecord record to be inserted
	 */
	@Override
	public void insert(Record pRecord) {
		getHelper().modifyRecordOnInsert(pRecord);
		super.insert(pRecord);
	}

	/**
	 * create relationship between product and sku repository items
	 * @param pRecord record to be inserted
	 * @param pInsertedItems map of inserted intes
	 */
	@Override
	protected void postInsert(Record pRecord, HashMap<String, RepositoryItem> pInsertedItems) {
		getHelper().postInsert(this, pRecord, pInsertedItems);
	}
}
