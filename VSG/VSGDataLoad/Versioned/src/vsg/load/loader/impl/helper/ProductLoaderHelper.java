package vsg.load.loader.impl.helper;

import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import vsg.load.loader.Record;
import vsg.load.loader.RecordValidationInfo;
import vsg.load.loader.impl.RepositoryLoader;
import vsg.load.tools.ILoggerTools;

import java.util.*;

/**
 * @author Dmitry Golubev
 */
public class ProductLoaderHelper extends BaseCatalogLoaderHelper {
	/**
	 * constructor
	 * @param pLoaderName  loader name
	 * @param pLoggerTools logger tools
	 */
	public ProductLoaderHelper(String pLoaderName, ILoggerTools pLoggerTools) {
		super(pLoaderName, pLoggerTools);
	}

	/**
	 * modify product and sku records with id and display name
	 * @param pRecord record to be inserted
	 */
	public void modifyRecordOnInsert(Record pRecord) {
		Record skuRecord = pRecord.getSubRecords().get( "sku" );
		if(skuRecord != null) {
			skuRecord.setId(pRecord.getId());
			skuRecord.getValues().put("displayName", pRecord.getValue("displayName"));
		}
	}

	/**
	 * init repository item with property value
	 * @param pRepositoryLoader repository loader
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pProperty property name
	 * @param pPropertyValue property value
	 * @throws Exception if error occurs
	 */
	public void initPropertyValue(RepositoryLoader pRepositoryLoader, Repository pRepository,
									 MutableRepositoryItem pItem, String pProperty,
									 Object pPropertyValue) throws Exception
	{
		if ("fixedRelatedProducts".equals(pProperty)) {
			initFixedRelatedProductsList(pItem, pProperty, (String) pPropertyValue,
					pRepositoryLoader);
//			initChildProductsList(pItem, pProperty, (String) pPropertyValue,
//					pRepositoryLoader);
		} else
		if ("additionalImages".equals(pProperty)) {
			Set<String> importAdditionalImages = new HashSet<String>();
			importAdditionalImages.addAll( (Collection) pPropertyValue );

			updateChildCollection(pItem, pProperty, importAdditionalImages, false,
					pRepositoryLoader);
		} else {
			pRepositoryLoader.initItemPropertyValue(pRepository, pItem, pProperty, pPropertyValue);
		}
	}


	/**
	 * create relationship between product and sku repository items
	 * @param pLoader repository loader
	 * @param pRecord record to be inserted
	 * @param pInsertedItems map of inserted intes
	 */
	public void postInsert(RepositoryLoader pLoader, Record pRecord, HashMap<String,
			RepositoryItem> pInsertedItems) {
		if(pRecord.getSubRecords().get("sku")!=null) {
			MutableRepositoryItem productItem = (MutableRepositoryItem) pInsertedItems.get("product");
			MutableRepositoryItem skuItem = (MutableRepositoryItem) pInsertedItems.get("sku");
			if(productItem == null || skuItem == null) {
				pLoader.getStatistics().addErrorRecord( new RecordValidationInfo(pRecord,
						"Could not obtain product-sku relationship"));
				return;
			}
			linkProductWithSku(productItem, skuItem);
		}
	}

	/**
	 * link product and sku
	 * @param pProductItem product repository item
	 * @param pSkuItem sku repository item
	 */
	private void linkProductWithSku(MutableRepositoryItem pProductItem, MutableRepositoryItem pSkuItem) {
		List<RepositoryItem> childSKUs = (List<RepositoryItem>) pProductItem.getPropertyValue("childSKUs");
		if(childSKUs == null) {
			childSKUs = new ArrayList<RepositoryItem>();
			pProductItem.setPropertyValue("childSKUs", childSKUs);
		}
		childSKUs.add( pSkuItem );
	}

	/**
	 * reinit record with images collection
	 * @param pRecord record to insert
	 */
	public void reInitProductImagesValues(Record pRecord) {
		Map<String, Object> values = pRecord.getValues();
		List<Object> additionalImages = new ArrayList<Object>();
		List<String> additionalImagesKeys = new ArrayList<String>();
		for(String key : values.keySet()) {
			if(key.startsWith( "addImage_" )) {
				String imageName = (String) values.get(key);
				if(!StringUtils.isBlank(imageName)) {
					additionalImages.add(values.get(key));
				}
				additionalImagesKeys.add( key );
			}
		}
		for(String key : additionalImagesKeys) {
			values.remove(key);
		}
		values.put("additionalImages", additionalImages);
	}
	
	/**
	 * call initializing of child products
	 * @param pItem catalog item
	 * @param pFieldName field name
	 * @param pFieldValue field value
	 * @param pRepositoryLoader repository loader
	 */
	public void initFixedRelatedProductsList(MutableRepositoryItem pItem, String pFieldName,
									  String pFieldValue, RepositoryLoader pRepositoryLoader) {
		Set<RepositoryItem> children = getChildrenToImport(pItem, pFieldValue, ITEM_DESC_PRODUCT);
		SortedSet<RepositoryItem> sotedChildren = sortChildren(pFieldValue, children);
		updateChildCollection(pItem, pFieldName, sotedChildren, false, pRepositoryLoader);
	}
	
	/**
	 * sort pChildren in order of pChildrenIds
	 * @param pChildrenIds - String
	 * @param pChildren - Set<RepositoryItem>
	 * @return List<RepositoryItem>
	 */
	protected SortedSet<RepositoryItem> sortChildren(String pChildrenIds, Set<RepositoryItem> pChildren) {
		SortedSet<RepositoryItem> sortedChildren = new TreeSet<RepositoryItem>();
		if (pChildren != null && pChildren.size() > 0) {
			if (!StringUtils.isBlank(pChildrenIds)) {
				String[] childrenIds = getArrayFromString(pChildrenIds);
				if (childrenIds != null && childrenIds.length > 0) {
					sortedChildren = getSortedChildrenSet(childrenIds, pChildren);
				}
			} else {
				sortedChildren.addAll(pChildren);
			}
		}
		return sortedChildren;
	}
	
	/**
	 * return sorted set with order like pChildrenIds
	 * @param pChildrenIds - String[]
	 * @param pChildren - Set<RepositoryItem>
	 * @return SortedSet<RepositoryItem>
	 */
	private SortedSet<RepositoryItem> getSortedChildrenSet(String[] pChildrenIds, Set<RepositoryItem> pChildren) {
		final List<String> childrenIdsList = Arrays.asList(pChildrenIds);
		TreeSet<RepositoryItem> sortedChildren = new TreeSet<RepositoryItem>(new Comparator<RepositoryItem>() {
			@Override
			public int compare(RepositoryItem o1, RepositoryItem o2) {
				int firstRate = 0;
				int secondRate = 0;
				if (o1 != null) {
					firstRate = childrenIdsList.indexOf(o1.getRepositoryId());
				}
				if (o2 != null) {
					secondRate = childrenIdsList.indexOf(o2.getRepositoryId());
				}
				return firstRate - secondRate;
			}

		});
		
		if(pChildren != null && pChildren.size() > 0) {
			sortedChildren.addAll(pChildren);
		}
		
		return sortedChildren;
	}
	
}
