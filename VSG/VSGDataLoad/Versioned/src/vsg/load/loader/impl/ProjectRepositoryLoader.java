package vsg.load.loader.impl;

import atg.adapter.gsa.GSARepository;
import atg.adapter.version.VersionRepository;
import atg.deployment.common.event.DeploymentEvent;
import atg.dtm.TransactionDemarcation;
import atg.epub.project.Process;
import atg.epub.project.ProcessHome;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.process.action.ActionConstants;
import atg.repository.RepositoryItem;
import atg.security.Persona;
import atg.security.ThreadSecurityManager;
import atg.security.User;
import atg.versionmanager.VersionManager;
import atg.versionmanager.WorkingContext;
import atg.versionmanager.Workspace;
import atg.workflow.WorkflowConstants;
import atg.workflow.WorkflowManager;
import atg.workflow.WorkflowView;
import vsg.load.connection.impl.Connector;
import vsg.load.deployment.ProjectDeploymentNotifier;
import vsg.load.loader.CommonProjectSettings;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public class ProjectRepositoryLoader extends RepositoryLoader implements IDeploymentSource {
	/**
	 * date format used for project name forming
	 */
	private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * current workspace
	 */
	private Workspace mWorkspace;
	/**
	 * current process
	 */
	private Process mProcess = null;
	/**
	 * connector to prod
	 */
	private Connector mConnectorProd;
	/**
	 * common project settings
	 */
	private CommonProjectSettings mCommonProjectSettings;

	/**
	 * project deployment flag
	 * @see vsg.load.deployment.ProjectDeploymentNotifier for flag values
	 */
	private int mProjectDeployFlag = -1;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return current workspace
	 * @return current workspace
	 */
	private Workspace getWorkspace() {
		return mWorkspace;
	}

	/**
	 * init current workspace
	 * @param pWorkspace current workspace
	 */
	private void setWorkspace(Workspace pWorkspace) {
		mWorkspace = pWorkspace;
	}


	/**
	 * return current process
	 * @return current process
	 */
	private Process getProcess() {
		return mProcess;
	}

	/**
	 * init current process
	 * @param pProcess current process
	 */
	private void setProcess(Process pProcess) {
		mProcess = pProcess;
	}

	/**
	 * return connector to prod instance
	 * @return connector to prod instance
	 */
	public Connector getConnectorProd() {
		return mConnectorProd;
	}

	/**
	 * init connector to prod instance
	 * @param pConnectorProd connector to prod instance
	 */
	public void setConnectorProd(Connector pConnectorProd) {
		mConnectorProd = pConnectorProd;
	}

	/**
	 * return common project settings
	 * @return common project settings
	 */
	public CommonProjectSettings getCommonProjectSettings() {
		return mCommonProjectSettings;
	}

	/**
	 * init common  project settings
	 * @param pCommonProjectSettings common project settings
	 */
	public void setCommonProjectSettings(CommonProjectSettings pCommonProjectSettings) {
		mCommonProjectSettings = pCommonProjectSettings;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * before repository will be modified call of project init is performed
	 */
	@Override
	protected void preImportRecords() {
		super.preImportRecords();
		mProjectDeployFlag = -1;
		setWorkspace(null);
		setProcess(null);
		projectInit();
	}

	/**
	 * after repository has been modified call of project deploy is performed
	 */
	@Override
	protected void postImportRecords() {
		super.postImportRecords();
		if(isImportFailed() ||
			(
				getStatistics().getCountInsert() == 0 &&
				getStatistics().getCountUpdate() == 0 &&
				getStatistics().getCountDelete() == 0
			)
		) {
			projectDelete();
		} else {
			projectDeploy();
		}
	}

	/**
	 * no split to separate transactions
	 * @return false
	 */
	@Override
	protected boolean isCommitRequired() {
		return false;
	}

	/**
	 * init project
	 */
	private void projectInit() {
		TransactionDemarcation td = new TransactionDemarcation();
		try {
			transactionStart(td);
			if(assumeUserIdentity()) {
				ProcessHome processHome =  ProjectConstants.getPersistentHomes().getProcessHome();
				setProcess( processHome.createProcessForImport(
						getProjectName(),
						getCommonProjectSettings().getWorkflowName()) );
				String workspaceName = getProcess().getProject().getWorkspace();
				setWorkspace( getVersionManager().getWorkspaceByName(workspaceName) );
				WorkingContext.pushDevelopmentLine(getWorkspace());
			}
		} catch (Exception e) {
			exceptionCatch(e);
		} finally {
			releaseUserIdentity();
			transactionEnd(td);
		}
	}

	/**
	 * deploy project
	 */
	private void projectDeploy() {
		TransactionDemarcation td = new TransactionDemarcation();
		try {
			transactionStart(td);
			if(assumeUserIdentity()) {
				getStatisticsService().setStatus(">> "+getLoaderName()+": push project to deploy <<");
				RepositoryItem processWorkflow = getProcess().getProject().getWorkflow();
				String workflowProcessName = processWorkflow.getPropertyValue("processName").toString();
				String subjectId = getProcess().getId();
				String projectId = getProcess().getProject().getId();

				WorkflowManager workflowManager = getCommonProjectSettings().getWorkflowManager();
				WorkflowView wv = workflowManager.getWorkflowView(ThreadSecurityManager.currentUser());
				wv.fireTaskOutcome(workflowProcessName, WorkflowConstants.DEFAULT_WORKFLOW_SEGMENT, subjectId,
						getCommonProjectSettings().getOutcomeElementId(), ActionConstants.ERROR_RESPONSE_DEFAULT);

				WorkingContext.popDevelopmentLine();

				getCommonProjectSettings().getProjectDeploymentNotifier().registerRequestForProjectDeployment(
						projectId, this
				);
			}
		} catch (Exception e) {
			exceptionCatch(e);
		} finally {
			releaseUserIdentity();
			transactionEnd(td);
			waitForProjectDeployment();
		}
	}

	/**
	 * wait for project deployment
	 */
	private void waitForProjectDeployment() {
		if(!isImportFailed()) {
			try {
				synchronized (this) {
					logDebug("Wait for project deployment");
					getStatisticsService().setStatus(">> "+getLoaderName()+": "+getFileInfo()+
							". wait for project deploy <<");
					while(mProjectDeployFlag == -1) {
						Thread.sleep(4000);
						if(isImportFailed()) {
							logError("Import is failed. Wait for notify is released.");
							break;
						}
					}
				}
			} catch (InterruptedException e) {
				logError(e);
			}
		}
	}

	/**
	 * delete process if project failed
	 */
	private void projectDelete() {
		try {
			logDebug("Project will be deleted.");
			assumeUserIdentity();
			Project project = getProcess().getProject();
			if (project == null) {
				getProcess().delete();
				logDebug("Process was found but there was no project associated with it. " +
						"Process was deleted successfully.");
				return;
			}
			project.delete("admin");
			// getProcess().delete();
			logDebug("Project with id " + project.getId() + " was found and deleted successfully.");
//			getProcess().delete();
			// getProcess().getProject().delete("admin");
			( (GSARepository) getCommonProjectSettings().getPublishingRepository()).invalidateCaches();
		} catch (Exception e) {
			exceptionCatch(e);
		}
	}

	/**
	 * checks if there is user with satisfied rights to perform project deploy
	 * @return flag true if there is user with satisfied rights
	 */
	protected boolean assumeUserIdentity() {
		User newUser = new User();
		Persona persona = getCommonProjectSettings().initPersona();
		if (persona == null) {
			logError("Could not obtain required rights to import data");
			setImportFailed(true);
			return false;
		}
		newUser.addPersona(persona);
		ThreadSecurityManager.setThreadUser(newUser);
		return true;
	}

	/**
	 * return version manager from repository
	 * @return version manager
	 */
	private VersionManager getVersionManager() {
		VersionRepository repository = (VersionRepository) getConnector().getRepository();
		return repository.getVersionManager();
	}

	/**
	 * return project name
	 * @return project name
	 */
	private String getProjectName() {
		return getLoaderName() +" ["+getCurrentTime()+"]";
	}

	/**
	 * format current date to string
	 * @return formatted date to string
	 */
	private String getCurrentTime() {
		Date currentTime = Calendar.getInstance().getTime();
		return mDateFormat.format(currentTime);
	}

	/**
	 * This method unsets the security context on the current thread.
	 */
	private void releaseUserIdentity() {
		ThreadSecurityManager.setThreadUser(null);
	}

	/**
	 * notify for project deployment finished
	 * @param pFlag one of three variants: COMPLETED, CANCELED, FAILED
	 */
	public void notifyDeployFinished(int pFlag) {
		if(pFlag == ProjectDeploymentNotifier.DEPLOYEMNT_FAILED) {
			setImportFailed(true);
			getStatistics().setFailReason("Project deployment failed.");
			getStatisticsService().setStatus(">> "+getLoaderName()+": project deployment failed <<");
		} else
		if(pFlag == ProjectDeploymentNotifier.DEPLOYEMNT_CANCELED) {
			setImportFailed(true);
			getStatistics().setFailReason("Project deployment canceled.");
			getStatisticsService().setStatus(">> "+getLoaderName()+": project deployment canceled <<");
		} else {
			getStatisticsService().setStatus(">> "+getLoaderName()+": project deployment finished <<");
		}
		logDebug("Project deployment notified");
		mProjectDeployFlag = pFlag;
	}
	
	@Override
	public boolean ownDeployment(DeploymentEvent pEvent) {
		if(getProcess() != null && !getProcess().isRemoved()) {
			String projectId = getProcess().getProject().getId();
			String[] projectIds = pEvent.getDeploymentProjectIDs();
			if(projectId != null && projectIds != null && projectIds.length == 1) {
				if (projectId.equals(projectIds[0])) {
					return true;
				}
			}
		}
		
		return false;
	}
}
