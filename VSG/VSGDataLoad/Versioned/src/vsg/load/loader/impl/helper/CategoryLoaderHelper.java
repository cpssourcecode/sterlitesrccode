package vsg.load.loader.impl.helper;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import vsg.load.loader.impl.RepositoryLoader;
import vsg.load.tools.ILoggerTools;

/**
 * @author Dmitry Golubev
 */
public class CategoryLoaderHelper extends BaseCatalogLoaderHelper {
	/**
	 * constructor
	 * @param pLoaderName  loader name
	 * @param pLoggerTools logger tools
	 */
	public CategoryLoaderHelper(String pLoaderName, ILoggerTools pLoggerTools) {
		super(pLoaderName, pLoggerTools);
	}

	/**
	 * init repository item with property value, redirects on child collection initialization
	 * @param pRepositoryLoader repository loader
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pProperty property name
	 * @param pPropertyValue property value
	 * @throws Exception if error occurs
	 */
	public void initPropertyValue(RepositoryLoader pRepositoryLoader, Repository pRepository,
									 MutableRepositoryItem pItem, String pProperty,
									 Object pPropertyValue) throws Exception
	{
		if ("fixedChildCategories".equals(pProperty)) {
			initChildCategoriesList(pItem, pProperty, (String) pPropertyValue,
					pRepositoryLoader);
		} else
		if ("fixedChildProducts".equals(pProperty)) {
			initChildProductsList(pItem, pProperty, (String) pPropertyValue,
					pRepositoryLoader);
		} else {
			pRepositoryLoader.initItemPropertyValue(pRepository, pItem, pProperty, pPropertyValue);
		}
	}

}
