package vsg.load.loader.impl.workspace;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import vsg.load.loader.impl.helper.ProductLoaderHelper;
import vsg.load.loader.impl.WorkspaceRelationsRepositoryLoader;

/**
 * @author Dmitry Golubev
 */
public class ProductRelationsWorkspaceLoader extends WorkspaceRelationsRepositoryLoader {

	/**
	 * catalogLoader helper
	 */
	private ProductLoaderHelper mHelper;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return BaseCatalogLoaderHelper object
	 * @return BaseCatalogLoaderHelper object
	 */
	private ProductLoaderHelper getHelper() {
		if(mHelper == null) {
			mHelper = new ProductLoaderHelper(getLoaderName(), getLoggerTools());
		}
		return mHelper;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init repository item with property value, redirects on child collection initialization
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pProperty property name
	 * @param pPropertyValue property value
	 * @throws Exception if error occurs
	 */
	@Override
	protected void initPropertyValue(Repository pRepository, MutableRepositoryItem pItem, String pProperty,
									 Object pPropertyValue) throws Exception {
		getHelper().initPropertyValue(this, pRepository, pItem, pProperty, pPropertyValue);
	}

}
