package vsg.load.loader.impl.helper;

import atg.repository.MutableRepositoryItem;
import vsg.load.loader.impl.helper.base.BaseLoaderHelper;
import vsg.load.loader.impl.RepositoryLoader;
import vsg.load.tools.ILoggerTools;

/**
 * @author Dmitry Golubev
 */
public class BaseCatalogLoaderHelper extends BaseLoaderHelper {
	/**
	 * category item descriptor name
	 */
	private static final String ITEM_DESC_CATEGORY = "category";
	/**
	 * catalog item descriptor name
	 */
	private static final String ITEM_DESC_CATALOG = "catalog";
	/**
	 * product item descriptor name
	 */
	public static final String ITEM_DESC_PRODUCT = "product";
	/**
	 * repository item property rootSubCatalogs
	 */
	private static final String FIELD_ROOT_SUB_CATALOG = "rootSubCatalogs";

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 * @param pLoaderName loader name
	 * @param pLoggerTools logger tools
	 */
	public BaseCatalogLoaderHelper(String pLoaderName, ILoggerTools pLoggerTools) {
		super(pLoaderName, pLoggerTools);
	}

	/**
	 * call initializing of root catalogs
	 * @param pItem catalog item
	 * @param pFieldValue field value
	 * @param pRepositoryLoader repository loader
	 */
	public void initRootSubCatalogs(MutableRepositoryItem pItem, String pFieldValue,
									RepositoryLoader pRepositoryLoader) {
		initChildElements(pItem, ITEM_DESC_CATALOG, FIELD_ROOT_SUB_CATALOG, pFieldValue, true, pRepositoryLoader);
	}

	/**
	 * call initializing of root categories
	 * @param pItem catalog item
	 * @param pFieldName field name
	 * @param pFieldValue field value
	 * @param pRepositoryLoader repository loader
	 */
	public void initChildCategoriesSet(MutableRepositoryItem pItem, String pFieldName,
									   String pFieldValue, RepositoryLoader pRepositoryLoader) {
		initChildElements(pItem, ITEM_DESC_CATEGORY, pFieldName, pFieldValue, true, pRepositoryLoader);
	}

	/**
	 * call initializing of child categories
	 * @param pItem catalog item
	 * @param pFieldName field name
	 * @param pFieldValue field value
	 * @param pRepositoryLoader repository loader
	 */
	public void initChildCategoriesList(MutableRepositoryItem pItem, String pFieldName,
										String pFieldValue, RepositoryLoader pRepositoryLoader) {
		initChildElements(pItem, ITEM_DESC_CATEGORY, pFieldName, pFieldValue, false, pRepositoryLoader);
	}

	/**
	 * call initializing of child products
	 * @param pItem catalog item
	 * @param pFieldName field name
	 * @param pFieldValue field value
	 * @param pRepositoryLoader repository loader
	 */
	public void initChildProductsList(MutableRepositoryItem pItem, String pFieldName,
									  String pFieldValue, RepositoryLoader pRepositoryLoader) {
		initChildElements(pItem, ITEM_DESC_PRODUCT, pFieldName, pFieldValue, false, pRepositoryLoader);
	}
}
