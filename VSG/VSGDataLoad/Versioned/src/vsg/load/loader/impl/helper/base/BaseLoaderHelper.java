package vsg.load.loader.impl.helper.base;

import atg.core.util.StringUtils;
import atg.repository.*;
import vsg.load.info.FileInfo;
import vsg.load.loader.impl.RepositoryLoader;
import vsg.load.tools.ILoggerTools;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Dmitry Golubev
 */
public class BaseLoaderHelper {
	/**
	 * logger tools
	 */
	private ILoggerTools mLoggerTools;
	/**
	 * loader name
	 */
	private String mLoaderName;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * constructor
	 * @param pLoaderName loader name
	 * @param pLoggerTools logger tools
	 */
	public BaseLoaderHelper(String pLoaderName, ILoggerTools pLoggerTools) {
		mLoaderName = pLoaderName;
		mLoggerTools = pLoggerTools;
	}

	/**
	 * common method to init child collections
	 * @param pItem catalog item
	 * @param pItemDescriptor child collection item descriptor name
	 * @param pChildrenPropertyName field name
	 * @param pChildrenIds field value
	 * @param pUseSet use java.util.Set as child collection
	 * @param pRepositoryLoader repository loader
	 */
	protected void initChildElements(MutableRepositoryItem pItem, String pItemDescriptor, String pChildrenPropertyName,
									 String pChildrenIds, boolean pUseSet, RepositoryLoader pRepositoryLoader) {
/*
		long start = Calendar.getInstance().getTimeInMillis();
*/
		Set<RepositoryItem> children = getChildrenToImport(pItem, pChildrenIds, pItemDescriptor);
		updateChildCollection(pItem, pChildrenPropertyName, children, pUseSet, pRepositoryLoader);
/*
		long end = Calendar.getInstance().getTimeInMillis();
		logDebug(getTime(pItem.getRepositoryId() + " - " + (children != null ? children
				.size() : "0"), end - start) + " " + pChildrenPropertyName);
*/
	}

	/**
	 * return children to import
	 * @param pItem catalog item
	 * @param pChildrenIds children ids
	 * @param pItemDescriptor child collection item descriptor name
	 * @return children to import
	 */
	protected Set<RepositoryItem> getChildrenToImport(MutableRepositoryItem pItem,
													String pChildrenIds, String pItemDescriptor) {
		Set<RepositoryItem> children = new HashSet<RepositoryItem>();
		if(!StringUtils.isBlank(pChildrenIds)) {
			Set<String> childrenIdsSet = new HashSet<String>();
			String[] childrenIds = getArrayFromString(pChildrenIds);
			childrenIdsSet.addAll( Arrays.asList( childrenIds ) );

			children = getChildren(pItem.getRepository(), childrenIdsSet, pItemDescriptor);
			if(childrenIdsSet.size() != children.size()) {
				logNotFoundedChildren(pItem.getRepositoryId(), children, childrenIdsSet);
			}
		}

		return children;
	}

	/**
	 * update children collection
	 * @param pItem catalog item
	 * @param pChildrenFieldName children collection field name
	 * @param pImportChildren new children set
	 * @param pUseSet use java.util.Set as child collection
	 * @param pRepositoryLoader repository loader
	 */
	public void updateChildCollection(MutableRepositoryItem pItem, String pChildrenFieldName,
									  Set pImportChildren, boolean pUseSet, RepositoryLoader pRepositoryLoader) {
		Collection currentChildren = getChildren(pItem, pChildrenFieldName, pUseSet);
		if(pImportChildren.size() == 0 && currentChildren.size() == 0) {
			return;
		}
		switch (pRepositoryLoader.getFileInfo().getAction()) {
			case FileInfo.ACTION_INSERT:
				importCreateChildCollection(pItem, pChildrenFieldName, pImportChildren, pUseSet, pRepositoryLoader);
				break;
			case FileInfo.ACTION_UPDATE:
				importUpdateChildCollection(pItem, pChildrenFieldName, currentChildren, pImportChildren, pUseSet,
						pRepositoryLoader);
				break;
			case FileInfo.ACTION_DELETE:
				importDeleteChildCollection(pItem, pChildrenFieldName, currentChildren, pImportChildren, pUseSet,
						pRepositoryLoader);
				break;
		}
	}

	/**
	 * import create children collection
	 * @param pItem catalog item
	 * @param pChildrenFieldName children collection field name
	 * @param pImportChildren new children set
	 * @param pUseSet use java.util.Set as child collection
	 * @param pRepositoryLoader repository loader
	 */
	protected void importCreateChildCollection(MutableRepositoryItem pItem, String pChildrenFieldName,
											   Set pImportChildren, boolean pUseSet,
											   RepositoryLoader pRepositoryLoader) {
		Collection newChildrenCollection;
		if(pUseSet) {
			newChildrenCollection = pImportChildren;
		} else {
			newChildrenCollection = new ArrayList();
			newChildrenCollection.addAll(pImportChildren);
		}

		pRepositoryLoader.setPropertyValue(pItem, pChildrenFieldName, newChildrenCollection);
	}

	/**
	 * import update children collection
	 * @param pItem catalog item
	 * @param pChildrenFieldName children collection field name
	 * @param pCurrentChildren current children set
	 * @param pImportChildren new children set
	 * @param pUseSet use java.util.Set as child collection
	 * @param pRepositoryLoader repository loader
	 */
	protected void importUpdateChildCollection(MutableRepositoryItem pItem, String pChildrenFieldName,
											   Collection pCurrentChildren, Set pImportChildren, boolean pUseSet,
											   RepositoryLoader pRepositoryLoader) {
		Collection<Object> newChildrenCollection;
		if(pUseSet) {
			newChildrenCollection = new HashSet<Object>();
			if(pCurrentChildren != null) {
				newChildrenCollection.addAll( pCurrentChildren );
			}
			if(pImportChildren != null) {
				newChildrenCollection.addAll( pImportChildren );
			}
		} else {
			newChildrenCollection = new ArrayList<Object>();
			if(pCurrentChildren != null) {
				newChildrenCollection.addAll( pCurrentChildren );
			}
			if(pImportChildren != null) {
				pImportChildren.removeAll(newChildrenCollection);
				newChildrenCollection.addAll( pImportChildren );
			}
		}
		pRepositoryLoader.setPropertyValue(pItem, pChildrenFieldName, newChildrenCollection);
	}

	/**
	 * import delete children collection
	 * @param pItem catalog item
	 * @param pChildrenFieldName children collection field name
	 * @param pCurrentChildren current children set
	 * @param pImportChildren children set to delete
	 * @param pUseSet use java.util.Set as child collection
	 * @param pRepositoryLoader repository loader
	 */
	protected void importDeleteChildCollection(MutableRepositoryItem pItem, String pChildrenFieldName,
											   Collection pCurrentChildren, Set pImportChildren, boolean pUseSet,
											   RepositoryLoader pRepositoryLoader) {
		Collection<Object> newChildrenCollection;
		if(pUseSet) {
			newChildrenCollection = new HashSet<Object>();
		} else {
			newChildrenCollection = new ArrayList<Object>();
		}
		if(pCurrentChildren != null) {
			newChildrenCollection.addAll( pCurrentChildren );
		}
		if(pImportChildren != null) {
			newChildrenCollection.removeAll( pImportChildren );
		}
		pRepositoryLoader.setPropertyValue(pItem, pChildrenFieldName, newChildrenCollection);
	}

	/**
	 * get children collection (init if is null)
	 * @param pItem catalog item
	 * @param pChildrenFieldName children collection field name
	 * @param pUseSet  use java.util.Set as child collection
	 * @return children collection
	 */
	private Collection getChildren(MutableRepositoryItem pItem, String pChildrenFieldName, boolean pUseSet) {
		Collection childrenCollection = (Collection) pItem.getPropertyValue(pChildrenFieldName);
		if(childrenCollection == null) {
			if(pUseSet) {
				childrenCollection = new HashSet();
			} else {
				childrenCollection = new ArrayList();
			}
		}
		return childrenCollection;
	}

	/**
	 * log not founded children
	 * @param pCategoryId category id
	 * @param pChildren set of loaded from db children
	 * @param pChildrenIds set of initial children ids
	 */
	private void logNotFoundedChildren(String pCategoryId, Set<RepositoryItem> pChildren,
									   Set<String> pChildrenIds) {
		Set<String> foundedChildrenIds = new HashSet<String>();
		for(RepositoryItem child : pChildren) {
			foundedChildrenIds.add(child.getRepositoryId());
		}
		pChildrenIds.removeAll( foundedChildrenIds );

		StringBuilder ids = new StringBuilder();
		for(String childId : pChildrenIds) {
			ids.append(childId).append(",");
		}
		if(pChildrenIds.size() > 0) {
			ids.deleteCharAt(ids.length()-1);
			logError( getNotFoundChildMsg(pCategoryId, ids.toString()) );
		}
	}

	/**
	 * generate message for missed child record
	 * @param pCategoryId category id
	 * @param pChildId child id
	 * @return message for missed child record
	 */
	private String getNotFoundChildMsg(String pCategoryId, String pChildId) {
		return "["+pCategoryId+"] not found ["+pChildId+"] ";
	}

	/**
	 * load set of repository items
	 * @param pRepository product catalog repository
	 * @param pChildrenIds children ids to find
	 * @param pItemDescriptor child item descriptor name
	 * @return loaded set of repository items
	 */
	private Set<RepositoryItem> getChildren(Repository pRepository, Set<String> pChildrenIds, String pItemDescriptor) {
		Set<RepositoryItem> childRecords = new HashSet<RepositoryItem>();
		if (pChildrenIds != null && pChildrenIds.size() > 0) {
			try {
				String[] stringArr = new String[pChildrenIds.size()];
				pChildrenIds.toArray(stringArr);
/*
				RepositoryItem[] results = ((GSARepository) pRepository).getItems(stringArr, pItemDescriptor, true);
*/

				RepositoryView view = pRepository.getView(pItemDescriptor);

				String [] preCachedProperties = new String[] { "displayName" };
				QueryOptions options = new QueryOptions(0, pChildrenIds.size(), null, preCachedProperties);

				List<Query> idQueries = new ArrayList<Query>();
				QueryBuilder qb = view.getQueryBuilder();
				for(String childId : pChildrenIds) {
					QueryExpression idProp = qb.createPropertyQueryExpression("id");
					QueryExpression idValue = qb.createConstantQueryExpression(childId);
					idQueries.add( qb.createComparisonQuery(idProp, idValue, QueryBuilder.EQUALS) );
				}
				Query orQuery = qb.createOrQuery(idQueries.toArray(new Query[idQueries.size()]));
				RepositoryItem[] results = view.executeQuery(orQuery, options);

				if (results != null && results.length > 0) {
					childRecords.addAll(Arrays.asList(results));
				}
			} catch (Exception e) {
				logError(e.toString());
			}
		}
		return childRecords;
	}


	/**
	 * reform children string property value
	 * @param pItem catalog item
	 * @param pProperty children property name
	 * @param pPropertyValue children property value
	 * @param pAction action (0 - insert, 1 - update, 2 - delete)
	 * @return reformed children string property value
	 * @throws Exception if error occurs
	 */
	protected String getInitChildStringElements(MutableRepositoryItem pItem, String pProperty,
												Object pPropertyValue, int pAction) throws Exception {
		Set<String> newChildIds     = getChildIdsSet((String)pPropertyValue);
						Set<String> currentChildIds = getChildIdsSet((String)pItem.getPropertyValue(pProperty));
						Set<String> modifiedChildIds = new HashSet<String>();
						switch (pAction) {
							case FileInfo.ACTION_INSERT:
								modifiedChildIds.addAll( newChildIds );
								break;
			case FileInfo.ACTION_UPDATE:
				if(newChildIds!=null) {
					modifiedChildIds.addAll( newChildIds );
				}
				if(currentChildIds!=null) {
					modifiedChildIds.addAll( currentChildIds );
				}
				break;
			case FileInfo.ACTION_DELETE:
				if(currentChildIds!=null) {
					modifiedChildIds.addAll( currentChildIds );
				}
				if(newChildIds!=null) {
					modifiedChildIds.removeAll( newChildIds );
				}
				break;
		}
		return getStringFromArray(modifiedChildIds.toArray(new String[modifiedChildIds.size()]));
	}

	/**
	 * form set of child ids
	 * @param pChildIds string of ids
	 * @return set of child ids
	 */
	private Set<String> getChildIdsSet(String pChildIds) {
		Set<String> childIds = new HashSet<String>();
		if(pChildIds == null) {
			return childIds;
		}
		String[] childIdsArr = getArrayFromString( pChildIds );
		Collections.addAll(childIds, childIdsArr);
		return childIds;
	}

	/**
	 * log error
	 * @param pError message
	 */
	private void logError(String pError) {
		if(mLoggerTools.isLoggingError()) {
			mLoggerTools.logError(mLoaderName + ": " +pError);
		}
	}

	/**
	 * log info
	 * @param pInfo message
	 */
	private void logInfo(String pInfo) {
		if(mLoggerTools.isLoggingInfo()) {
			mLoggerTools.logInfo(mLoaderName + ": " + pInfo);
		}
	}

	/**
	 * format time to string
	 * @param pMsg additional message before time
	 * @param pTime time in mills
	 * @return formatted string
	 */
	protected String getTime(String pMsg, long pTime){
		return String.format(pMsg + ": %d min %d sec.",
		    TimeUnit.MILLISECONDS.toMinutes(pTime),
		    TimeUnit.MILLISECONDS.toSeconds(pTime) -
		    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(pTime))
		);
	}

	/**
	 * split string with delimeter
	 * @param pValue string to be splitted
	 * @return array of initial string value parts, splitted by delimeter
	 */
	protected String[] getArrayFromString(String pValue) {
		return pValue.split("\\|");
	}

	/**
	 * form string with delimeters from array of values
	 * @param pValues child ids value
	 * @return string of child ids
	 */
	protected String getStringFromArray(String[] pValues) {
		StringBuilder result = new StringBuilder();
		if(pValues!=null) {
			for(String value : pValues) {
				result.append(value).append("|");
			}
			if(pValues.length > 0) {
				result.deleteCharAt(result.length()-1);
			}
		}
		return result.toString();
	}
}
