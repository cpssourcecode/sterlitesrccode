package vsg.load.loader.impl.workspace;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import vsg.load.loader.impl.helper.CategoryLoaderHelper;
import vsg.load.loader.impl.WorkspaceRelationsRepositoryLoader;

/**
 * @author Dmitry Golubev
 */
public class CategoryRelationsWorkspaceLoader extends WorkspaceRelationsRepositoryLoader {

	/**
	 * catalogLoader helper
	 */
	private CategoryLoaderHelper mHelper;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return BaseCatalogLoaderHelper object
	 * @return BaseCatalogLoaderHelper object
	 */
	private CategoryLoaderHelper getHelper() {
		if(mHelper == null) {
			mHelper = new CategoryLoaderHelper(getLoaderName(), getLoggerTools());
		}
		return mHelper;
	}

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * init repository item with property value, redirects on child collection initialization
	 * @param pRepository items repository
	 * @param pItem repository item
	 * @param pProperty property name
	 * @param pPropertyValue property value
	 * @throws Exception if error occurs
	 */
	@Override
	protected void initPropertyValue(Repository pRepository, MutableRepositoryItem pItem, String pProperty,
									 Object pPropertyValue) throws Exception {
		getHelper().initPropertyValue(this, pRepository, pItem, pProperty, pPropertyValue);
	}
}
