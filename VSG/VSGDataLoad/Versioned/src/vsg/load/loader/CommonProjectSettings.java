package vsg.load.loader;

import atg.repository.Repository;
import atg.security.Persona;
import atg.security.UserAuthority;
import atg.workflow.WorkflowManager;
import vsg.load.deployment.ProjectDeploymentNotifier;

/**
 * @author Dmitry Golubev
 */
public class CommonProjectSettings {
	/**
	 * workflow manager
	 */
	private WorkflowManager mWorkflowManager;
	/**
	 * user authority
	 */
	private UserAuthority mUserAuthority;

	/**
	 * element id
	 */
	private String mOutcomeElementId = "4.1.1";

	/**
	 * persona on bcc name
	 */
	private String mPersona = "Profile$login$admin";

	/**
	 * used workflow
	 */
	private String mWorkflowName = "/Content Administration/import.wdl";

	/**
	 * project deployment notifier
	 */
	private ProjectDeploymentNotifier mProjectDeploymentNotifier;

	/**
	 * /atg/epub/PublishingRepository
	 */
	private Repository mPublishingRepository;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * return workflow manager
	 * @return workflow manager
	 */
	public WorkflowManager getWorkflowManager() {
		return mWorkflowManager;
	}

	/**
	 * init workflow manager
	 * @param pWorkflowManager workflow manager
	 */
	public void setWorkflowManager(WorkflowManager pWorkflowManager) {
		mWorkflowManager = pWorkflowManager;
	}

	/**
	 * return user authority
	 * @return user authority
	 */
	public UserAuthority getUserAuthority() {
		return mUserAuthority;
	}

	/**
	 * init user authority
	 * @param pUserAuthority user authority
	 */
	public void setUserAuthority(UserAuthority pUserAuthority) {
		mUserAuthority = pUserAuthority;
	}

	/**
	 * return element id
	 * @return element id
	 */
	public String getOutcomeElementId() {
		return mOutcomeElementId;
	}

	/**
	 * init element id
	 * @param pOutcomeElementId element id
	 */
	public void setOutcomeElementId(String pOutcomeElementId) {
		mOutcomeElementId = pOutcomeElementId;
	}

	/**
	 * return used workflow
	 * @return used workflow
	 */
	public String getWorkflowName() {
		return mWorkflowName;
	}

	/**
	 * init used workflow
	 * @param pWorkflowName used workflow
	 */
	public void setWorkflowName(String pWorkflowName) {
		mWorkflowName = pWorkflowName;
	}

	/**
	 * return persona on bcc name
	 * @return persona on bcc name
	 */
	public String getPersona() {
		return mPersona;
	}

	/**
	 * init persona on bcc name
	 * @param pPersona persona on bcc name
	 */
	public void setPersona(String pPersona) {
		mPersona = pPersona;
	}

	/**
	 * return project deployment notifier
	 * @return project deployment notifier
	 */
	public ProjectDeploymentNotifier getProjectDeploymentNotifier() {
		return mProjectDeploymentNotifier;
	}

	/**
	 * init project deployment notifier
	 * @param pProjectDeploymentNotifier project deployment notifier
	 */
	public void setProjectDeploymentNotifier(ProjectDeploymentNotifier pProjectDeploymentNotifier) {
		mProjectDeploymentNotifier = pProjectDeploymentNotifier;
	}

	/**
	 * init Persona object
	 * @return initialized Persona object
	 */
	public Persona initPersona() {
		return getUserAuthority().getPersona(getPersona());
	}

	/**
	 * return publishing repository
	 * @return publishing repository
	 */
	public Repository getPublishingRepository() {
		return mPublishingRepository;
	}

	/**
	 * init publishing repository
	 * @param pPublishingRepository publishing repository
	 */
	public void setPublishingRepository(Repository pPublishingRepository) {
		mPublishingRepository = pPublishingRepository;
	}
}
