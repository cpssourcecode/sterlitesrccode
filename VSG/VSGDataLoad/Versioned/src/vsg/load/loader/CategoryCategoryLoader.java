package vsg.load.loader;

import java.util.ArrayList;
import java.util.List;

import vsg.load.exception.ValidationException;
import vsg.load.loader.Record;
import vsg.load.loader.impl.ProjectRepositoryLoader;
import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/**
 * @author Dmitry Golubev
 */
public class CategoryCategoryLoader extends ProjectRepositoryLoader {
	
	/** property name for child categories in the category */
	private static final String CHILD_CATEGORIES = "fixedChildCategories";

	// -----------------------------------------------------------------------------------------------------------------

	@Override
	protected boolean insertCheckItem(Record pRecord, RepositoryItem pItem) {
		return true;
	}

	@Override
	protected void initPropertyValue(Repository pRepository, MutableRepositoryItem pItem, String pFieldName,
			Object pFieldValue) throws Exception {
		if (CHILD_CATEGORIES.equals(pFieldName)) {
			String fieldValue = pFieldValue == null ? null : pFieldValue.toString();
			if(!StringUtils.isBlank(fieldValue)) {
				List<RepositoryItem> childCategories = new ArrayList<RepositoryItem>();
				String[] childCategoriesIds = fieldValue.split("\\|");
				for(String childCategoryId : childCategoriesIds) {
					RepositoryItem item = null;
					try {
						item = pRepository.getItem(childCategoryId, getItemDescriptor());
					} catch (RepositoryException e) {
						// no item
						throw new ValidationException("No child category " + pItem.getRepositoryId() + " found for "
								+ "category: " + childCategoryId);
					}
					childCategories.add(item);
				}
				pItem.setPropertyValue(CHILD_CATEGORIES, childCategories);
			} else {
				pItem.setPropertyValue(CHILD_CATEGORIES, null);
			}
		} else {
			super.initPropertyValue(pRepository, pItem, pFieldName, pFieldValue);
		}
	}
}
