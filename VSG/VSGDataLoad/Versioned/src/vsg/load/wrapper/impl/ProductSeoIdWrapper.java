package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;
import vsg.load.wrapper.impl.ValueWrapperImpl;

/**
 * @author Dmitry Golubev
 */
public class ProductSeoIdWrapper extends ValueWrapperImpl<String> {
	@Override
	public String getValue(Object pValueObj) {
		String pValue = (String) pValueObj;
		if(StringUtils.isBlank(pValue)) {
			return getEmptyValue();
		}
		int indexOfLastBy = pValue.lastIndexOf("-by-");
		if(indexOfLastBy != -1) {
			return pValue.substring(0, indexOfLastBy);
		}
		return pValue;
	}
}
