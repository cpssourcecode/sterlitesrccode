package vsg.load.wrapper.impl;

import vsg.load.wrapper.impl.value.ProductSkuRepositoryValue;
import vsg.load.wrapper.impl.RepositoryRepositoryValue;
import vsg.load.wrapper.impl.RepositoryWrapper;

/**
 * @author Dmitry Golubev
 */
public class ProductSkuWrapper extends RepositoryWrapper {
	/**
	 * get custom ProductSkuRepositoryValue to retrieve list with specified sku
	 * @param pValue sku id
	 * @return list with specified sku
	 */
	@Override
	protected RepositoryRepositoryValue initWrappedValue(String pValue) {
		return new ProductSkuRepositoryValue(pValue, getItemDescriptor());
	}
}
