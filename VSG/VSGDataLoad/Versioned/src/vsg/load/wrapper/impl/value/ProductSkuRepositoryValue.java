package vsg.load.wrapper.impl.value;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.load.wrapper.impl.RepositoryRepositoryValue;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class ProductSkuRepositoryValue extends RepositoryRepositoryValue {
	/**
	 * constructor
	 * @param pRepositoryId   repository id
	 * @param pItemDescriptor item descriptor
	 */
	public ProductSkuRepositoryValue(String pRepositoryId, String pItemDescriptor) {
		super(pRepositoryId, pItemDescriptor);
	}

	/**
	 * return childSKUs with inited sku
	 * @param pRepository repository
	 * @return childSKUs with inited sku
	 * @throws RepositoryException if error occurs
	 */
	@Override
	public Object getValue(Repository pRepository) throws RepositoryException {
		RepositoryItem sku =  pRepository.getItem( getRepositoryId(), getItemDescriptor() );
		List<RepositoryItem> childSKUs = new ArrayList<RepositoryItem>();
		childSKUs.add(sku);
		return childSKUs;
	}
}
