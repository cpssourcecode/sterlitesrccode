package vsg.load.wrapper.impl.value;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.repository.Repository;
import atg.repository.RepositoryView;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import vsg.load.wrapper.impl.RepositoryRepositoryValue;


/**
 * @author Andy Porter
 */
public class PropertyValueRepositoryValue extends RepositoryRepositoryValue {

	private ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(PropertyValueRepositoryValue.class);

	/**
	 * mPropertyName
	 */
	private String mPropertyName;

	/**
	 * nullRepositoryItem
	 */
	private RepositoryItem nullRepositoryItem = null;


	/**
	 * constructor
	 *
	 * @param pPropertyValue  propertyValue
	 * @param pItemDescriptor item descriptor
	 * @param mPropertyName   mPropertyName
	 */
	public PropertyValueRepositoryValue(String pPropertyValue, String pItemDescriptor, String mPropertyName) {
		super(pPropertyValue, pItemDescriptor);
		setPropertyName(mPropertyName);
	}

	/**
	 * return childSKUs with inited sku
	 *
	 * @param pRepository repository
	 * @return childSKUs with inited sku
	 * @throws atg.repository.RepositoryException if error occurs
	 */
	@Override
	public Object getValue(Repository pRepository) throws RepositoryException {
		return getRepositoryItem(pRepository, getPropertyName(), getRepositoryId());
	}


	/**
	 * return defined in repository RepositoryItem
	 * @param pRepository       Repository
	 * @param pPropertyName     pPropertyName
	 * @param pPropertyValue    pPropertyValue
	 * @return RepositoryItem
	 * <p/>
	 * NOTE: getRepositoryId() return  pPropertyValue
	 */
	public RepositoryItem getRepositoryItem(Repository pRepository, String pPropertyName, String pPropertyValue) {
		try {
			RepositoryView productView = pRepository.getView(getItemDescriptor());
			QueryBuilder queryBuilder = productView.getQueryBuilder();
			Query query = queryBuilder.createComparisonQuery(
					queryBuilder.createPropertyQueryExpression(pPropertyName),
					queryBuilder.createConstantQueryExpression(pPropertyValue),
					QueryBuilder.EQUALS);
			RepositoryItem[] repositoryItems = productView.executeQuery(query);
			if (repositoryItems == null || repositoryItems.length == 0) {
				return nullRepositoryItem;
			} else {
				return repositoryItems[0];
			}
		} catch (Exception e) {
			if(mLogger.isLoggingError()){
				mLogger.logError(e);
			}
		}
		return nullRepositoryItem;
	}


	/**
	 * Gets mPropertyName.
	 *
	 * @return Value of mPropertyName.
	 */
	public String getPropertyName() {
		return mPropertyName;
	}

	/**
	 * Sets new mPropertyName.
	 *
	 * @param pPropertyName New value of mPropertyName.
	 */
	public void setPropertyName(String pPropertyName) {
		mPropertyName = pPropertyName;
	}
}
