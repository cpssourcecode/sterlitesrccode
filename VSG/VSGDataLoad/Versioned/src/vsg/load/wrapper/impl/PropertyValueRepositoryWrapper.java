package vsg.load.wrapper.impl;

import atg.core.util.StringUtils;
import vsg.load.wrapper.impl.value.ProductSkuRepositoryValue;
import vsg.load.wrapper.impl.value.PropertyValueRepositoryValue;

/**
 * get property by some property.field value
 *
 * @author Andy Porter
 */
public class PropertyValueRepositoryWrapper extends RepositoryWrapper {

	/**
	 * mPropertyName
	 */
	private String mPropertyName;


	/**
	 * get custom ProductSkuRepositoryValue to retrieve list with specified sku
	 *
	 * @param pValue sku id
	 * @return list with specified sku
	 */
	@Override
	protected RepositoryRepositoryValue initWrappedValue(String pValue) {
		return new PropertyValueRepositoryValue(pValue, getItemDescriptor(), mPropertyName);
	}

	/**
	 * Gets mPropertyName.
	 *
	 * @return Value of mPropertyName.
	 */
	public String getPropertyName() {
		return mPropertyName;
	}

	/**
	 * Sets new mPropertyName.
	 *
	 * @param pPropertyName New value of mPropertyName.
	 */
	public void setPropertyName(String pPropertyName) {
		mPropertyName = pPropertyName;
	}
}
