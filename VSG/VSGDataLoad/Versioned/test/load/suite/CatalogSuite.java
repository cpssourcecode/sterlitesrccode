package load.suite;

import load.catalog.ingredient.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Dmitry Golubev
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
		{
				IngredientImportServiceTest.class,
				IngredientImportToolsTest.class,
				IngredientPathInfoTest.class,
				IngredientRepositoryTest.class,
				IngredientValidationToolsTest.class
		}
)
public class CatalogSuite {
}
