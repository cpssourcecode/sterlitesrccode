package load.suite;

/**
 * @author Dmitry Golubev
 */

import load.catalog.CatalogImportToolsTest;
import load.catalog.category.CategoryImportToolsTest;
import load.catalog.category.SubCategoryImportToolsTest;
import load.catalog.ingredient.IngredientImportToolsTest;
import load.catalog.product.ProductImportToolsTest;
import load.inventory.InvImportToolsTest;
import load.price.PriceImportToolsTest;
import load.profile.ProfileAddressImportToolsTest;
import load.profile.ProfileImportToolsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Dmitry Golubev
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
		{
				InvImportToolsTest.class,
				PriceImportToolsTest.class,
				ProfileImportToolsTest.class,
				ProfileAddressImportToolsTest.class,

				/* VERSIONED */
				IngredientImportToolsTest.class,
				ProductImportToolsTest.class,
				CatalogImportToolsTest.class,
				CategoryImportToolsTest.class,
				SubCategoryImportToolsTest.class
		}
)
public class ImportToolsSuiteVer {
}
