package load.suite;

/**
 * @author Dmitry Golubev
 */

import load.catalog.category.SubCategoryParseToolsTest;
import load.catalog.ingredient.IngredientParseToolsTest;
import load.catalog.product.ProductParseToolsTest;
import load.inventory.InvParseToolsTest;
import load.price.PriceParseToolsTest;
import load.profile.ProfileAddressParseToolsTest;
import load.profile.ProfileParseToolsTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Dmitry Golubev
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
		{
				InvParseToolsTest.class,
				PriceParseToolsTest.class,
				ProfileAddressParseToolsTest.class,
				ProfileParseToolsTest.class,

				/* VERSIONED */
				IngredientParseToolsTest.class,
				ProductParseToolsTest.class,
				SubCategoryParseToolsTest.class
		}
)
public class ParseToolsSuiteVer {
}
