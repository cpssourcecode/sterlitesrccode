package load.catalog.category;

import load.catalog.CatalogLoadNucleusTestHelper;
import load.common.ImportToolsTestHelper;

/**
 * @author Dmitry Golubev
 */
public class CategoryImportToolsTest extends CatalogLoadNucleusTestHelper {

	private ImportToolsTestHelper mHelper = new ImportToolsTestHelper();

	public void testFileProcessing() {
		mHelper.performTestWorkWithFiles( getCategoryImportTools() );
	}
}
