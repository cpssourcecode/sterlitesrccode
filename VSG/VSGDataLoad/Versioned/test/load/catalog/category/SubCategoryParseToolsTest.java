package load.catalog.category;

import load.catalog.CatalogLoadNucleusTestHelper;
import vsg.load.info.FieldsInfo;
import vsg.load.loader.Record;
import vsg.load.tools.IParserTools;

import java.util.Iterator;

/**
 * @author Dmitry Golubev
 * */
public class SubCategoryParseToolsTest extends CatalogLoadNucleusTestHelper {

	private String[] row = new String[] { "categoryId", "displayName"};

	public void testSubcategoryParseRecord() {

		FieldsInfo fieldsInfo = getSubCategoryFieldsInfo();
		IParserTools parserTools = getParserTools();

		Record record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);

		assertNotNull(record);

		// base null check
		assertNotNull(record.getId());
		assertNotNull(record.getValuesInitial());
		assertNotNull(record.toString());

		Iterator iterator = record.getValuesInitial().values().iterator();

		assertEquals( 2, record.getValuesInitial().size() );
		assertEquals( 3, record.getValues().size() );

		assertEquals( row[0], iterator.next() );
		assertEquals( row[1], iterator.next() );

		assertEquals( row[0], record.getValueInitial("id") );
		assertEquals( row[1], record.getValueInitial("displayName") );

		assertEquals( row[0], record.getId() );
		assertEquals( row[0], record.getValue("id") );
		assertEquals( row[1], record.getValue("displayName") );
		assertEquals( row[0], record.getValue("seoId") );

	}

}