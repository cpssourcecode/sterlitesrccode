package load.catalog.product;

import load.catalog.CatalogLoadNucleusTestHelper;
import load.common.ImportToolsTestHelper;

/**
 * @author Dmitry Golubev
 */
public class ProductImportToolsTest extends CatalogLoadNucleusTestHelper {

	private ImportToolsTestHelper mHelper = new ImportToolsTestHelper();

	public void testFileProcessing() {
		mHelper.performTestWorkWithFiles( getProductImportTools() );
	}
}
