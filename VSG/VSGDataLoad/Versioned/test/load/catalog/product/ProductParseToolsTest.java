package load.catalog.product;

import junit.framework.Assert;
import load.catalog.CatalogLoadNucleusTestHelper;
import vsg.load.loader.Record;
import vsg.load.tools.IParserTools;

/**
 * @author Dmitry Golubev
 */
public class ProductParseToolsTest extends CatalogLoadNucleusTestHelper {

	public void testProductFieldsInfoParsing() {
		IParserTools parseTools = getParserTools();

		String[] row = new String[] { "Oracle_repositoryid", "Oracle_legacyid", "Oracle_add_ice_cooler",
				"Oracle_alt_image1",
				"Oracle_alt_image2", "Oracle_alt_image3", "Oracle_alt_image4", "Oracle_alt_image5", "Oracle_autoship",
				"Oracle_brandID", "Oracle_cPopulate_highlights", "Oracle_description", "Oracle_displayName",
				"Oracle_dosageDescription", "Oracle_flavor", "Oracle_form", "Oracle_freeshipping",
				"Oracle_full_main_image", "Oracle_healthneeds", "Oracle_highlights", "Oracle_ingredients",
				"Oracle_ingredientsDecription", "Oracle_isNew", "Oracle_isTaxable", "Oracle_keywords",
				"Oracle_limit_per_order", "Oracle_mainIngredientID", "Oracle_newDay", "Oracle_notprop65",
				"Oracle_onsale", "Oracle_parentCategoryID", "Oracle_pd_hide_from_mobile", "Oracle_precautions",
				"Oracle_price_capsule", "Oracle_price_serving", "Oracle_productType", "Oracle_serving_size",
				"Oracle_ship_weight", "Oracle_size_amount", "Oracle_slow_vendor", "Oracle_startDate",
				"Oracle_strength", "Oracle_UPC", "Oracle_video_url" };

		Record record = parseTools.getRecord(getProductFieldsInfo(), row, 0);
		parseTools.initRecord(record, getProductFieldsInfo());

		Assert.assertTrue(record.getId().equals("Oracle_repositoryid"));
		Assert.assertTrue(record.getValueInitial("legacyId").equals("Oracle_legacyid"));
		Assert.assertTrue(record.getValueInitial("isIce").equals("Oracle_add_ice_cooler"));
		Assert.assertTrue(record.getValueInitial("firstImg").equals("Oracle_alt_image1"));
		Assert.assertTrue(record.getValueInitial("secondImg").equals("Oracle_alt_image2"));
		Assert.assertTrue(record.getValueInitial("thirdImg").equals("Oracle_alt_image3"));
		Assert.assertTrue(record.getValueInitial("fourthImg").equals("Oracle_alt_image4"));
		Assert.assertTrue(record.getValueInitial("fifthImg").equals("Oracle_alt_image5"));
		Assert.assertTrue(record.getValueInitial("isAutoship").equals("Oracle_autoship"));
		Assert.assertTrue(record.getValueInitial("manufacturer").equals("Oracle_brandID"));
		Assert.assertTrue(record.getValueInitial("pfDescription").equals("Oracle_description"));
		Assert.assertTrue(record.getValueInitial("displayName").equals("Oracle_displayName"));
		Assert.assertTrue(record.getValueInitial("dosageDescr").equals("Oracle_dosageDescription"));
		Assert.assertTrue(record.getValueInitial("isFreeShip").equals("Oracle_freeshipping"));
		Assert.assertTrue(record.getValueInitial("healthneeds").equals("Oracle_healthneeds"));
		Assert.assertTrue(record.getValueInitial("ingredients").equals("Oracle_ingredients"));
		Assert.assertTrue(record.getValueInitial("ingredientDescr").equals("Oracle_ingredientsDecription"));
		Assert.assertTrue(record.getValueInitial("isNew").equals("Oracle_isNew"));
		Assert.assertTrue(record.getValueInitial("isTax").equals("Oracle_isTaxable"));
		Assert.assertTrue(record.getValueInitial("newDay").equals("Oracle_newDay"));
		Assert.assertTrue(record.getValueInitial("isNotProp65").equals("Oracle_notprop65"));
		Assert.assertTrue(record.getValueInitial("isMobileHide").equals("Oracle_pd_hide_from_mobile"));
		Assert.assertTrue(record.getValueInitial("precautions").equals("Oracle_precautions"));
		Assert.assertTrue(record.getValueInitial("productType").equals("Oracle_productType"));

		Assert.assertTrue(record.getValueInitial("seoId") == null);

		Assert.assertTrue(record.getValue("isIce").equals(Boolean.FALSE));
		Assert.assertTrue(record.getValue("isAutoship").equals(Boolean.FALSE));
		Assert.assertTrue(record.getValue("isFreeShip").equals(Boolean.FALSE));
		Assert.assertTrue(record.getValue("isNew").equals(Boolean.FALSE));
		Assert.assertTrue(record.getValue("isTax").equals(Boolean.FALSE));
		Assert.assertTrue(record.getValue("isNotProp65").equals(Boolean.FALSE));
		Assert.assertTrue(record.getValue("isMobileHide").equals(Boolean.FALSE));
		Assert.assertTrue(record.getValue("seoId").equals("Oracle_legacyid"));

		// skipped values
		Assert.assertFalse(record.getValues().containsKey("populateHighlights"));
		Assert.assertFalse(record.getValues().containsKey("keywords"));
		Assert.assertFalse(record.getValues().containsKey("fullMainImg"));
		Assert.assertFalse(record.getValues().containsKey("mainIngredient"));
		Assert.assertFalse(record.getValues().containsKey("parentCategory"));
		Assert.assertFalse(record.getValues().containsKey("highlights"));

		// subcategory values
		Assert.assertFalse(record.getValues().containsKey("sku.flavor"));
		Assert.assertFalse(record.getValues().containsKey("flavor"));

		Assert.assertFalse(record.getValues().containsKey("flavor"));
		Assert.assertFalse(record.getValues().containsKey("form"));
		Assert.assertFalse(record.getValues().containsKey("orderLimit"));
		Assert.assertFalse(record.getValues().containsKey("onSale"));
		Assert.assertFalse(record.getValues().containsKey("capsulePrice"));
		Assert.assertFalse(record.getValues().containsKey("servingPrice"));
		Assert.assertFalse(record.getValues().containsKey("servingSize"));
		Assert.assertFalse(record.getValues().containsKey("shipWeight"));
		Assert.assertFalse(record.getValues().containsKey("sizeAmount"));
		Assert.assertFalse(record.getValues().containsKey("slowVendor"));
		Assert.assertFalse(record.getValues().containsKey("startDate"));
		Assert.assertFalse(record.getValues().containsKey("strength"));
		Assert.assertFalse(record.getValues().containsKey("manufacturer_part_number"));
		Assert.assertFalse(record.getValues().containsKey("videoUrl"));



		// subcategory
		Assert.assertTrue(record.getSubRecords()!=null);
		Assert.assertTrue(record.getSubRecords().size() == 1);
		Record subRecordSku = record.getSubRecords().get("sku");
		Assert.assertTrue(subRecordSku != null);


		Assert.assertTrue(subRecordSku.getId().equals( record.getId() ));
		Assert.assertTrue(subRecordSku.getIndex() == record.getIndex() );

		Assert.assertTrue(subRecordSku.getValueInitial("flavor").equals("Oracle_flavor"));
		Assert.assertTrue(subRecordSku.getValueInitial("form").equals("Oracle_form"));
		Assert.assertTrue(subRecordSku.getValueInitial("orderLimit").equals("Oracle_limit_per_order"));
		Assert.assertTrue(subRecordSku.getValueInitial("onSale").equals("Oracle_onsale"));
		Assert.assertTrue(subRecordSku.getValueInitial("capsulePrice").equals("Oracle_price_capsule"));
		Assert.assertTrue(subRecordSku.getValueInitial("servingPrice").equals("Oracle_price_serving"));
		Assert.assertTrue(subRecordSku.getValueInitial("servingSize").equals("Oracle_serving_size"));
		Assert.assertTrue(subRecordSku.getValueInitial("shipWeight").equals("Oracle_ship_weight"));
		Assert.assertTrue(subRecordSku.getValueInitial("sizeAmount").equals("Oracle_size_amount"));
		Assert.assertTrue(subRecordSku.getValueInitial("slowVendor").equals("Oracle_slow_vendor"));
		Assert.assertTrue(subRecordSku.getValueInitial("startDate").equals("Oracle_startDate"));
		Assert.assertTrue(subRecordSku.getValueInitial("strength").equals("Oracle_strength"));
		Assert.assertTrue(subRecordSku.getValueInitial("manufacturer_part_number").equals("Oracle_UPC"));
		Assert.assertTrue(subRecordSku.getValueInitial("videoUrl").equals("Oracle_video_url"));

		// prod attrs
		Assert.assertFalse(subRecordSku.getValues().containsKey("legacyId"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("isIce"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("firstImg"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("secondImg"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("thirdImg"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("fourthImg"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("fifthImg"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("isAutoship"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("manufacturer"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("pfDescription"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("displayName"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("dosageDescr"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("isFreeShip"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("healthneeds"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("highlights"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("ingredients"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("ingredientDescr"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("isNew"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("isTax"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("keywords"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("newDay"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("isNotProp65"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("isMobileHide"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("precautions"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("productType"));
		Assert.assertFalse(subRecordSku.getValues().containsKey("seoId"));



	}

	public void testRealProductFieldsInfoParsing() {
		IParserTools parseTools = getParserTools();

		String[] row = new String[] {
				"CORRECT_ID",
				"super-dha-liquid-150ml5oz-by-seroyal",
				"No",
				"/mod_images/productfullimage/super-dha-liquid-150ml5oz-by-seroyal-extra1",
				"/mod_images/productfullimage/super-dha-liquid-150ml5oz-by-seroyal-extra2",
				"/mod_images/productfullimage/super-dha-liquid-150ml5oz-by-seroyal-extra3",
				"/mod_images/productfullimage/super-dha-liquid-150ml5oz-by-seroyal-extra4",
				"/mod_images/productfullimage/super-dha-liquid-150ml5oz-by-seroyal-extra5",
				"",
				"seroyal",
				"item-artificial-color|item-artificial-flavor|item-corn-free|item-dairy-free|item-gluten-free|item-GMP-Certified|item-hypoallergenic|item-organic-product|item-salt-sodium-free|starch-free",
			    "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://lib.store.yahoo.net/lib/yhst-37598795206756/descriptionfield.css\"><p id=\"description\">Super DHA Liquid is a highly-concentrated, pharmaceutical-grade fish oil providing the necessary ratio of docosahexaenoic acid (DHA) over eicosapenaenoic acid (EPA), specifically formulated to ensure maximum support for brain and neural development from pregnancy to adulthood. This complex manufacturing process eliminates environmental pollutants and allows for the absorption of high concentrations of EPA and DHA without gastric distress. In addition, a patented lipid emulsification and micro-encapsulation process is used resulting in faster and greater absorption of long-chain fatty acids. Studies indicate that DHA plays a crucial role in the repair and regeneration of neural tissue, which affects retinal, visual, learning and memory function. Research shows that DHA decreases inflammatory mediators making it effective in all inflammatory conditions such as ear infections and skin disorders. EPA is important for the regulation of blood pressure, blood clotting, and immune and inflammatory responses. Super DHA Liquid is indicated to assist in the development of the neural tube and retina; for the prevention of spina bifida; for all neurovegetative conditions, including Parkinson&rsquo;s disease and schizophrenia; and for cardiovascular disease, eczema, psoriasis, and depression. <BR><BR></p><BR><p>These statements have not been evaluated by the Food and Drug Administration (FDA). These products are not meant to diagnose, treat or cure any disease or medical condition. Please consult your doctor before starting any exercise or nutritional supplement program or before using these or any product during pregnancy or if you have a serious medical condition.</p>",
				"Super DHA Liquid 5 oz (150 ml)",
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"http://lib.store.yahoo.net/lib/yhst-37598795206756/descriptionfield.css\"><p id=\"description\">Adults: Take one teaspoon two times daily with meals, or as recommended by your health care practitioner.<BR><BR></p>",
                "Orange",
				"liquid",
				"Yes",
				"/mod_images/productfullimage/super-dha-liquid-150ml5oz-by-seroyal",
				"",
				"item-artificial-color|item-artificial-flavor|item-corn-free|item-dairy-free|item-gluten-free|item-GMP-Certified|item-hypoallergenic|item-organic-product|item-salt-sodium-free|starch-free",
				"",
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"http://lib.store.yahoo.net/lib/yhst-37598795206756/descriptionfield.css\"><p id=\"description\">Each Teaspoon (5 ml) Contains:<BR><BR>Concentrated Pure Fish Oil 4383 mg  <BR><BR>From Wild Tuna   <BR><BR>*Yielding   <BR><BR>DHA (Docosahexaenoic Acid) 1200 mg  <BR><BR>EPA (Eicosapentaenoic Acid) 260 mg  <BR>Vitamin E (Natural Mixed Tocopherols)   <BR><BR>Naturally Flavored with Essential Oil of Orange   <BR>Sunflower Oil <BR><BR></p>",
				"",
				"No",
				"",
				"",
				"Omega-3 Fatty Acids",
				"",
				"Yes",
				"",
				"pureformulas",
				"No",
				"<link rel=\"stylesheet\" type=\"text/css\" href=\"http://lib.store.yahoo.net/lib/yhst-37598795206756/descriptionfield.css\"><p id=\"description\">Consult your healthcare professional.<BR><BR></p>",
				"",
				"",
				"",
				"",
				"8.4",
				"5 fl oz",
				"Yes",
				"",
				"",
				"883196114818",
				"http://www.youtube.com/watch_popup?v=tAm6QzH9HQU&vq=medium"
		};

		Record record = parseTools.getRecord(getProductFieldsInfo(), row, 0);
		parseTools.initRecord(record, getProductFieldsInfo());

		Record subRecordSku = record.getSubRecords().get("sku");

		Assert.assertTrue(subRecordSku.getValueInitial("manufacturer_part_number").equals("883196114818"));
		Assert.assertTrue(subRecordSku.getValue("manufacturer_part_number").equals("883196114818"));
	}

}
