package load.catalog.ingredient;

import load.catalog.CatalogLoadNucleusTestHelper;
import load.common.ImportToolsTestHelper;

/**
 * @author Dmitry Golubev
 */
public class IngredientImportToolsTest extends CatalogLoadNucleusTestHelper {

	private ImportToolsTestHelper mHelper = new ImportToolsTestHelper();

	public void testIngredientFileProcessing() {
		mHelper.performTestWorkWithFiles(getIngredientImportTools());
	}

}
