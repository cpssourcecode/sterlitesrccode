package load.catalog.ingredient;

import load.catalog.CatalogLoadNucleusTestHelper;
import vsg.load.info.FieldsInfo;
import vsg.load.loader.Record;
import vsg.load.tools.IParserTools;

import java.util.Iterator;

/**
 * @author Dmitry Golubev
 * */
public class IngredientParseToolsTest extends CatalogLoadNucleusTestHelper {

	private String[] row = new String[] { "ing_id_0001", "ing display name 00001", "ing description 00001",
					"ing strength 00001"};

	public void testIngredientListFormRecord() {

		FieldsInfo fieldsInfo = getIngredientFieldsInfo();
		IParserTools parserTools = getParserTools();

		Record record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);

		assertNotNull(record);

		// base null check
		assertNotNull(record.getId());
		assertNotNull(record.getValuesInitial());
		assertNotNull(record.toString());

		Iterator iterator = record.getValuesInitial().values().iterator();

		assertEquals( row[0], iterator.next() );
		assertEquals( row[1], iterator.next() );
		assertEquals( row[2], iterator.next() );
		assertEquals( row[3], iterator.next() );

		assertEquals( row[0], record.getValueInitial("ingredientId") );
		assertEquals( row[1], record.getValueInitial("name") );
		assertEquals( row[2], record.getValueInitial("description") );
		assertEquals( row[3], record.getValueInitial("strength") );

		assertEquals( row[0], record.getId() );
		assertEquals( row[0], record.getValue("ingredientId") );
		assertEquals( row[1], record.getValue("name") );
		assertEquals( row[2], record.getValue("description") );
		assertEquals( row[3], record.getValue("strength") );

	}

}
