package load.catalog.ingredient;

import load.catalog.CatalogLoadNucleusTestHelper;
import vsg.load.info.FieldsInfo;
import vsg.load.info.ValidationResult;
import vsg.load.loader.Record;
import vsg.load.tools.IParserTools;
import vsg.load.tools.IValidationTools;

/**
 * @author Dmitry Golubev
 */
public class IngredientValidationToolsTest extends CatalogLoadNucleusTestHelper {

	private void assertValid(ValidationResult pValidationResult) {
		assertNotNull(pValidationResult);
		assertTrue(pValidationResult.isValid());
		assertNull(pValidationResult.getMsg());
	}

	private void assertNotValid(ValidationResult pValidationResult) {
		assertNotNull(pValidationResult);
		assertFalse(pValidationResult.isValid());
		assertNotNull(pValidationResult.getMsg());
	}

	public void testIngredientRecordValidate() {
		IParserTools parserTools = getParserTools();
		IValidationTools validationTools = getValidationTools();
		FieldsInfo fieldsInfo = getIngredientFieldsInfo();

		String[] row = new String[] { "ing_id_0001", "ing display name 00001", "ing description 00001",
				"ing strength 00001"};

		Record record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);
		assertValid( validationTools.validateRecord(record, fieldsInfo) );

		row = new String[] {"ing_id_0001", "ing display name 00001", "", ""};

		record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);
		assertValid( validationTools.validateRecord(record, fieldsInfo) );

		row = new String[] {"ing_id_0001", "ing display name 00001", null, null};

		record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);
		assertValid( validationTools.validateRecord(record, fieldsInfo) );

		row = new String[] { "", "ing display name 00001", "ing description 00001", "ing strength 00001"};

		record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);
		assertNotValid(validationTools.validateRecord(record, fieldsInfo));

		row = new String[] { null, "ing display name 00001", "ing description 00001", "ing strength 00001"};

		record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);
		assertNotValid(validationTools.validateRecord(record, fieldsInfo));

		row = new String[] { "ing_id_0001", "", "ing description 00001", "ing strength 00001"};

		record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);
		assertNotValid(validationTools.validateRecord(record, fieldsInfo));

		row = new String[] { "ing_id_0001", null, "ing description 00001", "ing strength 00001"};

		record = parserTools.getRecord(fieldsInfo, row);
		parserTools.initRecord(record, fieldsInfo);
		assertNotValid( validationTools.validateRecord(record, fieldsInfo) );

	}

}
