package load.catalog.ingredient;

import load.catalog.CatalogLoadNucleusTestHelper;
import load.common.IImportServiceTestHelper;
import vsg.load.file.IWriter;
import vsg.load.file.csv.CSVBase;
import vsg.load.file.csv.CSVWriter;
import vsg.load.tools.IImportTools;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Golubev
 */
public class IngredientImportServiceTest extends CatalogLoadNucleusTestHelper {

	IImportServiceTestHelper mHelper = new IImportServiceTestHelper() {

		@Override
		public void fillFileWithData(IImportTools pImportTools, File pFile) {
			IWriter writer = pImportTools.getWriter(pFile);
			assertNotNull(writer);
			((CSVWriter)writer).setQuoteChar(CSVBase.NO_QUOTE_CHARACTER);

			List<String[]> rows = new ArrayList<String[]>();
			for(int i=0;i<1000;i++) {
				rows.add( new String[] { "test_ing_id_000"+i, "test_ing_display_name_000"+i,
						"test_ing_description_000"+i, "test_ing_strength_000"+i });
			}

			writer.writeAll(rows);

			writer.close();
		}
	};

	public void testInsertUpdateDeleteImport() {
		mHelper.testInsertUpdateDeleteImport(getIngredientImportTools(), getCatalogImportService(), 1000);
	}

}
