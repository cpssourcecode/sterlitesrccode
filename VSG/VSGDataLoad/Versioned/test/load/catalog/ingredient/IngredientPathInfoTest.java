package load.catalog.ingredient;

import load.catalog.CatalogLoadNucleusTestHelper;
import load.common.PathInfoTestHelper;

/**
 * @author Dmitry Golubev
 */
public class IngredientPathInfoTest extends CatalogLoadNucleusTestHelper {

	PathInfoTestHelper mHelper = new PathInfoTestHelper();

	public void testInitialIngredientRequirements() {
		mHelper.performTestInitialRequirements(getIngredientPathInfo(), "ingredient");
	}

}
