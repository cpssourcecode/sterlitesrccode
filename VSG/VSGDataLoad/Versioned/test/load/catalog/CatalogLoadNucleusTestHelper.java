package load.catalog;

import load.LoadNucleusTestHelper;
import vsg.load.connection.IConnector;
import vsg.load.info.FieldsInfo;
import vsg.load.info.PathInfo;
import vsg.load.service.IImportService;
import vsg.load.tools.IImportTools;
import vsg.load.tools.IParserTools;
import vsg.load.tools.IValidationTools;

/**
 * @author Dmitry Golubev
 */
public class CatalogLoadNucleusTestHelper extends LoadNucleusTestHelper {

	public FieldsInfo getSubCategoryFieldsInfo() {
		return (FieldsInfo) getNucleus().resolveName("/vsg/load/catalog/category/info/SubCategoryFieldsInfo", true);
	}

	public IImportTools getSubCategoryImportTools() {
		return (IImportTools) getNucleus().resolveName("/vsg/load/catalog/category/tools/SubCategoryImportTools", true);
	}

	public IImportTools getCategoryImportTools() {
		return (IImportTools) getNucleus().resolveName("/vsg/load/catalog/category/tools/CategoryImportTools", true);
	}

	public IImportTools getCatalogImportTools() {
		return (IImportTools) getNucleus().resolveName("/vsg/load/catalog/tools/CatalogImportTools", true);
	}

	public FieldsInfo getIngredientFieldsInfo() {
		return (FieldsInfo) getNucleus().resolveName("/vsg/load/catalog/ingredient/info/IngredientFieldsInfo", true);
	}

	public FieldsInfo getProductFieldsInfo() {
		return (FieldsInfo) getNucleus().resolveName("/vsg/load/catalog/product/info/ProductFieldsInfo", true);
	}

	public IParserTools getParserTools() {
		return (IParserTools) getNucleus().resolveName("/vsg/load/tools/ParserTools", true);
	}

	public IImportTools getIngredientImportTools() {
		return (IImportTools) getNucleus().resolveName("/vsg/load/catalog/ingredient/tools/IngredientImportTools", true);
	}

/*
	public IImportTools getCategoryCategoryImportTools() {
		return (IImportTools) getNucleus().resolveName("/vsg/load/catalog/category/tools/CategoryCategoryImportTools",
				true);
	}
*/

	public IImportTools getProductImportTools() {
		return (IImportTools) getNucleus().resolveName("/vsg/load/catalog/product/tools/ProductImportTools", true);
	}

	public PathInfo getIngredientPathInfo() {
		return (PathInfo) getNucleus().resolveName("/vsg/load/catalog/ingredient/info/IngredientPathInfo", true);
	}

	public IValidationTools getValidationTools() {
		return (IValidationTools) getNucleus().resolveName("/vsg/load/tools/ValidationTools", true);
	}

/*
	public IRepositoryLoader getIngredientLoader() {
		return (IRepositoryLoader) getNucleus().resolveName("/vsg/load/catalog/ingredient/loader/IngredientLoader", true);
	}
*/

	public IConnector getCatalogConnector() {
		return (IConnector) getNucleus().resolveName("/vsg/load/catalog/connection/CatalogConnector", true);
	}

	public IImportService getCatalogImportService() {
		return (IImportService) getNucleus().resolveName("/vsg/load/catalog/service/CatalogService", true);
	}

}
