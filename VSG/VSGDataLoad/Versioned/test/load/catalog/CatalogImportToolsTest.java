package load.catalog;

import load.common.ImportToolsTestHelper;

/**
 * @author Dmitry Golubev
 */
public class CatalogImportToolsTest extends CatalogLoadNucleusTestHelper {

	private ImportToolsTestHelper mHelper = new ImportToolsTestHelper();

	public void testFileProcessing() {
		mHelper.performTestWorkWithFiles( getCatalogImportTools() );
	}
}
