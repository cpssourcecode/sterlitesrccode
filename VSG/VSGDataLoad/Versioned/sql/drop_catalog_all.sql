truncate table dbc_measurement;
truncate table dbc_manufacturer;

truncate table dcs_foreign_cat;
truncate table dcs_config_opt;
truncate table dcs_conf_options;
--truncate table dcs_config_prop;
delete from dcs_config_prop;

--SKU
truncate table dcs_sku_conf;
truncate table dcs_sku_replace;
truncate table dcs_sku_aux_media;
truncate table dcs_sku_media;
truncate table dcs_sku_bndllnk;
--truncate table dcs_sku_link;
delete from dcs_sku_link;

truncate table dcs_sku_attr;
truncate table dcs_prd_ancestors;
truncate table dcs_prd_upslprd;
truncate table dcs_prd_rltdprd;
truncate table dcs_prd_groups;
truncate table dcs_prd_skuattr;
truncate table dcs_prd_chldsku;
truncate table dcs_prd_aux_media;
truncate table dcs_product_rltd_media;
truncate table dcs_product_rltd_article;
truncate table dcs_prd_media;
truncate table dcs_prd_keywrds;
truncate table dcs_cat_aux_media;
truncate table dcs_cat_media;
truncate table dcs_cat_keywrds;
truncate table dcs_cat_rltdcat;
truncate table dcs_cat_ancestors;
truncate table dcs_cat_chldcat;
truncate table dcs_cat_chldprd;
truncate table dcs_cat_groups;
--truncate table dcs_sku;
truncate table dcs_sku_sites;
truncate table dcs_product_sites;
truncate table dcs_category_sites;
truncate table dcs_catalog_sites;
truncate table dcs_sku_catalogs;
truncate table dcs_prd_catalogs;
truncate table dcs_cat_catalogs;
truncate table dcs_prd_anc_cats;
truncate table dcs_prd_prnt_cats;
truncate table dcs_cat_prnt_cats;
truncate table dcs_cat_anc_cats;
truncate table dcs_ctlg_anc_cats;
delete from dcs_sku;

--PRODUCT
truncate table dcs_product_acl;
--truncate table dcs_product;
truncate table CPS_PRODUCT_ALIAS;
truncate table CPS_PRODUCT;
truncate table CPS_PRODUCT_ALL;
truncate table CPS_PRODUCT_IMAGEONLY;
truncate table CPS_PRODUCT_PRICESVC;
truncate table CPS_PRODUCT_UNILOG;
truncate table CPS_PRODUCT_ATTRIBUTES;
truncate table CPS_PRODUCT_ATTRIBUTES_UOM;

--truncate table CPS_CONTENT;
delete from CPS_CONTENT;

truncate table DCS_UPSELL_PRODS;
delete from dcs_product;

--CATEGORY

truncate table dcs_category_acl;
--truncate table dcs_category;
truncate table CPS_CATEGORY;
truncate table CPS_CAT_CONTENT;
truncate table CPS_CAT_TAB;
truncate table dcs_root_subcats;
truncate table dcs_allroot_cats;
truncate table dcs_root_cats;
delete from dcs_category;

--tab
truncate table CPS_TAB_CONTENT;
truncate table CPS_TAB_PRD;
truncate table CPS_TAB_CAT;
--truncate table CPS_TAB;
delete from CPS_TAB;


--testimonial
truncate table CPS_TESTIMONIAL;

--testimonialsFolder
truncate table cps_testimonials_folder;

--cps_faq
truncate table cps_faq_category;
truncate table cps_faq_question;

--cps_affilations
truncate table cps_affilations;

--cps_prod_info
truncate table cps_prod_info_category;
truncate table cps_prod_info_item;
truncate table cps_prod_info_item_cat_map;

-- cps_insvid
truncate table cps_insvid_roles;
truncate table cps_insvid_items;
truncate table cps_insvid_item_role_map;

--MEDIA
delete from DCS_PRM_CLS_QLF;
delete from DCS_CLOSE_QUALIF;
truncate table dcs_media_txt;
truncate table dcs_media_bin;
truncate table dcs_media_ext;
--truncate table dcs_media;
delete from dcs_media;
--truncate table dcs_folder;
delete from dcs_folder;

--catalog custom
truncate table dcs_ind_anc_ctlgs;
truncate table dcs_dir_anc_ctlgs;
truncate table dcs_catfol_sites;
truncate table dcs_catfol_chld;
truncate table dcs_child_fol_cat;
--truncate table dcs_gen_fol_cat;
delete from dcs_gen_fol_cat;
truncate table dcs_skuinfo_rplc;
truncate table dcs_sku_skuinfo;
truncate table dcs_prdinfo_anc;
truncate table dcs_prdinfo_rdprd;
truncate table dcs_prd_prdinfo;
truncate table dcs_catinfo_anc;
truncate table dcs_cat_catinfo;
truncate table dcs_cat_subroots;
truncate table dcs_cat_subcats;
--truncate table dcs_sku_info;
delete from dcs_sku_info;
--truncate table dcs_product_info;
delete from dcs_product_info;
truncate table dcs_category_info;
--truncate table dcs_catalog;
delete from dcs_catalog;

--price
truncate table dcs_plfol_chld;
truncate table dcs_child_fol_pl;
--truncate table dcs_gen_fol_pl;
delete from dcs_gen_fol_pl;
truncate table dcs_price_level;
truncate table dcs_price_levels;
truncate table dcs_price;
--truncate table dcs_complex_price;
delete from dcs_complex_price;
--truncate table dcs_price_list;
delete from dcs_price_list;
-- promos
delete from dcs_prm_site_grps;
delete from dcs_prm_sites;
--sites
truncate table VSG_SITE;
truncate table site_group_shareable_types;
truncate table site_group_sites;
--truncate table site_group;
delete from site_group;
truncate table site_types;
truncate table site_additional_urls;
--truncate table site_configuration;
delete from site_configuration;
--truncate table dcs_site;
delete from dcs_site;

-- promo content CPS
truncate table cps_promo_segments;
truncate table cps_promo_keywords;
truncate table vsg_promo_categories;
truncate table vsg_promo_sites;
--truncate table vsg_promo_item;
delete from vsg_promo_item;

-- categoriesContent
truncate table vsg_categories_content;

-- productsContent
truncate table vsg_html_content;

-- htmlContent
truncate table vsg_html_content;

-- baseContentItem
--truncate table vsg_base_content;
delete from vsg_base_content;


--pagesFolder
truncate table cps_folder;

--staticPageTemplate
--truncate table cps_page_template_sites;
delete from cps_page_template_sites;
truncate table cps_page_template_pages;
--truncate table cps_page_template;
delete from cps_page_template;

-- locations
delete from dcs_location_store;
delete from cps_store;
delete from dcs_location_rltd_article;
delete from dcs_location_rltd_media;
delete from dcs_loc_site_grps;
delete from dcs_location_sites;
delete from dcs_location_addr;

delete from cps_store_images;
delete from cps_stores;

-- user messages
--truncate table CPS_USER_MESSAGE;
delete from CPS_USER_MESSAGE;
delete from CPS_PRODUCT_SPECIFIC_MESSAGE;

-- Brand and Menu
delete from CPS_MENU_CAT;
delete from CPS_MENU_BRAND;
delete from CPS_MENU_ITEM;
delete from CPS_MENU_SUBFOLDER;
delete from CPS_MENU_FOLDER;
delete from CPS_CATALOG_MENU;
delete from CPS_MENU;
delete from CPS_BRAND_ATTRIBUTES;
delete from CPS_BRAND;

-- Facets
delete from CPS_CAT_FACETS;

-- Associated products
delete from CPS_CAT_ASSOCIATED_PRODUCTS;

delete from CPS_PRODUCT_FEATURES;
delete from CPS_PRODUCT_STANDARDS;

delete from CPS_SITE_CONFIG;

delete from CPS_ROOT_CATS;
delete from CPS_CAT_CUSTALSBOUGHT_PROD;
delete from CPS_PRD_CUSTALSBOUGHT_PROD;



--clean workspaces/asset locks/devline
delete from AVM_ASSET_LOCK;
delete from AVM_WORKSPACE;
delete from AVM_DEVLINE where not (type = 0);
commit;
