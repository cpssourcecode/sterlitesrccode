Delete from epub_deployment; -- Deleting pending deployment.
DELETE FROM das_depl_depldat;-- Deleting Meta data files from BCC.
DELETE FROM das_depl_options;
DELETE FROM das_depl_repmaps;
DELETE FROM das_depl_item_ref;
DELETE FROM das_depl_progress;
DELETE FROM das_thread_batch;
DELETE FROM das_deploy_data;
DELETE FROM das_dd_markers;
DELETE FROM das_deploy_mark;
DELETE FROM das_rep_mark;
DELETE FROM das_file_mark;
DELETE FROM das_dep_fail_info;
DELETE FROM das_deployment;


--Agents Clean UP
delete from EPUB_TR_REP_TBL_MP;
delete from EPUB_TR_AGENTS;
delete from EPUB_TARGET;
delete from EPUB_TL_TARGETS;
delete from EPUB_PRJ_TG_SNSHT;
delete from EPUB_PR_HISTORY;
delete from EPUB_AGENT_TRNPRT;
delete from EPUB_INCLUD_ASSET;
delete from EPUB_PRINC_ASSET;
delete from EPUB_AGENT;
delete from EPUB_PR_TG_STATUS;

--Project Clean UP
delete from EPUB_PROJECT;
delete from EPUB_PR_TG_AP_TS;
--delete from EPUB_DEPLOYMENT;
delete from EPUB_PR_TG_DP_TS;
delete from EPUB_PROC_HISTORY;
delete from EPUB_PR_TG_DP_ID;
delete from EPUB_INT_PRJ_HIST;
delete from EPUB_PROC_TASKINFO;
delete from EPUB_WORKFLOW_STRS;
delete from EPUB_IND_WORKFLOW;
delete from EPUB_PROCESS;
delete from EPUB_PR_HISTORY;
delete from EPUB_PROC_HISTORY;
delete from avm_asset_lock;

delete from EPUB_DEP_LOG;
delete from EPUB_DEPLOY_PROJ;
commit;