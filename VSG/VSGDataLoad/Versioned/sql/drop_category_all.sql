truncate table dcs_cat_aux_media;
truncate table dcs_cat_media;
truncate table dcs_cat_keywrds;
truncate table dcs_cat_rltdcat;
truncate table dcs_cat_ancestors;
truncate table dcs_cat_chldcat;
truncate table dcs_cat_chldprd;
truncate table dcs_cat_groups;
truncate table dcs_category_sites;
truncate table dcs_cat_prnt_cats;
truncate table dcs_cat_anc_cats;
truncate table dcs_category_acl;

truncate table dcs_root_cats;
delete from dcs_category;
