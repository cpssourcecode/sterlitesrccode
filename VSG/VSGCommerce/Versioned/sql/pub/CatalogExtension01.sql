--drop table vsg_promo_sites;
--drop table vsg_promo_categories;
--drop table vsg_promo_item;

CREATE TABLE vsg_promo_item (
	promo_id				VARCHAR2(40)		NOT NULL,
	display_name			VARCHAR2(254)		NOT NULL,
	parent_folder_id		VARCHAR2(40)		NULL,
	description				VARCHAR2(1000)		NULL,
	slot_size				VARCHAR2(40)		NULL,
	html					CLOB				NULL,
	start_date				TIMESTAMP(6)		NULL,
	end_date				TIMESTAMP(6)		NULL,
	creation_date			TIMESTAMP(6)		NULL,
	path					VARCHAR2(254)		NULL,
	asset_version			NUMBER(19)			NOT NULL,
	workspace_id			VARCHAR2(40)		NOT NULL,
	branch_id				VARCHAR2(40)		NOT NULL,
	is_head					NUMBER(1)			NOT NULL,
	version_deleted			NUMBER(1)			NOT NULL,
	version_editable		NUMBER(1)			NOT NULL,
	pred_version			NUMBER(19)			NULL,
	checkin_date			TIMESTAMP			NULL,
	constraint vsg_promo_item_pk primary key (asset_version, promo_id)
);

CREATE INDEX vsg_promo_item_ws_idx on vsg_promo_item (workspace_id);
CREATE INDEX vsg_promo_item_ck_idx on vsg_promo_item (checkin_date);

CREATE TABLE vsg_promo_categories(
	asset_version			NUMBER(19)			NOT NULL,
	promo_id                VARCHAR2(40)		NOT NULL, 
	categories  	    	VARCHAR2(40)		NULL,
constraint vsg_promo_categories_pk primary key (asset_version, categories, promo_id)
);

CREATE TABLE vsg_promo_sites(
	asset_version		NUMBER(19)			NOT NULL,
	promo_id			VARCHAR2(40)	NOT NULL, 
	site			VARCHAR2(40)	NULL,
	constraint vsg_promo_sites_pk primary key (asset_version, site, promo_id)
);