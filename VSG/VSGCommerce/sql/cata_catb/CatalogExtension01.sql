--drop table vsg_promo_sites;
--drop table vsg_promo_categories;
--drop table vsg_promo_item;

CREATE TABLE vsg_promo_item (
	promo_id				VARCHAR2(40)		NOT NULL,
	display_name			VARCHAR2(254)		NOT NULL,
	parent_folder_id		VARCHAR2(40)		NULL,
	description				VARCHAR2(1000)		NULL,
	path					VARCHAR2(254)		NULL,
	slot_size				VARCHAR2(40)		NULL,
	html					CLOB				NULL,
	start_date				TIMESTAMP(6)		NULL,
	end_date				TIMESTAMP(6)		NULL,
	creation_date			TIMESTAMP(6)		NULL,
	constraint vsg_promo_item_pk primary key (promo_id),
	constraint vsg_promo_item_fk foreign key (parent_folder_id) references dcs_folder (folder_id)
);

CREATE TABLE vsg_promo_categories(
	promo_id			VARCHAR2(40)	NOT NULL references vsg_promo_item (promo_id), 
	categories			VARCHAR2(40)	NULL references dcs_category (category_id),
	constraint vsg_promo_categories_pk primary key (categories, promo_id)
);

CREATE TABLE vsg_promo_sites(
	promo_id			VARCHAR2(40)	NOT NULL references vsg_promo_item (promo_id), 
	site			VARCHAR2(40)	NULL references dcs_site (id),
	constraint vsg_promo_sites_pk primary key (site, promo_id)
);