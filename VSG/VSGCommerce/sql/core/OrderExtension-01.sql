-- drop table vsg_dcspp_cc_promos;
-- drop table vsg_dcspp_credit_card_promo;

CREATE TABLE vsg_dcspp_cc_promos (
	payment_group_id	varchar2(40)	not null,
	promo_id	varchar2(40)	not null,
	sequence_num	integer	not null
,constraint vsg_dcspp_cc_promos_p primary key (payment_group_id,sequence_num)
,constraint vsg_dcspp_cc_promos_f foreign key (payment_group_id) references dcspp_credit_card (payment_group_id));

CREATE TABLE vsg_dcspp_credit_card_promo (
	promo_id		varchar2(40)	not null,
	promo_code		varchar2(40)	not null,
	promo_text 	clob null,
	promo_amount 	number(19,7) null,
	user_accepted 	number(1,0)	null,
	CONSTRAINT VSG_DCSPP_CREDIT_CARD_PROMO_PK PRIMARY KEY (PROMO_ID)
);