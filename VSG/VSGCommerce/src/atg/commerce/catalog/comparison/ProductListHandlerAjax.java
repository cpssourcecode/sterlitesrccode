package atg.commerce.catalog.comparison;

import java.io.IOException;

import javax.servlet.ServletException;

import vsg.util.VSGFormUtils;
import atg.json.JSONException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * Extend ProductListHandler to handle ajax requests.
 * handleAddProduct and handleAddProduct methods can handle ajax request.
 * 
 * @author mmiller
 *
 */
public class ProductListHandlerAjax extends ProductListHandler {
	
	
	/**
	 * Add ajax response to the servlet response.
	 * 
	 * @param pResponse
	 * @throws IOException
	 */
	protected void addAjaxResponse(DynamoHttpServletResponse pResponse) throws IOException {
		try {
			if (!getFormError()) {
				VSGFormUtils.addAjaxSuccessResponse(pResponse);
			} else {
				VSGFormUtils.addAjaxErrorResponse(this, pResponse);
			}
		} catch (JSONException e) {
			logError(e);
		}
	}
	
	
	@Override
	public boolean handleAddProduct(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		boolean result = super.handleAddProduct(pRequest, pResponse);
		if (VSGFormUtils.isAjaxRequest(pRequest)) {
			addAjaxResponse(pResponse);
			result = false;
		}
		return result;
	}
	
	
	@Override
	public boolean handleRemoveProduct(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		boolean result = super.handleRemoveProduct(pRequest, pResponse);
		if (VSGFormUtils.isAjaxRequest(pRequest)) {
			addAjaxResponse(pResponse);
			result = false;
		}
		return result;
	}

    @Override
    public boolean handleClearList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        boolean result = super.handleClearList(pRequest, pResponse);
        if (VSGFormUtils.isAjaxRequest(pRequest)) {
            addAjaxResponse(pResponse);
            result = false;
        }
        return result;

    }

    @Override
	protected void redirectOrForward(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String arg2)
		throws IOException, ServletException {
		
		// forbid redirect and forward if we have ajax request
		if (!VSGFormUtils.isAjaxRequest(pRequest)) {
			super.redirectOrForward(pRequest, pResponse, arg2);
		}
		
	}

}