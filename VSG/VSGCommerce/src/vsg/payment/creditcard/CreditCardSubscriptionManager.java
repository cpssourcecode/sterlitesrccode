package vsg.payment.creditcard;

import java.util.Map;

import atg.commerce.order.CreditCard;
import atg.repository.RepositoryItem;

/**
 * CreditCardSubscriptionManager
 * 
 * <h4>Description</h4>
 * This interface will be the template for subscription management
 * with credit card services.
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 *
 */
public interface CreditCardSubscriptionManager {
		
	public abstract Map getCreditCards(Map pProfileCreditCards); 
	
	public abstract CreditCard getCreditCard(RepositoryItem pProfileCreditCard);

	public abstract CreditCard getCreditCard(String pSubscriptionId);

	public abstract boolean createCreditCard(RepositoryItem pProfileCreditCard, Map pUserInputCreditCard);

	public abstract boolean deleteCreditCard(RepositoryItem pProfileCreditCard);

	public abstract boolean deleteCreditCard(String pSubscriptionId);

	public abstract boolean updateCreditCard(RepositoryItem pProfileCreditCard, Map pUserInputCreditCard);

}