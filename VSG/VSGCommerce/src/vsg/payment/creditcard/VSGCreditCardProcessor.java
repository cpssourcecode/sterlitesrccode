package vsg.payment.creditcard;

import atg.nucleus.GenericService;
import atg.nucleus.spring.NucleusResolverUtil;
import atg.payment.PaymentStatus;
import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.CreditCardProcessor;
import atg.payment.creditcard.CreditCardStatus;
import atg.payment.creditcard.DecreaseCreditCardAuthorizationProcessor;

/**
 * VSGCreditCardProcessor
 * 
 * <h4>Description</h4>
 * This class will handle credit card integration in the
 * checkout pipeline.
 * 
 * <h4>Notes</h4>
 * 
 * 
 * @author Tim Harshbarger
 *
 */
public class VSGCreditCardProcessor extends GenericService implements
		CreditCardProcessor, DecreaseCreditCardAuthorizationProcessor {

	/**
	 * Added so can get it on request scope component
	 */
	private final static String COMPONENT_NAME = "/vsg/payment/creditcard/VSGCreditCardProcessor";

	/**
	 * Gets the instance of the class for request scope components
	 * @return
	 */
	public static VSGCreditCardProcessor getInstance() {
		return (VSGCreditCardProcessor) NucleusResolverUtil.resolveName(COMPONENT_NAME);
    }
	
	/**
	 * Default constructor
	 */
	public VSGCreditCardProcessor() {
	}

	/**
	 * Connection Manager property
	 */
	private CreditCardConnectionManager mConnectionManager = null;

	/**
	 * Return the connection manager proeprty
	 * @return
	 */
	public CreditCardConnectionManager getConnectionManager() {
		return mConnectionManager;
	}
	
	/**
	 * This method will set the connection manager property
	 * @param pConnectionManager
	 */
	public void setConnectionManager(CreditCardConnectionManager pConnectionManager) {
		mConnectionManager = pConnectionManager;
	}
	
	/**
	 * This method is implemented to cover the abstract extension
	 * in the OOB code for CreditCardProcessor.
	 * 
	 * This method will authorized the amount to be billed to the credit card
	 * on the order.
	 * 
	 * @see
	 * atg.payment.creditcard.CreditCardProcessor#authorize(atg.payment.creditcard
	 * .CreditCardInfo)
	 */
	public CreditCardStatus authorize(CreditCardInfo pCreditCardInfo) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".authorize - perform authorization for order:")
					.append(pCreditCardInfo.getOrder()).toString());
		return getConnectionManager().requestAuthorization(
				pCreditCardInfo.getOrder());
	}

	/**
	 * This method is not implemented because it is only used by a deprecated
	 * method in PaymentManager.
	 * 
	 * It is only here because it is required to be implemented by the abstract
	 * class it implements
	 *  
	 * @see
	 * atg.payment.creditcard.CreditCardProcessor#credit(atg.payment.creditcard
	 * .CreditCardInfo)
	 */
	public CreditCardStatus credit(CreditCardInfo creditcardinfo) {
		return null;
	}

	/**
	 * This method is used to credit a payment group.  The debit status
	 * contains the capture information.
	 * 
	 * @see
	 * atg.payment.creditcard.CreditCardProcessor#credit(atg.payment.creditcard
	 * .CreditCardInfo, atg.payment.creditcard.CreditCardStatus)
	 */
	public CreditCardStatus credit(CreditCardInfo pCreditCardInfo,
			CreditCardStatus pCreditCardStatus) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".credit - perform credit").toString());
		return getConnectionManager().requestCredit(pCreditCardInfo,
				pCreditCardStatus);
	}

	/**
	 * This method will be used to debit a credit card with Cyber Source
	 * 
	 * @see
	 * atg.payment.creditcard.CreditCardProcessor#debit(atg.payment.creditcard
	 * .CreditCardInfo, atg.payment.creditcard.CreditCardStatus)
	 */
	public CreditCardStatus debit(CreditCardInfo pCreditCardInfo,
			CreditCardStatus pCreditCardStatus) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".debit - perform debit").toString());
		return getConnectionManager().requestCapture(pCreditCardInfo,
				pCreditCardStatus);
	}

	/**
	 * This method will reverse an authorization for the credit card
	 * payment group.  The authorization status is passed in from the
	 * payment group.
	 * 
	 * @seeatg.payment.creditcard.DecreaseCreditCardAuthorizationProcessor#
	 * decreaseAuthorization(atg.payment.creditcard.CreditCardInfo,
	 * atg.payment.PaymentStatus)
	 */
	public CreditCardStatus decreaseAuthorization(
			CreditCardInfo pCreditCardInfo, PaymentStatus pPaymentStatus) {
		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".decreaseAuthorization - perform decrease")
					.toString());
		return getConnectionManager().requestAuthorizationReversal(
				pCreditCardInfo, (CreditCardStatus) pPaymentStatus);
	}
}
