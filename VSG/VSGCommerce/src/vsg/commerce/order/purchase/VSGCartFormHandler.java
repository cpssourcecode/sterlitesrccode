package vsg.commerce.order.purchase;

import java.io.IOException;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.order.Order;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * @author Igor Z.
 * Extends the default CartModifierFormHandler for custom functionality.
 * This class holds all the handle methods for the buttons on the cart
 * page.
 *
 */
public class VSGCartFormHandler extends CartModifierFormHandler{

	static final String USER_MESSAGES = "vsg.commerce.order.purchase.UserMessages";
	final ResourceBundle resourceBundle=ResourceBundle.getBundle(USER_MESSAGES);


	/**
	 * Update error redirect URL.
	 */
	private String mUpdateErrorURL;
	/**
	 * Update success redirect URL.
	 */
	private String mUpdateSuccessURL;

	/**
	 * @return the update error redirect URL.
	 */
	public String getUpdateErrorURL() {
		return mUpdateErrorURL;
	}

	/**
	 * @param pUpdateErrorURL - the update error redirect URL.
	 */
	public void setUpdateErrorURL(String pUpdateErrorURL) {
		mUpdateErrorURL = pUpdateErrorURL;
	}

	/**
	 * @return the update success redirect URL.
	 */
	public String getUpdateSuccessURL() {
		return mUpdateSuccessURL;
	}

	/**
	 * @param pUpdateSuccessURL - the update success redirect URL.
	 */
	public void setUpdateSuccessURL(String pUpdateSuccessURL) {
		mUpdateSuccessURL = pUpdateSuccessURL;
	}


	CustomCatalogTools mCatalogTools;

	public CustomCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	public void setCatalogTools(CustomCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

	public boolean handleRemoveItemFromOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws IOException,
			ServletException {

		if (isLoggingDebug()) {
			logDebug("VSGCartFormHandler.handleRemoveItemFromOrder start...");
		}

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "VSGCartFormHandler.handleRemoveItemFromOrder";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if (getUserLocale() == null)
					setUserLocale(getUserLocale(pRequest, pResponse));

				if (!checkFormRedirect(null, getRemoveItemFromOrderErrorURL(),
						pRequest, pResponse))
					return false;

				synchronized (getOrder()) {
					preRemoveItemFromOrder(pRequest, pResponse);

					if (!checkFormRedirect(null,
							getRemoveItemFromOrderErrorURL(), pRequest,
							pResponse))
						return false;

					if (getRemovalCommerceIds() != null) {
						try {
							deleteItems(pRequest, pResponse);
							Map extraParams = createRepriceParameterMap();
							runProcessRepriceOrder(getModifyOrderPricingOp(),
									getOrder(), getUserPricingModels(),
									getUserLocale(), getProfile(), extraParams);
						} catch (Exception exc) {
							processException(exc, MSG_ERROR_UPDATE_ORDER,
									pRequest, pResponse);
						}
					}
					if (!checkFormRedirect(null,
							getRemoveItemFromOrderErrorURL(), pRequest,
							pResponse))
						return false;

					postRemoveItemFromOrder(pRequest, pResponse);

					updateOrder(getOrder(), MSG_ERROR_UPDATE_ORDER, pRequest,
							pResponse);
				} // synchronized

				// If NO form errors are found, redirect to the success URL.
				// If form errors are found, redirect to the error URL.
				return checkFormRedirect(getRemoveItemFromOrderSuccessURL(),
						getRemoveItemFromOrderErrorURL(), pRequest, pResponse);
			} finally {
				if (tr != null)
					commitTransaction(tr);
				if (rrm != null)
					rrm.removeRequestEntry(myHandleMethod);
			}
		} else {
			return false;
		}
	}


}
