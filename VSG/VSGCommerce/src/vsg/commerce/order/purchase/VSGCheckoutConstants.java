package vsg.commerce.order.purchase;

public class VSGCheckoutConstants {
	
	public static final String MSG_LESS_THAN_MIN_QUANTITY = "quantityLessThanMin";
	public static final String MSG_MORE_THAN_MAX_QUANTITY = "quantityMoreThanMax";
	public static final String MSG_ITEM_LESS_THAN_MIN_QUANTITY = "itemQuantityLessThanMin";
	public static final String MSG_ITEM_MORE_THAN_MAX_QUANTITY = "itemQuantityMoreThanMax";
	public static final String MSG_SKU_MORE_THAN_MAX_QUANTITY = "skuQuantityMoreThanMax";
}
