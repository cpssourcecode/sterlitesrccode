package vsg.commerce.order.purchase;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import vsg.util.VSGFormUtils;

import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
/**
 *
 * Form Handler for taking billing information.
 *
 * @author v.ivus
 *
 */
public class VSGBillingInfoFormHandler extends PurchaseProcessFormHandler {

	/**
	 * Values map to collect user input.
	 */
	final private Map<String, Object> mValues = new HashMap<String, Object>();
	

	/**
	 * Returns values map.
	 * 
	 * @return values map
	 */
	public Map<String, Object> getValues() {
		return mValues;
	}

	/**
	 * Returns string value.
	 * 
	 * @param pKey key
	 * @return string value
	 */
	public String getStringValue(String pKey) {
		return (String) mValues.get(pKey);
	}
	
	/**
	 * Move To Review Success URL
	 */
	private String mMoveToReviewSuccessURL;
	
	/**
	 * Returns Move To Review Success URL.
	 * @return Move To Review Success URL
	 */
	public String getMoveToReviewSuccessURL() {
		return mMoveToReviewSuccessURL;
	}

	/**
	 * Sets Move To Review Success URL
	 * @param pMoveToReviewSuccessURL Move To Review Success URL
	 */
	public void setMoveToReviewSuccessURL(String pMoveToReviewSuccessURL) {
		mMoveToReviewSuccessURL = pMoveToReviewSuccessURL;
	}
	
	/**
	 * Move To Review Failure URL
	 */
	private String mMoveToReviewFailureURL;

	/**
	 * Returns Move To Review Failure URL.
	 * @return Move To Review Failure URL
	 */
	public String getMoveToReviewFailureURL() {
		return mMoveToReviewFailureURL;
	}

	/**
	 * Sets Move To Review Failure URL
	 * @param pMoveToReviewFailureURL Move To Review Failure URL
	 */
	public void setMoveToReviewFailureURL(String pMoveToReviewFailureURL) {
		mMoveToReviewFailureURL = pMoveToReviewFailureURL;
	}
	
	/**
	 * ApplyPromoCodeSuccessURL
	 */
	private String mApplyPromoCodeSuccessURL;
	
	/**
	 * Returns ApplyPromoCodeSuccessURL.
	 * @return ApplyPromoCodeSuccessURL.
	 */
	public String getApplyPromoCodeSuccessURL() {
		return mApplyPromoCodeSuccessURL;
	}

	/**
	 * Sets ApplyPromoCodeSuccessURL
	 * @param pApplyPromoCodeSuccessURL ApplyPromoCodeSuccessURL
	 */
	public void setApplyPromoCodeSuccessURL(String pApplyPromoCodeSuccessURL) {
		mApplyPromoCodeSuccessURL = pApplyPromoCodeSuccessURL;
	}
	
	/**
	 * ApplyPromoCodeFailureURL
	 */
	private String mApplyPromoCodeFailureURL;

	/**
	 * ApplyPromoCodeFailureURL.
	 * @return ApplyPromoCodeFailureURL.
	 */
	public String getApplyPromoCodeFailureURL() {
		return mApplyPromoCodeFailureURL;
	}

	/**
	 * Sets ApplyPromoCodeFailureURL
	 * @param pApplyPromoCodeFailureURL ApplyPromoCodeFailureURL
	 */
	public void setApplyPromoCodeFailureURL(String pApplyPromoCodeFailureURL) {
		mApplyPromoCodeFailureURL = pApplyPromoCodeFailureURL;
	}
	
	/**
	 * RemovePromoCodeSuccessURL
	 */
	private String mRemovePromoCodeSuccessURL;
	
	/**
	 * Returns RemovePromoCodeSuccessURL.
	 * @return RemovePromoCodeSuccessURL
	 */
	public String getRemovePromoCodeSuccessURL() {
		return mRemovePromoCodeSuccessURL;
	}

	/**
	 * Sets RemovePromoCodeSuccessURL
	 * @param pRemovePromoCodeSuccessURL RemovePromoCodeSuccessURL
	 */
	public void setRemovePromoCodeSuccessURL(String pRemovePromoCodeSuccessURL) {
		mRemovePromoCodeSuccessURL = pRemovePromoCodeSuccessURL;
	}
	
	/**
	 * RemovePromoFailureURL
	 */
	private String mRemovePromoFailureURL;

	/**
	 * Returns RemovePromoFailureURL.
	 * @return RemovePromoFailureURL
	 */
	public String getRemovePromoFailureURL() {
		return mRemovePromoFailureURL;
	}

	/**
	 * RemovePromoFailureURL
	 * @param pRemovePromoFailureURL RemovePromoFailureURL
	 */
	public void setRemovePromoFailureURL(String pRemovePromoFailureURL) {
		mRemovePromoFailureURL = pRemovePromoFailureURL;
	}
	
	/**
	 * AddGiftCardSuccessURL
	 */
	private String mAddGiftCardSuccessURL;
	
	/**
	 * Returns AddGiftCardSuccessURL.
	 * @return AddGiftCardSuccessURL
	 */
	public String getAddGiftCardSuccessURL() {
		return mAddGiftCardSuccessURL;
	}

	/**
	 * Sets AddGiftCardSuccessURL
	 * @param pAddGiftCardSuccessURL AddGiftCardSuccessURL
	 */
	public void setAddGiftCardSuccessURL(String pAddGiftCardSuccessURL) {
		mAddGiftCardSuccessURL = pAddGiftCardSuccessURL;
	}
	
	/**
	 * AddGiftCardFailureURL
	 */
	private String mAddGiftCardFailureURL;

	/**
	 * Returns AddGiftCardFailureURL.
	 * @return AddGiftCardFailureURL
	 */
	public String getAddGiftCardFailureURL() {
		return mAddGiftCardFailureURL;
	}

	/**
	 * Sets AddGiftCardFailureURL
	 * @param pAddGiftCardFailureURL AddGiftCardFailureURL
	 */
	public void setAddGiftCardFailureURL(String pAddGiftCardFailureURL) {
		mAddGiftCardFailureURL = pAddGiftCardFailureURL;
	}
	
	/**
	 * RemoveGiftCardSuccessURL
	 */
	private String mRemoveGiftCardSuccessURL;
	
	/**
	 * Returns RemoveGiftCardSuccessURL.
	 * @return RemoveGiftCardSuccessURL
	 */
	public String getRemoveGiftCardSuccessURL() {
		return mRemoveGiftCardSuccessURL;
	}

	/**
	 * Sets RemoveGiftCardSuccessURL
	 * @param pRemoveGiftCardSuccessURL RemoveGiftCardSuccessURL
	 */
	public void setRemoveGiftCardSuccessURL(String pRemoveGiftCardSuccessURL) {
		mRemoveGiftCardSuccessURL = pRemoveGiftCardSuccessURL;
	}
	
	/**
	 * RemoveGiftCardFailureURL
	 */
	private String mRemoveGiftCardFailureURL;

	/**
	 * Returns RemoveGiftCardFailureURL.
	 * @return RemoveGiftCardFailureURL
	 */
	public String getRemoveGiftCardFailureURL() {
		return mRemoveGiftCardFailureURL;
	}

	/**
	 * Sets RemoveGiftCardFailureURL
	 * @param pRemoveGiftCardFailureURL RemoveGiftCardFailureURL
	 */
	public void setRemoveGiftCardFailureURL(String pRemoveGiftCardFailureURL) {
		mRemoveGiftCardFailureURL = pRemoveGiftCardFailureURL;
	}

	/**
	 * Property paths set.
	 */
	final protected Set<String> mPropertyPath = new HashSet<String>();

	/**
	 * Redirect URL.
	 */
	protected String mRedirectURL = null;

	/**
	 * Ok status.
	 */
	protected String mOk = null;
	
	/**
	 * This template method for handle move order to review.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	public boolean handleMoveToReview(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "VSGBillingInfoFormHandler.handleMoveToReview";
		if ( null == rrm  || (rrm.isUniqueRequestEntry(myHandleMethod)) ) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if ( null == getUserLocale() ) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				synchronized (getOrder()) {
					preMoveToReview(pRequest, pResponse);
					moveToReview(pRequest, pResponse);
					postMoveToReview(pRequest, pResponse);
				}
			} finally {
				if ( null != tr ) {
					commitTransaction(tr);
				}
				if ( null != rrm ) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getMoveToReviewSuccessURL(), getMoveToReviewFailureURL(), pRequest, pResponse);
	}
	
	/**
	 * Pre move to review logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void preMoveToReview(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Move to review logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void moveToReview(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Post move to review logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void postMoveToReview(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	/**
	 * This template method for handle move order to review.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	public boolean handleMoveToReviewAddress(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "VSGBillingInfoFormHandler.handleMoveToReview";
		if ( null == rrm  || (rrm.isUniqueRequestEntry(myHandleMethod)) ) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if ( null == getUserLocale() ) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				synchronized (getOrder()) {
					preMoveToReviewAddress(pRequest, pResponse);
					if (getFormError()) {
						return preMoveToReviewExceptionHandling(pRequest,
								pResponse);
					}
					moveToReviewAddress(pRequest, pResponse);
					postMoveToReviewAddress(pRequest, pResponse);
				}
			} finally {
				if ( null != tr ) {
					commitTransaction(tr);
				}
				if ( null != rrm ) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getMoveToReviewSuccessURL(), getMoveToReviewFailureURL(), pRequest, pResponse);
	}
	
	/**
	 * Pre move to review logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void preMoveToReviewAddress(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Move to review logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void moveToReviewAddress(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Post move to review logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void postMoveToReviewAddress(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
		
	/**
	 * This template method for handle apply promo code.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	public boolean handleApplyPromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "VSGBillingInfoFormHandler.handleApplyPromoCode";
		if ( null == rrm  || (rrm.isUniqueRequestEntry(myHandleMethod)) ) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if ( null == getUserLocale() ) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				synchronized (getOrder()) {
					preApplyPromoCode(pRequest, pResponse);
					applyPromoCode(pRequest, pResponse);
					postApplyPromoCode(pRequest, pResponse);
				}
			} finally {
				if ( null != tr ) {
					commitTransaction(tr);
				}
				if ( null != rrm ) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getApplyPromoCodeSuccessURL(), getApplyPromoCodeFailureURL(), pRequest, pResponse);
	}
	
	/**
	 * Pre apply promo code logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void preApplyPromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Apply promo code logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void applyPromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Post apply promo code logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void postApplyPromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * This template method for handle remove promo code.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	public boolean handleRemovePromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "VSGBillingInfoFormHandler.handleRemovePromoCode";
		if ( null == rrm  || (rrm.isUniqueRequestEntry(myHandleMethod)) ) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if ( null == getUserLocale() ) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				synchronized (getOrder()) {
					preRemovePromoCode(pRequest, pResponse);
					removePromoCode(pRequest, pResponse);
					postRemovePromoCode(pRequest, pResponse);
				}
			} finally {
				if ( null != tr ) {
					commitTransaction(tr);
				}
				if ( null != rrm ) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getRemovePromoCodeSuccessURL(), getRemovePromoFailureURL(), pRequest, pResponse);
	}
	
	/**
	 * Pre remove promo code logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void preRemovePromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Remove promo code logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void removePromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Post remove promo code logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void postRemovePromoCode(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * This template method for handle add gift card.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	public boolean handleAddGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "VSGBillingInfoFormHandler.handleAddGiftCard";
		if ( null == rrm  || (rrm.isUniqueRequestEntry(myHandleMethod)) ) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if ( null == getUserLocale() ) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				synchronized (getOrder()) {
					preAddGiftCard(pRequest, pResponse);
					addGiftCard(pRequest, pResponse);
					postAddGiftCard(pRequest, pResponse);
				}
			} finally {
				if ( null != tr ) {
					commitTransaction(tr);
				}
				if ( null != rrm ) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getAddGiftCardSuccessURL(), getAddGiftCardFailureURL(), pRequest, pResponse);
	}
	
	/**
	 * Pre add gift card logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void preAddGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Add gift card logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void addGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Post add gift card logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void postAddGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * This template method for handle remove gift card.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	public boolean handleRemGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "VSGBillingInfoFormHandler.handleRemoveGiftCard";
		if ( null == rrm  || (rrm.isUniqueRequestEntry(myHandleMethod)) ) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if ( null == getUserLocale() ) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				synchronized (getOrder()) {
					preRemoveGiftCard(pRequest, pResponse);
					removeGiftCard(pRequest, pResponse);
					postRemoveGiftCard(pRequest, pResponse);
				}
			} finally {
				if ( null != tr ) {
					commitTransaction(tr);
				}
				if ( null != rrm ) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		return checkFormRedirect(getRemoveGiftCardSuccessURL(), getRemoveGiftCardFailureURL(), pRequest, pResponse);
	}
	
	/**
	 * Pre remove gift card logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void preRemoveGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Remove gift card logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void removeGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}
	
	/**
	 * Post remove gift card logic.
	 * 
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return if false then stay on same page( for axaj-call ), true redirect.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	protected void postRemoveGiftCard(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
	}

	/**
	 * Add ajax response logic.
	 *  
	 * @param pSuccessURL Success URL
	 * @param pFailureURL Failure URL
	 * @param pRequest DynamoHttpServletRequest
	 * @param pResponse DynamoHttpServletResponse
	 * @return true redirected, false on the same page.
	 * @throws ServletException Servlet Exception
	 * @throws IOException I/O Exception
	 */
	@Override
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		if ( VSGFormUtils.isAjaxRequest(pRequest) ) {
			try {
				if ( !StringUtils.isBlank(mRedirectURL) ) {
					VSGFormUtils.addAjaxRedirectResponse(this, pResponse, mRedirectURL);
					mRedirectURL = null;
				} else {
					if ( !getFormError() ) {
						VSGFormUtils.addAjaxOkResponse(pResponse, mOk);
						mOk = null;
					} else {
						VSGFormUtils.addAjaxErrorResponse(this, pResponse, mPropertyPath);
						mPropertyPath.clear();
					}
				}
			} catch (JSONException e) {
				logError(e);
			}
			return false;
		}else{
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}
	}

	/**
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean preMoveToReviewExceptionHandling(
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (isLoggingDebug())
			logDebug("Error in preMoveToReview, redirecting to error URL.");
		return checkFormRedirect(null, getMoveToReviewFailureURL(), pRequest,
				pResponse);
	}
	
}
