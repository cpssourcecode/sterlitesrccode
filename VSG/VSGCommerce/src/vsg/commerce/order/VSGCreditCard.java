package vsg.commerce.order;

import atg.commerce.order.CreditCard;

/**
 * Extends ATG credit card to add new values into Credit Card table
 * 
 * @author Tim Harshbarger
 * 
 */
public class VSGCreditCard extends CreditCard {

	/**
	 * generated
	 */
	private static final long serialVersionUID = -6634439989716103751L;
	
	/** Subscription Id property name */
	public final static String PN_SUBSCRIPTION_ID = "subscriptionId";

	/**
	 * @return
	 */
	public String getSubscriptionId() {
		return (String) super.getPropertyValue(PN_SUBSCRIPTION_ID);
	}

	/**
	 * @param pSubscriptionId
	 */
	public void setSubscriptionId(String pSubscriptionId) {
		super.setPropertyValue(PN_SUBSCRIPTION_ID, pSubscriptionId);
	}
}
