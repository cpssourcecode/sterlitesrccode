package vsg.servlet.pipeline;

import java.io.IOException;

import javax.servlet.ServletException;

import vsg.userprofiling.VSGProfileTools;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;
import atg.userprofiling.Profile;

public class PromotionServlet  extends InsertableServletImpl{
	
	private static final String PROFILE = "/atg/userprofiling/Profile";
	private static final String PROFILE_TOOLS = "/atg/userprofiling/ProfileTools";
	private static final String COUPON = "code";
	
	private boolean enabled;

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
			throws IOException, ServletException
	{
		try{
			if(isEnabled())
			{
				String coupon = pRequest.getParameter(COUPON);
				if(!StringUtils.isEmpty(coupon))
				{
					if (isLoggingDebug()) 
						logDebug("found coupon code in request "+ coupon +" will try to apply to user profile");
						
					Profile profile = (Profile)pRequest.resolveName(PROFILE);
					VSGProfileTools pTools = (VSGProfileTools)pRequest.resolveName(PROFILE_TOOLS);
					if(profile!=null & pTools!=null)
					{
						pTools.appendProfilePromotion(coupon, profile);
					}
				}
			}
			
		}catch (Exception ex){
			logError(ex);
		}
		passRequest(pRequest, pResponse);
	}

}
