package vsg.userprofiling;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.commerce.claimable.ClaimableManager;
import atg.commerce.profile.CommerceProfileTools;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import atg.userprofiling.RepositoryProfileItemFinder;

public class VSGProfileTools extends CommerceProfileTools {
	private Repository siteRepository;
	private ClaimableManager claimableManage;
	/**
	 * Email Format Regular Expression.
	 */
	private String mEmailformat;

	private static final String VSG_REG_EXP = "vsg.util.RegularExpressions";
	final ResourceBundle regularExpBundle = ResourceBundle.getBundle(VSG_REG_EXP);
	private List<String> rules = new ArrayList<String>();


	/**
	 * returns claimable manager component
	 */
	public ClaimableManager getClaimableManage() {
		return claimableManage;
	}

	public void setClaimableManage(ClaimableManager claimableManage) {
		this.claimableManage = claimableManage;
	}

	/**
	 * Validates an email address for correctness.
	 *
	 * @param pEmail email address
	 * @return boolean true if email address is valid
	 */
	public boolean validateEmailAddress(String pEmail) {
		String regularExp = getEmailFormat();
		if (regularExp == null) {
			regularExp = "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,})+$";
		}
		// Set the email pattern string
		Pattern p = Pattern.compile(regularExp);
		// Match the given string with the pattern
		Matcher m = p.matcher(pEmail);
		// check whether match is found
		return m.matches();
	}


	/**
	 * Determines if a profile already exists with the email address.
	 *
	 * @param pEmail - e-mail address
	 * @return true if a profile exists with the given email
	 */
	public boolean isDuplicateEmailAddress(String pEmail) {
		// make sure a user with this email address doesn't already exist
		// UPDATE: Do we need to check all types of profiles?
		RepositoryProfileItemFinder profileFinder = (RepositoryProfileItemFinder) getProfileItemFinder();
		String profileType = getDefaultProfileType();
		RepositoryItem[] users = profileFinder.findByEmail(pEmail, profileType);
		if (users != null) {
			// add form exception user already exists
			if (isLoggingDebug()) {
				logDebug("User already exists with this email address: "
						+ pEmail);
			}
			return true;
		}
		return false;
	}

	/**
	 * Overloading isDuplicateEmailAddress method to check email and login due
	 * to different logins depending on the site they where created.
	 */
	public boolean isDuplicateEmailAddress(String pEmail, String newLogin) {
		// make sure a user with this email address doesn't already exist
		// UPDATE: Do we need to check all types of profiles?
		RepositoryProfileItemFinder profileFinder = (RepositoryProfileItemFinder) getProfileItemFinder();
		String profileType = getDefaultProfileType();

		RepositoryItem[] users = profileFinder.findByEmail(pEmail, profileType);

		if (users != null) {
			for (RepositoryItem user : users) {
				if (user != null) {
					String login = (String) user
							.getPropertyValue(getPropertyManager()
									.getLoginPropertyName());
					if (login.equalsIgnoreCase(newLogin)) {
						// add form exception user already exists
						// if (isLoggingDebug())
						// logDebug("The email address you have provided is already being used.  Please enter another email address.");
						return true;
					}
				}

			}
		}
		return false;
	}

	public boolean validateEmailLink(String pProfileId, String pUserLogin) {
		boolean validation = false;
		try {
			RepositoryItem profileItem = getProfileRepository().getItem(pProfileId, getDefaultProfileType());
			String mUserLogin = (String) profileItem.getPropertyValue("login");
			if (mUserLogin.equalsIgnoreCase(pUserLogin))
				validation = true;
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError("Repository Exception thrown while validating email link", re);
			}
		}

		return validation;
	}

	/**
	 * This method apply promotion to the user profile based on coupon code parameter
	 *
	 * @param pCouponCode
	 * @param pProfile
	 */
	public void appendProfilePromotion(String pCouponCode, Profile pProfile) {
		try {
			if (pCouponCode != null && pCouponCode.length() <= 40) {
				if (isLoggingDebug())
					logDebug("appendProfilePromotion () + applying coupon to user profile " + pCouponCode);

				RepositoryItem couponItem = getClaimableManage().getClaimableTools().getClaimableItem(pCouponCode.toUpperCase());
				if (couponItem != null) {
					boolean isExpired = getClaimableManage().checkExpireDate(couponItem);
					boolean isValidType = getClaimableManage().checkPromotionType(couponItem);
					if (isLoggingDebug()) {
						logDebug("The promotion for this coupon " + pCouponCode + " is Expired? =" + isExpired);
						logDebug("The promotion type for  this coupon " + pCouponCode + " is Valid Type? =" + isValidType);
					}
					if (!isExpired && isValidType) {
						//ATG 10 changed property from single "promotion" to Set of "promotions" 
						Set<RepositoryItem> promoItems = (Set<RepositoryItem>) couponItem.getPropertyValue("promotions");
						if (promoItems != null && !promoItems.isEmpty()) {
							boolean canApply = getClaimableManage().canClaimCoupon(pProfile.getRepositoryId(), pCouponCode);
							//if canApply is true lets try to claim it
							if (canApply) {
								getClaimableManage().claimCoupon(pProfile.getRepositoryId(), pCouponCode);
							}
							if (isLoggingDebug())
								logDebug("The promotion for this coupon " + pCouponCode + " canApply  =" + canApply + " for user profile where profile id" + pProfile.getRepositoryId());
						}
					}
				}
			}
		} catch (Exception ex) {
			logError(ex);
		}
	}

	/**
	 * Is registered user boolean.
	 *
	 * @param pProfile the p profile
	 * @return the boolean
	 */
	public boolean isRegisteredUser(final RepositoryItem pProfile) {
		return pProfile != null && !pProfile.isTransient();
	}


	public String getEmailFormat() {
		return mEmailformat;
	}

	public void setEmailFormat(String pEmailformat) {
		mEmailformat = pEmailformat;
	}


}