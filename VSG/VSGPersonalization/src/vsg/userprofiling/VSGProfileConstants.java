package vsg.userprofiling;

import atg.droplet.DropletException;

import java.util.Dictionary;
import java.util.regex.Pattern;

public interface VSGProfileConstants {

	public static final Pattern MASK_MAIL = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");

	public static final Pattern MASK_PSWD = Pattern.compile(".{8,15}");

	public static final Pattern MASK_PHONE = Pattern.compile(".*");

	public static final Pattern MASK_ADDRESS = Pattern.compile("^[a-zA-Z0-9@,:;\\.'_\\(\\)\\/#\\s\\-]+$");

	public static final Pattern MASK_NAME = Pattern.compile("[A-Za-z\\s]*");

	public static final Pattern MASK_CC_NUMBER = Pattern.compile("[0-9]*");

	public static final Pattern MASK_CC_NAME = Pattern.compile("[A-Za-z'\\s]*");

	public static final Pattern MASK_CC_DATE = Pattern.compile("(\\d{2})/(\\d{4})");

	public static final Pattern MASK_ZIP = Pattern.compile("\\d{5}-?\\d{0,4}");

	public static final Pattern MASK_CITY = Pattern.compile("[A-Za-z.'-(\\s)]*");


	public Dictionary getValue();

	public String getAbsoluteName();

	public void addFormException(DropletException exc);

}
