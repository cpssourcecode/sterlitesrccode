package vsg.userprofiling;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import vsg.util.VSGConstants;
import vsg.util.VSGFormUtils;
import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.json.JSONException;
import atg.multisite.SiteContextManager;
import atg.projects.b2cstore.B2CProfileFormHandler;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.security.IdentityManager;
import atg.security.SaltedDigestPasswordHasher;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.ProfileSwapEvent;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;

public class VSGProfileFormHandler extends B2CProfileFormHandler implements VSGProfileConstants {

	final Set<String> propertyPath = new HashSet<String>();
	private VSGFormUtils tools;

	/**
	 * Adding Ajax response to redirect.
	 */
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
									 DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (VSGFormUtils.isAjaxRequest(pRequest)) {
			try {
				if (!getFormError()) {
					VSGFormUtils.addAjaxSuccessResponse(pResponse);
				} else {
					VSGFormUtils.addAjaxErrorResponse(this, pResponse, propertyPath);
					propertyPath.clear();
				}
			} catch (JSONException e) {
				logError(e);
			}
			return false;
		} else {
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}
	}

	public VSGFormUtils getTools() {
		return tools;
	}

	public void setTools(VSGFormUtils tools) {
		this.tools = tools;
	}

}
