--DROP TABLE vsg_dps_contact_info;
--DROP TABLE vsg_dps_user_address;
--DROP TABLE vsg_dps_user;

CREATE TABLE vsg_dps_user (
	user_id 		varchar2(40)	NOT NULL REFERENCES dps_user(id),
	optin_email 		number(1)	NULL,
	optin_email_html 	number(1)	NULL,
	CHECK (optin_email IN (0, 1)),
	CHECK (optin_email_html IN (0, 1)),
	CONSTRAINT vsg_user_pk PRIMARY KEY(user_id)
);

CREATE TABLE vsg_dps_contact_info (
id                      		varchar2(40) 	NOT NULL REFERENCES dps_contact_info(id),
validated_address		numeric(1,0)	NULL,
CONSTRAINT vsg_dps_contact_info_pk PRIMARY KEY(id));

CREATE TABLE vsg_dps_user_address (
id                      		varchar2(40) 	NOT NULL REFERENCES dps_user_address(id),
billing_addr_id		varchar2(40)	,
shipping_addr_id		varchar2(40)	,
CONSTRAINT vsg_dps_user_address_pk PRIMARY KEY(id));
