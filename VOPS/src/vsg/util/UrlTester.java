package vsg.util;
import java.util.Calendar;
import java.net.URL;
import java.net.HttpURLConnection;
import javax.net.ssl.HttpsURLConnection;
import java.lang.Exception;
import java.io.*;

public class UrlTester {
	String domain;
	String protocol;
	String uri;
	String port;
	
	public void testUrl(){
		
		Calendar before = Calendar.getInstance();
		String url=null;
		if(port!=null){
		url=getProtocol()+"://"+getDomain()+":"+getPort()+getUri();
		}else{
		url=getProtocol()+"://"+getDomain()+getUri();
		}
		System.out.println(url);
		if(getProtocol().equals("http")){
		try{	
		HttpURLConnection connection = (HttpURLConnection)new URL( url ).openConnection();
		System.out.println(connection.getResponseCode());
        Calendar end = Calendar.getInstance();
        Object theContent=connection.getContent();
        BufferedReader in = new BufferedReader(new InputStreamReader(
        connection.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null){ 
        System.out.println(inputLine);
        }
        Calendar endContent = Calendar.getInstance();
        long TTFB = end.getTimeInMillis() - before.getTimeInMillis();
        long justLoadTime = endContent.getTimeInMillis() - before.getTimeInMillis();
        System.out.println ("TTFB:  "+TTFB);
        System.out.println("Total Time:  "+justLoadTime);
		}catch(Exception e){
			e.printStackTrace();
		}
		}
		if(getProtocol().equals("https")){
			try{	
			HttpsURLConnection connection = (HttpsURLConnection)new URL( url ).openConnection();
			System.out.println(connection.getResponseCode());
            Calendar end = Calendar.getInstance();
            Object theContent=connection.getContent();
            BufferedReader in = new BufferedReader(new InputStreamReader(
            connection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null){ 
            System.out.println(inputLine);
            }
            Calendar endContent = Calendar.getInstance();
            long TTFB = end.getTimeInMillis() - before.getTimeInMillis();
            long justLoadTime = endContent.getTimeInMillis() - before.getTimeInMillis();
            System.out.println ("TTFB:  "+TTFB);
            System.out.println("Total Time:  "+justLoadTime);
			}catch(Exception e){
				e.printStackTrace();
			}
			}
		
	}
	
	
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}


	public String getUri() {
		return uri;
	}


	public void setUri(String uri) {
		this.uri = uri;
	}


	public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}
	

}
