package vsg.util;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;

import atg.service.email.MimeMessageUtils;

import com.sun.mail.smtp.SMTPTransport;



public class SMTPEmailSender extends atg.service.email.SMTPEmailSender {

	public Transport openConnection()
		throws atg.service.email.EmailException
		{
		
		Properties props = System.getProperties();
        props.put("mail.smtps.host",getEmailHandlerHostName());
        props.put("mail.smtps.auth","true");
        if(isLoggingDebug()){
        	logDebug("info:  "+getEmailHandlerHostName()+" "+getEmailHandlerPort());
        }
        Session session = Session.getInstance(props, null);
        SMTPTransport t = null;
        try{
        	if(isLoggingDebug()){
            	logDebug("calling for connection");
            }
        	t = (SMTPTransport)session.getTransport("smtps");
        	t.connect(getEmailHandlerHostName(), getUsername(), getPassword());
		
		}catch(Exception e){
			e.printStackTrace();
		}
        return t;
		}
	
			public void sendTestMessage(){
				try{
					Message msg= MimeMessageUtils.createMessage();
			        MimeMessageUtils.setFrom(msg, getDefaultFrom());
			        msg.setSubject("this is a test");
			        MimeMessageUtils.setRecipients(msg, Message.RecipientType.TO, getDefaultRecipients());
			        //msg.setHeader("Content-Type", "text/html");
			        //msg.setText(body);
			        msg.setContent("This is still a test", "text/html");
			        sendEmailMessage(msg);
				}catch(Exception e){
                    e.printStackTrace();
				}
}
	
	
}
