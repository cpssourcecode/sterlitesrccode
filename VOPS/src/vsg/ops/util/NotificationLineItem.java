package vsg.ops.util;

public class NotificationLineItem {
	public String customer;
	public String server;
	public String instance;
	public String errorProcString;
	public String component;
	public String errorClass;
	public String errorMessage;
	public String procStatusString;
	public String genericMessage;
	public boolean escalation;
	public String emailTitle;
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getInstance() {
		return instance;
	}
	public void setInstance(String instance) {
		this.instance = instance;
	}
	public String getErrorProcString() {
		return errorProcString;
	}
	public void setErrorProcString(String errorProcString) {
		this.errorProcString = errorProcString;
	}
	public String getErrorClass() {
		return errorClass;
	}
	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getProcStatusString() {
		return procStatusString;
	}
	public void setProcStatusString(String procStatusString) {
		this.procStatusString = procStatusString;
	}
	public boolean isEscalation() {
		return escalation;
	}
	public void setEscalation(boolean escalation) {
		this.escalation = escalation;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getEmailTitle() {
		return emailTitle;
	}
	public void setEmailTitle(String emailTitle) {
		this.emailTitle = emailTitle;
	}
	public String getGenericMessage() {
		return genericMessage;
	}
	public void setGenericMessage(String genericMessage) {
		this.genericMessage = genericMessage;
	}

}
