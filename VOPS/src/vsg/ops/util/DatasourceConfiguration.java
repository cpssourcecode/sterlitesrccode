package vsg.ops.util;

public class DatasourceConfiguration {
	
	public String pubUsername;
	public String pubPassword;
	public String agentUsername;
	public String agentPassword;
	public String cataUsername;
	public String cataPassword;
	public String catbUsername;
	public String catbPassword;
	public String coreUsername;
	public String corePassword;
	public String connectionURL;
	public String customer;

	
	public String getPubUsername() {
		return pubUsername;
	}
	public void setPubUsername(String pubUsername) {
		this.pubUsername = pubUsername;
	}
	public String getPubPassword() {
		return pubPassword;
	}
	public void setPubPassword(String pubPassword) {
		this.pubPassword = pubPassword;
	}
	public String getAgentUsername() {
		return agentUsername;
	}
	public void setAgentUsername(String agentUsername) {
		this.agentUsername = agentUsername;
	}
	public String getAgentPassword() {
		return agentPassword;
	}
	public void setAgentPassword(String agentPassword) {
		this.agentPassword = agentPassword;
	}
	public String getCataUsername() {
		return cataUsername;
	}
	public void setCataUsername(String cataUsername) {
		this.cataUsername = cataUsername;
	}
	public String getCataPassword() {
		return cataPassword;
	}
	public void setCataPassword(String cataPassword) {
		this.cataPassword = cataPassword;
	}
	public String getCatbUsername() {
		return catbUsername;
	}
	public void setCatbUsername(String catbUsername) {
		this.catbUsername = catbUsername;
	}
	public String getCatbPassword() {
		return catbPassword;
	}
	public void setCatbPassword(String catbPassword) {
		this.catbPassword = catbPassword;
	}
	public String getCoreUsername() {
		return coreUsername;
	}
	public void setCoreUsername(String coreUsername) {
		this.coreUsername = coreUsername;
	}
	public String getCorePassword() {
		return corePassword;
	}
	public void setCorePassword(String corePassword) {
		this.corePassword = corePassword;
	}
	public String getConnectionURL() {
		return connectionURL;
	}
	public void setConnectionURL(String connectionURL) {
		this.connectionURL = connectionURL;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	

}
