package vsg.ops.email;
import java.sql.*;
import vsg.ops.util.NotificationLineItem;


public class VONSEmailManager {
	public vsg.ops.email.VOPSEmailSender emailSender;
	public atg.service.jdbc.WatcherDataSource datasource;
	public java.util.ArrayList<VONSEmailLineItem> errorArrayList;
	public java.util.ArrayList<VONSEmailLineItem> procArrayList;
	public java.util.ArrayList<VONSEmailLineItem> escalatedErrorArrayList;
	
	
	public void generateHourlyReport(){
		try {
			setErrorArrayList(new java.util.ArrayList<VONSEmailLineItem>());
			Connection c=getDatasource().getConnection();
			Statement st=c.createStatement();
			String errorSQL="select count (*), CUSTOMER,COMPONENT_NAME, ERROR_CLASS from vsg_vons_entries where ERR_PROC_STRING='ERROR' and time_received > sysdate - interval '1' hour group by ERROR_CLASS,COMPONENT_NAME,CUSTOMER";
			ResultSet rs=st.executeQuery(errorSQL);
			while(rs.next()){
				System.out.println("Count:  "+rs.getInt(1)+" Value: "+rs.getString("ERROR_CLASS"));
				java.lang.Integer numberOfExec=new java.lang.Integer(rs.getInt(1));
				VONSEmailLineItem li=new VONSEmailLineItem();
				li.setNumbers(numberOfExec.toString());
				li.setCustomer(rs.getString("CUSTOMER"));
				li.setComponent(rs.getString("COMPONENT_NAME"));
				li.setErrorClass(rs.getString("ERROR_CLASS"));
				getErrorArrayList().add(li);
			}
		
			c.close();
			
			setProcArrayList(new java.util.ArrayList<VONSEmailLineItem>());
			Connection c2=getDatasource().getConnection();
			Statement st2=c2.createStatement();
			String errorSQL2="select count (*), CUSTOMER,COMPONENT_NAME, PROC_STATUS_STRING from vsg_vons_entries where ERR_PROC_STRING='PROCESS' and time_received > sysdate - interval '1' hour group by PROC_STATUS_STRING,COMPONENT_NAME,CUSTOMER  order by CUSTOMER, COMPONENT_NAME";
			ResultSet rs2=st2.executeQuery(errorSQL2);
			while(rs2.next()){
				System.out.println("Count:  "+rs2.getInt(1)+" Value: "+rs2.getString("PROC_STATUS_STRING"));
				java.lang.Integer numberOfExec2=new java.lang.Integer(rs2.getInt(1));
				VONSEmailLineItem li2=new VONSEmailLineItem();
				li2.setNumbers(numberOfExec2.toString());
				li2.setCustomer(rs2.getString("CUSTOMER"));
				li2.setComponent(rs2.getString("COMPONENT_NAME"));
				li2.setProcStatus(rs2.getString("PROC_STATUS_STRING"));
				getProcArrayList().add(li2);
			}
			
			c2.close();
			if(getErrorArrayList().isEmpty()&&getProcArrayList().isEmpty()){
				getEmailSender().sendEmptyVONSMessage("Nothing to see here, move along.","Empty Hourly Report");
			}else{
				getEmailSender().sendVONSMessage(generateEmailBody(getErrorArrayList(),getProcArrayList(),null),"Hourly Report");
				
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
	
	
	public void generateDailyReport(){
		
		try {
			
			
			setEscalatedErrorArrayList(new java.util.ArrayList<VONSEmailLineItem>());
			Connection ce=getDatasource().getConnection();
			Statement ste=ce.createStatement();
			String errorSQLe="select count (*), ERROR_CLASS,CUSTOMER,COMPONENT_NAME from vsg_vons_entries where ERR_PROC_STRING='ERROR' and time_received > sysdate - 1 and ESCALATION=1 group by ERROR_CLASS,COMPONENT_NAME,CUSTOMER";
			ResultSet rse=ste.executeQuery(errorSQLe);
			while(rse.next()){
				System.out.println("Count:  "+rse.getInt(1)+" Value: "+rse.getString("ERROR_CLASS"));
				System.out.println("Count:  "+rse.getInt(1)+" Value: "+rse.getString("ERROR_CLASS"));
				java.lang.Integer numberOfExec=new java.lang.Integer(rse.getInt(1));
				VONSEmailLineItem li=new VONSEmailLineItem();
				li.setNumbers(numberOfExec.toString());
				li.setCustomer(rse.getString("CUSTOMER"));
				li.setComponent(rse.getString("COMPONENT_NAME"));
				li.setErrorClass(rse.getString("ERROR_CLASS"));
				getEscalatedErrorArrayList().add(li);
			} 
		
			ce.close();
			
			
			setErrorArrayList(new java.util.ArrayList<VONSEmailLineItem>());
			Connection c=getDatasource().getConnection();
			Statement st=c.createStatement();
			String errorSQL="select count (*), ERROR_CLASS,CUSTOMER,COMPONENT_NAME from vsg_vons_entries where ERR_PROC_STRING='ERROR' and time_received > sysdate - 1 and ESCALATION=0 group by ERROR_CLASS,COMPONENT_NAME,CUSTOMER";
			ResultSet rs=st.executeQuery(errorSQL);
			while(rs.next()){
				System.out.println("Count:  "+rs.getInt(1)+" Value: "+rs.getString("ERROR_CLASS"));
				System.out.println("Count:  "+rs.getInt(1)+" Value: "+rs.getString("ERROR_CLASS"));
				java.lang.Integer numberOfExec=new java.lang.Integer(rs.getInt(1));
				VONSEmailLineItem li=new VONSEmailLineItem();
				li.setNumbers(numberOfExec.toString());
				li.setCustomer(rs.getString("CUSTOMER"));
				li.setComponent(rs.getString("COMPONENT_NAME"));
				li.setErrorClass(rs.getString("ERROR_CLASS"));
				getErrorArrayList().add(li);
			}
		
			c.close();
			
			setProcArrayList(new java.util.ArrayList<VONSEmailLineItem>());
			Connection c2=getDatasource().getConnection();
			Statement st2=c2.createStatement();
			String errorSQL2="select count (*), CUSTOMER,COMPONENT_NAME, PROC_STATUS_STRING from vsg_vons_entries where ERR_PROC_STRING='PROCESS' and time_received > sysdate -1 group by PROC_STATUS_STRING,COMPONENT_NAME,CUSTOMER";
			ResultSet rs2=st2.executeQuery(errorSQL2);
			while(rs2.next()){
				System.out.println("Count:  "+rs2.getInt(1)+" Value: "+rs2.getString("PROC_STATUS_STRING"));
				java.lang.Integer numberOfExec2=new java.lang.Integer(rs2.getInt(1));
				VONSEmailLineItem li2=new VONSEmailLineItem();
				li2.setNumbers(numberOfExec2.toString());
				li2.setCustomer(rs2.getString("CUSTOMER"));
				li2.setComponent(rs2.getString("COMPONENT_NAME"));
				li2.setProcStatus(rs2.getString("PROC_STATUS_STRING"));
				getProcArrayList().add(li2);
			}
			
			c2.close();
			getEmailSender().sendVONSMessage(generateEmailBody(getErrorArrayList(),getProcArrayList(),getEscalatedErrorArrayList()),"Daily Report");
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
	}
	
	public String generateEmailBody(java.util.ArrayList<VONSEmailLineItem> errorArrayList,java.util.ArrayList<VONSEmailLineItem> procArrayList,java.util.ArrayList<VONSEmailLineItem> escalatedErrorArrayList){
		StringBuffer body=new StringBuffer();
		char newline='\n';
	
		body.append("<HTML> "+newline);
		if(escalatedErrorArrayList!=null&&escalatedErrorArrayList.size()>0){
		body.append("<H3>Escalated Errors:</H3> "+newline);
		body.append("<TABLE BORDER='1' >");
		body.append("<TR><TH BGCOLOR='#CC0000'><font color='white'>Customer</font></TH><TH BGCOLOR='#CC0000'><font color='white'>Component</font></TH><TH BGCOLOR='#CC0000'><font color='white'>Error Class</font></TH><TH BGCOLOR='#CC0000'><font color='white'># of Entries</font></TH></TR>");
		for(int i=0;i<escalatedErrorArrayList.size();i++){
			VONSEmailLineItem li=escalatedErrorArrayList.get(i);
			body.append("<TR><TD>"+li.getCustomer()+"</TD><TD>"+li.getComponent()+"</TD><TD>"+li.getErrorClass()+"</TD><TD>"+li.getNumbers()+"</TD></TR>");
		}
		body.append("</TABLE>");
		body.append(newline);
		}
		body.append("<H3>Errors:</H3> "+newline);
		body.append("<TABLE BORDER='1' >");
		body.append("<TR><TH BGCOLOR='#FFFF00'>Customer</TH><TH BGCOLOR='#FFFF00'>Component</TH><TH BGCOLOR='#FFFF00'>Error Class</TH><TH BGCOLOR='#FFFF00'># of Entries</TH></TR>");
		for(int i=0;i<errorArrayList.size();i++){
			VONSEmailLineItem li=errorArrayList.get(i);
			body.append("<TR><TD>"+li.getCustomer()+"</TD><TD>"+li.getComponent()+"</TD><TD>"+li.getErrorClass()+"</TD><TD>"+li.getNumbers()+"</TD></TR>");
		}
		body.append("</TABLE>");
		body.append(newline);
		body.append("<H3>Procs: </H3>"+newline);
		body.append("<TABLE BORDER='1' >");
		body.append("<TR><TH BGCOLOR='#00CC00'>Customer</TH><TH BGCOLOR='#00CC00'>Component</TH><TH BGCOLOR='#00CC00'>Status</TH><TH BGCOLOR='#00CC00'># of Entries</TH></TR>");
		for(int i=0;i<procArrayList.size();i++){
			VONSEmailLineItem li=procArrayList.get(i);
			body.append("<TR><TD>"+li.getCustomer()+"</TD><TD>"+li.getComponent()+"</TD><TD>"+li.getProcStatus()+"</TD><TD>"+li.getNumbers()+"</TD></TR>");
		}
		body.append("</TABLE>");
		body.append("</HTML>"+newline);
		return body.toString();
	}
	

	public boolean sendImmediateMessage(NotificationLineItem item){
		boolean complete=true;
		StringBuffer body=new StringBuffer();
		char newline='\n';
		body.append("<HTML> "+newline);
		System.out.println("email title is: " + item.getEmailTitle());
		if( (item.getEmailTitle() == null) || (item.getEmailTitle().indexOf("Resolved") == -1) ) {
		body.append("<B>Escalated Error:</B> "+"<P></P>");
		}else{
		body.append("<B>Error Resolved:</B> "+"<P></P>");	
		}
		System.out.println("customer is: " + item.getCustomer());
		body.append("Customer:  "+item.getCustomer()+"<P></P>");
		System.out.println("server is: " + item.getServer());
		if(item.getServer()!=null){
		body.append("Server:  "+item.getServer()+"<P></P>");
		}
		if(item.getInstance()!=null){
		body.append("Instance:  "+item.getInstance()+"<P></P>");
		}
		if(item.getComponent()!=null){
		body.append("Component:  "+item.getComponent()+"<P></P>");
		}
		if(item.getErrorClass()!=null){
		body.append("Error Class:  "+item.getErrorClass()+"<P></P>");
		}
		System.out.println("error message is: " + item.getErrorMessage());
		body.append("Error Message:  "+item.getErrorMessage()+"<P></P>");
		if(item.getGenericMessage()!=null){
		body.append("Generic Message:  "+item.getGenericMessage()+"<P></P>");
		}
		body.append("</HTML>"+newline);
		System.out.println("in email manager, about to send message: " + body.toString() + "|" + item.getEmailTitle());
		getEmailSender().sendVONSMessage(body.toString(),item.getEmailTitle());
		System.out.println("am I HERE????");
		return complete;
	}
	
	
	public boolean sendLongDeploymentMessage(String customer, String processName, String time){
		boolean complete=true;
		StringBuffer body=new StringBuffer();
		char newline='\n';
		body.append("<HTML> "+newline);
		body.append("<H3>Deployment Info:</H3> "+newline);
		body.append("Customer:  "+customer+"<P></P>");
		body.append("Project Name:  "+processName+"<P></P>");
		body.append("Time Started:  "+time+"<P></P>");
		body.append("</HTML>"+newline);
		getEmailSender().sendVONSMessage(body.toString(),"Long Running Deployment Warning");
		return complete;
	}
	

	public vsg.ops.email.VOPSEmailSender getEmailSender() {
		return emailSender;
	}


	public void setEmailSender(vsg.ops.email.VOPSEmailSender emailSender) {
		this.emailSender = emailSender;
	}


	public atg.service.jdbc.WatcherDataSource getDatasource() {
		return datasource;
	}


	public void setDatasource(atg.service.jdbc.WatcherDataSource datasource) {
		this.datasource = datasource;
	}


	public java.util.ArrayList<VONSEmailLineItem> getErrorArrayList() {
		return errorArrayList;
	}


	public void setErrorArrayList(
			java.util.ArrayList<VONSEmailLineItem> errorArrayList) {
		this.errorArrayList = errorArrayList;
	}


	public java.util.ArrayList<VONSEmailLineItem> getProcArrayList() {
		return procArrayList;
	}


	public void setProcArrayList(
			java.util.ArrayList<VONSEmailLineItem> procArrayList) {
		this.procArrayList = procArrayList;
	}


	public java.util.ArrayList<VONSEmailLineItem> getEscalatedErrorArrayList() {
		return escalatedErrorArrayList;
	}


	public void setEscalatedErrorArrayList(
			java.util.ArrayList<VONSEmailLineItem> escalatedErrorArrayList) {
		this.escalatedErrorArrayList = escalatedErrorArrayList;
	}



}
