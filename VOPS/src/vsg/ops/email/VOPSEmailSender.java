package vsg.ops.email;

import vsg.util.SMTPEmailSender;
import atg.service.email.MimeMessageUtils;
import javax.mail.Message;

public class VOPSEmailSender extends SMTPEmailSender {

	public String[] emptyRecipients;

	public String getFrom()
	{
		return from;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public String[] getRecipients()
	{
		return recipients;
	}

	public void setRecipients(String recipients[])
	{
		this.recipients = recipients;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	/** Getter for property title.
	 * @return Value of property title.
	 *
	 */
	public String getTitle() {
		return this.title;
	}

	/** Setter for property title.
	 * @param title New value of property title.
	 *
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public String from;
	private String recipients[];
	private String message;

	public void sendVONSMessage(String message,String title){
		try{
			Message msg= MimeMessageUtils.createMessage();
		        MimeMessageUtils.setFrom(msg, getFrom());
		        msg.setSubject(title);
		        MimeMessageUtils.setRecipients(msg, Message.RecipientType.TO, getRecipients());
		        //msg.setHeader("Content-Type", "text/html");
		        //msg.setText(body);
		        msg.setContent(message, "text/html");
		        sendEmailMessage(msg);
            }catch(Exception e){
                        e.printStackTrace();
            }


	}


	public void sendEmptyVONSMessage(String message,String title){
		try{
			Message msg= MimeMessageUtils.createMessage();
		        MimeMessageUtils.setFrom(msg, getFrom());
		        msg.setSubject(title);
		        MimeMessageUtils.setRecipients(msg, Message.RecipientType.TO, getEmptyRecipients());
		        //msg.setHeader("Content-Type", "text/html");
		        //msg.setText(body);
		        msg.setContent(message, "text/html");
		        sendEmailMessage(msg);
            }catch(Exception e){
                        e.printStackTrace();
            }


	}
	/** Holds value of property title. */
	private String title;
	public void sendTestMessage(){
            try{
            this.sendEmailMessage(getFrom(), getRecipients(), getTitle(), getMessage());
            }catch(Exception e){
                        e.printStackTrace();
            }
}

	public String[] getEmptyRecipients() {
		return emptyRecipients;
	}

	public void setEmptyRecipients(String[] emptyRecipients) {
		this.emptyRecipients = emptyRecipients;
	}


}
