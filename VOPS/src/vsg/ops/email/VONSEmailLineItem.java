package vsg.ops.email;

public class VONSEmailLineItem {
	String customer;
	String errProc;
	String errorClass;
	String procStatus;
	String component;
	String numbers;
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getNumbers() {
		return numbers;
	}
	public void setNumbers(String numbers) {
		this.numbers = numbers;
	}
	public String getErrProc() {
		return errProc;
	}
	public void setErrProc(String errProc) {
		this.errProc = errProc;
	}
	public String getErrorClass() {
		return errorClass;
	}
	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}
	public String getProcStatus() {
		return procStatus;
	}
	public void setProcStatus(String procStatus) {
		this.procStatus = procStatus;
	}
	
	

}
