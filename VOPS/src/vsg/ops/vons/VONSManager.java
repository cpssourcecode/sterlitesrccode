package vsg.ops.vons;

import atg.nucleus.GenericService;
import atg.repository.*;

public class VONSManager extends GenericService {
	
	public vsg.ops.email.VOPSEmailSender emailJob;
	public atg.adapter.gsa.GSARepository vonsRepository;
	public vsg.ops.vons.VONSConfiguration vonsConfiguration;
	
	
	
	
	public boolean logVonsError(String componentName, String errorProcString, String errorClass, String errorMessage, boolean immediateNotify){
		boolean success=false;
		try {
			MutableRepositoryItem errorItem=getVonsRepository().createItem("vonsEntry");
			errorItem.setPropertyValue("customer", getVonsConfiguration().customer);
			errorItem.setPropertyValue("server", getVonsConfiguration().server);
			errorItem.setPropertyValue("instance", getVonsConfiguration().instance);
			errorItem.setPropertyValue("component", componentName);
			errorItem.setPropertyValue("errProcString", errorProcString);
			errorItem.setPropertyValue("errorClass", errorClass);
			errorItem.setPropertyValue("errorMessage", errorMessage);
			errorItem.setPropertyValue("escalation", immediateNotify);
			getVonsRepository().updateItem(errorItem);
			getVonsRepository().addItem(errorItem);
			
			
		} catch (RepositoryException e) {
			e.printStackTrace();
			
		}
		
		return success;
	}
	
	public boolean logVonsError(String customer, String server, String instance, String componentName, String errorProcString, String errorClass, String errorMessage, boolean immediateNotify){
		boolean success=false;
		try {
			MutableRepositoryItem errorItem=getVonsRepository().createItem("vonsEntry");
			errorItem.setPropertyValue("customer", customer);
			errorItem.setPropertyValue("server", server);
			errorItem.setPropertyValue("instance", instance);
			errorItem.setPropertyValue("component", componentName);
			errorItem.setPropertyValue("errProcString", errorProcString);
			errorItem.setPropertyValue("errorClass", errorClass);
			errorItem.setPropertyValue("errorMessage", errorMessage);
			errorItem.setPropertyValue("escalation", immediateNotify);
			getVonsRepository().updateItem(errorItem);
			getVonsRepository().addItem(errorItem);
			
			
		} catch (RepositoryException e) {
			e.printStackTrace();
			
		}
		
		return success;
	}
	
	public boolean logVonsProcess(String componentName, String errorProcString, String startSuccessFail, String errorMessage, boolean immediateNotify){
		boolean success=false;
		MutableRepositoryItem errorItem;
		try {
		errorItem = getVonsRepository().createItem("vonsEntry");
		errorItem.setPropertyValue("customer", getVonsConfiguration().customer);
		errorItem.setPropertyValue("server", getVonsConfiguration().server);
		errorItem.setPropertyValue("instance", getVonsConfiguration().instance);
		errorItem.setPropertyValue("component", componentName);
		errorItem.setPropertyValue("errProcString", errorProcString);
		errorItem.setPropertyValue("procStatus", startSuccessFail);
		errorItem.setPropertyValue("errorMessage", errorMessage);
		errorItem.setPropertyValue("escalation", immediateNotify);
		getVonsRepository().updateItem(errorItem);
		getVonsRepository().addItem(errorItem);
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return success;
	}
	
	public boolean logVonsProcess(String componentName, String errorProcString, String startSuccessFail, String errorMessage, boolean immediateNotify,String customer, String server,String instance){
		boolean success=false;
		MutableRepositoryItem errorItem;
		try {
		errorItem = getVonsRepository().createItem("vonsEntry");
		errorItem.setPropertyValue("customer", customer);
		errorItem.setPropertyValue("server", server);
		errorItem.setPropertyValue("instance", instance);
		errorItem.setPropertyValue("component", componentName);
		errorItem.setPropertyValue("errProcString", errorProcString);
		errorItem.setPropertyValue("procStatus", startSuccessFail);
		errorItem.setPropertyValue("errorMessage", errorMessage);
		errorItem.setPropertyValue("escalation", immediateNotify);
		getVonsRepository().updateItem(errorItem);
		getVonsRepository().addItem(errorItem);
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return success;
	}
	
	
	
	public void generateLogError1(){
		logVonsError("/this/is/myComponent", "ERROR", "java.sql.SQLException", "This is the error message", false);
		
	}
	public void generateLogError2(){
		logVonsError("/this/is/myComponent", "ERROR", "atg.repository.RepositoryException", "This is the error message", false);
		
	}
	public void generateLogProcessStart(){
		logVonsProcess("/this/is/myComponent", "PROCESS", "START", "This is the error message", false);
		
		
	}
	public void generateLogProcessSuccess(){
		logVonsProcess("/this/is/myComponent", "PROCESS", "SUCCESS", "This is the error message", false);
		
		
	}
	public void generateLogProcessFail(){
		logVonsProcess("/this/is/myComponent", "PROCESS", "FAIL", "This is the error message", false);
		
		
	}
	
	public atg.adapter.gsa.GSARepository getVonsRepository() {
		return vonsRepository;
	}
	public void setVonsRepository(atg.adapter.gsa.GSARepository vonsRepository) {
		this.vonsRepository = vonsRepository;
	}
	public vsg.ops.email.VOPSEmailSender getEmailJob() {
		return emailJob;
	}
	public void setEmailJob(vsg.ops.email.VOPSEmailSender emailJob) {
		this.emailJob = emailJob;
	}

	public vsg.ops.vons.VONSConfiguration getVonsConfiguration() {
		return vonsConfiguration;
	}

	public void setVonsConfiguration(
			vsg.ops.vons.VONSConfiguration vonsConfiguration) {
		this.vonsConfiguration = vonsConfiguration;
	}

}
