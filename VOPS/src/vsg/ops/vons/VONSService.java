package vsg.ops.vons;

import atg.nucleus.GenericService;
import atg.dtm.TransactionDemarcation;
import atg.dtm.StaticTransactionManagerWrapper;

public class VONSService extends GenericService {
	
	public vsg.ops.vons.VONSManager vonsManager;
	public StaticTransactionManagerWrapper transactionManager;
	
	public StaticTransactionManagerWrapper getTransactionManager() {
		return transactionManager;
	}
	public void setTransactionManager(
			StaticTransactionManagerWrapper transactionManager) {
		this.transactionManager = transactionManager;
	}
	public String logVonsError(String componentName, String errorProcString, String errorClass, String errorMessage, boolean immediateNotify){
		String success=new String("Success");
		try{
		TransactionDemarcation td=new TransactionDemarcation();
		
		try{
		td.begin(getTransactionManager());
		getVonsManager().logVonsError(componentName, errorProcString, errorClass, errorMessage, immediateNotify);
		
		}finally{
			td.end();
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return success;
	}
	public String logVonsProcess(String componentName, String errorProcString, String startSuccessFail, String errorMessage, boolean immediateNotify){
		String success=new String("Success");
		try{
		TransactionDemarcation td=new TransactionDemarcation();
		try{
		td.begin(getTransactionManager());
		getVonsManager().logVonsProcess(componentName, errorProcString, startSuccessFail, errorMessage, immediateNotify);
		}finally{
			td.end();
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return success;
		
	}
	

	public void generateLogError1(){
		logVonsError("/this/is/myComponent", "ERROR", "java.sql.SQLException", "This is the error message", false);
		
	}
	public void generateLogError2(){
		logVonsError("/this/is/myComponent", "ERROR", "atg.repository.RepositoryException", "This is the error message", false);
		
	}
	public void generateLogProcessStart(){
		logVonsProcess("/this/is/myComponent", "PROCESS", "START", "This is the error message", false);
		
		
	}
	public void generateLogProcessSuccess(){
		logVonsProcess("/this/is/myComponent", "PROCESS", "SUCCESS", "This is the error message", false);
		
		
	}
	public void generateLogProcessFail(){
		logVonsProcess("/this/is/myComponent", "PROCESS", "FAIL", "This is the error message", false);
		
		
	}
	
	
	public vsg.ops.vons.VONSManager getVonsManager() {
		return vonsManager;
	}
	public void setVonsManager(vsg.ops.vons.VONSManager vonsManager) {
		this.vonsManager = vonsManager;
	}
	
	

}
