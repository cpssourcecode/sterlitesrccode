/*
 * InstanceStatusService.java
 *
 * Created on July 8, 2014
 */
package vsg.ops.monitor;

import atg.nucleus.GenericService;
import vsg.util.SMTPEmailSender;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

/**
 *
 * @author  M.Chapman
 */
public class InstanceStatusService extends GenericService {
	private String from;
	private String recipient;
	private String subject;
	private String body;
	private String client;
    /** Holds value of property instances */
    private String[] instances;
    /** Holds value of property activeInstances */
    private Vector<InstanceStatus> activeInstances;
    /** Holds value of property smtpEmailSender */
    private SMTPEmailSender smtpEmailSender;

    /** Creates a new instance of InstanceStatusService */
    public InstanceStatusService() {
    }

    public void runService() {
	    if( getActiveInstances() == null ) {
			initInstances();
		}
	    checkInstanceStatus();
	    processEmail();
    }

    private void initInstances() {
		setActiveInstances(new Vector<InstanceStatus>());
		if( getInstances() != null ) {
			for( int i=0; i < getInstances().length; i++ ) {
				InstanceStatus instanceStatus = new InstanceStatus();
				instanceStatus.setName(getInstances()[i]);
				if( isLoggingDebug() ) {
					logDebug("init instance: " + instanceStatus.getName());
				}
				instanceStatus.setStatus("Up");
				instanceStatus.setNotified(false);
				getActiveInstances().add(instanceStatus);
			}
		}
    }

    private void checkInstanceStatus() {
		Vector<InstanceStatus> stati = new Vector<InstanceStatus>();
		for( int j=0; j < getActiveInstances().size(); j++ ) {
			InstanceStatus instance = (InstanceStatus)getActiveInstances().elementAt(j);
			try {
				String urlHolder1 = "http://"+instance.getName();
				URL url1 = new URL(urlHolder1);
				HttpURLConnection conn1 = (HttpURLConnection)url1.openConnection();
				conn1.getResponseCode();
				instance.setStatus("Up");
			} catch(java.lang.Exception e) {
				instance.setStatus("Down");
			}
			if( isLoggingDebug() ) {
				logDebug("check instance: " + instance.getName() + " has status: " + instance.getStatus());
			}
			stati.add(instance);
		}//end instances for
        setActiveInstances(stati);
	}

	private void processEmail() {
		Vector<InstanceStatus> emailInstances = new Vector<InstanceStatus>();
		for( int k=0; k < getActiveInstances().size(); k++ ) {
			InstanceStatus emailInstance = (InstanceStatus)getActiveInstances().elementAt(k);
			if( emailInstance.getStatus().equals("Down") && !emailInstance.isNotified() ) {
				if( isLoggingDebug() ) {
					logDebug("instance: " + emailInstance.getName() + " is down and OPS needs to be notified");
				}
				setSubject(getClient() + " Instance Down");
				setBody(emailInstance.getName() + " is down.  Please restart.");
				try {
					getSmtpEmailSender().sendEmailMessage(getFrom(), getRecipient(), getSubject(), getBody());
				} catch(Exception e) {
					e.printStackTrace();
				}
				emailInstance.setNotified(true);
			} else if( emailInstance.getStatus().equals("Up") && emailInstance.isNotified() ) {
				if( isLoggingDebug() ) {
					logDebug("instance: " + emailInstance.getName() + " is restarted and OPS needs to be notified");
				}
				setSubject(getClient() + " Instance Restarted");
				setBody(emailInstance.getName() + " is back up. ");
				try {
					getSmtpEmailSender().sendEmailMessage(getFrom(), getRecipient(), getSubject(), getBody());
				} catch(Exception e) {
					e.printStackTrace();
				}
				emailInstance.setNotified(false);
			} else {
				if( isLoggingDebug() ) {
					logDebug("instance is stable: " + emailInstance.getName());
				}
			} //end send email
			emailInstances.add(emailInstance);
		} //end email for
		setActiveInstances(emailInstances);
    } //end processEmail

	/**  Get and Set methods for from property */
	public String getFrom() {
		return this.from;
	}
	public void setFrom(String from) {
		this.from = from;
	}

	/**  Get and Set methods for recipient property */
	public String getRecipient() {
		return this.recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/**  Get and Set methods for subject property */
	public String getSubject() {
		return this.subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**  Get and Set methods for body property */
	public String getBody() {
		return this.body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	/**  Get and Set methods for client property */
	public String getClient() {
		return this.client;
	}
	public void setClient(String client) {
		this.client = client;
	}

    /** Getter for property instances
     * @return Value of property instances
     *
     */
    public String[] getInstances() {
        return this.instances;
    }
    /** Setter for property instances
     * @param instances New value of property instances
     *
     */
    public void setInstances(String[] instances) {
        this.instances = instances;
    }

    /** Getter for property activeInstances
     * @return Value of property activeInstances
     *
     */
    public Vector<InstanceStatus> getActiveInstances() {
        return this.activeInstances;
    }
    /** Setter for property activeInstances
     * @param activeInstances New value of property activeInstances
     *
     */
    public void setActiveInstances(Vector<InstanceStatus> activeInstances) {
        this.activeInstances = activeInstances;
    }

    /** Getter for property smtpEmailSender
     * @return Value of property smtpEmailSender
     *
     */
    public SMTPEmailSender getSmtpEmailSender() {
        return this.smtpEmailSender;
    }
    /** Setter for property smtpEmailSender
     * @param smtpEmailSender New value of property smtpEmailSender
     *
     */
    public void setSmtpEmailSender(SMTPEmailSender smtpEmailSender) {
        this.smtpEmailSender = smtpEmailSender;
    }

}