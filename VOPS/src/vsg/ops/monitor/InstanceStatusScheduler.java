/*
 * InstanceStatusScheduler.java
 *
 * Created on July 8, 2014
 */
package vsg.ops.monitor;

import atg.service.scheduler.SchedulableService;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

/**
 *
 * @author  M.Chapman
 */
public class InstanceStatusScheduler extends SchedulableService {
	private InstanceStatusService instanceStatusService;

    /** Creates a new instance of InstanceStatusScheduler */
    public InstanceStatusScheduler() {
    }

    public void doStartService() {
		setThreadMethod(ScheduledJob.REUSED_THREAD);
		super.startScheduledJob();
    }

	public void doStopService() {
		super.stopScheduledJob();
	}

    public void performScheduledTask(Scheduler scheduler, ScheduledJob scheduledJob) {
	    getInstanceStatusService().runService();
    }

	/**  Get and Set methods for instanceStatusService property */
	public InstanceStatusService getInstanceStatusService() {
		return this.instanceStatusService;
	}
	public void setInstanceStatusService(InstanceStatusService instanceStatusService) {
		this.instanceStatusService = instanceStatusService;
	}

}