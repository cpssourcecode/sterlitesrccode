package vsg.ops.monitor;

import atg.nucleus.GenericService;
import vsg.ops.email.VONSEmailManager;
import vsg.ops.vons.VONSManager;
import vsg.ops.util.NotificationLineItem;
import static java.util.concurrent.TimeUnit.*;

import java.util.Calendar;
import java.util.concurrent.*;



public class SchedulerMonitorService extends GenericService implements java.lang.Runnable {
	private final ScheduledExecutorService executorScheduler = Executors.newScheduledThreadPool(1);
	public atg.service.scheduler.Scheduler scheduler;
	public VONSEmailManager emailManager;
	public String customer;
	public String server;
	public String instance;
	public VONSManager vonsManager;
	public boolean enabled;




	public void pollScheduler(){
		logInfo("Polling Start time:  "+Calendar.getInstance().getTime().toString());
		java.lang.Long nextTime=getScheduler().getNextTime();
		nextTime=nextTime+1000000;
		java.lang.Long currentTime=java.util.Calendar.getInstance().getTimeInMillis();
		logDebug("Next time:  "+nextTime);
		logDebug("Current time:  "+java.util.Calendar.getInstance().getTimeInMillis());
		if(nextTime>currentTime){
			logDebug("All Cool");
		}else{
			logInfo("Houston We have a problem");
			NotificationLineItem nli=new NotificationLineItem();
			nli.setEmailTitle(getCustomer()+" Scheduler Issue");
			nli.setGenericMessage("Scheduler next time:  "+getScheduler().getNextTimeFormatted());
			nli.setCustomer(getCustomer());
			nli.setComponent("/atg/dynamo/service/Scheduler");
			nli.setServer(getServer());
			nli.setInstance(getInstance());
			nli.setErrorMessage("Scheduler next time is less than Scheduler current time");
			getEmailManager().sendImmediateMessage(nli);
		}



	}

	public void run(){

	}



	public void doStartService(){
		if(isEnabled()){
		executeService();
		}
	}

	public void executeService(){
		final Runnable poll = new Runnable() {
		       public void run() { pollScheduler(); }
		};
		final ScheduledFuture<?> pollHandle =
	    		 executorScheduler.scheduleAtFixedRate(poll, 2, 30, MINUTES);
	}


	public void test() {
		final Runnable beeper = new Runnable() {
		       public void run() { System.out.println("beep"); }
		     };
		     final ScheduledFuture<?> beeperHandle =
		    		 executorScheduler.scheduleAtFixedRate(beeper, 10, 10, SECONDS);



	}

	public VONSEmailManager getEmailManager() {
		return emailManager;
	}



	public void setEmailManager(VONSEmailManager emailManager) {
		this.emailManager = emailManager;
	}


	public VONSManager getVonsManager() {
		return vonsManager;
	}



	public void setVonsManager(VONSManager vonsManager) {
		this.vonsManager = vonsManager;
	}

	public atg.service.scheduler.Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(atg.service.scheduler.Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getInstance() {
		return instance;
	}

	public void setInstance(String instance) {
		this.instance = instance;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}





}
