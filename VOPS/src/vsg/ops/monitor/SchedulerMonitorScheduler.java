package vsg.ops.monitor;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

public class SchedulerMonitorScheduler extends GenericService{

	
	
private int jobId;
    
    Scheduler scheduler;
   public Scheduler getScheduler () {
       return scheduler;
   }
   public void setScheduler (Scheduler scheduler) {
       this.scheduler = scheduler;
   }
   
   // Schedule property
   Schedule schedule;
   public Schedule getSchedule ()  {return schedule;}
   public void setSchedule (Schedule schedule){this.schedule = schedule;}
   
   /**
    * Job name
    */
   private String jobName;
   public void setJobName(String jobName) { this.jobName = jobName; }
   public String getJobName() { return jobName; }
   
   /**
    * Job description
    */
   private String jobDescription;
   public void setJobDescription(String jobDescription) { this.jobDescription = jobDescription; }
   public String getJobDescription() { return jobDescription; }
   
   /**
    * Job component path
    */
   private String jobComponentPath;
   public void setJobComponentPath(String jobComponentPath) { this.jobComponentPath = jobComponentPath; }
   public String getJobComponentPath() { return jobComponentPath; }
   
   public void performScheduledTask (Scheduler scheduler,
   ScheduledJob job)
   {
   }
   public void doStartService () throws ServiceException {
       // resolve the component that performs the scheduled task
       atg.service.scheduler.Schedulable schedulable = (atg.service.scheduler.Schedulable) resolveName(getJobComponentPath());
       ScheduledJob job = new ScheduledJob(getJobName(),
       getJobDescription(),
       getJobComponentPath(),
       getSchedule(),
       schedulable,
       ScheduledJob.REUSED_THREAD);
       jobId = getScheduler ().addScheduledJob (job);
   }
   
   // Stop method
   public void doStopService () throws ServiceException {
       getScheduler ().removeScheduledJob (jobId);
   }



	

}
