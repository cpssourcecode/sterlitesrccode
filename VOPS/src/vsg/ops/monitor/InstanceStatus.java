/*
 * InstanceStatus.java
 *
 * Created on July 8, 2014
 */
package vsg.ops.monitor;

import java.io.Serializable;

/**
 *
 * @author  M.Chapman
 */
public class InstanceStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	/** Holds value of property name. */
    private String name;
    /** Holds value of property status. */
    private String status;
    /** Holds value of property notified. */
    private boolean notified;

    /** Creates a new instance of InstanceStatus */
    public InstanceStatus() {
    }

    /** Getter for property name
     * @return Value of property name
     *
     */
    public String getName() {
        return this.name;
    }
    /** Setter for property name
     * @param name New value of property name
     *
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Getter for property status
     * @return Value of property status
     *
     */
    public String getStatus() {
        return this.status;
    }
    /** Setter for property status
     * @param status New value of property status
     *
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /** Getter for property notified.
     * @return Value of property notified.
     *
     */
    public boolean isNotified() {
        return this.notified;
    }
    /** Setter for property notified.
     * @param notified New value of property notified.
     *
     */
    public void setNotified(boolean notified) {
        this.notified = notified;
    }

}