package vsg.endeca.index.admin;

import atg.endeca.index.admin.SimpleIndexingAdmin;
import atg.repository.search.indexing.IndexingException;
import java.util.Date;
import vsg.ops.vons.VONSService;

public class VSGSimpleIndexingAdmin extends SimpleIndexingAdmin {
	
	VONSService vonsService;
	
	
	public boolean indexBaseline() {
		boolean successIndex = true;
		try {
			getVonsService().logVonsError(getName(), "INFO", getName(), getName() + " started indexBaseline method: " + new Date(), false);
			//save start time
			if( isLoggingDebug() ) {
				logDebug("about to call the baseline indexing: " + new Date());
			}
			successIndex = super.indexBaseline();
			//save stop time
			if( isLoggingDebug() ) {
				logDebug("baseline indexing completed: " + new Date());
			}
			if( !successIndex ) {
				//add VONS hook for immediate notification
				if( isLoggingDebug() ) {
					logDebug("failure returned for indexing: " + new Date());
				}
				getVonsService().logVonsError(getName(), "ERROR", this.getAbsoluteName(), (getName() + " failure returned for indexing: " + new Date()), true);
			}
		} catch(IndexingException iex) {
			//add VONS hook for immediate notification with exception
			if( isLoggingDebug() ) {
				logDebug("IndexingException returned for indexing: " + new Date());
			}
			// send email
			
			getVonsService().logVonsError(getName(), "ERROR", iex.getClass().getCanonicalName(), iex.getMessage(), true);
		} catch(Exception ex) {
			//add VONS hook for immediate notification with exception
			if( isLoggingDebug() ) {
				logDebug("exception returned for indexing: " + new Date());
			}
			// send email
		
			getVonsService().logVonsError(getName(), "ERROR", ex.getClass().getCanonicalName(), ex.getMessage(), true);
		}

		getVonsService().logVonsError(getName(), "INFO", getName(), getName() + " finished indexBaseline method with: " + successIndex + " on: " + new Date(), false);
		return successIndex;
	}
	
	
	public boolean indexPartial() {
		boolean successIndex = true;
		try {
			getVonsService().logVonsError(getName(), "INFO", getName(), getName() + " started indexPartial method: " + new Date(), false);
			//save start time
			if( isLoggingDebug() ) {
				logDebug("about to call the partial indexing: " + new Date());
			}
			successIndex = super.indexPartial();
			//save stop time
			if( isLoggingDebug() ) {
				logDebug("partial indexing completed: " + new Date());
			}
			if( !successIndex ) {
				//add VONS hook for immediate notification
				if( isLoggingDebug() ) {
					logDebug("failure returned for partial indexing: " + new Date());
				}
				getVonsService().logVonsError(getName(), "ERROR", this.getAbsoluteName(), (getName() + " failure returned for partial indexing: " + new Date()), true);
			}
		} catch(IndexingException iex) {
			//add VONS hook for immediate notification with exception
			if( isLoggingDebug() ) {
				logDebug("IndexingException returned for indexing: " + new Date());
			}
			// send email
			
			getVonsService().logVonsError(getName(), "ERROR", iex.getClass().getCanonicalName(), iex.getMessage(), true);
		} catch(Exception ex) {
			//add VONS hook for immediate notification with exception
			if( isLoggingDebug() ) {
				logDebug("exception returned for partial indexing: " + new Date());
			}
			// send email
			
			getVonsService().logVonsError(getName(), "ERROR", ex.getClass().getCanonicalName(), ex.getMessage(), true);
		}
		getVonsService().logVonsError(getName(), "INFO", getName(), getName() + " finished indexPartial method with: " + successIndex + " on: " + new Date(), false);
		return successIndex;
	}


	public VONSService getVonsService() {
		return vonsService;
	}

	public void setVonsService(VONSService vonsService) {
		this.vonsService = vonsService;
	}
	
	
	
	

}
