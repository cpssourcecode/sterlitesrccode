#!/bin/bash
#------------------------------------------------------------------------------
# Variable declarations
#------------------------------------------------------------------------------

WL_INSTALL_DIR=$1
WL_DOMAIN_NAME=$2

WL_DOMAINS_DIR=user_projects/domains
    
# Execute start script
wlExists=`ps -eaf | grep "java"  | grep "AdminServer" | wc -l`

if [ $wlExists == 1 ]; then
     "$WL_INSTALL_DIR/$WL_DOMAINS_DIR/$WL_DOMAIN_NAME/bin/stopWebLogic.sh"
fi

"$WL_INSTALL_DIR/$WL_DOMAINS_DIR/$WL_DOMAIN_NAME/startWebLogic.sh"
echo "WebLogic starting"
