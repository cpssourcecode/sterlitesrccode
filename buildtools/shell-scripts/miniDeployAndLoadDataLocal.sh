#!/bin/bash

ENDECA_APPS=$1
APP_NAME=$2
DEPLOYMENT_TEMPLATE=$3
INSTALL_CONFIG_XML_PATH=$4
ENDECA_SITES=$5
ENDECA_XM_TEMPLATE_NAME=$6
ENDECA_APPS_TMP=$7

echo "----- Check if temporary folder exist..."
if [ ! -d "$ENDECA_APPS_TMP" ]; then
  mkdir $ENDECA_APPS_TMP
fi

echo "----- Deploying template..."
$ENDECA_TOOLS_ROOT/deployment_template/bin/deploy.sh --no-prompt --install-config $DEPLOYMENT_TEMPLATE/$INSTALL_CONFIG_XML_PATH

echo "----- Creating Endeca XM Sites..." 
export IFS="#"

for endecaSite in $ENDECA_SITES; do
$ENDECA_APPS_TMP/$APP_NAME/control/createEndecaSite.sh $endecaSite $endecaSite ${endecaSite,,} $ENDECA_XM_TEMPLATE_NAME $ENDECA_APPS_TMP/$APP_NAME/config
done

echo "----- Copying config folder..."
cp -R $ENDECA_APPS_TMP/$APP_NAME/config/. $ENDECA_APPS/$APP_NAME/config/

echo "---- Running modifiers..."
$ENDECA_APPS/$APP_NAME/control/set_templates.sh
$ENDECA_APPS/$APP_NAME/control/set_editors_config.sh
$ENDECA_APPS/$APP_NAME/control/runcommand.sh IFCR importApplication $ENDECA_APPS/$APP_NAME/config/import
$ENDECA_APPS/$APP_NAME/control/promote_content.sh

echo "----- Removing old temp application Archives-----"
cd $ENDECA_APPS_TMP
ls | sort -n -t _ -k 2 | grep "$APP_NAME." | head --lines=-3 | xargs rm -fr
cd -
