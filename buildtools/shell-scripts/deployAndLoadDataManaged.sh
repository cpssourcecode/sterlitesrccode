#!/bin/bash 

# $1 - Path to Key file
# $2 - Path to Compressed EAR
# $3 - Compresssed EAR file name
# $4 - User name
# $5 - IP Address
# $6 - Destination Directory
# $7 - Change directory to look for folder to compress
# $8 - Folder to compress (Deployment Template Folder)
# $9 - Endeca Apps Folder Path
# $10 - Endeca App Name
# $11 - Site Content XML Path 
# $12 - Install Config XML Path 

mkdir -p $2

rm -rf $2/$3 
tar -zcf $2/$3 -C $7 $8 

scp -i $1 -oStrictHostKeyChecking=no -oCheckHostIP=no $2/$3 $4@$5:$6

ssh -i $1 -oStrictHostKeyChecking=no -oCheckHostIP=no $4@$5 <<INNER
rm -rf $6/$8
tar -zxf $6/$3 -C $6

ENDECA_APPS=$9
APP_NAME=${10}
DEPLOYMENT_TEMPLATE_DIR=$6
DEPLOYMENT_TEMPLATE=$8
SITE_CONTENT_XML_PATH=${11}
INSTALL_CONFIG_XML_PREFIX=${12}
ENDECA_SITES=${13}
ENDECA_XM_TEMPLATE_NAME=${14}
LANGUAGE_KEYS=${15}

export IFS="#"

for language in \$LANGUAGE_KEYS; do

echo "----- Deploying template..."
\$ENDECA_TOOLS_ROOT/deployment_template/bin/deploy.sh --no-prompt --install-config \$DEPLOYMENT_TEMPLATE_DIR/\$DEPLOYMENT_TEMPLATE/\$INSTALL_CONFIG_XML_PREFIX\$language.xml

echo "----- Creating Endeca XM Sites..." 

for endecaSite in \$ENDECA_SITES; do
\$ENDECA_APPS/\$APP_NAME\$language/control/createEndecaSite.sh \$endecaSite \$endecaSite \${endecaSite,,} \$ENDECA_XM_TEMPLATE_NAME \$ENDECA_APPS/\$APP_NAME\$language/config
done

echo "----- Initializing Services..."
\$ENDECA_APPS/\$APP_NAME\$language/control/initialize_services.sh --force
    
echo "----- Promoting Content from Authoring to Live-----"
\$ENDECA_APPS/\$APP_NAME\$language/control/promote_content.sh

echo "----- Removing old application Archives-----" 
cd \$ENDECA_APPS 
ls | sort -n -t _ -k 2 | grep -F "\$APP_NAME\$language." | head --lines=-3 | xargs rm -fr 
cd -

done

INNER

