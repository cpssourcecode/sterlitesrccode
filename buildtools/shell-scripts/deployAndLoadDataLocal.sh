#!/bin/bash

ENDECA_APPS=$1
APP_NAME=$2
DEPLOYMENT_TEMPLATE=$3
SITE_CONTENT_XML_PATH=$4
INSTALL_CONFIG_XML_PATH=$5
ENDECA_SITES=$6
ENDECA_XM_TEMPLATE_NAME=$7

echo "----- Deploying template..."
$ENDECA_TOOLS_ROOT/deployment_template/bin/deploy.sh --no-prompt --install-config $DEPLOYMENT_TEMPLATE/$INSTALL_CONFIG_XML_PATH

echo "----- Creating Endeca XM Sites..." 
export IFS="#"

for endecaSite in $ENDECA_SITES; do
$ENDECA_APPS/$APP_NAME/control/createEndecaSite.sh $endecaSite $endecaSite ${endecaSite,,} $ENDECA_XM_TEMPLATE_NAME $ENDECA_APPS/$APP_NAME/config
done

echo "----- Initializing Services..."
$ENDECA_APPS/$APP_NAME/control/initialize_services.sh --force
    
echo "----- Promoting Content from Authoring to Live-----"
$ENDECA_APPS/$APP_NAME/control/promote_content.sh

echo "----- Removing old application Archives-----"
cd $ENDECA_APPS
ls | sort -n -t _ -k 2 | grep "$APP_NAME." | head --lines=-3 | xargs rm -fr
cd -
