WL_HOME=$1
SCRIPT_DIR=$2
WL_SERVER=$3
EAR_PATH=$4
APP_NAME=$5
cd $WL_HOME/common/bin
./wlst.sh $SCRIPT_DIR/stopServer.py $WL_SERVER
./wlst.sh $SCRIPT_DIR/deployApplication.py $EAR_PATH $APP_NAME $WL_SERVER
./wlst.sh $SCRIPT_DIR/startServer.py $WL_SERVER
