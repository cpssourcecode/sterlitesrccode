#!/bin/bash

HOSTNAME=$1
PORT=$2

result=$(curl --silent -X POST http://${HOSTNAME}:${PORT}/rest/bean/atg/commerce/endeca/index/ProductCatalogSimpleIndexingAdmin/indexBaseline)
echo "$result"
if [ "$result" != "{\"atgResponse\": true}" ]
then
	echo "Executing Baseline Update Failed."
	exit 1
fi
