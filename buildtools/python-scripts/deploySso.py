connect('weblogic','webl0gic', 't3://localhost:7001')

edit()
startEdit()

try:
    undeploy(appName='sso',  timeout=60000)
    deploy(appName='sso', path='/opt/dev/products/weblogicApps/local/CPSsso.ear', targets='CPSSSO', stageMode='stage')
    save()
    activate()          
    print "~~~~COMMAND SUCCESFULL~~~~"

except:
    print "Unexpected error:", sys.exc_info()[0]
    undo('true','y')
    stopEdit('y')
    print "~~~~COMMAND FAILED~~~~"
    raise
