connect('weblogic','webl0gic', 't3://localhost:7001')

env = sys.argv[1]

edit()
startEdit()

try:
    undeploy(appName='CPSProd',  timeout=60000)
    deploy(appName='CPSProd', path='/opt/dev/products/weblogicApps/' + env + '/CPSProd.ear', targets='CPSProd', stageMode='nostage')
    save()
    activate()          
    print "~~~~COMMAND SUCCESFULL~~~~"

except:
    print "Unexpected error:", sys.exc_info()[0]
    undo('true','y')
    stopEdit('y')
    print "~~~~COMMAND FAILED~~~~"
    raise
