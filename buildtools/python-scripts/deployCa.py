connect('weblogic','webl0gic', 't3://localhost:7001')

edit()
startEdit()

try:
    undeploy(appName='ca',  timeout=60000)
    deploy(appName='ca', path='/opt/dev/products/weblogicApps/local/CPSPub.ear', targets='CPSPub', stageMode='stage')
    save()
    activate()          
    print "~~~~COMMAND SUCCESFULL~~~~"

except:
    print "Unexpected error:", sys.exc_info()[0]
    undo('true','y')
    stopEdit('y')
    print "~~~~COMMAND FAILED~~~~"
    raise
