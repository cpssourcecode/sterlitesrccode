connect('weblogic','webl0gic', 't3://localhost:7001')

edit()
startEdit()

try:
    undeploy(appName='aux',  timeout=60000)
    deploy(appName='aux', path='/opt/dev/products/weblogicApps/local/CPSaux.ear', targets='CPSAux', stageMode='stage')
    save()
    activate()          
    print "~~~~COMMAND SUCCESFULL~~~~"

except:
    print "Unexpected error:", sys.exc_info()[0]
    undo('true','y')
    stopEdit('y')
    print "~~~~COMMAND FAILED~~~~"
    raise
