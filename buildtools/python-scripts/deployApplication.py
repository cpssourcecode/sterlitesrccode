connect('weblogic','webl0gic', 't3://localhost:7001')

earPath = sys.argv[1]
appName = sys.argv[2]
wlServer = sys.argv[3]

edit()
startEdit()

try:
    undeploy(appName=appName,  timeout=60000)
    deploy(appName=appName, path=earPath, targets=wlServer, stageMode='nostage')
    save()
    activate()          
    print "~~~~COMMAND SUCCESFULL~~~~"

except:
    print "Unexpected error:", sys.exc_info()[0]
    undo('true','y')
    stopEdit('y')
    print "~~~~COMMAND FAILED~~~~"
    raise
