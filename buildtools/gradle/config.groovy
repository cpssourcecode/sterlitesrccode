// File: config.groovy

// http://mrhaki.blogspot.nl/2009/11/gradle-goodness-using-properties-for.html
common {
    shellscriptsfolder = 'buildtools/shell-scripts'
    pythonscriptsfolder = 'buildtools/python-scripts'
}

db {
    host = 'localhost'
    port = 1521
    name = 'XE'
    jdbcdriverpath = '/Software/ojdbc6.jar'
    core {
        username = 'CPS_core'
        password = 'CPS_core'
        jndi = 'ATGCoreDS'
        datasourceid = 'nonswitchingCore'
    }
    ca {
        username = 'CPS_ca'
        password = 'CPS_ca'
        jndi = 'ATGCaDS'
        datasourceid = 'management'
    }
}

weblogic {
    wlhome = '/opt/dev/Middleware/wlserver'
    wlinstalldir='/opt/dev/Middleware/'
    storefront {
        host = 'localhost'
        port = 7103
    }
}

cim {
    core {
        productidlist = 'endeca,publishing,siteadmin,REST,wcs_extensions,endeca_reader,commerce'
        addonidlist = 'endeca,endeca_reader,sso,merch,preview,ssoIntegration,nonswitchingdatasource,dcsui_siteadmin_versioned,REST,publishing_management,publishing_nonswitchingdatasource,publishing_externalPreviewServer'
    }
    ca {
        productidlist = 'endeca,publishing,siteadmin,wcs_extensions,endeca_reader,commerce'
        addonidlist = 'endeca,endeca_reader,sso,merch,preview,ssoIntegration,nonswitchingdatasource,dcsui_siteadmin_versioned,publishing_management,publishing_nonswitchingdatasource,publishing_externalPreviewServer'
    }
}


	
deploy {
    port = 22
    keyfile = ''
    remotehost = ''
    remoteuser = ''
    destdir = ''
    servershortcutsdir='/home/vagrant/Desktop'
    domainsdir = '/opt/dev/Middleware/user_projects/domains'
    assembledir = '/opt/dev/products/weblogicApps'
    scriptsdir = '/opt/scripts/wls'
	
	assemble {
		app { 
			modules = 'DafEar.Admin DCS.PublishingAgent DCS.AbandonedOrderServices DPS.WebServices DCS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index VSG.VSGOrderExport Columbia.CPS Columbia.CPSDataLoad '
			standalone=''
			layer=''
			dynamoserver='cps_prod_local'
			otheroptions=''
			earname = 'CPSProd.ear'
			name = 'App'
			wlserver = 'cps_prod_local'
			wlapp = 'CPSProd'
		}
		ca {
			modules = 'DCS-UI.Versioned BIZUI PubPortlet DafEar.Admin DCS-UI.SiteAdmin.Versioned SiteAdmin.Versioned ContentMgmt.Endeca.Index.Versioned DCS.Versioned DPS-UI DCS.Endeca.Index.Versioned DCS.Endeca.Assembler ContentMgmt-UI.Versioned BCC.Versioned Columbia.CPSDataLoad.Versioned Columbia.CPS.Versioned'
			standalone=''
			layer=''
			dynamoserver='cps_pub_local'
			otheroptions=''
			earname = 'CPSPub.ear'
			name = 'Ca'
			wlserver = 'cps_pub_local'
			wlapp = 'CPSPub'
		}
		sso {
			modules = 'DafEar.Admin SSO DafEar'
			standalone=''
			layer=''
			dynamoserver='cps_sso_local'
			otheroptions=''
			earname = 'CPSSso.ear'
			name = 'Sso'
			wlserver = 'cps_sso_local'
			wlapp = 'CPSSso'
		}
		aux {
			modules = 'DafEar.Admin DCS.PublishingAgent DCS.WebServices DCS.AbandonedOrderServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index VSG.VSGOrderExport Columbia.CPS Columbia.CPSDataLoad'
			standalone=''
			layer=''
			dynamoserver='cps_aux_local'
			otheroptions=''
			earname = 'CPSAux.ear'
			name = 'Aux'
			wlserver='cps_aux_local'
			wlapp = 'CPSAux'
		}
		stage {
			modules = 'DafEar.Admin DCS.PublishingAgent DPS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index Columbia.CPS Columbia.CPSDataLoad'
			standalone=''
			layer='EndecaPreview'
			dynamoserver='cps_stage_local'
			otheroptions=''
			earname = 'CPSStage.ear'
			name = 'Stage'
			wlserver='cps_stage_local'
			wlapp = 'CPSStage'
		}
		
	}
}

axis {
	CPSInventory = 'ItemAvailabilityManager'
	CPSStatement = 'ARStatementsManager'
	CPSOrder = 'order'
	CPSMaterialTestReport = 'pricing'
	SnapPay = 'manageservices'
	CPSPricing = 'SalesOrderManager'
	CPSOnHoldAccount = 'OnHoldAccountManager'
	CPSInvoice = 'InvoiceStatusManager'
	CPSPackingSlip = 'packingslip'
	CPSOrderStatus = 'OrderStatusManager'
}

apache {
	wwwroot = '/opt/dev/assets'
}

endeca {
    appspath = '/opt/dev/endeca/Apps'
    tmpappspath = '/home/vagrant/tmp'
    appname = 'CPS'
    pathtodeploymenttemplate = 'endeca'
    deploymenttemplate = 'deployment-template-updated2'
    sites = 'CPS'
    templatesitename = 'template'

    installconfigs {
        eacport = "8888"
        workbenchhost = "localhost"
        workbenchport = "8006"
        cashost = "localhost"
        casport = "8500"
        casroot = "/opt/dev/endeca/CAS/11.3.0"
        casjarversion = "11.3.0"
        prodappserverhost = "localhost"
        prodappserverport = "8001"
        previewhost = "localhost"
        previewport = "8001"
        previewcontextroot = "/"
        usersegmentshost = "localhost"
        usersegmentsport = "7103"
        jpsconfiglocation = "/opt/dev/endeca/ToolsAndFrameworks/11.3.0/server/workspace/credential_store/jps-config.xml"
        appexportdirectory = "/opt/dev/endeca/Apps/CPS/data/workbench/application_export_archive"
        authappexportdirectory = "/opt/dev/endeca/ToolsAndFrameworks/11.3.0/server/workspace/state/generation_data"
        additionalmodule = "deploy.xml"
        fileprefix = "cps-install-config_"
        templatename = "install-config.xml.template"
    
        languages{
            en {
                dgraph1port = "15000"
                authdgraphport = "15002"
                logserverport = "15010"
                languageid = "en"
            }
        }
    }
}

environments {

    local {
    	deploy { 
	    	assemble {
				app {
					modules = 'DafEar.Admin DCS.PublishingAgent DCS.AbandonedOrderServices DPS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index VSG.VSGOrderExport Columbia.CPS Columbia.CPSDataLoad '
	    		}
	    	}
    	}
    }
    
    dev {

  	apache { 
  		wwwroot = '/apps/staticAssets/assets' 
  	}

	deploy {
	    port = 22
	    keyfile = ''
	    remotehost = ''
	    remoteuser = ''
	    destdir = ''
	    servershortcutsdir='/home/oracle/Desktop'
	    domainsdir = '/apps/Oracle/Middleware/Oracle_Home/user_projects/domains'
	    assembledir = '/apps/products/weblogicApps'
	    scriptsdir = '/opt/scripts/wls'
	
		assemble {
			app {
				standalone='standalone'
				layer=''
				dynamoserver='DEVpageServer1'
				otheroptions=''
				earname = 'CPSProd.ear'
				name = 'App'
				wlserver = 'DEVpageServer1'
				wlapp = 'CPSProd'
			}
			ca {
				standalone='standalone'
				layer='staging'
				dynamoserver='DEVbcc'
				otheroptions=''
				earname = 'CPSPub.ear'
				name = 'Ca'
				wlserver = 'DEVbcc'
				wlapp = 'CPSPub'
			}
			sso {
				standalone='standalone'
				layer=''
				dynamoserver='DEVsso'
				otheroptions=''
				earname = 'CPSSso.ear'
				name = 'Sso'
				wlserver = 'DEVsso'
				wlapp = 'CPSSso'
			}
			aux {
				standalone='standalone'
				layer=''
				dynamoserver='DEVauxServer1'
				otheroptions=''
				earname = 'CPSAux.ear'
				name = 'Aux'
				wlserver='DEVauxServer1'
				wlapp = 'CPSAux'
			}
			stage {
				standalone='standalone'
				layer='EndecaPreview'
				dynamoserver='DEVstagingPreview'
				otheroptions=''
				earname = 'CPSStage.ear'
				name = 'Stage'
				wlserver='DEVstagingPreview'
				wlapp = 'CPSStage'
			}
		}
	}
 }
   
	uat {

		weblogic {
		    wlhome = '/opt/oracle/product/Middleware/wlserver'
		    wlinstalldir='/opt/oracle/product/Middleware/'
		    storefront {
			host = 'uatatgendeca.columbiapipe.com'
			port = 7101
		    }
		}


		deploy {
		    servershortcutsdir='/home/oracle/Desktop'
		    domainsdir = '/opt/oracle/product/Middleware/user_projects/domains'
		    assembledir = '/opt/oracle/product/products/weblogicApps'
		    scriptsdir = '/opt/scripts/wls'
	
			assemble {
				app {
					modules = 'DafEar.Admin DCS.PublishingAgent DCS.AbandonedOrderServices DPS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index Columbia.CPS Columbia.CPSDataLoad '
					standalone='standalone'
					layer=''
					dynamoserver=''
					otheroptions=''
					earname = 'CPSProd.ear'
					name = 'App'
					wlserver = ''
					wlapp = 'CPSProd'
				}
				ca {
					standalone='standalone'
					layer='staging'
					dynamoserver=''
					otheroptions=''
					earname = 'CPSPub.ear'
					name = 'Ca'
					wlserver = ''
					wlapp = 'CPSPub'
				}
				sso {
					standalone='standalone'
					layer=''
					dynamoserver=''
					otheroptions=''
					earname = 'CPSSso.ear'
					name = 'Sso'
					wlserver = ''
					wlapp = 'CPSSso'
				}
				aux {
					standalone='standalone'
					layer=''
					dynamoserver=''
					otheroptions=''
					earname = 'CPSAux.ear'
					name = 'Aux'
					wlserver=''
					wlapp = 'CPSAux'
				}
				stage {
					standalone='standalone'
					layer='EndecaPreview'
					dynamoserver=''
					otheroptions=''
					earname = 'CPSStage.ear'
					name = 'Stage'
					wlserver=''
					wlapp = 'CPSStage'
				}				
		}
	}

}

localqa {

		weblogic {
		    wlhome = '/opt/oracle/product/Middleware/wlserver'
		    wlinstalldir='/opt/oracle/product/Middleware/'
		    storefront {
		    }
		}


		deploy {
		    servershortcutsdir='/home/ansibleuser/Desktop'
		    domainsdir = '/opt/oracle/product/Middleware/user_projects/domains'
		    assembledir = '/opt/oracle/product/products/weblogicApps'
		    scriptsdir = '/opt/scripts/wls'
	
			assemble {
				app {
					layer=''
					dynamoserver=''
					otheroptions=''
					earname = 'CPSProd.ear'
					name = 'App'
					wlserver = ''
					wlapp = 'CPSProd'
				}
				ca {
					layer='staging'
					dynamoserver=''
					otheroptions=''
					earname = 'CPSPub.ear'
					name = 'Ca'
					wlserver = ''
					wlapp = 'CPSPub'
				}
				sso {
					layer=''
					dynamoserver=''
					otheroptions=''
					earname = 'CPSSso.ear'
					name = 'Sso'
					wlserver = ''
					wlapp = 'CPSSso'
				}
				aux {
					layer=''
					dynamoserver=''
					otheroptions=''
					earname = 'CPSAux.ear'
					name = 'Aux'
					wlserver=''
					wlapp = 'CPSAux'
				}
				stage {
					layer='EndecaPreview'
					dynamoserver=''
					otheroptions=''
					earname = 'CPSStage.ear'
					name = 'Stage'
					wlserver=''
					wlapp = 'CPSStage'
				}
				
		}
	}

}

stg {

apache { 
  		wwwroot = '/apps/staticAssets/assets' 
  	}

	deploy {
	    port = 22
	    keyfile = ''
	    remotehost = ''
	    remoteuser = ''
	    destdir = ''
	    servershortcutsdir='/home/oracle/Desktop'
	    domainsdir = '/apps/Oracle/Middleware/Oracle_Home/user_projects/domains'
	    assembledir = '/apps/products/weblogicApps'
	    scriptsdir = '/opt/scripts/wls'
	
		assemble {
			app {
				modules = 'DafEar.Admin DCS.PublishingAgent DCS.AbandonedOrderServices DPS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index VSG.VSGOrderExport Columbia.CPS Columbia.CPSDataLoad '
				standalone='standalone'
				layer=''
				dynamoserver='STGpageServer1'
				otheroptions=''
				earname = 'CPSProd.ear'
				name = 'App'
				wlserver = 'STGpageServer1'
				wlapp = 'CPSProd'
			}
			ca {
				standalone='standalone'
				layer='staging'
				dynamoserver='STGbcc'
				otheroptions=''
				earname = 'CPSPub.ear'
				name = 'Ca'
				wlserver = 'STGbcc'
				wlapp = 'CPSPub'
			}
			sso {
				standalone='standalone'
				layer=''
				dynamoserver='STGsso'
				otheroptions=''
				earname = 'CPSSso.ear'
				name = 'Sso'
				wlserver = 'STGsso'
				wlapp = 'CPSSso'
			}
			aux {
				standalone='standalone'
				layer=''
				dynamoserver='STGauxServer1'
				otheroptions=''
				earname = 'CPSAux.ear'
				name = 'Aux'
				wlserver='STGauxServer1'
				wlapp = 'CPSAux'
			}
			stage {
				standalone='standalone'
				layer='EndecaPreview'
				dynamoserver='STGstagingPreview'
				otheroptions=''
				earname = 'CPSStage.ear'
				name = 'Stage'
				wlserver='STGstagingPreview'
				wlapp = 'CPSStage'
			}
		}
	}
 }

prod {

apache { 
  		wwwroot = '/apps/staticAssets/assets' 
  	}

	deploy {
	    port = 22
	    keyfile = ''
	    remotehost = ''
	    remoteuser = ''
	    destdir = ''
	    servershortcutsdir='/home/oracle/Desktop'
	    domainsdir = '/apps/Oracle/Middleware/Oracle_Home/user_projects/domains'
	    assembledir = '/apps/products/weblogicApps'
	    scriptsdir = '/opt/scripts/wls'
	
		assemble {
			app {
				modules = 'DafEar.Admin DCS.PublishingAgent DCS.AbandonedOrderServices DCS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index Columbia.CPS Columbia.CPSDataLoad '
				standalone='standalone'
				layer=''
				dynamoserver='pageServer1'
				otheroptions=''
				earname = 'CPSProd.ear'
				name = 'App'
				wlserver = 'pageServer1'
				wlapp = 'CPSProd'
			}
			ca {
				standalone='standalone'
				layer='staging'
				dynamoserver='bcc'
				otheroptions=''
				earname = 'CPSPub.ear'
				name = 'Ca'
				wlserver = 'bcc'
				wlapp = 'CPSPub'
			}
			sso {
				standalone='standalone'
				layer=''
				dynamoserver='sso'
				otheroptions=''
				earname = 'CPSSso.ear'
				name = 'Sso'
				wlserver = 'sso'
				wlapp = 'CPSSso'
			}
			aux {
				standalone='standalone'
				layer=''
				dynamoserver='auxServer1'
				otheroptions=''
				earname = 'CPSAux.ear'
				name = 'Aux'
				wlserver='auxServer1'
				wlapp = 'CPSAux'
			}
			stage {
				standalone='standalone'
				layer='EndecaPreview'
				dynamoserver='stagingPreview'
				otheroptions=''
				earname = 'CPSStage.ear'
				name = 'Stage'
				wlserver='stagingPreview'
				wlapp = 'CPSStage'
			}
		}
	}
 }

prodPY {

apache { 
  		wwwroot = '/apps/staticAssets/assets' 
  	}

	deploy {
	    port = 22
	    keyfile = ''
	    remotehost = ''
	    remoteuser = ''
	    destdir = ''
	    servershortcutsdir='/home/oracle/Desktop'
	    domainsdir = '/apps/Oracle/Middleware/Oracle_Home/user_projects/domains'
	    assembledir = '/apps/products/weblogicApps'
	    scriptsdir = '/opt/scripts/wls'
	
		assemble {
			app {
				modules = 'DafEar.Admin DCS.PublishingAgent DCS.AbandonedOrderServices DCS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index Columbia.CPS Columbia.CPSDataLoad '
				standalone='standalone'
				layer=''
				dynamoserver='pageServer1'
				otheroptions=''
				earname = 'CPSProd.ear'
				name = 'App'
				wlserver = 'pageServer1'
				wlapp = 'CPSProd'
			}
			ca {
				standalone='standalone'
				layer='staging'
				dynamoserver='bcc'
				otheroptions=''
				earname = 'CPSPub.ear'
				name = 'Ca'
				wlserver = 'bcc'
				wlapp = 'CPSPub'
			}
			sso {
				standalone='standalone'
				layer=''
				dynamoserver='sso'
				otheroptions=''
				earname = 'CPSSso.ear'
				name = 'Sso'
				wlserver = 'sso'
				wlapp = 'CPSSso'
			}
			aux {
				standalone='standalone'
				layer=''
				dynamoserver='auxServer1'
				otheroptions=''
				earname = 'CPSAux.ear'
				name = 'Aux'
				wlserver='auxServer1'
				wlapp = 'CPSAux'
			}
			stage {
				standalone='standalone'
				layer='EndecaPreview'
				dynamoserver='stagingPreview'
				otheroptions=''
				earname = 'CPSStage.ear'
				name = 'Stage'
				wlserver='stagingPreview'
				wlapp = 'CPSStage'
			}
		}
	}
 }
 
 prodPD {

apache { 
  		wwwroot = '/apps/staticAssets/assets' 
  	}

	deploy {
	    port = 22
	    keyfile = ''
	    remotehost = ''
	    remoteuser = ''
	    destdir = ''
	    servershortcutsdir='/home/oracle/Desktop'
	    domainsdir = '/apps/Oracle/Middleware/Oracle_Home/user_projects/domains'
	    assembledir = '/apps/products/weblogicApps'
	    scriptsdir = '/opt/scripts/wls'
	
		assemble {
			app {
				modules = 'DafEar.Admin DCS.PublishingAgent DCS.AbandonedOrderServices DCS.WebServices DCS.Endeca.Assembler REST DCS.Endeca.Index DCS.Endeca.Index.SKUIndexing ContentMgmt.Endeca.Index Columbia.CPS Columbia.CPSDataLoad '
				standalone='standalone'
				layer=''
				dynamoserver='pageServer1'
				otheroptions=''
				earname = 'CPSProd.ear'
				name = 'App'
				wlserver = 'pageServer1'
				wlapp = 'CPSProd'
			}
			ca {
				standalone='standalone'
				layer='staging'
				dynamoserver='bcc'
				otheroptions=''
				earname = 'CPSPub.ear'
				name = 'Ca'
				wlserver = 'bcc'
				wlapp = 'CPSPub'
			}
			sso {
				standalone='standalone'
				layer=''
				dynamoserver='sso'
				otheroptions=''
				earname = 'CPSSso.ear'
				name = 'Sso'
				wlserver = 'sso'
				wlapp = 'CPSSso'
			}
			aux {
				standalone='standalone'
				layer=''
				dynamoserver='auxServer1'
				otheroptions=''
				earname = 'CPSAux.ear'
				name = 'Aux'
				wlserver='auxServer1'
				wlapp = 'CPSAux'
			}
			stage {
				standalone='standalone'
				layer='EndecaPreview'
				dynamoserver='stagingPreview'
				otheroptions=''
				earname = 'CPSStage.ear'
				name = 'Stage'
				wlserver='stagingPreview'
				wlapp = 'CPSStage'
			}
		}
	}
 }
 
    vm {
    	deploy {
    		assemble {
    			app {
    				server='cps_prod_local'
    			}
    			ca {
    				server='cps_pub_local'
    			}
			    sso {
			 	   server='cps_sso_local'
			    }
			    
			    aux {
			    	server='cps_aux_local'
			    }
			    stage {
			    	server='cps_stage_local'
			    }
		    }
    	}
    }
}
