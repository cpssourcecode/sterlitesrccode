create table cps_user_mailing
(
	user_id   varchar2(40) not null references dps_user (id),
	cps_mailings	varchar2(40) null,
	constraint cps_user_mailing_p primary key (user_id, cps_mailings)
);