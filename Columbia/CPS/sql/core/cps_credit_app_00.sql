--drop table CPS_CREDIT_APP;
create table CPS_CREDIT_APP (
  id  varchar2(40) NOT NULL,
  user_id  varchar2(40)  NOT NULL,
  user_name  varchar2(40) NULL,
  business_name  varchar2(40) NULL,
  trade_name  varchar2(40) NULL,
  org_type  varchar2(40) NULL,
  business_phone  varchar2(40) NULL,
  business_phone_ext  varchar2(40) NULL,
  business_fax  varchar2(40) NULL,
  business_type  varchar2(40) NULL,
  contractors_license_num  varchar2(40) NULL,
  year_established  varchar2(40) NULL,
  customer_type  varchar2(40) NULL,
  po_required  number(1,0) NULL,
  tax_exempt  number(1,0) NULL,
  exp_ann_purch_amt  varchar2(40) NULL,
  CONSTRAINT cps_credit_app_pk PRIMARY KEY(id)
);

--drop table CPS_CREDIT_APP_PRODUCT_LIST;
create table CPS_CREDIT_APP_PRODUCT_LIST (
  id varchar2(40)  not null,
  PRODUCT_LIST varchar2(40)  null,
  constraint cps_credit_app_prod_list_p primary key (id, PRODUCT_LIST),
  constraint cps_credit_app_prod_list_f foreign key (id) references CPS_CREDIT_APP (id)
);
--drop table CPS_CREDIT_APP_CONTACTS;
create table CPS_CREDIT_APP_CONTACTS (
	id	varchar2(40)	not null,
	tag	varchar2(42)	not null,
	contact_id	varchar2(40)	not null,
  constraint CPS_CREDIT_APP_CONTACTS_p primary key (id,tag) ,
  constraint CPS_CREDIT_APP_CONTACTS_f foreign key (id) references CPS_CREDIT_APP (id)
);

--drop table CPS_CREDIT_APP_CONTACT_INFO;
create table CPS_CREDIT_APP_CONTACT_INFO (
  id  varchar2(40)  not null,
  display_name  varchar2(40)  null,
  title varchar2(40)  null,
  address_1 varchar2(40)  null,
  address_2 varchar2(40)  null,
  city  varchar2(40)  null,
  state varchar2(40)  null,
  zip varchar2(40)  null,
  phone_number  varchar2(40)  null,
  phone_number_ext  varchar2(40)  null,
  fax varchar2(40)  null,
  account_number varchar2(40)  null,
  account_officer varchar2(40)  null,
  constraint CPS_CREDIT_APP_CONTACT_INFO_P primary key (id)
);

--drop table CPS_CREDIT_APP_PLACE_ORDERS;
create table CPS_CREDIT_APP_PLACE_ORDERS (
  id varchar2(40) not null,
  orders_id varchar2(40) null,
  constraint cps_credit_app_place_ordr_p primary key (id, orders_id),
  constraint cps_credit_app_place_ordr_f foreign key (id) references CPS_CREDIT_APP (id),
  constraint cps_credit_app_prod_listordr_f foreign key (orders_id) references CPS_CREDIT_APP_CONTACT_INFO (id)
);
