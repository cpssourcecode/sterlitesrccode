drop TABLE cps_zip_to_store_distance;

CREATE TABLE cps_zip_to_store_distance (
id	                   		varchar2(40)	    NOT NULL,
store_id            	varchar2(40)	    NOT NULL,
postal_code		varchar2(10)	    NOT NULL,
distance					NUMBER(19,7)		NULL,
CONSTRAINT cps_zip_to_store_dist_pk PRIMARY KEY(store_id, postal_code));
