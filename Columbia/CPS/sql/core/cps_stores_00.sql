drop table cps_stores;

create table cps_stores (
  id                    VARCHAR2(40)  NOT NULL,
  area                  VARCHAR2(100) NOT NULL,
  address               VARCHAR2(100) NOT NULL,
  city                  VARCHAR2(100) NOT NULL,
  state                 VARCHAR2(2)   NOT NULL,
  zip                   VARCHAR2(5)   NOT NULL,
  phone                 VARCHAR2(12)  NOT NULL,
  open_time             TIMESTAMP(9)  NOT NULL,
  close_time            TIMESTAMP(9)  NOT NULL,
  timezone              VARCHAR2(3)   NOT NULL,
CONSTRAINT cps_stores_pk PRIMARY KEY(id));

--Chicago Metro	1120 W. Pershing Road	Chicago	IL	60609	1-800-572-1904	7:00am - 4:30pm
insert into cps_stores values (1,'Chicago Metro','1120 W. Pershing Road','Chicago','IL','60609','1-800-572-1904',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Northern Indiana	850 165th Street	Hammond	IN	46325	219-852-4444	7:00am - 4:30pm	CST
insert into cps_stores values (2,'Northern Indiana','850 165th Street','Hammond','IN','46325','219-852-4444',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Chicago Metro	1803 Moen Avenue	Rockdale	IL	60436	773-843-5720	7:00am - 4:30pm	CST
insert into cps_stores values (3,'Chicago Metro','1803 Moen Avenue','Rockdale','IL','60436','773-843-5720',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Chicago Metro	60 Ann Street	Elgin	IL	60120	847-608-6060	7:00am - 4:30pm	CST
insert into cps_stores values (4,'Chicago Metro','60 Ann Street','Elgin','IL','60120','847-608-6060',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Chicago Metro	3900 Grove Avenue	Gurnee	IL	60031	847-625-5444	7:00am - 4:30pm	CST
insert into cps_stores values (5,'Chicago Metro','3900 Grove Avenue	Gurnee','Gurnee','IL','60031','847-625-5444',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Wisconsin	5107 Westfair Ave	Wausau	WI	54476	715-241-8086	7:00am - 4:30pm	CST
insert into cps_stores values (6,'Wisconsin','5107 Westfair Ave','Wausau','WI','54476','715-241-8086',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Michigan	2400-A Turner Avenue NW	Grand Rapids	MI	49544	616-364-4275	7:00am - 4:30pm	EST
insert into cps_stores values (7,'Michigan','2400-A Turner Avenue NW','Grand Rapids','MI','49544','616-364-4275',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'EST');
--Michigan	3225 E. Kilgore Rd	Kalamazoo	MI	49001	269-342-5880	7:00am - 4:30pm	EST
insert into cps_stores values (8,'Michigan','3225 E. Kilgore Rd','Kalamazoo','MI','49001','269-342-5880',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'EST');
--Michigan	102 Fast Ice Drive	Midland	MI	48642	989-496-9260	7:00am - 4:30pm	EST
insert into cps_stores values (9,'Michigan','102 Fast Ice Drive','Midland','MI','48642','989-496-9260',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'EST');
--Central Illinois	125 Thunderbird Lane	Peoria	IL	61611	309-694-9290	7:00am - 4:30pm	CST
insert into cps_stores values (10,'Central Illinois','125 Thunderbird Lane','Peoria','IL','61611','309-694-9290',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Central Illinois	2881 Parkway Drive	Decatur	IL	62526	217-428-0871	7:00am - 4:30pm	CST
insert into cps_stores values (11,'Central Illinois','2881 Parkway Drive','Decatur','IL','62526','217-428-0871',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--NW Illinois	5730 Columbia Pkwy	Rockford	IL	61108	815-229-3300	7:00am - 4:30pm	CST
insert into cps_stores values (12,'NW Illinois','5730 Columbia Pkwy','Rockford','IL','61108','815-229-3300',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Wisconsin	5624 Technology Circle	Appleton	WI	54914	920-734-9370	7:00am - 4:30pm	CST
insert into cps_stores values (13,'Wisconsin','5624 Technology Circle','Appleton','WI','54914','920-734-9370',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Wisconsin	2100 S. 54th Street	West Allis	WI	53219	414-672-7687	7:00am - 4:30pm	CST
insert into cps_stores values (14,'Wisconsin','2100 S. 54th Street','West Allis','WI','53219','414-672-7687',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Michigan	1108 56th Avenue	Menominee	MI	49858	906-864-3960	7:00am - 4:30pm	CST
insert into cps_stores values (15,'Michigan','1108 56th Avenue','Menominee','MI','49858','906-864-3960',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--NW Illinois	1000 Courtaulds Drive	Woodstock	IL	60098	815-334-0575	7:00am - 4:30pm	CST
insert into cps_stores values (16,'NW Illinois','1000 Courtaulds Drive','Woodstock','IL','60098','815-334-0575',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--NW Illinois	900 Industrial Avenue	Rock Falls	IL	61071	815-625-6692	7:00am - 4:30pm	CST
insert into cps_stores values (17,'NW Illinois','900 Industrial Avenue','Rock Falls','IL','61071','815-625-6692',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Minnesota	920 Apollo Road	Eagan	MN	55121	651-454-3880	7:00am - 4:30pm	CST
insert into cps_stores values (18,'Minnesota','920 Apollo Road','Eagan','MN','55121','651-454-3880',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Wisconsin	16150 W. Lincoln Ave Unit C	New Berlin	WI	53151	262-786-7180	7:00am - 4:30pm	CST
insert into cps_stores values (19,'Wisconsin','16150 W. Lincoln Ave Unit C','New Berlin','WI','53151','262-786-7180',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'CST');
--Michigan	41554 Koppernick	Canton	MI	48187	734-414-9170	7:00am - 4:30pm	EST
insert into cps_stores values (20,'Michigan','41554 Koppernick','Canton','MI','48187','734-414-9170',TO_TIMESTAMP('07:00:00 am', 'HH12:MI:SS AM'),TO_TIMESTAMP('04:30:00 pm', 'HH12:MI:SS AM'),'EST');

select * from cps_stores;