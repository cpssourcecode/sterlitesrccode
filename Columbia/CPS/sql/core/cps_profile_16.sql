create table cps_user_orgs (
  user_id	varchar2(40)	not null,
  org_id	varchar2(40)	not null,
  constraint cps_user_orgs_p primary key (user_id, org_id),
  constraint cps_user_orgs_user_f foreign key (user_id) references dps_user(id),
  constraint cps_user_orgs_org_f foreign key (org_id) references dps_organization(org_id)
);