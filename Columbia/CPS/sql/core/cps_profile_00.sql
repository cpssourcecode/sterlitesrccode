alter table dps_user modify login varchar2(256);
	
drop table cps_b2b_user;

CREATE TABLE cps_b2b_user (
		user_id 			varchar2(40)	NOT NULL REFERENCES dps_user(id),
		security_question 		varchar2(100)	NULL,
		security_question_answer 	varchar2(100)	NULL,
		approval_status			varchar2(100)	NULL,
		reg_email_sent			number(1,0)	NULL,
		active			        number(1,0)	NULL,
		deActivatedDate		        timestamp	NULL,
		admin_approved                   number(1,0)	NULL,
		credit_appr_reqd	          number(1,0)	NULL,
		cps_vett_complete 	        number(1,0)	NULL,
		cps_vett_complete_email_sent 	number(1,0)	NULL,
		
		CONSTRAINT cps_b2b_user_pk PRIMARY KEY(user_id)
);
	
	
drop table cps_b2b_user_mapped_cs;
	
create table cps_b2b_user_mapped_cs (
		user_id	varchar2(40)	not null,
		address_id	varchar2(40)	not null
	,constraint cps_b2b_user_mapped_cs_p primary key (user_id,address_id)
	,constraint cps_b2b_user_mapped_cs_f foreign key (user_id) references dps_user (id)
	,constraint cps_b2b_user_map_cscontact_f foreign key (address_id) references dps_contact_info (id));


drop table cps_contact_info;


CREATE TABLE cps_contact_info (
	id                     		varchar2(40) 	NOT NULL REFERENCES dps_contact_info(id),
	jde_address_number 		varchar2(40) null,
	is_active		        number(1,0)	NULL,
        email_address                   varchar2(255) null,
        phone_number_ext                varchar2(10) null,
        alternate_phone_number                 varchar2(30) null,
        alternate_phone_ext             varchar2(10) null,
	tax_code				varchar2(40) null,
	is_tax_exempt		        number(1,0)	NULL,
	
	CONSTRAINT cps_contact_info_pk PRIMARY KEY(id)
);


drop table cps_organization;

CREATE TABLE cps_organization (
	org_id 				varchar2(40)	NOT NULL REFERENCES dps_organization(org_id),
	is_active	 		number(1,0) null,
	org_type			number(3) null,
	jde_account_number		varchar2(40) null,
	CONSTRAINT cps_organization PRIMARY KEY(org_id)
);










