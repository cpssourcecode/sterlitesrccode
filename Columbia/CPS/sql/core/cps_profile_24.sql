/*adding new column login1*/
ALTER TABLE dps_user ADD login1 VARCHAR2(255);

/*copying values from login column to login1*/
UPDATE dps_user SET login1 = login;

/*drop column login*/
ALTER TABLE dps_user DROP COLUMN login;

/*Creating login with 255 characters*/
ALTER TABLE dps_user ADD login VARCHAR2(255);

/*copying values from login1 to login*/
UPDATE dps_user SET login = login1;

/*drop column login1*/
ALTER TABLE dps_user DROP COLUMN login1;
