create table cps_dcs_giftitem (
  id  varchar2(40)  not null references dcs_giftitem(id),
  e_commerce_unavailable number(1,0) null,
  constraint  cps_giftitem_p primary key(id)
);