
drop table cps_contract;

CREATE TABLE cps_contract (
	contract_id			varchar2(40) 	NOT NULL REFERENCES dbc_contract(CONTRACT_ID),
	is_tax_exempt			number(1,0) null,
	tax_code			varchar2(40) null
,constraint cps_contract_p primary key (contract_id));
