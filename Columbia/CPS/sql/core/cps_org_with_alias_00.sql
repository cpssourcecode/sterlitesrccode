CREATE TABLE cps_org_alias_item (
        id                      VARCHAR2(40)     not null,
        primary key(id)
);

CREATE TABLE cps_org_with_aliases (
        id                      VARCHAR2(40)     not null references cps_org_alias_item(id),
        organization            VARCHAR2(255)    not null,
        primary key(id, organization)
);