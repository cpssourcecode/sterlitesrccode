CREATE TABLE CPS_USER_VIEWED_PRODUCTS(
    USER_ID	varchar2(40) NOT NULL,
	  PRODUCT_ID	varchar2(40) NOT NULL,
	  SEQ_NUM INTEGER NOT NULL,
	  PRIMARY KEY (SEQ_NUM, USER_ID, PRODUCT_ID)
);