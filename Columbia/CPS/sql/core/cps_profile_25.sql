/*adding new column login1*/
ALTER TABLE vsg_dps_user_log ADD login1 VARCHAR2(255);

/*copying values from login column to login1*/
UPDATE vsg_dps_user_log SET login1 = login;

/*drop column login*/
ALTER TABLE vsg_dps_user_log DROP COLUMN login;

/*Creating login with 255 characters*/
ALTER TABLE vsg_dps_user_log ADD login VARCHAR2(255);

/*copying values from login1 to login*/
UPDATE vsg_dps_user_log SET login = login1;

/*drop column login1*/
ALTER TABLE vsg_dps_user_log DROP COLUMN login1;
