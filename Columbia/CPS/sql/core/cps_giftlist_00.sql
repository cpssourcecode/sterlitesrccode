create table cps_dcs_giftlist (
  id  varchar2(40)  not null references dcs_giftlist(id),
  cs  varchar2(40)  null,
  constraint  cps_giftlist_p primary key(id)
);