CREATE TABLE CPS_PRODUCT_ATTRIBUTES_UOM (
  PRODUCT_ID      VARCHAR2(40)  NOT NULL,
  ATTRIBUTE_NAME  VARCHAR2(800)  NOT NULL,
  ATTRIBUTE_UOM VARCHAR2(4000) NOT NULL,
  CONSTRAINT CPS_PRODUCT_ATTRIBUTES_UOM_PK PRIMARY KEY (PRODUCT_ID, ATTRIBUTE_NAME)
);

--CATA_PRV
GRANT ALL ON ATGDB_CATA_PRV.CPS_PRODUCT_ATTRIBUTES_UOM TO ATGDB_PUB;
--CATB_PRV
GRANT ALL ON ATGDB_CATB_PRV.CPS_PRODUCT_ATTRIBUTES_UOM TO ATGDB_PUB;
--CATA
GRANT ALL ON ATGDB_CATA.CPS_PRODUCT_ATTRIBUTES_UOM TO ATGDB_PUB;
--CATB
GRANT ALL ON ATGDB_CATB.CPS_PRODUCT_ATTRIBUTES_UOM TO ATGDB_PUB;
