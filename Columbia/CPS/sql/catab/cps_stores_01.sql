drop table store_images;

create table store_images (
	id	varchar2(40)	not null,
	media_id	varchar2(40)	not null
,constraint store_images_p primary key (id,media_id)
,constraint store_images_d_f foreign key (id) references cps_stores (id)
,constraint store_imagesxmdmed_d_f foreign key (media_id) references dcs_media (media_id));