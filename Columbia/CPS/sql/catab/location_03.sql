DROP TABLE cps_store;

CREATE TABLE cps_store (
 location_id VARCHAR2(40)  NOT NULL,
 region      VARCHAR2(100) NULL,
 description CLOB          NULL,
 branch_id varchar2(100) null,
 manager_name varchar2(100) null,
 will_call_hours varchar2(100) null,
 CONSTRAINT cps_store_p PRIMARY KEY (location_id)
);