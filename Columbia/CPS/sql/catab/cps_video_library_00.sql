drop table cps_video;
drop table cps_video_category;

CREATE TABLE cps_video_category (
id				varchar2(40)	NOT NULL,
name			varchar2(254)	NOT NULL,
CONSTRAINT cps_video_category_pk PRIMARY KEY(ID));

CREATE TABLE cps_video (
video_id		varchar2(40)	not null,
category_id		varchar2(40)		null references cps_video_category(id),
video_url		varchar2(254)	NOT NULL,
title			varchar2(254)		NULL,
time			varchar2(254)		NULL,
description		varchar2(254)		NULL,
CONSTRAINT cps_video_pk PRIMARY KEY(video_id));