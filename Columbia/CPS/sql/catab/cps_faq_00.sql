DROP TABLE cps_faq_category;
DROP TABLE cps_faq_question;

CREATE TABLE cps_faq_category (
	id 					VARCHAR2(40) NOT NULL,
	category_name		VARCHAR2(40) NOT NULL,
	display_order		NUMERIC(3) NULL,
	CONSTRAINT cps_faq_category_pk PRIMARY KEY (id)
);

CREATE TABLE cps_faq_question (
	id 					VARCHAR2(40) NOT NULL,
	question 			VARCHAR2(255) NOT NULL,
	answer				VARCHAR2(255) NOT NULL,
	category 			VARCHAR(40) NOT NULL,
	display_order		NUMERIC(3) NULL,
	CONSTRAINT cps_faq_question_pk PRIMARY KEY (id)
);