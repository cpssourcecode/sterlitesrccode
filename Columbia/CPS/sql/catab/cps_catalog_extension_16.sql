ALTER TABLE CPS_PRODUCT_ALL DROP (
	CATEGORY_CODE10,
	CATEGORY_CODE9,
	I2_CAT_NUM,
	LONG_DESC2,
	MANUFACTURER_NAME,
	MANUFACTURER_PART_NUMBER,
	MFR_DESCRIPTION,
	MFR_REPLACED_BY_CAT_NO,
	MFR_REPLACED_BY_UPC,
	MFR_REPLACES_CAT_NO,
	MFR_REPLACES_UPC,
	MFR_SHORTNAME,
	MFR_UNSPSC,
	MSDS_EFF_DATE,
	MSDS_FORM_NUM,
	PERCENT_MFGD_IN_USA,
	PRICESVC_I2_CAT_NUM,
	PRICESVC_ITEM_PIK,
	PRICESVC_MFR_UCC_NUM,
	PRINT_MESSAGE,
	PRODUCT_CATEGORY,
	PRODUCT_TYPE,
	SALES_CATEGORY_CODE4,
	SALES_CATEGORY_CODE5,
	CATEGORY_CODE7,
	SALES_RPT_PRODUCT_DISCOUNT_GRP,
	SALES_RPT_PRODUCT_LINE,
	THUMBNAIL_FILENAME,
	TWO_K_DESC,
	UNILOG_BRAND_NAME,
	UNILOG_UNSPSC,
	UNILOG_UPC,
	UOM_VOLUME_OR_WEIGHT,
	UOM_WEIGHT,
	WEB_FILENAME
);

ALTER TABLE CPS_PRODUCT_ALL ADD(
	HEIGHT                       NUMBER(19, 7)          NULL,
	HEIGHT_UOM                   VARCHAR2(2)            NULL,
	INVOICE_DESCRIPTION          VARCHAR2(255)          NULL,
	ITEM_DOCUMENT_TYPE_1         VARCHAR2(40)           NULL,
	ITEM_DOCUMENT_TYPE_2         VARCHAR2(40)           NULL,
	ITEM_DOCUMENT_TYPE_3         VARCHAR2(40)           NULL,
	ITEM_DOCUMENT_TYPE_4         VARCHAR2(40)           NULL,
	ITEM_DOCUMENT_TYPE_5         VARCHAR2(40)           NULL,
	ITEM_IMAGE_DETAILIMAGE2      VARCHAR2(800)          NULL,
	ITEM_IMAGE_DETAILIMAGE3      VARCHAR2(800)          NULL,
	ITEM_IMAGE_DETAILIMAGE4      VARCHAR2(800)          NULL,
	ITEM_IMAGE_DETAILIMAGE5      VARCHAR2(800)          NULL,
	ITEM_IMAGE_ENLARGEIMAGE2     VARCHAR2(800)          NULL,
	ITEM_IMAGE_ENLARGEIMAGE3     VARCHAR2(800)          NULL,
	ITEM_IMAGE_ENLARGEIMAGE4     VARCHAR2(800)          NULL,
	ITEM_IMAGE_ENLARGEIMAGE5     VARCHAR2(800)          NULL,
	ITEM_IMAGE_ITEMIMAGE2        VARCHAR2(800)          NULL,
	ITEM_IMAGE_ITEMIMAGE3        VARCHAR2(800)          NULL,
	ITEM_IMAGE_ITEMIMAGE4        VARCHAR2(800)          NULL,
	ITEM_IMAGE_ITEMIMAGE5        VARCHAR2(800)          NULL,
	KEYWORDS                     VARCHAR2(1000)         NULL,
	LEAD_FREE                    NUMBER(1, 0) DEFAULT 1 NULL,
	LENGTH                       NUMBER(19, 7)          NULL,
	LENGTH_UOM                   VARCHAR2(2)            NULL,
	MINIMUM_ORDER_QTY            INTEGER                NULL,
	ORDER_QTY_INTERVAL           INTEGER                NULL,
	VOLUME                       NUMBER(19, 7)          NULL,
	WEB_CATEGORY_CODE            VARCHAR2(255)          NULL,
	WIDTH                        NUMBER(19, 7)          NULL,
	WIDTH_UOM                    VARCHAR2(2)            NULL
);

CREATE TABLE CPS_PRODUCT_FEATURES(
	PRODUCT_ID                   VARCHAR2(40)       NOT NULL,
	FEATURE                      VARCHAR2(1000)              NULL,
	SEQUENCE_NUM                 INTEGER                 NOT NULL,
	CONSTRAINT CPS_PRODUCT_FEATURES_PK PRIMARY KEY (PRODUCT_ID, SEQUENCE_NUM)
);

CREATE TABLE CPS_PRODUCT_STANDARDS(
	PRODUCT_ID                   VARCHAR2(40)       NOT NULL,
	STANDARD                     VARCHAR2(1000)              NULL,
	SEQUENCE_NUM                 INTEGER                 NOT NULL,
	CONSTRAINT CPS_PRODUCT_STANDARDS_PK PRIMARY KEY (PRODUCT_ID, SEQUENCE_NUM)
);