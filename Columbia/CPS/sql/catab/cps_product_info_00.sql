DROP TABLE cps_prod_info_category;
DROP TABLE cps_prod_info_item;
DROP TABLE cps_prod_info_item_cat_map;

CREATE TABLE cps_prod_info_category (
	id				VARCHAR(40) NOT NULL,
	category_name	VARCHAR(100) NOT NULL,
	display_order	NUMERIC(3) NULL,
	CONSTRAINT cps_prod_info_category_pk PRIMARY KEY (id)
);

CREATE TABLE cps_prod_info_item (
	id				VARCHAR(40) NOT NULL,
	item_name			VARCHAR2(255) NOT NULL,
	item_url		VARCHAR(512) NOT NULL,
	CONSTRAINT cps_prod_info_item_pk PRIMARY KEY (id)
);

CREATE TABLE cps_prod_info_item_cat_map (
	item_id		VARCHAR(40) NOT NULL,
	category_id	VARCHAR(40) NOT NULL,
	CONSTRAINT cps_prod_info_item_cat_map_pk PRIMARY KEY (item_id, category_id)
);