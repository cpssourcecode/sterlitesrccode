-- drop table CPS_MENU
-- drop table CPS_MENU_CAT
-- drop table CPS_MENU_BRAND
-- drop table CPS_MENU_ITEM
-- drop table CPS_MENU_SUBFOLDER
-- drop table CPS_MENU_FOLDER
-- drop table CPS_CATALOG_MENU

CREATE TABLE CPS_MENU (
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    MENU_TYPE                      NUMBER(19, 0)          NOT NULL,
    MENU_NAME                      VARCHAR2(254)          NULL,
    DESCRIPTION                    VARCHAR2(1024)         NULL,
    MENU_LINK                      VARCHAR2(1024)         NULL,
    MENU_IMAGE                     VARCHAR2(1024)         NULL,
    MENU_ICON                      VARCHAR2(1024)         NULL,
    ITEM_ACL                       VARCHAR2(1024)         NULL,
    CONSTRAINT CPS_MENU_PK PRIMARY KEY (MENU_ID)
);

CREATE TABLE CPS_MENU_CAT (
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    CATEGORY_ID                    VARCHAR2(40)           NULL,
    CONSTRAINT CPS_MENU_CAT_PK PRIMARY KEY (MENU_ID)
);

CREATE TABLE CPS_MENU_BRAND (
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    BRAND_ID                       VARCHAR2(40)           NULL,
    CONSTRAINT CPS_MENU_BRAND_PK PRIMARY KEY (MENU_ID)
);

CREATE TABLE CPS_MENU_FOLDER (
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    VERSION                        NUMBER(19, 0)          NOT NULL,
    FOLDER_NAME                    VARCHAR2(100)          NOT NULL,
    FOLDER_PATH                    VARCHAR2(1024)         NOT NULL,
    PARENT_FOLDER_ID               VARCHAR2(40)           NULL,
    DESCRIPTION                    VARCHAR2(254)          NULL,
    MENU_ID                        VARCHAR2(40)           NULL,
    ITEM_ACL                       VARCHAR2(1024)         NULL,
    CONSTRAINT CPS_MENU_FOLDER_PK PRIMARY KEY (FOLDER_ID)
);

CREATE TABLE CPS_MENU_SUBFOLDER (
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    SEQUENCE_NUM                   NUMBER(19, 0)          NOT NULL,
    SUBFOLDER_ID                   VARCHAR2(40)           NOT NULL,
    CONSTRAINT CPS_MENU_SUBFOLDER_PK PRIMARY KEY (FOLDER_ID, SEQUENCE_NUM)
);

CREATE TABLE CPS_MENU_ITEM (
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    SEQUENCE_NUM                   NUMBER(19, 0)          NOT NULL,
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    CONSTRAINT CPS_MENU_ITEM_PK PRIMARY KEY (FOLDER_ID, SEQUENCE_NUM)
);

CREATE TABLE CPS_CATALOG_MENU (
    CATALOG_ID                     VARCHAR2(40)           NOT NULL,
    SEQUENCE_NUM                   NUMBER(19, 0)          NOT NULL,
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    CONSTRAINT CPS_CATALOG_MENU_PK PRIMARY KEY (CATALOG_ID, SEQUENCE_NUM)
);