CREATE TABLE cps_store (
		location_id 			varchar2(40)	NOT NULL REFERENCES dcs_location_store(location_id),
		region 		varchar2(100)	NULL,
    description 		clob	NULL
);