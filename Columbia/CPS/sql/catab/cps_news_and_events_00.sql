drop table cps_news_and_events;

CREATE TABLE cps_news_and_events (
id				varchar2(40)	NOT NULL,
title			varchar2(254)	NULL,
image_uri		varchar2(254)	NULL,
text_content	CLOB	NULL,
creation_date	TIMESTAMP(6)	NULL,
CONSTRAINT cps_news_and_events_pk PRIMARY KEY(ID));