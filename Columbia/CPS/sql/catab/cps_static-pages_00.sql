create table cps_folder(
	folder_id	varchar2(40) not null,
	version	number	not null,
	creation_date	timestamp(6)	null,
	start_date	timestamp(6)	null,
	end_date	timestamp(6)	null,
	description	varchar2(254)	null,
	name	varchar2(254) not null,
	path	varchar2(254)	null,
	parent_folder_id	varchar2(40)	null,
	constraint cps_folder_pk primary key (folder_id)
);

create table cps_page_template (
	page_id				varchar2(40)	not null,
	display_name		varchar2(254)	null,
	description			varchar2(254)	null,
	page_title			varchar2(254)	null,
	meta_description	varchar2(254)	null,
	meta_keywords		varchar2(254)	null,
	page_url			varchar2(254)	not null,
	html				clob			null,
	start_date			timestamp(6)	null,
	end_date			timestamp(6)	null,
	page_order 			number			null,
	left_navigation		number(1)		null,
	parent_folder_id	varchar2(40)	null,
	path				varchar2(254)	null,
	constraint cps_t_t_pk primary key (page_id)
);

CREATE TABLE CPS_CONTENT (
	CONTENT_ID				VARCHAR2(40)		NOT NULL,
	MEDIA_TYPE				INTEGER				NOT NULL,
	DISPLAY_DESCRIPTION		VARCHAR2(5)			NULL,
	DISPLAY_UPSELLS			VARCHAR2(5)			NULL,
	FEATURE_PROMO_ID		VARCHAR2(40)		NULL,
	PDF_PATH				VARCHAR2(254)		NULL,
	HTML_BLOCK				CLOB				NULL,
	CONSTRAINT CPS_CONTENT_PK PRIMARY KEY (CONTENT_ID),
	CONSTRAINT CPS_CONTENT_FK1 FOREIGN KEY (CONTENT_ID) REFERENCES DCS_PRODUCT (PRODUCT_ID)
);


CREATE TABLE CPS_TAB (
	TAB_ID			VARCHAR2(40)		NOT NULL,
	DISPLAY_NAME	VARCHAR2(254)		NOT NULL,
	TITLE			VARCHAR2(100)		NOT NULL,
	START_DATE		TIMESTAMP			NULL,
	END_DATE		TIMESTAMP			NULL,
	TEMPLATE_ID		VARCHAR2(40)		NOT NULL,
	TAB_CONTENT		CLOB				NULL,
	CONSTRAINT CPS_TAB_PK PRIMARY KEY (TAB_ID)
);

create table cps_page_template_sites (
	page_id					varchar2(40)	not null references cps_page_template (page_id), 
	site_name				varchar2(254)	null,
	constraint cps_p_t_sites_pk primary key (page_id, site_name)
);

create table cps_page_template_pages(
	page_id varchar2(40) not null references cps_page_template (page_id), 
	page varchar2(100) null,
constraint cps_p_t_pk primary key (page_id, page)
);


create table cps_promo_segments(
	promo_id varchar2(40) not null references vsg_promo_item (promo_id), 
	segments varchar2(100) null,
constraint cps_promo_segments_pk primary key (promo_id, segments)
);

create table cps_promo_keywords(
	promo_id varchar2(40) not null references vsg_promo_item (promo_id), 
	keywords varchar2(100) null,
constraint cps_promo_keywords_pk primary key (promo_id, keywords)
);

CREATE TABLE CPS_CAT_CONTENT (
	CATEGORY_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM		INTEGER				NOT NULL,
	CONTENT_ID			VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_CAT_CONTENT_PK PRIMARY KEY (CATEGORY_ID, SEQUENCE_NUM),
	CONSTRAINT CPS_CAT_CONTENT_FK1 FOREIGN KEY (CATEGORY_ID) REFERENCES DCS_CATEGORY (CATEGORY_ID),
	CONSTRAINT CPS_CAT_CONTENT_FK2 FOREIGN KEY (CONTENT_ID) REFERENCES CPS_CONTENT (CONTENT_ID)
);


CREATE TABLE CPS_CAT_TAB (
	CATEGORY_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM		INTEGER				NOT NULL,
	TAB_ID				VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_CAT_TAB_PK PRIMARY KEY (CATEGORY_ID, SEQUENCE_NUM),
	CONSTRAINT CBS_CAT_TAB_FK1 FOREIGN KEY (CATEGORY_ID) REFERENCES DCS_CATEGORY (CATEGORY_ID),
	CONSTRAINT CPS_CAT_TAB_FK2 FOREIGN KEY (TAB_ID) REFERENCES CPS_TAB (TAB_ID)
);




CREATE TABLE CPS_TAB_CAT (
	TAB_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM	INTEGER				NOT NULL,
	CATEGORY_ID		VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_TAB_CAT_PK PRIMARY KEY (TAB_ID, SEQUENCE_NUM),
	CONSTRAINT CPS_TAB_CAT_FK1 FOREIGN KEY (TAB_ID) REFERENCES CPS_TAB (TAB_ID),
	CONSTRAINT CPS_TAB_CAT_FK2 FOREIGN KEY (CATEGORY_ID) REFERENCES DCS_CATEGORY (CATEGORY_ID)
);


CREATE TABLE CPS_TAB_PRD (
	TAB_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM	INTEGER				NOT NULL,
	PRODUCT_ID		VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_TAB_PRD_PK PRIMARY KEY (TAB_ID, SEQUENCE_NUM),
	CONSTRAINT CPS_TAB_PRD_FK1 FOREIGN KEY (TAB_ID) REFERENCES CPS_TAB (TAB_ID),
	CONSTRAINT CPS_TAB_PRD_FK2 FOREIGN KEY (PRODUCT_ID) REFERENCES DCS_PRODUCT (PRODUCT_ID)
);

CREATE TABLE CPS_TAB_CONTENT (
	TAB_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM	INTEGER				NOT NULL,
	CONTENT_ID		VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_TAB_CONTENT_PK PRIMARY KEY (TAB_ID, SEQUENCE_NUM),
	CONSTRAINT CPS_TAB_CONTENT_FK1 FOREIGN KEY (TAB_ID) REFERENCES CPS_TAB (TAB_ID)
);

