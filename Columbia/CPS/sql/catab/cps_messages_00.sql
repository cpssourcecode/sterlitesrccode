CREATE TABLE CPS_USER_MESSAGE(
    ID                              VARCHAR2(100)    NOT NULL, 
    DISPLAY_NAME                    VARCHAR2(254)   NOT NULL UNIQUE,
    MODULE_NAME                     VARCHAR2(254)   NOT NULL, 
    MESSAGE                         CLOB   NOT NULL, 
    MESSAGE_TYPE                    INTEGER,
	CONSTRAINT CPS_USER_MESSAGE_PK PRIMARY KEY(ID,DISPLAY_NAME)
);