drop table cps_blackout_date;

CREATE TABLE cps_blackout_date (
id			varchar2(40)	NOT NULL,
blackout_date		date		NOT NULL,
description		varchar2(80)	NULL,
CONSTRAINT cps_blackout_date_pk PRIMARY KEY(ID));