drop table cps_regional_managers;

CREATE TABLE cps_regional_managers (
id						varchar2(40)	NOT NULL,
image_uri				varchar2(254)	NULL,
name					varchar2(254)	NULL,
title					varchar2(254)	NULL,
localtion				varchar2(254)	NULL,
email					varchar2(254)	NULL,
phone					varchar2(254)	NULL,
description				CLOB 			NULL,
CONSTRAINT cps_regional_managers_pk PRIMARY KEY(ID));