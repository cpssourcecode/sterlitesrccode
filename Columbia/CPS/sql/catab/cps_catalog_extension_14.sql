-- drop table CPS_CAT_FACETS

CREATE TABLE CPS_CAT_FACETS (
	CATEGORY_ID				VARCHAR2(40) 					NOT NULL,
	CAT_FACET 				VARCHAR2(1024 BYTE) 			NULL,
	SEQUENCE_NUM 			INTEGER 						NOT NULL,
	CONSTRAINT CPS_CAT_FACETS_PK PRIMARY KEY (CATEGORY_ID, SEQUENCE_NUM)
);