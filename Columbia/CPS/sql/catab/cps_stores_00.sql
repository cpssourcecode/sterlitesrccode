drop table cps_stores;

create table cps_stores (
  id                    VARCHAR2(40)  NOT NULL,
  name					VARCHAR2(100) NOT NULL,
  area                  VARCHAR2(100) NOT NULL,
  address               VARCHAR2(100) NOT NULL,
  city                  VARCHAR2(100) NOT NULL,
  state                 VARCHAR2(2)   NOT NULL,
  zip                   VARCHAR2(5)   NOT NULL,
  phone                 VARCHAR2(12)  NOT NULL,
  open_time             TIMESTAMP(9)  NOT NULL,
  close_time            TIMESTAMP(9)  NOT NULL,
  timezone              VARCHAR2(3)   NOT NULL,
  description			CLOB	NOT	NULL,
  lat					VARCHAR2(24)	NOT	NULL,
  lng					VARCHAR2(24)	NOT	NULL,
CONSTRAINT cps_stores_pk PRIMARY KEY(id));

create table store_images (
	id	varchar2(40)	not null,
	sequence_num	integer	not null,
	media_id	varchar2(40)	not null
,constraint store_images_p primary key (id,sequence_num)
,constraint store_images_d_f foreign key (id) references cps_stores (id)
,constraint store_imagesxmdmed_d_f foreign key (media_id) references dcs_media (media_id));