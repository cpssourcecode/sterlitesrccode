ALTER TABLE CPS_PRODUCT_ALL ADD(
  substitute_brand_message_id     INTEGER    NULL
  );


--drop table CPS_SUBSTITUTE_BRAND_MESSAGE;

CREATE TABLE CPS_SUBSTITUTE_BRAND_MESSAGE(
  ID                              VARCHAR2(100)    NOT NULL,
  DISPLAY_NAME                    VARCHAR2(254)   NOT NULL UNIQUE,
  MESSAGE                         VARCHAR2 (800)   NOT NULL,
  CONSTRAINT CPS_SUB_BRAND_MESSAGE_PK PRIMARY KEY(ID,DISPLAY_NAME)
);

--GRANT all on atgdb_cata.cps_substitute_brand_message to atgdb_pub;
--GRANT all on atgdb_catb.cps_substitute_brand_message to atgdb_pub;