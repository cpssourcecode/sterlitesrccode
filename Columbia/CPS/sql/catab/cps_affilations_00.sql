drop table cps_affilations;

CREATE TABLE cps_affilations (
id				varchar2(40)	NOT NULL,
image_uri		varchar2(254)	NULL,
description		CLOB	NULL,
link	varchar2(254)	NULL,
CONSTRAINT cps_affilations_pk PRIMARY KEY(ID));