ALTER TABLE cps_faq_question ADD temp_answer CLOB NULL;
UPDATE cps_faq_question SET temp_answer = answer;
ALTER TABLE cps_faq_question DROP COLUMN answer;
ALTER TABLE cps_faq_question ADD answer CLOB NULL;
UPDATE cps_faq_question SET answer = temp_answer;
ALTER TABLE cps_faq_question DROP COLUMN temp_answer;