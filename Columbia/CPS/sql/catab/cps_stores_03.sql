drop table store_images;

create table store_images (
  
	id	varchar2(40)	not null,
	sequence_num	integer	not null,
	media_id	varchar2(40)	not null
,constraint store_images_p primary key (id,sequence_num));