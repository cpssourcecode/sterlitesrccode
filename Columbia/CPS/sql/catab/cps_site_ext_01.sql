create table CPS_SITE_CONFIG (
	SITE_ID					VARCHAR2(40)	NOT NULL,
	SITE_GA_ID				VARCHAR2(40)	NULL,
	SITE_GA_SITENAME		VARCHAR2(40)	NULL,
	constraint CPS_SITE_CONFIG_PK primary key (SITE_ID)
);

alter table CPS_SITE_CONFIG add constraint CPS_SITE_CONFIG_FK foreign key (SITE_ID) references SITE_CONFIGURATION (ID);