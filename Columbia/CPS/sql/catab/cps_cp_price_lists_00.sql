--drop table cps_price_lists;
--drop table cps_price_list_updates;
--drop table cps_price_sections;
--drop table cps_publisher_emails;

CREATE TABLE cps_price_lists (
id				varchar2(40)	NOT NULL,
description		CLOB	NULL,
CONSTRAINT cps_price_lists_pk PRIMARY KEY(ID));

CREATE TABLE cps_price_list_updates (
	id      varchar2(40)  NOT NULL,
	name    varchar2(254)  NOT NULL,
	price_list_update varchar2(254) NOT NULL,
	CONSTRAINT cps_price_list_updates_pk PRIMARY KEY (id, name)
);

CREATE TABLE cps_price_sections (
	id      varchar2(40)  NOT NULL,
	name    varchar2(254)  NOT NULL,
	price_section varchar2(254) NOT NULL,
	CONSTRAINT cps_price_sections_pk PRIMARY KEY (id, name)
);

CREATE TABLE cps_publisher_emails (
	id      varchar2(40)  NOT NULL,
	email    varchar2(254)  NOT NULL,
	CONSTRAINT cps_publisher_emails_pk PRIMARY KEY (id, email)
);
