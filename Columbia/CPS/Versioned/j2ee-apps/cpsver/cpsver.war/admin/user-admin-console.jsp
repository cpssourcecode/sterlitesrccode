<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/prelude.jspf"%>
<dsp:page>
	<dsp:include page="/includes/admin-console-header.jsp" />
		<p>Upload Users From Excel Sheet</p>
	<div>
		<a class="btn" href="/cpsver/import/importUsers.jsp">Import Users</a><br>
		<p>Export Active users Excel</p>
		<a class="btn" href="/cpsver/export/exportActiveUser.jsp">Export Active Users Report</a>
		<p>Resend confirmation Email</p>
		<a class="btn" href="/cpsver/export/sendConformationEmail.jsp">Send	Email</a>
		<p>Remove inactive user More than 60 days</p>
		<a class="btn" href="/cpsver/export/removeInactiveUsers.jsp">Remove Users</a>
		<p>Orders without JDE Order Number</p>
		<a class="btn" href="/cpsver/order/viewOrders.jsp">View Orders</a>
		<p>Repository cache invalidation</p>
		<a class="btn" href="/cpsver/invalidateCache/invalidateCache.jsp">Cache Invalidation</a>
		<p>Sales Order Report</p>
		<a class="btn" href="/cpsver/order/salesOrders.jsp">Sales Order Report</a>
	</div>
</dsp:page>
