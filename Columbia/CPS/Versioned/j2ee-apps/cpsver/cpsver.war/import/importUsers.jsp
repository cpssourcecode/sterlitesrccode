<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<dsp:page>
	<dsp:importbean bean="/cps/util/FileUploadHandler" />
	<dsp:include page="/includes/admin-console-header.jsp" />
		<h2>Bulk Upload Users</h2>
	<dsp:droplet name='/atg/dynamo/droplet/ErrorMessageForEach'>
		<dsp:param name='exceptions' bean='FileUploadHandler.formExceptions' />
		<dsp:oparam name='output'>
			<li><dsp:valueof param='message' />
		</dsp:oparam>
	</dsp:droplet>


	<div class="modal fade in" id="modal-usersin" tabindex="-1" role="dialog" aria-labelledby="modal-usersin-title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<dsp:form enctype="multipart/form-data" method="post" action="<%=request.getRequestURI()%>" id="import-users" formid="import-users">

					<div class="modal-body">
						<h4 class="modal-title" id="modal-usersin-title">Import User(s)</h4>
						<p>To add User(s), upload a spreadsheet in .xls format.</p>
						<div class="form-group">
							<dsp:input bean="FileUploadHandler.userFile" name="userFile" id="userFile" iclass="form-control" type="file" />
						</div>

						<div class="row row-flush">
							<div class="col-sm-6 col-sm-push-6 text-right text-left-xs">
							<p>
								<a href="/cpsver/document/import_users_template_bcc_v1.0.xlsx" class="btn btn-link"><i class="fa fa-download mr5"></i>
									Download Template
								</a>
							</p>
							</div>
							<div class="col-sm-6 col-sm-pull-6">
								<dsp:input type="hidden"
									bean="FileUploadHandler.readUserFileSuccessURL" value="/cpsver/import/listImportedUsers.jsp" />
								<dsp:input type='hidden' bean='FileUploadHandler.bulkUploadFromBcc' value='true' />
								<dsp:input type="submit" bean="FileUploadHandler.readUserFile" value="upload" />


							</div>
						</div>
					</div>
				</dsp:form>
			</div>
		</div>
	</div>
</dsp:page>