<%@page import="com.cps.userprofiling.service.LoginService"%>
<%@page import="atg.repository.*"%>
<%@page import="atg.repository.rql.RqlStatement"%>
<%@page import="atg.servlet.ServletUtil"%>
<%@page import="atg.repository.Repository"%>
<%@ include file="/includes/utils/taglibs.jspf" %>
<%@ include file="/includes/utils/context.jspf" %>
<dsp:page>
	<dsp:importbean bean="/cps/userprofiling/service/LoginService"    var='loginService'/>
	
	<%
String email=request.getParameter("email");
if(email==null) {
out.println("required parameter email is missing");
return;
}
Repository profileRep=(Repository)ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/ProfileAdapterRepository");
RqlStatement stmt=RqlStatement.parseRqlStatement("login=?0");
RepositoryView profileView=profileRep.getView("user");
Object[] params=new Object[1];
params[0]=email;
RepositoryItem[] items=stmt.executeQuery(profileView, params);
if(items!=null) {
    RepositoryItem oneUser=items[0];
    LoginService loginService=(LoginService)ServletUtil.getCurrentRequest().resolveName("/cps/userprofiling/service/LoginService");
    String encryptedParams=loginService.encryptLinkParams(oneUser.getRepositoryId(), email);
    out.println(encryptedParams);
} else {
    out.println("no user exits with email id " + email);
}



%>


	 
</dsp:page>