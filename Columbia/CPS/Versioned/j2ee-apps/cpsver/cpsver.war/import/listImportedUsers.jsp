<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<dsp:page>
	<dsp:include page="/includes/admin-console-header.jsp" />
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler" />
	<dsp:importbean bean="/cps/util/FileDownloadHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<h2>Uploaded Users</h2>
	<p align='right'>
		<a href='/atg/bcc'>Back to BCC Home</a>
	</p>
	<BR>
	<BR>
	<table width='100%' border='2'>
		<dsp:droplet name='ForEach'>
			<dsp:param name='array'
				bean='MultiUserAddFormHandler.bccImportedUsers' />
			<dsp:oparam name='output'>
				<tr>
					<td><dsp:valueof param='key' /></td>
					<td><dsp:valueof param='element' /></td>
				</tr>
			</dsp:oparam>
		</dsp:droplet>
	</table>
	<div id="export-bulk-users">
		<dsp:form id="exportBulkUsers" formid="exportBulkUsers" action="/" method="post">
			<button class="btn btn-primary">Export as Excel</button>
			<dsp:input type="hidden" bean="FileDownloadHandler.createUserSpreadsheetSuccessURL" value="" />
			<dsp:input type="hidden" bean="FileDownloadHandler.createUserSpreadsheetErrorURL" value="" />
			<dsp:input type="hidden" bean="FileDownloadHandler.exportBulkUsers" value="true" />
		</dsp:form>
</div>
</dsp:page>