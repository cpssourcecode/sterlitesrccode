<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<dsp:page>
<dsp:include page="/includes/admin-console-header.jsp" />
<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
<dsp:droplet name='/atg/dynamo/droplet/ErrorMessageForEach'>
		<dsp:param name='exceptions' bean='MultiUserAddFormHandler.formExceptions' />
		<dsp:oparam name='output'>
			<li><dsp:valueof param='message'/></li>
		</dsp:oparam>
</dsp:droplet>
<dsp:getvalueof var="error" param="error" />
<c:if test="${error == 'true' }">
<p>Users who are approvers of another users cannot be deleted. Please remove the approver relation and try to delete again.</p>
</c:if>
<h3>List of preregistered accounts more than 60 days prior and have not had a password set</h3>
<dsp:importbean bean="/cps/droplet/CPSConfirmationPendingUsersDroplet"/>
<dsp:form   enctype="multipart/form-data" method="post">
<dsp:droplet name="CPSConfirmationPendingUsersDroplet">
<dsp:param name="inactiveUsers" value="true"/>
<dsp:oparam name="output">
<dsp:getvalueof var="users" param="usersList" />
<table width='100%' border='3'>
        <thead>
          <tr>
            <th >Select</th>
            <th >Email</th>
            <th >Active Status</th>
          </tr>
       </thead>
	   <tbody>
<dsp:droplet name="/atg/dynamo/droplet/ForEach">
<dsp:param name="array" value="${users}"/>
<dsp:param name="elementName" value="user"/>
<dsp:oparam name="output">

<dsp:getvalueof var="email" param="user.login"/>
<dsp:getvalueof var="id" param="user.id" />
<dsp:getvalueof var="isActive" param="user.isActive"/> 
          <tr>
            <td> <input type="checkbox" name="user"  value="${id}" /></td>
            <td>${email}</td>
            <td>${isActive}</td>
           </tr>
 </dsp:oparam>
 </dsp:droplet>
 </tbody>
 </table>
 <a class="btn" id="removeProfile">Remove Accounts</a>
 </dsp:oparam>
 
<dsp:oparam name="empty">
No users Found for your search.
</dsp:oparam>
</dsp:droplet>
<dsp:input type="hidden" bean="MultiUserAddFormHandler.userId" id="removeUsersList"></dsp:input>
<dsp:input type="hidden" bean="MultiUserAddFormHandler.removeUsers" value="true"></dsp:input>
<button  style="display:none"id="triggerRemoveProfile">send</button>
 </dsp:form>
 <script type="text/javascript">
 $(document).ready(function(){
 var userArray = new Array();
 $("#removeProfile").click(function(e){
     e.preventDefault();
 	 $("input:checkbox[name=user]:checked").each(function(){
        userArray.push($(this).val());
	 });
 	 $("#removeUsersList").val(userArray);
 	 $("#triggerRemoveProfile").click();
	});
});
 </script>

</dsp:page>