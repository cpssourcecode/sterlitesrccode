<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/prelude.jspf"%>
<dsp:page>
<dsp:include page="/includes/admin-console-header.jsp" />
<br>
<dsp:getvalueof var="userEmail" param="email" />
<div>
PLEASE ENTER THE USER EMAIL TO FIND:
<p>
<label>Email:</label>
<input type="text" id="userEmail"/>
<input type="button" id="searchProfile" value="Search"/>
</p>
</div>
<div id="pending_list">
 <dsp:include page="/export/ConfirmationPendingList.jsp?email=${userEmail}"/>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $("#searchProfile").click(function(){
        var email= "/cpsver/export/ConfirmationPendingList.jsp?email="+$("#userEmail").val();
      $("#pending_list").load(email);
        });
});
</script>
</dsp:page>
