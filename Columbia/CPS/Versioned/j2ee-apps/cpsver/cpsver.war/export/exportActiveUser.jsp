<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<dsp:page>
	<dsp:importbean bean="/cps/util/FileDownloadHandler" />

<dsp:include page="/includes/admin-console-header.jsp" />
<br>
	<dsp:droplet name='/atg/dynamo/droplet/ErrorMessageForEach'>
		<dsp:param name='exceptions' bean='FileDownloadHandler.formExceptions' />
		<dsp:oparam name='output'>
			<li><dsp:valueof param='message'/></li>
		</dsp:oparam>
	</dsp:droplet>
	<label>Active users that have reset their password (someone actually using the site).</label>
	<dsp:form id="export-active-users" formid="export-active-users" action="/" method="post">
		<button class="btn btn-primary">Export</button>
		<dsp:input type="hidden"
			bean="FileDownloadHandler.reportQuery" value="activeUsers" />
		<dsp:input type="hidden"
			bean="FileDownloadHandler.sheetName" value="ActiveUsers" />
		<dsp:input type="hidden"
			bean="FileDownloadHandler.reportName" value="ActiveUserReport" />
		
		<dsp:input type="hidden" bean="FileDownloadHandler.exportUserSpreadsheet" value="true" />
	</dsp:form></br></br>
	<label> Active users that have not reset their password (has a preregistration link but has not used it).</label>
	<dsp:form id="export-users" formid="export-users" action="/" method="post">
		<button class="btn btn-primary">Export</button>
		<dsp:input type="hidden"
			bean="FileDownloadHandler.reportQuery" value="pwdNotReset" />
		<dsp:input type="hidden"
			bean="FileDownloadHandler.sheetName" value="preregistrationNotCompleted" />
		<dsp:input type="hidden"
			bean="FileDownloadHandler.reportName" value="Active_preRegistration_notCompleted _Report" />
		
		<dsp:input type="hidden" bean="FileDownloadHandler.exportUserSpreadsheet" value="true" />
	</dsp:form></br></br>
	<label> Inactive user with a set password (someone that registered, but did not click the confirmation email).</label>
	<dsp:form id="export-inactive-users" formid="export-inactive-users" action="/" method="post">
		<button class="btn btn-primary">Export</button>
		<dsp:input type="hidden"
			bean="FileDownloadHandler.reportQuery" value="activeNull" />
		<dsp:input type="hidden"
			bean="FileDownloadHandler.sheetName" value="InactiveUsers" />
		<dsp:input type="hidden"
			bean="FileDownloadHandler.reportName" value="User_verfication_notCompleted_report_" />
		
		<dsp:input type="hidden" bean="FileDownloadHandler.exportUserSpreadsheet" value="true" />
	</dsp:form>
</dsp:page>