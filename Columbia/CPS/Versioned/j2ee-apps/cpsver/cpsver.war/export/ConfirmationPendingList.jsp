<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<dsp:page>
<dsp:getvalueof var="userEmail" param="email" />
<dsp:getvalueof var="error" param="error" />
<c:if test="${error == 'false' }">
<p>Email Sent Successfully....!</p>
</c:if>

<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
<dsp:importbean bean="/cps/droplet/CPSConfirmationPendingUsersDroplet"/>
<dsp:form   enctype="multipart/form-data" method="post">
<dsp:droplet name="CPSConfirmationPendingUsersDroplet">
<dsp:param name="email" value="${userEmail}" />
<dsp:oparam name="output">
<dsp:getvalueof var="users" param="usersList" />
<table width='80%' border='2'>
        <thead>
          <tr>
            <th >Select</th>
            <th >Email</th>
          </tr>
       </thead>
	   <tbody>
<dsp:droplet name="/atg/dynamo/droplet/ForEach">
<dsp:param name="array" value="${users}"/>
<dsp:param name="elementName" value="user"/>
<dsp:oparam name="output">

<dsp:getvalueof var="email" param="user.login"/>
<dsp:getvalueof var="id" param="user.id" />
          <tr>
            <td> <input type="checkbox" name="user"  value="${id}" /></td>
            <td>${email}</td>
           </tr>
 </dsp:oparam>
 </dsp:droplet>
 </tbody>
 </table>
 <p><a class="btn" id="sendEmail">Send Email</a></p>
 </dsp:oparam>
 
<dsp:oparam name="empty">
No users found for your search.
</dsp:oparam>
</dsp:droplet>
<dsp:input type="hidden" bean="MultiUserAddFormHandler.userId" id="emailResendUserList"></dsp:input>
<dsp:input type="hidden" bean="MultiUserAddFormHandler.conformationEmailResend" value="true"></dsp:input>
<button  style="display:none"id="triggerSendEmail">send</button>
 </dsp:form>
 <script type="text/javascript">
 $(document).ready(function(){
 var userArray = new Array();
 $("#sendEmail").click(function(e){
     e.preventDefault();
 	 $("input:checkbox[name=user]:checked").each(function(){
        userArray.push($(this).val());
	 });
 	 $("#emailResendUserList").val(userArray);
 	 $("#triggerSendEmail").click();
	});
});
 </script>
</dsp:page>