<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
<dsp:include page="/includes/admin-console-header.jsp" />
<dsp:getvalueof var="error" param="error" />
<c:if test="${error == 'false' }">
<p>Repository cache invalidated successfully.</p>
</c:if>
<br>
	<dsp:droplet name='/atg/dynamo/droplet/ErrorMessageForEach'>
		<dsp:param name='exceptions' bean='MultiUserAddFormHandler.formExceptions' />
		<dsp:oparam name='output'>
			<li><dsp:valueof param='message'/></li>
		</dsp:oparam>
	</dsp:droplet>
	<p>Below button will invalidate profile repository cache.</p>
		<dsp:form id="invalidate-cache" formid="invalidate-cache" >
		<button class="btn btn-primary">invalidate Cache</button>
		<dsp:input type="hidden" bean="MultiUserAddFormHandler.invalidateCache" value="true" />
	</dsp:form>
</dsp:page>