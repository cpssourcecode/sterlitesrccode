<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<dsp:page>

 <dsp:droplet name='/cps/datacleanup/CPSTransactionDataCleanupDroplet'>
 
 <dsp:oparam name='noUsersToDelete'>
 NO Users to Delete.
 
 </dsp:oparam>
  <dsp:oparam name='error'>
 Errors Occurred.
 <dsp:getvalueof param='errorMessage' var='err'/>
<c:out value="${err}" escapeXml="false"/>
 </dsp:oparam>
   <dsp:oparam name='success'>
Users and transactional data deleted<BR>
<dsp:getvalueof param='successMessage' var='success'/>
<c:out value="${success}" escapeXml="false"/>
 </dsp:oparam>
 
 
 </dsp:droplet>

 
 

</dsp:page>