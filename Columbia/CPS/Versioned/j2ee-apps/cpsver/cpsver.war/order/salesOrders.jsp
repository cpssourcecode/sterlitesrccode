<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>

<dsp:page>
	<dsp:include page="/includes/admin-console-header.jsp" />
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<%-- <dsp:importbean
		bean="/atg/commerce/order/purchase/CPSSalesOrderReportFormHandler" /> --%>
	<dsp:importbean bean="/cps/util/FileDownloadHandler" />

	<h3>Sales Order Report</h3>
	<div>
		<dsp:form action="/" method="post" id="sales-order-report-form"
			formid="sales-order-report-form">

			<dsp:droplet name='/atg/dynamo/droplet/ErrorMessageForEach'>
				<dsp:param name='exceptions'
					bean='FileDownloadHandler.formExceptions' />
				<dsp:oparam name='output'>
					<p class="error-msg">
						<dsp:valueof param='message' />
					</p>
				</dsp:oparam>
			</dsp:droplet>

			<dsp:select bean="FileDownloadHandler.reportDay"
				id="sales-report-option">
				<%-- <dsp:option value="">Select Sales Report</dsp:option> --%>
				<dsp:option value="currentDay">Current Day</dsp:option>
				<dsp:option value="previousDay">Previous Day</dsp:option>
			</dsp:select>

			<p>
				<button class="btn btn-primary">Generate Sales Order Report</button>
			</p>
			<dsp:input type="hidden"
				bean="FileDownloadHandler.salesReportSuccessURL"
				value="/cpsver/order/salesOrders.jsp" />
			<dsp:input type="hidden"
				bean="FileDownloadHandler.salesReportErrorURL"
				value="/cpsver/order/salesOrders.jsp" />
			<dsp:input type="hidden"
				bean="FileDownloadHandler.generateSalesReport"
				value="true" />
		</dsp:form>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#sales-report-option").change(function() {
				$('.error-msg').remove();
			});
		});
	</script>
	<style type="text/css">
p.error-msg {
	color: #d3191e;
}
</style>
</dsp:page>