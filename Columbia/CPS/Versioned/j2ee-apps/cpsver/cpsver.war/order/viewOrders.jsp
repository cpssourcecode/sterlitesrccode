<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>

<dsp:page>
	<dsp:include page="/includes/admin-console-header.jsp" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CPSOrderFormHandler"/>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />

	<h3>List of orders without JDE order number</h3>
	<dsp:getvalueof var="error" param="error" />
	<c:if test="${error == 'false' }">
		<p>JDE order number updated successfully.</p>
	</c:if>
	
	<dsp:droplet name='/atg/dynamo/droplet/ErrorMessageForEach'>
		<dsp:param name='exceptions' bean='CPSOrderFormHandler.formExceptions' />
		<dsp:oparam name='output'>
			<p class="error-msg"><dsp:valueof param='message'/></p>
		</dsp:oparam>
	</dsp:droplet>

	<div>
	<dsp:form action="#" method="post" id="update-all-jde-order-number" formid="update-all-jde-order-number">
		<p><a class="btn" id="updateOrderTop">Update JDE Order Number</a></p>
        <table style="width: auto;" border='1' id="jdeOrderTable">
		    <thead>
			    <tr>
				    <%-- <th>Select</th> --%>
				    <th>Order Id</th>
				    <th>User Name</th>
				    <th>Email</th>
				    <th>Account</th>
				    <th>PO Number</th>
				    <th>Job Number</th>
				    <th>Delivery Method</th>
				    <th>JDE Number</th>
			    </tr>
		    </thead>
		    <tbody>
                <dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
			        <dsp:param name="repository" value="/atg/commerce/order/OrderRepository"/>
			        <dsp:param name="itemDescriptor" value="order"/>
			        <dsp:param name="queryRQL" value="jdeOrderNumber IS NULL AND jdeSuccess = 0"/>
			        <dsp:oparam name="output">
				        <dsp:getvalueof var="orderId" param="element.id"/>
				        <dsp:getvalueof var="profileId" param="element.profileId"/>
				        <dsp:getvalueof var="email" param="element.profileEmail" />
				        <dsp:getvalueof var="orgId" param="element.organizationId" />
				        <dsp:getvalueof var="webOrderId" param="element.webOrderId" />
                        <dsp:getvalueof var="jobNumberOrName" param="element.jobName"/>
                        <dsp:tomap var="paymentGroup" param="element.paymentGroups[0]" />
                        <dsp:tomap var="shippingGroup" param="element.shippingGroups[0]" />
                        <dsp:getvalueof var="deliveryMethod" value="${shippingGroup.shippingMethod}" />
						<tr>
							<%-- <td align="center"><input type="checkbox" name="order" value="${orderId}" /></td> --%>
				            <td><dsp:a href="/cpsver/order/viewOrder.jsp"><i>${orderId}</i>
							    	<dsp:param name="orderId" param="element.id"/>
							    </dsp:a>
							</td>
				            <td>
				            <dsp:droplet name="/atg/targeting/RepositoryLookup">
					            <dsp:param name="repository" bean="/atg/userprofiling/ProfileAdapterRepository"/>
					            <dsp:param name="itemDescriptor" value="user"/>
					            <dsp:param name="id" param="element.profileId"/>
					             <dsp:param name="elementName" value="profile"/>
					            <dsp:oparam name="output">
		                            <dsp:valueof param="profile.firstName"/>&nbsp;
		                            <dsp:valueof param="profile.lastName"/>
		                        </dsp:oparam>
		                    </dsp:droplet>
				            </td>
				            <td>${email}</td>
				            <td>${orgId}</td>
				            <td>${paymentGroup.PONumber}</td>
				            <td>${jobNumberOrName}</td>
				            <td>
				            	<c:choose>
				            		<c:when test="${deliveryMethod == 'hardgoodShippingGroup'}">
				            			Have it shipped
				            		</c:when>
				            		<c:when test="${deliveryMethod == 'inStorePickupShippingGroup'}">
				            			pick-up
				            		</c:when>
				            	</c:choose>
				            </td>
				            <td><input type="text" name="${orderId}" id="${orderId}" class="jdeOrderNumber" value="" /></td>
						</tr>
				    </dsp:oparam>
				    <dsp:oparam name="empty">
				    	<p>There is no order to update JDE order number. </p>
				    	<script type="text/javascript">
				    	$(document).ready(function(){
				    		$("#updateOrderTop,#updateOrder").hide();
							$("#jdeOrderTable").hide();
				    	});
				    	</script>
				    </dsp:oparam>
			    </dsp:droplet>
	        </tbody>
	    </table>
		<p><a class="btn" id="updateOrder">Update JDE Order Number</a></p>
		<dsp:getvalueof var="ordersToUpdate" param="ordersToUpdate" />
		<%-- <dsp:input type="hidden" bean="CPSOrderFormHandler.errorURL" value="/cpsver/order/viewOrders.jsp" id="jde-order-error-url"/>
		<dsp:input type="hidden" bean="CPSOrderFormHandler.successURL" value="/cpsver/order/viewOrders.jsp" id="jde-order-success-url"/> --%>
		<dsp:input type="hidden" bean="CPSOrderFormHandler.jdeOrdersMap" id="jde-order-number" converter="map" value="${ordersToUpdate}" />
		<dsp:input type="hidden" bean="CPSOrderFormHandler.updateJDEOrderNumber" value="true"></dsp:input>
		<button  style="display:none"id="triggerUpdateOrder">send</button>
	</dsp:form>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
	 		$("#updateOrder").click(function(e){
	     		e.preventDefault();
	     		var isError = false;
	     		var ordersToUpdate = $("input.jdeOrderNumber").map(function () {
	    	        var orderId = $.trim($(this).attr('name'));
	    	        var jdeOrderNumber = $.trim($(this).val());
	    	        var regex = /^[a-zA-Z0-9]*$/;
	    	        $(this).removeClass("error");
	    	        if(!regex.test(jdeOrderNumber)){
	    	       		$(this).addClass("error");
	    	         	isError = true;
	    	         	return;
	    	        }
	    	        return orderId + "=" + jdeOrderNumber;
	    	    }).get().join(",");
	     		if(!isError){
	     			$("#jde-order-number").val(ordersToUpdate);
	                $("#triggerUpdateOrder").click();
	     		}	     		
			});
	 		
	 		$('#updateOrderTop').on('click', function() {
	 	    	$('#updateOrder').trigger('click');
	 	    });
		});
	</script>
	<style type="text/css">
		.error {
			border-color: #d3191e;
  			color: #d3191e;
  			background-color: #f2dede;
		}
        p.error-msg {
            color: #d3191e;
        }
	</style>
</dsp:page>