<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>

<dsp:page>
	<dsp:include page="/includes/admin-console-header.jsp" />
	<dsp:importbean bean="/atg/commerce/order/AdminOrderLookup"/>
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
	<dsp:importbean bean="/atg/commerce/catalog/SKULookup"/>
	<dsp:importbean bean="/atg/commerce/inventory/InventoryLookup"/>
	<dsp:importbean bean="/atg/dynamo/droplet/RQLQueryForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	
	<p align="right"><a class="btn" href="/cpsver/order/viewOrders.jsp">Go Back</a></p>
	<h2> Order <dsp:valueof param="orderId" /></h2>
	<dsp:droplet name="AdminOrderLookup">
		<dsp:param name="orderId" param="orderId" />
		<dsp:oparam name="error">
    		Error looking up order.<br>
			<dsp:valueof param="errorMsg"></dsp:valueof>
		</dsp:oparam>
		<dsp:oparam name="output">
			<dsp:setvalue param="order" paramvalue="result"/>
			<dsp:getvalueof var="stateDetail" param="order.stateDetail" />
			<dsp:getvalueof var="completedDate" param="order.completedDate" />
			<dsp:getvalueof var="specialInstructions" param="order.specialInstructions" />
			<table border="1"><tr><td>State</td><td><dsp:valueof param="order.stateAsString">no state</dsp:valueof></td></tr>
			<c:if test="${not empty stateDetail}"> 
		    	<tr><td>Detail </td><td><dsp:valueof param="order.stateDetail" /></td></tr>
		    </c:if>
		    <tr><td>Created on </td><td><dsp:valueof param="order.creationDate" date="YYYY-MM-dd HH:mm">no creation date</dsp:valueof></td></tr>
		    <tr><td>Submitted on </td><td><dsp:valueof param="order.submittedDate" date="YYYY-MM-dd HH:mm">no submitted date</dsp:valueof></td></tr>
		    <tr><td>Last modified on </td><td><dsp:valueof param="order.lastModifiedDate" date="YYYY-MM-dd HH:mm">no last modified date</dsp:valueof></td></tr>
		    <c:if test="${not empty completedDate}">
		    	<tr><td>Completed on </td><td><dsp:valueof param="order.completedDate" date="YYYY-MM-dd HH:mm">no completed date</dsp:valueof></td></tr>
		    </c:if>
		    <c:if test="${not empty specialInstructions}">
		    	<tr><td>Special Instructions: </td><td><dsp:valueof param="specialInstructions"></dsp:valueof></td></tr>
		    </c:if>
			</table>
			<hr>
			<h2>Product Details:</h2>
		    <table border="1">
		    	<thead>
					<tr>
						<th><b>Product Id</b></th>
		    			<th><b>Item Number</b></th>
					    <%-- <th><b>SKU</b></th> --%>
					    <th><b>List Price</b></th>
					    <%-- <th><b>Sale Price</b></th> --%>
		    			<th align="right"><b>Order Quantity</b></th>
					    <th align=right><b>Total Price</b></th>
		    		</tr>
		    	</thead>
		    	<tbody>
					<dsp:droplet name="ForEach">
					    <dsp:param name="array" param="order.commerceItems"/>
					    <dsp:param name="elementName" value="item"/>
					    <dsp:oparam name="output">
      						<tr valign=top>
      							<td><dsp:valueof param="item.auxiliaryData.productId"/></td>
	      						<td>
						            <dsp:valueof param="item.auxiliaryData.productRef.displayName"/>
								</td>
							    <%-- <td>
							        <dsp:valueof param="item.auxiliaryData.catalogRef.displayName"/>
							        <br>Sku # <dsp:valueof param="item.catalogRefId"/>
							    </td> --%>
							    <td align="right">
							    	<dsp:valueof param="item.priceInfo.listPrice" converter="currency">no price</dsp:valueof>
							    </td>
							    <%-- <td align="right">
							    	<dsp:droplet name="Switch">
							        	<dsp:param name="value" param="item.priceInfo.onSale"/>
							          	<dsp:oparam name="true">
							            	<dsp:valueof param="item.priceInfo.salePrice" converter="currency"></dsp:valueof>
							            </dsp:oparam>
							    	</dsp:droplet>
							    </td> --%>
							    <td align="right">
	      							<dsp:getvalueof var="qty" vartype="java.lang.Long" param="item.quantity"/>${qty}
	      						</td>
	      						<td align=right>
							        <dsp:valueof param="item.priceInfo.amount" converter="currency">no price</dsp:valueof>
							    </td>
      						</tr>
      					</dsp:oparam>
      					<dsp:oparam name="empty">
      						<tr valign=top><td>No Items</td></tr>
      					</dsp:oparam>
      				</dsp:droplet>
					<tr>
					    <td colspan = "4" align=right>Subtotal</td>
					    <td align=right>
					        <dsp:valueof param="order.priceInfo.rawSubtotal" converter="currency">no price</dsp:valueof>
					    </td>
      				</tr>
     				<tr>
					    <td colspan = "4" align=right><b>Total</b></td>
					    <td align=right>
					        <b><dsp:valueof param="order.priceInfo.total" converter="currency">no price</dsp:valueof></b>
					    </td>
					</tr>
		    	</tbody>
		    </table>
		    <hr>
		    <!-- On-site contact details  -->
		    <h2>Contact Details</h2>
		    <dsp:getvalueof var="onsiteContact" param="order.onsiteContact"/>
		    <c:if test="${not empty onsiteContact}">
		    	<c:set var="onsiteContact" value="${fn:split(onsiteContact,'|')}"/>
				<c:forEach var="i" begin="0" end="2">
					<c:set value="${onsiteContact[0]}" var="onsiteContactName"/>
					<c:set value="${onsiteContact[1]}" var="onsiteContactphone"/>			 
				</c:forEach>
				<p><b>Contact Name:</b> ${onsiteContactName}</p>
				<p><b>Contact Phone:</b> ${onsiteContactphone}</p>
		    </c:if>
			<hr>
	        <!-- Shipping information -->
			<h2>Shipping Groups</h2>
			<dsp:droplet name="IsNull">
				<dsp:param name="value" param="order.shippingGroups"/>
				<dsp:oparam name="true">
					<p>There are no shipping groups in this order.</p>
				</dsp:oparam>
				<dsp:oparam name="false">
					<table>
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="order.shippingGroups"/>
							<dsp:param name="elementName" value="sg"/>
							<dsp:oparam name="empty">
								There were no shipping groups.<br>
							</dsp:oparam>
							<dsp:oparam name="output">
						  		<tr valign=top>
									<td>
									<dsp:droplet name="Switch">
						            	<dsp:param name="value" param="sg.shippingGroupClassType"/>
						                <dsp:oparam name="hardgoodShippingGroup">
							                <b>Delivery method : </b> shipped<%-- <dsp:valueof param="sg.shippingMethod">No shippingMethod</dsp:valueof> --%><br>
							                <b>Ship to Account : </b> <dsp:valueof param="sg.jdeAddressNumber"></dsp:valueof><br>
							                <b><i>Shipping address:</i></b><br>
							                <dsp:valueof param="sg.shippingAddress.firstName"></dsp:valueof>&nbsp;
							                <dsp:valueof param="sg.shippingAddress.lastName"></dsp:valueof><br>
							                <dsp:valueof param="sg.shippingAddress.address1"></dsp:valueof><br> 
							                <dsp:droplet name="IsEmpty">
												<dsp:param name="value" param="sg.shippingAddress.address2" />
												<dsp:oparam name="false">
													<dsp:valueof param="sg.shippingAddress.address2" /><br>
												</dsp:oparam>
											</dsp:droplet>
							                <dsp:valueof param="sg.shippingAddress.city"></dsp:valueof>,
							                <dsp:getvalueof var="state" param="sg.shippingAddress.state" />
							                <c:if test="${not empty state}">
							                	<dsp:valueof param="sg.shippingAddress.state"></dsp:valueof>,
							                </c:if>
							                <dsp:valueof param="sg.shippingAddress.postalCode"></dsp:valueof><br>
							                <dsp:valueof param="sg.shippingAddress.phoneNumber"/>
						               	</dsp:oparam>
							            <dsp:oparam name="inStorePickupShippingGroup">
							            	<b>Delivery method : </b> pick-up<%-- <dsp:valueof param="sg.shippingMethod">No shippingMethod</dsp:valueof> --%><br>
							                <b><i>Pick up at:</i></b><br>
							            	<dsp:droplet name="/atg/commerce/locations/StoreLookupDroplet">
												<dsp:param name="id" param="sg.locationId" />
												<dsp:param name="elementName" value="store" />
												<dsp:oparam name="output">
													<dsp:valueof param="store.name"></dsp:valueof><br>
									                <dsp:valueof param="store.address1"></dsp:valueof><br>
									                <dsp:droplet name="IsEmpty">
														<dsp:param name="value" param="store.address2" />
														<dsp:oparam name="false">
															<dsp:valueof param="store.address2" /><br>
														</dsp:oparam>
													</dsp:droplet>
									                <dsp:valueof param="store.city"></dsp:valueof>, 
									                <dsp:valueof param="store.stateAddress"></dsp:valueof>, 
									                <dsp:valueof param="store.postalCode"></dsp:valueof><br>
									                <dsp:valueof param="store.country"></dsp:valueof><br>
									                <dsp:valueof param="store.phoneNumber"/>
												</dsp:oparam>
												<dsp:oparam name="empty">
													<dsp:valueof param="element.locationId" />
												</dsp:oparam>
											</dsp:droplet>
										</dsp:oparam>
									</dsp:droplet>
									</td>
								</tr>
							</dsp:oparam>
						</dsp:droplet>
					</table>
				</dsp:oparam>
			</dsp:droplet>
			<hr>
			
			<!-- Payment information -->
			<h2>Payment Groups</h2>
			<dsp:droplet name="IsNull">
				<dsp:param name="value" param="order.paymentGroups"/>
				<dsp:oparam name="true">
					<p>There are no payment groups in this order.</p>
				</dsp:oparam>
				<dsp:oparam name="false">
				<table>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="order.paymentGroups"/>
						<dsp:param name="elementName" value="pg"/>
						<dsp:oparam name="empty">
							There were no payment groups.<br>
						</dsp:oparam>
						<dsp:oparam name="output">
							<dsp:droplet name="Switch">
			                	<dsp:param name="value" param="pg.paymentMethod"/>
			                	<%-- If credit card payment method is used, uncomment below code --%>
			                	<%-- <dsp:oparam name="creditCard">
			                	    <b><i>Pay with <dsp:valueof param="pg.creditCardType"></dsp:valueof> :</i></b> #
					                <dsp:valueof param="pg.creditCardNumber">no number</dsp:valueof><br>
					                Expiration date is <dsp:valueof param="pg.expirationMonth"></dsp:valueof>/
					                <dsp:valueof param="pg.expirationYear"></dsp:valueof><br>
					                <b><i>Billing address:</i></b><br>
					                <dsp:valueof param="pg.billingAddress.firstName"></dsp:valueof> 
					                <dsp:valueof param="pg.billingAddress.lastName"></dsp:valueof><br>
					                <dsp:valueof param="pg.billingAddress.address1"></dsp:valueof> 
					                <dsp:valueof param="pg.billingAddress.address2"></dsp:valueof><br>
					                <dsp:valueof param="pg.billingAddress.city"></dsp:valueof>, 
					                <dsp:valueof param="pg.billingAddress.state"></dsp:valueof> 
					                <dsp:valueof param="pg.billingAddress.postalCode"></dsp:valueof><br>
					       		</dsp:oparam> --%>
					        	<dsp:oparam name="invoiceRequest">
					            	<b><i>Payment Method : Invoice <%-- <dsp:valueof param="pg.paymentMethod" /> --%> </i></b><br>
					            	<tr valign=top>
										<td>			
											<b>PO Number : </b><dsp:valueof param="pg.PONumber"/><br>								
											<b><i>Billing address:</i></b>
											<dsp:valueof param="pg.billingAddress.firstName"></dsp:valueof> 
					                		<dsp:valueof param="pg.billingAddress.lastName"></dsp:valueof><br>
							                <dsp:valueof param="pg.billingAddress.address1"></dsp:valueof><br>
							                <dsp:droplet name="IsEmpty">
												<dsp:param name="value" param="pg.billingAddress.address2" />
												<dsp:oparam name="false">
													<dsp:valueof param="pg.billingAddress.address2" /><br>
												</dsp:oparam>
											</dsp:droplet>
							                <dsp:valueof param="pg.billingAddress.city"></dsp:valueof>, 
							                <dsp:valueof param="pg.billingAddress.state"></dsp:valueof>&nbsp;
							                <dsp:valueof param="pg.billingAddress.postalCode"></dsp:valueof><br>
										</td>
									</tr>
					        	</dsp:oparam>
			                </dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</table>
				</dsp:oparam>
			</dsp:droplet><hr>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>