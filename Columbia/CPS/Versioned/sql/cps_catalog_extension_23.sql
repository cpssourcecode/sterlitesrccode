ALTER TABLE CPS_PRODUCT_ALL ADD(
  substitute_brand_message_id     INTEGER    NULL
  );




--drop table  CPS_SUBSTITUTE_BRAND_MESSAGE;

CREATE TABLE CPS_SUBSTITUTE_BRAND_MESSAGE(
  ASSET_VERSION                   NUMBER(19)      NOT NULL,
  WORKSPACE_ID                    VARCHAR2(40)    NOT NULL,
  BRANCH_ID                       VARCHAR2(40)    NOT NULL,
  IS_HEAD                         NUMBER(1)       NOT NULL,
  VERSION_DELETED                 NUMBER(1)       NOT NULL,
  VERSION_EDITABLE                NUMBER(1)       NOT NULL,
  PRED_VERSION                    NUMBER(19)      NULL,
  CHECKIN_DATE                    TIMESTAMP       NULL,
  ID                              VARCHAR2(100)    NOT NULL,
  DISPLAY_NAME                    VARCHAR2 (254)  NOT NULL,
  MESSAGE                         VARCHAR2 (800)   NOT NULL,
  CONSTRAINT CPS_SUB_BRAND_MESSAGE_PK PRIMARY KEY(ID,DISPLAY_NAME,ASSET_VERSION)
);
CREATE INDEX CPS_SUB_BRAND_M_WORKSPACE_ID ON CPS_SUBSTITUTE_BRAND_MESSAGE (WORKSPACE_ID);
CREATE INDEX CPS_SUB_BRAND_M_CHECKIN_ID ON CPS_SUBSTITUTE_BRAND_MESSAGE (CHECKIN_DATE);