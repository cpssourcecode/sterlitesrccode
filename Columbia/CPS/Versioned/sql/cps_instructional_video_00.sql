DROP TABLE cps_insvid_roles;
DROP TABLE cps_insvid_items;
DROP TABLE cps_insvid_item_role_map;

CREATE TABLE cps_insvid_roles (
	asset_version	INT NOT NULL,
	workspace_id	VARCHAR(40) NOT NULL,
	branch_id		VARCHAR(40) NOT NULL,
	is_head			NUMERIC(1) NOT NULL,
	version_deleted	NUMERIC(1) NOT NULL,
	version_editable	NUMERIC(1) NOT NULL,
	pred_version	INT NULL,
	checkin_date	TIMESTAMP NULL,
	id			VARCHAR(40) NOT NULL,
	role_name	VARCHAR(40) NOT NULL,
	display_order	INT NULL,
	view_all		NUMERIC(1) NOT NULL,
	CONSTRAINT cps_insvid_roles_pk PRIMARY KEY (id, asset_version)
);

CREATE TABLE cps_insvid_items (
	asset_version	INT NOT NULL,
	workspace_id	VARCHAR(40) NOT NULL,
	branch_id		VARCHAR(40) NOT NULL,
	is_head			NUMERIC(1) NOT NULL,
	version_deleted	NUMERIC(1) NOT NULL,
	version_editable	NUMERIC(1) NOT NULL,
	pred_version	INT NULL,
	checkin_date	TIMESTAMP NULL,
	id			VARCHAR(40) NOT NULL,
	video_url	VARCHAR(255) NOT NULL,
	video_title	VARCHAR(50) NOT NULL,
	video_length	VARCHAR(20) NULL,
	topic_1		VARCHAR(35) NULL,
	topic_2		VARCHAR(35) NULL,
	topic_3		VARCHAR(35) NULL,
	topic_4		VARCHAR(35) NULL,
	topic_5		VARCHAR(35) NULL,
	topic_6		VARCHAR(35) NULL,
	CONSTRAINT cps_insvid_items_pk PRIMARY KEY (id, asset_version)
);

CREATE TABLE cps_insvid_item_role_map (
	asset_version	INT NOT NULL,
	item_id	VARCHAR(40) NOT NULL,
	role_id	VARCHAR(40) NOT NULL,
	CONSTRAINT cps_insvid_item_role_map_pk PRIMARY KEY (item_id, role_id, asset_version)
);