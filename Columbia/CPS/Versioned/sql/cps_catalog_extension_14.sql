-- drop table CPS_CAT_FACETS

CREATE TABLE CPS_CAT_FACETS (
	ASSET_VERSION			NUMBER(19) 						NOT NULL,
	CATEGORY_ID				VARCHAR2(40) 					NOT NULL,
	CAT_FACET 				VARCHAR2(1024 BYTE) 			NULL,
	SEQUENCE_NUM 			INTEGER 						NOT NULL,
	CONSTRAINT CPS_CAT_FACETS_PK PRIMARY KEY (ASSET_VERSION, CATEGORY_ID, SEQUENCE_NUM)
);