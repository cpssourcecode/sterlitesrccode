drop table store_images;

create table cps_store_images (
  ASSET_VERSION NUMBER(19) NOT NULL,
    id varchar2(40) not null,
    media_id varchar2(40) not null
,constraint store_images_p primary key (asset_version,id,media_id));
