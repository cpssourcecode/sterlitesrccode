drop table cps_affilations;

CREATE TABLE cps_affilations (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id			VARCHAR(40)	NOT NULL,
is_head				NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
id				varchar2(40)	NOT NULL,
image_uri		varchar2(254)	NULL,
description		CLOB	NULL,
link	varchar2(254)	NULL,
CONSTRAINT cps_affilations_pk PRIMARY KEY(ID,ASSET_VERSION));