-- drop table CPS_MENU
-- drop table CPS_MENU_CAT
-- drop table CPS_MENU_BRAND
-- drop table CPS_MENU_ITEM
-- drop table CPS_MENU_SUBFOLDER
-- drop table CPS_MENU_FOLDER
-- drop table CPS_CATALOG_MENU

CREATE TABLE CPS_MENU (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    WORKSPACE_ID                   VARCHAR2(40)           NOT NULL,
    BRANCH_ID                      VARCHAR2(40)           NOT NULL,
    IS_HEAD                        NUMBER(1)              NOT NULL,
    VERSION_DELETED                NUMBER(1)              NOT NULL,
    VERSION_EDITABLE               NUMBER(1)              NOT NULL,
    PRED_VERSION                   NUMBER(19, 0)          NULL,
    CHECKIN_DATE                   TIMESTAMP              NULL,
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    MENU_TYPE                      NUMBER(19, 0)          NOT NULL,
    MENU_NAME                      VARCHAR2(254)          NULL,
    DESCRIPTION                    VARCHAR2(1024)         NULL,
    MENU_LINK                      VARCHAR2(1024)         NULL,
    MENU_IMAGE                     VARCHAR2(1024)         NULL,
    MENU_ICON                      VARCHAR2(1024)         NULL,
    ITEM_ACL                       VARCHAR2(1024)         NULL,
    CONSTRAINT CPS_MENU_PK PRIMARY KEY (MENU_ID)
);

create index CPS_MENU_WORKSPACE_ID on CPS_MENU (WORKSPACE_ID); 
create index CPS_MENU_CHECKIN_DATE on CPS_MENU (CHECKIN_DATE);

CREATE TABLE CPS_MENU_CAT (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    CATEGORY_ID                    VARCHAR2(40)           NULL,
    CONSTRAINT CPS_MENU_CAT_PK PRIMARY KEY (ASSET_VERSION, MENU_ID)
);

CREATE TABLE CPS_MENU_BRAND (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    BRAND_ID                       VARCHAR2(40)           NULL,
    CONSTRAINT CPS_MENU_BRAND_PK PRIMARY KEY (ASSET_VERSION, MENU_ID)
);

CREATE TABLE CPS_MENU_FOLDER (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    WORKSPACE_ID                   VARCHAR2(40)           NOT NULL,
    BRANCH_ID                      VARCHAR2(40)           NOT NULL,
    IS_HEAD                        NUMBER(1)              NOT NULL,
    VERSION_DELETED                NUMBER(1)              NOT NULL,
    VERSION_EDITABLE               NUMBER(1)              NOT NULL,
    PRED_VERSION                   NUMBER(19, 0)          NULL,
    CHECKIN_DATE                   TIMESTAMP              NULL,
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    VERSION                        NUMBER(19, 0)          NOT NULL,
    FOLDER_NAME                    VARCHAR2(100)          NOT NULL,
    FOLDER_PATH                    VARCHAR2(1024)         NOT NULL,
    PARENT_FOLDER_ID               VARCHAR2(40)           NULL,
    DESCRIPTION                    VARCHAR2(254)          NULL,
    MENU_ID                        VARCHAR2(40)           NULL,
    ITEM_ACL                       VARCHAR2(1024)         NULL,
    CONSTRAINT CPS_MENU_FOLDER_PK PRIMARY KEY (ASSET_VERSION, FOLDER_ID)
);

create index CPS_MENU_FOLDER_WORKSPACE_ID on CPS_MENU_FOLDER (WORKSPACE_ID); 
create index CPS_MENU_FOLDER_CHECKIN_DATE on CPS_MENU_FOLDER (CHECKIN_DATE);

CREATE TABLE CPS_MENU_SUBFOLDER (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    SEQUENCE_NUM                   NUMBER(19, 0)          NOT NULL,
    SUBFOLDER_ID                   VARCHAR2(40)           NOT NULL,
    CONSTRAINT CPS_MENU_SUBFOLDER_PK PRIMARY KEY (ASSET_VERSION, FOLDER_ID, SEQUENCE_NUM)
);

CREATE TABLE CPS_MENU_ITEM (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    SEQUENCE_NUM                   NUMBER(19, 0)          NOT NULL,
    MENU_ID                        VARCHAR2(40)           NOT NULL,
    CONSTRAINT CPS_MENU_ITEM_PK PRIMARY KEY (ASSET_VERSION, FOLDER_ID, SEQUENCE_NUM)
);

CREATE TABLE CPS_CATALOG_MENU (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    CATALOG_ID                     VARCHAR2(40)           NOT NULL,
    SEQUENCE_NUM                   NUMBER(19, 0)          NOT NULL,
    FOLDER_ID                      VARCHAR2(40)           NOT NULL,
    CONSTRAINT CPS_CATALOG_MENU_PK PRIMARY KEY (ASSET_VERSION, CATALOG_ID, SEQUENCE_NUM)
);