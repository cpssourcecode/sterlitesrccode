drop table cps_blackout_date;

CREATE TABLE cps_blackout_date (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id			VARCHAR(40)	NOT NULL,
is_head				NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
id				varchar2(40)	NOT NULL,
blackout_date			date		NOT NULL,
description			varchar2(80)	NULL,
CONSTRAINT cps_blackout_date_pk PRIMARY KEY(ID,ASSET_VERSION));