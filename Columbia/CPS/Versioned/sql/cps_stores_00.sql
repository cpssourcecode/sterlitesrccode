drop table cps_stores;

create table cps_stores (
  id                    VARCHAR2(40) NOT NULL,
  area                  VARCHAR2(100) NOT NULL,
  address               VARCHAR2(100) NOT NULL,
  city                  VARCHAR2(100) NOT NULL,
  state                 VARCHAR2(2)  NOT NULL,
  zip                   VARCHAR2(5)  NOT NULL,
  phone                 VARCHAR2(12) NOT NULL,
  open_time             TIMESTAMP(9)  NOT NULL,
  close_time            TIMESTAMP(9)  NOT NULL,
  timezone              VARCHAR2(3)   NOT NULL,
CONSTRAINT cps_stores_pk PRIMARY KEY(id));

select * from cps_stores;