DROP TABLE cps_faq_category;
DROP TABLE cps_faq_question;

CREATE TABLE cps_faq_category (
	asset_version		INT NOT NULL,
	workspace_id		VARCHAR(40) NOT NULL,
	branch_id			VARCHAR(40) NOT NULL,
	is_head				NUMERIC(1) NOT NULL,
	version_deleted		NUMERIC(1) NOT NULL,
	version_editable	NUMERIC(1) NOT NULL,
	pred_version		INT	NULL,
	checkin_date		TIMESTAMP NULL,
	id 					VARCHAR2(40) NOT NULL,
	category_name		VARCHAR2(40) NOT NULL,
	display_order		NUMERIC(3) NULL,
	CONSTRAINT cps_faq_category_pk PRIMARY KEY (id, asset_version)
);

CREATE TABLE cps_faq_question (
	asset_version		INT NOT NULL,
	workspace_id		VARCHAR(40) NOT NULL,
	branch_id			VARCHAR(40) NOT NULL,
	is_head				NUMERIC(1) NOT NULL,
	version_deleted		NUMERIC(1) NOT NULL,
	version_editable	NUMERIC(1) NOT NULL,
	pred_version		INT	NULL,
	checkin_date		TIMESTAMP NULL,
	id 					VARCHAR2(40) NOT NULL,
	question 			VARCHAR2(255) NOT NULL,
	answer				VARCHAR2(255) NOT NULL,
	category 			VARCHAR(40) NOT NULL,
	display_order		NUMERIC(3) NULL,
	CONSTRAINT cps_faq_question_pk PRIMARY KEY (id, asset_version)
);