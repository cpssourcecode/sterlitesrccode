drop table cps_order;
create table cps_order (
order_id                varchar2(40)  not null,
last_applied_coupon     varchar2(40)  null,
jde_order_number        varchar2(40)  null,
sent_to_fulfillment     numeric(1,0)  null,
mtr_requested           numeric(1,0)  null,
pricing_call_status     numeric(1,0)  null,
constraint cps_order_p primary key (order_id),
constraint cps_order_f foreign key (order_id) references dcspp_order (order_id));
select count(*) from cps_order;

drop table cps_hrd_ship_grp;
CREATE TABLE cps_hrd_ship_grp (
shipping_group_id              VARCHAR2(40)  NOT NULL,
branch_id                      VARCHAR2(40)  NULL,
pickup_date                    VARCHAR2(40)  NULL,
pickup_time                    VARCHAR2(40)  NULL,
carrier                        VARCHAR2(40)  NULL,
carrier_acct_number            VARCHAR2(40)  NULL,
CONSTRAINT cps_hrd_ship_grp_PK PRIMARY KEY (shipping_group_id),
CONSTRAINT cps_hrd_ship_grp_FK1 FOREIGN KEY (shipping_group_id) REFERENCES dcspp_ship_group (shipping_group_id));
select count(*) from cps_hrd_ship_grp;

drop table cps_dcspp_ship_addr;
create table cps_dcspp_ship_addr (
SHIPPING_GROUP_ID                 VARCHAR2(40)  NOT NULL,
jde_address_number                VARCHAR2(40)  NULL,
billto_phone_ext                  VARCHAR2(10)  NULL,
billto_alternate_phone            VARCHAR2(12)  NULL,
billto_alternate_phone_ext        VARCHAR2(10)  NULL,
tax_code                          VARCHAR2(40)  NULL,
CONSTRAINT cps_dcspp_ship_addr_PK PRIMARY KEY (SHIPPING_GROUP_ID),
CONSTRAINT cps_dcspp_ship_addr_FK FOREIGN KEY (SHIPPING_GROUP_ID) REFERENCES DCSPP_SHIP_GROUP (SHIPPING_GROUP_ID));
select count(*) from cps_dcspp_ship_addr;

drop table cps_dcspp_bill_addr;
create table cps_dcspp_bill_addr (
PAYMENT_GROUP_ID                  VARCHAR2(40)  NOT NULL,
jde_address_number                VARCHAR2(40)  NULL,
billto_zip_code                   VARCHAR2(6)   NULL,
billto_phone_ext                  VARCHAR2(10)  NULL,
billto_alternate_phone            VARCHAR2(12)  NULL,
billto_alternate_phone_ext        VARCHAR2(10)  NULL,
CONSTRAINT cps_dcspp_bill_addr_PK PRIMARY KEY (PAYMENT_GROUP_ID),
CONSTRAINT cps_dcspp_bill_addr_FK FOREIGN KEY (PAYMENT_GROUP_ID) REFERENCES DCSPP_PAY_GROUP (PAYMENT_GROUP_ID));
select count(*) from cps_dcspp_bill_addr;

drop table cps_dcspp_item;
create table cps_dcspp_item (
COMMERCE_ITEM_ID             VARCHAR2(40)  NOT NULL,
is_hazardous_item            numeric(1,0)  null,
inventory_qty                number(4,0)   null,
inventory_status             number(4,0)   null,
CONSTRAINT cps_dcspp_item_PK PRIMARY KEY (COMMERCE_ITEM_ID),
CONSTRAINT cps_dcspp_item_FK FOREIGN KEY (COMMERCE_ITEM_ID) REFERENCES DCSPP_ITEM (COMMERCE_ITEM_ID));
select count(*) from cps_dcspp_item;

drop table cps_order_hold_reason;
create table cps_order_hold_reason (
order_id                            varchar2(40)  not null,
sequence_num                        integer       not null,
hold_reason                         varchar2(40)  null,
constraint cps_order_hold_reason_pk primary key (order_id,sequence_num),
constraint cps_order_hold_reason_fk foreign key (order_id) references dcspp_order (order_id));
select count(*) from cps_dcspp_item;