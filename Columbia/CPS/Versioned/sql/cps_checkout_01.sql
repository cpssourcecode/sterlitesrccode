--drop table cps_order_coupon_codes;
create table cps_order_coupon_codes (
	sequence_num integer not null,
	order_id	varchar2(40)	not null,
	coupon_codes	varchar2(40)	null
,constraint cps_order_coupon_codes_p primary key (sequence_num,order_id)
,constraint cps_order_coupon_codes_f foreign key (order_id) references dcspp_order (order_id));
