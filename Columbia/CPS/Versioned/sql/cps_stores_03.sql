drop table store_images;

create table store_images (
  ASSET_VERSION		NUMBER(19) 			NOT NULL,
	id	varchar2(40)	not null,
	sequence_num	integer	not null,
	media_id	varchar2(40)	not null
,constraint store_images_p primary key (asset_version,id,sequence_num));