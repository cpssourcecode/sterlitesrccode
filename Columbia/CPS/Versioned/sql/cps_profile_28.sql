create table CPS_ADMIN_ORDER_NOTIFICATIONS (
user_id	varchar2(40)	not null references dps_user (id), 
notification_enabled_users	varchar2(40)	not null,
constraint cps_order_notify_pk primary key (user_id, notification_enabled_users)
);