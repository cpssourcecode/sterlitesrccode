drop table CPS_ROOT_CATS;

CREATE TABLE CPS_ROOT_CATS (
    ASSET_VERSION           NUMBER(19)                      NOT NULL,
    CATALOG_ID              VARCHAR2(40)                    NOT NULL,
    ROOT_CAT_ID             VARCHAR2(40)                    NOT NULL,
    SEQUENCE_NUM            INTEGER                         NOT NULL,
    CONSTRAINT CPS_ROOT_CATS_P PRIMARY KEY (CATALOG_ID, SEQUENCE_NUM, ROOT_CAT_ID, ASSET_VERSION)
);