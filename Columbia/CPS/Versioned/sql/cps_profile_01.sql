alter table cps_b2b_user drop column security_question;
alter table cps_b2b_user drop column security_question_answer;

create table CPS_USER (
  id  varchar2(40)  NOT NULL REFERENCES dps_user(id),
  security_question varchar2(100) NULL,
  security_question_answer  varchar2(100) NULL,
  time_locked       timestamp          null,
  CONSTRAINT cps_user_pk PRIMARY KEY(id)
);
