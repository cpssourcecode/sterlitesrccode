drop table cps_video;
drop table cps_video_category;

CREATE TABLE cps_video_category (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id			VARCHAR(40)	NOT NULL,
is_head				NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
id				varchar2(40)	NOT NULL,
name			varchar2(254)	NOT NULL,
CONSTRAINT cps_video_category_pk PRIMARY KEY(ID,ASSET_VERSION));

CREATE TABLE cps_video (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id			VARCHAR(40)	NOT NULL,
is_head				NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
video_id		varchar2(40)	not null,
category_id		varchar2(40)		null,
video_url		varchar2(254)	NOT NULL,
title			varchar2(254)		NULL,
time			varchar2(254)		NULL,
description		varchar2(254)		NULL,
CONSTRAINT cps_video_pk PRIMARY KEY(video_id,ASSET_VERSION));