drop table cps_news_and_events;

CREATE TABLE cps_news_and_events (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id				VARCHAR(40)	NOT NULL,
is_head					NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
id				varchar2(40)	NOT NULL,
title			varchar2(254)	NULL,
image_uri		varchar2(254)	NULL,
text_content	CLOB			NULL,
creation_date	TIMESTAMP(6)	NULL,
CONSTRAINT cps_news_and_events_pk PRIMARY KEY(ID,ASSET_VERSION));