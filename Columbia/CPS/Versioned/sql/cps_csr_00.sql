CREATE TABLE cps_csr_action (
  ID VARCHAR2(40) NOT NULL,
  user_type NUMBER(3) NOT NULL,
  time_logged TIMESTAMP NOT NULL,
  csr_user VARCHAR2(40) NOT NULL,
  impersonated_user VARCHAR2(40) NULL,
  account_id VARCHAR2(40) NULL,
  action_type NUMBER(3) NOT NULL,
  action_detail VARCHAR2(255) NOT NULL,
  CONSTRAINT csr_action_pk PRIMARY KEY(ID)
);