--drop table cps_price_lists;
--drop table cps_price_list_updates;
--drop table cps_price_sections;
--drop table cps_publisher_emails;

CREATE TABLE cps_price_lists (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id			VARCHAR(40)	NOT NULL,
is_head				NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
id				varchar2(40)	NOT NULL,
description		CLOB	NULL,
CONSTRAINT cps_price_lists_pk PRIMARY KEY(ID,ASSET_VERSION));

CREATE TABLE cps_price_list_updates (
	asset_version   int     NOT NULL,
	id      varchar2(40)  NOT NULL,
	name    varchar2(254)  NOT NULL,
	price_list_update varchar2(254) NOT NULL,
	CONSTRAINT cps_price_list_updates_pk PRIMARY KEY (id, name, asset_version)
);

CREATE TABLE cps_price_sections (
	asset_version   int     NOT NULL,
	id      varchar2(40)  NOT NULL,
	name    varchar2(254)  NOT NULL,
	price_section varchar2(254) NOT NULL,
	CONSTRAINT cps_price_sections_pk PRIMARY KEY (id, name, asset_version)
);

CREATE TABLE cps_publisher_emails (
	asset_version   int     NOT NULL,
	id      varchar2(40)  NOT NULL,
	email    varchar2(254)  NOT NULL,
	CONSTRAINT cps_publisher_emails_pk PRIMARY KEY (id, email, asset_version)
);

