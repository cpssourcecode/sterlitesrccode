drop table cps_case_studies;

CREATE TABLE cps_case_studies (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id			VARCHAR(40)	NOT NULL,
is_head				NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
id			varchar2(40)	NOT NULL,
image_url		varchar2(254)	NULL,
title		        varchar2(254)	NULL,
description		CLOB	NULL,
link	                varchar2(254)	NULL,
creation_date           timestamp(6)     NULL,
problem		        CLOB	NULL,
solution		CLOB	NULL,
value		        CLOB	NULL,
testimonial_id             varchar2(40) NULL,
CONSTRAINT cps_case_studies_pk PRIMARY KEY(ID,ASSET_VERSION));