alter table cps_user add IS_SEND_EMAIL number(1) ;

create table cps_dps_mailing(
    id varchar2(40) not null references dps_mailing(id),
    forgot_password_link varchar2(1000),
    orderId varchar2(40),
    email_create_date timestamp,
    CONSTRAINT cps_dps_mailing_pk PRIMARY KEY(id)
);
