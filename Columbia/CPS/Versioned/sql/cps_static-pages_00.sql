create table cps_folder(
	asset_version		number(19)		not null,
	workspace_id		varchar2(40)	not null,
	branch_id			varchar2(40)	not null,
	is_head				number(1)		not null,
	version_deleted		number(1)		not null,
	version_editable	number(1)		not null,
	pred_version		number(19)		null,
	checkin_date		timestamp		null,
	folder_id	varchar2(40) not null,
	version	number	not null,
	creation_date	timestamp(6)	null,
	start_date	timestamp(6)	null,
	end_date	timestamp(6)	null,
	description	varchar2(254)	null,
	name	varchar2(254) not null,
	path	varchar2(254)	null,
	parent_folder_id	varchar2(40)	null,
	constraint cps_folder_pk primary key (folder_id, asset_version)
);

create index cps_folder_workspace_id on cps_folder (workspace_id); 
create index cps_folder_checkin_date on cps_folder (checkin_date);

create table cps_page_template (
	asset_version		number(19)		not null,
	workspace_id		varchar2(40)	not null,
	branch_id			varchar2(40)	not null,
	is_head				number(1)		not null,
	version_deleted		number(1)		not null,
	version_editable	number(1)		not null,
	pred_version		number(19)		null,
	checkin_date		timestamp		null,
	page_id				varchar2(40)	not null,
	display_name		varchar2(254)	null,
	description			varchar2(254)	null,
	page_title			varchar2(254)	null,
	meta_description	varchar2(254)	null,
	meta_keywords		varchar2(254)	null,
	page_url			varchar2(254)	not null,
	html				clob			null,
	start_date			timestamp(6)	null,
	end_date			timestamp(6)	null,
	page_order 			number			null,
	left_navigation		number(1)		null,
	parent_folder_id	varchar2(40)	null,
	path				varchar2(254)	null,
	constraint cps_t_t_pk primary key (page_id, asset_version)
);

create index cps_page_template_workspace_id on cps_page_template (workspace_id); 
create index cps_page_template_checkin_date on cps_page_template (checkin_date);

CREATE TABLE CPS_CONTENT (
	ASSET_VERSION			NUMBER(19) 			NOT NULL,
	CONTENT_ID				VARCHAR2(40)		NOT NULL,
	MEDIA_TYPE				INTEGER				NOT NULL,
	DISPLAY_DESCRIPTION		VARCHAR2(5)			NULL,
	DISPLAY_UPSELLS			VARCHAR2(5)			NULL,
	FEATURE_PROMO_ID		VARCHAR2(40)		NULL,
	PDF_PATH				VARCHAR2(254)		NULL,
	HTML_BLOCK				CLOB				NULL,
	CONSTRAINT CPS_CONTENT_PK PRIMARY KEY (ASSET_VERSION, CONTENT_ID)
);


CREATE TABLE CPS_TAB (
	ASSET_VERSION			NUMBER(19) 			NOT NULL,
	WORKSPACE_ID			VARCHAR2(40) 		NOT NULL,
	BRANCH_ID 				VARCHAR2(40) 		NOT NULL,
	IS_HEAD 				NUMBER(1) 			NOT NULL,
	VERSION_DELETED			NUMBER(1) 			NOT NULL,
	VERSION_EDITABLE		NUMBER(1) 			NOT NULL,
	PRED_VERSION 			NUMBER(19)			NULL,
	CHECKIN_DATE 			DATE 				NULL,
	TAB_ID			VARCHAR2(40)		NOT NULL,
	DISPLAY_NAME	VARCHAR2(254)		NOT NULL,
	TITLE			VARCHAR2(100)		NOT NULL,
	START_DATE		TIMESTAMP			NULL,
	END_DATE		TIMESTAMP			NULL,
	TEMPLATE_ID		VARCHAR2(40)		NOT NULL,
	TAB_CONTENT		CLOB				NULL,
	CONSTRAINT CPS_TAB_PK PRIMARY KEY (ASSET_VERSION, TAB_ID)
);

create table cps_page_template_sites (
	asset_version		number(19)		not null,
	page_id					varchar2(40)	not null, 
	site_name				varchar2(254)	null,
	constraint cps_p_t_sites_pk primary key (page_id, site_name, asset_version)
);

create table cps_page_template_pages(
	asset_version		number(19)		not null,
	sec_asset_version	number(19)		not null,
	page_id varchar2(40) not null, 
	page varchar2(100) null,
constraint cps_p_t_pk primary key (page_id, page, asset_version, sec_asset_version)
);


create table cps_promo_segments(
	asset_version			number(19)		not null,
	promo_id varchar2(40) not null, 
	segments varchar2(100) null,
constraint cps_promo_segments_pk primary key (asset_version, promo_id, segments)
);

create table cps_promo_keywords(
	asset_version			number(19)		not null,
	promo_id varchar2(40) not null, 
	keywords varchar2(100) null,
constraint cps_promo_keywords_pk primary key (asset_version, promo_id, keywords)
);

CREATE TABLE CPS_CAT_CONTENT (
	ASSET_VERSION			NUMBER(19) 			NOT NULL,
	CATEGORY_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM		INTEGER				NOT NULL,
	CONTENT_ID			VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_CAT_CONTENT_PK PRIMARY KEY (ASSET_VERSION, CATEGORY_ID, SEQUENCE_NUM)
);


CREATE TABLE CPS_CAT_TAB (
	ASSET_VERSION			NUMBER(19) 			NOT NULL,
	CATEGORY_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM		INTEGER				NOT NULL,
	TAB_ID				VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_CAT_TAB_PK PRIMARY KEY (ASSET_VERSION, CATEGORY_ID, SEQUENCE_NUM)
);




CREATE TABLE CPS_TAB_CAT (
	ASSET_VERSION		NUMBER(19) 			NOT NULL,
	TAB_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM	INTEGER				NOT NULL,
	CATEGORY_ID		VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_TAB_CAT_PK PRIMARY KEY (ASSET_VERSION, TAB_ID, SEQUENCE_NUM)
);


CREATE TABLE CPS_TAB_PRD (
	ASSET_VERSION		NUMBER(19) 			NOT NULL,
	TAB_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM	INTEGER				NOT NULL,
	PRODUCT_ID		VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_TAB_PRD_PK PRIMARY KEY (ASSET_VERSION, TAB_ID, SEQUENCE_NUM)
);

CREATE TABLE CPS_TAB_CONTENT (
	ASSET_VERSION		NUMBER(19) 			NOT NULL,
	TAB_ID			VARCHAR2(40)		NOT NULL,
	SEQUENCE_NUM	INTEGER				NOT NULL,
	CONTENT_ID		VARCHAR2(40)		NOT NULL,
	CONSTRAINT CPS_TAB_CONTENT_PK PRIMARY KEY (ASSET_VERSION, TAB_ID, SEQUENCE_NUM)
);

