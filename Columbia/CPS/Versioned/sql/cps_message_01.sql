drop table CPS_USER_MESSAGE;

CREATE TABLE CPS_USER_MESSAGE(
    ASSET_VERSION                   NUMBER(19)      NOT NULL,
    WORKSPACE_ID                    VARCHAR2(40)    NOT NULL,
    BRANCH_ID                       VARCHAR2(40)    NOT NULL,
    IS_HEAD                         NUMBER(1)       NOT NULL,
    VERSION_DELETED                 NUMBER(1)       NOT NULL,
    VERSION_EDITABLE                NUMBER(1)       NOT NULL,
    PRED_VERSION                    NUMBER(19)      NULL,
    CHECKIN_DATE                    TIMESTAMP       NULL,
    ID                              VARCHAR2(100)    NOT NULL, 
    DISPLAY_NAME                    VARCHAR2 (254)  NOT NULL,
    MODULE_NAME                     VARCHAR2(254)   NOT NULL, 
    MESSAGE                         CLOB   NOT NULL, 
    MESSAGE_TYPE                    INTEGER,
	CONSTRAINT CPS_USER_MESSAGE_PK PRIMARY KEY(ID,DISPLAY_NAME,ASSET_VERSION)
);
CREATE INDEX CPS_USER_MESSAGE_WORKSPACE_ID ON CPS_USER_MESSAGE (WORKSPACE_ID); 
CREATE INDEX CPS_USER_MESSAGE_CHECKIN_ID ON CPS_USER_MESSAGE (CHECKIN_DATE);