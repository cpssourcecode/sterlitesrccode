alter table cps_b2b_user drop column active;
alter table cps_b2b_user drop column deActivatedDate;

alter table cps_user add active number(1,0) null;
alter table cps_user add deActivatedDate timestamp null;

alter table cps_contact_info add address_type number(3) null;
alter table cps_organization add outside_rep_code varchar2(100) null;

create table cps_b2b_sales_rep (
  user_id 			varchar2(40)	NOT NULL REFERENCES dps_user(id),
  outside_rep_code varchar2(100) NULL,
  CONSTRAINT cps_b2b_sales_rep_pk PRIMARY KEY(user_id)
);

create table cps_b2b_sales_rep_anc_org (
  user_id	varchar2(40)	not null,
  anc_org_id	varchar2(40)	not null,
  constraint cps_b2b_sales_rep_anc_org_p primary key (user_id, anc_org_id),
  constraint cps_b2b_sales_rep_anc_org_f foreign key (user_id) references dps_user(id),
  constraint cps_b2b_sales_rep_org_f foreign key (anc_org_id) references dps_organization(org_id)
);