ALTER TABLE CPS_STORES ADD DESCRIPTION CLOB NOT NULL;
ALTER TABLE CPS_STORES ADD LAT varchar2(24) NOT NULL;
ALTER TABLE CPS_STORES ADD LNG varchar2(24) NOT NULL;
ALTER TABLE CPS_STORES ADD NAME VARCHAR2(100) NOT NULL;