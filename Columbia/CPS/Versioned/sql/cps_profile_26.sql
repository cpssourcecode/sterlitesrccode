/*adding new column login1*/
ALTER TABLE cps_mail_subscriber ADD login1 VARCHAR2(255);

/*copying values from login column to login1*/
UPDATE cps_mail_subscriber SET login1 = login;

/*drop column login*/
ALTER TABLE cps_mail_subscriber DROP COLUMN login;

/*Creating login with 255 characters*/
ALTER TABLE cps_mail_subscriber ADD login VARCHAR2(255);

/*copying values from login1 to login*/
UPDATE cps_mail_subscriber SET login = login1;

/*drop column login1*/
ALTER TABLE cps_mail_subscriber DROP COLUMN login1;