drop table cps_stores;

create table cps_stores (
asset_version			INT		NOT NULL,
workspace_id			VARCHAR(40)	NOT NULL,
branch_id			VARCHAR(40)	NOT NULL,
is_head				NUMERIC(1)	NOT NULL,
version_deleted			NUMERIC(1)	NOT NULL,
version_editable		NUMERIC(1)	NOT NULL,
pred_version			INT		NULL,
checkin_date			TIMESTAMP	NULL,
  id                    VARCHAR2(40) NOT NULL,
  area                  VARCHAR2(100) NOT NULL,
  address               VARCHAR2(100) NOT NULL,
  city                  VARCHAR2(100) NOT NULL,
  state                 VARCHAR2(2)  NOT NULL,
  zip                   VARCHAR2(5)  NOT NULL,
  phone                 VARCHAR2(12) NOT NULL,
  open_time             TIMESTAMP(9)  NOT NULL,
  close_time            TIMESTAMP(9)  NOT NULL,
  timezone              VARCHAR2(3)   NOT NULL,
CONSTRAINT cps_stores_pk PRIMARY KEY(id, asset_version));

create table store_images (
	id	varchar2(40)	not null,
	sequence_num	integer	not null,
	media_id	varchar2(40)	not null
,constraint store_images_p primary key (id,sequence_num));
