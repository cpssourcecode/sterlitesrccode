drop table cps_regional_managers;

CREATE TABLE cps_regional_managers (
asset_version			INT				NOT NULL,
workspace_id			VARCHAR(40)		NOT NULL,
branch_id				VARCHAR(40)		NOT NULL,
is_head					NUMERIC(1)		NOT NULL,
version_deleted			NUMERIC(1)		NOT NULL,
version_editable		NUMERIC(1)		NOT NULL,
pred_version			INT				NULL,
checkin_date			TIMESTAMP		NULL,
id						varchar2(40)	NOT NULL,
image_uri				varchar2(254)	NULL,
name					varchar2(254)	NULL,
title					varchar2(254)	NULL,
localtion				varchar2(254)	NULL,
email					varchar2(254)	NULL,
phone					varchar2(254)	NULL,
description				CLOB 			NULL,
CONSTRAINT cps_regional_managers_pk PRIMARY KEY(ID,ASSET_VERSION));