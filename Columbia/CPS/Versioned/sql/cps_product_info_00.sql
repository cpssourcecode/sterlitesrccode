DROP TABLE cps_prod_info_category;
DROP TABLE cps_prod_info_item;
DROP TABLE cps_prod_info_item_cat_map;

CREATE TABLE cps_prod_info_category (
	asset_version	INT NOT NULL,
	workspace_id	VARCHAR(40) NOT NULL,
	branch_id		VARCHAR(40) NOT NULL,
	is_head			NUMERIC(1) NOT NULL,
	version_deleted	NUMERIC(1) NOT NULL,
	version_editable	NUMERIC(1) NOT NULL,
	pred_version	INT NULL,
	checkin_date	TIMESTAMP NULL,
	id				VARCHAR(40) NOT NULL,
	category_name	VARCHAR(100) NOT NULL,
	display_order	NUMERIC(3) NULL,
	CONSTRAINT cps_prod_info_category_pk PRIMARY KEY (id, asset_version)
);

CREATE TABLE cps_prod_info_item (
	asset_version	INT NOT NULL,
	workspace_id	VARCHAR(40) NOT NULL,
	branch_id		VARCHAR(40) NOT NULL,
	is_head			NUMERIC(1) NOT NULL,
	version_deleted	NUMERIC(1) NOT NULL,
	version_editable	NUMERIC(1) NOT NULL,
	pred_version	INT NULL,
	checkin_date	TIMESTAMP NULL,
	id				VARCHAR(40) NOT NULL,
	item_name			VARCHAR2(255) NOT NULL,
	item_url		VARCHAR(512) NOT NULL,
	CONSTRAINT cps_prod_info_item_pk PRIMARY KEY (id, asset_version)
);

CREATE TABLE cps_prod_info_item_cat_map (
	asset_version	INT NOT NULL,
	item_id		VARCHAR(40) NOT NULL,
	category_id	VARCHAR(40) NOT NULL,
	CONSTRAINT cps_prod_info_item_cat_map_pk PRIMARY KEY (item_id, category_id, asset_version)
);