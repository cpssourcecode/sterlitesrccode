alter table CPS_CREDIT_APP modify user_name varchar2(80);
alter table CPS_CREDIT_APP modify business_name varchar2(80);
alter table CPS_CREDIT_APP modify trade_name varchar2(80);
alter table CPS_CREDIT_APP modify business_type varchar2(80);
alter table CPS_CREDIT_APP modify contractors_license_num varchar2(50);