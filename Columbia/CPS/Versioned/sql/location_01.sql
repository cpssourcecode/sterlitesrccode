CREATE TABLE cps_store (
    ASSET_VERSION NUMBER(19) NOT NULL,
		location_id 			varchar2(40),
		region 		varchar2(100)	NULL,
    description 		clob	NULL,
    CONSTRAINT CPS_LOCATION_PK PRIMARY KEY (location_id, ASSET_VERSION)
);