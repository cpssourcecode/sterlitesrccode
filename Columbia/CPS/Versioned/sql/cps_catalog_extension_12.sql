-- drop table CPS_BRAND_ATTRIBUTES
-- drop table CPS_BRAND

CREATE TABLE CPS_BRAND (
    ASSET_VERSION                  NUMBER(19, 0)          NOT NULL,
    WORKSPACE_ID                   VARCHAR2(40)           NOT NULL,
    BRANCH_ID                      VARCHAR2(40)           NOT NULL,
    IS_HEAD                        NUMBER(1)              NOT NULL,
    VERSION_DELETED                NUMBER(1)              NOT NULL,
    VERSION_EDITABLE               NUMBER(1)              NOT NULL,
    PRED_VERSION                   NUMBER(19, 0)          NULL,
    CHECKIN_DATE                   TIMESTAMP              NULL,
    BRAND_ID                       VARCHAR2(40)           NOT NULL,
    BRAND_NAME                     VARCHAR2(254)          NULL,
    BRAND_DESCRIPTION              VARCHAR2(4000)         NULL,
    BRAND_IMAGE                    VARCHAR2(1024)         NULL,
    ITEM_ACL                       VARCHAR2(1024)         NULL,
    CONSTRAINT CPS_BRAND_PK PRIMARY KEY (ASSET_VERSION, BRAND_ID)
);

create index CPS_BRAND_WORKSPACE_ID on CPS_BRAND (WORKSPACE_ID); 
create index CPS_BRAND_CHECKIN_DATE on CPS_BRAND (CHECKIN_DATE);

CREATE TABLE CPS_BRAND_ATTRIBUTES (
    ASSET_VERSION                  NUMBER(19)             NOT NULL,
    BRAND_ID                       VARCHAR2(40)           NOT NULL,
    ATTRIBUTE_NAME                 VARCHAR2(800)          NOT NULL,
    ATTRIBUTE_VALUE                VARCHAR2(1024)         NOT NULL,
    CONSTRAINT CPS_BRAND_ATTRIBUTES_PK PRIMARY KEY (ASSET_VERSION, BRAND_ID, ATTRIBUTE_NAME)
);

