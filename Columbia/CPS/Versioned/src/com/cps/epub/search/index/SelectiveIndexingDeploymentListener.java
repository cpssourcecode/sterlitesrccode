package com.cps.epub.search.index;

import atg.deployment.common.event.DeploymentEvent;
import atg.epub.search.indexing.IndexingDeploymentListener;
import atg.naming.AbsoluteNameable;
import vsg.load.loader.impl.IDeploymentSource;

/**
 * This class extends IndexingDeploymentListener to ignore deployment events
 * when listener is not enabled or event's deployment belongs to deployment
 * source in escape list.
 * 
 * @author Kate Koshman
 */
public class SelectiveIndexingDeploymentListener extends IndexingDeploymentListener {
	
	/** The Enable listener. */
	private boolean mEnableListener = true;
	
	/**
	 * Checks if listener is enabled.
	 * 
	 * @return true, if listener is enabled
	 */
	public boolean isEnableListener() {
		return mEnableListener;
	}
	
	/**
	 * Sets enable flag for listener
	 * 
	 * @param pEnableListener
	 *            the new enable flag value
	 */
	public void setEnableListener(boolean pEnableListener) {
		mEnableListener = pEnableListener;
	}
	
	/** The Escape indexing for sources. */
	private IDeploymentSource[] mEscapeIndexingForSources;
	
	/**
	 * Gets the array of sources to escape indexing.
	 * 
	 * @return the array of sources to escape indexing
	 */
	public IDeploymentSource[] getEscapeIndexingForSources() {
		return mEscapeIndexingForSources;
	}
	
	/**
	 * Sets the escape indexing sources array.
	 * 
	 * @param pEscapeIndexingForSources
	 *            the new escape indexing sources array
	 */
	public void setEscapeIndexingForSources(IDeploymentSource[] pEscapeIndexingForSources) {
		mEscapeIndexingForSources = pEscapeIndexingForSources;
	}
	
	/*----------------------------------------------------------------------------------------------------------------*/

	@Override
	public void deploymentEvent(DeploymentEvent pEvent) {
		if (shouldIgnoreEvent(pEvent)) {
			vlogDebug("Ignore event");
		} else {
			super.deploymentEvent(pEvent);
		}
	}
	
	/**
	 * Check whether we should ignore event.
	 * 
	 * @param pEvent
	 *            the deployment event
	 * @return true, if should ignore
	 */
	protected boolean shouldIgnoreEvent(DeploymentEvent pEvent) {
		boolean ignoreEvent = false;
		if (!isEnableListener()) {
			vlogDebug("Escape deployment event {0}, cause listener is not enabled");
			ignoreEvent = true;
		} else
			if (getEscapeIndexingForSources() != null) {
				for (IDeploymentSource deploymentSource : getEscapeIndexingForSources()) {
					if (deploymentSource != null && deploymentSource.ownDeployment(pEvent)) {
						String sourceName = getSourceName(deploymentSource);
						vlogDebug("Escape deployment event {0}, cause it belongs to {1} in list.", pEvent, sourceName);
						ignoreEvent = true;
						break;
					}
				}
			}
		return ignoreEvent;
	}
	
	/**
	 * Gets the deployment source name.
	 * 
	 * @param pDeploymentSource
	 *            the deployment source
	 * @return the source name
	 */
	protected String getSourceName(IDeploymentSource pDeploymentSource) {
		String name;
		if (pDeploymentSource instanceof AbsoluteNameable) {
			name = ((AbsoluteNameable)pDeploymentSource).getAbsoluteName();
		} else {
			name = pDeploymentSource.toString();
		}
		return name;
	}
}
