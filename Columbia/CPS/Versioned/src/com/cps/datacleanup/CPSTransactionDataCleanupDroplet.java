/**
 * 
 */
package com.cps.datacleanup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.cps.csr.userprofiling.CPSCSRProfileTools;
import com.cps.util.RepositoryUtils;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistManager;
import atg.commerce.order.OrderManager;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.naming.ParameterName;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * @author R.Srinivasan
 * 
 *         A droplet to clean all the transactional data of test users before go-live. It will clear all the profile users data other than the userIds
 *         configured in the List property
 *
 */
public class CPSTransactionDataCleanupDroplet extends DynamoServlet {
    
    private Repository profileRepository;
    private Repository orderRepository;
    private Repository giftlistsRepository;
    private TransactionManager transactionManager;
    private OrderManager orderManager;
    private CPSCSRProfileTools profileTools;
    private List<String> ignoreProfileIds = new ArrayList<>();
    private RepositoryUtils repositoryUtils;
    private GiftlistManager giftlistsManager;
    private ParameterName NO_USERS_TO_DELETE = ParameterName.getParameterName("noUsersToDelete");
    private ParameterName ERROR = ParameterName.getParameterName("error");
    private ParameterName SUCCESS = ParameterName.getParameterName("success");
    private boolean testMode;

    /**
     * @return the testMode
     */
    public boolean isTestMode() {
        return testMode;
    }

    /**
     * @param testMode
     *            the testMode to set
     */
    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    /**
     * @return the giftlistsManager
     */
    public GiftlistManager getGiftlistsManager() {
        return giftlistsManager;
    }

    /**
     * @param giftlistsManager
     *            the giftlistsManager to set
     */
    public void setGiftlistsManager(GiftlistManager giftlistsManager) {
        this.giftlistsManager = giftlistsManager;
    }

    /**
     * @return the repositoryUtils
     */
    public RepositoryUtils getRepositoryUtils() {
        return repositoryUtils;
    }

    /**
     * @param repositoryUtils
     *            the repositoryUtils to set
     */
    public void setRepositoryUtils(RepositoryUtils repositoryUtils) {
        this.repositoryUtils = repositoryUtils;
    }

    /**
     * @return the ignoreProfileIds
     */
    public List<String> getIgnoreProfileIds() {
        return ignoreProfileIds;
    }

    /**
     * @param ignoreProfileIds
     *            the ignoreProfileIds to set
     */
    public void setIgnoreProfileIds(List<String> ignoreProfileIds) {
        this.ignoreProfileIds = ignoreProfileIds;
    }
    /**
     * @return the profileRepository
     */
    public Repository getProfileRepository() {
        return profileRepository;
    }
    /**
     * @param profileRepository the profileRepository to set
     */
    public void setProfileRepository(Repository profileRepository) {
        this.profileRepository = profileRepository;
    }
    /**
     * @return the orderRepository
     */
    public Repository getOrderRepository() {
        return orderRepository;
    }
    /**
     * @param orderRepository the orderRepository to set
     */
    public void setOrderRepository(Repository orderRepository) {
        this.orderRepository = orderRepository;
    }
    /**
     * @return the giftlistsRepository
     */
    public Repository getGiftlistsRepository() {
        return giftlistsRepository;
    }
    /**
     * @param giftlistsRepository the giftlistsRepository to set
     */
    public void setGiftlistsRepository(Repository giftlistsRepository) {
        this.giftlistsRepository = giftlistsRepository;
    }
    /**
     * @return the transactionManager
     */
    public TransactionManager getTransactionManager() {
        return transactionManager;
    }
    /**
     * @param transactionManager the transactionManager to set
     */
    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }
    /**
     * @return the orderManager
     */
    public OrderManager getOrderManager() {
        return orderManager;
    }
    /**
     * @param orderManager the orderManager to set
     */
    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }
    /**
     * @return the profileTools
     */
    public CPSCSRProfileTools getProfileTools() {
        return profileTools;
    }
    /**
     * @param profileTools the profileTools to set
     */
    public void setProfileTools(CPSCSRProfileTools profileTools) {
        this.profileTools = profileTools;
    }
    
    /**
     * This will fetch all the user profiles from the profileRepository and iterate through them and check whether the profielId is there in the ignorable list
     * if yes, it will be skipped. if not, it will be taken up for cleaning and all the transactional data of that profile would be cleaned up.
     **/

    @Override
    public void service(DynamoHttpServletRequest request, DynamoHttpServletResponse response) throws ServletException, IOException {
        boolean rollback = isTestMode(); // if test mode is true, this will always rollback
        boolean hasErrors = false;
        StringBuilder errorMsg = new StringBuilder();
        StringBuilder successMsg = new StringBuilder();
        StringBuilder skippedEmailsMsg = new StringBuilder();
        TransactionDemarcation td=new TransactionDemarcation();
        MutableRepository mutableProfileRepo=(MutableRepository) getProfileRepository();
        try {
            td.begin(getTransactionManager());
            try {

                RqlStatement allUsersQuery = RqlStatement.parseRqlStatement("ALL");
                RepositoryItem[] allUsers = getRepositoryUtils().executeQuery("user", profileRepository, allUsersQuery, new Object[] {});

                if (allUsers == null) {
                    vlogInfo("no users to delete");
                    request.serviceLocalParameter(NO_USERS_TO_DELETE, request, response);
                    return;
                }

                // Now cleaning up all the autoOrders by disabling them

                RepositoryItem[] autoOrders = getRepositoryUtils().executeQuery("autoOrder", orderRepository, "ALL", new Object[] {});

                if (autoOrders != null) {
                    successMsg.append("<BR> disabling auto orders ").append(autoOrders.toString());
                    for (RepositoryItem oneAutoOrder : autoOrders) {
                        MutableRepositoryItem mutableAutoOrder = (MutableRepositoryItem) oneAutoOrder;
                        mutableAutoOrder.setPropertyValue("enabled", Boolean.FALSE);
                        ((MutableRepository) orderRepository).updateItem(mutableAutoOrder);
                    }
                }

                for (RepositoryItem oneUser : allUsers) {
                    String oneUserId = oneUser.getRepositoryId();
                    String emailId = (String) oneUser.getPropertyValue("email");
                    if (ignoreProfileIds.contains(oneUserId)) {
                        vlogInfo("skipping emailid - {0} as it is there in the ignore list ", emailId);
                        skippedEmailsMsg.append(emailId).append(',');

                    } else {
                    vlogInfo("going to clean up the email - {0}", emailId);
                    successMsg.append("<BR> cleaning up email ").append(emailId);
                    RepositoryItem profile = getProfileTools().getItemFromEmail(emailId);

                    MutableRepositoryItem editableProfile = (MutableRepositoryItem) profile;
                    vlogInfo("cleaning up recentlyViewedProducts for the email - {0}", emailId);
                    editableProfile.setPropertyValue("recentlyViewedProducts", new ArrayList());


                    Collection giftLists = getGiftlistsManager().getGiftlistTools().getGiftlists(profile);                   
                    if (giftLists != null) {
                        vlogInfo("cleaning up material lists for the user - {0}", giftLists);
                        // now remove those giftlists associations with the profile.
                        editableProfile.setPropertyValue("wishlist", null);
                        editableProfile.setPropertyValue("giftlists", null);
                        editableProfile.setPropertyValue("otherGiftlists", null);
                        ((MutableRepository) getProfileRepository()).updateItem(editableProfile);
                        successMsg.append("<BR><BR>Material Lists for th email ").append(giftLists.toString());
                        for (Object obj : giftLists) {
                            RepositoryItem giftList = (RepositoryItem) obj;
                                if (giftList != null) {
                            MutableRepository giftsRepo = (MutableRepository) getGiftlistsRepository();
                            giftsRepo.removeItem(giftList.getRepositoryId(), "gift-list");
                                }
                        }
                    } else {
                        vlogInfo("no material lists for this user - {0}", emailId);
                    }



                  //  vlogInfo("cleaning up explicitly set billingAccounts for the email - {0}", emailId);
                 //   editableProfile.setPropertyValue("billingAccounts", new HashSet());
                   
                //    editableProfile.setPropertyValue("cpsMailings", new HashSet());
                    
                    //now fetching incomplete orders for that user
                    
                    String query = "profileId=?0 and state=?1";

                    Object[] params = new Object[2];
                    params[0] = oneUserId;
                    params[1] = "INCOMPLETE";
                    RepositoryItem[] ordersForThisProfile = getRepositoryUtils().executeQuery("order", getOrderRepository(), query, params);
                    if (ordersForThisProfile != null) {
                        successMsg.append("<BR> removing incomplete orders for this profile").append(ordersForThisProfile.toString());
                        vlogInfo("got incomplete orders for the user - {0}", ordersForThisProfile);
                        for (RepositoryItem oneOrder : ordersForThisProfile) {
                            String orderId = oneOrder.getRepositoryId();
                            getOrderManager().removeOrder(orderId);
                        }
                    } else {
                        vlogInfo("no incomplete orders for this user");
                    }
                        // get the list of users whose approvers list contains this user and remove this user from that list.

                        Object[] queryparams = new String[]{oneUserId};

                        RepositoryItem[] usersApprovedByThisUser = getRepositoryUtils().executeQuery("user", profileRepository, "approvers includes item (id=?0)", queryparams);
                        vlogInfo("users approved by this user - {0} " + usersApprovedByThisUser, oneUserId);
                        if(usersApprovedByThisUser!=null) {
                            for(RepositoryItem userApproved: usersApprovedByThisUser) {
                                vlogInfo("approved user is - {0}", userApproved.getRepositoryId());
                                MutableRepositoryItem updateableUser=(MutableRepositoryItem) userApproved;
                                List<RepositoryItem> approvers=(List<RepositoryItem> )updateableUser.getPropertyValue("approvers");
                                vlogInfo("approvers list is " + approvers);
                                vlogInfo(" {0} - approved user's approvers list  is -{1} ", userApproved.getRepositoryId(), approvers.toString());
                                approvers.remove(profile);
                                vlogInfo("approved user's approvers list after removal is - {0}", approvers);
                                updateableUser.setPropertyValue("approvers", new ArrayList());
                                mutableProfileRepo.updateItem(updateableUser);
                            }
                        }

                        mutableProfileRepo.removeItem(oneUserId, "user");
                }
                }
            }catch(RepositoryException re) {
                rollback = true;
                vlogError("unable to remove transactional data - {0}", re.getMessage());
                logError(re);
                errorMsg.append("<BR>").append(re.fillInStackTrace().toString());
                hasErrors = true;
            } catch (CommerceException ce) {
                rollback = true;
                vlogError("unable to remove incomplete orders for the user -  {0}", ce.getMessage());
                logError(ce);
                errorMsg.append("<BR>").append(ce.getMessage());
                hasErrors = true;
            } finally {
                td.end(rollback);
                errorMsg.append("transaction committed ").append(rollback);
                successMsg.append("<BR>transaction committed ").append(rollback);
            }
        } catch (TransactionDemarcationException tde) {
            vlogError("transaction demarcation exception occurred - {0}", tde.getMessage());
        }

        if (hasErrors) {
            request.setParameter("errorMessage", errorMsg.toString());
            request.serviceLocalParameter(ERROR, request, response);
        } else {
            successMsg.append(skippedEmailsMsg);
            request.setParameter("successMessage", successMsg.toString());
            request.serviceLocalParameter(SUCCESS, request, response);
        }
    }

}
