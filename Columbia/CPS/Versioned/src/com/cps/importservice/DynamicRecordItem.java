package com.cps.importservice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Shebi
 *
 */

@SuppressWarnings({ "PMD.UseConcurrentHashMap" })
public class DynamicRecordItem extends CatalogItem {

    private Map<String, Object> values = new HashMap<>();

    /**
     * @return the values
     */
    public Map<String, Object> getValues() {
        return values;
    }

    /**
     * @param values
     *            the values to set
     */
    public void setValues(Map<String, Object> values) {
        this.values = values;
    }

    /**
     * @param key
     * @param value
     */
    public void setRecordValue(String key, Object value) {
        values.put(key, value);
    }

    /**
     * @param key
     * @return
     */
    public Object getRecordValue(String key) {
        return values.get(key);
    }

    /**
     * simple helper method to return the value as a String so that no type casting is needed in the caller code
     *
     * @param key
     * @return
     */
    public String getRecordValueAsString(String key) {
        return values.get(key) == null ? null : values.get(key).toString();
        // return values.get(key).toString();
    }

    /**
     * utility method to remove the record item in case
     *
     * @param key
     */
    public void removeRecordValue(String key) {
        values.remove(key);
    }

    /**
     * override the toString method to return the map of values
     */
    @Override
    public String toString() {
        return values.toString();
    }

}
