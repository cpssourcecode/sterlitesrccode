package com.cps.importservice;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import atg.dtm.TransactionDemarcation;
import atg.epub.ProgramaticImportService;
import atg.epub.project.Process;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;

/**
 * This class reads Product Id and alt text from csv file and inserts into
 * database via BCC.
 * 
 * @author shebi
 */

public class CPSImportService extends ProgramaticImportService {

	private final static String PRODUCT = "product";
	protected MutableRepository productCatalog;
	protected Map<String, String> recordToItemPropertiesMap;
	protected String productId;
	protected String altText;
	protected String csvFile;
	private boolean testMode;
	private String itemIdParamName;

	@Override
	public void importUserData(Process pProcess, TransactionDemarcation pTD) throws Exception {

		if (isLoggingInfo())
			logInfo("[ProgrammaticImportService.importUserData()] Here!!! ");

		if (isTestMode()) {
			MutableRepositoryItem item = getProductCatalog().getItemForUpdate(productId, PRODUCT);
			item.setPropertyValue("altText", getAltText());
			this.getProductCatalog().updateItem(item);
		} else {
			// read the products from the csv file and update the values
			try {
				CSVParser csvParser = getParser();

				if (csvParser == null) {
					vlogError("the parser is null. Feed file not found");
				}
				vlogDebug("csv parser :: {0}", csvParser.getRecordNumber());
				List<DynamicRecordItem> dynamicRecordListFull = populateDynamicRecordItems(csvParser);
			}

			catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
	}

	/**
	 *
	 * @return CSV parser object representing the input CSV File.
	 */
	public CSVParser getParser() {
		File feedFile = new File(getCsvFile());

		CSVFormat format = CSVFormat.TDF.withQuote(null).withFirstRecordAsHeader().withIgnoreHeaderCase()
				.withIgnoreEmptyLines().withIgnoreSurroundingSpaces();
		CSVParser parser = null;

		try {
			parser = new CSVParser(new FileReader(feedFile), format);
		} catch (IOException ex) {
			logError(ex);
		}
		return parser;
	}

	protected List<DynamicRecordItem> populateDynamicRecordItems(CSVParser csvParser) {

		try {
			Map additionalRelatedValues = prePopulateRelatedValuesNotInFeed();
		} catch (FeedProcessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<DynamicRecordItem> dynamicRecordsItemList = new ArrayList<>();
		boolean allRecordsValid = true;

		try {
			for (CSVRecord record : csvParser.getRecords()) {
				vlogDebug("CSVRecord {0}", record);
				DynamicRecordItem recordItem = instantiateDynamicRecordItem(record);
				MutableRepositoryItem itemForUpdate = null;
				if (recordItem != null) {
					itemForUpdate = (MutableRepositoryItem) getProductCatalog().getItem(recordItem.getItemId(),
							PRODUCT);
					if (itemForUpdate != null) {
						String altTextVal = recordItem.getRecordValueAsString("altText");
						if (altTextVal.length() > 128) {
							altTextVal = altTextVal.substring(0, 128);
							itemForUpdate.setPropertyValue("altText", altTextVal);
						} else {
							itemForUpdate.setPropertyValue("altText", altTextVal);
						}
						this.getProductCatalog().updateItem(itemForUpdate);
						vlogDebug("Product Id : {0} and Alt Text : {1}", recordItem.getItemId(),
								recordItem.getRecordValueAsString("altText"));
					} else {
						vlogInfo("Product {0} NOT FOUND", recordItem.getItemId());
					}
				}
				dynamicRecordsItemList.add(recordItem);
			}

			// add additional dynamic record items itself to the list.

		} catch (NumberFormatException | IOException ex) {
			vlogError("Error while getting parser records :: for record {0}  ", ex.getMessage());
			logError(ex);
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return dynamicRecordsItemList;
	}

	protected Map prePopulateRelatedValuesNotInFeed() throws FeedProcessException {
		vlogDebug("nothing to add additionally. default implementation");

		return null;

	}

	private DynamicRecordItem instantiateDynamicRecordItem(CSVRecord record) {

		vlogInfo("inside instantiateDynamicRecordItem {0}", record);

		Set recordHeadersSet = recordToItemPropertiesMap.keySet();

		Iterator<String> recordHeaderIter = recordHeadersSet.iterator();
		DynamicRecordItem recordItem = new DynamicRecordItem();
		while (recordHeaderIter.hasNext()) {
			String recordKey = recordHeaderIter.next();
			String repoItemPropName = recordToItemPropertiesMap.get(recordKey);
			recordItem.setRecordValue(repoItemPropName,
					org.apache.commons.lang.StringUtils.trim(record.get(recordKey)));
			recordItem.setRecordIndex(record.getRecordNumber());
		}
		recordItem.setItemId(record.get(getItemIdParamName()));
		return recordItem;
	}

	/**
	 * @return the productCatalog
	 */
	public MutableRepository getProductCatalog() {
		return productCatalog;
	}

	/**
	 * @param productCatalog
	 *            the productCatalog to set
	 */
	public void setProductCatalog(MutableRepository productCatalog) {
		this.productCatalog = productCatalog;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the altText
	 */
	public String getAltText() {
		return altText;
	}

	/**
	 * @param altText
	 *            the altText to set
	 */
	public void setAltText(String altText) {
		this.altText = altText;
	}

	/**
	 * @return the csvFile
	 */
	public String getCsvFile() {
		return csvFile;
	}

	/**
	 * @param csvFile
	 *            the csvFile to set
	 */
	public void setCsvFile(String csvFile) {
		this.csvFile = csvFile;
	}

	/**
	 * @return the testMode
	 */
	public boolean isTestMode() {
		return testMode;
	}

	/**
	 * @param testMode
	 *            the testMode to set
	 */
	public void setTestMode(boolean testMode) {
		this.testMode = testMode;
	}

	/**
	 * @return the recordToItemPropertiesMap
	 */
	public Map<String, String> getRecordToItemPropertiesMap() {
		return recordToItemPropertiesMap;
	}

	/**
	 * @param recordToItemPropertiesMap
	 *            the recordToItemPropertiesMap to set
	 */
	public void setRecordToItemPropertiesMap(Map<String, String> recordToItemPropertiesMap) {
		this.recordToItemPropertiesMap = recordToItemPropertiesMap;
	}

	/**
	 * @return the itemIdParamName
	 */
	public String getItemIdParamName() {
		return itemIdParamName;
	}

	/**
	 * @param itemIdParamName
	 *            the itemIdParamName to set
	 */
	public void setItemIdParamName(String itemIdParamName) {
		this.itemIdParamName = itemIdParamName;
	}

}
