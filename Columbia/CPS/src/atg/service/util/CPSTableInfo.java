package atg.service.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class extends oob TableInfo.
 * Placed in atg.service.util package since inner TableInfo.Column has default access modifier.
 */

public class CPSTableInfo extends TableInfo {

    public void setAdditionalColumns(List<String> pAdditionalAttributes) {

        Column[] columns = getTableColumns();
        List<Column> columnsList = new ArrayList<>(Arrays.asList(columns));

        for (String attrName: pAdditionalAttributes) {
            boolean columnExist = false;
            for (Column column : columnsList) {
                String cName = column.getName();
                if (cName.equals(attrName)) {
                    columnExist = true;
                }
            }
            if (!columnExist) {
                Column col = new Column(attrName, attrName, "product.attributeValueMap");
                columnsList.add(col);
            }
        }
        mColumns = columnsList.toArray(columns);
    }

    public void clearTableColumns() {
        int sizeTableInfo = this.mTableInfo.size();
        mColumns = Arrays.copyOfRange(mColumns, 0, sizeTableInfo);
    }
}
