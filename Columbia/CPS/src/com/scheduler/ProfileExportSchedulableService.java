package com.scheduler;

import javax.transaction.TransactionManager;

import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.ProfileExportManager;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

public class ProfileExportSchedulableService extends GenericService implements Schedulable{

	private final static String SCHEDULER_NAME = "Export Profiles Scheduler";
	private final static String B2B_USER_ITEM_DESCRIPTOR = "b2b-user";
	private final static String B2B_USER_TYPE = "2";
	private final static String QUERY_STMT = "exportedFlag = ?0 AND userType = ?1";
	
	private Scheduler scheduler;
	private Schedule schedule;
	private CPSProfileTools profileTools;
	private ProfileExportManager profileExportManager;
	int jobId;
	int maxProfilesPerTransaction;
	
	public int getMaxProfilesPerTransaction() {
		return maxProfilesPerTransaction;
	}

	public void setMaxProfilesPerTransaction(int maxProfilesPerTransaction) {
		this.maxProfilesPerTransaction = maxProfilesPerTransaction;
	}

	int maxProfilesPerQuery;
	
	private TransactionManager transactionManager;
	
	public TransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void doStartService() throws ServiceException {
		ScheduledJob job = new ScheduledJob(SCHEDULER_NAME, SCHEDULER_NAME,
				getAbsoluteName(), getSchedule(), this,
				ScheduledJob.REUSED_THREAD);

		if (isLoggingDebug())
			logDebug("ProfileExportSchedulableService start................");

		jobId = getScheduler().addScheduledJob(job);
	}

	public void doStopService() {
		getScheduler().removeScheduledJob(jobId);
	}

	public void performScheduledTask(Scheduler scheduler, ScheduledJob job) {
		try {
			
			if (isLoggingDebug())
				logDebug("Export Profile Scheduler start................");
			int success = runService();

			if (isLoggingDebug())
				logDebug("Export Profile Scheduler complete  with success========" + success);
		} catch (Exception ex) {
			logError(ex);
		}
	}

	public int runService() {

		TransactionDemarcation td = new TransactionDemarcation();
		int success = 0;
		int profileCount = 0;
		try {
			MutableRepository mRepository = getProfileTools().getProfileRepository();
			RepositoryView rpView = mRepository.getView(B2B_USER_ITEM_DESCRIPTOR);
			RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_STMT);
			Object params[] = new Object[2];
			params[0] = false;
			params[1] = B2B_USER_TYPE;
			RepositoryItem[] rsItems  = statement.executeQuery(rpView, params);
			//RepositoryItem[] rsItems = rpView.executeQuery(query, 0, getMaxProfilesPerQuery());
			
			int maxProfileCount = getMaxProfilesPerTransaction();
			int maxCount = 0;
			if( rsItems != null ) {
				if (isLoggingDebug()){
					logDebug("number of profiles returned by query=" + rsItems.length);
				}
				td.begin(getTransactionManager(), 3);
				getProfileExportManager().createSpreadsheetForExport(rsItems);
				for (RepositoryItem rsItem : rsItems) {
					if(++maxCount >= maxProfileCount){
						td.end();
						td.begin(getTransactionManager(), 3);
						maxCount = 0;
					}
					profileCount++;
					success = 1;
				}
				td.end();
			}

		} catch (Exception ex) {
			if (isLoggingError())
				logError(ex);
		} finally {
			try {
				if ( getTransactionManager() != null)
					td.end();
			} catch (TransactionDemarcationException e) {
				if(isLoggingError()){
					logError(e);
				}
			}
		}
		if (isLoggingDebug()){
			logDebug("profileCount=" + profileCount);
		}

		return success;
	}
	
	
	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	public ProfileExportManager getProfileExportManager() {
		return profileExportManager;
	}

	public void setProfileExportManager(ProfileExportManager profileExportManager) {
		this.profileExportManager = profileExportManager;
	}
}
