package com.scheduler;

import static com.cps.email.CPSEmailConstants.*;


import java.util.Set;

import javax.transaction.TransactionManager;

import com.cps.userprofiling.CPSProfileTools;
import com.cps.email.CommonEmailSender;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

public class EmailSchedulableService extends GenericService implements Schedulable {

	private final static String USER_ITEM_DESCRIPTOR = "user";
	private final static String SCHEDULER_NAME = "Email Sending Scheduler";
	//	private final static String CPS_MAILINGS = "cpsMailings";
	private final static String MAILING_ITEM_DECRIPTOR = "mailing";
	//	private final static String IS_SEND_EMAIL = "isSendEmail";
	private final static String RQL = "isSendEmail = ?0";
	private final static String CPS_VETT_COMPLETE = "cpsVettComplete";
	private final static String CPS_VETT_COMPLETE_SENT = "cpsVettCompleteEmailSent";
	private final static String VETTED_EMAIL_RQL = "cpsVettComplete = ?0 and cpsVettCompleteEmailSent = ?1";

	private Scheduler mScheduler;
	private Schedule mSchedule;
	private String mJobName;
	private String mJobDescription;
	private boolean mEnableJob;
	private CPSProfileTools mProfileTools;
	private int jobId;
	private TransactionManager transactionManager;
	private CommonEmailSender emailSender;
	int maxProfilesPerQuery;
	int maxProfilesPerTransaction;

	public int getMaxProfilesPerTransaction() {
		return maxProfilesPerTransaction;
	}

	public void setMaxProfilesPerTransaction(int maxProfilesPerTransaction) {
		this.maxProfilesPerTransaction = maxProfilesPerTransaction;
	}


	public Scheduler getScheduler() {
		return mScheduler;
	}

	public void setScheduler(Scheduler pScheduler) {
		mScheduler = pScheduler;
	}

	public Schedule getSchedule() {
		return mSchedule;
	}

	public void setSchedule(Schedule pSchedule) {
		mSchedule = pSchedule;
	}

	public String getJobName() {
		return mJobName;
	}

	public void setJobName(String pJobName) {
		mJobName = pJobName;
	}

	public String getJobDescription() {
		return mJobDescription;
	}

	public void setJobDescription(String pJobDescription) {
		mJobDescription = pJobDescription;
	}

	public boolean isEnableJob() {
		return mEnableJob;
	}

	public void setEnableJob(boolean pEnableJob) {
		mEnableJob = pEnableJob;
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int pJobId) {
		jobId = pJobId;
	}


	public void doStartService() throws ServiceException {
		ScheduledJob job = new ScheduledJob(SCHEDULER_NAME, SCHEDULER_NAME,
				getAbsoluteName(), getSchedule(), this,
				ScheduledJob.REUSED_THREAD);
		jobId = getScheduler().addScheduledJob(job);
	}

	public void doStopService() {
		getScheduler().removeScheduledJob(jobId);
	}

	public void performScheduledTask(Scheduler scheduler, ScheduledJob job) {
		try {
			vlogDebug("Email Sending Scheduler start");

			if (!isEnableJob()) {
				vlogDebug("Email Sending Scheduler not enabled. Not started. Exiting.");
				return;
			}
			int success = runService();

			vlogDebug("Email Sending Scheduler complete  with success=" + success);
		} catch (Exception ex) {
			logError(ex);
		}
	}


	@SuppressWarnings("unchecked")
	public int runService() {
		vlogDebug("runService(): start.");
		TransactionDemarcation td = new TransactionDemarcation();
		int success = 0;
		int profileCount = 0;
		try {
			MutableRepository mRepository = getProfileTools().getProfileRepository();
			Object params[] = new Object[1];
			params[0] = true;

			Object params2[] = new Object[2];
			params2[0] = true;
			params2[1] = false;

			RepositoryItem[] rsItems = queryRepository(params, RQL, mRepository);
			RepositoryItem[] importedProfiles = queryRepository(params2, VETTED_EMAIL_RQL, mRepository);

			vlogDebug("runService(): rsItems query run");

			int maxProfileCount = getMaxProfilesPerTransaction();
			int maxCount = 0;
			if (rsItems != null || importedProfiles != null) {

				td.begin(getTransactionManager(), 3);
				if (rsItems != null) {

					vlogDebug("number of profiles returned by query=" + rsItems.length);

					for (RepositoryItem rsItem : rsItems) {
						Set<RepositoryItem> emailItems = (Set<RepositoryItem>) rsItem.getPropertyValue(EMAIL_CPS_MAILINGS);
						MutableRepositoryItem profileItem = (MutableRepositoryItem) rsItem;

						if (emailItems != null && !emailItems.isEmpty()) {
							for (RepositoryItem emailItem : emailItems) {
								boolean sendSuccess = getEmailSender().sendBatchEmailNotification(emailItem, profileItem);
								if (sendSuccess) {
									mRepository.removeItem(emailItem.getRepositoryId(), MAILING_ITEM_DECRIPTOR);
									emailItems.remove(emailItem);
								}
							}
							vlogDebug("emailItems=" + emailItems);

							if (emailItems == null || emailItems.isEmpty()) {
								profileItem.setPropertyValue(EMAIL_CPS_MAILINGS, null);
								profileItem.setPropertyValue(EMAIL_IS_SEND_EMAIL, false);
							} else {
								profileItem.setPropertyValue(EMAIL_CPS_MAILINGS, emailItems);
							}

						} else {
							profileItem.setPropertyValue(EMAIL_IS_SEND_EMAIL, false);
						}

						if (++maxCount >= maxProfileCount) {
							td.end();
							td.begin(getTransactionManager(), 3);
							maxCount = 0;
						}
						profileCount++;
						success = 1;
					}
				}
				if (importedProfiles != null) {
					for (RepositoryItem importedProfile : importedProfiles) {
						boolean sendSuccess = getEmailSender().sendCheckoutAvailableEmail(importedProfile);
						MutableRepositoryItem importedProfileItem = (MutableRepositoryItem) importedProfile;
						//updating profile properties
						importedProfileItem.setPropertyValue(CPS_VETT_COMPLETE, false);
						importedProfileItem.setPropertyValue(CPS_VETT_COMPLETE_SENT, true);
						mRepository.updateItem(importedProfileItem);
					}
				}
				td.end();
			}

		} catch (Exception ex) {
			if (isLoggingError())
				logError(ex);
		} finally {
			try {
				if (getTransactionManager() != null)
					td.end();
			} catch (TransactionDemarcationException e) {
				if(isLoggingError()){
					logError(e);
				}
			}
		}
		if (isLoggingDebug()) {
			vlogDebug("profileCount=" + profileCount);
		}
		vlogDebug("runService(): end.");
		return success;
	}


	protected RepositoryItem[] queryRepository(Object params[], String rql, MutableRepository mRepository) {
		RepositoryView rpView;
		RepositoryItem[] rItems = null;
		try {
			rpView = mRepository.getView(USER_ITEM_DESCRIPTOR);
			RqlStatement statement = RqlStatement.parseRqlStatement(rql);
			rItems = statement.executeQuery(rpView, params);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException " + e);
			}
		}
		return rItems;
	}


	public TransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(TransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	public CommonEmailSender getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(CommonEmailSender emailSender) {
		this.emailSender = emailSender;
	}

}
