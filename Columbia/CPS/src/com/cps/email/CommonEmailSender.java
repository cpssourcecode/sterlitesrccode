package com.cps.email;

import static com.cps.email.CPSEmailConstants.PARAM_PO_PART;
import static com.cps.util.CPSConstants.PRODUCTION_URL;
import static com.cps.util.CPSConstants.SITE_ID;

import java.io.File;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.transaction.TransactionManager;

import org.apache.commons.collections.CollectionUtils;

import com.cps.commerce.order.CPSInStorePickupShippingGroup;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.multisite.CPSSiteURLManager;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.userprofiling.credit.application.pojo.UserCreditApplication;
import com.cps.util.CPSConstants;
import com.cps.util.CPSGlobalProperties;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InvoiceRequest;
import atg.commerce.order.Order;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroupImpl;
import atg.commerce.pricing.OrderPriceInfo;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.userprofiling.PropertyManager;
import atg.userprofiling.email.HtmlContentProcessor;
import atg.userprofiling.email.TemplateEmailBatchPersister;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;
import vsg.common.message.IMessageUtils;
import vsg.core.util.DataUtils;

public class CommonEmailSender extends GenericService {

    private TemplateEmailSender templateEmailSender;
    private TemplateEmailBatchPersister templateEmailBatchPersister;
    private CPSProfileTools profileTools;
    private IMessageUtils messageUtils;
    private TransactionManager mTransactionManager;

    private String templateCreateProfileURL;
    private String templateUpdateProfileURL;
    private String templateContactUsURL;
    private String templateContactUsCPSURL;
    private String templateCheckoutAvailable;
    private String templateApplicationCreditURL;

    private String templateAbandonedCartURL;
    private String templateAdminAddedUserURL;
    private String templateCPSRegionalManagerContactUs;
    private String templateCPSServicesContactUs;
    private String templateCPSDeactivatedUserURL;
    private String templateNewUserWelcomeURL;
    private String templateOrderApprovalExpiredURL;
    private String templateOrderApprovalRequiredURL;
    private String mTemplateOrderRequestReceivedURL;
    private String templateOrderApprovalPendingURL;
    private String templateOrderApprovedURL;
    private String templateOrderConfirmationURL;
    private String templateOrderRejectedURL;
    private String templateOrderRejectedNoCommentsURL;
    private String templatePasswordResetConfirmationURL;
    private String templatePasswordResetLinkURL;
    private String templatePriceListUpdateConfirmationURL;
    private String templateQuoteRequestedURL;
    private String templateShareMaterialListURL;
    private String templateSharePageURL;
    private String templateTemporaryPasswordURL;
    private String templateTestimonialURL;
    private String templateUserProfileUpdatedURL;
    private String templateEmailConfirmation;
    private String templateNewUserRegistered;
    private String templateAccountActivated;
    private String templateUserTestimonial;
    private String templateUserPriceListUpdateConfirmation;
    private String templateUserRegionalManagerContactUs;
    private String templateUserQuoteRequested;
    private String templateUserRequestCurrentStatementCopy;
    private String templateUserSavedCartReminder;
    private String templateOrderUpdateFailed;
    private String templateRequestAccess;
    private String templateRequestCurrentStatementCopy;
    private String templatePDPErrorURL;
    private String templateNewUserReport;
    private String templateNewUserNotActivatedReport;
    private String templateReminderConfirmationEmail;
    private String templateOrderConfirmationAdminURL;
    private String templateMissingWebOrderURL;
    private String templateAbandonedOrderNotificationURL;
    private String templateSalesOrderReport;

    private String emailFrom;
    private String siteId;
    private String emailToCPSService;
    private String emailToCPSQuoteRequest;
    private String emailToCPSPriceListUpdateRequest;
    private String emailToCPSTestimonial;
    private String emailToCPSRegionalManagerOverride;
    private String emailToCPSAdmin;
    private List<String> emailToCPSMarketing;
    private String currencyLocale;
    private String currencyCountry;
    private String subjectLineTruncateRegex;
    private Long subjectLineMaxLength;
    private String emailToCreateAccount;
    private String requestAccessEmail;
    private String requestStatementEmail;
    private boolean requestAccessSendCopyToApprover = false;

    private Map<String, String> cpsEmailAddresses;
    private boolean mTest = true;
    private List allEmailsType;
    private String emailToCPSPdpError;
    private String defaultWorkGroupMail;
    private String templateFeedImport;
    private String emailForFeedStatusmails;
    private String anonymousUserName;
    private String anonymousUserEmail;
    private CPSGlobalProperties cpsGlobalProperties;
    List<String> jsonFeedStatusEmails;
    List<String> priceFeedStatusEmails;
    List<String> addressLoaderStatusEmails;

    public List<String> getAddressLoaderStatusEmails() {
        return addressLoaderStatusEmails;
    }

    public void setAddressLoaderStatusEmails(List<String> addressLoaderStatusEmails) {
        this.addressLoaderStatusEmails = addressLoaderStatusEmails;
    }

    /**
     * for test purposes only
     *
     * @param fName
     * @param lName
     * @param email
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public boolean sendTestEmail(String fName, String lName, String email, String siteId) {
        boolean flag = true;
        try {

            if (isLoggingDebug()) {
                logDebug("sendTestEmail..................email...." + email);
                logDebug("fName......................" + fName);
                logDebug("lName......................" + lName);
            }
            List recipients = new ArrayList();
            recipients.add(email);
            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.FIRST_NAME, fName);
            emailParams.put(CPSEmailConstants.LAST_NAME, lName);
            flag = sendEmailNotification(recipients, emailParams, "/email/template.jsp", CPSEmailConstants.TEST_EMAIL_SUBJECT);

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

    /**
     * Sends out post registration email
     *
     * @param pProfile
     * @return
     */
    public boolean sendRegistrationEmail(RepositoryItem pProfile) {
        boolean flag = true;
        try {
            PropertyManager pmgr = getPropertyManager();
            String email = (String) pProfile.getPropertyValue(pmgr.getEmailAddressPropertyName());
            String name = (String) pProfile.getPropertyValue(pmgr.getFirstNamePropertyName());
            if (isLoggingDebug()) {
                logDebug("sendRegistrationEmail..................email...." + email);
                logDebug("getTemplateCreateProfileURL()......................" + getTemplateCreateProfileURL());
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.REGISTER_EMAIL);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, name);
            populateRealTimeParams(emailParams, pProfile);
            List recipients = new ArrayList();
            recipients.add(getEmailToCreateAccount());
            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(email);

            sendEmailNotification(recipients, emailParams, getTemplateEmailConfirmation(), CPSEmailConstants.EMAIL_NAME_REGISTRATION);
            flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateCreateProfileURL(), CPSEmailConstants.EMAIL_NAME_REGISTRATION);

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

    /**
     * Sends out profile update email
     *
     * @param pProfile
     * @return
     */
    public boolean sendUpdateEmail(RepositoryItem pProfile) {
        boolean flag = true;
        try {

            PropertyManager pmgr = getPropertyManager();
            String email = (String) pProfile.getPropertyValue(pmgr.getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendUpdateEmail..................email...." + email);
                logDebug("getTemplateCreateProfileURL()......................" + getTemplateCreateProfileURL());
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            populateRealTimeParams(emailParams, pProfile);
            List recipients = new ArrayList();
            recipients.add(email);
            flag = sendEmailNotification(recipients, emailParams, getTemplateUpdateProfileURL(), CPSEmailConstants.EMAIL_NAME_USER_UPDATE);

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

    /**
     * Method to send an email with confirmation link to newly registered user
     *
     * @param pProfile
     * @param confirmURL
     * @return
     */
    public boolean sendRegisterEmailConfirmation(RepositoryItem pProfile, String confirmURL) {
        boolean flag = true;
        try {

            PropertyManager pmgr = getPropertyManager();
            String email = (String) pProfile.getPropertyValue(pmgr.getEmailAddressPropertyName());
            String name = (String) pProfile.getPropertyValue(pmgr.getFirstNamePropertyName());
            if (isLoggingDebug()) {
                logDebug("Sending activation letter to: " + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            populateRealTimeParams(emailParams, pProfile);
            emailParams.put(CPSEmailConstants.PARAM_CONFIRMATION_LINK, getSiteUrl(siteId) + confirmURL);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, name);
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, email);
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.REGISTER_EMAIL);
            List recipients = new ArrayList();
            recipients.add(getEmailToCreateAccount());
            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(email);

            String cpsSubject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_NAME_EMAIL_CONFIRMATION), emailParams);
            // SM-129 New User account confirmations are routing to helpdesk instead of the customer
            // sendEmailNotification(recipients, emailParams, getTemplateEmailConfirmation(), CPSEmailConstants.EMAIL_NAME_EMAIL_CONFIRMATION);
            flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateEmailConfirmation(), cpsSubject);

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

    /**
     * Method to send notification to CustAccAdmin about new user registration
     *
     * @param createdUser
     * @param organizationAdmins
     * @return
     */
    public boolean sendNewUserRegistered(RepositoryItem createdUser, Set<RepositoryItem> organizationAdmins) {
        if (isLoggingDebug()) {
            logDebug("Sending notifications about new user");
        }
        boolean flag = true;
        try {
            PropertyManager pmgr = getPropertyManager();
            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            populateRealTimeParams(emailParams, createdUser);
            List recipients = new ArrayList();
            for (Object currentAdmin : organizationAdmins) {
                if (currentAdmin instanceof RepositoryItem) {
                    RepositoryItem organizationAdmin = (RepositoryItem) currentAdmin;
                    if (organizationAdmin.getRepositoryId().equals(createdUser.getRepositoryId())) {
                        continue;
                    }

                    if (isLoggingDebug()) {
                        logDebug("Sending activation letter to: " + organizationAdmin.getPropertyValue(pmgr.getEmailAddressPropertyName()));
                    }
                    recipients.add(organizationAdmin.getPropertyValue(pmgr.getEmailAddressPropertyName()));
                }
            }
            flag = sendEmailNotification(recipients, emailParams, getTemplateNewUserRegistered(), CPSEmailConstants.EMAIL_NAME_NEW_USER_REGISTERED);

        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    /**
     * Method to notify CPSAdmin about account activation (first user registered using account)
     *
     * @param createdUser
     * @return
     */
    public boolean sendAccountActivated(RepositoryItem createdUser) {
        if (isLoggingDebug()) {
            logDebug("Sending notifications about new account activation");
        }
        boolean flag = true;
        try {
            PropertyManager pmgr = getPropertyManager();
            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            populateRealTimeParams(emailParams, createdUser);
            List recipients = new ArrayList();
            recipients.add(getEmailToCPSAdmin());
            flag = sendEmailNotification(recipients, emailParams, getTemplateAccountActivated(), CPSEmailConstants.EMAIL_NAME_ACCOUNT_ACTIVATED);

        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    /**
     * Order confirmation email. First system will try to send email in real time if not success - saving for batch email process
     *
     * @param profile
     * @param order
     * @return
     */
    public boolean sendOrderConfirmationEmail(RepositoryItem profile, Order order, String siteId) {
        boolean flag = true;
        List recipients = new ArrayList();
        Set<String> notificationEmails = null;
        try {
            PropertyManager pmgr = getPropertyManager();
            String email = (String) profile.getPropertyValue(pmgr.getEmailAddressPropertyName());
            RepositoryItem billingAddress = (RepositoryItem) profile.getPropertyValue(CPSConstants.BILLING_ADDRESS);
            if(billingAddress !=null){
            notificationEmails= (Set<String>) billingAddress.getPropertyValue("orderNotificationEmails");
            }
            if (isLoggingDebug()) {
                logDebug("sendOrderConfirmationEmail..................email...." + email);
                logDebug("sendOrderConfirmationEmail..................order...." + order);
                logDebug("siteId....................." + siteId);
            }

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.ORDER_ID, ((CPSOrderImpl) order).getWebOrderId());
            emailParams.put(CPSEmailConstants.ORDER_OBJ, order);
            emailParams.put(CPSEmailConstants.PARAM_ORDER, order);
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) order).getWebOrderId());
            emailParams.put(CPSEmailConstants.PARAM_ORDER_DATE, sdf.format(order.getSubmittedDate()));
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER_ITEMS, order.getCommerceItems().size());

            String poNumber = null;
            List<PaymentGroup> paymentGroups = order.getPaymentGroups();
            if (paymentGroups != null && paymentGroups.size() > 0) {
                for (PaymentGroup paymentGroup : paymentGroups) {
                    if (paymentGroup instanceof InvoiceRequest) {
                        InvoiceRequest invoiceRequest = (InvoiceRequest) paymentGroup;
                        poNumber = invoiceRequest.getPONumber();
                        break;
                    }
                }
            }
            emailParams.put(CPSEmailConstants.PARAM_ORDER_PO_NUMBER, poNumber);

            String poNumberPart = "";

            if (poNumber != null) {
                Map<String, Object> poPartParams = new HashMap<>();
                poPartParams.put(CPSEmailConstants.PARAM_ORDER_PO_NUMBER, poNumber);

                poNumberPart = DataUtils.paramsMessage(messageUtils.getMessage("emailBodyOrderConfirmationPONumberPart"), poPartParams);
            }
            emailParams.put(PARAM_PO_PART, poNumberPart);

            List shippingGroups = order.getShippingGroups();

            if (shippingGroups != null && shippingGroups.size() > 0) {
                ShippingGroupImpl sg = (ShippingGroupImpl) shippingGroups.get(0);

                if (sg instanceof HardgoodShippingGroup) {
                    ContactInfo address = (ContactInfo) ((HardgoodShippingGroup) sg).getShippingAddress();
                    if (address != null) {
                        emailParams.put(CPSEmailConstants.PARAM_SHIPTO_OR_PICKUP, "Ship to Address: ");
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_1, address.getAddress1());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_2, getAddress2ForOrderConfirm(address.getAddress2(), address.getAddress3()));
                        // emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_3, address.getAddress3());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_CITY, address.getCity());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_STATE, address.getState());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_ZIP, address.getPostalCode());
                    }
                } else if (sg instanceof CPSInStorePickupShippingGroup) {
                    RepositoryItem store = ((CPSInStorePickupShippingGroup) sg).getStore();

                    if (store != null) {
                        emailParams.put(CPSEmailConstants.PARAM_SHIPTO_OR_PICKUP, "Pick up in store: ");
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_1, store.getPropertyValue(CPSConstants.ADDRESS1));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_2, getAddress2ForOrderConfirm(
                                        (String) store.getPropertyValue(CPSConstants.ADDRESS2), (String) store.getPropertyValue(CPSConstants.ADDRESS3)));
                        // emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_3, store.getPropertyValue(CPSConstants.ADDRESS3));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_CITY, store.getPropertyValue(CPSConstants.CITY));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_STATE, store.getPropertyValue(CPSConstants.STATE_ADDRESS));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_ZIP, store.getPropertyValue(CPSConstants.POSTAL_CODE));
                    }

                }

            }

            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(new Locale(getCurrencyLocale(), getCurrencyCountry()));
            OrderPriceInfo priceInfo = order.getPriceInfo();
            if (priceInfo != null) {
                emailParams.put(CPSEmailConstants.PARAM_ORDER_SUBTOTAL, currencyFormatter.format(priceInfo.getRawSubtotal()));
                emailParams.put(CPSEmailConstants.PARAM_ORDER_SHIPPING, currencyFormatter.format(priceInfo.getShipping()));
                emailParams.put(CPSEmailConstants.PARAM_ORDER_TAX, currencyFormatter.format(priceInfo.getTax()));
                emailParams.put(CPSEmailConstants.PARAM_ORDER_TOTAL, currencyFormatter.format(priceInfo.getTotal()));
            }

            populateRealTimeParams(emailParams, profile);
            recipients.add(email);
            if(!notificationEmails.isEmpty()){
            	List recipientsTo = new ArrayList();
            	recipientsTo.addAll(notificationEmails);
            	sendWorkGroupMails(recipientsTo, emailParams, getTemplateOrderConfirmationURL(), CPSEmailConstants.EMAIL_NAME_ORDER_CONFIRMATION,null);
            }
            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderConfirmationURL(), CPSEmailConstants.EMAIL_NAME_ORDER_CONFIRMATION);

            if (!flag) {
                getProfileTools().createMailingItem(profile, CPSEmailConstants.EMAIL_NAME_ORDER_CONFIRMATION, getTemplateOrderConfirmationURL(), null, order,
                                siteId);
            }

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public boolean sendContactUsFormEmail(Dictionary dictionary, String siteId) {
        boolean flag = true;
        try {
            HashMap<String, Object> emailParamsToCPS = new HashMap<String, Object>();
            HashMap<String, Object> emailParamsToCustomer = new HashMap<String, Object>();

            String fName = (String) dictionary.get(CPSConstants.FIRST_NAME);
            String lName = (String) dictionary.get(CPSConstants.LAST_NAME);
            String emailAddress = (String) dictionary.get(CPSConstants.EMAIL);
            String account = (String) dictionary.get(CPSConstants.ACCOUNT);
            String company = (String) dictionary.get(CPSConstants.COMPANY);
            String primaryPhone = (String) dictionary.get(CPSConstants.PRIMARY_PHONE);
            String primaryPhoneExt = (String) dictionary.get(CPSConstants.PRIMARY_PHONE_EXT);
            String subject = (String) dictionary.get(CPSConstants.SUBJECT);
            String comments = (String) dictionary.get(CPSConstants.COMMENTS);

            List recipients = new ArrayList();
            recipients.add(emailAddress);
            List recipientsCPS = new ArrayList();

            // recipientsCPS.add(getEmailToCPS());

            // getting CPS recipient address based on contact us form Subject
            recipientsCPS.add(getRecipientFromSubject(subject));

            emailParamsToCPS.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParamsToCPS.put(CPSEmailConstants.FIRST_NAME, fName);
            emailParamsToCPS.put(CPSEmailConstants.LAST_NAME, lName);
            emailParamsToCPS.put(CPSConstants.COMPANY, company);
            emailParamsToCPS.put(CPSConstants.EMAIL, emailAddress);
            emailParamsToCPS.put(CPSConstants.PRIMARY_PHONE, primaryPhone);
            emailParamsToCPS.put(CPSConstants.PRIMARY_PHONE_EXT, primaryPhoneExt);
            emailParamsToCPS.put(CPSConstants.ACCOUNT, account);
            emailParamsToCPS.put(CPSConstants.COMMENTS, comments);
            emailParamsToCPS.put(CPSConstants.SUBJECT, subject);

            if (isTest()) {
                flag = sendEmailNotification(recipients, emailParamsToCPS, getTemplateContactUsCPSURL(), subject);
            } else {
                flag = sendEmailNotification(recipientsCPS, emailParamsToCPS, getTemplateContactUsCPSURL(), subject);
            }

            emailParamsToCustomer.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParamsToCustomer.put(CPSEmailConstants.FIRST_NAME, fName);
            emailParamsToCustomer.put(CPSEmailConstants.LAST_NAME, lName);

            // email to the user
            flag = sendEmailNotification(recipients, emailParamsToCustomer, getTemplateContactUsURL(), CPSEmailConstants.EMAIL_NAME_CONTACT_US_CPS);

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public boolean sendContactUsEmail(Map pParams, String pSiteId) {
        boolean flag = true;

        try {
            String userEmail = (String) pParams.get(CPSConstants.EMAIL);
            String contactSubject = (String) pParams.get(CPSConstants.SUBJECT);

            if (isLoggingDebug()) {
                logDebug("sendContactUsEmail......." + userEmail);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, null);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pParams.get(CPSEmailConstants.FIRST_NAME));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pParams.get(CPSEmailConstants.LAST_NAME));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pParams.get(CPSEmailConstants.EMAIL_FIELD));
            emailParams.put(CPSEmailConstants.PARAM_USER_COMPANY, pParams.get(CPSEmailConstants.COMPANY));
            emailParams.put(CPSEmailConstants.PARAM_USER_ACCOUNT_NUMBER, pParams.get(CPSEmailConstants.ACCOUNT));
            emailParams.put(CPSEmailConstants.PARAM_USER_PHONE, pParams.get(CPSEmailConstants.PHONE));
            emailParams.put(CPSEmailConstants.PARAM_PREFERRED_CONTACT_METHOD, pParams.get(CPSEmailConstants.CONTACT_METHOD));
            emailParams.put(CPSEmailConstants.PARAM_USER_MESSAGE, pParams.get(CPSEmailConstants.COMMENTS));
            emailParams.put(CPSEmailConstants.PARAM_CONTACT_REASON, contactSubject);
            emailParams.put(CPSEmailConstants.REPLY_TO, userEmail);
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.CONTACTUS_EMAIL);
            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(userEmail);

            List cpsRecipients = new ArrayList();
            cpsRecipients.add(getRecipientFromSubject(contactSubject));

            String cpsSubject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_CPS_CONTACT_US), emailParams);
            String confirmationSubject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_CONTACT_US_CONFIRMATION), emailParams);

            sendEmailNotification(cpsRecipients, emailParams, getTemplateContactUsCPSURL(), cpsSubject);
            flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateContactUsURL(), confirmationSubject);
        } catch (Exception e) {
            vlogError(e, "sendContactUsEmail error");
        }
        return flag;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public boolean sendCreditApplicationFile(UserCreditApplication mUser, File file) {
        boolean flag = true;
        try {
            String contactSubject = "Credit Application";
            HashMap<String, Object> emailParams = new HashMap<>();
            populateRealTimeParams(emailParams, null);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, mUser.getName());
            emailParams.put(CPSEmailConstants.PARAM_USER_LEGAL_BUSINESS_NAME, mUser.getLegalBusinessName());
            emailParams.put(CPSEmailConstants.PARAM_USER_TRADE_NAME, mUser.getTradeName());
            emailParams.put(CPSEmailConstants.PARAM_USER_ACCOUNT_NUMBER, mUser.getBankReference().getAccountNumber());
            emailParams.put(CPSEmailConstants.PARAM_USER_PHONE, mUser.getPhoneNumber());
            emailParams.put(CPSEmailConstants.PARAM_CONTACT_REASON, contactSubject);
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, mUser.getMail());
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.CREDIT_APPLICATION_EMAIL);
            List cpsRecipients = new ArrayList();
            cpsRecipients.add(getRecipientFromSubject(contactSubject));
            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(mUser.getMail());
            File[] files = new File[1];
            files[0] = file;
            String cpsSubject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_CREDIT_APPLICATION), emailParams);

            sendEmailNotification(cpsRecipients, emailParams, getTemplateApplicationCreditURL(), cpsSubject, files);

            flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateApplicationCreditURL(), cpsSubject);
        } catch (Exception e) {
            vlogError(e, "sendCreditApplicationFile error");
        }
        return flag;
    }

    public boolean sendCheckoutAvailableEmail(RepositoryItem profile) {
        boolean flag = true;
        try {

            String email = (String) profile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());
            if (isLoggingDebug()) {
                logDebug("sendVettedEmail..................email...." + email);
            }
            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, profile);
            // emailParams.put(CPSEmailConstants.PRODUCTION_URL,
            // getSiteUrl(siteId));
            List recipients = new ArrayList();
            recipients.add(email);
            flag = sendEmailNotification(recipients, emailParams, getTemplateCheckoutAvailable(), CPSEmailConstants.EMAIL_NAME_CHECKOUT_AVAILABLE);

            if (!flag) {
                getProfileTools().createMailingItem(profile, CPSEmailConstants.EMAIL_NAME_CHECKOUT_AVAILABLE, getTemplateCheckoutAvailable(), null, null,
                                getSiteId());
            }

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////

    public boolean sendAbandonedCartEmail(RepositoryItem pProfile) {
        boolean flag = true;

        try {
            String email = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendAbandonedCartEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ABANDONED_CART), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateAbandonedCartURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendAdminAddedUserEmail(RepositoryItem pAdminProfile, RepositoryItem pNewProfile) {
        boolean flag = true;

        try {
            String email = (String) pAdminProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendAdminAddedUserEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pAdminProfile);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pNewProfile.getPropertyValue(getPropertyManager().getFirstNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pNewProfile.getPropertyValue(getPropertyManager().getLastNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pNewProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ADMIN_ADDED_USER), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateAdminAddedUserURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }

        return flag;
    }

    public boolean sendCPSDeactivatedUserEmail(RepositoryItem pAdminProfile, RepositoryItem pDeactivatedProfile) {
        boolean flag = true;

        try {
            String email = (String) pAdminProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendCPSDeactivatedUserEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pAdminProfile);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pDeactivatedProfile.getPropertyValue(getPropertyManager().getFirstNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pDeactivatedProfile.getPropertyValue(getPropertyManager().getLastNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pDeactivatedProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_CPS_DEACTIVATED_USER), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateCPSDeactivatedUserURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendNewUserWelcomeEmail(RepositoryItem pProfile) {
        boolean flag = true;

        try {
            String email = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendNewUserWelcomeEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_NEW_USER_WELCOME), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateNewUserWelcomeURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendOrderApprovalExpiredEmail(RepositoryItem pProfile, Order pOrder) {
        boolean flag = true;

        try {
            String email = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendOrderApprovalExpiredEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) pOrder).getWebOrderId());

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ORDER_APPROVAL_EXPIRED), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderApprovalExpiredURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendOrderUpdateFailureEmail(String pEmailTo, Order pOrder) {
        boolean flag = true;
        try {
            String email = pEmailTo;
            if (isLoggingDebug()) {
                logDebug("sendOrderUpdateFailureEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) pOrder).getWebOrderId());

            List recipients = getRecipients(pEmailTo);
            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ORDER_UPDATE_FAILED), emailParams);
            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderApprovalExpiredURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    /**
     * Gets the recipients.
     *
     * @param pEmailTo
     *            the values
     * @return the recipients
     */
    @SuppressWarnings("unchecked")
    protected List<String> getRecipients(String pEmailTo) {
        List<String> result = new ArrayList<String>();
        if (!StringUtils.isBlank(pEmailTo)) {
            String[] emails = pEmailTo.split(";");
            for (String email : emails) {
                if (StringUtils.isNotBlank(email)) {
                    result.add(email.trim());
                }
            }
        }
        return result;
    }

    public boolean sendOrderRequestReceivedEmail(RepositoryItem pBuyerProfile, Order pOrder) {
        boolean flag = true;

        try {
            String email = (String) pBuyerProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendOrderRequestReceivedEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pBuyerProfile);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pBuyerProfile.getPropertyValue(getPropertyManager().getFirstNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pBuyerProfile.getPropertyValue(getPropertyManager().getLastNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pBuyerProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) pOrder).getWebOrderId());

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ORDER_REQUEST_RECEIVED), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderRequestReceivedURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendOrderApprovalRequiredEmail(RepositoryItem pAdminProfile, RepositoryItem pBuyerProfile, Order pOrder) {
        boolean flag = true;

        try {
            String email = (String) pAdminProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());
            List<String> recipients = new ArrayList();
            if (isLoggingDebug()) {
                logDebug("sendOrderApprovalRequiredEmail......." + email);
            }
            RepositoryItem billingAddress =(RepositoryItem) pBuyerProfile.getPropertyValue(CPSConstants.BILLING_ADDRESS);
            if (billingAddress != null) {
	            Set<String> cpsNotificationEmails= (Set<String>) billingAddress.getPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAILS);
	            if (!cpsNotificationEmails.isEmpty()) {
	            	for (String cpsNotificationEmail : cpsNotificationEmails) {
						recipients.add(cpsNotificationEmail);
					}
				}
            }
            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pAdminProfile);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pBuyerProfile.getPropertyValue(getPropertyManager().getFirstNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pBuyerProfile.getPropertyValue(getPropertyManager().getLastNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pBuyerProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) pOrder).getWebOrderId());
            emailParams.put(CPSEmailConstants.PARAM_PENDING_APPROVALS_LINK, getSiteUrl(siteId) + CPSEmailConstants.PENDING_APPROVALS_URL);

            
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ORDER_APPROVAL_REQUIRED), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderApprovalRequiredURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendOrderApprovalPendingEmail(RepositoryItem pAdminProfile, RepositoryItem pBuyerProfile, Order pOrder) {
        boolean flag = true;

        try {
            String email = (String) pAdminProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendOrderApprovalPendingEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pAdminProfile);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pBuyerProfile.getPropertyValue(getPropertyManager().getFirstNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pBuyerProfile.getPropertyValue(getPropertyManager().getLastNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pBuyerProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) pOrder).getWebOrderId());
            emailParams.put(CPSEmailConstants.PARAM_PENDING_APPROVALS_LINK, getSiteUrl(siteId) + CPSEmailConstants.PENDING_APPROVALS_URL);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ORDER_APPROVAL_PENDING), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderApprovalPendingURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendOrderApprovedEmail(RepositoryItem pBuyerProfile, Order pOrder) {
        boolean flag = true;

        try {
            String email = (String) pBuyerProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendOrderApprovedEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pBuyerProfile);
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) pOrder).getWebOrderId());

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ORDER_APPROVED), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderApprovedURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendOrderRejectedEmail(RepositoryItem pBuyerProfile, RepositoryItem pApproverProfile, Order pOrder, String explanation) {
        boolean flag = true;

        try {
            String email = (String) pBuyerProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendOrderRejectedEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pBuyerProfile);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pApproverProfile.getPropertyValue(getPropertyManager().getFirstNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pApproverProfile.getPropertyValue(getPropertyManager().getLastNamePropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pApproverProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) pOrder).getWebOrderId());
            emailParams.put(CPSEmailConstants.PARAM_REJECTION_REASON, explanation);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ORDER_REJECTED), emailParams);
            if (StringUtils.isNotEmpty(explanation)) {
                flag = sendEmailNotification(recipients, emailParams, getTemplateOrderRejectedURL(), subject);
            } else {
                flag = sendEmailNotification(recipients, emailParams, getTemplateOrderRejectedNoCommentsURL(), subject);
            }

        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendPasswordResetConfirmationEmail(RepositoryItem pProfile) {
        boolean flag = true;

        try {
            String email = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendPasswordResetConfirmationEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_PSWD_RESET_CONFIRMATION), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplatePasswordResetConfirmationURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendPasswordResetLinkEmail(RepositoryItem pProfile, String resetUrl) {
        boolean flag = true;

        try {
            String email = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendPasswordResetLinkEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);
            emailParams.put(CPSEmailConstants.PARAM_RESET_LINK, getSiteUrl(siteId) + resetUrl);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_PSWD_RESET_LINK), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplatePasswordResetLinkURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendPriceListUpdateConfirmationEmail(String pName, String pEmail) {
        boolean flag = true;

        try {
            if (isLoggingDebug()) {
                logDebug("sendPriceListUpdateConfirmationEmail.......");
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, null);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pName);
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pEmail);
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.PRICELIST_UPDATE);
            List recipients = new ArrayList();
            recipients.add(getEmailToCPSPriceListUpdateRequest());
            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(pEmail);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_PRICE_LIST_UPDATE_CONFIRMATION), emailParams);

            sendEmailNotification(recipients, emailParams, getTemplatePriceListUpdateConfirmationURL(), subject);

            flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateUserPriceListUpdateConfirmation(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendQuoteRequestedEmail(RepositoryItem pCustomerProfile, String quoteComments, File[] attachments) {
        boolean flag = false;

        if (!getEmailToCPSQuoteRequest().trim().equals("")) {
            try {
                if (isLoggingDebug()) {
                    logDebug("sendQuoteRequestedEmail......." + (attachments != null ? attachments.length : "no") + " attachments");
                }
                HashMap<String, Object> emailParams = new HashMap<String, Object>();
                populateRealTimeParams(emailParams, pCustomerProfile);
                emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, pCustomerProfile.getPropertyValue(getPropertyManager().getFirstNamePropertyName()));
                emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, pCustomerProfile.getPropertyValue(getPropertyManager().getLastNamePropertyName()));
                emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, pCustomerProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName()));
                emailParams.put(CPSEmailConstants.PARAM_QUOTE_COMMENTS, quoteComments);

                RepositoryItem org = (RepositoryItem) pCustomerProfile.getPropertyValue(CPSConstants.PARENT_ORG);
                emailParams.put(CPSEmailConstants.PARAM_USER_ACCOUNT_NUMBER, org != null ? org.getRepositoryId() : "N/A");
                for (Entry<String, Object> entry : emailParams.entrySet()) {
                    logDebug("key: " + entry.getKey() + " value: " + entry.getValue());
                }

                List recipients = Arrays.asList(getEmailToCPSQuoteRequest().trim().split(","));

                String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_QUOTE_REQUESTED), emailParams);

                flag = sendEmailNotification(recipients, emailParams, getTemplateQuoteRequestedURL(), subject, attachments);
            } catch (Exception e) {
                vlogError(e, "Error");
            }
        } else {
            vlogWarning("Quote request was skipped - no e-mail address set in module configuration.");
        }

        return flag;
    }

    public boolean sendQuoteRequestedEmail(RepositoryItem pCustomerProfile, Map<String, Object> values, File[] attachments) {
        boolean flag = false;

        if (!getEmailToCPSQuoteRequest().trim().equals("")) {
            try {
                if (isLoggingDebug()) {
                    logDebug("sendQuoteRequestedEmail......." + (attachments != null ? attachments.length : "no") + " attachments");
                }
                String userEmail = String.valueOf(values.get("emailAddress"));
                RepositoryItem selectedCSItem = (RepositoryItem) pCustomerProfile.getPropertyValue("selectedCS");
                Set<String> workGroupMails =  new HashSet<String>();
                if(selectedCSItem!=null){
                	String csId = selectedCSItem.getRepositoryId();
                    RepositoryItem workGroupItem = (RepositoryItem) selectedCSItem.getPropertyValue("workGroupId");
                    if(workGroupItem != null){
                        vlogDebug("workGroupId :: {0}", workGroupItem.getRepositoryId());
                    	workGroupMails = (Set<String>) workGroupItem.getPropertyValue("workGroupMails");
                        vlogDebug("Work group emails :: {0}", workGroupMails);
                    }
                }                
                if (CollectionUtils.isEmpty(workGroupMails)) {
                    workGroupMails.add(getDefaultWorkGroupMail());
                }

                HashMap<String, Object> emailParams = new HashMap<String, Object>();

                // Leaving existing params to avoid breaking other
                // e-mails that may
                // be unrelated to the BCC-templated ones
                emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));

                emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, values.get("firstName"));
                emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, values.get("lastName"));
                emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, userEmail);
                emailParams.put(CPSEmailConstants.PARAM_USER_PHONE, values.get("phone"));
                emailParams.put(CPSEmailConstants.PARAM_USER_ACCOUNT_NUMBER, values.get("account") != null ? values.get("account") : "N/A");
                emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.REQUEST_QUOTE_EMAIL);
                String comments = "";
                for (Entry<String, Object> entry : values.entrySet()) {
                    comments += entry.getKey() + ":" + entry.getValue() + "<br>";
                }
                emailParams.put(CPSEmailConstants.PARAM_QUOTE_COMMENTS, comments);

                List recipients = new ArrayList();
                recipients.add(getEmailToCPSQuoteRequest());
                List confirmationRecipients = new ArrayList();
                confirmationRecipients.addAll(workGroupMails);

                String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_QUOTE_REQUESTED), emailParams);

                sendWorkGroupMails(confirmationRecipients, emailParams, getTemplateQuoteRequestedURL(), subject, attachments);

                flag = sendEmailNotification(recipients, emailParams, getTemplateQuoteRequestedURL(), subject, attachments);
            } catch (Exception e) {
                vlogError(e, "Error");
            }
        } else {
            logWarning("Quote request was skipped - no e-mail address set in module configuration.");
        }

        return flag;
    }

    public boolean sendShareMaterialListEmail(RepositoryItem pProfile, String recipient, String giftlistId, String pUrl, File[] attachments) {
        boolean flag = true;

        try {
            if (isLoggingDebug()) {
                logDebug("sendShareMaterialListEmail.......");
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);
            emailParams.put(CPSEmailConstants.PARAM_SHARED_URL, getSiteUrl(siteId) + pUrl);

            List recipients = new ArrayList();
            recipients.add(recipient);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_SHARE_MATERIAL_LIST), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateShareMaterialListURL(), subject, attachments);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendSharePageEmail(String senderEmailAddress, String senderFirstName, String senderLastName, String recipientList, String message,
                    String pageUrl) {
        boolean flag = true;

        try {
            if (isLoggingDebug()) {
                logDebug("sendSharePageEmail......." + recipientList);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, null);
            emailParams.put(CPSEmailConstants.PARAM_EMAIL_ADDRESS, senderEmailAddress);
            emailParams.put(CPSEmailConstants.PARAM_SHARED_URL, getSiteUrl(siteId) + pageUrl);
            emailParams.put(CPSEmailConstants.PARAM_SHARE_COMMENTS, message);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, senderFirstName);
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, senderLastName);

            List recipients = Arrays.asList(recipientList.split(","));

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_SHARE_PRODUCT), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateSharePageURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendTemporaryPasswordEmail(RepositoryItem pProfile, String temporaryPassword, String url) {
        boolean flag = true;

        try {
            String email = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendTemporaryPasswordEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);
            emailParams.put(CPSEmailConstants.PARAM_PSWD, temporaryPassword);
            emailParams.put(CPSEmailConstants.PARAM_RESET_LINK, getSiteUrl(siteId) + url);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_TEMPORARY_PSWD), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateTemporaryPasswordURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendTestimonialEmail(String firstName, String lastName, String company, String testimonialMessage, String userEmail) {
        boolean flag = true;

        if (!getEmailToCPSTestimonial().trim().equals("")) {
            try {
                HashMap<String, Object> emailParams = new HashMap<String, Object>();
                emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
                emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
                emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, firstName);
                emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, lastName);
                emailParams.put(CPSEmailConstants.PARAM_USER_COMPANY, company);
                emailParams.put(CPSEmailConstants.PARAM_TESTIMONIAL, testimonialMessage);
                if(userEmail == null || StringUtils.isBlank(userEmail)) {
                	emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, getAnonymousUserEmail());
                } else {
                	emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, userEmail);
                }                
                emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.TESTIMONIAL_EMAIL);
                // emailParams.put(CPSEmailConstants.PARAM_CASE_STUDY_URL,
                // caseStudyUrl);

                List confirmationRecipients = new ArrayList();
                confirmationRecipients.add(userEmail);

                List recipients = new ArrayList();
                recipients.add(getEmailToCPSTestimonial());

                String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_TESTIMONIAL), emailParams);
                sendEmailNotification(recipients, emailParams, getTemplateTestimonialURL(), subject);
                if(userEmail != null || StringUtils.isNotBlank(userEmail)) {
                	flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateUserTestimonial(), subject);
                }
            } catch (Exception e) {
                vlogError(e, "Error");
            }
        }

        return flag;
    }

    public boolean sendCurrentStatementCopyEmail(String email, String firstName, String lastName, String customerNumber) {
        boolean flag = true;

        if (!getEmailToCPSTestimonial().trim().equals("")) {
            try {
                HashMap<String, Object> emailParams = new HashMap<String, Object>();
                emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, firstName);
                emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, lastName);
                emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
                emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.REQUEST_STATEMENT_EMAIL);
                emailParams.put(CPSEmailConstants.PARAM_CUSTOMER_NUMBER, customerNumber);
                emailParams.put(CPSEmailConstants.REPLY_TO, email);
                emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, email);

                List confirmationRecipients = new ArrayList();
                confirmationRecipients.add(email);

                List cpsRecipients = new ArrayList();
                cpsRecipients.add(getRequestStatementEmail());

                String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_USER_REQUEST_CURRENT_STATEMENT_COPY),
                                emailParams);
                sendEmailNotification(cpsRecipients, emailParams, getTemplateRequestCurrentStatementCopy(), subject);

                flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateUserRequestCurrentStatementCopy(), subject);
            } catch (Exception e) {
                vlogError(e, "Error");
            }
        }

        return flag;
    }

    public boolean sendSavedCartReminder(Map<String, String> profiles) {
        boolean flag = true;

        for (Map.Entry<String, String> entry : profiles.entrySet()) {
            try {
                HashMap<String, Object> emailParams = new HashMap<String, Object>();
                emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, entry.getValue());
                emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));

                String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_USER_SAVED_CART_REMINDER), emailParams);

                List<String> recipients = new ArrayList<String>();
                recipients.add(entry.getKey());

                flag = sendEmailNotification(recipients, emailParams, getTemplateUserSavedCartReminder(), subject);
            } catch (Exception e) {
                vlogError(e, "Error");
            }
        }
        return flag;
    }

    public boolean sendUserProfileUpdatedEmail(RepositoryItem pProfile) {
        boolean flag = true;

        try {
            String email = (String) pProfile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());

            if (isLoggingDebug()) {
                logDebug("sendUserProfileUpdatedEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            populateRealTimeParams(emailParams, pProfile);

            List recipients = new ArrayList();
            recipients.add(email);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_USER_PROFILE_UPDATED), emailParams);

            flag = sendEmailNotification(recipients, emailParams, getTemplateUserProfileUpdatedURL(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendCPSServicesContactUsEmail(String firstName, String lastName, String emailAddress, String service, String message) {
        boolean flag = true;

        try {
            String email = getEmailToCPSService();

            if (isLoggingDebug()) {
                logDebug("sendCPSServicesContactUsEmail......." + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, firstName);
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, lastName);
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, emailAddress);
            emailParams.put(CPSEmailConstants.PARAM_USER_MESSAGE, message);
            emailParams.put(CPSEmailConstants.PARAM_SERVICE_NAME, service);
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.SERVICESUS_EMAIL);
            List recipients = new ArrayList();
            recipients.add(email);
            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(emailAddress);

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_CPS_SERVICES_CONTACT_US), emailParams);
            sendEmailNotification(recipients, emailParams, getTemplateCPSServicesContactUs(), subject);

            flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateCPSServicesContactUs(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendCPSRegionalManagerContactUsEmail(RepositoryItem regionalManager, String firstName, String lastName, String emailAddress,
                    String message) {
        boolean flag = true;

        try {
            String email = (String) regionalManager.getPropertyValue(CPSConstants.EMAIL);

            // Requirements state that it should be possible to redirect all
            // e-mails to a single address
            String rmOverrideEmail = getEmailToCPSRegionalManagerOverride();
            if (rmOverrideEmail != null && !"".equals(rmOverrideEmail.trim())) {
                email = rmOverrideEmail;
            }

            if (!"".equals(email)) {
                if (isLoggingDebug()) {
                    logDebug("sendCPSRegionalManagerContactUsEmail........" + email);
                }

                HashMap<String, Object> emailParams = new HashMap<String, Object>();
                emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
                emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
                emailParams.put(CPSEmailConstants.PARAM_FIRST_NAME, regionalManager.getPropertyValue(CPSConstants.NAME));
                emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, firstName);
                emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, lastName);
                emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, emailAddress);
                emailParams.put(CPSEmailConstants.PARAM_USER_MESSAGE, message);

                List recipients = new ArrayList();
                recipients.add(email);

                String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_CPS_REGIONAL_MANAGER_CONTACT_US), emailParams);

                if (emailAddress != null) {
                    sendEmailNotification(Arrays.asList(emailAddress), emailParams, getTemplateUserRegionalManagerContactUs(), subject);
                }
                sendEmailNotification(recipients, emailParams, getTemplateCPSServicesContactUs(), subject);
                flag = sendEmailNotification(recipients, emailParams, getTemplateCPSRegionalManagerContactUs(), subject);
            }
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    public boolean sendRequestAccessEmail(Map<String, String> params) {
        boolean flag = true;

        try {
            if (StringUtils.isEmpty(getRequestAccessEmail())) {
                if (isLoggingDebug()) {
                    logDebug("recipient Email is empty");
                }
                return false;
            }
            String email = getRequestAccessEmail();
            if (isLoggingDebug()) {
                logDebug("sendRequestAccessEmail........" + email);
            }
            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));

            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, params.get(CPSConstants.FIRST_NAME));
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, params.get(CPSConstants.LAST_NAME));
            emailParams.put(CPSEmailConstants.PARAM_USER_PHONE_NUMBER, params.get(CPSConstants.PHONE_NUMBER));
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, params.get(CPSConstants.EMAIL));

            emailParams.put(CPSEmailConstants.PARAM_APPROVER_FIRST_NAME, params.get(CPSConstants.APPROVAL_FIRST_NAME));
            emailParams.put(CPSEmailConstants.PARAM_APPROVER_LAST_NAME, params.get(CPSConstants.APPROVAL_LAST_NAME));
            emailParams.put(CPSEmailConstants.PARAM_APPROVER_PHONE_NUMBER, params.get(CPSConstants.APPROVAL_PHONE_NUMBER));
            emailParams.put(CPSEmailConstants.PARAM_APPROVER_EMAIL, params.get(CPSConstants.APPROVAL_EMAIL));

            emailParams.put(CPSEmailConstants.PARAM_ROLES, params.get(CPSConstants.ROLES));
            emailParams.put(CPSEmailConstants.PARAM_COMPANY_NAME, params.get(CPSConstants.COMPANY_NAME));
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.REQUEST_ACCESS_EMAIL);
            List<String> recipients = new ArrayList<>();
            recipients.add(email);
            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(params.get(CPSConstants.EMAIL));
            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_REQUEST_ACCESS), emailParams);

            if (isRequestAccessSendCopyToApprover()) {
                recipients.add(params.get(CPSConstants.APPROVAL_EMAIL));
            }
            sendEmailNotification(confirmationRecipients, emailParams, getTemplateRequestAccess(), subject);
            flag = flag && sendEmailNotification(recipients, emailParams, getTemplateRequestAccess(), subject);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        return flag;
    }

    /**
     * Method to notify CPS about PDP error finding
     * 
     * @param emailParams
     * @return
     */
    public boolean sendPDPErrorEmail(HashMap<String, Object> emailParams) {
        boolean flag = true;
        try {
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
            if(StringUtils.isBlank((String) emailParams.get(CPSEmailConstants.PARAM_USER_FIRST_NAME))){
            	emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, getAnonymousUserName());
            }
            if(StringUtils.isBlank((String) emailParams.get(CPSEmailConstants.PARAM_USER_EMAIL))){
            	emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, getAnonymousUserEmail());
            }

            List<String> recipients = new ArrayList<>();
            recipients.add(getEmailToCPSPdpError());

            String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_PDP_ERROR), emailParams);
            // Send confirmation email notification to customer
            /*
             * List confirmationRecipients = new ArrayList(); confirmationRecipients.add(emailParams.get(CPSConstants.EMAIL));
             * sendEmailNotification(confirmationRecipients, emailParams, getTemplatePDPErrorURL(), subject);
             */

            flag = sendEmailNotification(recipients, emailParams, getTemplatePDPErrorURL(), subject);
        } catch (Exception e) {
            vlogError("Error while sending pdp error {0}", e.getMessage());
        }

        return flag;
    }
    
    /**
     * 
     * @param attachments
     * @param emailParams
     * @param environment
     * @return
     */
    public boolean sendMissingWebOrderReportEmail(File[] attachments, HashMap<String, Object> emailParams, String environment) {
		
		boolean flag = true;

		emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
		emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
		List<String> recipients = (List<String>) emailParams.get(CPSConstants.EMAIL);

		String subject = DataUtils.paramsMessage(
				messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_MISSING_WEBORDERS), emailParams);
		subject = getSubjectForEmail(environment, subject);

		flag = sendEmailNotification(recipients, emailParams, getTemplateMissingWebOrderURL(), subject, attachments);

		return flag;
	}
    
	public boolean sendRegisteredUserReportEmail(File[] attachments, HashMap<String, Object> emailParams, String environment) {
    	boolean flag = true;
    	
    	emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
        emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
        boolean isEnableLink = (boolean) emailParams.get(CPSConstants.ENABLE_DOWNLOAD_LINK);
        String downloadFileLink = (String) emailParams.get(CPSConstants.DOWNLOAD_FILE_LINK);
       
    	if(attachments != null && attachments.length > 0 && !isEnableLink){
        	String emailBodyNewUserAttachmentMsg = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.PARAM_NEW_USER_ATTACHMENT_MSG), emailParams);
        	emailParams.put(CPSEmailConstants.PARAM_NEW_USER_REPORT, emailBodyNewUserAttachmentMsg);
        	emailParams.put(CPSEmailConstants.PARAM_NEW_USER_DOWNLOAD_LINK, "");
        } else if(isEnableLink && StringUtils.isNotBlank(downloadFileLink)){
        	String emailBodyNewUserDownloadLinkMsg = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.PARAM_NEW_USER_DOWNLOAD_LINK_MSG), emailParams);
        	emailParams.put(CPSEmailConstants.PARAM_NEW_USER_REPORT, emailBodyNewUserDownloadLinkMsg);
        	emailParams.put(CPSEmailConstants.PARAM_NEW_USER_DOWNLOAD_LINK, downloadFileLink);
        } else {
        	String emailBodyNoUserRegisteredMsg = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.PARAM_NO_USER_REGISTERED_MSG), emailParams);
        	emailParams.put(CPSEmailConstants.PARAM_NEW_USER_REPORT, emailBodyNoUserRegisteredMsg);
        	emailParams.put(CPSEmailConstants.PARAM_NEW_USER_DOWNLOAD_LINK, "");
        }
    	List<String> recipients= (List<String>) emailParams.get(CPSConstants.EMAIL);
    	
        String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_NEW_USER_REPORT), emailParams);
		subject = getSubjectForEmail(environment, subject);
    	if(isEnableLink && StringUtils.isNotBlank(downloadFileLink)){
        	flag = sendEmailNotification(recipients, emailParams, getTemplateNewUserReport(), subject);
        } else {
        	flag = sendEmailNotification(recipients, emailParams, getTemplateNewUserReport(), subject, attachments);
        }
       
    	return flag;
    }
	
	public boolean sendNotActivatedUsersReportEmail(File[] attachments, HashMap<String, Object> emailParams, String environment) {
		boolean flag = true;

		emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
		emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
        boolean isEnableLink = (boolean) emailParams.get(CPSConstants.ENABLE_DOWNLOAD_LINK);
        String downloadFileLink = (String) emailParams.get(CPSConstants.DOWNLOAD_FILE_LINK);

		if (attachments != null && attachments.length > 0 && !isEnableLink) {
			String emailBodyNewUserAttachmentMsg = DataUtils.paramsMessage(
					messageUtils.getMessage(CPSEmailConstants.PARAM_INACTIVE_USER_ATTACHMENT_MSG), emailParams);
			emailParams.put(CPSEmailConstants.PARAM_INACTIVE_USER_REPORT, emailBodyNewUserAttachmentMsg);
			emailParams.put(CPSEmailConstants.PARAM_INACTIVE_USER_DOWNLOAD_LINK, "");
		} else if (isEnableLink && StringUtils.isNotBlank(downloadFileLink )) {
			String emailBodyNewUserDownloadLinkMsg = DataUtils.paramsMessage(
					messageUtils.getMessage(CPSEmailConstants.PARAM_INACTIVE_USER_DOWNLOAD_LINK_MSG), emailParams);
			emailParams.put(CPSEmailConstants.PARAM_INACTIVE_USER_REPORT, emailBodyNewUserDownloadLinkMsg);
			emailParams.put(CPSEmailConstants.PARAM_INACTIVE_USER_DOWNLOAD_LINK, downloadFileLink);
		} else {
			String emailBodyNoUserRegisteredMsg = DataUtils
					.paramsMessage(messageUtils.getMessage(CPSEmailConstants.PARAM_NO_INACTIVE_USER_MSG), emailParams);
			emailParams.put(CPSEmailConstants.PARAM_INACTIVE_USER_REPORT, emailBodyNoUserRegisteredMsg);
			emailParams.put(CPSEmailConstants.PARAM_INACTIVE_USER_DOWNLOAD_LINK, "");
		}
		String subject = DataUtils.paramsMessage(
				messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_INACTIVE_USER_REPORT), emailParams);
		subject = getSubjectForEmail(environment, subject);
		String receipientEmail= (String) emailParams.get(CPSConstants.EMAIL);
		List<String> recipients = new ArrayList<>();
		recipients.add(receipientEmail);
		
		if (isEnableLink && StringUtils.isNotBlank(downloadFileLink )) {
			flag = sendEmailNotification(recipients, emailParams, getTemplateNewUserNotActivatedReport(), subject);
		} else {
			flag = sendEmailNotification(recipients, emailParams, getTemplateNewUserNotActivatedReport(), subject, attachments);
		}

		return flag;
	}
	
	/**
	 * To send sales order report
	 * @param attachments
	 * @param emailParams
	 * @param environment
	 * @return boolean value
	 */
	public boolean sendSalesOrderReportEmail(File[] attachments, HashMap<String, Object> emailParams, String environment) {
    	vlogInfo("Sending sales order report via email...");
		boolean flag = true;
    	
    	emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
        emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
        boolean isEnableLink = (boolean) emailParams.get(CPSConstants.ENABLE_DOWNLOAD_LINK);
        String downloadFileLink = (String) emailParams.get(CPSConstants.DOWNLOAD_FILE_LINK);
       
    	if(attachments != null && attachments.length > 0 && !isEnableLink){
        	String emailBodySalesOrderAttachmentMsg = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.PARAM_SALES_ORDER_ATTACHMENT_MSG), emailParams);
        	emailParams.put(CPSEmailConstants.PARAM_SALES_ORDER_REPORT, emailBodySalesOrderAttachmentMsg);
        	emailParams.put(CPSEmailConstants.PARAM_SALES_ORDER_DOWNLOAD_LINK, "");
        } else if(isEnableLink && StringUtils.isNotBlank(downloadFileLink)){
        	String emailBodySalesOrderDownloadLinkMsg = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.PARAM_SALES_ORDER_DOWNLOAD_LINK_MSG), emailParams);
        	emailParams.put(CPSEmailConstants.PARAM_SALES_ORDER_REPORT, emailBodySalesOrderDownloadLinkMsg);
        	emailParams.put(CPSEmailConstants.PARAM_SALES_ORDER_DOWNLOAD_LINK, downloadFileLink);
        } else {
        	String emailBodyNoSalesOrderReportMsg = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.PARAM_NO_SALES_ORDER_REPORT_MSG), emailParams);
        	emailParams.put(CPSEmailConstants.PARAM_SALES_ORDER_REPORT, emailBodyNoSalesOrderReportMsg);
        	emailParams.put(CPSEmailConstants.PARAM_SALES_ORDER_DOWNLOAD_LINK, "");
        }
    	List<String> recipients= (List<String>) emailParams.get(CPSConstants.EMAIL);
    	
        String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_SALES_ORDER_REPORT), emailParams);
		subject = getSubjectForEmail(environment, subject);
    	if(isEnableLink && StringUtils.isNotBlank(downloadFileLink)){
        	flag = sendEmailNotification(recipients, emailParams, getTemplateSalesOrderReport(), subject);
        } else {
        	flag = sendEmailNotification(recipients, emailParams, getTemplateSalesOrderReport(), subject, attachments);
        }
       
    	return flag;
    }
	
	/**
	 * 
	 * @param environment
	 * @param subject
	 * @return
	 */
	protected String getSubjectForEmail(String environment, String subject) {
		if (environment == null || environment.isEmpty()) {
			return new StringBuilder(subject).toString();
		} else {
			return new StringBuilder(environment).append(subject).toString();
		}
		
    }

    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////

    private String getRecipientFromSubject(String subject) {
        Map<String, String> emails = getCpsEmailAddresses();
        String recipientAddress = null;
        if (!StringUtils.isEmpty(subject) && emails != null) {
            recipientAddress = emails.get(subject);
        }
        return recipientAddress;
    }

    /**
     * Sends out email message using TemplateEmailSender
     *
     * @param emailsTo
     * @param emailParams
     * @param emailTemplate
     * @param emailSubject
     * @return
     */
    @SuppressWarnings("rawtypes")
    public boolean sendEmailNotification(List emailsTo, HashMap<String, Object> emailParams, String emailTemplate, String emailSubject) {
        return this.sendEmailNotification(emailsTo, emailParams, emailTemplate, emailSubject, null); // Call
        // attachment
        // version
        // with
        // null
        // attachment
    }

    public void sendFeedStatusEmailNotification(List<String> emailsTo, String emailSubject, String emailBody) {
        HashMap<String, Object> emailParams = new HashMap<String, Object>();
        emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
        emailParams.put("PARAM_FEED_STATUS_TEXT", emailBody);
        sendEmailNotification(emailsTo, emailParams, getTemplateFeedImport(), emailSubject);

    }

    /**
     * Sends out email messages using TemplateEmailSender
     *
     * @param emailsTo
     * @param emailParams
     * @param emailTemplate
     * @param emailSubject
     * @param attachments
     */
    @SuppressWarnings("rawtypes")
    public boolean sendEmailNotification(List emailsTo, HashMap<String, Object> emailParams, String emailTemplate, String emailSubject, File[] attachments) {
        boolean returnFlag = true;
        try {
            TemplateEmailInfoImpl templateInfo = new TemplateEmailInfoImpl();
            HtmlContentProcessor contentProcessor = new HtmlContentProcessor();
            contentProcessor = new HtmlContentProcessor();
            contentProcessor.setSendAsHtml(true);
            contentProcessor.setSendAsText(true);
            contentProcessor.setIndentWidth(4);
            contentProcessor.setLineWidth(80);
            templateInfo.setContentProcessor(contentProcessor);
            generateTemplatedEmailParameters(emailParams);
            templateInfo.setTemplateParameters(emailParams);
            templateInfo.setTemplateURL(emailTemplate);
            String emailType = null;
            emailType = (String) emailParams.get(CPSEmailConstants.EMAIL_TYPE);
            List allEmails = new ArrayList();
            allEmails = getAllEmailsType();
            allEmails.add(allEmails);
            if (allEmails.contains(emailType)) {

                if (emailsTo.contains(emailParams.get(CPSEmailConstants.PARAM_USER_EMAIL))) {
                    templateInfo.setMessageFrom(getEmailFrom());
                } else {
                    templateInfo.setMessageFrom((String) emailParams.get(CPSEmailConstants.PARAM_USER_FIRST_NAME) + "<"
                                    + (String) emailParams.get(CPSEmailConstants.PARAM_USER_EMAIL) + ">");
                }
                if (CPSEmailConstants.REQUEST_STATEMENT_EMAIL.equals(emailType)) {
                    templateInfo.setMessageReplyTo((String) emailParams.get(CPSEmailConstants.REPLY_TO));
                } else {
                    for (Object email : emailsTo) {
                        if (getCpsEmailAddresses().containsValue(email)) {
                            templateInfo.setMessageReplyTo((String) emailParams.get(CPSEmailConstants.REPLY_TO));
                        }
                    }
                }
            } else {
                templateInfo.setMessageFrom(getEmailFrom());
            }
            templateInfo.setMessageSubject(truncateSubject(emailSubject));

            if (attachments != null && attachments.length > 0) {
                templateInfo.setMessageAttachments(attachments);
            }
            // send it
            if (isLoggingDebug()) {
                logDebug("New E-mail - To: " + emailsTo + ", Template: " + emailTemplate + ", Subject: " + emailSubject + ", Attachments (Count): "
                                + (attachments != null ? attachments.length : "null"));
                logDebug("Params: " + emailParams);
            }

            TransactionManager tm = getTransactionManager();
            if (tm != null) {
                TransactionDemarcation td = new TransactionDemarcation();
                boolean rollbackTransaction = true;

                try {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                    getTemplateEmailSender().sendEmailMessage(templateInfo, emailsTo, true, true);
                    rollbackTransaction = false;
                } catch (Exception ex) {
                    vlogError(ex, "Error");
                } finally {
                    td.end(rollbackTransaction);
                }
            } else {
                getTemplateEmailSender().sendEmailMessage(templateInfo, emailsTo, true, true);
            }
        } catch (Exception ex) {
            vlogError(ex, "Error");
            returnFlag = false;
        }
        vlogDebug("Message send: " + returnFlag);
        return returnFlag;
    }
    
	/**
	 * @param emailsTo
	 * @param emailParams
	 * @param emailTemplate
	 * @param emailSubject
	 * @param attachments
	 * @return
	 */
	public boolean sendWorkGroupMails(List emailsTo, HashMap<String, Object> emailParams, String emailTemplate,
			String emailSubject, File[] attachments) {
		return this.sendWorkGroupMails(emailsTo, emailParams, emailTemplate, emailSubject, attachments, null, null);
	}
    
    public boolean sendWorkGroupMails(List emailsTo, HashMap<String, Object> emailParams, String emailTemplate, String emailSubject, File[] attachments, List emailsBcc, List emailsCc) {
        boolean returnFlag = true;
        try {
            TemplateEmailInfoImpl templateInfo = new TemplateEmailInfoImpl();
            HtmlContentProcessor contentProcessor = new HtmlContentProcessor();
            contentProcessor = new HtmlContentProcessor();
            contentProcessor.setSendAsHtml(true);
            contentProcessor.setSendAsText(true);
            contentProcessor.setIndentWidth(4);
            contentProcessor.setLineWidth(80);
            templateInfo.setContentProcessor(contentProcessor);
            generateTemplatedEmailParameters(emailParams);
            templateInfo.setTemplateParameters(emailParams);
            templateInfo.setTemplateURL(emailTemplate);
            String emailType = null;
            emailType = (String) emailParams.get(CPSEmailConstants.EMAIL_TYPE);
            List allEmails = new ArrayList();
            allEmails = getAllEmailsType();
            allEmails.add(allEmails);
            if (allEmails.contains(emailType)) {

                if (emailsTo.contains(emailParams.get(CPSEmailConstants.PARAM_USER_EMAIL))) {
                    templateInfo.setMessageFrom(getEmailFrom());
                } else {
                    templateInfo.setMessageFrom((String) emailParams.get(CPSEmailConstants.PARAM_USER_FIRST_NAME) + "<"
                                    + (String) emailParams.get(CPSEmailConstants.PARAM_USER_EMAIL) + ">");
                }
                if (CPSEmailConstants.REQUEST_STATEMENT_EMAIL.equals(emailType)) {
                    templateInfo.setMessageReplyTo((String) emailParams.get(CPSEmailConstants.REPLY_TO));
                } else {
                    for (Object email : emailsTo) {
                        if (getCpsEmailAddresses().containsValue(email)) {
                            templateInfo.setMessageReplyTo((String) emailParams.get(CPSEmailConstants.REPLY_TO));
                        }
                    }
                }
            } else {
                templateInfo.setMessageFrom(getEmailFrom());
            }
            templateInfo.setMessageSubject(truncateSubject(emailSubject));
            String messageToRecipient=null;
            String messageTo=null;
            String messageBcc=null;
            String messageCc=null;
            if(CollectionUtils.isNotEmpty(emailsTo)) {
            	messageToRecipient = (String) emailsTo.get(0);
            	String[] emailsToArray=new String[emailsTo.size()];
            	 messageTo = StringUtils.joinStrings((String[]) emailsTo.toArray(emailsToArray), ',');
            	templateInfo.setMessageTo(messageTo);
            }
            if(CollectionUtils.isNotEmpty(emailsBcc)) {
            	String[] emailsBccArray=new String[emailsBcc.size()];
            	messageBcc = StringUtils.joinStrings((String[]) emailsBcc.toArray(emailsBccArray), ',');
            	templateInfo.setMessageBcc(messageBcc);
            }
            if(CollectionUtils.isNotEmpty(emailsCc)) {
            	String[] emailsCcArray=new String[emailsCc.size()];
            	messageCc = StringUtils.joinStrings((String[]) emailsCc.toArray(emailsCcArray), ',');
            	templateInfo.setMessageCc(messageCc);
            }
            if (attachments != null && attachments.length > 0) {
                templateInfo.setMessageAttachments(attachments);
            }
            List messageToRecipients=new ArrayList();
            if(messageToRecipient != null) {
            	messageToRecipients.add(messageToRecipient);
            }
            // send it
            if (isLoggingDebug()) {
                logDebug("New E-mail - To: " + messageToRecipients + ", Template: " + emailTemplate + ", Subject: " + emailSubject + ", Attachments (Count): "
                                + (attachments != null ? attachments.length : "null"));
                logDebug("Params: " + emailParams);
                logDebug("To emails" +messageTo);
            }

            TransactionManager tm = getTransactionManager();
            if (tm != null) {
                TransactionDemarcation td = new TransactionDemarcation();
                boolean rollbackTransaction = true;

                try {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                    getTemplateEmailSender().sendEmailMessage(templateInfo, messageToRecipients, true, true);
                    rollbackTransaction = false;
                } catch (Exception ex) {
                    vlogError(ex, "Error");
                } finally {
                    td.end(rollbackTransaction);
                }
            } else {
                getTemplateEmailSender().sendEmailMessage(templateInfo, messageToRecipients, true, true);
            }
        } catch (Exception ex) {
            vlogError(ex, "Error");
            returnFlag = false;
        }
        vlogDebug("Message send: " + returnFlag);
        return returnFlag;
    }
    
    public String truncateSubject(String subject) {
        String result = "";

        // Using a regular expression, try to truncate to the nearest whitespace
        // (looks better than the middle of a word)
        Pattern regex = Pattern.compile(getSubjectLineTruncateRegex());
        Matcher matcher = regex.matcher(subject);

        try {
            if (matcher.find()) {
                result = matcher.group(1);
            }
        } catch (IllegalStateException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        // Fallback method of straight XX characters, should something happen
        // with the regular expression
        if (result.equals("")) {
            int maxLength = getSubjectLineMaxLength() != null ? getSubjectLineMaxLength().intValue() : 80;
            result = subject.substring(0, subject.length() > maxLength ? maxLength : subject.length());
        }

        if (isLoggingDebug()) {
            if (result.equals(subject)) {
                logDebug("truncateSubject - no truncation required");
            } else {
                logDebug("truncateSubject - subject truncated from '" + subject + "' to '" + result + "'");
            }
        }

        return result;
    }

    /**
     * Returns site url for email template
     *
     * @param pSiteId
     * @return
     */
    public String getSiteUrl(String pSiteId) {
        return getSiteURLManager().getSiteUrl(pSiteId);
    }

    /**
     * Returns default site url for email template
     *
     * @return SiteUrl
     */
    public String getSiteUrl() {
        return getSiteURLManager().getSiteUrl(getSiteId());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public boolean sendBatchEmailNotification(RepositoryItem mailing, RepositoryItem profile) {
        boolean sendSuccess = true;
        if (mailing != null) {
            String emailSubject = (String) mailing.getPropertyValue(CPSEmailConstants.EMAIL_SUBJECT);
            String emailTo = (String) profile.getPropertyValue(getPropertyManager().getEmailAddressPropertyName());
            List emailsTo = new ArrayList<String>();

            if (emailSubject.equals(CPSEmailConstants.EMAIL_NAME_REORDER_LIST)) {
                emailsTo = (List) mailing.getPropertyValue(CPSEmailConstants.EMAIL_ADDRESSES);
            } else {
                emailsTo.add(emailTo);
            }

            // HashMap params = (HashMap)
            // mailing.getPropertyValue(MAILING_PARAMS);

            String emailTemplate = (String) mailing.getPropertyValue(CPSEmailConstants.EMAIL_TEMPLATE);

            try {
                // getTemplateEmailSender().sendEmailMessage(templateInfo,recipients);
                sendSuccess = sendEmailNotification(emailsTo, getEmailParams(mailing, profile), emailTemplate, emailSubject);
            } catch (Exception e) {
                vlogError(e, "Error");
                sendSuccess = false;
            }
        }

        return sendSuccess;

    }
    
    public boolean sendEmailConfirmationReminder(RepositoryItem pProfile, String confirmURL) {
    	boolean flag = true;
        try {

            PropertyManager pmgr = getPropertyManager();
            String email = (String) pProfile.getPropertyValue(pmgr.getEmailAddressPropertyName());
            String name = (String) pProfile.getPropertyValue(pmgr.getFirstNamePropertyName());
            if (isLoggingDebug()) {
                logDebug("Sending activation letter to: " + email);
            }

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            populateRealTimeParams(emailParams, pProfile);
            emailParams.put(CPSEmailConstants.PARAM_CONFIRMATION_LINK, getSiteUrl(siteId) + confirmURL);
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, name);
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, email);
            emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.REGISTER_EMAIL);

            List confirmationRecipients = new ArrayList();
            confirmationRecipients.add(email);

            String cpsSubject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_NAME_EMAIL_CONFIRMATION_REMINDER), emailParams);
            flag = flag && sendEmailNotification(confirmationRecipients, emailParams, getTemplateReminderConfirmationEmail(), cpsSubject);

        } catch (Exception ex) {
            vlogError("Exception while sending mail : {0}",ex.getMessage());
        }
        return flag;
    }
    
    public boolean sendOrderConfirmationEmailToAdmin(RepositoryItem adminProfile,RepositoryItem profile, Order order, String siteId) {
        boolean flag = true;
        try {
            PropertyManager pmgr = getPropertyManager();
            String adminEmail = (String) adminProfile.getPropertyValue(pmgr.getEmailAddressPropertyName());
            String buyerEmail = (String) profile.getPropertyValue(pmgr.getEmailAddressPropertyName());
            String buyerFirstName = (String) profile.getPropertyValue(pmgr.getFirstNamePropertyName());
            String buyerLastName = (String) profile.getPropertyValue(pmgr.getLastNamePropertyName());
            if (isLoggingDebug()) {
                logDebug("sendOrderConfirmationEmail..................email...." + adminEmail);
                logDebug("sendOrderConfirmationEmail..................order...." + order);
                logDebug("siteId....................." + siteId);
            }

            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

            HashMap<String, Object> emailParams = new HashMap<String, Object>();
            emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
            emailParams.put(CPSEmailConstants.ORDER_ID, ((CPSOrderImpl) order).getWebOrderId());
            emailParams.put(CPSEmailConstants.ORDER_OBJ, order);
            emailParams.put(CPSEmailConstants.PARAM_ORDER, order);
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER, ((CPSOrderImpl) order).getWebOrderId());
            emailParams.put(CPSEmailConstants.PARAM_ORDER_DATE, sdf.format(order.getSubmittedDate()));
            emailParams.put(CPSEmailConstants.PARAM_ORDER_NUMBER_ITEMS, order.getCommerceItems().size());
            emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, buyerFirstName);
            emailParams.put(CPSEmailConstants.PARAM_USER_LAST_NAME, buyerLastName);
            emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, buyerEmail);

            String poNumber = null;
            List<PaymentGroup> paymentGroups = order.getPaymentGroups();
            if (paymentGroups != null && paymentGroups.size() > 0) {
                for (PaymentGroup paymentGroup : paymentGroups) {
                    if (paymentGroup instanceof InvoiceRequest) {
                        InvoiceRequest invoiceRequest = (InvoiceRequest) paymentGroup;
                        poNumber = invoiceRequest.getPONumber();
                        break;
                    }
                }
            }
            emailParams.put(CPSEmailConstants.PARAM_ORDER_PO_NUMBER, poNumber);

            String poNumberPart = "";

            if (poNumber != null) {
                Map<String, Object> poPartParams = new HashMap<>();
                poPartParams.put(CPSEmailConstants.PARAM_ORDER_PO_NUMBER, poNumber);

                poNumberPart = DataUtils.paramsMessage(messageUtils.getMessage("emailBodyOrderConfirmationPONumberPart"), poPartParams);
            }
            emailParams.put(PARAM_PO_PART, poNumberPart);

            List shippingGroups = order.getShippingGroups();

            if (shippingGroups != null && shippingGroups.size() > 0) {
                ShippingGroupImpl sg = (ShippingGroupImpl) shippingGroups.get(0);

                if (sg instanceof HardgoodShippingGroup) {
                    ContactInfo address = (ContactInfo) ((HardgoodShippingGroup) sg).getShippingAddress();
                    if (address != null) {
                        emailParams.put(CPSEmailConstants.PARAM_SHIPTO_OR_PICKUP, "Ship to Address: ");
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_1, address.getAddress1());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_2, getAddress2ForOrderConfirm(address.getAddress2(), address.getAddress3()));
                        // emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_3, address.getAddress3());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_CITY, address.getCity());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_STATE, address.getState());
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_ZIP, address.getPostalCode());
                    }
                } else if (sg instanceof CPSInStorePickupShippingGroup) {
                    RepositoryItem store = ((CPSInStorePickupShippingGroup) sg).getStore();

                    if (store != null) {
                        emailParams.put(CPSEmailConstants.PARAM_SHIPTO_OR_PICKUP, "Pick up in store: ");
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_1, store.getPropertyValue(CPSConstants.ADDRESS1));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_2, getAddress2ForOrderConfirm(
                                        (String) store.getPropertyValue(CPSConstants.ADDRESS2), (String) store.getPropertyValue(CPSConstants.ADDRESS3)));
                        // emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_3, store.getPropertyValue(CPSConstants.ADDRESS3));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_CITY, store.getPropertyValue(CPSConstants.CITY));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_STATE, store.getPropertyValue(CPSConstants.STATE_ADDRESS));
                        emailParams.put(CPSEmailConstants.PARAM_ORDER_ADDRESS_ZIP, store.getPropertyValue(CPSConstants.POSTAL_CODE));
                    }

                }

            }

            NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(new Locale(getCurrencyLocale(), getCurrencyCountry()));
            OrderPriceInfo priceInfo = order.getPriceInfo();
            if (priceInfo != null) {
                emailParams.put(CPSEmailConstants.PARAM_ORDER_SUBTOTAL, currencyFormatter.format(priceInfo.getRawSubtotal()));
                emailParams.put(CPSEmailConstants.PARAM_ORDER_SHIPPING, currencyFormatter.format(priceInfo.getShipping()));
                emailParams.put(CPSEmailConstants.PARAM_ORDER_TAX, currencyFormatter.format(priceInfo.getTax()));
                emailParams.put(CPSEmailConstants.PARAM_ORDER_TOTAL, currencyFormatter.format(priceInfo.getTotal()));
            }

            populateRealTimeParams(emailParams, profile);
            List recipients = new ArrayList();
            recipients.add(adminEmail);

            flag = sendEmailNotification(recipients, emailParams, getTemplateOrderConfirmationAdminURL(), CPSEmailConstants.EMAIL_NAME_ORDER_CONFIRMATION_NOTIFICATION);
			if (!flag) {
				getProfileTools().createMailingItem(profile,
						CPSEmailConstants.EMAIL_NAME_ORDER_CONFIRMATION_NOTIFICATION,
						getTemplateOrderConfirmationAdminURL(), null, order, siteId);
			}

        } catch (Exception ex) {
            vlogError(ex, "Error");
        }
        return flag;
    }

	/**
	 * @param pProfileItem
	 * @param pOrder
	 * @return
	 */
	public boolean sendAbandonedOrderEmailNotification(RepositoryItem pProfileItem, Order pOrder) {
		vlogInfo("sendAbandonedOrderEmailNotification...");
		boolean flag = false;
		PropertyManager pmgr = getPropertyManager();
		HashMap<String, Object> emailParams = new HashMap<String, Object>();
		emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));
		emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
        emailParams.put(CPSEmailConstants.ORDER_OBJ, pOrder);
        emailParams.put(CPSEmailConstants.PARAM_ORDER, pOrder);
        String emailAddress = (String) pProfileItem.getPropertyValue(pmgr.getEmailAddressPropertyName());
        
        RepositoryItem derivedBillingAddressItem = (RepositoryItem) pProfileItem.getPropertyValue(CPSConstants.BILLING_ADDRESS);
        Set<String> workGroupMails =  new HashSet<String>();
        if(derivedBillingAddressItem!=null){
        	String csId = derivedBillingAddressItem.getRepositoryId();
            RepositoryItem workGroupItem = (RepositoryItem) derivedBillingAddressItem.getPropertyValue(CPSConstants.WORKGROUP_NUMBER);
            if(workGroupItem != null){
                vlogDebug("WorkGroupId :: {0}", workGroupItem.getRepositoryId());
            	workGroupMails = (Set<String>) workGroupItem.getPropertyValue(CPSConstants.WORKGROUP_EMAILS);
                vlogDebug("Work group emails :: {0}", workGroupMails);
            }
        }                
        if (CollectionUtils.isEmpty(workGroupMails)) {
            workGroupMails.add(getDefaultWorkGroupMail());
        }

        List recipientsBcc = new ArrayList();
        List recipients = new ArrayList();
        recipients.add(emailAddress);
        recipientsBcc.addAll(getEmailToCPSMarketing());
        recipientsBcc.addAll(workGroupMails);

		String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ABANDONED_ORDER_NOTIFICATION), emailParams);
		flag = sendWorkGroupMails(recipients, emailParams, getTemplateAbandonedOrderNotificationURL(), subject, null, recipientsBcc, null);

		return flag;
	}
    /**
     * Setting email specific parameters
     *
     * @param mailingItem
     * @param profile
     * @return
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    private HashMap getEmailParams(RepositoryItem mailingItem, RepositoryItem profile) {
        HashMap emailParams = new HashMap();
        if (mailingItem != null && profile != null) {
            emailParams.put(PRODUCTION_URL, getSiteUrl((String) mailingItem.getPropertyValue(SITE_ID)));
            String emailName = (String) mailingItem.getPropertyValue(CPSEmailConstants.EMAIL_SUBJECT);
            if (!StringUtils.isBlank(emailName)) {
                if (emailName.equalsIgnoreCase(CPSEmailConstants.EMAIL_NAME_REGISTRATION)) {
                    emailParams.put(CPSEmailConstants.FIRST_NAME, profile.getPropertyValue(CPSEmailConstants.FIRST_NAME));
                    emailParams.put(CPSEmailConstants.LAST_NAME, profile.getPropertyValue(CPSEmailConstants.LAST_NAME));
                } else if (emailName.equalsIgnoreCase(CPSEmailConstants.EMAIL_NAME_USER_UPDATE)) {
                    emailParams.put(CPSEmailConstants.FIRST_NAME, profile.getPropertyValue(CPSEmailConstants.FIRST_NAME));
                    emailParams.put(CPSEmailConstants.LAST_NAME, profile.getPropertyValue(CPSEmailConstants.LAST_NAME));
                } else if (emailName.equalsIgnoreCase(CPSEmailConstants.EMAIL_NAME_FORGOT_PSWD)) {
                    emailParams.put(CPSEmailConstants.FIRST_NAME, profile.getPropertyValue(CPSEmailConstants.FIRST_NAME));
                    emailParams.put(CPSEmailConstants.LAST_NAME, profile.getPropertyValue(CPSEmailConstants.LAST_NAME));
                    emailParams.put(CPSEmailConstants.LINK, mailingItem.getPropertyValue(CPSEmailConstants.EMAIL_FORGOT_PSWD_LINK));
                } else if (emailName.equalsIgnoreCase(CPSEmailConstants.EMAIL_NAME_ORDER_CONFIRMATION)) {
                    String orderId = (String) mailingItem.getPropertyValue(CPSEmailConstants.ORDER_ID);
                    emailParams.put(CPSEmailConstants.FIRST_NAME, profile.getPropertyValue(CPSEmailConstants.FIRST_NAME));
                    emailParams.put(CPSEmailConstants.LAST_NAME, profile.getPropertyValue(CPSEmailConstants.LAST_NAME));
                    emailParams.put(CPSEmailConstants.ORDER_ID, orderId);
                } else if (emailName.equalsIgnoreCase(CPSEmailConstants.EMAIL_NAME_REORDER_LIST)) {
                    List emailAddresses = (List) mailingItem.getPropertyValue(CPSEmailConstants.EMAIL_ADDRESSES);
                    if (emailAddresses != null) {
                        emailParams.put(CPSEmailConstants.REORDER_EMAIL_ADDR, emailAddresses.toString());
                    }
                    emailParams.put(CPSEmailConstants.REORDER_NOTES, mailingItem.getPropertyValue(CPSEmailConstants.REORDER_NOTES));
                    emailParams.put(CPSEmailConstants.GIFT_LIST_ID, mailingItem.getPropertyValue(CPSEmailConstants.ORDER_ID));
                    emailParams.put(CPSEmailConstants.FIRST_NAME, profile.getPropertyValue(CPSEmailConstants.FIRST_NAME));
                    emailParams.put(CPSEmailConstants.LAST_NAME, profile.getPropertyValue(CPSEmailConstants.LAST_NAME));
                }
            }
        }
        return emailParams;
    }

    private Map<String, ?> generateTemplatedEmailParameters(Map<String, Object> values) {
        Map<String, String> result = new HashMap<String, String>();

        if (values != null) {
            if (!values.isEmpty()) {
                for (Entry<String, ?> entry : values.entrySet()) {
                    String key = entry.getKey();
                    if (key != null && key.startsWith("PARAM_")) {
                        Object value = entry.getValue();
                        result.put(key, value == null ? null : value.toString());
                    }
                }
            }

            values.put(CPSEmailConstants.EMAIL_PARAMS, result);
        }

        return result;
    }

    private String getAddress2ForOrderConfirm(String address2, String address3) {
        if (!StringUtils.isEmpty(address2) && !StringUtils.isEmpty(address3)) {
            return address2 + ", " + address3 + "<br>";
        } else if (!StringUtils.isEmpty(address2)) {
            return address2 + "<br>";
        } else {
            return "";
        }
    }

    public HashMap populateRealTimeParams(HashMap<String, Object> emailParams, RepositoryItem profile) {
        if (profile != null) {
            PropertyManager pmgr = getPropertyManager();
            String fName = (String) profile.getPropertyValue(pmgr.getFirstNamePropertyName());
            String lName = (String) profile.getPropertyValue(pmgr.getLastNamePropertyName());
            String emailAddress = (String) profile.getPropertyValue(pmgr.getEmailAddressPropertyName());

            vlogDebug("fname: " + fName + " lname: " + lName + " emailAddress: " + emailAddress);
            emailParams.put(CPSEmailConstants.FIRST_NAME, fName);
            emailParams.put(CPSEmailConstants.LAST_NAME, lName);
            emailParams.put(CPSEmailConstants.EMAIL_ADDRESS, emailAddress);

            // Leaving existing params to avoid breaking other e-mails that may
            // be unrelated to the BCC-templated ones
            emailParams.put(CPSEmailConstants.PARAM_FIRST_NAME, fName);
            emailParams.put(CPSEmailConstants.PARAM_LAST_NAME, lName);
            emailParams.put(CPSEmailConstants.PARAM_EMAIL_ADDRESS, emailAddress);
        }

        emailParams.put(CPSEmailConstants.PARAM_PRODUCTION_URL, getSiteUrl(siteId));

        return emailParams;
    }

    private SiteContextManager mSiteContextManager;
    private CPSSiteURLManager siteURLManager;
    private PropertyManager propertyManager;

    public PropertyManager getPropertyManager() {
        return propertyManager;
    }

    public void setPropertyManager(PropertyManager propertyManager) {
        this.propertyManager = propertyManager;
    }

    public CPSSiteURLManager getSiteURLManager() {
        return siteURLManager;
    }

    public void setSiteURLManager(CPSSiteURLManager siteURLManager) {
        this.siteURLManager = siteURLManager;
    }

    public SiteContextManager getSiteContextManager() {
        return mSiteContextManager;
    }

    public void setSiteContextManager(SiteContextManager pSiteContextManager) {
        mSiteContextManager = pSiteContextManager;
    }

    public TemplateEmailSender getTemplateEmailSender() {
        return templateEmailSender;
    }

    public void setTemplateEmailSender(TemplateEmailSender templateEmailSender) {
        this.templateEmailSender = templateEmailSender;
    }

    public String getTemplateCreateProfileURL() {
        return templateCreateProfileURL;
    }

    public void setTemplateCreateProfileURL(String templateCreateProfileURL) {
        this.templateCreateProfileURL = templateCreateProfileURL;
    }

    public String getTemplateUpdateProfileURL() {
        return templateUpdateProfileURL;
    }

    public void setTemplateUpdateProfileURL(String templateUpdateProfileURL) {
        this.templateUpdateProfileURL = templateUpdateProfileURL;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public TemplateEmailBatchPersister getTemplateEmailBatchPersister() {
        return templateEmailBatchPersister;
    }

    public void setTemplateEmailBatchPersister(TemplateEmailBatchPersister templateEmailBatchPersister) {
        this.templateEmailBatchPersister = templateEmailBatchPersister;
    }

    public CPSProfileTools getProfileTools() {
        return profileTools;
    }

    public void setProfileTools(CPSProfileTools profileTools) {
        this.profileTools = profileTools;
    }

    public IMessageUtils getMessageUtils() {
        return messageUtils;
    }

    public void setMessageUtils(IMessageUtils messageUtils) {
        this.messageUtils = messageUtils;
    }

    public void setTransactionManager(TransactionManager pTransactionManager) {
        mTransactionManager = pTransactionManager;
    }

    public TransactionManager getTransactionManager() {
        return mTransactionManager;
    }

    public String getCurrencyLocale() {
        return currencyLocale;
    }

    public void setCurrencyLocale(String currencyLocale) {
        this.currencyLocale = currencyLocale;
    }

    public String getCurrencyCountry() {
        return currencyCountry;
    }

    public void setCurrencyCountry(String currencyCountry) {
        this.currencyCountry = currencyCountry;
    }

    public String getSubjectLineTruncateRegex() {
        return subjectLineTruncateRegex;
    }

    public void setSubjectLineTruncateRegex(String subjectLineTruncateRegex) {
        this.subjectLineTruncateRegex = subjectLineTruncateRegex;
    }

    public Long getSubjectLineMaxLength() {
        return subjectLineMaxLength;
    }

    public void setSubjectLineMaxLength(Long subjectLineMaxLength) {
        this.subjectLineMaxLength = subjectLineMaxLength;
    }

    public String getTemplateContactUsURL() {
        return templateContactUsURL;
    }

    public void setTemplateContactUsURL(String templateContactUsURL) {
        this.templateContactUsURL = templateContactUsURL;
    }

    public String getTemplateRequestCurrentStatementCopy() {
        return templateRequestCurrentStatementCopy;
    }

    public void setTemplateRequestCurrentStatementCopy(String templateRequestCurrentStatementCopy) {
        this.templateRequestCurrentStatementCopy = templateRequestCurrentStatementCopy;
    }

    public String getEmailToCPSService() {
        return emailToCPSService;
    }

    public void setEmailToCPSService(String emailToCPSService) {
        this.emailToCPSService = emailToCPSService;
    }

    public String getEmailToCPSRegionalManagerOverride() {
        return emailToCPSRegionalManagerOverride;
    }

    public void setEmailToCPSRegionalManagerOverride(String emailToCPSRegionalManagerOverride) {
        this.emailToCPSRegionalManagerOverride = emailToCPSRegionalManagerOverride;
    }

    public String getEmailToCPSQuoteRequest() {
        return emailToCPSQuoteRequest;
    }

    public void setEmailToCPSQuoteRequest(String emailToCPSQuoteRequest) {
        this.emailToCPSQuoteRequest = emailToCPSQuoteRequest;
    }

    public String getEmailToCPSPriceListUpdateRequest() {
        return emailToCPSPriceListUpdateRequest;
    }

    public void setEmailToCPSPriceListUpdateRequest(String emailToCPSPriceListUpdateRequest) {
        this.emailToCPSPriceListUpdateRequest = emailToCPSPriceListUpdateRequest;
    }

    public String getEmailToCPSTestimonial() {
        return emailToCPSTestimonial;
    }

    public void setEmailToCPSTestimonial(String emailToCPSTestimonial) {
        this.emailToCPSTestimonial = emailToCPSTestimonial;
    }

    public String getTemplateContactUsCPSURL() {
        return templateContactUsCPSURL;
    }

    public void setTemplateContactUsCPSURL(String templateContactUsCPSURL) {
        this.templateContactUsCPSURL = templateContactUsCPSURL;
    }

    public Map<String, String> getCpsEmailAddresses() {
        return cpsEmailAddresses;
    }

    public void setCpsEmailAddresses(Map<String, String> cpsEmailAddresses) {
        this.cpsEmailAddresses = cpsEmailAddresses;
    }

    public String getTemplateCheckoutAvailable() {
        return templateCheckoutAvailable;
    }

    public void setTemplateCheckoutAvailable(String templateCheckoutAvailable) {
        this.templateCheckoutAvailable = templateCheckoutAvailable;
    }

    /************************/
    /************************/
    /************************/
    /************************/

    public void setTemplateAbandonedCartURL(String templateAbandonedCartURL) {
        this.templateAbandonedCartURL = templateAbandonedCartURL;
    }

    public String getTemplateAbandonedCartURL() {
        return templateAbandonedCartURL;
    }

    public void setTemplateAdminAddedUserURL(String templateAdminAddedUserURL) {
        this.templateAdminAddedUserURL = templateAdminAddedUserURL;
    }

    public String getTemplateAdminAddedUserURL() {
        return templateAdminAddedUserURL;
    }

    public void setTemplateCPSServicesContactUs(String templateCPSServicesContactUs) {
        this.templateCPSServicesContactUs = templateCPSServicesContactUs;
    }

    public String getTemplateCPSServicesContactUs() {
        return templateCPSServicesContactUs;
    }

    public void setTemplateCPSRegionalManagerContactUs(String templateCPSRegionalManagerContactUs) {
        this.templateCPSRegionalManagerContactUs = templateCPSRegionalManagerContactUs;
    }

    public String getTemplateCPSRegionalManagerContactUs() {
        return templateCPSRegionalManagerContactUs;
    }

    public void setTemplateCPSDeactivatedUserURL(String templateCPSDeactivatedUserURL) {
        this.templateCPSDeactivatedUserURL = templateCPSDeactivatedUserURL;
    }

    public String getTemplateCPSDeactivatedUserURL() {
        return templateCPSDeactivatedUserURL;
    }

    public void setTemplateNewUserWelcomeURL(String templateNewUserWelcomeURL) {
        this.templateNewUserWelcomeURL = templateNewUserWelcomeURL;
    }

    public String getTemplateNewUserWelcomeURL() {
        return templateNewUserWelcomeURL;
    }

    public void setTemplateOrderApprovalExpiredURL(String templateOrderApprovalExpiredURL) {
        this.templateOrderApprovalExpiredURL = templateOrderApprovalExpiredURL;
    }

    public String getTemplateOrderApprovalExpiredURL() {
        return templateOrderApprovalExpiredURL;
    }

    public void setTemplateOrderUpdateFailed(String templateOrderUpdateFailed) {
        this.templateOrderUpdateFailed = templateOrderUpdateFailed;
    }

    public String getTemplateOrderUpdateFailed() {
        return templateOrderUpdateFailed;
    }

    public void setTemplateOrderApprovalRequiredURL(String templateOrderApprovalRequiredURL) {
        this.templateOrderApprovalRequiredURL = templateOrderApprovalRequiredURL;
    }

    public String getTemplateOrderApprovalRequiredURL() {
        return templateOrderApprovalRequiredURL;
    }

    public void setTemplateOrderRequestReceivedURL(String pTemplateOrderRequestReceivedURL) {
        mTemplateOrderRequestReceivedURL = pTemplateOrderRequestReceivedURL;
    }

    public String getTemplateOrderRequestReceivedURL() {
        return mTemplateOrderRequestReceivedURL;
    }

    public void setTemplateOrderApprovalPendingURL(String templateOrderApprovalPendingURL) {
        this.templateOrderApprovalPendingURL = templateOrderApprovalPendingURL;
    }

    public String getTemplateOrderApprovalPendingURL() {
        return templateOrderApprovalPendingURL;
    }

    public void setTemplateOrderApprovedURL(String templateOrderApprovedURL) {
        this.templateOrderApprovedURL = templateOrderApprovedURL;
    }

    public String getTemplateOrderApprovedURL() {
        return templateOrderApprovedURL;
    }

    public void setTemplateOrderConfirmationURL(String templateOrderConfirmationURL) {
        this.templateOrderConfirmationURL = templateOrderConfirmationURL;
    }

    public String getTemplateOrderConfirmationURL() {
        return templateOrderConfirmationURL;
    }

    public void setTemplateOrderRejectedURL(String templateOrderRejectedURL) {
        this.templateOrderRejectedURL = templateOrderRejectedURL;
    }

    public String getTemplateOrderRejectedURL() {
        return templateOrderRejectedURL;
    }

    public void setTemplatePasswordResetConfirmationURL(String templatePasswordResetConfirmationURL) {
        this.templatePasswordResetConfirmationURL = templatePasswordResetConfirmationURL;
    }

    public String getTemplatePasswordResetConfirmationURL() {
        return templatePasswordResetConfirmationURL;
    }

    public void setTemplatePasswordResetLinkURL(String templatePasswordResetLinkURL) {
        this.templatePasswordResetLinkURL = templatePasswordResetLinkURL;
    }

    public String getTemplatePasswordResetLinkURL() {
        return templatePasswordResetLinkURL;
    }

    public void setTemplatePriceListUpdateConfirmationURL(String templatePriceListUpdateConfirmationURL) {
        this.templatePriceListUpdateConfirmationURL = templatePriceListUpdateConfirmationURL;
    }

    public String getTemplatePriceListUpdateConfirmationURL() {
        return templatePriceListUpdateConfirmationURL;
    }

    public void setTemplateQuoteRequestedURL(String templateQuoteRequestedURL) {
        this.templateQuoteRequestedURL = templateQuoteRequestedURL;
    }

    public String getTemplateQuoteRequestedURL() {
        return templateQuoteRequestedURL;
    }

    public void setTemplateShareMaterialListURL(String templateShareMaterialListURL) {
        this.templateShareMaterialListURL = templateShareMaterialListURL;
    }

    public String getTemplateShareMaterialListURL() {
        return templateShareMaterialListURL;
    }

    public void setTemplateSharePageURL(String templateSharePageURL) {
        this.templateSharePageURL = templateSharePageURL;
    }

    public String getTemplateSharePageURL() {
        return templateSharePageURL;
    }

    public void setTemplateTemporaryPasswordURL(String templateTemporaryPasswordURL) {
        this.templateTemporaryPasswordURL = templateTemporaryPasswordURL;
    }

    public String getTemplateTemporaryPasswordURL() {
        return templateTemporaryPasswordURL;
    }

    public void setTemplateTestimonialURL(String templateTestimonialURL) {
        this.templateTestimonialURL = templateTestimonialURL;
    }

    public String getTemplateTestimonialURL() {
        return templateTestimonialURL;
    }

    public void setTemplateUserProfileUpdatedURL(String templateUserProfileUpdatedURL) {
        this.templateUserProfileUpdatedURL = templateUserProfileUpdatedURL;
    }

    public String getTemplateUserProfileUpdatedURL() {
        return templateUserProfileUpdatedURL;
    }

    public String getTemplateOrderRejectedNoCommentsURL() {
        return templateOrderRejectedNoCommentsURL;
    }

    public void setTemplateOrderRejectedNoCommentsURL(String templateOrderRejectedNoCommentsURL) {
        this.templateOrderRejectedNoCommentsURL = templateOrderRejectedNoCommentsURL;
    }

    public String getTemplateEmailConfirmation() {
        return templateEmailConfirmation;
    }

    public void setTemplateEmailConfirmation(String templateEmailConfirmation) {
        this.templateEmailConfirmation = templateEmailConfirmation;
    }

    public String getTemplateNewUserRegistered() {
        return templateNewUserRegistered;
    }

    public void setTemplateNewUserRegistered(String templateNewUserRegistered) {
        this.templateNewUserRegistered = templateNewUserRegistered;
    }

    public String getTemplateAccountActivated() {
        return templateAccountActivated;
    }

    public void setTemplateAccountActivated(String templateAccountActivated) {
        this.templateAccountActivated = templateAccountActivated;
    }

    public String getEmailToCPSAdmin() {
        return emailToCPSAdmin;
    }

    public void setEmailToCPSAdmin(String emailToCPSAdmin) {
        this.emailToCPSAdmin = emailToCPSAdmin;
    }

    public String getTemplateApplicationCreditURL() {
        return templateApplicationCreditURL;
    }

    public void setTemplateApplicationCreditURL(String templateApplicationCreditURL) {
        this.templateApplicationCreditURL = templateApplicationCreditURL;
    }

    public String getTemplateUserTestimonial() {
        return templateUserTestimonial;
    }

    public void setTemplateUserTestimonial(String templateUserTestimonial) {
        this.templateUserTestimonial = templateUserTestimonial;
    }

    public String getTemplateUserPriceListUpdateConfirmation() {
        return templateUserPriceListUpdateConfirmation;
    }

    public void setTemplateUserPriceListUpdateConfirmation(String templateUserPriceListUpdateConfirmation) {
        this.templateUserPriceListUpdateConfirmation = templateUserPriceListUpdateConfirmation;
    }

    public String getTemplateUserRegionalManagerContactUs() {
        return templateUserRegionalManagerContactUs;
    }

    public void setTemplateUserRegionalManagerContactUs(String templateUserRegionalManagerContactUs) {
        this.templateUserRegionalManagerContactUs = templateUserRegionalManagerContactUs;
    }

    public String getTemplateUserQuoteRequested() {
        return templateUserQuoteRequested;
    }

    public void setTemplateUserQuoteRequested(String templateUserQuoteRequested) {
        this.templateUserQuoteRequested = templateUserQuoteRequested;
    }

    public String getTemplateUserRequestCurrentStatementCopy() {
        return templateUserRequestCurrentStatementCopy;
    }

    public void setTemplateUserRequestCurrentStatementCopy(String templateUserRequestCurrentStatementCopy) {
        this.templateUserRequestCurrentStatementCopy = templateUserRequestCurrentStatementCopy;
    }

    public String getTemplateUserSavedCartReminder() {
        return templateUserSavedCartReminder;
    }

    public void setTemplateUserSavedCartReminder(String templateUserSavedCartReminder) {
        this.templateUserSavedCartReminder = templateUserSavedCartReminder;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public boolean isTest() {
        return mTest;
    }

    public void setTest(boolean pTest) {
        mTest = pTest;
    }

    public String getTemplateRequestAccess() {
        return templateRequestAccess;
    }

    public void setTemplateRequestAccess(String pTemplateRequestAccess) {
        templateRequestAccess = pTemplateRequestAccess;
    }

    public String getRequestAccessEmail() {
        return requestAccessEmail;
    }

    public void setRequestAccessEmail(String pRequestAccessEmail) {
        requestAccessEmail = pRequestAccessEmail;
    }

    public String getRequestStatementEmail() {
        return requestStatementEmail;
    }

    public void setRequestStatementEmail(String requestStatementEmail) {
        this.requestStatementEmail = requestStatementEmail;
    }

    public String getEmailToCreateAccount() {
        return emailToCreateAccount;
    }

    public void setEmailToCreateAccount(String emailToCreateAccount) {
        this.emailToCreateAccount = emailToCreateAccount;
    }

    public boolean isRequestAccessSendCopyToApprover() {
        return requestAccessSendCopyToApprover;
    }

    public void setRequestAccessSendCopyToApprover(boolean pRequestAccessSendCopyToApprover) {
        requestAccessSendCopyToApprover = pRequestAccessSendCopyToApprover;
    }

    public List getAllEmailsType() {
        return allEmailsType;
    }

    public void setAllEmailsType(List allEmailsType) {
        this.allEmailsType = allEmailsType;
    }

    /**
     * @return the templatePDPErrorURL
     */
    public String getTemplatePDPErrorURL() {
        return templatePDPErrorURL;
    }

    /**
     * @param templatePDPErrorURL
     *            the templatePDPErrorURL to set
     */
    public void setTemplatePDPErrorURL(String templatePDPErrorURL) {
        this.templatePDPErrorURL = templatePDPErrorURL;
    }

    /**
     * @return the emailToCPSPdpError
     */
    public String getEmailToCPSPdpError() {
        return emailToCPSPdpError;
    }

    /**
     * @param emailToCPSPdpError
     *            the emailToCPSPdpError to set
     */
    public void setEmailToCPSPdpError(String emailToCPSPdpError) {
        this.emailToCPSPdpError = emailToCPSPdpError;
    }

    public String getDefaultWorkGroupMail() {
        return defaultWorkGroupMail;
    }

    public void setDefaultWorkGroupMail(String defaultWorkGroupMail) {
        this.defaultWorkGroupMail = defaultWorkGroupMail;
    }

    /**
     * @return the templateFeedImport
     */
    public String getTemplateFeedImport() {
        return templateFeedImport;
    }

    /**
     * @param templateFeedImport
     *            the templateFeedImport to set
     */
    public void setTemplateFeedImport(String templateFeedImport) {
        this.templateFeedImport = templateFeedImport;
    }

    /**
     * @return the emailForFeedStatusmails
     */
    public String getEmailForFeedStatusmails() {
        return emailForFeedStatusmails;
    }

    /**
     * @param emailForFeedStatusmails
     *            the emailForFeedStatusmails to set
     */
    public void setEmailForFeedStatusmails(String emailForFeedStatusmails) {
        this.emailForFeedStatusmails = emailForFeedStatusmails;
    }

	/**
	 * @return the anonymousUserName
	 */
	public String getAnonymousUserName() {
		return anonymousUserName;
	}

	/**
	 * @param anonymousUserName the anonymousUserName to set
	 */
	public void setAnonymousUserName(String anonymousUserName) {
		this.anonymousUserName = anonymousUserName;
	}

	/**
	 * @return the anonymousUserEmail
	 */
	public String getAnonymousUserEmail() {
		return anonymousUserEmail;
	}

	/**
	 * @param anonymousUserEmail the anonymousUserEmail to set
	 */
	public void setAnonymousUserEmail(String anonymousUserEmail) {
		this.anonymousUserEmail = anonymousUserEmail;
	}

	/**
	 * @return the templateNewUserReport
	 */
	public String getTemplateNewUserReport() {
		return templateNewUserReport;
	}

	/**
	 * @param templateNewUserReport the templateNewUserReport to set
	 */
	public void setTemplateNewUserReport(String templateNewUserReport) {
		this.templateNewUserReport = templateNewUserReport;
	}

	/**
	 * @return the templateNewUserNotActivatedReport
	 */
	public String getTemplateNewUserNotActivatedReport() {
		return templateNewUserNotActivatedReport;
	}

	/**
	 * @param templateNewUserNotActivatedReport the templateNewUserNotActivatedReport to set
	 */
	public void setTemplateNewUserNotActivatedReport(String templateNewUserNotActivatedReport) {
		this.templateNewUserNotActivatedReport = templateNewUserNotActivatedReport;
	}

	/**
	 * @return the templateReminderConfirmationEmail
	 */
	public String getTemplateReminderConfirmationEmail() {
		return templateReminderConfirmationEmail;
	}

	/**
	 * @param templateReminderConfirmationEmail the templateReminderConfirmationEmail to set
	 */
	public void setTemplateReminderConfirmationEmail(String templateReminderConfirmationEmail) {
		this.templateReminderConfirmationEmail = templateReminderConfirmationEmail;
	}

	/**
	 * @return the templateOrderConfirmationAdminURL
	 */
	public String getTemplateOrderConfirmationAdminURL() {
		return templateOrderConfirmationAdminURL;
	}

	/**
	 * @param templateOrderConfirmationAdminURL the templateOrderConfirmationAdminURL to set
	 */
	public void setTemplateOrderConfirmationAdminURL(String templateOrderConfirmationAdminURL) {
		this.templateOrderConfirmationAdminURL = templateOrderConfirmationAdminURL;
	}

	/**
	 * @return the templateMissingWebOrderURL
	 */
	public String getTemplateMissingWebOrderURL() {
		return templateMissingWebOrderURL;
	}

	/**
	 * @param templateMissingWebOrderURL the templateMissingWebOrderURL to set
	 */
	public void setTemplateMissingWebOrderURL(String templateMissingWebOrderURL) {
		this.templateMissingWebOrderURL = templateMissingWebOrderURL;
	}

	/**
	 * 
	 * @return templateAbandonedOrderNotificationURL
	 */
	public String getTemplateAbandonedOrderNotificationURL() {
		return templateAbandonedOrderNotificationURL;
	}

	/**
	 * 
	 * @param templateAbandonedOrderNotificationURL
	 */
	public void setTemplateAbandonedOrderNotificationURL(String templateAbandonedOrderNotificationURL) {
		this.templateAbandonedOrderNotificationURL = templateAbandonedOrderNotificationURL;
	}

	/**
	 * @return the emailToCPSMarketing
	 */
	public List<String> getEmailToCPSMarketing() {
		return emailToCPSMarketing;
	}

	/**
	 * @param emailToCPSMarketing the emailToCPSMarketing to set
	 */
	public void setEmailToCPSMarketing(List<String> emailToCPSMarketing) {
		this.emailToCPSMarketing = emailToCPSMarketing;
	}

	/**
	 * 
	 */
	public void sendJsonBaseFeedStatusEmail() {
		vlogInfo("called common email sender..............");
		if(getCpsGlobalProperties().getSkippedItems().size()>0){
			String skippedItems = getCpsGlobalProperties().getSkippedItems().toString();
			StringBuilder sb = new StringBuilder();
			
			
	        HashMap<String, Object> emailParams = new HashMap<String, Object>();
	        emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
	        String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_JSON_PARSE_SCHEDULER), emailParams);
	        String bodyMessage = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_BODY_JSON_PARSE_SCHEDULER), emailParams);
	        sb.append(bodyMessage);
	        sb.append(skippedItems);
	        emailParams.put("PARAM_FEED_STATUS_TEXT", sb.toString());
	        sendEmailNotification(getJsonFeedStatusEmails(), emailParams, getTemplateFeedImport(), subject);
			
			getCpsGlobalProperties().getSkippedItems().clear();
		}
	}	
	public void sendPriceFeedStatusEmail() {
		vlogInfo("called common email sender..............");
		if(getCpsGlobalProperties().getErrorProducts().size()>0){
			String skippedItems = getCpsGlobalProperties().getErrorProducts().toString();
			StringBuilder sb = new StringBuilder();
			
			
	        HashMap<String, Object> emailParams = new HashMap<String, Object>();
	        emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
	        String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_PRICE_PARSE_SCHEDULER), emailParams);
	        String bodyMessage = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_BODY_PRICE_PARSE_SCHEDULER), emailParams);
	        sb.append(bodyMessage);
	        sb.append(skippedItems);
	        emailParams.put("PARAM_FEED_STATUS_TEXT", sb.toString());
	        sendEmailNotification(getPriceFeedStatusEmails(), emailParams, getTemplateFeedImport(), subject);
			
			getCpsGlobalProperties().getSkippedItems().clear();
		}
	}
	public void sendAddressLoaderFileErrorEmail() {
		vlogInfo("called common email sender..............");			
	        HashMap<String, Object> emailParams = new HashMap<String, Object>();
	        emailParams.put(PRODUCTION_URL, getSiteUrl(siteId));
	        String subject = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_SUBJECT_ADDRESS_LOADEER_SCHEDULER), emailParams);
	        String bodyMessage = DataUtils.paramsMessage(messageUtils.getMessage(CPSEmailConstants.EMAIL_BODY_ADDRESS_LOADEER_SCHEDULER), emailParams);
	        StringBuilder sb = new StringBuilder();
	        sb.append(bodyMessage);
	        sb.append(getCpsGlobalProperties().getErrorAddressLoaderFile());
	        emailParams.put("PARAM_FEED_STATUS_TEXT", sb.toString());
	        sendEmailNotification(getAddressLoaderStatusEmails(), emailParams, getTemplateFeedImport(), subject);
		getCpsGlobalProperties().setErrorAddressLoaderFile("");
	}	
	

	/**
	 * @return the cpsGlobalProperties
	 */
	public CPSGlobalProperties getCpsGlobalProperties() {
		return cpsGlobalProperties;
	}

	/**
	 * @param cpsGlobalProperties the cpsGlobalProperties to set
	 */
	public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
		this.cpsGlobalProperties = cpsGlobalProperties;
	}

	/**
	 * @return the jsonFeedStatusEmails
	 */
	public List<String> getJsonFeedStatusEmails() {
		return jsonFeedStatusEmails;
	}

	/**
	 * @param jsonFeedStatusEmails the jsonFeedStatusEmails to set
	 */
	public void setJsonFeedStatusEmails(List<String> jsonFeedStatusEmails) {
		this.jsonFeedStatusEmails = jsonFeedStatusEmails;
	}

	/**
	 * @return the templateSalesOrderReport
	 */
	public String getTemplateSalesOrderReport() {
		return templateSalesOrderReport;
	}

	/**
	 * @param templateSalesOrderReport the templateSalesOrderReport to set
	 */
	public void setTemplateSalesOrderReport(String templateSalesOrderReport) {
		this.templateSalesOrderReport = templateSalesOrderReport;
	}
	    public List<String> getPriceFeedStatusEmails() {
	        return priceFeedStatusEmails;
	    }

	    public void setPriceFeedStatusEmails(List<String> priceFeedStatusEmails) {
	        this.priceFeedStatusEmails = priceFeedStatusEmails;
	    }
}
