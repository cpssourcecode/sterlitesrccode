package com.cps.reorders.model;

import atg.repository.RepositoryItem;
import com.cps.util.model.RepositoryModel;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrderItem extends RepositoryModel {

	public AutoOrderItem(RepositoryItem pSource) {
		super(pSource);
	}

	public String getProductId(){
		return (String) getPropertyValue("productId");
	}

	public void setProductId(String pProductId){
		setPropertyValue("productId", pProductId);
	}

	public String getSkuId(){
		return (String) getPropertyValue("skuId");
	}

	public void setSkuId(String pId){
		setPropertyValue("skuId", pId);
	}

	public Long getQuantity(){
		return (Long) getPropertyValue("quantity");
	}

	public void setQuantity(Long pQuantity){
		setPropertyValue("quantity", pQuantity);
	}

}
