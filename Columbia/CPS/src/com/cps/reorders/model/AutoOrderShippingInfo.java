package com.cps.reorders.model;

import atg.repository.RepositoryItem;
import com.cps.util.model.RepositoryModel;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrderShippingInfo extends RepositoryModel {

	public AutoOrderShippingInfo(RepositoryItem pSource) {
		super(pSource);
	}

	public String getDeliveryMethod(){
		return (String) getPropertyValue("deliveryMethod");
	}

	public void setDeliveryMethod(String pValue){
		setPropertyValue("deliveryMethod", pValue);
	}

	public String getAddressId(){
		return (String) getPropertyValue("addressId");
	}

	public void setAddressId(String pValue){
		setPropertyValue("addressId", pValue);
	}

	public String getJDEAddressNumber(){
		return (String) getPropertyValue("jdeAddressNumber");
	}

	public void setJDEAddressNumber(String pJDEAddressNumber){
		setPropertyValue("jdeAddressNumber", pJDEAddressNumber);
	}
}
