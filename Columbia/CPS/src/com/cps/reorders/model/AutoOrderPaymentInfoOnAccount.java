package com.cps.reorders.model;

import atg.repository.RepositoryItem;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrderPaymentInfoOnAccount extends AutoOrderPaymentInfo {

	public AutoOrderPaymentInfoOnAccount(RepositoryItem pSource) {
		super(pSource);
	}

	public String getPoNumber(){
		return (String) getPropertyValue("poNumber");
	}

	public void setPoNumber(String pValue){
		setPropertyValue("poNumber", pValue);
	}

}
