package com.cps.reorders.model;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import com.cps.util.model.RepositoryModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrder extends RepositoryModel {
	private double mCurrentTotalAmount;
	private String mShipToAddress;

	public AutoOrder(RepositoryItem pRepositoryItem){
		super(pRepositoryItem);
	}

	public String getAutoOrderId(){
		return (String) getPropertyValue("autoOrderId");
	}

	public void setAutoOrderId(String pAutoOrderId){
		setPropertyValue("autoOrderId", pAutoOrderId);
	}

	public String getSubmitterId(){
		return (String) getPropertyValue("submitterId");
	}

	public void setSubmitterId(String pSubmitterId){
		setPropertyValue("submitterId", pSubmitterId);
	}

	public boolean isEnabled(){
		Boolean enabled = (Boolean) getPropertyValue("enabled");
		return enabled != null && enabled;
	}

	public void setEnabled(Boolean pValue){
		setPropertyValue("enabled", pValue);
	}

	public Integer getScheduleDays(){
		return (Integer) getPropertyValue("scheduleDays");
	}

	public void setScheduleDays(Integer pScheduleDays){
		setPropertyValue("scheduleDays", pScheduleDays);
	}

	public Date getNextSubmitDate(){
		return (Date)getPropertyValue("nextSubmitDate");
	}

	public void setNextSubmitDate(Date pDate){
		setPropertyValue("nextSubmitDate", pDate);
	}

	public List<AutoOrderItem> getItems(){
		List<RepositoryItem> repositoryItems = (List<RepositoryItem>) getPropertyValue("items");

		List<AutoOrderItem> result = new ArrayList<>();
		for(RepositoryItem repositoryItem : repositoryItems){
			result.add(new AutoOrderItem((MutableRepositoryItem)repositoryItem));
		}

		return result;
	}

	public void setItems(List<AutoOrderItem> pItems){
		List<RepositoryItem> result = new ArrayList<>();

		for(AutoOrderItem item : pItems){
			result.add(item.getSource());
		}

		setPropertyValue("items", result);
	}

	public AutoOrderPaymentInfo getPaymentInfo(){
		RepositoryItem repositoryItem = (RepositoryItem) getPropertyValue("paymentInfo");

		if(repositoryItem == null)
			return null;

		Integer type = (Integer) repositoryItem.getPropertyValue("type");
		switch (type){
			case 0:
				return new AutoOrderPaymentInfoOnAccount(repositoryItem);
			default:
				return new AutoOrderPaymentInfo(repositoryItem);
		}
	}

	public void setPaymentInfo(AutoOrderPaymentInfo pPaymentInfo){
		setPropertyValue("paymentInfo", pPaymentInfo.getSource());
	}

	public AutoOrderShippingInfo getShippingInfo(){
		RepositoryItem repositoryItem = (RepositoryItem) getPropertyValue("shippingInfo");

		if(repositoryItem == null)
			return null;

		return new AutoOrderShippingInfo(repositoryItem);
	}

	public void setShippingInfo(AutoOrderShippingInfo pShippingInfo){
		setPropertyValue("shippingInfo", pShippingInfo.getSource());
	}

	public String getSiteId() {
		return (String) getPropertyValue("siteId");
	}

	public void setSiteId(String pSiteId) {
		setPropertyValue("siteId", pSiteId);
	}

	public double getCurrentTotalAmount() {
		return mCurrentTotalAmount;
	}

	public void setCurrentTotalAmount(double pCurrentTotalAmount) {
		mCurrentTotalAmount = pCurrentTotalAmount;
	}

	public String getShipToAddress() {
		return mShipToAddress;
	}

	public void setShipToAddress(String pShipToAddress) {
		mShipToAddress = pShipToAddress;
	}
}
