package com.cps.reorders.model;

import atg.repository.RepositoryItem;
import com.cps.util.model.RepositoryModel;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrderPaymentInfo extends RepositoryModel {

	public AutoOrderPaymentInfo(RepositoryItem pSource) {
		super(pSource);
	}

	public String getBillingAddressId(){
		return (String) getPropertyValue("billingAddressId");
	}

	public void setBillingAddressId(String pId){
		setPropertyValue("billingAddressId", pId);
	}

	public String getType(){
		return (String) getPropertyValue("type");
	}

	public void setType(String pValue){
		setPropertyValue("type", pValue);
	}

}
