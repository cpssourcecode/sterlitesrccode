package com.cps.reorders.model;

import java.util.Comparator;

public class AutoOrderItemInfo {
    private String productId;
    private String skuId;
    private Long quantity;
    private Integer scheduleDays;
    private String siteId;

    public AutoOrderItemInfo() {
    }

    public AutoOrderItemInfo(String productId, String skuId, Long quantity, Integer scheduleDays, String siteId) {
        this.productId = productId;
        this.skuId = skuId;
        this.quantity = quantity;
        this.scheduleDays = scheduleDays;
        this.siteId = siteId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Integer getScheduleDays() {
        return scheduleDays;
    }

    public void setScheduleDays(Integer scheduleDays) {
        this.scheduleDays = scheduleDays;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }
}
