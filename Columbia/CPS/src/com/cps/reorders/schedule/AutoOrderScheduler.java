package com.cps.reorders.schedule;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import com.cps.commerce.order.CPSAvailabilityManager;
import com.cps.commerce.order.CPSHolidaysService;
import com.cps.reorders.AutoOrderManager;
import com.cps.reorders.AutoOrderProcessor;
import com.cps.reorders.model.AutoOrder;
import com.cps.reorders.model.AutoOrderItemInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrderScheduler extends GenericService implements Schedulable {

	private static final String SCHEDULER_NAME = "Auto Order Scheduler";

	private AutoOrderManager mAutoOrderManager;
	private AutoOrderProcessor mAutoOrderProcessor;

	private CPSAvailabilityManager mAvailabilityManager;
	private CPSHolidaysService mHolidaysService;

	private boolean mEnabled;
	private int jobId;

	private Scheduler mScheduler;
	private Schedule mSchedule;

    @Override
    public void doStartService() throws ServiceException {
        ScheduledJob scheduledJob = new ScheduledJob(SCHEDULER_NAME, SCHEDULER_NAME,
                getAbsoluteName(), getSchedule(), this,
                ScheduledJob.REUSED_THREAD);

		jobId = getScheduler().addScheduledJob(scheduledJob);
    }

	public void doStopService() throws ServiceException
	{
		getScheduler().removeScheduledJob(jobId);
	}

	@Override
	public void performScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {
        try {
            if (isLoggingDebug())
                logDebug("AutoOrderScheduler - start");

            if (isEnabled() && getAvailabilityManager().isJdeAvailable()) {
                processOrders();
            } else {
                if (isLoggingDebug())
                    logDebug("AutoOrderScheduler doesn't process orders");
            }

            if (isLoggingDebug())
                logDebug("AutoOrderScheduler - end");
        } catch (Exception ex) {
            logError(ex);
        }
    }

	private void processOrders() {
		if (getHolidaysService() != null && getHolidaysService().isEnabled()) {
			if (!getHolidaysService().isTodayWeekend() && !getHolidaysService().isTodayHoliday()) {
				reOrderItems(true);
			}
		} else {
			reOrderItems(false);
		}
	}

	private void reOrderItems(boolean holidayServiceActive) {
		try {
			List<AutoOrder> autoOrders = getAutoOrderManager().loadExpiredAutoOrders();

			if (autoOrders != null && !autoOrders.isEmpty()) {
				for (AutoOrder autoOrder : autoOrders) {
					if (autoOrder.isEnabled()) {
						getAutoOrderProcessor().processOrder(autoOrder);

						//set next submit date
						Date nextSubmitDate=getAutoOrderManager().getReorderDate(autoOrder.getScheduleDays());
						autoOrder.setNextSubmitDate(nextSubmitDate);
					}
				}
			}
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("Error during auto orders scheduler job", e);
			}
		}
	}

	public void runJobOnce(){
		performScheduledTask(null, null);
	}

	public void processOrderOnce(){
		try {
			List<AutoOrderItemInfo> list = new ArrayList<>();
			AutoOrder autoOrder = getAutoOrderManager().createAutoOrder("206011", list);

			getAutoOrderProcessor().processOrder(autoOrder);

		}catch (Exception e){
			logError(e);
		}
	}

	public AutoOrderManager getAutoOrderManager() {
		return mAutoOrderManager;
	}

	public void setAutoOrderManager(AutoOrderManager pAutoOrderManager) {
		mAutoOrderManager = pAutoOrderManager;
	}

	public AutoOrderProcessor getAutoOrderProcessor() {
		return mAutoOrderProcessor;
	}

	public void setAutoOrderProcessor(AutoOrderProcessor pAutoOrderProcessor) {
		mAutoOrderProcessor = pAutoOrderProcessor;
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public boolean getEnabled() {
		return isEnabled();
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	public Scheduler getScheduler() {
		return mScheduler;
	}

	public void setScheduler(Scheduler pScheduler) {
		mScheduler = pScheduler;
	}

	public Schedule getSchedule() {
		return mSchedule;
	}

	public void setSchedule(Schedule pSchedule) {
		mSchedule = pSchedule;
	}

	public int getJobId() {
		return jobId;
	}

	public void setJobId(int pJobId) {
		jobId = pJobId;
	}

	public CPSAvailabilityManager getAvailabilityManager() {
		return mAvailabilityManager;
	}

	public void setAvailabilityManager(CPSAvailabilityManager pAvailabilityManager) {
		mAvailabilityManager = pAvailabilityManager;
	}

	public CPSHolidaysService getHolidaysService() {
		return mHolidaysService;
	}

	public void setHolidaysService(CPSHolidaysService pHolidaysService) {
		mHolidaysService = pHolidaysService;
	}
}
