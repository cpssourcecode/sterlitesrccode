package com.cps.reorders;

import atg.commerce.CommerceException;
import atg.commerce.order.*;
import atg.commerce.states.OrderStates;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.userprofiling.address.AddressTools;
import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.commerce.order.CPSInvoiceRequest;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.service.CPSCartModifierService;
import com.cps.commerce.approval.ApprovalManager;
import com.cps.commerce.pricing.service.CPSMultiPriceService;
import com.cps.core.util.CPSContactInfo;
import com.cps.email.CommonEmailSender;
import com.cps.reorders.model.AutoOrder;
import com.cps.reorders.model.AutoOrderItem;
import com.cps.reorders.model.AutoOrderPaymentInfo;
import com.cps.reorders.model.AutoOrderPaymentInfoOnAccount;
import com.cps.reorders.model.AutoOrderShippingInfo;
import com.cps.util.CPSConstants;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import javax.transaction.TransactionManager;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;

import static com.cps.util.CPSConstants.*;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrderProcessor extends GenericService {

	private AutoOrderManager mAutoOrderManager;
	private OrderManager mOrderManager;
	private PaymentGroupManager mPaymentGroupManager;
	private ShippingGroupManager mShippingGroupManager;
	private CPSCartModifierService mCartModifierService;
	private Repository mOrderRepository;
	private CommerceItemManager mCommerceItemManager;
	private CPSMultiPriceService mMultiPriceService;
	private Repository mProfileRepository;
	private OrderStates mOrderStates;
	private ApprovalManager mApprovalManager;
	private CommonEmailSender mEmailSender;
	private CPSCatalogTools mCatalogTools;
	private TransactionManager mTransactionManager;

	/**
	 * Creates & commit a new Order based on Auto Order
	 * order updated in a bunch the separate transaction is needed for each order in a bunch
	 * @param pAutoOrder an Auto Order
	 * @return Order which was submitted
	 */
	public CPSOrderImpl processOrder(AutoOrder pAutoOrder) {
		if(isLoggingDebug()){
			logDebug("Processing Auto Order: " + pAutoOrder.getSource());
		}

		CPSOrderImpl order = null;

		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;
		try {
			try {
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRES_NEW);
				order = createOrder(pAutoOrder);
				getOrderManager().addOrder(order);
				synchronized (order) {
					processShippingGroup(pAutoOrder, order);
					processPaymentGroup(pAutoOrder, order);
					processCommerceItems(pAutoOrder, order);

					order.getPriceInfo().setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
					order.setAutoOrderId(pAutoOrder.getAutoOrderId());

					processRepriceOrder(pAutoOrder, order);
					order.setOrderTotal(order.getPriceInfo().getTotal());
					PipelineResult pipelineResult = processCommitOrder(pAutoOrder, order);

					if (pipelineResult != null) {
						checkForApproval(order);
						//sending order confirmation email or approval notification message based on approval status
						if (order.getState() == getOrderStates().getStateValue(OrderStates.PENDING_APPROVAL)) {
							// Iterate over eligible order approvers and notify each
							List<RepositoryItem> approvers = getApprovalManager().getApproversForUser(getProfile(order));
							if (approvers != null) {
								for (RepositoryItem approver : approvers) {
									getEmailSender().sendOrderApprovalRequiredEmail(approver, getProfile(order), order);
								}
							}
							getEmailSender().sendOrderRequestReceivedEmail(getProfile(order), order);
						} else {
							String siteId = order.getSiteId();
							getEmailSender().sendOrderConfirmationEmail(getProfile(order), order, siteId);
						}
					}
				}
			} catch (Exception e) {
				rollback = true;
				if (isLoggingError()) {
					logError(e);
				}
			} finally {
				td.end(rollback);
			}
		} catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		finally {
			if(isLoggingDebug()){
				logDebug(String.format("Completed processing {%s}, result: {%s}", pAutoOrder.getSource(), order));
			}
		}

		return order;
	}


	protected CPSOrderImpl createOrder(AutoOrder pAutoOrder) throws CommerceException {
		CPSOrderImpl order = (CPSOrderImpl) getOrderManager().createOrder(pAutoOrder.getSubmitterId());
		order.setSubmittedDate(pAutoOrder.getNextSubmitDate());
		order.setSiteId(pAutoOrder.getSiteId());
		return order;
	}

	protected void processCommerceItems(AutoOrder pAutoOrder, CPSOrderImpl pOrder) throws CommerceException {
		for(AutoOrderItem orderItem : pAutoOrder.getItems()){
			CommerceItem commerceItem = getCommerceItemManager().createCommerceItem(orderItem.getSkuId(), orderItem.getProductId(), orderItem.getQuantity());
			if (getCatalogTools().isItemValid(commerceItem)) {
				getCommerceItemManager().addItemToOrder(pOrder, commerceItem);
				commerceItem.getPriceInfo().setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);

				getCommerceItemManager().addItemQuantityToShippingGroup(pOrder, commerceItem.getId(), pOrder.getShippingGroup().getId(), commerceItem.getQuantity());
			}
		}

		getOrderManager().updateOrder(pOrder);
	}

	protected void processShippingGroup(AutoOrder pAutoOrder, CPSOrderImpl pOrder) throws RepositoryException, CommerceException, IntrospectionException {
		AutoOrderShippingInfo shippingInfo = pAutoOrder.getShippingInfo();

		String deliveryMethod = shippingInfo.getDeliveryMethod();

		List<ShippingGroup> exceptions = new ArrayList<>();

		if(StringUtils.isNotEmpty(deliveryMethod)){
			switch (deliveryMethod) {
				case DELEVERY_METHOD_SHIPPED: {
					CPSHardgoodShippingGroup shippingGroup = (CPSHardgoodShippingGroup) getCartModifierService().createHardgoodShipGroupIfNotExists(pOrder);

					RepositoryItem shippingInfoAddress = getOrderRepository().getItem(shippingInfo.getAddressId(), CPSConstants.AUTO_ORDER_ADDRESS_VIEW_NAME);
					CPSContactInfo address = new CPSContactInfo();
					AddressTools.copyAddress(shippingInfoAddress, address);

					shippingGroup.setShippingAddress(address);
					shippingGroup.setJdeAddressNumber((String) shippingInfoAddress.getPropertyValue("jdeAddressNumber"));

					exceptions.add(shippingGroup);
					break;
				}
				case DELEVERY_METHOD_PICK_UP: {
					InStorePickupShippingGroup shippingGroup = getCartModifierService().createInStorePickupShippingGroupIfNotExists(pOrder);
					shippingGroup.setPropertyValue(JDE_ADDRESS_NUM, shippingInfo.getJDEAddressNumber());
					shippingGroup.setLocationId(shippingInfo.getAddressId());

					exceptions.add(shippingGroup);

					break;
				}
			}
		}

		getShippingGroupManager().removeAllShippingGroupsFromOrder(pOrder, exceptions);

		getOrderManager().updateOrder(pOrder);
	}

	protected void processPaymentGroup(AutoOrder pAutoOrder, CPSOrderImpl pOrder) throws RepositoryException, CommerceException, IntrospectionException {
		AutoOrderPaymentInfo paymentInfo = pAutoOrder.getPaymentInfo();

		if(paymentInfo instanceof AutoOrderPaymentInfoOnAccount) {
			CPSInvoiceRequest invoiceRequest = (CPSInvoiceRequest) getCartModifierService().createInvoiceRequestPayGroupIfNotExists(pOrder);

			RepositoryItem billingAddress = getOrderRepository().getItem(paymentInfo.getBillingAddressId(), CPSConstants.AUTO_ORDER_ADDRESS_VIEW_NAME);
			CPSContactInfo contactInfo = new CPSContactInfo();
			AddressTools.copyAddress(billingAddress, contactInfo);

			invoiceRequest.setBillingAddress(contactInfo);
			invoiceRequest.setJdeAddressNumber((String) billingAddress.getPropertyValue("jdeAddressNumber"));
			invoiceRequest.setPONumber(((AutoOrderPaymentInfoOnAccount) paymentInfo).getPoNumber());

			getOrderManager().addRemainingOrderAmountToPaymentGroup(pOrder, invoiceRequest.getId());

			getOrderManager().updateOrder(pOrder);
		}
	}

	protected void processRepriceOrder(AutoOrder pAutoOrder, CPSOrderImpl pOrder) throws RepositoryException, CommerceException {
		RepositoryItem profile = getProfile(pOrder);

		getMultiPriceService().updateFullOrderPrices(pOrder, profile);
		pOrder.updateVersion();
		getOrderManager().updateOrder(pOrder);
	}

	protected PipelineResult processCommitOrder(AutoOrder pAutoOrder, CPSOrderImpl pOrder) throws CommerceException {
		return this.getOrderManager().processOrderWithReprice(pOrder, null, this.getOrderManager().getProcessOrderMap(null, null));
	}

	protected void checkForApproval(Order pOrder) throws RepositoryException {
		boolean approvalRequired = getApprovalManager().isApprovalRequired(pOrder, getProfile(pOrder));
		if (approvalRequired) {
			pOrder.setState(getOrderStates().getStateValue(OrderStates.PENDING_APPROVAL));
			List<RepositoryItem> approvers = getApprovalManager().getApproversForUser(getProfile(pOrder));
			if (approvers != null) {
				for (RepositoryItem approver : approvers) {
					pOrder.getAuthorizedApproverIds().add(approver.getRepositoryId());
				}
			}
			getApprovalManager().saveApprovalRequest(pOrder);
		}
	}


	// =========================================================================================================

 	AutoOrderManager getAutoOrderManager() {
		return mAutoOrderManager;
	}

	public void setAutoOrderManager(AutoOrderManager pAutoOrderManager) {
		mAutoOrderManager = pAutoOrderManager;
	}

	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public PaymentGroupManager getPaymentGroupManager() {
		return mPaymentGroupManager;
	}

	public void setPaymentGroupManager(PaymentGroupManager pPaymentGroupManager) {
		mPaymentGroupManager = pPaymentGroupManager;
	}

	public CPSCartModifierService getCartModifierService() {
		return mCartModifierService;
	}

	public void setCartModifierService(CPSCartModifierService pCartModifierService) {
		mCartModifierService = pCartModifierService;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	public ShippingGroupManager getShippingGroupManager() {
		return mShippingGroupManager;
	}

	public void setShippingGroupManager(ShippingGroupManager pShippingGroupManager) {
		mShippingGroupManager = pShippingGroupManager;
	}

	public Repository getOrderRepository() {
		return mOrderRepository;
	}

	public void setOrderRepository(Repository pOrderRepository) {
		mOrderRepository = pOrderRepository;
	}

	public CommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	public void setCommerceItemManager(CommerceItemManager pCommerceItemManager) {
		mCommerceItemManager = pCommerceItemManager;
	}

	public CPSMultiPriceService getMultiPriceService() {
		return mMultiPriceService;
	}

	public void setMultiPriceService(CPSMultiPriceService pMultiPriceService) {
		mMultiPriceService = pMultiPriceService;
	}

	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}

	public OrderStates getOrderStates() {
		return mOrderStates;
	}

	public void setOrderStates(OrderStates pOrderStates) {
		mOrderStates = pOrderStates;
	}

	public ApprovalManager getApprovalManager() {
		return mApprovalManager;
	}

	public void setApprovalManager(ApprovalManager pApprovalManager) {
		mApprovalManager = pApprovalManager;
	}

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	public RepositoryItem getProfile(Order pOrder) {
		try {
			return getProfileRepository().getItem(pOrder.getProfileId(), "user");
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		return null;
	}

	public CPSCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	public void setCatalogTools(CPSCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}

}
