package com.cps.reorders;

import atg.commerce.CommerceException;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvoiceRequest;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.userprofiling.address.AddressTools;
import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.commerce.order.CPSHolidaysService;
import com.cps.commerce.order.CPSInvoiceRequest;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.reorders.model.*;
import com.cps.util.CPSConstants;
import com.ning.http.util.DateUtil;

import java.beans.IntrospectionException;
import java.util.*;

import javax.transaction.TransactionManager;

import org.apache.commons.lang3.time.DateUtils;

import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;
import static com.cps.util.CPSConstants.JDE_ADDRESS_NUM;

/**
 * @author Alex Turchynovich.
 */
public class AutoOrderManager extends GenericService{

	private static final String ORDERS_BY_SUBMITTER_QUERY = "submitterId = ?0";
	private static final String ORDERS_BY_NEXT_DATE_QUERY = "?0 >= nextSubmitDate";

	private MutableRepository mOrderRepository;
	private Repository mProfileRepository;
	private OrderManager mOrderManager;
	private CPSHolidaysService mHolidaysService;
	private int testScheduleDays;
	private int cutOffTime;
	private String savedCartQuery;
	
	public String getSavedCartQuery() {
		return savedCartQuery;
	}

	public void setSavedCartQuery(String savedCartQuery) {
		this.savedCartQuery = savedCartQuery;
	}
	
	/**
	 * load auto order by id
	 * @param autoOrderId auto order id
	 * @return auto order
	 */
	public AutoOrder loadAutoOrder(String autoOrderId) throws RepositoryException {
		logDebug("Load auto order - start");
		MutableRepositoryItem repositoryItem = mOrderRepository.getItemForUpdate(autoOrderId, CPSConstants.AUTO_ORDER_VIEW_NAME);

		return new AutoOrder(repositoryItem);
	}

	/**
	 * Get all auto orders for user
	 * @param userId user id
	 * @return list of auto orders
	 */
	public List<AutoOrder> loadAutoOrders(String userId) throws RepositoryException {
		return queryAutoOrders(ORDERS_BY_SUBMITTER_QUERY, userId);
	}

	/**
	 * Get all auto which need to be processed
	 * @return list of auto orders.
	 */
	public List<AutoOrder> loadExpiredAutoOrders() throws RepositoryException {
		return queryAutoOrders(ORDERS_BY_NEXT_DATE_QUERY, new Date());
	}

	/**
	 * Creates a new AutoOrder based on existing and place it into repository.
	 * @param orderId id of existing order
	 * @return Auto Order
	 */
	public AutoOrder createAutoOrder(String orderId, List<AutoOrderItemInfo> products) throws CommerceException, IntrospectionException, RepositoryException {
		return createAutoOrder((CPSOrderImpl)getOrderManager().loadOrder(orderId), products);
	}

	/**
	 * Creates a new AutoOrder and place it into repository based on existing order
	 * @param pOrder order as a base
	 * @return new auto order
	 */
	public AutoOrder createAutoOrder(CPSOrderImpl pOrder, List<AutoOrderItemInfo> products) throws RepositoryException, IntrospectionException {
		MutableRepositoryItem repositoryItem = getOrderRepository().createItem(CPSConstants.AUTO_ORDER_VIEW_NAME);
		AutoOrder result = new AutoOrder(repositoryItem);

		result.setEnabled(true);
		result.setSubmitterId(pOrder.getProfileId());

		result.setItems(getItems(products));
		result.setScheduleDays(products.get(0).getScheduleDays());

		//Calendar calendar = new GregorianCalendar();
		//calendar.add(Calendar.DATE, products.get(0).getScheduleDays());
		//result.setNextSubmitDate(calendar.getTime());

		
		Date reorderDate=getReorderDate(result.getScheduleDays());
		
		result.setNextSubmitDate(reorderDate);
		result.setShippingInfo(createShippingInfo(pOrder));
		result.setPaymentInfo(createPaymentInfo(pOrder));
		result.setSiteId(pOrder.getSiteId());

		mOrderRepository.addItem(result.getTarget());

		return result;
	}

	public Date testReorderDate() {
		return getReorderDate(getTestScheduleDays());
	}
	public Date getReorderDate(int scheduleDays) {
		Date nextSubmitDate;
		Date recalcDeliveryDate;
		Date recalcNextSubmitDate;
		Date reorderDate=null;
		
		Calendar calendar = Calendar.getInstance();
		Calendar recalcDeliveryCal= (Calendar) calendar.clone();
		Calendar recalcNextSubmitCal = (Calendar) calendar.clone();
		Calendar actualNextSubmitCal = (Calendar) calendar.clone();
		Calendar nextSubmitCal=(Calendar) calendar.clone();

		vlogDebug("Current date {0}",calendar.getTime());
		
		Calendar deliveryDateCal= (Calendar) calendar.clone();
		int currentHour=calendar.get(Calendar.HOUR_OF_DAY);
		if (currentHour < getCutOffTime()) {
			deliveryDateCal.add(Calendar.DATE, 1);
		}else{
			deliveryDateCal.add(Calendar.DATE, 2);
		}
		vlogDebug("deliveryDateCal {0}",deliveryDateCal.getTime());
		nextSubmitCal.add(Calendar.DATE, scheduleDays);
		vlogDebug("nextSubmitCal {0}",nextSubmitCal.getTime());

		if (getHolidaysService() != null && getHolidaysService().isEnabled()) {
		  	recalcDeliveryDate = getHolidaysService().getDelayedReorderDate(deliveryDateCal).getTime();
			vlogDebug("recalcDeliveryDate {0}",recalcDeliveryDate);
			recalcDeliveryCal.setTime(recalcDeliveryDate);
			recalcNextSubmitDate = getHolidaysService().getDelayedReorderDate(nextSubmitCal).getTime();
			vlogDebug("recalcNextSubmitDate {0}",recalcNextSubmitDate);
			recalcNextSubmitCal.setTime(recalcNextSubmitDate);
			if(((recalcDeliveryCal.get(Calendar.DAY_OF_YEAR)) >= (recalcNextSubmitCal.get(Calendar.DAY_OF_YEAR)))) {
				actualNextSubmitCal= (Calendar) recalcDeliveryCal.clone();
				actualNextSubmitCal.add(Calendar.DATE, 1);
				vlogDebug("if actualNextSubmitCal :: {0}",actualNextSubmitCal.getTime());
				reorderDate = getHolidaysService().getDelayedReorderDate(actualNextSubmitCal).getTime();
			}else{
				vlogDebug("else recalcNextSubmitCal :: {0}",recalcNextSubmitCal.getTime());
				reorderDate = getHolidaysService().getDelayedReorderDate(recalcNextSubmitCal).getTime();
			}
			vlogDebug("reorderDate {0}",reorderDate);
		}

		return reorderDate;
	}
	
	private AutoOrderShippingInfo createShippingInfo(CPSOrderImpl pOrder) throws RepositoryException, IntrospectionException {
		AutoOrderShippingInfo result = new AutoOrderShippingInfo(getOrderRepository().createItem(CPSConstants.AUTO_ORDER_SHIPPING_INFO_VIEW_NAME));

		List shippingGroups = pOrder.getShippingGroups();
		if(shippingGroups.size() > 0) {
			ShippingGroup shippingGroup = (ShippingGroup) shippingGroups.get(0);

			if (shippingGroup instanceof CPSHardgoodShippingGroup) {
				result.setDeliveryMethod("shipped");

				CPSHardgoodShippingGroup hardgoodShippingGroup = (CPSHardgoodShippingGroup) shippingGroup;

				MutableRepositoryItem address = getOrderRepository().createItem(CPSConstants.AUTO_ORDER_ADDRESS_VIEW_NAME);
				AddressTools.copyAddress(hardgoodShippingGroup.getShippingAddress(), address);
				address.setPropertyValue(JDE_ADDRESS_NUM, hardgoodShippingGroup.getJdeAddressNumber());
				getOrderRepository().addItem(address);

				result.setJDEAddressNumber(hardgoodShippingGroup.getJdeAddressNumber());
				result.setAddressId(address.getRepositoryId());
			}

			if (shippingGroup instanceof InStorePickupShippingGroup) {
				InStorePickupShippingGroup pickupShippingGroup = ((InStorePickupShippingGroup) shippingGroup);

				result.setDeliveryMethod("pick-up");
				result.setAddressId(pickupShippingGroup.getLocationId());
				result.setJDEAddressNumber((String) pickupShippingGroup.getPropertyValue(JDE_ADDRESS_NUM));
			}

			getOrderRepository().addItem(result.getTarget());
		}

		return result;
	}

	private AutoOrderPaymentInfo createPaymentInfo(CPSOrderImpl pOrder) throws RepositoryException, IntrospectionException {
		PaymentGroup paymentGroup = pOrder.getPaymentGroup();

		if(paymentGroup instanceof CPSInvoiceRequest) {
			AutoOrderPaymentInfoOnAccount result = new AutoOrderPaymentInfoOnAccount(getOrderRepository().createItem(CPSConstants.AUTO_ORDER_PAYMENT_INFO_ON_ACCOUNT_VIEW_NAME));

			MutableRepositoryItem address = getOrderRepository().createItem(CPSConstants.AUTO_ORDER_ADDRESS_VIEW_NAME);
			AddressTools.copyAddress(((InvoiceRequest) paymentGroup).getBillingAddress(), address);
			getOrderRepository().addItem(address);

			address.setPropertyValue(JDE_ADDRESS_NUM, ((CPSInvoiceRequest) paymentGroup).getJdeAddressNumber());

			result.setBillingAddressId(address.getRepositoryId());
			result.setPoNumber(((InvoiceRequest) paymentGroup).getPONumber());

			getOrderRepository().addItem(result.getTarget());

			return result;
		}

		return null;
	}

	private List<AutoOrderItem> getItems(List<AutoOrderItemInfo> products) throws RepositoryException {
		List<AutoOrderItem> result = new ArrayList<>();

		for(AutoOrderItemInfo autoOrderItemInfo : products){

			AutoOrderItem autoOrderItem = new AutoOrderItem(mOrderRepository.createItem(CPSConstants.AUTO_ORDER_ITEM_VIEW_NAME));
			autoOrderItem.setProductId(autoOrderItemInfo.getProductId());
			autoOrderItem.setQuantity(autoOrderItemInfo.getQuantity());
			autoOrderItem.setSkuId(autoOrderItemInfo.getSkuId());

			mOrderRepository.addItem(autoOrderItem.getTarget());

			result.add(autoOrderItem);
		}

		return result;
	}

	public void updateAutoOrder(AutoOrder pAutoOrder) throws RepositoryException {
		getOrderRepository().updateItem(pAutoOrder.getTarget());
	}


	public void cancelAutoOrder(String autoOrderId) throws RepositoryException {
		AutoOrder autoOrder = loadAutoOrder(autoOrderId);
		autoOrder.setEnabled(false);
		updateAutoOrder(autoOrder);
	}

	/**
	 * query orders using rql
	 * @param rql rql query
	 * @param params params for query
	 * @return List of Auto Orders
	 * @throws RepositoryException Repository Exception thrown by repository operations.
	 */
	public List<AutoOrder> queryAutoOrders(String rql, Object... params) throws RepositoryException {
		RqlStatement statement = RqlStatement.parseRqlStatement(rql);

		RepositoryView view = mOrderRepository.getView(CPSConstants.AUTO_ORDER_VIEW_NAME);
		RepositoryItem[] items = statement.executeQuery(view, params);

		List<AutoOrder> result = new ArrayList<>();

		if (items != null && items.length > 0) {
			for (RepositoryItem item : items) {
				result.add(new AutoOrder(item));
			}
		}

		return result;
	}	public void updateSavedCart(Order currentOrder) {
		Object[] params = new Object[2];
		params[0] = currentOrder.getProfileId();
		try {
			Repository orderRepository = getOrderManager().getOrderTools().getOrderRepository();
			RepositoryView orderView = orderRepository.getView(ITEM_DESCRIPTOR_ORDER);
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(getSavedCartQuery());
			RepositoryItem[] results = rqlStatement.executeQuery(orderView, params);
			MutableRepository mutableRepository = (MutableRepository) getOrderManager().getOrderTools().getOrderRepository();
			if(results != null){
				for (RepositoryItem orderItem : results) {
					if(!orderItem.getRepositoryId().equalsIgnoreCase(currentOrder.getId())){
						MutableRepositoryItem mutableOrderItem= mutableRepository.getItemForUpdate(orderItem.getRepositoryId(),"order");
						mutableOrderItem.setPropertyValue("explicitlySaved", true);
					}
				}
			}
		} catch (RepositoryException re) {
			re.printStackTrace();
		}
	}

	public RepositoryItem getSubmitter(AutoOrder pAutoOrder) throws RepositoryException {
		String profileId = pAutoOrder.getSubmitterId();

		return mProfileRepository.getItem(profileId, "user");
	}

	public MutableRepository getOrderRepository() {
		return mOrderRepository;
	}

	public void setOrderRepository(MutableRepository pOrderRepository) {
		mOrderRepository = pOrderRepository;
	}

	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}

	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public CPSHolidaysService getHolidaysService() {
		return mHolidaysService;
	}

	public void setHolidaysService(CPSHolidaysService pHolidaysService) {
		mHolidaysService = pHolidaysService;
	}

	public int getTestScheduleDays() {
		return testScheduleDays;
	}

	public void setTestScheduleDays(int testScheduleDays) {
		this.testScheduleDays = testScheduleDays;
	}

	public int getCutOffTime() {
		return cutOffTime;
	}

	public void setCutOffTime(int cutOffTime) {
		this.cutOffTime = cutOffTime;
	}
}
