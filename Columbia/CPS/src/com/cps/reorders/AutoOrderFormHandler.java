package com.cps.reorders;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;
import java.io.IOException;

/**
 * @author Alexey Meleshko
 */
public class AutoOrderFormHandler extends GenericFormHandler {
    private AutoOrderManager mAutoOrderManager;
    private String mAutoOrderId;

    private RepeatingRequestMonitor mRepeatingRequestMonitor;

    public boolean handleCancelAutoOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "AutoOrderFormHandler.handleCancelAutoOrder";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                mAutoOrderManager.cancelAutoOrder(getAutoOrderId());
            } catch (RepositoryException e) {
                vlogError(e, "Error");
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        AjaxUtils.addAjaxResponse(this, pResponse, null);
        return false;
    }

    public AutoOrderManager getAutoOrderManager() {
        return mAutoOrderManager;
    }

    public void setAutoOrderManager(AutoOrderManager mAutoOrderManager) {
        this.mAutoOrderManager = mAutoOrderManager;
    }

    public String getAutoOrderId() {
        return mAutoOrderId;
    }

    public void setAutoOrderId(String pAutoOrderId) {
        this.mAutoOrderId = pAutoOrderId;
    }

    public RepeatingRequestMonitor getRepeatingRequestMonitor() {
        return mRepeatingRequestMonitor;
    }

    public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
        mRepeatingRequestMonitor = pRepeatingRequestMonitor;
    }
}
