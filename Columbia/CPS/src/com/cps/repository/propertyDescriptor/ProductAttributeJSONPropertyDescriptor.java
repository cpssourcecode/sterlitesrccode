package com.cps.repository.propertyDescriptor;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import atg.json.JSONObject;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryItemImpl.NullObject;
import atg.repository.RepositoryPropertyDescriptor;

/**
 *
 *
 * @author Naga
 *
 */
public class ProductAttributeJSONPropertyDescriptor extends RepositoryPropertyDescriptor {
    private static final String PROPERTY_NAME = "propertyName";
    private String propertyName;

    @Override
    public void setValue(String pAttributeName, Object pValue) {

        super.setValue(pAttributeName, pValue);

        if (PROPERTY_NAME.equals(pAttributeName)) {
            propertyName = (String) pValue;
        }
    }

    @Override
    public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
        Object result = null;
        if (pValue != null || pValue instanceof NullObject) {
            result = pValue;
        } else {
            Object propertyValue = pItem.getPropertyValue(propertyName);
            if (propertyValue != null && !(propertyValue instanceof NullObject)) {
                String propertyValueStr = (String) propertyValue;
                try {
                    Map<String, String> sizes = calculateDimensions(propertyValueStr);

                    // //System.out.println("$$$$$$$ sizes JSON :: " + sizes);
                    result = new JSONObject(sizes).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.err.println("Exception occurred trying to calculate dimensions for product with id :: " + pItem.getRepositoryId() + ","
                                    + propertyName + " :: " + propertyValueStr);
                }
            }
            pItem.setPropertyValueInCache(this, result);
        }
        return result;
    }

    public static void main(String[] args) {

        String[] inputValues = new String[] { "1/2 x 3/8 in", "4 x 2 in", "8 x 6 in", "1-1/2 x 1 in", "1 x 1/2 in", "8 x 4 in", "1-1/4 x 1 in",
                "1-1/2 x 1-1/4 in", "3 x 1-1/2 in", "2-1/2 x 1-1/2 in", "DN100", "No 10-32 x 5/32 in", "1 x 1-11-1/2 in", "1-1/ in", "6.031 in", "2.864 in",
                "125 lb", "150 lb", "0.94", "0.12" };
        ProductAttributeJSONPropertyDescriptor pd = new ProductAttributeJSONPropertyDescriptor();
        for (String inputValue : inputValues) {
            System.out.println(pd.calculateDimensions(inputValue));
        }
    }

    public Map<String, String> calculateDimensions(String nominalSize) {
        Map<String, String> sizes = new HashMap<>();
        Pattern simpleDigitPattern = Pattern.compile("(\\d+)");
        Pattern simpleFractionPattern = Pattern.compile("(\\d+/\\d+)");
        Pattern complexFractionPattern = Pattern.compile("(\\d+-\\d+/\\d+)");
        Pattern decimalPattern = Pattern.compile("(\\d+\\.\\d+)");

        String inputValue = nominalSize;
        // System.out.println("************* START *****************");
        // System.out.println("inputValue: " + inputValue);

        String tempValue = inputValue.replaceAll("in", "").replaceAll("lb", "");
        // System.out.println("tempValue: " + tempValue);

        String[] dims = tempValue.split("x");
        if (dims != null) {
            int k = 1;
            sizes.put("dim" + k, "9999");
            for (String dim1 : dims) {
                String dim = dim1.trim();
                String currentDimSize = "9999";
                Matcher simpleDigitMatcher = simpleDigitPattern.matcher(dim);
                Matcher complexFractionMatcher = complexFractionPattern.matcher(dim);
                Matcher simpleFractionMatcher = simpleFractionPattern.matcher(dim);
                Matcher decimalMatcher = decimalPattern.matcher(dim);

                if (complexFractionMatcher.matches()) {
                    // System.out.println("text 1: " + dim);
                    String gp1 = complexFractionMatcher.group(1);
                    // System.out.println("first dimension 1: " + gp1);
                    if (gp1.contains("/")) {
                        String[] both_vals = complexFractionMatcher.group(1).split("\\/");
                        String remainder = "";
                        String numerator = "";
                        String divisor = "";

                        if (both_vals[0].contains("-")) {
                            String[] numerator_vals = both_vals[0].split("\\-");
                            remainder = numerator_vals[0];
                            numerator = numerator_vals[1];
                        } else {
                            numerator = both_vals[0];
                        }

                        if (both_vals[1] != null) {
                            divisor = both_vals[1];
                        } else {
                            divisor = "1";
                        }

                        // System.out.println("yields 1 : " + (Float.parseFloat(remainder) + Float.parseFloat(numerator) / Float.parseFloat(divisor)));
                        currentDimSize = "" + (Float.parseFloat(remainder) + Float.parseFloat(numerator) / Float.parseFloat(divisor));
                    }
                } else if (simpleFractionMatcher.matches()) {
                    // System.out.println("text 2: " + dim);
                    String gp1 = simpleFractionMatcher.group(1);
                    // System.out.println("first dimension 2: " + gp1);
                    if (gp1.contains("/")) {
                        String[] both_vals = simpleFractionMatcher.group(1).split("\\/");
                        String numerator = "";
                        String divisor = "";

                        numerator = both_vals[0];
                        divisor = both_vals[1];

                        // System.out.println("yields 2: " + Float.parseFloat(numerator) / Float.parseFloat(divisor));
                        currentDimSize = "" + Float.parseFloat(numerator) / Float.parseFloat(divisor);
                    }
                } else if (decimalMatcher.matches()) {
                    System.out.println("text 3: " + dim);
                    System.out.println("first dimension 3: " + decimalMatcher.group(1));
                    currentDimSize = decimalMatcher.group(1);
                } else if (simpleDigitMatcher.matches()) {
                    // System.out.println("text 4: " + dim);
                    // System.out.println("first dimension 4: " + simpleDigitMatcher.group(1));
                    currentDimSize = simpleDigitMatcher.group(1);
                }
                if (StringUtils.isNotBlank(currentDimSize)) {
                    sizes.put("dim" + k, currentDimSize);
                }
                k++;
            }
        }
        // System.out.println("************* END *****************");
        return sizes;
    }
}
