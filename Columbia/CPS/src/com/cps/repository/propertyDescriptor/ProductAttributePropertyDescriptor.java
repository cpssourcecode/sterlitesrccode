package com.cps.repository.propertyDescriptor;

import java.util.Map;

import org.apache.commons.lang3.StringEscapeUtils;

import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/**
 * This class is
 *
 * @author Naga
 *
 */
public class ProductAttributePropertyDescriptor extends RepositoryPropertyDescriptor {
    private static final String PROPERTY_NAME = "attributeKey";
    private String propertyName;

    @Override
    public void setValue(String pAttributeName, Object pValue) {

        super.setValue(pAttributeName, pValue);

        if (PROPERTY_NAME.equals(pAttributeName)) {
            propertyName = (String) pValue;
        }
    }

    @Override
    public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
        Object result = null;
        if (pValue != null) {
            result = pValue;
        } else {
            StringBuilder retVal = new StringBuilder();
            Map<String, String> attrValueMap = (Map<String, String>) pItem.getPropertyValue(CPSConstants.ATTRIBUTE_VALUE_MAP);
            Map<String, String> attrValueUomMap = (Map<String, String>) pItem.getPropertyValue(CPSConstants.ATTRIBUTE_UOM_MAP);

            if (attrValueMap != null) {
                String primaryVal = attrValueMap.get(propertyName);
                if (StringUtils.isNotBlank(primaryVal)) {
                    retVal.append(primaryVal);
                }
                if (attrValueUomMap != null) {
                    String secVal = attrValueUomMap.get(propertyName);
                    if (StringUtils.isNotBlank(secVal)) {
                        retVal.append(" ").append(secVal);
                    }
                }
            }
            if (StringUtils.isNotBlank(retVal.toString())) {
                result = retVal.toString();
                result = StringEscapeUtils.unescapeHtml4(retVal.toString());
            }
            pItem.setPropertyValueInCache(this, result);
        }
        return result;
    }

}
