package com.cps.repository.propertyDescriptor;

import atg.adapter.gsa.GSAPropertyDescriptor;
import atg.repository.RepositoryItemImpl;

/**
 * @author Dmitry Golubev
 **/
public class CategoryECommerceDisplayPropDescriptor extends GSAPropertyDescriptor {
	/**
	 * @param pItem item to retrieve value
	 * @param pValue val
	 * @return default true if no value is specified, otherwise - what is stored in DB
	 */
	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
        Object superValue = super.getPropertyValue(pItem, pValue);
		if(superValue == null || !( superValue instanceof Boolean )) {
			return Boolean.TRUE;
		}
		return superValue;
	}
}
