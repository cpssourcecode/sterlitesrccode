package com.cps.repository.seo;

import java.util.HashMap;
import java.util.Map;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.nucleus.logging.ApplicationLogging;
import atg.repository.RepositoryException;
import atg.repository.seo.IndirectUrlTemplate;
import atg.repository.seo.ItemLinkException;
import atg.repository.seo.UrlParameter;
import atg.repository.seo.UrlParameterLookup;
import atg.service.webappregistry.WebApp;
import atg.servlet.DynamoHttpServletRequest;

public class CPSIndirectUrlTemplate extends IndirectUrlTemplate {

	public String formatUrl(UrlParameter pUrlParams[], WebApp pDefaultWebApp)
			throws ItemLinkException {
		return super.formatUrl(pUrlParams, pDefaultWebApp);
	}

	@Override
	public UrlParameter[] cloneUrlParameters() {
		UrlParameter cloneParams[] = new UrlParameter[mUrlParameters.length];
		Map<String, UrlParameterLookup> lookupMap = null;
		if (mLookups != null) {
			lookupMap = new HashMap<String, UrlParameterLookup>();
			for (UrlParameterLookup mLookup : mLookups) {
				lookupMap.put(mLookup.getName(), (UrlParameterLookup) mLookup.clone());
			}

		}
		for (int i = 0; i < cloneParams.length; i++) {
//			cloneParams[i] = (UrlParameter)mUrlParameters[i].clone();
			UrlParameter parameterToClone = mUrlParameters[i];
			cloneParams[i] = new UrlParameterWrap(mLogger, parameterToClone.getName());
			cloneParams[i].setValue(parameterToClone.getValue());
			cloneParams[i].setLookup(parameterToClone.getLookup());
			cloneParams[i].setEscaped(parameterToClone.isEscaped());

			if (cloneParams[i].getLookup() != null && lookupMap!=null) {
				cloneParams[i].setLookup(lookupMap.get(cloneParams[i].getLookup().getName()));
			}
		}

		return cloneParams;
	}
}

class UrlParameterWrap extends UrlParameter {
	UrlParameterWrap(ApplicationLogging pLogger, String pName) {
		super(pLogger, pName);
	}

	public String lookupValue(DynamoHttpServletRequest pRequest, Object pElementValue)
			throws ItemLinkException {
		String value = null;
		Object objectValue = null;
		Object object = null;
		if (getLookup() != null) {
			if (mLogger.isLoggingDebug())
				mLogger.logDebug("Getting value from lookup");
			try {
				value = getLookup().getValue(getPropertyName(), pRequest, pElementValue);
			} catch (RepositoryException re) {
				value = null;
			}
		}
		if (value == null) {
			if (mLogger.isLoggingDebug())
				mLogger.logDebug("Getting value from HTTP request");
			object = pRequest.getObjectParameter(mItemName);
			if (object == null) {
				if (getPropertyName() != null && getPropertyName().equalsIgnoreCase("template.url"))
					throw new ItemLinkException("ERROR_NO_REPOSITORY_ITEM");
				value = "";
			} else if (object instanceof String)
				value = (String) object;
			else if (getPropertyName() != null) {
				if (mLogger.isLoggingDebug())
					mLogger.logDebug("Getting value from repository item");
				try {
					objectValue = DynamicBeans.getSubPropertyValue(object, getPropertyName());
				} catch (PropertyNotFoundException pnfe) {
					if(mLogger.isLoggingDebug()){
						mLogger.logDebug(pnfe);
					}
				}
				if (objectValue != null)
					value = objectValue.toString();
			}
			if (value == null)
				value = object.toString();
		}
		if (value != null && isEscaped()) {
			value = value.replaceAll("[^a-zA-Z0-9]+"," ");
			value = urlEncodeParameter(value);
		}
		setValue(value);
		return value;
	}
}

