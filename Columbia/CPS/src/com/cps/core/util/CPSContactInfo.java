package com.cps.core.util;

import vsg.core.util.VSGContactInfo;

/**
 * @author Andy Porter
 */
public class CPSContactInfo extends VSGContactInfo {

	private String mJdeAddressNumber;


	/**
	 * Gets mJdeAddressNumber.
	 *
	 * @return Value of mJdeAddressNumber.
	 */
	public String getJdeAddressNumber() {
		return mJdeAddressNumber;
	}

	/**
	 * Sets new mJdeAddressNumber.
	 *
	 * @param pJdeAddressNumber New value of mJdeAddressNumber.
	 */
	public void setJdeAddressNumber(String pJdeAddressNumber) {
		mJdeAddressNumber = pJdeAddressNumber;
	}

	private String mSegmentName;

	public String getSegmentName() {
		return mSegmentName;
	}

	public void setSegmentName(String pSegmentName) {
		mSegmentName = pSegmentName;
	}

	private String mSegmentCode;

	public String getSegmentCode() {
		return mSegmentCode;
	}

	public void setSegmentCode(String pSegmentCode) {
		mSegmentCode = pSegmentCode;
	}

	private String mBusinessUnit;

	public String getBusinessUnit() {
		return mBusinessUnit;
	}

	public void setBusinessUnit(String pBusinessUnit) {
		mBusinessUnit = pBusinessUnit;
	}

	private String mAlternatePhoneNumberExt;

	public String getAlternatePhoneNumberExt() {
		return mAlternatePhoneNumberExt;
	}

	public void setAlternatePhoneNumberExt(String pAlternatePhoneNumberExt) {
		mAlternatePhoneNumberExt = pAlternatePhoneNumberExt;
	}

	private String mTaxCode;

	public String getTaxCode() {
		return mTaxCode;
	}

	public void setTaxCode(String pTaxCode) {
		mTaxCode = pTaxCode;
	}

	private String mAlternatePhoneNumber;

	public String getAlternatePhoneNumber() {
		return mAlternatePhoneNumber;
	}

	public void setAlternatePhoneNumber(String pAlternatePhoneNumber) {
		mAlternatePhoneNumber = pAlternatePhoneNumber;
	}

	private boolean mIsActive;

	public boolean getIsActive() {
		return mIsActive;
	}

	public void setIsActive(boolean pIsActive) {
		mIsActive = pIsActive;
	}

}
