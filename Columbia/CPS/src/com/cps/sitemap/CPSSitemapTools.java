package com.cps.sitemap;

import atg.sitemap.SitemapTools;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

public class CPSSitemapTools extends SitemapTools {

	String currentSite;

	public String getCurrentSite() {
		return currentSite;
	}
	public void setCurrentSite(String currentSite) {
		this.currentSite = currentSite;
	}

	/*public RepositoryItem[] lookupAllItems(RepositoryItemDescriptor pDesc) throws RepositoryException {
		RepositoryView view = pDesc.getRepositoryView();
		RqlStatement stmt = RqlStatement.parseRqlStatement("filename STARTS WITH ?0");
		Object[] params = { getCurrentSite() };
		RepositoryItem[] repItems = stmt.executeQuery(view, params);
		return repItems;
	}*/

}