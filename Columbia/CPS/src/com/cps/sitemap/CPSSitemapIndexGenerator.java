package com.cps.sitemap;

import atg.core.util.StringUtils;
import atg.sitemap.SitemapIndexGenerator;
import atg.sitemap.SitemapTools;

public class CPSSitemapIndexGenerator extends SitemapIndexGenerator {

	public String getSitemapUrl(String pFileName){
		StringBuilder url = new StringBuilder();
		if (getWebApp() != null) {
			String contextRoot = getWebApp().getContextRoot();
			if (!StringUtils.isBlank(contextRoot)) {
			    url.append(contextRoot).append(SitemapTools.SLASH);
			}
		}
		url.append(pFileName);
		if (getUrlPrefix() != null){
			SitemapTools.addPrefixToUrl(getUrlPrefix(), url);
		}
		return url.toString();
	}

}
