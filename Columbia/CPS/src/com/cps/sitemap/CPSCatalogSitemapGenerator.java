package com.cps.sitemap;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import atg.commerce.sitemap.CatalogSitemapGenerator;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.seo.ItemLinkException;
import atg.sitemap.SitemapGeneratorService;

public class CPSCatalogSitemapGenerator extends CatalogSitemapGenerator {
	private final static String PRODUCT_ITEM = "product";
	private final static String CATEGORY_ITEM = "category";
	private final static String PARENT_CATEGORY = "parentCategory";
	private final static String IS_SEARCHABLE = "searchable";
	private final static String ACTIVE = "activeFlag";
	private final static String CATEGORY_ID = "id";

	private final static String TRUE = "Yes";

	private String excludeCategory;
	private String site;

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public List<String> getSites() {
		List<String> sites = new ArrayList();
		sites.add(getSite());
		return sites;
	}

	public void generateSitemap(SitemapGeneratorService sitemapGeneratorService) {
		((CPSSitemapTools) ((CPSSitemapGeneratorService) sitemapGeneratorService)
				.getSitemapTools()).setCurrentSite(getSite());
		super.generateSitemap(sitemapGeneratorService);
	}

	protected Query getQuery(Repository repository, String itemDescriptorName)
			throws RepositoryException {
		RepositoryView repView = repository.getView(itemDescriptorName);
		QueryBuilder qb = repView.getQueryBuilder();
		String siteName = SiteContextManager.getCurrentSite()
				.getItemDisplayName();
		QueryExpression propExp = null;
		QueryExpression valueExp = null;
		QueryExpression propExp1 = null;
		QueryExpression valueExp1 = null;
		Query[] queries = new Query[2];
		Query query = null;
		if (itemDescriptorName.equals(PRODUCT_ITEM)) {
			if (!StringUtils.isBlank(getExcludeCategory())) {
				propExp = qb.createPropertyQueryExpression(ACTIVE);
				valueExp = qb.createConstantQueryExpression(TRUE);
				queries[0] = qb.createPatternMatchQuery(propExp, valueExp,
						QueryBuilder.EQUALS);
				propExp1 = qb.createPropertyQueryExpression(PARENT_CATEGORY);
				valueExp1 = qb
						.createConstantQueryExpression(getExcludeCategory());
				queries[1] = qb.createPatternMatchQuery(propExp1, valueExp1,
						QueryBuilder.NOT_EQUALS);
				query = qb.createAndQuery(queries);
			} else {
				propExp = qb.createPropertyQueryExpression(ACTIVE);
				valueExp = qb.createConstantQueryExpression(TRUE);
				query = qb.createPatternMatchQuery(propExp, valueExp,
						QueryBuilder.EQUALS);
			}

		} else if (itemDescriptorName.equals(CATEGORY_ITEM)) {
			if (!StringUtils.isBlank(getExcludeCategory())) {
				propExp = qb.createPropertyQueryExpression(CATEGORY_ID);
				valueExp = qb
						.createConstantQueryExpression(getExcludeCategory());
				if (isLoggingDebug()) {
					logDebug("propexp: " + propExp.getQueryRepresentation());
					logDebug("valueexp: " + valueExp.getQueryRepresentation());
				}
				query = qb.createPatternMatchQuery(propExp, valueExp,
						QueryBuilder.NOT_EQUALS);
			} else {
				propExp = qb.createPropertyQueryExpression(CATEGORY_ID);
				valueExp = qb.createConstantQueryExpression(" ");
				query = qb.createPatternMatchQuery(propExp, valueExp,
						QueryBuilder.NOT_EQUALS);
			}
		}
		return query;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.sitemap.DynamicSitemapGenerator#getSitemapURL(atg.repository.
	 * RepositoryItem, atg.repository.RepositoryItem,
	 * atg.sitemap.SitemapGeneratorService)
	 */
	public String getSitemapURL(RepositoryItem pRepositoryItem,
			RepositoryItem pSite,
			SitemapGeneratorService pSitemapGeneratorService)
			throws RepositoryException {

		if (null != pRepositoryItem
				&& CATEGORY_ITEM.equals(pRepositoryItem.getItemDescriptor()
						.getItemDescriptorName())) {
			try {
				return getHostSitemapURL(
						getCategorySitemapURL(pRepositoryItem, pSite,
								pSitemapGeneratorService), pSite,
						pSitemapGeneratorService);
			} catch (ItemLinkException e) {
				logError(e);
			}
		}
		return super.getSitemapURL(pRepositoryItem, pSite,
				pSitemapGeneratorService);
	}

	/**
	 * Gets category sitemap URL
	 * 
	 * @param pRepositoryItem
	 * @param pSite
	 * @param pSitemapGeneratorService
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected String getCategorySitemapURL(RepositoryItem pRepositoryItem,
			RepositoryItem pSite,
			SitemapGeneratorService pSitemapGeneratorService) {
		String result = null;

		if (null != pRepositoryItem && null != pSite
				&& null != pSitemapGeneratorService) {
			RepositoryItem parentCategory = pRepositoryItem;
			List<RepositoryItem> fixedChildCategories = (List<RepositoryItem>) pRepositoryItem
					.getPropertyValue("fixedChildCategories");
			if (fixedChildCategories != null
					&& fixedChildCategories.size() == 1) {
				parentCategory = (RepositoryItem) fixedChildCategories.get(0);

			}

			StringBuilder urlBuffer = new StringBuilder(200);

			String siteName = SiteContextManager.getCurrentSite()
					.getItemDisplayName();

			urlBuffer.append("/").append(parentCategory.getRepositoryId())
					.append(".cpsc");

			while (parentCategory != null) {
				String categorySeoName = (String) parentCategory
						.getPropertyValue("seoName");
				urlBuffer.insert(0, categorySeoName);
				urlBuffer.insert(0, "/");
				parentCategory = retrieveParentCategory(parentCategory);
			}
			result = urlBuffer.toString();
		}
		return result;
	}

	/**
	 * Returns parent category of the given category
	 * 
	 * @param pCategory
	 *            that parent category should be returned
	 * @return parent category if it exists, null otherwise
	 */
	@SuppressWarnings("unchecked")
	protected RepositoryItem retrieveParentCategory(RepositoryItem pCategory) {
		RepositoryItem parentCategory = null;
		if (pCategory != null) {
			parentCategory = (RepositoryItem) pCategory
					.getPropertyValue("cpsParentCategory");
			if (parentCategory == null) {
				Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) pCategory
						.getPropertyValue("fixedParentCategories");
				if (parentCategories != null && !parentCategories.isEmpty()) {
					// get first parent category
					parentCategory = parentCategories.iterator().next();
				}
			}
		}

		return parentCategory;
	}

	/**
	 * Returns full url with host and seo
	 * 
	 * @param pURL
	 * @param pSite
	 * @param pSitemapGeneratorService
	 * @return
	 * @throws ItemLinkException
	 */
	protected String getHostSitemapURL(String pURL, RepositoryItem pSite,
			SitemapGeneratorService pSitemapGeneratorService)
			throws ItemLinkException {
		String result = null;

		if (!StringUtils.isBlank(pURL) && null != pSite) {
			StringBuilder sb = new StringBuilder();
			String finalURL = pSitemapGeneratorService.getSiteURLManager()
					.getProductionSiteBaseURL(pSite.getRepositoryId(),
							getTemplate().formatUrl(null, null), null, null,
							false);
			if (!StringUtils.isBlank(finalURL)) {
				String[] finalURLs = finalURL.split("/");

				for (int i = 0; i < finalURLs.length - 1; i++) {
					sb.append(finalURLs[i]);
					if (i != finalURLs.length - 2) {
						sb.append("/");
					}
				}
				sb.append(pURL);
			}
			result = sb.toString();
		}

		return result;
	}

	public void setExcludeCategory(String excludeCategory) {
		this.excludeCategory = excludeCategory;
	}

	public String getExcludeCategory() {
		return excludeCategory;
	}

}