package com.cps.sitemap;

import atg.nucleus.GenericService;

/**
 * The Class SitemapConfiguration.
 */
public class SitemapConfiguration extends GenericService {

	/**
	 * The War dir.
	 */
	private String mWarDir;

	public String getWarDir() {
		return mWarDir;
	}

	public void setWarDir(String pWarDir) {
		mWarDir = pWarDir;
	}

}