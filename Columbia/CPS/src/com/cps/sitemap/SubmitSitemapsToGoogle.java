package com.cps.sitemap;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import vsg.notifications.InternalNotificationService;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

public class SubmitSitemapsToGoogle extends GenericService implements
		Schedulable {
	private final static String SCHEDULER_NAME = "Submit Sitemap to Google";
	private final static String PROD_URL_PREFIX = "www";
	private final static String GOOGLE_URL = "http://www.google.com/webmasters/tools/ping?sitemap=http%3A%2F%2F";
	int jobId;
	private Scheduler scheduler;
	private Schedule schedule;
	private String[] sitemaps;
	private InternalNotificationService internalNotificationService;
	private String adminEmailSubject;
	private String adminEmailAddress;

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	/**
	 * @return the sitemaps
	 */
	public String[] getSitemaps() {
		return sitemaps;
	}

	/**
	 * @param sitemaps the sitemaps to set
	 */
	public void setSitemaps(String[] sitemaps) {
		this.sitemaps = sitemaps;
	}

	/**
	 * @return the internalNotificationService
	 */
	public InternalNotificationService getInternalNotificationService() {
		return internalNotificationService;
	}

	/**
	 * @param internalNotificationService param
	 */
	public void setInternalNotificationService(
			InternalNotificationService internalNotificationService) {
		this.internalNotificationService = internalNotificationService;
	}

	/**
	 * @return the adminEmailSubject
	 */
	public String getAdminEmailSubject() {
		return adminEmailSubject;
	}

	/**
	 * @param adminEmailSubject param
	 */
	public void setAdminEmailSubject(String adminEmailSubject) {
		this.adminEmailSubject = adminEmailSubject;
	}

	/**
	 * @return the adminEmailAddress
	 */
	public String getAdminEmailAddress() {
		return adminEmailAddress;
	}

	/**
	 * @param adminEmailAddress param
	 */
	public void setAdminEmailAddress(String adminEmailAddress) {
		this.adminEmailAddress = adminEmailAddress;
	}

	public void doStartService() throws ServiceException {
		ScheduledJob job = new ScheduledJob(SCHEDULER_NAME, SCHEDULER_NAME,
				getAbsoluteName(), getSchedule(), this,
				ScheduledJob.REUSED_THREAD);
		jobId = getScheduler().addScheduledJob(job);
	}

	public void doStopService() {
		getScheduler().removeScheduledJob(jobId);
	}

	public void performScheduledTask(Scheduler scheduler, ScheduledJob job) {
		try {
			if (isLoggingDebug())
				logDebug("SubmitSitemapToGoogle invoked ----------------------->");
			boolean success = submitSitemaps();
			if (isLoggingDebug())
				logDebug("success? =" + success);
		} catch (Exception ex) {
			logError(ex);
		}
	}

	/**
	 * Submits the sitemaps to Google
	 *
	 * @return boolean
	 */
	public boolean submitSitemaps() {
		List<String> failedSitemaps = new ArrayList<>();
		for (String sitemap : getSitemaps()) {
			if (isLoggingDebug()) {
				logDebug("submitting sitemap: " + sitemap);
			}
			try {
				if (sitemap.contains(PROD_URL_PREFIX)) {
					URL url = new URL(GOOGLE_URL + sitemap);
					HttpURLConnection urlConnection = (HttpURLConnection) url
							.openConnection();
				}
			} catch (Exception ex) {
				logError(ex);
				failedSitemaps.add(sitemap);
			}
		}

		for (String sitemap : failedSitemaps){
			StringBuilder body = new StringBuilder();
			body.append("Error submitting sitemap - ")
					.append(sitemap)
					.append(" - to Google. 5 attempts were made.");
			getInternalNotificationService().setSubject(
					getAdminEmailSubject());
			getInternalNotificationService().setBody(
					body.toString());
			getInternalNotificationService().createNotification(
					getAdminEmailAddress(), null);
		}

		return failedSitemaps.isEmpty();
	}
}