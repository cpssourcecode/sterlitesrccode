package com.cps.sitemap;

import java.util.Iterator;

import org.apache.commons.lang.StringUtils;

import com.cps.multisite.CPSSiteURLManager;
import com.endeca.navigation.ENEConnection;
import com.endeca.navigation.ENEQueryException;
import com.endeca.navigation.ERec;
import com.endeca.navigation.FieldList;
import com.endeca.navigation.HttpENEConnection;
import com.endeca.navigation.UrlENEQuery;

import atg.dtm.TransactionDemarcation;
import atg.endeca.assembler.configuration.AssemblerApplicationConfiguration;
import atg.sitemap.DynamicSitemapGenerator;
import atg.sitemap.SitemapGeneratorService;
import atg.sitemap.SitemapTools;

/**
 * Generate sitemap based on endeca.
 * 
 * @author Vasili
 *
 */
public class CPSEndecaSitemapGenerator extends DynamicSitemapGenerator {

	/** The endeca configuration. */
	private AssemblerApplicationConfiguration mEndecaConfiguration;

	/**
	 * Gets the endeca configuration.
	 *
	 * @return the endeca configuration
	 */
	public AssemblerApplicationConfiguration getEndecaConfiguration() {
		return mEndecaConfiguration;
	}

	/**
	 * Sets the endeca configuration.
	 *
	 * @param pEndecaConfiguration the new endeca configuration
	 */
	public void setEndecaConfiguration(AssemblerApplicationConfiguration pEndecaConfiguration) {
		mEndecaConfiguration = pEndecaConfiguration;
	}

	/** The sitemap count. */
	private int sitemapCount;

	/* (non-Javadoc)
	 * @see atg.sitemap.DynamicSitemapGenerator#generateSitemap(atg.sitemap.SitemapGeneratorService)
	 */
	public void generateSitemap(SitemapGeneratorService pSitemapGeneratorService) {
		sitemapCount = 0;
		super.generateSitemap(pSitemapGeneratorService);
	}

	/* (non-Javadoc)
	 * @see atg.sitemap.DynamicSitemapGenerator#generateSitemapUrls(java.lang.String, atg.sitemap.SitemapGeneratorService)
	 */
	public void generateSitemapUrls(String pItemName, SitemapGeneratorService pSitemapService) {
		generateSitemapUrls(pItemName, pSitemapService, null);
	}

	/** The seo link name. */
	private String mSeoLinkName = "product.seoUrl";

	/**
	 * Gets the seo link name.
	 *
	 * @return the seo link name
	 */
	public String getSeoLinkName() {
		return mSeoLinkName;
	}

	/**
	 * Sets the seo link name.
	 *
	 * @param pSeoLinkName the new seo link name
	 */
	public void setSeoLinkName(String pSeoLinkName) {
		mSeoLinkName = pSeoLinkName;
	}

	/** The navigation. */
	private String mNavigation = null;

	/**
	 * Gets the navigation.
	 *
	 * @return the navigation
	 */
	public String getNavigation() {
		return mNavigation;
	}

	/**
	 * Sets the navigation.
	 *
	 * @param pNavigation the new navigation
	 */
	public void setNavigation(String pNavigation) {
		mNavigation = pNavigation;
	}

	private String recordFilter;
	
	  /**
     * @return the recordFilter
     */
    public String getRecordFilter() {
        return recordFilter;
    }

    /**
     * @param recordFilter the recordFilter to set
     */
    public void setRecordFilter(String recordFilter) {
        this.recordFilter = recordFilter;
    }
	/* (non-Javadoc)
	 * @see atg.sitemap.DynamicSitemapGenerator#generateSitemapUrls(java.lang.String, atg.sitemap.SitemapGeneratorService, java.lang.String)
	 */
	public void generateSitemapUrls(String pItemName, SitemapGeneratorService pSitemapService, String pSiteId) {
		SitemapTools sitemapTools = pSitemapService.getSitemapTools();
		try {
			TransactionDemarcation td = new TransactionDemarcation();
			StringBuilder si = new StringBuilder();
			sitemapTools.appendSitemapHeader(si);
			int urlCount = 0;
			sitemapCount += 1;
			td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
			if ( isLoggingDebug() ) {
				logDebug("generateSitemapUrls: begin transaction");
			}
			String siteUrl = ((CPSSiteURLManager) pSitemapService.getSiteURLManager()).getSiteUrl(pSiteId);
			Iterator<ERec> iter = queryRecords();
			while ( iter.hasNext() ) {
				ERec erec = iter.next();
				String urlXml = sitemapTools.generateSitemapUrlXml(siteUrl + (String)erec.getProperties().get(getSeoLinkName()),
						getChangeFrequency(), getPriority().toString(), pSitemapService.isDebugMode());
				if ( isNewFileNeeded(urlCount, pSitemapService, sitemapTools, urlXml, si) ) {
					sitemapTools.appendSitemapFooter(si);
					sitemapTools.writeSitemap(si, getSitemapFilePrefix(), sitemapCount);
					si = new StringBuilder();
					sitemapTools.appendSitemapHeader(si);
					urlCount = 0;
					sitemapCount += 1;
				}
				if ( null != urlXml ) {
					si.append(urlXml);
					++urlCount;
				}
			}
			sitemapTools.appendSitemapFooter(si);
			sitemapTools.writeSitemap(si, getSitemapFilePrefix(), this.sitemapCount);
			td.end();
		} catch ( Exception ex ) {
			if ( isLoggingError() ) {
				logError(ex);
			}
		}
	}

	private boolean isNewFileNeeded(int urlCount,
			SitemapGeneratorService pServ,
			SitemapTools pTools,
			String pXml,
			StringBuilder si) {
		return (urlCount >= pServ.getMaxUrlsPerSitemap()) || (!(pTools.validateSitemapSize(pXml, si, pServ.getMaxSitemapSize())));
	}
	
	private Iterator<ERec> queryRecords() throws ENEQueryException {
		FieldList fieldList = new FieldList();
		fieldList.addField(getSeoLinkName());
		return queryRecords(mEndecaConfiguration.getCurrentMdexHostName(),
				"" + mEndecaConfiguration.getCurrentMdexPort(),
				getNavigation(), fieldList);
	}

	/**
	 * Query records.
	 *
	 * @param eneHost the ene host
	 * @param enePort the ene port
	 * @param query the query
	 * @param fieldList the field list
	 * @return the iterator
	 * @throws ENEQueryException the eNE query exception
	 */
	private Iterator<ERec> queryRecords(String eneHost, String enePort, String query, FieldList fieldList)
			throws ENEQueryException {

		ENEConnection nec = new HttpENEConnection(eneHost, enePort);
		UrlENEQuery usq = new UrlENEQuery(query, "UTF-8");
		usq.setNavNumBulkERecs(-1);
		if(StringUtils.isNotBlank(getRecordFilter())) {
		    usq.setNavRecordFilter(getRecordFilter());
		}
		usq.setSelection(fieldList);
		return (Iterator<ERec>) nec.query(usq).getNavigation().getBulkERecIter();
	}

}