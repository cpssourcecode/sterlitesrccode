package com.cps.sitemap;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.sitemap.SitemapIndexGenerator;
import atg.sitemap.SitemapTools;
import atg.sitemap.SitemapWriterService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import com.cps.rmi.CacheInvalidatorImpl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.List;

public class CPSSitemapWriterService extends SitemapWriterService {
	private CacheInvalidatorImpl cacheInvalidatorImpl;
	private SitemapIndexGenerator mSitemapIndexGenerator;

	public SitemapIndexGenerator getSitemapIndexGenerator() {
		return mSitemapIndexGenerator;
	}

	public void setSitemapIndexGenerator(SitemapIndexGenerator pSitemapIndexGenerator) {
		mSitemapIndexGenerator = pSitemapIndexGenerator;
	}

	private String mSite;

	public String getSite() {
		return mSite;
	}

	public void setSite(String pSite) {
		mSite = pSite;
	}

	private String mSitemapFilesDirectory;

	public String getSitemapFilesDirectory() {
		return mSitemapFilesDirectory;
	}

	public void setSitemapFilesDirectory(String pSitemapFilesDirectory) {
		mSitemapFilesDirectory = pSitemapFilesDirectory;
	}

	private List<String> mSitemapFiles;

	public List<String> getSitemapFiles() {
		return mSitemapFiles;
	}

	public void setSitemapFiles(List<String> pSitemapFiles) {
		mSitemapFiles = pSitemapFiles;
	}

	public void writeSitemapFiles() {
		if (isLoggingDebug()) {
			logDebug("writeSitemapFiles() - start");
		}
		try {
			writeSitemaps(getSitemapTools().lookupSitemapItems());
//			writeSitemaps(getSitemapFilesDirectory());
			writeSiteIndex();
			//writeSiteindex(getSitemapTools().lookupSiteindexItems());
			getCacheInvalidatorImpl().invalidateCache(getSitemapTools().getSitemapRepository());
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		} catch (IOException ioe) {
			if (isLoggingError()) {
				logError(ioe);
			}
		}

		if (isLoggingDebug()) {
			logDebug("files available at " + getWarDir());
			logDebug("writeSitemapFiles() - end");
		}
			
	}

	public void writeSitemaps(String pDirectory) throws IOException {

		File directory = new File(pDirectory);
		Collection<File> files = FileUtils.listFiles(directory, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		List<String> fileNames = getSitemapFiles();
		String path = SitemapTools.formatWarDir(getWarDir()) + SitemapTools.FILE_SEPARATOR;
		if (isLoggingDebug()) {
			logDebug("Checking directory for sitemap files: " + pDirectory);
		}
		for (File localFile : files) {
			if (isLoggingDebug()) {
				logDebug("Checking file/dir: " + localFile.getAbsolutePath());
				logDebug("is file: " + localFile.isFile() + " ; name: " + localFile.getName());
			}
			if (localFile.isFile() && fileNames.contains(localFile.getName())) {
				Files.copy(localFile.toPath(), (new File(path + localFile.getName())).toPath(), StandardCopyOption.REPLACE_EXISTING);
			}
		}

	}

	private void writeSiteIndex() throws IOException {
		writeIndexToFile(generateSitemapIndex());
	}

	public void writeIndexToFile(String pXmlContent) {
		try {
			File file = new File(SitemapTools.formatWarDir(getWarDir()) + SitemapTools.FILE_SEPARATOR + getSitemapIndexGenerator().getSiteIndexFilename());
			try (FileWriter fileWriter = new FileWriter(file);) {
				fileWriter.write(pXmlContent);
				fileWriter.flush();
				fileWriter.close();
			}
		} catch (IOException ioe) {
			if (isLoggingError()) {
				logError(ioe);
			}
		}
	}

	private String generateSitemapIndex() throws IOException {
		StringBuilder siteIndex = new StringBuilder();
		try {
			getSitemapTools().appendSiteindexHeader(siteIndex);
			RepositoryItem[] sitemaps = getSitemapTools().lookupSitemapItems();
			for (RepositoryItem sitemap : sitemaps) {
				getSitemapTools().addSitemapEntry(siteIndex, getSitemapIndexGenerator().getSitemapUrl(sitemap, getSitemapPropertiesManager()), false);
			}

			File directory = new File(getSitemapFilesDirectory());
			List<String> fileNames = getSitemapFiles();
			Collection<File> files = FileUtils.listFiles(directory, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			for (File localFile : files) {
				if (localFile.isFile() && fileNames.contains(localFile.getName())) {
					getSitemapTools().addSitemapEntry(siteIndex, ((CPSSitemapIndexGenerator) getSitemapIndexGenerator()).getSitemapUrl(localFile.getName()), false);
				}
			}

			getSitemapTools().appendSiteindexFooter(siteIndex);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}
		return siteIndex.toString();
	}

	/**
	 * @return the cacheInvalidatorImpl
	 */
	public CacheInvalidatorImpl getCacheInvalidatorImpl() {
		return cacheInvalidatorImpl;
	}

	/**
	 * @param cacheInvalidatorImpl the cacheInvalidatorImpl to set
	 */
	public void setCacheInvalidatorImpl(CacheInvalidatorImpl cacheInvalidatorImpl) {
		this.cacheInvalidatorImpl = cacheInvalidatorImpl;
	}

}