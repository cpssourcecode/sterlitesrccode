package com.cps.sitemap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.cps.content.StaticPagesService;
import com.cps.content.VSGEndecaPageInfo;

import atg.core.util.StringUtils;
import atg.multisite.SiteURLManager;
import atg.repository.RepositoryException;
import atg.sitemap.SitemapGeneratorService;
import atg.sitemap.SitemapTools;
import atg.sitemap.StaticSitemapGenerator;
import com.cps.multisite.CPSSiteURLManager;

/**
 * Generate static pages.
 * 
 * @author Vasili Ivus
 *
 */
public class CPSStaticPagesGenerator extends StaticSitemapGenerator {

	/**
	 * The Constant SEPARATOR.
	 */
	public static final String SEPARATOR = "/";

	/**
	 * The Static pages service.
	 */
	private StaticPagesService mStaticPagesService;

	/**
	 * The Site url manager.
	 */
	private CPSSiteURLManager mSiteURLManager;

	private int sitemapCount;

	public List<VSGEndecaPageInfo> getResolvedStaticPagesInfo() {
		final List<VSGEndecaPageInfo> resolvedPages;
		final StaticPagesService staticPagesService = getStaticPagesService();
		if (null != staticPagesService) {
			resolvedPages = staticPagesService.getResolvedStaticPagesInfo();
		} else {
			resolvedPages = new ArrayList<>();
		}
		return resolvedPages;
	}
	
	public List<String> getResolvedStaticPages() {
		final List<String> resolvedPages;
		final StaticPagesService staticPagesService = getStaticPagesService();
		if (null != staticPagesService) {
			resolvedPages = staticPagesService.getResolvedStaticPages();
		} else {
			resolvedPages = new ArrayList<>();
		}
		return resolvedPages;
	}

	@Override
	public void generateSitemap(SitemapGeneratorService pSitemapGeneratorService) {
		vlogDebug("Start generating sitemap urls for static pages...");
		List sites = pSitemapGeneratorService.getSites();
		List pages = getResolvedStaticPages();
		setSitemapCount(0);
		if (sites != null) {
			Iterator sitesIterator = sites.iterator();
			do {
				if (!sitesIterator.hasNext())
					break;
				String site = (String)sitesIterator.next();
				generateSitemapUrls(pSitemapGeneratorService, pages, site);
				vlogDebug((new StringBuilder()).append("Finish generating static sitemap urls for site: ").append(site)
						.toString());
			} while (true);
		} else {
			generateSitemapUrls(pSitemapGeneratorService, pages, null);
		}
		vlogDebug("Finish generating sitemap urls for static pages");
	}

	@Override
	protected void generateSitemapUrls(SitemapGeneratorService pSitemapGeneratorService, List pages, String pSiteId) {
		if (pages != null) {
			SitemapTools sitemapTools = pSitemapGeneratorService.getSitemapTools();
			StringBuilder si = new StringBuilder();
			sitemapTools.appendSitemapHeader(si);
			int urlCount = 0;
			increaseSitemapCount();
			for (Iterator iterator = pages.iterator(); iterator.hasNext();) {
				String page = (String)iterator.next();
				if (isLoggingDebug())
					logDebug((new StringBuilder()).append("Next page: ").append(page).toString());
				//String relativeUrl = getPageURL(page);
				String pageChangeFrequency = getChangeFrequency();
				String pagePriority = getPriority().toString();
				/*String fullUrl = null;
				if (pSiteId == null) {
					fullUrl = getLocationForPage(page);
				}
				else{
					fullUrl = getLocationForPage(page, pSiteId, pSitemapGeneratorService);
				}*/
				String urlXml = sitemapTools.generateSitemapUrlXml(page, pageChangeFrequency, pagePriority,
						pSitemapGeneratorService.isDebugMode());
				boolean newSitemapStarted = addURLToSitemap(pSitemapGeneratorService, si, urlXml, urlCount,
						getSitemapCount());
				if (newSitemapStarted) {
					urlCount = 0;
					increaseSitemapCount();
				}
				urlCount++;
			}

			sitemapTools.appendSitemapFooter(si);
			sitemapTools.writeSitemap(si, getSitemapFilePrefix(), getSitemapCount());
		}
	}

	/**
	 * Increase sitemap count.
	 */
	private void increaseSitemapCount() {
		sitemapCount++;
	}

	private String buildChangeFrequency(final VSGEndecaPageInfo pPageInfo) {
		String changeFrequency = null;
		if (null != pPageInfo) {
			changeFrequency = pPageInfo.getChangeFrequency();
		}
		return (null == changeFrequency || StringUtils.isEmpty(changeFrequency)) ? getChangeFrequency()
				: changeFrequency;
	}

	private String buildPriority(final VSGEndecaPageInfo pPageInfo) {
		String priority = null;
		if (null != pPageInfo) {
			priority = pPageInfo.getPriority();
		}
		return (null == priority || StringUtils.isEmpty(priority)) ? getPriority().toString() : priority;
	}

	/**
	 * Gets the location for page.
	 *
	 * @param pPageURL the page url
	 * @param pSiteId the site id
	 * @param pSitemapGeneratorService the sitemap generator service
	 * @return the location for page
	 * @see atg.sitemap.StaticSitemapGenerator#getLocationForPage(java.lang.String, java.lang.String,
	 * atg.sitemap.SitemapGeneratorService)
	 */
	public String getLocationForPage(String pPageURL, String pSiteId,
			SitemapGeneratorService pSitemapGeneratorService) {
		final StringBuilder sb = new StringBuilder();
		final CPSSiteURLManager siteUrlManager = getCPSSiteURLManager(pSitemapGeneratorService);
		if (null != siteUrlManager) {
			final String siteUrl = siteUrlManager.getSiteUrl(pSiteId);
			if (StringUtils.isNotBlank(siteUrl) && StringUtils.isNotBlank(pPageURL)) {
				sb.append(siteUrl);
				if (!pPageURL.startsWith(SEPARATOR)) {
					sb.append(SEPARATOR);
				}
				sb.append(pPageURL);
			}
		}

		return sb.toString();
	}

	/**
	 * Gets the VSG site url manager.
	 *
	 * @param pSitemapGeneratorService the sitemap generator service
	 * @return the VSG site url manager
	 */
	protected CPSSiteURLManager getCPSSiteURLManager(final SitemapGeneratorService pSitemapGeneratorService) {
		CPSSiteURLManager vsgSiteURLManager = null;
		if (null != pSitemapGeneratorService) {
			final SiteURLManager siteManager = pSitemapGeneratorService.getSiteURLManager();
			if (siteManager instanceof CPSSiteURLManager) {
				vsgSiteURLManager = (CPSSiteURLManager)siteManager;
			}
		}
		return vsgSiteURLManager;
	}

	public StaticPagesService getStaticPagesService() {
		return mStaticPagesService;
	}

	public void setStaticPagesService(final StaticPagesService pStaticPagesService) {
		mStaticPagesService = pStaticPagesService;
	}

	public CPSSiteURLManager getSiteURLManager() {
		return mSiteURLManager;
	}

	public void setSiteURLManager(CPSSiteURLManager pSiteURLManager) {
		mSiteURLManager = pSiteURLManager;
	}

	public int getSitemapCount() {
		return sitemapCount;
	}

	public void setSitemapCount(int pSitemapCount) {
		sitemapCount = pSitemapCount;
	}

}
