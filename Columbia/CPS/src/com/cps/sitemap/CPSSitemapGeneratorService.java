package com.cps.sitemap;

import atg.sitemap.SitemapGenerator;
import atg.sitemap.SitemapGeneratorService;

import java.util.ArrayList;
import java.util.List;

public class CPSSitemapGeneratorService extends SitemapGeneratorService {
	private String site;

	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}

	public List<String> getSites() {
		List<String> sites = new ArrayList();
		sites.add(getSite());
		return sites;
	}
	
	public void generateSitemaps(){
		vlogDebug("GenerateSitemaps");
		vlogDebug("Sitemap generators - "+getSitemapGenerators());
		for (SitemapGenerator gen : getSitemapGenerators()){
			vlogDebug("Generator - "+gen);
		}
		super.generateSitemaps();
	}
}