package com.cps.endeca;

import java.util.Map;

import atg.nucleus.GenericService;

public class CPSDimensionService extends GenericService {

	private Map<String, String> mDimensionContextMap;

	public Map<String, String> getDimensionContextMap() {
		return mDimensionContextMap;
	}

	public void setDimensionContextMap(Map<String, String> pDimensionContextMap) {
		mDimensionContextMap = pDimensionContextMap;
	}

	private String mDefaultDimensionContext;

	public String getDefaultDimensionContext() {
		return mDefaultDimensionContext;
	}

	public void setDefaultDimensionContext(String pDefaultDimensionContext) {
		mDefaultDimensionContext = pDefaultDimensionContext;
	}
	

}