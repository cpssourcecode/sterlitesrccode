package com.cps.endeca.assembler.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

/**
 * Validator for navigation state's parameters and filters.
 *
 */
public class NavigationStateValidator extends GenericService {

	/** The Constant EMPTY_SYMBOL. */
	private static final String EMPTY_SYMBOL = " ";

	/** The search terms skip symbols. */
	private List<String> mSearchTermsSkipSymbols = new ArrayList<String>();

	/** The search terms regexp. */
	private String mSearchTermsRegexp;

	/** The param name regexp map. */
	private Map<String,String> mParamNameRegexpMap = new HashMap<String,String>();

	/** The nav state param validation enabled. */
	private boolean mNavStateParamValidationEnabled;

	/** The search terms validation enabled. */
	private boolean mSearchTermsValidationEnabled = true;

	/**
	 * Validate navigation state.
	 *
	 * @param pNavigationState the navigation state
	 * @return the navigation state
	 */
	public NavigationState validateNavigationState(NavigationState pNavigationState) {
		NavigationState resultNavState = pNavigationState;

		if (getSearchTermsValidationEnabled()) {
			resultNavState = validateSearchTerms(pNavigationState);
		}

		if (getNavStateParamValidationEnabled()) {
			resultNavState = validateNavStateParams(resultNavState);
		}

		return resultNavState;
	}

	/**
	 * Validate search terms.
	 *
	 * @param pNavigationState the navigation state
	 * @return the navigation state
	 */
	public NavigationState validateSearchTerms(NavigationState pNavigationState) {
		NavigationState resultNavState = pNavigationState;
		if (pNavigationState != null) {
			List<SearchFilter> listSf = pNavigationState.getFilterState().getSearchFilters();
			if (null != listSf && !listSf.isEmpty()) {
				boolean searchFilterChanged = false;
				List<SearchFilter> listSfToRemove = new ArrayList<SearchFilter>();
				for (SearchFilter sf : listSf) {
					String terms = sf.getTerms();
					if (!StringUtils.isBlank(terms) && !isValidSearchTerm(terms)) {
						searchFilterChanged = true;
						terms = getParsedSearchURL(terms);
						if (StringUtils.isBlank(terms)) {
							sf.setTerms(" ");
						} else {
							sf.setTerms(terms);
						}
					}
				}
				if (searchFilterChanged) {
					updateSearchFiltersList(listSf, listSfToRemove);
					resultNavState = pNavigationState.updateSearchFilters(listSf);
				}
			}
		}

		return resultNavState;
	}

	/**
	 * Validate nav state params.
	 *
	 * @param pNavigationState the navigation state
	 * @return the navigation state
	 */
	public NavigationState validateNavStateParams(NavigationState pNavigationState) {
		NavigationState resultNavState = pNavigationState;
		if (pNavigationState != null) {
			List<String> paramNamesToRemove = new ArrayList<String>();
			for (Map.Entry<String,String> paramRegexpEntry : getParamNameRegexpMap().entrySet()) {
				String paramValue = pNavigationState.getParameter(paramRegexpEntry.getKey());
				if (paramValue != null) {
					if (!paramValue.matches(paramRegexpEntry.getValue())) {
						paramNamesToRemove.add(paramRegexpEntry.getKey());
					}
				}
			}
			resultNavState = updateNavStateParametersList(pNavigationState, paramNamesToRemove);
		}

		return resultNavState;
	}

	/**
	 * Update search filters list.
	 *
	 * @param pListSf the list sf
	 * @param pListSfToRemove the list sf to remove
	 */
	private void updateSearchFiltersList(List<SearchFilter> pListSf, List<SearchFilter> pListSfToRemove) {
		if (pListSf != null && !pListSf.isEmpty() && pListSfToRemove != null && !pListSfToRemove.isEmpty()) {
			pListSf.removeAll(pListSfToRemove);
		}
	}

	/**
	 * Update nav state parameters list.
	 *
	 * @param pNavigationState the navigation state
	 * @param pParamNamesToRemove the param names to remove
	 * @return the navigation state
	 */
	private NavigationState updateNavStateParametersList(NavigationState pNavigationState, List<String> pParamNamesToRemove) {
		if (pParamNamesToRemove != null) {
			for (String paramName : pParamNamesToRemove) {
				pNavigationState = pNavigationState.removeParameter(paramName);
			}
		}

		return pNavigationState;
	}

	/**
	 * Checks if is valid search term.
	 *
	 * @param value the value
	 * @return true, if is valid search term
	 */
	private boolean isValidSearchTerm(String value) {
		return value.matches(getSearchTermsRegexp());
	}

	/**
	 * Gets the parsed search url.
	 *
	 * @param pURL the url
	 * @return the parsed search url
	 */
	protected String getParsedSearchURL(String pURL) {
		String result = pURL;

		if (!StringUtils.isBlank(pURL)) {
			for (String symbol : getSearchTermsSkipSymbols()) {
				result = result.replace(symbol, EMPTY_SYMBOL);
			}
			result = result.trim();
		}

		return result;
	}

	public List<String> getSearchTermsSkipSymbols() {
		return mSearchTermsSkipSymbols;
	}

	public void setSearchTermsSkipSymbols(List<String> pSearchTermsSkipSymbols) {
		mSearchTermsSkipSymbols = pSearchTermsSkipSymbols;
	}

	public String getSearchTermsRegexp() {
		return mSearchTermsRegexp;
	}

	public void setSearchTermsRegexp(String pSearchTermsRegexp) {
		mSearchTermsRegexp = pSearchTermsRegexp;
	}

	public Map<String, String> getParamNameRegexpMap() {
		return mParamNameRegexpMap;
	}

	public void setParamNameRegexpMap(Map<String, String> pParamNameRegexpMap) {
		mParamNameRegexpMap = pParamNameRegexpMap;
	}

	public boolean getNavStateParamValidationEnabled() {
		return mNavStateParamValidationEnabled;
	}

	public void setNavStateParamValidationEnabled(
			boolean pNavStateParamValidationEnabled) {
		mNavStateParamValidationEnabled = pNavStateParamValidationEnabled;
	}

	public boolean getSearchTermsValidationEnabled() {
		return mSearchTermsValidationEnabled;
	}

	public void setSearchTermsValidationEnabled(
			boolean pSearchTermsValidationEnabled) {
		mSearchTermsValidationEnabled = pSearchTermsValidationEnabled;
	}
}
