package com.cps.endeca.assembler.navigation;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.endeca.assembler.navigation.NavigationStateProcessor;

import com.cps.commerce.endeca.cache.CPSDimensionValueCacheObject;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.content.UserState;
//import com.endeca.infront.navigation.model.RangeFilter;
//import com.endeca.infront.navigation.model.SearchFilter;

/**
 * Listing SEO Navigation State Processor. Check navigation state to add
 * segment to set on the user state and used within Experience Manager
 * to control routing the user to the category page instead of the search results page.
 *
 * "listing" segment for more than one navigation filter.
 * "subCategory" segment for sub category navigation filter.
 *
 * @author Vasili
 *
 */
public class ListingSeoNavigationStateProcessor implements NavigationStateProcessor {

	/** The listing segment. */
	private String mListingSegment;

	/**
	 * Sets the listing segment.
	 *
	 * @param pListingSegment the new listing segment
	 */
	public void setListingSegment(String pListingSegment) {
		mListingSegment = pListingSegment;
	}

	/**
	 * Gets the listing segment.
	 *
	 * @return the listing segment
	 */
	public String getListingSegment() {
		return mListingSegment;
	}

	/** The category level segment. */
	private String mCategoryLevelSegment;

	/**
	 * Sets the category level segment.
	 *
	 * @param pCategoryLevelSegment the new listing segment
	 */
	public void setCategoryLevelSegment(String pCategoryLevelSegment) {
		mCategoryLevelSegment = pCategoryLevelSegment;
	}

	/**
	 * Gets the category level segment.
	 *
	 * @return the category level segment
	 */
	public String getCategoryLevelSegment() {
		return mCategoryLevelSegment;
	}

	/** The sub category segment. */
	private String mSubCategorySegment;

	/**
	 * Sets the sub category segment.
	 *
	 * @param pSubCategorySegment the new listing segment
	 */
	public void setSubCategorySegment(String pSubCategorySegment) {
		mSubCategorySegment = pSubCategorySegment;
	}

	/**
	 * Gets the sub category segment.
	 *
	 * @return the sub category segment
	 */
	public String getSubCategorySegment() {
		return mSubCategorySegment;
	}

	/** The segment map. */
	private Map<String, String> mSegmentMap;

	/**
	 * Gets the segment map.
	 *
	 * @return the segment map
	 */
	public Map<String, String> getSegmentMap() {
		return mSegmentMap;
	}

	/**
	 * Sets the segment map.
	 *
	 * @param pSegmentMap the segment map
	 */
	public void setSegmentMap(Map<String, String> pSegmentMap) {
		mSegmentMap = pSegmentMap;
	}

	/** The user state. */
	private UserState mUserState;

	/**
	 * Sets the user state.
	 *
	 * @param pUserState the new user state
	 */
	public void setUserState(UserState pUserState) {
		mUserState = pUserState;
	}

	/**
	 * Gets the user state.
	 *
	 * @return the user state
	 */
	public UserState getUserState() {
		return (mUserState);
	}

	/** The dimension value cache tools. */
	private DimensionValueCacheTools mDimensionValueCacheTools;

	/**
	 * Sets the dimension value cache tools.
	 *
	 * @param pDimensionValueCacheTools the new dimension value cache tools
	 */
	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	/**
	 * Gets the dimension value cache tools.
	 *
	 * @return the dimension value cache tools
	 */
	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	/** CategoryDimensionName. */
	private String mCategoryDimensionName;

	public void setCategoryDimensionName(String pCategoryDimensionName) {
		mCategoryDimensionName = pCategoryDimensionName;
	}

	public String getCategoryDimensionName() {
		return mCategoryDimensionName;
	}

	/**
	 * Process navigation state.
	 *
	 * @param pNavigationState this navigation state.
	 */
	@Override
	public NavigationState process(NavigationState pNavigationState) {
		List<String> navFilters = pNavigationState.getFilterState().getNavigationFilters();
		//List<RangeFilter> rangeFilters = pNavigationState.getFilterState().getRangeFilters();
		//List<SearchFilter> searchFilters = pNavigationState.getFilterState().getSearchFilters();
		UserState userState = getUserState();
		if ( null != userState ) {
			if ( navFilters.size() > 0 ) {
				DimensionValueCacheObject dimObj = getDimensionValueCacheTools().getCachedObjectForDimval(navFilters.get(0));
				if ( null != dimObj ) {
					String repositoryId = dimObj.getRepositoryId();
					if ( !StringUtils.isBlank(repositoryId) && repositoryId.contains(":") ) {
						Map<String, String> segmentMap = getSegmentMap();
						if ( null != segmentMap ) {
							for ( Entry<String, String> segment: segmentMap.entrySet() ) {
								if ( repositoryId.startsWith(segment.getKey()) ) {
									userState.addUserSegments(segment.getValue());
								}
							}
						}
					} else {
						List<String> ids = dimObj.getAncestorRepositoryIds();
						if (navFilters.size() > 1) {
							getUserState().addUserSegments("categoryLevel3");
						} else {
							if (null == ids) {
								if (dimObj instanceof CPSDimensionValueCacheObject) {
									if (getCategoryDimensionName().equals(((CPSDimensionValueCacheObject)dimObj).getDimensionName())) {
										// for root category landing template
										userState.addUserSegments(getCategoryLevelSegment() + "1");
									}
								}
							} else {
								int idsSize = ids.size();
								userState.addUserSegments(getCategoryLevelSegment() + (idsSize + 1));
								if (0 < idsSize) {
									userState.addUserSegments(getSubCategorySegment());
								}
							}
						}
					}
				}
			}
		}

		return pNavigationState;

	}

	/**
	 * Checks if is special path. Not Endeca SEO request(brand/collection)
	 *
	 * @param pPath the path
	 * @return true, if is special path
	 */
	private boolean isSpecialPath(String pPath) {
		return null !=pPath && pPath.contains("/_/");
	}

}
