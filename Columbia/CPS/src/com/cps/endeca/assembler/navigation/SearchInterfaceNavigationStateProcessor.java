package com.cps.endeca.assembler.navigation;

import java.util.List;

import atg.endeca.assembler.navigation.NavigationStateProcessor;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.cps.service.OrganizationAliasService;
import com.cps.util.CPSConstants;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;

/**
 * NavigationStateProcessor to change search filter interface according to current profile
 */
public class SearchInterfaceNavigationStateProcessor implements NavigationStateProcessor {
	
	/**
	 * profile
	 */
	private Profile mProfile;
	
	/**
	 * Organization Alias Service
	 */
	private OrganizationAliasService mOrganizationAliasService;
	
	/**
	 * Process navigation state.
	 *
	 * @param pNavigationState this navigation state.
	 */
	@Override
	public NavigationState process(NavigationState pNavigationState) {
		List<SearchFilter> searchFilters = pNavigationState.getFilterState().getSearchFilters();
		if (getProfile() != null && !getProfile().isTransient() && getOrganizationAliasService().isEnableSearchByAliasInterface()) {
			RepositoryItem org = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
			if (org != null && searchFilters != null && !searchFilters.isEmpty() && getOrganizationAliasService().isOrganizationHasAlias(org.getRepositoryId())) {
				for (SearchFilter filter : searchFilters) {
					if ("All".equals(filter.getKey())) {
						filter.setKey("All_" + org.getRepositoryId());
					}
				}
			}
			pNavigationState = pNavigationState.updateSearchFilters(searchFilters);
		}
		return pNavigationState;
	}

	public Profile getProfile() {
		return mProfile;
	}

	public void setProfile(Profile pProfile) {
		mProfile = pProfile;
	}
	
	public OrganizationAliasService getOrganizationAliasService() {
		return mOrganizationAliasService;
	}

	public void setOrganizationAliasService(OrganizationAliasService pOrganizationAliasService) {
		mOrganizationAliasService = pOrganizationAliasService;
	}
}
