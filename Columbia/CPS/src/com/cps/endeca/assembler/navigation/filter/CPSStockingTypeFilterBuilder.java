package com.cps.endeca.assembler.navigation.filter;

import atg.endeca.assembler.navigation.filter.PropertyConstraint;
import atg.endeca.assembler.navigation.filter.RecordFilterBuilderImpl;
import vsg.endeca.EndecaConstants;

import atg.endeca.assembler.navigation.filter.FilterUtils;

/**
 * Builds the record filter based product's stockingType property .
 *
 * @author Andy P.
 */
public class CPSStockingTypeFilterBuilder extends RecordFilterBuilderImpl {


	/**
	 * Flag indicates if the filter is enabled.
	 */
	private boolean mEnabled;

	/**
	 * The filter.
	 */
	private String mFilter;

	public boolean getEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	public String getFilter() {
		return mFilter;
	}

	public void setFilter(String pFilter) {
		mFilter = pFilter;
	}

	@Override
	public String buildRecordFilter() {
		if (getFilter() != null) {
			return getFilter();
		} else {
			buildFilter();
			return getFilter();
		}
	}

	/**
	 * Builds the filter.
	 */
	protected void buildFilter() {
		if (getEnabled()) {
//			String filter = FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "U").toString();
//			filter = FilterUtils.not(filter);

			PropertyConstraint[] constraints = new PropertyConstraint[4];
			constraints[0] = FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "O");
			constraints[1] = FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "U");
			constraints[2] = FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "K");
			constraints[3] = FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "X");
			String filter = FilterUtils.not(FilterUtils.or(constraints));

//			String filter = FilterUtils.not(FilterUtils.or(
//					FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "O"),
//					FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "U"),
//					FilterUtils.constraint(EndecaConstants.STOCKING_TYPE_PROPERTY_NAME, "K")));

			setFilter(filter);
		} else {
			setFilter(null);
		}
	}
}
