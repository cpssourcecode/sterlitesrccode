package com.cps.endeca.assembler.navigation;

import javax.servlet.http.HttpServletRequest;

import atg.endeca.assembler.navigation.ExtendedNavigationStateBuilder;

import com.endeca.infront.navigation.NavigationException;
import com.endeca.infront.navigation.NavigationState;


public class CPSNavigationStateBuilder extends ExtendedNavigationStateBuilder {
	
	private NavigationStateValidator mNavigationStateValidator;

	@Override
	public NavigationState parseNavigationState(HttpServletRequest request) throws NavigationException {
		NavigationState navigationState = super.parseNavigationState(request);

		NavigationStateValidator navStateValidator = getNavigationStateValidator();

		if (navStateValidator != null) {
			navigationState = navStateValidator.validateNavigationState(navigationState);
		}

		return navigationState;
	}
	
	public NavigationStateValidator getNavigationStateValidator() {
		return mNavigationStateValidator;
	}

	public void setNavigationStateValidator(
			NavigationStateValidator pNavigationStateValidator) {
		mNavigationStateValidator = pNavigationStateValidator;
	}
}