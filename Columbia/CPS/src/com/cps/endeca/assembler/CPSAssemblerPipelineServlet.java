package com.cps.endeca.assembler;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerPipelineServlet;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.cps.seo.SeoTools;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RedirectAwareContentInclude;
import vsg.constants.VSGConstants;
import vsg.endeca.EndecaConstants;

import static vsg.constants.VSGConstants.NAVIGATION_PARAM;
import static vsg.constants.VSGConstants.SECONDARY_SEARCH_PARAM;

public class CPSAssemblerPipelineServlet extends AssemblerPipelineServlet {

	public static final String NAVIGATION_STATE_COMPONENT_NAME = "/atg/endeca/assembler/cartridge/manager/NavigationState";

	/**
	 * The Make single product result redirect flag.
	 */
	private boolean mMakeSingleProductResultRedirect = true;

	/**
	 * The Redirecting url template.
	 */
	private String mRedirectingUrlTemplate = "/global/gadgets/redirect-container.jsp?url={0}";


	public static final String PAGE_NOT_FOUND_N_1 = "/pageNotFound.jsp?N=1";
	public static final String PAGE_NOT_FOUND = "/pageNotFound.jsp";
	public static final String HTML_URL_EXTENSION = ".html";

	private SeoTools seoTools;
	private String mZeroResultsUrlTemplate = "/global/gadgets/zero-results-container.jsp";

	/**
	 * is page is not eligble for assembler pipeline servlet and not found found
	 *
	 * @param pRequest
	 * @return true if not found, false otherwise.
	 */
	private boolean isPageNotFound(DynamoHttpServletRequest pRequest) {
		if (!getSeoTools().isSeoRequest(pRequest) && shouldIgnoreRequest(pRequest)) {
			vlogDebug("CPSAssemblerPipelineServlet Ignoring (passing on) request for " + pRequest.getRequestURI());
			if (PAGE_NOT_FOUND.equals(pRequest.getRequestURI()) && !StringUtils.isBlank(pRequest.getQueryParameter(NAVIGATION_PARAM))) {
				return true;
			}
		}
		return false;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		if (isEnable() && !shouldIgnoreRequest(pRequest)) {
			if (isPageNotFound(pRequest)) {
				RequestDispatcher dispatcher = pRequest.getRequestDispatcher(PAGE_NOT_FOUND_N_1);
				dispatcher.forward(pRequest, pResponse);
				return;
			}
		}

		super.service(pRequest, pResponse);
	}

	/**
	 * Check pRequest for the zero results redirect URL attribute.
	 *
	 * @param pRequest  the request
	 * @param pResponse the response
	 * @return true if request was redirected to zero results url
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected boolean redirectZeroResults(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		boolean zeroResultsRedirect = false;
		if (!StringUtils.isBlank(pRequest.getQueryParameter(SECONDARY_SEARCH_PARAM))
				&& Boolean.TRUE.equals(pRequest.getAttribute(VSGConstants.REDIRECT_PARAM_EMPTY_RESULTS))) {
			zeroResultsRedirect = true;
			forwardToUrl(pRequest, pResponse, getZeroResultsUrlTemplate());
		}
		return zeroResultsRedirect;
	}

	/*
 	* Do redirect to PDP for single product result. Forward request without
 	* modifying request URI when site request with virtual context root
 	*
 	* @see
 	* atg.endeca.assembler.AssemblerPipelineServlet#forwardRequest(atg.servlet
 	* .DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse,
 	* com.endeca.infront.assembler.ContentItem)
 	*/
	@Override
	protected void forwardRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
								  ContentItem pContentItem) throws ServletException, IOException {

		if (!redirectSingleProductResult(pRequest, pResponse, pContentItem) && !redirectZeroResults(pRequest, pResponse)) {
			super.forwardRequest(pRequest, pResponse, pContentItem);
		}
	}

	@Override
	public void passRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
		Object failed = pRequest.getAttribute(EndecaConstants.INVOKE_ASSEMBLER_FAILED_ATTR_NAME);
		if (failed instanceof Boolean && ((Boolean) failed)) {
			pRequest.removeAttribute(EndecaConstants.INVOKE_ASSEMBLER_FAILED_ATTR_NAME);
			pResponse.sendRedirect(PAGE_NOT_FOUND);
			return;
		}
		super.passRequest(pRequest, pResponse);
	}

	@Override
	protected ContentItem createContentInclude(DynamoHttpServletRequest pRequest, String pResourcePath) {
		String newResourcePath = pResourcePath;

		if (newResourcePath == null) {
			if (getSeoTools().isSeoRequest(pRequest) && !getAssemblerTools().getContentPath(pRequest).startsWith("/search")) {
				newResourcePath = getSeoTools().getPagePath(pRequest);
			} else {
				newResourcePath = getAssemblerTools().getContentPath(pRequest);
			}
		}

		return new RedirectAwareContentInclude(newResourcePath);
	}


	/**
	 * Check pRequest for the single product redirect URL attribute and redirect if it isn't blank.
	 *
	 * @param pRequest     the request
	 * @param pResponse    the response
	 * @param pContentItem the root content item
	 * @return true if request was redirected to PDP
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected boolean redirectSingleProductResult(DynamoHttpServletRequest pRequest,
												  DynamoHttpServletResponse pResponse, ContentItem pContentItem) throws ServletException, IOException {
		boolean singleProductRedirect = false;
		if (isMakeSingleProductResultRedirect() && StringUtils.isNotEmpty(getRedirectingUrlTemplate())
				&& StringUtils.isBlank(pRequest.getQueryParameter(NAVIGATION_PARAM))
				&& StringUtils.isBlank(pRequest.getQueryParameter(SECONDARY_SEARCH_PARAM))) {
			String prodUrl = (String) pRequest.getAttribute(VSGConstants.SINGLE_PRODUCT_REDIRECT_PARAM);
			if (!StringUtils.isBlank(prodUrl)) {
				vlogDebug("product url = {0} for redirect to PDP is set in the request.", prodUrl);
				String redirectUrl = MessageFormat.format(getRedirectingUrlTemplate(), prodUrl);
				//redirectRequest(pRequest, pResponse, pContentItem, redirectUrl);
				forwardToUrl(pRequest, pResponse, redirectUrl);
				singleProductRedirect = true;
			}
		}
		return singleProductRedirect;
	}

	/**
	 * Forward to specified URL.
	 *
	 * @param pRequest  the request
	 * @param pResponse the response
	 * @param pUrl      the url
	 * @throws ServletException the servlet exception
	 * @throws IOException      Signals that an I/O exception has occurred.
	 */
	protected void forwardToUrl(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
								String pUrl) throws ServletException, IOException {

		RequestDispatcher dispatcher = pRequest.getRequestDispatcher(pUrl);
		dispatcher.forward(pRequest, pResponse);
	}


	public SeoTools getSeoTools() {
		return seoTools;
	}

	public void setSeoTools(SeoTools pSeoTools) {
		seoTools = pSeoTools;
	}

	public String getZeroResultsUrlTemplate() {
		return mZeroResultsUrlTemplate;
	}

	public void setZeroResultsUrlTemplate(String pZeroResultsUrlTemplate) {
		mZeroResultsUrlTemplate = pZeroResultsUrlTemplate;
	}

	/**
	 * @return the makeSingleProductResultRedirect
	 */
	public boolean isMakeSingleProductResultRedirect() {
		return mMakeSingleProductResultRedirect;
	}

	/**
	 * @param pMakeSingleProductResultRedirect the makeSingleProductResultRedirect to set
	 */
	public void setMakeSingleProductResultRedirect(boolean pMakeSingleProductResultRedirect) {
		mMakeSingleProductResultRedirect = pMakeSingleProductResultRedirect;
	}

	/**
	 * @return the redirectingUrlTemplate
	 */
	public String getRedirectingUrlTemplate() {
		return mRedirectingUrlTemplate;
	}

	/**
	 * @param pRedirectingUrlTemplate the redirectingUrlTemplate to set
	 */
	public void setRedirectingUrlTemplate(String pRedirectingUrlTemplate) {
		mRedirectingUrlTemplate = pRedirectingUrlTemplate;
	}


}