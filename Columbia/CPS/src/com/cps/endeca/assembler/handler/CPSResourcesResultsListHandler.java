package com.cps.endeca.assembler.handler;

import java.util.List;

import atg.endeca.assembler.cartridge.handler.ResultsListHandler;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.ResultsListConfig;

public class CPSResourcesResultsListHandler extends ResultsListHandler {
	
	@Override
	public void preprocess(ResultsListConfig cartridgeConfig) throws CartridgeHandlerException {
		
		if(getNavigationState()!=null && getNavigationState().getFilterState()!=null){
			List<String> recordFilters =getNavigationState().getFilterState().getRecordFilters();
			recordFilters.add("product.type:cps-content");
			getNavigationState().updateRecordFilters(recordFilters);
		}
		super.preprocess(cartridgeConfig);
	}
}
