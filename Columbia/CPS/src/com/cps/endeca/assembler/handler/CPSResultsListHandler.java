package com.cps.endeca.assembler.handler;


import static vsg.constants.VSGConstants.INDEX_PRODUCT_URL;
import static vsg.constants.VSGConstants.OFFSET_PARAM;
import static vsg.constants.VSGConstants.RECORDS_PER_PAGE_PARAM;
import static vsg.constants.VSGConstants.SORTING_PARAM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.endeca.infront.cartridge.model.CPSSortOptionCinfig;
import com.cps.endeca.infront.cartridge.model.RecordsPerPageConfig;
import com.cps.endeca.infront.support.RecordsPerPageControl;
import com.cps.seo.SeoTools;
import com.cps.service.CPSProductPricingService;
import com.cps.service.OrganizationAliasService;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.ResultsList;
import com.endeca.infront.cartridge.ResultsListConfig;
import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.Record;
import com.endeca.infront.cartridge.model.SortOptionConfig;
import com.endeca.infront.cartridge.model.SortOptionLabel;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.model.SortSpec;

import atg.commerce.order.OrderHolder;
import atg.core.util.StringUtils;
import atg.endeca.assembler.cartridge.handler.ResultsListHandler;
import atg.nucleus.ServiceEvent;
import atg.nucleus.ServiceException;
import atg.nucleus.ServiceListener;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.logging.ApplicationLoggingSender;
import atg.nucleus.logging.LoggingPropertied;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import vsg.constants.VSGConstants;
import vsg.endeca.EndecaConstants;

/**
 * Created by
 */
public class CPSResultsListHandler extends ResultsListHandler implements LoggingPropertied, ServiceListener {

	ApplicationLoggingImpl mLogging = new ApplicationLoggingImpl(this.getClass().getName());

	public static final String SORTING_OPTIONS_PARAM = "sortOptions";
	public static final String RECORDS_PER_PAGE_OPTIONS_PARAM = "recordsPerPageOptions";
	public static final String DEFAULT_RECORDS_PER_PAGE = "20";
	public static final String SEARCH_PARAM = "Ntt";
	private static final String ITEM_PRICES = "itemPrices";
	private static final String RECORD_SEARCH_AUTO_SUGGEST_ITEM_TYPE = "RecordSearchAutoSuggestItem";

	private RecordsPerPageConfig[] mRecordsPerPageOptions = new RecordsPerPageConfig[0];
	private SortOptionConfig[] mSearchSorters;
	private List<SortOptionConfig> mSearchSortOptions;
	private List<SortOptionLabel> mSortOptionLabels;
	private String mPopularRecordsDisplayProperty;
	private CPSProductPricingService productPricingService;

	/**
	 * Organization Alias Service
	 */
	private OrganizationAliasService mOrganizationAliasService;



	private Profile mProfile;

	private OrderHolder mShoppingCart;

	private SeoTools mSeoTools;

	/**
	 * @return the productPricingService
	 */
	public CPSProductPricingService getProductPricingService() {
		return productPricingService;
	}

	/**
	 * @param productPricingService
	 *            the productPricingService to set
	 */
	public void setProductPricingService(CPSProductPricingService productPricingService) {
		this.productPricingService = productPricingService;
	}

	public ApplicationLoggingSender getLogging() {
		return mLogging;
	}

	public SeoTools getSeoTools() {
		return mSeoTools;
	}

	public void setSeoTools(SeoTools pSeoTools) {
		mSeoTools = pSeoTools;
	}

	public void startService (ServiceEvent pEvent) throws ServiceException {
		mLogging.initializeFromServiceEvent(pEvent);
	}

	@Override
	public void stopService() throws ServiceException {

	}

	@Override
	public ResultsList process(ResultsListConfig pCartridgeConfig) throws CartridgeHandlerException {
		Object recordsType = pCartridgeConfig.get("recordsType");

		ResultsList resultList = null;
		if(recordsType == null || !recordsType.equals("popular")) {

			if (getLogging().isLoggingDebug()) {
				getLogging().logDebug("ResultsListHandler - NavState Before: "+getNavigationState().getFilterState().getSearchFilters());
			}
			mSortOptionLabels = populateSortOptions(pCartridgeConfig);
			processSortParameters(pCartridgeConfig);
			resultList = super.process(pCartridgeConfig);

			if (getLogging().isLoggingDebug()) {
				getLogging().logDebug("Items count: " + resultList.getTotalNumRecs());
				getLogging().logDebug("ResultsListHandler - NavState After: "+getNavigationState().getFilterState().getSearchFilters());
			}

			String sortParam = getNavigationState().getParameter(SORTING_PARAM);

			if (StringUtils.isNotBlank(sortParam)) {
				NavigationAction na = resultList.getPagingActionTemplate();
				String ns = na.getNavigationState();
				ns = updateParam(ns, SORTING_PARAM, sortParam);
				na.setNavigationState(ns);
				resultList.setPagingActionTemplate(na);
			}
			resultList.put(RECORDS_PER_PAGE_OPTIONS_PARAM, populateRecordsPerPageOptions(pCartridgeConfig, getNavigationState()));
			resultList.put(SORTING_OPTIONS_PARAM, mSortOptionLabels);

			checkSingleProduct(resultList);
			checkZeroResults(resultList);
			if (!RECORD_SEARCH_AUTO_SUGGEST_ITEM_TYPE.equals(pCartridgeConfig.getType()))  {
				resultList = priceRecords(resultList);
			}

			
		}else{
			resultList = super.process(pCartridgeConfig);

			if(getPopularRecordsDisplayProperty() != null) {
				Set<String> popularPropertySet = new LinkedHashSet<>();

				List<Record> recordList = new ArrayList<>();

				for (Record record : resultList.getRecords()) {
					Attribute attribute = record.getAttributes().get(getPopularRecordsDisplayProperty());
					if(attribute != null && attribute.size() > 0) {
						String propertyValue = attribute.get(0).toString();
						if (!popularPropertySet.contains(propertyValue)) {
							popularPropertySet.add(propertyValue);
							recordList.add(record);
						}
					}
				}
				resultList.setRecords(recordList);
			}

//			return resultsList;
		}
		if (resultList != null) {
			changeSeoUrlToLower(resultList);
		}
		return resultList;
	}


	public ResultsList priceRecords(ResultsList pResultsList) throws CartridgeHandlerException {
	 
		// commenting out the checking of webservice enablement here. it has
		// been moved to CPSProductPricingService.
		
		// if (mExternalProductService.isPricingWebserviceEnable()) {
			List<Record> records = pResultsList.getRecords();
			if (records != null) {
				pResultsList = priceItems(records, pResultsList);
			}
		// }
			
		return pResultsList;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void changeSeoUrlToLower(ResultsList resultList) {
		List<Record> records = resultList.getRecords();
		for (Record record : records) {
			Attribute attribute = record.getAttributes().get("product.seoUrl");
			String seo = attribute.toString();
			attribute.set(0, seo.toLowerCase());
		}
	}

	/**
	 * Price all products from in PLP page
	 *
	 * @param pRecords     pRecords
	 * @param pResultsList pResultsList
	 */

	private ResultsList priceItems(List<Record> pRecords, ResultsList pResultsList) throws CartridgeHandlerException {
		Set<String> products = new HashSet<String>();
		for (Record record : pRecords) {
			Attribute attribute = record.getAttributes().get("product.repositoryId");
			String productId = attribute.toString();
			products.add(productId);
		}

		try {
            // get the prices from the ProductPricingService
			Map<String, Double> itemPrices = getProductPricingService().getCustomerItemPrice(products, getProfile(), (CPSOrderImpl) getShoppingCart().getCurrent());
			pResultsList.put(ITEM_PRICES, itemPrices);
		} catch (Exception ex) {
			if (getLogging().isLoggingError()) {
				getLogging().logError(ex);
			}
			throw new CartridgeHandlerException("Cannot get prices: ", ex);
		}
		return pResultsList;
	}


	/**
	 * processes sort parameters (removes incorrect)
	 */
	private void processSortParameters(ResultsListConfig pCartridgeConfig) {
		if (pCartridgeConfig.getSortOption() != null) {
			List<SortSpec> sorts = pCartridgeConfig.getSortOption().getSorts();
			List<SortSpec> copySorts = new ArrayList<SortSpec>(sorts);
			if (sorts != null && sorts.size() > 0) {
				for (SortSpec sort : sorts) {
					if (getSortOptions() != null && getSortOptions().size() > 0 && !StringUtils.isEmpty(sort.getKey())) {
						List<String> sortingValues = new ArrayList<String>();
						boolean isCustomSorting = false;
						for (SortOptionConfig sortConfig : getSortOptions()) {
							if (sortConfig instanceof CPSSortOptionCinfig && !StringUtils.isEmpty(sort.getKey()) && sort.getKey().startsWith(EndecaConstants.ALIAS_PROPERTY_NAME)) {
								isCustomSorting = true;
								String[] splittedSorting = sort.getKey().split(EndecaConstants.ALIAS_PROPERTY_NAME);
								String orgId = splittedSorting != null && splittedSorting.length > 0 ? splittedSorting[splittedSorting.length - 1] : "";
								if (!getOrganizationAliasService().isOrganizationHasAlias(orgId)) {
									copySorts.remove(sort);
								}
							} else if (!StringUtils.isEmpty(sortConfig.getValue())) {
								String sortKey = sortConfig.getValue().split("\\|")[0];
								sortingValues.add(sortKey);
							}
						}
						if (!sortingValues.contains(sort.getKey()) && !isCustomSorting) {
							copySorts.remove(sort);
						}
					}
				}
			}
			if (copySorts.isEmpty()) {
				pCartridgeConfig.getSortOption().setDefault(true);
			}
			pCartridgeConfig.getSortOption().setSorts(copySorts);
		}

	}

	protected List<RecordsPerPageControl> populateRecordsPerPageOptions(ResultsListConfig pCartridgeConfig,
																		NavigationState pNavigationState) {
		String sortParam = pNavigationState.getParameter(SORTING_PARAM);
		List<SearchFilter> searchFilters = pNavigationState.getFilterState().getSearchFilters();
		int size = getRecordsPerPageOptions() == null ? 0 : getRecordsPerPageOptions().length;
		List<RecordsPerPageControl> controls = new ArrayList<RecordsPerPageControl>(size);
		if (size > 0) {
			for (RecordsPerPageConfig rppConfig : getRecordsPerPageOptions()) {
				RecordsPerPageControl rppControl = new RecordsPerPageControl();
				rppControl.setLabel(rppConfig.getLabel());
				rppControl.setRecordsPerPage(rppConfig.getRecordsPerPage());
				if (rppConfig.getRecordsPerPage() == 0) {
					rppControl.setNavigationState(pNavigationState.removeParameter(RECORDS_PER_PAGE_PARAM)
							.removeParameter(OFFSET_PARAM).updateSearchFilters(searchFilters).toString());
				} else {
					NavigationState holder = pNavigationState;
					String rpp = Integer.toString(rppConfig.getRecordsPerPage());
					String holderUrl = holder.toString();
					holderUrl = updateParam(holderUrl, RECORDS_PER_PAGE_PARAM, rpp);
					if (StringUtils.isNotBlank(sortParam)) {
						holderUrl = updateParam(holderUrl, SORTING_PARAM, sortParam);
					}
					rppControl.setNavigationState(holderUrl);

				}

				populateNavigationPathDefaults(rppControl);
				controls.add(rppControl);
			}
		}
		return controls;
	}

	protected List<SortOptionLabel> populateSortOptions(ResultsListConfig resultsListConfig) {
		List<SortOptionConfig> sortOptionConfigs = getSortOptions();
		NavigationState navigationState = getNavigationState();
		List<SearchFilter> searchFilters = navigationState.getFilterState().getSearchFilters();
		List<SortOptionLabel> sortOptionLabelList = new ArrayList<SortOptionLabel>();
		String sortParam;
		if (null != sortOptionConfigs) {
			sortParam = null;
			if (null != resultsListConfig.getSortOption() && !resultsListConfig.getSortOption().isDefault()) {
				sortParam = resultsListConfig.getSortOption().toString();
			}
			for (SortOptionConfig sortOptionConfig : sortOptionConfigs) {

				if ((sortOptionConfig instanceof CPSSortOptionCinfig && !StringUtils.isEmpty(sortOptionConfig.getValue()) && getOrganizationAliasService().isOraganizationHasAlias((RepositoryItem) getProfile())) 
						|| !(sortOptionConfig instanceof CPSSortOptionCinfig)) {
					SortOptionLabel sortOptionLabel = new SortOptionLabel();
					sortOptionLabel.setLabel(sortOptionConfig.getLabel());
					populateNavigationPathDefaults(sortOptionLabel);
					if (null != sortOptionConfig) {
						if (((null == sortParam) ? "" : sortParam).equals(sortOptionConfig.getValue())) {
							sortOptionLabel.setSelected(true);
						}
					}
					if (null == sortOptionConfig || null == sortOptionConfig.getValue() || 0 == sortOptionConfig.getValue().length()) {
						sortOptionLabel.setNavigationState(navigationState.removeParameter("No").removeParameter("Ns").removeParameter("Nr").toString());
					} else {
						navigationState.getUrlFilterState().setRecordFilters(new ArrayList<String>());
						String holderUrl = navigationState.toString();
						holderUrl = updateParam(holderUrl, SORTING_PARAM, sortOptionConfig.getValue()).toString();
						sortOptionLabel.setNavigationState(holderUrl);
					}
					sortOptionLabelList.add(sortOptionLabel);
				}
			}
		}
		return sortOptionLabelList;
	}

	public static String updateParam(String pUrl, String pParamName, String pParamValue) {
		String result = null;
		if (pUrl != null && !StringUtils.isBlank(pParamName) && pParamValue != null) {
			String paramFirstPart = pParamName + "=";
			if (!pUrl.contains(paramFirstPart)) {
				result = new StringBuilder(pUrl).append(pUrl.contains("?") ? "&" : "?").append(paramFirstPart).append(pParamValue).toString();
			} else {
				int start = pUrl.indexOf(paramFirstPart);
				int end = pUrl.indexOf('&', start);
				if (end < 0)
					end = pUrl.length();
				result = new StringBuilder(pUrl).replace(start, end, paramFirstPart + pParamValue).toString();
			}
		}

		return result;
	}

	/**
	 * Adds zero results found redirect param
	 *
	 * @param pResultsList output model, contains result records
	 */
	protected void checkZeroResults(ResultsList pResultsList) {
		if (pResultsList != null && pResultsList.getTotalNumRecs() == 0) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			request.setAttribute(VSGConstants.REDIRECT_PARAM_EMPTY_RESULTS, Boolean.TRUE);
		}
	}

	/**
	 * Adds single product redirect parameter
	 *
	 * @param pResultsList output model, contains result records
	 */
	protected void checkSingleProduct(ResultsList pResultsList) {
		// do not redirect if there are navigation filters, on /browse pages it should return list with one item
		if (pResultsList != null && getNavigationState().getUrlFilterState().getNavigationFilters().size() == 0) {
			if (pResultsList.getTotalNumRecs() == 1) {
				List<Record> records = pResultsList.getRecords();

				// one record result, set redirect
				DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
				String productLink = getProductPDPUrl(records.get(0), request);
				if (StringUtils.isNotEmpty(productLink)) {
					// set attribute in order to get it on the
					// AssemblerPipelineServlet and do a redirect to the PDP
					request.setAttribute(VSGConstants.SINGLE_PRODUCT_REDIRECT_PARAM, productLink);
				}
			}
		}
	}

	private String seoLink(final Object productId) {
		String link = "/catalog/pdp.jsp?prodId=" + productId;
		SeoTools seoTools = getSeoTools();
		RepositoryItem product;
		try {
			product = seoTools.getCatalogTools().findProduct("" + productId);
		} catch (RepositoryException e) {
			if (mLogging.isLoggingError()) {
				mLogging.logError(e);
			}
			product = null;
		}
		if (null != product) {
			link = seoTools.makeSisterSeoLink(product);
		}
		return link;
	}

	/**
	 * Gets the url to product detail page.
	 *
	 * @param pRecord  the record
	 * @param pRequest the request
	 * @return the product pdp url
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	private String getProductPDPUrl(Record pRecord, DynamoHttpServletRequest pRequest) {
		String url = null;
		Attribute<String> productUrlAttribute = pRecord.getAttributes().get(INDEX_PRODUCT_URL);
		if (productUrlAttribute == null || productUrlAttribute.isEmpty()) {
			Attribute productId = pRecord.getAttributes().get(VSGConstants.INDEX_PRODUCT_ID);
			url = seoLink(productId.get(0));
		} else {
			url = productUrlAttribute.get(0);
		}
		return url;
	}

	public RecordsPerPageConfig[] getRecordsPerPageOptions() {
		return mRecordsPerPageOptions;
	}

	public void setRecordsPerPageOptions(RecordsPerPageConfig[] pRecordsPerPageOptions) {
		this.mRecordsPerPageOptions = pRecordsPerPageOptions;
	}

	public SortOptionConfig[] getSearchSorters() {
		return mSearchSorters;
	}

	public void setSearchSorters(SortOptionConfig[] pSorters) {
		mSearchSorters = pSorters;
		setSearchSortOptions((null != mSearchSorters) ? Arrays.asList(mSearchSorters) : new ArrayList<SortOptionConfig>());
	}

	protected void setSearchSortOptions(List<SortOptionConfig> sortOptions) {
		mSearchSortOptions = sortOptions;
	}

	public List<SortOptionConfig> getSearchSortOptions() {
		return mSearchSortOptions;
	}

	private List<SearchFilter> getSearchFilters() {
		return getNavigationState().getFilterState().getSearchFilters();
	}

	private boolean isEmptySearchFilters(List<SearchFilter> pSearchFilters) {
		return (null == pSearchFilters || pSearchFilters.isEmpty());
	}

	private boolean isEmptySearchFilters() {
		return isEmptySearchFilters(getSearchFilters());
	}

	public OrganizationAliasService getOrganizationAliasService() {
		return mOrganizationAliasService;
	}

	public void setOrganizationAliasService(
			OrganizationAliasService pOrganizationAliasService) {
		this.mOrganizationAliasService = pOrganizationAliasService;
    }

	public String getPopularRecordsDisplayProperty() {
		return mPopularRecordsDisplayProperty;
	}

	public void setPopularRecordsDisplayProperty(String pPopularRecordsDisplayProperty) {
		mPopularRecordsDisplayProperty = pPopularRecordsDisplayProperty;
    }

	public Profile getProfile() {
		return mProfile;
	}

	public void setProfile(Profile pProfile) {
		mProfile = pProfile;
	}

	public OrderHolder getShoppingCart() {
		return mShoppingCart;
	}

	public void setShoppingCart(OrderHolder pShoppingCart) {
		mShoppingCart = pShoppingCart;
	}
}
