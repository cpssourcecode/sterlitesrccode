package com.cps.endeca.assembler.content;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cps.content.VSGEndecaPageInfo;
import com.endeca.infront.shaded.org.apache.commons.io.IOUtils;
import com.endeca.store.Node;
import com.endeca.store.Store;

import atg.core.util.StringUtils;
import atg.endeca.assembler.content.ExtendedFileStoreFactory;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.logging.ClassLoggingFactory;
import vsg.constants.VSGConstants;

/**
 * Class DefaultFileStoreFactory add current revision name getter.
 * 
 * @author Vasili Ivus
 *
 */
public class DefaultFileStoreFactory extends ExtendedFileStoreFactory  {

	/**
	 * The Logging.
	 */
	protected ApplicationLoggingImpl mLogging = (ApplicationLoggingImpl)ClassLoggingFactory.getFactory()
			.getLoggerForClass(DefaultFileStoreFactory.class);

	/**
	 * Endeca navigation pages
	 */
	private String[] mNavigationPages;

	/**
	 * Array of skip pages
	 */
	private String[] mSkipPages;

	/**
	 * The Nodes to skip.
	 */
	private String[] mNodesToSkip;

	/**
	 * Gets current revision name
	 * 
	 * @return current rev name
	 * @throws IOException IO error
	 */
	public String getCurrentRevName() throws IOException {
		String currentZipName = null;
		BufferedReader reader = null;
		final File referenceFile = new File(getConfigurationPath(), "current_application_config.txt");
		if (referenceFile.exists()) {
			try {
				reader = new BufferedReader(new FileReader(referenceFile));
				currentZipName = reader.readLine();
			} finally {
				IOUtils.closeQuietly(reader);
			}
		}
		return currentZipName;
	}

	/**
	 * Get store pages
	 * 
	 * @return store pages
	 */
	public Map<String, ? extends Node> getPages() {
		Map<String, ? extends Node> pages = null;
		final Store store = getStore();
		Node rnode = (null == store) ? null : store.getRootAsset().getDataNode();
		if (null != rnode) {
			Map<String, ? extends Node> nodes = rnode.getNodes();
			Node pagesNode = (null == nodes) ? null : nodes.get("pages");
			if (null != pagesNode) {
				Map<String, ? extends Node> sites = pagesNode.getNodes();
				if (null != sites && !sites.isEmpty()) {
					final String siteName = getSiteName(sites.keySet().iterator());
					mLogging.vlogDebug("Selected site name: {0}", siteName);
					Node site = sites.get(siteName);
					// Node site = sites.get(sites.keySet().iterator().next());
					pages = (null == site) ? null : site.getNodes();
				}
			}
		}
		return pages;
	}

	/**
	 * Gets site name, the first not equal "ecr:permissions"
	 *
	 * @param pSites sites
	 * @return site name
	 */
	private String getSiteName(Iterator<String> pSites) {
		String result = VSGConstants.EMPTY;
		while (pSites.hasNext()) {
			String siteName = pSites.next();
			mLogging.vlogDebug("Check site name: {0}", siteName);
			if (!siteName.equals("ecr:permissions")) {
				result = siteName;
				break;
			}
		}
		mLogging.vlogDebug("Return site name: {0}", result);
		return result;
	}

	/**
	 * Get static pages
	 * 
	 * @return static pages
	 */
	public List<String> getStaticPages() {
		final List<String> statics = new ArrayList<String>();
		final Map<String, ? extends Node> pages = getPages();
		if (null != pages) {
			for (Entry<String, ? extends Node> page : pages.entrySet()) {
				final String key = page.getKey();
				if (StringUtils.isNotBlank(key) && isNotNavigationPage(key)) {
					addChildrenNodes(page, statics, VSGConstants.EMPTY);
				}
			}
		}
		return statics;
	}

	/**
	 * Get static pages Info(for VSGStaticPagesSitemapGenerator)
	 * 
	 * @return static pages
	 */
	public List<VSGEndecaPageInfo> getStaticPagesEndecaInfo() {
		final List<VSGEndecaPageInfo> statics = new ArrayList<VSGEndecaPageInfo>();
		final Map<String, ? extends Node> pages = getPages();
		if (null != pages) {
			for (Entry<String, ? extends Node> page : pages.entrySet()) {
				final String key = page.getKey();
				if (StringUtils.isNotBlank(key) && isNotSkipPage(key)) {
					mLogging.vlogDebug("addChildreNodesInfo for Page = {0}", key);
					addChildrenNodesInfo(page, statics, VSGConstants.EMPTY);
				}
			}
		}
		return statics;
	}

	/**
	 * Adds the children nodes.
	 *
	 * @param pPage the page
	 * @param pStatics the statics
	 */
	protected void addChildrenNodes(final Entry<String, ? extends Node> pPage, final List<String> pStatics,
			final String pParent) {
		if (null != pPage && null != pStatics) {
			final String key = pPage.getKey();
			final String url = new StringBuilder(pParent).append(VSGConstants.SLASH).append(key).toString();
			if (StringUtils.isNotBlank(key) && isNotNodeToSkipPage(key)) {
				pStatics.add(url);
			}
			final Map<String, ? extends Node> children = pPage.getValue().getNodes();
			if (null != children) {
				for (Entry<String, ? extends Node> child : children.entrySet()) {
					addChildrenNodes(child, pStatics, url);
				}
			}
		}
	}

	/**
	 * Adds the children nodes info
	 *
	 * @param pPage the page
	 * @param pStatics the statics ( List of VSGEndecaPageInfo )
	 * @param pParent the parent
	 */
	protected void addChildrenNodesInfo(final Entry<String, ? extends Node> pPage,
			final List<VSGEndecaPageInfo> pStatics, final String pParent) {
		if (null != pPage && null != pStatics) {
			final String key = pPage.getKey();
			final String url = new StringBuilder(pParent).append(VSGConstants.SLASH).append(key).toString();
			if (StringUtils.isNotBlank(key) && isNotNodeToSkipPage(key)) {
				if ("home".equals(key)) {
					pStatics.add(0, getPageInfo(pPage, url));
				} else {
					pStatics.add(getPageInfo(pPage, url));
				}
			}
			final Map<String, ? extends Node> children = pPage.getValue().getNodes();
			if (null != children) {
				for (Entry<String, ? extends Node> child : children.entrySet()) {
					addChildrenNodesInfo(child, pStatics, url);
				}
			}
		}
	}

	/**
	 * Gets page info
	 *
	 * @param pPage the page
	 * @param pUrl the URL
	 * @return the page info
	 */
	protected VSGEndecaPageInfo getPageInfo(final Entry<String, ? extends Node> pPage, final String pUrl) {
		VSGEndecaPageInfo result = new VSGEndecaPageInfo();
		result.setPageURL(pUrl);
		Map<String, ? extends Node> nodeMap = pPage.getValue().getNodes();
		mLogging.vlogDebug("NodeMap: {0}", nodeMap);
		if (nodeMap != null) {
			Node content = nodeMap.get("content.xml");
			mLogging.vlogDebug("Content: {0}", content);
			if (content != null) {
				Map<String, Object> mapProperties = content.getProperties();
				mLogging.vlogDebug("mapProperties: {0}", mapProperties);
				if (mapProperties != null) {
					Document doc = getXmlDocument((byte[])mapProperties.get("ecr:data"));
					mLogging.vlogDebug("Document: {0}", doc);
					if (doc != null) {
						Element root = doc.getDocumentElement();
						NodeList nodeList = root.getElementsByTagName("Property");
						for (int i = 0; i < nodeList.getLength(); ++i) {
							Element el = (Element)nodeList.item(i);
							org.w3c.dom.Node property = el.getAttributes().item(0);
							if ("priority".equals(property.getNodeValue())) {
								result.setPriority(getElementValue(el));
							}
							if ("changeFrequency".equals(property.getNodeValue())) {
								result.setChangeFrequency(getElementValue(el));
							}
						}
					}
				}
			}
		}

		return result;
	}

	/**
	 * Gets value from element
	 *
	 * @param pElement the document's element
	 * @return element's value
	 */
	private String getElementValue(final Element pElement) {
		return pElement.getTextContent().trim();
	}

	/**
	 * gets document from data
	 *
	 * @param pData the document data
	 * @return the document
	 */
	private Document getXmlDocument(final byte[] pData) {
		Document result = null;
		if (pData != null) {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			try {
				DocumentBuilder builder = factory.newDocumentBuilder();
				result = builder.parse(new ByteArrayInputStream(pData));
			} catch (ParserConfigurationException e) {
				mLogging.vlogError("Error in parsing Xml:" + e);
			} catch (SAXException e) {
				mLogging.vlogError("Error in parsing Xml:" + e);
			} catch (IOException e) {
				mLogging.vlogError("Error in parsing Xml:" + e);
			}
		}
		return result;
	}

	/**
	 * Get endeca pages
	 * 
	 * @return endeca pages
	 */
	public Set<String> getEndecaPages() {
		final Set<String> statics = new HashSet<String>();
		final Map<String, ? extends Node> pages = getPages();
		addEndecaPage(statics, pages, VSGConstants.EMPTY);
		return statics;
	}

	/**
	 * add endeca pages
	 * 
	 * @param pStatics - array of static pages
	 * @param pPages - pages
	 */
	private void addEndecaPage(Set<String> pStatics, Map<String, ? extends Node> pPages, String pUrl) {
		if (null != pPages) {
			for (Entry<String, ? extends Node> page : pPages.entrySet()) {
				String url = pUrl + (page.getValue().getNodes().isEmpty() ? VSGConstants.EMPTY : VSGConstants.SLASH + page.getKey());

				Map<String, ? extends Node> innerNode = page.getValue().getNodes();
				for (Entry<String, ? extends Node> innerPage : innerNode.entrySet()) {
					if (!innerPage.getValue().getNodes().isEmpty()) {
						pStatics.add(url);
						String cUrl = url + VSGConstants.SLASH + innerPage.getKey();
						pStatics.add(cUrl);
						addEndecaPage(pStatics, innerPage.getValue().getNodes(), cUrl);
					}
				}

				if (StringUtils.isNotBlank(url)) {
					pStatics.add(url);
				}

			}
		}
	}

	/**
	 * Check endeca pages
	 * 
	 * @param pValue page
	 * @return if endeca page then true
	 */
	public boolean isEndecaPage(final String pValue) {
		boolean result;
		final Set<String> endecaPages = getEndecaPages();
		result = !StringUtils.isBlank(pValue) && endecaPages.contains(pValue);
		return result;
	}

	/**
	 * Checks if is navigation page.
	 *
	 * @param pValue the value
	 * @return true, if is navigation page
	 */
	public boolean isNavigationPage(final String pValue) {
		return !isNotNavigationPage(pValue);
	}

	/**
	 * Checks if is not navigation page.
	 *
	 * @param pValue the value
	 * @return true, if is not navigation page
	 */
	public boolean isNotNavigationPage(final String pValue) {
		return (null == mNavigationPages || 0 > Arrays.binarySearch(mNavigationPages, pValue));
	}

	/**
	 * Checks if is not skip page.
	 * 
	 * @param pValue the value
	 * @return true, if is not skip page
	 */
	public boolean isNotSkipPage(final String pValue) {
		return (null == getSkipPages() || 0 > Arrays.binarySearch(getSkipPages(), pValue));
	}

	/**
	 * Checks if is node to skip page.
	 *
	 * @param pValue the value
	 * @return true, if is node to skip page
	 */
	public boolean isNodeToSkipPage(final String pValue) {
		return !isNotNodeToSkipPage(pValue);
	}

	/**
	 * Checks if is not node to skip page.
	 *
	 * @param pValue the value
	 * @return true, if is not node to skip page
	 */
	public boolean isNotNodeToSkipPage(final String pValue) {
		return (null == mNodesToSkip || 0 > Arrays.binarySearch(mNodesToSkip, pValue));
	}

	/**
	 * Check not endeca pages
	 * 
	 * @param pValue page
	 * @return if not endeca page then true
	 */
	public boolean isNotEndecaPage(final String pValue) {
		return !isEndecaPage(pValue);
	}

	public String[] getNavigationPages() {
		return mNavigationPages;
	}

	public void setNavigationPages(String[] pNavigationPages) {
		mNavigationPages = pNavigationPages;
	}

	public String[] getNodesToSkip() {
		return mNodesToSkip;
	}

	public void setNodesToSkip(String[] pNodesToSkip) {
		mNodesToSkip = pNodesToSkip;
	}

	public String[] getSkipPages() {
		return mSkipPages;
	}

	public void setSkipPages(String[] pSkipPages) {
		mSkipPages = pSkipPages;
	}
}
