package com.cps.endeca.assembler.urlformatter;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import com.cps.commerce.endeca.cache.CPSDimensionValueCacheTools;
import com.cps.endeca.CPSDimensionService;
import com.cps.seo.SeoTools;
import com.cps.util.CPSConstants;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimLocationList;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.MDimLocationList;
import com.endeca.soleng.urlformatter.NavStateUrlParam;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlParam;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.seo.NavStateCanonicalizer;
import com.endeca.soleng.urlformatter.seo.SeoUrlFormatter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CPSSeoUrlFormatter extends SeoUrlFormatter {

	private static final String NAVIGATION_STATE = "/atg/endeca/assembler/cartridge/manager/NavigationState";
	public static final String CARTRIDGE_HANDLER_PARAM_NAME = "_cartridge_handler_";

	private boolean mUseDimensionSeo = false;
	private CPSDimensionService mDimensionService;
	private SeoTools mSeoTools;
	private String[] mParamsToAdd = { "Nrpp", "Ns", "N" };
	public static String SEO_URL_PROP = "category.seoUrl";
	private CPSDimensionValueCacheTools mDimensionValueCacheTools;
	private String mAllBrandLink = "/all-brands";

	@Override
	public UrlState parseRequest(String pQueryString, String pPathInfo, String pCharacterEncoding)
			throws UrlFormatException {
		Site currentSite = SiteContextManager.getCurrentSite();

		UrlState urlState = super.parseRequest(pQueryString, pPathInfo, pCharacterEncoding);
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();

		if (getSeoTools().isSeoRequest(request)) {
			List<DimensionValueCacheObject> cacheEntries = null;
			if (!(pPathInfo == null)) {
				// it is not a CacheRefresh call
				cacheEntries = getSeoTools().getSeoItemCacheEntries(request);
			}
			/*
			 * Boolean cacheRefreshCall =
			 * (Boolean)request.getAttribute("_cacheRefreshCall_");
			 * if(!Boolean.TRUE.equals(cacheRefreshCall)) {
			 * request.setAttribute("_cacheRefreshCall_", Boolean.TRUE);
			 * cacheEntry = getSeoTools().getSeoItemCacheEntry(request); }
			 */
			if (cacheEntries != null && !cacheEntries.isEmpty()) {
				String n = urlState.getParam("N");
				StringBuilder sb = new StringBuilder();
				for (DimensionValueCacheObject entry : cacheEntries) {
					String dimValId = entry.getDimvalId();
					if (n == null || !n.contains(dimValId)) {
						sb.append(entry.getDimvalId()).append(" ");
					}
				}
				if (n != null) {
					sb.append(n).append(" ");
				}
				urlState.setParam("N", sb.toString().trim());
			}
		}

		return urlState;
	}

	protected void removeUnderlineParam(UrlState pUrlState) {
		String underlineParam = pUrlState.getParam("_");
		if (!StringUtils.isEmpty(underlineParam)) {
			pUrlState.removeParam("_");
		}
	}

	@Override
	public String formatUrl(UrlState pUrlState) throws UrlFormatException {
		removeUnderlineParam(pUrlState);

		UrlParam nUrlParam = pUrlState.getUrlParam("N");
		pUrlState.removeParam(CARTRIDGE_HANDLER_PARAM_NAME);
		String result = "";
		if (nUrlParam instanceof NavStateUrlParam) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			String url = request.getRequestURI();
			if (url.equals(getAllBrandLink())) {
				result = allBrandsSeoLinks((NavStateUrlParam) nUrlParam);
				if (StringUtils.isBlank(result)) {
					result = processNavStateUrlParam((NavStateUrlParam) nUrlParam, pUrlState);
				}
			} else {
				result = processNavStateUrlParam((NavStateUrlParam) nUrlParam, pUrlState);
			}
		} else {
			result = formatUrl(pUrlState, false);
		}
		return result;
	}

	private String allBrandsSeoLinks(NavStateUrlParam nUrlParam) {
		String seoUrl = "";
		DimLocationList dimLocList = nUrlParam.getDimLocationList();
		if (dimLocList != null && !dimLocList.isEmpty()) {
			DimVal dimVal = ((DimLocation) dimLocList.get(0)).getDimValue();
			if (dimVal != null) {
				String dimName = dimVal.getDimensionName();
				if (CPSConstants.DIM_PRODUCT_MANUFACTURER.equals(dimName)) {
					String seoName = SeoTools.seoName(dimVal.getName());
					DimensionValueCacheObject cacheObject = getDimensionValueCacheTools()
							.getCachedObjectForDimval(String.valueOf(dimVal.getId()));
					if (cacheObject != null) {
						seoUrl = ("/" + seoName);
					}
				}
			}
		}
		return seoUrl;
	}

	/**
	 * @param pUrlState
	 *            url state
	 * @return true '_cartridge_handler_' is in request params
	 */
	private boolean getFromRefinementMenuFlag(UrlState pUrlState) {
		UrlParam cartridgeHandlerUrlParam = pUrlState.getUrlParam(CARTRIDGE_HANDLER_PARAM_NAME);
		String cartridgeHandlerVal = null;
		if (cartridgeHandlerUrlParam != null) {
			cartridgeHandlerVal = cartridgeHandlerUrlParam.getValue();
		}
		return CARTRIDGE_HANDLER_PARAM_NAME.equals(cartridgeHandlerVal);
	}

	/**
	 * @param pNavState
	 *            navigation state
	 * @return current nav seo url
	 */
	private String getCurrentNavStateSeoUrl(NavigationState pNavState) {
		String currentNavStateSeoUrl = null;
		List<String> navFilters = pNavState.getFilterState().getNavigationFilters();
		if (navFilters != null && !navFilters.isEmpty()) {
			String firstDimValIdCurNavState = navFilters.get(0);
			DimLocation dimLocation = pNavState.getDimLocation(firstDimValIdCurNavState);
			if (dimLocation != null) {
				DimVal firstDimValCurNavState = dimLocation.getDimValue();
				if (firstDimValCurNavState != null) {
					currentNavStateSeoUrl = (String) firstDimValCurNavState.getProperties().get(SEO_URL_PROP);
				}
			}
		}
		return currentNavStateSeoUrl;
	}

	/**
	 * @param pUrlParam
	 *            navigation N param
	 * @param pUrlState
	 *            url state
	 * @return navigation url
	 * @throws UrlFormatException
	 *             if error occurs
	 */
	private String processNavStateUrlParam(NavStateUrlParam pUrlParam, UrlState pUrlState) throws UrlFormatException {
		String seoUrl = "";

		DimLocationList dimLocList = pUrlParam.getDimLocationList();
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		NavigationState navState = (NavigationState) request.resolveName(NAVIGATION_STATE);
		if (dimLocList != null && !dimLocList.isEmpty()) {
			DimVal firstDimVal = ((DimLocation) dimLocList.get(0)).getDimValue();
			if (firstDimVal != null && firstDimVal.getProperties() != null) {
				String seoUrlForDimVal = (String) firstDimVal.getProperties().get(SEO_URL_PROP);
				List<DimLocation> list = dimLocList;
				List<Integer> dimValsToRemove = new ArrayList<Integer>();
				if (StringUtils.isNotEmpty(seoUrlForDimVal)) {
					dimValsToRemove.add(0);
					String currentNavStateSeoUrl = getCurrentNavStateSeoUrl(navState);
					if ((!getFromRefinementMenuFlag(pUrlState) || currentNavStateSeoUrl != null) &&
						(getDimensionService() == null || (!request.getRequestURI().equals(getDimensionService().getDefaultDimensionContext())))) {
						
						if(list.size() > 1){
							for(int i = 1; i < list.size(); i++ ){
								DimensionValueCacheObject cacheObject = getDimensionValueCacheTools()
										.getCachedObjectForDimval(String.valueOf(list.get(i).getDimValue().getId()));
								if (cacheObject != null) {
									//seoUrlForDimVal += cacheObject.getUrl();
									seoUrlForDimVal += ("/" + SeoTools.seoName(list.get(i).getDimValue().getName()));
									dimValsToRemove.add(i);
								}
							}

						}
						seoUrl = seoUrlForDimVal;
					}
				} else {
					for (int i = 0; i < list.size(); i++) {
						DimLocation dimLoc = list.get(i);
						DimVal dimVal = dimLoc.getDimValue();
						if (dimVal != null) {
							String dimName = dimVal.getDimensionName();
							if (CPSConstants.DIM_PRODUCT_MANUFACTURER.equals(dimName)){
								seoUrl += "/" + SeoTools.seoName(dimVal.getName());
								dimValsToRemove.add(i);
							} else if (CPSConstants.DIM_PRODUCT_CATEGORY.equals(dimName)) {
								String categorySeoUrl = (String) dimVal.getProperties().get(SEO_URL_PROP);
								if (!StringUtils.isBlank(categorySeoUrl)) {
									seoUrl += categorySeoUrl;
								} else {
									seoUrl += "/" + SeoTools.seoName(dimVal.getName());
								}
								dimValsToRemove.add(i);
							}
						}
					}

				}
				MDimLocationList dimLocationList = MDimLocationList.mutableCopy(dimLocList);
				for (int i = 0; i < dimValsToRemove.size(); i++) {
					dimLocationList.remove(dimValsToRemove.get(i) - i);
				}
				pUrlState.setNavState(dimLocationList);
			}
		}
		String result = "";
		if (pUrlState != null) {
			if (seoUrl == null) {
				result = formatUrl(pUrlState, false);
			} else {
				result = seoUrl;
			}
		}

		Boolean isCacheRefresh = false;
		if (request.getAttribute("_cacheRefreshCall_") != null) {
			isCacheRefresh = (Boolean) request.getAttribute("_cacheRefreshCall_");
		}
		if (getParamsToAdd() != null && getParamsToAdd().length > 0 && !isCacheRefresh) {
			result = addParametersToResult(result, pUrlState);
			result = addSearchParametersToResult(result, navState, request);
		}

		return result;
	}

	private String addParametersToResult(String pResult, UrlState pNavState) throws UrlFormatException {
		if (getParamsToAdd() != null && getParamsToAdd().length > 0) {
			Collection<UrlParam> urlParameters = pNavState.getParameters();
			for (UrlParam urlParam : urlParameters) {
				for (String param : getParamsToAdd()) {
					if (param.equals(urlParam.getEncodedKey()) && StringUtils.isNotBlank(urlParam.getEncodedValue())) {
						if (pResult.contains("?")) {
							pResult += ("&" + urlParam.getEncodedKey() + "=" + urlParam.getEncodedValue());
						} else {
							pResult += ("?" + urlParam.getEncodedKey() + "=" + urlParam.getEncodedValue());
						}
					}
				}
			}
		}

		return pResult;
	}
	
	private String addSearchParametersToResult(String pResult, NavigationState pNavState, DynamoHttpServletRequest pRequest){
		List<SearchFilter> searchFilters = pNavState.getFilterState().getSearchFilters();
		if (searchFilters != null && !searchFilters.isEmpty()) {
			if (pResult.contains("?")) {
				pResult += "&Ntt=";
			} else {
				pResult += "?Ntt=";
			}
			for (SearchFilter filter : searchFilters) {
				pResult += (filter.getTerms() + "|");
			}
			pResult = pResult.substring(0, pResult.length() - 1);
		}
		if(pRequest != null && StringUtils.isNotBlank(pRequest.getRequestURI()) && pRequest.getRequestURI().startsWith("/search")){
			pResult = "/search" + pResult;
		}
		return pResult;
	}

	private String formatUrl(UrlState pUrlState, boolean forceCanonical) throws UrlFormatException {
		NavStateCanonicalizer navStateCanonicalizer = getNavStateCanonicalizer();
		if ((null != navStateCanonicalizer) && (((useNavStateCanonicalizer()) || forceCanonical))) {
			UrlParam urlParam = pUrlState.getUrlParam(getNavStateParamKey());
			if (urlParam instanceof NavStateUrlParam) {
				navStateCanonicalizer.canonicalize((NavStateUrlParam) urlParam);
			}
		}

		String pathStrings = formatPathKeywords(pUrlState);
		if (!StringUtils.isBlank(pathStrings)) {
			pathStrings = pathStrings.replaceAll("%2F", " ");
		}

		String pathParams = null;
		if (isUseDimensionSeo()) {
			pathParams = getSeoTools().getFormattedURLString(pUrlState, true, false);
		} else {
			pathParams = formatPathParams(pUrlState);
		}

		String queryString = formatQueryString(pUrlState);

		String pathSeparatorToken = getPathSeparatorToken();
		int lnPathStrings = pathStrings.length();
		int lnPathParams = pathParams.length();
		int lnQueryString = queryString.length();
		int lnPathSeparatorToken = pathSeparatorToken.length();

		StringBuilder buffer = new StringBuilder(lnPathStrings + lnPathParams + lnQueryString + lnPathSeparatorToken
				+ 1);
		if (0 < lnPathStrings) {
			buffer.append(pathStrings);
		}

		if (0 < lnPathParams) {
			buffer.append('/');
			buffer.append(pathSeparatorToken);
			buffer.append(pathParams);
		}

		if (0 < lnQueryString) {
			buffer.append('?');
			buffer.append(queryString);
		}

		return buffer.toString();
	}

	public CPSDimensionService getDimensionService() {
		return mDimensionService;
	}

	public void setDimensionService(CPSDimensionService dimensionService) {
		mDimensionService = dimensionService;
	}

	public boolean isUseDimensionSeo() {
		return mUseDimensionSeo;
	}

	public void setUseDimensionSeo(boolean pUseDimensionSeo) {
		this.mUseDimensionSeo = pUseDimensionSeo;
	}

	public SeoTools getSeoTools() {
		return mSeoTools;
	}

	public void setSeoTools(SeoTools pSeoTools) {
		mSeoTools = pSeoTools;
	}

	public String[] getParamsToAdd() {
		return mParamsToAdd;
	}

	public void setParamsToAdd(String[] pParamsToAdd) {
		this.mParamsToAdd = pParamsToAdd;
	}

	public CPSDimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	public void setDimensionValueCacheTools(CPSDimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	public String getAllBrandLink() {
		return mAllBrandLink;
	}

	public void setAllBrandLink(String pAllBrandLink) {
		mAllBrandLink = pAllBrandLink;
	}

}