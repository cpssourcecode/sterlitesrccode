package com.cps.endeca.assembler;

import atg.endeca.assembler.AssemblerTools;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import com.cps.util.CPSConstants;
import com.endeca.infront.assembler.AssemblerException;
import com.endeca.infront.assembler.ContentItem;
import vsg.endeca.EndecaConstants;

public class CPSAssemblerTools extends AssemblerTools {

    private static final String CONTENT_ITEM_ERROR = "@error";

    @Override
    public ContentItem invokeAssembler(ContentItem pContentItem) throws AssemblerException {
        final String operationName = "CPSAssemblerTools.invokeAssembler";

        PerformanceMonitor.startOperation(operationName);
        boolean success = true;
        try {
            final ContentItem responseContentItem = super.invokeAssembler(pContentItem);
            if(responseContentItem.containsKey(CONTENT_ITEM_ERROR)){
                setAssemblerFailedFlag();
            }

            return responseContentItem;
        } catch (AssemblerException e){
            setAssemblerFailedFlag();
            success = false;
            throw e;
        } finally {
            if (success){
                PerformanceMonitor.endOperation(operationName);
            } else {
                PerformanceMonitor.cancelOperation(operationName);
            }
        }
    }

    private void setAssemblerFailedFlag() {
        DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
        if(request != null){
            request.setAttribute(EndecaConstants.INVOKE_ASSEMBLER_FAILED_ATTR_NAME, true);
        }
    }
}
