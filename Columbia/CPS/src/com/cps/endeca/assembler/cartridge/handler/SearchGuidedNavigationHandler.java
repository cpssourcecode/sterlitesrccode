package com.cps.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Consumer;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.cps.commerce.endeca.cache.CPSDimensionValueCacheTools;
import com.cps.endeca.CategoryGuidedNavigationMenu;
import com.cps.util.CPSConstants;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SortOption;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RecordsMdexQuery;
import com.endeca.infront.navigation.request.RefinementMdexQuery;
import com.endeca.infront.navigation.request.support.NavigationRequest;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERec;
import com.endeca.navigation.ERecList;
import com.endeca.navigation.Navigation;

import atg.commerce.catalog.CatalogTools;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;
import vsg.constants.VSGConstants;
import vsg.constants.VSGRepositoriesConstants;

/**
 *
 * @author VSG
 *
 */
public class SearchGuidedNavigationHandler extends StoreRefinementMenuHandler {

    /** The catalog tools */
    private CatalogTools mCatalogTools;
    /** dimension value cache tools */
    private CPSDimensionValueCacheTools mDimensionValueCacheTools;
    /** The config mdex pairs */
    private List<Pair<RefinementMenuConfig, MdexRequest>> mConfigMdexPairs;

    private final String PRODUCT_ITEM_DESCRIPTOR = "product";
    private final String PROP_PRODUCT_REP_ID = "product.repositoryId";
    private final String PROP_PARENT_CATEGORIES = "parentCategories";

    private Repository mProductCatalog;

    /**
     * initialize mdex request without dimension id
     *
     * @param pCartridgeConfig
     *            - refinement menu config
     * @throws CartridgeHandlerException
     *             - CartridgeHandlerException
     */
    @Override
    public void preprocess(RefinementMenuConfig pCartridgeConfig) throws CartridgeHandlerException {
        String displayNameAlias = (String) pCartridgeConfig.get(DISPLAY_NAME_PROPERTY_ALIAS);
        if (displayNameAlias != null) {
            String sourceName = getLocaleAttributeAliasManager().getSourceNameForAttributeAlias(ServletUtil.getCurrentRequest(), displayNameAlias);

            if (sourceName != null) {
                pCartridgeConfig.put(DISPLAY_NAME_PROPERTY, sourceName);
            } else if (pCartridgeConfig.get(DISPLAY_NAME_PROPERTY_ALIAS) != null) {
                pCartridgeConfig.put(DISPLAY_NAME_PROPERTY, displayNameAlias);
            }
        }
        mConfigMdexPairs = buildRefinementMenuConfigs(pCartridgeConfig, getNavigationState());
    }

    /**
     * make call to endeca and iterate through all returned dimensions
     *
     * @param pCartridgeConfig
     *            - refinement menu config
     * @return RefinementMenu
     * @throws CartridgeHandlerException
     *             - CartridgeHandlerException
     */
    @Override
    public RefinementMenu process(RefinementMenuConfig pCartridgeConfig) throws CartridgeHandlerException {
        CategoryGuidedNavigationMenu guidedNavMenu = null;
        if (mConfigMdexPairs != null && !mConfigMdexPairs.isEmpty()) {
            guidedNavMenu = new CategoryGuidedNavigationMenu(pCartridgeConfig);
            List<RefinementMenu> refinementMenus = new ArrayList<RefinementMenu>();
            for (Pair<RefinementMenuConfig, MdexRequest> pair : mConfigMdexPairs) {
                ENEQueryResults results = executeMdexRequest(pair.getRight());
                getNavigationState().inform(results);
                RefinementMenu refinementMenu = buildRefinementMenus(pair.getLeft(), results);
                if (refinementMenu.getRefinements() != null && !refinementMenu.getRefinements().isEmpty()) {
                    refinementMenus.add(refinementMenu);
                }
            }
            guidedNavMenu.setRefinementMenus(refinementMenus);
        }
        return guidedNavMenu;
    }

    /**
     * iterate through all returned dimensions and build refinement for spicified on category level dimension names
     * 
     * @param pCartridgeConfig
     *            - refinement menu config
     * @param pResults
     *            - endeca response
     * @return RefinementMenu
     */
    private RefinementMenu buildRefinementMenus(RefinementMenuConfig pCartridgeConfig, ENEQueryResults pResults) {
        RefinementMenu refinementMenu = new RefinementMenu(pCartridgeConfig);
        Dimension dimension = pResults.getNavigation().getRefinementDimensions().getDimension(Long.parseLong(pCartridgeConfig.getDimensionId()));
        if (dimension == null) {
            buildImplicitRefinements(pResults, pCartridgeConfig, refinementMenu);
        } else {
            buildRegularRefinements(dimension, pCartridgeConfig, refinementMenu);
        }
        if (refinementMenu.getRefinements() != null) {
            ListIterator<Refinement> it = refinementMenu.getRefinements().listIterator();
            while (it.hasNext()) {
                Refinement ref = it.next();
                if (ref.getCount() == null || ref.getCount().equals(0)) {
                    it.remove();
                }
            }
        }
        return refinementMenu;
    }

    private List<Pair<RefinementMenuConfig, MdexRequest>> buildRefinementMenuConfigs(RefinementMenuConfig pCartridgeConfig, NavigationState pNavigationState)
                    throws CartridgeHandlerException {
        List<Pair<RefinementMenuConfig, MdexRequest>> pairs = new ArrayList<Pair<RefinementMenuConfig, MdexRequest>>();
        try {
            List<Pair<String, Long>> facets = getListOfDimensions(pNavigationState);
            if (facets != null && !facets.isEmpty()) {
                for (Pair<String, Long> facet : facets) {
                    RefinementMenuConfig config = new RefinementMenuConfig(pCartridgeConfig);
                    config.setDimensionId(String.valueOf(facet.getRight()));
                    config.put("dimensionName", facet.getLeft());
                    config.put(DISPLAY_NAME_PROPERTY, pCartridgeConfig.get(DISPLAY_NAME_PROPERTY));
                    RefinementMdexQuery refinement = new RefinementMdexQuery();
                    refinement.setDimensionId(config.getDimensionId());
                    if (getCpsGlobalProperties().getDimensionSortMap() != null && !getCpsGlobalProperties().getDimensionSortMap().isEmpty()) {
                        if (getCpsGlobalProperties().getDimensionSortMap().containsKey(facet.getLeft())) {
                            config.setSort(getCpsGlobalProperties().getDimensionSortMap().get(facet.getLeft()));
                        } else {
                            config.setSort(getCpsGlobalProperties().getDefaultDimensionSort());
                        }
                    }
                    updateSortConfig(config, refinement);
                    refinement.setNumRefinements(Integer.valueOf(config.getLimit()));
                    refinement.setWhyPrecedenceRuleFiredEnabled(config.isWhyPrecedenceRuleFired());
                    MdexRequest mdexRequest = createMdexRequest(getNavigationState().getFilterState(), refinement);
                    Pair<RefinementMenuConfig, MdexRequest> pair = new ImmutablePair<RefinementMenuConfig, MdexRequest>(config, mdexRequest);
                    pairs.add(pair);
                }
            }
        } catch (RepositoryException e) {
            throw new CartridgeHandlerException(e);
        } catch (Exception e) {
            throw new CartridgeHandlerException(e);
        }
        return pairs;
    }

    /**
     * get dimensions by dimension names from category
     *
     * @param pResults
     *            - response from endeca
     * @return List<Pair<String, Long>>
     * @throws Exception
     */
    private List<Pair<String, Long>> getListOfDimensions(NavigationState pNavigationState) throws Exception {
        List<Pair<String, Long>> dimensionList = new ArrayList<Pair<String, Long>>();
        Set<String> categories = getListOfCategories();

        Map<Long, Pair<String, Long>> dimList = new HashMap<>();
        for (String categoryId : categories) {
            RepositoryItem repositoryItem = getCatalogTools().findCategory(categoryId);
            if (repositoryItem != null) {
                List<String> categoryFacetNames = getFacetNames(repositoryItem);
                if (categoryFacetNames != null && !categoryFacetNames.isEmpty()) {
                    for (String facetName : categoryFacetNames) {
                        String facetNameKey = facetName.replaceAll(CPSConstants.FILTER_SPEC_SIMBOLS, VSGConstants.EMPTY).toUpperCase();
                        Long dimensionId = getDimensionValueCacheTools().getDimensionIdByDimensionName(facetNameKey);
                        if (dimensionId != null) {
                            Pair<String, Long> pair = new ImmutablePair<String, Long>(facetName, dimensionId);
                            dimList.put(dimensionId, pair);
                        }
                    }
                }
            }
        }
        dimensionList.addAll(dimList.values());
        return dimensionList;
    }

    @SuppressWarnings("unchecked")
    private Set<String> getListOfCategories() throws CartridgeHandlerException {
        NavigationRequest mRequest = new NavigationRequest(getMdexRequestBroker().getMdexResource(), getNavigationState().getFilterState(), false);

        RecordsMdexQuery recordsQuery = new RecordsMdexQuery();
        recordsQuery.setOffset(0);
        recordsQuery.setRecordsPerPage(999);
        recordsQuery.setSortOption(new SortOption());
        recordsQuery.setWhyMatchEnabled(false);
        recordsQuery.setWhyRankEnabled(false);

        List<String> names = new ArrayList<>();
        names.add(PROP_PRODUCT_REP_ID);
        recordsQuery.setFieldNames(names);
        mRequest.setRecordsQuery(recordsQuery);

        ENEQueryResults results1 = executeMdexRequest(mRequest);
        Navigation nav = results1.getNavigation();
        // Get record list
        ERecList records = nav.getERecs();
        final Set<String> categories = new TreeSet<>();
        records.forEach(new Consumer<ERec>() {
            @Override
            public void accept(ERec record) {
                String productId = (String) record.getProperties().get(PROP_PRODUCT_REP_ID);
                if (productId != null) {
                    try {
                        RepositoryItem product = getProductCatalog().getItem(productId, PRODUCT_ITEM_DESCRIPTOR);
                        if (product != null) {
                            Set<RepositoryItem> category = (Set<RepositoryItem>) product.getPropertyValue(PROP_PARENT_CATEGORIES);
                            if (category != null && category.size() > 0) {
                                categories.add(((RepositoryItem) category.toArray()[0]).getRepositoryId());
                            }
                        }
                    } catch (RepositoryException e) {
                    }
                }
            }
        });

        return categories;
    }

    /**
     * return first list of facet names from parent categories
     * 
     * @param pCategory
     *            - {@link RepositoryItem}
     * @return List<String>
     */
    @SuppressWarnings("unchecked")
    private List<String> getFacetNames(RepositoryItem pCategory) {
        List<String> categoryFacetNames = null;
        if (pCategory != null) {
            List<String> facets = (List<String>) pCategory.getPropertyValue(CPSConstants.CATEGORY_FACETS);
            if (facets != null && !facets.isEmpty()) {
                categoryFacetNames = facets;
            } else {
                Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) pCategory.getPropertyValue(VSGRepositoriesConstants.CATEGORY_FIXED_PARENT_CATS);
                if (parentCategories != null && !parentCategories.isEmpty()) {
                    for (RepositoryItem category : parentCategories) {
                        categoryFacetNames = getFacetNames(category);
                        if (categoryFacetNames != null) {
                            break;
                        }
                    }
                }
            }
        }
        return categoryFacetNames;
    }

    public CatalogTools getCatalogTools() {
        return mCatalogTools;
    }

    public void setCatalogTools(CatalogTools pCatalogTools) {
        mCatalogTools = pCatalogTools;
    }

    public CPSDimensionValueCacheTools getDimensionValueCacheTools() {
        return mDimensionValueCacheTools;
    }

    public void setDimensionValueCacheTools(CPSDimensionValueCacheTools pDimensionValueCacheTools) {
        mDimensionValueCacheTools = pDimensionValueCacheTools;
    }

    public Repository getProductCatalog() {
        return mProductCatalog;
    }

    public void setProductCatalog(Repository pProductCatalog) {
        mProductCatalog = pProductCatalog;
    }

}
