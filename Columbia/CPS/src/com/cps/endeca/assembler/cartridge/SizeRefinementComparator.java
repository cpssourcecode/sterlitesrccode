package com.cps.endeca.assembler.cartridge;

import java.util.Comparator;

import com.endeca.infront.cartridge.model.Refinement;

public class SizeRefinementComparator implements Comparator<Refinement> {

    @Override
    public int compare(Refinement r1, Refinement r2) {

        Double r1dim1 = 0.0;
        if (r1.getProperties().containsKey("dimval.prop.dim1")) {
            r1dim1 = Double.parseDouble(r1.getProperties().get("dimval.prop.dim1"));
        }

        Double r2dim1 = 0.0;
        if (r2.getProperties().containsKey("dimval.prop.dim1")) {
            r2dim1 = Double.parseDouble(r2.getProperties().get("dimval.prop.dim1"));
        }

        Double r1dim2 = 0.0;
        if (r1.getProperties().containsKey("dimval.prop.dim2")) {
            r1dim2 = Double.parseDouble(r1.getProperties().get("dimval.prop.dim2"));
        }

        Double r2dim2 = 0.0;
        if (r2.getProperties().containsKey("dimval.prop.dim2")) {
            r2dim2 = Double.parseDouble(r2.getProperties().get("dimval.prop.dim2"));
        }

        Double r1dim3 = 0.0;
        if (r1.getProperties().containsKey("dimval.prop.dim3")) {
            r1dim3 = Double.parseDouble(r1.getProperties().get("dimval.prop.dim3"));
        }

        Double r2dim3 = 0.0;
        if (r2.getProperties().containsKey("dimval.prop.dim3")) {
            r2dim3 = Double.parseDouble(r2.getProperties().get("dimval.prop.dim3"));
        }

        Double r1dim4 = 0.0;
        if (r1.getProperties().containsKey("dimval.prop.dim4")) {
            r1dim4 = Double.parseDouble(r1.getProperties().get("dimval.prop.dim4"));
        }

        Double r2dim4 = 0.0;
        if (r2.getProperties().containsKey("dimval.prop.dim4")) {
            r2dim4 = Double.parseDouble(r2.getProperties().get("dimval.prop.dim4"));
        }

        int c = r1dim1.compareTo(r2dim1);
        if (c != 0) {
            return c;
        }
        c = r1dim2.compareTo(r2dim2);
        if (c != 0) {
            return c;
        }
        c = r1dim3.compareTo(r2dim3);
        if (c != 0) {
            return c;
        }
        return r1dim4.compareTo(r2dim4);
    }
}