package com.cps.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.List;

import com.cps.endeca.infront.assembler.SecondarySearchResults;
import vsg.util.VSGTagUtils;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.support.ActionBuilder;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;

/**
 * The Class SecondarySearch.
 */
public class SecondarySearch extends NavigationCartridgeHandler<ContentItem, SecondarySearchResults> {

	/**
	 * Search key
	 */
	private String mSearchKey = "All";

	/**
	 * Search term
	 */
	private String mSearchTerm = "{search-term}";

	@Override
	protected ContentItem wrapConfig(ContentItem pContentItem) {
		SecondarySearchResults contentItem = new SecondarySearchResults(pContentItem);
		return contentItem;
	}

	@Override
	public void preprocess(ContentItem pCartridgeConfig) throws CartridgeHandlerException {
		super.preprocess(pCartridgeConfig);
	}

	@Override
	public SecondarySearchResults process(ContentItem pCartridgeConfig) {
		SecondarySearchResults result = (SecondarySearchResults) pCartridgeConfig;
		result.setNavigationState(getNavgationStateTemplate());
		return result;
	}

	/**
	 * Get navigation state template
	 * 
	 * @return String nav state
	 */
	protected String getNavgationStateTemplate() {
		NavigationState navState = getNavigationState();
		List<SearchFilter> searchFilters = getNavigationState().getFilterState().getSearchFilters();
		List<SearchFilter> searchFiltersNew = new ArrayList<SearchFilter>();
		if (null != searchFilters) {
			SearchFilter sf = new SearchFilter(getSearchKey(), getSearchTerm());
			searchFiltersNew.add(sf);
			searchFiltersNew.addAll(searchFilters);
			navState = navState.updateSearchFilters(searchFiltersNew);
		}
		NavigationAction navAction = new NavigationAction(navState.toString());
		ActionBuilder.populateNavigationPathDefaults(getActionPathProvider(), navAction, getSiteState());
		return VSGTagUtils.getUrlForAction(navAction);
	}

	public String getSearchKey() {
		return mSearchKey;
	}

	public void setSearchKey(String mSearchKey) {
		this.mSearchKey = mSearchKey;
	}

	public String getSearchTerm() {
		return mSearchTerm;
	}

	public void setSearchTerm(String pSearchTerm) {
		this.mSearchTerm = pSearchTerm;
	}

}
