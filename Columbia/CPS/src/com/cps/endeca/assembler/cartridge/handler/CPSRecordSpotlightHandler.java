package com.cps.endeca.assembler.cartridge.handler;

import vsg.endeca.EndecaConstants;

import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.RecordSpotlight;
import com.endeca.infront.cartridge.RecordSpotlightConfig;
import com.endeca.infront.cartridge.RecordSpotlightHandler;
import com.endeca.infront.cartridge.RecordSpotlightSelection;
import com.endeca.infront.navigation.NavigationException;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.FilterState;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RecordsMdexQuery;
import com.endeca.navigation.ENEQueryResults;

/**
 * RecordSpotlightHandler extension. Added the following functionality: setting
 * a rollup key to the filter state of the mdex request in order to make a
 * request with a featured records.
 *
 * @author Pavel P.
 */
public class CPSRecordSpotlightHandler extends RecordSpotlightHandler {

	/** The mdex request. */
	private MdexRequest mMdexRequest;

	/** The dim val info request. */
	private MdexRequest mDimValInfoRequest;

	/**
	 * Instantiates a new VSG record spotlight handler.
	 */
	public CPSRecordSpotlightHandler() {
		setDimValInfoRequest(null);
	}

	@Override
	public void preprocess(RecordSpotlightConfig cartridgeConfig) throws CartridgeHandlerException {
		setMdexRequest(createMdexRequest(getNavigationState().getFilterState(), new BreadcrumbsMdexQuery()));

		RecordSpotlightSelection recordSelection = cartridgeConfig.getRecordSelection();

		FilterState configFilterState = recordSelection.getFilterState();

		if ((configFilterState == null) || (configFilterState.getNavigationFilters() == null)
				|| (!(recordSelection.isAugment()))) {
			return;
		}

		setDimValInfoRequest(createMdexRequest(configFilterState, new BreadcrumbsMdexQuery()));
	}

	@Override
	public RecordSpotlight process(RecordSpotlightConfig cartridgeConfig) throws CartridgeHandlerException {
		RecordSpotlightSelection recordSelection = cartridgeConfig.getRecordSelection();

		if (recordSelection.isEmpty()) {
			return null;
		}

		ENEQueryResults results = executeMdexRequest(getMdexRequest());
		NavigationState navigationState = getNavigationState();
		navigationState.inform(results);

		executeDimValInfoRequest(navigationState, recordSelection);

		NavigationState correctedNavigationState = correctFilterState(navigationState, cartridgeConfig);
		try {
			MdexRequest spotlightRequest = getMdexRequestBroker().createMdexRequest(
					correctedNavigationState.getFilterState(), buildMdexQuery(navigationState, cartridgeConfig));

			ENEQueryResults spotlightResults = spotlightRequest.execute();
			correctedNavigationState.inform(spotlightResults);
			return processSpotlightMdexResponse(correctedNavigationState, cartridgeConfig, spotlightResults);
		} catch (NavigationException e) {
			throw new CartridgeHandlerException(e);
		}
	}

	/**
	 * Execute dim val info request.
	 *
	 * @param pNavigationState
	 *            the navigation state
	 * @param pRecordSelection
	 *            the record selection
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception
	 */
	protected void executeDimValInfoRequest(NavigationState pNavigationState, RecordSpotlightSelection pRecordSelection)
			throws CartridgeHandlerException {
		MdexRequest dimValInfoRequest = getDimValInfoRequest();
		if (dimValInfoRequest != null) {
			FilterState filterState = pRecordSelection.getFilterState();
			for (String dval : filterState.getNavigationFilters()) {
				if (pNavigationState.getDimLocation(dval) == null) {
					pNavigationState.inform(executeMdexRequest(dimValInfoRequest, false));
					break;
				}
			}
		}
	}

	/**
	 * Correct filter state. Setting a proper rollup key.
	 *
	 * @param pNavigationState
	 *            the navigation state
	 * @param pCartridgeConfig
	 *            the cartridge config
	 * @return the navigation state
	 */
	protected NavigationState correctFilterState(NavigationState pNavigationState,
			RecordSpotlightConfig pCartridgeConfig) {
		NavigationState spotlightNavigationState = createSpotlightNavigationState(pNavigationState, pCartridgeConfig);

		FilterState correctedFilterState = spotlightNavigationState.getFilterState();
		correctedFilterState.setRollupKey(EndecaConstants.PRODUCT_REPOSITORY_ID_ROLLUP_KEY);
		spotlightNavigationState = spotlightNavigationState.updateFilterState(correctedFilterState);

		return spotlightNavigationState;
	}

	/**
	 * Builds the mdex query.
	 *
	 * @param navigationState
	 *            the navigation state
	 * @param config
	 *            the config
	 * @return the records mdex query
	 * @throws CartridgeHandlerException
	 *             the cartridge handler exception
	 */
	private RecordsMdexQuery buildMdexQuery(NavigationState navigationState, RecordSpotlightConfig config)
			throws CartridgeHandlerException {
		RecordsMdexQuery recordsQuery = new RecordsMdexQuery();

		recordsQuery.setFieldNames(combineFieldNames(navigationState, config));

		RecordSpotlightSelection selection = config.getRecordSelection();

		int maxNumRecords = config.getMaxNumRecords();
		if ((selection.getRecordLimit() == 0) || (selection.getRecordLimit() > maxNumRecords)) {
			selection.setRecordLimit(maxNumRecords);
		}

		if (selection.getSortOption() != null) {
			selection.getSortOption().setDefault(false);
			recordsQuery.setSortOption(selection.getSortOption());
		}

		recordsQuery.setRecordsPerPage(selection.getRecordLimit());

		return recordsQuery;
	}

	public MdexRequest getMdexRequest() {
		return mMdexRequest;
	}

	public void setMdexRequest(MdexRequest pMdexRequest) {
		mMdexRequest = pMdexRequest;
	}

	public MdexRequest getDimValInfoRequest() {
		return mDimValInfoRequest;
	}

	public void setDimValInfoRequest(MdexRequest pDimValInfoRequest) {
		mDimValInfoRequest = pDimValInfoRequest;
	}
}
