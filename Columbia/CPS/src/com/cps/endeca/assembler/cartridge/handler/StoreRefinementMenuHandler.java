/*<ORACLECOPYRIGHT>
 * Copyright (C) 1994-2012 Oracle and/or its affiliates. All rights reserved.
 * Oracle and Java are registered trademarks of Oracle and/or its affiliates.
 * Other names may be trademarks of their respective owners.
 * UNIX is a registered trademark of The Open Group.
 *
 * This software and related documentation are provided under a license agreement
 * containing restrictions on use and disclosure and are protected by intellectual property laws.
 * Except as expressly permitted in your license agreement or allowed by law, you may not use, copy,
 * reproduce, translate, broadcast, modify, license, transmit, distribute, exhibit, perform, publish,
 * or display any part, in any form, or by any means. Reverse engineering, disassembly,
 * or decompilation of this software, unless required by law for interoperability, is prohibited.
 *
 * The information contained herein is subject to change without notice and is not warranted to be error-free.
 * If you find any errors, please report them to us in writing.
 *
 * U.S. GOVERNMENT RIGHTS Programs, software, databases, and related documentation and technical data delivered to U.S.
 * Government customers are "commercial computer software" or "commercial technical data" pursuant to the applicable
 * Federal Acquisition Regulation and agency-specific supplemental regulations.
 * As such, the use, duplication, disclosure, modification, and adaptation shall be subject to the restrictions and
 * license terms set forth in the applicable Government contract, and, to the extent applicable by the terms of the
 * Government contract, the additional rights set forth in FAR 52.227-19, Commercial Computer Software License
 * (December 2007). Oracle America, Inc., 500 Oracle Parkway, Redwood City, CA 94065.
 *
 * This software or hardware is developed for general use in a variety of information management applications.
 * It is not developed or intended for use in any inherently dangerous applications, including applications that
 * may create a risk of personal injury. If you use this software or hardware in dangerous applications,
 * then you shall be responsible to take all appropriate fail-safe, backup, redundancy,
 * and other measures to ensure its safe use. Oracle Corporation and its affiliates disclaim any liability for any
 * damages caused by use of this software or hardware in dangerous applications.
 *
 * This software or hardware and documentation may provide access to or information on content,
 * products, and services from third parties. Oracle Corporation and its affiliates are not responsible for and
 * expressly disclaim all warranties of any kind with respect to third-party content, products, and services.
 * Oracle Corporation and its affiliates will not be responsible for any loss, costs,
 * or damages incurred due to your access to or use of third-party content, products, or services.
 </ORACLECOPYRIGHT>*/
package com.cps.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.cps.endeca.CPSEndecaUtils;
import com.cps.endeca.assembler.cartridge.SizeRefinementComparator;
import com.cps.util.CPSGlobalProperties;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.RefinementMenuConfig;
import com.endeca.infront.cartridge.RefinementMenuConfig.RefinementsShown;
import com.endeca.infront.cartridge.RefinementMenuHandler;
import com.endeca.infront.cartridge.model.Refinement;
import com.endeca.infront.cartridge.support.RefinementBuilder;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.infront.navigation.request.RefinementMdexQuery;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.ENEQueryResults;

import atg.core.util.StringUtils;
import atg.search.record.alias.AttributeAliasManager;
import atg.servlet.ServletUtil;
import vsg.endeca.EndecaConstants;

/**
 * Extends RefinementMenuHandler to override preprocess method to use CategoryToDimensionAttributeAliasManager to get the source name for attribute alias.
 * Process method is overridden to update unlocalized category names to localized ones.
 *
 * @author Yekaterina Kostenevich
 * @version $Id:
 *          //hosting-blueprint/B2CBlueprint/version/10.1.2/Endeca/Assembler/src/atg/projects/store/assembler/cartridge/handler/StoreRefinementMenuHandler.java#6
 *          $$Change: 721793 $
 * @updated $DateTime: 2012/09/13 04:30:49 $$Author: ykostene $
 */
public class StoreRefinementMenuHandler extends RefinementMenuHandler implements EndecaConstants {

    private static final String REFINEMENT_NON_ALPHA_FIRST_SYMBOL = "#";
    /** displayNameProperty constant */
    protected static final String DISPLAY_NAME_PROPERTY = "displayNameProperty";
    /** displayNamePropertyAlias constant */
    protected static final String DISPLAY_NAME_PROPERTY_ALIAS = "displayNamePropertyAlias";
    /** mdex request */
    private MdexRequest mMdexRequest;
    /** clear navigations state */
    private NavigationState mClearNavigationState;
    /** LocaleAttributeAliasManager property */
    private AttributeAliasManager mLocaleAttributeAliasManager;

    private String mWebCategoryEmptyCategory = "ZZZ";

    private CPSGlobalProperties cpsGlobalProperties;

    /**
     * Create a new StoreRefinementMenuConfig using the passed in ContentItem
     */
    @Override
    protected RefinementMenuConfig wrapConfig(ContentItem pContentItem) {
        return new RefinementMenuConfig(pContentItem);
    }

    /**
     * Determine the source property name for aliased display name property before calling super.preprocess method
     */
    @Override
    public void preprocess(RefinementMenuConfig pCartridgeConfig) throws CartridgeHandlerException {
        // If we have an alias try to find the source property
        String displayNameAlias = (String) pCartridgeConfig.get(DISPLAY_NAME_PROPERTY_ALIAS);
        if (displayNameAlias != null) {
            String sourceName = getLocaleAttributeAliasManager().getSourceNameForAttributeAlias(ServletUtil.getCurrentRequest(), displayNameAlias);

            if (sourceName != null) {
                pCartridgeConfig.put(DISPLAY_NAME_PROPERTY, sourceName);
            } else if ((String) pCartridgeConfig.get(DISPLAY_NAME_PROPERTY_ALIAS) != null) {
                pCartridgeConfig.put(DISPLAY_NAME_PROPERTY, displayNameAlias);
            }
        }
        mClearNavigationState = getNavigationState().clearFilterState();
        validate(pCartridgeConfig);
        RefinementMdexQuery refinement = new RefinementMdexQuery();
        refinement.setDimensionId(pCartridgeConfig.getDimensionId());
        updateSortConfig(pCartridgeConfig, refinement);
        refinement.setNumRefinements(Integer.valueOf(pCartridgeConfig.getLimit()));
        refinement.setBoostedDvals(pCartridgeConfig.getBoostRefinements());
        refinement.setBuriedDvals(pCartridgeConfig.getBuryRefinements());
        refinement.setWhyPrecedenceRuleFiredEnabled(pCartridgeConfig.isWhyPrecedenceRuleFired());
        this.mMdexRequest = createMdexRequest(getNavigationState().getFilterState(), refinement);

    }

    /**
     * Creates a new RefinementMenu with localized category labels
     */
    @Override
    public RefinementMenu process(RefinementMenuConfig pCartridgeConfig) throws CartridgeHandlerException {

        ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
        getNavigationState().inform(results);
        try {

            if (results.getNavigation() != null) {

                Dimension dimension = results.getNavigation().getRefinementDimensions().getDimension(Long.parseLong(pCartridgeConfig.getDimensionId()));
                RefinementMenu outputModel = new RefinementMenu(pCartridgeConfig);
                if (dimension == null) {
                    buildImplicitRefinements(results, pCartridgeConfig, outputModel);
                } else {
                    buildRegularRefinements(dimension, pCartridgeConfig, outputModel);
                }
                outputModel.put("sort", pCartridgeConfig.getSort().toUpperCase());
                return outputModel;
            }
        } catch (NumberFormatException e) {
            throw new CartridgeHandlerException(e);
        }
        return null;
    }

    /**
     * build implicit refinements
     *
     * @param pResults
     *            - query results
     * @param pCartridgeConfig
     *            - cartridge config
     * @param pOutputModel
     *            - output model
     */
    protected void buildImplicitRefinements(ENEQueryResults pResults, RefinementMenuConfig pCartridgeConfig, RefinementMenu pOutputModel) {
        List<Refinement> refinements = new ArrayList<Refinement>();
        Dimension dimension = pResults.getNavigation().getCompleteDimensions().getDimension(Long.parseLong(pCartridgeConfig.getDimensionId()));
        if (dimension != null && dimension.getImplicitLocations() != null) {
            for (final DimLocation dimLocation : (List<DimLocation>) dimension.getImplicitLocations()) {
                final DimVal dimVal = dimLocation.getDimValue();
                // mClearNavigationState.inform(dimLocation);
                getNavigationState().inform(dimLocation);
                final Refinement ref = RefinementBuilder.createRefinement(getActionPathProvider(), dimVal, getNavigationState(), getSiteState());
                changeUrlToLower(ref);
                Map<String, String> propertyMap = ref.getProperties();
                String categoryId = propertyMap.get(INDEX_CATEGORY_REPOSITORY_ID_PROPERTY_NAME);
                if (!getWebCategoryEmptyCategory().equalsIgnoreCase(categoryId)) {
                    String currentLabel = ref.getLabel();
                    String localizedLabel = findLocalizedLabel(ref.getProperties(), currentLabel, (String) pCartridgeConfig.get(DISPLAY_NAME_PROPERTY));
                    ref.setLabel(localizedLabel);
                    ref.setCount((int) CPSEndecaUtils.getTotalNumRecs(pResults));
                    refinements.add(ref);
                }
            }
            pOutputModel.setRefinements(refinements);
            pOutputModel.setMultiSelect(dimension.getRoot().isMultiSelectAnd() || dimension.getRoot().isMultiSelectOr());
            // System.out.println("Dimension Implicit :: " + dimension.getName());
        }
    }

    protected void buildRegularRefinements(Dimension pDimension, RefinementMenuConfig pCartridgeConfig, RefinementMenu pOutputModel) {
        List<Refinement> refinements = new ArrayList<Refinement>();
        String displayName = pOutputModel.getDimensionName();
        RefinementBuilder.populateRefinementMenuProperties(pOutputModel, pCartridgeConfig, pDimension, REFINEMENT_MENU_DYNAMIC_CONFIG_PARAMETER,
                        REFINEMENT_MENU_SHOW_MORE_PARAMETER, getNavigationState(), getSiteState(), getActionPathProvider());
        if (StringUtils.isNotBlank(displayName)) {
            pOutputModel.setDisplayName(displayName);
        }

        DimValList list = pDimension.getRefinements();
        // System.out.println("Dimension Regular :: " + pDimension.getName());

        for (int j = 0; j < list.size(); j++) {
            Refinement ref = RefinementBuilder.createRefinement(getActionPathProvider(), list.getDimValue(j), getNavigationState(), getSiteState());
            changeUrlToLower(ref);
            Map<String, String> propertyMap = ref.getProperties();
            String categoryId = propertyMap.get(INDEX_CATEGORY_REPOSITORY_ID_PROPERTY_NAME);
            if (!getWebCategoryEmptyCategory().equalsIgnoreCase(categoryId)) {
                propertyMap.put(REFINEMENT_DIMVAL_ID_PROP_NAME, "" + list.getDimValue(j).getId());
                String currentLabel = ref.getLabel();
                String localizedLabel = findLocalizedLabel(ref.getProperties(), currentLabel, (String) pCartridgeConfig.get(DISPLAY_NAME_PROPERTY));
                ref.setLabel(localizedLabel);
                refinements.add(ref);
            }
        }

        boolean customSortEnabled = Boolean.valueOf(getCpsGlobalProperties().getCustomSortingEnabledMap().get(pDimension.getName()));
        // System.out.println(pDimension.getName() + " <==> " + customSortEnabled);
        if (customSortEnabled) {
            Comparator<Refinement> refinementComparator = new SizeRefinementComparator();
            Collections.sort(refinements, refinementComparator);

        }
        pOutputModel.setRefinements(refinements);

        Object number = pCartridgeConfig.get(REFINEMENT_MENU_NUMBER_TO_DISPLAY);
        if (number != null) {
            Integer refNumberToDisplay = 10;
            if (number instanceof String && !StringUtils.isBlank((String) number)) {
                refNumberToDisplay = Integer.parseInt((String) number);
            } else if (number instanceof Integer) {
                refNumberToDisplay = (Integer) number;
            }
            pOutputModel.put(REFINEMENT_MENU_SHOW_MORE_LINK, pCartridgeConfig.get(REFINEMENT_MENU_SHOW_MORE_LINK));
            pOutputModel.put(REFINEMENT_MENU_NUMBER_TO_DISPLAY, refNumberToDisplay);
        }

        Object isSplitByFirstLetter = pCartridgeConfig.get(REFINEMENT_MENU_SPLIT_BY_FIRST_LETTER);
        if (isSplitByFirstLetter instanceof Boolean && (Boolean) isSplitByFirstLetter) {
            if (refinements != null && !refinements.isEmpty()) {
                pOutputModel.put(REFINEMENT_MENU_SPLITTED_REFINEMENTS, getSplittedRefinements(refinements));
            }
        }
    }

    private void changeUrlToLower(Refinement ref) {
        String url = ref.getNavigationState();
        String result = "";
        if (StringUtils.isNotBlank(url)) {
            if (url.contains("?")) {
                result = url.substring(0, url.indexOf("?")).toLowerCase() + url.substring(url.indexOf("?"));
            } else {
                result = url.toLowerCase();
            }
        }
        if (!result.equals("")) {
            ref.setNavigationState(result);
        }
    }

    private Map<String, List<Refinement>> getSplittedRefinements(List<Refinement> refinements) {
        Map<String, List<Refinement>> splittedRefinements = new TreeMap<>();
        for (Refinement refinement : refinements) {
            if (StringUtils.isNotBlank(refinement.getLabel())) {
                String firstSymbol = refinement.getLabel().trim().substring(0, 1);
                if (!org.apache.commons.lang3.StringUtils.isAlpha(firstSymbol)) {
                    firstSymbol = REFINEMENT_NON_ALPHA_FIRST_SYMBOL;
                } else {
                    firstSymbol = firstSymbol.toUpperCase();
                }
                List<Refinement> letterRefinements = null;
                if (splittedRefinements.containsKey(firstSymbol)) {
                    letterRefinements = splittedRefinements.get(firstSymbol);
                    letterRefinements.add(refinement);
                    letterRefinements.sort(new RefinementLabelComparator());
                } else {
                    letterRefinements = new ArrayList<>();
                    letterRefinements.add(refinement);
                    splittedRefinements.put(firstSymbol, letterRefinements);
                }
            }
        }
        return splittedRefinements;
    }

    /**
     * Looks for localized label for refinements
     *
     * @param pProperties
     *            the properties of current refinement
     * @param pLabel
     *            the non-localized label of refinement
     * @param pDisplayNameProperty
     *            the localized display name property
     * @return
     */
    public String findLocalizedLabel(Map<String, String> pProperties, String pLabel, String pDisplayNameProperty) {
        String localizedLabel = pLabel;
        String localizedName = pProperties.get(pDisplayNameProperty);
        if (!StringUtils.isEmpty(localizedName)) {
            localizedLabel = localizedName;
        }
        return localizedLabel;
    }

    /**
     * update sort config
     *
     * @param cartridgeConfig
     *            - the cartridge config
     * @param dimConfig
     *            - dimension config
     * @throws CartridgeHandlerException
     *             - the cartridge exception
     */
    protected static void updateSortConfig(RefinementMenuConfig cartridgeConfig, RefinementMdexQuery dimConfig) throws CartridgeHandlerException {
        RefinementMdexQuery.RefinementSortType sort = RefinementMdexQuery.RefinementSortType.DISABLED;
        if (cartridgeConfig.getSort() != null) {
            String configuredSort = cartridgeConfig.getSort().toUpperCase();
            if ("DYNRANK".equalsIgnoreCase(configuredSort)) {
                sort = RefinementMdexQuery.RefinementSortType.DYNAMIC;
            } else if ("STATIC".equalsIgnoreCase(configuredSort)) {
                sort = RefinementMdexQuery.RefinementSortType.STATIC;
            } else if (!"DEFAULT".equalsIgnoreCase(configuredSort)) {
                throw new CartridgeHandlerException("sort configuration: " + configuredSort + " is invalid.");
            }
        }
        dimConfig.setSortType(sort);
    }

    /**
     * validate cartridge config
     *
     * @param pCartridgeConfig
     *            - the cartridge config
     * @throws CartridgeHandlerException
     *             - cartridge exception
     */
    void validate(RefinementMenuConfig pCartridgeConfig) throws CartridgeHandlerException {
        if (pCartridgeConfig.getDimensionId() == null || pCartridgeConfig.getDimensionId().equals("")) {
            throw new CartridgeHandlerException("property: dimensionId should not be null or an emtpy string");
        }
        try {
            RefinementsShown.getValue(pCartridgeConfig.getRefinementsShown());
        } catch (IllegalArgumentException e) {
            throw new CartridgeHandlerException("\"all\", \"some\", and \"none\" are the only valid values for the property: refinementsShown");
        }
    }

    public class RefinementLabelComparator implements Comparator<Refinement> {
        @Override
        public int compare(Refinement r1, Refinement r2) {
            return r1.getLabel().compareTo(r2.getLabel());
        }
    }

    /**
     * @return The Manager component used to determine the source property for the display name aliased property.
     */
    public AttributeAliasManager getLocaleAttributeAliasManager() {
        return mLocaleAttributeAliasManager;
    }

    /**
     * @param pLocaledAttributeAliasManager
     *            the mLocaleAttributeAliasResolver to set
     */
    public void setLocaleAttributeAliasManager(AttributeAliasManager pLocaleAttributeAliasManager) {
        this.mLocaleAttributeAliasManager = pLocaleAttributeAliasManager;
    }

    public String getWebCategoryEmptyCategory() {
        return mWebCategoryEmptyCategory;
    }

    public void setWebCategoryEmptyCategory(String pWebCategoryEmptyCategory) {
        mWebCategoryEmptyCategory = pWebCategoryEmptyCategory;
    }

    /**
     * @return the cpsGlobalProperties
     */
    public CPSGlobalProperties getCpsGlobalProperties() {
        return cpsGlobalProperties;
    }

    /**
     * @param cpsGlobalProperties
     *            the cpsGlobalProperties to set
     */
    public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
        this.cpsGlobalProperties = cpsGlobalProperties;
    }

}
