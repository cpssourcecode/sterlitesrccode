package com.cps.endeca.assembler.cartridge.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import atg.commerce.endeca.assembler.cartridge.handler.DimensionValueCacheRefreshHandler;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.nucleus.logging.VariableArgumentApplicationLogging;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;

import com.cps.commerce.endeca.cache.CPSDimensionValueCache;
import com.cps.commerce.endeca.cache.CPSDimensionValueCacheTools;
import com.cps.commerce.endeca.cache.ReverseDimensionValueCache;
import com.cps.seo.SeoTools;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.cartridge.DimensionSearchResults;
import com.endeca.infront.cartridge.DimensionSearchResultsConfig;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.model.SearchFilter;
import com.endeca.infront.navigation.request.DimensionSearchMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimLocationList;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.DimValList;
import com.endeca.navigation.DimensionSearchResultGroup;
import com.endeca.navigation.DimensionSearchResultGroupList;
import com.endeca.navigation.ENEQueryResults;


public class CPSSeoDimensionValueCacheRefreshHandler extends DimensionValueCacheRefreshHandler {

	ApplicationLoggingImpl mLogging = new ApplicationLoggingImpl(this.getClass().getName());

	public static final String CATEGORY = "product.category";
	public static final String CATEGORY_SITEID = "category.siteId";
	public static final String SEO_URL_PROP = "category.seoUrl";

	private boolean mUseDimensionSeo = false;
	private ReverseDimensionValueCache mReverseDimensionValueCache = null;
	private SeoTools mSeoTools = null;
	private MdexRequest mMdexRequest;
	private List<String> mDimensionNamesForCache;
	
	public void preprocess(DimensionSearchResultsConfig cartridgeConfig) throws CartridgeHandlerException {
		NavigationState navigationState = getNavigationState();
		SearchFilter searchFilter = getDimensionSearchFilter(navigationState);
		if ((cartridgeConfig.isEnabled()) && (searchFilter != null)) {
			NavigationState dimNavState = createDimensionSearchNavigationState(navigationState);
			this.mMdexRequest = createMdexRequest(dimNavState.getFilterState(),
					buildMdexQuery(cartridgeConfig, searchFilter));
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DimensionSearchResults process(DimensionSearchResultsConfig pConfig) throws CartridgeHandlerException {
		DimensionSearchResults allDimensions = new DimensionSearchResults(pConfig);
		VariableArgumentApplicationLogging logger = AssemblerTools.getApplicationLogging();
		CPSDimensionValueCache newCache = (CPSDimensionValueCache) getDimensionValueCacheTools().createEmptyCache();
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		request.setAttribute("_cacheRefreshCall_", Boolean.TRUE);

		if (mMdexRequest != null) {
			ENEQueryResults results = executeMdexRequest(mMdexRequest);
			NavigationState navigationState = getNavigationState();
			navigationState.inform(results);
			navigationState = navigationState.clearRecordFilters().clearSearchFilters().clearNavigationFilters()
					.clearParameters();
			createDimensionNameDimensionIdCachMap(results);
			for (Object dimSearchResultGroupObj : results.getDimensionSearch().getResults()) {
				DimensionSearchResultGroup dimSearchResultGroup = (DimensionSearchResultGroup) dimSearchResultGroupObj;
				DimVal dimensionRoot = (DimVal) dimSearchResultGroup.getRoots().get(0);
				
				// if
				// (dimensionRoot.getProperties().containsKey(getRepositoryIdProperty()))
				// {
				Set<String> seoSet = getSeoTools().getSeoDimensionsMap().keySet();
				if (seoSet != null && seoSet.contains(dimensionRoot.getDimensionName())) {
					for (Object dimLocationListObj : dimSearchResultGroup) {
						DimLocation dimLocation = (DimLocation) (((DimLocationList) dimLocationListObj).get(0));
						navigationState.inform(dimLocation);
						DimVal dimval = dimLocation.getDimValue();
						
						if (CATEGORY.equals(dimensionRoot.getDimensionName())) {
							String siteId = (String) dimval.getProperties().get(CATEGORY_SITEID);
							getReverseDimensionValueCache().putCategoryCacheValueIntoDimension(dimLocation,siteId);
						} else {
							getReverseDimensionValueCache().putCacheValueIntoDimension(dimLocation);
						}
					}
				}
				
				if (getDimensionNamesForCache() != null && getDimensionNamesForCache().contains(dimensionRoot.getDimensionName())) {
					for (Object dimLocationListObj : dimSearchResultGroup) {
						DimLocation dimLocation = (DimLocation) (((DimLocationList) dimLocationListObj).get(0));
						navigationState.inform(dimLocation);
						DimVal dimval = dimLocation.getDimValue();
						String repositoryId = (String) dimval.getProperties().get(getRepositoryIdProperty());
						
						if (StringUtils.isBlank(repositoryId)) {
							repositoryId = SeoTools.seoName(dimval.getName());
						}
						
						if (StringUtils.isBlank(repositoryId)) {
							logger.vlogError(
									"An entry in the list of {0} dimension values does not have a {1} property. This cannot be used as the key in the cache.",
									getDimensionName(), getRepositoryIdProperty());
						}
						String dimvalId = String.valueOf(dimval.getId());
						if (StringUtils.isBlank(dimvalId)) {
							logger.vlogError(
									"Cannot find the dimension value id for item {0}, this item will not be cached",
									repositoryId);
						}

						List<String> ancestorIds = null;
						DimValList ancestors = dimLocation.getAncestors();
						if ((ancestors != null) && !ancestors.isEmpty()) {
							ancestorIds = new ArrayList(ancestors.size());
							for (int i = 0; i < ancestors.size(); ++i) {
								ancestorIds.add(((DimVal) ancestors.get(i)).getProperties()
										.get(getRepositoryIdProperty()).toString());
							}
						}
						if (mLogging.isLoggingDebug()) {
							mLogging.logDebug("CPSSeoDimensionValueCacheRefreshHandler: Dimval.getProperties() - "+dimval.getProperties());
						}
						String seoUrl = (String) dimval.getProperties().get(SEO_URL_PROP);
						if (mLogging.isLoggingDebug()) {
							mLogging.logDebug("CPSSeoDimensionValueCacheRefreshHandler: seourl - "+seoUrl);
						}
						String url = StringUtils.isEmpty(seoUrl) ? navigationState.clearSearchFilters().clearRecordFilters().clearParameters()
								.selectNavigationFilter(dimvalId).toString() : seoUrl;
						newCache.put(repositoryId, dimvalId, url, ancestorIds, dimensionRoot.getDimensionName());
					}
				}
			}
		}

		if (newCache.getNumCacheEntries() > 0) {
			getDimensionValueCacheTools().swapCache(newCache);
		} else {
			AssemblerTools.getApplicationLogging().vlogWarning(
					"A cache refresh occurred which returned 0 entries, cache will not be updated", new Object[0]);

		}

		return allDimensions;
	}
	
	private static DimensionSearchMdexQuery buildMdexQuery(DimensionSearchResultsConfig itemConfig, SearchFilter search)
			throws CartridgeHandlerException {
		DimensionSearchMdexQuery query = new DimensionSearchMdexQuery();
		if ((itemConfig.getDimensionList() != null) && (itemConfig.getDimensionList().size() > 0)) {
			query.setDimensionValues(itemConfig.getDimensionList());
		}

		query.setShowCountsEnabled(itemConfig.isShowCountsEnabled());

		if ((itemConfig.getRelRankStrategy() == null) || ("".equals(itemConfig.getRelRankStrategy())))
			query.setRelRankStrategy(null);
		else {
			query.setRelRankStrategy(itemConfig.getRelRankStrategy());
		}

		if (search != null) {
			query.setTerms(search.getTerms());
		}

		query.setMaxDvalsPerDimension(itemConfig.getMaxResultsPerDimension());

		return query;
	}
	
	private void createDimensionNameDimensionIdCachMap(ENEQueryResults pResults) {
		if(pResults != null) {
			Map<String, Long> dimensionNameIdMap = new HashMap<String, Long>();
			DimensionSearchResultGroupList groupList = pResults.getDimensionSearch().getResults();
			if(groupList != null && !groupList.isEmpty()) {
				for(Object val: groupList) {
					DimensionSearchResultGroup group = (DimensionSearchResultGroup) val;
					DimVal dimensionRoot = (DimVal) group.getRoots().get(0);
					dimensionNameIdMap.put(dimensionRoot.getDimensionName(), dimensionRoot.getDimensionId());
				}
			}
			DimensionValueCacheTools cacheTools = getDimensionValueCacheTools();
			if(cacheTools instanceof CPSDimensionValueCacheTools) {
				((CPSDimensionValueCacheTools) cacheTools).swapDimensionNameIdMap(dimensionNameIdMap);
			}
		}
	}
	
	public ReverseDimensionValueCache getReverseDimensionValueCache() {
		return mReverseDimensionValueCache;
	}

	public void setReverseDimensionValueCache(ReverseDimensionValueCache pReverseDimensionValueCache) {
		this.mReverseDimensionValueCache = pReverseDimensionValueCache;
	}

	public SeoTools getSeoTools() {
		return mSeoTools;
	}

	public void setSeoTools(SeoTools pSeoTools) {
		this.mSeoTools = pSeoTools;
	}
	
	public List<String> getDimensionNamesForCache() {
		return mDimensionNamesForCache;
	}

	public void setDimensionNamesForCache(List<String> dimensionNamesForCache) {
		mDimensionNamesForCache = dimensionNamesForCache;
	}

	public boolean isUseDimensionSeo() {
		return mUseDimensionSeo;
	}

	public void setUseDimensionSeo(boolean pUseDimensionSeo) {
		this.mUseDimensionSeo = pUseDimensionSeo;
	}
}