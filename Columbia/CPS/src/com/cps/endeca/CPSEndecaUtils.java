package com.cps.endeca;

import com.endeca.navigation.ENEQueryResults;
import vsg.endeca.EndecaConstants;

/**
 * Endeca Utils
 */
public class CPSEndecaUtils implements EndecaConstants {

    /**
     * Gets the total num recs.
     *
     * @param results the results
     * @return the total num recs
     */
    public static long getTotalNumRecs(final ENEQueryResults results) {
        long numRecs = 0L;
        if (results.containsNavigation()) {
            numRecs = results.getNavigation().getTotalNumAggrERecs();
            if (numRecs == 0L) {
                numRecs = results.getNavigation().getTotalNumERecs();
            }
        }
        return numRecs;
    }

}
