package com.cps.endeca.infront.assembler;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;

/**
 * SecondarySearchResults class used for search within results
 * 
 * @author osednev
 * 
 */
public class SecondarySearchResults extends BasicContentItem {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7610298196277559861L;

	/**
	 * NAVIGATION_STATE
	 */
	private static final String NAVIGATION_STATE = "navigationState";

	/**
	 * Public constructor
	 * 
	 * @param pContentItem
	 *            content item
	 */
	public SecondarySearchResults(ContentItem pContentItem) {
		super(pContentItem);
	}

	/**
	 * Gets the navigation state
	 * 
	 * @return String navigation state
	 */
	public String getNavigationState() {
		return ((String) getTypedProperty(NAVIGATION_STATE));
	}

	/**
	 * 
	 * Sets the navigation state
	 * 
	 * @param pNavigationState
	 *            navitaion state
	 */
	public void setNavigationState(String pNavigationState) {
		put(NAVIGATION_STATE, pNavigationState);
	}
}