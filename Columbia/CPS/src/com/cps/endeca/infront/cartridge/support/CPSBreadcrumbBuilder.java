package com.cps.endeca.infront.cartridge.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vsg.endeca.EndecaConstants;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;

import com.cps.util.CPSConstants;
import com.endeca.infront.cartridge.Breadcrumbs;
import com.endeca.infront.cartridge.model.NavigationAction;
import com.endeca.infront.cartridge.model.RefinementBreadcrumb;
import com.endeca.infront.cartridge.support.ActionBuilder;
import com.endeca.infront.cartridge.support.BreadcrumbBuilder;
import com.endeca.infront.cartridge.support.RefinementBuilder;
import com.endeca.infront.cartridge.support.ResourceUtil;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.url.ActionPathProvider;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;
import com.endeca.infront.site.model.SiteState;
import com.endeca.navigation.DimVal;
import com.endeca.navigation.Dimension;
import com.endeca.navigation.Navigation;

/**
 * 
 * CPS BreadcrumbBuilder
 *
 */
public class CPSBreadcrumbBuilder extends BreadcrumbBuilder implements EndecaConstants {
	
	/**
	 * create refinement breadcrumbs
	 * @param pPathProvider = path provider
	 * @param pNavigationState - navigation state
	 * @param pSiteState - site state
	 * @param pNavigation - navigation
	 * @return refinement breadcrumbs
	 */
	public static List<RefinementBreadcrumb> createRefinementBreadcrumbs(ActionPathProvider pPathProvider, NavigationState pNavigationState,
			SiteState pSiteState, Navigation pNavigation, DimensionValueCacheTools pDimensionValueCacheTools) {
		List<RefinementBreadcrumb> refinementcrumbs = new ArrayList();
		List<Dimension> dimList = pNavigation.getDescriptorDimensions();
		String categoryNavState = null;
		
		for (Dimension dim : dimList) {
			if(dim.getRoot() != null && CPSConstants.DIM_PRODUCT_CATEGORY.equals(dim.getRoot().getDimensionName())){
				DimensionValueCacheObject dvco = pDimensionValueCacheTools.getCachedObjectForDimval(Long.toString(dim.getDescriptor().getId()));
				if(dvco != null){
					categoryNavState = dvco.getUrl();
				}
			}
		}
		
		for (Dimension dim : dimList) {
			refinementcrumbs.add(createRefinementBreadcrumb(pPathProvider, pNavigationState, pSiteState, dim, categoryNavState));
		}
		return refinementcrumbs;
	}
	
	/**
	 * create all breadcrumbs
	 * @param pBreadcrumbs - breadcrumbs
	 * @param pNavigation - navigation 
	 * @param pNavigationState - navigation state
	 * @param pSiteState - site state
	 * @param pActionPathProvider - action path provider
	 */
	public static void createAllBreadcrumbs(Breadcrumbs pBreadcrumbs, Navigation pNavigation, NavigationState pNavigationState,
			SiteState pSiteState, ActionPathProvider pActionPathProvider, DimensionValueCacheTools pDimensionValueCacheTools) {
		ActionPathProvider pathProvider = pActionPathProvider;
		pBreadcrumbs.setSearchCrumbs(createSearchBreadcrumbs(pathProvider, pNavigationState, pSiteState, pNavigation));
		pBreadcrumbs.setRefinementCrumbs(createRefinementBreadcrumbs(pathProvider, pNavigationState, pSiteState, pNavigation, pDimensionValueCacheTools));
		pBreadcrumbs.setRangeFilterCrumbs(createRangeFiltercrumbs(pathProvider, pNavigationState, pSiteState));
		pBreadcrumbs.setGeoFilterCrumb(createGeoFilterCrumb(pathProvider, pNavigationState, pSiteState));
		NavigationAction removeAllAction = new NavigationAction(pNavigationState.clearFilterState().toString());
		ActionBuilder.populateNavigationPathDefaults(pActionPathProvider,removeAllAction, pSiteState);
		pBreadcrumbs.setRemoveAllAction(removeAllAction);
	}
	
	/**
	 * create refinement breadcrumb
	 * @param pPathProvider - path provider
	 * @param pNavigationState - navigation state
	 * @param pSiteState - site state
	 * @param pDim - dimension
	 * @return refinement breadcrumb
	 */
	public static RefinementBreadcrumb createRefinementBreadcrumb(ActionPathProvider pPathProvider, NavigationState pNavigationState,
			SiteState pSiteState, Dimension pDim, String pCategoryNavState) {
		RefinementBreadcrumb refinementCrumb = new RefinementBreadcrumb();

		String dimensionName = pDim.getName();
		refinementCrumb.setDimensionName(dimensionName);

		String displayName = ResourceUtil.getLocalizedFieldName(dimensionName);
		refinementCrumb.setDisplayName(displayName);

		refinementCrumb.setAncestors(RefinementBuilder.createAncestors(
				pPathProvider, pDim.getAncestors(), pNavigationState, pSiteState,
				false, false));

		refinementCrumb.setMultiSelect((pDim.getRoot().isMultiSelectAnd()) || (pDim.getRoot().isMultiSelectOr()));
		refinementCrumb.setLabel(pDim.getDescriptor().getName());
		refinementCrumb.setProperties(RefinementBuilder.createProperties(pDim.getDescriptor()));
		refinementCrumb.setCount(RefinementBuilder.getCount(pDim.getDescriptor()));
		Map<String,String> properties = new HashMap<String,String>();
		properties.put(REFINEMENT_DIMVAL_ID_PROP_NAME, ""+pDim.getDescriptor().getId());
		if(dimensionName.equals(CATEGORY_DIMENSION_NAME)){
			properties.put(INDEX_CATEGORY_DESCRIPTION_PROPERTY_NAME, getCategoryCrumbDescription(pDim));
		}
		if(StringUtils.isNotEmpty(pCategoryNavState)){
			properties.put(CATEGORY_NAV_SATTE_PROPERTY, pCategoryNavState);
		}
		refinementCrumb.setProperties(properties);
		pNavigationState = pNavigationState.removeNavigationFilter(String.valueOf(pDim.getDescriptor().getId()));
		NavigationAction removeAction = new NavigationAction(pNavigationState.toString());
		ActionBuilder.populateNavigationPathDefaults(pPathProvider, removeAction, pSiteState);
		refinementCrumb.setRemoveAction(removeAction);

		return refinementCrumb;
	}
	
	/**
	 * add category description to refinement crumb
	 * @param pDim
	 * @return
	 */
	private static String getCategoryCrumbDescription(Dimension pDim){
		if(pDim != null && pDim.getCompletePath() != null){
			List<DimVal> dimValList = pDim.getCompletePath();
			for(DimVal dimVal : dimValList){
				if (dimVal.getId() == pDim.getDescriptor().getId() && dimVal.getProperties() != null){
					return (String) dimVal.getProperties().get(INDEX_CATEGORY_DESCRIPTION_PROPERTY_NAME);
				}
			}
		}
		return null;
	}
	
}
