package com.cps.endeca.infront.cartridge.model;

/**
 * This class is used to configure available for user records per page option
 *
 * @author Kate Koshman
 */
public class RecordsPerPageConfig {
	/** Records per page */
	private int mRecPerPage = 0;
	
	/** Label for option */
	private String mLabel;

	/**
	 * Gets records per page
	 * 
	 * @return records per page
	 */
	public int getRecordsPerPage() {
		return mRecPerPage;
	}

	/**
	 * Sets records per page
	 * 
	 * @param pRecPerPage - new records per page
	 */
	public void setRecordsPerPage(int pRecPerPage) {
		if (pRecPerPage >= 0) {
			mRecPerPage = pRecPerPage;
		}
	}

	/**
	 * Gets label for option
	 * 
	 * @return label for option
	 */
	public String getLabel() {
		return mLabel;
	}

	/**
	 * Sets label for option
	 * 
	 * @param pLabel - new label
	 */
	public void setLabel(String pLabel) {
		mLabel = pLabel;
	}
	
	@Override
	public boolean equals(Object pObj) {
		if (pObj instanceof RecordsPerPageConfig) {
			RecordsPerPageConfig otherConfig = (RecordsPerPageConfig)pObj;
			return mRecPerPage == otherConfig.mRecPerPage;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return mRecPerPage;
	}
}
