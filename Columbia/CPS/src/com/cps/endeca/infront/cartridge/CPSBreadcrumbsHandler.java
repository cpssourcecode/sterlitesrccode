package com.cps.endeca.infront.cartridge;

import atg.commerce.endeca.cache.DimensionValueCacheTools;

import com.cps.endeca.infront.cartridge.support.CPSBreadcrumbBuilder;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.Breadcrumbs;
import com.endeca.infront.cartridge.BreadcrumbsConfig;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;
import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.navigation.request.BreadcrumbsMdexQuery;
import com.endeca.infront.navigation.request.MdexRequest;
import com.endeca.navigation.ENEQueryResults;
import com.endeca.navigation.ERecCompoundSearchKey;
import com.endeca.navigation.ERecCompoundSearchKeyList;

/**
 * 
 * CPS Breadcrumbs handler
 *
 */
public class CPSBreadcrumbsHandler extends NavigationCartridgeHandler<BreadcrumbsConfig, Breadcrumbs> {
	
	/**
	 * mdex request
	 */
	private MdexRequest mMdexRequest;
	/**
	 * cache tools
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools;

	public CPSBreadcrumbsHandler() {
	}

	protected BreadcrumbsConfig wrapConfig(ContentItem pContentItem) {
		return new BreadcrumbsConfig(pContentItem);
	}

	public void preprocess(BreadcrumbsConfig cartridgeConfig)
			throws CartridgeHandlerException {
		this.mMdexRequest = createMdexRequest(getNavigationState().getFilterState(), new BreadcrumbsMdexQuery());
	}
	
	public void setDimensionValueCacheTools(
			DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}
	
	public Breadcrumbs process(BreadcrumbsConfig cartridgeConfig)
			throws CartridgeHandlerException {
		ENEQueryResults results = executeMdexRequest(this.mMdexRequest);
		NavigationState navigationState = getNavigationState();
		navigationState.inform(results);
		Breadcrumbs breadcrumbs = new Breadcrumbs(cartridgeConfig);
		CPSBreadcrumbBuilder.createAllBreadcrumbs(breadcrumbs, results.getNavigation(), getNavigationState(), getSiteState(), getActionPathProvider(), getDimensionValueCacheTools());
		return breadcrumbs;
	}
	
}
