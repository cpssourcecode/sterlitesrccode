package com.cps.endeca.infront.cartridge.model;

import java.text.MessageFormat;

import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import com.cps.service.OrganizationAliasService;
import com.cps.util.CPSConstants;
import com.endeca.infront.cartridge.model.SortOptionConfig;

/**
 * custom sort option config
 */
public class CPSSortOptionCinfig extends SortOptionConfig {
	
	/**
	 * profile
	 */
	private Profile mProfile;
	
	/**
	 * Organization Alias Service
	 */
	private OrganizationAliasService mOrganizationAliasService;
	
	/**
	 * value template
	 */
	private String mValueTemplate;
	
	@Override
	public String getValue() {
		if(getProfile() != null && !getProfile().isTransient()){
			Object org = getProfile() == null ? null : getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
			if(org != null){
				String orgId = ((RepositoryItem) org).getRepositoryId();
				return MessageFormat.format(getValueTemplate(), new Object[] { orgId });
			}
		}
		return null;
	}
	
	public Profile getProfile() {
		return mProfile;
	}

	public void setProfile(Profile pProfile) {
		this.mProfile = pProfile;
	}
	
	public String getValueTemplate() {
		return mValueTemplate;
	}

	public void setValueTemplate(String pValueTemplate) {
		this.mValueTemplate = pValueTemplate;
	}
	
	public OrganizationAliasService getOrganizationAliasService() {
		return mOrganizationAliasService;
	}

	public void setOrganizationAliasService(
			OrganizationAliasService pOrganizationAliasService) {
		this.mOrganizationAliasService = pOrganizationAliasService;
	}
}
