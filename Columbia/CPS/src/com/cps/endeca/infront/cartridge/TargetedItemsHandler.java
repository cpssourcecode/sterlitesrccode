package com.cps.endeca.infront.cartridge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.naming.NameResolver;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.targeting.Targeter;
import atg.targeting.TargetingException;
import atg.targeting.TargetingSourceMap;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.CartridgeHandlerException;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.NavigationCartridgeHandler;


/**
 * TargetedItemsHandler adds prefix to configured in cartridge component if
 * needed, resolves targeter component, targets for mItemCount of items and set
 * them in TargeterItems.
 * 
 */
public class TargetedItemsHandler extends NavigationCartridgeHandler<ContentItem, TargetedItemsContentItem> {

	/**
	 * path separator
	 */
	protected final static String COMPONENT_PATH_SEPARATOR = "/";
	/**
	 * component path property name
	 */
	protected static final String COMPONENT_PATH = "componentPath";
	/**
	 * use random property name
	 */
	protected static final String USE_RANDOM = "useRandom";
	/**
	 * item count property name
	 */
	protected static final String ITEM_COUNT = "itemCount";
	/**
	 * default not random
	 */
	protected static final boolean DEFAULT_USE_RANDOM = false;
	/**
	 * default amount 0
	 */
	protected static final int DEFAULT_ITEMS_AMOUNT = 0;

	/**
	 * property path prefix
	 */
	private String mPathPrefix;

	/**
	 * sort property
	 */
	private String mSortProperty;
	/**
	 * @return the pathPrefix
	 */
	public String getPathPrefix() {
		return mPathPrefix;
	}

	/**
	 * @param pPathPrefix
	 *            the pathPrefix to set
	 */
	public void setPathPrefix(String pPathPrefix) {
		mPathPrefix = pPathPrefix;
	}

	/**
	 *  Descriptor of item that should be returned be this targeter
	 */
	private String mItemDescriptor;

	/**
	 * @return the itemDescriptor
	 */
	public String getItemDescriptor() {
		return mItemDescriptor;
	}

	/**
	 * @param pItemDescriptor
	 *            the itemDescriptor to set
	 */
	public void setItemDescriptor(String pItemDescriptor) {
		mItemDescriptor = pItemDescriptor;
	}

	/**
	 * Repository of item that should be returned by this targeter
	 */
	private Repository mRepository;

	/**
	 * @return the repository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * @param pRepository
	 *            the repository to set
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	/**
	 * Targeting Source Map
	 */
	TargetingSourceMap mSourceMap;

	/**
	 * @return the mSourceMap
	 */
	public TargetingSourceMap getSourceMap() {
		return mSourceMap;
	}

	/**
	 * 
	 * @return mSortProperty
	 */
	public String getSortProperty() {
		return mSortProperty;
	}

	/**
	 * 
	 * @param mSortProperty - mSortProperty
	 */
	public void setSortProperty(String mSortProperty) {
		this.mSortProperty = mSortProperty;
	}

	/**
	 * @param pSourceMap
	 *            the sourceMap to set
	 */
	public void setSourceMap(TargetingSourceMap pSourceMap) {
		mSourceMap = pSourceMap;
	}

	/**
	 * Create a new BasicContentItem using the passed in ContentItem
	 * @return BasicContentItem
	 * @param pContentitem - ContentItem
	 */
	@Override
	protected ContentItem wrapConfig(ContentItem pContentitem) {
		return new BasicContentItem(pContentitem);
	}

	@Override
	public void preprocess(ContentItem pContentItem) throws CartridgeHandlerException {
		// Nothing to do in preprocess method
	}

	/**
	 * This method builds full targeter\slot component path is build using
	 * component path configured in Experience Manager and pathPrefix property.
	 * Then the targeter\slot component is resolved and itemCount items are
	 * targeted. Items returned by slot\targeter should be of type
	 * itemDescriptor. Only items of this type will be put in
	 * TargetedItemsContentItem. If item of another type is found, error will be
	 * logged. If itemCount is not set, then all items returned by slot or
	 * targeter of type itemDescriptor will be put in TargetedItemsContentItem.
	 * 
	 * @param pCartridgeConfig - cartridge config
	 * @throws CartridgeHandlerException - CartridgeHandlerException
	 * @return targetedItems
	 */
	@Override
	public TargetedItemsContentItem process(ContentItem pCartridgeConfig) throws CartridgeHandlerException {
		TargetedItemsContentItem targetedItems = new TargetedItemsContentItem(pCartridgeConfig);
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		NameResolver nameResolver = getSourceMap().getNameResolver(request);

		String fullcomponentPath = buildFullTargeterPath(pCartridgeConfig);

		if (!StringUtils.isEmpty(fullcomponentPath)) {
			try {
				Targeter targeter = (Targeter) request.resolveName(fullcomponentPath);
				if (targeter != null) {
					generateTargetedItems(pCartridgeConfig, targetedItems, nameResolver, targeter);
				} else {
					AssemblerTools.getApplicationLogging().vlogDebug(
							"TargetedItemsHandler.process: failed to resolve component: {0}", fullcomponentPath);
				}
			} catch (Exception ex) {
				AssemblerTools.getApplicationLogging().vlogDebug(
						"Error occurs in TargetedItemsHandler.process method: ", ex);
			}

		}
		
		return targetedItems;
	}

	/**
	 * generates targeted items
	 * @param pCartridgeConfig - crttridge config
	 * @param targetedItems - targeted items
	 * @param nameResolver - name resolver
	 * @param targeter - targeter
	 * @throws TargetingException - TargetingException
	 * @throws RepositoryException - RepositoryException
	 */
	private void generateTargetedItems(ContentItem pCartridgeConfig, TargetedItemsContentItem targetedItems,
			NameResolver nameResolver, Targeter targeter) throws TargetingException, RepositoryException {
		Object[] itemsAll = null;

		Integer howMany = ((BasicContentItem) pCartridgeConfig).getIntProperty(ITEM_COUNT,
				DEFAULT_ITEMS_AMOUNT);

		Boolean useRandom = ((BasicContentItem) pCartridgeConfig).getBooleanProperty(USE_RANDOM,
				DEFAULT_USE_RANDOM);

		if (useRandom) {
			itemsAll = targeter.target(nameResolver);
		} else {
			itemsAll = targeter.target(nameResolver, howMany);
		}

		if (itemsAll != null && itemsAll.length > 0) {

			howMany = Math.min(howMany, itemsAll.length);

			Object[] items = checkUseRandom(itemsAll, howMany, useRandom);
			
			targetedItems.setItems(checkSpecificRepository(items));
			
			
		}
	}
	/**
	 * Sorting pItemsAll if all them is promoContent by sortPriority
	 * @param pItemsAll - all items
	 * @return sort items
	 */
	@SuppressWarnings("unchecked")
	private Object[] sortBySortPriority(Object[] pItemsAll) {
		List<RepositoryItem> promoContentItems;
		try {
			promoContentItems = checkSpecificRepository(pItemsAll);
		} catch (RepositoryException e) {
			return pItemsAll;
		}
		
		Collections.sort(promoContentItems, new Comparator<RepositoryItem>() {
			@Override
			public int compare(RepositoryItem o1, RepositoryItem o2) {
				return (Integer) o1.getPropertyValue(getSortProperty())
						- (Integer) o2.getPropertyValue(getSortProperty());
			}
		});
		return promoContentItems.toArray();
	}
	
	/**
	 * randomizes the array if useRandom = true
	 * @param itemsAll - all items
	 * @param howMany - items count
	 * @param useRandom - boolean flag
	 * @return items
	 */
	private Object[] checkUseRandom(Object[] itemsAll, Integer howMany, Boolean useRandom) {
		Object[] items = null;

		if (useRandom) {
			int[] randomIntegerArray = randomizeIntegerArray(itemsAll.length, howMany);

			items = new Object[howMany];

			for (int i = 0; i < howMany; ++i) {
				int index = randomIntegerArray[i];
				items[i] = itemsAll[index];
			}
		} else {
			items = sortBySortPriority(itemsAll);
		}
		return items;
	}

	/**
	 * Build full targeter component path
	 * @param pCartridgeConfig - catridge config
	 * @return full component path
	 */
	private String buildFullTargeterPath(ContentItem pCartridgeConfig) {
		String fullcomponentPath = "";
		if (!StringUtils.isEmpty(getPathPrefix())) {
			fullcomponentPath = getFullPath(getPathPrefix(), (String) pCartridgeConfig.get(COMPONENT_PATH));
		} else {
			fullcomponentPath = (String) pCartridgeConfig.get(COMPONENT_PATH);
		}
		return fullcomponentPath;
	}

	/**		
	 * Check that all item are from specified repository with specified item descriptor
	 * @param items - targeted items
	 * @throws RepositoryException - RepositoryException
	 * @return valid items
	 */
	private List checkSpecificRepository(Object[] items) throws RepositoryException {
		List validItems = new ArrayList();
		for (Object item : items) {
			if (item instanceof RepositoryItem) {
				Repository itemRepository = ((RepositoryItem) item).getRepository();
				// Check that item is from required repository
				if (itemRepository.equals(getRepository())) {
					RepositoryItemDescriptor expectedItemDescriptor = itemRepository
							.getItemDescriptor(getItemDescriptor());
					// Check that item has required item descriptor
					if (expectedItemDescriptor.isInstance(item)) {
						validItems.add(item);
					} else {
						AssemblerTools.getApplicationLogging().vlogDebug(
								"TargetedItemsHandler.process: expect item with item descriptor: {0} ,"
										+ " but got item with item descriptor: {1} ", getItemDescriptor(),
								((RepositoryItem) item).getItemDisplayName());
					}
				} else {
					AssemblerTools.getApplicationLogging().vlogDebug(
							"TargetedItemsHandler.process: expect item from repository: {0} ,"
									+ " but got item from repository: {1} ", getRepository(), itemRepository);
				}
			}
		}
		return validItems;
	}

	/**
	 * Builds full path adding pPathPrefix to pComponentPath if needed
	 * 
	 * @param pPathPrefix
	 *            path prefix
	 * @param pComponentPath
	 *            configured component path
	 * @return String
	 */
	private String getFullPath(String pPathPrefix, String pComponentPath) {
		StringBuilder sb = new StringBuilder();
		if (!StringUtils.isEmpty(pComponentPath)) {
			if (!pComponentPath.startsWith(pPathPrefix)) {
				sb.append(pPathPrefix);

				if (!pPathPrefix.endsWith(COMPONENT_PATH_SEPARATOR)) {
					sb.append(COMPONENT_PATH_SEPARATOR);
				}

				if (pComponentPath.startsWith(COMPONENT_PATH_SEPARATOR)) {
					sb.append(pComponentPath.substring(1));
				} else {
					sb.append(pComponentPath);
				}
			} else {

				return pComponentPath;
			}
		}
		return sb.toString();
	}

	/**
	 * Randomize array of pSetSize elements with the pGroupSize result size
	 * 
	 * @param pSetSize - set size
	 * @param pGroupSize - group size
	 * @return int[]
	 */
	private int[] randomizeIntegerArray(int pSetSize, int pGroupSize) {

		int tmp = 0;
		int[] inPlaceRandomArray = new int[pSetSize];

		for (int i = 0; i < pSetSize; ++i) {
			inPlaceRandomArray[i] = i;
		}
		for (int offset = 0; offset < pGroupSize; --pSetSize) {
			int index = (int) (Math.random() * pSetSize);

			index = Math.min(index, pSetSize - 1) + offset;

			if (index != offset) {
				tmp = inPlaceRandomArray[offset];
				inPlaceRandomArray[offset] = inPlaceRandomArray[index];
				inPlaceRandomArray[index] = tmp;
			}
			++offset;
		}

		return inPlaceRandomArray;
	}

}
