package com.cps.endeca.infront.cartridge;

import java.util.List;

import com.endeca.infront.assembler.BasicContentItem;
import com.endeca.infront.assembler.ContentItem;

/**
 * 
 * targeted items object
 *
 */
public class TargetedItemsContentItem extends BasicContentItem {

	/**
	 * Default Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Item to be displayed in the slot
	 */
	private static final String ITEMS = "items";

	/**
	 * @return the item
	 */
	public List getItems() {
		return getTypedProperty(ITEMS);
	}

	/**
	 * @param pItems
	 *            the items to set
	 */
	public void setItems(List pItems) {
		put(ITEMS, pItems);
	}

	/**
	 * @param pConfig
	 *            Construct a TargetedItems from the pConfig passed in.
	 */
	public TargetedItemsContentItem(ContentItem pConfig) {
		super(pConfig);
	}

}
