package com.cps.endeca.infront.support;


import com.endeca.infront.cartridge.model.NavigationAction;

/**
 * This class is used to present change records per page action
 *
 * @author Kate Koshman
 */
public class RecordsPerPageControl extends NavigationAction {
	/** Records per page */
	private int mRecPerPage = 0;

	/**
	 * Default constructor
	 */
	public RecordsPerPageControl() {
		super();
	}

	/**
	 * Constructor by navigation state
	 * @param pNavigationState - navigation state
	 */
	public RecordsPerPageControl(String pNavigationState) {
		super(pNavigationState);
	}

	/**
	 * Constructor by navigation state and label
	 *
	 * @param pNavigationState - navigation state
	 * @param pLabel - label
	 */
	public RecordsPerPageControl(String pNavigationState, String pLabel) {
		super(pNavigationState, pLabel);
	}

	/** Constructor by navigation state, label, site root path and content path
	 * @param pNavigationState - navigation state
	 * @param pLabel - label
	 * @param pSiteRootPath - site root path
	 * @param pContentPath - content path
	 */
/*
	public RecordsPerPageControl(String pNavigationState, String pLabel, String pSiteRootPath, String pContentPath) {
		//super(pNavigationState, pLabel, pSiteRootPath, pContentPath); CHANGE
		super(pNavigationState, pLabel, pSiteRootPath, pContentPath, null);
	}
*/

	/**
	 * Gets records per page
	 *
	 * @return records per page
	 */
	public int getRecordsPerPage() {
		return mRecPerPage;
	}

	/**
	 * Sets records per page
	 * @param pRecPerPage - new records per page
	 */
	public void setRecordsPerPage(int pRecPerPage) {
		if (pRecPerPage >= 0) {
			mRecPerPage = pRecPerPage;
		}
	}
}
