package com.cps.endeca.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import com.cps.service.OrganizationAliasService;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.FilePermissionsHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.RepositoryItem;

public class SearchInterfaceConfigService extends GenericService {

    /**
     * Organization Alias Service
     */
    private OrganizationAliasService mOrganizationAliasService;

    private File searchInterfaceFolderConfigFilePath;
    private File searchInterfaceConfigFilePath;
    private File searchInterfaceConfigTemplateFilePath;
    private File searchInterfaceLookupConfigTemplateFilePath;
    private File searchInterfaceLookupConfigFilePath;

    private boolean mClearSearchInterfaceConfigFolder = true;
    private boolean mCopyToEndecaConfig = false;
    private boolean mCreateConfigForOrganizationsWithAlias = false;
    private FilePermissionsHelper filePermissionsHelper;

    /**
     * search interface config path
     */
    // private String mSearchInterfaceConfigPath;

    /**
     * search interface folder config path
     */
    // private String mSearchInterfaceFolderConfigPath;

    /**
     * search interface config template path
     */
    // private String mSearchInterfaceConfigTemplatePath;

    /**
     * @return the filePermissionsHelper
     */
    public FilePermissionsHelper getFilePermissionsHelper() {
        return filePermissionsHelper;
    }

    /**
     * @param filePermissionsHelper
     *            the filePermissionsHelper to set
     */
    public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
        this.filePermissionsHelper = filePermissionsHelper;
    }

    /**
     * temp search interface config folder path
     */
    private String mTempSearchInterfaceConfigFolderPath;

    /**
     * search interface config folder path
     */
    private String mSearchInterfaceConfigFolderPath;

    public OrganizationAliasService getOrganizationAliasService() {
        return mOrganizationAliasService;
    }

    public void setOrganizationAliasService(OrganizationAliasService pOrganizationAliasService) {
        mOrganizationAliasService = pOrganizationAliasService;
    }

    public void setClearSearchInterfaceConfigFolder(boolean pClearSearchInterfaceConfigFolder) {
        mClearSearchInterfaceConfigFolder = pClearSearchInterfaceConfigFolder;
    }

    public boolean isClearSearchInterfaceConfigFolder() {
        return mClearSearchInterfaceConfigFolder;
    }

    public void setCopyToEndecaConfig(boolean pCopyToEndecaConfig) {
        mCopyToEndecaConfig = pCopyToEndecaConfig;
    }

    public boolean isCopyToEndecaConfig() {
        return mCopyToEndecaConfig;
    }

    public void setCreateConfigForOrganizationsWithAlias(boolean pCreateConfigForOrganizationsWithAlias) {
        mCreateConfigForOrganizationsWithAlias = pCreateConfigForOrganizationsWithAlias;
    }

    public boolean isCreateConfigForOrganizationsWithAlias() {
        return mCreateConfigForOrganizationsWithAlias;
    }

    // public String getSearchInterfaceFolderConfigPath() {
    // return mSearchInterfaceFolderConfigPath;
    // }
    //
    // public void setSearchInterfaceFolderConfigPath(String pSearchInterfaceFolderConfigPath) {
    // mSearchInterfaceFolderConfigPath = pSearchInterfaceFolderConfigPath;
    // }
    //
    // public String getSearchInterfaceConfigPath() {
    // return mSearchInterfaceConfigPath;
    // }
    //
    // public void setSearchInterfaceConfigPath(String pSearchInterfaceConfigPath) {
    // mSearchInterfaceConfigPath = pSearchInterfaceConfigPath;
    // }
    //
    // public String getSearchInterfaceConfigTemplatePath() {
    // return mSearchInterfaceConfigTemplatePath;
    // }
    //
    // public void setSearchInterfaceConfigTemplatePath(String pSearchInterfaceConfigTemplatePath) {
    // mSearchInterfaceConfigTemplatePath = pSearchInterfaceConfigTemplatePath;
    // }

    /**
     * @return the searchInterfaceFolderConfigFilePath
     */
    public File getSearchInterfaceFolderConfigFilePath() {
        return searchInterfaceFolderConfigFilePath;
    }

    /**
     * @param searchInterfaceFolderConfigFilePath
     *            the searchInterfaceFolderConfigFilePath to set
     */
    public void setSearchInterfaceFolderConfigFilePath(File searchInterfaceFolderConfigFilePath) {
        this.searchInterfaceFolderConfigFilePath = searchInterfaceFolderConfigFilePath;
    }

    /**
     * @return the searchInterfaceConfigFilePath
     */
    public File getSearchInterfaceConfigFilePath() {
        return searchInterfaceConfigFilePath;
    }

    /**
     * @param searchInterfaceConfigFilePath
     *            the searchInterfaceConfigFilePath to set
     */
    public void setSearchInterfaceConfigFilePath(File searchInterfaceConfigFilePath) {
        this.searchInterfaceConfigFilePath = searchInterfaceConfigFilePath;
    }

    /**
     * @return the searchInterfaceConfigTemplateFilePath
     */
    public File getSearchInterfaceConfigTemplateFilePath() {
        return searchInterfaceConfigTemplateFilePath;
    }

    /**
     * @param searchInterfaceConfigTemplateFilePath
     *            the searchInterfaceConfigTemplateFilePath to set
     */
    public void setSearchInterfaceConfigTemplateFilePath(File searchInterfaceConfigTemplateFilePath) {
        this.searchInterfaceConfigTemplateFilePath = searchInterfaceConfigTemplateFilePath;
    }

    /**
     * @return the searchInterfaceLookupConfigTemplateFilePath
     */
    public File getSearchInterfaceLookupConfigTemplateFilePath() {
        return searchInterfaceLookupConfigTemplateFilePath;
    }

    /**
     * @param searchInterfaceLookupConfigTemplateFilePath
     *            the searchInterfaceLookupConfigTemplateFilePath to set
     */
    public void setSearchInterfaceLookupConfigTemplateFilePath(File searchInterfaceLookupConfigTemplateFilePath) {
        this.searchInterfaceLookupConfigTemplateFilePath = searchInterfaceLookupConfigTemplateFilePath;
    }

    public File getSearchInterfaceLookupConfigFilePath() {
        return searchInterfaceLookupConfigFilePath;
    }

    public void setSearchInterfaceLookupConfigFilePath(File searchInterfaceLookupConfigFilePath) {
        this.searchInterfaceLookupConfigFilePath = searchInterfaceLookupConfigFilePath;
    }

    public String getTempSearchInterfaceConfigFolderPath() {
        return mTempSearchInterfaceConfigFolderPath;
    }

    public void setTempSearchInterfaceConfigFolderPath(String pTempSearchInterfaceConfigFolderPath) {
        mTempSearchInterfaceConfigFolderPath = pTempSearchInterfaceConfigFolderPath;
    }

    public String getSearchInterfaceConfigFolderPath() {
        return mSearchInterfaceConfigFolderPath;
    }

    public void setSearchInterfaceConfigFolderPath(String pSearchInterfaceConfigFolderPath) {
        mSearchInterfaceConfigFolderPath = pSearchInterfaceConfigFolderPath;
    }

    public boolean generateSearchInterfaceConfig() {

        CPSProfileTools profileTools = getOrganizationAliasService().getProfileTools();
        RepositoryItem[] organizations = profileTools.queryOrganizations();
        boolean result = true;
        String mainDirPath = null;
        if (organizations != null) {
            try {

                String searchInterfaceFolderConfigPath = getSearchInterfaceFolderConfigFilePath().getAbsolutePath();
                String searchInterfaceConfigPath = getSearchInterfaceConfigFilePath().getAbsolutePath();
                String searchInterfaceConfigTemplatePath = getSearchInterfaceConfigTemplateFilePath().getAbsolutePath();
                String searchInterfaceLookupConfigPath = getSearchInterfaceLookupConfigFilePath().getAbsolutePath();
                String searchInterfaceLookupConfigTemplatePath = getSearchInterfaceLookupConfigTemplateFilePath().getAbsolutePath();

                String searchInterfaceFolderConfig = profileTools.readFile(searchInterfaceFolderConfigPath, StandardCharsets.UTF_8);
                String allSearchInterfaceConfig = profileTools.readFile(searchInterfaceConfigPath, StandardCharsets.UTF_8);
                String searchInterfaceConfigTemplate = profileTools.readFile(searchInterfaceConfigTemplatePath, StandardCharsets.UTF_8);
                String lookupSearchInterfaceConfig = profileTools.readFile(searchInterfaceLookupConfigPath, StandardCharsets.UTF_8);
                String searchInterfaceLookupConfigTemplate = profileTools.readFile(searchInterfaceLookupConfigTemplatePath, StandardCharsets.UTF_8);

                mainDirPath = getTempSearchInterfaceConfigFolderPath();
                Path mainDir = Paths.get(mainDirPath);

                // add folder config file
                vlogDebug(new StringBuilder().append("Creating folder: ").append(mainDir).toString());

                Files.createDirectories(mainDir);

                if (isClearSearchInterfaceConfigFolder()) {
                    File src = new File(mainDirPath);
                    FileUtils.cleanDirectory(src);
                }

                createAndWriteFile(new StringBuilder(mainDirPath).append("/_.json").toString(), searchInterfaceFolderConfig);

                // add All search interface folder and config file
                String allInterfaceDirPath = new StringBuilder(mainDirPath).append("/All").toString();
                Path allInterfaceDir = Paths.get(allInterfaceDirPath);

                vlogDebug(new StringBuilder().append("Creating folder: ").append(allInterfaceDirPath).toString());

                Files.createDirectories(allInterfaceDir);
                createAndWriteFile(new StringBuilder(allInterfaceDirPath).append("/_.json").toString(), allSearchInterfaceConfig);

                String lookupInterfaceDirPath = new StringBuilder(mainDirPath).append("/Lookup").toString();
                Path lookupInterfaceDir = Paths.get(lookupInterfaceDirPath);

                vlogDebug(new StringBuilder().append("Creating folder: ").append(lookupInterfaceDirPath).toString());

                Files.createDirectories(lookupInterfaceDir);
                createAndWriteFile(new StringBuilder(lookupInterfaceDirPath).append("/_.json").toString(), lookupSearchInterfaceConfig);

                for (RepositoryItem org : organizations) {
                    String orgId = org.getRepositoryId();
                    Set<String> orgIds = getOrganizationAliasService().getOrganizationsWithAlias();
                    if (!isCreateConfigForOrganizationsWithAlias() || isCreateConfigForOrganizationsWithAlias() && orgIds.contains(orgId)) {
                        String orgInterfaceDirPath = new StringBuilder(mainDirPath).append("/All_").append(orgId).toString();
                        Path orgInterfaceDir = Paths.get(orgInterfaceDirPath);

                        vlogDebug(new StringBuilder().append("Creating folder: ").append(orgInterfaceDirPath).toString());

                        Files.createDirectories(orgInterfaceDir);

                        String searchInterfaceConfig = MessageFormat.format(searchInterfaceConfigTemplate, new Object[] { orgId });

                        result = isJSONValid(searchInterfaceConfig);
                        if (!result) {
                            vlogWarning(new StringBuilder("searchInterfaceConfig for org: ").append(orgId).append(" is not valid").toString());
                            result = false;
                            break;
                        }

                        String orgInterfaceConfigFilePath = new StringBuilder(orgInterfaceDirPath).append("/_.json").toString();
                        createAndWriteFile(orgInterfaceConfigFilePath, searchInterfaceConfig);

                        // ---- Lookup

                        String orgInterfaceLookupDirPath = new StringBuilder(mainDirPath).append("/Lookup_").append(orgId).toString();
                        Path orgInterfaceLookupDir = Paths.get(orgInterfaceLookupDirPath);

                        vlogDebug(new StringBuilder().append("Creating folder: ").append(orgInterfaceLookupDirPath).toString());

                        Files.createDirectories(orgInterfaceLookupDir);

                        String searchInterfaceLookupConfig = MessageFormat.format(searchInterfaceLookupConfigTemplate, new Object[] { orgId });

                        result = isJSONValid(searchInterfaceLookupConfig);
                        if (!result) {
                            vlogWarning(new StringBuilder("searchInterfaceLookupConfig for org: ").append(orgId).append(" is not valid").toString());
                            result = false;
                            break;
                        }

                        String orgInterfaceLookupConfigFilePath = new StringBuilder(orgInterfaceLookupDirPath).append("/_.json").toString();
                        createAndWriteFile(orgInterfaceLookupConfigFilePath, searchInterfaceLookupConfig);
                    }

                }

            } catch (IOException e) {
                vlogError(e, "Error");
                logError(e);
            }
        }
        setFilePermissions(mainDirPath);

        if (isCopyToEndecaConfig() && result) {
            result = copyToEndecaConfig();
        }

        return result;
    }

    private void createAndWriteFile(String pPath, String pContent) throws IOException {

        vlogDebug(new StringBuilder().append("Writing file: ").append(pPath).toString());
        File tempFile = new File(pPath);
        if (!tempFile.exists()) {
            tempFile.createNewFile();
        }
        Path filePath = Paths.get(pPath);
        Files.write(filePath, pContent.getBytes(StandardCharsets.UTF_8));

    }

    public boolean copyToEndecaConfig() {

        boolean result = true;
        try {
            // Files.copy(Paths.get(getTempSearchInterfaceConfigFolderPath()), Paths.get(getSearchInterfaceConfigFolderPath()), REPLACE_EXISTING);
            File src = new File(getTempSearchInterfaceConfigFolderPath());
            File dest = new File(getSearchInterfaceConfigFolderPath());
            // clear destination folder before copying
            if (isClearSearchInterfaceConfigFolder()) {
                FileUtils.cleanDirectory(dest);
            }
            FileUtils.copyDirectory(src, dest);
        } catch (IOException e) {
            vlogError(e, "Error copying config to endeca");
            result = false;
        }

        return result;
    }

    public static boolean isJSONValid(String pJsonInString) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(pJsonInString);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private void setFilePermissions(String destPath) {
        if (destPath == null) {
            vlogInfo("no search config directory to set permissions. exiting the path - {0}", destPath);
            return;
        }
        FilePermissionsHelper fpHelper = getFilePermissionsHelper();
        if (fpHelper == null) {
            fpHelper = (FilePermissionsHelper) Nucleus.getGlobalNucleus().resolveName("/cps/util/FilePermissionsHelper");
        }
        try {
            fpHelper.setFilePermissionsRecursivelyOnDir(destPath);
        } catch (IOException e) {
            vlogError(e.getMessage());
            logError(e);
        }

    }

}
