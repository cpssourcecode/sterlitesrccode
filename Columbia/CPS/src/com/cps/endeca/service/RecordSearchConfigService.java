package com.cps.endeca.service;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.cps.userprofiling.CPSProfileTools;
import org.xml.sax.SAXException;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryItem;

public class RecordSearchConfigService extends GenericService {

	/**
	 * temp record search config path
	 */
	private String mTempRecordSearchConfigPath;
	/**
	 * temp record search config path
	 */
	private String mRecordSearchConfigPath;
	/**
	 * record search config template path
	 */
	private String mRecordSearchConfigTemplatePath;
	/**
	 * record search interface template path
	 */
	private String mRecordSearchInterfaceTemplatePath;

	private CPSProfileTools mProfileTools;

	public boolean generateRecordSearchConfig() {

		CPSProfileTools profileTools = getProfileTools();
		RepositoryItem[] organizations = profileTools.queryOrganizations();
		boolean result = false;
		if (organizations != null) {
			try {
				String searchInterfaceTemplate = profileTools.readFile(getRecordSearchInterfaceTemplatePath(), StandardCharsets.UTF_8);
				String configTemplate = profileTools.readFile(getRecordSearchConfigTemplatePath(), StandardCharsets.UTF_8);

				StringBuffer interfaces = new StringBuffer();
				for (RepositoryItem org : organizations) {
					interfaces.append(MessageFormat.format(searchInterfaceTemplate, new Object[]{org.getRepositoryId()}) + "\n");
				}

				configTemplate = MessageFormat.format(configTemplate, new Object[]{interfaces});
				File tempFile = new File(getTempRecordSearchConfigPath());
				if (!tempFile.exists()) {
					tempFile.createNewFile();
				}
				Path path = Paths.get(getTempRecordSearchConfigPath());
				Files.write(path, configTemplate.getBytes(StandardCharsets.UTF_8));
				result = validateRecordConfig();

			} catch (IOException e) {
				vlogError(e, e.getMessage());
			}
		}

		return result;
	}

	public boolean copyToEndecaConfig() {

		boolean result = true;
		try {
			Files.copy(Paths.get(getTempRecordSearchConfigPath()), Paths.get(getRecordSearchConfigPath()), REPLACE_EXISTING);
		} catch (IOException e) {
			vlogError(e, e.getMessage());
			result = false;
		}

		return result;
	}

	/**
	 * validate record config file
	 *
	 * @return true if valid
	 */
	private boolean validateRecordConfig() {
		try {
			File fXmlFile = new File(getTempRecordSearchConfigPath());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			dBuilder.parse(fXmlFile);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			vlogError(e, e.getMessage());
			return false;
		}
		return true;
	}

	public String getRecordSearchConfigPath() {
		return mRecordSearchConfigPath;
	}

	public void setRecordSearchConfigPath(String pRecordSearchConfigPath) {
		this.mRecordSearchConfigPath = pRecordSearchConfigPath;
	}

	public String getRecordSearchConfigTemplatePath() {
		return mRecordSearchConfigTemplatePath;
	}

	public void setRecordSearchConfigTemplatePath(
			String pRecordSearchConfigTemplatePath) {
		this.mRecordSearchConfigTemplatePath = pRecordSearchConfigTemplatePath;
	}

	public String getRecordSearchInterfaceTemplatePath() {
		return mRecordSearchInterfaceTemplatePath;
	}

	public void setRecordSearchInterfaceTemplatePath(
			String pRecordSearchInterfaceTemplatePath) {
		this.mRecordSearchInterfaceTemplatePath = pRecordSearchInterfaceTemplatePath;
	}

	public String getTempRecordSearchConfigPath() {
		return mTempRecordSearchConfigPath;
	}

	public void setTempRecordSearchConfigPath(String pTempRecordSearchConfigPath) {
		this.mTempRecordSearchConfigPath = pTempRecordSearchConfigPath;
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

}
