package com.cps.endeca;

import java.util.List;

import com.endeca.infront.cartridge.RefinementMenu;
import com.endeca.infront.cartridge.RefinementMenuConfig;

/**
 * 
 * @author VSG
 *
 */
public class CategoryGuidedNavigationMenu extends RefinementMenu {

	/** The REFINEMENT_MENUS constant */
	private static final String REFINEMENT_MENUS = "refinementMenus";

	public CategoryGuidedNavigationMenu(RefinementMenuConfig pConfig) {
		super(pConfig);
	}

	public List<RefinementMenu> getRefinementMenus() {
		return getTypedProperty(REFINEMENT_MENUS);
	}

	public void setRefinementMenus(List<RefinementMenu> pRefinementMenus) {
		put(REFINEMENT_MENUS, pRefinementMenus);
	}
	
}
