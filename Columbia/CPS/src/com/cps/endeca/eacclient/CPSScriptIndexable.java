package com.cps.endeca.eacclient;

import com.cps.endeca.service.RecordSearchConfigService;

import atg.endeca.eacclient.ScriptIndexable;
import atg.endeca.index.admin.IndexingTask;
import atg.repository.search.indexing.BulkLoaderResults;
import atg.repository.search.indexing.IndexingException;
import com.cps.endeca.service.SearchInterfaceConfigService;

/**
 * 
 * ScriptIndexable extension
 *
 */
public class CPSScriptIndexable extends ScriptIndexable {
	
	private boolean mSwitchToJSONFormat = true;

	public void setSwitchToJSONFormat(boolean pSwitchToJSONFormat) {
		mSwitchToJSONFormat = pSwitchToJSONFormat;
	}

	public boolean isSwitchToJSONFormat() {
		return mSwitchToJSONFormat;
	}

	private boolean mPromoteContentEnabled = true;

	public void setPromoteContentEnabled(boolean pPromoteContentEnabled) {
		mPromoteContentEnabled = pPromoteContentEnabled;
	}

	public boolean isPromoteContentEnabled() {
		return mPromoteContentEnabled;
	}

	private boolean mCreateSearchInterfaceConfig = true;

	public void setCreateSearchInterfaceConfig(boolean pCreateSearchInterfaceConfig) {
		mCreateSearchInterfaceConfig = pCreateSearchInterfaceConfig;
	}

	public boolean isCreateSearchInterfaceConfig() {
		return mCreateSearchInterfaceConfig;
	}

	private boolean mSearchInterfaceUpdateEnabled = true;

	public void setSearchInterfaceUpdateEnabled(boolean pSearchInterfaceUpdateEnabled) {
		mSearchInterfaceUpdateEnabled = pSearchInterfaceUpdateEnabled;
	}

	public boolean isSearchInterfaceUpdateEnabled() {
		return mSearchInterfaceUpdateEnabled;
	}

	private String mSearchInterfaceUpdateScriptName = "SearchInterfaceUpdate";

	public void setSearchInterfaceUpdateScriptName(String pSearchInterfaceUpdateScriptName) {
		mSearchInterfaceUpdateScriptName = pSearchInterfaceUpdateScriptName;
	}

	public String getSearchInterfaceUpdateScriptName() {
		return mSearchInterfaceUpdateScriptName;
	}

	/**
	 * search interface config service
	 */
	private SearchInterfaceConfigService mSearchInterfaceConfigService;

	public SearchInterfaceConfigService getSearchInterfaceConfigService() {
		return mSearchInterfaceConfigService;
	}

	public void setSearchInterfaceConfigService(SearchInterfaceConfigService pSearchInterfaceConfigService) {
		mSearchInterfaceConfigService = pSearchInterfaceConfigService;
	}

	/**
	 * record search config service
	 */
	private RecordSearchConfigService mRecordSearchConfigService;
	
	public RecordSearchConfigService getRecordSearchConfigService() {
		return mRecordSearchConfigService;
	}

	public void setRecordSearchConfigService(RecordSearchConfigService pRecordSearchConfigService) {
		mRecordSearchConfigService = pRecordSearchConfigService;
	}

	public boolean performSearchInterfaceUpdate(IndexingTask pTask) throws IndexingException {

		vlogDebug("performSearchInterfaceUpdate({0})", new Object[]{pTask});

		if (isSearchInterfaceUpdateEnabled()) {
			boolean result = runUpdateScript(getSearchInterfaceUpdateScriptName());
			if (isPromoteContentEnabled() && result) {
				result = promoteContent();
				vlogDebug("promoteContent returning {0}", new Object[]{Boolean.valueOf(result)});
			}
			vlogDebug("performSearchInterfaceUpdate returning {0}", new Object[]{Boolean.valueOf(result)});
			return result;
		} else {
			vlogWarning("Disabled but returning success for performSearchInterfaceUpdate", new Object[0]);
			return true;
		}

	}

	@Override
	public BulkLoaderResults performBaselineUpdate(IndexingTask pTask) throws IndexingException {
		
		SearchInterfaceConfigService searchInterfaceConfigService = getSearchInterfaceConfigService();
		if (isCreateSearchInterfaceConfig()) {
			if (isSwitchToJSONFormat() && searchInterfaceConfigService.generateSearchInterfaceConfig()){
				if (performSearchInterfaceUpdate(pTask)) {
					return super.performBaselineUpdate(pTask);
				} else {
					throw new IndexingException("Error creating search interface config");
				}
			} else if (getRecordSearchConfigService().generateRecordSearchConfig()) {
				getRecordSearchConfigService().copyToEndecaConfig();
				return super.performBaselineUpdate(pTask);
			} else {
				throw new IndexingException("Error creating search interface config");
			}
		} else {
			return super.performBaselineUpdate(pTask);
		}

	}
	
	public void testGenerateAndCopyAndUpdate() {

		SearchInterfaceConfigService searchInterfaceConfigService = getSearchInterfaceConfigService();
		if (isSearchInterfaceUpdateEnabled() && searchInterfaceConfigService.generateSearchInterfaceConfig()){
			searchInterfaceConfigService.copyToEndecaConfig();
			try {
				performSearchInterfaceUpdate(null);
			} catch (IndexingException e) {
				vlogError(e, "Error");
			}
		}

	}
	
}
