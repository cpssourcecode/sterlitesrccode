package com.cps.endeca.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import com.cps.commerce.endeca.cache.ReverseDimensionValueCache;

import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * This class will get the dimensionId of the passed Dgraph.Spec
 * @author R.Srinivasan
 *
 */
public class FetchDimensionIdsDroplet extends DynamoServlet {
	
	private ReverseDimensionValueCache reverseDimensionValueCache;
	
	public ReverseDimensionValueCache getReverseDimensionValueCache() {
		return reverseDimensionValueCache;
	}

	public void setReverseDimensionValueCache(ReverseDimensionValueCache reverseDimensionValueCache) {
		this.reverseDimensionValueCache = reverseDimensionValueCache;
	}

	@Override
	public void service(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		 
		String dgraphSpec=request.getParameter("dgraphSpec");
		
		DimensionValueCacheTools tools=(DimensionValueCacheTools)request.resolveGlobalName("/atg/commerce/endeca/cache/DimensionValueCacheTools/");
		
		
		
		Map<String, Object> results=reverseDimensionValueCache.getDimensionByName(dgraphSpec);
		vlogDebug("cached value is - {0}", results);
		
		
	}

}
