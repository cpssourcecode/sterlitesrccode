package com.cps.endeca;

import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by
 */
public class CPSSearchFormHandler extends GenericFormHandler {
	private static final String REFERER = "Referer";
	private static final String REDIRECT_TO  ="redirectTo";

	private String mQuestion;
	private String mSearchURL = "/search";
	private String mErrorURL = "/";
	private String mSearchParam = "?Ntt=";
	private List<String> mQuerySkipSymbols = new ArrayList<String>();
	private String mSearchParamReplacer = " ";
	private String headerCategory;

	

	public boolean handleSearch(final DynamoHttpServletRequest pRequest,
								final DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		Map<String, Object> result = new HashMap<>();
		if(!getHeaderCategory().equals("all") || !StringUtils.isEmpty(getQuestion())) {
			result.put(REDIRECT_TO, getSearchRedirectURL());
			if (AjaxUtils.isAjaxRequest(pRequest)) {
				AjaxUtils.addAjaxResponse(this, pResponse, result);
				return false;
			} else {
				return super.checkFormRedirect(getSearchRedirectURL(), getErrorURL(), pRequest, pResponse);
			}
		}else{
			String refererPage = (String) pRequest.getHeaders(REFERER).nextElement();
			return super.checkFormRedirect(refererPage, refererPage, pRequest, pResponse);
		}
	}

	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
									 DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (AjaxUtils.isAjaxRequest(pRequest)) {
			// add ajax response in handle method
			return false;
		} else {
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}
	}

	protected String getSearchRedirectURL() {
		String result = null;
		StringBuilder sb=null;

		if (!StringUtils.isBlank(getSearchURL())) {
			if(StringUtils.isNotBlank(getQuestion())){
				sb = new StringBuilder(getSearchURL());
				if(!getHeaderCategory().equals("all")) {
					sb.append(getHeaderCategory());
				}				
				sb.append(getSearchParam()).append(getParsedSearchURL(getQuestion()));
			}else{
				if(!getHeaderCategory().equals("all")) {
					sb = new StringBuilder(getHeaderCategory());					
				}				
			}			
			result = sb.toString();			
		}
		return result;
	}

	public String getParsedSearchURL(String pURL) {

		String result = pURL;
		if (!StringUtils.isBlank(pURL)) {
			for (String symbol : getQuerySkipSymbols()) {
				result = result.replace(symbol, getSearchParamReplacer());				
			}
			//SM-152 search 
			if(result.contains("”")){
				result=result.replaceAll("”","\"");
			}
		}		
		return result;
	}

	public String getErrorURL() {
		return mErrorURL;
	}

	public void setErrorURL(String pErrorURL) {
		mErrorURL = pErrorURL;
	}

	public String getSearchParamReplacer() {
		return mSearchParamReplacer;
	}

	public void setSearchParamReplacer(String pSearchParamReplacer) {
		mSearchParamReplacer = pSearchParamReplacer;
	}

	public List<String> getQuerySkipSymbols() {
		return mQuerySkipSymbols;
	}

	public void setQuerySkipSymbols(List<String> pQuerySkipSymbols) {
		mQuerySkipSymbols = pQuerySkipSymbols;
	}

	public String getSearchParam() {
		return mSearchParam;
	}

	public void setSearchParam(String pSearchParam) {
		mSearchParam = pSearchParam;
	}

	public String getSearchURL() {
		return mSearchURL;
	}

	public void setSearchURL(String pSearchURL) {
		mSearchURL = pSearchURL;
	}

	public String getQuestion() {
		return mQuestion;
	}

	public void setQuestion(String pQuestion) {
		mQuestion = pQuestion;
	}
	public String getHeaderCategory() {
		return headerCategory;
	}

	public void setHeaderCategory(String headerCategory) {
		this.headerCategory = headerCategory;
	}
}
