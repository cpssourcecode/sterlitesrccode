package com.cps.integrations;

import java.math.BigDecimal;
import java.util.Map;

import com.cps.integrations.orderstatus.CPSOrderStatusConstants;

public class CPSOrderLineDetailBean {

	private double amountExtendedPrice;
	private double amountPricePerUnit;
	private String attachmentDetail;
	private String identifierShortTerm;
	private String identifier2NdItem;
	private String identifier3RdItem;
	private BigDecimal lineNumber;
	private String lineStatusDescription;
	private String itemDescription;
	private String lineType;
	private String relatedKit;
	private int transactionQuantity;
	private boolean disabled;

	public CPSOrderLineDetailBean (){

	}

	public CPSOrderLineDetailBean(Map<String,Object> lineItems){
		if (lineItems.get(CPSOrderStatusConstants.AMOUNT_EXTENDED_PRICE) != null) {
			amountExtendedPrice = ((BigDecimal)lineItems.get(CPSOrderStatusConstants.AMOUNT_EXTENDED_PRICE)).doubleValue();
		}
		if (lineItems.get(CPSOrderStatusConstants.AMOUNT_PRICE_PER_UNIT) != null) {
			amountPricePerUnit = ((BigDecimal)lineItems.get(CPSOrderStatusConstants.AMOUNT_PRICE_PER_UNIT)).doubleValue();
		}
		if (lineItems.get(CPSOrderStatusConstants.ATTACHMENT_DETAIL) != null) {
			attachmentDetail = (String)lineItems.get(CPSOrderStatusConstants.ATTACHMENT_DETAIL);
		}
		if (lineItems.get(CPSOrderStatusConstants.IDENTIFIER_SHORT_TERM) != null) {
			identifierShortTerm = String.valueOf(lineItems.get(CPSOrderStatusConstants.IDENTIFIER_SHORT_TERM));
		}
		if (lineItems.get(CPSOrderStatusConstants.IDENTIFIER_2ND_ITEM) != null) {
			identifier2NdItem = (String)lineItems.get(CPSOrderStatusConstants.IDENTIFIER_2ND_ITEM);
		}
		if (lineItems.get(CPSOrderStatusConstants.IDENTIFIER_3RD_ITEM) != null) {
			identifier3RdItem = (String)lineItems.get(CPSOrderStatusConstants.IDENTIFIER_3RD_ITEM);
		}
		if (lineItems.get(CPSOrderStatusConstants.LINE_NUMBER) != null) {
			lineNumber = (BigDecimal)lineItems.get(CPSOrderStatusConstants.LINE_NUMBER);
		}
		if (lineItems.get(CPSOrderStatusConstants.LINE_STATUS_DESCRIPTION) != null) {
			lineStatusDescription = (String)lineItems.get(CPSOrderStatusConstants.LINE_STATUS_DESCRIPTION);
		}
		if (lineItems.get(CPSOrderStatusConstants.LINE_TYPE) != null) {
			lineType = (String)lineItems.get(CPSOrderStatusConstants.LINE_TYPE);
		}
		if (lineItems.get(CPSOrderStatusConstants.ITEM_DESCRIPTION) != null) {
			itemDescription = (String)lineItems.get(CPSOrderStatusConstants.ITEM_DESCRIPTION);
		}
		if (lineItems.get(CPSOrderStatusConstants.RELATED_KIT) != null) {
			relatedKit = ((String) lineItems.get(CPSOrderStatusConstants.RELATED_KIT)).trim();
		}
		if (lineItems.get(CPSOrderStatusConstants.TRANSACTION_QUANTITY) != null ) {
			transactionQuantity = (Integer)lineItems.get(CPSOrderStatusConstants.TRANSACTION_QUANTITY);
		}
		if (lineItems.get(CPSOrderStatusConstants.DISABLED) != null) {
			disabled = (Boolean) lineItems.get(CPSOrderStatusConstants.DISABLED);
		}
	}

	public double getAmountExtendedPrice() {
		return amountExtendedPrice;
	}

	public void setAmountExtendedPrice(double amountExtendedPrice) {
		this.amountExtendedPrice = amountExtendedPrice;
	}

	public double getAmountPricePerUnit() {
		return amountPricePerUnit;
	}

	public void setAmountPricePerUnit(double amountPricePerUnit) {
		this.amountPricePerUnit = amountPricePerUnit;
	}

	public String getAttachmentDetail() {
		return attachmentDetail;
	}

	public void setAttachmentDetail(String attachmentDetail) {
		this.attachmentDetail = attachmentDetail;
	}

	public String getIdentifierShortTerm() {
		return identifierShortTerm;
	}

	public void setIdentifierShortTerm(String identifierShortTerm) {
		this.identifierShortTerm = identifierShortTerm;
	}

	public String getIdentifier2NdItem() {
		return identifier2NdItem;
	}

	public void setIdentifier2NdItem(String identifier2NdItem) {
		this.identifier2NdItem = identifier2NdItem;
	}

	public String getIdentifier3RdItem() {
		return identifier3RdItem;
	}

	public void setIdentifier3RdItem(String identifier3RdItem) {
		this.identifier3RdItem = identifier3RdItem;
	}

	public BigDecimal getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(BigDecimal lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getLineStatusDescription() {
		return lineStatusDescription;
	}

	public void setLineStatusDescription(String lineStatusDescription) {
		this.lineStatusDescription = lineStatusDescription;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public int getTransactionQuantity() {
		return transactionQuantity;
	}

	public void setTransactionQuantity(int transactionQuantity) {
		this.transactionQuantity = transactionQuantity;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getRelatedKit() {
		return relatedKit;
	}

	public void setRelatedKit(String relatedKit) {
		this.relatedKit = relatedKit;
	}
}
