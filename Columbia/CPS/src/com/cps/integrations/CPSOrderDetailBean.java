package com.cps.integrations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cps.integrations.orderstatus.CPSOrderStatusConstants;

public class CPSOrderDetailBean {

	private Map<String,Object> rawData = new HashMap<String,Object>();
	private Date orderDate;
	private Integer numberOfItems;
	private BigDecimal itemTotalAmount;
	private BigDecimal shippingHandlingTotalAmount;
	private BigDecimal taxTotalAmount;
	private BigDecimal orderTotalAmount;
	private String shipToAddressLine1;
	private String shipToAddressLine2;
	private String shipToAddressLine3;
	private String shipToAddressLine4;
	private String shipToCity;
	private String shipToCountryCode;
	private String shipToCountyCode;
	private String shipToMailingName;
	private String shipToPostalCode;
	private String shipToStateCode;
	private List<Map<String,Object>> lineDetails;
	private List<CPSOrderLineDetailBean> lineDetailBeans = new ArrayList<CPSOrderLineDetailBean>();

	public CPSOrderDetailBean(){
	}

	public CPSOrderDetailBean(Map<String, Object> orderDetailData) {
		orderDate = (Date)orderDetailData.get(CPSOrderStatusConstants.ORDER_DATE);
		numberOfItems = (Integer)orderDetailData.get(CPSOrderStatusConstants.NUMBER_OF_ITEMS);
		itemTotalAmount = (BigDecimal)orderDetailData.get(CPSOrderStatusConstants.ITEM_TOTAL_AMOUNT);
		shippingHandlingTotalAmount = (BigDecimal)orderDetailData.get(CPSOrderStatusConstants.SHIPPING_HANDLING_TOTAL_AMOUNT);
		taxTotalAmount = (BigDecimal)orderDetailData.get(CPSOrderStatusConstants.TAX_TOTAL_AMOUNT);
		orderTotalAmount = (BigDecimal)orderDetailData.get(CPSOrderStatusConstants.ORDER_TOTAL_AMOUNT);
		shipToAddressLine1 = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_1);
		shipToAddressLine2 = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_2);
		shipToAddressLine3 = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_3);
		shipToAddressLine4 = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_4);
		shipToCity = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_CITY);
		shipToCountryCode = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_COUNTRY_CODE);
		shipToCountyCode = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_COUNTY_CODE);
		shipToMailingName = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_MAILING_NAME);
		shipToPostalCode = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_POSTAL_CODE);
		shipToStateCode = (String)orderDetailData.get(CPSOrderStatusConstants.SHIP_TO_STATE_CODE);
		lineDetails = (List<Map<String,Object>>)orderDetailData.get("lineDetails");
		if (lineDetails != null && !lineDetails.isEmpty()) {
			for (int i = 0; i < lineDetails.size(); i++){
				lineDetailBeans.add(new CPSOrderLineDetailBean(lineDetails.get(i)));
			}
		}
	}

	public void setRawData(HashMap<String,Object> rawData){
		this.rawData = rawData;
	}

	public Map<String, Object> getRawData(){
		return rawData;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(Integer numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public BigDecimal getItemTotalAmount() {
		return itemTotalAmount;
	}

	public void setItemTotalAmount(BigDecimal itemTotalAmount) {
		this.itemTotalAmount = itemTotalAmount;
	}

	public BigDecimal getShippingHandlingTotalAmount() {
		return shippingHandlingTotalAmount;
	}

	public void setShippingHandlingTotalAmount(BigDecimal shippingHandlingTotalAmount) {
		this.shippingHandlingTotalAmount = shippingHandlingTotalAmount;
	}

	public BigDecimal getTaxTotalAmount() {
		return taxTotalAmount;
	}

	public void setTaxTotalAmount(BigDecimal taxTotalAmount) {
		this.taxTotalAmount = taxTotalAmount;
	}

	public BigDecimal getOrderTotalAmount() {
		return orderTotalAmount;
	}

	public void setOrderTotalAmount(BigDecimal orderTotalAmount) {
		this.orderTotalAmount = orderTotalAmount;
	}

	public String getShipToAddressLine1() {
		return shipToAddressLine1;
	}

	public void setShipToAddressLine1(String shipToAddressLine1) {
		this.shipToAddressLine1 = shipToAddressLine1;
	}

	public String getShipToAddressLine2() {
		return shipToAddressLine2;
	}

	public void setShipToAddressLine2(String shipToAddressLine2) {
		this.shipToAddressLine2 = shipToAddressLine2;
	}

	public String getShipToAddressLine3() {
		return shipToAddressLine3;
	}

	public void setShipToAddressLine3(String shipToAddressLine3) {
		this.shipToAddressLine3 = shipToAddressLine3;
	}

	public String getShipToAddressLine4() {
		return shipToAddressLine4;
	}

	public void setShipToAddressLine4(String shipToAddressLine4) {
		this.shipToAddressLine4 = shipToAddressLine4;
	}

	public String getShipToCity() {
		return shipToCity;
	}

	public void setShipToCity(String shipToCity) {
		this.shipToCity = shipToCity;
	}

	public String getShipToCountryCode() {
		return shipToCountryCode;
	}

	public void setShipToCountryCode(String shipToCountryCode) {
		this.shipToCountryCode = shipToCountryCode;
	}

	public String getShipToCountyCode() {
		return shipToCountyCode;
	}

	public void setShipToCountyCode(String shipToCountyCode) {
		this.shipToCountyCode = shipToCountyCode;
	}

	public String getShipToMailingName() {
		return shipToMailingName;
	}

	public void setShipToMailingName(String shipToMailingName) {
		this.shipToMailingName = shipToMailingName;
	}

	public String getShipToPostalCode() {
		return shipToPostalCode;
	}

	public void setShipToPostalCode(String shipToPostalCode) {
		this.shipToPostalCode = shipToPostalCode;
	}

	public String getShipToStateCode() {
		return shipToStateCode;
	}

	public void setShipToStateCode(String shipToStateCode) {
		this.shipToStateCode = shipToStateCode;
	}

	public List<Map<String,Object>> getLineDetails() {
		return lineDetails;
	}

	public void setLineDetails(List<Map<String, Object>> lineDetails) {
		this.lineDetails = lineDetails;
	}

	public List<CPSOrderLineDetailBean> getLineDetailBeans() {
		return lineDetailBeans;
	}

	public void setLineDetailBeans(List<CPSOrderLineDetailBean> lineDetailBeans) {
		this.lineDetailBeans = lineDetailBeans;
	}

}