package com.cps.integrations;

import java.util.Map;

import com.cps.integrations.orderstatus.CPSDummyOrderStatusClient;
import com.cps.integrations.orderstatus.CPSOrderStatusClient;

import atg.nucleus.GenericService;

/**
 * CPSOrderStatusConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author David Mednikov
 * 
 */
public class CPSOrderStatusConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/integrations/orderstatus/CPSOrderStatusConnectionManager";

	/** Client property */
	private CPSOrderStatusClient mClient;

	/**
	 * @return
	 */
	public CPSOrderStatusClient getClient() {
		return mClient;
	}

	/**
	 * @param pClient
	 */
	public void setClient(CPSOrderStatusClient pClient) {
		mClient = pClient;
	}

	/** Dummy Client property */
	private CPSDummyOrderStatusClient mDummyClient;

	/**
	 * @return
	 */
	public CPSDummyOrderStatusClient getDummyClient() {
		return mDummyClient;
	}

	/**
	 * @param pDummyClient
	 */
	public void setDummyClient(CPSDummyOrderStatusClient pDummyClient) {
		mDummyClient = pDummyClient;
	}

	public CPSOrderDetailBean requestOrderStatus(int pOrderNumber) throws Exception {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrderStatus").append(" - start").toString());
		}

		CPSOrderDetailBean orderDetail = null;

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append("Find orders starting with orderNumber:").append(pOrderNumber).toString());
		}

		Map<String, Object> orderDetailData = null;
		// call to client if enabled
		if (getClient().getWebServiceConfig().getWebServiceEnabled()) {
			if (isLoggingDebug()) {
				logDebug("trying service call- " + pOrderNumber);
			}
			orderDetailData = getClient().getOrderStatus(pOrderNumber);
			if (isLoggingDebug()) {
				logDebug("orderDetailData- " + orderDetailData);
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("Service is not enabled, use dummy client to mock data.");
			}
			orderDetailData = getDummyClient().getOrderStatus(pOrderNumber);
		}

		if (orderDetailData != null && orderDetailData.size() > 0){
			if (isLoggingDebug()) {
				logDebug("Convert order detail data to order detail bean.");
			}
			orderDetail = new CPSOrderDetailBean(orderDetailData);
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Returning order detail bean: ").append(orderDetail.toString()).toString());
			}
		} else {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("No order detail available for orderNumber: ").append(pOrderNumber).toString());
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrderStatus").append(" - exit").toString());
		}
		return orderDetail;
	}
	
	
	/**
	 * 
	 */
	public void testOrderStatus(){

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(".testOrderStatus").append(" - start").toString());
		}
		try {
			requestOrderStatus(102585);
		} catch (Exception e){
			if (isLoggingError()) {
				logError(e);
			}
		}
		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(".testOrderStatus").append(" - exit").toString());
		}

	}
	
}