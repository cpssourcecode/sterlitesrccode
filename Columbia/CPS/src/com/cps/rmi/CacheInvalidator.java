package com.cps.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

import atg.repository.Repository;

public interface CacheInvalidator extends Remote {
    public void invalidateCache(String repositoryPath) throws RemoteException;

    public void invalidateCache(Repository repository) throws RemoteException;

}
