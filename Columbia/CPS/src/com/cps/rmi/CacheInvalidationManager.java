package com.cps.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import atg.adapter.gsa.invalidator.GSAInvalidator;
import atg.deployment.server.AgentStatusCache;
import atg.nucleus.GenericService;

public class CacheInvalidationManager extends GenericService {

    private static final String AGENT_TRANSPORT_PATH = "/atg/epub/AgentTransport";
    private AgentStatusCache agentStatusCache;
    private String cacheInvalidatorPath;

    public String getCacheInvalidatorPath() {
        return cacheInvalidatorPath;
    }

    public void setCacheInvalidatorPath(String cacheInvalidatorPath) {
        this.cacheInvalidatorPath = cacheInvalidatorPath;
    }

    public AgentStatusCache getAgentStatusCache() {
        return agentStatusCache;
    }

    public void setAgentStatusCache(AgentStatusCache agentStatusCache) {
        this.agentStatusCache = agentStatusCache;
    }

    public void invalidateCache(String repositoryPath) {
        vlogDebug("invalidate cache called with :: {0}", repositoryPath);
        String[] cachedAgents = getAgentStatusCache().getCachedAgents();
        if (cachedAgents != null && cachedAgents.length > 0) {
            for (String agentTransportURI : cachedAgents) {
                String remoteURL = agentTransportURI.replaceAll(AGENT_TRANSPORT_PATH, getCacheInvalidatorPath());
                CacheInvalidator invoker = null;
                try {
                    vlogDebug("before naming lookup :: remoteURL :: {0}", remoteURL);
                    invoker = (CacheInvalidator) Naming.lookup(remoteURL);
                } catch (MalformedURLException | RemoteException | NotBoundException e) {
                    vlogDebug("Is the remote host & port properly configured?");
                    vlogDebug("Errors occurred doing a RMI lookup for {0}", remoteURL);
                }
                if (invoker != null) {
                    try {
                        invoker.invalidateCache(repositoryPath);
                    } catch (RemoteException e) {
                        vlogDebug("Remote exception invoking cache invalidator");
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        CacheInvalidator invoker = null;
        try {
            System.out.println("before naming lookup :: remoteURL " + "rmi://localhost:8860/cps/rmi/CacheInvalidator");
            invoker = (CacheInvalidator) Naming.lookup("rmi://localhost:8860/cps/rmi/CacheInvalidator");
        } catch (MalformedURLException | RemoteException | NotBoundException e) {
            System.err.println("Is the remote host & port properly configured?");
            System.err.println("Errors occurred doing a RMI lookup for rmi://localhost:8860/cps/rmi/CacheInvalidator");
        }
        if (invoker != null) {
            try {
                invoker.invalidateCache("/atg/commerce/pricing/priceLists/PriceLists");
            } catch (RemoteException e) {
                System.err.println("Remote exception invoking cache invalidator");
            }
        }
        GSAInvalidator invalidator = null;
        try {
            System.out.println("before naming lookup :: remoteURL " + "rmi://localhost:8860/atg/dynamo/service/GSAInvalidatorService");
            invalidator = (GSAInvalidator) Naming.lookup("rmi://localhost:8860/atg/dynamo/service/GSAInvalidatorService");
        } catch (MalformedURLException | RemoteException | NotBoundException e) {
            System.err.println("Is the remote host & port properly configured?");
            System.err.println("Errors occurred doing a RMI lookup for rmi://localhost:8860/atg/dynamo/service/GSAInvalidatorService");
        }
        if (invalidator != null) {
            try {
                invalidator.invalidate("/atg/commerce/pricing/priceLists/PriceLists", null, null);
            } catch (RemoteException e) {
                System.err.println("Remote exception invoking cache invalidator");
            }
        }
    }
}
