package com.cps.rmi;

import java.rmi.RemoteException;

import atg.adapter.gsa.GSARepository;
import atg.nucleus.GenericRMIService;
import atg.nucleus.Nucleus;
import atg.repository.Repository;

public class CacheInvalidatorImpl extends GenericRMIService implements CacheInvalidator {

    public CacheInvalidatorImpl() throws RemoteException {
        super();
    }

    @Override
    public void invalidateCache(Repository repository) throws RemoteException {
        vlogDebug("Entering invalidate cache with repository :: {0}", repository.getRepositoryName());
        ((GSARepository) repository).invalidateCaches();
        vlogDebug("Exiting invalidate cache with repository :: {0}", repository.getRepositoryName());
    }

    @Override
    public void invalidateCache(String repositoryPath) throws RemoteException {
        invalidateCache((Repository) Nucleus.getGlobalNucleus().resolveName(repositoryPath));
    }
}
