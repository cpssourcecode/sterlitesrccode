package com.cps.commerce.packingslip;

import java.util.Calendar;

/**
 * CPSPackingSlipBean
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 *
 */
public class CPSPackingSlipBean {

	/**  Packing Slip Link property */
	private String mPackingSlipLink;
	
	/**  Packing Slip Number property */
	private String mPackingSlipNumber;
	
	/**  Packing Slip CS property */
	private String mPackingSlipCS;
	
	/**  Packing Slip Date property */
	private Calendar mPackingSlipDate;
	/**
	 * @return
	 */
	public String getPackingSlipLink(){
		return mPackingSlipLink;
	}
	/**
	 * @param pPackingSlipLink
	 */
	public void setPackingSlipLink(String pPackingSlipLink){
		mPackingSlipLink = pPackingSlipLink;
	}
	/**
	 * @return
	 */
	public String getPackingSlipNumber(){
		return mPackingSlipNumber;
	}
	/**
	 * @param pPackingSlipNumber
	 */
	public void setPackingSlipNumber(String pPackingSlipNumber){
		mPackingSlipNumber = pPackingSlipNumber;
	}
	/**
	 * @return
	 */
	public String getPackingSlipCS(){
		return mPackingSlipCS;
	}
	/**
	 * @param pPackingSlipCS
	 */
	public void setPackingSlipCS(String pPackingSlipCS){
		mPackingSlipCS = pPackingSlipCS;
	}
	/**
	 * @return
	 */
	public Calendar getPackingSlipDate(){
		return mPackingSlipDate;
	}
	/**
	 * @param pPackingSlipDate
	 */
	public void setPackingSlipDate(Calendar pPackingSlipDate){
		mPackingSlipDate = pPackingSlipDate;
	}

	public String toString() {
		return (new StringBuilder()).append("CPS Packing Slip Bean:").
				append("\n").append("Packing Slip Number:")
				.append(getPackingSlipNumber())
				.append("\n").append("Packing Slip Link:")
				.append(getPackingSlipLink())
				.append("\n").append("Packing Slip CS:")
				.append(getPackingSlipCS())
				.append("\n").append("Packing Slip Date:")
				.append(getPackingSlipDate())
				.toString();
	}
}
