package com.cps.commerce.packingslip;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

import com.cps.integrations.packingslip.CPSDummyPackingSlipClient;
import com.cps.integrations.packingslip.CPSPackingSlipClient;

/**
 * CPSPackingSlipConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 * 
 */
public class CPSPackingSlipConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/commerce/packingslip/CPSPackingSlipConnectionManager";

	private boolean mTest;

	public boolean isTest() {
		return mTest;
	}

	public void setTest(boolean pTest) {
		mTest = pTest;
	}

	/** Client property */
	private CPSPackingSlipClient mClient;

	/**
	 * @return
	 */
	public CPSPackingSlipClient getClient() {
		return mClient;
	}

	/**
	 * @param pClient
	 */
	public void setClient(CPSPackingSlipClient pClient) {
		mClient = pClient;
	}
	
	/** Dummy Client property */
	private CPSDummyPackingSlipClient mDummyClient;

	/**
	 * @return
	 */
	public CPSDummyPackingSlipClient getDummyClient() {
		return mDummyClient;
	}

	/**
	 * @param pDummyClient
	 */
	public void setDummyClient(CPSDummyPackingSlipClient pDummyClient) {
		mDummyClient = pDummyClient;
	}

	/**
	 * This method will request packing slips with the ws packing slip client based on an
	 * start and end date and CS.
	 *
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> requestPackingSlips(String pCustomerNumber,
											   Calendar pStartDate,
											   Calendar pEndDate,
											   String pOrderNumber,
											   String pPONumber,
											   List<String> pShipTo,
											   int pPageSize,
											   int pStartIndex,
											   String pSortField,
											   String pSortOption) throws Exception {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestPackingSlips").append(" - start").toString());
		}

		Map<String, Object> result = new HashMap<String, Object>();

		Calendar startDate = pStartDate;
		Calendar endDate = pEndDate;

		if (!StringUtils.isBlank(pOrderNumber) || !StringUtils.isBlank(pPONumber)) {
			startDate = null;
			endDate = null;
		}

		if (getClient().getWebServiceConfig().getWebServiceEnabled()) {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Service is enabled, use client to get data.").toString());
			}
			if (isTest()) {
				List<String> shipto = new ArrayList<String>();
				shipto.add("110145");
				result = getClient().requestPackingSlips("110144", startDate, endDate, pOrderNumber, pPONumber, shipto, pPageSize, pStartIndex, pSortField, pSortOption);
				//result = getClient().requestPackingSlips("101010", 1422856800000l, 1422856800000l, "1098500", "50612", shipto, pPageSize, pStartIndex, pSortField, pSortOption);
			} else {
				result = getClient().requestPackingSlips(pCustomerNumber, startDate, endDate, pOrderNumber, pPONumber, pShipTo, pPageSize, pStartIndex, pSortField, pSortOption);
			}
		} else {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Service is NOT enabled!").toString());
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestPackingSlips").append(" - exit").toString());
		}
		return result;

	}

}
