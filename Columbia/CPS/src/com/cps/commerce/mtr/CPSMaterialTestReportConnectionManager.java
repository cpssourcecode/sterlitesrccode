package com.cps.commerce.mtr;

import java.util.Map;

import atg.nucleus.GenericService;

import com.cps.integrations.mtr.CPSMaterialTestReportClient;

/**
 * CPSMaterialTestReportConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Steve Neverov
 * 
 */
public class CPSMaterialTestReportConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/commerce/mtr/CPSMaterialTestReportConnectionManager";

	/** Client property */
	private CPSMaterialTestReportClient mClient;

	/**
	 * @return
	 */
	public CPSMaterialTestReportClient getClient() {
		return mClient;
	}

	/**
	 * @param pClient
	 */
	public void setClient(CPSMaterialTestReportClient pClient) {
		mClient = pClient;
	}

	private boolean mTest;

	public boolean isTest() {
		return mTest;
	}

	public void setTest(boolean pTest) {
		mTest = pTest;
	}

	/**
	 * This method will request a material test report with the client.
	 */
	public Map<String, Object> requestMTRs(String pCustomerNumber,
										   	   Map<String, String> pHeatNumbers,
											   int pPageSize,
											   int pStartIndex,
											   String pSortField,
											   String pSortOption) throws Exception {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestMTRs").append(" - start").toString());
		}

		Map<String, Object> reply = null;

		// call to client if enabled
		if (getClient().getWebServiceConfig().getWebServiceEnabled()) {
			if (isLoggingDebug())
				logDebug(new StringBuilder().append(
						"Service is enabled, use client to get data.")
						.toString());
			String customerNumber = pCustomerNumber;

			if (isTest()) {
				customerNumber = "101010";
			}

			reply = getClient().requestMTRs(customerNumber, pHeatNumbers, pPageSize, pStartIndex, pSortField, pSortOption);

		} else {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Service is NOT enabled!").toString());
			}
		}

		if (isLoggingDebug())
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestMTRs").append(" - exit").toString());

		return reply;

	}

}
