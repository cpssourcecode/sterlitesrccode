package com.cps.commerce.mtr;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.cps.mtr.MTRInfo;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;
import com.cps.util.CPSSessionBean;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

/**
 * @author Steve Neverov
 */
public class CPSMaterialTestReportFormHandler extends GenericFormHandler {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/commerce/mtr/CPSMaterialTestReportFormHandler";

    private CPSProfileTools mProfileTools;

    private Profile mProfile;

    public Profile getProfile() {
        return mProfile;
    }

    public void setProfile(Profile pProfile) {
        mProfile = pProfile;
    }

    /**
     * connection manager for the client
     */
    private CPSMaterialTestReportConnectionManager mConnectionManager;

    public void setConnectionManager(CPSMaterialTestReportConnectionManager pConnectionManager) {
        mConnectionManager = pConnectionManager;
    }

    public CPSMaterialTestReportConnectionManager getConnectionManager() {
        return mConnectionManager;
    }

    /**
     * heat numbers
     */
    private Map<String, String> mValues = new HashMap<String, String>();

    public void setValues(Map<String, String> pValues) {
        mValues = pValues;
    }

    public Map<String, String> getValues() {
        return mValues;
    }

    private String[] mRemoveMTRIds;

    public void setRemoveMTRIds(String[] pRemoveMTRIds) {
        mRemoveMTRIds = pRemoveMTRIds;
    }

    public String[] getRemoveMTRIds() {
        return mRemoveMTRIds;
    }

    private CPSSessionBean mSessionBean;

    public void setSessionBean(CPSSessionBean pSessionBean) {
        mSessionBean = pSessionBean;
    }

    public CPSSessionBean getSessionBean() {
        return mSessionBean;
    }

    private RepeatingRequestMonitor mRepeatingRequestMonitor;

    public RepeatingRequestMonitor getRepeatingRequestMonitor() {
        return mRepeatingRequestMonitor;
    }

    public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
        mRepeatingRequestMonitor = pRepeatingRequestMonitor;
    }

    private void createError(String pMsg, String pPty, DynamoHttpServletRequest pRequest) {
        CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, pMsg, pRequest.getLocale(), pPty);
    }

    /**
     * @param pRequest
     * @param pResponse
     * @return
     * @throws Exception
     */
    public boolean handleRequestMTR(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSMaterialTestReportFormHandler.handleRequestMTR";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append(COMPONENT_NAME).append(".handleRequestMTR").append(" - start").toString());
                }

                RepositoryItem organization = getProfileTools().getParentOrganization(getProfile());

                if (organization != null) {

                    preHandleRequestMTR(pRequest, pResponse);

                    if (!getFormError()) {

                        String customerNumber = organization.getRepositoryId();

                        try {

                            getSessionBean().getFoundHeatNums().clear();
                            getSessionBean().getMaterialReports().clear();

                            Map<String, Object> result = getConnectionManager().requestMTRs(customerNumber, getValues(), 100000, 0, null, null);

                            if (result != null && result.size() > 0) {
                                List<MTRInfo> mtrs = (List<MTRInfo>) result.get("mtrs");

                                // need to update them with found heatNum
                                getSessionBean().setFoundHeatNums(processMTRs(getValues(), mtrs));

                                getSessionBean().getMaterialReports().addAll(mtrs);
                                getSessionBean().setMTRTotal(getSessionBean().getMaterialReports().size());
                            }

                        } catch (Exception e) {
                            if (isLoggingError()) {
                                logError(e);
                            }
                        }

                        vlogDebug("MTRs: " + getSessionBean().getMaterialReports());

                    }

                    if (isLoggingDebug()) {
                        logDebug(new StringBuilder().append(COMPONENT_NAME).append(".handleRequestMTR").append(" - end").toString());
                    }

                } else {
                    createError(CPSErrorCodes.ERR_INVOICES_MISSING_ORG, null, pRequest);
                }

                getProfileTools().checkErrors(pResponse, this);

            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        return false;

    }

    private String getContainedCaseInsensitiveValue(Set<String> pFields, String pValue) {

        String result = null;
        if (pFields.contains(pValue)) {
            result = pValue;
        } else {
            for (String sValue : pFields) {
                if (pValue.equalsIgnoreCase(sValue)) {
                    result = sValue;
                    break;
                }
            }
        }
        return result;

    }

    private Set<String> processMTRs(Map<String, String> pValues, List<MTRInfo> pMTRs) {

        Set<String> foundHeatNums = new HashSet<String>();
        for (String key : pValues.keySet()) {
            if (!StringUtils.isBlank(pValues.get(key))) {
                for (MTRInfo info : pMTRs) {
                    String value = getContainedCaseInsensitiveValue(info.getFields(), pValues.get(key));
                    if (value != null) {
                        foundHeatNums.add(key);
                        // info.getHeatFields().add(key);
                        if (StringUtils.isBlank(info.getHeatNum())) {
                            info.setHeatNum(value);
                        }
                    }
                }
            }
        }
        return foundHeatNums;

    }

    public void preHandleRequestMTR(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        if (getValues() == null) {
            createError(CPSErrorCodes.ERR_MTR_NO_HEAT_NUMBER, CPSConstants.HEAT_NUMBER, pRequest);
        }

    }

    public void preRemoveMTRs(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        if (StringUtils.isBlank(getValues().get("removeMTRIds"))) {
            createError(CPSErrorCodes.ERR_MTR_NO_HEAT_NUMBER, CPSConstants.HEAT_NUMBER, pRequest);
        } else {
            setRemoveMTRIds(getValues().get("removeMTRIds").split(","));
        }

    }

    public boolean handleRemoveMTRs(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSMaterialTestReportFormHandler.handleRemoveMTRs";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append(COMPONENT_NAME).append(".handleRemoveMTRs").append(" - start").toString());
                }

                preRemoveMTRs(pRequest, pResponse);

                if (!getFormError()) {
                    Set<MTRInfo> infos = getSessionBean().getMaterialReports();
                    for (String id : getRemoveMTRIds()) {
                        for (Iterator<MTRInfo> iterator = infos.iterator(); iterator.hasNext();) {
                            MTRInfo info = iterator.next();
                            if (id.equals(info.getId())) {
                                iterator.remove();
                            }
                        }
                    }
                    getSessionBean().setMaterialReports(infos);
                    getSessionBean().setMTRTotal(infos.size());

                }

                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append(COMPONENT_NAME).append(".handleRemoveMTRs").append(" - end").toString());
                }

                getProfileTools().checkErrors(pResponse, this);

            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        return false;

    }

    // ------------------------------------------------------------------------------------------------------------------

    public CPSProfileTools getProfileTools() {
        return mProfileTools;
    }

    public void setProfileTools(CPSProfileTools pProfileTools) {
        mProfileTools = pProfileTools;
    }

}
