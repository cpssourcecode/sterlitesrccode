package com.cps.commerce.profile;

import java.io.IOException;

import javax.servlet.ServletException;

import com.cps.userprofiling.CPSProfileRequest;
import com.cps.util.CPSConstants;
import com.cps.util.security.CriptoUtils;

import atg.adapter.version.CurrentVersionItem;
import atg.beans.PropertyNotFoundException;
import atg.commerce.profile.CommerceProfileRequestServlet;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileRequest;

/**
 * @author Andy Porter
 */
public class CPSProfileRequestServlet extends CommerceProfileRequestServlet {

	private boolean mEnableLoginFromWithlistEmail;

	@Override
	public void passRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if(isEnableLoginFromWithlistEmail()) {
			checkWishlistLogin(pRequest, pResponse);
		}
		final Profile profile = this.getProfile(pRequest, pResponse);
		if (null != profile && null != SiteContextManager.getCurrentSite()) {
			RepositoryItem defaultCatalog = (RepositoryItem) SiteContextManager.getCurrentSite()
					.getPropertyValue("defaultCatalog");
			if (!(defaultCatalog instanceof CurrentVersionItem)) {
				profile.setPropertyValue("catalog", defaultCatalog);
			}
		}
		super.passRequest(pRequest, pResponse);
	}

	private void checkWishlistLogin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		try {
			Profile profile = getProfile(pRequest, pResponse);
			if (getProfileTools().getSecurityStatus(profile) < getProfileTools().getPropertyManager().getSecurityStatusLogin()) {
				String wishListIdParam = pRequest.getParameter(CPSConstants.WISHLIST_ID_PARAM);
				String profileIdParam = pRequest.getParameter(CPSConstants.PROFILE_ID_PARAM);
				if (StringUtils.isNotBlank(wishListIdParam) && StringUtils.isNotBlank(profileIdParam)) {
					String wishListId = decrypt(wishListIdParam);
					String profileId = decrypt(profileIdParam);
					if (StringUtils.isNotBlank(wishListId) && StringUtils.isNotBlank(profileId)) {
						ProfileRequest profileRequest = getProfileRequest(pRequest, pResponse);
						if(profileRequest instanceof CPSProfileRequest) {
							((CPSProfileRequest) profileRequest).setRequestSource(1);
						}
						boolean profileLoaded = getProfileTools().locateUserFromId((String) profileId, profile);
						if (profileLoaded) {
							boolean initProfileSuccess = initProfileAfterAutoLogin(profile,
									getProfileRequest(pRequest, pResponse), pRequest, pResponse);
							if (initProfileSuccess) {
								initUserAfterAutoLogin(profile, pRequest, pResponse);
								getProfileTools().setSecurityStatus(profile,
										getProfileTools().getPropertyManager().getSecurityStatusLogin());
							}
						}
					}
				}
			}
		} catch (PropertyNotFoundException | RepositoryException e) {
			vlogError(e, "Error in CPSProfileRequestServlet.checkWishlistLogin");
		}
	}

	private String decrypt(String pParam) {
		String result = null;
		try {
			result = CriptoUtils.aesDecrypt(pParam);
		} catch (Exception e) {
			vlogError(e, "Error in CPSProfileRequest");
		}
		return result;
	}

	public boolean isEnableLoginFromWithlistEmail() {
		return mEnableLoginFromWithlistEmail;
	}

	public void setEnableLoginFromWithlistEmail(boolean pEnableLoginFromWithlistEmail) {
		mEnableLoginFromWithlistEmail = pEnableLoginFromWithlistEmail;
	}

}
