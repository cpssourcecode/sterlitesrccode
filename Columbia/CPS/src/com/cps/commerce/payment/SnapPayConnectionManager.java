package com.cps.commerce.payment;

import atg.commerce.order.Order;
import atg.commerce.pricing.OrderPriceInfo;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.userprofiling.Profile;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.integrations.snappay.SnapPayClient;
import com.cps.multisite.CPSSiteURLManager;
import com.cps.util.Retry;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import static com.cps.integrations.snappay.SnapPayConstants.AMOUNT;
import static com.cps.integrations.snappay.SnapPayConstants.CUSTOMER_ID;
import static com.cps.integrations.snappay.SnapPayConstants.INVOICE_ID;
import static com.cps.integrations.snappay.SnapPayConstants.ORDER_ID;
import static com.cps.integrations.snappay.SnapPayConstants.REDIRECT_URL;
import static com.cps.integrations.snappay.SnapPayConstants.REQUEST_NUMBER;
import static com.cps.integrations.snappay.SnapPayConstants.STATUS;
import static com.cps.integrations.snappay.SnapPayConstants.USER_ID;
import static com.cps.integrations.snappay.SnapPayConstants.YES;
import static com.cps.integrations.snappay.SnapPayConstants.EMAIL;

/**
 * SnapPayConnectionManager
 * 
 * @author Steve Neverov
 * 
 */
public class SnapPayConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/commerce/payment/SnapPayConnectionManager";
	private final static int REQUEST_NUMBER_MAX_LENGTH = 25;

	private boolean mTest;

	public boolean isTest() {
		return mTest;
	}

	public void setTest(boolean pTest) {
		mTest = pTest;
	}

	/** Client property */
	private SnapPayClient mClient;

	public SnapPayClient getClient() {
		return mClient;
	}

	public void setClient(SnapPayClient pClient) {
		mClient = pClient;
	}

	/** Order Manager property */
	private CPSOrderManager mOrderManager;

	public CPSOrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(CPSOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	private int mOrderUpdateRetryNumber = 5;

	public int getOrderUpdateRetryNumber() {
		return mOrderUpdateRetryNumber;
	}

	public void setOrderUpdateRetryNumber(int pOrderUpdateRetryNumber) {
		mOrderUpdateRetryNumber = pOrderUpdateRetryNumber;
	}

	private int mOrderUpdateRetryWaitTime = 2000;

	public int getOrderUpdateRetryWaitTime() {
		return mOrderUpdateRetryWaitTime;
	}

	public void setOrderUpdateRetryWaitTime(int pOrderUpdateRetryWaitTime) {
		mOrderUpdateRetryWaitTime = pOrderUpdateRetryWaitTime;
	}

	private String mMaintainCreditCardsRedirectUrl;

	public String getMaintainCreditCardsRedirectUrl() {
		return mMaintainCreditCardsRedirectUrl;
	}

	public void setMaintainCreditCardsRedirectUrl(String pMaintainCreditCardsRedirectUrl) {
		mMaintainCreditCardsRedirectUrl = pMaintainCreditCardsRedirectUrl;
	}

	private String mCheckoutRedirectUrl;

	public String getCheckoutRedirectUrl() {
		return mCheckoutRedirectUrl;
	}

	public void setCheckoutRedirectUrl(String pCheckoutRedirectUrl) {
		mCheckoutRedirectUrl = pCheckoutRedirectUrl;
	}

	private String mCancelUrl;

	public String getCancelUrl() {
		return mCancelUrl;
	}

	public void setCancelUrl(String pCancelUrl) {
		mCancelUrl = pCancelUrl;
	}

	private String mConfirmationUrl;

	public String getConfirmationUrl() {
		return mConfirmationUrl;
	}

	public void setConfirmationUrl(String pConfirmationUrl) {
		mConfirmationUrl = pConfirmationUrl;
	}

	private CPSSiteURLManager mSiteURLManager;

	public CPSSiteURLManager getSiteURLManager() {
		return mSiteURLManager;
	}

	public void setSiteURLManager(CPSSiteURLManager pSiteURLManager) {
		mSiteURLManager = pSiteURLManager;
	}

	public String setTransactionDetails(Order pOrder, Profile pProfile, boolean pMaintainCreditCards) {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".setTransactionDetails").append(" - start").toString());

		String requestNumber = null;
		CPSOrderImpl order = (CPSOrderImpl)pOrder;
		Map<String, String> params = new HashMap<String, String>();
		Map<String, String> response = new HashMap<String, String>();
		String customerId = null;
		if (pProfile.isTransient()) {
			//todo for test guest checkout use profile id temporary until we get default id
			customerId = pProfile.getRepositoryId();
		} else {
			customerId = getOrderManager().getProfileTools().getCustomerId(pProfile);
		}
		if (!StringUtils.isBlank(customerId)) {
			requestNumber = getOrderManager().getUniqueRequestNumber();
			if (!StringUtils.isBlank(requestNumber)) {
				if (requestNumber.length() > REQUEST_NUMBER_MAX_LENGTH) {
					requestNumber = requestNumber.substring(0, REQUEST_NUMBER_MAX_LENGTH - 1);
				}
				params.put(REQUEST_NUMBER, requestNumber);
				params.put(CUSTOMER_ID, customerId);
				params.put(USER_ID, (String)pProfile.getPropertyValue(EMAIL));
				String siteUrl = getSiteURLManager().getSiteUrl();
				if (!StringUtils.isBlank(siteUrl)) {
					String redirectUrl = new StringBuilder(siteUrl).append(pMaintainCreditCards ? getMaintainCreditCardsRedirectUrl()
							: getCheckoutRedirectUrl() + requestNumber).toString();
					params.put(REDIRECT_URL, redirectUrl);
				}

				params.put(ORDER_ID, order.getId());
				params.put(INVOICE_ID, "");

				double amount = 0.0;
				if (!pMaintainCreditCards) {
					OrderPriceInfo opi = order.getPriceInfo();
					if (opi != null) {
						amount = opi.getAmount();
					}
				}
				DecimalFormat df = new DecimalFormat("#.00");
				params.put(AMOUNT, df.format(amount));

				response = getClient().setTransactionDetails(params, pMaintainCreditCards);
			}

		}

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".setTransactionDetails").append(" - exit").toString());

		return YES.equals(response.get(STATUS)) ? requestNumber : null;

	}

	public boolean externalOrderUpdate(Order pOrder) {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".externalOrderUpdate").append(" - start").toString());

		if (isTest()) {
			return true;
		}
		boolean success = false;
		CPSOrderImpl order = (CPSOrderImpl)pOrder;
		Retry retry = new Retry(getOrderUpdateRetryNumber(), getOrderUpdateRetryWaitTime());
		while (retry.shouldRetry()) {
			vlogDebug("Making {0} attempt", getOrderUpdateRetryNumber() + 1 - retry.getRetriesNumberLeft());
			success = getClient().externalOrderUpdate(order.getRequestNumber(), order.getJdeOrderNumber());
			if (success) {
				retry.cancel();
			} else {
				retry.errorOccurred();
			}
		}

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".externalOrderUpdate").append(" - exit").toString());

		return success;

	}

	public void testSetTransactionDetails() {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".testSetTransactionDetails").append(" - start").toString());

		Map<String, String> params = new HashMap<String, String>();
		params.put(REQUEST_NUMBER, getOrderManager().getUniqueRequestNumber());
		params.put(CUSTOMER_ID, "123456");
		params.put(USER_ID, "test@testing.com");
		params.put(REDIRECT_URL, "http://local.columbiapipe.com");

		params.put(ORDER_ID, "7777777");
		params.put(INVOICE_ID, "555");
		params.put(AMOUNT, "130.00");

		getClient().setTransactionDetails(params, false);

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".testSetTransactionDetails").append(" - exit").toString());

	}

}
