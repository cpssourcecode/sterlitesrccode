package com.cps.commerce.order.purchase;

import java.util.List;
import java.util.Map;

public class CPSLastProductsAddedToCart {
    private Map<String, Long> products;
    private List<String> relatedProducts;

    public CPSLastProductsAddedToCart() {
    }

    public Map<String, Long> getProducts() {
        return products;
    }

    public void setProducts(Map<String, Long> products) {
        this.products = products;
    }

    public List<String> getRelatedProducts() {
        return relatedProducts;
    }

    public void setRelatedProducts(List<String> relatedProducts) {
        this.relatedProducts = relatedProducts;
    }
}
