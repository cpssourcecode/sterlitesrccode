package com.cps.commerce.order.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.util.CPSConstants;
import com.cps.util.CPSMessageUtils;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

public class CPSOrderFormHandler extends GenericFormHandler {

	private CPSOrderManager orderManager;
	private Map<String, String> jdeOrdersMap;
	private String errorURL;
	private String successURL;
	private String rqlQueryAllJDEOrderNumbers;

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleUpdateJDEOrderNumber(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		vlogInfo("Called handle method - handleUpdateJDEOrderNumber");
		Map<String, String> jdeOrderNumberMap = getJdeOrdersMap();
		preUpdateJDEOrderNumber(jdeOrderNumberMap, pRequest, pResponse);
		if(!getFormError()){
		if (jdeOrderNumberMap != null && jdeOrderNumberMap.size() > 0) {
			boolean isJDEOrderNumberExist = false;
			for (Map.Entry<String, String> entry : jdeOrderNumberMap.entrySet()) {
				vlogDebug("Order Id and JDE order number {0} - {1}", entry.getKey(), entry.getValue());
				if (StringUtils.isNotBlank(entry.getValue().trim())) {
					try {
						CPSOrderImpl order = (CPSOrderImpl) getOrderManager().loadOrder(entry.getKey());
						if(StringUtils.isBlank(order.getJdeOrderNumber())){
							getOrderManager().updateOrderWithJDEOrderNumber(order, order.getProfileId(), entry.getValue());
						}else{
							isJDEOrderNumberExist = true;
						}
					} catch (CommerceException e) {
						e.printStackTrace();
					}
				}
			}
			if(isJDEOrderNumberExist){
				CPSMessageUtils.addFormException(this, "errJdeOrderNumberExist", pRequest.getLocale(), "jdeOrderNumber");
			}
		}
		}
		return super.checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest, pResponse);
	}

	/**
	 * 
	 * @param pJdeOrderItems
	 * @param pRequest
	 * @param pResponse
	 */
	private void preUpdateJDEOrderNumber(Map<String, String> pJdeOrderItems, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		List<String> jdeNumbers = new ArrayList<String>();
		if (pJdeOrderItems != null && pJdeOrderItems.size() > 0) {
			String jdeOrderNumber = null;
			List<String> jdeOrderNumbers = new ArrayList<String>();
			for (Map.Entry<String, String> entry : pJdeOrderItems.entrySet()) {
				jdeOrderNumber = entry.getValue().trim();
				jdeNumbers.add(jdeOrderNumber);
				if (StringUtils.isNotBlank(jdeOrderNumber) && !jdeOrderNumber.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
					CPSMessageUtils.addFormException(this, "errRegInvJdeOrderNumber", pRequest.getLocale(), "jdeOrderNumber");
				}
				if(StringUtils.isBlank(jdeOrderNumber)){
					jdeOrderNumbers.add(jdeOrderNumber);
				}
			}
			if (jdeOrderNumbers.size() == pJdeOrderItems.size()) {
				CPSMessageUtils.addFormException(this, "emptyJdeOrderNumber", pRequest.getLocale(), "jdeOrderNumber");
			}
		} else {
			vlogInfo("There is no items to update.");
		}
		try {
			List<Order> orders = getOrderManager().retrieveOrders(null, getRqlQueryAllJDEOrderNumbers());
			if(orders != null && orders.size()>0){
				List<String> jdeOrderNumbers = new ArrayList<>();
				for (Order order : orders) {
					CPSOrderImpl orderImpl = (CPSOrderImpl) order;
					jdeOrderNumbers.add(orderImpl.getJdeOrderNumber());
				}
				for (String jdeNumber : jdeOrderNumbers) {
					if(jdeNumbers != null && jdeNumbers.contains(jdeNumber)){
						vlogDebug("The JDE order number - {0} is already exist", jdeNumber);
						CPSMessageUtils.addFormException(this, "errJdeOrderNumberExist", pRequest.getLocale(), "jdeOrderNumber");
						break;
					}
				}
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the jdeOrdersMap
	 */
	public Map<String, String> getJdeOrdersMap() {
		return jdeOrdersMap;
	}

	/**
	 * @param jdeOrdersMap
	 *            the jdeOrdersMap to set
	 */
	public void setJdeOrdersMap(Map<String, String> jdeOrdersMap) {
		this.jdeOrdersMap = jdeOrdersMap;
	}

	/**
	 * @return the errorURL
	 */
	public String getErrorURL() {
		return errorURL;
	}

	/**
	 * @param errorURL
	 *            the errorURL to set
	 */
	public void setErrorURL(String errorURL) {
		this.errorURL = errorURL;
	}

	/**
	 * @return the successURL
	 */
	public String getSuccessURL() {
		return successURL;
	}

	/**
	 * @param successURL
	 *            the successURL to set
	 */
	public void setSuccessURL(String successURL) {
		this.successURL = successURL;
	}

	/**
	 * @return the orderManager
	 */
	public CPSOrderManager getOrderManager() {
		return orderManager;
	}

	/**
	 * @param orderManager
	 *            the orderManager to set
	 */
	public void setOrderManager(CPSOrderManager orderManager) {
		this.orderManager = orderManager;
	}

	/**
	 * @return the rqlQueryAllJDEOrderNumbers
	 */
	public String getRqlQueryAllJDEOrderNumbers() {
		return rqlQueryAllJDEOrderNumbers;
	}

	/**
	 * @param rqlQueryAllJDEOrderNumbers the rqlQueryAllJDEOrderNumbers to set
	 */
	public void setRqlQueryAllJDEOrderNumbers(String rqlQueryAllJDEOrderNumbers) {
		this.rqlQueryAllJDEOrderNumbers = rqlQueryAllJDEOrderNumbers;
	}

}
