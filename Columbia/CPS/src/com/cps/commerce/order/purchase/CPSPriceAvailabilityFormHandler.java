package com.cps.commerce.order.purchase;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Arrays;

import javax.servlet.ServletException;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;


import com.cps.commerce.order.purchase.CPSPriceAvailability;
import com.cps.commerce.order.purchase.CPSPurchaseProcessHelper;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;
import com.cps.util.CPSSessionBean;

import atg.userprofiling.Profile;


public class CPSPriceAvailabilityFormHandler extends GenericFormHandler {

    private Profile profile;
    private CPSSessionBean mSessionBean;
    private Map<String, String> priceActionMap = new HashMap<String, String>();
    private CPSPriceAvailability item = new CPSPriceAvailability();
    private String sku;
    private String qty;
    private String itemcount;
    private String updatedqty;
    private String removeitems;
    private String additems;
    private long total;
    private CPSPurchaseProcessHelper purchaseProcessHelper;
    private RepeatingRequestMonitor mRepeatingRequestMonitor;

    private CatalogTools catalogTools;

    public boolean handleDelegatePaAction(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException, RepositoryException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSPriceAvailabilityFormHandler.handleDelegatePaAction";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                vlogDebug("Handle PA Action" + getPriceActionMap().get("action"));

                if (getPriceActionMap().get("action") != null && getPriceActionMap().get("action").equals("needToLoginList")) {
                    vlogDebug("needs to login ");

                    createError(CPSErrorCodes.ERR_CART_LOGIN_NEEDED, null, pRequest);

                    getPriceActionMap().clear();
                    return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                } else if (getPriceActionMap().get("action") != null && getPriceActionMap().get("action").equals("addItemToPA")) {
                    getPriceActionMap().clear();
                    if (isNumeric(qty)) {
                        vlogDebug("handle adding items ");
                        return handleAddItemToPa(pRequest, pResponse);
                    } else {
                        createError(CPSErrorCodes.ERR_PA_NO_QTY, null, pRequest);
                        return handleAddItemToPa(pRequest, pResponse);
                    }
                } else if (getPriceActionMap().get("update") != null && getPriceActionMap().get("update").equals("updatepa")) {

                    vlogDebug("Update items" + getUpdatedqty());
                    handleupdateqty();
                    return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                } else if (getPriceActionMap().get("remove") != null && getPriceActionMap().get("remove").equals("removeOneItem")) {
                    String index = pRequest.getParameter("itemcount");
                    if (!index.isEmpty()) {
                        if (!getSessionBean().getPalist().isEmpty()) {
                            handlePaRemove(index);
                            return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                        } else {
                            vlogDebug("test1");
                            createError(CPSErrorCodes.ERR_PA_NO_ITEM_REMOVE, null, pRequest);
                            return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                        }
                    } else {
                        vlogDebug("test2");
                        createError(CPSErrorCodes.ERR_PA_NO_ITEM_REMOVE, null, pRequest);
                        return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                    }
                } else if (getPriceActionMap().get("removeselected") != null && getPriceActionMap().get("removeselected").equals("removeselected")) {
                    vlogDebug("removing items");
                    handleRemoveSelected(pRequest, pResponse);
                    return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                } else if (getPriceActionMap().get("action") != null && getPriceActionMap().get("action").equals("noitemschecked")) {
                    vlogDebug("need to check items");

                    createError(CPSErrorCodes.ERR_PA_CHECK_ITEMS, null, pRequest);

                    getPriceActionMap().clear();
                    return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                }
                vlogDebug("nothing happened");
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
    }


    public boolean handleRemoveSelected(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSPriceAvailabilityFormHandler.handleRemoveSelected";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                String test = pRequest.getParameter("removeitems");

                vlogDebug("Test1" + test);
                vlogDebug("test2" + getRemoveitems());
                if (getRemoveitems() == null) {
                    setRemoveitems(pRequest.getParameter("removeitems"));
                }
                if (getRemoveitems() != null) {
                    vlogDebug("how many items are being removed" + getRemoveitems().length());
                    String[] paitem = getRemoveitems().split(",");
                    vlogDebug("after array " + Arrays.toString(paitem));
                    if (getRemoveitems().length() != 0) {
                        if (!Arrays.toString(paitem).isEmpty()) {
                            for (int i = 0; i < paitem.length; i++) {
                                vlogDebug("removing items final" + paitem[i]);
                                getSessionBean().getPalist().remove(Integer.parseInt(paitem[i]));
                            }
                        }
                    } else {
                        createError(CPSErrorCodes.ERR_PA_NO_ITEM_REMOVE, null, pRequest);
                    }
                }
                totalQty();
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return false;
    }


    public boolean handleAddItemToPa(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSPriceAvailabilityFormHandler.handleAddItemToPa";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                vlogDebug("Checking if item is real");
                String CSCheck = null;
                if (!getProfile().isTransient()) {
                    CSCheck = (String) ((RepositoryItem) getProfile().getPropertyValue(CPSConstants.SELECTED_CS)).getPropertyValue(CPSConstants.ID);
                }
                RepositoryItem pName = null;
                try {
                    pName = getCatalogTools().findSKU(sku);
                } catch (RepositoryException e) {
                    createError(CPSErrorCodes.ERR_PA_INVALID_SKU, null, pRequest);
                    vlogError(e, "Error");
                }

                if (pName == null) {
                    String skuIdFromNumber = findSkuIdFromNumber(sku);
                    if (StringUtils.isEmpty(skuIdFromNumber)) {
                        vlogDebug("nothing happened this is bad");
                        createError(CPSErrorCodes.ERR_PA_ADD_ITEM, null, pRequest);
                        totalQty();
                        return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
                    } else {
                        sku = skuIdFromNumber;
                    }

                }
                vlogDebug("DO we Make it HERE");


                if (qty != "0" && sku != null) {
                    vlogDebug("this where we should be");
                    if (isNumeric(qty)) {
                        if (CSCheck != null) {
                            item.setCsid((String) ((RepositoryItem) getProfile().getPropertyValue(CPSConstants.SELECTED_CS)).getPropertyValue(CPSConstants.ID));
                        }
                        item.setQty(Long.parseLong(qty));
                        item.setSku(sku);
                        getSessionBean().getPalist().add(getItem());

                        vlogDebug("Items" + item.getQty() + item.getSku() + getSessionBean().getPalist().size());
                    }
                }
                totalQty();
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return checkFormRedirect("/checkout/pa.jsp", null, pRequest, pResponse);
    }

    public void handlePaRemove(String current) {

        vlogDebug("itemcount" + current);
        try {
            int index = Integer.parseInt(current);
            vlogDebug("item count" + index);
            getSessionBean().getPalist().remove(index);
        } catch (NumberFormatException e) {
            vlogError(e, "Error");
        }
        totalQty();
    }

    public void handleupdateqty() {
        item = getSessionBean().getPalist().get(Integer.parseInt(getItemcount()));
        vlogDebug("set new QTY" + getUpdatedqty());
        item.setQty(Long.parseLong(getUpdatedqty()));
        getSessionBean().getPalist().get(Integer.parseInt(getItemcount())).setQty(Long.parseLong(getUpdatedqty()));
        totalQty();
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public void totalQty() {
        long tally = 0;
        long difitem = mSessionBean.getPalist().size();
        for (int i = 0; i < difitem; i++) {
            tally = tally + mSessionBean.getPalist().get(i).getQty();
        }

        setTotal(tally);
        mSessionBean.setTotalpaitems(tally);
    }


    private String findSkuIdFromNumber(String catalogRefId) {
        if (!StringUtils.isBlank(catalogRefId)) {
            RepositoryItem org = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
            if (org != null) {
                //let's try to find by alias number
                RepositoryItem productItem = getPurchaseProcessHelper().findProductFormAlias(catalogRefId, org.getRepositoryId());

                vlogDebug("prduct found by alias number =:" + productItem);

                if (productItem != null) {
                    List<RepositoryItem> childSkus = (List<RepositoryItem>) productItem.getPropertyValue("childSKUs");
                    if (childSkus != null && childSkus.size() > 0) {
                        return childSkus.get(0).getRepositoryId();
                    }
                }
            }
        }
        return null;
    }


    public CPSSessionBean getSessionBean() {
        return mSessionBean;
    }

    public void setSessionBean(CPSSessionBean mSessionBean) {
        this.mSessionBean = mSessionBean;
    }

    public Map getPriceActionMap() {
        return priceActionMap;
    }

    private void createError(String msg, String pty, DynamoHttpServletRequest pRequest) {
        CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
    }

    public String getSku() {
        return sku;
    }


    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getQty() {
        return qty;
    }


    public void setQty(String qty) {
        this.qty = qty;
    }

    public CPSPriceAvailability getItem() {
        return item;
    }


    public void setItem(CPSPriceAvailability item) {
        this.item = item;
    }


    public CatalogTools getCatalogTools() {
        return catalogTools;
    }


    public void setCatalogTools(CatalogTools catalogTools) {
        this.catalogTools = catalogTools;
    }


    public Profile getProfile() {
        return profile;
    }


    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getItemcount() {
        return itemcount;
    }


    public void setItemcount(String itemcount) {
        this.itemcount = itemcount;
    }

    public String getUpdatedqty() {
        return updatedqty;
    }


    public void setUpdatedqty(String updatedqty) {
        this.updatedqty = updatedqty;
    }


    public String getRemoveitems() {
        return removeitems;
    }


    public void setRemoveitems(String removeitems) {
        this.removeitems = removeitems;
    }


    public String getAdditems() {
        return additems;
    }


    public void setAdditems(String additems) {
        this.additems = additems;
    }

    public long getTotal() {
        return total;
    }


    public void setTotal(long total) {
        this.total = total;
    }


    public CPSPurchaseProcessHelper getPurchaseProcessHelper() {
        return purchaseProcessHelper;
    }


    public void setPurchaseProcessHelper(CPSPurchaseProcessHelper purchaseProcessHelper) {
        this.purchaseProcessHelper = purchaseProcessHelper;
    }

    public RepeatingRequestMonitor getRepeatingRequestMonitor() {
        return mRepeatingRequestMonitor;
    }

    public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
        mRepeatingRequestMonitor = pRepeatingRequestMonitor;
    }
}
