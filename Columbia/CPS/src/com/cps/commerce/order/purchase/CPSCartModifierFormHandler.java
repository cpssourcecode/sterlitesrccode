package com.cps.commerce.order.purchase;

import static com.cps.util.CPSConstants.CART_SELECTED_DELIVERY_METHOD;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_PICK_UP;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_SHIPPED;
import static com.cps.util.CPSConstants.MINIMUM_ORDER_QTY;
import static com.cps.util.CPSConstants.ORDER_QTY_INTERVAL;
import static com.cps.util.CPSConstants.SHIPPING_GROUP_COMMERCE_ITEM_CL_TYPE;
import static com.cps.util.CPSConstants.STOCKING_TYPE;
import static com.cps.util.CPSConstants.STOCKING_TYPE_K;
import static com.cps.util.CPSConstants.STOCKING_TYPE_OBSOLETE;
import static com.cps.util.CPSConstants.STOCKING_TYPE_USEUP;
import static com.cps.util.CPSConstants.STOCKING_TYPE_X;
import static com.cps.util.CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.commerce.inventory.CPSInventoryManager;
import com.cps.commerce.order.CPSAvailabilityManager;
import com.cps.commerce.order.CPSCommerceItemImpl;
import com.cps.commerce.order.CPSInvoiceRequest;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.service.CPSCartModifierService;
import com.cps.common.handler.DuplicatesMatchesHandlerInterface;
import com.cps.core.util.CPSContactInfo;
import com.cps.csr.util.CPSCSRLoggingManager;
import com.cps.inventory.InventoryAvailabilityInfo;
import com.cps.service.CPSExternalProductService;
import com.cps.service.DuplicatesMatchesService;
import com.cps.service.SixLengthIdService;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.userprofiling.service.LoginService;
import com.cps.userprofiling.util.LoginSessionInfo;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;
import com.cps.util.CPSSessionBean;
import com.sun.org.apache.bcel.internal.generic.NEW;

import atg.commerce.CommerceException;
import atg.commerce.catalog.CatalogTools;
import atg.commerce.inventory.InventoryException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemImpl;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.Relationship;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.CollectionUtils;
import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import vsg.util.VSGFormUtils;
import vsg.util.ajax.AjaxUtils;

public class CPSCartModifierFormHandler extends CartModifierFormHandler implements DuplicatesMatchesHandlerInterface {

    private static final String JSON_RESPONSE_COMMERCE_ITEM_ID = "cid";
    private static final String JSON_RESPONSE_ORDER_SUBTOTAL = "orderSubtotal";
    private static final String JSON_RESPONSE_ORDER_TOTAL_ITEM_COUNT = "totalCommerceItemCount";

    private static final String HEADER_X_NO_REDIRECT = "X-No-Redirect";
    private static final String HEADER_PARAMETER_TRUE = "true";
    private static final String DEFAULT_LEAD_TIME_BRANCH = "200";  

    private Repository mProductRepository;
    private boolean notAddedItemsFlag = false;
    private CPSSessionBean mSessionBean;
    private String skuIdsList;
    private Boolean giftIds = false;
    private Boolean mOrderItems = false;
    private String scheduleDays;
    private Boolean mFromAvailabilityModal = false;
    private boolean qoFormError;

    private String qtyList;
    private Map<String, String> skuQtyMap = new HashMap<String, String>();
    private String paitemlist;
    private Boolean paIds = false;
    boolean multierror = false;
    private Boolean resetCheckAvailability = false;

    private String quickOrderAddItemsToOrderFromListErrorURL;
    private String quickOrderAddItemsToOrderFromListSuccessURL;
    private String quickOrderAddItemsToOrderFromListInvalidURL;
    private String addFromErrorModal;
    private HashMap<String, HashMap<Integer, ArrayList<String>>> quickOrderDuplicates = null;
    private ArrayList<String> quickOrderFinalSkus = new ArrayList<String>();
    private ArrayList<Integer> quickOrderFinalQuantities = new ArrayList<Integer>();

    private String handleCheckoutSuccessURL;
    private String mHandleCheckoutErrorURL;

    private String mComparisonReferer;

    private CPSCSRLoggingManager csrLoggingManager;

    private CPSExternalProductService externalProductService;
    private MutableRepository productCatalog;
    /**
     * Include profileTools
     */
    private CPSProfileTools profileTools;
    private Map<String, String> mItemsInfo = new HashMap<>();
    private String mPoNumber;
    private String mDeliveryMethod;
    private String mShippingAddressId;
    private String mBillingAddressId;
    /**
     * selected shipping method on availability modal
     */
    private String mShippingMethodOnAvailabilityModal;
    /**
     * cart modifier service
     */
    private CPSCartModifierService mCartModifierService;
    /**
     * Six Length Ids ervice
     */
    private SixLengthIdService mSixLengthIdService;
    /**
     * Duplicates Matches Service
     */
    private DuplicatesMatchesService mDuplicatesMatchesService;
    /**
     * current profile
     */
    private Profile mCurrentProfile;
    /**
     * selected store id
     */
    private String mSelectedLocationId;
    /**
     * login service
     */
    private LoginService mLoginService;
    /**
     * session bean
     */
    private LoginSessionInfo mLoginSessionInfo;

    private final Map<String, Object> mJsonResponse = new HashMap<>();

    private CPSLastProductsAddedToCart mLastProductsAddedToCart;

    private CatalogTools mCatalogTools;

    private CPSAvailabilityManager mAvailabilityManager;

    private CPSFailedProductsNotAddedToCart failedProductsNotAddedToCart;

    @Override
    public void beforeGet(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
        super.beforeGet(request, response);
        getFailedProductsNotAddedToCart().setFailedProducts(new ArrayList<CPSFailedProduct>());
    }

    @Override
    public void preAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
        boolean error = true;
        if (isLoggingDebug()) {
            vlogDebug("preAddItemToOrder start");
        }

        if (isPaIds()) {
            vlogDebug("its a PA item");
            String paIndex = getPaitemlist();
            if (StringUtils.isBlank(paIndex)) {
                vlogDebug("getting items to add");
                paIndex = pRequest.getParameter("paitemlist");
            }
            String[] indexs = paIndex.split(",");
            String[] skuId = new String[indexs.length];
            // setPaqty()= getSessionBean().getPalist().get(Integer.parseInt(paitem[i])).getQty();
            vlogDebug("indexs " + indexs.length);
            setAddItemCount(indexs.length);
            for (int i = 0; i < indexs.length; i++) {
                vlogDebug("looping" + i);
                if (!getSessionBean().getPalist().isEmpty()) {
                    try {
                        CPSPriceAvailability currentPa = getSessionBean().getPalist().get(Integer.parseInt(indexs[i]));

                        skuId[i] = currentPa.getSku();
                        Long qtys = currentPa.getQty();
                        pRequest.setParameter(currentPa.getSku(), qtys);
                        getItems()[i].setProductId(productIdFromSku(currentPa.getSku()));
                        getItems()[i].setCatalogRefId(currentPa.getSku());
                        getItems()[i].setQuantity(currentPa.getQty());
                        vlogDebug("What is the Item  " + currentPa.getSku() + currentPa.getQty() + qtys);
                    } catch (Exception e) {
                        createError(CPSErrorCodes.ERR_PA_NO_ITEM, null, pRequest);
                        if (isLoggingError()) {
                            logError(e);
                        }
                        error = false;
                    }
                    if (error) {
                        setCatalogRefIds(skuId);
                    }
                } else {
                    createError(CPSErrorCodes.ERR_PA_NO_ITEM, null, pRequest);
                }
            }
        } else if (isGiftIds() || getOrderItems()) {
            if (!StringUtils.isBlank(getSkuIdsList())) {
                setCatalogRefIds(getSkuIdsList().split(","));
            }
            String[] qtys = getQtyList().split(",");
            vlogDebug("These items: " + getSkuIdsList());
            if (isGiftIds()) {
                try {
                    String[] temp = new String[getCatalogRefIds().length];
                    int count = 0;
                    for (String itemId : getCatalogRefIds()) {
                        temp[count++] = (String) getGiftlistManager().getGiftitem(itemId).getPropertyValue(CPSConstants.CATALOG_REF_ID);
                        vlogDebug("New id: " + temp[count - 1]);
                    }
                    setCatalogRefIds(temp);
                } catch (CommerceException e) {
                    if (isLoggingError()) {
                        logError(e);
                    }
                }
            }
            setAddItemCount(getCatalogRefIds().length);
            for (int i = 0; i < getCatalogRefIds().length; i++) {
                try {
                    Long qty = Long.parseLong(qtys[i]);
                    vlogDebug("Setting " + getCatalogRefIds()[i] + " qty to - " + qty);
                    pRequest.setParameter(getCatalogRefIds()[i], qty);

                    vlogDebug("Atempt to add to getItems array");
                    if (getItems() != null) {
                        vlogDebug("getItems != null");
                        try {
                            getItems()[i].setProductId(productIdFromSku(getCatalogRefIds()[i]));
                            getItems()[i].setCatalogRefId(getCatalogRefIds()[i]);
                            getItems()[i].setQuantity(qty);
                            vlogDebug("Item added to array");
                        } catch (Exception e) {
                            if (isLoggingError()) {
                                logError(e);
                            }
                        }
                    }
                } catch (NumberFormatException e) {
                    if (isLoggingError()) {
                        logError(e);
                    }
                }
            }
            vlogDebug("GetItems: " + Arrays.toString(getItems()));
        } else {
            vlogDebug("normal flow");
            if (getItems() != null) {
                for (int i = 0; i < getItems().length; i++) {
                    getItems()[i].setProductId(productIdFromSku(getItems()[i].getCatalogRefId()));
                }
            }
        }

        if (getItems() != null) {
            setCatalogRefIds(null);
            setProductIds(null);
            for (AddCommerceItemInfo commerceItemInfo : getItems()) {
                String productId = commerceItemInfo.getCatalogRefId();
                String stockingType = null;

                try {
                    RepositoryItem catalogProduct = getCatalogTools().findProduct(productId);
                    if (catalogProduct != null) {
                        stockingType = (String) catalogProduct.getPropertyValue(STOCKING_TYPE);
                    }
                } catch (Exception e) {
                    logError("Exception raised while looking up product ID <" + productId + "> to add to order");
                }

                // Prevent users from attempting to add obsolete materials to cart
                if (STOCKING_TYPE_OBSOLETE.equals(stockingType) || STOCKING_TYPE_USEUP.equals(stockingType) || STOCKING_TYPE_K.equals(stockingType)
                                || STOCKING_TYPE_X.equals(stockingType) || PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR.equals(stockingType)) {
                    commerceItemInfo.setQuantity(0);
                }
            }
        }

        if (getItems() != null && getItems().length > 0) {
            validateItems(getItems(), pRequest);
        }

        vlogDebug("preAddItemToOrder.end");
    }

    /**
     * Checking inventory status and level before adding to cart.
     *
     * @param items
     * @param pRequest
     */
    protected void validateItems(AddCommerceItemInfo[] items, DynamoHttpServletRequest pRequest) {
        CPSInventoryManager invManager = (CPSInventoryManager) getOrderManager().getOrderTools().getInventoryManager();
        for (AddCommerceItemInfo item : items) {
            String skuId = item.getCatalogRefId();
            long qty = item.getQuantity();

            vlogDebug("skuId===" + skuId + "   qty" + qty);

            if (!invManager.isValidAddToCart(skuId, qty, getOrder(), null)) {

                vlogDebug("No valid add to cart: skuId===" + skuId + "   qty" + qty);

                Object[] params = { skuId };
                CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_ITEM_INVENTORY, pRequest.getLocale(), skuId,
                                params);
            }

        }
    }

    protected boolean validateOnQuickOrder(ArrayList<AddCommerceItemInfo> itemsToAdd, String skuId, long quantity, DynamoHttpServletRequest pRequest) {
        long totalQty = quantity;
        boolean isValid = true;
        CPSInventoryManager invManager = (CPSInventoryManager) getOrderManager().getOrderTools().getInventoryManager();
        if (itemsToAdd != null) {
            for (AddCommerceItemInfo item : itemsToAdd) {
                String skuIdFromList = item.getCatalogRefId();
                long itemQty = item.getQuantity();
                vlogDebug("itemQty====" + itemQty + "skuIdFromList===" + skuIdFromList + "   skuId" + skuId);

                if (!StringUtils.isEmpty(skuIdFromList) && skuIdFromList.equalsIgnoreCase(skuId)) {
                    totalQty = totalQty + itemQty;
                    vlogDebug("new totalQty" + totalQty);
                }
            }
        }
        isValid = checkIfProductPurchasable(pRequest, skuId, false);
        if (isValid) {
            isValid = validateMinimumQuantityForProduct(pRequest, skuId, quantity, false, false);
        }
        if (!invManager.isValidAddToCart(skuId, totalQty, getOrder(), null)) {
            isValid = false;
            vlogDebug("No valid add to cart: skuId===" + skuId + "   totalQty" + totalQty);
        }
        return isValid;
    }

    /**
     * @param pRequest
     * @param pResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    public boolean handleQuickOrderAddItemsToOrderFromList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
                    throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleQuickOrderAddItemsToOrderFromList";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                ArrayList<String> invalidSkuSessionList = new ArrayList<String>();

                addQuickOrderItems(pRequest, pResponse, invalidSkuSessionList);

                if (getFormError() && getItems() != null && getItems().length == 0) {
                    vlogDebug("FORM ERROR REDIRECT HAPPENING");
                    if (isNoRedirectAjaxRequest(pRequest)) {
                        return false;
                    } else {
                        return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(), getQuickOrderAddItemsToOrderFromListInvalidURL(), pRequest,
                                        pResponse);
                    }
                } else {
                    getSessionBean().setQtys(null);
                    getSessionBean().setItemIds(null);
                    vlogDebug("RETURNING NO REDIRECT");
                    handleAddItemToOrderNoRedirect(pRequest, pResponse, invalidSkuSessionList);
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        return false;
    }
    
	public boolean handleQuickOrderAddItemsToOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSCartModifierFormHandler.handleQuickOrderAddItemsToOrderFromList";
		Map<String, Object> params = new HashMap<String, Object>();
		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = true;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				ArrayList<String> invalidSkuSessionList = new ArrayList<String>();

				addQuickOrderItems(pRequest, pResponse, invalidSkuSessionList);

				if (getFormError() && getItems() != null && getItems().length == 0) {
					vlogDebug("FORM ERROR REDIRECT HAPPENING");
					if (isNoRedirectAjaxRequest(pRequest)) {
						return false;
					} else {
						return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(),
								getQuickOrderAddItemsToOrderFromListInvalidURL(), pRequest, pResponse);
					}
				} else {
					getSessionBean().setQtys(null);
					getSessionBean().setItemIds(null);
					vlogDebug("RETURNING NO REDIRECT");
					if (!getFormError() && !qoFormError) {
						rollback = handleAddItemToOrderNoRedirect(pRequest, pResponse, invalidSkuSessionList);
						// handleAddItemToOrderNoRedirect(pRequest, pResponse,
						// invalidSkuSessionList);
						params.put("result", true);
					} else {
						getSessionBean().setHasAddedNewProductToShoppingCart(false);
						Map<String, String> invalidSkuQtyMap = new HashMap<String, String>();
						vlogDebug("Sku Qty map entered by user on Quick Pad skuQtyMap {0}", skuQtyMap);
						if (skuQtyMap != null) {
							for (String sku : skuQtyMap.keySet()) {
								boolean skuFound = false;
								String productId = productIdFromSku(sku);
								if (productId == null) {
									RepositoryItem[] productItems = getDuplicatesMatchesService().findKeyProduct(sku,
											getProfile());
									if (productItems != null) {
										RepositoryItem prodItem = productItems[0];
										productId = prodItem.getRepositoryId();
									}
								}
								if (getItems() != null) {
									if (productId != null) {
										for (AddCommerceItemInfo item : getItems()) {
											String itemSkuId = item.getCatalogRefId();
											RepositoryItem skuItem = getProductCatalog().getItem(itemSkuId, "sku");
											if (skuItem != null) {
												Set<RepositoryItem> productItems = (Set<RepositoryItem>) skuItem
														.getPropertyValue("parentProducts");
												if (productItems != null) {
													RepositoryItem productItem = productItems.iterator().next();
													String prodId = productItem.getRepositoryId();
													String skuName = (String) skuItem.getPropertyValue("displayName");
													if (sku.equalsIgnoreCase(skuName) || sku.equalsIgnoreCase(itemSkuId)
															|| productId.equals(prodId)) {
														skuFound = true;
													}
												}
											}
										}
									} else {
										skuFound = false;
									}
									if (!skuFound) {
										invalidSkuQtyMap.put(sku, skuQtyMap.get(sku));
									}
								} else {
									invalidSkuQtyMap.putAll(skuQtyMap);
								}
								vlogDebug("Invalid Skus/Qty not added to cart {0}", invalidSkuQtyMap);
								params.put("invalidSkuQtyMap", new JSONObject(invalidSkuQtyMap));
							}
							List<CPSFailedProduct> failedProductsList = getFailedProductsNotAddedToCart()
									.getFailedProducts();
							Map<String, String> failedProductsMap = new HashMap<String, String>();
							if (failedProductsList != null) {
								for (CPSFailedProduct failedProduct : failedProductsList) {
									String sku = failedProduct.getSku();
									String errorMessage = failedProduct.getErrorMessage();
									failedProductsMap.put(sku, errorMessage);
								}
							}
							params.put("failedProductsMap", new JSONObject(failedProductsMap));
							vlogDebug("failedProductsMap {0}", failedProductsMap);
							params.put("skuQtyMap", new JSONObject(skuQtyMap));
						}
						if (isQoFormError()) {
							CPSMessageUtils.addFormException(this, CPSErrorCodes.ERR_QUICK_ORDER_GENERAL,
									pRequest.getLocale(), CPSErrorCodes.ERR_QUICK_ORDER_GENERAL);
						}
					}
				}
			} catch (TransactionDemarcationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				if (null != rrm) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException pE) {
						if (isLoggingError()) {
							logError(pE);
						}
					}
				}
			}
		}

		return response(pResponse, params);
	}

    public boolean handleQuickOrderPadAddItems(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse)
                    throws ServletException, IOException, CommerceException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "FileUploadHandler.handleCreateMaterialListFromFile";
        Map<String, Object> params = new HashMap<String, Object>();

        if (null == rrm || rrm.isUniqueRequestEntry(handleMethodName)) {
            TransactionManager tm = getTransactionManager();
            TransactionDemarcation td = new TransactionDemarcation();
            boolean rollback = true;
            try {
                if (tm != null) {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }

                ArrayList<String> invalidSkuSessionList = new ArrayList<>();
                addQuickOrderItems(pRequest, pResponse, invalidSkuSessionList);

                if (getFormError() && getItems() != null && getItems().length == 0) {
                    vlogDebug("FORM ERROR REDIRECT HAPPENING");

                    VSGFormUtils.addAjaxErrorResponse(this, pResponse);
                } else {
                    getSessionBean().setQtys(null);
                    getSessionBean().setItemIds(null);
                    vlogDebug("RETURNING NO REDIRECT");
                    if(!getFormError() && !qoFormError){
                        rollback = handleAddItemToOrderNoRedirect(pRequest, pResponse, invalidSkuSessionList);
                    }
                    getSessionBean().setHasAddedNewProductToShoppingCart(false);
                    Map<String, String> invalidSkuQtyMap = new HashMap<String, String>();
                    vlogDebug("Sku Qty map entered by user on Quick Pad skuQtyMap {0}", skuQtyMap);
                    if (skuQtyMap != null) {
                        for (String sku : skuQtyMap.keySet()) {
                            boolean skuFound = false;
                            String productId = productIdFromSku(sku);
                            if (productId == null) {
                                RepositoryItem[] productItems = getDuplicatesMatchesService().findKeyProduct(sku, getProfile());
                                if (productItems != null) {
                                    RepositoryItem prodItem = productItems[0];
                                    productId = prodItem.getRepositoryId();
                                }
                            }
                            if (getItems() != null) {
                                if (productId != null) {
                                    for (AddCommerceItemInfo item : getItems()) {
                                        String itemSkuId = item.getCatalogRefId();
                                        RepositoryItem skuItem = getProductCatalog().getItem(itemSkuId, "sku");
                                        if (skuItem != null) {
                                            Set<RepositoryItem> productItems = (Set<RepositoryItem>) skuItem.getPropertyValue("parentProducts");
                                            if (productItems != null) {
                                                RepositoryItem productItem = productItems.iterator().next();
                                                String prodId = productItem.getRepositoryId();
                                                String skuName = (String) skuItem.getPropertyValue("displayName");
                                                if (sku.equalsIgnoreCase(skuName) || sku.equalsIgnoreCase(itemSkuId) || productId.equals(prodId)) {
                                                    skuFound = true;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    skuFound = false;
                                }
                                if (!skuFound) {
                                    invalidSkuQtyMap.put(sku, skuQtyMap.get(sku));
                                }
                            } else {
                                invalidSkuQtyMap.putAll(skuQtyMap);
                            }
                            vlogDebug("Invalid Skus/Qty not added to cart {0}", invalidSkuQtyMap);
                            params.put("invalidSkuQtyMap", new JSONObject(invalidSkuQtyMap));
                        }
                        List<CPSFailedProduct> failedProductsList = getFailedProductsNotAddedToCart().getFailedProducts();
                        Map<String, String> failedProductsMap = new HashMap<String, String>();
                        if (failedProductsList != null) {
                            for (CPSFailedProduct failedProduct : failedProductsList) {
                                String sku = failedProduct.getSku();
                                String errorMessage = failedProduct.getErrorMessage();
                                failedProductsMap.put(sku, errorMessage);
                            }
                        }
                        params.put("failedProductsMap", new JSONObject(failedProductsMap));
                        vlogDebug("failedProductsMap {0}", failedProductsMap);
                        params.put("skuQtyMap", new JSONObject(skuQtyMap));
                    }
                    if(isQoFormError()){
                        CPSMessageUtils.addFormException(this, CPSErrorCodes.ERR_QUICK_ORDER_GENERAL, pRequest.getLocale(), CPSErrorCodes.ERR_QUICK_ORDER_GENERAL);
                    }
                }

            } catch (final Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
            } finally {
                if (null != rrm) {
                    rrm.removeRequestEntry(handleMethodName);
                }
                if (tm != null) {
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException pE) {
                        if (isLoggingError()) {
                            logError(pE);
                        }
                    }
                }
            }
        }

        return response(pResponse, params);
    }

    private boolean addQuickOrderItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, ArrayList<String> invalidSkuSessionList)
                    throws ServletException, IOException {
        String skuId = "";
        Object[] invalidSkus = new Object[2];
        invalidSkus[0] = new StringBuilder();
        int skuorder = 0;

        if (isLoggingDebug()) {
            vlogDebug("Entering handleQuickOrderAddItemsToOrderFromList");
        }
        if (!"yes".equals(getAddFromErrorModal()) && (StringUtils.isBlank(getSkuIdsList()) || StringUtils.isBlank(getQtyList()))) {
            // blank form redirect
            createError(CPSErrorCodes.ERR_BLANK_QUICK_ORDER_FORM, "import", pRequest);
            return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(), getQuickOrderAddItemsToOrderFromListErrorURL(), pRequest, pResponse);
            // return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(), getQuickOrderAddItemsToOrderFromListErrorURL(), pRequest, pResponse);
        }
        if ("yes".equals(getAddFromErrorModal())) {
            String[] sList = getSkuIdsList().split(",");
            String[] qList = getQtyList().split(",");
            if (sList.length != qList.length || sList.length == 1 && StringUtils.isEmpty(sList[0])
                            || sList.length != getSessionBean().getQuickOrderDuplicates().size()) {
                createError(CPSErrorCodes.ERR_QUICK_ORDER_DUPE, "import", pRequest);
                return checkFormRedirect(getQuickOrderAddItemsToOrderFromListInvalidURL(), getQuickOrderAddItemsToOrderFromListInvalidURL(), pRequest,
                                pResponse);
                // redirectOrForward(pRequest, pResponse, getQuickOrderAddItemsToOrderFromListInvalidURL());
                // return false;
            }
        }
        if (!StringUtils.isBlank(getSkuIdsList()) && !StringUtils.isBlank(getQtyList())) {
            String[] originalSkuNameList = getSkuIdsList().split(",");

            if (!"yes".equals(getAddFromErrorModal())) {
                boolean isValid = getDuplicatesMatchesService().makeValidSkuIdsList(getProfile(), this); // call out to query possible input values before
                                                                                                         // adding //updated SkuIdsList,QtyList will be "" if
                                                                                                         // lengths on input are not equal
                if (!isValid) {
                    createError(CPSErrorCodes.ERR_QUICK_ORDER_QTY, "import", pRequest);
                    return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(), getQuickOrderAddItemsToOrderFromListErrorURL(), pRequest,
                                    pResponse);
                }
            }
            if ("".equals(getSkuIdsList()) || "".equals(getQtyList())) {
                // no addable values found, check if duplicates only or just nothing at all..
                if (invalidSkuSessionList != null && invalidSkuSessionList.size() > 0 || getDuplicates() != null && getDuplicates().keySet().size() > 0) {
                    getSessionBean().setQtys(null);
                    getSessionBean().setItemIds(null);
                    return handleAddItemToOrderNoRedirect(pRequest, pResponse, invalidSkuSessionList);
                }
            }
            String[] skuList = getSkuIdsList().split(",");
            if (isLoggingDebug()) {
                vlogDebug("skuList: " + skuList);
            }
            getSessionBean().setItemIds(new ArrayList<String>(Arrays.asList(skuList)));
            String[] qtyList = getQtyList().split(",");
            for (int i = 0; i < skuList.length; i++) {
                getSkuQtyMap().put(getFinalSkus().get(i), qtyList[i]);
            }
            if (isLoggingDebug()) {
                vlogDebug("qtyList: " + skuList);
            }

            ArrayList<Integer> intQtyList = new ArrayList<Integer>();
            try {
                for (String qty : qtyList) {
                    if (qty.compareTo("Qty") != 0) {
                        intQtyList.add(Integer.parseInt(qty));
                    } else {
                        intQtyList.add(1);
                    }

                }
            } catch (NumberFormatException nfe) {
                createError(CPSErrorCodes.ERR_QUICK_ORDER_QTY, "import", pRequest);
                return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(), getQuickOrderAddItemsToOrderFromListErrorURL(), pRequest, pResponse);
            }
            getSessionBean().setQtys(intQtyList);
            for (int i = 0; i < skuList.length; i++) {
                pRequest.setParameter(skuList[i], qtyList[i]);
            }

            ArrayList<AddCommerceItemInfo> itemsToAdd = new ArrayList<>();
            List<CPSFailedProduct> failedProductsList = new ArrayList<CPSFailedProduct>();

            for (int i = 0; i < skuList.length; i++) {
                boolean badSku = false;
                String enteredSku = getFinalSkus().get(i);
                CPSFailedProduct failedProduct = new CPSFailedProduct();

                if (isLoggingDebug()) {
                    vlogDebug("working on: " + skuList[i]);
                }
                // if ( skuList[i].compareTo("Item #") != 0 ) {
                //
                // }
                AddCommerceItemInfo iInfo = new AddCommerceItemInfo();
                skuId = skuList[i];
                String qty = "";

                if (qtyList.length > i) {
                    qty = qtyList[i];
                } else {
                    qty = "1";
                }

                long quantity;
                try {
                    quantity = Long.parseLong(qty);
                } catch (NumberFormatException e) {
                    quantity = 1;
                }
                String productId = productIdFromSku(skuId);

                if (productIdFromSku(skuId) == null) {
                    if (skuorder == 0) {
                        invalidSkus[0] = new StringBuilder().append(skuId);
                        skuorder++;
                    } else if (skuorder == 1) {
                        invalidSkus[0] = new StringBuilder().append(invalidSkus[0].toString());
                        invalidSkus[1] = skuId;
                        skuorder++;
                    } else if (skuorder > 1) {
                        invalidSkus[0] = new StringBuilder().append(invalidSkus[0].toString()).append(", ").append(invalidSkus[1].toString());
                        invalidSkus[1] = skuId;
                    }
                    // addFormException(new DropletFormException("", getAbsoluteName(), "itemNumP" + i)); //skip exception still add
                    // multierror = true;
                    if (invalidSkuSessionList != null) {
                        invalidSkuSessionList.add(skuId);
                    }
                    badSku = true;
                    failedProduct.setSku(enteredSku);
                    failedProduct.setProductId(skuId);
                    failedProduct.setProductQty(quantity);
                    String errorMessage = CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_ITEMS_NOT_FOUND,
                                    pRequest.getLocale(), null);
                    failedProduct.setErrorMessage(errorMessage);
                    qoFormError = true;
                }
                // inventory validation. Will look Cart and 'itemsToAdd' list for item.
                if (!validateOnQuickOrder(itemsToAdd, skuId, quantity, pRequest)) {
                    // Object[] params = {skuId};
                    // CPSMessageUtils.addFormException(this,CPSConstants.ERR_MESSAGES_REPOSITORY,CPSErrorCodes.ERR_ITEM_INVENTORY,pRequest.getLocale(),"itemNumP"
                    // + i,params);
                    if (!badSku) {
                        // dont double down if productIdFromSku already null
                        // if (invalidSkuSessionList != null) {
                        // invalidSkuSessionList.add(skuId);
                        // }
                        if (!invalidSkuSessionList.contains(getFinalSkus().get(i) + ": ")) {
                            invalidSkuSessionList.add(getFinalSkus().get(i) + ": ");
                        }
                        // invalidSkuSessionList.add(originalSkuNameList[i] + ": ");
                        if (pRequest.getParameter(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE) != null) {
                            if (!invalidSkuSessionList.contains(getFinalSkus().get(i) + ": ")) {
                                invalidSkuSessionList.add(getFinalSkus().get(i) + ": ");
                            }
                            invalidSkuSessionList.add(pRequest.getParameter(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE));

                            pRequest.removeParameter(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE);
                            // createError(CPSErrorCodes.ERR_QTY_ERROR, CPSErrorCodes.ERR_QTY_ERROR, pRequest);
                            failedProduct.setSku(enteredSku);
                            failedProduct.setProductId(productId);
                            failedProduct.setProductQty(quantity);
                            String errorMessage = CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY,
                                            CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE, pRequest.getLocale(), null);
                            failedProduct.setErrorMessage(errorMessage);
                            qoFormError = true;
                        }

                        if (pRequest.getParameter(CPSErrorCodes.ERR_MIN_ORDER_QTY) != null) {
                            if (!invalidSkuSessionList.contains(getFinalSkus().get(i) + ": ")) {
                                invalidSkuSessionList.add(getFinalSkus().get(i) + ": ");
                            }
                            invalidSkuSessionList.add(pRequest.getParameter(CPSErrorCodes.ERR_MIN_ORDER_QTY));

                            pRequest.removeParameter(CPSErrorCodes.ERR_MIN_ORDER_QTY);
                            // createError(CPSErrorCodes.ERR_QTY_ERROR, CPSErrorCodes.ERR_QTY_ERROR, pRequest);
                            failedProduct.setSku(enteredSku);
                            failedProduct.setProductId(productId);
                            failedProduct.setProductQty(quantity);
                            String minOrderQty = pRequest.getParameter("minOrderQty");
                            Object[] params = new Object[] { minOrderQty };
                            String errorMessage = CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_MIN_ORDER_QTY,
                                            pRequest.getLocale(), params);
                            failedProduct.setErrorMessage(errorMessage);
                            qoFormError = true;
                        }
                        if (pRequest.getParameter(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL) != null) {
                            if (!invalidSkuSessionList.contains(getFinalSkus().get(i) + ": ")) {
                                invalidSkuSessionList.add(getFinalSkus().get(i) + ": ");
                            }
                            invalidSkuSessionList.add(pRequest.getParameter(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL));
                            pRequest.removeParameter(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL);
                            // if (!getFormError()) {
                            // createError(CPSErrorCodes.ERR_QTY_ERROR, CPSErrorCodes.ERR_QTY_ERROR, pRequest);
                            // }
                            failedProduct.setSku(enteredSku);
                            failedProduct.setProductId(productId);
                            failedProduct.setProductQty(quantity);
                            String orderQtyInterval = pRequest.getParameter("orderQtyInterval");
                            Object[] params = new Object[] { orderQtyInterval };
                            String errorMessage = CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY,
                                            CPSErrorCodes.ERR_ORDER_QTY_INTERVAL, pRequest.getLocale(), params);
                            failedProduct.setErrorMessage(errorMessage);
                            qoFormError = true;
                        }
                        badSku = true;
                    }

                }
                if (!badSku) {
                    // just ignore badSkus rather than cancel add to cart completely
                    iInfo.setCatalogRefId(findSkuId(skuId));
                    iInfo.setProductId(productIdFromSku(skuId));
                    iInfo.setQuantity(quantity);
                    itemsToAdd.add(iInfo);
                }
                if (failedProduct.getProductId() != null) {
                    failedProductsList.add(failedProduct);
                }
            }

            if (itemsToAdd.size() > 0) {
                setAddItemCount(itemsToAdd.size());
                itemsToAdd.toArray(getItems()); // ?getItems()
            }
            getFailedProductsNotAddedToCart().setFailedProducts(new ArrayList<CPSFailedProduct>());
            if (!CollectionUtils.isEmpty(failedProductsList)) {
                getFailedProductsNotAddedToCart().setFailedProducts(failedProductsList);
            }
//             if (!getFormError() && itemsToAdd.isEmpty() && skuList.length > 0) {
//             getSessionBean().setInvalidItemList(invalidSkuSessionList);
//             createError(CPSErrorCodes.ERR_ITEMS_NOT_FOUND, CPSErrorCodes.ERR_ITEMS_NOT_FOUND, pRequest);
//             }
        }
        if (multierror) {
            if (skuorder == 1) {
                CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_Quick_Item_Validation, pRequest.getLocale(),
                                skuId, invalidSkus);

            } else {
                CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_Quick_Order_Mult_Item_Validation,
                                pRequest.getLocale(), skuId, invalidSkus);
            }
            skuorder = 0;
        }
        return false;
    }

    private boolean isNoRedirectAjaxRequest(DynamoHttpServletRequest pRequest) {
        String xNoRedirectHeader = pRequest.getHeader(HEADER_X_NO_REDIRECT);
        if (xNoRedirectHeader == null) {
            return false;
        } else {
            return xNoRedirectHeader.equalsIgnoreCase(HEADER_PARAMETER_TRUE);
        }
    }

    public boolean handleAddItemToOrderNoRedirect(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse,
                    ArrayList<String> invalidSkuSessionList) throws IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleAddItemToOrderNoRedirect";

        TransactionManager tm = getTransactionManager();
        TransactionDemarcation td = new TransactionDemarcation();
        boolean rollback = false;

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (tm != null) {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }
                checkForAbandantOrderEmail(getOrder(),getCurrentProfile());
                super.handleAddItemToOrder(pRequest, pResponse);

                synchronized (getOrder()) {
                    // adding update for total price per item:

                    List<CommerceItem> newItems = getAddItemsToOrderResult();
                    boolean isProductsAddedToCart = newItems != null && !newItems.isEmpty();
                    if (isProductsAddedToCart) {
                        createLastProductsAddedToCart();
                        for (int ind = 0; ind < newItems.size(); ind++) {
                            CommerceItem newItem = newItems.get(ind);
                            addJsonToResponse(JSON_RESPONSE_COMMERCE_ITEM_ID, newItem.getId());
                            ItemPriceInfo itemPriceInfo = newItem.getPriceInfo();
                            if (itemPriceInfo != null) {
                                try {
                                    double unitPrice = getExternalProductService().getCustomerItemPrice(newItem.getCatalogRefId(), (Profile) getProfile(),
                                                    (CPSOrderImpl) getOrder()); // productId
                                    itemPriceInfo.setListPrice(unitPrice);
                                    List<CommerceItem> existingCommerceItem = getOrder().getCommerceItemsByCatalogRefId(newItem.getCatalogRefId()); // sku
                                    long totalItemQuantity = existingCommerceItem == null || existingCommerceItem.size() < 1 ? newItem.getQuantity()
                                                    : existingCommerceItem.get(0).getQuantity(); // qty
                                    double itemAmount = unitPrice * totalItemQuantity;
                                    itemAmount = Math.round(itemAmount * 100);
                                    itemAmount = itemAmount / 100;
                                    itemPriceInfo.setAmount(itemAmount);
                                    itemPriceInfo.setAmountIsFinal(true);
                                    itemPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
                                } catch (CommerceItemNotFoundException | InvalidParameterException e) {
                                    vlogError(e, "Unable to get commerce item quantity");
                                }
                            }
                        }
                    }

                    double orderAmount = 0;
                    List<CommerceItem> commerceItems = getOrder().getCommerceItems();
                    for (CommerceItem commerceItem : commerceItems) {
                        double itemAmount = 0;
                        ItemPriceInfo itemPriceInfo = commerceItem.getPriceInfo();
                        if (itemPriceInfo != null) {
                            itemAmount = itemPriceInfo.getAmount();
                        }
                        CatalogTools catalogTools = getCatalogTools();
                        if (catalogTools instanceof CPSCatalogTools) {
                            if (((CPSCatalogTools) catalogTools).isItemValid(commerceItem)) {
                                orderAmount += itemAmount;
                            }
                        } else {
                            orderAmount += itemAmount;
                        }
                    }
                    OrderPriceInfo orderPriceInfo = getOrder().getPriceInfo();
                    if (orderPriceInfo != null) {
                        orderPriceInfo.setAmount(orderAmount);
                        orderPriceInfo.setRawSubtotal(orderAmount);
                        orderPriceInfo.setAmountIsFinal(true);
                        orderPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
                    }
                    /////
                    if (invalidSkuSessionList != null && invalidSkuSessionList.size() > 0 || getDuplicates() != null && getDuplicates().keySet().size() > 0) {
                        // need to redirect to error modal if either duplicates or invalid values were added, will need to check for null in modal droplet.
                        getSessionBean().setInvalidItemList(invalidSkuSessionList); // set list of input values that had no return values to session
                        if (getDuplicates() != null && getDuplicates().keySet().size() > 0) {
                            getSessionBean().setQuickOrderDuplicates(getDuplicates());
                            for (String s : getDuplicates().keySet()) {
                                for (Integer i : getDuplicates().get(s).keySet()) {
                                    for (String sku : getDuplicates().get(s).get(i)) {
                                    }
                                }
                            }
                            getSessionBean().setHasQuickOrderDuplicates(true);
                        }

                        if (!isNoRedirectAjaxRequest(pRequest)) {
                            redirectOrForward(pRequest, pResponse, getQuickOrderAddItemsToOrderFromListInvalidURL());
                        }
                    } else {
                        getSessionBean().setInvalidItemList(null);
                        getSessionBean().setQuickOrderDuplicates(null);
                        getSessionBean().setHasQuickOrderDuplicates(false);
                        getSessionBean().setHasAddedNewProductToShoppingCart(isProductsAddedToCart);
                        // addAjaxSuccessResponse(pResponse);
                        if (!isNoRedirectAjaxRequest(pRequest)) {
                            if (isProductsAddedToCart) {
                            	getSessionBean().setDifferentShipto(false);
                                redirectOrForward(pRequest, pResponse, getQuickOrderAddItemsToOrderFromListSuccessURL());
                            } else {
                                redirectOrForward(pRequest, pResponse, getQuickOrderAddItemsToOrderFromListErrorURL());
                            }
                        }
                        // redirectOrForward(pRequest, pResponse, getQuickOrderAddItemsToOrderFromListInvalidURL());
                    }

                    getOrderManager().updateOrder(getOrder());
                }
            } catch (Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
                rollback = true;
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
                if (tm != null) {
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException e) {
                        if (isLoggingError()) {
                            logError(e);
                        }
                    }
                }
            }
        }

        return false;
    }

    public String productIdFromSku(String catalogRefId) {
        RepositoryItem skuri = null;
        if (!StringUtils.isBlank(catalogRefId)) {
            try {
                skuri = getOrderManager().getCatalogTools().findSKU(catalogRefId);
                if (skuri == null) {
                    skuri = getOrderManager().getCatalogTools().findSKU(getSixLengthIdService().removeFirstZeros(catalogRefId));
                }
            } catch (RepositoryException e) {
                if (isLoggingError()) {
                    logError(e);
                }
            }
            RepositoryItem prdri = null;
            if (skuri != null) {
                Object[] prdris = ((Set) skuri.getPropertyValue("parentProducts")).toArray();
                prdri = (RepositoryItem) prdris[0];
                if (prdri != null) {
                    // since we have the product lets also set that on the AddCommercItemInfo
                    return prdri.getRepositoryId();
                }
            } else {
                // if product was not found will check if parent org exists, then trying to find
                // product by organization id and alias number map
                RepositoryItem org = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
                if (org != null) {
                    // let's try to find by alias number
                    RepositoryItem productItem = ((CPSPurchaseProcessHelper) getPurchaseProcessHelper()).findProductFormAlias(catalogRefId,
                                    org.getRepositoryId());

                    vlogDebug("prduct found by alias number =:" + productItem);

                    if (productItem != null) {
                        return productItem.getRepositoryId();
                    }
                }
            }
        }
        return null;
    }

    private String findSkuId(String catalogRefId) {
        if (!StringUtils.isBlank(catalogRefId)) {
            RepositoryItem skuItem = null;
            try {
                skuItem = getOrderManager().getCatalogTools().findSKU(catalogRefId);
                if (skuItem == null) {
                    skuItem = getOrderManager().getCatalogTools().findSKU(getSixLengthIdService().removeFirstZeros(catalogRefId));
                    catalogRefId = getSixLengthIdService().removeFirstZeros(catalogRefId);
                }
                if (skuItem != null) {
                    return catalogRefId;
                }
            } catch (RepositoryException e) {
                if (isLoggingError()) {
                    logError(e);
                }
            }

            RepositoryItem org = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
            if (org != null) {
                // let's try to find by alias number
                RepositoryItem productItem = ((CPSPurchaseProcessHelper) getPurchaseProcessHelper()).findProductFormAlias(catalogRefId, org.getRepositoryId());

                vlogDebug("prduct found by alias number =:" + productItem);

                if (productItem != null) {
                    List<RepositoryItem> childSkus = (List<RepositoryItem>) productItem.getPropertyValue("childSKUs");
                    if (childSkus != null && childSkus.size() > 0) {
                        return childSkus.get(0).getRepositoryId();
                    }
                }
            }
        }
        return catalogRefId;
    }

    @Override
    public void postAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        if (!getProfile().isTransient() && getProfile().getPropertyValue(CPSConstants.CSR_USER_ID) != null) {
            // CSR Impersonating User, log action
            getCsrLoggingManager().logAction(2, 1, (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG),
                            (String) getProfile().getPropertyValue(CPSConstants.CSR_USER_ID), getProfile().getRepositoryId(), "CSR added item to cart.");
        } else if (!getProfile().isTransient() && (Integer) getProfile().getPropertyValue(CPSConstants.USER_TYPE) == 3) {
            // Sales rep
            getCsrLoggingManager().logAction(2, 2, (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG), getProfile().getRepositoryId(),
                            null, "Sales Rep added item to cart.");
        }
    }

    /**
     * Method handles "Checkout" button on the cart page to check inventory status and level before user proceeds with checkout Since cart can have multiple
     * line items with the same sku - item might be removed or/and updated based on validation. Appropriate error message will be displayed on the cart page.
     * Also setting flag on the order "allowCheckout" to true in order to make sure user clicked on checkout and not typed in checkout shipping page url -
     * bypassing inventory validation
     *
     * @param pRequest
     * @param pResponse
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public boolean handleCheckInventoryForCheckout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
                    throws ServletException, IOException {

        RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        String myHandleMethod = "CartModifierOrderFormHandler.handleCheckInventoryForCheckout";

        if (isLoggingDebug()) {
            vlogDebug(myHandleMethod + " start");
        }

        if (rrm == null || rrm.isUniqueRequestEntry(myHandleMethod)) {
            Transaction tr = null;
            try {
                tr = ensureTransaction();
                if (getUserLocale() == null) {
                    setUserLocale(getUserLocale(pRequest, pResponse));
                }
                if (!checkFormRedirect(null, getHandleCheckoutErrorURL(), pRequest, pResponse)) {
                    return false;
                }
                List<CommerceItem> cItems = getOrder().getCommerceItems();
                if (cItems != null && !cItems.isEmpty()) {
                    CPSInventoryManager invManager = (CPSInventoryManager) getOrderManager().getOrderTools().getInventoryManager();
                    List<String> arrayListToRemove = new ArrayList<String>();
                    // information for warning/error message display on the cart if cart was updated
                    Map<String, Map<String, ArrayList<String>>> actionList = new HashMap<String, Map<String, ArrayList<String>>>();
                    Map changedItemMap = new HashMap();
                    for (CommerceItem cItem : cItems) {
                        String skuId = cItem.getCatalogRefId();
                        // checking if items needs to be removed(or updated?)
                        if (invManager.isRemoveCartItem(getOrder(), skuId, arrayListToRemove)) {
                            Map<String, ArrayList<String>> messageMap = new HashMap<String, ArrayList<String>>();

                            long inventoryLevel = 0;
                            try {
                                inventoryLevel = invManager.getStockLevel(skuId);
                            } catch (InventoryException e) {
                                if (isLoggingError()) {
                                    logError(e);
                                }
                            }
                            long oldQty = cItem.getQuantity();
                            ArrayList params = new ArrayList();
                            params.add(skuId);
                            params.add(String.valueOf(inventoryLevel));
                            // Quantity of item in the cart is more then qty in the inventory
                            if (inventoryLevel > 0 && oldQty > inventoryLevel) {
                                // inventory level is > 0. So updating this commerce item to inventory level
                                cItem.setQuantity(inventoryLevel);
                                // remember what changed
                                changedItemMap.put(cItem, oldQty);
                                messageMap.put(CPSErrorCodes.WARNING_ITEM_UPDATED, params);
                            } else {
                                arrayListToRemove.add(cItem.getId());
                                // Object[] params = {skuId, inventoryLevel};
                                messageMap.put(CPSErrorCodes.WARNING_ITEM_REMOVED, params);
                            }
                            actionList.put(cItem.getId(), messageMap);
                        }
                    }

                    if (arrayListToRemove != null && !arrayListToRemove.isEmpty()) {
                        setRemovalCommerceIds(arrayListToRemove.toArray(new String[0]));
                    }

                    synchronized (getOrder()) {
                        boolean isUpdate = false;
                        if (!checkFormRedirect(null, getHandleCheckoutErrorURL(), pRequest, pResponse)) {
                            return false;
                        }

                        ((CPSOrderImpl) getOrder()).setAllowCheckout(true);

                        if (getRemovalCommerceIds() != null) {
                            try {
                                deleteItems(pRequest, pResponse);
                                Map extraParams = createRepriceParameterMap();
                                runProcessRepriceOrder(getModifyOrderPricingOp(), getOrder(), getUserPricingModels(), getUserLocale(), getProfile(),
                                                extraParams);
                                isUpdate = true;
                            } catch (Exception exc) {
                                processException(exc, MSG_ERROR_UPDATE_ORDER, pRequest, pResponse);
                            }
                        }
                        // will loop through each and format message to be displayed on the cart
                        if (actionList != null && !actionList.isEmpty()) {
                            Set<String> ciList = actionList.keySet();
                            for (String ciId : ciList) {
                                Map<String, ArrayList<String>> messageMap = actionList.get(ciId);
                                for (Entry<String, ArrayList<String>> messageParams : messageMap.entrySet()) {
                                    ArrayList<String> params = messageParams.getValue();
                                    String skuId = params.get(0);
                                    Object[] objectArray = params.toArray(new Object[0]);
                                    CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, messageParams.getKey(), pRequest.getLocale(),
                                                    skuId, objectArray);
                                }
                            }
                            isUpdate = true;
                        }

                        if (!checkFormRedirect(null, getHandleCheckoutErrorURL(), pRequest, pResponse)) {
                            ((CPSOrderImpl) getOrder()).setAllowCheckout(false);
                            // return false;
                        }
                        if (isUpdate) {
                            updateOrder(getOrder(), MSG_ERROR_UPDATE_ORDER, pRequest, pResponse);
                        }
                    } // synchronized
                }
                // If NO form errors are found, redirect to the success URL.
                // If form errors are found, redirect to the error URL.
                return checkFormRedirect(getHandleCheckoutSuccessURL(), getHandleCheckoutErrorURL(), pRequest, pResponse);
            } finally {
                if (tr != null) {
                    commitTransaction(tr);
                }
                if (rrm != null) {
                    rrm.removeRequestEntry(myHandleMethod);
                }
            }
        } else {
            return false;
        }
    }

    public boolean handleQuickAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleQuickAddItemToOrder";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                preQuickAddItemToOrder(pRequest, pResponse);
                if (!getFormError()) {
                    Map.Entry<String, String> inputData = getItemsInfo().entrySet().iterator().next();
                    String padItem = inputData.getKey();
                    final long qty = extractQtyFromString(inputData.getValue());
                    setQtyList("" + qty);
                    setSkuIdsList("" + padItem);

                    ArrayList<String> invalidSkuSessionList = new ArrayList<String>();
                    addQuickOrderItems(pRequest, pResponse, invalidSkuSessionList);
                    if (getFormError()) {
                        vlogDebug("FORM ERROR REDIRECT HAPPENING");
                        if (isVectorContainsError(getFormExceptions(), CPSErrorCodes.ERR_QTY_ERROR)) {
                            getSessionBean().setInvalidItemList(invalidSkuSessionList);
                            return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(), getQuickOrderAddItemsToOrderFromListInvalidURL(),
                                            pRequest, pResponse);
                        } else {
                            return checkFormRedirect(getQuickOrderAddItemsToOrderFromListSuccessURL(), getQuickOrderAddItemsToOrderFromListErrorURL(), pRequest,
                                            pResponse);
                        }
                    } else {
                        String productId = null;
                        if (getItems() != null && getItems().length > 0) {
                            productId = getItems()[0].getProductId();
                        }
                        getSessionBean().setQtys(null);
                        getSessionBean().setItemIds(null);
                        return handleAddItemToOrderNoRedirect(pRequest, pResponse, invalidSkuSessionList);
                    }
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return false;
    }

    private boolean isVectorContainsError(Vector formExceptions, String error) {
        for (Object exception : formExceptions) {
            if (exception instanceof DropletFormException) {
                String errorCode = ((DropletFormException) getFormExceptions().get(0)).getPropertyPath();
                if (errorCode.compareTo(error) == 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private long extractQtyFromString(String qty) {
        try {
            return Long.parseLong(qty);
        } catch (NumberFormatException e) {
            return 1;
        }
    }

    protected void preQuickAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        if (getItemsInfo() == null || getItemsInfo().size() != 1) {
            createError(CPSErrorCodes.ERR_CHECKOUT_EMPTY_ITEMS_MAP, null, pRequest);
        }
    }

    @Override
    public void preAddMultipleItemsToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        super.preAddMultipleItemsToOrder(pRequest, pResponse);
        if (!getFormError()) {
            boolean isFromMt = Boolean.valueOf(pRequest.getParameter(CPSConstants.IS_ITEMS_FROM_MT));
            AddCommerceItemInfo[] items = getItems();
            if (items != null) {
                for (AddCommerceItemInfo item2 : items) {
                    AddCommerceItemInfo item = item2;
                    String productId = item.getProductId();
                    if (!isFromMt) {
                        boolean valid = checkIfProductPurchasable(pRequest, productId, true);
                        if (valid) {
                            validateMinimumQuantityForProduct(pRequest, productId, item.getQuantity(), true, true);
                        }
                    } else {
                        RepositoryItem product = null;
                        String displayName = "";
                        try {
                            product = getOrderManager().getCatalogTools().findProduct(productId);
                            if (product != null) {
                                displayName = product.getItemDisplayName();
                            }
                        } catch (RepositoryException e) {
                            vlogError("Product not found: " + productId);
                        }
                        boolean valid = checkIfProductPurchasable(pRequest, productId, false);
                        if (!valid) {
                            if (pRequest.getParameter(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE) != null) {
                                this.addFormException(new DropletFormException(
                                                displayName + ": " + pRequest.getParameter(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE), null));
                                pRequest.removeParameter(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE);
                            }
                        } else {
                            if (!validateMinimumQuantityForProduct(pRequest, productId, item.getQuantity(), true, false)) {
                                if (pRequest.getParameter(CPSErrorCodes.ERR_MIN_ORDER_QTY) != null) {
                                    this.addFormException(new DropletFormException(displayName + ": " + pRequest.getParameter(CPSErrorCodes.ERR_MIN_ORDER_QTY),
                                                    null));
                                    pRequest.removeParameter(CPSErrorCodes.ERR_MIN_ORDER_QTY);
                                }
                                if (pRequest.getParameter(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL) != null) {
                                    this.addFormException(new DropletFormException(
                                                    displayName + ": " + pRequest.getParameter(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL), null));
                                    pRequest.removeParameter(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL);
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    @Override
    protected void addMultipleItemsToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        if (!getFormError()) {
            super.addMultipleItemsToOrder(pRequest, pResponse);
            OrderPriceInfo orderPriceInfo = getOrder().getPriceInfo();
            if (orderPriceInfo != null) {
                orderPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
            }
            List<CommerceItem> cis = getOrder().getCommerceItems();
            if (cis != null && !cis.isEmpty()) {
                for (CommerceItem ci : cis) {
                    ItemPriceInfo itemPriceInfo = ci.getPriceInfo();
                    itemPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
                }
            }
        }
    }

    @Override
    public boolean handleAddMultipleItemsToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleAddMultipleItemsToOrder";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (getAvailabilityManager().isJdeAvailable()) {
                    if (getFromAvailabilityModal()) {
                        getCartModifierService().updateProfileFromAvailabilityModal(this);
                    }
                	checkForAbandantOrderEmail(getOrder(),getCurrentProfile());
                    if (VSGFormUtils.isAjaxRequest(pRequest)) {
                        super.handleAddMultipleItemsToOrder(pRequest, pResponse);
                        try {
                            if (!getFormError()) {
                                createLastProductsAddedToCart();
                                getSessionBean().setDifferentShipto(false);
                                VSGFormUtils.addAjaxSuccessResponse(pResponse);
                            } else {
                                VSGFormUtils.addAjaxErrorResponse(this, pResponse);
                            }
                        } catch (JSONException e) {
                            logError(e);
                        }
                        return false;
                    } else {
                        return super.handleAddMultipleItemsToOrder(pRequest, pResponse);
                    }
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        return false;
    }

    public boolean handleUpdateWithRepriceReview(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleUpdateWithRepriceReview";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                Order order = getOrder();
                List<CommerceItem> commerceItems = order.getCommerceItems();
                CommerceItem currentCommerceItem = null;
                double orderAmount = 0;
                for (CommerceItem commerceItem : commerceItems) {
                    ItemPriceInfo itemPriceInfo = commerceItem.getPriceInfo();
                    if (itemPriceInfo != null) {
                        if (getItemsInfo().containsKey(commerceItem.getId())) {
                            currentCommerceItem = commerceItem;
                            String newQty = getItemsInfo().get(commerceItem.getId());
                            long qty = StringUtils.isNotBlank(newQty) && !newQty.equals("0") ? Integer.parseInt(newQty) : 1;
                            validateMinimumQuantityForProduct(pRequest, commerceItem.getCatalogRefId(), qty, true, true);
                        } else {
                            orderAmount += itemPriceInfo.getAmount();
                        }
                    }
                }

                if (!getFormError()) {
                    TransactionDemarcation td = new TransactionDemarcation();
                    boolean rollback = false;
                    try {
                        try {
                            td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
                            synchronized (order) {
                                double itemAmount = 0;
                                if (currentCommerceItem != null) {
                                    ItemPriceInfo itemPriceInfo = currentCommerceItem.getPriceInfo();
                                    if (getItemsInfo().containsKey(currentCommerceItem.getId())) {
                                        String newQty = getItemsInfo().get(currentCommerceItem.getId());
                                        long qty = StringUtils.isNotBlank(newQty) && !newQty.equals("0") ? Integer.parseInt(newQty) : 1;
                                        currentCommerceItem.setQuantity(qty);
                                        double unitPrice = itemPriceInfo.getListPrice();
                                        itemAmount = unitPrice * qty;
                                        itemAmount = Math.round(itemAmount * 100);
                                        itemAmount = itemAmount / 100;
                                        itemPriceInfo.setAmount(itemAmount);
                                        itemPriceInfo.setAmountIsFinal(true);
                                        itemPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
                                    } else {
                                        itemAmount = itemPriceInfo.getAmount();
                                    }
                                    CatalogTools catalogTools = getCatalogTools();
                                    if (catalogTools instanceof CPSCatalogTools) {
                                        if (((CPSCatalogTools) catalogTools).isItemValid(currentCommerceItem)) {
                                            orderAmount += itemAmount;
                                        }
                                    } else {
                                        orderAmount += itemAmount;
                                    }
                                }
                                OrderPriceInfo orderPriceInfo = order.getPriceInfo();
                                if (orderPriceInfo != null) {
                                    orderPriceInfo.setAmount(orderAmount);
                                    orderPriceInfo.setRawSubtotal(orderAmount);
                                    orderPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
                                }

                                List<Relationship> relationships = order.getRelationships();
                                for (Relationship rel : relationships) {
                                    String relType = rel.getRelationshipClassType();
                                    if (SHIPPING_GROUP_COMMERCE_ITEM_CL_TYPE.equals(relType)) {
                                        ShippingGroupCommerceItemRelationship shipGrCItemRel = (ShippingGroupCommerceItemRelationship) rel;
                                        String commItemId = ((CommerceItemImpl) shipGrCItemRel.getCommerceItem()).getRepositoryItem().getRepositoryId();

                                        if (getItemsInfo().containsKey(commItemId)) {
                                            String newQty = getItemsInfo().get(commItemId);
                                            long qty = StringUtils.isNotBlank(newQty) && !newQty.equals("0") ? Integer.parseInt(newQty) : 1;
                                            shipGrCItemRel.setQuantity(qty);
                                        }

                                    }
                                }

                                getOrderManager().updateOrder(order);
                                addJsonToResponse(JSON_RESPONSE_ORDER_SUBTOTAL, orderAmount);
                                addJsonToResponse(JSON_RESPONSE_ORDER_TOTAL_ITEM_COUNT, order.getTotalCommerceItemCount());
                            }
                        } catch (Exception e) {
                            rollback = true;
                            if (isLoggingError()) {
                                logError(e);
                            }
                        } finally {
                            td.end(rollback);
                        }
                    } catch (TransactionDemarcationException e) {
                        if (isLoggingError()) {
                            logError(e);
                        }
                    }
                }
                try {
                    VSGFormUtils.addAjaxErrorResponse(this, pResponse);
                } catch (JSONException e) {
                    vlogError(e, "Error");
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return false;
    }

    protected void preCheckAvailability(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        if (getItemsInfo() == null || getItemsInfo().isEmpty()) {
            createError(CPSErrorCodes.ERR_CHECKOUT_EMPTY_ITEMS_MAP, null, pRequest);
        }
    }

    public boolean handleCheckAvailability(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleCheckAvailability";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                preCheckAvailability(pRequest, pResponse);
                if (!getFormError()) {

                    TransactionDemarcation td = new TransactionDemarcation();
                    boolean rollback = false;
                    try {
                        try {
                            td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
                            Order order = getOrder();
                            synchronized (order) {
                                double orderAmount = 0;
                                List<CommerceItem> commerceItems = order.getCommerceItems();
                                for (CommerceItem commerceItem : commerceItems) {
                                    CPSCommerceItemImpl itemImpl = (CPSCommerceItemImpl) commerceItem;
                                    if (isResetCheckAvailability()) {
                                        itemImpl.setIsAvailabilityChecked(false);
                                    }
                                    boolean isAvailabilityChecked = itemImpl.getIsAvailabilityChecked();
                                    ItemPriceInfo itemPriceInfo = commerceItem.getPriceInfo();
                                    if (itemPriceInfo != null) {
                                        if (!isAvailabilityChecked) {
                                            if (getItemsInfo().containsKey(commerceItem.getId())) {
                                                long availableQty = 0;
                                                int leadTime = 0;
                                                try {
                                                    Profile profile = (Profile) getProfile();
                                                    String cartSelectedDeliveryMethod = (String) profile.getPropertyValue(CART_SELECTED_DELIVERY_METHOD);
                                                    
                                                    if (cartSelectedDeliveryMethod != null && cartSelectedDeliveryMethod.trim().equalsIgnoreCase("pick-up")) {
                                                        List<InventoryAvailabilityInfo> infos = getExternalProductService().requestStoreInventoryFromCart(
                                                                        commerceItem.getCatalogRefId(), profile, (CPSOrderImpl) order,
                                                                        commerceItem.getQuantity());
                                                        if (!infos.isEmpty()) {
                                                            for (InventoryAvailabilityInfo info : infos) {
                                                                if (info != null && info.isBranchDefault()) {
                                                                    availableQty = info == null ? 0 : info.getQuantity();
                                                                    leadTime = info == null ? 0 : info.getLeadTime();
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                    	List<Integer> infoLeadTimes = new ArrayList<Integer>();
                                                        List<InventoryAvailabilityInfo> infos = getExternalProductService().requestAllInventory(
                                                                        commerceItem.getCatalogRefId(), profile, (CPSOrderImpl) order,
                                                                        commerceItem.getQuantity());
                                                        if (!infos.isEmpty()) {
                                                            for (InventoryAvailabilityInfo info : infos) {
                                                                if(info.isBranchDefault()){
                                                                	itemImpl.setDefaultBranchQty((int) info.getQuantity());
                                                                } else {
                                                                	availableQty += info.getQuantity();
                                                                    infoLeadTimes.add(info.getLeadTime());
                                                                }
                                                            }

                                                            if (availableQty == 0) {
                                                            	Collections.sort(infoLeadTimes);
                                                            	leadTime=infoLeadTimes.get(0);
                                                            }
                                                            vlogDebug("leadTime :{0}",leadTime);

                                                        }
                                                    }

                                                    itemImpl.setIsAvailabilityChecked(true);
                                                } catch (Exception ex) {
                                                    if (isLoggingError()) {
                                                        logError(ex);
                                                    }
                                                }
                                                int availableQtyInt = (int) availableQty;
                                                if (availableQtyInt < 0) {
                                                    availableQtyInt = 0;
                                                }
                                                itemImpl.setAvailableQuantity(availableQtyInt);
                                                itemImpl.setLeadTime(leadTime);
                                            }
                                        }
                                        CatalogTools catalogTools = getCatalogTools();
                                        if (catalogTools instanceof CPSCatalogTools) {
                                            if (((CPSCatalogTools) catalogTools).isItemValid(commerceItem)) {
                                                orderAmount += itemPriceInfo.getAmount();
                                            }
                                        } else {
                                            orderAmount += itemPriceInfo.getAmount();
                                        }

                                    }
                                }

                                updateOrderPriceInfo(order, orderAmount);
                            }
                        } catch (Exception e) {
                            rollback = true;
                            if (isLoggingError()) {
                                logError(e);
                            }
                        } finally {
                            td.end(rollback);
                        }
                    } catch (TransactionDemarcationException e) {
                        if (isLoggingError()) {
                            logError(e);
                        }
                    }
                }
                ajaxResponse(pResponse);
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return false;
    }

    private void updateOrderPriceInfo(Order pOrder, double pOrderAmount) throws CommerceException {
        OrderPriceInfo orderPriceInfo = pOrder.getPriceInfo();
        if (orderPriceInfo != null) {
            orderPriceInfo.setAmount(pOrderAmount);
            orderPriceInfo.setRawSubtotal(pOrderAmount);
            orderPriceInfo.setAmountIsFinal(true);
            orderPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
        }
        getOrderManager().updateOrder(pOrder);
        addJsonToResponse(JSON_RESPONSE_ORDER_SUBTOTAL, pOrderAmount);
        addJsonToResponse(JSON_RESPONSE_ORDER_TOTAL_ITEM_COUNT, pOrder.getTotalCommerceItemCount());
    }

    protected void preItemsRemoveWithReprice(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        if (getItemsInfo() == null || getItemsInfo().isEmpty()) {
            createError(CPSErrorCodes.ERR_CHECKOUT_EMPTY_ITEMS_MAP, null, pRequest);
        }
    }

    public boolean handleItemsRemoveWithReprice(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleItemsRemoveWithReprice";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                preItemsRemoveWithReprice(pRequest, pResponse);
                if (!getFormError()) {

                    TransactionDemarcation td = new TransactionDemarcation();
                    boolean rollback = false;
                    try {
                        try {
                            td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
                            Order order = getOrder();
                            synchronized (order) {
                                for (String cid : getItemsInfo().keySet()) {
                                    getCommerceItemManager().removeItemFromOrder(order, cid);
                                }
                                getItemsInfo().clear();
                                double orderAmount = 0;
                                List<CommerceItem> commerceItems = order.getCommerceItems();
                                for (CommerceItem commerceItem : commerceItems) {
                                    ItemPriceInfo itemPriceInfo = commerceItem.getPriceInfo();
                                    CatalogTools catalogTools = getCatalogTools();
                                    if (catalogTools instanceof CPSCatalogTools) {
                                        if (((CPSCatalogTools) catalogTools).isItemValid(commerceItem) && itemPriceInfo != null) {
                                            orderAmount += itemPriceInfo.getAmount();
                                        }
                                    } else if (itemPriceInfo != null) {
                                        orderAmount += itemPriceInfo.getAmount();
                                    }
                                }
                                updateOrderPriceInfo(order, orderAmount);
                                if(order.getCommerceItemCount() == 0){
                                    Profile profile = getCurrentProfile();
                                    profile.setPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL, false);
                                    vlogInfo("order Notification set to {0} on removing all items from the cart for {1}", profile.getPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL),profile.getRepositoryId());
                                    profile.setPropertyValue(CPSConstants.FIRST_ITEM_TO_CURRENT_CART, null);
                                }
                            }
                        } catch (Exception e) {
                            rollback = true;
                            if (isLoggingError()) {
                                logError(e);
                            }
                        } finally {
                            td.end(rollback);
                        }
                    } catch (TransactionDemarcationException e) {
                        if (isLoggingError()) {
                            logError(e);
                        }
                    }
                }
                ajaxResponse(pResponse);
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return false;
    }

    @Override
    public void preMoveToPurchaseInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        Profile profile = getCurrentProfile();

        if (StringUtils.isBlank(getDeliveryMethod())) {
            createError(CPSErrorCodes.ERR_CHECKOUT_INVALID_DELIVERY_METHOD, "deliveryMethod", pRequest);
        } else {
            if (DELEVERY_METHOD_SHIPPED.equals(getDeliveryMethod())) {
                if (StringUtils.isBlank(getShippingAddressId()) && !profile.isTransient()) {
                    createError(CPSErrorCodes.ERR_CHECKOUT_EMPTY_SHIPPING_ADDRESS, "shippingAddress", pRequest);
                }
            } else if (DELEVERY_METHOD_PICK_UP.equals(getDeliveryMethod()) && StringUtils.isBlank(getShippingAddressId())) {
                createError(CPSErrorCodes.ERR_CHECKOUT_EMPTY_PICKUP_LOCATION_ADDRESS, "shippingAddress", pRequest);
            }
        }

        if (!profile.isTransient() && StringUtils.isBlank(getBillingAddressId())) {
            createError(CPSErrorCodes.ERR_CHECKOUT_EMPTY_BILLING_ADDRESS, "billingAddress", pRequest);
        }

        if (!getFormError()) {
            RepositoryItem shipAddress = (RepositoryItem) profile.getPropertyValue(CPSConstants.SELECTED_CS);
            if (shipAddress != null) {
                RepositoryItem billingOrganization = getProfileTools()
                                .findOrgFromShippingAddress((RepositoryItem) profile.getPropertyValue(CPSConstants.PARENT_ORG), shipAddress);
                if (billingOrganization != null) {
                    RepositoryItem billingAddress = (RepositoryItem) billingOrganization.getPropertyValue(CPSConstants.BILLING_ADDRESS);
                    if (billingAddress != null) {
                        setBillingAddressId(billingAddress.getRepositoryId());
                    }
                }
            }
            Set<String> ids = new HashSet<>();
            List<CommerceItem> commerceItems = getOrder().getCommerceItems();
            for (CommerceItem commerceItem : commerceItems) {
                if (!isValidCommerceItemToCheckout(commerceItem)) {
                    ids.add(commerceItem.getId());
                }
            }
            for (String cid : ids) {
                try {
                    getCommerceItemManager().removeItemFromOrder(getOrder(), cid);
                    if (getOrder().getCommerceItemCount() == 0) {
                        createError(CPSErrorCodes.ERR_CHECKOUT_EMPTY_ORDER, "emptyOrder", pRequest);
                    }
                } catch (CommerceException e) {
                    if (isLoggingError()) {
                        logError(e);
                    }
                    createError(CPSErrorCodes.ERR_CHECKOUT_ERROR_MODIFYING_ORDER, "orderError", pRequest);
                }
            }
        }
    }

    private boolean isValidCommerceItemToCheckout(CommerceItem pCommerceItem) {
        if (pCommerceItem instanceof CPSCommerceItemImpl) {
            CPSCommerceItemImpl commerceItem = (CPSCommerceItemImpl) pCommerceItem;
            ItemPriceInfo priceInfo = commerceItem.getPriceInfo();
            // if (commerceItem.getAvailableQuantity() > 0 && commerceItem.getQuantity() > 0 && priceInfo != null && priceInfo.getListPrice() > 0) {
            if (commerceItem.getQuantity() > 0 && priceInfo != null && priceInfo.getListPrice() > 0) { // updated according to CPS-483
                return ((CPSCatalogTools) getCatalogTools()).isItemValid(commerceItem);
            }
        }
        return false;
    }

    @Override
    public boolean handleMoveToPurchaseInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleMoveToPurchaseInfo";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                super.handleMoveToPurchaseInfo(pRequest, pResponse);
                ajaxResponse(pResponse);
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        return false;
    }

    @Override
    public void postMoveToPurchaseInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        super.postMoveToPurchaseInfo(pRequest, pResponse);

        Profile profile = getCurrentProfile();

        if (!getFormError()) {
            Order order = getOrder();
            try {
                if (profile.isTransient()) {
                    if (DELEVERY_METHOD_PICK_UP.equals(getDeliveryMethod())) {
                        getCartModifierService().updateShippingGroups(getDeliveryMethod(), getShippingAddressId(), order, getProfile());
                    } else {
                        getCartModifierService().createHardgoodShipGroupIfNotExists(order);
                    }
                } else {
                    getCartModifierService().updateShippingGroups(getDeliveryMethod(), getShippingAddressId(), order, getProfile());
                    CPSInvoiceRequest invoiceRequest = (CPSInvoiceRequest) getCartModifierService().createInvoiceRequestPayGroupIfNotExists(order);
                    CPSContactInfo billingAddress = getCartModifierService().findContactInfoById(getBillingAddressId());
                    invoiceRequest.setBillingAddress(billingAddress);
                    invoiceRequest.setJdeAddressNumber(billingAddress.getJdeAddressNumber());

                    getOrderManager().addRemainingOrderAmountToPaymentGroup(order, invoiceRequest.getId());
                }
            } catch (RepositoryException | CommerceException e) {
                if (isLoggingError()) {
                    logError(e);
                }
                createError(CPSErrorCodes.ERR_CHECKOUT_ERROR_MODIFYING_ORDER, null, pRequest);
            }
        }
        if (!getFormError() && !profile.isTransient()) {
            RepositoryItem profileItem = getProfile();
            RepositoryItem parentOrg = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
            if (parentOrg != null && null != getLoginService() && null != getLoginSessionInfo()) {
                boolean isUserOnHold = getLoginService().isUserOnHold(parentOrg.getRepositoryId());
                ((MutableRepositoryItem) profileItem).setPropertyValue(CPSConstants.ON_HOLD, isUserOnHold);
                getLoginSessionInfo().setOnHold(isUserOnHold);
                if (isUserOnHold) {
                    createError(CPSErrorCodes.ERR_CREDIT_HOLD, "onHold", pRequest);
                }
            }
        }
    }

    @Override
    public boolean handleAddItemToOrder(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse)
                    throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleAddItemToOrder";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (getAvailabilityManager().isJdeAvailable()) {
                    if (isGiftIds()) {
                        validateMinimumQuantityMaterialList(pRequest);
                    } else {
                        validateMinimumQuantitForProducts(pRequest);
                    }
                    
                    if (!getFormError()) {
                        getSessionBean().setDifferentShipto(false);
                        checkForAbandantOrderEmail(getOrder(),getCurrentProfile());
                        super.handleAddItemToOrder(pRequest, pResponse);
                    }
                    try {
                        if (!getFormError()) {
                            VSGFormUtils.addAjaxSuccessResponse(pResponse);
                        } else {
                            VSGFormUtils.addAjaxErrorResponse(this, pResponse);
                        }
                    } catch (JSONException e) {
                        vlogError(e, "Error");
                    }
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return false;

    }

    @Override
    protected void addItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        super.addItemToOrder(pRequest, pResponse);
        createLastProductsAddedToCart();
        OrderPriceInfo orderPriceInfo = getOrder().getPriceInfo();
        if (orderPriceInfo != null) {
            orderPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
        }
        List<CommerceItem> cis = getOrder().getCommerceItems();
        if (cis != null && !cis.isEmpty()) {
            for (CommerceItem ci : cis) {
                ItemPriceInfo itemPriceInfo = ci.getPriceInfo();
                itemPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
            }
        }
    }

    public boolean handleAddItemToAutoOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
                    throws InvalidParameterException, CommerceItemNotFoundException, ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleAddItemToAutoOrder";

        TransactionManager tm = getTransactionManager();
        TransactionDemarcation td = new TransactionDemarcation();
        boolean rollback = false;
        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (tm != null) {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }

                super.handleAddItemToOrder(pRequest, pResponse);

                synchronized (getOrder()) {
                    Integer scheduleDays = Integer.parseInt(getScheduleDays());
                    for (int i = 0; i < getItems().length; i++) {
                        String catalogRefId = getItems()[i].getCatalogRefId();
                        CPSCommerceItemImpl commerceItem = (CPSCommerceItemImpl) getOrder().getCommerceItemsByCatalogRefId(catalogRefId).get(0);
                        commerceItem.setScheduledItem(true);
                        commerceItem.setScheduleDays(scheduleDays);
                    }
                    getOrderManager().updateOrder(getOrder());
                }
            } catch (Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
                rollback = true;
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
                if (tm != null) {
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException e) {
                        if (isLoggingError()) {
                            logError(e);
                        }
                    }
                }
            }
        }
        ajaxResponse(pResponse);
        return false;
    }

    public boolean handleUpdateScheduleDays(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleUpdateScheduleDays";

        TransactionManager tm = getTransactionManager();
        TransactionDemarcation td = new TransactionDemarcation();
        boolean rollback = false;

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (tm != null) {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }
                synchronized (getOrder()) {
                    List<CPSCommerceItemImpl> commerceItems = getOrder().getCommerceItems();
                    for (CPSCommerceItemImpl commerceItem : commerceItems) {
                        String radioButtonReorder = pRequest.getParameter("ctr_reorder_" + commerceItem.getProductId());
                        if ("every".equals(radioButtonReorder)) {
                            Integer scheduleDays = Integer.parseInt(pRequest.getParameter("scheduled-days-field-" + commerceItem.getProductId()));
                            commerceItem.setScheduledItem(true);
                            commerceItem.setScheduleDays(scheduleDays);
                        } else {
                            commerceItem.setScheduledItem(false);
                            commerceItem.setScheduleDays(0);
                        }
                    }

                    getOrderManager().updateOrder(getOrder());
                }
                ajaxResponse(pResponse);
            } catch (Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
                rollback = true;
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
                if (tm != null) {
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException e) {
                        if (isLoggingError()) {
                            logError(e);
                        }
                    }
                }
            }
        }
        return false;
    }

    private void createError(String msg, String pty, DynamoHttpServletRequest pRequest) {
        CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
    }

    private void createError(String msg, String pty, DynamoHttpServletRequest pRequest, Object[] pParams) {
        CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty, pParams);
    }

    private boolean ajaxResponse(DynamoHttpServletResponse pResponse) {
        try {
            VSGFormUtils.ajaxJSONResponse(this, pResponse, mJsonResponse);
        } catch (IOException | JSONException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }
        mJsonResponse.clear();
        return false;
    }

    private boolean response(DynamoHttpServletResponse pResponse) {
        AjaxUtils.addAjaxResponse(this, pResponse, null);
        return false;
    }

    private boolean response(DynamoHttpServletResponse pResponse, Map<String, Object> params) {
        AjaxUtils.addAjaxResponse(this, pResponse, params);
        return false;
    }

    private void addJsonToResponse(String key, Object value) {
        mJsonResponse.put(key, value);
    }

    private void createLastProductsAddedToCart() {
        mLastProductsAddedToCart.setProducts(new LinkedHashMap<String, Long>());
        mLastProductsAddedToCart.setRelatedProducts(new ArrayList<String>());
        AddCommerceItemInfo[] items = getItems();
        if (items != null) {
            for (AddCommerceItemInfo item : items) {
                List<RepositoryItem> relatedProducts = null;
                try {
                    RepositoryItem product = getProductRepository().getItem(item.getProductId(), "product");
                    if (product != null) {
                        relatedProducts = (List<RepositoryItem>) product.getPropertyValue("relatedProducts");
                    }
                } catch (RepositoryException e) {
                    vlogError(e, "Error");
                }
                mLastProductsAddedToCart.getProducts().put(item.getProductId(), item.getQuantity());
                if (relatedProducts != null) {
                    for (RepositoryItem product : relatedProducts) {
                        String repositoryId = product.getRepositoryId();
                        mLastProductsAddedToCart.getRelatedProducts().add(repositoryId);
                    }
                }
            }
        }
    }

    private void validateMinimumQuantityMaterialList(DynamoHttpServletRequest pRequest) {
        if (!getFormError()) {
            String[] skuIds = getSkuIdsList().split(",");
            String[] qtyList = getQtyList().split(",");
            if (skuIds.length != 0) {
                for (int i = 0; i < skuIds.length; i++) {
                    String productId = null;
                    try {
                        productId = (String) getGiftlistManager().getGiftitem(skuIds[i]).getPropertyValue(CPSConstants.CATALOG_REF_ID);
                    } catch (CommerceException e) {
                        vlogError("Product not found: " + skuIds[i]);
                    }
                    Long qty = Long.valueOf(qtyList[i]);
                    boolean valid = checkIfProductPurchasable(pRequest, productId, true);
                    if (valid) {
                        validateMinimumQuantityForProduct(pRequest, productId, qty, true, true);
                    }
                }
            }

        }
    }

    private void validateMinimumQuantitForProducts(DynamoHttpServletRequest pRequest) {
        if (!getFormError()) {
            String[] skuIds = getSkuIdsList().split(",");
            String[] qtyList = getQtyList().split(",");
            if (skuIds.length != 0) {
                for (int i = 0; i < skuIds.length; i++) {
                    String productId = productIdFromSku(skuIds[i]);
                    Long qty = Long.valueOf(qtyList[i]);
                    boolean valid = checkIfProductPurchasable(pRequest, productId, true);
                    if (valid) {
                        validateMinimumQuantityForProduct(pRequest, productId, qty, true, true);
                    }
                }
            }
        }
    }

    public boolean checkIfProductPurchasable(DynamoHttpServletRequest pRequest, String productId, boolean isCreateFormError) {
        boolean isValid = false;
        if (productId != null) {
            RepositoryItem product = null;
            try {
                product = getOrderManager().getCatalogTools().findProduct(productId);
            } catch (RepositoryException e) {
                vlogError("Product not found: " + productId);
            }
            if (product != null) {
                if (getCatalogTools() instanceof CPSCatalogTools) {
                    isValid = ((CPSCatalogTools) getCatalogTools()).isProductPurchasable(product);
                    if (!isValid) {
                        if (isCreateFormError) {
                            createError(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE, "errProductNotPurchasable", pRequest, null);
                        } else {
                            pRequest.setParameter(CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE, CPSMessageUtils.getFormattedMessage(
                                            CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE, pRequest.getLocale(), null));
                        }
                    }
                }
            }
        }
        return isValid;
    }

    public boolean validateMinimumQuantityForProduct(DynamoHttpServletRequest pRequest, String productId, long qty, boolean isAddProductId,
                    boolean isCreateFormError) {
        boolean isValid = true;
        RepositoryItem product = null;
        try {
            product = getOrderManager().getCatalogTools().findProduct(productId);
        } catch (RepositoryException e) {
            vlogError("Product not found: " + productId);
        }
        if (product != null) {
            productId = product.getRepositoryId();
            Integer minOrderQty = (Integer) product.getPropertyValue(MINIMUM_ORDER_QTY);
            if (minOrderQty != null) {
                if (qty < minOrderQty) {
                    Object[] params;
                    if (isAddProductId) {
                        params = new Object[] { minOrderQty + "-" + productId };
                    } else {
                        params = new Object[] { minOrderQty };
                    }
                    if (isCreateFormError) {
                        createError(CPSErrorCodes.ERR_MIN_ORDER_QTY, "minQtyError", pRequest, params);
                    } else {
                        pRequest.setParameter(CPSErrorCodes.ERR_MIN_ORDER_QTY, CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY,
                                        CPSErrorCodes.ERR_MIN_ORDER_QTY, pRequest.getLocale(), params));
                        pRequest.setParameter("minOrderQty", minOrderQty);
                    }
                    isValid = false;
                }
            } else {
                minOrderQty = 0;
            }

            Integer orderQtyInterval = (Integer) product.getPropertyValue(ORDER_QTY_INTERVAL);
            if (orderQtyInterval != null) {
                if ((qty - orderQtyInterval) % orderQtyInterval != 0) {
                    Object[] params;
                    if (isAddProductId) {
                        params = new Object[] { orderQtyInterval + "-" + productId };
                    } else {
                        params = new Object[] { orderQtyInterval };
                    }
                    if (isCreateFormError) {
                        createError(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL, "minQtyError", pRequest, params);
                    } else {
                        pRequest.setParameter(CPSErrorCodes.ERR_ORDER_QTY_INTERVAL, CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY,
                                        CPSErrorCodes.ERR_ORDER_QTY_INTERVAL, pRequest.getLocale(), params));
                        pRequest.setParameter("orderQtyInterval", orderQtyInterval);
                    }
                    isValid = false;
                }
            }
        }
        return isValid;
    }

    public boolean handleSetPONumber(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCartModifierFormHandler.handleSetPONumber";

        TransactionManager tm = getTransactionManager();
        TransactionDemarcation td = new TransactionDemarcation();
        boolean rollback = false;

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                if (tm != null) {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }
                String poNumber = pRequest.getParameter(CPSConstants.PO_NUMBER);
                synchronized (getOrder()) {
                    CPSOrderImpl order = null;
                    if (getOrder() instanceof CPSOrderImpl) {
                        order = (CPSOrderImpl) getOrder();
                    }
                    if (order != null) {
                        order.setPONumber(poNumber);
                    }
                    getOrderManager().updateOrder(order);
                }
                ajaxResponse(pResponse);
            } catch (Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
                rollback = true;
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
                if (tm != null) {
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException e) {
                        if (isLoggingError()) {
                            logError(e);
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean handleCompareProducts(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
                    throws ServletException, IOException, RepositoryException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSProfileFormHandler.handleCompareProducts";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                CPSSessionBean sessionBean = getSessionBean();
                sessionBean.setComparisonReferer(getComparisonReferer());
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        return response(pResponse);
    }
    public void checkForAbandantOrderEmail(Order order,Profile profile){
    	if(order.getCommerceItemCount() == 0){
            profile.setPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL, true);
            vlogInfo("order Notification set to {0} on adding 1st items to the cart for {1}", profile.getPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL),profile.getRepositoryId());
            profile.setPropertyValue(CPSConstants.FIRST_ITEM_TO_CURRENT_CART, new Date());


    	}
    }
    // Getter/Setter section

    public boolean isNotAddedItemsFlag() {
        return notAddedItemsFlag;
    }

    public void setNotAddedItemsFlag(boolean notAddedItemsFlag) {
        this.notAddedItemsFlag = notAddedItemsFlag;
    }

    public CPSSessionBean getSessionBean() {
        return mSessionBean;
    }

    public void setSessionBean(CPSSessionBean mSessionBean) {
        this.mSessionBean = mSessionBean;
    }

    @Override
    public String getSkuIdsList() {
        return skuIdsList;
    }

    @Override
    public void setSkuIdsList(String skuIdsList) {
        this.skuIdsList = skuIdsList;
    }

    public Map<String, String> getSkuQtyMap() {
        return skuQtyMap;
    }

    public void setSkuQtyMap(Map<String, String> skuQtyMap) {
        this.skuQtyMap = skuQtyMap;
    }

    public boolean isGiftIds() {
        return giftIds;
    }

    public boolean isPaIds() {
        return paIds;
    }

    public void setPaIds(boolean paIds) {
        this.paIds = paIds;
    }

    public String getPaitemlist() {

        return paitemlist;
    }

    public String getScheduleDays() {
        return scheduleDays;
    }

    public void setScheduleDays(String scheduleDays) {
        this.scheduleDays = scheduleDays;
    }

    public void setPaitemlist(String paitemlist) {
        this.paitemlist = paitemlist;
    }

    public void setGiftIds(boolean giftIds) {
        this.giftIds = giftIds;
    }

    @Override
    public String getQtyList() {
        return qtyList;
    }

    @Override
    public void setQtyList(String qtyList) {
        this.qtyList = qtyList;
    }

    public CPSCSRLoggingManager getCsrLoggingManager() {
        return csrLoggingManager;
    }

    public void setCsrLoggingManager(CPSCSRLoggingManager csrLoggingManager) {
        this.csrLoggingManager = csrLoggingManager;
    }

    public Repository getProductRepository() {
        return mProductRepository;
    }

    public void setProductRepository(Repository mProductRepository) {
        this.mProductRepository = mProductRepository;
    }

    public String getQuickOrderAddItemsToOrderFromListErrorURL() {
        return quickOrderAddItemsToOrderFromListErrorURL;
    }

    public void setQuickOrderAddItemsToOrderFromListErrorURL(String quickOrderAddItemsToOrderFromListErrorURL) {
        this.quickOrderAddItemsToOrderFromListErrorURL = quickOrderAddItemsToOrderFromListErrorURL;
    }

    public String getQuickOrderAddItemsToOrderFromListSuccessURL() {
        return quickOrderAddItemsToOrderFromListSuccessURL;
    }

    public void setQuickOrderAddItemsToOrderFromListSuccessURL(String quickOrderAddItemsToOrderFromListSuccessURL) {
        this.quickOrderAddItemsToOrderFromListSuccessURL = quickOrderAddItemsToOrderFromListSuccessURL;
    }

    public String getHandleCheckoutSuccessURL() {
        return handleCheckoutSuccessURL;
    }

    public void setHandleCheckoutSuccessURL(String handleCheckoutSuccessURL) {
        this.handleCheckoutSuccessURL = handleCheckoutSuccessURL;
    }

    public String getHandleCheckoutErrorURL() {
        return mHandleCheckoutErrorURL;
    }

    public void setHandleCheckoutErrorURL(String pHandleCheckoutErrorURL) {
        mHandleCheckoutErrorURL = pHandleCheckoutErrorURL;
    }

    public String getComparisonReferer() {
        return mComparisonReferer;
    }

    public void setComparisonReferer(String pComparisonReferer) {
        mComparisonReferer = pComparisonReferer;
    }

    public Map<String, String> getItemsInfo() {
        return mItemsInfo;
    }

    public void setItemsInfo(Map<String, String> pItemsInfo) {
        mItemsInfo = pItemsInfo;
    }

    public CPSExternalProductService getExternalProductService() {
        return externalProductService;
    }

    public void setExternalProductService(CPSExternalProductService externalProductService) {
        this.externalProductService = externalProductService;
    }

    public String getPoNumber() {
        return mPoNumber;
    }

    public void setPoNumber(String pPoNumber) {
        mPoNumber = pPoNumber;
    }

    public String getShippingAddressId() {
        return mShippingAddressId;
    }

    public void setShippingAddressId(String pShippingAddressId) {
        mShippingAddressId = pShippingAddressId;
    }

    public String getBillingAddressId() {
        return mBillingAddressId;
    }

    public void setBillingAddressId(String pBillingAddressId) {
        mBillingAddressId = pBillingAddressId;
    }

    public String getDeliveryMethod() {
        return mDeliveryMethod;
    }

    public void setDeliveryMethod(String pDeliveryMethod) {
        this.mDeliveryMethod = pDeliveryMethod;
    }

    public LoginSessionInfo getLoginSessionInfo() {
        return mLoginSessionInfo;
    }

    public void setLoginSessionInfo(LoginSessionInfo pLoginSessionInfo) {
        mLoginSessionInfo = pLoginSessionInfo;
    }

    public LoginService getLoginService() {
        return mLoginService;
    }

    public void setLoginService(LoginService pLoginService) {
        mLoginService = pLoginService;
    }

    public CPSProfileTools getProfileTools() {
        return profileTools;
    }

    public void setProfileTools(CPSProfileTools profileTools) {
        this.profileTools = profileTools;
    }

    public void setProductCatalog(MutableRepository productCatalog) {
        this.productCatalog = productCatalog;
    }

    public MutableRepository getProductCatalog() {
        return productCatalog;
    }

    public void setQuickOrderAddItemsToOrderFromListInvalidURL(String quickOrderAddItemsToOrderFromListInvalidURL) {
        this.quickOrderAddItemsToOrderFromListInvalidURL = quickOrderAddItemsToOrderFromListInvalidURL;
    }

    public String getQuickOrderAddItemsToOrderFromListInvalidURL() {
        return quickOrderAddItemsToOrderFromListInvalidURL;
    }

    @Override
    public void setDuplicates(HashMap<String, HashMap<Integer, ArrayList<String>>> quickOrderDuplicates) {
        this.quickOrderDuplicates = quickOrderDuplicates;
    }

    @Override
    public HashMap<String, HashMap<Integer, ArrayList<String>>> getDuplicates() {
        return quickOrderDuplicates;
    }

    @Override
    public void setFinalSkus(ArrayList<String> quickOrderFinalSkus) {
        this.quickOrderFinalSkus = quickOrderFinalSkus;
    }

    @Override
    public ArrayList<String> getFinalSkus() {
        return quickOrderFinalSkus;
    }

    @Override
    public void setFinalQuantities(ArrayList<Integer> quickOrderFinalQuantities) {
        this.quickOrderFinalQuantities = quickOrderFinalQuantities;
    }

    @Override
    public ArrayList<Integer> getFinalQuantities() {
        return quickOrderFinalQuantities;
    }

    @Override
    public void setAddFromErrorModal(String addFromErrorModal) {
        this.addFromErrorModal = addFromErrorModal;
    }

    @Override
    public String getAddFromErrorModal() {
        return addFromErrorModal;
    }

    public CPSCartModifierService getCartModifierService() {
        return mCartModifierService;
    }

    public void setCartModifierService(CPSCartModifierService pCartModifierService) {
        this.mCartModifierService = pCartModifierService;
    }

    public String getShippingMethodOnAvailabilityModal() {
        return mShippingMethodOnAvailabilityModal;
    }

    public void setShippingMethodOnAvailabilityModal(String pShippingMethodOnAvailabilityModal) {
        this.mShippingMethodOnAvailabilityModal = pShippingMethodOnAvailabilityModal;
    }

    public Profile getCurrentProfile() {
        return mCurrentProfile;
    }

    public void setCurrentProfile(Profile pProfile) {
        this.mCurrentProfile = pProfile;
    }

    public String getSelectedLocationId() {
        return mSelectedLocationId;
    }

    public void setSelectedLocationId(String pSelectedLocationId) {
        this.mSelectedLocationId = pSelectedLocationId;
    }

    public SixLengthIdService getSixLengthIdService() {
        return mSixLengthIdService;
    }

    public void setSixLengthIdService(SixLengthIdService pSixLengthIdService) {
        this.mSixLengthIdService = pSixLengthIdService;
    }

    public DuplicatesMatchesService getDuplicatesMatchesService() {
        return mDuplicatesMatchesService;
    }

    public void setDuplicatesMatchesService(DuplicatesMatchesService pDuplicatesMatchesService) {
        this.mDuplicatesMatchesService = pDuplicatesMatchesService;
    }

    /**
     * Gets mOrderItems.
     *
     * @return Value of mOrderItems.
     */
    public Boolean getOrderItems() {
        return mOrderItems;
    }

    /**
     * Sets new mOrderItems.
     *
     * @param pOrderItems
     *            New value of mOrderItems.
     */
    public void setOrderItems(Boolean pOrderItems) {
        mOrderItems = pOrderItems;
    }

    public CPSLastProductsAddedToCart getLastProductsAddedToCart() {
        return mLastProductsAddedToCart;
    }

    public void setLastProductsAddedToCart(CPSLastProductsAddedToCart pLastProductsAddedToCart) {
        mLastProductsAddedToCart = pLastProductsAddedToCart;
    }

    public CPSFailedProductsNotAddedToCart getFailedProductsNotAddedToCart() {
        return failedProductsNotAddedToCart;
    }

    public void setFailedProductsNotAddedToCart(CPSFailedProductsNotAddedToCart failedProductsNotAddedToCart) {
        this.failedProductsNotAddedToCart = failedProductsNotAddedToCart;
    }

    public CatalogTools getCatalogTools() {
        return mCatalogTools;
    }

    public void setCatalogTools(CatalogTools pCatalogTools) {
        mCatalogTools = pCatalogTools;
    }

    public Boolean getFromAvailabilityModal() {
        return mFromAvailabilityModal;
    }

    public void setFromAvailabilityModal(Boolean pFromAvailabilityModal) {
        mFromAvailabilityModal = pFromAvailabilityModal;
    }

    public Boolean isResetCheckAvailability() {
        return resetCheckAvailability;
    }

    public void setResetCheckAvailability(Boolean pResetCheckAvailability) {
        resetCheckAvailability = pResetCheckAvailability;
    }

    public CPSAvailabilityManager getAvailabilityManager() {
        return mAvailabilityManager;
    }

    public void setAvailabilityManager(CPSAvailabilityManager pAvailabilityManager) {
        mAvailabilityManager = pAvailabilityManager;
    }

	/**
	 * @return the qoFormError
	 */
	public boolean isQoFormError() {
		return qoFormError;
	}

	/**
	 * @param qoFormError the qoFormError to set
	 */
	public void setQoFormError(boolean qoFormError) {
		this.qoFormError = qoFormError;
	}
    
}
