package com.cps.commerce.order.purchase;

public class CPSPriceAvailability {

	private String pasku;
	private long paqty;
	private Double amount;
	private String csid; 
	private String paproductid;
	private Double unitprice;
	
	
	
	public String getSku() {
		return pasku;
	}
	public void setSku(String sku) {
		this.pasku = sku;
	}
	public long getQty() {
		return paqty;
	}
	public void setQty(long qty) {
		this.paqty = qty;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getPaproductid() {
		return paproductid;
	}
	public void setPaproductid(String paproductid) {
		this.paproductid = paproductid;
	}
	public String getCsid() {
		return csid;
	}
	public void setCsid(String csid) {
		this.csid = csid;
	}
	public Double getUnitprice() {
		return unitprice;
	}
	public void setUnitprice(Double unitprice) {
		this.unitprice = unitprice;
	}
	
}
