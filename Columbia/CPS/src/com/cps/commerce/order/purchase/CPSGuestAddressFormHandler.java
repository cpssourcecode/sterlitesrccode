package com.cps.commerce.order.purchase;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.commerce.order.service.CPSCartModifierService;
import com.cps.core.util.CPSContactInfo;
import com.cps.userprofiling.service.ProfileDefaultShippingAddressService;
import com.cps.userprofiling.validator.ProfileDefaultShippingAddressValidator;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;
import java.io.IOException;
import java.util.Arrays;

public class CPSGuestAddressFormHandler extends PurchaseProcessFormHandler {

    private CPSCartModifierService mCartModifierService;
    private ProfileDefaultShippingAddressService mShippingAddressService;
    private ProfileDefaultShippingAddressValidator mShippingAddressValidator;

    private String mCompanyName;
    private String mContactName;
    private String mPhoneNumber;
    private String mAddress1;
    private String mAddress2;
    private String mCity;
    private String mZipCode;
    private String mState;
    private String mCountry = "US";
    private String mEmail;

    private CPSContactInfo mAddress;

    private void setNames(CPSContactInfo pAddress) {
        final String contactName = getContactName();
        if (StringUtils.isNotBlank(contactName)) {
            final String[] names = contactName.trim().split("\\s");

            // join all parts except the last one into a single String
            // example: "A B C D", firstName: "A B C", lastName: "D"

            if (names.length > 0) {
                final int joinTo = names.length > 1 ? names.length - 1 : 1;
                final String firstName = StringUtils.joinStrings(Arrays.copyOfRange(names, 0, joinTo), ' ');
                pAddress.setFirstName(firstName);

                if (names.length > 1) {
                    final String lastName = names[names.length - 1];
                    pAddress.setLastName(lastName);
                }
            }
        }
    }

    private CPSContactInfo packAddress() {
        CPSContactInfo address = new CPSContactInfo();

        setNames(address);
        address.setCompanyName(getCompanyName());
        address.setPhoneNumber(getPhoneNumber());
        address.setAddress1(getAddress1());
        address.setAddress2(getAddress2());
        address.setCity(getCity());
        address.setState(getState());
        address.setPostalCode(getZipCode());
        address.setCountry(getCountry());
        address.setEmail(getEmail());

        return address;
    }

    protected void preSetAddressOnCart(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse){
        CPSContactInfo address = packAddress();
        setAddress(address);
        getShippingAddressValidator().validateShippingAddress(this, pRequest, address);
    }

    protected void setAddressOnCart(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws CommerceException, RepositoryException {
        final RepositoryItem profile = getProfile();
        final Order order = getOrder();
        final CPSContactInfo address = getAddress();

        CPSCartModifierService service = getCartModifierService();
        if(service != null) {
            service.updateJdeAddressNumber(address);
            service.createContactInfoForAddress(address);
            service.updateShippingAddress(order, address);
            service.updateBillingAddress(order, address);
            getShippingAddressService().populateGuestProfileAddress((MutableRepositoryItem) profile, address);
            getOrderManager().updateOrder(order);
        }
    }


    /**
     * set shipping address to user on cart
     *
     * @param pRequest  request
     * @param pResponse response
     * @return false
     * @throws ServletException if error occurs
     * @throws IOException      if error occurs
     */
    public boolean handleSetAddressOnCart(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {

        final String handleMethod = "CPSGuestAddressFormHandler.handleSetAddressOnCart";
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();

        if (null == rrm || rrm.isUniqueRequestEntry(handleMethod)) {
            TransactionManager tm = getTransactionManager();
            TransactionDemarcation td = new TransactionDemarcation();
            boolean rollback = false;
            try{
                if (tm != null){
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }
                synchronized (getOrder()) {
                    preSetAddressOnCart(pRequest, pResponse);
                    if (!getFormError()) {
                        setAddressOnCart(pRequest, pResponse);
                    }
                }
                AjaxUtils.addAjaxResponse(this, pResponse, null);
            } catch (final Exception e) {
                vlogError(e, "Error during setting shipping location");
                rollback = true;
            } finally {
                if (null != rrm) {
                    rrm.removeRequestEntry(handleMethod);
                }
                if (tm != null){
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException pE) {
                        if (isLoggingError()){
                            logError(pE);
                        }
                    }
                }
            }
        }
        return false;
    }

    public CPSCartModifierService getCartModifierService() {
        return mCartModifierService;
    }

    public void setCartModifierService(CPSCartModifierService pCartModifierService) {
        mCartModifierService = pCartModifierService;
    }

    public ProfileDefaultShippingAddressService getShippingAddressService() {
        return mShippingAddressService;
    }

    public void setShippingAddressService(ProfileDefaultShippingAddressService pShippingAddressService) {
        mShippingAddressService = pShippingAddressService;
    }

    public ProfileDefaultShippingAddressValidator getShippingAddressValidator() {
        return mShippingAddressValidator;
    }

    public void setShippingAddressValidator(ProfileDefaultShippingAddressValidator pShippingAddressValidator) {
        mShippingAddressValidator = pShippingAddressValidator;
    }

    public String getCompanyName() {
        return mCompanyName;
    }

    public void setCompanyName(String pCompanyName) {
        mCompanyName = pCompanyName;
    }

    public String getContactName() {
        return mContactName;
    }

    public void setContactName(String pContactName) {
        mContactName = pContactName;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String pPhoneNumber) {
        mPhoneNumber = pPhoneNumber;
    }

    public String getAddress1() {
        return mAddress1;
    }

    public void setAddress1(String pAddress1) {
        mAddress1 = pAddress1;
    }

    public String getAddress2() {
        return mAddress2;
    }

    public void setAddress2(String pAddress2) {
        mAddress2 = pAddress2;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String pCity) {
        mCity = pCity;
    }

    public String getZipCode() {
        return mZipCode;
    }

    public void setZipCode(String pZipCode) {
        mZipCode = pZipCode;
    }

    public String getState() {
        return mState;
    }

    public void setState(String pState) {
        mState = pState;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String pCountry) {
        mCountry = pCountry;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String pEmail) {
        mEmail = pEmail;
    }

    public CPSContactInfo getAddress() {
        return mAddress;
    }

    public void setAddress(CPSContactInfo pAddress) {
        mAddress = pAddress;
    }
}
