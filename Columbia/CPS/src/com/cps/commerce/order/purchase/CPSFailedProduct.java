package com.cps.commerce.order.purchase;


public class CPSFailedProduct {
    private String productId;
    private String productDisplayName;
    private long productQty;
    private String errorMessage;
    private String sku;
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductDisplayName() {
		return productDisplayName;
	}
	public void setProductDisplayName(String productDisplayName) {
		this.productDisplayName = productDisplayName;
	}
	public long getProductQty() {
		return productQty;
	}
	public void setProductQty(long productQty) {
		this.productQty = productQty;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	@Override
	public String toString() {
		return "CPSFailedProduct [productId=" + productId + ", productDisplayName=" + productDisplayName
				+ ", productQty=" + productQty + ", errorMessage=" + errorMessage + ", sku=" + sku + "]";
	}
    
    
}