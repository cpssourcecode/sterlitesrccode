package com.cps.commerce.order.purchase;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.order.Order;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.util.CPSConstants;
import com.cps.util.CPSMessageUtils;

import atg.commerce.CommerceException;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.droplet.DropletFormException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

public class CPSCouponFormHandler extends PurchaseProcessFormHandler {
	
	public static final String MSG_UNCLAIMABLE_COUPON = "couponNotClaimable";
	public static final String MSG_ERROR_REMOVING_COUPON = "errorRemovingCoupon";

	private String couponCode;
	private String applyCouponSuccessURL;
	private String applyCouponErrorURL;
	private String removeCouponSuccessURL;
	private String removeCouponErrorURL;

	public CPSOrderImpl getOrder() {
		return (CPSOrderImpl)super.getOrder();
	}

	/**
	 * returns active coupons on the cart.
	 * includes multisite shared cart functionality
	 * @return
	 */
	public List<String> getCurrentCouponCode() {
		try {
			CPSOrderManager orderManager =  (CPSOrderManager) getOrderManager();
			return orderManager.getCouponCode(getOrder());
		} catch (CommerceException e) {
			return null;
		}
	}
	
	/**
	 * returns Map of active coupons on the cart and discount amount for each.
	 * @return
	 */
	public Map<String, Double> getCurrentCouponCodesMap() {
		try {
			CPSOrderManager orderManager =  (CPSOrderManager) getOrderManager();
			return orderManager.getCouponCodeMap(getOrder());
		} catch (CommerceException e) {
			return null;
		}
	}


	public boolean handleClaimCoupon(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)
					throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSCouponFormHandler.handleClaimCoupon";

		TransactionDemarcation td = new TransactionDemarcation();
		TransactionManager tm = getTransactionManager();
		boolean rollback = false;

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (tm != null){
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				boolean couponTendered = false;
				synchronized (getOrder()) {
					couponTendered = ((CPSPurchaseProcessHelper) getPurchaseProcessHelper()).tenderCoupon(getCouponCode(),
							getOrder(),
							getProfile(),
							getUserPricingModels(),
							getUserLocale());

					getOrderManager().updateOrder(getOrder());
				}

				/*
				 * If the coupon could not be claimed add an error
				 */
				if (!couponTendered) {
					//addFormException(new DropletFormException("Error applying this coupon", MSG_UNCLAIMABLE_COUPON));
					CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, MSG_UNCLAIMABLE_COUPON, pRequest.getLocale(), MSG_UNCLAIMABLE_COUPON);
				}
			} catch (CommerceException commerceException) {
				//addFormException(new DropletFormException("Error applying this coupon", MSG_UNCLAIMABLE_COUPON));
				CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, MSG_UNCLAIMABLE_COUPON, pRequest.getLocale(), MSG_UNCLAIMABLE_COUPON);
				rollback = true;
			} catch (TransactionDemarcationException pE) {
				if (isLoggingError()){
					logError(pE);
				}
				rollback = true;
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null){
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException pE) {
						if (isLoggingError()){
							logError(pE);
						}
					}
				}
			}
		}

		return checkFormRedirect(getApplyCouponSuccessURL(),
				getApplyCouponErrorURL(),
				pRequest, pResponse);
	}


	public boolean handleRemoveCoupon(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)
					throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSCouponFormHandler.handleRemoveCoupon";

		TransactionDemarcation td = new TransactionDemarcation();
		TransactionManager tm = getTransactionManager();
		boolean rollback = false;

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if(tm != null){
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				synchronized (getOrder()) {
					boolean removeCouponFromOrder = ((CPSPurchaseProcessHelper) getPurchaseProcessHelper()).removeCouponFromOrder(getCouponCode(),
							getOrder(),
							getProfile(),
							getUserPricingModels(),
							getUserLocale());

					getOrderManager().updateOrder(getOrder());
				}
			} catch (CommerceException commerceException) {
				//CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, MSG_ERROR_REMOVING_COUPON, pRequest.getLocale(), MSG_ERROR_REMOVING_COUPON);
				addFormException(new DropletFormException("Error removing this coupon", MSG_ERROR_REMOVING_COUPON));
				rollback = true;
			} catch (TransactionDemarcationException pE) {
				if (isLoggingError()){
					logError(pE);
				}
				rollback = true;
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null){
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException pE) {
						if (isLoggingError()){
							logError(pE);
						}
					}
				}
			}
		}

		return checkFormRedirect(getRemoveCouponSuccessURL(),
				getRemoveCouponErrorURL(),
				pRequest, pResponse);
	}


	public String getCouponCode() {
		//to upper case functionality
		//if(!StringUtils.isBlank(couponCode)){
		//	couponCode = couponCode.toUpperCase();
		//}
		return couponCode;
	}


	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}


	public String getApplyCouponSuccessURL() {
		return applyCouponSuccessURL;
	}


	public void setApplyCouponSuccessURL(String applyCouponSuccessURL) {
		this.applyCouponSuccessURL = applyCouponSuccessURL;
	}


	public String getApplyCouponErrorURL() {
		return applyCouponErrorURL;
	}


	public void setApplyCouponErrorURL(String applyCouponErrorURL) {
		this.applyCouponErrorURL = applyCouponErrorURL;
	}


	public String getRemoveCouponSuccessURL() {
		return removeCouponSuccessURL;
	}


	public void setRemoveCouponSuccessURL(String removeCouponSuccessURL) {
		this.removeCouponSuccessURL = removeCouponSuccessURL;
	}


	public String getRemoveCouponErrorURL() {
		return removeCouponErrorURL;
	}


	public void setRemoveCouponErrorURL(String removeCouponErrorURL) {
		this.removeCouponErrorURL = removeCouponErrorURL;
	}
}
