package com.cps.commerce.order.purchase;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.CommitOrderFormHandler;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.states.OrderStates;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.json.JSONException;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.cps.commerce.approval.ApprovalManager;
import com.cps.commerce.order.CPSCommerceItemImpl;
import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.commerce.order.CPSInvoiceRequest;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.commerce.order.service.CPSCartModifierService;
import com.cps.core.util.CPSContactInfo;
import com.cps.droplet.DropletConstants;
import com.cps.email.CommonEmailSender;
import com.cps.reorders.AutoOrderManager;
import com.cps.reorders.model.AutoOrderItemInfo;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;
import vsg.util.VSGFormUtils;

import javax.servlet.ServletException;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.*;

import static com.cps.util.CPSConstants.CHILD_ORGS;
import static com.cps.util.CPSConstants.ROLES;
import static com.cps.util.CPSConstants.ROLE_ACCOUNT_ADMIN;
import static com.cps.util.CPSConstants.SECONDARY_ADDRESSES;

public class CPSCommitOrderFormHandler extends CommitOrderFormHandler {

    private static final String PAYMENT_ON_ACCOUNT = "onAccount";
    private CommonEmailSender mEmailSender;
    private boolean mtrRequested;
    private String mPoNumber;
    private String mPaymentMethod;
    private boolean mDebugMode = false;
    private AutoOrderManager mAutoOrderManager;
    private String onsiteName;
    private String onsitePhone;
    private String jobName;
    private String concatenatedPoJobName;

    public CommonEmailSender getEmailSender() {
        return mEmailSender;
    }

    public void setEmailSender(CommonEmailSender pEmailSender) {
        mEmailSender = pEmailSender;
    }

    private OrderStates mOrderStates;

    public OrderStates getOrderStates() {
        return mOrderStates;
    }

    public void setOrderStates(OrderStates pOrderStates) {
        mOrderStates = pOrderStates;
    }

    private ApprovalManager mApprovalManager;

    public ApprovalManager getApprovalManager() {
        return mApprovalManager;
    }

    public void setApprovalManager(ApprovalManager pApprovalManager) {
        mApprovalManager = pApprovalManager;
    }

    private CPSCartModifierService mCartModifierService;

    private void checkForApproval(Order pOrder) {
        boolean approvalRequired = getApprovalManager().isApprovalRequired(pOrder, getProfile());
        if (approvalRequired) {
            pOrder.setState(getOrderStates().getStateValue(OrderStates.PENDING_APPROVAL));
            List<RepositoryItem> approvers = getApprovalManager().getApproversForUser(getProfile());
            if (approvers != null) {
                for (RepositoryItem approver : approvers) {
                    pOrder.getAuthorizedApproverIds().add(approver.getRepositoryId());
                }
            }
            getApprovalManager().saveApprovalRequest(pOrder);
        }
    }

    @Override
    public void preCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        super.preCommitOrder(pRequest, pResponse);
        CPSOrderImpl order = (CPSOrderImpl) getOrder();
        String contactName=null;
        String contactPhone=null;
        boolean validContactName=false;
        boolean validContactPhone=false;
        
        contactName=getOnsiteName();
        contactPhone=getOnsitePhone();
        validContactName=validateContactName(contactName);
        validContactPhone=validateContactPhone(contactPhone);
	        
        vlogDebug("ContactName :: {0}", contactName);
        vlogDebug("ContactNumber :: {0}", contactPhone);
        vlogDebug("validContactName :: {0}", validContactName);
        vlogDebug("validContactNumber :: {0}", validContactPhone);
	
        if(!validContactName) {
			createError(CPSErrorCodes.ERR_INVALID_ONSITE_NAME, null, pRequest);
        }
        if(!validContactPhone) {
			createError(CPSErrorCodes.ERR_INVALID_ONSITE_PHONE, null, pRequest);
        }
		

        if (!getFormError()) {
            OrderPriceInfo opi = order.getPriceInfo();
            if (opi != null) {
                order.setOrderTotal(opi.getTotal());
            }
            if(validContactName && validContactPhone) {
            	String onsiteContact= new StringBuilder(contactName).append("|").append(contactPhone).toString();
            	order.setOnsiteContact(onsiteContact);
            }
            order.setMtrRequested(isMtrRequested());
            
            if(StringUtils.isNotEmpty(getJobName().trim())){
            	order.setJobName(getJobName().trim());
            	String concatenatedPoJobName = new StringBuilder(getPoNumber()).append("-").append(getJobName()).toString();
				if (concatenatedPoJobName.length() <= 25) {
					order.setConcatenatedPoJobName(concatenatedPoJobName);
				} else {
					order.setConcatenatedPoJobName(concatenatedPoJobName.substring(0,25));
				}
            } else {
            	order.setConcatenatedPoJobName(getPoNumber());
            }
            try {
                if (PAYMENT_ON_ACCOUNT.equals(getPaymentMethod()) && !order.isTransient()) {
                    CPSInvoiceRequest invoiceRequest = (CPSInvoiceRequest) getCartModifierService().createInvoiceRequestPayGroupIfNotExists(order);
                    CPSContactInfo billingAddress = getBillingAddress();

                    invoiceRequest.setBillingAddress(billingAddress);
                    invoiceRequest.setJdeAddressNumber(getJdeAddressNumber());
                    invoiceRequest.setPONumber(getPoNumber());
                }
            } catch (CommerceException | RepositoryException e) {
                vlogError(e, "Error applying PO");
            }
        }
    }

	protected boolean isShipOrPickup(CPSOrderImpl order) {
		List shippingGroups = order.getShippingGroups();
		boolean isShip=false;
		
		if(shippingGroups.size() > 0) {
			ShippingGroup shippingGroup = (ShippingGroup) shippingGroups.get(0);
		
			if (shippingGroup instanceof CPSHardgoodShippingGroup) {
				isShip=true;
			}else {
				isShip=false;
			}
		}
		return isShip;
	}
    
    protected boolean validateContactName(String contactName) {
    	boolean validName=true;
    	if(StringUtils.isBlank(contactName)) {
    		validName=false;
    	}
    	if (StringUtils.isNotBlank(contactName) && !contactName.matches(CPSConstants.REG_EXP_NAME)) {
    		validName=false;
        }
    	return validName;
    }
    
    protected boolean validateContactPhone(String contactPhone) {
    	boolean validPhone=true;
    	if(StringUtils.isBlank(contactPhone)) {
    		validPhone=false;
    	}
    	if (StringUtils.isNotBlank(contactPhone) && !contactPhone.matches(CPSConstants.REG_EXP_PHONE)) {
    		validPhone=false;
        }
    	return validPhone;
    }

    @Override
    public void commitOrder(Order pOrder, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        super.commitOrder(pOrder, pRequest, pResponse);
        if (!getFormError()) {
            checkForApproval(pOrder);
        }
    }

    @Override
    public void postCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        if (isLoggingDebug()) {
            logDebug(" Start of post handle commit order");
        }

        super.postCommitOrder(pRequest, pResponse);
        Order comitedOrder = getShoppingCart().getLast();
        if (comitedOrder != null) {
            CPSOrderManager om = (CPSOrderManager) getOrderManager();
            ShippingGroup sg = (ShippingGroup) comitedOrder.getShippingGroups().get(0);
            //Decrementing level only if not shipping method selected is not "Pick up"
            if (sg != null && !CPSConstants.CHECKOUT_METHOD_PICKUP.equalsIgnoreCase(sg.getShippingMethod())) {
                om.decrementInventoryOnCheckout(comitedOrder);
            }
            //sending order confirmation email or approval notification message based on approval status
            if (comitedOrder.getState() == getOrderStates().getStateValue(OrderStates.PENDING_APPROVAL)) {
                // Iterate over eligible order approvers and notify each
                List<RepositoryItem> approvers = getApprovalManager().getApproversForUser(getProfile());
                if (approvers != null) {
                    for (RepositoryItem approver : approvers) {
                        getEmailSender().sendOrderApprovalRequiredEmail(approver, getProfile(), comitedOrder);
                    }
                }
                getEmailSender().sendOrderRequestReceivedEmail(getProfile(), comitedOrder);
            } else {
                String siteId = SiteContextManager.getCurrentSiteId();
                sendAdminNotificationEmail(comitedOrder, siteId);
                getEmailSender().sendOrderConfirmationEmail(getProfile(), comitedOrder, siteId);
            }
            Profile profile = (Profile) getProfile();
            profile.setPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL, false);
            vlogInfo("order Notification set to {0} completing order for {1}", profile.getPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL),profile.getRepositoryId());
            profile.setPropertyValue(CPSConstants.FIRST_ITEM_TO_CURRENT_CART, null);
        }
        
        createAutoOrder();

        if (isLoggingDebug()) {
            logDebug(" End of post handle commit order");
        }
    }

    @Override
    public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
    		DynamoHttpServletResponse pResponse) throws ServletException, IOException {
    	if(VSGFormUtils.isAjaxRequest(pRequest) && getFormError()){
    		return false;
    	}
    	return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
    }
    
    @Override
    public boolean handleCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CPSCommitOrderFormHandler.handleCommitOrder";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            PerformanceMonitor.startOperation(handleMethodName);
            try {
                if (VSGFormUtils.isAjaxRequest(pRequest)) {
                    super.handleCommitOrder(pRequest, pResponse);

                    try {
                        if (!getFormError()) {
                            VSGFormUtils.addAjaxSuccessResponse(pResponse);
                        } else {
                            VSGFormUtils.addAjaxErrorResponse(this, pResponse);
                        }
                    } catch (JSONException e) {
                        logError(e);
                    }
                    return false;
                } else {
                    return super.handleCommitOrder(pRequest, pResponse);
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
                PerformanceMonitor.endOperation(handleMethodName);
            }
        }

        return false;
    }

    private void createAutoOrder() {
        CPSOrderImpl orderImpl = (CPSOrderImpl) getOrder();

        String siteId = orderImpl.getSiteId();
        List<AutoOrderItemInfo> products = new ArrayList<>();
        List commerceItems = getOrder().getCommerceItems();
        for (Object ci : commerceItems) {
            CPSCommerceItemImpl commerceItem = (CPSCommerceItemImpl) ci;
            if (commerceItem.isScheduledItem()) {
                String productId = commerceItem.getProductId();
                String skuId = commerceItem.getCatalogRefId();
                Long quantity = commerceItem.getQuantity();
                Integer scheduleDays = commerceItem.getScheduleDays();
                products.add(new AutoOrderItemInfo(productId, skuId, quantity, scheduleDays, siteId));
            }
        }

        Collections.sort(products, new Comparator<AutoOrderItemInfo>() {
            @Override
            public int compare(AutoOrderItemInfo o1, AutoOrderItemInfo o2) {
                return o1.getScheduleDays().compareTo(o2.getScheduleDays());
            }
        });

        List<AutoOrderItemInfo> productsWithOneSchedule = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            productsWithOneSchedule.add(products.get(i));
            try {
                if (i != products.size() - 1) {
                    if (!Objects.equals(productsWithOneSchedule.get(0).getScheduleDays(), products.get(i + 1).getScheduleDays())) {
                        mAutoOrderManager.createAutoOrder(orderImpl, productsWithOneSchedule);
                        productsWithOneSchedule = new ArrayList<>();
                    }
                } else {
                    mAutoOrderManager.createAutoOrder(orderImpl, productsWithOneSchedule);
                }
            } catch (RepositoryException | IntrospectionException e) {
                vlogError(e, "Error");
            }
        }
    }
    
    /**
	 * Find address in org
	 *
	 * @param pParentOrg the parent org
	 * @param pAddress   pAddress
	 * @return the orgs shipping addresses
	 */
	private RepositoryItem findOrgFromShippingAddress(RepositoryItem pParentOrg, RepositoryItem pAddress) {
		if (pParentOrg != null) {
			Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) pParentOrg.getPropertyValue(CHILD_ORGS);
			if (childOrgs != null && childOrgs.size() > 0) {
				for (RepositoryItem org : childOrgs) {
					Map<String, RepositoryItem> secondaryAddresses = 
							(Map<String, RepositoryItem>) org.getPropertyValue(SECONDARY_ADDRESSES);
					if (secondaryAddresses != null) {
						for (RepositoryItem address : secondaryAddresses.values()) {
							if (address.getRepositoryId().equals(pAddress.getRepositoryId())) {
								return org;
							}
						}
					}
				}
			}
		}
		return null;
	}
	
	   private void sendAdminNotificationEmail(Order order, String siteId) {
		   	 RepositoryItem organization= (RepositoryItem) getProfile().getPropertyValue("currentOrganization");
		        if (organization == null) {
		     	   organization= (RepositoryItem) getProfile().getPropertyValue("parentOrganization");
				}
		        Set<RepositoryItem> allMembersItem = (Set<RepositoryItem>) organization.getPropertyValue("allMembers");
		        Set<RepositoryItem> enabledOrderNotificationUsers = new HashSet<RepositoryItem>();
		        
				try {
					RepositoryItem currentUser = getProfileRepository().getItem(getProfile().getRepositoryId(), CPSConstants.USER);
					Set roles = null;
					for (RepositoryItem memberItem : allMembersItem) {
						roles = (Set) memberItem.getPropertyValue(CPSConstants.ROLES);
						if (roles.toString().contains(CPSConstants.ROLE_ACCOUNT_ADMIN) && memberItem != null) {
							enabledOrderNotificationUsers = (Set<RepositoryItem>) memberItem.getPropertyValue("notificationEnabledUsers");
							if (enabledOrderNotificationUsers != null && enabledOrderNotificationUsers.size() > 0
									&& enabledOrderNotificationUsers.contains(currentUser)) {
								//send email to admin user
								getEmailSender().sendOrderConfirmationEmailToAdmin(memberItem,currentUser, order, siteId);
								vlogDebug("send email to admin users {0}", memberItem);
							}
						}
					}			
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	   
    private RepositoryItem getBillingAddressRepositoryItem() {
        RepositoryItem profile = getProfile();
        RepositoryItem billingAddress = null;
        
        if (profile != null) {
        	RepositoryItem shipAddress = (RepositoryItem) profile.getPropertyValue(CPSConstants.SELECTED_CS);
    		if (shipAddress != null) {
    			RepositoryItem billingOrganization = findOrgFromShippingAddress((RepositoryItem) profile.getPropertyValue(CPSConstants.PARENT_ORG), shipAddress);
    			if (billingOrganization != null) {
    				billingAddress = (RepositoryItem) billingOrganization.getPropertyValue(CPSConstants.BILLING_ADDRESS);
    			}
    		}

        	if (billingAddress == null) {
	            RepositoryItem parentOrganization = (RepositoryItem) profile.getPropertyValue("parentOrganization");
	            if (parentOrganization != null){
	                billingAddress = (RepositoryItem) parentOrganization.getPropertyValue("billingAddress");
	            } else {
	                billingAddress = (RepositoryItem) profile.getPropertyValue("derivedBillingAddress");
	            }
        	}
        }

        return billingAddress;
    }

    private CPSContactInfo getBillingAddress() throws CommerceException {
        RepositoryItem billingAddress = getBillingAddressRepositoryItem();

        if (billingAddress != null){
            CPSContactInfo result = new CPSContactInfo();
            OrderTools.copyAddress(billingAddress, result);
            return result;
        }

        return null;
    }

    private String getJdeAddressNumber() {
        RepositoryItem billingAddress = getBillingAddressRepositoryItem();

        if (billingAddress != null){
            return (String)billingAddress.getPropertyValue("jdeAddressNumber");
        }

        return null;
    }

    private void createError(String msg, String pty, DynamoHttpServletRequest pRequest) {
        CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
    }

   
    public CPSCartModifierService getCartModifierService() {
        return mCartModifierService;
    }

    public void setCartModifierService(CPSCartModifierService pCartModifierService) {
        mCartModifierService = pCartModifierService;
    }

    public boolean isMtrRequested() {
        return mtrRequested;
    }

    public void setMtrRequested(boolean mtrRequested) {
        this.mtrRequested = mtrRequested;
    }

    public String getPoNumber() {
        return mPoNumber;
    }

    public void setPoNumber(String pPoNumber) {
        mPoNumber = pPoNumber;
    }

    public String getOnsiteName() {
		return onsiteName;
	}

	public void setOnsiteName(String onsiteName) {
		this.onsiteName = onsiteName;
	}

	public String getOnsitePhone() {
		return onsitePhone;
	}

	public void setOnsitePhone(String onsitePhone) {
		this.onsitePhone = onsitePhone;
	}

	public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String pPaymentMethod) {
        mPaymentMethod = pPaymentMethod;
    }

    public boolean isDebugMode() {
        return mDebugMode;
    }

    public void setDebugMode(boolean pDebugMode) {
        mDebugMode = pDebugMode;
    }

    public AutoOrderManager getAutoOrderManager() {
        return mAutoOrderManager;
    }

    public void setAutoOrderManager(AutoOrderManager pAutoOrderManager) {
        this.mAutoOrderManager = pAutoOrderManager;
    }

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
    
}
