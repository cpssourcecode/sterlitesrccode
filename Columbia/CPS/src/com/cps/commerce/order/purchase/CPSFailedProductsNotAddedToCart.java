package com.cps.commerce.order.purchase;

import java.util.List;

public class CPSFailedProductsNotAddedToCart {
    private List<CPSFailedProduct> failedProducts;

	public List<CPSFailedProduct> getFailedProducts() {
		return failedProducts;
	}

	public void setFailedProducts(List<CPSFailedProduct> failedProducts) {
		this.failedProducts = failedProducts;
	}
    
}