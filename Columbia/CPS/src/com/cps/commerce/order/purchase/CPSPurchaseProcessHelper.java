package com.cps.commerce.order.purchase;

import static com.cps.util.CPSConstants.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import atg.repository.rql.RqlStatement;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;

import atg.adapter.gsa.GSARepository;
import atg.adapter.gsa.query.Builder;
import atg.commerce.CommerceException;
import atg.commerce.claimable.ClaimableException;
import atg.commerce.claimable.ClaimableManager;
import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.pricing.PricingTools;
import atg.commerce.promotion.DuplicatePromotionException;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.Repository;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

public class CPSPurchaseProcessHelper extends PurchaseProcessHelper {

	private ClaimableManager claimableManager;
	private PricingTools pricingTools;
	private Repository catalogRep;
	
	private String queryString;
	private String mRqlQueryString;


	/**
	 * Rollback the provided transaction.
	 * @param pTransactionManager - Transaction Manager containing the transaction to rollback, A <code>TransactionManager</code> value.
	 */
	public void rollbackTransaction(TransactionManager pTransactionManager) {
		if (pTransactionManager != null) {
			try {
				Transaction t = pTransactionManager.getTransaction();

				if (t != null) {
					t.setRollbackOnly();
				}
			} catch (SystemException exc) {
				if (isLoggingError()) {
					logError(exc);
				}
			}
		}
	}
	protected Transaction ensureTransaction() {
		try {
			TransactionManager tm = getTransactionManager();
			if ( null == tm ) {
				if ( isLoggingError() ) {
					logError("Missing Transaction Manager");
				}
				return null;
			}
			Transaction t = tm.getTransaction();
			if ( null == t ) {
				tm.begin();
				t = tm.getTransaction();
				return t;
			}
			return null;
		} catch (NotSupportedException exc) {
			if ( isLoggingError() ) {
				logError(exc);
			}
		} catch (SystemException exc) {
			if ( isLoggingError() ) {
				logError(exc);
			}
		}
		return null;
	}

	protected void commitTransaction(Transaction pTransaction, Order pOrder) {
		boolean exception = false;
		if ( null != pTransaction ) {
			try {
				TransactionManager tm = getTransactionManager();
				if ( isTransactionMarkedAsRollBack() ) {
					if ( null != tm ) {
						tm.rollback();
					} else {
						pTransaction.rollback();
					}
					if ( pOrder instanceof OrderImpl ) {
						( (OrderImpl) pOrder ).invalidateOrder();
					}
				} else {
					if ( null != tm ) {
						tm.commit();
					} else {
						pTransaction.commit();
					}
				}
			} catch (Exception exc) {
				exception = true;
				if ( isLoggingError() ) {
					logError(exc);
				}
			} finally {
				if (exception) {
					if ( pOrder instanceof OrderImpl ) {
						( (OrderImpl) pOrder).invalidateOrder();
					}
				}
			}
		}
	}


	public boolean tenderCoupon(String pCouponCode, 
			CPSOrderImpl pOrder, 
			RepositoryItem pProfile,
			PricingModelHolder pUserPricingModels, 
			Locale pUserLocale)
					throws CommerceException, IllegalArgumentException {
		CPSOrderManager orderManager = (CPSOrderManager) getOrderManager();
		List currentCouponCodes = orderManager.getCouponCode(pOrder);
		String tenderedCouponCode = pCouponCode;

		boolean canClaimTenderedCoupon = false;
		// If this coupon has already been claimed just return
		if (!StringUtils.isEmpty(tenderedCouponCode) && currentCouponCodes!=null && currentCouponCodes.contains(tenderedCouponCode)) {
			return true;
		}
		try {
			canClaimTenderedCoupon = getClaimableManager().canClaimCoupon(pProfile.getRepositoryId(), tenderedCouponCode);
			// Claim the new coupon code if it is valid
			if (canClaimTenderedCoupon) {
				claimCoupon(tenderedCouponCode, pOrder, pProfile, pUserPricingModels, pUserLocale);
			}

			return (canClaimTenderedCoupon);
		} catch (Exception exception) {
			logError("exception==="+exception);
			throw new CommerceException(exception);
		}
	}



	public boolean removeCouponFromOrder(String pCouponCode, 
			CPSOrderImpl pOrder,
			RepositoryItem pProfile,
			PricingModelHolder pUserPricingModels,
			Locale pUserLocale)
					throws CommerceException {
		RepositoryItem profile = pProfile;
		CPSOrderManager orderManager = (CPSOrderManager) getOrderManager();
		List coupons = orderManager.getCouponCode(pOrder);
		boolean removeFlag = false;

		// If there's no couponCode, there's nothing to remove
		if (coupons == null || coupons.isEmpty() || StringUtils.isBlank(pCouponCode)) {
			return removeFlag;
		}
		try {
				// Get and remove all coupon's promotions from order
				RepositoryItem coupon = getClaimableManager().claimItem(pCouponCode);
				String promotionsPropertyName = getClaimableManager().getClaimableTools().getPromotionsPropertyName();
				Collection<RepositoryItem> promotions = null;
				if(coupon!=null){
					promotions = (Collection<RepositoryItem>) coupon.getPropertyValue(promotionsPropertyName);
				}else{
					return removeFlag;
				}
				// Ensure profile to be a MutableRepositoryItem
				if (!(profile instanceof MutableRepositoryItem)) {
					// profile uses a MutableRepository for sure
					profile = ((MutableRepository) profile.getRepository()).getItemForUpdate(profile.getRepositoryId(), profile.getItemDescriptor().getItemDescriptorName());
				}
				if(promotions!=null && !promotions.isEmpty()){
					for (RepositoryItem promotion: promotions) {
						// Now we can cast profile to the type we need
						removeFlag = getClaimableManager().getPromotionTools().removePromotion((MutableRepositoryItem) profile, promotion, false);
					}
				}else{
					return removeFlag;
				}

				//removing coupon from the list
				removeCouponFromList(pCouponCode, pOrder);

				// Initialize pricing models to use current promotions, that is exclude coupon's promotions from pricing models
				pUserPricingModels.initializePricingModels();
				// Re-price order to display most recent prices
				getPricingTools().priceOrderTotal(pOrder, pUserPricingModels, pUserLocale, pProfile, new HashMap());
				return removeFlag;
		} catch (RepositoryException ce) {
			logError("ce="+ce);
			throw new CommerceException(ce);
		}
	}



	/**
	 * This method claims a coupon specified by its code for a specific user and order.
	 *
	 * @param pCouponCode - coupon code to be claimed
	 * @param pOrder - order to be repriced when the coupon has been claimed
	 * @param pProfile - user who claims a coupon
	 * @param pUserPricingModels - user's pricing models to be used for order reprice process
	 * @param pUserLocale - user's locale to be used when repricing order
	 * @throws CommerceException - if something goes wrong
	 * @throws IllegalArgumentException - if order has a claimed coupon already
	 */
	private void claimCoupon(String pCouponCode,
			CPSOrderImpl pOrder,
			RepositoryItem pProfile,
			PricingModelHolder pUserPricingModels,
			Locale pUserLocale)
					throws CommerceException, IllegalArgumentException {

		// First, check the coupon code to be used
		if (StringUtils.isBlank(pCouponCode)) {
			return;
		}

		CPSOrderManager orderManager = (CPSOrderManager) getOrderManager();
		List coupons = orderManager.getCouponCode(pOrder);

		// Then check if the order specified has this coupon already; if true, throw an exception
		if (coupons!=null && coupons.contains(pCouponCode)) {
			throw new IllegalArgumentException("There is a coupon claimed for order specified!");
		}

		// And after that claim a coupon
		TransactionDemarcation td = new TransactionDemarcation();
		boolean shouldRollback = true; // We should rollback transaction, if any exception occurs

		try {
			td.begin(getTransactionManager());
			getClaimableManager().claimCoupon(pProfile.getRepositoryId(), pCouponCode);

			if (isLoggingDebug()){
				logDebug("after claim coupon");
			}
			//saving coupon to list of coupons
			addCouponToList(pCouponCode, pOrder);
			// Initialize pricing models in order to use recently claimed coupon's promotions
			pUserPricingModels.initializePricingModels();
			// Re-price order in order to display most recent prices for items etc.
			getPricingTools().priceOrderTotal(pOrder, pUserPricingModels, pUserLocale, pProfile, new HashMap());
			shouldRollback = false;
		}
		catch (ClaimableException e) {
			// Propagate only exceptions that are not 'Duplicate promotion for current user'; this exact exception should be surpressed!
			if (e.getCause() instanceof DuplicatePromotionException) {
				return;
			}
			throw e;
		}
		catch (TransactionDemarcationException e) {
			throw new CommerceException(e);
		} 
		finally {
			// Commit or rollback transaction, only if we have created it
			// If someone's called this method, then he should take care of it
			try {
				td.end(shouldRollback);
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()){
					logError(e);
				}
			}
		}
	}

	protected void addCouponToList(String couponName, CPSOrderImpl pOrder) {
		List couponeList = pOrder.getOrderCouponCodes();
		if(couponeList!=null){
			if(!couponeList.contains(couponName)) {
				couponeList.add(couponName);
			}
		}else{
			couponeList = new ArrayList();
			couponeList.add(couponName);
		}
		pOrder.setOrderCouponCodes(couponeList);
	}

	protected void removeCouponFromList(String couponName, CPSOrderImpl pOrder) {
		List couponeList = pOrder.getOrderCouponCodes();
		if(couponeList != null && couponeList.contains(couponName)) {
			couponeList.remove(couponName);
			pOrder.setOrderCouponCodes(couponeList);
		}
	}


	/**
	 * Product lookup by orgId and alias number
	 * @param aliasNumber
	 * @param orgId
	 * @return
	 */
	public RepositoryItem findProductFormAlias(String aliasNumber, String orgId) {
		RepositoryItem productOrgAlias = null;
		RepositoryItem prodItem = null;

		if (isLoggingDebug()) {
			logDebug("findProductFormAlias started with: aliasNumber:=" + aliasNumber + " and orgId=" + orgId);
		}

		try {
			if (!StringUtils.isEmpty(aliasNumber) && !StringUtils.isEmpty(orgId)) {
				//RepositoryView rView = getOrderManager().getOrderTools().getCatalogTools().getCatalog().getView("product");
				GSARepository repo = (GSARepository) getCatalogRep();
				RepositoryView rView = repo.getView(PRODUCT_ITEM_DESCRIPTOR);
				Object params[] = new Object[1];
				Builder builder = (Builder) rView.getQueryBuilder();
				params[0] = orgId;
//				params[1] = aliasNumber;
//				RepositoryItem[] items = rView.executeQuery (builder.createSqlPassthroughQuery(getQueryString(), params));
				RqlStatement rqlStatement = RqlStatement.parseRqlStatement(getRqlQueryString());
				RepositoryItem[] items = rqlStatement.executeQuery(rView, params);

				if (isLoggingDebug()) {
					logDebug("gettting items back:=" + items);
				}

				prodItem = findProductByAliasInQueryResults(items, aliasNumber, orgId);
				
			}

		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return prodItem;
	}
	
	/**
	 * 
	 * @param pItems - query results
	 * @param pAliasNumber - alias number
	 * @param pOrgId - organization id
	 * @return product
	 */
	private RepositoryItem findProductByAliasInQueryResults(RepositoryItem[] pItems, String pAliasNumber, String pOrgId){
		if (pItems != null && pItems.length > 0) {
			for (RepositoryItem repItem : pItems) {
				Map<String, String> prodAliasMap = (Map<String, String>) repItem.getPropertyValue(PRODUCT_ITEM_DESCRIPTOR_ALIAS_MAP);

				String aliasNumbers = prodAliasMap.get(pOrgId);
				vlogDebug("aliasNumbers :: {0}",aliasNumbers);
				if(!StringUtils.isEmpty(aliasNumbers)){
					String[] splittedValues = aliasNumbers.split("\\|");
					for(String alias : splittedValues){
						if (pAliasNumber.equals(alias)) {
							vlogDebug("Found product with id :: {0}", repItem.getRepositoryId());
							return repItem;
						}
					}
				}
				
			}
		}
		return null;
	}

	public ClaimableManager getClaimableManager() {
		return claimableManager;
	}
	public void setClaimableManager(ClaimableManager claimableManager) {
		this.claimableManager = claimableManager;
	}
	public PricingTools getPricingTools() {
		return pricingTools;
	}
	public void setPricingTools(PricingTools pricingTools) {
		this.pricingTools = pricingTools;
	}
	public String getQueryString() {
		return queryString;
	}
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	public Repository getCatalogRep() {
		return catalogRep;
	}
	public void setCatalogRep(Repository catalogRep) {
		this.catalogRep = catalogRep;
	}


	/**
	 * Gets mRqlQueryString.
	 *
	 * @return Value of mRqlQueryString.
	 */
	public String getRqlQueryString() {
		return mRqlQueryString;
	}

	/**
	 * Sets new mRqlQueryString.
	 *
	 * @param pRqlQueryString New value of mRqlQueryString.
	 */
	public void setRqlQueryString(String pRqlQueryString) {
		mRqlQueryString = pRqlQueryString;
	}
}
