package com.cps.commerce.order.purchase;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.commerce.order.CPSCreditCard;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.service.CPSCartModifierService;
import com.cps.core.util.CPSContactInfo;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;
import java.io.IOException;

public class CPSCreditCardFormHandler extends PurchaseProcessFormHandler {

    private CPSCartModifierService mCartModifierService;

    private boolean mMtrRequested;

    private CPSContactInfo getBillingAddress() throws CommerceException {
        RepositoryItem billingAddress = null;

        RepositoryItem profile = getProfile();
        if (profile != null) {
            RepositoryItem parentOrganization = (RepositoryItem) profile.getPropertyValue("parentOrganization");
            if (parentOrganization != null){
                billingAddress = (RepositoryItem) parentOrganization.getPropertyValue("billingAddress");
            } else {
                billingAddress = (RepositoryItem) profile.getPropertyValue("derivedBillingAddress");
            }
        }

        if (billingAddress != null){
            CPSContactInfo result = new CPSContactInfo();
            OrderTools.copyAddress(billingAddress, result);
            return result;
        }

        return null;
    }

    protected void createCreditCard(final Order pOrder, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws CommerceException, RepositoryException {
        CPSCreditCard creditCard = (CPSCreditCard) getCartModifierService().createCreditCardPaymentIfNotExists(pOrder);

        CPSContactInfo billingAddress = getBillingAddress();
        if (billingAddress != null) {
            creditCard.setBillingAddress(billingAddress);
            creditCard.setJdeAddressNumber(billingAddress.getJdeAddressNumber());
        }
    }

    protected void removeCreditCard(final Order pOrder, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws CommerceException, RepositoryException {
        getCartModifierService().removeCreditCardPayment(pOrder);
    }

    /**
     * set shipping address to user on cart
     *
     * @param pRequest  request
     * @param pResponse response
     * @return false
     * @throws ServletException if error occurs
     * @throws IOException      if error occurs
     */
    public boolean handleCreateCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {

         final String handleMethod = "CPSCreditCardFormHandler.handleCreateCreditCard";
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();

        if (null == rrm || rrm.isUniqueRequestEntry(handleMethod)) {
            TransactionManager tm = getTransactionManager();
            TransactionDemarcation td = new TransactionDemarcation();
            boolean rollback = false;
            try{
                if (tm != null){
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }

                final CPSOrderImpl order = (CPSOrderImpl)getOrder();

                synchronized (order) {
                    order.setMtrRequested(isMtrRequested());
                    createCreditCard(order, pRequest, pResponse);
                    getOrderManager().updateOrder(order);
                }

                AjaxUtils.addAjaxResponse(this, pResponse, null);
            } catch (final Exception e) {
                if (isLoggingError()){
                    logError(e);
                }
                rollback = true;
            } finally {
                if (null != rrm) {
                    rrm.removeRequestEntry(handleMethod);
                }
                if (tm != null){
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException pE) {
                        if (isLoggingError()){
                            logError(pE);
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean handleRemoveCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        final String handleMethod = "CPSCreditCardFormHandler.handleRemoveCreditCard";
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();

        if (null == rrm || rrm.isUniqueRequestEntry(handleMethod)) {
            TransactionManager tm = getTransactionManager();
            TransactionDemarcation td = new TransactionDemarcation();
            boolean rollback = false;
            try{
                if (tm != null){
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }

                final CPSOrderImpl order = (CPSOrderImpl)getOrder();

                synchronized (order) {
                    removeCreditCard(order, pRequest, pResponse);
                    getOrderManager().updateOrder(order);
                }

                AjaxUtils.addAjaxResponse(this, pResponse, null);
            } catch (final Exception e) {
                if (isLoggingError()){
                    logError(e);
                }
                rollback = true;
            } finally {
                if (null != rrm) {
                    rrm.removeRequestEntry(handleMethod);
                }
                if (tm != null){
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException pE) {
                        if (isLoggingError()){
                            logError(pE);
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean isMtrRequested() {
        return mMtrRequested;
    }

    public void setMtrRequested(boolean pMtrRequested) {
        mMtrRequested = pMtrRequested;
    }

    public CPSCartModifierService getCartModifierService() {
        return mCartModifierService;
    }

    public void setCartModifierService(CPSCartModifierService pCartModifierService) {
        mCartModifierService = pCartModifierService;
    }
}
