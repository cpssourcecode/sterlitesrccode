package com.cps.commerce.order;

import atg.nucleus.Nucleus;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import java.util.List;

/**
 * If user selected pick up during checkout the value should be set to "S7"
 * otherwise the value should be set to S6
 *
 * @author Dmitry Golubev
 */
public class WebOrderTypePropertyDescriptor extends RepositoryPropertyDescriptor {

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		String ITEM_DESC_ORDER = "order";

		if(pItem == null || !ITEM_DESC_ORDER.equals(pItem.getItemDescriptor().getItemDescriptorName())) {
			return null;
		}
		try {
			String PRP_ORDER_SHIPPING_GROUPS = "shippingGroups";
			String ITEM_DESC_IN_STORE_PICKUP_SHIP_GROUP = "inStorePickupShippingGroup";
			String ORDER_WEB_TYPE_PICKUP = "S7";
			String ORDER_WEB_TYPE_REGULAR = "S6";

			List<RepositoryItem> shippingGroups = (List<RepositoryItem>) pItem.getPropertyValue(PRP_ORDER_SHIPPING_GROUPS);
			if(shippingGroups!=null && shippingGroups.size() > 0) {
				String shippingGroupName = shippingGroups.get(0).getItemDescriptor().getItemDescriptorName();
				if(ITEM_DESC_IN_STORE_PICKUP_SHIP_GROUP.equals(shippingGroupName)) {
					return ORDER_WEB_TYPE_PICKUP;
				} else {
					return ORDER_WEB_TYPE_REGULAR;
				}
			}
			return ORDER_WEB_TYPE_REGULAR;
		} catch (RepositoryException e) {
			Nucleus.getGlobalNucleus().vlogError("Unable to get web order type: {0}", e.toString());
		}
		return null;
	}

	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		//
	}
}
