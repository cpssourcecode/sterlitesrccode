package com.cps.commerce.order;

import atg.commerce.order.TokenizedCreditCard;
import com.cps.util.CPSConstants;

import atg.payment.creditcard.CreditCardInfo;

/**
 * 
 * @author Jason Robert Juvingo
 * 
 */
public class CPSCreditCard extends TokenizedCreditCard implements CreditCardInfo {
	
	public String getJdeAddressNumber() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_JDE_ADDRESS_NUMBER);
	}

	public void setJdeAddressNumber(String pJdeAddressNumber) {
		setPropertyValue(CPSConstants.CHECKOUT_JDE_ADDRESS_NUMBER, pJdeAddressNumber);
	}
	
	public String getCardHolderFirstName() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_CREDIT_CARD_FIRST_NAME);
	}

	public void setCardHolderFirstName(String pCardHolderFirstName) {
		setPropertyValue(CPSConstants.CHECKOUT_CREDIT_CARD_FIRST_NAME, pCardHolderFirstName);
	}

	public String getCardHolderLastName() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_CREDIT_CARD_LAST_NAME);
	}

	public void setCardHolderLastName(String pCardHolderLastName) {
		setPropertyValue(CPSConstants.CHECKOUT_CREDIT_CARD_LAST_NAME, pCardHolderLastName);
	}

	public void setPONumber(String pPONumber) {
		setPropertyValue(CPSConstants.PO_NUMBER_PROPERTY, pPONumber);
	}

	public String getPONumber() {
		return ((String) getPropertyValue(CPSConstants.PO_NUMBER_PROPERTY));
	}
}
