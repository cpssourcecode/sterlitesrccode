package com.cps.commerce.order;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import static com.cps.util.CPSConstants.*;


/**
 * Get Store from Location repository by locationId
 *
 * @author Steve Neverov
 */
public class StorePropertyDescriptor extends RepositoryPropertyDescriptor {

	/**
	 * /atg/commerce/locations/LocationRepository
	 */
	private Repository mLocationRepository;

	public Repository getLocationRepository() {
		if (mLocationRepository == null) {
			mLocationRepository = (Repository) Nucleus.getGlobalNucleus().resolveName("/atg/commerce/locations/LocationRepository");
		}
		return mLocationRepository;
	}

	//------------------------------------------------------------------------------------------------------------------

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

		if (SHIPPPING_TYPE_IN_STORE_PICKUP.equals(pItem.getItemDescriptor().getItemDescriptorName())) {
			try {
				String locationId = (String) pItem.getPropertyValue(ISPU_LOCATION_ID_PRTY);
				if (StringUtils.isBlank(locationId)) {
					Nucleus.getGlobalNucleus().vlogError("inStorePickupShippingGroup {0} has no specified locationId", pItem.getRepositoryId());
				} else {
					RepositoryItem store = getLocationRepository().getItem(locationId, STORE_ITEM_TYPE);
					if (store == null) {
						Nucleus.getGlobalNucleus().vlogError("Unable to load store by locationId {0} from inStorePickupShippingGroup {1}", locationId, pItem.getRepositoryId());
					} else {
						return store;
					}
				}
			} catch (RepositoryException e) {
				Nucleus.getGlobalNucleus().vlogError("Unable to get inStorePickupShippingGroup {0} branchId:", e.toString());
			}
		}
		return null;

	}

	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		//
	}

}
