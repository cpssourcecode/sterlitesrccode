package com.cps.commerce.order;

import atg.commerce.order.InStorePickupShippingGroup;
import atg.repository.RepositoryItem;

import static com.cps.util.CPSConstants.*;

public class CPSInStorePickupShippingGroup extends InStorePickupShippingGroup {

	public RepositoryItem getStore() {
		return (RepositoryItem)getRepositoryItem().getPropertyValue(STORE_ITEM_TYPE);
	}

}
