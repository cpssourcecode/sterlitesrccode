package com.cps.commerce.order;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import java.util.List;

/**
 * If user selected pick up during checkout the value should be set to "S7"
 * otherwise the value should be set to S6
 *
 * @author Dmitry Golubev
 */
public class CustomerEmailPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
	 * /atg/userprofiling/ProfileAdapterRepository
	 */
	private Repository mProfileRepository;

	//------------------------------------------------------------------------------------------------------------------

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		String ITEM_DESC_ORDER = "order";

		if(pItem == null || !ITEM_DESC_ORDER.equals(pItem.getItemDescriptor().getItemDescriptorName())) {
			return null;
		}
		try {
			String PRP_ORDER_PROFILE_ID = "profileId";
			String ITEM_DESC_USER = "user";
			String PRP_USER_LOGIN = "login";

			String profileId = (String) pItem.getPropertyValue(PRP_ORDER_PROFILE_ID);
			if(StringUtils.isBlank(profileId)) {
				Nucleus.getGlobalNucleus().vlogError("Order {0} has no specified profile id", pItem.getRepositoryId());
			} else {
				RepositoryItem user = getProfileRepository().getItem(profileId, ITEM_DESC_USER);
				if(user == null) {
					Nucleus.getGlobalNucleus().vlogError(
							"Unable to load user by id {0} from order {1}", profileId, pItem.getRepositoryId()
					);
				} else {
					return user.getPropertyValue(PRP_USER_LOGIN);
				}
			}

		} catch (RepositoryException e) {
			Nucleus.getGlobalNucleus().vlogError("Unable to get order customer email: {0}", e.toString());
		}
		return null;
	}

	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		//
	}

	//------------------------------------------------------------------------------------------------------------------

	public Repository getProfileRepository() {
		if(mProfileRepository == null) {
			mProfileRepository = (Repository) Nucleus.getGlobalNucleus().resolveName(
					"/atg/userprofiling/ProfileAdapterRepository"
			);
		}
		return mProfileRepository;
	}
}
