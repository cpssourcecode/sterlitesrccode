package com.cps.commerce.order;

import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.json.JSONException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.commerce.order.service.CPSCartValidationService;
import vsg.util.VSGFormUtils;

import javax.servlet.ServletException;
import javax.transaction.Transaction;
import java.io.IOException;

/**
 * @author Dmitry Golubev
 **/
public class CPSCartValidationHandler extends CartModifierFormHandler {
	/**
	 * cart validation service
	 */
	private CPSCartValidationService mCartValidationService;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false - everything is OK, true - stay on the page
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleCheckCart(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException {
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "CartValidationHandler.handleCheckCart";

		vlogDebug(myHandleMethod + " start");

		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				synchronized (getOrder()) {
					getCartValidationService().checkOrder(this, pRequest, pResponse);
				}
			} catch (Exception e) {
				vlogError(e, "Unable to check cart");
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
			}
		}
		try {
			if (!getFormError()) {
				VSGFormUtils.addAjaxSuccessResponse(pResponse);
			} else {
				VSGFormUtils.addAjaxErrorResponse(this, pResponse);
			}
		} catch (JSONException e) {
			logError(e);
		}
		return false;
	}

	/**
	 * delete items (public method for service)
	 * @param pRequest request
	 * @param pResponse response
	 * @throws ServletException if exception occurs
	 * @throws IOException  if exception occurs
	 */
	@Override
	public void deleteItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws
			ServletException, IOException {
		super.deleteItems(pRequest, pResponse);
	}

	//------------------------------------------------------------------------------------------------------------------

	public CPSCartValidationService getCartValidationService() {
		return mCartValidationService;
	}

	public void setCartValidationService(CPSCartValidationService pCartValidationService) {
		mCartValidationService = pCartValidationService;
	}
}
