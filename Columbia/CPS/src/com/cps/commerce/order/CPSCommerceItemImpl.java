package com.cps.commerce.order;

import atg.commerce.order.CommerceItemImpl;

import static com.cps.util.CPSConstants.AVAILABLE_QTY;
import static com.cps.util.CPSConstants.DEFAULT_BRANCH_QTY;
import static com.cps.util.CPSConstants.IS_AVAILABILITY_CHECKED;
import static com.cps.util.CPSConstants.IS_SCHEDULED_ITEM;
import static com.cps.util.CPSConstants.LEAD_TIME;
import static com.cps.util.CPSConstants.SCHEDULE_DAYS;

/**
 * @author Jason Robert Juvingo
 */
public class CPSCommerceItemImpl extends CommerceItemImpl {

	public int getDefaultBranchQty() {
		Integer defaultBranchQty = (Integer) this.getPropertyValue(DEFAULT_BRANCH_QTY);
		return defaultBranchQty == null ? 0 : defaultBranchQty.intValue();
	}

	public void setDefaultBranchQty(int pDefaultBranchQty) {
		this.setPropertyValue(DEFAULT_BRANCH_QTY, pDefaultBranchQty);
	}

	public int getLeadTime() {
		Integer leadTime = (Integer) getPropertyValue(LEAD_TIME);
		return leadTime == null ? 0 : leadTime.intValue();
	}

	public void setLeadTime(int pLeadTime) {
		setPropertyValue(LEAD_TIME, pLeadTime);
	}

	public int getAvailableQuantity() {
		Integer quantity = (Integer) this.getPropertyValue(AVAILABLE_QTY);
		return quantity == null ? 0 : quantity.intValue();
	}

	public void setAvailableQuantity(int pQuantity) {
		this.setPropertyValue(AVAILABLE_QTY, pQuantity);
	}

	public Integer getScheduleDays() {
		return (Integer) this.getPropertyValue(SCHEDULE_DAYS);
	}

	public void setScheduleDays(Integer pScheduleDays) {
		this.setPropertyValue(SCHEDULE_DAYS, pScheduleDays);
	}

	public boolean isScheduledItem() {
		//Integer scheduled = (Integer) this.getPropertyValue(IS_SCHEDULED_ITEM);
		//return scheduled != 0;
		return Boolean.valueOf(this.getPropertyValue(IS_SCHEDULED_ITEM).toString());
	}

	public boolean getIsScheduledItem() {
		//Integer scheduled = (Integer) this.getPropertyValue(IS_SCHEDULED_ITEM);
		//return scheduled != 0;
		return isScheduledItem();
	}

	public void setScheduledItem(boolean pScheduledItem) {
		this.setPropertyValue(IS_SCHEDULED_ITEM, pScheduledItem);
	}

	public boolean getIsAvailabilityChecked() {
		Boolean isAvailabilityChecked = (Boolean)getPropertyValue(IS_AVAILABILITY_CHECKED);
		if (isAvailabilityChecked == null) {
			isAvailabilityChecked = false;
		}
		return isAvailabilityChecked;
	}

	public void setIsAvailabilityChecked(boolean pIsAvailabilityChecked) {
		setPropertyValue(IS_AVAILABILITY_CHECKED, pIsAvailabilityChecked);
	}

}
