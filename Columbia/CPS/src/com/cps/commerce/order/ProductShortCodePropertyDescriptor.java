package com.cps.commerce.order;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.repository.*;

/**
 * Short code is the stored as display name on product and sku level
 *
 * @author Dmitry Golubev
 */
public class ProductShortCodePropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
	 * /atg/commerce/catalog/ProductCatalog/
	 */
	private Repository mProductCatalog;

	//------------------------------------------------------------------------------------------------------------------

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		String ITEM_DESC_COMMERCE_ITEM = "commerceItem";

		if(pItem == null || !ITEM_DESC_COMMERCE_ITEM.equals(pItem.getItemDescriptor().getItemDescriptorName())) {
			return null;
		}
		try {
			String PRP_COMMERCE_ITEM_PRODUCT_ID = "productId";

			String ITEM_DESC_PRODUCT = "product";
			String PRP_PRODUCT_DISPLAY_NAME = "displayName";

			String productId = (String) pItem.getPropertyValue(PRP_COMMERCE_ITEM_PRODUCT_ID);

			if(StringUtils.isBlank(productId)) {
				Nucleus.getGlobalNucleus().vlogError("Commerce item {0} has no specified product id", pItem.getRepositoryId());
			} else {
				RepositoryItem product = getProductCatalog().getItem(productId, ITEM_DESC_PRODUCT);
				if(product == null) {
					Nucleus.getGlobalNucleus().vlogError(
							"Unable to load product by id {0} from commerce item {1}", productId, pItem.getRepositoryId()
					);
				} else {
					return product.getPropertyValue(PRP_PRODUCT_DISPLAY_NAME);
				}
			}

		} catch (RepositoryException e) {
			Nucleus.getGlobalNucleus().vlogError("Unable to get commerce item short product code: {0}", e.toString());
		}
		return null;
	}

	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		//
	}

	//------------------------------------------------------------------------------------------------------------------

	public Repository getProductCatalog() {
		if(mProductCatalog== null) {
			mProductCatalog = (Repository) Nucleus.getGlobalNucleus().resolveName(
					"/atg/commerce/catalog/ProductCatalog/"
			);
		}
		return mProductCatalog;
	}
}
