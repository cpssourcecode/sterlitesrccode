package com.cps.commerce.order.processor;

import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.PipelineConstants;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.service.idgen.IdGeneratorException;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

public class ProcAddJDEOrderNumberToOrder extends ApplicationLoggingImpl implements PipelineProcessor {

	
	
	private final int SUCCESS = 1;
	static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";
	static final String USER_MSGS_RES_NAME = "atg.commerce.order.UserMessages";
	private static java.util.ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());
	private static java.util.ResourceBundle sUserResourceBundle = LayeredResourceBundle.getBundle(USER_MSGS_RES_NAME, java.util.Locale.getDefault());
	@Override
	public int[] getRetCodes() {
		int[] ret = {SUCCESS};
		return ret;
	}

	@Override
	public int runProcess(Object pParam, PipelineResult pPipelineresult) throws Exception {
		Map lMap=(Map)pParam;
		CPSOrderImpl order=(CPSOrderImpl)lMap.get(PipelineConstants.ORDER);
		Locale locale = (Locale) lMap.get(PipelineConstants.LOCALE);
		CPSOrderManager orderManager=(CPSOrderManager)lMap.get(PipelineConstants.ORDERMANAGER);
		ResourceBundle resourceBundle;
	
		if (locale == null) {
			resourceBundle = sUserResourceBundle;
		} else {
			resourceBundle = LayeredResourceBundle.getBundle(USER_MSGS_RES_NAME, locale);
		}
		
		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource("InvalidOrderParameter",MY_RESOURCE_NAME, sResourceBundle));
		}
		
		try{
			String jdeOrderNumber = orderManager.getJdeOrderNumber();
			vlogDebug("ProcAddJDEOrderNumberToOrder-runProcess():JdeOrderNumber : " + jdeOrderNumber);
			order.setJdeOrderNumber(jdeOrderNumber);
		} catch (IdGeneratorException e) {
			if(isLoggingError()){
				logError("exception at assigning jdeOrder number to order:"+e);
			}
			pPipelineresult.addError("AssignJdeOrderNumber", "Exception while assinging jdeOrder number to order.");
		}
	
		if(pPipelineresult.hasErrors()) {
			return STOP_CHAIN_EXECUTION_AND_ROLLBACK;
		}
		
		return SUCCESS;
	}
	
}
