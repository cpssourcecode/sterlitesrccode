package com.cps.commerce.order.processor;

import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.PipelineConstants;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLoggingImpl;
import atg.repository.RepositoryException;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;

import java.util.HashMap;

public class CPSProcUpdateOrderWithWebOrderId extends ApplicationLoggingImpl implements PipelineProcessor {

	protected static final String MY_RESOURCE_NAME = "atg.commerce.order.OrderResources";

	/** Resource Bundle **/
	protected static java.util.ResourceBundle sResourceBundle =
			java.util.ResourceBundle.getBundle(MY_RESOURCE_NAME, atg.service.dynamo.LangLicense.getLicensedDefault());

	private String mLoggingIdentifier = "ProcUpdateOrderWithWebOrderId";

	public void setLoggingIdentifier(String pLoggingIdentifier) {
		mLoggingIdentifier = pLoggingIdentifier;
	}

	public String getLoggingIdentifier() {
		return mLoggingIdentifier;
	}

	private CPSOrderManager mOrderManager;

	public void setOrderManager(CPSOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public CPSOrderManager getOrderManager() {
		return mOrderManager;
	}

	protected final int SUCCESS = 1;

	/**
	 * OOB return codes available for this processor
	 * @see atg.service.pipeline.PipelineProcessor#getRetCodes()
	 */
	public int[] getRetCodes() {
		int[] ret = { SUCCESS };
		return ret;
	}

//	private Long mSeedValue;
//
//	public Long getSeedValue() {
//		return mSeedValue;
//	}
//
//	public void setSeedValue(Long pSeedValue) {
//		mSeedValue = pSeedValue;
//	}

	// -----------------------------------------------
	/**
	 * This method set webOrderId to the order.
	 *
	 * @param pParam
	 *            a HashMap which must contain an Order object
	 * @param pResult
	 *            a PipelineResult object which stores any information which
	 *            must be returned from this method invocation
	 * @return an integer specifying the processor's return code
	 * @exception Exception
	 *                throws any exception back to the caller
	 * @see atg.service.pipeline.PipelineProcessor#runProcess(Object, atg.service.pipeline.PipelineResult)
	 */
	public int runProcess(Object pParam, PipelineResult pResult) throws Exception {

		HashMap map = (HashMap) pParam;
		Order order = (Order) map.get(PipelineConstants.ORDER);

		if (order == null) {
			throw new InvalidParameterException(ResourceUtils.getMsgResource(
					"InvalidOrderParameter", MY_RESOURCE_NAME, sResourceBundle));
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append("Update order:").append(order.getId()).append(" with webOrderId").toString());
		}

		updateOrderWithWebOrderId(order);

		return SUCCESS;
	}

	private void updateOrderWithWebOrderId(Order pOrder) throws RepositoryException {

		if (isLoggingDebug()) {
			logDebug("CPSProcUpdateOrderWithWebOrderId.updateOrderWithWebOrderId - start");
		}

		CPSOrderImpl order = (CPSOrderImpl) pOrder;
		String webOrderId = order.getWebOrderId();
		if (StringUtils.isBlank(webOrderId)) {
			getOrderManager().setWebOrderId(order);
			vlogDebug("Order {0} updated with webOrderId {1} ", order.getId(), order.getWebOrderId());
		} else {
			if (isLoggingDebug()){
				vlogDebug("Order {0} has webOrderId {1} ", order.getId(), webOrderId);
			}
		}
		if (isLoggingDebug()){
			logDebug("CPSProcUpdateOrderWithWebOrderId.updateOrderWithWebOrderId - exit");
		}

	}

}
