package com.cps.commerce.order.processor;

import atg.commerce.order.CreditCard;
import atg.commerce.order.processor.ProcValidateCreditCard;
import atg.service.pipeline.PipelineResult;

import java.util.ResourceBundle;

public class CPSProcValidateCreditCard extends ProcValidateCreditCard {

    private boolean mValidationEnabled;

    @Override
    protected void validateCreditCardFields(CreditCard cc, PipelineResult pResult, ResourceBundle pResourceBundle) {
        if (isValidationEnabled()) {
            super.validateCreditCardFields(cc, pResult, pResourceBundle);
        }
    }

    public boolean isValidationEnabled() {
        return mValidationEnabled;
    }

    public void setValidationEnabled(boolean pValidationEnabled) {
        mValidationEnabled = pValidationEnabled;
    }
}
