package com.cps.commerce.order;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.cps.util.CPSConstants.QUERY_ALL;

public class CPSHolidaysService extends GenericService {
    private static final String DATE = "date";
    private static final String HOLIDAYS = "holidays";

    private Repository mDateRepository;
    private boolean mEnabled;

    public boolean isTodayWeekend() {
        Calendar today = Calendar.getInstance();
        return isDateWeekend(today);
    }

    public boolean isTodayHoliday() {
        Calendar today = Calendar.getInstance();
        return isDateHoliday(today);
    }

    public Calendar getDelayedReorderDate(Calendar plannedReorderDate) {
        while (isDateWeekend(plannedReorderDate) || isDateHoliday(plannedReorderDate)) {
            plannedReorderDate.add(Calendar.DATE, 1);
        }
        return plannedReorderDate;
    }

    private boolean isDateWeekend(Calendar today) {
        int currentDayOfWeek = today.get(Calendar.DAY_OF_WEEK);
        return currentDayOfWeek == Calendar.SATURDAY || currentDayOfWeek == Calendar.SUNDAY;
    }

    private boolean isDateHoliday(Calendar today) {
        RepositoryItem[] holidaysItems = queryHolidays();
        if (holidaysItems != null) {

            List<Date> holidaysDates = getHolidaysDateList(holidaysItems);
            for (Date date : holidaysDates) {
                Calendar holiday = Calendar.getInstance();
                holiday.setTime(date);
                if (today.get(Calendar.YEAR) == holiday.get(Calendar.YEAR) && today.get(Calendar.DAY_OF_YEAR) == holiday.get(Calendar.DAY_OF_YEAR)) {
                    return true;
                }
            }
        }

        return false;
    }

    private RepositoryItem[] queryHolidays() {
        RepositoryItem[] items = null;
        try {
            RepositoryView repositoryView = getDateRepository().getView(HOLIDAYS);
            RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_ALL);
            Object params[] = new Object[0];
            items = statement.executeQuery(repositoryView, params);
        } catch (RepositoryException e) {
            vlogError(e, "Repository exception during querying Holidays.");
        }
        return items;
    }

    private List<Date> getHolidaysDateList(RepositoryItem[] items) {
        List<Date> holidays = new ArrayList<>();
        for (RepositoryItem item : items) {
            holidays.add((Date) item.getPropertyValue(DATE));
        }
        return holidays;
    }

    public Repository getDateRepository() {
        return mDateRepository;
    }

    public void setDateRepository(Repository pDateRepository) {
        mDateRepository = pDateRepository;
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean pEnabled) {
        mEnabled = pEnabled;
    }
}
