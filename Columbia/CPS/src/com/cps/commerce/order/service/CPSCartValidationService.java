package com.cps.commerce.order.service;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.droplet.DropletException;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.commerce.order.CPSCartValidationHandler;
import vsg.messages.IMessagesTools;
import vsg.messages.MessageKey;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Dmitry Golubev
 **/
public class CPSCartValidationService extends GenericService implements ICPSCartValidationService {
	/**
	 * message tools
	 */
	private IMessagesTools mMessagesTools;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * checks order and remove 'wrong' items
	 * @param pHandler CartValidationHandler
	 * @param pRequest request
	 * @param pResponse response
	 */
	public void checkOrder(CPSCartValidationHandler pHandler,
						   DynamoHttpServletRequest pRequest,
						   DynamoHttpServletResponse pResponse)
		throws ServletException, IOException
	{
		Order order = pHandler.getOrder();
		List<CommerceItem> commerceItems = order.getCommerceItems();
		Set<CommerceItem> itemsToRemove = new HashSet<CommerceItem>();
		for(CommerceItem commerceItem : commerceItems) {
			boolean removeItem = false;

			removeItem = isItemUnderNotECommerceCategory(commerceItem); // additional checks later with &&

			if(removeItem) {
				itemsToRemove.add(commerceItem);
			}
		}
		removeItems(pHandler, itemsToRemove, pRequest, pResponse);
	}

	/**
	 * @param pCommerceItem commerce item
	 * @return true if item is under category, which eCommerceDisplay is true
	 */
	private boolean isItemUnderNotECommerceCategory(CommerceItem pCommerceItem) {
		RepositoryItem product = (RepositoryItem) pCommerceItem.getAuxiliaryData().getProductRef();
		RepositoryItem parentCategory = (RepositoryItem) product.getPropertyValue(PROP_PARENT_CATEGORY);
		if(parentCategory == null) {
			vlogError("No parent category for product: "+ product.getRepositoryId());
			return false;
		}
		return (boolean) parentCategory.getPropertyValue(PROP_ECOMMERCE_DISPLAY);
	}

	/**
	 * remove items from order
	 * @param pHandler CartValidationHandler
	 * @param pRequest request
	 * @param pResponse response
	 * @param pItemsToRemove items to remove
	 */
	private void removeItems(CPSCartValidationHandler pHandler,
							 Set<CommerceItem> pItemsToRemove,
							 DynamoHttpServletRequest pRequest,
							 DynamoHttpServletResponse pResponse)
		throws ServletException, IOException
	{
		if(pItemsToRemove.isEmpty()) {
			return;
		}

		pHandler.setRemovalCommerceIds(getRemovableItemsIds(pItemsToRemove));
		pHandler.deleteItems(pRequest, pResponse);

		String names = getRemovableItemsNames(pItemsToRemove);
		pHandler.addFormException(new DropletException(
			getMessagesTools().getMessage(MessageKey.ERR_SC_ITEMS_DONOT_EXIST, names )
		));
	}

	/**
	 * @param pItems items to remove
	 * @return String with their names
	 */
	private String getRemovableItemsNames(Set<CommerceItem> pItems) {
		StringBuilder productNames = new StringBuilder();
		for (CommerceItem item : pItems) {
			productNames.append(
				((RepositoryItem) item.getAuxiliaryData().getProductRef()).getPropertyValue(PROP_DISPLAY_NAME)
			).append(",");
		}
		if (productNames.length() > 0) {
			productNames.deleteCharAt(productNames.length() - 1);
		}
		return productNames.toString();
	}

	/**
	 * @param pItems items to remove
	 * @return array of ids
	 */
	private String[] getRemovableItemsIds(Set<CommerceItem> pItems) {
		String[] itemsIds = new String[pItems.size()];
		int i=0;
		for (CommerceItem item : pItems) {
			itemsIds[i] = item.getId();
			i++;
		}
		return itemsIds;
	}


	//------------------------------------------------------------------------------------------------------------------

	public IMessagesTools getMessagesTools() {
		return mMessagesTools;
	}

	public void setMessagesTools(IMessagesTools pMessagesTools) {
		mMessagesTools = pMessagesTools;
	}
}
