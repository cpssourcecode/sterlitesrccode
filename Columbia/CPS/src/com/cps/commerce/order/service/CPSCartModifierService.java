package com.cps.commerce.order.service;

import atg.commerce.CommerceException;
import atg.commerce.order.*;
import atg.nucleus.GenericService;
import atg.repository.*;
import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.commerce.order.purchase.CPSCartModifierFormHandler;
import com.cps.core.util.CPSContactInfo;
import com.cps.integrations.order.GHAccountClient;
import com.cps.util.CPSConstants;
import com.cps.util.StoreTools;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 *
 * Service for Cart Modifier Form Handler
 *
 */
public class CPSCartModifierService extends GenericService implements CPSConstants{

	private static final String PAYMENT_GROUP_TYPE_INVOICE_REQUEST = "invoiceRequest";
	private static final String PAYMENT_GROUP_TYPE_CREDIT_CARD = "creditCard";

	/**
	 * haveItShipped checkbox
	 */
	private static final String HAVE_IT_SHIPPED = "haveItShipped";
	/**
	 * pickItUp checkbox
	 */
	private static final String PICK_IT_UP = "pickItUp";

	private static final String ITEM_DESCRIPTOR_CONTACT_INFO = "contactInfo";

	/**
	 * profile repository
	 */
	private Repository mProfileRepository;
	/**
	 * Shipping group manager
	 */
	private ShippingGroupManager mShippingGroupManager;

	private PaymentGroupManager mPaymentGroupManager;

	private CommerceItemManager mCommerceItemManager;


	private GHAccountClient mGHAccountClient;
	private StoreTools mStoreTools;

	/**
	 * update profile selected delivery method
	 * @param pHandler -  CPSCartModifierFormHandler
	 */
	public void updateProfileFromAvailabilityModal(CPSCartModifierFormHandler pHandler){

		if(HAVE_IT_SHIPPED.equals(pHandler.getShippingMethodOnAvailabilityModal())){
			RepositoryItem defualtCS = (RepositoryItem) pHandler.getProfile().getPropertyValue(SELECTED_CS);
			pHandler.getCurrentProfile().setPropertyValue(CART_SELECTED_CS, defualtCS);
			pHandler.getCurrentProfile().setPropertyValue(CART_SELECTED_DELIVERY_METHOD, DELEVERY_METHOD_SHIPPED);
		} else if (PICK_IT_UP.equals(pHandler.getShippingMethodOnAvailabilityModal())){
			pHandler.getCurrentProfile().setPropertyValue(CART_SELECTED_STORE, pHandler.getSelectedLocationId());
			pHandler.getCurrentProfile().setPropertyValue(CART_SELECTED_DELIVERY_METHOD, DELEVERY_METHOD_PICK_UP);
		}

	}

	public void addItemsToShippingGroup(Order pOrder, ShippingGroup pShippingGroup) throws CommerceException {

		if (pOrder != null && pShippingGroup != null) {
			List<CommerceItem> commerceItems = pOrder.getCommerceItems();
			for (CommerceItem commerceItem : commerceItems) {
				getCommerceItemManager().addItemQuantityToShippingGroup(pOrder, commerceItem.getId(), pShippingGroup.getId(), commerceItem.getQuantity());
			}
		}

	}

	public void updateShippingAddress(Order pOrder, CPSContactInfo pAddress) throws CommerceException {
		CPSHardgoodShippingGroup shippingGroup = (CPSHardgoodShippingGroup) createHardgoodShipGroupIfNotExists(pOrder);
		shippingGroup.setShippingAddress(pAddress);
	}

	public void updateBillingAddress(Order pOrder, CPSContactInfo pAddress){
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup pg : paymentGroups){
			if (pg instanceof InvoiceRequest){
				((InvoiceRequest) pg).setBillingAddress(pAddress);
			}
			else if (pg instanceof CreditCard){
				((CreditCard) pg).setBillingAddress(pAddress);
			}
		}
	}

	public void updateShippingGroups(String pDeliveryMethod, String pShippingAddressId, Order pOrder, RepositoryItem pProfile) throws CommerceException, RepositoryException {

		if (StringUtils.isNotEmpty(pDeliveryMethod)){
			switch (pDeliveryMethod) {
				case DELEVERY_METHOD_SHIPPED: {
					CPSHardgoodShippingGroup shippingGroup = (CPSHardgoodShippingGroup) createHardgoodShipGroupIfNotExists(pOrder);
					CPSContactInfo shippingAddress = findContactInfoById(pShippingAddressId);
					shippingGroup.setShippingAddress(shippingAddress);
					shippingGroup.setJdeAddressNumber(shippingAddress.getJdeAddressNumber());
					break;
				}
				case DELEVERY_METHOD_PICK_UP: {
					InStorePickupShippingGroup shippingGroup = createInStorePickupShippingGroupIfNotExists(pOrder);
					RepositoryItem selectedCS = (RepositoryItem) pProfile.getPropertyValue(SELECTED_CS);  // contactInfo ShipTO address
					String jdeAddressNumber = "";
					if (null != selectedCS) {
						jdeAddressNumber = (String) selectedCS.getPropertyValue(CONTACT_INFO_JDE_ADDRESS_NUMBER);
					}
					if(pProfile.isTransient() && StringUtils.isBlank(jdeAddressNumber)){
						jdeAddressNumber = getJdeAddressNumberForGuest(pShippingAddressId);
					}
					// set jdeAddressNumber
					shippingGroup.setPropertyValue(JDE_ADDRESS_NUM, jdeAddressNumber);
					shippingGroup.setLocationId(pShippingAddressId);
					break;
				}
			}
		}

	}

	private String getJdeAddressNumberForGuest(String pLocationId){
		String jdeAddressNumber = "";
		StoreTools storeTools = getStoreTools();
		GHAccountClient ghAccountClient = getGHAccountClient();
		if(storeTools != null && ghAccountClient != null){
			RepositoryItem store = storeTools.findStoreByLocationId(pLocationId);
			String city = (String) store.getPropertyValue(ADDRESS_CITY);
			String state = (String) store.getPropertyValue(STATE_ADDRESS);
			try {
				Map<String,String> results = ghAccountClient.getEComDetails(city,state,isLoggingDebug());
				jdeAddressNumber = results.get(ADDRESS_NUMBER);
			} catch (InterruptedException | IOException | ExecutionException e) {
				vlogDebug("Error while getting jdeNumber for City: {0},State: {1}", city, state);
			}
		}
		return jdeAddressNumber;
	}

	public CPSContactInfo findContactInfoById(String contactInfoId) throws RepositoryException, CommerceException {
		RepositoryItem repositoryContactInfo = getProfileRepository().getItem(contactInfoId, ITEM_DESCRIPTOR_CONTACT_INFO);
		if (repositoryContactInfo != null) {
			CPSContactInfo contactInfo = new CPSContactInfo();
			OrderTools.copyAddress(repositoryContactInfo, contactInfo);
			return contactInfo;
		} else {
			throw new RepositoryException();
		}
	}

	public HardgoodShippingGroup createHardgoodShipGroupIfNotExists(Order order) throws CommerceException {
		if (order.getShippingGroupCount() > 0) {
			List<ShippingGroup> shippingGroups = order.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof HardgoodShippingGroup) {
					return (HardgoodShippingGroup) shippingGroup;
				}
			}
		}
		ShippingGroup shippingGroup = getShippingGroupManager().createShippingGroup(CPSConstants.SHIPPPING_TYPE_HARDGOOD);
		getShippingGroupManager().addShippingGroupToOrder(order, shippingGroup);
		List<ShippingGroup> exceptions = new ArrayList<>();
		exceptions.add(shippingGroup);
		getShippingGroupManager().removeAllShippingGroupsFromOrder(order, exceptions);
		addItemsToShippingGroup(order, shippingGroup);
		return (HardgoodShippingGroup) shippingGroup;
	}

	public InStorePickupShippingGroup createInStorePickupShippingGroupIfNotExists(Order order) throws CommerceException {
		if (order.getShippingGroupCount() > 0) {
			List<ShippingGroup> shippingGroups = order.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof InStorePickupShippingGroup) {
					return (InStorePickupShippingGroup) shippingGroup;
				}
			}
		}
		ShippingGroup shippingGroup = getShippingGroupManager().createShippingGroup(CPSConstants.SHIPPPING_TYPE_IN_STORE_PICKUP);
		getShippingGroupManager().addShippingGroupToOrder(order, shippingGroup);
		List<ShippingGroup> exceptions = new ArrayList<>();
		exceptions.add(shippingGroup);
		getShippingGroupManager().removeAllShippingGroupsFromOrder(order, exceptions);
		addItemsToShippingGroup(order, shippingGroup);
		return (InStorePickupShippingGroup) shippingGroup;
	}

	public InvoiceRequest createInvoiceRequestPayGroupIfNotExists(Order order) throws CommerceException, RepositoryException {
		List<PaymentGroup> paymentGroups = order.getPaymentGroups();
		List<PaymentGroup> supportedPaymentGroups = new ArrayList<>();
		InvoiceRequest invoiceRequest = null;
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof InvoiceRequest) {
				invoiceRequest = (InvoiceRequest) paymentGroup;
				supportedPaymentGroups.add(paymentGroup);
				break;
			}
		}

		getPaymentGroupManager().removeAllPaymentGroupsFromOrder(order, supportedPaymentGroups);

		if (invoiceRequest == null) {
			invoiceRequest = (InvoiceRequest) getPaymentGroupManager().createPaymentGroup(PAYMENT_GROUP_TYPE_INVOICE_REQUEST);
			getPaymentGroupManager().addPaymentGroupToOrder(order, invoiceRequest);
		}
		return invoiceRequest;
	}

	public CreditCard createCreditCardPaymentIfNotExists(Order pOrder) throws CommerceException {
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		List<PaymentGroup> supportedPaymentGroups = new ArrayList<>();
		CreditCard creditCard = null;
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof CreditCard) {
				creditCard = (CreditCard) paymentGroup;
				supportedPaymentGroups.add(paymentGroup);
				break;
			}
		}

		getPaymentGroupManager().removeAllPaymentGroupsFromOrder(pOrder, supportedPaymentGroups);

		if (creditCard == null) {
			creditCard = (CreditCard) getPaymentGroupManager().createPaymentGroup(PAYMENT_GROUP_TYPE_CREDIT_CARD);
			getPaymentGroupManager().addPaymentGroupToOrder(pOrder, creditCard);
		}
		return creditCard;
	}

	public void removeCreditCardPayment(Order pOrder) throws CommerceException {
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();

		CreditCard creditCard = null;
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof CreditCard) {
				creditCard = (CreditCard) paymentGroup;
				break;
			}
		}

		if (creditCard != null) {
			getPaymentGroupManager().removePaymentGroupFromOrder(pOrder, creditCard.getPaymentId());
		}
	}

    public void createContactInfoForAddress(CPSContactInfo pAddress) throws RepositoryException, CommerceException {


        MutableRepository profileRepository = (MutableRepository) getProfileRepository();
        MutableRepositoryItem contactInfo = profileRepository.createItem(CONTACT_INFO);
        OrderTools.copyAddress(pAddress,contactInfo);

        /*contactInfo.setPropertyValue(EMAIL_ADDRESS, pAddress.getEmail());
        contactInfo.setPropertyValue(BUSINESS_UNIT, pAddress.getBusinessUnit());
        contactInfo.setPropertyValue(CONTACT_INFO_JDE_ADDRESS_NUMBER, pAddress.getJdeAddressNumber());
        contactInfo.setPropertyValue(PHONE_NUMBER, pAddress.getPhoneNumber());
        contactInfo.setPropertyValue(ADDRESS1, pAddress.getAddress1());
        contactInfo.setPropertyValue(ADDRESS2, pAddress.getAddress2());
        contactInfo.setPropertyValue(CITY, pAddress.getCity());
        contactInfo.setPropertyValue(COUNTRY, pAddress.getCountry());
        contactInfo.setPropertyValue(POSTAL_CODE, pAddress.getPostalCode());
        contactInfo.setPropertyValue(STATE, pAddress.getState());
        contactInfo.setPropertyValue(COMPANY_NAME, pAddress.getCompanyName());
        contactInfo.setPropertyValue(FIRST_NAME, pAddress.getFirstName());
        contactInfo.setPropertyValue(LAST_NAME, pAddress.getLastName());*/
        profileRepository.addItem(contactInfo);
    }

    public void updateJdeAddressNumber(CPSContactInfo pAddress) {
        String city = pAddress.getCity();
        String state = pAddress.getState();
        boolean isDebug = isLoggingDebug();
        try {
            GHAccountClient ghAccountClient = getGHAccountClient();
            if(ghAccountClient != null) {
                Map<String, String> results = ghAccountClient.getEComDetails(city, state, isDebug);
                String addressNumber = results.get(ADDRESS_NUMBER);
                String businessUnit = results.get(BUSINESS_UNIT);
                if (addressNumber != null) {
                    pAddress.setJdeAddressNumber(addressNumber);
                }
                if (businessUnit != null) {
                    pAddress.setBusinessUnit(businessUnit);
                }
            }
        } catch (InterruptedException | IOException | ExecutionException e) {
            vlogDebug("Error while getting jdeNumber for City: {0},State: {1}", city, state);
        }
    }


	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	public void setProfileRepository(Repository pProfileRepository) {
		this.mProfileRepository = pProfileRepository;
	}

	public ShippingGroupManager getShippingGroupManager() {
		return mShippingGroupManager;
	}

	public void setShippingGroupManager(ShippingGroupManager pShippingGroupManager) {
		this.mShippingGroupManager = pShippingGroupManager;
	}

	public PaymentGroupManager getPaymentGroupManager() {
		return mPaymentGroupManager;
	}

	public void setPaymentGroupManager(PaymentGroupManager pPaymentGroupManager) {
		mPaymentGroupManager = pPaymentGroupManager;
	}

	public CommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	public void setCommerceItemManager(CommerceItemManager pCommerceItemManager) {
		mCommerceItemManager = pCommerceItemManager;
	}

    public GHAccountClient getGHAccountClient() {
        return mGHAccountClient;
    }

    public void setGHAccountClient(GHAccountClient pGHAccountClient) {
        mGHAccountClient = pGHAccountClient;
    }

	public StoreTools getStoreTools() {
		return mStoreTools;
	}

	public void setStoreTools(StoreTools pStoreTools) {
		mStoreTools = pStoreTools;
	}
}
