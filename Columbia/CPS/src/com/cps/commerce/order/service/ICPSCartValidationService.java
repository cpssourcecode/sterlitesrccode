package com.cps.commerce.order.service;

/**
 * @author Dmitry Golubev
 **/
public interface ICPSCartValidationService {
	String PROP_PARENT_CATEGORY    = "parentCategory";
	String PROP_ECOMMERCE_DISPLAY  = "eCommerceDisplay";
	String PROP_DISPLAY_NAME       = "displayName";
}
