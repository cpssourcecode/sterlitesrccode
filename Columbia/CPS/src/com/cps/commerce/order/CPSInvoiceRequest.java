package com.cps.commerce.order;

import com.cps.util.CPSConstants;

import atg.commerce.order.InvoiceRequest;

/**
 * 
 * @author Jason Robert Juvingo
 * 
 */
public class CPSInvoiceRequest extends InvoiceRequest {

	public String getJdeAddressNumber() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_JDE_ADDRESS_NUMBER);
	}

	public void setJdeAddressNumber(String pJdeAddressNumber) {
		setPropertyValue(CPSConstants.CHECKOUT_JDE_ADDRESS_NUMBER, pJdeAddressNumber);
	}
}
