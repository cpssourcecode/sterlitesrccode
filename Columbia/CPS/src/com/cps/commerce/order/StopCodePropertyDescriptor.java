package com.cps.commerce.order;

import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;
import static com.cps.util.CPSConstants.CHECKOUT_MTR;

/**
 * @author Andy Porter
 */
public class StopCodePropertyDescriptor extends RepositoryPropertyDescriptor {

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

		if (pItem == null || !ITEM_DESCRIPTOR_ORDER.equals(pItem.getItemDescriptor().getItemDescriptorName())) {
			return null;
		}

		boolean mtrRequested = (boolean) pItem.getPropertyValue(CHECKOUT_MTR);
		if (mtrRequested) {
			return "MTR";
		} else {
			return "";
		}
	}

	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		//
	}
}