package com.cps.commerce.order;

import atg.nucleus.GenericService;
import com.cps.integrations.order.CPSDummyOrderClient;
import com.cps.integrations.order.CPSOrderClient;

import java.util.*;

/**
 * CPSOrderConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Steve Neverov
 * 
 */
public class CPSOrderConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/commerce/order/CPSOrderConnectionManager";

	private boolean mTest;

	public boolean isTest() {
		return mTest;
	}

	public void setTest(boolean pTest) {
		mTest = pTest;
	}

	private int mDaysBefore;

	/** Client property */
	private CPSOrderClient mClient;

	/**
	 * @return
	 */
	public CPSOrderClient getClient() {
		return mClient;
	}

	/**
	 * @param pClient
	 */
	public void setClient(CPSOrderClient pClient) {
		mClient = pClient;
	}
	
	/** Dummy Client property */
	private CPSDummyOrderClient mDummyClient;

	/**
	 * @return
	 */
	public CPSDummyOrderClient getDummyClient() {
		return mDummyClient;
	}

	/**
	 * @param pDummyClient
	 */
	public void setDummyClient(CPSDummyOrderClient pDummyClient) {
		mDummyClient = pDummyClient;
	}

	/**
	 * This method will request orders with the ws packing slip client based on an
	 * start and end date and CS.
	 *
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> requestOrders(String pCustomerNumber,
											   Calendar pStartDate,
											   Calendar pEndDate,
											   String pOrderNumber,
											   String pPONumber,
											   String pSalesOrderNumber,
											   List<String> pShipTo,
											   int pPageSize,
											   int pStartIndex,
											   String pSortField,
											   String pSortOption) throws Exception {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrders").append(" - start").toString());
		}

		Map<String, Object> result = new HashMap<String, Object>();

		Calendar startDate = pStartDate;
		Calendar endDate = pEndDate;

		if (startDate == null || endDate == null) {
			startDate = Calendar.getInstance();
			startDate.add(Calendar.DAY_OF_MONTH, -1*getDaysBefore());
			endDate = Calendar.getInstance();
		}

		if (getClient().getWebServiceConfig().getWebServiceEnabled()) {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Service is enabled, use client to get data.").toString());
			}
			if (isTest()) {
				List<String> shipto = new ArrayList<String>();
				shipto.add("100552");
				shipto.add("153654");
				shipto.add("176862");
				//result = getClient().requestOrders("101010", 1440791762000l, 1590791762000l, "1162709", "test", shipto, pPageSize, pStartIndex, pSortField, pSortOption);
				result = getClient().requestOrders("101010", startDate, endDate, null, "test", null, shipto, pPageSize, pStartIndex, pSortField, pSortOption);
			} else {
				result = getClient().requestOrders(pCustomerNumber, startDate, endDate, pOrderNumber, pPONumber, pSalesOrderNumber, pShipTo, pPageSize, pStartIndex, pSortField, pSortOption);
			}
		} else {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Service is NOT enabled!").toString());
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrders").append(" - exit").toString());
		}
		return result;

	}

	public int getDaysBefore() {
		return mDaysBefore;
	}

	public void setDaysBefore(int pDaysBefore) {
		mDaysBefore = pDaysBefore;
	}

}

