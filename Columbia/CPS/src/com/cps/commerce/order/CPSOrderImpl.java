package com.cps.commerce.order;

import java.util.Date;
import java.util.List;

import com.cps.util.CPSConstants;

import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvoiceRequest;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;

/**
 * 
 * @author Jason Robert Juvingo
 *
 */
public class CPSOrderImpl extends OrderImpl {
	
	private Date prevLastModifiedDate;
	
	public PaymentGroup getPaymentGroup() {
		List<PaymentGroup> paymentGroupList = getPaymentGroups();
		if (paymentGroupList != null && paymentGroupList.size() == 1) {
			return paymentGroupList.get(0);
		}
		return null;
	}

	public String getInvoiceRequestPoNumber() {
		List<PaymentGroup> paymentGroups = getPaymentGroups();
		for(PaymentGroup paymentGroup: paymentGroups) {
			if(paymentGroup instanceof InvoiceRequest) {
				InvoiceRequest invoiceRequest = (InvoiceRequest) paymentGroup;
				return invoiceRequest.getPONumber();
			}
		}
		return null;
	}
	
	public String getPaymentType() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_PAYMENT_TYPE);
	}
	
	public Date getPrevLastModifiedDate() {
		return prevLastModifiedDate;
	}
	
	/**
	 * getShippingGroup
	 * Method will return the one and only shipping group on the order.
	 * @return CPSHardgoodShippingGroup
	 */
	public ShippingGroup getShippingGroup() {
		List<ShippingGroup> shippingGroupList = getShippingGroups();
		if (shippingGroupList != null && shippingGroupList.size() == 1) {
			return shippingGroupList.get(0);
		}
		return null;
	}

	public boolean isMtrRequested() {
		Boolean mtrRequested = (Boolean) getPropertyValue(CPSConstants.CHECKOUT_MTR);
		return mtrRequested == null ? false : mtrRequested;
	}

	public void setMtrRequested(Boolean pMtrRequested) {
		setPropertyValue(CPSConstants.CHECKOUT_MTR, pMtrRequested == null ? false : pMtrRequested);
	}

	public String getOnsiteContact() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_ONSITE_CONTACT);

	}

	public void setOnsiteContact(String onsiteContact) {
		setPropertyValue(CPSConstants.CHECKOUT_ONSITE_CONTACT, onsiteContact);

	}

	public void setPaymentType(String pPaymentType) {
		setPropertyValue(CPSConstants.CHECKOUT_PAYMENT_TYPE,pPaymentType);
	}
	
	public void setPrevLastModifiedDate(Date prevLastModifiedDate) {
		this.prevLastModifiedDate = prevLastModifiedDate;
	}
	
	public String getJdeOrderNumber() {
		return (String) getPropertyValue("jdeOrderNumber");
	}

	public void setJdeOrderNumber(String pJdeOrderNumber) {
		setPropertyValue("jdeOrderNumber", pJdeOrderNumber);
	}
	
	public List getOrderCouponCodes() {
		return (List) getPropertyValue(CPSConstants.COUPON_CODES);
	}

	public void setOrderCouponCodes(List couponCodes) {
		setPropertyValue(CPSConstants.COUPON_CODES, couponCodes);
	}
	
	public boolean isAllowCheckout() {
		return (Boolean) getPropertyValue(CPSConstants.ALLOW_CHECKOUT);
	}

	public void setAllowCheckout(boolean allowCheckout) {
		setPropertyValue(CPSConstants.ALLOW_CHECKOUT, allowCheckout);
	}

	public Double getOrderTotal() {
		return (Double) getPropertyValue(CPSConstants.ORDER_TOTAL);
	}

	public void setOrderTotal(Double pOrderTotal) {
		setPropertyValue(CPSConstants.ORDER_TOTAL, pOrderTotal);
	}

	public String getWebOrderId() {
		return (String) getPropertyValue(CPSConstants.WEB_ORDER_ID);
	}

	public void setWebOrderId(String pWebOrderId) {
		setPropertyValue(CPSConstants.WEB_ORDER_ID, pWebOrderId);
	}

	public String getAutoOrderId() {
		return (String) getPropertyValue(CPSConstants.AUTO_ORDER_ID);
	}

	public void setAutoOrderId(String pAutoOrderId) {
		setPropertyValue(CPSConstants.AUTO_ORDER_ID, pAutoOrderId);
	}

	public boolean isUseProfileOrganization() {
		Boolean useProfileOrganization = (Boolean) getPropertyValue(CPSConstants.USE_PROFILE_ORGANIZATION);
		if (useProfileOrganization == null) {
			useProfileOrganization = false;
		}
		return useProfileOrganization;
	}

	public void setUseProfileOrganization(boolean pUseProfileOrganization) {
		setPropertyValue(CPSConstants.USE_PROFILE_ORGANIZATION, pUseProfileOrganization);
	}

	public String getRequestNumber() {
		return (String) getPropertyValue(CPSConstants.REQUEST_NUMBER);
	}

	public void setRequestNumber(String pRequestNumber) {
		setPropertyValue(CPSConstants.REQUEST_NUMBER, pRequestNumber);
	}

	public boolean isPaymentUpdated() {
		Boolean paymentUpdated = (Boolean) getPropertyValue(CPSConstants.PAYMENT_UPDATED);
		if (paymentUpdated == null) {
			paymentUpdated = false;
		}
		return paymentUpdated;
	}

	public void setPaymentUpdated(boolean pPaymentUpdated) {
		setPropertyValue(CPSConstants.PAYMENT_UPDATED, pPaymentUpdated);
	}

	public String getPONumber() {
		return (String) getPropertyValue(CPSConstants.PO_NUMBER_PROPERTY);
	}

	public void setPONumber(String pPoNumber) {
		setPropertyValue(CPSConstants.PO_NUMBER_PROPERTY, pPoNumber);
	}
	
	public String getJobName() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_JOB_NAME);
	}

	public void setJobName(String pJobName) {
		setPropertyValue(CPSConstants.CHECKOUT_JOB_NAME, pJobName);
	}
	
	public String getConcatenatedPoJobName() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_CONCATENATED_PO_JOB_NAME);
	}

	public void setConcatenatedPoJobName(String pConcatenatedPoJobName) {
		setPropertyValue(CPSConstants.CHECKOUT_CONCATENATED_PO_JOB_NAME, pConcatenatedPoJobName);
	}

    /**
     * This method will check whether the order contains an InstorePickupSG and if yes, will return true.
     * 
     */
    public boolean isInStorePickupOrder() {
        List<ShippingGroup> shippingGroupList = getShippingGroups();
        boolean inStorePickupOrder = false;
        if (shippingGroupList != null) {
            for (ShippingGroup shippingGroup : shippingGroupList) {
                if (shippingGroup instanceof InStorePickupShippingGroup) {
                    inStorePickupOrder = true;
                }
            }
        }
        return inStorePickupOrder;
    }
    
	public boolean isJdeSuccess() {
		return (boolean) getPropertyValue(CPSConstants.JDE_SUCCESS);
	}
	
	public void setJdeSuccess(boolean pJdeSuccess) {
		setPropertyValue(CPSConstants.JDE_SUCCESS, pJdeSuccess);
	}

}
