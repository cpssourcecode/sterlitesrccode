package com.cps.commerce.order;

import atg.commerce.order.OrderTools;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.json.JSONException;
import atg.servlet.DynamoHttpServletResponse;
import vsg.util.VSGFormUtils;

import java.io.IOException;
import java.util.*;

public class CPSOrderTools extends OrderTools {

	private Properties mDuplicateOrderExcludedProperties;

	public void setDuplicateOrderExcludedProperties(Properties pDuplicateOrderExcludedProperties) {
		mDuplicateOrderExcludedProperties = pDuplicateOrderExcludedProperties;
	}

	public Properties getDuplicateOrderExcludedProperties() {
		return mDuplicateOrderExcludedProperties;
	}

	protected Map mDuplicateOrderExcludedOrderPropertiesMap;

	public Map getDuplicateOrderExcludedOrderPropertiesMap() {
		if (mDuplicateOrderExcludedOrderPropertiesMap == null) {
			mDuplicateOrderExcludedOrderPropertiesMap = new HashMap();
			if (getDuplicateOrderExcludedProperties() != null) {
				Set items = getDuplicateOrderExcludedProperties().keySet();
				for (Object itemName : items) {
					String propertyString = getDuplicateOrderExcludedProperties().getProperty(itemName.toString());
					List properties = parseString(propertyString);
					mDuplicateOrderExcludedOrderPropertiesMap.put(itemName, properties);
				}
			}
		}
		return mDuplicateOrderExcludedOrderPropertiesMap;
	}

	public static List parseString(String pString) {
		List c = new ArrayList();
		StringTokenizer st = new StringTokenizer(pString, "|");
		while (st.hasMoreTokens()) {
			c.add(st.nextToken());
		}
		return c;
	}


	public boolean processAjaxResponse(DynamoHttpServletResponse pResponse, GenericFormHandler pFormHandler) throws IOException {
		try {
			if (!pFormHandler.getFormError()) {
				if (isLoggingDebug()) {
					logDebug("no errors, return success");
				}
				VSGFormUtils.addAjaxSuccessResponse(pResponse);
			} else {
				if (isLoggingDebug()) {
					logDebug("errors, return errors for display");
				}
				Set<String> errorProperties = new HashSet<String>();
				for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
					DropletException exc = (DropletException) pFormHandler.getFormExceptions().get(i);
					errorProperties.add(exc.getErrorCode());
				}
				VSGFormUtils.addAjaxErrorResponse(pFormHandler, pResponse, errorProperties);
			}
		} catch (JSONException e) {
			logError(e);
		}
		return false;
	}

}
