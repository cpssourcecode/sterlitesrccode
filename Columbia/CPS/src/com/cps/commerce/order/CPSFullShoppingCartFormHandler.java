package com.cps.commerce.order;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import atg.commerce.CommerceException;
import atg.commerce.order.*;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.json.JSONException;
import atg.payment.creditcard.CreditCardInfo;
import atg.payment.creditcard.CreditCardTools;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import javax.transaction.Transaction;

import com.cps.commerce.inventory.CPSInventoryConnectionManager;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;
import vsg.util.VSGFormUtils;

public class CPSFullShoppingCartFormHandler extends FullShoppingCartFormHandler {

	private Map<String, Object> mValues = new HashMap<String, Object>();

	public Map<String, Object> getValues() {
		return mValues;
	}

	public String getStringValue(String pKey) {
		return (String) mValues.get(pKey);
	}

	public boolean getBooleanValue(String pKey) {
		Boolean result = Boolean.valueOf(getStringValue(pKey));
		if (result == null) {
			result = false;
		}
		return result;
	}

	private CPSInventoryConnectionManager mInventoryClient;
	
	public CPSInventoryConnectionManager getInventoryClient() {
		return mInventoryClient;
	}

	public void setInventoryClient(CPSInventoryConnectionManager pInventoryClient) {
		this.mInventoryClient = pInventoryClient;
	}

	private PaymentGroupManager mPaymentGroupManager;
	
	private ShippingGroupManager mShippingGroupManager;

	private CommerceItemManager mCommerceItemManager;

	private RepeatingRequestMonitor mRepeatingRequestMonitor;


	/**
	 * This takes a single error key and property and displays the error and
	 * adds the property to be highlighted. Use this if only adding one error.
	 * 
	 * @param msg
	 * @param pty
	 * @param pRequest
	 */
	private void createError(String msg, String pty, DynamoHttpServletRequest pRequest) {
		CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
	}

	public PaymentGroupManager getPaymentGroupManager() {
		return mPaymentGroupManager;
	}

	public ShippingGroupManager getShippingGroupManager() {
		return mShippingGroupManager;
	}

	public boolean handleMoveToConfirmation(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) { logDebug(" Start of handle move to confirmation"); }

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSFullShoppingCartFormHandler.handleMoveToConfirmation";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			PerformanceMonitor.startOperation(handleMethodName);
			Transaction tr = null;
			try {
				tr = ensureTransaction();

				synchronized (getOrder()) {

					try {
						preMoveToConfirmation(pRequest, pResponse);

						if (!getFormError()) {
							moveToConfirmation(pRequest, pResponse);
						}

						if (!getFormError()) {
							getOrderManager().updateOrder(getOrder());
						}

					} catch (Exception e) {
						processException(e, MSG_ERROR_UPDATE_ORDER, pRequest, pResponse);
					}
				} // synchronized

				if (isLoggingDebug()) {
					logDebug(" End of handle move to confirmation");
				}

			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
				PerformanceMonitor.endOperation(handleMethodName);
			}
		}

		return checkFormRedirect(getMoveToConfirmationSuccessURL(), getMoveToConfirmationErrorURL(), pRequest, pResponse);
	}

	private boolean checkCCInfo(CreditCardInfo pCreditCardInfo, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		boolean resultCheck = false;
		if (pCreditCardInfo != null) {
			switch(CreditCardTools.verifyCreditCard(pCreditCardInfo) ) {
			case CreditCardTools.SUCCESS:
				if (isLoggingDebug()) { logDebug("Success"); }
				resultCheck = true;
				break;
			case CreditCardTools.CARD_EXPIRED:
				createError(CPSErrorCodes.CC_EXPIRED, CPSConstants.CHECKOUT_CREDIT_CARD_EXP_MONTH, pRequest);
				if (isLoggingDebug()) { logDebug("Card Expired"); }
				resultCheck = false;
				break;
			case CreditCardTools.CARD_NUMBER_HAS_INVALID_CHARS:
				createError(CPSErrorCodes.CC_INVALID_CHARS, CPSConstants.CHECKOUT_CREDIT_CARD_NUMBER, pRequest);
				if (isLoggingDebug()) { logDebug("Card Number has invalid characters"); }
				resultCheck = false;
				break;
			case CreditCardTools.CARD_NUMBER_DOESNT_MATCH_TYPE:
				createError(CPSErrorCodes.CC_INVALID_TYPE_NUM_MISMATCH, CPSConstants.CHECKOUT_CREDIT_CARD_NUMBER, pRequest);
				if (isLoggingDebug()) { logDebug("Card Number does not match type"); }
				resultCheck = false;
				break;
			case CreditCardTools.CARD_LENGTH_NOT_VALID:
				createError(CPSErrorCodes.CC_NUMBER_SHORT, CPSConstants.CHECKOUT_CREDIT_CARD_NUMBER, pRequest);
				if (isLoggingDebug()) { logDebug("Card Number not long enough"); }
				resultCheck = false;
				break;
			case CreditCardTools.CARD_NUMBER_NOT_VALID:
				createError(CPSErrorCodes.CC_NUMBER_WRONG, CPSConstants.CHECKOUT_CREDIT_CARD_NUMBER, pRequest);
				if (isLoggingDebug()) { logDebug("Card Number not valid"); }
				resultCheck = false;
				break;
			case CreditCardTools.CARD_INFO_NOT_VALID:
				createError(CPSErrorCodes.CC_INFO_WRONG, CPSConstants.CHECKOUT_CREDIT_CARD_NUMBER, pRequest);
				if (isLoggingDebug()) { logDebug("Card Info not valid"); }
				resultCheck = false;
				break;
			case CreditCardTools.CARD_EXP_DATE_NOT_VALID:
				createError(CPSErrorCodes.CC_EXP_DATE_WRONG, CPSConstants.CHECKOUT_CREDIT_CARD_EXP_MONTH, pRequest);
				if (isLoggingDebug()) { logDebug("Card expiration not valid"); }
				resultCheck = false;
				break;
			case CreditCardTools.CARD_TYPE_NOT_VALID:
				createError(CPSErrorCodes.CC_INVALID_TYPE, CPSConstants.CHECKOUT_CREDIT_CARD_TYPE, pRequest);
				if (isLoggingDebug()) { logDebug("Card Type not Valid"); }
				resultCheck = false;
				break;
			default:
				if (isLoggingDebug()) { logDebug("We have really failed now..."); }
				resultCheck = false;
				break;
			}
		}
		return resultCheck;
	}
	
	public void moveToConfirmation(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {

		setShippingInfo(pRequest, pResponse);

		setOrderInstructions(pRequest, pResponse);

		setBillingInfo(pRequest, pResponse);

		//setRelationships(pRequest, pResponse);

	}

	private void validatePayment(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		// Payment Section Validations
		if (CPSConstants.CHECKOUT_PAYMENT_CC.equals(getStringValue("paymentType"))) {
			if (StringUtils.isEmpty(getStringValue("creditCardType"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CREDIT_CARD_TYPE_EMPTY, CPSConstants.CHECKOUT_CREDIT_CARD_TYPE, pRequest);
			}

			if (StringUtils.isEmpty(getStringValue("firstName"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CREDIT_CARD_FIRST_NAME_EMPTY, CPSConstants.CHECKOUT_CREDIT_CARD_FIRST_NAME, pRequest);
			}

			if (StringUtils.isEmpty(getStringValue("lastName"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CREDIT_CARD_LAST_NAME_EMPTY, CPSConstants.CHECKOUT_CREDIT_CARD_LAST_NAME, pRequest);
			}

			if (StringUtils.isEmpty(getStringValue("creditCardNumber"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CREDIT_CARD_NUMBER_EMPTY, CPSConstants.CHECKOUT_CREDIT_CARD_NUMBER, pRequest);
			}

			if (StringUtils.isEmpty(getStringValue("expirationMonth"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CREDIT_CARD_EXP_MONTH_EMPTY, CPSConstants.CHECKOUT_CREDIT_CARD_EXP_MONTH, pRequest);
			}

			if (StringUtils.isEmpty(getStringValue("expirationYear"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CREDIT_CARD_EXP_YEAR_EMPTY,CPSConstants.CHECKOUT_CREDIT_CARD_EXP_YEAR, pRequest);
			}

			if (StringUtils.isEmpty(getStringValue("creditCardPostalCode"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CREDIT_CARD_ZIPCODE_EMPTY, CPSConstants.CHECKOUT_CREDIT_CARD_ZIPCODE, pRequest);
			}

			if (!getFormError()) {
				checkCCInfo(createCreditCard(), pRequest, pResponse);
			}

		} else {
			// NOTE no special validation is required for PO
			if(!CPSConstants.CHECKOUT_PAYMENT_PO.equals(getStringValue("paymentType"))) {
				// if paymentType is not PO raise an unknown error
				createError(CPSErrorCodes.ERR_CHECKOUT_UNKNOWN_PAYMENT_TYPE, CPSConstants.CHECKOUT_PAYMENT_TYPE, pRequest);
			}
		}

	}

	private void validateShipping(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		// Delivery Option Validations
		if (CPSConstants.CHECKOUT_DELIVERY_METHOD_STANDARD_GROUND.equals(getStringValue("deliveryMethod")) ) {
			if (isLoggingDebug()) { logDebug("Chosen Delivery Option: " + CPSConstants.CHECKOUT_DELIVERY_METHOD_STANDARD_GROUND); }
			// No Validations at this time

		} else if (CPSConstants.CHECKOUT_DELIVERY_METHOD_PICKUP.equals(getStringValue("deliveryMethod"))) {
			if (isLoggingDebug()) { logDebug("Chosen Delivery Option: " + CPSConstants.CHECKOUT_DELIVERY_METHOD_PICKUP); }

			if (StringUtils.isEmpty(getStringValue("branchId"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_PICKUP_BRANCH_EMPTY, CPSConstants.CHECKOUT_PICKUP_BRANCH, pRequest);
			} else {
				if (StringUtils.isEmpty(getStringValue("pickupDate"))) {
					createError(CPSErrorCodes.ERR_CHECKOUT_PICKUP_DATE_EMPTY, CPSConstants.CHECKOUT_PICKUP_DATE, pRequest);
				}

				if ( StringUtils.isEmpty(getStringValue("pickupTime")) ) {
					createError(CPSErrorCodes.ERR_CHECKOUT_PICKUP_TIME_EMPTY, CPSConstants.CHECKOUT_DISPLAY_PICKUP_TIME, pRequest);
				}
			}

			if (!StringUtils.isEmpty(getStringValue("branchId"))
					&& !StringUtils.isEmpty(getStringValue("pickupDate"))
					&& !StringUtils.isEmpty(getStringValue("pickupTime")) ) {
				// Make will call check if selected and is today
				Date today = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				if (CPSConstants.CHECKOUT_DELIVERY_METHOD_PICKUP.equals(getStringValue("deliveryMethod"))
						&& sdf.format(today).equals(getStringValue("pickupDate"))) {
					if (isLoggingDebug()) { logDebug("Make Will call check"); }

					List<CommerceItem> myItems = getOrder().getCommerceItems();

					Map<String, Integer> availableQtys = null;
					try {
						availableQtys = getInventoryClient().requestInventory(getOrder(), getStringValue("branchId"));
					} catch (Exception e) {
						if (isLoggingError()) {
							logError("Error making will call", e);
						}
						createError(CPSErrorCodes.ERR_CHECKOUT_WILLCALL_ISSUE,null,pRequest);
					}

					if(availableQtys != null) {
						Set<String> myKeys = availableQtys.keySet();
						// for output to check data
						for (String sku : myKeys) {
							if (isLoggingDebug()) {
								logDebug(new StringBuffer().append("Sku: <").append(sku).append("> amount: <").append(availableQtys.get(sku)).append(">").toString());
							}

							int totalQtyNeeded = 0;
							for (CommerceItem commerceItem : myItems) {

								if (commerceItem.getCatalogRefId().compareTo(sku) == 0) {
									totalQtyNeeded += commerceItem.getQuantity();
								}
							}

							if (availableQtys.get(sku).intValue() == 0) {
								createError(CPSErrorCodes.ERR_CHECKOUT_NONE_AVAILABLE, sku, pRequest);
							} else if (totalQtyNeeded > availableQtys.get(sku).intValue()) {
								if (isLoggingDebug()) {
									logDebug(new StringBuffer().append("Not enough in stock flag it.").toString());
								}
								addFormException(new DropletFormException("Product with SKU " + sku + " is partially available at your selected store.", getAbsoluteName()));
							}
						}
					}
				}
			}

		} else if (CPSConstants.CHECKOUT_DELIVERY_METHOD_COLLECT.equals(getStringValue("deliveryMethod")) ) {
			if (isLoggingDebug()) { logDebug("Chosen Delivery Option: " + CPSConstants.CHECKOUT_DELIVERY_METHOD_COLLECT); }

			if (StringUtils.isEmpty(getStringValue("carrier"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_CARRIER_EMPTY, CPSConstants.CHECKOUT_COLLECT_CARRIER, pRequest);
			}

			if (StringUtils.isEmpty(getStringValue("carrierAcctNumber"))) {
				createError(CPSErrorCodes.ERR_CHECKOUT_ACCOUNT_NUMBER_EMPTY, CPSConstants.CHECKOUT_COLLECT_ACCOUNT_NUMBER, pRequest);
			}

		} else {
			createError(CPSErrorCodes.ERR_CHECKOUT_UNKNOWN_DELIVERY_TYPE, CPSConstants.CHECKOUT_DELIVERY_METHOD, pRequest);
		}

	}

	@Override
	public void preMoveToConfirmation(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug("Start of pre handle move to confirmation");
		}
		
		//getSessionBean().clearFormMessages();

		validateShipping(pRequest, pResponse);
		
		validatePayment(pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug(" End of pre handle move to confirmation");
		}

	}
	
	private void setBillingInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws CommerceException {
		if (isLoggingDebug()) { logDebug(new StringBuffer().append("Start Setting Billing Info").toString()); }
		PaymentGroupManager paymentGroupManager = getPaymentGroupManager();
		
		// check what kind of payment group we have on the order and create the
		// correct one if needed.
		PaymentGroup paymentGroupOnOrder = null;
		paymentGroupManager.removeAllPaymentGroupsFromOrder(getOrder());
		
		if (CPSConstants.CHECKOUT_PAYMENT_CC.equals(getStringValue("paymentType"))) {
			// create the billing group
			paymentGroupOnOrder = createCreditCard();
		} else if (CPSConstants.CHECKOUT_PAYMENT_PO.equals(getStringValue("paymentType"))) {
			paymentGroupOnOrder = createInvoicerRequest();
		}
		if(paymentGroupOnOrder != null) {
			paymentGroupManager.addPaymentGroupToOrder(getOrder(), paymentGroupOnOrder);
			getOrderManager().addRemainingOrderAmountToPaymentGroup(getOrder(), paymentGroupOnOrder.getId());
		} else {
			createError(CPSErrorCodes.ERR_CHECKOUT_UNKNOWN_PAYMENT_TYPE, CPSConstants.CHECKOUT_PAYMENT_TYPE, pRequest);
		}
		if (isLoggingDebug()) { logDebug(new StringBuffer().append("End Setting Billing Info").toString()); }
	}
	
	private void setOrderInstructions(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (isLoggingDebug()) { logDebug(new StringBuffer().append("Setting Order Instructions").toString()); }
		// Set MTR
		if (isLoggingDebug()) { logDebug("MTR: " + getBooleanValue("mtrRequested"));}
		((CPSOrderImpl) getOrder()).setMtrRequested(getBooleanValue("mtrRequested"));
		
		// Set Special Instructions if not null
		if (isLoggingDebug()) { logDebug(new StringBuffer().append("Special Instructions: ").append(getStringValue("specialInstruction")).toString()); }

		Map<String, String> orderInstructions = getOrder().getSpecialInstructions();
		if (orderInstructions == null || orderInstructions.size() == 0) {
			Map<String, String> instructionMap = new HashMap<String, String>(1);
			if (isLoggingDebug()) { logDebug(new StringBuffer().append("Saving new saving instructions ").append(getStringValue("specialInstruction")).toString()); }
			instructionMap.put("specialInstructions", getStringValue("specialInstruction"));
			((CPSOrderImpl) getOrder()).setSpecialInstructions(instructionMap);
		} else {
			orderInstructions.clear();
			if (isLoggingDebug()) { logDebug(new StringBuffer().append("Special Instructions already exists ").append(getStringValue("specialInstruction")).toString()); }
			orderInstructions.put("specialInstructions",  getStringValue("specialInstruction"));
			((CPSOrderImpl) getOrder()).setSpecialInstructions(orderInstructions);
		}
	}

	public void setPaymentGroupManager(PaymentGroupManager pPaymentGroupManager) {
		mPaymentGroupManager = pPaymentGroupManager;
	}

	private void setRelationships(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

	}
	
	public void setShippingGroupManager(ShippingGroupManager pShippingGroupManager) {
		mShippingGroupManager = pShippingGroupManager;
	}
	
	private void setShippingInfo(DynamoHttpServletRequest pRequest , DynamoHttpServletResponse pResponse) throws CommerceException {

		if( isLoggingDebug() ) {
			logDebug(new StringBuffer().append("Start Setting Shipping Info").toString());
		}

		ShippingGroupManager shippingGroupManager = getShippingGroupManager();
		shippingGroupManager.removeAllShippingGroupsFromOrder(getOrder());

		ContactInfo shippingAddress = createShippingAddress();

		CPSHardgoodShippingGroup shippingGroup = (CPSHardgoodShippingGroup) shippingGroupManager.createShippingGroup(CPSConstants.SHIPPPING_TYPE_HARDGOOD);

		if (CPSConstants.CHECKOUT_DELIVERY_METHOD_STANDARD_GROUND.equals(getStringValue("deliveryMethod"))) {

			//set shippingMethod
			shippingGroup.setShippingMethod(CPSConstants.CHECKOUT_METHOD_STANDARD_GROUND);
			
			//set properties
			shippingGroup.setShippingAddress(shippingAddress);
			shippingGroup.setJdeAddressNumber(getStringValue("jdeAddressNumberCS"));

		} else if (CPSConstants.CHECKOUT_DELIVERY_METHOD_PICKUP.equals(getStringValue("deliveryMethod"))) {

			//set shippingMethod
			shippingGroup.setShippingMethod(CPSConstants.CHECKOUT_METHOD_PICKUP);
			
			//set properties
			shippingGroup.setBranch(getStringValue("branchId"));
			shippingGroup.setPickupDate(getStringValue("pickupDate"));
			shippingGroup.setPickupTime(getStringValue("pickupTime"));
			shippingGroup.setShippingAddress(shippingAddress);
			shippingGroup.setJdeAddressNumber(getStringValue("jdeAddressNumberCS"));

		} else if (CPSConstants.CHECKOUT_DELIVERY_METHOD_COLLECT.equals(getStringValue("deliveryMethod"))) {

			//set shippingMethod
			shippingGroup.setShippingMethod(CPSConstants.CHECKOUT_METHOD_COLLECT);
			
			//set properties
			shippingGroup.setCarrier(getStringValue("carrier"));
			shippingGroup.setCarrierAccountNumber(getStringValue("carrierAcctNumber"));
			shippingGroup.setShippingAddress(shippingAddress);
			shippingGroup.setJdeAddressNumber(getStringValue("jdeAddressNumberCS"));
			
		} else { // unknown type.
			createError(CPSErrorCodes.ERR_CHECKOUT_UNKNOWN_DELIVERY_TYPE, CPSConstants.CHECKOUT_DELIVERY_METHOD, pRequest);
		}
		
		if( isLoggingDebug() ) { logDebug(new StringBuffer().append("Adding Shipping Group to Order").toString()); }
		shippingGroupManager.addShippingGroupToOrder(getOrder(), shippingGroup);
		
		
		if( isLoggingDebug() ) { logDebug(new StringBuffer().append("Add relationships").toString()); }
		List<CommerceItem> commerceItems = getOrder().getCommerceItems();
		CommerceItem currentCommerceItem = null;
		for (int i = 0, commerceItemListSize = commerceItems.size() ; i < commerceItemListSize ; i++ ) {
			currentCommerceItem = commerceItems.get(i);
			getCommerceItemManager().addItemQuantityToShippingGroup(getOrder(), currentCommerceItem.getId(), shippingGroup.getId(), currentCommerceItem.getQuantity());
		}
		if( isLoggingDebug() ) { logDebug(new StringBuffer().append("End Setting Shipping Info").toString()); }
	}

	private CPSInvoiceRequest createInvoicerRequest() throws CommerceException {

		PaymentGroup paymentGroupOnOrder = getPaymentGroupManager().createPaymentGroup(CPSConstants.PAYMENT_TYPE_INVOICE);
		
		// Set payment info
		String poNumber = getStringValue("poReferenceNumber");
		if (StringUtils.isBlank(poNumber)) {
			 poNumber = CPSConstants.CHECKOUT_ON_ACCOUNT;
		}
		((CPSInvoiceRequest) paymentGroupOnOrder).setPONumber(poNumber);
		
		// Set billing info
		((CPSInvoiceRequest) paymentGroupOnOrder).setBillingAddress(createBillingAddress());
		((CPSInvoiceRequest) paymentGroupOnOrder).setJdeAddressNumber(getStringValue("jdeAddressNumberCB"));
		return (CPSInvoiceRequest) paymentGroupOnOrder;
	}

	private CPSCreditCard createCreditCardInfo() throws CommerceException {
		PaymentGroup paymentGroupOnOrder = null;
		paymentGroupOnOrder = getPaymentGroupManager().createPaymentGroup(CPSConstants.PAYMENT_TYPE_CC);

		// Set payment info
		((CPSCreditCard) paymentGroupOnOrder).setCreditCardNumber(getStringValue("creditCardNumber"));
		((CPSCreditCard) paymentGroupOnOrder).setCreditCardType(getStringValue("creditCardType"));
		((CPSCreditCard) paymentGroupOnOrder).setExpirationMonth(getStringValue("expirationMonth"));
		((CPSCreditCard) paymentGroupOnOrder).setExpirationYear(getStringValue("expirationYear"));
		((CPSCreditCard) paymentGroupOnOrder).setCardHolderFirstName(getStringValue("firstName"));
		((CPSCreditCard) paymentGroupOnOrder).setCardHolderLastName(getStringValue("lastName"));

		// Set billing info
		((CPSCreditCard) paymentGroupOnOrder).setBillingAddress(createBillingAddress());
		((CPSCreditCard) paymentGroupOnOrder).setJdeAddressNumber(getStringValue("jdeAddressNumberCB"));
		return (CPSCreditCard) paymentGroupOnOrder;
	}

	private CPSCreditCard createCreditCard() {

		PaymentGroup paymentGroupOnOrder = null;
		try {
			paymentGroupOnOrder = getPaymentGroupManager().createPaymentGroup(CPSConstants.PAYMENT_TYPE_CC);

			// Set payment info
			((CPSCreditCard) paymentGroupOnOrder).setCreditCardNumber(getStringValue("creditCardNumber"));
			((CPSCreditCard) paymentGroupOnOrder).setCreditCardType(getStringValue("creditCardType"));
			((CPSCreditCard) paymentGroupOnOrder).setExpirationMonth(getStringValue("expirationMonth"));
			((CPSCreditCard) paymentGroupOnOrder).setExpirationYear(getStringValue("expirationYear"));
			((CPSCreditCard) paymentGroupOnOrder).setCardHolderFirstName(getStringValue("firstName"));
			((CPSCreditCard) paymentGroupOnOrder).setCardHolderLastName(getStringValue("lastName"));

			// Set billing info
			((CPSCreditCard) paymentGroupOnOrder).setBillingAddress(createBillingAddress());
			((CPSCreditCard) paymentGroupOnOrder).setJdeAddressNumber(getStringValue("jdeAddressNumberCB"));
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError(ce);
			}
		}
		return (CPSCreditCard) paymentGroupOnOrder;

	}
	
	private ContactInfo createBillingAddress() {
		ContactInfo billingAddress = new ContactInfo();
		billingAddress.setCompanyName(getStringValue("billingCompanyName"));
		billingAddress.setAddress1(getStringValue("billingAddress1"));
		billingAddress.setAddress2(getStringValue("billingAddress2"));
		billingAddress.setAddress3(getStringValue("billingAddress3"));
		billingAddress.setCity(getStringValue("billingCity"));
		billingAddress.setState(getStringValue("billingState"));
		billingAddress.setPostalCode(getStringValue("billingZip"));
		billingAddress.setFirstName((String)getProfile().getPropertyValue(CPSConstants.FIRST_NAME));
		billingAddress.setLastName((String)getProfile().getPropertyValue(CPSConstants.LAST_NAME));
		billingAddress.setCountry("US");
		return billingAddress;
	}

	private ContactInfo createShippingAddress() {
		ContactInfo shippingAddress = new ContactInfo();
		shippingAddress.setAddress1(getStringValue("shippingAddress1"));
		shippingAddress.setAddress2(getStringValue("shippingAddress2"));
		shippingAddress.setAddress3(getStringValue("shippingAddress3"));
		shippingAddress.setCity(getStringValue("shippingCity"));
		shippingAddress.setState(getStringValue("shippingState"));
		shippingAddress.setPostalCode(getStringValue("shippingZip"));
		shippingAddress.setFirstName((String)getProfile().getPropertyValue(CPSConstants.FIRST_NAME));
		shippingAddress.setLastName((String)getProfile().getPropertyValue(CPSConstants.LAST_NAME));
		shippingAddress.setCountry("US");
		return shippingAddress;
	}

	public boolean checkFormRedirect (String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (VSGFormUtils.isAjaxRequest(pRequest)) {
			try {
				if (!getFormError()) {
					VSGFormUtils.addAjaxSuccessResponse(pResponse);
				} else {
					Set<String> errorProperties = new HashSet<String>();
					for(int i = 0; i < getFormExceptions().size(); i++){
						DropletException exc = (DropletException) getFormExceptions().get(i);
						errorProperties.add(exc.getErrorCode());
					}
					VSGFormUtils.addAjaxErrorResponse(this, pResponse, errorProperties);
				}
			} catch (JSONException e) {
				logError(e);
			}
			return false;
		} else {
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}

	}

	public CommerceItemManager getCommerceItemManager() {
		return mCommerceItemManager;
	}

	public void setCommerceItemManager(CommerceItemManager pCommerceItemManager) {
		this.mCommerceItemManager = pCommerceItemManager;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}
}
