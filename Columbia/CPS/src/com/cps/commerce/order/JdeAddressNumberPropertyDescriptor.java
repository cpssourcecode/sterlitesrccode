package com.cps.commerce.order;

import atg.repository.RepositoryItemImpl;
import atg.repository.RepositoryPropertyDescriptor;

/**
 * this property descriptor will be placed on level of paymentgroup,
 * which doesn't know about jdeAddressNumber property, defined in its subtypes
 *
 * @author Dmitry Golubev
 */
public class JdeAddressNumberPropertyDescriptor extends RepositoryPropertyDescriptor {

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		String JDE_ADDRESS_NUMBER = "jdeAddressNumber";
		if(pItem!=null && pItem.getItemDescriptor().hasProperty(JDE_ADDRESS_NUMBER)) {
			return pItem.getPropertyValue(JDE_ADDRESS_NUMBER);
		}
		return null;
	}

	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		//
	}
}
