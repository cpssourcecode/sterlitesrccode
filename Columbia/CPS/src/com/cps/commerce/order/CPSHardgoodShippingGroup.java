package com.cps.commerce.order;

import com.cps.util.CPSConstants;
import atg.commerce.order.HardgoodShippingGroup;

/**
 * 
 * @author Jason Robert Juvingo
 *
 */
public class CPSHardgoodShippingGroup extends HardgoodShippingGroup {
	
	public String getJdeAddressNumber() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_JDE_ADDRESS_NUMBER);
	}
	
	public void setJdeAddressNumber(String pJdeAddressNumber) {
		setPropertyValue(CPSConstants.CHECKOUT_JDE_ADDRESS_NUMBER,pJdeAddressNumber);
	}
	
	public String getCarrier() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_COLLECT_CARRIER);
	}
	
	public void setCarrier(String pCarrier) {
		setPropertyValue(CPSConstants.CHECKOUT_COLLECT_CARRIER,pCarrier);
	}
	
	public String getCarrierAccountNumber() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_COLLECT_ACCOUNT_NUMBER);
	}
	
	public void setCarrierAccountNumber(String pCarrierAcctNumber) {
		setPropertyValue(CPSConstants.CHECKOUT_COLLECT_ACCOUNT_NUMBER,pCarrierAcctNumber);
	}
	
	public String getPickupDate() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_PICKUP_DATE);
	}
	
	public void setPickupDate(String pPickupDate) {
		setPropertyValue(CPSConstants.CHECKOUT_PICKUP_DATE,pPickupDate);
	}
	
	public String getPickupTime() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_PICKUP_TIME);
	}
	
	public void setPickupTime(String pPickupTime) {
		setPropertyValue(CPSConstants.CHECKOUT_PICKUP_TIME,pPickupTime);
	}
	
	public String getBranch() {
		return (String) getPropertyValue(CPSConstants.CHECKOUT_PICKUP_BRANCH);
	}
	
	public void setBranch(String pBranch) {
		setPropertyValue(CPSConstants.CHECKOUT_PICKUP_BRANCH,pBranch);
	}
}  