package com.cps.commerce.order;

import atg.core.util.ContactInfo;

import java.util.Date;

public class CPSSavedCartBean {

	private String mId;

	public String getId(){
		return mId;
	}

	public void setId(String pId){
		mId = pId;
	}

	private String mProfileId;

	public String getProfileId(){
		return mProfileId;
	}

	public void setProfileId(String pProfileId){
		mProfileId = pProfileId;
	}

	private ContactInfo mAddress;

	public ContactInfo getAddress(){
		return mAddress;
	}

	public void setAddress(ContactInfo pAddress){
		mAddress = pAddress;
	}

	private Date mSavedDate;

	public Date getSavedDate(){
		return mSavedDate;
	}

	public void setSavedDate(Date pSavedDate){
		mSavedDate = pSavedDate;
	}

}
