package com.cps.commerce.order;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import atg.commerce.claimable.ClaimableManager;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.OrderTools;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.PaymentGroupOrderRelationship;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.SimpleOrderManager;
import atg.commerce.order.Order;
import atg.commerce.pricing.AmountInfo;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.promotion.PromotionTools;
import atg.commerce.CommerceException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryUtils;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.idgen.IdGenerator;
import atg.service.idgen.IdGeneratorException;

import atg.service.lockmanager.ClientLockManager;
import atg.service.lockmanager.DeadlockException;
import atg.userprofiling.Profile;
import com.cps.commerce.inventory.CPSInventoryManager;
import com.cps.csr.userprofiling.CPSCSRProfileTools;
import com.cps.util.CPSConstants;

import javax.transaction.TransactionManager;

import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;

public class CPSOrderManager extends SimpleOrderManager {

	private IdGenerator mIdGenerator;

	public IdGenerator getIdGenerator() {
		return mIdGenerator;
	}

	public void setIdGenerator(IdGenerator pIdGenerator) {
		mIdGenerator = pIdGenerator;
	}

	private boolean mUseSystemTimeInRequestNumberGeneration = true;

	public boolean isUseSystemTimeInRequestNumberGeneration() {
		return mUseSystemTimeInRequestNumberGeneration;
	}

	public void setUseSystemTimeInRequestNumberGeneration(boolean pUseSystemTimeInRequestNumberGeneration) {
		mUseSystemTimeInRequestNumberGeneration = pUseSystemTimeInRequestNumberGeneration;
	}

	private CPSCSRProfileTools mProfileTools;

	public CPSCSRProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSCSRProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	private ClientLockManager mLocalLockManager;

	public void setLocalLockManager(ClientLockManager pLocalLockManager) {
		mLocalLockManager = pLocalLockManager;
	}

	public ClientLockManager getLocalLockManager() {
		return mLocalLockManager;
	}

	private TransactionManager mTransactionManager;

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void mergeOrders(Order pSrcOrder, Order pDestOrder) throws CommerceException {

		Date b = pDestOrder.getLastModifiedDate();

		vlogDebug("Setting Time to Compare" + ((CPSOrderImpl) pDestOrder).getPrevLastModifiedDate());

		//b = ((CPSOrderImpl) pDestOrder).getLastModifiedDate();

		Calendar cal = Calendar.getInstance();
		vlogDebug("Current time:" + cal.getTime());
		cal.setTime(b);
		vlogDebug("what time should be " + cal.getTime());
		cal.add(Calendar.MINUTE, getProfileTools().getLogoutlength());
		Calendar n = Calendar.getInstance();
		vlogDebug("should we remove items" + n.compareTo(cal));
		if (n.compareTo(cal) > 0) {
			vlogDebug("removing items form cart....");
			pDestOrder.removeAllCommerceItems();
			pDestOrder.removeAllRelationships();
			cal = null;
		}
		super.mergeOrders(pSrcOrder, pDestOrder);

	}


	/**
	 * Return multiple coupon codes for provided order. Looks up profile from order, looping through
	 * active promotions for profile, getting coupons with correct site (check for shared cart in multisite)
	 *
	 * @param pOrder
	 * @return
	 * @throws CommerceException
	 */
	@SuppressWarnings("unchecked")
	public List<String> getCouponCode(CPSOrderImpl pOrder) throws CommerceException {
		List<String> couponList = new ArrayList();
		try {
			OrderTools orderTools = getOrderTools();
			Repository profileRepository = orderTools.getProfileRepository();
			CommerceProfileTools profileTools = orderTools.getProfileTools();
			ClaimableManager claimableManager = profileTools.getClaimableManager();
			PromotionTools promotionTools = claimableManager.getPromotionTools();
			RepositoryItem profile = profileRepository.getItem(pOrder.getProfileId(), profileTools.getDefaultProfileType());
			Collection<RepositoryItem> activePromotions = (Collection<RepositoryItem>) profile.getPropertyValue(promotionTools.getActivePromotionsProperty());

			if (isLoggingDebug()) {
				logDebug("getCouponCode start!");
				logDebug("activePromotions=" + activePromotions);
			}
			for (RepositoryItem promotionStatus : activePromotions) {
				Collection<RepositoryItem> coupons = (Collection<RepositoryItem>) promotionStatus.getPropertyValue(promotionTools.getPromoStatusCouponsPropertyName());
				for (RepositoryItem coupon : coupons) {
					//checking site
					if (claimableManager.checkCouponSite(coupon)) {
						String couponId = (String) coupon.getPropertyValue(claimableManager.getClaimableTools().getIdPropertyName());
						couponList.add(couponId);
					}
				}
			}

			if (isLoggingDebug()) {
				logDebug("couponList=" + couponList);
			}
		} catch (RepositoryException e) {
			throw new CommerceException(e);
		}
		return couponList;
	}

	/**
	 * Return map with coupon code and discount amount
	 *
	 * @param pOrder
	 * @return
	 * @throws CommerceException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Double> getCouponCodeMap(CPSOrderImpl pOrder) throws CommerceException {
		Map<String, Double> couponMapList = new HashMap<String, Double>();
		try {
			OrderTools orderTools = getOrderTools();
			Repository profileRepository = orderTools.getProfileRepository();
			CommerceProfileTools profileTools = orderTools.getProfileTools();
			ClaimableManager claimableManager = profileTools.getClaimableManager();
			PromotionTools promotionTools = claimableManager.getPromotionTools();
			RepositoryItem profile = profileRepository.getItem(pOrder.getProfileId(), profileTools.getDefaultProfileType());
			Collection<RepositoryItem> activePromotions = (Collection<RepositoryItem>) profile.getPropertyValue(promotionTools.getActivePromotionsProperty());

			List<PricingAdjustment> promoAdjustments = new ArrayList<PricingAdjustment>();
			findAllPromotions(pOrder, promoAdjustments);

			if (isLoggingDebug()) {
				logDebug("getCouponCodeMap start!");
				logDebug("activePromotions=" + activePromotions);
			}
			for (RepositoryItem promotionStatus : activePromotions) {
				Collection<RepositoryItem> coupons = (Collection<RepositoryItem>) promotionStatus.getPropertyValue(promotionTools.getPromoStatusCouponsPropertyName());
				for (RepositoryItem coupon : coupons) {
					//checking site
					if (claimableManager.checkCouponSite(coupon)) {
						String couponId = (String) coupon.getPropertyValue(claimableManager.getClaimableTools().getIdPropertyName());
						double discounAmount = 0.0;
						Collection<RepositoryItem> promos = (Collection<RepositoryItem>) coupon.getPropertyValue(claimableManager.getClaimableTools().getPromotionsPropertyName());
						for (RepositoryItem promo : promos) {
							for (PricingAdjustment adj : promoAdjustments) {
								String itemDisplayName = adj.getPricingModel().getItemDisplayName();
								if (itemDisplayName != null && itemDisplayName.equals((String) promo.getPropertyValue(claimableManager.getClaimableTools().getDisplayNamePropertyName()))) {
									discounAmount += adj.getTotalAdjustment();
									if (discounAmount < 0.0) {
										discounAmount = discounAmount * (-1);
									}
								}
							}
						}
						couponMapList.put(couponId, discounAmount);
					}
				}
			}

			if (isLoggingDebug()) {
				logDebug("couponMapList......................" + couponMapList);
			}
		} catch (RepositoryException e) {
			throw new CommerceException(e);
		}
		return couponMapList;
	}


	private void findAllPromotions(CPSOrderImpl order, List<PricingAdjustment> pPromoAdjustments) {
		OrderPriceInfo oPriceInfo = order.getPriceInfo();
		findPromotions(oPriceInfo, pPromoAdjustments);

		List<CommerceItem> cItems = (List<CommerceItem>) order.getCommerceItems();
		for (CommerceItem ci : cItems) {
			ItemPriceInfo pi = ci.getPriceInfo();
			findPromotions(pi, pPromoAdjustments);
		}

		List<ShippingGroup> shippingGroups = order.getShippingGroups();
		if (shippingGroups != null) {
			for (ShippingGroup sg : shippingGroups) {
				ShippingPriceInfo sPriceInfo = sg.getPriceInfo();
				if (sPriceInfo != null) {
					findPromotions(sPriceInfo, pPromoAdjustments);
				}
			}
		}
	}

	private void findPromotions(AmountInfo pPriceInfo, List<PricingAdjustment> pAdjustments) {
		if (pPriceInfo != null) {
			List<PricingAdjustment> listAdj = pPriceInfo.getAdjustments();
			for (PricingAdjustment adj : listAdj) {
				if (adj.getPricingModel() != null) {
					pAdjustments.add(adj);
				}
			}
		}
	}

	private boolean findPromotions(ShippingPriceInfo pPriceInfo, List<PricingAdjustment> pAdjustments) {
		boolean result = false;
		if (pPriceInfo != null) {
			double total = 0;
			List<PricingAdjustment> listAdj = pPriceInfo.getAdjustments();
			for (PricingAdjustment adj : listAdj) {
				if (adj.getPricingModel() != null) {
					pAdjustments.add(adj);
					//isShip = true;
					if (adj.getTotalAdjustment() == (-1) * total) {
						result = true;
					}
				} else {
					total = adj.getTotalAdjustment();
				}
			}
		}
		return result;
	}


	/**
	 * decrementing inventory level on checkout
	 *
	 * @param pOrder
	 */
	public void decrementInventoryOnCheckout(Order pOrder) {
		CPSInventoryManager inventoryManager = (CPSInventoryManager) getOrderTools().getInventoryManager();
		List<CommerceItem> commerceItems = (List<CommerceItem>) pOrder.getCommerceItems();
		Iterator<CommerceItem> iter = commerceItems.iterator();
		while (iter.hasNext()) {
			CommerceItem ci = iter.next();
			long q = ci.getQuantity();
			String skuId = ci.getCatalogRefId();
			if (isLoggingDebug()) {
				logDebug(new StringBuilder("Inventory on checkout. sku id: ").append(skuId).append("Inventory on checkout. quantity: ").append(q).toString());
			}

			inventoryManager.decrementInventoryLevel(skuId, q);
		}
	}

	public String getJdeOrderNumber() throws IdGeneratorException {
		String jdeOrderNumber = getIdGenerator().generateStringId("jdeOrderNumber");
		vlogDebug("Order Manager's getJdeOrderNumber() : jde order number: " + jdeOrderNumber);
		return jdeOrderNumber;
	}

	private void resetPaymentAmounts(Order pOrder) throws CommerceException {
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup.getOrderRelationshipCount() > 0) {
				PaymentGroupOrderRelationship rel = (PaymentGroupOrderRelationship) paymentGroup.getOrderRelationship();
				if (rel != null) {
					rel.setAmount(0.0);
				}
			}
		}
	}

	public Order duplicateOrder(String pOrderId) throws CommerceException {

		Order order = null;
		try {
			Map exclProps = ((CPSOrderTools) getOrderTools()).getDuplicateOrderExcludedOrderPropertiesMap();
			RepositoryItem orderItem = getOrderTools().getOrderRepository().getItem(pOrderId, getOrderItemDescriptorName());
			RepositoryItem clonedOrderItem = RepositoryUtils.cloneItem(orderItem, true, null, exclProps, null, null);

			if (clonedOrderItem == null) {
				return null;
			}
			order = loadOrder(clonedOrderItem.getRepositoryId());

			if (isLoggingDebug()) {
				logDebug("Order " + order.getId() + " loaded!");
			}

			resetOrderCommerceItemRelationships(order);
			resetPaymentAmounts(order);
			resetOrder(order);
			updateOrder(order);

		} catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
			throw new CommerceException(e);
		}

		return order;

	}

	public Order findOrderByWebOrderId(String pWebOrderId) {
		Order result = null;
		RepositoryItem[] items = getOrdersByWebId(pWebOrderId);
		try {
			if (items != null && items.length > 0) {
				result = loadOrder(items[0].getRepositoryId());
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return result;
	}

	public RepositoryItem[] getOrdersByWebId(String pWebOrderId) {
		RepositoryItem[] items = null;
		try {
			Repository orderRepository = getOrderTools().getOrderRepository();
			RepositoryView view = orderRepository.getView(getOrderItemDescriptorName());
			RqlStatement statement = RqlStatement.parseRqlStatement(CPSConstants.WEB_ORDER_ID + "=?0");
			items = statement.executeQuery(view, new Object[]{pWebOrderId});
		} catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return items;
	}

	public String setWebOrderId(Order pOrder) {
		String webOrderId = null;
		if (pOrder instanceof CPSOrderImpl) {
			webOrderId = getWebOrderId();
			((CPSOrderImpl) pOrder).setWebOrderId(webOrderId);
		} else {
			if (isLoggingError()) {
				vlogError("Order {0} is not instanceof CPSOrderImpl", pOrder.getId());
			}
		}
		return webOrderId;
	}

	private String getWebOrderId() {
		String webOrderId = null;
		try {
			webOrderId = getIdGenerator().generateStringId("cps");
		} catch (IdGeneratorException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return webOrderId;
	}

	public Order retrieveOrder(String pRqlStatement, Object[] pParams){

		Order order = null;
		RepositoryItem[] orderList = retrieveOrders(pRqlStatement, pParams);
		if (orderList != null && orderList.length > 0) {
			try {
				order = loadOrder(orderList[0].getRepositoryId());
			} catch (CommerceException e) {
				vlogError(e, "Error");
			}
		}
		return order;

	}

	public List<Order> retrieveOrders(Object[] pParams, String pRqlStatement) throws RepositoryException {

		List<Order> orderList = null;

		Repository orderRepository = getOrderTools().getOrderRepository();
		RepositoryView orderView = orderRepository.getView(ITEM_DESCRIPTOR_ORDER);
		RqlStatement rqlStatement = RqlStatement.parseRqlStatement(pRqlStatement);
		RepositoryItem[] results = rqlStatement.executeQuery(orderView, pParams);
		if (results != null && results.length > 0) {
			// instantiate arrayList object to add to
			orderList = new ArrayList<Order>();
			for (RepositoryItem orderRepositoryItem : results) {
				if (isLoggingDebug()) {
					logDebug(new StringBuilder().append("Found orderRepositoryItem:")
							.append(orderRepositoryItem.getRepositoryId())
							.append(", now load it as an object.")
							.toString());
				}
				try {
					Order order = loadOrder(orderRepositoryItem.getRepositoryId());
					orderList.add(order);
				} catch (CommerceException ce) {
					if (isLoggingError()) {
						logError(ce);
					}
				}
			}
		}
		return orderList;
	}

	public String getUniqueRequestNumber() {
		String requestNumber = null;
		try {
			requestNumber = getIdGenerator().generateStringId("uniqueRequestNumber");
			if (isUseSystemTimeInRequestNumberGeneration()) {
				requestNumber = new StringBuilder(requestNumber).append(System.currentTimeMillis()).toString();
			}
		} catch (IdGeneratorException e) {
			vlogError(e, "Error");
		}
		return requestNumber;
	}

	public RepositoryItem[] retrieveOrders(String pRqlStatement, Object[] pParams) {
		RepositoryItem[] orders = null;
		try {
			RepositoryView view = getOrderTools().getOrderRepository().getView(ITEM_DESCRIPTOR_ORDER);
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(pRqlStatement);
			orders = rqlStatement.executeQuery(view, pParams);
		} catch (Exception e) {
			vlogError(e, "retrieveOrders: Could not get orders from repository");
		}
		return orders;
	}

	public void updateOrderWithRequestNumber(Order pOrder, Profile pProfile, String pRequestNumber){
		boolean acquireLock = false;
		CPSOrderImpl order = (CPSOrderImpl)pOrder;
		try {
			acquireLock = !getLocalLockManager().hasWriteLock(pProfile.getRepositoryId(), Thread.currentThread());
			if (acquireLock) {
				getLocalLockManager().acquireWriteLock(pProfile.getRepositoryId(), Thread.currentThread());
			}
			TransactionDemarcation td = new TransactionDemarcation();
			td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
			boolean shouldRollback = false;
			try {
				synchronized (order) {
					order.setRequestNumber(pRequestNumber);
					updateOrder(order);
				}
			} catch (CommerceException e) {
				shouldRollback = true;
				vlogError(e, "CommerceException");
			} finally {
				try {
					td.end(shouldRollback);
				} catch (Throwable th) {
					vlogError(th, "Error");
				}
			}
		} catch (DeadlockException e) {
			vlogError(e, "DeadLockException");
		} catch (TransactionDemarcationException e) {
			vlogError(e, "TransactionDemarcationException");
		} finally {
			try {
				if (acquireLock) {
					getLocalLockManager().releaseWriteLock(pProfile.getRepositoryId(), Thread.currentThread(), true);
				}
			} catch (Throwable th) {
				vlogError(th, "Error");
			}
		}

	}
	
	/**
	 * Updating the order with JDE order number after web-service failure
	 * @param pOrder
	 * @param pProfileId
	 * @param pJDEOrderNumber
	 */
	public void updateOrderWithJDEOrderNumber(Order pOrder, String pProfileId, String pJDEOrderNumber){
		boolean acquireLock = false;
		CPSOrderImpl order = (CPSOrderImpl)pOrder;
		try {
			acquireLock = !getLocalLockManager().hasWriteLock(pProfileId, Thread.currentThread());
			if (acquireLock) {
				getLocalLockManager().acquireWriteLock(pProfileId, Thread.currentThread());
			}
			TransactionDemarcation td = new TransactionDemarcation();
			td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
			boolean shouldRollback = false;
			try {
				synchronized (order) {
					order.setJdeOrderNumber(pJDEOrderNumber);
					order.setJdeSuccess(true);
					updateOrder(order);
				}
			} catch (CommerceException e) {
				shouldRollback = true;
				vlogError("CommerceException: {0}",e);
			} finally {
				try {
					td.end(shouldRollback);
				} catch (Throwable th) {
					vlogError(th, "Error");
				}
			}
		} catch (DeadlockException e) {
			vlogError("DeadLockException: {0}",e);
		} catch (TransactionDemarcationException e) {
			vlogError("TransactionDemarcationException: {0}",e);
		} finally {
			try {
				if (acquireLock) {
					getLocalLockManager().releaseWriteLock(pProfileId, Thread.currentThread(), true);
				}
			} catch (Throwable th) {
				vlogError(th, "Error");
			}
		}	
		
	}

}
