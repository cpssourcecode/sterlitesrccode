package com.cps.commerce.order;

import atg.commerce.CommerceException;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.states.OrderStates;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.ContactInfo;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.core.util.CPSContactInfo;

import javax.servlet.ServletException;
import javax.transaction.Transaction;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import static com.cps.util.CPSConstants.*;

public class CPSSavedCartFormHandler extends CartModifierFormHandler {

	private static final String ITEM_DESCRIPTOR_CONTACT_INFO = "contactInfo";

	private String mOrderId;

	private static boolean isOrderEmpty(Object object) {
		Order order = (Order) object;
		return order.getCommerceItemCount() < 1;
	}

	public String getOrderId() {
		return mOrderId;
	}

	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	private boolean mCopyOrder;

	public boolean isCopyOrder() {
		return mCopyOrder;
	}

	public void setCopyOrder(boolean pCopyOrder) {
		mCopyOrder = pCopyOrder;
	}

	private String mIncompleteOrderState;

	public String getIncompleteOrderState() {
		return mIncompleteOrderState;
	}

	public void setIncompleteOrderState(String pIncompleteOrderState) {
		mIncompleteOrderState = pIncompleteOrderState;
	}

	private String mDeletedOrderState;

	public String getDeletedOrderState() {
		return mDeletedOrderState;
	}

	public void setDeletedOrderState(String pDeletedOrderState) {
		mDeletedOrderState = pDeletedOrderState;
	}

	private OrderStates mOrderStates;

	public OrderStates getOrderStates() {
		return mOrderStates;
	}

	public void setOrderStates(OrderStates pOrderStates) {
		mOrderStates = pOrderStates;
	}

	private Repository mLocationRepository;

	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}

	public boolean handleDeleteOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSSavedCartFormHandler.handleDeleteOrder";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				TransactionDemarcation td = new TransactionDemarcation();
				try {
					td.begin(getTransactionManager(), td.REQUIRED);
					Order order = getOrderManager().loadOrder(getOrderId());
					synchronized (order) {
						order.setState(getOrderStates().getStateValue(getDeletedOrderState().toLowerCase()));
						updateOrder(order, MSG_ERROR_UPDATE_ORDER, pRequest, pResponse);
					}
				} finally {
					td.end();
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				addFormException(new DropletException("Unable to delete order", e));
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}

	private Order createNewOrder(Order pOrderToDuplicate) throws CommerceException {
		return ((CPSOrderManager) getOrderManager()).duplicateOrder(pOrderToDuplicate.getId());
	}

	private boolean isSaved(String pOrderId) {
		boolean saved = false;
		Collection<Order> savedOrders = getShoppingCart().getSaved();
		if (savedOrders != null && savedOrders.size() > 0) {
			for (Order order : savedOrders) {
				if (pOrderId.equals(order.getId())) {
					saved = true;
					break;
				}
			}
		}
		return saved;
	}

	private boolean shouldSaveOrder(Order pOrder) {
		boolean shouldSave = false;
		if (pOrder != null) {
			shouldSave = (pOrder.getCommerceItemCount() > 0);
		}
		return shouldSave;
	}

	private CPSContactInfo findContactInfoById(String contactInfoId) throws RepositoryException, CommerceException {
		RepositoryItem repositoryContactInfo = getProfileRepository().getItem(contactInfoId, ITEM_DESCRIPTOR_CONTACT_INFO);
		if (repositoryContactInfo != null) {
			CPSContactInfo contactInfo = new CPSContactInfo();
			OrderTools.copyAddress(repositoryContactInfo, contactInfo);
			return contactInfo;
		} else {
			throw new RepositoryException();
		}
	}

	private RepositoryItem getStore(String pBranchId) {
		RepositoryItem store = null;
		try {
			String rqlQuery = STORE_BRANCH_ID_PRTY + "=" + pBranchId;
			RepositoryView rpView = getLocationRepository().getView(STORE_ITEM_TYPE);
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
			RepositoryItem[] results = stmt.executeQuery(rpView, null);
			if (results != null && results.length == 1) {
				store = results[0];
			}
		} catch (Exception e) {
			vlogError(e, "LocationByBranchLookupDroplet.getStore:");
		}
		return store;
	}

	private void updateCurrentCart(Order pOrder) throws RepositoryException, CommerceException {
		List<ShippingGroup> shippingGroups = (List<ShippingGroup>) pOrder.getShippingGroups();
		RepositoryItem selectedCS = (RepositoryItem) getProfile().getPropertyValue(SELECTED_CS);
		if (shippingGroups != null && shippingGroups.size() > 0) {
			ShippingGroup shippingGroup = shippingGroups.get(0);
			if (shippingGroup instanceof CPSHardgoodShippingGroup) {
				CPSHardgoodShippingGroup sg = (CPSHardgoodShippingGroup) shippingGroup;
				ContactInfo shipAddress = (ContactInfo) sg.getShippingAddress();
				if (shipAddress == null || StringUtils.isBlank(shipAddress.getAddress1())) {
					RepositoryItem address = null;
					String addressId = null;
					RepositoryItem cartSelectedCS = (RepositoryItem) getProfile().getPropertyValue(CART_SELECTED_CS);
					if (cartSelectedCS != null) {
						address = cartSelectedCS;
						addressId = cartSelectedCS.getRepositoryId();
					} else if (selectedCS != null) {
						address = selectedCS;
						addressId = selectedCS.getRepositoryId();
					}
					if (address != null) {
						CPSContactInfo shippingAddress = findContactInfoById(addressId);
						sg.setShippingAddress(shippingAddress);
						sg.setJdeAddressNumber(shippingAddress.getJdeAddressNumber());
					}
				}
			} else if (shippingGroup instanceof InStorePickupShippingGroup) {
				InStorePickupShippingGroup sg = (InStorePickupShippingGroup) shippingGroup;
				if (StringUtils.isBlank(sg.getLocationId())) {
					String cartSelectedStoreId = (String) getProfile().getPropertyValue(CART_SELECTED_STORE);
					String jdeAddressNumber = "";
					String branchId = "";
					String storeId = "";
					if (selectedCS != null) {
						jdeAddressNumber = (String) selectedCS.getPropertyValue(CONTACT_INFO_JDE_ADDRESS_NUMBER);
						branchId = (String) selectedCS.getPropertyValue(BUSINESS_UNIT);
						RepositoryItem store = getStore(branchId);
						if (store != null) {
							storeId = store.getRepositoryId();
						}
					}
					if (!StringUtils.isBlank(cartSelectedStoreId)) {
						storeId = cartSelectedStoreId;
					}
					sg.setLocationId(storeId);
					sg.setPropertyValue(JDE_ADDRESS_NUM, jdeAddressNumber);
				}
			}
		}
	}

	public boolean handleEditOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		if (isLoggingDebug()) {
			logDebug("CPSSavedCartFormHandler.handleEditOrder - start");
		}

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "CPSSavedCartFormHandler.handleEditOrder";
		if ((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				if (getUserLocale() == null) {
					setUserLocale(getUserLocale(pRequest, pResponse));
				}
				Order order = null;
				if (isCopyOrder()) {
					Order orderToDuplicate = getOrderManager().loadOrder(getOrderId());
					order = createNewOrder(orderToDuplicate);
				} else {
					order = getOrderManager().loadOrder(getOrderId());
				}
				if (order != null) {
					synchronized (order) {
						try {
							if (isLoggingDebug()) {
								logDebug(new StringBuilder()
										.append("Current shoppingCart:")
										.append(getShoppingCart().getCurrent().getId())
										.append(", replace (switch) with new order:")
										.append(getOrderId())
										.toString());

								logDebug(new StringBuilder().append("Saved carts: ").append(getShoppingCart().getSaved()).toString());
							}

							order.setState(getOrderStates().getStateValue(getIncompleteOrderState().toLowerCase()));
							if (!isSaved(order.getId())) {
								getShoppingCart().getSaved().add(order);
							}
							Order currentOrder = getOrder();
							if (shouldSaveOrder(currentOrder)) {
								updateCurrentCart(currentOrder);
								if (currentOrder.isTransient()) {
									if (isLoggingDebug()) {
										logDebug(new StringBuilder().append("Adding order to repository: ").append(currentOrder.getId()).toString());
									}
									getOrderManager().addOrder(currentOrder);
								} else {
									if (isLoggingDebug()) {
										logDebug(new StringBuilder().append("Updating order: ").append(currentOrder.getId()).toString());
									}
									getOrderManager().updateOrder(currentOrder);
								}
							}
							getShoppingCart().switchCurrentOrder(order.getId());
							//runProcessMoveToPurchaseInfo(order, getUserPricingModels(), getUserLocale(), getProfile(), null);
						} catch (Exception exc) {
							processException(exc, MSG_ERROR_MOVE_TO_PURCHASE_INFO, pRequest, pResponse);
						}

					} // end synchronized
				} else {
					if (isLoggingError()) {
						logError(new StringBuilder().append("Failed to load copy order based on orderId:")
								.append(getOrderId())
								.toString());
					}
					throw new Exception(new StringBuilder().append("Failed to load copy order:").append(getOrderId()).toString());
				}

			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				addFormException(new DropletException(new StringBuilder().append("Error loading order.")
						.append(" Error:").append(e.getMessage()).toString()));
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(myHandleMethod);
				}
				//getShoppingCart().getSaved().stream().filter(object -> isOrderEmpty(object)).findFirst();
				//getShoppingCart().getSaved().removeIf(CPSSavedCartFormHandler::isOrderEmpty);
			}
		}

		if (isLoggingDebug()) {
			logDebug("CPSSavedCartFormHandler.handleEditOrder - end");
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}

}
