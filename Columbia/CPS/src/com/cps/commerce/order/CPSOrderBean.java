package com.cps.commerce.order;

import java.util.Calendar;
import java.util.Date;

public class CPSOrderBean {

	private double mOrderTotal;
	
	public double getOrderTotal(){
		return mOrderTotal;
	}

	public void setOrderTotal(double pOrderTotal){
		mOrderTotal = pOrderTotal;
	}

	private double mSpendingLimit;

	public double getSpendingLimit(){
		return mSpendingLimit;
	}

	public void setSpendingLimit(double pSpendingLimit){
		mSpendingLimit = pSpendingLimit;
	}

	private Date mSubmittedDate;
	
	public Date getSubmittedDate(){
		return mSubmittedDate;
	}

	public void setSubmittedDate(Date pSubmittedDate){
		mSubmittedDate = pSubmittedDate;
	}

	private String mId;

	public String getId(){
		return mId;
	}

	public void setId(String pId){
		mId = pId;
	}

	private String mWebOrderId;

	public String getWebOrderId(){
		return mWebOrderId;
	}

	public void setWebOrderId(String pWebOrderId){
		mWebOrderId = pWebOrderId;
	}

	private String mProfileId;

	public String getProfileId(){
		return mProfileId;
	}

	public void setProfileId(String pProfileId){
		mProfileId = pProfileId;
	}

	private String mProfileName;

	public String getProfileName(){
		return mProfileName;
	}

	public void setProfileName(String pProfileName){
		mProfileName = pProfileName;
	}

}
