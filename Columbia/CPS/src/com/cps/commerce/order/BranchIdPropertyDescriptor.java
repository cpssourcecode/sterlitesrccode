package com.cps.commerce.order;

import atg.core.util.StringUtils;
import atg.nucleus.Nucleus;
import atg.repository.*;
import atg.repository.rql.RqlStatement;

import static com.cps.util.CPSConstants.*;


/**
 * Get BranchId from Location repository by locationId
 *
 * @author Andy Porter
 */
public class BranchIdPropertyDescriptor extends RepositoryPropertyDescriptor {
	/**
	 * /atg/commerce/locations/LocationRepository
	 */
	private Repository mLocationRepository;
	/**
	 * /atg/userprofiling/ProfileAdapterRepository
	 */
	private Repository mProfileRepository;

	private static final String QUERY_JDE_ADDRESS = "jdeAddressNumber = ?0";

	//------------------------------------------------------------------------------------------------------------------

	@Override
	public Object getPropertyValue(RepositoryItemImpl pItem, Object pValue) {

		if (pItem == null) {
			return null;
		} else if (SHIPPPING_TYPE_HARDGOOD.equals(pItem.getItemDescriptor().getItemDescriptorName())) {
			String jdeAddressNumber = (String) pItem.getPropertyValue(CONTACT_INFO_JDE_ADDRESS_NUMBER);
			if (StringUtils.isBlank(jdeAddressNumber)) {
				Nucleus.getGlobalNucleus().vlogError("hardgoodShippingGroup {0} has no specified jdeAddressNumber", pItem.getRepositoryId());
			} else {
				RepositoryItem address = getShippingNameByShipTo(jdeAddressNumber);
				if (address == null) {
					Nucleus.getGlobalNucleus().vlogError("Unable to load address by jdeAddressNumber {0} from hardgoodShippingGroup {1}", jdeAddressNumber, pItem.getRepositoryId());
				} else {
					String businessUnit = (String) address.getPropertyValue(BUSINESS_UNIT);
					if (!StringUtils.isBlank(jdeAddressNumber)) {
						return businessUnit;
					} else {
						Nucleus.getGlobalNucleus().vlogError("contactInfo with jdeAddressNumber {0} has no specified businessUnit property", jdeAddressNumber);
						return LOCATION_DEFAULT_BRANCH_ID;   // 100
					}
				}
			}

		} else if (SHIPPPING_TYPE_IN_STORE_PICKUP.equals(pItem.getItemDescriptor().getItemDescriptorName())) {
			try {
				String locationId = (String) pItem.getPropertyValue(ISPU_LOCATION_ID_PRTY);
				if (StringUtils.isBlank(locationId)) {
					Nucleus.getGlobalNucleus().vlogError("inStorePickupShippingGroup {0} has no specified locationId", pItem.getRepositoryId());
				} else {
					RepositoryItem store = getLocationRepository().getItem(locationId, STORE_ITEM_TYPE);
					if (store == null) {
						Nucleus.getGlobalNucleus().vlogError("Unable to load store by locationId {0} from inStorePickupShippingGroup {1}", locationId, pItem.getRepositoryId());
					} else {
						return store.getPropertyValue(STORE_BRANCH_ID_PRTY);
					}
				}
			} catch (RepositoryException e) {
				Nucleus.getGlobalNucleus().vlogError(e, "Unable to get inStorePickupShippingGroup branchId:");
			}
		}
		return null;
	}

	@Override
	public void setPropertyValue(RepositoryItemImpl pItem, Object pValue) {
		//
	}

	//------------------------------------------------------------------------------------------------------------------

	public RepositoryItem getShippingNameByShipTo(String pJdeAddressNumber) {
		RepositoryItem result = null;
		try {
			RepositoryView view = getProfileRepository().getView(CONTACT_INFO);
			RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_JDE_ADDRESS);
			RepositoryItem[] items = statement.executeQuery(view, new Object[]{pJdeAddressNumber});
			if (items != null && items.length > 0) {
				result = items[0];
			}
		} catch (RepositoryException e) {
			Nucleus.getGlobalNucleus().vlogError(e, "Unable to load address by jdeAddressNumber {0} from hardgoodShippingGroup", pJdeAddressNumber);
		}

		return result;
	}


	public Repository getLocationRepository() {
		if (mLocationRepository == null) {
			mLocationRepository = (Repository) Nucleus.getGlobalNucleus().resolveName("/atg/commerce/locations/LocationRepository");
		}
		return mLocationRepository;
	}

	public Repository getProfileRepository() {
		if (mProfileRepository == null) {
			mProfileRepository = (Repository) Nucleus.getGlobalNucleus().resolveName("/atg/userprofiling/ProfileAdapterRepository");
		}
		return mProfileRepository;
	}
}
