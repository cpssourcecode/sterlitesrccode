package com.cps.commerce.order;

import atg.nucleus.GenericService;
import com.cps.integrations.pricing.PricingClientMulti;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.cps.integrations.pricing.CPSPricingConstants.DEFAULT_QTY;

public class CPSAvailabilityManager extends GenericService {
    private static final String TEST_DATA = "1";

    private PricingClientMulti mPricingClientMulti;

    public boolean isJdeAvailable() {
        boolean isDebug = isLoggingDebug();
        boolean availablePricesWebService;
        try {
            Set<String> products = new HashSet<>();
            products.add(TEST_DATA);
            getPricingClientMulti().getCustomerItemPriceAsync(products, TEST_DATA, DEFAULT_QTY, TEST_DATA, isDebug);
            availablePricesWebService = true;
        } catch (ExecutionException | InterruptedException | IOException e) {
            availablePricesWebService = false;
        }
        return availablePricesWebService;
    }

    public PricingClientMulti getPricingClientMulti() {
        return mPricingClientMulti;
    }

    public void setPricingClientMulti(PricingClientMulti pPricingClientMulti) {
        mPricingClientMulti = pPricingClientMulti;
    }
}
