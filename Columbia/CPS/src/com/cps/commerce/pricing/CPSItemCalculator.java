package com.cps.commerce.pricing;

import atg.commerce.pricing.ItemListPriceCalculator;
import atg.commerce.pricing.PricingException;

/**
 * CPSItemCalculator
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Jason Robert Juvingo
 *
 */
public class CPSItemCalculator extends ItemListPriceCalculator {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/commerce/pricing/CPSItemCalculator";

    /** Session Cart Price List property */
    private String mSessionCartPriceList;

    /**
     * @return
     */
    public String getSessionCartPriceList() {
        return mSessionCartPriceList;
    }

    /**
     * @param pSessionCartPriceList
     */
    public void setSessionCartPriceList(String pSessionCartPriceList) {
        mSessionCartPriceList = pSessionCartPriceList;
    }

    /**
     * Return the price that should be used in pricing. Return -1 if no pricing should take place for this object. The priceSource will be a catalogRef
     * (RepositoryItem) which will be the sku that is referenced in the session map.
     */
    @Override
    protected double getPrice(Object pPriceSource) throws PricingException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".getPrice").append(" - start").toString());
        }

        double price = 0.0;
        // if (pPriceSource != null) {
        // if (isLoggingDebug())
        // logDebug(new StringBuilder()
        // .append("Overriding price look up by getting skuId price from session map based on priceSource:")
        // .append(pPriceSource.getClass()).toString());
        // if (pPriceSource instanceof RepositoryItem) {
        // if (isLoggingDebug())
        // logDebug(new StringBuilder()
        // .append("Price source is a RepositoryItem and based on the flag priceFromCatalogRef:")
        // .append(isPriceFromCatalogRef())
        // .append(", it is a sku.")
        // .append(" Looking in the session cart price list map for a price based on this sku:")
        // .append(((RepositoryItem) pPriceSource)
        // .getRepositoryId()).toString());
        // // get session price list object by resolving from current request
        // DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
        // CPSSessionPriceList cartPriceList = (CPSSessionPriceList)request.resolveName(getSessionCartPriceList());
        // price = (Double) cartPriceList.getValues().get(
        // ((RepositoryItem) pPriceSource).getRepositoryId());
        // } else {
        // StringBuilder errorMessage = new StringBuilder(
        // "Price source is not a repository item in the getPrice lookup in the calculator.");
        // if (isLoggingError())
        // logError(errorMessage.toString());
        // throw new PricingException(errorMessage.toString());
        // }
        // } else {
        // StringBuilder errorMessage = new StringBuilder(
        // "Price source is null in the getPrice lookup in the calculator.");
        // if (isLoggingError())
        // logError(errorMessage.toString());
        // throw new PricingException(errorMessage.toString());
        // }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".getPrice").append(" - exit with price:").append(price).toString());
        }
        return price;
    }
}
