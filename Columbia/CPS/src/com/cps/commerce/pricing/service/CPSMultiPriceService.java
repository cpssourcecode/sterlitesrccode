package com.cps.commerce.pricing.service;

import static com.cps.util.CPSConstants.IS_AVAILABILITY_CHECKED;
import static com.cps.util.CPSConstants.IS_PRICED;
import static com.cps.util.CPSConstants.RESET_AVAILABLE_QTY;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.commerce.order.CPSCommerceItemImpl;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.service.CPSProductPricingService;
import com.cps.util.CPSConstants;
import com.endeca.infront.cartridge.model.Attribute;
import com.endeca.infront.cartridge.model.Record;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

public class CPSMultiPriceService extends GenericService {

	private CommerceItemManager commerceItemManager;
	private TransactionManager transactionManager;
	private OrderManager orderManager;


    private CPSProductPricingService productPricingService;

	private CPSCatalogTools mCatalogTools;

	public boolean isPricingWebserviceEnable() {
        return getProductPricingService().getExternalProductService().isPricingWebserviceEnable();
	}


	/**
     * @return the productPricingService
     */
    public CPSProductPricingService getProductPricingService() {
        return productPricingService;
    }

    /**
     * @param productPricingService
     *            the productPricingService to set
     */
    public void setProductPricingService(CPSProductPricingService productPricingService) {
        this.productPricingService = productPricingService;
    }

    /**
     * Price all products from in PLP page
     *
     * @param pRecords
     *            pRecords
     * @param pProfile
     *            pProfile
     * @throws ServletException
     * @throws IOException
     */
	public Map<String, Double> priceItems(List<Record> pRecords, Profile pProfile, CPSOrderImpl pOrder) throws ServletException, IOException {
		Set<String> products = new HashSet<String>();
		for (Record record : pRecords) {
			Attribute attribute = record.getAttributes().get("product.repositoryId");
			String productId = attribute.toString();
			products.add(productId);
		}

		Map<String, Double> itemPrices = new HashMap<>();

        try {
            itemPrices = getProductPricingService().getCustomerItemPrice(products, pProfile, pOrder);
		} catch (Exception ex) {
			if (isLoggingError()) {
				logError(ex);
			}
		}

		return itemPrices;
	}

	/**
	 * Update all commerce items in order with prices
	 *
	 * @param pOrder    Order
	 * @param pProfile pProfile
	 */
	public void updateFullOrderPrices(final Order pOrder, RepositoryItem pProfile) {
		final Order order = pOrder;

		Map<String, Double> itemPrices = getUpdatedPricesForOrder(order, pProfile);

		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;
		try {
			try {
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
				synchronized (order) {
					List<CPSCommerceItemImpl> commerceItems = order.getCommerceItems();

					double orderAmount = 0;
					for (CommerceItem commerceItem : commerceItems) {
						CPSCatalogTools catalogTools = getCatalogTools();
						if(catalogTools != null && catalogTools.isItemValid(commerceItem)) {
							orderAmount += priceItem(commerceItem, itemPrices);
						}
					}

					OrderPriceInfo orderPriceInfo = order.getPriceInfo();
					if (orderPriceInfo != null) {
						orderPriceInfo.setAmount(orderAmount);
						orderPriceInfo.setRawSubtotal(orderAmount);
						orderPriceInfo.setAmountIsFinal(true);
						orderPriceInfo.setCurrencyCode(CPSConstants.DEFAULT_CURRENCY_CODE);
					}
					getOrderManager().updateOrder(order);
				}
			} catch (Exception e) {
				rollback = true;
				if (isLoggingError()) {
					logError(e);
				}
			} finally {
				td.end(rollback);
			}
		} catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
	}

	/**
	 * get all prices for products from Order
	 *
	 * @param order    Order
	 * @param pProfile pProfile
	 * @return Map<String, Double> with prices
	 */
	private Map<String, Double> getUpdatedPricesForOrder(Order order, RepositoryItem pProfile) {
		Map<String, Double> itemPrices = null;

		Set<String> products = new HashSet<String>();
		List<CPSCommerceItemImpl> commerceItems = order.getCommerceItems();
		for (CPSCommerceItemImpl commerceItem : commerceItems) {
			boolean isPriced = (boolean) commerceItem.getPropertyValue(IS_PRICED);

			// According to CLMB0205-405 we have to change product price each time when shipping address changes
			if (/*!isPriced &&*/ isValidItemToPricing(commerceItem)) {
				String productId = commerceItem.getCatalogRefId();
				products.add(productId);
			}
		}

		try {

            itemPrices = getProductPricingService().getCustomerItemPrice(products, pProfile, (CPSOrderImpl) order);
		} catch (Exception ex) {
			if (isLoggingError()) {
				logError(ex);
			}
		}

		return itemPrices;
	}

	/**
	 * Price one commerceItem
	 *
	 * @param pCommerceItem CommerceItem
	 * @param pItemPrices   Map<String, Double> items prices returned by WS
	 * @return item amount double
	 */
	private double priceItem(CommerceItem pCommerceItem, Map<String, Double> pItemPrices) {
		CPSCommerceItemImpl commerceItem = (CPSCommerceItemImpl) pCommerceItem;
		double itemAmount = 0;

		String productId = commerceItem.getCatalogRefId();
		if (pItemPrices != null) {
			Double unitPrice = pItemPrices.get(productId);
			if (null != unitPrice) {

				long qty = commerceItem.getQuantity();
				itemAmount = unitPrice * qty;
				itemAmount = (double) Math.round(itemAmount * 100);
				itemAmount = itemAmount / 100;
				if (unitPrice > 0.0) {
					commerceItem.setPropertyValue(IS_PRICED, true);
				}
				ItemPriceInfo itemPriceInfo = commerceItem.getPriceInfo();

				if (itemPriceInfo != null) {
					itemPriceInfo.setListPrice(unitPrice);
					itemPriceInfo.setAmount(itemAmount);
					itemPriceInfo.setAmountIsFinal(true);
				}
				commerceItem.setAvailableQuantity(RESET_AVAILABLE_QTY);
				commerceItem.setLeadTime(0);
				commerceItem.setPropertyValue(IS_AVAILABILITY_CHECKED, false);
			} else {
				ItemPriceInfo itemPriceInfo = commerceItem.getPriceInfo();
				if (itemPriceInfo != null) {
					long qty = commerceItem.getQuantity();
					unitPrice = itemPriceInfo.getListPrice();
					itemAmount = unitPrice * qty;
					if (unitPrice <= 0.0) {
						commerceItem.setPropertyValue(IS_PRICED, false);
					}
				}
			}
		}

		return itemAmount;
	}

	/**
	 * check if it reasonable to price item
	 *
	 * @param commerceItem CommerceItem
	 * @return boolean if need to be priced
	 */
	private boolean isValidItemToPricing(CommerceItem commerceItem) {
		RepositoryItem product = (RepositoryItem) commerceItem.getAuxiliaryData().getProductRef();
		if (product != null) {
			RepositoryItem parentCategory = (RepositoryItem) product.getPropertyValue("parentCategory");
			if (parentCategory == null) {
				Set<RepositoryItem> set = ((Set<RepositoryItem>) product.getPropertyValue("parentCategories"));
				if (set.size() > 0)
					parentCategory = set.iterator().next();
			}
			if (parentCategory != null) {
				return (Boolean) parentCategory.getPropertyValue("eCommerceDisplay");
			}
		}
		return false;
	}

	private String getProductId(CommerceItem commerceItem) {
		String skuId = commerceItem.getCatalogRefId();
		return getCommerceItemManager().getProductIdFromSkuId(skuId);
	}

	public CommerceItemManager getCommerceItemManager() {
		return commerceItemManager;
	}

	public void setCommerceItemManager(CommerceItemManager pCommerceItemManager) {
		commerceItemManager = pCommerceItemManager;
	}

	public TransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		transactionManager = pTransactionManager;
	}

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager pOrderManager) {
		orderManager = pOrderManager;
	}

	public CPSCatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	public void setCatalogTools(CPSCatalogTools pCatalogTools) {
		mCatalogTools = pCatalogTools;
	}
}
