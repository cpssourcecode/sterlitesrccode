package com.cps.commerce.gifts;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistTools;
import atg.commerce.gifts.InvalidGiftTypeException;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.service.dynamo.LangLicense;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

public class CPSGiftlistTools extends GiftlistTools {

	private static final String MY_RESOURCE_NAME = "atg.commerce.gifts.GiftlistResources";

	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, LangLicense.getLicensedDefault());

	public boolean validateNewGiftlistNameExist(String name, List<String> listNames) {
		boolean valid = true;
		for (String listName : listNames) {
			if (name.toLowerCase().equals(listName.toLowerCase())) {
				valid = false;
				break;
			}
		}
		return valid;
	}

	public Map<String, Object> validateUpdatedGiftlist(String name, List<String> listNames) {

		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		if (StringUtils.isBlank(name)) {
			errors.put(CPSErrorCodes.ERR_NEW_MAT_LIST_MISSING, CPSConstants.EVENT_NAME);
		} else {
			for (String listName : listNames) {
				if (name.equals(listName)) {
					errors.put(CPSErrorCodes.ERR_REORDER_LIST_EXISTS, CPSConstants.EVENT_NAME);
				}
			}
		}
		return errors;

	}

	public Map<String, Object> validateEmails(String[] emails) {
		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		if (emails == null)
			errors.put(CPSErrorCodes.ERR_REORDER_MISSING_EMAIL, CPSConstants.EMAIL);
		else {
			for (String email : emails) {
				if (!checkValid(email.trim(), CPSConstants.REG_EXP_EMAIL))
					errors.put(CPSErrorCodes.ERR_REORDER_INVALID_EMAIL, CPSConstants.EMAIL);
			}
		}
		return errors;
	}

	public boolean checkValid(String value, String regExp) {
		if (value != null)
			return value.matches(regExp);
		return false;
	}

	private RepositoryItem[] getProfilesWithGiftList(RepositoryItem pGiftlist) throws RepositoryException {
		MutableRepository mutRep = (MutableRepository) getProfileRepository();
		RepositoryView view = mutRep.getView("user");
		QueryBuilder b = view.getQueryBuilder();
		QueryExpression lists = b.createPropertyQueryExpression("giftlists");
		QueryExpression list = b.createConstantQueryExpression(pGiftlist.getRepositoryId());
		Query query = b.createIncludesQuery(lists, list);
		return view.executeQuery(query);
	}

	public void removeGiftlistFromProfiles(RepositoryItem pGiftlist) throws CommerceException, RepositoryException {

		RepositoryItem[] users = getProfilesWithGiftList(pGiftlist);
		if (users == null) {
			MutableRepository mutRep = (MutableRepository) getProfileRepository();
			for (RepositoryItem user : users) {
				MutableRepositoryItem userForUpdate = mutRep.getItemForUpdate(user.getRepositoryId(), "user");
				List giftLists = (List) userForUpdate.getPropertyValue("giftlists");
				if (giftLists.contains(pGiftlist)) {
					giftLists.remove(pGiftlist);
				}
				removeOtherGiftlistFromProfile(userForUpdate, pGiftlist);
				mutRep.updateItem(userForUpdate);
			}
		}

	}

	public boolean removeGiftlistFromProfile(MutableRepositoryItem pProfile, RepositoryItem pGiftlist)
			throws CommerceException, RepositoryException {

		if (pProfile == null) {
			throw new InvalidGiftTypeException(ResourceUtils.getMsgResource("InvalidProfileParameter", MY_RESOURCE_NAME, sResourceBundle));
		} else if (pGiftlist == null) {
			throw new InvalidGiftTypeException(ResourceUtils.getMsgResource("InvalidGiftlistParameter", MY_RESOURCE_NAME, sResourceBundle));
		}
		List giftLists = (List) pProfile.getPropertyValue("giftlists");
		removeGiftlistFromProfiles(pGiftlist);
		removeGiftlistFromOtherProfiles(pGiftlist);
		giftLists.remove(pGiftlist);
		((MutableRepository) getProfileRepository()).updateItem(pProfile);
		((MutableRepository) getGiftlistRepository()).removeItem(pGiftlist.getRepositoryId(), getGiftlistItemDescriptor());
		return true;
	}

}