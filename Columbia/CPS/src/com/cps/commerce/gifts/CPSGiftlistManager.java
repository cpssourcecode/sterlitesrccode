package com.cps.commerce.gifts;

import static com.cps.util.CPSConstants.CATALOG_REF_ID;
import static com.cps.util.CPSConstants.DESCRIPTION;
import static com.cps.util.CPSConstants.DISPLAY_NAME;
import static com.cps.util.CPSConstants.ERR_MESSAGES_REPOSITORY;
import static com.cps.util.CPSConstants.EVENT_NAME;
import static com.cps.util.CPSConstants.EXTENDED_AMOUNT;
import static com.cps.util.CPSConstants.FILE_EXT_CSV;
import static com.cps.util.CPSConstants.FILE_EXT_XLS;
import static com.cps.util.CPSConstants.GIFTLIST_ITEMS;
import static com.cps.util.CPSConstants.ORG;
import static com.cps.util.CPSConstants.PRODUCT_ID;
import static com.cps.util.CPSConstants.QUANTITY;
import static com.cps.util.CPSConstants.QUANTITY_DESIRED;
import static com.cps.util.CPSConstants.QUANTITY_PURCHASED;
import static com.cps.util.CPSConstants.SITE_ID;
import static com.cps.util.CPSConstants.UNIT_PRICE;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFooter;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.BorderFormatting;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.pricing.service.CPSMultiPriceService;
import com.cps.service.CPSExternalProductService;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.userprofiling.validator.MaterialListValidator;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;
import com.cps.util.PoiUtils;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistFormHandler;
import atg.commerce.gifts.GiftlistManager;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.OrderHolder;
import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;
import cps.commerce.pricing.priceLists.CPSPriceListManager;

public class CPSGiftlistManager extends GiftlistManager {

    private static final String HEADER_DESCRIPTION = "Product Description";
    private static final String HEADER_DISPLAY_NAME = "Display Name";

	private static final String BOLD = "Bold";

	private static final String COLUMBIA_PIPE_SUPPLY_CO = "COLUMBIA PIPE SUPPLY && CO.";

	private static final String QUOTE_ACKNOWLEDGEMENT_HEADER = "QUOTE ACKNOWLEDGEMENT";

	private static final String CART_ITEMS = "Cart_Items";

	private static final String REGULAR = "Regular";

	private static final String ARIAL = "Arial";

	// ** Fields **
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("MM-dd-yyyy--hh-mm-ss");

    private boolean mIgnoreCaseItemsQuery;
    /**
     * contact us validator
     */
    private MaterialListValidator mValidator;
    /**
     * file ext
     */
    private String mFileExt = FILE_EXT_CSV;
    private CPSExternalProductService externalProductService;
    private CPSPriceListManager priceListManager;
    private CPSMultiPriceService multiPriceService;
    private Repository productRepository;
    private int footerFontSize;
    private String convertGiftListName(String pName) {
        String result = pName;
        if (!StringUtils.isBlank(pName)) {
            result = pName.replaceAll("[-\\+ ]+", "_");
        }
        return result;
    }

    public String getGiftlistFileName(RepositoryItem pGiftList) {
        String giftListName = (String) pGiftList.getPropertyValue(EVENT_NAME);
        if (StringUtils.isBlank(giftListName)) {
            Date currentDate = Calendar.getInstance().getTime();
            String currentDateStr = mDateFormat.format(currentDate);
            giftListName = "MaterialList_" + currentDateStr;
        } else if (giftListName.length() < 3) {
            String giftListDescription = (String) pGiftList.getPropertyValue(DESCRIPTION);
            giftListName = "MaterialList_" + giftListName + "_" + (StringUtils.isNotEmpty(giftListDescription) ? giftListDescription : "");
        }

        return convertGiftListName(giftListName);
    }

    public File createGiftlistFile(RepositoryItem pGiftList,RepositoryItem profile, CPSProfileTools profileTools, CPSOrderImpl currentOrder) {
        File file = null;
        String fileName = getGiftlistFileName(pGiftList);
        HSSFWorkbook workbook = createWorkbook(pGiftList, fileName, (Profile) profile , profileTools, currentOrder);
        if (null != workbook) {
            try {
//                file = File.createTempFile(fileName, getFileExt(), null);
                file = new File(fileName+getFileExt());
                if (FILE_EXT_XLS.equals(getFileExt())) {
//                  Path path = Paths.get(file.getAbsolutePath());
//                  Files.write(path, workbook.getBytes());
//                	fix SM-571 Sharing material list sends corrupted file
                	FileOutputStream output = FileUtils.openOutputStream(file);
                	workbook.write(output);
                	output.close();
                } else if (FILE_EXT_CSV.equals(getFileExt())) {
                    BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
                    writeCsvFile(bw, PoiUtils.convertToCSV(workbook));
                }
            } catch (IOException e) {
                vlogError(e, "Error");
            } catch (ServletException e) {
                vlogError(e, "Error");
            }
        }
        return file;
    }

    public void writeCsvFile(BufferedWriter pBW, ArrayList<ArrayList<String>> pCsvData) throws ServletException, IOException {
        ArrayList<String> line = null;
        StringBuffer buffer = null;
        String csvLineElement = null;
        try {
            for (int i = 0; i < pCsvData.size(); i++) {
                buffer = new StringBuffer();

                line = pCsvData.get(i);
                for (int j = 0; j < line.size(); j++) {
                    if (line.size() > j) {
                        csvLineElement = line.get(j);
                        if (csvLineElement != null) {
                            buffer.append(PoiUtils.escapeEmbeddedCharacters(csvLineElement));
                        }
                    }
                    if (j < line.size() - 1) {
                        buffer.append(PoiUtils.separator);
                    }
                }

                pBW.write(buffer.toString().trim());
                if (i < pCsvData.size() - 1) {
                    pBW.newLine();
                }
            }
        } finally {
            if (pBW != null) {
                pBW.flush();
                pBW.close();
            }
        }
    }

    public HSSFWorkbook createWorkbook(RepositoryItem pGiftList, String pGiftListName,Profile profile,
			CPSProfileTools profileTools, CPSOrderImpl currentOrder) {
		HSSFWorkbook workbook = null;
		try{
			vlogDebug("createWorkbook(): Write GiftLists to spreadsheet ");
        	workbook = new HSSFWorkbook();
        	HSSFName name;
        	name = workbook.createName();
			name.setNameName("Material_List_" + pGiftListName);
			HSSFSheet worksheet = workbook.createSheet(GIFTLIST_ITEMS);
			
			HSSFPrintSetup printSetup = worksheet.getPrintSetup();
            worksheet.getPrintSetup().setFitWidth((short) 1);
            worksheet.getPrintSetup().setFitHeight((short) 0);
            worksheet.setAutobreaks(true);
            printSetup.setLandscape(true);
            HSSFCellStyle firtCellStyle = workbook.createCellStyle();
            HSSFCellStyle lastCellStyle = workbook.createCellStyle();
            HSSFCellStyle cellStyle = workbook.createCellStyle();
            HSSFCellStyle emptyFootercellStyle = workbook.createCellStyle();
            HSSFCellStyle firstFootercellStyle = workbook.createCellStyle();
            HSSFCellStyle headerCellStyle = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            HSSFFooter footer = worksheet.getFooter();
            HSSFHeader header = worksheet.getHeader();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            HSSFDataFormat df = workbook.createDataFormat();
            cellStyle.setDataFormat(df.getFormat("$#,##0.00"));
            headerCellStyle.setFont(font);
            headerCellStyle.setBorderBottom(BorderFormatting.BORDER_THIN);
            headerCellStyle.setBorderTop(BorderFormatting.BORDER_THIN);
            headerCellStyle.setBorderLeft(BorderFormatting.BORDER_THIN);
            headerCellStyle.setBorderRight(BorderFormatting.BORDER_THIN);
            headerCellStyle.setDataFormat(df.getFormat("$#,##0.00"));
            headerCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            headerCellStyle.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());
            headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            lastCellStyle.setDataFormat(df.getFormat("$#,##0.00"));
            
            emptyFootercellStyle.setBorderBottom(BorderFormatting.BORDER_THIN);
            emptyFootercellStyle.setBorderLeft(BorderFormatting.BORDER_DOTTED);
            emptyFootercellStyle.setBorderRight(BorderFormatting.BORDER_DOTTED);
            firstFootercellStyle.setBorderLeft(BorderFormatting.BORDER_THIN);
            firstFootercellStyle.setBorderBottom(BorderFormatting.BORDER_THIN);
            firtCellStyle.setBorderLeft(BorderFormatting.BORDER_THIN);
            firtCellStyle.setBorderBottom(BorderFormatting.BORDER_DOTTED);
            firtCellStyle.setAlignment(CellStyle.ALIGN_LEFT);
            cellStyle.setBorderBottom(BorderFormatting.BORDER_DOTTED);
            cellStyle.setBorderLeft(BorderFormatting.BORDER_DOTTED);
            cellStyle.setBorderRight(BorderFormatting.BORDER_DOTTED);
            lastCellStyle.setBorderRight(BorderFormatting.BORDER_THIN);
            lastCellStyle.setBorderBottom(BorderFormatting.BORDER_DOTTED);
            HSSFRow titleRow = worksheet.createRow(0);
            HSSFCell titleCell1 = titleRow.createCell(0);
            HSSFCell titleCell2 = titleRow.createCell(1);
            HSSFCell titleCell3 = titleRow.createCell(2);
            HSSFCell titleCell4 = titleRow.createCell(3);
            HSSFCell titleCell5 = titleRow.createCell(4);	
                  
            titleCell1.setCellValue(QUANTITY);
            titleCell2.setCellValue(HEADER_DISPLAY_NAME);
            titleCell3.setCellValue(HEADER_DESCRIPTION);
            titleCell4.setCellValue(UNIT_PRICE);
            titleCell5.setCellValue(EXTENDED_AMOUNT);
            
            titleCell1.setCellStyle(headerCellStyle);
            titleCell2.setCellStyle(headerCellStyle);
            titleCell3.setCellStyle(headerCellStyle);
            titleCell4.setCellStyle(headerCellStyle);
            titleCell5.setCellStyle(headerCellStyle);
            
            if(pGiftList !=null){
            	List giftlistItems = (List) pGiftList.getPropertyValue(GIFTLIST_ITEMS);
            vlogDebug("giftlistItems and length : {0} : {1}", giftlistItems, giftlistItems.size());
            int count = 1;
            BigDecimal total = new BigDecimal(0);
            for (Object oGiftItem : giftlistItems) {
                HSSFRow itemRow = worksheet.createRow(count++);
                HSSFCell itemCell1 = itemRow.createCell(0);
                HSSFCell itemCell2 = itemRow.createCell(1);
                HSSFCell itemCell3 = itemRow.createCell(2);
                HSSFCell itemCell4 = itemRow.createCell(3);
                HSSFCell itemCell5 = itemRow.createCell(4);
                itemCell1.setCellValue(getPropertyValue(oGiftItem, QUANTITY_DESIRED));
                itemCell1.setCellStyle(firtCellStyle);
                itemCell2.setCellValue(getPropertyValue(oGiftItem, DISPLAY_NAME));
                itemCell2.setCellStyle(cellStyle);
                itemCell3.setCellValue(getPropertyValue(oGiftItem, DESCRIPTION));
                itemCell3.setCellStyle(cellStyle);
                String productId=getPropertyValue(oGiftItem, PRODUCT_ID);
                //get product price
                double unitPrice = getUnitPrice(profile, profileTools, currentOrder, productId);
                BigDecimal bdUnitPrice = BigDecimal.valueOf(unitPrice); //convert double to BigDecimal
                BigDecimal qty = new BigDecimal(getPropertyValue(oGiftItem, QUANTITY_DESIRED));
                //multiply qty and price
                BigDecimal amount = qty.multiply(bdUnitPrice);
                amount = amount.setScale(2,RoundingMode.HALF_UP);
                bdUnitPrice = bdUnitPrice.setScale(2, RoundingMode.HALF_UP);
                itemCell4.setCellValue(bdUnitPrice.doubleValue());
                itemCell4.setCellStyle(cellStyle);
                itemCell5.setCellValue(amount.doubleValue());
                itemCell5.setCellStyle(lastCellStyle);
                total = total.add(amount);                
                vlogDebug("ItemRow display name:: {0}", getPropertyValue(oGiftItem, DISPLAY_NAME));                
            }
	            HSSFRow itemRow = worksheet.createRow(count);
	            HSSFCell itemCell1 = itemRow.createCell(0);
	            HSSFCell itemCell2 = itemRow.createCell(1);
	            HSSFCell itemCell3 = itemRow.createCell(2);
	            itemCell1.setCellStyle(firstFootercellStyle);
	            itemCell2.setCellStyle(emptyFootercellStyle);
	            itemCell3.setCellStyle(emptyFootercellStyle);
	            HSSFCell totalCellContent = itemRow.createCell(3);
	            totalCellContent.setCellValue("Total");
	            totalCellContent.setCellStyle(headerCellStyle);
	            HSSFCell cellTotal = itemRow.createCell(4);
	            cellTotal.setCellValue(total.setScale(2, RoundingMode.HALF_UP).doubleValue());
	            cellTotal.setCellStyle(headerCellStyle);
	            autoSizeColumn(worksheet);
			
            }
        	
		} catch (Exception e) {
			vlogError("Error: {0}", e.getMessage());
        }
		
		
		
//        try {
//            vlogDebug("createWorkbook(): Write GiftLists to spreadsheet ");
//            workbook = new HSSFWorkbook();
//            HSSFName name;
//
//            name = workbook.createName();
//            name.setNameName("Material_List_" + pGiftListName);
//            HSSFSheet worksheet = workbook.createSheet(GIFTLIST_ITEMS);
//            HSSFCellStyle cellStyle = workbook.createCellStyle();
//            HSSFDataFormat df = workbook.createDataFormat();
//            cellStyle.setDataFormat(df.getFormat("$#,##0.00"));
//            
//            HSSFRow titleRow = worksheet.createRow(0);
//            HSSFCell titleCell1 = titleRow.createCell(0);
//            HSSFCell titleCell2 = titleRow.createCell(1);
//            HSSFCell titleCell3 = titleRow.createCell(2);
//            HSSFCell titleCell4 = titleRow.createCell(3);
//            HSSFCell titleCell5 = titleRow.createCell(4);
//            HSSFCell titleCell6 = titleRow.createCell(5);
//            HSSFCell titleCell7 = titleRow.createCell(6);
//            titleCell1.setCellValue(DESCRIPTION);
//            titleCell2.setCellValue(QUANTITY_PURCHASED);
//            titleCell3.setCellValue(DISPLAY_NAME);
//            titleCell4.setCellValue(SITE_ID);
//            titleCell5.setCellValue(QUANTITY_DESIRED);
//            titleCell6.setCellValue(CATALOG_REF_ID);
//            titleCell7.setCellValue(PRODUCT_ID);
//                        
//            List giftlistItems = (List) pGiftList.getPropertyValue(GIFTLIST_ITEMS);
//            System.out.println("giftlistItems length ::  " + giftlistItems.size());
//            System.out.println("giftlistItems :: " + giftlistItems);
//            int count = 1;
//            for (Object oGiftItem : giftlistItems) {
//                HSSFRow itemRow = worksheet.createRow(count++);
//                HSSFCell itemCell1 = itemRow.createCell(0);
//                HSSFCell itemCell2 = itemRow.createCell(1);
//                HSSFCell itemCell3 = itemRow.createCell(2);
//                HSSFCell itemCell4 = itemRow.createCell(3);
//                HSSFCell itemCell5 = itemRow.createCell(4);
//                HSSFCell itemCell6 = itemRow.createCell(5);
//                HSSFCell itemCell7 = itemRow.createCell(6);
//                itemCell1.setCellValue(getPropertyValue(oGiftItem, DESCRIPTION));
//                itemCell2.setCellValue(getPropertyValue(oGiftItem, QUANTITY_PURCHASED));
//                itemCell3.setCellValue(getPropertyValue(oGiftItem, DISPLAY_NAME));
//                itemCell4.setCellValue(getPropertyValue(oGiftItem, SITE_ID));
//                itemCell5.setCellValue(getPropertyValue(oGiftItem, QUANTITY_DESIRED));
//                itemCell6.setCellValue(getPropertyValue(oGiftItem, CATALOG_REF_ID));
//                itemCell7.setCellValue(getPropertyValue(oGiftItem, PRODUCT_ID));
//                System.out.println("itemRow :: " + itemRow);
//                System.out.println("itemRow display name:: " + getPropertyValue(oGiftItem, DISPLAY_NAME));
//            }
//
//            worksheet.autoSizeColumn(0);
//            worksheet.autoSizeColumn(1);
//            worksheet.autoSizeColumn(2);
//            worksheet.autoSizeColumn(3);
//            worksheet.autoSizeColumn(4);
//            worksheet.autoSizeColumn(5);
//            worksheet.autoSizeColumn(6);
//
//        } catch (Exception e) {
//			vlogError("Error: {0}", e.getMessage());
//        }

        return workbook;
    }
    
    /**
     * 
     * @param giftList
     * @param giftListName
     * @param profile
     * @param profileTools
     * @param currentOrder
     * @return
     */
	public HSSFWorkbook createWorkbook(RepositoryItem giftList, String giftListName, Profile profile,
			CPSProfileTools profileTools, CPSOrderImpl currentOrder, String priceDisclaimerMessage,List<String> cIds) {
        HSSFWorkbook workbook = null;
        try {
            vlogDebug("createWorkbook(): Write GiftLists to spreadsheet ");
            workbook = new HSSFWorkbook();
            HSSFName name;
            HSSFSheet worksheet =null;
            name = workbook.createName();
            if(giftListName == null){
            	name.setNameName(CPSConstants.QUOTE_ACKNOWLEDGEMENT);
            	worksheet=  workbook.createSheet(CART_ITEMS);

            }else{
            	name.setNameName("Material_List_" + giftListName);	
               worksheet = workbook.createSheet(GIFTLIST_ITEMS);

            }
            HSSFPrintSetup printSetup = worksheet.getPrintSetup();
            worksheet.getPrintSetup().setFitWidth((short) 1);
            worksheet.getPrintSetup().setFitHeight((short) 0);
            worksheet.setAutobreaks(true);
            printSetup.setLandscape(true);
            HSSFCellStyle firtCellStyle = workbook.createCellStyle();
            HSSFCellStyle lastCellStyle = workbook.createCellStyle();
            HSSFCellStyle cellStyle = workbook.createCellStyle();
            HSSFCellStyle emptyFootercellStyle = workbook.createCellStyle();
            HSSFCellStyle firstFootercellStyle = workbook.createCellStyle();
            HSSFCellStyle headerCellStyle = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            HSSFFooter footer = worksheet.getFooter();
            HSSFHeader header = worksheet.getHeader();
            
            header.setLeft(HeaderFooter.font(ARIAL, BOLD)+
                    HeaderFooter.fontSize((short) 16)+COLUMBIA_PIPE_SUPPLY_CO);
            header.setRight(HeaderFooter.font(ARIAL, BOLD)+
                    HeaderFooter.fontSize((short) 12)+QUOTE_ACKNOWLEDGEMENT_HEADER);
            footer.setLeft("");
            footer.setCenter(HeaderFooter.font(ARIAL, REGULAR)+
                    HeaderFooter.fontSize((short) getFooterFontSize())+priceDisclaimerMessage);
            footer.setRight(HeaderFooter.font(ARIAL, REGULAR)+
                    HeaderFooter.fontSize((short) getFooterFontSize())+HeaderFooter.date() +" "+ HeaderFooter.time());
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
           // cellStyle.setFont(font);
           // firtCellStyle.setFont(font);
            HSSFDataFormat df = workbook.createDataFormat();
            cellStyle.setDataFormat(df.getFormat("$#,##0.00"));
            headerCellStyle.setFont(font);
            headerCellStyle.setBorderBottom(BorderFormatting.BORDER_THIN);
            headerCellStyle.setBorderTop(BorderFormatting.BORDER_THIN);
            headerCellStyle.setBorderLeft(BorderFormatting.BORDER_THIN);
            headerCellStyle.setBorderRight(BorderFormatting.BORDER_THIN);
            headerCellStyle.setDataFormat(df.getFormat("$#,##0.00"));
            headerCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            headerCellStyle.setFillBackgroundColor(IndexedColors.YELLOW.getIndex());
            headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            lastCellStyle.setDataFormat(df.getFormat("$#,##0.00"));
            
            emptyFootercellStyle.setBorderBottom(BorderFormatting.BORDER_THIN);
            emptyFootercellStyle.setBorderLeft(BorderFormatting.BORDER_DOTTED);
            emptyFootercellStyle.setBorderRight(BorderFormatting.BORDER_DOTTED);
            firstFootercellStyle.setBorderLeft(BorderFormatting.BORDER_THIN);
            firstFootercellStyle.setBorderBottom(BorderFormatting.BORDER_THIN);
            firtCellStyle.setBorderLeft(BorderFormatting.BORDER_THIN);
            firtCellStyle.setBorderBottom(BorderFormatting.BORDER_DOTTED);
            firtCellStyle.setAlignment(CellStyle.ALIGN_LEFT);
            cellStyle.setBorderBottom(BorderFormatting.BORDER_DOTTED);
            cellStyle.setBorderLeft(BorderFormatting.BORDER_DOTTED);
            cellStyle.setBorderRight(BorderFormatting.BORDER_DOTTED);
            lastCellStyle.setBorderRight(BorderFormatting.BORDER_THIN);
            lastCellStyle.setBorderBottom(BorderFormatting.BORDER_DOTTED);
            HSSFRow titleRow = worksheet.createRow(0);
            HSSFCell titleCell1 = titleRow.createCell(0);
            HSSFCell titleCell2 = titleRow.createCell(1);
            HSSFCell titleCell3 = titleRow.createCell(2);
            HSSFCell titleCell4 = titleRow.createCell(3);
            HSSFCell titleCell5 = titleRow.createCell(4);	
                  
            titleCell1.setCellValue(QUANTITY);
            titleCell2.setCellValue(HEADER_DISPLAY_NAME);
            titleCell3.setCellValue(HEADER_DESCRIPTION);
            titleCell4.setCellValue(UNIT_PRICE);
            titleCell5.setCellValue(EXTENDED_AMOUNT);
            
            titleCell1.setCellStyle(headerCellStyle);
            titleCell2.setCellStyle(headerCellStyle);
            titleCell3.setCellStyle(headerCellStyle);
            titleCell4.setCellStyle(headerCellStyle);
            titleCell5.setCellStyle(headerCellStyle);
             
            if(giftList !=null){
            	List giftlistItems = (List) giftList.getPropertyValue(GIFTLIST_ITEMS);
            vlogDebug("giftlistItems and length : {0} : {1}", giftlistItems, giftlistItems.size());
            int count = 1;
            BigDecimal total = new BigDecimal(0);
            for (Object oGiftItem : giftlistItems) {
                HSSFRow itemRow = worksheet.createRow(count++);
                HSSFCell itemCell1 = itemRow.createCell(0);
                HSSFCell itemCell2 = itemRow.createCell(1);
                HSSFCell itemCell3 = itemRow.createCell(2);
                HSSFCell itemCell4 = itemRow.createCell(3);
                HSSFCell itemCell5 = itemRow.createCell(4);
                itemCell1.setCellValue(getPropertyValue(oGiftItem, QUANTITY_DESIRED));
                itemCell1.setCellStyle(firtCellStyle);
                itemCell2.setCellValue(getPropertyValue(oGiftItem, DISPLAY_NAME));
                itemCell2.setCellStyle(cellStyle);
                itemCell3.setCellValue(getPropertyValue(oGiftItem, DESCRIPTION));
                itemCell3.setCellStyle(cellStyle);
                String productId=getPropertyValue(oGiftItem, PRODUCT_ID);
                //get product price
                double unitPrice = getUnitPrice(profile, profileTools, currentOrder, productId);
                BigDecimal bdUnitPrice = BigDecimal.valueOf(unitPrice); //convert double to BigDecimal
                BigDecimal qty = new BigDecimal(getPropertyValue(oGiftItem, QUANTITY_DESIRED));
                //multiply qty and price
                BigDecimal amount = qty.multiply(bdUnitPrice);
                amount = amount.setScale(2,RoundingMode.HALF_UP);
                bdUnitPrice = bdUnitPrice.setScale(2, RoundingMode.HALF_UP);
                itemCell4.setCellValue(bdUnitPrice.doubleValue());
                itemCell4.setCellStyle(cellStyle);
                itemCell5.setCellValue(amount.doubleValue());
                itemCell5.setCellStyle(lastCellStyle);
                total = total.add(amount);                
                vlogDebug("ItemRow display name:: {0}", getPropertyValue(oGiftItem, DISPLAY_NAME));                
            }
            
            HSSFRow itemRow = worksheet.createRow(count);
            HSSFCell itemCell1 = itemRow.createCell(0);
            HSSFCell itemCell2 = itemRow.createCell(1);
            HSSFCell itemCell3 = itemRow.createCell(2);
            itemCell1.setCellStyle(firstFootercellStyle);
            itemCell2.setCellStyle(emptyFootercellStyle);
            itemCell3.setCellStyle(emptyFootercellStyle);
            HSSFCell totalCellContent = itemRow.createCell(3);
            totalCellContent.setCellValue("Total");
            totalCellContent.setCellStyle(headerCellStyle);
            HSSFCell cellTotal = itemRow.createCell(4);
            cellTotal.setCellValue(total.setScale(2, RoundingMode.HALF_UP).doubleValue());
            cellTotal.setCellStyle(headerCellStyle);
            autoSizeColumn(worksheet);
            }else{
            	//List<CommerceItem> cItems = currentOrder.getCommerceItems();
            	vlogDebug("giftlistItems and length : {0} : {1}", cIds, cIds.size());
                int count = 1;
                BigDecimal total = new BigDecimal(0);
                for (String cId : cIds) {
                	CommerceItem cItem=currentOrder.getCommerceItem(cId);
                    HSSFRow itemRow = worksheet.createRow(count++);
                    HSSFCell itemCell1 = itemRow.createCell(0);
                    HSSFCell itemCell2 = itemRow.createCell(1);
                    HSSFCell itemCell3 = itemRow.createCell(2);
                    HSSFCell itemCell4 = itemRow.createCell(3);
                    HSSFCell itemCell5 = itemRow.createCell(4);
                    itemCell1.setCellValue(cItem.getQuantity());
                    itemCell1.setCellStyle(firtCellStyle);
                    itemCell2.setCellValue(getPropertyValue(cItem, DISPLAY_NAME));
                    itemCell2.setCellStyle(cellStyle);
                    itemCell3.setCellValue(getPropertyValue(cItem, DESCRIPTION));
                    itemCell3.setCellStyle(cellStyle);
                    //get product price
                    double unitPrice = cItem.getPriceInfo().getListPrice();
                    BigDecimal bdUnitPrice = BigDecimal.valueOf(unitPrice); //convert double to BigDecimal
                    BigDecimal qty = new BigDecimal(cItem.getQuantity());
                    //multiply qty and price
                    BigDecimal amount = qty.multiply(bdUnitPrice);
                    amount = amount.setScale(2,RoundingMode.HALF_UP);
                    bdUnitPrice = bdUnitPrice.setScale(2, RoundingMode.HALF_UP);
                    itemCell4.setCellValue(bdUnitPrice.doubleValue());
                    itemCell4.setCellStyle(cellStyle);
                    itemCell5.setCellValue(amount.doubleValue());
                    itemCell5.setCellStyle(lastCellStyle);
                    total = total.add(amount);                
                    vlogDebug("ItemRow display name:: {0}", getPropertyValue(cItem, DISPLAY_NAME));                
                }
                
                HSSFRow itemRow = worksheet.createRow(count);
                HSSFCell itemCell1 = itemRow.createCell(0);
                HSSFCell itemCell2 = itemRow.createCell(1);
                HSSFCell itemCell3 = itemRow.createCell(2);
                itemCell1.setCellStyle(firstFootercellStyle);
                itemCell2.setCellStyle(emptyFootercellStyle);
                itemCell3.setCellStyle(emptyFootercellStyle);
                HSSFCell totalCellContent = itemRow.createCell(3);
                totalCellContent.setCellValue("Total");
                totalCellContent.setCellStyle(headerCellStyle);
                HSSFCell cellTotal = itemRow.createCell(4);
                cellTotal.setCellValue(total.setScale(2, RoundingMode.HALF_UP).doubleValue());
                cellTotal.setCellStyle(headerCellStyle);
                autoSizeColumn(worksheet);
            }
            
        } catch (Exception ex) {
			ex.printStackTrace();
        }
                
        return workbook;
    }

	/**
	 * @param worksheet
	 */
	private void autoSizeColumn(HSSFSheet worksheet) {
		int noOfColumns = worksheet.getRow(0).getPhysicalNumberOfCells();
		for (int i = 0; i < noOfColumns; i++) {
			worksheet.autoSizeColumn(i);
		}
	}

	/**
	 * @param profile
	 * @param profileTools
	 * @param currentOrder
	 * @param productId
	 * @return
	 */
	private double getUnitPrice(Profile profile, CPSProfileTools profileTools, CPSOrderImpl currentOrder,
			String productId) {
		double unitPrice = 0.0;
		if (externalProductService.isPricingWebserviceEnable()) {
			String priceMatrix = profileTools.getPriceMatrixLevelForGivenProfileAddress(profile, currentOrder);
			if (priceMatrix != null) {
				// the profile qualifies for fetching prices from the ATG PriceList Repository.
				String skuId = productId; // in CPS, both the productId and SKUid have one-to-one mapping
				unitPrice = getPriceListManager().getOnePrice(productId, skuId, profile, priceMatrix, currentOrder);
			} else {
				unitPrice = externalProductService.getCustomerItemPrice(productId, profile, currentOrder);
			}
		}
		return unitPrice;
	}

	private String getPropertyValue(Object oGiftItem, String pPropertyName) throws RepositoryException {
        String propertyValue = null;
        if (oGiftItem instanceof RepositoryItem) {
            RepositoryItem giftItem = (RepositoryItem) oGiftItem;
            propertyValue = String.valueOf(giftItem.getPropertyValue(pPropertyName));
        }
        if(oGiftItem instanceof CommerceItem){
        	CommerceItem cItem = (CommerceItem) oGiftItem;
        	RepositoryItem item = getProductRepository().getItem(cItem.getCatalogRefId(),"product");
            propertyValue = String.valueOf(item.getPropertyValue(pPropertyName));
        }
        if (StringUtils.isBlank(propertyValue)) {
            propertyValue = "";
        }
        return propertyValue;
    }

    public RepositoryItem copyGiftList(String pSourceGiftListId, String pUserId) throws RepositoryException, CommerceException {

        String giftlistId = createGiftlist(pUserId, false, getGiftlistEventName(pSourceGiftListId), getGiftlistEventDate(pSourceGiftListId),
                        getGiftlistEventType(pSourceGiftListId), getGiftlistDescription(pSourceGiftListId), getGiftlistComments(pSourceGiftListId), null, null,
                        getGiftlistSite(pSourceGiftListId));
        RepositoryItem sourceGiftList = getGiftlist(pSourceGiftListId);
        MutableRepositoryItem giftlist = (MutableRepositoryItem) getGiftlist(giftlistId);
        giftlist.setPropertyValue(ORG, sourceGiftList.getPropertyValue(ORG));
        copyGiftListItems(pSourceGiftListId, giftlistId);
        return giftlist;

    }

    public void copyGiftListItems(String pSourceGiftListId, String pDestinationGiftListId) throws RepositoryException, CommerceException {

        List<RepositoryItem> items = getGiftlistItems(pSourceGiftListId);
        for (RepositoryItem item : items) {
            copyGiftListItem(item.getRepositoryId(), pDestinationGiftListId);
        }

    }

    private void copyGiftListItem(String pGiftItemId, String pDestinationGiftListId) throws CommerceException, RepositoryException {

        addCatalogItemToGiftlist(getGiftlistItemCatalogRefId(pGiftItemId), getGiftlistItemProductId(pGiftItemId), pDestinationGiftListId,
                        getGiftlistItemSite(pGiftItemId), getGiftlistItemQuantityDesired(pGiftItemId));

    }

    public boolean isHaveAccessToGiftlist(String pGiftlistId, Profile pProfile) {
        boolean result = false;
        if (StringUtils.isNotBlank(pGiftlistId) && pProfile != null) {
            List<RepositoryItem> giftlists = (List<RepositoryItem>) pProfile.getPropertyValue(CPSConstants.GIFTLISTS);
            if (giftlists != null) {
                for (RepositoryItem giftlist : giftlists) {
                    if (pGiftlistId.equals(giftlist.getRepositoryId())) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    public void validatePreCreateListAddItem(DynamoHttpServletRequest pRequest, RepositoryItem pProfile, GiftlistFormHandler pFormHandler)
                    throws ServletException, IOException {
        vlogDebug("validatePreCreateListAddItem.start");
        boolean error = false;

        RepositoryItem organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(pProfile);
        if (organization == null) {
            error = true;
        }

        if (!error) {
            getValidator().validateCreateListAddItem(pFormHandler, pRequest);
        } else {
            CPSMessageUtils.addFormException(pFormHandler, ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_REORDER_MISSING_ORG, pRequest.getLocale(), null);
        }

        vlogDebug("validatePreCreateListAddItem.end");
    }

    public void setIgnoreCaseItemsQuery(boolean pIgnoreCaseItemsQuery) {
        mIgnoreCaseItemsQuery = pIgnoreCaseItemsQuery;
    }

    public boolean isIgnoreCaseItemsQuery() {
        return mIgnoreCaseItemsQuery;
    }

    public MaterialListValidator getValidator() {
        return mValidator;
    }

    public void setValidator(MaterialListValidator pValidator) {
        mValidator = pValidator;
    }

    /**
     * return file extension, which will be used to filter files
     *
     * @return file extension, which will be used to filter files
     */
    public String getFileExt() {
        return mFileExt;
    }

    /**
     * init file extension, which will be used to filter files
     *
     * @param pFileExt
     *            file extension, which will be used to filter files
     */
    public void setFileExt(String pFileExt) {
        mFileExt = pFileExt;
    }

	/**
	 * @return the externalProductService
	 */
	public CPSExternalProductService getExternalProductService() {
		return externalProductService;
	}

	/**
	 * @param externalProductService the externalProductService to set
	 */
	public void setExternalProductService(CPSExternalProductService externalProductService) {
		this.externalProductService = externalProductService;
	}

	/**
	 * @return the priceListManager
	 */
	public CPSPriceListManager getPriceListManager() {
		return priceListManager;
	}

	/**
	 * @param priceListManager the priceListManager to set
	 */
	public void setPriceListManager(CPSPriceListManager priceListManager) {
		this.priceListManager = priceListManager;
	}

	/**
	 * @return the multiPriceService
	 */
	public CPSMultiPriceService getMultiPriceService() {
		return multiPriceService;
	}

	/**
	 * @param multiPriceService the multiPriceService to set
	 */
	public void setMultiPriceService(CPSMultiPriceService multiPriceService) {
		this.multiPriceService = multiPriceService;
	}

	public Repository getProductRepository() {
		return productRepository;
	}

	public void setProductRepository(Repository productRepository) {
		this.productRepository = productRepository;
	}

	public int getFooterFontSize() {
		return footerFontSize;
	}

	public void setFooterFontSize(int footerFontSize) {
		this.footerFontSize = footerFontSize;
	}
		

}