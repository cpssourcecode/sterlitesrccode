package com.cps.commerce.gifts;

import static com.cps.util.CPSConstants.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import atg.commerce.gifts.GiftlistFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.repository.*;

import com.cps.service.DuplicatesMatchesService;
import com.cps.service.SixLengthIdService;

import com.cps.util.*;
import vsg.constants.VSGConstants;
import vsg.util.VSGFormUtils;
import atg.adapter.gsa.ChangeAwareList;
import atg.adapter.gsa.ChangeAwareSet;
import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistManager;
import atg.commerce.gifts.InvalidDateException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.purchase.CPSPriceAvailability;
import com.cps.commerce.order.purchase.CPSPurchaseProcessHelper;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.common.handler.DuplicatesMatchesHandlerInterface;
import com.cps.csr.util.CPSCSRLoggingManager;
import com.cps.email.CommonEmailSender;
import com.cps.util.security.CriptoUtils;

public class CPSGiftlistFormHandler extends GiftlistFormHandler implements DuplicatesMatchesHandlerInterface {
	private static final String PARAM_X_REQUESTED_WITH = "X-Requested-With";
	private static final String PARAM_XML_HTTP_REQUEST = "XMLHttpRequest";

	private String removeListId;
	private String cs;
	private String org;
	private String emailList;
	private String emailNotes;
	private String selectedLists;
	private String addedCommerceItems;
	private String itemIdsList;
	private String quantities;
	private String indexList;
	private String mUserId;

	private String addItemToPASuccessURL;
	private String addItemToPAErrorURL;

	private CPSSessionBean sessionBean;
	private CPSCSRLoggingManager csrLoggingManager;
	private CommonEmailSender emailSender;

	private PurchaseProcessHelper purchaseProcessHelper;

	/**
	 * Duplicates Matches Service
	 */
	private DuplicatesMatchesService mDuplicatesMatchesService;
	private String skuIdsList;
	private String qtyList;
	private ArrayList<String> quickOrderFinalSkus = new ArrayList<String>();
	private ArrayList<Integer> quickOrderFinalQuantities = new ArrayList<Integer>();
	private HashMap<String, HashMap<Integer, ArrayList<String>>> mDuplicates = null;
	private String addFromErrorModal;
	private String mAddItemsToMaterialListSuccessUrl;
	private String mAddItemsToMaterialListErrorUrl;
	private String mAddItemsToMaterialListInvalidUrl;
	private boolean mGiftlistCheckAvailability = false;
	private String mDisplayGiftlistBaseUrl;

	/**
	 * Six Length Ids ervice
	 */
	private SixLengthIdService mSixLengthIdService;

	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	/**
	 * repository
	 */
//	private Repository mRepository;
	public boolean handleSaveGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {
		vlogDebug("handleSaveGiftlist.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleSaveGiftlist";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				String pProfileId = (String) getProfile().getPropertyValue("id");
				try {
					((CPSGiftlistManager) getGiftlistManager()).validatePreCreateListAddItem(pRequest, getProfile(), this);
					vlogDebug("FORM ERRORS=" + getFormError());
					if (!getFormError()) {
						String giftlistId = saveGiftlist(pProfileId);
						setGiftlistId(giftlistId);
					}
				} catch (InvalidDateException ide) {
					processException(ide, "invalidEventDate", pRequest, pResponse);
				} catch (CommerceException ce) {
					processException(ce, "errorSavingGiftlist", pRequest, pResponse);
				}

				postSaveGiftlist(pRequest, pResponse);

			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		vlogDebug("handleSaveGiftlist.end");
		return false;
	}

	public void preUpdateGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		vlogDebug("preUpdateGiftlist.start");
		ChangeAwareList usersGiftlists = (ChangeAwareList) getProfile().getPropertyValue(GIFTLISTS);
		boolean error = false;
		String currentOrgId = "";

		RepositoryItem organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(getProfile());
		if (organization != null) {
			currentOrgId = organization.getRepositoryId();
		} else {
			error = true;
		}

		List<String> listNames = new ArrayList<String>();
		if (!StringUtils.isBlank(currentOrgId)) {
			for (Object listObj : usersGiftlists) {
				RepositoryItem list = (RepositoryItem) listObj;
				if (currentOrgId.equals(list.getPropertyValue(ORG)) && !list.getRepositoryId().equals(getGiftlistId())) {
					listNames.add((String) list.getPropertyValue(EVENT_NAME));
				}
			}
		}

		if (!error) {
			Map<String, Object> errors = ((CPSGiftlistTools) getGiftlistManager().getGiftlistTools()).validateUpdatedGiftlist(getEventName(), listNames);
			if (errors != null) {
				vlogDebug("Errors: " + errors.toString());
			} else {
				vlogDebug("No errors");
			}
			if (errors != null && !errors.isEmpty()) {
				checkErrorMessages(errors, pRequest);
			}
		} else {
			createError(CPSErrorCodes.ERR_REORDER_MISSING_ORG, null, pRequest);
		}

		vlogDebug("preUpdateGiftlist.end");

	}

	public void postSaveGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("postSaveGiftlist.start");
		if (!getFormError()) {
			vlogDebug("giftlistId: " + getGiftlistId());
			setCustomProperties();
			//setSaveGiftlistSuccessURL("/account/reorder-detail.jsp?giftlistId=" + getGiftlistId());

		}
		vlogDebug("new success url: " + getSaveGiftlistSuccessURL());

		//try {
		if (!getFormError()) {
			//addAjaxSuccessResponse(pResponse);
			redirectOrForward(pRequest, pResponse, "/account/material-lists.jsp");
		} else {
			//logDebug("ADDING AJAX ERROR RESPONSE");
			//logDebug("FORM ERRORS = "+getFormExceptions());
			//setSaveGiftlistSuccessURL("/account/material-lists.jsp?matmodal=true");
			vlogDebug("new error url: /account/material-lists.jsp?matmodal=true");
			redirectOrForward(pRequest, pResponse, "/account/material-lists.jsp?matmodal=true");

			//addAjaxSuccessResponse(pResponse);
			//CPSFormUtils.addAjaxErrorResponse(this, pResponse);
		}
		//} catch (JSONException e) {
		//	logError(e);
		//}

		if (getProfile().getPropertyValue(CSR_USER_ID) != null) {
			// CSR Impersonating User, log action
			getCsrLoggingManager().logAction(1, 1, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
					(String) getProfile().getPropertyValue(CSR_USER_ID), getProfile().getRepositoryId(), "CSR created new reorder list for impersonated user.");
		} else if ((Integer) getProfile().getPropertyValue(USER_TYPE) == 3) {
			// Sales rep
			getCsrLoggingManager().logAction(1, 2, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
					getProfile().getRepositoryId(), null, "Sales Rep created new reorder list under selected org.");
		}

		vlogDebug("postSaveGiftlist.end");
	}

	private void setCustomProperties() {
		try {
			MutableRepositoryItem newGiftlist = (MutableRepositoryItem) getGiftlistManager().getGiftlist(getGiftlistId());
			newGiftlist.setPropertyValue(CS, getCs());

			String currentOrgId = "";
			RepositoryItem organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(getProfile());
			if (organization != null) {
				currentOrgId = organization.getRepositoryId();
			}
			newGiftlist.setPropertyValue(ORG, currentOrgId);

		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
	}

	public boolean handleDeleteList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException, RepositoryException {
		vlogDebug("handleDeleteList.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleDeleteList";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				preDeleteList(pRequest, pResponse);

				if (!getFormError()) {
					deleteList(pRequest, pResponse);
					if (!getFormError()) {
						postDeleteList(pRequest, pResponse);
					}
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		vlogDebug("handleDeleteList.end");
		return false;
	}

	private void preDeleteList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (StringUtils.isBlank(getRemoveListId())) {
			createError(CPSErrorCodes.ERR_REORDER_DELETE, "", pRequest);
		}
	}

	private void deleteList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("List id to delete: " + getRemoveListId());
		try {
			if (!StringUtils.isBlank(getRemoveListId())) {
				getGiftlistManager().removeGiftlist((String) getProfile().getPropertyValue(ID), getRemoveListId());
			}
		} catch (Exception e) {
			createError(CPSErrorCodes.ERR_REORDER_DELETE, "", pRequest);
			logError(e);
		}
	}

	private void postDeleteList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException {

		if (getProfile().getPropertyValue(CSR_USER_ID) != null) {
			// CSR Impersonating User, log action
			getCsrLoggingManager().logAction(1, 1, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
					(String) getProfile().getPropertyValue(CSR_USER_ID), getProfile().getRepositoryId(), "CSR deleted reorder list for impersonated user.");
		} else if ((Integer) getProfile().getPropertyValue(USER_TYPE) == 3) {
			// Sales rep
			getCsrLoggingManager().logAction(1, 2, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
					getProfile().getRepositoryId(), null, "Sales Rep deleted reorder list under selected org.");
		}
		try {
			if (!getFormError()) {
				vlogDebug("no errors, return success");
				VSGFormUtils.addAjaxSuccessResponse(pResponse);
			} else {
				vlogDebug("errors, return errors for display");
				Set<String> errorProperties = new HashSet<String>();
				for (int i = 0; i < getFormExceptions().size(); i++) {
					DropletException exc = (DropletException) getFormExceptions().get(i);
					errorProperties.add(exc.getErrorCode());
				}
				VSGFormUtils.addAjaxErrorResponse(this, pResponse, errorProperties);
			}

		} catch (JSONException e) {
			logError(e);
		}
	}

	public boolean handleUpdateGiftlistItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleUpdateGiftlistItems";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				boolean result = super.handleUpdateGiftlistItems(pRequest, pResponse);
				if (VSGFormUtils.isAjaxRequest(pRequest)) {
					try {
						if (!getFormError()) {
							addAjaxSuccessResponse(pResponse);
						} else {
							CPSFormUtils.addAjaxErrorResponse(this, pResponse);
						}
					} catch (JSONException e) {
						logError(e);
					}
					return false;
				} else {
					return result;
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}

	public void preUpdateGiftlistItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("Pre update giftlist item");
		if (!StringUtils.isBlank(getRemoveGiftitemIds()[0])) {
			vlogDebug("There are items in remove: " + getRemoveGiftitemIds()[0]);
			setRemoveGiftitemIds(getRemoveGiftitemIds()[0].split(","));
			if (!StringUtils.isBlank(getUpdateGiftlistItemsSuccessURL())) {
				setUpdateGiftlistItemsSuccessURL(getUpdateGiftlistItemsSuccessURL().concat("&del=success"));
			}
		} else {
			vlogDebug("Not removing, that means update");
			try {
				updateGiftlist(pRequest, pResponse);
				if (!StringUtils.isBlank(getUpdateGiftlistItemsSuccessURL())) {
					setUpdateGiftlistItemsSuccessURL(getUpdateGiftlistItemsSuccessURL().concat("&upd=success"));
				}
			} catch (InvalidDateException e) {
				vlogError(e, "Error");
			} catch (CommerceException e) {
				vlogError(e, "Error");
			}
		}
	}

	protected void updateGiftlistItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {
		vlogDebug("Hitting this update");
		GiftlistManager mgr = getGiftlistManager();
		String pGiftlistId = getGiftlistId();
		List items = mgr.getGiftlistItems(pGiftlistId);
		if (items == null)
			return;
		try {
			for (int i = 0; i < items.size(); i++) {
				RepositoryItem item = (RepositoryItem) items.get(i);
				String id = (String) item.getPropertyValue("id");

				long quantity = getQuantity(id, pRequest, pResponse);

				if (haveId(id, getRemoveGiftitemIds())) {
					vlogDebug("Remove this item: " + id);
					quantity = -1L;
				}

				if (quantity == this.QUANTITY_NOT_VALID) {
					addFormException("invalidGiftlistQuantity", pRequest, pResponse);
					setRollbackTransaction(true);
					return;
				}

				if (quantity > -1L) {
					if (mgr.getGiftlistItemQuantityDesired(id) != quantity && quantity > 0) {
						mgr.setGiftlistItemQuantityDesired(pGiftlistId, id, quantity);
					}
				} else {
					mgr.removeItemFromGiftlist(pGiftlistId, id);
				}
			}
		} catch (RepositoryException ex) {
			processException(ex, "errorUpdatingGiftlistItems", pRequest, pResponse);
			setRollbackTransaction(true);
		}
	}

	final boolean haveId(String pId, String[] pIds) {
		if ((pIds != null) && (pId != null)) {
			int length = pIds.length;
			for (int c = 0; c < length; c++) {
				if (pId.equals(pIds[c])) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean handleShareGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException {
		vlogDebug("handleShareGiftlist.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleShareGiftlist";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				preShareGiftlist(pRequest, pResponse);

				if (!getFormError()) {
					shareGiftlist(pRequest, pResponse);

					if (!getFormError()) {
						postShareGiftlist(pRequest, pResponse);
					}
				}

				try {
					if (!getFormError()) {
						vlogDebug("no errors, return success");
						VSGFormUtils.addAjaxSuccessResponse(pResponse);
					} else {
						vlogDebug("errors, return errors for display");
						Set<String> errorProperties = new HashSet<String>();
						for (int i = 0; i < getFormExceptions().size(); i++) {
							DropletException exc = (DropletException) getFormExceptions().get(i);
							errorProperties.add(exc.getErrorCode());
						}
//				VSGFormUtils.addAjaxErrorResponse(this, pResponse, errorProperties);
						VSGFormUtils.addAjaxErrorResponseGiftList(this, pResponse, errorProperties, getGiftlistId());
					}
				} catch (JSONException e) {
					logError(e);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		vlogDebug("handleShareGiftlist.end");

		return false;
	}

	private void preShareGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("preShareGiftlist.start");

		String giftlistId = getGiftlistId();
		String userId = getUserId();
		String email = getUserEmail(userId);
		setEmailList(email);
		String emailList = getEmailList();

		vlogDebug("Giftlist to share: " + giftlistId);
		vlogDebug("email to share: " + email);
		vlogDebug("Email list: " + emailList);

		String[] emails = emailList.split(",");

		RepositoryItem giftlist = null;
		try {
			giftlist = getGiftlistManager().getGiftlist(giftlistId);
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError(ce);
			}
		}
		if (giftlist == null) {
			// Shouldn't ever get this error, but better safe
			createError(CPSErrorCodes.ERR_REORDER_MISSING_GIFTLIST, null, pRequest);
			logError("Could not find giftlist from passed id: " + giftlistId);
		}
		// validate the emails entered
		Map<String, Object> errors = ((CPSGiftlistTools) getGiftlistManager().getGiftlistTools()).validateEmails(emails);
		if (errors != null) {
			vlogDebug("Errors: " + errors.toString());
		} else {
			vlogDebug("No errors");
		}
		if (errors != null && !errors.isEmpty()) {
			checkErrorMessages(errors, pRequest);
		}

		vlogDebug("preShareGiftlist.end");
	}


	private String getUserEmail(String pUserId) {
		String email = "";
		if ((!StringUtils.isBlank(pUserId)) && (null != getProfileTools().getProfileRepository())) {
			try {
				RepositoryItem user = getProfileTools().getProfileRepository().getItem(pUserId, USER);
				if (null != user) {
					email = (String) user.getPropertyValue(EMAIL);
				}

			} catch (RepositoryException re) {
				vlogError("getUserEmail() RepositoryException", re);
			}
		}
		vlogDebug("email:" + email);
		return email;
	}

	private RepositoryItem getRepositoryItem(String pRepItemId, String pRepItemType) {
		RepositoryItem repItem = null;
		try {
			if (!StringUtils.isBlank(pRepItemId)) {
				repItem = getProfileTools().getProfileRepository().getItem(pRepItemId, pRepItemType);
			}
		} catch (RepositoryException re) {
			vlogError("getRepositoryItem() RepositoryException", re);
		}
		return repItem;
	}


	private void shareGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("shareGiftlist.start");

		String giftlistId = getGiftlistId();
		String emailList = getEmailList();
		String[] emails = emailList.split(",");
		RepositoryItem giftlist = null;
		RepositoryItem newGiftlist = null;
		RepositoryItem user = getRepositoryItem(getUserId(), USER);
		CPSGiftlistManager giftlistManager = ((CPSGiftlistManager) getGiftlistManager());
		try {
			if(user != null) {
				giftlist = getGiftlistManager().getGiftlist(giftlistId);
				// If giftlist is null here we have a problem
				if (giftlist != null) {
					// create a copy and assing it to user
					newGiftlist = giftlistManager.copyGiftList(giftlistId, getUserId());
					String url = generateLink(newGiftlist.getRepositoryId(), user.getRepositoryId());
					File[] attachments = null;
					File giftlistFile = giftlistManager.createGiftlistFile(giftlist, getProfile(),
							(CPSProfileTools) getProfileTools(), (CPSOrderImpl) getShoppingCart().getCurrent());
					if (giftlistFile != null) {
						attachments = new File[]{giftlistFile};
					}
					for (String email : emails) {
						getEmailSender().sendShareMaterialListEmail(user, email, newGiftlist.getRepositoryId(), url, attachments);
					}
				} else {
					// We have a problem
					vlogError("Could not find giftlist {0} from passed id in shareGiftlist", giftlistId);
				}
			}
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError(ce);
			}
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}

		vlogDebug("shareGiftlist.end");
	}

	private String generateLink(String pGiftlistId, String pProfileId) {
		StringBuilder sb = new StringBuilder();
		try {
			String encriptedProfileId = CriptoUtils.aesEncrypt(pProfileId);
			String encryptedGiftlistId = CriptoUtils.aesEncrypt(pGiftlistId);
			sb.append(getDisplayGiftlistBaseUrl()).append(pGiftlistId);
			sb.append(VSGConstants.AMPERSAND).append(CPSConstants.PROFILE_ID_PARAM).append(VSGConstants.EQUALS_OP)
					.append(encriptedProfileId).append(VSGConstants.AMPERSAND).append(CPSConstants.WISHLIST_ID_PARAM)
					.append(VSGConstants.EQUALS_OP).append(encryptedGiftlistId);
		} catch (Exception e) {
			vlogDebug(e, "error encrypting");
		}
		return sb.toString();
	}

	private void postShareGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("postShareGiftlist.start");

		// Not sure if this is needed yet...

		vlogDebug("postShareGiftlist.end");
	}

	public boolean handleAddItemsToLists(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws InvalidDateException, CommerceException, ServletException, IOException {
		vlogDebug("handleAddItemsToLists.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleAddItemsToLists";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				preAddItemsToLists(pRequest, pResponse);
				if (!getFormError()) {
					addItemsToLists(pRequest, pResponse);
				}

				try {
					if (!getFormError()) {
						vlogDebug("no errors, return success");
						VSGFormUtils.addAjaxSuccessResponse(pResponse);
					} else {
						vlogDebug("errors, return errors for display");
						Set<String> errorProperties = new HashSet<String>();
						for (int i = 0; i < getFormExceptions().size(); i++) {
							DropletException exc = (DropletException) getFormExceptions().get(i);
							errorProperties.add(exc.getErrorCode());
						}
						VSGFormUtils.addAjaxErrorResponse(this, pResponse, errorProperties);
					}
				} catch (JSONException e) {
					logError(e);
				}

				if (getProfile().getPropertyValue(CSR_USER_ID) != null) {
					// CSR Impersonating User, log action
					getCsrLoggingManager().logAction(1, 1, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
							(String) getProfile().getPropertyValue(CSR_USER_ID), getProfile().getRepositoryId(), "CSR added items to impersonated users reorder list.");
				} else if ((Integer) getProfile().getPropertyValue(USER_TYPE) == 3) {
					// Sales rep
					getCsrLoggingManager().logAction(1, 2, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
							getProfile().getRepositoryId(), null, "Sales Rep added items to list under selected org.");
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		vlogDebug("handleAddItemsToLists.end");
		return false;
	}

	private void preAddItemsToLists(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("preAddItemsToLists.start");

		String selectedLists = getSelectedLists();
		String commerceItems = getAddedCommerceItems();
		String itemIds = getItemIdsList();
		String indexList = getIndexList();

		if (StringUtils.isBlank(selectedLists))
			selectedLists = pRequest.getParameter("selectedLists");
		if (StringUtils.isBlank(commerceItems))
			commerceItems = pRequest.getParameter("addedCommerceItems");
		if (StringUtils.isBlank(itemIds))
			itemIds = pRequest.getParameter("itemIdsList");
		if (StringUtils.isBlank(indexList))
			indexList = pRequest.getParameter("indexList");

		vlogDebug("Selected lists: " + selectedLists);
		vlogDebug("Commerce items: " + commerceItems);
		vlogDebug("Regular items: " + itemIds);
		vlogDebug("Index list: " + indexList);

		if (StringUtils.isBlank(selectedLists)) {
			createError(CPSErrorCodes.ERR_REORDER_NO_LISTS, null, pRequest);
		}
		if (StringUtils.isBlank(commerceItems) && StringUtils.isBlank(itemIds) && StringUtils.isBlank(indexList)) {
			createError(CPSErrorCodes.ERR_REORDER_NO_ITEMS, null, pRequest);
		}

		vlogDebug("preAddItemsToLists.end");
	}

	private void addItemsToLists(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, InvalidDateException, CommerceException {
		vlogDebug("addItemsToLists.start");

		String selectedLists = getSelectedLists();
		String commerceItems = getAddedCommerceItems();
		String itemIds = getItemIdsList();
		String itemQuans = getQuantities();
		String indexList = getIndexList();

		if (StringUtils.isBlank(selectedLists))
			selectedLists = pRequest.getParameter("selectedLists");
		if (StringUtils.isBlank(commerceItems))
			commerceItems = pRequest.getParameter("addedCommerceItems");
		if (StringUtils.isBlank(itemIds))
			itemIds = pRequest.getParameter("itemIdsList");
		if (StringUtils.isBlank(itemQuans))
			itemQuans = pRequest.getParameter("quantities");
		if (StringUtils.isBlank(indexList))
			indexList = pRequest.getParameter("indexList");

		String[] lists = null;
		String[] citems = null;
		String[] items = null;
		String[] qtys = null;
		String[] indexs = null;

		if (!StringUtils.isBlank(selectedLists))
			lists = selectedLists.split(",");
		if (!StringUtils.isBlank(commerceItems))
			citems = commerceItems.split(",");
		if (!StringUtils.isBlank(itemIds))
			items = itemIds.split(",");
		if (!StringUtils.isBlank(itemQuans))
			qtys = itemQuans.split(",");
		if (!StringUtils.isBlank(indexList))
			indexs = indexList.split(",");

		if (citems != null && citems.length > 0) {
			vlogDebug("Commerce items were passed in, get the product info from them.");
			boolean addItemsFromCart = false;
			String idString = "";
			String qtyString = "";
			for (int i = 0; i < citems.length; i++) {
				vlogDebug("trying to get items from order");
				if (!StringUtils.isBlank(citems[i])) {
					try {
						if (StringUtils.isBlank(idString))
							idString = ((CommerceItem) getOrder().getCommerceItem(citems[i])).getCatalogRefId();
						else
							idString += "," + ((CommerceItem) getOrder().getCommerceItem(citems[i])).getCatalogRefId();
						if (StringUtils.isBlank(qtyString))
							qtyString = "" + ((CommerceItem) getOrder().getCommerceItem(citems[i])).getQuantity();
						else
							qtyString += "," + ((CommerceItem) getOrder().getCommerceItem(citems[i])).getQuantity();
					} catch (Exception e) {
						addItemsFromCart = true;
						vlogDebug("Exception was thrown, break and search through cart");
						break;
					}
				}
			}
			if (!StringUtils.isBlank(idString))
				items = idString.split(",");
			if (!StringUtils.isBlank(qtyString))
				qtys = qtyString.split(",");
			if (addItemsFromCart) {
				vlogDebug("Searching through cart");
				idString = "";
				qtyString = "";
				for (int i = 0; i < citems.length; i++) {
					try {
						CommerceItem commerceItem = getShoppingCart().getCurrent().getCommerceItem(citems[i]);
						if (commerceItem != null) {
							if (StringUtils.isBlank(idString))
								idString = commerceItem.getCatalogRefId();
							else
								idString += "," + commerceItem.getCatalogRefId();
							if (StringUtils.isBlank(qtyString))
								qtyString = "" + ((CommerceItem) getOrder().getCommerceItem(citems[i])).getQuantity();
							else
								qtyString += "," + ((CommerceItem) getOrder().getCommerceItem(citems[i])).getQuantity();
						}
					} catch (Exception e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
				if (!StringUtils.isBlank(idString))
					items = idString.split(",");
				if (!StringUtils.isBlank(qtyString))
					qtys = qtyString.split(",");
				addItemsFromCart = false;
			}
			vlogDebug("Item & Qty string: " + idString + " | " + qtyString);
		}
		if (indexs != null && indexs.length > 0) {
			vlogDebug("Adding items from pa page");

			items = new String[indexs.length];
			qtys = new String[indexs.length];
			for (int i = 0; i < indexs.length; i++) {
				try {
					Integer index = Integer.parseInt(indexs[i]);
					items[i] = getSessionBean().getPalist().get(index).getSku();
					qtys[i] = ((Long) getSessionBean().getPalist().get(index).getQty()).toString();
				} catch (Exception e) {
					if (isLoggingError()) {
						logError(e);
					}
				}
			}
		}
		// Double check there are lists and items
		if (lists != null && items != null) {
			Long quantity;
			for (String list : lists) {
				if (list.equalsIgnoreCase("list-new")) {
					lists = checkNewList(lists, pRequest, pResponse);
					break;
				}
			}
			if (!getFormError()) {
				GiftlistManager mgr = getGiftlistManager();
				for (int i = 0; i < items.length; i++) {
					try {
						// Try to set qty, if fails, set to 1
						quantity = Long.parseLong(qtys[i]);
					} catch (NumberFormatException e) {
						vlogDebug("Quantity was not a number, set to 1");
						quantity = 1L;
					}
					for (String list : lists) {
						vlogDebug("Adding item " + items[i] + " with quantity " + quantity + " to giftlist " + list);
						mgr.addCatalogItemToGiftlist(items[i], items[i], list, getSiteId(), quantity);
						if (getFormError()) {
							vlogDebug("There was an error adding " + items[i] + " to giftlist " + list);
						}
					}
				}
			}
		}

		vlogDebug("addItemsToLists.end");
	}

	private String[] checkNewList(String[] lists, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, InvalidDateException, CommerceException {
		String pProfileId = (String) getProfile().getPropertyValue(ID);

		((CPSGiftlistManager) getGiftlistManager()).validatePreCreateListAddItem(pRequest, getProfile(), this);

		if (!getFormError()) {
			for (int i = 0; i < lists.length; i++) {
				if (lists[i].equalsIgnoreCase("list-new")) {
					lists[i] = saveGiftlist(pProfileId);
					setGiftlistId(lists[i]);
					setCustomProperties();
					break;
				}
			}
		}

		return lists;
	}

	private RepositoryItem findSkuByIdOrDisplayName(String pSearchTerm) {

		RepositoryItem sku = null;
		Object[] paramsForIdQuery = {pSearchTerm};
		String keyQuery = "displayName=?0 OR id=?0";
		RepositoryItem[] products = ((CPSProfileTools) getProfileTools()).queryRepository(paramsForIdQuery, keyQuery, "product",
				(MutableRepository) getGiftlistManager().getCatalogTools().getCatalog());
		if (products != null && products.length > 0) {
			ChangeAwareList childSkus = (ChangeAwareList) products[0].getPropertyValue("childSKUs");
			if (childSkus != null) {
				sku = (RepositoryItem) childSkus.iterator().next();
			}
		}
		return sku;
	}

	public boolean handleAddMaterialListItemsFromFile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleAddMaterialListItemsFromFile";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				List<String> skuList = new ArrayList<String>();
				List<Long> qtyList = new ArrayList<Long>();

				if (StringUtils.isNotEmpty(getSkuIdsList()) && StringUtils.isNotEmpty(getQtyList())) {
					String[] skuArray = getSkuIdsList().split(",");
					skuList = new ArrayList<String>(Arrays.asList(skuArray));

					String[] qtyArray = getQtyList().split(",");
					Long[] longQtyArray = new Long[qtyArray.length];
					for (int i = 0; i < qtyArray.length; i++) {
						longQtyArray[i] = Long.valueOf(qtyArray[i]);
					}
					qtyList = new ArrayList<Long>(Arrays.asList(longQtyArray));

				}

				return addMaterialListItemsFromFile(skuList, qtyList, pRequest, pResponse, getGiftlistId(), isGiftlistCheckAvailability());
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}

	private boolean isFileUploadedAjaxRequest(DynamoHttpServletRequest pRequest) {
		return pRequest.getHeader(PARAM_X_REQUESTED_WITH) != null && pRequest.getHeader(PARAM_X_REQUESTED_WITH).equals(PARAM_XML_HTTP_REQUEST) && pRequest.getHeader("Content-Type").contains("multipart/form-data");
	}

	public boolean addMaterialListItemsFromFile(List<String> products, List<Long> qtys, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String pGiftlistId, boolean isCheckAvail) throws ServletException, IOException {
		String successUrl = getAddItemsToMaterialListSuccessUrl() + pGiftlistId + (isCheckAvail ? "&ca=true" : "");
		String errorUrl = getAddItemsToMaterialListErrorUrl() + pGiftlistId + (isCheckAvail ? "&ca=true" : "");
		String invalidUrl = getAddItemsToMaterialListInvalidUrl() + pGiftlistId + (isCheckAvail ? "&ca=true" : "");

		if (isLoggingDebug()) {
			vlogDebug("Entering addMaterialListItemsFromFile");
		}

		if (("yes").equals(getAddFromErrorModal())) {
			if (products.size() != qtys.size() || products.size() != getSessionBean().getMaterialDuplicates().size()) {
				createError(CPSErrorCodes.ERR_MATERIAL_LIST_DUPE, "import", pRequest);
//				return checkFormRedirect(successUrl, errorUrl + "&im=true", pRequest, pResponse);

				if (isFileUploadedAjaxRequest(pRequest)) {
					return false;
				} else {
					return checkFormRedirect(successUrl, errorUrl + "&im=true", pRequest, pResponse);
				}

				//redirectOrForward(pRequest, pResponse, getQuickOrderAddItemsToOrderFromListInvalidURL());
				//return false;
			} else {
				addItemsFromImport(products, qtys, pGiftlistId, null);
				getSessionBean().setMaterialDuplicates(null);
				getSessionBean().setInvalidMaterialItemsList(null);
				getSessionBean().setHasMaterialDuplicates(false);

				if (isFileUploadedAjaxRequest(pRequest)) {
					return false;
				} else {
					return checkFormRedirect(successUrl, errorUrl, pRequest, pResponse);
				}
			}
		}

		if (!StringUtils.isBlank(getSkuIdsList()) && !StringUtils.isBlank(getQtyList())) {
			if (!("yes").equals(getAddFromErrorModal())) {
				boolean hasInputError = getDuplicatesMatchesService().makeValidSkuIdsList(getProfile(), this); //call out to query possible input values before adding //updated SkuIdsList,QtyList will be "" if lengths on input are not equal
				if (!hasInputError) {
					createError(CPSErrorCodes.ERR_INVALID_FILE_FORMAT, "import", pRequest);

					if (isFileUploadedAjaxRequest(pRequest)) {
						return false;
					} else {
						return checkFormRedirect(successUrl, errorUrl, pRequest, pResponse);
					}
				}
			}

			//no addable values found, check if duplicates only or just nothing at all..

			List<String> invalidItemsList = addItemsFromImport(products, qtys, pGiftlistId, getDuplicates());
			if ((invalidItemsList != null && invalidItemsList.size() > 0) || (getDuplicates() != null && getDuplicates().keySet() != null && getDuplicates().keySet().size() > 0)) {
				//need to redirect to error modal if either duplicates or invalid values were added, will need to check for null in modal droplet.

				getSessionBean().setInvalidMaterialItemsList(invalidItemsList); //set list of input values that had no return values to session
				if (getDuplicates() != null && getDuplicates().keySet() != null && getDuplicates().keySet().size() > 0) {
					getSessionBean().setMaterialDuplicates(getDuplicates());
					getSessionBean().setHasMaterialDuplicates(true);
				}

				if (!isFileUploadedAjaxRequest(pRequest)) {
					redirectOrForward(pRequest, pResponse, invalidUrl);
				}
			} else {
				getSessionBean().setInvalidMaterialItemsList(null);
				getSessionBean().setMaterialDuplicates(null);
				getSessionBean().setHasMaterialDuplicates(false);

				if (!isFileUploadedAjaxRequest(pRequest)) {
					redirectOrForward(pRequest, pResponse, successUrl);
				}
			}

		}

		return false;
	}

	public List<String> addItemsFromImport(List<String> products, List<Long> qtys, String giftlistId, HashMap<String, HashMap<Integer, ArrayList<String>>> pDuplicates) {
		vlogDebug("Importing items to giftlist: " + giftlistId);

		RepositoryItem giftlist = null;
		CPSGiftlistManager giftlistManager = (CPSGiftlistManager) getGiftlistManager();
		List<String> invalidItemsList = new ArrayList<String>();
		try {
			giftlist = giftlistManager.getGiftlist(giftlistId);
			if (giftlist != null && !products.isEmpty() && !qtys.isEmpty()) {
				for (int i = 0; i < products.size(); i++) {
					if (pDuplicates == null || pDuplicates.isEmpty() || !pDuplicates.containsKey(products.get(i))
							|| (giftlistManager.isIgnoreCaseItemsQuery() && !pDuplicates.containsKey(products.get(i).toUpperCase()))) {
						RepositoryItem product = null;
						try {
							product = findSkuByIdOrDisplayName(products.get(i));
							if (product == null && giftlistManager.isIgnoreCaseItemsQuery()) {
								product = findSkuByIdOrDisplayName(products.get(i).toUpperCase());
							}
							if (product == null && products.get(i).startsWith("0")) {
								product = findSkuByIdOrDisplayName(getSixLengthIdService().removeFirstZeros(products.get(i)));
							}
							if (product == null) {
								RepositoryItem parentOrg = (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG);
								if (parentOrg != null) {
									product = ((CPSPurchaseProcessHelper) getPurchaseProcessHelper()).findProductFormAlias(products.get(i), parentOrg.getRepositoryId());
									if (product != null) {
										ChangeAwareList childSkus = (ChangeAwareList) product.getPropertyValue("childSKUs");
										if (childSkus != null) {
											product = (RepositoryItem) childSkus.iterator().next();
										}
									}
								}
							}
							vlogDebug("Product: " + product);
							if (product != null) {
								// Do some quick quantity validations
								Long qty = qtys.get(i);
								if (qty == null) {
									// Seriously!?!
									qty = 1L;
								} else if (qty > 99999) {
									// Only allow 5 digits
									qty = 99999L;
								} /*else if (qty < 1) {
									// Haha nice try
									qty = 1L;
								}*/
								ChangeAwareSet parentProducts = (ChangeAwareSet) product.getPropertyValue("parentProducts");
								String productId = null;
								if (parentProducts != null) {
									productId = ((RepositoryItem) parentProducts.iterator().next()).getRepositoryId();
								}
								String skuId = product.getRepositoryId();
								vlogDebug("Importing ProdId - " + productId + ", SkuId - " + skuId + " to giftlist " + giftlistId + " with quantity " + qty);
								getGiftlistManager().addCatalogItemToGiftlist(skuId, productId, giftlistId, getSiteId(), qty);
							} else {
								invalidItemsList.add(products.get(i));
							}
						} catch (Exception e) {
							vlogError(e, "Not valid item id entered, no item found");
						}
					}
				}
			}
		} catch (CommerceException ce) {
			vlogError(ce, "Not valid giftlistId passed for list import.");
		}

		vlogDebug("Done importing.");
		return invalidItemsList;
		// Yay!!!
	}

	public boolean handleAddItemToPA(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("handleAddItemToPA.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleAddItemToPA";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				String skuList = getItemIdsList();
				String qtyList = getQuantities();

				if (StringUtils.isBlank(skuList)) {
					skuList = pRequest.getParameter("itemIdsList");
				}
				if (StringUtils.isBlank(qtyList)) {
					qtyList = pRequest.getParameter("quantities");
				}
				vlogDebug("skuList: " + skuList);
				vlogDebug("qtyList: " + qtyList);

				if (!StringUtils.isBlank(skuList) && !StringUtils.isBlank(qtyList)) {
					vlogDebug("Items and quantites entered");
					String[] skus = skuList.split(",");
					String[] qtys = qtyList.split(",");

					try {
						String[] temp = new String[skus.length];
						int count = 0;
						for (String itemId : skus) {
							temp[count++] = (String) getGiftlistManager().getGiftitem(itemId).getPropertyValue(CATALOG_REF_ID);
							vlogDebug("New id: " + temp[count - 1]);
						}
						skus = temp;
					} catch (CommerceException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}

					vlogDebug("Skus: " + Arrays.toString(skus));
					vlogDebug("Qtys: " + Arrays.toString(qtys));

					if (skus.length == qtys.length) {
						vlogDebug("Add the items");
						CPSPriceAvailability item;
						RepositoryItem csObject = (RepositoryItem) getProfile().getPropertyValue(SELECTED_CS);
						String cs = null;
						if (csObject != null) {
							cs = (String) csObject.getPropertyValue(ID);
						}
						Map<String, Long> itemsForPA = new HashMap<String, Long>();
						for (int i = 0; i < skus.length; i++) {
							item = new CPSPriceAvailability();
							item.setCsid(cs);
							Long qty = 1L;
							try {
								qty = Long.parseLong(qtys[i]);
							} catch (NumberFormatException e) {
								logError(e);
								qty = 1L;
							}
							item.setQty(qty);
							item.setSku(skus[i]);
							String productId = productIdFromSku(skus[i]);
							item.setPaproductid(productId);
							if (!StringUtils.isBlank(productId)) {
								itemsForPA.put(productId, qty);
							}
							vlogDebug("Adding Item - " + item + " to pa list");
							getSessionBean().getPalist().add(item);
						}
						getSessionBean().getItemIdsPA().clear();
						if (itemsForPA.size() > 0) {
							getSessionBean().setItemIdsPA(itemsForPA);
						}
					}
				} else {
					if (StringUtils.isBlank(skuList)) {
						createError(CPSErrorCodes.ERR_PA_NO_ITEM, "itemIdsList", pRequest);
					}
					if (StringUtils.isBlank(qtyList)) {
						createError(CPSErrorCodes.ERR_PA_NO_QTY, "quantities", pRequest);
					}
				}

				if (StringUtils.isBlank(getAddItemToPASuccessURL())) {
					setAddItemToPASuccessURL(pRequest.getParameter("addItemToPASuccessURL"));
				}
				if (StringUtils.isBlank(getAddItemToPAErrorURL())) {
					setAddItemToPAErrorURL(pRequest.getParameter("addItemToPAErrorURL"));
				}

				if (VSGFormUtils.isAjaxRequest(pRequest)) {
					try {
						if (!getFormError()) {
							addAjaxSuccessResponse(pResponse);
						} else {
							CPSFormUtils.addAjaxErrorResponse(this, pResponse);
						}
					} catch (JSONException e) {
						logError(e);
					}
					return false;
				} else {
					return checkFormRedirect(getAddItemToPASuccessURL(), getAddItemToPAErrorURL(), pRequest, pResponse);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		vlogDebug("handleAddItemToPA.end");

		return false;
	}

	public String productIdFromSku(String catalogRefId) {
		RepositoryItem skuri = null;
		if (!StringUtils.isBlank(catalogRefId)) {
			try {
				skuri = getOrderManager().getCatalogTools().findSKU(catalogRefId);
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
			RepositoryItem prdri = null;
			if (skuri != null) {
				Object[] prdris = ((Set) skuri.getPropertyValue("parentProducts")).toArray();
				prdri = (RepositoryItem) prdris[0];
				if (prdri != null) {
					// since we have the product lets also set that on the AddCommercItemInfo
					return prdri.getRepositoryId();
				}
			}
		}
		return null;
	}

	public boolean handleUpdateGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, CommerceException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSGiftlistFormHandler.handleUpdateGiftlist";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				boolean result = super.handleUpdateGiftlist(pRequest, pResponse);
				if (VSGFormUtils.isAjaxRequest(pRequest)) {
					try {
						if (!getFormError()) {
							addAjaxSuccessResponse(pResponse);
						} else {
							CPSFormUtils.addAjaxErrorResponse(this, pResponse);
						}
					} catch (JSONException e) {
						logError(e);
					}
					return false;
				} else {
					return result;
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}

	/**
	 * Takes in a map of error messages and properties to display and highlight
	 * Use this if you have a list of errors to display
	 *
	 * @param errors
	 * @param pRequest
	 */
	public void checkErrorMessages(Map<String, Object> errors, DynamoHttpServletRequest pRequest) {
		for (Entry<String, Object> error : errors.entrySet())
			createError(error.getKey(), (String) error.getValue(), pRequest);
	}


	/**
	 * This takes a single error key and property and displays the error and adds the
	 * property to be highlighted. Use this if only adding one error.
	 *
	 * @param msg
	 * @param pty
	 * @param pRequest
	 */
	protected void createError(String msg, String pty, DynamoHttpServletRequest pRequest) {
		CPSMessageUtils.addFormException(this, ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
	}

	public void addAjaxSuccessResponse(DynamoHttpServletResponse pResponse)
			throws IOException, JSONException {
		pResponse.setContentType("application/json");

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put("error", "false");
		responseJson.put("url", getSaveGiftlistSuccessURL());

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	/////////////////////////////////////////////////////////////////
	// Getters and setters
	/////////////////////////////////////////////////////////////////

	public String getRemoveListId() {
		return removeListId;
	}

	public void setRemoveListId(String removeListId) {
		this.removeListId = removeListId;
	}

	public CPSSessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(CPSSessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getCs() {
		return cs;
	}

	public void setCs(String cs) {
		this.cs = cs;
	}

	public String getEmailList() {
		return emailList;
	}

	public void setEmailList(String emailList) {
		this.emailList = emailList;
	}

	public String getEmailNotes() {
		return emailNotes;
	}

	public void setEmailNotes(String emailNotes) {
		this.emailNotes = emailNotes;
	}

	public String getSelectedLists() {
		return selectedLists;
	}

	public void setSelectedLists(String selectedLists) {
		this.selectedLists = selectedLists;
	}

	public String getAddedCommerceItems() {
		return addedCommerceItems;
	}

	public void setAddedCommerceItems(String addedCommerceItems) {
		this.addedCommerceItems = addedCommerceItems;
	}

	public String getItemIdsList() {
		return itemIdsList;
	}

	public void setItemIdsList(String itemIdsList) {
		this.itemIdsList = itemIdsList;
	}

	public String getQuantities() {
		return quantities;
	}

	public void setQuantities(String quantities) {
		this.quantities = quantities;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	public CPSCSRLoggingManager getCsrLoggingManager() {
		return csrLoggingManager;
	}

	public void setCsrLoggingManager(CPSCSRLoggingManager csrLoggingManager) {
		this.csrLoggingManager = csrLoggingManager;
	}

	public String getAddItemToPASuccessURL() {
		return addItemToPASuccessURL;
	}

	public void setAddItemToPASuccessURL(String addItemToPASuccessURL) {
		this.addItemToPASuccessURL = addItemToPASuccessURL;
	}

	public String getAddItemToPAErrorURL() {
		return addItemToPAErrorURL;
	}

	public void setAddItemToPAErrorURL(String addItemToPAErrorURL) {
		this.addItemToPAErrorURL = addItemToPAErrorURL;
	}

	public String getIndexList() {
		return indexList;
	}

	public void setIndexList(String indexList) {
		this.indexList = indexList;
	}

	public CommonEmailSender getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(CommonEmailSender emailSender) {
		this.emailSender = emailSender;
	}

	public PurchaseProcessHelper getPurchaseProcessHelper() {
		return purchaseProcessHelper;
	}

	public void setPurchaseProcessHelper(PurchaseProcessHelper purchaseProcessHelper) {
		this.purchaseProcessHelper = purchaseProcessHelper;
	}

	public SixLengthIdService getSixLengthIdService() {
		return mSixLengthIdService;
	}

	public void setSixLengthIdService(SixLengthIdService pSixLengthIdService) {
		this.mSixLengthIdService = pSixLengthIdService;
	}

	// Getter and Setter methods

	/**
	 * Gets mUserId.
	 *
	 * @return Value of mUserId.
	 */
	public String getUserId() {
		return mUserId;
	}

	/**
	 * Sets new mUserId.
	 *
	 * @param pUserId New value of mUserId.
	 */
	public void setUserId(String pUserId) {
		mUserId = pUserId;
	}

	public String getSkuIdsList() {
		return skuIdsList;
	}

	public void setSkuIdsList(String skuIdsList) {
		this.skuIdsList = skuIdsList;
	}

	public String getQtyList() {
		return qtyList;
	}

	public void setQtyList(String qtyList) {
		this.qtyList = qtyList;
	}

	public ArrayList<String> getFinalSkus() {
		return quickOrderFinalSkus;
	}

	public void setFinalSkus(ArrayList<String> quickOrderFinalSkus) {
		this.quickOrderFinalSkus = quickOrderFinalSkus;
	}

	public void setFinalQuantities(ArrayList<Integer> quickOrderFinalQuantities) {
		this.quickOrderFinalQuantities = quickOrderFinalQuantities;
	}

	public ArrayList<Integer> getFinalQuantities() {
		return quickOrderFinalQuantities;
	}

	public DuplicatesMatchesService getDuplicatesMatchesService() {
		return mDuplicatesMatchesService;
	}

	public void setDuplicatesMatchesService(
			DuplicatesMatchesService pDuplicatesMatchesService) {
		this.mDuplicatesMatchesService = pDuplicatesMatchesService;
	}

	public void setDuplicates(HashMap<String, HashMap<Integer, ArrayList<String>>> pDuplicates) {
		this.mDuplicates = pDuplicates;
	}

	public HashMap<String, HashMap<Integer, ArrayList<String>>> getDuplicates() {
		return mDuplicates;
	}

	public void setAddFromErrorModal(String addFromErrorModal) {
		this.addFromErrorModal = addFromErrorModal;
	}

	public String getAddFromErrorModal() {
		return addFromErrorModal;
	}

	public String getAddItemsToMaterialListSuccessUrl() {
		return mAddItemsToMaterialListSuccessUrl;
	}

	public void setAddItemsToMaterialListSuccessUrl(
			String addItemsToMaterialListSuccessUrl) {
		this.mAddItemsToMaterialListSuccessUrl = addItemsToMaterialListSuccessUrl;
	}

	public String getAddItemsToMaterialListErrorUrl() {
		return mAddItemsToMaterialListErrorUrl;
	}

	public void setAddItemsToMaterialListErrorUrl(
			String addItemsToMaterialListErrorUrl) {
		this.mAddItemsToMaterialListErrorUrl = addItemsToMaterialListErrorUrl;
	}

	public String getAddItemsToMaterialListInvalidUrl() {
		return mAddItemsToMaterialListInvalidUrl;
	}

	public void setAddItemsToMaterialListInvalidUrl(
			String pAddItemsToMaterialListInvalidUrl) {
		this.mAddItemsToMaterialListInvalidUrl = pAddItemsToMaterialListInvalidUrl;
	}

	public boolean isGiftlistCheckAvailability() {
		return mGiftlistCheckAvailability;
	}

	public void setGiftlistCheckAvailability(boolean pGiftlistCheckAvailability) {
		this.mGiftlistCheckAvailability = pGiftlistCheckAvailability;
	}

	public String getDisplayGiftlistBaseUrl() {
		return mDisplayGiftlistBaseUrl;
	}

	public void setDisplayGiftlistBaseUrl(String pDisplayGiftlistBaseUrl) {
		mDisplayGiftlistBaseUrl = pDisplayGiftlistBaseUrl;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

}