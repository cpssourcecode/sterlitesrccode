package com.cps.commerce.endeca.index.accessor;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

public class ProductAttributePropertyAccessor extends GenerativePropertyAccessor {

    private String propertyName;

    @Override
    protected Map<String, Object> getPropertyNamesAndValues(Context paramContext, RepositoryItem paramRepositoryItem, String paramString,
                    PropertyTypeEnum paramPropertyTypeEnum, boolean paramBoolean) {
        Map<String, Object> sizeData = null;
        Object sizeJSONObj = paramRepositoryItem.getPropertyValue(getPropertyName());
        if (sizeJSONObj instanceof String) {
            String sizeJSONData = (String) sizeJSONObj;
            if (StringUtils.isNotBlank(sizeJSONData)) {
                JSONObject sizeJSON = null;
                sizeData = new HashMap<String, Object>();
                try {
                    sizeJSON = new JSONObject(sizeJSONData);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (sizeJSON != null) {
                    for (String sizeKey : sizeJSON.keySet()) {
                        try {
                            sizeData.put("dimval.prop." + sizeKey, sizeJSON.get(sizeKey));
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return sizeData;
    }

    @Override
    public boolean ownsDynamicPropertyName(String paramString, IndexingOutputConfig paramIndexingOutputConfig, OutputProperty paramOutputProperty) {
        return false;
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName
     *            the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}