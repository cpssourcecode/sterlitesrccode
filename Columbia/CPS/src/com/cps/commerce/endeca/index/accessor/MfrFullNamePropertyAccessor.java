package com.cps.commerce.endeca.index.accessor;

import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

/**
 * 
 * @author VSG
 *
 */
public class MfrFullNamePropertyAccessor extends PropertyAccessorImpl {

	boolean mUseBrandName = true;

	public boolean isUseBrandName() {
		return mUseBrandName;
	}

	public void setUseBrandName(boolean pUseBrandName) {
		mUseBrandName = pUseBrandName;
	}

	@Override
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
			PropertyTypeEnum pType) {

		String result = null;
		if (isUseBrandName()) {
			result = (String) pItem.getPropertyValue(CPSConstants.BRAND_NAME);
		} else {
			result = (String) pItem.getPropertyValue(CPSConstants.VENDOR_NAME);
		if(StringUtils.isBlank(result)) {
			result = (String) pItem.getPropertyValue(CPSConstants.MFR_FULL_NAME);
		}
		}

		return result;
	}
}
