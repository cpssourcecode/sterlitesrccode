package com.cps.commerce.endeca.index.accessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

/**
 * The product map property accessor. used for product.attributeValueMap property accessor.
 *
 */
public class ProductAttributeValueAccessor extends PropertyAccessorImpl {

    private String mMapPropertyName;
    private String mSecondaryPropertyName;
    // properties
    private int mQuantity = -1;

    @Override
    protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType) {
        return getMapping(pItem);
    }

    /**
     * Checks if is leaf category.
     *
     * @param pItem
     *            the item
     * @return the object
     */
    @SuppressWarnings("unchecked")
    private Object getMapping(RepositoryItem pItem) {
        List prodAttributeList = new ArrayList<String>();
        Object prodPrimaryPropertyValue = pItem.getPropertyValue(getMapPropertyName());
        Object prodSecondPropertyValue = pItem.getPropertyValue(getSecondaryPropertyName());
        int quantity = getQuantity();

        if (prodPrimaryPropertyValue instanceof Map) {
            Map prodAttrbsMap = (Map) prodPrimaryPropertyValue;
            Map secondaryMap = null;
            if (prodSecondPropertyValue instanceof Map) {
                secondaryMap = (Map) prodSecondPropertyValue;
            }
            int i = 0;
            for (Object oKey : prodAttrbsMap.keySet()) {
                String customerAlias = (String) prodAttrbsMap.get(oKey);
                if (secondaryMap != null) {
                    String secondaryValue = (String) secondaryMap.get(oKey);
                    if (StringUtils.isNotBlank(secondaryValue)) {
                        customerAlias += " " + secondaryValue;
                    }
                }
                prodAttributeList.add(customerAlias);
                i++;
                if (quantity != -1 && i > quantity - 1) {
                    break;
                }
            }

        }
        // System.out.println("prodAttributeList ::" + prodAttributeList);

        return prodAttributeList;
    }

    /**
     * Gets mMapPropertyName.
     *
     * @return Value of mMapPropertyName.
     */
    public String getMapPropertyName() {
        return mMapPropertyName;
    }

    /**
     * Sets new mMapPropertyName.
     *
     * @param pMapPropertyName
     *            New value of mMapPropertyName.
     */
    public void setMapPropertyName(String pMapPropertyName) {
        mMapPropertyName = pMapPropertyName;
    }

    /**
     * Gets mQuantity.
     *
     * @return Value of mQuantity.
     */
    public int getQuantity() {
        return mQuantity;
    }

    /**
     * Sets new mQuantity.
     *
     * @param pQuantity
     *            New value of mQuantity.
     */
    public void setQuantity(int pQuantity) {
        mQuantity = pQuantity;
    }

    public String getSecondaryPropertyName() {
        return mSecondaryPropertyName;
    }

    public void setSecondaryPropertyName(String pSecondaryPropertyName) {
        mSecondaryPropertyName = pSecondaryPropertyName;
    }
}
