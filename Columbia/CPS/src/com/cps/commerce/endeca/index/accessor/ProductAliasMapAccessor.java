package com.cps.commerce.endeca.index.accessor;

import static com.cps.util.CPSConstants.PRODUCT_ALIAS_MAP;
import static vsg.constants.VSGConstants.MAP_KEY_VALUE_DELIMITER;     //      http://unicode-table.com/en/#00B6
import static vsg.constants.VSGConstants.MAP_PAIR_DELIMITER;          //      http://unicode-table.com/en/#00A7


import java.util.Map;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

/**
 * The product.prodAliasMap property accessor.
 *
 * @author Andy Porter
 */
public class ProductAliasMapAccessor extends PropertyAccessorImpl {


	@Override
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName,
												PropertyTypeEnum pType) {
		return getMapping(pItem);
	}

	/**
	 * Checks if is leaf category.
	 *
	 * @param pItem the item
	 * @return the object
	 */
	@SuppressWarnings("unchecked")
	private Object getMapping(RepositoryItem pItem) {

		Map prodAliasMap = (Map) pItem.getPropertyValue(PRODUCT_ALIAS_MAP);

		StringBuilder sbValue = new StringBuilder();
		int index = 0;
		for (Object oKey : prodAliasMap.keySet()) {
			String customerAlias = (String) prodAliasMap.get(oKey);
			String key = null;
			if (oKey instanceof String) {
				key = (String) oKey;
			}
			if (null != key) {
				if (index > 0) {
					sbValue.append(MAP_PAIR_DELIMITER);
				}
				sbValue.append(key).append(MAP_KEY_VALUE_DELIMITER).append(customerAlias);
			}
			index++;
		}

		return sbValue.toString();
	}


}
