package com.cps.commerce.endeca.index.accessor;

import org.apache.commons.lang3.StringUtils;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

/**
 *
 * This class is used to Pad zeroes to the left of the item id to provide the necessary value. Used primarily for sorting purposes.
 *
 * @author Naga
 *
 */
public class PadZeroesToIdPropertyAccessor extends PropertyAccessorImpl {
    private static final char ZERO = '0';
    private String fillText;
    private int maxLength;

    /**
     * @return the fillText
     */
    public String getFillText() {
        return fillText;
    }

    /**
     * @param fillText
     *            the fillText to set
     */
    public void setFillText(String fillText) {
        this.fillText = fillText;
    }

    /**
     * @return the maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * @param maxLength
     *            the maxLength to set
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        setFillText(StringUtils.repeat(ZERO, maxLength));
    }

    private String padLeftFixed(String input) {
        return input.length() < getMaxLength() ? getFillText().substring(input.length()) + input : input;
    }

    @Override
    protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType) {
        return padLeftFixed(pItem.getRepositoryId());
    }
}