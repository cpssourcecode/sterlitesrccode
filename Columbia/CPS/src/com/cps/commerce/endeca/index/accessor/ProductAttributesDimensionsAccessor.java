package com.cps.commerce.endeca.index.accessor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryUtils;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;
import vsg.constants.VSGConstants;
import vsg.constants.VSGRepositoriesConstants;

/**
 *
 * @author VSG
 *
 */
public class ProductAttributesDimensionsAccessor extends GenerativePropertyAccessor {

    /** The attribute map property name */
    private String mPropertyName;
    private String mSecondaryPropertyName;
    private boolean mSearchable;
    private List<String> mListAttributesNames;

    @Override
    protected Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pRepositoryItem, String pString,
                    PropertyTypeEnum pPropertyTypeEnum, boolean pBoolean) {
        return getProductAttributesMap(pRepositoryItem);
    }

    /**
     * return attributes map for the product
     *
     * @param pProduct
     *            - {@link RepositoryItem}
     * @return - Map<String, Object>
     */
    private Map<String, Object> getProductAttributesMap(RepositoryItem pProduct) {
        Map<String, Object> attributesMap = new HashMap<String, Object>();
        try {
            if (pProduct != null && RepositoryUtils.isTypeOfItemDesc(pProduct, CPSConstants.PRODUCT_ITEM_DESCRIPTOR)) {
                Object value = pProduct.getPropertyValue(getPropertyName());
                if (value instanceof Map) {
                    Set<String> attributeKeys = attributeKeysForProduct(pProduct);
                    if (attributeKeys != null && !attributeKeys.isEmpty()) {
                        Object oProdSecondaryMap = pProduct.getPropertyValue(getSecondaryPropertyName());
                        Map secondaryMap = null;
                        if (oProdSecondaryMap instanceof Map) {
                            secondaryMap = (Map) oProdSecondaryMap;
                        }
                        for (Map.Entry<String, String> entry : ((Map<String, String>) value).entrySet()) {
                            String key = entry.getKey().replaceAll(CPSConstants.FILTER_SPEC_SIMBOLS, VSGConstants.EMPTY).toUpperCase();
                            if (isSearchable() && getListAttributesNames().contains(key)) {
                                putValuesInAttributeMap(attributesMap, attributeKeys, secondaryMap, entry, key);
                            }
                            if (!isSearchable() && !getListAttributesNames().contains(key)) {
                                putValuesInAttributeMap(attributesMap, attributeKeys, secondaryMap, entry, key);
                            }
                        }
                    }
                }
            }
        } catch (RepositoryException e) {
            vlogError(e, "Error in ProductAttributesDimensionsAccessor.getProductAttributesMap");
        }

        // since this way of adding doesnt allow us to disale autogen dim values
        attributesMap.remove(CPSConstants.NOMINAL_SIZE_FILTER_NAME);
        attributesMap.remove(CPSConstants.OUTSIDE_DIAMETER_FILTER_NAME);
        attributesMap.remove(CPSConstants.INSIDE_DIAMETER_FILTER_NAME);
        attributesMap.remove(CPSConstants.ROD_SIZE_FILTER_NAME);
        attributesMap.remove(CPSConstants.PIPE_SIZE_FILTER_NAME);
        attributesMap.remove(CPSConstants.CENTER_DISTANCE_FILTER_NAME);
        attributesMap.remove(CPSConstants.FLOW_RATE_FILTER_NAME);
        attributesMap.remove(CPSConstants.PRESSURE_CLASS_FILTER_NAME);
        attributesMap.remove(CPSConstants.THICKNESS_FILTER_NAME);
        // System.out.println("attributesMap ::" + attributesMap);

        return attributesMap;
    }

    private void putValuesInAttributeMap(Map<String, Object> attributesMap, Set<String> attributeKeys, Map secondaryMap, Map.Entry<String, String> enrty,
                    String key) {
        if (attributeKeys.contains(key)) {
            String valueStr = enrty.getValue();
            if (secondaryMap != null) {
                String secondaryValue = (String) secondaryMap.get(key);
                if (StringUtils.isNotBlank(secondaryValue)) {
                    valueStr += " " + secondaryValue;
                }
            }
            attributesMap.put(key, valueStr);
        }
    }

    /**
     * retrieve attribute keys for product from parent categories
     *
     * @param pProduct
     *            - {@link RepositoryItem}
     * @return Set<String>
     */
    private Set<String> attributeKeysForProduct(RepositoryItem pProduct) {
        Set<String> attributeKeys = new HashSet<String>();
        if (pProduct != null) {
            Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) pProduct.getPropertyValue(CPSConstants.PRODUCT_PARENT_CATEGORIES);
            if (parentCategories != null && !parentCategories.isEmpty()) {
                for (RepositoryItem category : parentCategories) {
                    attributeKeys.addAll(attributeKeysForCategory(category));
                }
            }
        }
        return attributeKeys;
    }

    /**
     * retrieve attribute keys for category
     *
     * @param pCategory
     *            - {@link RepositoryItem}
     * @return Set<String>
     */
    private Set<String> attributeKeysForCategory(RepositoryItem pCategory) {
        Set<String> attributes = new HashSet<String>();
        if (pCategory != null) {
            List<String> attributeKeys = (List<String>) pCategory.getPropertyValue(CPSConstants.CATEGORY_FACETS);
            if (attributeKeys != null && !attributeKeys.isEmpty()) {
                for (String key : attributeKeys) {
                    String filteredKey = key.replaceAll(CPSConstants.FILTER_SPEC_SIMBOLS, VSGConstants.EMPTY);
                    attributes.add(filteredKey.toUpperCase());
                }
            }
            Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) pCategory.getPropertyValue(VSGRepositoriesConstants.CATEGORY_FIXED_PARENT_CATS);
            if (parentCategories != null && !parentCategories.isEmpty()) {
                for (RepositoryItem category : parentCategories) {
                    attributes.addAll(attributeKeysForCategory(category));
                }
            }
        }
        return attributes;
    }

    @Override
    public boolean ownsDynamicPropertyName(String arg0, IndexingOutputConfig arg1, OutputProperty arg2) {
        return true;
    }

    public String getPropertyName() {
        return mPropertyName;
    }

    public void setPropertyName(String pPropertyName) {
        mPropertyName = pPropertyName;
    }

    public String getSecondaryPropertyName() {
        return mSecondaryPropertyName;
    }

    public void setSecondaryPropertyName(String pSecondaryPropertyName) {
        mSecondaryPropertyName = pSecondaryPropertyName;
    }

    public boolean isSearchable() {
        return mSearchable;
    }

    public void setSearchable(boolean pSearchable) {
        mSearchable = pSearchable;
    }

    public List<String> getListAttributesNames() {
        return mListAttributesNames;
    }

    public void setListAttributesNames(List<String> pListAttributesNames) {
        mListAttributesNames = pListAttributesNames;
    }
}
