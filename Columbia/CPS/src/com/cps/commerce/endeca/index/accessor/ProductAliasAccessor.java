package com.cps.commerce.endeca.index.accessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vsg.endeca.EndecaConstants;
import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.GenerativePropertyAccessor;
import atg.repository.search.indexing.IndexingOutputConfig;
import atg.repository.search.indexing.specifier.OutputProperty;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

import com.cps.service.OrganizationAliasService;
import com.cps.util.CPSConstants;
/**
 * The product map property accessor.
 * used for product.prodAlias property accessor .
 *
 * @author Andy Porter
 */
public class ProductAliasAccessor extends GenerativePropertyAccessor {
	
	/**
	 * Organization Alias Service
	 */
	private OrganizationAliasService mOrgAliasService;
	
	/**
	 * delimeter
	 */
	private static final String DELIMITER = ",";
	
	@Override
	protected Map<String, Object> getPropertyNamesAndValues(Context pContext, RepositoryItem pProduct, String arg2, PropertyTypeEnum arg3,
			boolean arg4) {
		
		Map<String,String> aliasMap = (Map<String, String>) pProduct.getPropertyValue(CPSConstants.PRODUCT_ITEM_DESCRIPTOR_ALIAS_MAP);
		Map<String, Object> propMap = new HashMap<String, Object>();
		
		for (Map.Entry<String, String> entry : aliasMap.entrySet()) {
			//getOrgAliasService().addOrganization(entry.getKey());
			String key = EndecaConstants.ALIAS_PROPERTY_NAME + entry.getKey();
			String[] alias = entry.getValue().split(DELIMITER);
			List<String> prodAliasArrayList = new ArrayList<String>();
			for(String al : alias){
				prodAliasArrayList.add(al);
			}
			propMap.put(key, prodAliasArrayList);
		}
		
		return propMap;
		
	}
	
	public OrganizationAliasService getOrgAliasService() {
		return mOrgAliasService;
	}

	public void setOrgAliasService(OrganizationAliasService pOrgAliasService) {
		this.mOrgAliasService = pOrgAliasService;
	}

	@Override
	public boolean ownsDynamicPropertyName(String arg0,
			IndexingOutputConfig arg1, OutputProperty arg2) {
		return false;
	}
}
