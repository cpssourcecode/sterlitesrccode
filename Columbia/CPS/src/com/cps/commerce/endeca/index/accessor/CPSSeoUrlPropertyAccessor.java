package com.cps.commerce.endeca.index.accessor;

import com.cps.seo.SeoTools;

import atg.repository.RepositoryItem;
import atg.repository.search.indexing.Context;
import atg.repository.search.indexing.PropertyAccessorImpl;
import atg.repository.search.indexing.specifier.PropertyTypeEnum;

public class CPSSeoUrlPropertyAccessor extends PropertyAccessorImpl {
	private SeoTools mSeoTools;
	
	public SeoTools getSeoTools() {
		return mSeoTools;
	}

	public void setSeoTools(SeoTools pSeoTools) {
		mSeoTools = pSeoTools;
	}

	@Override
	protected Object getTextOrMetaPropertyValue(Context pContext, RepositoryItem pItem, String pPropertyName, PropertyTypeEnum pType) {
		SeoTools seoTools = getSeoTools();
		return ( null == seoTools ) ? null : seoTools.makeSisterSeoLink(pItem);
	}
}