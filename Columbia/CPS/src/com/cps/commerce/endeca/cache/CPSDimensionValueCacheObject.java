package com.cps.commerce.endeca.cache;

import java.util.List;

import atg.commerce.endeca.cache.DimensionValueCacheObject;

public class CPSDimensionValueCacheObject extends DimensionValueCacheObject {

	public CPSDimensionValueCacheObject(String pRepositoryId, String pDimvalId) {
		super(pRepositoryId, pDimvalId);
	}
	
	private String mDimensionName;

	public String getDimensionName() {
		return mDimensionName;
	}

	public void setDimensionName(String pDimensionName) {
		mDimensionName = pDimensionName;
	}

	@Override
	public void setURL(String pUrl) {
		if (pUrl != null) {
			super.setURL(pUrl.toLowerCase());
		} else {
			super.setURL(pUrl);
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder(getDimvalId());

		String url = getUrl();
		if (url != null) {
			sb.append(" ");
			sb.append(url);
		}

		List<String> ancestors = getAncestorRepositoryIds();
		if ((ancestors != null) && (!(ancestors.isEmpty()))) {
			for (String id : ancestors) {
				sb.append(" ");
				sb.append(id);
			}
		}

		sb.append(" ").append(getDimensionName());

		return sb.toString();
	}
}
