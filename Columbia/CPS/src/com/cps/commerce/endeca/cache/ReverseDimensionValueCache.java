package com.cps.commerce.endeca.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import atg.core.util.StringUtils;
import atg.endeca.assembler.AssemblerTools;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.cps.seo.SeoTools;
import com.endeca.infront.assembler.AssemblerException;
import com.endeca.navigation.DimLocation;

public class ReverseDimensionValueCache extends GenericService {
	public static final String SITE_IDS = "siteIds";
	public static final String CATEGORY = "category";

	/**
	 * Cache used in the component
	 */
	private ConcurrentHashMap<String, ConcurrentHashMap<String, Object>> mCache = new ConcurrentHashMap<String, ConcurrentHashMap<String, Object>>();
	/**
	 * SeoTools component
	 */
	private SeoTools mSeoTools = null;

	/**
	 * Product Catalog repository
	 */
	private Repository mProductCatalog = null;

	/**
	 * Assembler tools
	 */
	private AssemblerTools mAssemblerTools = null;
	
	/**
	 * Returns the cache value for the dimension by name
	 * 
	 * @param pDimensionName
	 * @return Map<String, String>
	 */
	public Map<String, String> getCacheForDimension(String pDimensionName) {
		Map<String, String> result = null;

		if (!StringUtils.isBlank(pDimensionName)) {
			getCache().get(pDimensionName);
		}

		return result;
	}

	/**
	 * Returns the dimension by the dimension name
	 * 
	 * @param pDimensionName
	 * @return Map<String, String>
	 */
	public Map<String, Object> getDimensionByName(String pDimensionName) {
		Map<String, Object> result = null;

		if (!StringUtils.isBlank(pDimensionName)) {
			result = getCache().get(pDimensionName);
		}

		return result;
	}

	/**
	 * Method used to put the category dimension value to the cache
	 * 
	 * @param pDimLocation
	 * @throws RepositoryException
	 */
	public void putCategoryCacheValueIntoDimension(DimLocation pDimLocation, String pSiteId) {
		if (null != pDimLocation && !StringUtils.isBlank(pSiteId)) {
			String dimlocationId = Long.toString(pDimLocation.getDimValue().getId());
			String dimensionName = pDimLocation.getDimValue().getDimensionName();
			if (null != getSeoTools()) {
				dimensionName = getSeoTools().getSeoDimensionName(dimensionName);
			}
			String dimensionValue = getSeoTools().cleanDimensionName(pDimLocation.getDimValue().getName());
			// Set<String> siteIds = getSiteIdsForCategory(pCatId);

			if (!StringUtils.isBlank(dimlocationId) && !StringUtils.isBlank(dimensionName)
					&& !StringUtils.isBlank(dimensionValue)) {
				Map<String, Object> dimension = getCache().get(dimensionName);
				if (null == dimension) {
					dimension = new ConcurrentHashMap<String, Object>();
					getCache().put(dimensionName, (ConcurrentHashMap<String, Object>) dimension);
				}
				// for (String siteId : siteIds) {
				updateCategorySiteCache(dimlocationId, dimension, dimensionValue, pSiteId);
				// }
			}
		}
	}

	/**
	 * Updates the category by site cache value
	 * 
	 * @param pDimlocationId
	 * @param pDimension
	 * @param pDimensionValue
	 * @param pSiteId
	 */
	@SuppressWarnings("unchecked")
	private void updateCategorySiteCache(String pDimlocationId, Map<String, Object> pDimension, String pDimensionValue,
			String pSiteId) {

		Map<String, String> siteMap = (Map<String, String>) pDimension.get(pSiteId);
		if (null == siteMap) {
			siteMap = new HashMap<String, String>();
			pDimension.put(pSiteId, siteMap);
		}
		siteMap.put(pDimensionValue, pDimlocationId);
	}

	/**
	 * Gets site ids for the category
	 * 
	 * @param pCatId
	 * @return result
	 * @throws RepositoryException
	 */
	@SuppressWarnings("unchecked")
	public Set<String> getSiteIdsForCategory(String pCatId) {
		Set<String> result = null;
		if (!StringUtils.isBlank(pCatId) && null != getProductCatalog()) {
			try {
				RepositoryItem cat = getProductCatalog().getItem(pCatId, CATEGORY);
				if (null != cat) {
					result = (Set<String>) cat.getPropertyValue(SITE_IDS);
				}
			} catch (RepositoryException e) {
				vlogError(e, e.getMessage());
			}
		}

		return result;
	}

	/**
	 * Method used to put the dimension value to the cache
	 * 
	 * @param pDimLocation
	 */
	public void putCacheValueIntoDimension(DimLocation pDimLocation) {
		if (null != pDimLocation) {
			String dimlocationId = Long.toString(pDimLocation.getDimValue().getId());
			String dimensionName = pDimLocation.getDimValue().getDimensionName();
			if (null != getSeoTools()) {
				dimensionName = getSeoTools().getSeoDimensionName(dimensionName);
			}
			String dimensionValue = getSeoTools().cleanDimensionName(pDimLocation.getDimValue().getName());
			if (!StringUtils.isBlank(dimlocationId) && !StringUtils.isBlank(dimensionName)
					&& !StringUtils.isBlank(dimensionValue)) {
				Map<String, Object> dimension = getCache().get(dimensionName);
				if (null == dimension) {
					dimension = new ConcurrentHashMap<String, Object>();
					getCache().put(dimensionName, (ConcurrentHashMap<String, Object>) dimension);
				}
				dimension.put(dimensionValue, dimlocationId);
			}
		}
	}

	/**
	 * Prints the cache to the log
	 */
	public void printCache() {
		for (String key : getCache().keySet()) {
			Map<String, Object> map = getCache().get(key);
			if (null != map) {
				logInfo(key);
				logInfo(map.toString());
			}
		}
	}

	/**
	 * Clears the cache
	 */
	public void flush() {
		getCache().clear();
	}

	/**
	 * Refreshes the reverse cache
	 */
	public void refreshReverseCache() {
		try {
			getAssemblerTools().invokeAssembler("DimensionValueCacheRefresh");
		} catch (AssemblerException e) {
			vlogError(e, "An exception occurred refreshing the cache.", new Object[0]);
		}
	}

	/**
	 * @return the mCache
	 */
	public ConcurrentHashMap<String, ConcurrentHashMap<String, Object>> getCache() {
		return mCache;
	}

	/**
	 * @param mCache
	 *            the pCache to set
	 */
	public void setCache(ConcurrentHashMap<String, ConcurrentHashMap<String, Object>> pCache) {
		if (pCache == null) {
			pCache = new ConcurrentHashMap<String, ConcurrentHashMap<String, Object>>();
		}
		this.mCache = pCache;
	}

	/**
	 * @return the mSeoTools
	 */
	public SeoTools getSeoTools() {
		return mSeoTools;
	}

	/**
	 * @param mSeoTools
	 *            the pSeoTools to set
	 */
	public void setSeoTools(SeoTools pSeoTools) {
		this.mSeoTools = pSeoTools;
	}

	/**
	 * @return the mProductCatalog
	 */
	public Repository getProductCatalog() {
		return mProductCatalog;
	}

	/**
	 * @param mProductCatalog
	 *            the pProductCatalog to set
	 */
	public void setProductCatalog(Repository pProductCatalog) {
		this.mProductCatalog = pProductCatalog;
	}

	/**
	 * @return the mAssemblerTools
	 */
	public AssemblerTools getAssemblerTools() {
		return mAssemblerTools;
	}

	/**
	 * @param mAssemblerTools
	 *            the pAssemblerTools to set
	 */
	public void setAssemblerTools(AssemblerTools pAssemblerTools) {
		this.mAssemblerTools = pAssemblerTools;
	}
}