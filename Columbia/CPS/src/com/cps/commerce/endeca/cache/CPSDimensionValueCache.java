package com.cps.commerce.endeca.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheObject;

public class CPSDimensionValueCache extends DimensionValueCache {

	Map<String, List<DimensionValueCacheObject>> mCache;
	Map<String, DimensionValueCacheObject> mDimvalToCacheEntry;
	ReadWriteLock mCacheLock;
	ReadWriteLock mDimvalToCacheEntryLock;
	
	public CPSDimensionValueCache() {
		mCache = new HashMap<String, List<DimensionValueCacheObject>>();
		mDimvalToCacheEntry = new HashMap<>();
		mCacheLock = new ReentrantReadWriteLock();
		mDimvalToCacheEntryLock = new ReentrantReadWriteLock();
	}
	
	public boolean put(String pRepositoryId, String pDimvalId, String pURL, List<String> pAncestorRepositoryIds,
					   String pDimensionName) {
		mCacheLock.writeLock().lock();
		mDimvalToCacheEntryLock.writeLock().lock();
		try {
			boolean result = put(pRepositoryId, pDimvalId, pURL, pAncestorRepositoryIds);

			if (result) {
				List<DimensionValueCacheObject> dimCacheValues = get(pRepositoryId);
				if (dimCacheValues != null && dimCacheValues.size() > 0) {
					for (DimensionValueCacheObject dimensionValueCacheObject : dimCacheValues) {
						if (pDimvalId.equals(dimensionValueCacheObject.getDimvalId())) {
							((CPSDimensionValueCacheObject) dimensionValueCacheObject).setDimensionName(pDimensionName);
						}
					}
				}
			}
			return result;
		} finally {
			mDimvalToCacheEntryLock.writeLock().unlock();
			mCacheLock.writeLock().unlock();
		}
	}

	public List<DimensionValueCacheObject> get(String pRepositoryId) {
		mCacheLock.readLock().lock();
		try {
			if ((pRepositoryId == null) || (pRepositoryId.isEmpty())) {
				vlogError("Repository id is null or empty, this cannot be used as a cache key", new Object[0]);
				return null;
			}

			return this.mCache.get(pRepositoryId);
		} finally {
			mCacheLock.readLock().unlock();
		}
	}

	public DimensionValueCacheObject getCachedObjectForDimval(String pDimvalId) {
		mDimvalToCacheEntryLock.readLock().lock();
		try {
		if (pDimvalId == null) {
			return null;
		}

		DimensionValueCacheObject result = (DimensionValueCacheObject) this.mDimvalToCacheEntry.get(pDimvalId);

		return result;
		} finally {
			mDimvalToCacheEntryLock.readLock().unlock();
		}
	}

	public boolean put(String pRepositoryId, String pDimvalId, String pURL, List<String> pAncestorRepositoryIds) {
		mCacheLock.writeLock().lock();
		mDimvalToCacheEntryLock.writeLock().lock();
		try {
			if ((pRepositoryId == null) || (pRepositoryId.length() == 0) || (pDimvalId == null)
					|| (pDimvalId.length() == 0)) {
				vlogError("The repository Id and dimension value Id are required parameters", new Object[0]);
				return false;
			}

			DimensionValueCacheObject newEntry = createDimvalCacheEntry(pRepositoryId, pDimvalId, pURL,
					pAncestorRepositoryIds);

			if (this.mCache.containsKey(pRepositoryId)) {
				List existingCachedValues = (List) this.mCache.get(pRepositoryId);

				int match = -1;
				for (int i = 0; i < existingCachedValues.size(); ++i) {
					if (((DimensionValueCacheObject) existingCachedValues.get(i)).getDimvalId().equals(pDimvalId)) {
						match = i;
						break;
					}

				}

				if (match != -1) {
					existingCachedValues.set(match, newEntry);
				} else {
					existingCachedValues.add(newEntry);
				}

			} else {
				List cachedInfoForThisItem = new ArrayList(1);

				cachedInfoForThisItem.add(newEntry);
				this.mCache.put(pRepositoryId, cachedInfoForThisItem);
			}

			this.mDimvalToCacheEntry.put(pDimvalId, newEntry);
			return true;
		} finally {
			mDimvalToCacheEntryLock.writeLock().unlock();
			mCacheLock.writeLock().unlock();
		}
	}

	public int getNumCacheEntries() {
		mCacheLock.readLock().lock();
		try {
			return this.mCache.size();
		} finally {
			mCacheLock.readLock().unlock();
		}
	}

	public void flush() {
		mCacheLock.writeLock().lock();
		try {
			this.mCache.clear();
		} finally {
			mCacheLock.writeLock().unlock();
		}
	}

	public String printCache() {
		mCacheLock.readLock().lock();
		try {
			StringBuilder cacheContents = new StringBuilder("Key | Dimval id | URL | Ancestors | Dimension Name");
			cacheContents.append(System.getProperty("line.separator"));

			for (Map.Entry<String, List<DimensionValueCacheObject>> entry : this.mCache.entrySet()) {
				cacheContents.append((String) entry.getKey());
				cacheContents.append(": ");

				List<DimensionValueCacheObject> cachedValues = entry.getValue();
				for (DimensionValueCacheObject cacheValue : cachedValues) {
					cacheContents.append(cacheValue.toString());
					cacheContents.append(" ");
				}
				cacheContents.append(System.getProperty("line.separator"));
			}
			vlogInfo(cacheContents.toString(), new Object[0]);
			return cacheContents.toString();
		} finally {
			mCacheLock.readLock().unlock();
		}
	}
	
	protected DimensionValueCacheObject createDimvalCacheEntry(String pRepositoryId, String pDimvalId, String pURL,
															   List<String> pAncestorRepositoryIds) {

		CPSDimensionValueCacheObject entry = new CPSDimensionValueCacheObject(pRepositoryId, pDimvalId);

		if (pURL != null) {
			entry.setURL(pURL);
		}

		if (pAncestorRepositoryIds != null) {
			entry.setAncestorRepositoryIds(pAncestorRepositoryIds);
		}

		return entry;
	}
}
