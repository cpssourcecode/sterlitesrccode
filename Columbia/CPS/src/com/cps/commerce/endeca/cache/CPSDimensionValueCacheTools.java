package com.cps.commerce.endeca.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import atg.commerce.endeca.cache.DimensionValueCache;
import atg.commerce.endeca.cache.DimensionValueCacheTools;

public class CPSDimensionValueCacheTools extends DimensionValueCacheTools {
	
	private Map<String, Long> mDimensionNameIdMap = new ConcurrentHashMap<String, Long>();
	
	public DimensionValueCache createEmptyCache() {
		return new CPSDimensionValueCache();
	}

	public void swapDimensionNameIdMap(Map<String, Long> pNameIdMap) {
		String key = getAssemblerApplicationConfiguration().getCurrentApplicationKey();
		synchronized (getCacheRefreshLock(key)) {
			mDimensionNameIdMap.clear();
			mDimensionNameIdMap.putAll(pNameIdMap);
		}
	}
	
	public Long getDimensionIdByDimensionName(String pDimensionName) {
		return mDimensionNameIdMap.get(pDimensionName);
	}
	
	public String printDimensionNameIdMap() {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Long> entry : mDimensionNameIdMap.entrySet()) {
			sb.append(entry.getKey());
			sb.append(": ");
			sb.append(entry.getValue());
			sb.append(System.getProperty("line.separator"));
		}

		vlogInfo(sb.toString(), new Object[0]);
		return sb.toString();
	}
	
}