package com.cps.commerce.util;

import atg.commerce.util.TransactionLockFactory;
import atg.repository.RepositoryItem;

/**
 *  TransactionLockFactory extension
 */
public class CPSTransactionLockFactory extends TransactionLockFactory {

    protected String getTransactionLockName() {
        RepositoryItem profile = getProfile();
        if (profile != null) {
            return profile.getRepositoryId();
        } else {
            return String.valueOf(System.currentTimeMillis());
        }
    }

}
