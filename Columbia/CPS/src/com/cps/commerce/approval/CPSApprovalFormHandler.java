package com.cps.commerce.approval;

import atg.commerce.CommerceException;
import atg.commerce.approval.ApprovalFormHandler;
import atg.commerce.order.InvoiceRequest;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderImpl;
import atg.commerce.order.PaymentGroup;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.multisite.SiteContextManager;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import vsg.util.ajax.AjaxUtils;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderTools;
import com.cps.email.CommonEmailSender;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;

import javax.servlet.ServletException;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CPSApprovalFormHandler extends ApprovalFormHandler {

	private String poNumber;
	private String jobName;
	private String orderId;
	
	/**
	 * @return the poNumber
	 */
	public String getPoNumber() {
		return poNumber;
	}

	/**
	 * @param poNumber the poNumber to set
	 */
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the orderId
	 */
	public String getOrderId() {
		return orderId;
	}

	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	private String mOrderIds;

	public String getOrderIds() {
		return mOrderIds;
	}

	public void setOrderIds(String pOrderIds) {
		mOrderIds = pOrderIds;
	}

	private OrderHolder mShoppingCart;

	public void setShoppingCart(OrderHolder pShoppingCart) {
		mShoppingCart = pShoppingCart;
	}

	public OrderHolder getShoppingCart() {
		return mShoppingCart;
	}

	private Profile mProfile;

	public Profile getProfile() {
		return mProfile;
	}

	public void setProfile(Profile pProfile) {
		mProfile = pProfile;
	}

	private String mResubmitOrderState;

	public String getResubmitOrderState() {
		return mResubmitOrderState;
	}

	public void setResubmitOrderState(String pResubmitOrderState) {
		mResubmitOrderState = pResubmitOrderState;
	}

	private String mDeletedOrderState;

	public String getDeletedOrderState() {
		return mDeletedOrderState;
	}

	public void setDeletedOrderState(String pDeletedOrderState) {
		mDeletedOrderState = pDeletedOrderState;
	}

	private String mIncompleteOrderState;

	public String getIncompleteOrderState() {
		return mIncompleteOrderState;
	}

	public void setIncompleteOrderState(String pIncompleteOrderState) {
		mIncompleteOrderState = pIncompleteOrderState;
	}

	private CommonEmailSender mEmailSender;

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	private ApprovalManager mApprovalManager;

	public ApprovalManager getApprovalManager() {
		return mApprovalManager;
	}

	public void setApprovalManager(ApprovalManager pApprovalManager) {
		mApprovalManager = pApprovalManager;
	}

	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public Set<Order> getOrders() throws CommerceException {

		Set<Order> orders = new HashSet<Order>();
		if (!StringUtils.isBlank(getOrderIds())) {
			String[] ids = getOrderIds().split(",");
			for (String id : ids) {
				orders.add(loadOrder(id));
			}
		}
		return orders;

	}

	public void approveOrder(Order pOrder, RepositoryItem pProfile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		//super.approveOrder(pOrder, pProfile, pRequest, pResponse);
		if (!getFormError()) {
			if (!pOrder.getApproverIds().contains(getProfile().getRepositoryId())) {
				pOrder.getApproverIds().add(getProfile().getRepositoryId());
			}
			pOrder.setState(getOrderStates().getStateValue(getApprovedOrderState().toLowerCase()));
			pOrder.setStateDetail("The order was approved.");

			// Send e-mail notification
			getEmailSender().sendOrderApprovedEmail(pProfile, pOrder);
			String siteId = SiteContextManager.getCurrentSiteId();
			getEmailSender().sendOrderConfirmationEmail(pProfile, pOrder, siteId);
			sendAdminNotificationEmail(pOrder, siteId);
		}

	}

	public boolean handleApproveOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSApprovalFormHandler.handleApproveOrder";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				super.handleApproveOrder(pRequest, pResponse);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}

	public void rejectOrder(Order pOrder, RepositoryItem pProfile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		//super.rejectOrder(pOrder, pProfile, pRequest, pResponse);
		if (!getFormError()) {
			if (!pOrder.getApproverIds().contains(getProfile().getRepositoryId())) {
				pOrder.getApproverIds().add(getProfile().getRepositoryId());
			}
			String message = getApproverMessage();
			if (!StringUtils.isBlank(message)) {
				pOrder.getApproverMessages().add(message.trim());
			}
			pOrder.setState(getOrderStates().getStateValue(getRejectedOrderState().toLowerCase()));
			pOrder.setStateDetail("The order was rejected.");
			getApprovalManager().resetAuthorization(pOrder);
			getEmailSender().sendOrderRejectedEmail(pProfile, getProfile(), pOrder, message.trim());
		}

	}

	public boolean handleRejectOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSApprovalFormHandler.handleRejectOrder";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				super.handleRejectOrder(pRequest, pResponse);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}

	public boolean handleApproveOrders(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSApprovalFormHandler.handleApproveOrders";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				TransactionDemarcation td = new TransactionDemarcation();
				try {
					td.begin(getTransactionManager(), td.REQUIRED);
					Repository profileRepository = getOrderManager().getOrderTools().getProfileRepository();
					Set<Order> orders = getOrders();
					for (Order order : orders) {
						RepositoryItem owner = profileRepository.getItem(order.getProfileId(), CPSConstants.USER);
						synchronized (order) {
							approveOrder(order, owner, pRequest, pResponse);
							updateOrder(order);
						}
					}
				} catch (RepositoryException re) {
					processException(re, MSG_UNABLE_TO_APPROVE_ORDER, pRequest, pResponse);
				} finally {
					td.end();
				}
			} catch (Exception e) {
				processException(e, MSG_UNABLE_TO_APPROVE_ORDER, pRequest, pResponse);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}

	public boolean handleRejectOrders(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSApprovalFormHandler.handleRejectOrders";

		if (rrm != null && rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				TransactionDemarcation td = new TransactionDemarcation();
				try {
					td.begin(getTransactionManager(), td.REQUIRED);
					Repository profileRepository = getOrderManager().getOrderTools().getProfileRepository();
					Set<Order> orders = getOrders();
					for (Order order : orders) {
						RepositoryItem owner = profileRepository.getItem(order.getProfileId(), CPSConstants.USER);
						synchronized (order) {
							rejectOrder(order, owner, pRequest, pResponse);
							updateOrder(order);
						}
					}
				} catch (RepositoryException re) {
					processException(re, MSG_UNABLE_TO_REJECT_ORDER, pRequest, pResponse);
				} finally {
					td.end();
				}
			} catch (Exception e) {
				processException(e, MSG_UNABLE_TO_REJECT_ORDER, pRequest, pResponse);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}

	public boolean handleResubmitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSApprovalFormHandler.handleResubmitOrder";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				TransactionDemarcation td = new TransactionDemarcation();
				try {
					td.begin(getTransactionManager(), td.REQUIRED);
					Order order = getOrder();
					synchronized (order) {
						Calendar c = Calendar.getInstance();
						order.setState(getOrderStates().getStateValue(getResubmitOrderState().toLowerCase()));
						((OrderImpl) order).setSubmittedDate(c.getTime());
						updateOrder(order);
						getApprovalManager().saveApprovalRequest(order);

						List<String> approverIds = order.getAuthorizedApproverIds();
						for (String id : approverIds) {
							try {
								RepositoryItem approver = getOrderManager().getOrderTools().getProfileTools().getProfileItem(id);
								if (approver != null) {
									getEmailSender().sendOrderApprovalRequiredEmail(approver, getProfile(), order);
									//getEmailSender().sendOrderApprovalPendingEmail(approver, getProfile(), order);
								}
							} catch (RepositoryException re) {
								if (isLoggingError()) {
									logError(re);
								}
							}
						}
					}
				} finally {
					td.end();
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				addFormException(new DropletException("Unable to resubmit order", e));
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}

	public boolean handleDeleteOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSApprovalFormHandler.handleDeleteOrder";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				TransactionDemarcation td = new TransactionDemarcation();
				try {
					td.begin(getTransactionManager(), td.REQUIRED);
					Order order = getOrder();
					synchronized (order) {
						order.setState(getOrderStates().getStateValue(getDeletedOrderState().toLowerCase()));
						updateOrder(order);
					}
				} finally {
					td.end();
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				addFormException(new DropletException("Unable to delete order", e));
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return ((CPSOrderTools) getOrderManager().getOrderTools()).processAjaxResponse(pResponse, this);
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws IOException
	 * @throws ServletException
	 */
	public boolean handleUpdateOrderDetails(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSApprovalFormHandler.handleUpdateOrderDetails";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				//form validation
				preUpdateOrderDetails(pRequest, pResponse);
				
				if (!getFormError()) {
					try {
						String orderId = getOrderId();
						Order order = loadOrder(orderId);
						synchronized (order) {
							if (StringUtils.isNotEmpty(getJobName().trim())) {
								((CPSOrderImpl) order).setJobName(getJobName().trim());
								String concatenatedPoJobName = new StringBuilder(getPoNumber()).append("-")
										.append(getJobName()).toString();
								if (concatenatedPoJobName.length() <= 25) {
									((CPSOrderImpl) order).setConcatenatedPoJobName(concatenatedPoJobName);
								} else {
									((CPSOrderImpl) order).setConcatenatedPoJobName(concatenatedPoJobName.substring(0, 25));
								}
							} else {
								((CPSOrderImpl) order).setConcatenatedPoJobName(getPoNumber());
								((CPSOrderImpl) order).setJobName(getJobName().trim());
							}

							List<PaymentGroup> paymentGroups = order.getPaymentGroups();
							for (PaymentGroup paymentGroup : paymentGroups) {
								if (paymentGroup instanceof InvoiceRequest) {
									InvoiceRequest invoiceRequest = (InvoiceRequest) paymentGroup;
									String ponumber = invoiceRequest.getPONumber();
									invoiceRequest.setPONumber(getPoNumber());
								}
							}
							updateOrder(order);
						}
					} catch (CommerceException e) {
						e.printStackTrace();
					}
				}
				
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		if (AjaxUtils.isAjaxRequest(pRequest)) {
			Map<String, Object> result = new HashMap<String, Object>();			
			AjaxUtils.addAjaxResponse(this, pResponse, result);
			return false;
		} else {
			return (!getFormError());
		}
	}

	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 * @throws IOException
	 */
	public void preUpdateOrderDetails(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
		if(StringUtils.isBlank(getPoNumber())) {
			CPSMessageUtils.addFormException(this, CPSErrorCodes.ERR_CHECKOUT_EMPTY_PO_NUMBER, pRequest.getLocale(), "approvedPONumber");
        } else if(StringUtils.isNotBlank(getPoNumber())){
        	if(getPoNumber().length()>25){
        		CPSMessageUtils.addFormException(this, CPSErrorCodes.ERR_CHECKOUT_TOO_LONG_PO_NUMBER, pRequest.getLocale(), "approvedPONumber");
        	}/*else if (!getPoNumber().matches(CPSConstants.REG_EXP_NAME)) {
        		CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_CHECKOUT_INVALID_PO_NUMBER, pRequest.getLocale(), null);
        	}*/
        }
		
	}	
	
	  private void sendAdminNotificationEmail(Order order, String siteId) {
		   	 RepositoryItem organization= (RepositoryItem) getProfile().getPropertyValue("currentOrganization");
		        if (organization == null) {
		     	   organization= (RepositoryItem) getProfile().getPropertyValue("parentOrganization");
				}
		        Set<RepositoryItem> allMembersItem = (Set<RepositoryItem>) organization.getPropertyValue("allMembers");
		        Set<RepositoryItem> enabledOrderNotificationUsers = new HashSet<RepositoryItem>();
		        
				try {
					Repository profileRepository = getOrderManager().getOrderTools().getProfileRepository();
					RepositoryItem currentUser = profileRepository.getItem(order.getProfileId(), CPSConstants.USER);
					
					Set roles = null;
					for (RepositoryItem memberItem : allMembersItem) {
						roles = (Set) memberItem.getPropertyValue(CPSConstants.ROLES);
						if (roles.toString().contains(CPSConstants.ROLE_ACCOUNT_ADMIN) && memberItem != null) {
							enabledOrderNotificationUsers = (Set<RepositoryItem>) memberItem.getPropertyValue("notificationEnabledUsers");
							if (enabledOrderNotificationUsers != null && enabledOrderNotificationUsers.size() > 0
									&& enabledOrderNotificationUsers.contains(currentUser)) {
								//send email to admin user
								getEmailSender().sendOrderConfirmationEmailToAdmin(memberItem,currentUser, order, siteId);
								vlogDebug("send email to admin users {0}", memberItem);
							}
						}
					}			
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

}
