package com.cps.commerce.approval;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.InvoiceRequest;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.pricing.PricingTools;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.userprofiling.Profile;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.email.CommonEmailSender;
import com.cps.util.CPSConstants;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;

public class ApprovalManager extends GenericService {

	private static final String ITEM_DESCRIPTOR_APPROVAL_REQUEST = "approvalRequest";
	private static final String REMIND_DATE_PRPTY = "remindDate";
	private static final String ORDER_STATE_INCOMPLETE = "INCOMPLETE";

	private static final String RETRIEVE_REQUESTS_FOR_PROCESSING_RQL_QUERY = "remindDate <=?0";
	private static final String RETRIEVE_ORDERS_RQL_QUERY = "state !=?0 AND submittedDate >=?1 AND submittedDate <=?2 AND profileId =?3 AND id !=?4";
	private CommonEmailSender emailSender;
	private int mRemindHours = 72; //default value

	public void setRemindHours(int pRemindHours) {
		mRemindHours = pRemindHours;
	}

	public int getRemindHours() {
		return mRemindHours;
	}

	private OrderManager mOrderManager;

	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	private boolean mUseOrganizationApprovers;

	public void setUseOrganizationApprovers(boolean pUseOrganizationApprovers) {
		mUseOrganizationApprovers = pUseOrganizationApprovers;
	}

	public boolean isUseOrganizationApprovers() {
		return mUseOrganizationApprovers;
	}

	public Repository getOrderRepository() {
		return getOrderManager().getOrderTools().getOrderRepository();
	}

	public PricingTools getPricingTools() {
		return getOrderManager().getOrderTools().getProfileTools().getPricingTools();
	}

	private Date calculateRemindDate(Date pDate) {
		Calendar c = Calendar.getInstance();
		c.setTime(pDate);
		c.add(Calendar.HOUR, getRemindHours());
		return c.getTime();
	}

	public void updateApprovalRequest(RepositoryItem pItem) {

		try {
			MutableRepository orderRepository = (MutableRepository)getOrderRepository();
			MutableRepositoryItem item = (MutableRepositoryItem) pItem;
			Calendar c = Calendar.getInstance();
			item.setPropertyValue(REMIND_DATE_PRPTY, calculateRemindDate(c.getTime()));
			orderRepository.updateItem(item);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}

	}

	public void saveApprovalRequest(String pId, Date pDate) {

		try {
			MutableRepository orderRepository = (MutableRepository)getOrderRepository();
			MutableRepositoryItem item = (MutableRepositoryItem) orderRepository.getItem(pId, ITEM_DESCRIPTOR_APPROVAL_REQUEST);
			if (item == null) {
				item = orderRepository.createItem(pId, ITEM_DESCRIPTOR_APPROVAL_REQUEST);
				orderRepository.addItem(item);
			}
			item.setPropertyValue(REMIND_DATE_PRPTY, calculateRemindDate(pDate));
			orderRepository.updateItem(item);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}

	}

	public void saveApprovalRequest(Order pOrder) {
		saveApprovalRequest(pOrder.getId(), pOrder.getSubmittedDate());
	}

	public RepositoryItem[] getApprovalRequests() {

		RepositoryItem[] items = null;
		Calendar currentDate = Calendar.getInstance();
		Object[] params = new Object[1];
		params[0] = new Timestamp(currentDate.getTime().getTime());

		try {
			RepositoryView view = getOrderManager().getOrderTools().getOrderRepository().getView(ITEM_DESCRIPTOR_APPROVAL_REQUEST);
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(RETRIEVE_REQUESTS_FOR_PROCESSING_RQL_QUERY);
			items = rqlStatement.executeQuery(view, params);
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("getApprovalRequests: Could not get items from repository", e);
			}
		}

		return items;

	}

	public void deleteApprovalRequest(String pItemId) {

		try {
			((MutableRepository) getOrderRepository()).removeItem(pItemId, ITEM_DESCRIPTOR_APPROVAL_REQUEST);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}

	}

	private double calculateOrderTotals(String pProfileId, Date pStartDate, Date pEndDate, Order pOrder) {
		double result = 0.0;
		RepositoryItem[] orders = getOrdersToCheckSpendingLimit(pProfileId, pStartDate, pEndDate, pOrder.getId());
		if (orders != null) {
			for (RepositoryItem order : orders) {
				Double orderTotal = (Double)order.getPropertyValue(CPSConstants.ORDER_TOTAL);
				if (orderTotal == null) {
					vlogDebug("calculateOrderTotals: order {0}, old order - skip calculation", order.getRepositoryId());
					orderTotal = 0.0;
				}
				vlogDebug("calculateOrderTotals: order {0}, total {1}", order.getRepositoryId(), orderTotal);
				result += orderTotal;
			}
		}

		vlogDebug("calculateOrderTotals: result {0}", result);

		return getPricingTools().round(result);
	}

	private void setStartOfTheDay(Calendar pDate){
		pDate.set(Calendar.HOUR_OF_DAY, 0);
		pDate.set(Calendar.MINUTE, 0);
		pDate.set(Calendar.SECOND, 0);
		pDate.set(Calendar.MILLISECOND, 0);
	}

	private boolean checkApprovalRequiredPerOrder(Order pOrder, double pSpendingLimit) {
		vlogDebug("checkApprovalRequired: order {0}, orderTotal {1}, spendingLimit {2}", pOrder.getId(), pOrder.getPriceInfo().getTotal(), pSpendingLimit);
		return (pOrder.getPriceInfo().getTotal() > pSpendingLimit);
	}

	private boolean checkApprovalRequiredPerDay(Order pOrder, RepositoryItem pProfile, Double pSpendingLimit) {
		Calendar startDate = Calendar.getInstance();
		setStartOfTheDay(startDate);
		Calendar endDate = Calendar.getInstance();
		return checkApprovalRequired(pProfile.getRepositoryId(), startDate.getTime(), endDate.getTime(), pOrder, pSpendingLimit);
	}

	private boolean checkApprovalRequiredPerWeek(Order pOrder, RepositoryItem pProfile, Double pSpendingLimit) {
		Calendar startDate = Calendar.getInstance();
		startDate.set(Calendar.DAY_OF_WEEK, 2); //Starting from Monday
		setStartOfTheDay(startDate);
		Calendar endDate = Calendar.getInstance();
		return checkApprovalRequired(pProfile.getRepositoryId(), startDate.getTime(), endDate.getTime(), pOrder, pSpendingLimit);
	}

	private boolean checkApprovalRequiredPerMonth(Order pOrder, RepositoryItem pProfile, Double pSpendingLimit) {
		Calendar startDate = Calendar.getInstance();
		startDate.set(Calendar.DAY_OF_MONTH, 1);
		setStartOfTheDay(startDate);
		Calendar endDate = Calendar.getInstance();
		return checkApprovalRequired(pProfile.getRepositoryId(), startDate.getTime(), endDate.getTime(), pOrder, pSpendingLimit);
	}

	private boolean checkApprovalRequired(String pProfileId, Date pStartDate, Date pEndDate, Order pOrder, double pSpendingLimit) {
		double orderTotals = calculateOrderTotals(pProfileId, pStartDate, pEndDate, pOrder);
		vlogDebug("checkApprovalRequired: order {0}, orderTotal {1}, orderTotals {2}, spendingLimit {3}", pOrder.getId(), pOrder.getPriceInfo().getTotal(), orderTotals, pSpendingLimit);
		return getPricingTools().round(pOrder.getPriceInfo().getTotal() + orderTotals) > pSpendingLimit;
	}

	public boolean isApprovalRequired(Order pOrder, RepositoryItem pProfile) {

		boolean approvalRequired = false;
		if (pProfile != null) {
			Double spendingLimit = (Double)pProfile.getPropertyValue(CPSConstants.ORDER_LIMIT);
			if (spendingLimit != null && spendingLimit > 0.0d) {
				String frequency = (String)pProfile.getPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY);
				if (StringUtils.isBlank(frequency)) {
					frequency = CPSConstants.ORDER_LIMIT_FREQUENCY_ORDER;
				}
				vlogDebug("isApprovalRequired: order {0}, profile {1}, frequency {2}", pOrder.getId(), pProfile.getRepositoryId(), frequency);
				if (CPSConstants.ORDER_LIMIT_FREQUENCY_ORDER.equalsIgnoreCase(frequency)) {
					approvalRequired = checkApprovalRequiredPerOrder(pOrder, spendingLimit);
				} else if (CPSConstants.ORDER_LIMIT_FREQUENCY_DAY.equalsIgnoreCase(frequency)) {
					approvalRequired = checkApprovalRequiredPerDay(pOrder, pProfile, spendingLimit);
				} else if (CPSConstants.ORDER_LIMIT_FREQUENCY_WEEK.equalsIgnoreCase(frequency)) {
					approvalRequired = checkApprovalRequiredPerWeek(pOrder, pProfile, spendingLimit);
				} else if (CPSConstants.ORDER_LIMIT_FREQUENCY_MONTH.equalsIgnoreCase(frequency)) {
					approvalRequired = checkApprovalRequiredPerMonth(pOrder, pProfile, spendingLimit);
				}
			}
		}
		vlogDebug("isApprovalRequired: order {0}, profile {1}, {2}", pOrder.getId(), pProfile.getRepositoryId(), approvalRequired);

		return approvalRequired;

	}

	private RepositoryItem[] getOrdersToCheckSpendingLimit(String pProfileId, Date pStartDate, Date pEndDate, String pOrderId) {

		Object[] params = new Object[5];
		params[0] = ORDER_STATE_INCOMPLETE;
		params[1] = new Timestamp(pStartDate.getTime());
		params[2] = new Timestamp(pEndDate.getTime());
		params[3] = pProfileId;
		params[4] = pOrderId;

		RepositoryItem [] results = null;

		try {
			Repository orderRepository = getOrderManager().getOrderTools().getOrderRepository();
			RepositoryView orderView = orderRepository.getView(ITEM_DESCRIPTOR_ORDER);
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(RETRIEVE_ORDERS_RQL_QUERY);
			results = rqlStatement.executeQuery(orderView, params);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}

		return results;

	}

	public List<RepositoryItem> getApproversForUser(RepositoryItem pProfile) {

		List<RepositoryItem> approvers = null;
		if (isUseOrganizationApprovers()) {
			RepositoryItem parentOrganization = (RepositoryItem)pProfile.getPropertyValue(CPSConstants.PARENT_ORG);
			if (parentOrganization != null) {
				approvers = new ArrayList<RepositoryItem>();
				Set<RepositoryItem> organizationMembers = (Set<RepositoryItem>)parentOrganization.getPropertyValue(CPSConstants.ORGANIZATION_MEMBERS_PRTY);
				for (RepositoryItem organizationMember : organizationMembers) {
					if (isApprover(organizationMember)) {
						approvers.add(organizationMember);
					}
				}
			}
		} else {
			approvers = (List<RepositoryItem>) pProfile.getPropertyValue(CPSConstants.APPROVERS);
		}
		return approvers;

	}

	public boolean isApprover(RepositoryItem pProfile) {
		boolean isApprover = false;
		if (pProfile != null) {
			Set<RepositoryItem> roles = (Set<RepositoryItem>) pProfile.getPropertyValue(CPSConstants.ROLES_PRPTY);
			vlogDebug("User Roles: " + roles);
			if (roles != null) {
				for (RepositoryItem role : roles) {
					if (CPSConstants.ROLE_APPROVER.equalsIgnoreCase((String) role.getPropertyValue(CPSConstants.NAME))){
						isApprover = true;
						break;
					}
				}
			}
		}
		vlogDebug("User is approver: " + isApprover);
		return isApprover;
	}

	public void resetAuthorization(Order pOrder) {
		List<PaymentGroup> paymentGroups = pOrder.getPaymentGroups();
		for (PaymentGroup paymentGroup : paymentGroups) {
			if (paymentGroup instanceof InvoiceRequest) {
				InvoiceRequest invoiceRequest = (InvoiceRequest) paymentGroup;
				List statuses = invoiceRequest.getAuthorizationStatus();
				if (!statuses.isEmpty()) {
					statuses.clear();
				}
				invoiceRequest.setAmountAuthorized(0d);
				break;
			}
		}
	}

	/**
	 * @param abandonedOrderQuery
	 * @param emailInterval
	 */
	public void getAbandonedOrderEmails(String abandonedOrderQuery, int emailInterval) {
		try {
			Object[] params = new Object[2];
			params[0] = ORDER_STATE_INCOMPLETE;
			params[1] = false;
			Repository orderRepository = getOrderManager().getOrderTools().getOrderRepository();
			RepositoryView orderView = orderRepository.getView(ITEM_DESCRIPTOR_ORDER);
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(abandonedOrderQuery);
			RepositoryItem[] results = rqlStatement.executeQuery(orderView, params);
			List<String> userListForUpdate = new ArrayList<String>();
			if (results != null) {
				vlogDebug("query result Orders {0}", results.toString());
				for (RepositoryItem orderItem : results) {
					vlogDebug("Order grtting processed {0}", orderItem.toString());
					String profileId = (String) orderItem.getPropertyValue(CPSConstants.PROFILE_ID);
					vlogDebug("Current order Profile ID {0}", profileId);
					RepositoryItem profileItem = getOrderManager().getOrderTools().getProfileRepository().getItem(profileId, CPSConstants.USER);
					if (profileItem != null) {
						vlogDebug("Profile item form current processing Order {0}", profileItem.toString());
						boolean isNotificationEnabled = (boolean) profileItem.getPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL);
						Date lastLogout = (Timestamp) profileItem.getPropertyValue(CPSConstants.LAST_LOGOUT);
						Date firstCartItemTime = (Timestamp) profileItem.getPropertyValue(CPSConstants.FIRST_ITEM_TO_CURRENT_CART);
						Date cartLastModified = (Timestamp) orderItem.getPropertyValue("lastModifiedDate");
						Date currentTime = new Date();
						if (cartLastModified != null && firstCartItemTime != null) {
							long diffHour = (currentTime.getTime() - firstCartItemTime.getTime()) / 1000 / 60;
							vlogDebug("isNotificationEnabled flag current profile {0}", isNotificationEnabled);
							vlogDebug("lastlogout time of current profile {0}", lastLogout);
							vlogDebug("time difference between logout and current time {0}", diffHour);
							if (diffHour >= emailInterval && isNotificationEnabled
									&& cartLastModified.before(currentTime)) {
								Order order = getOrderManager().loadOrder(orderItem.getRepositoryId());
								if (getEmailSender().sendAbandonedOrderEmailNotification(profileItem, order)) {
									vlogDebug("email sent to current profile {0}and order {1}", profileId,orderItem.getRepositoryId());
									userListForUpdate.add(profileId);
								}
							}
						}

					}
				}
			}
			if (!userListForUpdate.isEmpty()) {
				vlogDebug("user list to update {0}", userListForUpdate);
				MutableRepository mutableRepository = (MutableRepository) getOrderManager().getOrderTools()
						.getProfileTools().getProfileRepository();
				for (String userId : userListForUpdate) {
					MutableRepositoryItem mutableUser = mutableRepository.getItemForUpdate(userId, CPSConstants.USER);
					mutableUser.setPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL, false);
					mutableUser.setPropertyValue(CPSConstants.FIRST_ITEM_TO_CURRENT_CART, null);
					vlogDebug("reseting flag after email sent {0}",
							mutableUser.getPropertyValue(CPSConstants.ORDER_NOTIFICATION_EMAIL));
				}
			}
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		} catch (CommerceException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
	}

	/**
	 * @return emailSender
	 */
	public CommonEmailSender getEmailSender() {
		return emailSender;
	}

	/**
	 * @param emailSender
	 */
	public void setEmailSender(CommonEmailSender emailSender) {
		this.emailSender = emailSender;
	}

}
