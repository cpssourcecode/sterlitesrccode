package com.cps.commerce.inventory;

import java.util.List;

import javax.transaction.TransactionManager;

import com.cps.commerce.order.purchase.CPSPriceAvailability;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import atg.commerce.inventory.InventoryException;
import atg.commerce.inventory.RepositoryInventoryManager;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;


public class CPSInventoryManager extends RepositoryInventoryManager {

	static final String INVENTORY_LEVEL_REP = "inventoryLevel";
	static final String INVENTORY_STOCK_LEVEL = "stockLevel";
	static final String AVAILABILITY_STATUS = "availabilityStatus";
	static final String STOCK_ITEMS_MSG = "Available for Delivery";
	static final String SHIPS_FROM_MANUF_MSG = "Ships from Manufacturer";
	static final String PARTIALLY_AVAL_MSG = "Partially Available";

	static final String STOCKING_TYPE_STOCK = "S";
	static final String STOCKING_TYPE_USEUP = "U";
	static final int INSTOCK_STATUS_VALUE = 1000;
	static final int USEUP_STATUS_VALUE = 1106;
	static final String INVENTORY_ITEM_DESC_NAME = "inventory";
	final static String INVENTORY_CREATION_DATE = "creationDate";
	final static String CAT_REF_ID_PROP_NAME = "catalogRefId";
	final static String DISPLAY_NAME_PROP_NAME = "displayName";

	private boolean mTesting;

	public boolean isTesting() {
		return mTesting;
	}

	public void setTesting(boolean pTesting) {
		mTesting = pTesting;
	}

	public long getStockLevel(String pSkuId) throws InventoryException {

		if (isLoggingDebug()) {
			logDebug("CPSInventoryManager start: skuId=" + pSkuId);
		}
		if (isTesting()) {
			return -1;
		}
		long stockLevel = 0;
		RepositoryItem inventory;
		try {
			inventory = getInventoryItem(pSkuId);
		} catch (RepositoryException e) {
			throw new InventoryException(e);
		}
		stockLevel = getInventoryLevel(inventory, getStockLevelPropertyName(), getDefaultStockLevel());

		if (isLoggingDebug()) {
			logDebug("stockLevel=" + stockLevel);
		}
		if (stockLevel == -1) {
			if (isLoggingDebug()) {
				logDebug("item stockLevel=" + stockLevel);
			}
		}
		return stockLevel;

	}

	public String getStockMessage(String pSkuId, Order order, List<CPSPriceAvailability> list, boolean isCart)
			throws InventoryException {
		if (isLoggingDebug()) {
			logDebug("getStockMessage start: skuId=" + pSkuId);
			logDebug("....: order=" + order);
			logDebug("....: isCart=" + isCart);
		}
		long stockLevel = 0;
		long qtyInTheCart = 0;
		String stockMsg = "";
		RepositoryItem inventory;
		try {
			inventory = getInventoryItem(pSkuId);
		} catch (RepositoryException e) {
			throw new InventoryException(e);
		}
		stockLevel = getInventoryLevel(inventory, getStockLevelPropertyName(), getDefaultStockLevel());

		if (isLoggingDebug()) {
			logDebug("stockLevel=" + stockLevel);
		}
		if (isCart) {
			qtyInTheCart = skuQtyInTheCart(order, pSkuId, null);
		} else {
			if (list != null && !list.isEmpty()) {
				for (CPSPriceAvailability paItem : list) {
					String paSkuId = paItem.getSku();
					if (!StringUtils.isBlank(paSkuId) && paSkuId.equalsIgnoreCase(pSkuId)) {
						Long paQty = paItem.getQty();
						if (paQty != null) {
							qtyInTheCart = qtyInTheCart + paItem.getQty();
						}
					}
				}
			}
		}

		if (isLoggingDebug()) {
			logDebug("qtyInTheCart=" + qtyInTheCart);
		}

		if (stockLevel <= 0) {
			stockMsg = SHIPS_FROM_MANUF_MSG;
		} else if (stockLevel < qtyInTheCart) {
			stockMsg = PARTIALLY_AVAL_MSG;
		} else {
			stockMsg = STOCK_ITEMS_MSG;
		}

		return stockMsg;
	}


	protected long getInventoryLevel(RepositoryItem inventory, String inventoryLevelPropertyName, long defaultLevel)
			throws InventoryException {
		long level = 0;
		Long value = (Long) getPropertyValue(inventory, inventoryLevelPropertyName);
		if (value == null) {
			level = defaultLevel;
		} else {
			level = value.longValue();
		}

		return level;
	}

	protected Object getPropertyValue(RepositoryItem item, String propertyName)
			throws InventoryException {
		if ((item != null) && (propertyName != null)) {
			return item.getPropertyValue(propertyName);
		} else {
			return null;
		}
	}

	public Integer getAvailabilityStatus(String pSkuId)
			throws InventoryException {
		if (isLoggingDebug()) {
			logDebug("PCRInventoryManager start: skuId=" + pSkuId);
		}
		Integer availabilityStatus = null;
		RepositoryItem inventory;
		try {
			inventory = getInventoryItem(pSkuId);
		} catch (RepositoryException e) {
			throw new InventoryException(e);
		}
		if (inventory != null) {
			availabilityStatus = (Integer) inventory.getPropertyValue(AVAILABILITY_STATUS);
		}

		if (isLoggingDebug()) {
			logDebug("availabilityStatus=" + availabilityStatus);
		}

		return availabilityStatus;
	}


	public boolean isRemoveCartItem(Order pOrder, String pSkuId, List<String> arrayListToRemove) {
		boolean remove = false;
		//checking if needs to be removed
		remove = !isValidAddToCart(pSkuId, 0, pOrder, arrayListToRemove);
		return remove;
	}

	/**
	 * This method takes skuId as parameter and tries to find the corresponding inventory repository item
	 *
	 * @param pSkuId
	 * @return
	 * @throws InventoryException
	 * @throws RepositoryException
	 */
	public RepositoryItem getInventoryItemBySkuId(String pSkuId) throws InventoryException {

		RepositoryItem inventory;
		try {
			inventory = getInventoryItem(pSkuId);
		} catch (RepositoryException e) {
			throw new InventoryException(e);
		}
		return inventory;
	}


	/**
	 * 1000 = stock item (S from feed)
	 * 1106 = USE-UP (U from feed)
	 *
	 * @param level
	 * @param stockingType
	 * @return
	 */
	public int getAvailabilityStatus(Long level, String stockingType) {
		int availabilityStatus = 0;
		if (stockingType.equalsIgnoreCase(STOCKING_TYPE_STOCK)) {
			availabilityStatus = getAvailabilityStatusInStockValue();
		} else if (stockingType.equalsIgnoreCase(STOCKING_TYPE_USEUP)) {
			availabilityStatus = USEUP_STATUS_VALUE;
		}
		return availabilityStatus;

	}

	public void decrementInventoryLevel(String pSkuId, long qty) {

		if (isLoggingDebug()) {
			logDebug("decrementInventoryLevel start with " + pSkuId + " qty=" + qty);
		}

		TransactionManager transactionManager = getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;
		try {
			td.begin(transactionManager, TransactionDemarcation.REQUIRED);
			MutableRepositoryItem invItem = (MutableRepositoryItem) getInventoryItemBySkuId(pSkuId);
			if (invItem != null) {
				Long invQty = (Long) invItem.getPropertyValue(getStockLevelPropertyName());
				Long newLevel = invQty - qty;
				if (newLevel < 0) {
					newLevel = 0L;
				}
				invItem.setPropertyValue(getStockLevelPropertyName(), newLevel);
			}
			rollback = false;
		} catch (Exception e) {
			rollback = true;
			if (isLoggingError()) {
				logError(e);
			}
		} finally {
			if (null != td) {
				try {
					td.end(rollback);
				} catch (TransactionDemarcationException tde) {
					if (isLoggingError()) {
						logError(tde);
					}
				}
			}
		}
	}


	/**
	 * Return total quantity of the same item in the cart excluding list ciToExclude
	 *
	 * @param pOrder
	 * @param pSkuId
	 * @param ciToExclude
	 * @return
	 */
	protected long skuQtyInTheCart(Order pOrder, String pSkuId, List<String> ciToExclude) {
		long qtyInTheCart = 0;
		if (pOrder != null) {
			List<CommerceItem> ciList = pOrder.getCommerceItems();
			if (ciList != null && !ciList.isEmpty()) {
				for (CommerceItem item : ciList) {
					String ciSkuId = item.getCatalogRefId();
					if (!StringUtils.isBlank(ciSkuId)
							&& ciSkuId.equalsIgnoreCase(pSkuId)
							&& !(ciToExclude != null && ciToExclude.contains(item.getId()))) {
						qtyInTheCart = qtyInTheCart + item.getQuantity();
					}
				}
			}
		}
		return qtyInTheCart;
	}


	/**
	 * Check to see if item can be added (or needs to be removed) to the cart
	 * Checking first inventory status then checking all commerce items in the cart and getting combine quantity
	 * for the same sku.
	 *
	 * @param skuId
	 * @param qtyRequested
	 * @param order
	 * @param ciToExclude
	 * @return boolean
	 */
	public boolean isValidAddToCart(String skuId, long qtyRequested, Order order, List<String> ciToExclude) {

		boolean canAdd = true;

		if (isTesting()) {
			return canAdd;
		}
		RepositoryItem inventory = null;
		try {
			inventory = getInventoryItem(skuId);
			if (inventory != null) {
				Integer status = (Integer) inventory.getPropertyValue(getAvailabilityStatusPropertyName());
				// checking only if status is '1106' (USE_UP)
				//validation checks were moved to obsolete statuses on product level (O,U,K)
				if (status > 0 && status == USEUP_STATUS_VALUE) {
					//status is use-up. Not allowing add to cart if adding more than in the inventory
					long stockLevel = getInventoryLevel(inventory, getStockLevelPropertyName(), getDefaultStockLevel());
					long qtyInTheCart = 0;
					if (order != null) {
						qtyInTheCart = skuQtyInTheCart(order, skuId, ciToExclude);
					}
					if ((qtyInTheCart + qtyRequested) > stockLevel) {
						//do not allow to add this item
						canAdd = false;
					}
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(e);
			}
		} catch (InventoryException ex) {
			if (isLoggingError()) {
				logError(ex);
			}
		}
		return canAdd;
	}

	public boolean isValidUpdateCart(CommerceItem ci, long qtyRequested, Order order) {

		boolean canAdd = true;

		if (isTesting()) {
			return canAdd;
		}
		RepositoryItem inventory = null;
		String skuId = null;
		String ciId = "";
		if (ci != null) {
			skuId = ci.getCatalogRefId();
			ciId = ci.getId();
		}
		try {
			inventory = getInventoryItem(skuId);
			if (inventory != null) {
				Integer status = (Integer) inventory.getPropertyValue(getAvailabilityStatusPropertyName());
				if (status > 0 && status == USEUP_STATUS_VALUE) {
					//status is use-up. Not allowing add to cart if adding more than in the inventory
					long stockLevel = getInventoryLevel(inventory, getStockLevelPropertyName(), getDefaultStockLevel());
					long qtyInTheCart = 0;
					if (order != null) {
						List<CommerceItem> ciList = order.getCommerceItems();
						if (ciList != null && !ciList.isEmpty()) {
							for (CommerceItem item : ciList) {
								String ciSkuId = item.getCatalogRefId();
								if (!StringUtils.isBlank(ciSkuId) && ciSkuId.equalsIgnoreCase(skuId) && !(item.getId()).equalsIgnoreCase(ciId)) {
									qtyInTheCart = qtyInTheCart + item.getQuantity();
								}
							}
						}
					}
					if ((qtyInTheCart + qtyRequested) > stockLevel) {
						//do not allow to add this item
						canAdd = false;
					}
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(e);
			}
		} catch (InventoryException ex) {
			if (isLoggingError()) {
				logError(ex);
			}
		}
		return canAdd;
	}


}
