package com.cps.commerce.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cps.integrations.inventory.CPSDummyInventoryClient;
import com.cps.integrations.inventory.CPSInventoryClient;
import com.cps.inventory.InventoryAvailabilityInfo;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.nucleus.GenericService;

/**
 * CPSInventoryConnectionManager
 * <p/>
 * <h4>Description</h4>
 * <p/>
 * <h4>Notes</h4>
 *
 * @author Steve Neverov
 */
public class CPSInventoryConnectionManager extends GenericService {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/commerce/inventory/CPSInventoryConnectionManager";

    /**
     * Client property
     */
    private CPSInventoryClient mClient;

    /**
     * @return
     */
    public CPSInventoryClient getClient() {
        return mClient;
    }

    /**
     * @param pClient
     */
    public void setClient(CPSInventoryClient pClient) {
        mClient = pClient;
    }

    /**
     * Dummy Client property
     */
    private CPSDummyInventoryClient mDummyClient;

    /**
     * @return
     */
    public CPSDummyInventoryClient getDummyClient() {
        return mDummyClient;
    }

    /**
     * @param pDummyClient
     */
    public void setDummyClient(CPSDummyInventoryClient pDummyClient) {
        mDummyClient = pDummyClient;
    }

    /**
     * This method will request quantities with the inventory client based on an order.
     * <p/>
     * determine if storeId will be the actual identifier with JDE or if it's a repository id that must query the store repository to get the correct identifier
     * for the inventory check
     *
     * @param pOrder
     * @param pStoreId
     * @return
     * @throws Exception
     */
    public Map<String, Integer> requestInventory(Order pOrder, String pStoreId) throws Exception {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - start").toString());
        }
        Map<String, Integer> inventoryMap = null;
        if (pOrder != null && pOrder.getCommerceItemCount() > 0) {
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Find quantities for order:").append(pOrder.getId()).toString());
            }
            // get skuIds from commerceItems
            ArrayList<String> skuList = new ArrayList<String>();
            List<CommerceItem> commerceItems = pOrder.getCommerceItems();
            for (CommerceItem commerceItem : commerceItems) {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append("Found skuId:").append(commerceItem.getCatalogRefId()).append(" on commerceItem:")
                                    .append(commerceItem.getId()).toString());
                }
                skuList.add(commerceItem.getCatalogRefId());
            }
            // call to client if enabled
            if (getClient().getWebServiceConfig().getWebServiceEnabled()) {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append("Service is enabled, use client to get data.").toString());
                }
                // quantity based on sku and storeId
                inventoryMap = getClient().requestInventory(skuList, pStoreId);
            } else {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append("Service is not enabled, use dummy client to mock data.").toString());
                }
                inventoryMap = getDummyClient().requestInventory(skuList, null);
            }

        } else {
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Incoming order is null or has no commerceItems, return null for the map.").toString());
            }
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - exit").toString());
        }
        return inventoryMap;

    }

    /**
     * This method will request quantity with the inventory client based on an sku id.
     *
     * @param pCustomerNumber
     * @param pSkuId
     * @return
     * @throws Exception
     */
    public InventoryAvailabilityInfo requestInventory(String pCustomerNumber, String pSkuId, long pOrderedQty) throws Exception {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - start").toString());
        }
        InventoryAvailabilityInfo info = null;
        // quantity based on sku
        List<InventoryAvailabilityInfo> infos = getClient().requestInventory(pCustomerNumber, pSkuId, true, pOrderedQty, null);
        if (infos != null && infos.size() > 0) {
            info = infos.get(0);
        } else {
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("No inventory information available").toString());
            }
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - exit").toString());
        }
        return info;

    }

    /**
     * This method will request quantity with the inventory client based on an sku id.
     *
     * @param pCustomerNumber
     * @param pSkuId
     * @return
     * @throws Exception
     */
    public List<InventoryAvailabilityInfo> requestInventory(String pCustomerNumber, String pSkuId, String pBranchId, long pOrderedQty) throws Exception {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - start").toString());
        }
        // quantity based on sku
        List<InventoryAvailabilityInfo> infos = getClient().requestInventory(pCustomerNumber, pSkuId, true, pOrderedQty, pBranchId);
        vlogDebug("Availability Infos :: {0}", infos);
        vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - exit").toString());
        return infos;
    }

    /**
     * This method will request quantity with the inventory client based on an sku id and all available branches.
     *
     * @param pCustomerNumber
     * @param pSkuId
     * @return
     * @throws Exception
     */
    public List<InventoryAvailabilityInfo> requestAllInventory(String pCustomerNumber, String pSkuId, long pOrderedQty) throws Exception {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestAllInventory").append(" - start").toString());
        }
        // quantity based on sku
        List<InventoryAvailabilityInfo> infos = getClient().requestInventory(pCustomerNumber, pSkuId, true, pOrderedQty, null);
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestAllInventory").append(" - exit").toString());
        }
        return infos;

    }

}