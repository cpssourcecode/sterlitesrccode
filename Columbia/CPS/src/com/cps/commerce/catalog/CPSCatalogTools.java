package com.cps.commerce.catalog;

import static com.cps.util.CPSConstants.ECOMMERCE_DISPLAY;
import static com.cps.util.CPSConstants.PARENT_CATEGORY;
import static com.cps.util.CPSConstants.PRODUCT_PARENT_CATEGORIES;
import static com.cps.util.CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE;
import static com.cps.util.CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_K;
import static com.cps.util.CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_O;
import static com.cps.util.CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_U;
import static com.cps.util.CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_X;

import java.util.List;
import java.util.Set;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;

import atg.adapter.gsa.GSAItem;
import atg.commerce.catalog.custom.CustomCatalogTools;
import atg.commerce.gifts.GiftlistFormHandler;
import atg.commerce.order.CommerceItem;
import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.multisite.Site;
import atg.multisite.SiteContextManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;

/**
 * The Class CPSCatalogTools extends basic ATG CustomCatalogTools functionality
 *
 * @author Andy Porter
 */


public class CPSCatalogTools extends CustomCatalogTools {
	public static final String BRAND_ITEM_NAME = "brand";
	public static final String COLLECTION_ITEM_NAME = "collection";
	public static final String CATEGORY_ITEM_NAME = "category";

	public RepositoryItem findBrand(String pBrandId) throws RepositoryException {
		return pBrandId != null ? getCatalog().getItem(pBrandId, BRAND_ITEM_NAME) : null;
	}

	public RepositoryItem findCollection(String pCollectionId) throws RepositoryException {
		return pCollectionId != null ? getCatalog().getItem(pCollectionId, COLLECTION_ITEM_NAME) : null;
	}

	public RepositoryItem findCategory(String pCategoryId) throws RepositoryException {
		return pCategoryId != null ? getCatalog().getItem(pCategoryId, CATEGORY_ITEM_NAME) : null;
	}

	public RepositoryItem findCategoryByName(String pCategoryName) throws RepositoryException {
		String rqlQuery = "";
		RepositoryItem[] items = null;
		final Object[] p = new Object[1];

		rqlQuery = "displayName =?0";
		pCategoryName = pCategoryName.replaceAll("-", " ");
		p[0] = pCategoryName;

		final RepositoryView view = getCatalog().getView(CATEGORY_ITEM_NAME);
		final RqlStatement rqlStatement = RqlStatement.parseRqlStatement(rqlQuery);
		items = rqlStatement.executeQuery(view, p);

		if (items != null && items.length > 0) {
			return items[0];
		}
		return null;
	}

	/**
	 * Gets the product name by product id.
	 *
	 * @param pProductId the product id
	 * @return the product name by product id
	 */
	public String getProductNameByProductId(final String pProductId) {
		String productName = pProductId;
		try {
			RepositoryItem product = findProduct(pProductId);
			if (product != null) {
				productName = (String) product.getPropertyValue(CPSConstants.DISPLAY_NAME);
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return productName;
	}

	public boolean isItemValid(CommerceItem pCommerceItem) {

		boolean valid = false;
		RepositoryItem product = (RepositoryItem) pCommerceItem.getAuxiliaryData().getProductRef();
		if (product != null) {
			String stockingType = (String) product.getPropertyValue(PRODUCT_REP_ITEM_STOCKING_TYPE);
			boolean productPurchasable = (boolean) product.getPropertyValue(CPSConstants.PRODUCT_PURCHASABLE);
			boolean isValidStockingType = !(StringUtils.isBlank(stockingType)
					|| CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_O.equalsIgnoreCase(stockingType)
					|| CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_U.equalsIgnoreCase(stockingType)
					|| CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_K.equalsIgnoreCase(stockingType)
					|| CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_X.equalsIgnoreCase(stockingType)
					|| CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR.equalsIgnoreCase(stockingType));
			if (isValidStockingType && productPurchasable) {
				RepositoryItem parentCategory = (RepositoryItem) product.getPropertyValue(PARENT_CATEGORY);
				Set parentCategories = (Set) product.getPropertyValue(PRODUCT_PARENT_CATEGORIES);

				if (parentCategory != null || !parentCategories.isEmpty()) {
					if (parentCategory != null) {
						Boolean eCommerceDisplay = (Boolean) parentCategory.getPropertyValue(ECOMMERCE_DISPLAY);
						if (eCommerceDisplay == null) {
							eCommerceDisplay = false;
						}
						valid = eCommerceDisplay;
					} else {
						GSAItem gsaItem = (GSAItem) parentCategories.iterator().next();
						Boolean eCommerceDisplay = (Boolean) gsaItem.getPropertyValue(ECOMMERCE_DISPLAY);
						if (eCommerceDisplay == null) {
							eCommerceDisplay = false;
						}
						valid = eCommerceDisplay;
					}
				}
			}
		}
		return valid;
	}

	@Override
	public RepositoryItem getCurrentCatalog() {
		RepositoryItem catalog = super.getCurrentCatalog();
		if (catalog == null){
			Site site = SiteContextManager.getCurrentSite();
			if (site != null) {
				catalog = (RepositoryItem) site.getPropertyValue("defaultCatalog");
			}
		}

		return catalog;
	}
	
	
	public boolean isProductPurchasable(RepositoryItem product) {
		boolean purchasable=false;
		purchasable=(boolean) product.getPropertyValue(CPSConstants.PRODUCT_PURCHASABLE) && !CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR.equalsIgnoreCase((String) product.getPropertyValue(CPSConstants.STOCKING_TYPE));
		
		return purchasable;
	}

    public boolean isProductInCategory(RepositoryItem pProduct, String pCategoryId) {
        boolean founded = false;
        try {
            if (pProduct != null && StringUtils.isNotBlank(pCategoryId)) {
                founded = findRecursively(pProduct, pCategoryId);
            }
        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            founded = false;
        }
        return founded;
    }

    public boolean isProductInCategories(RepositoryItem pProduct, List<String> pCategoriesId) {
        boolean founded = false;
        if (pProduct != null) {
            try {
                for (String pCategoryId : pCategoriesId) {
                    founded = findRecursively(pProduct, pCategoryId);
                    if (founded) {
                        break;
                    }
                }
            } catch (Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
                founded = false;
            }
        }
        return founded;
    }

    private boolean findRecursively(RepositoryItem pItem, String pCategoryId) {
        boolean founded;
        if (pItem != null && !org.apache.commons.lang.StringUtils.isEmpty(pCategoryId)) {
            RepositoryItem parentCategory = (RepositoryItem) pItem.getPropertyValue(PARENT_CATEGORY);
            if (parentCategory == null || parentCategory.getRepositoryId() == null) {
                founded = false;
            } else {
                founded = parentCategory.getRepositoryId().trim().equals(pCategoryId.trim()) || findRecursively(parentCategory, pCategoryId);
            }
        } else {
            founded = false;
        }
        return founded;
    }

}