package com.cps.commerce.catalog;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import vsg.util.ajax.AjaxUtils;

import com.endeca.infront.navigation.NavigationState;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * 
 * handler to create new navigation state according to selections on view all modal
 *
 */
public class NavStateFromMultipleRefinementsHandler extends GenericFormHandler {
	
	/** addDimValIds constant */
	private static final String ADD_DIM_VAL_IDS = "addDimValIds";
	/** addDimValIdsSingle constant */
	private static final String ADD_DIM_VAL_IDS_SINGLE = "addDimValIdsSingle";
	/** removeDimValIds constant */
	private static final String REMOVE_DIM_VAL_IDS = "removeDimValIds";
	/** navState constant */
	private static final String NAV_STATE = "navState";
	/** currentUrl constant */
	private static final String CURRENT_URL = "currentUrl";	
	/** the delimeter */
	private static final String DELIMENER = "###";
	/** navigation state */
	private NavigationState mNavigationState;
	/** values */
	private Map<String, String> mValue = new HashMap<String, String>();	
	/** transaction manager */
	private TransactionManager mTransactionManager;
	/**
	 * cache tools
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools;
	
	
	/**
	 * create new navigation state according to selections on view all modal
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleCreateNavigationState(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;
		try {
			try {
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
				
				String url = getValue().get(CURRENT_URL);
				
				url = modifyUrl(url, false);
				url = modifyUrl(url, true);
				
				Map<String, Object> params = new HashMap<String, Object>();
				params.put(NAV_STATE, url);
				
				AjaxUtils.addAjaxResponse(this, pResponse, params);
			} catch (Exception e) {
				rollback = true;
				if (isLoggingError()) {
					logError(e);
				}
			} finally {
				td.end(rollback);
			}
		} catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return false;
	}
	
	/**
	 * modify url
	 * @param isRemove - remove flag
	 */
	private String modifyUrl(String pUrl, boolean isRemove){
		String ids = null;
		if(isRemove){
			ids = getValue().get(REMOVE_DIM_VAL_IDS);
		} else {
			ids = "" + getValue().get(ADD_DIM_VAL_IDS_SINGLE) + getValue().get(ADD_DIM_VAL_IDS);
		}
		return processIds(pUrl, ids, isRemove);
	}
	
	/**
	 * processes ids. remove or add
	 * @param pIds - ids
	 * @param isRemove - remove flag
	 */
	private String processIds(String pUrl, String pIds, boolean isRemove){
		String[] idsArray = null;
		if(!StringUtils.isEmpty(pIds)){
			idsArray = pIds.split(DELIMENER);
			Set<String> set = new LinkedHashSet<String>();
			for(String str : idsArray){
				set.add(str);
			}
			if(set.size() > 0){
				for(String id : set){
					DimensionValueCacheObject dvco = getDimensionValueCacheTools().getCachedObjectForDimval(id);
					String urlSegment = dvco.getUrl();
					if(StringUtils.isNotEmpty(urlSegment) && StringUtils.isNotEmpty(pUrl)){
						if(isRemove){
							pUrl = pUrl.replace(urlSegment, "");
						} else if (!pUrl.contains(urlSegment)) {
							if(pUrl.contains("?")){
								pUrl = pUrl.replace("?", urlSegment + "?");
							} else {
								pUrl = pUrl + urlSegment;
							}
						}
					}
				}
			}
		}
		return pUrl;
	}
	
	public NavigationState getNavigationState() {
		return mNavigationState;
	}

	public void setNavigationState(NavigationState pNavigationState) {
		this.mNavigationState = pNavigationState;
	}
	
	public Map<String, String> getValue() {
		return mValue;
	}

	public void setValue(Map<String, String> pValue) {
		mValue = pValue;
	}
	
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}
	
	public void setDimensionValueCacheTools(
			DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

}
