package com.cps.commerce.catalog;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.catalog.comparison.ProductComparisonList;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.util.CPSTableInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CPSProductComparisonList extends ProductComparisonList {

    @Override
    public boolean add(String pProductId, String pCategoryId, String pSkuId, String pCatalogKey, String pSiteId) throws RepositoryException {

        CPSTableInfo tableInfo = (CPSTableInfo) getTableInfo();

        if(getItems().size() == 0) {
            tableInfo.clearTableColumns();
        }

        boolean added = super.add(pProductId, pCategoryId, pSkuId, pCatalogKey, pSiteId);

        if (added) {
            List<String> propertiesList = new ArrayList<>();

            CatalogTools catalogTools = getCatalogTools();
            RepositoryItem product = catalogTools.findProduct(pProductId, pCatalogKey);
            if (product != null) {
                Map attributeValueMap = (Map) product.getPropertyValue("attributeValueMap");

                vlogDebug("attributeValueMap=" + attributeValueMap);
                if (attributeValueMap != null && !attributeValueMap.isEmpty()) {
                    for (Object attribute : attributeValueMap.keySet()) {
                        try {
                            String key = (String) attribute;
                            String value = (String) attributeValueMap.get(key);
                            if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value)) {
                                propertiesList.add(key);
                            }
                        } catch (Exception e) {
                            logError("Could not read product attribute");
                        }
                    }
                    tableInfo.setAdditionalColumns(propertiesList);
                }
            }
        }
        return added;
    }
}
