package com.cps.droplet;

import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.invoice.InvoiceInfo;
import com.cps.userprofiling.CPSInvoiceConnectionManager;
import com.cps.userprofiling.CPSProfileTools;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupImpl;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

/**
 * Created by
 */
public class InvoicesDroplet extends DynamoServlet {

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final String EMPTY_RESULT = "emptyResult";
	private static final String INVOICES = "invoices";
	private static final String PROFILE = "profile";
	
	private CPSInvoiceConnectionManager mInvoiceConnectionManager;
	private OrderManager orderManager;
	private CPSProfileTools mProfileTools;
	private int mResultSize = 3;
	private RqlStatement profileOrderRql;
	private RqlStatement orgOrderRql;

	public CPSInvoiceConnectionManager getInvoiceConnectionManager() {
		return mInvoiceConnectionManager;
	}

	public void setInvoiceConnectionManager(CPSInvoiceConnectionManager pInvoiceConnectionManager) {
		mInvoiceConnectionManager = pInvoiceConnectionManager;
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public int getResultSize() {
		return mResultSize;
	}

	public void setResultSize(int pResultSize) {
		mResultSize = pResultSize;
	}

	public OrderManager getOrderManager() {
		return orderManager;
	}

	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	

	public RqlStatement getProfileOrderRql() {
		return profileOrderRql;
	}

	public void setProfileOrderRql(RqlStatement profileOrderRql) {
		this.profileOrderRql = profileOrderRql;
	}

	public RqlStatement getOrgOrderRql() {
		return orgOrderRql;
	}

	public void setOrgOrderRql(RqlStatement orgOrderRql) {
		this.orgOrderRql = orgOrderRql;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		RqlStatement orderRql=null;
		Object[] params=null;
		List<String> orderShipTos=new ArrayList<String>();
		List<String> profileShipTos=new ArrayList<String>();


		if (isLoggingDebug()) {
			logDebug("service - start");
		}

		boolean emptyResult = true;

		Profile profile = (Profile) pRequest.getObjectParameter(PROFILE);
		RepositoryItem organization = getProfileTools().getParentOrganization(profile);
		
		
		

		if (organization != null && profile != null) {
			Calendar start = Calendar.getInstance();
			start.add(Calendar.DAY_OF_MONTH, -120);
			Calendar end = Calendar.getInstance();
			
			String profileId=profile.getRepositoryId();  
			String parentOrgId=organization.getRepositoryId();
			
			List<RepositoryItem> csList = new ArrayList<RepositoryItem>();
			
			if(!getProfileTools().initAddressesByProfile(profile,csList)) {
				getProfileTools().initAddressesByOrganization(profile, csList);
				orderRql=getOrgOrderRql();
				params=new Object[]{parentOrgId};
			}else{
				orderRql=getProfileOrderRql();
				params=new Object[]{profileId};
			}
			
			for (RepositoryItem cs : csList) {
				profileShipTos.add(cs.getRepositoryId());
			}
			try {
				//TODO should use RepositoryUtils
				Repository orderRepository = getOrderManager().getOrderTools().getOrderRepository();
				RepositoryView orderView = orderRepository.getView(ITEM_DESCRIPTOR_ORDER);
				RepositoryItem[] orderItems= orderRql.executeQuery(orderView, params );
				if(orderItems != null) {
					for(RepositoryItem orderItem : orderItems) {
						String orderId=orderItem.getRepositoryId();
						Order order=getOrderManager().loadOrder(orderId);
						List<ShippingGroup> shippingGroups=order.getShippingGroups();
						if(shippingGroups != null) {
							ShippingGroup shipGroup=shippingGroups.get(0);
							ShippingGroupImpl sgImpl=(ShippingGroupImpl) shipGroup;
							if(shipGroup instanceof CPSHardgoodShippingGroup) {
								String jdeAddress=(String) sgImpl.getPropertyValue("jdeAddressNumber");
								orderShipTos.add(jdeAddress);
							}
						}
					}
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError(re);
				}
			} catch (CommerceException e) {
				vlogError(e,"Exception while getting orders ");
			} 
			Set<String> allShipTos=new HashSet<String>();
			allShipTos.addAll(profileShipTos);
			allShipTos.addAll(orderShipTos);
			List<String> shipTo=new ArrayList<String>();
			shipTo.addAll(allShipTos);
			vlogDebug("CS Addresses {0}",profileShipTos);
			vlogDebug("Order addresses {0}",orderShipTos);
			vlogDebug("CS Addresses size {0}",profileShipTos.size());
			vlogDebug("Order addresses size {0}",orderShipTos.size());
			vlogDebug("Total addresses size {0}",shipTo.size());
			
			profile.setPropertyValue("csShipTos", shipTo);
		
			try {
				Map<String, Object> result = getInvoiceConnectionManager().requestInvoices(organization.getRepositoryId(), start, end,
						null, null, null, shipTo,null, getResultSize(), 0, null, null);

				if (result != null && result.size() > 0) {
					List<InvoiceInfo> invoices = (List<InvoiceInfo>) result.get(INVOICES);
					pRequest.setParameter(INVOICES, invoices);
					emptyResult = false;
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		} else {
			if (isLoggingDebug()) {
				logDebug("Organization is empty for profile: " + profile);
			}
		}

		pRequest.setParameter(EMPTY_RESULT, emptyResult);
		pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug("service - end");
		}

	}

}
