package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.droplet.ForEach;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Droplet used to generate category tree values.
 * 
 */
public class AllProductsDroplet extends DynamoServlet {

	/** The Constant OUTPUT_OPARAM. */
	private static final String OUTPUT_OPARAM = "output";

	/** The Constant EMPTY_OPARAM. */
	private static final String EMPTY_OPARAM = "empty";

	/** The catalog input param. */
	private static final String CATALOG_PARAM = "catalog";

	/** The rootCategories. */
	private static final String ROOT_CATEGORIES = "rootCategories";

	/** The fixedChildCategories. */
	private static final String FIXED_CHILD_CATEGORIES = "fixedChildCategories";

	/** The rootCategoriesMap. */
	private static final String ROOT_CATEGORIES_MAP = "rootCategoriesMap";

	/** The childCategoriesMap. */
	private static final String CHILD_CATEGORIES_MAP = "childCategoriesMap";

	/** The childCategoriesMap. */
	private static final String CAT_COUNT = "catCount";

	/** The childCategoriesMap. */
	private static final String SUB_CAT_COUNT = "subCatCount";

	/** The all count. */
	private static final String ALL_COUNT = "-1";

	/**
	 * Dimension value cache tools
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools = null;

	/**
	 * Maximum category count limit
	 */
	private int mMaximumCount = 400;

	private CategoryNavigationTreeDroplet mCategoryNavigationTreeDroplet;

	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		Object catalogParam = pRequest.getObjectParameter(CATALOG_PARAM);
		Map<String, String> rootCatsResult = null;
		List<Map<String, RepositoryItem>> subCatsResult = null;
		if (catalogParam instanceof RepositoryItem && null != getDimensionValueCacheTools()) {
			RepositoryItem catalog = (RepositoryItem) catalogParam;
			List<RepositoryItem> rootCats = getSortedCollection(
					(Collection<RepositoryItem>) catalog.getPropertyValue(ROOT_CATEGORIES), pRequest);
			vlogDebug("CategoryNavigation sorted root cats: "+rootCats);
			rootCats = getCatsLimited(rootCats, getCountValue(pRequest.getParameter(CAT_COUNT)));
			vlogDebug("CategoryNavigation limited root cats: "+rootCats+" | cat count: "+pRequest.getParameter(CAT_COUNT));
			rootCatsResult = getRootCatsNavigation(rootCats);
			subCatsResult = getSubCatsNavigation(rootCats, getCountValue(pRequest.getParameter(SUB_CAT_COUNT)),
					pRequest);
		}

		vlogDebug("CategoryNavigation droplet results: RootCats - "+rootCatsResult+" | SubCats - "+subCatsResult);
		if ((null != subCatsResult && !subCatsResult.isEmpty())
				|| (null != rootCatsResult && !rootCatsResult.isEmpty())) {
			pRequest.setParameter(ROOT_CATEGORIES_MAP, rootCatsResult);
			pRequest.setParameter(CHILD_CATEGORIES_MAP, subCatsResult);
			pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		}
	}

	/**
	 * Gets the sorted object
	 * 
	 * @param pObject
	 *            object
	 * @param pRequest
	 *            request
	 * @return result object
	 */
	protected Object getSortedObject(Object pObject, DynamoHttpServletRequest pRequest) {
		Object result = null;
		if (null != pObject) {
			result = ForEach.getSortedArray(this, pObject, pRequest);
		}
		return result;
	}

	/**
	 * Gets the sorted categories collection
	 * 
	 * @param pCollection
	 *            categories
	 * @param pRequest
	 *            request
	 * @return result
	 */
	protected List<RepositoryItem> getSortedCollection(Collection<RepositoryItem> pCollection,
			DynamoHttpServletRequest pRequest) {
		List<RepositoryItem> result = new ArrayList<RepositoryItem>();
		if (null != pCollection && null != pRequest) {
			Object sortedCats = getSortedObject(pCollection, pRequest);
			if (sortedCats instanceof Object[]) {
				Object[] sortedCatsArray = (Object[]) sortedCats;
				Object cat = null;
				for (int i = 0; i < sortedCatsArray.length; i++) {
					cat = sortedCatsArray[i];

					String webCategoryEmptyCategoryId = null;
					if (null != getCategoryNavigationTreeDroplet()) {
						webCategoryEmptyCategoryId = getCategoryNavigationTreeDroplet().getWebCategoryEmptyCategory();
					}

					if ((cat instanceof RepositoryItem) &&
							((null == webCategoryEmptyCategoryId) || ( !((RepositoryItem) cat).getRepositoryId().equalsIgnoreCase(webCategoryEmptyCategoryId)))) {
						result.add((RepositoryItem) cat);
					}
				}

			}
		}
		return result;
	}

	/**
	 * Gets the count int value from string
	 * 
	 * @param pCountvalue
	 *            count value
	 * @return result
	 */
	protected int getCountValue(String pCountvalue) {
		int result = 0;

		try {
			if (!StringUtils.isBlank(pCountvalue)) {
				if (ALL_COUNT.equals(pCountvalue)) {
					result = getMaximumCount();
				} else {
					result = Integer.parseInt(pCountvalue);
				}
			}
		} catch (Exception e) {
			logError(e);
		}
		return result;
	}

	/**
	 * Gets root categories map to print on the web ui
	 * 
	 * @param pRootCats
	 *            root cats
	 * @return result map
	 */
	protected Map<String, String> getRootCatsNavigation(List<RepositoryItem> pRootCats) {
		Map<String, String> result = new LinkedHashMap<String, String>();

		if (null != pRootCats && !pRootCats.isEmpty()) {
			String catId = null;
			DimensionValueCacheObject dvco = null;
			for (RepositoryItem cat : pRootCats) {
				catId = cat.getRepositoryId();
				if (StringUtils.isNotBlank(catId)){
					dvco = getDimensionValueCacheTools().get(catId, null);
				}
				if (null != dvco) {
					result.put(dvco.getUrl(), cat.getItemDisplayName());
				}
			}
		}
		return result;
	}

	/**
	 * Gets sub categories map for root category
	 * 
	 * @param pRootCat
	 *            root cat
	 * @param pSubCatsCount
	 *            sub cats count
	 * @param pRequest
	 *            request
	 * @return result map
	 */
	@SuppressWarnings("unchecked")
	protected Map<String, RepositoryItem> getSubCatsForRoot(RepositoryItem pRootCat, int pSubCatsCount,
			DynamoHttpServletRequest pRequest) {
		Map<String, RepositoryItem> result = new LinkedHashMap<String, RepositoryItem>();

		if (null != pRootCat) {
			List<RepositoryItem> subCats = getSortedCollection(
					(Collection<RepositoryItem>) pRootCat.getPropertyValue(FIXED_CHILD_CATEGORIES), pRequest);
			if (null != subCats && !subCats.isEmpty()) {
				DimensionValueCacheObject dvco = null;
				String catId = null;
				int index = 0;
				for (RepositoryItem cat : subCats) {
					catId = cat.getRepositoryId();
					dvco = getDimensionValueCacheTools().get(catId, null);
					if (null != dvco) {
						index++;
						if (index > pSubCatsCount) {
							break;
						}
						result.put(dvco.getUrl(), cat);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Gets sub categories map to print on the web ui
	 * 
	 * @param pRootCats
	 *            root cats
	 * @param pSubCatsCount
	 *            sub cats count
	 * @param pRequest
	 *            request
	 * @return result map
	 */
	protected List<Map<String, RepositoryItem>> getSubCatsNavigation(List<RepositoryItem> pRootCats,
			int pSubCatsCount, DynamoHttpServletRequest pRequest) {
		List<Map<String, RepositoryItem>> result = new ArrayList<Map<String, RepositoryItem>>();

		if (null != pRootCats && !pRootCats.isEmpty()) {
			Map<String, RepositoryItem> subCats = null;
			for (RepositoryItem rootCat : pRootCats) {
				subCats = getSubCatsForRoot(rootCat, pSubCatsCount, pRequest);
				if (null != subCats && !subCats.isEmpty()) {
					result.add(subCats);
				}
			}
		}
		return result;
	}

	/**
	 * Gets limited size of categories
	 * 
	 * @param pCats
	 *            categories
	 * @param pRootCatsCount
	 *            cats count
	 * @return result set
	 */
	protected List<RepositoryItem> getCatsLimited(List<RepositoryItem> pCats, int pRootCatsCount) {
		List<RepositoryItem> result = new ArrayList<RepositoryItem>();

		if (null != pCats && !pCats.isEmpty()) {
			try {
				int index = 0;
				for (RepositoryItem rootCat : pCats) {
					if (index >= pRootCatsCount) {
						break;
					}
					result.add(rootCat);
					index++;
				}
			} catch (Exception e) {
				logError(e);
			}
		}

		return result;
	}

	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		this.mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	public int getMaximumCount() {
		return mMaximumCount;
	}

	public void setMaximumCount(int pMaximumCount) {
		this.mMaximumCount = pMaximumCount;
	}


	public CategoryNavigationTreeDroplet getCategoryNavigationTreeDroplet() {
		return mCategoryNavigationTreeDroplet;
	}

	public void setCategoryNavigationTreeDroplet(CategoryNavigationTreeDroplet pCategoryNavigationTreeDroplet) {
		mCategoryNavigationTreeDroplet = pCategoryNavigationTreeDroplet;
	}
}