package com.cps.droplet;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;
import atg.userprofiling.Profile;

import com.cps.userprofiling.util.UserRolesAccessRights;
/**
 * Takes in access right key and checks if user has permission
 * Returns true if user has permission for content, false otherwise
 * 
 */
public class AccessRightDroplet extends DynamoServlet {
	
	/** profile param */
	public static ParameterName PROFILE = ParameterName.getParameterName("profile");
	/** accessRightKey param */
	public static ParameterName ACCESS_RIGHT_KEY = ParameterName.getParameterName("accessRightKey");
	/** true param */
	public static ParameterName TRUE = ParameterName.getParameterName("true");
	/** false param */
	public static ParameterName FALSE = ParameterName.getParameterName("false");
	/** output param */
	public static ParameterName OUTPUT = ParameterName.getParameterName("output");
	
	/** user directory */
	private UserDirectory mUserDirectory;
	/** user roles access rights */
	private UserRolesAccessRights mUserRolesAccessRights;
	
	@Override
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		Profile profile = (Profile) pRequest.getObjectParameter(PROFILE);
		String accessRight = pRequest.getParameter(ACCESS_RIGHT_KEY);
		vlogDebug("In role lookup droplet to see if user: " + profile + " has access right: " + accessRight);
		
		if(profile != null){
			User user = getUserDirectory().findUserByPrimaryKey(profile.getRepositoryId());
			if (user != null && !StringUtils.isBlank(accessRight)){
				if (userHasPermission(user, accessRight)){
					pRequest.serviceLocalParameter(TRUE, pRequest, pResponse);
				} else {
					pRequest.serviceLocalParameter(FALSE, pRequest, pResponse);
				}
			} else {
				pRequest.serviceLocalParameter(FALSE, pRequest, pResponse);
			}
		}
		
	}
	
	/**
	 * check if user has permission by key
	 * @param pUser - user
	 * @param pRightKey - key
	 * @return - true if user has permission
	 */
	private boolean userHasPermission (User pUser, String pRightKey){		
		String roles = getUserRolesAccessRights().getRolesAccessRights().get(pRightKey);
		String[] rolesArray = StringUtils.isBlank(roles) ? null : roles.split(";");
		Collection userAssignedRoles=pUser.getAssignedRoles();
        vlogDebug("user assigned roles are -- {0}", userAssignedRoles);
		if(rolesArray != null && rolesArray.length > 0){ 
			for(String role : rolesArray){ 
				Role currentRole = getUserDirectory().findRoleByPrimaryKey(role);				
				if(currentRole != null && pUser.hasAssignedRole(currentRole)){
					return true;
				}
			}
		}
		return false;
	}
	
	public UserRolesAccessRights getUserRolesAccessRights() {
		return mUserRolesAccessRights;
	}

	public void setUserRolesAccessRights(
			UserRolesAccessRights pUserRolesAccessRights) {
		this.mUserRolesAccessRights = pUserRolesAccessRights;
	}

	public UserDirectory getUserDirectory() {
		return mUserDirectory;
	}

	public void setUserDirectory(UserDirectory pUserDirectory) {
		this.mUserDirectory = pUserDirectory;
	}
}