package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.adapter.gsa.GSARepository;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.util.CPSConstants;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;


/**
 * GetCategoryDescriptionDroplet
 */
public class GetCategoryDescriptionDroplet extends DynamoServlet {


	private final static ParameterName CHILD_CATEGORY_ID = ParameterName.getParameterName("childCategoryId");
	private final static ParameterName OUTPUT = ParameterName.getParameterName("output");
	private static final String LONG_DESCRIPTION = "longDescription";
	private static final String CATEGORY_ITEM_NAME = "category";

	private GSARepository mProductCatalog;

	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
		String childCategoryId = (String) req.getObjectParameter(CHILD_CATEGORY_ID);

		try {
			if(!StringUtils.isEmpty(childCategoryId)){
				RepositoryItem childCategory = getProductCatalog().getItem(childCategoryId, CATEGORY_ITEM_NAME);
				RepositoryItem parentCategory = childCategory == null ? null : (RepositoryItem) childCategory.getPropertyValue(CPSConstants.PARENT_CATEGORY);
				if(parentCategory != null){
					String longDescription = (String) parentCategory.getPropertyValue(LONG_DESCRIPTION);
					req.setParameter(LONG_DESCRIPTION, longDescription);
					req.serviceLocalParameter(OUTPUT, req, res);
				}
			}
		} catch (RepositoryException e) {
			vlogError(e, e.getMessage());
		}

	}

	/**
	 * Gets mProductCatalog.
	 *
	 * @return Value of mProductCatalog.
	 */
	public GSARepository getProductCatalog() {
		return mProductCatalog;
	}

	/**
	 * Sets new mProductCatalog.
	 *
	 * @param pProductCatalog New value of mProductCatalog.
	 */
	public void setProductCatalog(GSARepository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}


}