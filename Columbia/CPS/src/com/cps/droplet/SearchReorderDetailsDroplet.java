package com.cps.droplet;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistManager;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.naming.ParameterName;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.cps.service.GiftListService;
import com.cps.service.OrganizationAliasService;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;
import java.io.IOException;
import java.util.*;

public class SearchReorderDetailsDroplet extends DynamoServlet {

	private static ParameterName SEARCH_TERM = ParameterName.getParameterName("term");
	private static ParameterName GIFTLIST_ID = ParameterName.getParameterName("giftlistId");

	private static ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");

	private GiftlistManager giftlistManager;

	private CPSProfileTools mProfileTools;

	private OrganizationAliasService mOrganizationAliasService;

	private TransactionManager mTransactionManager;

	/**
	 * gift list service
	 */
	private GiftListService mGiftListService;

	//------------------------------------------------------------------------------------------------------------------

	private void removeUnavailableItems(RepositoryItem pGiftList) {
		if (pGiftList != null) {
			List<RepositoryItem> gItems = (List) pGiftList.getPropertyValue(CPSConstants.GIFTLIST_ITEMS);
			vlogDebug("gItems :: {0}", gItems);
			if (gItems != null && !gItems.isEmpty()) {
				for (RepositoryItem gItem : gItems) {
					Boolean unavailable = (Boolean) gItem.getPropertyValue(CPSConstants.ECOMMERCE_UNAVAILABLE);
					if (unavailable == null) {
						unavailable = false;
					}
					vlogDebug("unavailable :: {0}",unavailable);
					if (unavailable) {
						vlogDebug("removing item");
						try {
							getGiftlistManager().removeItemFromGiftlist(pGiftList.getRepositoryId(), gItem.getRepositoryId());
						} catch (Exception e) {
							if (isLoggingError()) {
								logError(e);
							}
						}
					}
				}
			}
		}
	}

	private void updateUnavailableItems(Set<RepositoryItem> pExcludedItems) {
		if (pExcludedItems != null && pExcludedItems.size() > 0) {
			for (RepositoryItem gItem : pExcludedItems) {
				try {
					((MutableRepositoryItem) gItem).setPropertyValue(CPSConstants.ECOMMERCE_UNAVAILABLE, true);
				} catch (Exception e) {
					if (isLoggingError()) {
						logError(e);
					}
				}
			}
		}
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		vlogDebug("Search Reorder list Droplet");
		List<RepositoryItem> listSearchResults = new ArrayList<RepositoryItem>();
		boolean emptyResult = false;

		List<LinkedHashMap<String, Object>> sortableItems = new ArrayList<LinkedHashMap<String, Object>>();
		List<RepositoryItem> giftlistItems = new ArrayList();
		Set<RepositoryItem> excludedItems = new HashSet<RepositoryItem>();
		ParameterName renderParam = OUTPUT_OPARAM;
        StringBuilder ids = new StringBuilder();

		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;
		try {
			if (tm != null){
				td.begin(tm, TransactionDemarcation.REQUIRED);
			}
			String searchTerm = pRequest.getParameter(SEARCH_TERM);
			String giftlistId = pRequest.getParameter(GIFTLIST_ID);

			RepositoryItem giftlist = null;
			vlogDebug("passed giftlistId: " + giftlistId);

			Profile profile = getProfile();

			try {
				giftlist = getGiftlistManager().getGiftlist(giftlistId);
			} catch (CommerceException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}

			// first check if unavailable items should be removed
			removeUnavailableItems(giftlist);


			String orgId = null;

			RepositoryItem organization = getProfileTools().getParentOrganization(profile);
			if (organization != null) {
				orgId = organization.getRepositoryId();
			}

			if (!StringUtils.isBlank(searchTerm) && giftlist != null) { 

				giftlistItems = (List) giftlist.getPropertyValue(CPSConstants.GIFTLIST_ITEMS);
				if (giftlistItems != null && !giftlistItems.isEmpty()) {

					vlogDebug("list has items");
					for (RepositoryItem item : giftlistItems) {
						String productAlias = null;
						// Null check because I'm bad at that
						if (item != null) {
							RepositoryItem product = null;
							RepositoryItem sku = null;
							try {
								product = getGiftlistManager().getCatalogTools().findProduct((String) item.getPropertyValue(CPSConstants.PRODUCT_ID));
								sku = getGiftlistManager().getCatalogTools().findSKU((String) item.getPropertyValue(CPSConstants.PRODUCT_ID));

								if (orgId != null) {
									productAlias = getOrganizationAliasService().findAliasByOrganizationId(product, orgId);
								}

								// shortCode "secondItemNumber" as displayName per design
								String displayName = (String) sku.getPropertyValue(CPSConstants.DISPLAY_NAME);
								String description = (String) sku.getPropertyValue(CPSConstants.DESCRIPTION);
								String name = (String) item.getPropertyValue(CPSConstants.DISPLAY_NAME);
								String desc = (String) item.getPropertyValue(CPSConstants.DESCRIPTION);
								String productId = (String) item.getPropertyValue(CPSConstants.PRODUCT_ID);
								String catalogRefId = (String) item.getPropertyValue(CPSConstants.CATALOG_REF_ID);

								String mfgName = (String) product.getPropertyValue(CPSConstants.MFG_NAME);
								String mfgNumber = (String) product.getPropertyValue(CPSConstants.MFG_PART_NUMBER);
								String ups = (String) product.getPropertyValue(CPSConstants.UPC);
								String vendorItem = (String) product.getPropertyValue(CPSConstants.VENDOR_ITEM);

								if ((!StringUtils.isBlank(name) && name.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(desc) && desc.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(productId) && productId.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(catalogRefId) && catalogRefId.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(displayName) && displayName.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(description) && description.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(productAlias) && productAlias.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(mfgName) && mfgName.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(mfgNumber) && mfgNumber.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(vendorItem) && vendorItem.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1) ||
										(!StringUtils.isBlank(ups) && ups.toLowerCase().indexOf(searchTerm.toLowerCase()) != -1)) {
									listSearchResults.add(item);
									vlogDebug("Product in list matched, add list");
								}
							} catch (Exception e) {
								if (isLoggingError()) {
									logError(e);
								}
							}
						}
					}
				}
			} else {
				vlogDebug("No search entered, return all items");
				if (giftlist != null) {
					giftlistItems = (List) giftlist.getPropertyValue(CPSConstants.GIFTLIST_ITEMS);
					if (giftlistItems != null && !giftlistItems.isEmpty()) {
						for (Object itemObj : giftlistItems) {
							RepositoryItem item = (RepositoryItem) itemObj;
							listSearchResults.add(item);
						}
					}
				}
			}
			vlogDebug("listSearchResults - " + listSearchResults.toString());
			if (listSearchResults.isEmpty()) {
				giftlistItems = (List) giftlist.getPropertyValue(CPSConstants.GIFTLIST_ITEMS);
				if (giftlistItems != null && !giftlistItems.isEmpty()) {
					for (Object itemObj : giftlistItems) {
						RepositoryItem item = (RepositoryItem) itemObj;
						listSearchResults.add(item);
					}
				}
				if (!StringUtils.isBlank(searchTerm)) {
					emptyResult = true;
				}
			}

			for (RepositoryItem giftItem : listSearchResults) {
				if (getGiftListService().isItemShouldBeExcluded(giftItem)) { 
					excludedItems.add(giftItem);
				}
			}
			vlogDebug("excludedItems :: {0}", excludedItems); 
			// update items in search result eCommerceUnavailable property so they will be displayed as "Unavailable" first time
			updateUnavailableItems(excludedItems);
			for (RepositoryItem giftItem : listSearchResults) {
				LinkedHashMap<String, Object> listMap = new LinkedHashMap<String, Object>();
				listMap.put(CPSConstants.ID, giftItem.getPropertyValue(CPSConstants.ID));
				listMap.put(CPSConstants.PRODUCT_ID, giftItem.getPropertyValue(CPSConstants.PRODUCT_ID));
				listMap.put(CPSConstants.CATALOG_REF_ID, giftItem.getPropertyValue(CPSConstants.CATALOG_REF_ID));
				listMap.put(CPSConstants.QUANTITY_DESIRED, giftItem.getPropertyValue(CPSConstants.QUANTITY_DESIRED));
				listMap.put(CPSConstants.ECOMMERCE_UNAVAILABLE, giftItem.getPropertyValue(CPSConstants.ECOMMERCE_UNAVAILABLE));

				try {
					RepositoryItem product = getGiftlistManager().getCatalogTools().findProduct((String) giftItem.getPropertyValue(CPSConstants.PRODUCT_ID));
					if (product != null) {
						vlogDebug("Looking up product, display name = " + product.getPropertyValue(CPSConstants.DISPLAY_NAME));
						listMap.put(CPSConstants.DISPLAY_NAME, product.getPropertyValue(CPSConstants.DISPLAY_NAME));
						listMap.put(CPSConstants.DESCRIPTION, "" + product.getPropertyValue(CPSConstants.DESCRIPTION));
						listMap.put(CPSConstants.ALIAS_NUMBER, "" + getOrganizationAliasService().findAliasByOrganizationId(product, orgId));
					} else {
						vlogDebug("Product not found, this is an issue...");
					}
				} catch (Exception e) {
					if (isLoggingError()) {
						logError(e);
					}
				}
				sortableItems.add(listMap);
				if(sortableItems.size() > 1){
				    ids.append(",");
                }
                ids.append(listMap.get(CPSConstants.ID)).append(":").append(listMap.get(CPSConstants.QUANTITY_DESIRED));
			}
		} catch (Exception e){
			if (isLoggingError()){ 
				logError(e);
			}
			rollback = true;
		} finally {
			if (tm != null){
				try {
					td.end(rollback);
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()){
						logError(e);
					}
				}
			}
		}

		vlogDebug("sortable list {0} ",sortableItems);

		pRequest.setParameter("totalGiftlistItems", getGiftListService().getDesiredQty(giftlistItems));
		pRequest.setParameter("giftItems", sortableItems);
		pRequest.setParameter("allIds",ids.toString());
		if (excludedItems.size() > 0) {
			pRequest.setParameter("unavailableItems", excludedItems);
		}
		pRequest.setParameter("emptyResult", emptyResult);
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
	}

	//------------------------------------------------------------------------------------------------------------------

	public Profile getProfile() {
		return (Profile) ServletUtil.getCurrentUserProfile();
	}

	public GiftlistManager getGiftlistManager() {
		return giftlistManager;
	}

	public void setGiftlistManager(GiftlistManager giftlistManager) {
		this.giftlistManager = giftlistManager;
	}

	public GiftListService getGiftListService() {
		return mGiftListService;
	}

	public void setGiftListService(GiftListService pGiftListService) {
		mGiftListService = pGiftListService;
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public OrganizationAliasService getOrganizationAliasService() {
		return mOrganizationAliasService;
	}

	public void setOrganizationAliasService(OrganizationAliasService pOrganizationAliasService) {
		this.mOrganizationAliasService = pOrganizationAliasService;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
}