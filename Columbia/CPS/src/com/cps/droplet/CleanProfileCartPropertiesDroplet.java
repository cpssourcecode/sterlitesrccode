package com.cps.droplet;

import static com.cps.util.CPSConstants.*;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;


/**
 * Checks cart settings and define delivery method, cart selected address or cart selected location based on this.
 *
 * @author Andy Porter
 */
public class CleanProfileCartPropertiesDroplet extends DynamoServlet {

	public static final ParameterName PROFILE_PARAM = ParameterName.getParameterName(PROFILE);

	/**
	 * output.
	 */
	public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");

	/**
	 * EMPTY
	 */
	public static final ParameterName EMPTY = ParameterName.getParameterName("empty");


	@SuppressWarnings("rawtypes")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		Profile profile = (Profile) pRequest.getObjectParameter(PROFILE_PARAM);
		if (null != profile) {
//			profile.setPropertyValue(CART_SELECTED_CS, null);
//			profile.setPropertyValue(CART_SELECTED_STORE, null);
			profile.setPropertyValue(CART_SELECTED_DELIVERY_METHOD, null);
		}
		vlogDebug("CleanProfileCartPropertiesDroplet invoked.");
	}

}
