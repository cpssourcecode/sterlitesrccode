package com.cps.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * @author Manikandan
 * 
 * This class is used to display different page header in checkout and not-checkout pages
 *
 */
public class CPSPageHeaderDroplet extends DynamoServlet {

	// Input parameter name
    public static ParameterName CURRENT_PAGE = ParameterName.getParameterName("currentPage");
    // Output parameter name
	public static final ParameterName OPARAM_OUTPUT = ParameterName.getParameterName("output");
	
    private List<String> checkoutPageURLs;
    private String homePage;
    private String cartPage;
	
	@Override
	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res)
			throws ServletException, IOException {
		boolean isCheckoutPage = false;
		boolean isHomePage = false;
		boolean isCartPage = false;
		String currentPage = req.getParameter(CURRENT_PAGE);
		for (String checkoutPageURL : getCheckoutPageURLs()) {
			if(currentPage.equals(checkoutPageURL)){
				isCheckoutPage = true;
				break;
			}
		}
		if(currentPage.equalsIgnoreCase(getHomePage())){
			isHomePage = true;
		}
		if(currentPage.equalsIgnoreCase(getCartPage())){
			isCartPage = true;
		}
		req.setParameter("isCheckoutPage", isCheckoutPage);
		req.setParameter("isHomePage", isHomePage);
		req.setParameter("isCartPage", isCartPage);
		req.serviceLocalParameter(OPARAM_OUTPUT, req, res);
	}

	/**
	 * @return the checkoutPageURLs
	 */
	public List<String> getCheckoutPageURLs() {
		return checkoutPageURLs;
	}

	/**
	 * @param checkoutPageURLs the checkoutPageURLs to set
	 */
	public void setCheckoutPageURLs(List<String> checkoutPageURLs) {
		this.checkoutPageURLs = checkoutPageURLs;
	}

	/**
	 * @return the homePage
	 */
	public String getHomePage() {
		return homePage;
	}

	/**
	 * @param homePage the homePage to set
	 */
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}

	/**
	 * @return the cartPage
	 */
	public String getCartPage() {
		return cartPage;
	}

	/**
	 * @param cartPage the cartPage to set
	 */
	public void setCartPage(String cartPage) {
		this.cartPage = cartPage;
	}
	
	
	
}
