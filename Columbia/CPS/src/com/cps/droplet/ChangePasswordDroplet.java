package com.cps.droplet;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.security.CriptoUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class ChangePasswordDroplet extends DynamoServlet{

	private static ParameterName LN = ParameterName.getParameterName("ln");
	private static ParameterName PR = ParameterName.getParameterName("pr");
	private static ParameterName RD = ParameterName.getParameterName("rd");

	private static ParameterName ERROR_CODE = ParameterName.getParameterName("errorCode");

	private static ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");
	private static ParameterName SUCCESS_OPARAM = ParameterName.getParameterName("success");
	private static ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");

	private CPSProfileTools mProfileTools;

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	private int mLinkExpirationDays = 2;

	public int getLinkExpirationDays() {
		return mLinkExpirationDays;
	}

	public void setLinkExpirationDays(int pLinkExpirationDays) {
		mLinkExpirationDays = pLinkExpirationDays;
	}

	private int mFridayLinkExpirationInterval = 1;

	public int getFridayLinkExpirationInterval() {
		return mFridayLinkExpirationInterval;
	}

	public void setFridayLinkExpirationInterval(int pFridayLinkExpirationInterval) {
		mFridayLinkExpirationInterval = pFridayLinkExpirationInterval;
	}

	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug("ChangePasswordDroplet.start");
		}

		ParameterName renderParam = EMPTY_OPARAM;

		String sEncryptedLogin = pRequest.getParameter(LN);
		String sEncryptedProfileId = pRequest.getParameter(PR);
		String sEncryptedDate = pRequest.getParameter(RD);

		try {
			if(!isCurrentUserTransient()) {
				pRequest.setParameter(ERROR_CODE.getName(), CPSErrorCodes.ERR_FORGOT_PASS_USER_ALREADY_LOGGED_IN);
				renderParam = ERROR_OPARAM;
			} else if (!StringUtils.isBlank(sEncryptedLogin) && !StringUtils.isBlank(sEncryptedProfileId) && !StringUtils.isBlank(sEncryptedDate)){
				String sDecryptedDate = CriptoUtils.aesDecrypt(sEncryptedDate);
				Date decryptedDate = new SimpleDateFormat(CPSConstants.RECOVER_PSWD_DATE_FORMAT).parse(sDecryptedDate);

				LocalDate localDate = decryptedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				DayOfWeek day = localDate.getDayOfWeek();

				int linkExpirationDays = getLinkExpirationDays();
				if (DayOfWeek.FRIDAY.equals(day)) {
					linkExpirationDays += getFridayLinkExpirationInterval();
				}

				Calendar now = Calendar.getInstance();
				now.add(Calendar.DAY_OF_YEAR, -linkExpirationDays);
				if (decryptedDate.after(now.getTime())) {
					String sDecryptedLogin = CriptoUtils.aesDecrypt(sEncryptedLogin);
					String sDecryptedProfileId = CriptoUtils.aesDecrypt(sEncryptedProfileId);

					boolean validateUser = getProfileTools().validateEmailLink(sDecryptedProfileId, sDecryptedLogin);
					if (validateUser) {
						RepositoryItem user = getProfileTools().getProfileRepository().getItem(sDecryptedProfileId, CPSConstants.USER);
						Boolean generatedPassword = (Boolean)user.getPropertyValue(CPSConstants.FORGOT_PSWD);
						if (generatedPassword != null && generatedPassword.booleanValue()){
							// user is clear to reset password
							pRequest.setParameter(CPSConstants.USER, user);
							renderParam = SUCCESS_OPARAM;
						} else {
							// User did not go through reset password flow, access not granted
							if (isLoggingDebug()) {
								logDebug("generatedPassword is false");
							}
							pRequest.setParameter(ERROR_CODE.getName(), CPSErrorCodes.ERR_FORGOT_PASS_ALREADY_CHANGED);
							renderParam = ERROR_OPARAM;
						}
					} else {
						if (isLoggingDebug()) {
							logDebug("user's id and login don't match");
						}
						pRequest.setParameter(ERROR_CODE.getName(), CPSErrorCodes.ERR_FORGOT_PASS_LINK_EXPIRED);
						renderParam = ERROR_OPARAM;
					}
				} else {
					if (isLoggingDebug()) {
						logDebug("reset password email link is expired");
					}
					pRequest.setParameter(ERROR_CODE.getName(), CPSErrorCodes.ERR_FORGOT_PASS_LINK_EXPIRED);
					renderParam = ERROR_OPARAM;
				}
			} else {
				renderParam = EMPTY_OPARAM;
			}
		}catch (Exception e){
			if (isLoggingError()) {
				logError("Exception occurred in Change Password Droplet: ", e);
			}
		}

		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug("ChangePasswordDroplet.end");
		}

	}
	
	private boolean isCurrentUserTransient() {
		RepositoryItem profile = ServletUtil.getCurrentUserProfile();
		return profile != null && profile.isTransient();
	}

}