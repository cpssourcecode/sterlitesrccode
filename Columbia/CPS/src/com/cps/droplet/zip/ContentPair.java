package com.cps.droplet.zip;

import java.io.InputStream;

/**
 * @author Ihar Zharykau
 */
public class ContentPair {
	private InputStream mInputStream;
	private String mFileName;
	private String mUrl;

	public ContentPair(InputStream mInputStream, String mFileName, String pUrl) {
		this.mInputStream = mInputStream;
		this.mFileName = mFileName;
		this.mUrl = pUrl;
	}

	public InputStream getInputStream() {
		return mInputStream;
	}

	public void setInputStream(InputStream pInputStream) {
		mInputStream = pInputStream;
	}

	public String getFileName() {
		return mFileName;
	}

	public void setFileName(String pFileName) {
		mFileName = pFileName;
	}

	public String getUrl() {
		return mUrl;
	}

	public void setUrl(String pUrl) {
		mUrl = pUrl;
	}
}
