package com.cps.droplet.zip;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.cps.service.FilesCacheService;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author Ihar Zharykau
 */
public class ZipContentDroplet extends DynamoServlet {


	private final static String CONTENT_URLS = "contentUrls";
	private final static String RESULT_ZIP_NAME = "zipName";


	private FilesCacheService mFilesCacheService;
	private int mCountOfThreadsToDownload = 10;

	@Override
	@SuppressWarnings("unchecked")
	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
		CompletionService<ContentPair> taskCompletionService = createTaskCompletionService();
		List<String> contentUrls = new ArrayList<>(Arrays.asList(req.getParameter(CONTENT_URLS).split(",")));
		List<ContentPair> savedFiles = processSavedFiles(contentUrls);
		submitDownloadTasks(taskCompletionService, contentUrls);
		ZipArchive zipArchive = new ZipArchive(req.getParameter(RESULT_ZIP_NAME));
		for (int i = 0; i < contentUrls.size(); i++) {
			try {
				ContentPair contentPair = taskCompletionService.take().get();
				mFilesCacheService.saveFile(contentPair.getInputStream(), contentPair.getUrl());
				writeResultToArchive(zipArchive, contentPair);
			} catch (InterruptedException | ExecutionException e) {
				logError("ERROR", e);
			}
		}
		for ( ContentPair contentPair : savedFiles){
			writeResultToArchive(zipArchive, contentPair);
		}
		zipArchive.flush();
		zipArchive.close();
		writeZipToResponse(zipArchive, res.getResponse());
	}

	private List<ContentPair> processSavedFiles(List<String> pContentUrls) throws FileNotFoundException{
		List<ContentPair> result = new ArrayList<>();
		Iterator<String> iterator = pContentUrls.iterator();
		while(iterator.hasNext()){
			String url = iterator.next();
			InputStream inputStream = mFilesCacheService.getFileIfExistsByUrl(url);
			if ( inputStream != null){
				iterator.remove();
				result.add(new ContentPair(inputStream, FilesCacheService.getFilenameFromUrl(url), url));
			}
		}
		return result;
	}

	private static void writeZipToResponse(ZipArchive pZipArchive, HttpServletResponse pResponse) throws IOException {
		ServletOutputStream sos = pResponse.getOutputStream();
		pResponse.reset();
		pResponse.setContentType("application/zip");
		pResponse.setHeader("Content-Disposition", "attachment; filename=" + pZipArchive.getZipName());
		pResponse.setContentLength(pZipArchive.size());
		sos.write(pZipArchive.toByteArray());
		sos.flush();
	}

	private void writeResultToArchive(ZipArchive pZipArchive, ContentPair pContent) {
		try {
			writeStreamToZip(pZipArchive.getZipOutputStream(), pContent.getInputStream(), pContent.getFileName());
			pContent.getInputStream().close();
		} catch (IOException e) {
			logError("ERROR", e);
		}
	}

	private CompletionService<ContentPair> createTaskCompletionService() {
		ExecutorService executorService = Executors.newFixedThreadPool(mCountOfThreadsToDownload);
		return new ExecutorCompletionService<>(executorService);
	}

	private void submitDownloadTasks(CompletionService<ContentPair> pTaskCompletionService, List<String> pContentUrls) {
		List<Callable<ContentPair>> callables = new ArrayList<>();
		for (String contentUrl : pContentUrls) {
			callables.add(downloadCallable(contentUrl));
		}
		for (Callable<ContentPair> callable : callables) {
			pTaskCompletionService.submit(callable);
		}
	}

	private static void writeStreamToZip(ZipOutputStream zipOS, InputStream inputStream, String fileName) throws IOException {
		ZipEntry zipEntry = new ZipEntry(fileName);
		zipOS.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = inputStream.read(bytes)) >= 0) {
			zipOS.write(bytes, 0, length);
		}
		zipOS.closeEntry();

	}

	private static Callable<ContentPair> downloadCallable(final String url) {
		return new Callable<ContentPair>() {
			@Override
			public ContentPair call() throws Exception {
				return new ContentPair(new URL(url).openStream(), FilesCacheService.getFilenameFromUrl(url), url);
			}
		};
	}

	public int getCountOfThreadsToDownload() {
		return mCountOfThreadsToDownload;
	}

	public void setCountOfThreadsToDownload(int pCountOfThreadsToDownload) {
		mCountOfThreadsToDownload = pCountOfThreadsToDownload;
	}

	public FilesCacheService getFilesCacheService() {
		return mFilesCacheService;
	}

	public void setFilesCacheService(FilesCacheService pFilesCacheService) {
		mFilesCacheService = pFilesCacheService;
	}
}
