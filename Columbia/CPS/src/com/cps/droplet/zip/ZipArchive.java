package com.cps.droplet.zip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipOutputStream;

/**
 * @author Ihar Zharykau
 */
public class ZipArchive {
	private ByteArrayOutputStream baos;
	private ZipOutputStream zipOS;
	private String zipName;

	public ZipArchive(String pZipName) {
		baos = new ByteArrayOutputStream();
		zipOS = new ZipOutputStream(baos);
		zipName = pZipName;
	}

	public ZipOutputStream getZipOutputStream() {
		return zipOS;
	}

	public void flush() throws IOException{
		zipOS.flush();
		baos.flush();
	}

	public void close() throws IOException{
		zipOS.close();
		baos.close();
	}

	public byte[] toByteArray(){
		return baos.toByteArray();
	}

	public int size(){
		return baos.size();
	}

	public String getZipName() {
		return zipName;
	}
}
