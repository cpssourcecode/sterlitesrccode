package com.cps.droplet;

import atg.nucleus.naming.ParameterName;

public class DropletConstants {
	
	public static final ParameterName SEARCH_TERM = ParameterName.getParameterName("term");
	public static final ParameterName PAGE = ParameterName.getParameterName("page");
	public static final ParameterName SORT = ParameterName.getParameterName("sort");
	public static final ParameterName ORDER = ParameterName.getParameterName("order");
	public static final ParameterName ROLE = ParameterName.getParameterName("role");
	public static final ParameterName COMPANY_NAME = ParameterName.getParameterName("companyName");
	
	public static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	public static final ParameterName EMPTY_OUTPUT = ParameterName.getParameterName("empty");
	public static final String EMPTY = "empty";
	public static final String ERROR = "error";
	public static final String LIST = "list";
	public static final String LETTER_MAP = "letterMap";
	public static final String TOTAL = "total";
	public static final String NUM_PER_PAGE = "numPerPage";
	public static final String NUM_PAGES = "numPages";
	
	public static final String ORDER_DESCENDING = "-";
	public static final String ORDER_ASCENDING = "+";
		
	public static final double NUM_PER_PAGE_AMOUNT = 20;
	
	public static final String PARENT_ORG = "parentOrganization";
	public static final String MEMBERS = "members";
	
	public static final String SORT_LAST_NAME = "lastName";
	public static final String SORT_ROLE = "role";
	public static final String SORT_EMAIL = "email";
	public static final String SORT_COMPANY = "company";
	public static final String SORT_SPENDING = "limit";
	public static final String SORT_FREQUENCY = "frequency";
	public static final String SORT_STATUS = "status";
	public static final String SORT_COMPANY_NAME = "companyName";
	public static final String SORT_ACCOUNT_NUMBER = "accountNumber";
	public static final String SORT_ADDRESS = "address";
	
	public static final String APPROVERS = "approvers";
	public static final String NOTIFICATION_ENABLED_USERS = "notificationEnabledUsers";
}