package com.cps.droplet;

import java.io.IOException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.commerce.catalog.CatalogTools;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import javax.servlet.ServletException;

public class CategoryPropertiesLookupDroplet extends DynamoServlet {

    private static final String CATEGORY_PROPERTY_DISPLAY_NAME = "displayName";
    private static final String IMAGE_PROPERTY_URL = "url";

    private static final String PARAM_NAME_DIMVAL_ID = "dimValId";
    private static final String PARAM_NAME_CATEGORY_NAME = "categoryName";
    private static final String PARAM_NAME_CATEGORY_IMAGE = "categoryImage";
    private static final String PARAM_NAME_CATEGORY_URL = "categoryUrl";
    private static final String OUTPUT = "output";

    private String defaultImageUrl;
    private DimensionValueCacheTools mDimensionValueCacheTools;
    private CatalogTools catalogTools;

    private String mImagePropertyName = "thumbnail_url";

    public String getImagePropertyName() {
        return mImagePropertyName;
    }

    public void setImagePropertyName(String pImagePropertyName) {
        mImagePropertyName = pImagePropertyName;
    }

    public CategoryPropertiesLookupDroplet() {
    }

    public void service(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
            throws ServletException, IOException {

        String dimvalId = request.getParameter(PARAM_NAME_DIMVAL_ID);

        if (dimvalId != null) {
            DimensionValueCacheObject categoryDimval = getDimensionValueCacheTools().getCachedObjectForDimval(dimvalId);

            if (categoryDimval != null) {
                String categoryId = categoryDimval.getRepositoryId();
                String categoryUrl = categoryDimval.getUrl();
                try {
                    RepositoryItem categoryRepositoryItem = getCatalogTools().findCategory(categoryId);
                    request.setParameter(PARAM_NAME_CATEGORY_URL, categoryUrl);
                    request.setParameter(PARAM_NAME_CATEGORY_NAME, categoryRepositoryItem.getPropertyValue(CATEGORY_PROPERTY_DISPLAY_NAME));
                    Object categoryImage = categoryRepositoryItem.getPropertyValue(getImagePropertyName());

                    if (categoryImage == null) {
                        request.setParameter(PARAM_NAME_CATEGORY_IMAGE, getDefaultImageUrl());
                    } else {
                        if (categoryImage instanceof String) {
                            request.setParameter(PARAM_NAME_CATEGORY_IMAGE, categoryImage);
                        } else if (categoryImage instanceof RepositoryItem) {
                            request.setParameter(PARAM_NAME_CATEGORY_IMAGE,
                                    ((RepositoryItem)categoryImage).getPropertyValue(IMAGE_PROPERTY_URL));
                        }
                    }

                } catch (RepositoryException e) {
                    vlogError(e, "Error");
                }
            }
            request.serviceParameter(OUTPUT, request, response);
        }
    }

    public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
        mDimensionValueCacheTools = pDimensionValueCacheTools;
    }

    public DimensionValueCacheTools getDimensionValueCacheTools() {
        return mDimensionValueCacheTools;
    }

    public CatalogTools getCatalogTools() {
        return catalogTools;
    }

    public void setCatalogTools(CatalogTools catalogTools) {
        this.catalogTools = catalogTools;
    }

    public String getDefaultImageUrl() {
        return defaultImageUrl;
    }

    public void setDefaultImageUrl(String defaultImageUrl) {
        this.defaultImageUrl = defaultImageUrl;
    }
}
