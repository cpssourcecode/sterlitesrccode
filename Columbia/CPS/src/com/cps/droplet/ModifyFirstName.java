package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Takes in name and if greater than 20 character, returns string of first 20 characters with ...
 * Otherwise returns entered string
 * @author Chris
 *
 */

public class ModifyFirstName extends DynamoServlet {
	
	private static String OUTPUT = "output";
	
	private static String RETURN_NAME = "name";
	
	public static ParameterName FIRST_NAME = ParameterName.getParameterName("firstName");
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		String firstName = pRequest.getParameter(FIRST_NAME);
		
		if (!StringUtils.isBlank(firstName)){
			if (firstName.length() > 20){
				String newName = firstName.substring(0, 20).concat("...");
				pRequest.setParameter(RETURN_NAME, newName);
			}else{
				pRequest.setParameter(RETURN_NAME, firstName);
			}
		}
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
	}
}