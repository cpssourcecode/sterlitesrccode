package com.cps.droplet;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import com.cps.userprofiling.service.ProfileRecentlyViewedService;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;
import java.io.IOException;

/**
 * @author Alex Turchynovich.
 */
public class CPSAddRecentlyViewedDroplet extends DynamoServlet {

    private static final String PARAM_PRODUCT = "product";

    private static final String PARAM_PROFILE_RECENTLY_VIEWED_SERVICE = "profileRecentlyViewedService";

    private TransactionManager mTransactionManager;

    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        TransactionManager tm = getTransactionManager();
        TransactionDemarcation td = new TransactionDemarcation();
        boolean rollback = false;
        try {
            if (tm != null) {
                td.begin(tm, TransactionDemarcation.REQUIRED);
            }
            ProfileRecentlyViewedService profileRecentlyViewedService = (ProfileRecentlyViewedService) req.getObjectParameter(PARAM_PROFILE_RECENTLY_VIEWED_SERVICE);
            if (profileRecentlyViewedService != null) {
                Object product = req.getObjectParameter(PARAM_PRODUCT);
                if (product != null && product instanceof RepositoryItem) {
                    profileRecentlyViewedService.addRecentlyViewed((RepositoryItem) product);
                }
            }
        } catch (Exception e) {
            if (isLoggingError()){
                logError(e);
            }
            rollback = true;
        } finally {
            if (tm != null){
                try {
                    td.end(rollback);
                } catch (TransactionDemarcationException e) {
                    if (isLoggingError()){
                        logError(e);
                    }
                }
            }
        }
    }

    public TransactionManager getTransactionManager() {
        return mTransactionManager;
    }

    public void setTransactionManager(TransactionManager pTransactionManager) {
        mTransactionManager = pTransactionManager;
    }
}
