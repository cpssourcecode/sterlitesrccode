package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Class to be included on droplets that contains common methods
 * 
 * @author Chris
 *
 */
public abstract class DropletTools extends DynamoServlet {
	/**
	 * Sets return values on the request
	 * @param outputName - oparam name to set
	 * @param list - final list to return
	 * @param total - total of items in complete list
	 * @param numPerPage - number of items per page
	 * @param numPages - number of pages
	 * @param pRequest - request
	 * @param pResponse - response
	 * @throws ServletException 
	 * @throws IOException 
	 */
	protected void setReturnValues (ParameterName outputName, List<RepositoryItem> list, int total, int numPerPage, int numPages, 
			DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		pRequest.setParameter(DropletConstants.LIST, list);
		pRequest.setParameter(DropletConstants.TOTAL, (int)total);
		pRequest.setParameter(DropletConstants.NUM_PER_PAGE, (int)DropletConstants.NUM_PER_PAGE_AMOUNT);
		pRequest.setParameter(DropletConstants.NUM_PAGES, (int)numPages);
		pRequest.serviceLocalParameter(outputName, pRequest, pResponse);
	}
	
	/**
	 * Pulls the page listing out of all sorted lists
	 * 
	 * @param list list to filter
	 * @param page page to filter by
	 * @return subsetList
	 */
	protected List<RepositoryItem> getSubsetList(List<RepositoryItem> list, int page){
		
		List<RepositoryItem> subsetList = new ArrayList<RepositoryItem>();
		int max = page*(int)DropletConstants.NUM_PER_PAGE_AMOUNT;
		int min = max-(int)DropletConstants.NUM_PER_PAGE_AMOUNT;
		
		// No one likes IndexOutOfBounds Exceptions
		if (max > list.size()){
			max = list.size();
		}
		
		// Only include lists that should display on the current page
		for (int i=min; i<max; i++){
			subsetList.add(list.get(i));
		}
		
		return subsetList;

	}
	
	
	/**
	 * Actually sorts the lists
	 * @param lists list to sort
	 * @param sort property to sort by
	 * @param order direction of sort
	 */
	protected void sortCollection (List<RepositoryItem> lists, final String sort, final String order){
		Collections.sort(lists, new Comparator<RepositoryItem>() {
			public int compare(RepositoryItem item1, RepositoryItem item2){
				int returnValue = 0;
				returnValue = sort((String)item1.getPropertyValue(sort), (String)item2.getPropertyValue(sort));
				if (DropletConstants.ORDER_DESCENDING.equalsIgnoreCase(order)){
					returnValue *= -1;
				}
				return returnValue;
			}
		});
	}
	
	/**
	 * General sort method
	 * @param value1 - value1 to sort
	 * @param value2 - value2 to sort
	 * @return int
	 */
	private int sort (String value1, String value2){
		int returnValue = 0;
		if (value1 != null){
			if (value2 != null){
				// Both values not null, compare as usual
				returnValue = (value1.toLowerCase())
						.compareTo(value2.toLowerCase());
			} else {
				// item1 not null and item2 null, return <0
				returnValue = -1;
			}
		} else if (value2 != null){
			// item1 null and item2 not null, return >0
			returnValue = 1;
		}
		
		return returnValue;
	}
	
	/**
	 * Handles setting filtered lists to new list based on passed search term
	 * 
	 * @param origList original list to filter
	 * @param searchTerm search term to filter by
	 * @return filteredList
	 */
	protected List<RepositoryItem> filterListBySearch(List<RepositoryItem> origList, String searchTerm){
		List<RepositoryItem> filteredList = new ArrayList<RepositoryItem>();
		
		// Loop through each list and add to new list if search term found
		for (RepositoryItem item : origList){
			if (checkItem(searchTerm, item)){
				filteredList.add(item);
			}
		}
		
		return filteredList;
	}
	
	/**
	 * Abstract method to be overloaded to check the passed repositoryItem
	 * for the passed search value
	 * @param s - search value passed to filter against
	 * @param r - repositoryItem to check
	 * @return boolean
	 */
	protected abstract boolean checkItem(String s, RepositoryItem r);
}