package com.cps.droplet;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import com.cps.userprofiling.CPSProfileTools;

import javax.servlet.ServletException;
import java.io.IOException;

public class CPSBranchPhoneNumberDroplet extends DynamoServlet {
    private static final String BUSINESS_UNIT = "businessUnit";
    private static final String BRANCH_PHONE_NUMBER = "branchPhoneNumber";
    private static final String BILLING_ADDRESS = "billingAddress";
    private static final String DERIVED_BILLING_ADDRESS = "derivedBillingAddress";
    private static final String DEFAULT_BRANCH = "100";

    private static final String RQL_SELECT_BY_BRANCH_ID = "branchId = ?0";

    private static final String PARAMETER_PHONE_NUMBER = "phoneNumber";

    public static ParameterName PROFILE = ParameterName.getParameterName("profile");
    public static ParameterName OUTPUT = ParameterName.getParameterName("output");

    private Repository mRepository;
    private CPSProfileTools profileTools;

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        Profile profile = (Profile) pRequest.getObjectParameter(PROFILE);

        if (profile != null) {
            String phoneNumber;

            RepositoryItem billingAddress;
            RepositoryItem parentOrganization = getProfileTools().getParentOrganization(profile);
            if (parentOrganization != null) {
                billingAddress = (RepositoryItem) parentOrganization.getPropertyValue(BILLING_ADDRESS);
            } else {
                billingAddress = (RepositoryItem) profile.getPropertyValue(DERIVED_BILLING_ADDRESS);
            }

            if (billingAddress != null) {
                String businessUnit = (String) billingAddress.getPropertyValue(BUSINESS_UNIT);
                if (StringUtils.isNotBlank(businessUnit)) {
                    phoneNumber = getPhoneNumberByBranchId(businessUnit);
                } else {
                    phoneNumber = getPhoneNumberByBranchId(DEFAULT_BRANCH);
                }
            } else {
                phoneNumber = getPhoneNumberByBranchId(DEFAULT_BRANCH);
            }

            pRequest.setParameter(PARAMETER_PHONE_NUMBER, phoneNumber);
        }

        pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
    }

    private String getPhoneNumberByBranchId(String branchId) {
        String phoneNumber = null;

        try {
            RepositoryView repositoryView = getRepository().getView(BRANCH_PHONE_NUMBER);
            RqlStatement statement = RqlStatement.parseRqlStatement(RQL_SELECT_BY_BRANCH_ID);
            Object params[] = {branchId};
            RepositoryItem[] items = statement.executeQuery(repositoryView, params);

            if (items != null && items.length > 0) {
                phoneNumber = (String) items[0].getPropertyValue(PARAMETER_PHONE_NUMBER);
            }
        } catch (RepositoryException e) {
            vlogError(e, "Repository exception during querying Phone Number by Branch ID.");
        }

        return phoneNumber;
    }

    public Repository getRepository() {
        return mRepository;
    }

    public void setRepository(Repository pRepository) {
        this.mRepository = pRepository;
    }

    public CPSProfileTools getProfileTools() {
        return profileTools;
    }

    public void setProfileTools(CPSProfileTools profileTools) {
        this.profileTools = profileTools;
    }
}
