package com.cps.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.service.pipeline.PipelineManager;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.constants.VSGRepositoriesConstants;

/**
 * 
 * @author VSG
 *
 */
public class CPSRefreshOrderDroplet extends DynamoServlet {

	private static final String INVALIDATE_CACHE = "InvalidateCache";
	private static final String ORDER_REPOSITORY = "OrderRepository";
	private static final String ORDER_MANAGER = "OrderManager";
	private static final String ORDER_REPOSITORY_ITEM = "OrderRepositoryItem";
	private static final String ORDER2 = "Order";
	private static final String ORDER_ID = "OrderId";
	public static final String ORDER_INPUT_PARAM = "order";
	public static final String OUTPUT = "output";
	
	private TransactionManager mTransactionManager;
	private OrderManager mOrderManager;
	private MutableRepository mOrderRepository;
	private PipelineManager mPipelineManager;
	private String mRefreshOrderChain;

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final Order order = getOrder(pRequest);
		if(order != null) {
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				try {
					td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
					synchronized (order) {
						runProcessRefreshOrder(order);
						getOrderManager().updateOrder(order);
					}
				} catch (Exception e) {
					rollback = true;
					vlogError(e, "Error in CPSRefreshOrderDroplet.refreshOrder");
				} finally {
					td.end(rollback);
				}
			} catch (TransactionDemarcationException e) {
				vlogError(e, "Error in CPSRefreshOrderDroplet.refreshOrder");
			}
		}
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
	}

	private void runProcessRefreshOrder(Order order) throws RepositoryException {
		Map<String, Object> paramMap = new HashMap<String, Object>();

		MutableRepository orderRepository = getOrderRepository();
		MutableRepositoryItem orderItem = getOrderRepository().getItemForUpdate(order.getId(),
				VSGRepositoriesConstants.ORDER);
		vlogDebug("OrderId: {0}, OrderRepositoryItem: {1}", order.getId(), orderItem);
		if(orderItem != null) {
			paramMap.put(ORDER_ID, order.getId());
			paramMap.put(ORDER2, order);
			paramMap.put(ORDER_REPOSITORY_ITEM, orderItem);
			paramMap.put(ORDER_MANAGER, getOrderManager());
			paramMap.put(ORDER_REPOSITORY, orderRepository);
			paramMap.put(INVALIDATE_CACHE, true);
	
			runProcess(getRefreshOrderChain(), paramMap);
		}
	}

	protected void runProcess(String pChainId, Map<String, Object> pParameters) {
		try {
			if (pChainId != null) {
				getPipelineManager().runProcess(pChainId, pParameters);
			}
		} catch (RunProcessException e) {
			vlogError(e, "Error in CPSRefreshOrderDroplet.runProcess");
		}
	}
	
	private Order getOrder(DynamoHttpServletRequest pRequest) {
		Order order = null;
		Object obj = pRequest.getObjectParameter(ORDER_INPUT_PARAM);
		if(obj instanceof Order) {
			order = (Order) obj;
		}
		return order;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public MutableRepository getOrderRepository() {
		return mOrderRepository;
	}

	public void setOrderRepository(MutableRepository pOrderRepository) {
		mOrderRepository = pOrderRepository;
	}

	public PipelineManager getPipelineManager() {
		return mPipelineManager;
	}

	public void setPipelineManager(PipelineManager pPipelineManager) {
		mPipelineManager = pPipelineManager;
	}

	public String getRefreshOrderChain() {
		return mRefreshOrderChain;
	}

	public void setRefreshOrderChain(String pRefreshOrderChain) {
		mRefreshOrderChain = pRefreshOrderChain;
	}

}
