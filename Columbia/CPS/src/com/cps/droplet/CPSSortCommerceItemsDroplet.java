package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;

import com.cps.service.OrganizationAliasService;

import atg.commerce.catalog.CatalogTools;
import atg.commerce.order.CommerceItem;
import atg.core.util.StringUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

/**
 * droplet to sort commerce items on cart page
 *
 */
public class CPSSortCommerceItemsDroplet extends DynamoServlet {
	
	/** commerceItems param */
	private final static String COMMERCE_ITEMS = "commerceItems";
	/** sortedCommerceItems param */
	private final static String SORTED_COMMERCE_ITEMS = "sortedCommerceItems";
	/** sortOption param */
	private final static String SORT_OPTION = "sortOption";
	/** sortOptionType param */
	private final static String SORT_OPTION_TYPE = "sortOptionType";
	/** The Constant OUTPUT_OPARAM. */
	private static final String OUTPUT_OPARAM = "output";
	/** The Constant EMPTY_OPARAM. */
	private static final String EMPTY_OPARAM = "empty";
	/** sort-option-item-number constant */
	private static final String SORT_OPTION_ITEM_NUMBER = "sort-option-item-number";
	/** sort-option-alias-number constant */
	private static final String SORT_OPTION_ALIAS_NUMBER = "sort-option-alias-number";
	/** sort-option-price constant */
	private static final String SORT_OPTION_PRICE = "sort-option-price";
	/** sort-option-description */
	private static final String SORT_OPTION_DESCRIPTION = "sort-option-description";
	/** desc order constant */
	private static final String DESC_ORDER = "desc";
	
	/**
	 * catalog tools
	 */
	private CatalogTools mCatalogTools;
	/**
	 * Organization Alias Service
	 */
	private OrganizationAliasService mOrganizationAliasService;
	
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		String sortOption = pRequest.getParameter(SORT_OPTION);
		String sortOptionType = pRequest.getParameter(SORT_OPTION_TYPE);
		List<CommerceItem> commerceItems = (List<CommerceItem>) pRequest.getObjectParameter(COMMERCE_ITEMS);
		
		if (commerceItems == null || commerceItems.size() == 0) {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		} else {
			List<CommerceItem> modifiableCommerceItems = new ArrayList<CommerceItem>(commerceItems);
			if(SORT_OPTION_ITEM_NUMBER.equals(sortOption)){
				Collections.sort(modifiableCommerceItems, getComparatorByItemNumber());
			} else if (SORT_OPTION_ALIAS_NUMBER.equals(sortOption)){
				Profile profile = (Profile)ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/Profile");
				Collections.sort(modifiableCommerceItems, getComparatorByAlias(profile));
			} else if (SORT_OPTION_PRICE.equals(sortOption)){
				Collections.sort(modifiableCommerceItems, getComparatorByPrice());
			} else if(SORT_OPTION_DESCRIPTION.equals(sortOption)){
				Collections.sort(modifiableCommerceItems, getComparatorByDescription());
			}
			
			if(!StringUtils.isEmpty(sortOption) && !StringUtils.isEmpty(sortOptionType) && DESC_ORDER.equals(sortOptionType)){
				Collections.reverse(modifiableCommerceItems);
			}
			pRequest.setParameter(SORTED_COMMERCE_ITEMS, modifiableCommerceItems);
			pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
	}
	
	/**
	 * get comparator to sort by item number
	 * @return comparator
	 */
	public Comparator<CommerceItem> getComparatorByItemNumber() {
		return new Comparator<CommerceItem>() {
			@Override
			public int compare(CommerceItem ci1, CommerceItem ci2) {
				try {
					RepositoryItem pr1 = getCatalogTools().findProduct(ci1.getCatalogRefId());
					RepositoryItem pr2 = getCatalogTools().findProduct(ci2.getCatalogRefId());
					if(pr1 != null && pr2 != null){
						String diplayName1 = pr1.getItemDisplayName();
						String diplayName2 = pr2.getItemDisplayName();
						if(diplayName1 != null && diplayName2 != null){
							return diplayName1.compareTo(diplayName2);
						}
					}
				} catch (RepositoryException e) {
					vlogError(e, e.getMessage());
				}
				
				return 0;
			}
		};
	}
	
	/**
	 * get comparator to sort by alias
	 * @return comparator
	 */
	public Comparator<CommerceItem> getComparatorByAlias(final Profile pProfile) {
		return new Comparator<CommerceItem>() {
			@Override
			public int compare(CommerceItem ci1, CommerceItem ci2) {
				try {
					RepositoryItem pr1 = getCatalogTools().findProduct(ci1.getCatalogRefId());
					RepositoryItem pr2 = getCatalogTools().findProduct(ci2.getCatalogRefId());
					
					if(pr1 != null && pr2 != null){
						String alias1 = getOrganizationAliasService().findAlias(pProfile, pr1);
						String alias2 = getOrganizationAliasService().findAlias(pProfile, pr2);
						if(StringUtils.isEmpty(alias1) && !StringUtils.isEmpty(alias2)){
							return 1;
						} else if (!StringUtils.isEmpty(alias1) && StringUtils.isEmpty(alias2)){
							return -1;
						} else if (StringUtils.isEmpty(alias1) && StringUtils.isEmpty(alias2)){
							return 0;
						} else {
							return alias1.compareTo(alias2);
						}
					}
					
				} catch (RepositoryException e) {
					vlogError(e, e.getMessage());
				}
				return 0;
			}
		};
	}
	
	/**
	 * get comparator to sort by price
	 * @return comparator
	 */
	public Comparator<CommerceItem> getComparatorByPrice() {
		return new Comparator<CommerceItem>() {
			@Override
			public int compare(CommerceItem ci1, CommerceItem ci2) {
				if(ci1.getPriceInfo() != null && ci2.getPriceInfo() != null){
					if(ci1.getPriceInfo().getListPrice() - ci2.getPriceInfo().getListPrice() == 0){
						return 0;
					} else {
						return ci1.getPriceInfo().getListPrice() - ci2.getPriceInfo().getListPrice() <= 0 ? -1 : 1;
					}
				}
				return 0;
			}
		};
	}

	/**
	 * get description from commerceItem by combining description and descriptionLine2 of product.
	 * @param pCommerceItem commerce item
	 * @return combined description
	 */
	private String getDescription(CommerceItem pCommerceItem){
		StringBuilder result = new StringBuilder();

		Object productRef = pCommerceItem.getAuxiliaryData().getProductRef();
		if(productRef instanceof RepositoryItem){
			RepositoryItem product = (RepositoryItem) productRef;

			result.append(product.getPropertyValue("description"));

//			Object descriptionLine2 = product.getPropertyValue("descriptionLine2");
//			if(descriptionLine2 != null) {
//				result.append(", ").append(descriptionLine2);
//			}
		}

		return result.toString();
	}

	public Comparator<CommerceItem> getComparatorByDescription() {
		return new Comparator<CommerceItem>() {
			@Override
			public int compare(CommerceItem ci1, CommerceItem ci2) {

				String description1 = getDescription(ci1);
				String description2 = getDescription(ci2);

				return description1.compareTo(description2);
			}
		};
	}

	public CatalogTools getCatalogTools() {
		return mCatalogTools;
	}

	public void setCatalogTools(CatalogTools pCatalogTools) {
		this.mCatalogTools = pCatalogTools;
	}
	
	public OrganizationAliasService getOrganizationAliasService() {
		return mOrganizationAliasService;
	}

	public void setOrganizationAliasService(
			OrganizationAliasService pOrganizationAliasService) {
		this.mOrganizationAliasService = pOrganizationAliasService;
	}
}
