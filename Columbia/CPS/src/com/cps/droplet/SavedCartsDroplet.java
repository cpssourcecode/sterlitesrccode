package com.cps.droplet;

import atg.commerce.CommerceException;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.ShippingAddressContainer;
import atg.commerce.order.ShippingGroup;
import atg.core.util.ContactInfo;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.commerce.order.CPSSavedCartBean;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.cps.util.CPSConstants.*;

public class SavedCartsDroplet extends DynamoServlet {

	private static final String COMPONENT_NAME = "/cps/droplet/SavedCartsDroplet";

	private static final String ORDER_STATE_INCOMPLETE = "INCOMPLETE";

	private static final ParameterName PROFILE = ParameterName.getParameterName("profile");

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

	private static final String RETRIEVE_ORDERS_RQL_QUERY = "state =?0 AND profileId =?1 AND COUNT (commerceItems) > 0";

	private CPSOrderManager mOrderManager;

	public CPSOrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(CPSOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	private Repository mLocationRepository;

	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - start").toString());
		}

		List<Order> orders = null;
		Profile profile = (Profile)pRequest.getObjectParameter(PROFILE);
		if (profile != null) {
			orders = retrieveOrders(profile);
		}

		ParameterName renderParam = OUTPUT_OPARAM;
		if (orders != null && orders.size() > 0) {
			List<CPSSavedCartBean> orderInfos = new ArrayList<CPSSavedCartBean>();

			OrderHolder shoppingCart = (OrderHolder)pRequest.resolveName(SHOPPING_CART_PATH);
			Order currentOrder = shoppingCart.getCurrent();

			for (Order order : orders) {
				if (currentOrder != null && order.getId().equals(currentOrder.getId())) {
					continue;
				}
				CPSSavedCartBean orderBean = new CPSSavedCartBean();
				orderBean.setId(order.getId());
				orderBean.setProfileId(order.getProfileId());
				orderBean.setSavedDate(order.getLastModifiedDate());
				ContactInfo address = null;
				List<ShippingGroup> shippingGroups = order.getShippingGroups();
				if (shippingGroups != null && shippingGroups.size() > 0) {
					ShippingGroup sg = (ShippingGroup)order.getShippingGroups().get(0);
					if (sg instanceof ShippingAddressContainer) {
						address = (ContactInfo)((ShippingAddressContainer) sg).getShippingAddress();
					} else if (sg instanceof InStorePickupShippingGroup) {
						String locationId = ((InStorePickupShippingGroup)sg).getLocationId();
						try {
							RepositoryItem store = getLocationRepository().getItem(locationId, STORE_ITEM_TYPE);
							if (store != null) {
								address = new ContactInfo();
								//address.setCompanyName((String)store.getPropertyValue(COMPANY_NAME));
								address.setAddress1((String)store.getPropertyValue(ADDRESS1));
								address.setAddress2((String)store.getPropertyValue(ADDRESS2));
								address.setAddress3((String)store.getPropertyValue(ADDRESS3));
								address.setCity((String)store.getPropertyValue(CITY));
								address.setState((String)store.getPropertyValue(STATE_ADDRESS));
								address.setPostalCode((String)store.getPropertyValue(POSTAL_CODE));
								address.setCountry((String)store.getPropertyValue(COUNTRY));
							}
						} catch (RepositoryException e) {
							if (isLoggingError()) {
								logError(e);
							}
						}
					}
				}
				orderBean.setAddress(address);
				orderInfos.add(orderBean);
			}

			pRequest.setParameter("orders", orderInfos);
			//pRequest.setParameter("orders", orders);
			pRequest.setParameter("ordersSize", orderInfos.size());
		} else {
			renderParam = EMPTY_OPARAM;
		}
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - end").toString());
		}

	}

	private List<Order> retrieveOrders(Profile pProfile) {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".retrieveOrders").append(" - start").toString());
		}

		Object[] params = new Object[2];
		params[0] = ORDER_STATE_INCOMPLETE;
		params[1] = pProfile.getRepositoryId();

		List<Order> orderList = null;

		try {
			orderList = getOrderManager().retrieveOrders(params, RETRIEVE_ORDERS_RQL_QUERY);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".retrieveOrders").append(" - exit").toString());
		}
		return orderList;

	}

}