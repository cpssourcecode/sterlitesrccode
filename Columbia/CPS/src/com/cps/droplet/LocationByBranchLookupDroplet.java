package com.cps.droplet;

import static com.cps.util.CPSConstants.*;

import java.io.IOException;
import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;


/**
 * @author Andy Porter
 */
public class LocationByBranchLookupDroplet extends DynamoServlet {


	public static final ParameterName BRANCH_ID_PARAM = ParameterName.getParameterName(STORE_BRANCH_ID_PRTY);


	private final static String STORE_OUTPUT = "store";

	/**
	 * LocationRepository
	 */
	private Repository mLocationRepository;

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		ParameterName renderParam = DropletConstants.OUTPUT;
		ParameterName emptyParam = DropletConstants.EMPTY_OUTPUT;

		String branchId = (String) pRequest.getObjectParameter(BRANCH_ID_PARAM);
		if (StringUtils.isNotBlank(branchId)) {
			// branchId id entered, find and return store object
			RepositoryItem store = getStore(branchId);

			pRequest.setParameter(STORE_OUTPUT, store);
			pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(emptyParam, pRequest, pResponse);
		}


	}

	private RepositoryItem getStore(String pBranchId) {
		RepositoryItem store = null;

		try {
			String rqlQuery = STORE_BRANCH_ID_PRTY + "=" + pBranchId;
			RepositoryView rpView = getLocationRepository().getView(STORE_ITEM_TYPE);
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);

			RepositoryItem[] results = stmt.executeQuery(rpView, null);

			if (results != null && results.length == 1) {
				store = results[0];
			}
		} catch (Exception e) {
			vlogError(e, "LocationByBranchLookupDroplet.getStore:");
		}

		return store;
	}

	/**
	 * Gets LocationRepository.
	 *
	 * @return Value of LocationRepository.
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * Sets new LocationRepository.
	 *
	 * @param pLocationRepository New value of LocationRepository.
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}
}
