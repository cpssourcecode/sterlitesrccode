package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.service.CPSProductPricingService;
import com.cps.userprofiling.service.ProfileRecentlyViewedService;

import atg.beans.DynamicBeanMap;
import atg.commerce.order.OrderHolder;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * @author Alex Turchynovich.
 */
public class CPSRecentlyViewedDroplet extends DynamoServlet {
    private static final String STOCKING_TYPE = "stockingType";
    private static final String OBSOLETE_STOCKING_TYPE = "O";
    private static final String USE_UP_STOCKING_TYPE = "U";
    private static final String K_STOCKING_TYPE = "K";
    private static final String X_STOCKING_TYPE = "X";
    private static final String FOUR_STOCKING_TYPE = "4";

    private static final String PARAM_LENGTH = "length";
    private static final String PARAM_ELEMENTS = "elements";
    private static final String ITEM_PRICES = "itemPrices";
    private static final String PARAM_PRICES = "prices";
    private static final String PARAM_PROFILE_RECENTLY_VIEWED_SERVICE = "profileRecentlyViewedService";
    private static final String PARAM_SHOPPING_CART = "shoppingCart";


    private static final String OPARAM_OUTPUT = "output";
    private static final String OPARAM_EMPTY = "empty";

    private CPSProductPricingService productPricingService;

    /**
     * @return the productPricingService
     */
    public CPSProductPricingService getProductPricingService() {
        return productPricingService;
    }

    /**
     * @param productPricingService
     *            the productPricingService to set
     */
    public void setProductPricingService(CPSProductPricingService productPricingService) {
        this.productPricingService = productPricingService;
    }

    private List<DynamicBeanMap> getRecentlyViewedItems(ProfileRecentlyViewedService pRecentlyViewedService) {
        List<DynamicBeanMap> result = new ArrayList<>();

        if (pRecentlyViewedService != null) {
            for (RepositoryItem item : pRecentlyViewedService.getRecentlyViewed()) {
                if (isItemAvailable(item)) {
                    result.add(new DynamicBeanMap(item));
                }
            }
        }

        return result;
    }



    private void priceItems(DynamoHttpServletRequest pRequest, ProfileRecentlyViewedService pProfileRecentlyViewedService) {
		try {
			if (pProfileRecentlyViewedService != null) {
				Set<String> products = new HashSet<>();
				for (RepositoryItem item : pProfileRecentlyViewedService.getRecentlyViewed()) {
					products.add(item.getRepositoryId());
				}
				if (!products.isEmpty()) {
                    OrderHolder shoppingCart = (OrderHolder) pRequest.getObjectParameter(PARAM_SHOPPING_CART);
                    Map<String, Double> itemPrices = getProductPricingService().getCustomerItemPrice(products, pProfileRecentlyViewedService.getProfile(),
                                    (CPSOrderImpl) shoppingCart.getCurrent());
					pRequest.setParameter(ITEM_PRICES, itemPrices);
				}
			}
		} catch (Exception ex) {
			if (isLoggingError()) {
				logError(ex);
			}
		}
	}
    
    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        ProfileRecentlyViewedService profileRecentlyViewedService = (ProfileRecentlyViewedService)req.getObjectParameter(PARAM_PROFILE_RECENTLY_VIEWED_SERVICE);
        List<DynamicBeanMap> elements = getRecentlyViewedItems(profileRecentlyViewedService);
        req.setParameter(PARAM_LENGTH, elements.size());
        req.setParameter(PARAM_ELEMENTS, elements);
        if (elements.size() > 4 && includePrices(req)) {
        	priceItems(req, profileRecentlyViewedService);
        }

        if (elements.isEmpty()){
            req.serviceParameter(OPARAM_EMPTY, req, res);
        } else {
            req.serviceParameter(OPARAM_OUTPUT, req, res);
        }

    }

    private boolean includePrices(DynamoHttpServletRequest pRequest) {
    	return Boolean.TRUE.equals(pRequest.getLocalParameter(PARAM_PRICES));
    }

    private boolean isItemAvailable(RepositoryItem item) {
        String itemStockingType = (String) item.getPropertyValue(STOCKING_TYPE);
        return !itemStockingType.equals(OBSOLETE_STOCKING_TYPE) && !itemStockingType.equals(USE_UP_STOCKING_TYPE) &&
                !itemStockingType.equals(K_STOCKING_TYPE) && !itemStockingType.equals(X_STOCKING_TYPE);
    }


}
