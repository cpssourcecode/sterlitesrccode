package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Droplet used to generate category tree values.
 *
 * @author osednev
 */
public class CategoryNavigationTreeDroplet extends DynamoServlet {

	/**
	 * The Constant OUTPUT_OPARAM.
	 */
	private static final String OUTPUT_OPARAM = "output";

	/**
	 * The Constant EMPTY_OPARAM.
	 */
	private static final String EMPTY_OPARAM = "empty";

	/**
	 * The catalog input param.
	 */
	private static final String CATALOG_PARAM = "catalog";

	/**
	 * The rootCategories.
	 */
	private static final String MENU_ROOT_CATEGORIES = "menuRootCategories";

	/**
	 * The zeroResults.
	 */
	private static final String ZERO_RESULTS = "zeroResults";

	/**
	 * The NAVIGATION_MENU.
	 */
	private static final String NAVIGATION_MENU = "navmenu";

	/**
	 * The fixedChildCategories.
	 */
	private static final String FIXED_CHILD_CATEGORIES = "fixedChildCategories";

	/**
	 * The rootCategoriesMap.
	 */
	private static final String ROOT_CATEGORIES_MAP = "rootCategoriesMap";

	/**
	 * The childCategoriesMap.
	 */
	private static final String CHILD_CATEGORIES_MAP = "childCategoriesMap";

	/**
	 * The childCategoriesMap.
	 */
	private static final String CAT_COUNT = "catCount";

	/**
	 * The childCategoriesMap.
	 */
	private static final String SUB_CAT_COUNT = "subCatCount";

	/**
	 * The all count.
	 */
	private static final String ALL_COUNT = "-1";

	/**
	 * The menuList.
	 */
	private static final String MENU_LIST = "menuList";

	private String mWebCategoryEmptyCategory = "ZZZ";

	private boolean mEnableWebCategoryEmptyCategory = false;

	/**
	 * Dimension value cache tools
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools = null;

	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		this.mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	/**
	 * Maximum category count limit
	 */
	private int mMaximumCount = 400;

	public int getMaximumCount() {
		return mMaximumCount;
	}

	public void setMaximumCount(int pMaximumCount) {
		this.mMaximumCount = pMaximumCount;
	}

	/**
	 * configured name of Products menu
	 * (if id or name matches with menu folder data will be used for menu build)
	 */
	private String mProductsMenuName = "Products";

	public String getProductsMenuName() {
		return mProductsMenuName;
	}

	public void setProductsMenuName(String pProductsMenuName) {
		mProductsMenuName = pProductsMenuName;
	}

	/**
	 * configured id of Products menu
	 * (if id or name matches with menu folder data will be used for menu build)
	 */
	private String mProductsMenuId = "products";

	public String getProductsMenuId() {
		return mProductsMenuId;
	}

	public void setProductsMenuId(String pProductsMenuId) {
		mProductsMenuId = pProductsMenuId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		final List<NavigationMenuItem> menuList = new ArrayList<NavigationMenuItem>();

		Object catalogParam = pRequest.getObjectParameter(CATALOG_PARAM);
		if (catalogParam instanceof RepositoryItem && null != getDimensionValueCacheTools()) {
			RepositoryItem catalog = (RepositoryItem) catalogParam;
			String isZeroResults = pRequest.getParameter(ZERO_RESULTS);
			if (StringUtils.isNotEmpty(isZeroResults)) {
				createMenuFromRootCategories(pRequest, catalog, menuList);
			} else {
				RepositoryItem products = null;
				List<RepositoryItem> navmenu = (List<RepositoryItem>) catalog.getPropertyValue(NAVIGATION_MENU);
				if (navmenu != null) {
					for (RepositoryItem folder : navmenu) {
						String name = (String) folder.getPropertyValue("name");
						if (getProductsMenuName().equals(name) || getProductsMenuId().equals(folder.getRepositoryId())) {
							products = folder;
							break;
						}
					}
				}
				if (null != products) {
					List<RepositoryItem> subfolders = (List<RepositoryItem>) products.getPropertyValue("subfolders");
					if (null != subfolders && !subfolders.isEmpty()) {
						for (RepositoryItem subfolder : subfolders) {
							RepositoryItem menu = (RepositoryItem) subfolder.getPropertyValue("menu");
							if (null != menu) {
								NavigationMenuItem cnav = getCategoryMenuItem(menu);
								menuList.add(cnav);
								List<RepositoryItem> subfolders2 = (List<RepositoryItem>) subfolder.getPropertyValue("subfolders");
								if (null != subfolders2 && !subfolders2.isEmpty()) {
									for (RepositoryItem subfolder2 : subfolders2) {
										RepositoryItem menu2 = (RepositoryItem) subfolder2.getPropertyValue("menu");
										if (null != menu2) {
											cnav.addMenuItem(getCategoryMenuItem(menu2));
										}
									}
								}
								List<RepositoryItem> submenu2 = (List<RepositoryItem>) subfolder.getPropertyValue("submenu");
								if (null != submenu2 && !submenu2.isEmpty()) {
									for (RepositoryItem menu2 : submenu2) {
										cnav.addMenuItem(getCategoryMenuItem(menu2));
									}
								}
							}
						}
					}
					List<RepositoryItem> submenu = (List<RepositoryItem>) products.getPropertyValue("submenu");
					if (null != submenu && !submenu.isEmpty()) {
						for (RepositoryItem menu : submenu) {
							NavigationMenuItem cnav = getCategoryMenuItem(menu);
							menuList.add(cnav);
						}
					}
				}
				if (menuList.isEmpty()) {
					createMenuFromRootCategories(pRequest, catalog, menuList);
				}
			}
		}

		if (menuList.isEmpty()) {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		} else {
			pRequest.setParameter(MENU_LIST, menuList);
			pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
	}

	private void createMenuFromRootCategories(DynamoHttpServletRequest pRequest, RepositoryItem catalog, List<NavigationMenuItem> menuList) {
		List<RepositoryItem> rootCats = (List<RepositoryItem>) catalog.getPropertyValue(MENU_ROOT_CATEGORIES);
		vlogDebug("CategoryNavigation sorted root cats: {0}", rootCats);
		rootCats = getCatsLimited(rootCats, getCountValue(pRequest.getParameter(CAT_COUNT)));
		vlogDebug("CategoryNavigation limited root cats: {0} | cat count: {1}", rootCats, pRequest.getParameter(CAT_COUNT));
		setRootCatsNavigation(rootCats, menuList, pRequest);
	}

	private NavigationMenuItem getCategoryMenuItem(final RepositoryItem pMenu) {
		String link = (String) pMenu.getPropertyValue("link");
		String name = (String) pMenu.getPropertyValue("name");
		String image = "";
		RepositoryItem category = null;
		try {
			category = (RepositoryItem) pMenu.getPropertyValue("category");
		} catch (Exception e) {
			category = null;
		}
		if (null != category) {
			image = (String) category.getPropertyValue("thumbnail_url");
			if (StringUtils.isBlank(name) || StringUtils.isBlank(link)) {
				if (StringUtils.isBlank(name)) {
					name = (String) category.getPropertyValue("displayName");
				}
				if (StringUtils.isBlank(link)) {
					String categoryId = category.getRepositoryId();
					if (StringUtils.isNotBlank(categoryId)) {
						DimensionValueCacheObject dvco = getDimensionValueCacheTools().get(categoryId, null);
						if (null != dvco) {
							link = dvco.getUrl();
						}
					}
				}
			}
		}
		return new NavigationMenuItem(link.toLowerCase(), name, image);
	}


	/**
	 * Gets the count int value from string
	 *
	 * @param pCountvalue count value
	 * @return result
	 */
	protected int getCountValue(String pCountvalue) {
		int result = 0;

		try {
			if (!StringUtils.isBlank(pCountvalue)) {
				if (ALL_COUNT.equals(pCountvalue)) {
					result = getMaximumCount();
				} else {
					result = Integer.parseInt(pCountvalue);
				}
			}
		} catch (Exception e) {
			logError(e);
		}
		return result;
	}

	/**
	 * Gets root categories map to print on the web ui
	 *
	 * @param pRootCats root cats
	 * @return result map
	 */
	protected void setRootCatsNavigation(List<RepositoryItem> pRootCats, List<NavigationMenuItem> menuList, DynamoHttpServletRequest pRequest) {
		if (null != pRootCats && !pRootCats.isEmpty()) {
			String catId = null;
			DimensionValueCacheObject dvco = null;
			for (RepositoryItem cat : pRootCats) {
				catId = cat.getRepositoryId();
				if (StringUtils.isNotBlank(catId)) {
					dvco = getDimensionValueCacheTools().get(catId, null);
				}
				if (null != dvco) {
					NavigationMenuItem cnav = new NavigationMenuItem(dvco.getUrl().toLowerCase(), cat.getItemDisplayName(), (String) cat.getPropertyValue("thumbnail_url"));
					getSubCatsForRoot(cat, cnav, pRequest);
					menuList.add(cnav);
				}
			}
		}
	}

	/**
	 * Gets sub categories map for root category
	 *
	 * @param pRootCat root cat
	 * @param cnav     sub cats count
	 * @param pRequest request
	 * @return result map
	 */
	@SuppressWarnings("unchecked")
	protected void getSubCatsForRoot(RepositoryItem pRootCat, NavigationMenuItem cnav, DynamoHttpServletRequest pRequest) {
		int subCatsCount = getCountValue(pRequest.getParameter(SUB_CAT_COUNT));
		if (null != pRootCat) {
			List<RepositoryItem> subCats = (List<RepositoryItem>) pRootCat.getPropertyValue(FIXED_CHILD_CATEGORIES);
			if (null != subCats && !subCats.isEmpty()) {
				DimensionValueCacheObject dvco = null;
				String catId = null;
				int index = 0;
				for (RepositoryItem cat : subCats) {
					catId = cat.getRepositoryId();
					dvco = getDimensionValueCacheTools().get(catId, null);
					if (null != dvco) {
						index++;
						if (index > subCatsCount) {
							break;
						}
						cnav.addMenuItem(new NavigationMenuItem(dvco.getUrl(), (String) cat.getPropertyValue("displayName")));
					}
				}
			}
		}
	}


	/**
	 * Gets limited size of categories
	 *
	 * @param pCats          categories
	 * @param pRootCatsCount cats count
	 * @return result set
	 */
	protected List<RepositoryItem> getCatsLimited(List<RepositoryItem> pCats, int pRootCatsCount) {
		List<RepositoryItem> result = new ArrayList<RepositoryItem>();

		if (null != pCats && !pCats.isEmpty()) {
			try {
				int index = 0;
				for (RepositoryItem rootCat : pCats) {
					if (index >= pRootCatsCount) {
						break;
					}
					if (!rootCat.getRepositoryId().equalsIgnoreCase(getWebCategoryEmptyCategory()) ||
							(rootCat.getRepositoryId().equalsIgnoreCase(getWebCategoryEmptyCategory()) && isEnableWebCategoryEmptyCategory())) {
						result.add(rootCat);
					}
					index++;
				}
			} catch (Exception e) {
				logError(e);
			}
		}

		return result;
	}

	public class NavigationMenuItem {

		private String mURL;

		private String mName;

		private String mImg;

		private List<NavigationMenuItem> mSubMenu;

		public NavigationMenuItem(final String pURL, final String pName, final String pImg) {
			mURL = pURL;
			mName = pName;
			mImg = pImg;
			mSubMenu = new ArrayList<NavigationMenuItem>();
		}

		public NavigationMenuItem(final String pURL, final String pName) {
			mURL = pURL;
			mName = pName;
			mSubMenu = new ArrayList<NavigationMenuItem>();
		}

		public void addMenuItem(final NavigationMenuItem pNavigationMenuItem) {
			mSubMenu.add(pNavigationMenuItem);
		}

		public String getURL() {
			return mURL;
		}

		public void setURL(final String pURL) {
			mURL = pURL;
		}

		public String getName() {
			return mName;
		}

		public void setName(final String pName) {
			mName = pName;
		}

		public List<NavigationMenuItem> getSubMenu() {
			return mSubMenu;
		}

		public void setSubMenu(final List<NavigationMenuItem> pSubMenu) {
			mSubMenu = pSubMenu;
		}

		public String getImg() {
			return mImg;
		}

		public void setImg(String pImg) {
			this.mImg = pImg;
		}


	}

	public boolean isEnableWebCategoryEmptyCategory() {
		return mEnableWebCategoryEmptyCategory;
	}

	public void setEnableWebCategoryEmptyCategory(boolean pEnableWebCategoryEmptyCategory) {
		mEnableWebCategoryEmptyCategory = pEnableWebCategoryEmptyCategory;
	}

	public String getWebCategoryEmptyCategory() {
		return mWebCategoryEmptyCategory;
	}

	public void setWebCategoryEmptyCategory(String pWebCategoryEmptyCategory) {
		mWebCategoryEmptyCategory = pWebCategoryEmptyCategory;
	}
}