package com.cps.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import com.cps.commerce.inventory.CPSInventoryManager;
import com.cps.commerce.order.purchase.CPSPriceAvailability;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class CPSInventoryLookupDroplet extends DynamoServlet {
	static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	static final ParameterName ITEM_ID = ParameterName.getParameterName("itemId");
	static final ParameterName ORDER = ParameterName.getParameterName("order");
	static final ParameterName PA_LIST = ParameterName.getParameterName("paList");
	static final ParameterName PAGE_FROM = ParameterName.getParameterName("pageFrom");
	static final String INVENTORY_LEVEL_REP = "inventoryLevel";
	static final String INVENTORY_STOCK_LEVEL = "stockLevel";
	static final String AVAILABILITY_STATUS = "availabilityStatus";
	static final String AVAILABILITY_MSG = "availabilityMessage";
	
	static final String STOCK_ITEMS_MSG = "Available for Delivery";
	static final String SHIPS_FROM_MANUF_MSG = "Ships from Manufacturer";
	static final String PARTIALLY_AVAL_MSG = "Partially Available";
	
	static final String CART_PAGE = "cartPage";
	static final String PA_PAGE = "paPage";

	private CPSInventoryManager mInventoryManager;

	public CPSInventoryManager getInventoryManager() {
		return mInventoryManager;
	}

	public void setInventoryManager(CPSInventoryManager pInventoryManager) {
		mInventoryManager = pInventoryManager;
	}

	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse)throws ServletException, IOException {

		try{
			// get request parameters
			String skuId = pRequest.getParameter(ITEM_ID);
			Order order = (Order)pRequest.getObjectParameter(ORDER);
			List<CPSPriceAvailability> paList = (List<CPSPriceAvailability>) pRequest.getObjectParameter(PA_LIST);
			String pageFrom = pRequest.getParameter(PAGE_FROM);
			
			if (isLoggingDebug()) {
				logDebug("CPSInventoryLookupDroplet start: skuId=" + skuId);
				logDebug("order=" + order);
				logDebug("paList=" + paList);
				logDebug("pageFrom=" + pageFrom);
			}
			
			//check to make sure they were passed into request
			if(!StringUtils.isEmpty(skuId)) {
				pRequest.setParameter(INVENTORY_STOCK_LEVEL, getInventoryManager().getStockLevel(skuId));
			} else {
				pRequest.setParameter(INVENTORY_STOCK_LEVEL, 0);
			}
			if (!StringUtils.isBlank(pageFrom) && pageFrom.equalsIgnoreCase(CART_PAGE)){
				pRequest.setParameter(AVAILABILITY_MSG, getInventoryManager().getStockMessage(skuId, order, null, true));
			} else if (!StringUtils.isBlank(pageFrom) && pageFrom.equalsIgnoreCase(PA_PAGE)){
				pRequest.setParameter(AVAILABILITY_MSG, getInventoryManager().getStockMessage(skuId, order, paList, false));
			}
			
			pRequest.setParameter(AVAILABILITY_STATUS, getInventoryManager().getAvailabilityStatus(skuId));
			pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);

		}catch (Exception ex){
			if (isLoggingError()) {
				logError(ex);
			}
		}
	}

}
