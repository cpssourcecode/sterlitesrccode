package com.cps.droplet;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.util.CPSConstants;

import static com.cps.util.CPSConstants.*;
import static com.cps.util.CPSConstants.POSTAL_CODE;

public class LocationLookupDroplet extends DynamoServlet {
	
	private final static String LOCATION_ID = "location";

	private static final ParameterName SEARCH_TERM = ParameterName.getParameterName("filter");

	private final static String LOCATION_MAP = "locationMap";
	
	private final static String ITEM_DESCRIPTOR_STORE = "store";
	
	private final static String STORE_OUTPUT = "store";
	private final static String STORE_IMG_OUTPUT = "storeIMG";
	
	private Repository locationRepository;
	
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
			throws ServletException, IOException {
		vlogDebug("Location lookup droplet");
		ParameterName renderParam = DropletConstants.OUTPUT;
		
		String locationId = pRequest.getParameter(LOCATION_ID);

		vlogDebug("locationId: "+locationId);

		if (StringUtils.isNotBlank(locationId)){
			// Location id entered, find and return location object
			RepositoryItem store = null;
			String imgURL = null;
			
			store = getStore(locationId);
			// Get store image
			if (store != null){
				List<RepositoryItem> mediaContent = (List<RepositoryItem>) store.getPropertyValue(CPSConstants.RELATED_MEDIA_CONTENT);
				if (mediaContent != null && !mediaContent.isEmpty()){
					RepositoryItem content = mediaContent.iterator().next();
					if (content != null){
						imgURL = (String) content.getPropertyValue(CPSConstants.URL);
					}
				}
			}
			
			pRequest.setParameter(STORE_IMG_OUTPUT, imgURL);
			pRequest.setParameter(STORE_OUTPUT, store);
		} else {
			// No location entered, return map of locations
			Map<String, List<RepositoryItem>> locationMap = null;

			String filter = pRequest.getParameter(SEARCH_TERM);
			
			locationMap = generateLocationMap(filter);
			vlogDebug("locationMap: "+locationMap);
			pRequest.setParameter(LOCATION_MAP, locationMap);
		}
		
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
	}
	
	private Map<String, List<RepositoryItem>> generateLocationMap(String filter) {
		Map<String, List<RepositoryItem>> locationMap = new HashMap<String, List<RepositoryItem>>();

		try {
			String rqlQuery = "ALL";
			RepositoryView rpView = getLocationRepository().getView(ITEM_DESCRIPTOR_STORE);
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
	
			RepositoryItem[] results = stmt.executeQuery(rpView, null);

			if (results != null && results.length > 0) {
				// Locations found
				for (RepositoryItem result : results) {
					String region = (String)result.getPropertyValue(CPSConstants.REGION);

                    if(isAddressSuitable(result, filter)) {
                        if (locationMap.containsKey(region)) {
                            // Region already added to map, add item to list
                            List<RepositoryItem> list = locationMap.get(region);
                            list.add(result);
                            locationMap.put(region, list);
                        } else {
                            // Region not added yet, add new region
                            List<RepositoryItem> newList = new ArrayList<RepositoryItem>();
                            newList.add(result);
                            locationMap.put(region, newList);
                        }
                    }
				}
			}
		} catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		
		return locationMap;
	}
	
	private RepositoryItem getStore (String locationId) {
		RepositoryItem store = null;
		
		try {
			String rqlQuery = "locationId = "+locationId;
			RepositoryView rpView = getLocationRepository().getView(ITEM_DESCRIPTOR_STORE);
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
			
			RepositoryItem[] results = stmt.executeQuery(rpView, null);
			
			if (results != null && results.length == 1) {
				store = results[0];
			}
		}catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		
		return store;
	}

	private static final List<String> searchProperties = Arrays.asList(ADDRESS1, ADDRESS2, CITY, STATE_ADDRESS, POSTAL_CODE, REGION);

    /**
     * https://vachiosolutions.atlassian.net/wiki/display/CPSC/Manage+Addresses
     * Shipping Address Name, Shipping Address Line, Shipping City, Shipping State, Shipping Zipcode
     *
     * @param pAddress address item to check
     * @param pFilter  filter input
     * @return true if address is suitable
     */
    private boolean isAddressSuitable(RepositoryItem pAddress, String pFilter) {
        if(StringUtils.isBlank(pFilter))
            return true;

        for(String searchProperty : searchProperties){
            String value = (String)pAddress.getPropertyValue(searchProperty);
            if(doStringMatch(value, pFilter))
                return true;
        }

        return false;
    }

    /**
     * @param pStr1 string to check
     * @param pStr2 filter string
     * @return true if 1st string contains 2nd string
     */
    private boolean doStringMatch(String pStr1, String pStr2) {
        return pStr1 != null && pStr2 != null && pStr1.toLowerCase().contains(pStr2.trim().toLowerCase());
    }

	public Repository getLocationRepository() {
		return locationRepository;
	}

	public void setLocationRepository(Repository locationRepository) {
		this.locationRepository = locationRepository;
	}
}