package com.cps.droplet;

import atg.commerce.order.Order;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.commerce.payment.SnapPayConnectionManager;

import javax.servlet.ServletException;
import java.io.IOException;

public class SnapPayFormDroplet extends DynamoServlet {

	private static final String COMPONENT_NAME = "/cps/droplet/SnapPayFormDroplet";

	private static final ParameterName PROFILE = ParameterName.getParameterName("profile");
	private static final ParameterName ORDER = ParameterName.getParameterName("order");
	private static final ParameterName MAINTAIN_CREDIT_CARDS = ParameterName.getParameterName("maintainCreditCards");

	private static final String REQUEST_NUMBER = "requestNumber";

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");

	private String mIframeUrl;

	public void setIframeUrl(String pIframeUrl) {
		mIframeUrl = pIframeUrl;
	}

	public String getIframeUrl() {
		return mIframeUrl;
	}

	private SnapPayConnectionManager mSnapPayConnectionManager;

	public void setSnapPayConnectionManager(SnapPayConnectionManager pSnapPayConnectionManager) {
		mSnapPayConnectionManager = pSnapPayConnectionManager;
	}

	public SnapPayConnectionManager getSnapPayConnectionManager() {
		return mSnapPayConnectionManager;
	}

	private CPSOrderManager mOrderManager;

	public void setOrderManager(CPSOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public CPSOrderManager getOrderManager() {
		return mOrderManager;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - start").toString());

		Order order = (Order)pRequest.getObjectParameter(ORDER);
		Profile profile = (Profile)pRequest.getObjectParameter(PROFILE);
		Boolean maintainCreditCards = (Boolean)pRequest.getObjectParameter(MAINTAIN_CREDIT_CARDS);
		if (maintainCreditCards == null) {
			maintainCreditCards = false;
		}
		String requestNumber = getSnapPayConnectionManager().setTransactionDetails(order, profile, maintainCreditCards);
		ParameterName renderParam = !StringUtils.isBlank(requestNumber) ? OUTPUT_OPARAM : ERROR_OPARAM;
		if (!maintainCreditCards && !StringUtils.isBlank(requestNumber)) {
			// set request number for checkout process
			getOrderManager().updateOrderWithRequestNumber(order, profile, requestNumber);
		}
		pRequest.setParameter(REQUEST_NUMBER, requestNumber);
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - end").toString());

	}

}