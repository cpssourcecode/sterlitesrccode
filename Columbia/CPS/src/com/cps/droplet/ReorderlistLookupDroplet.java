package com.cps.droplet;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistManager;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.cps.service.GiftListService;

public class ReorderlistLookupDroplet extends DynamoServlet {
	public static final ParameterName GIFT_ID = ParameterName.getParameterName("id");
	public static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	
	private GiftlistManager giftlistManager;
	/**
	 * gift list service
	 */
	private GiftListService mGiftListService;

	//------------------------------------------------------------------------------------------------------------------

	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException{
		String giftlistId = pRequest.getParameter(GIFT_ID);
		RepositoryItem giftlist = null;
		try {
			giftlist = getGiftlistManager().getGiftlist(giftlistId);
		} catch (CommerceException ce) {
			if (isLoggingError()) {
				logError(ce);
			}
		}
		ParameterName renderParam = OUTPUT;
		pRequest.setParameter("giftlist", giftlist);
		Set<RepositoryItem> giftlistItems = getGiftListService().getAvailableGiftListItems(giftlist);
		pRequest.setParameter("giftlist-items", giftlistItems);
		pRequest.setParameter("totalGiftlistItems", getGiftListService().getDesiredQty(giftlistItems));
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
	}

	//------------------------------------------------------------------------------------------------------------------

	public GiftlistManager getGiftlistManager() {
		return giftlistManager;
	}

	public void setGiftlistManager(GiftlistManager giftlistManager) {
		this.giftlistManager = giftlistManager;
	}

	public GiftListService getGiftListService() {
		return mGiftListService;
	}

	public void setGiftListService(GiftListService pGiftListService) {
		mGiftListService = pGiftListService;
	}
}