package com.cps.droplet;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;
import atg.userprofiling.Profile;

import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;

/**
 * Takes in user id and role and checks if user has role assigned
 * Returns true if user has role, false otherwise
 *
 * @author root
 */
public class RoleLookupDroplet extends DynamoServlet {

	public static ParameterName USER_ID = ParameterName.getParameterName("userId");
	public static ParameterName ROLE = ParameterName.getParameterName("role");

	public static ParameterName TRUE = ParameterName.getParameterName("true");
	public static ParameterName FALSE = ParameterName.getParameterName("false");
	public static ParameterName OUTPUT = ParameterName.getParameterName("output");

	private CPSProfileTools mProfileTools;


	@SuppressWarnings("rawtypes")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		Profile profile = (Profile) ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/Profile");
		UserDirectory userDirectory = (UserDirectory) ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/ProfileUserDirectory");

		vlogDebug("Profile: " + profile);
		vlogDebug("UserDirectory: " + userDirectory);

		String userId = pRequest.getParameter(USER_ID);
		String roleString = pRequest.getParameter(ROLE);

		vlogDebug("In role lookup droplet to see if user: " + userId + " has role: " + roleString);

		String[] roles = null;
		if (!StringUtils.isBlank(roleString)) {
			roles = roleString.split(",");
		}

		User user = null;

		if (userDirectory != null) {
			user = userDirectory.findUserByPrimaryKey(userId);
		}

		if (user != null) {
			vlogDebug("User found, check roles");
			Collection collection = user.getAssignedRoles();

			if (roles != null && roles.length > 0) {
				if (userHasRole(collection, roles, profile)) {
					pRequest.serviceLocalParameter(TRUE, pRequest, pResponse);
					return;
				} else {
					pRequest.serviceLocalParameter(FALSE, pRequest, pResponse);
					return;
				}
			} else {
				// No roles passed in, return assigned roles in comma separated string
				vlogDebug("Return assigned roles");
				String returnRolesString = "";
				for (Iterator iterator = collection.iterator(); iterator.hasNext(); ) {
					Role role = (Role) iterator.next();
					if (role != null) {
						vlogDebug("Role name: " + role.getName());
						if (StringUtils.isBlank(returnRolesString)) {
							returnRolesString = getProfileTools().formatRole(role.getName());
						} else {
							returnRolesString += ", " + getProfileTools().formatRole(role.getName());
						}
					}
				}
				vlogDebug("Assigned Roles: " + returnRolesString);
				pRequest.setParameter(CPSConstants.USER_ROLES, returnRolesString);
				pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
				return;
			}
		} else {
			// No user found
			pRequest.serviceLocalParameter(FALSE, pRequest, pResponse);
			vlogDebug("No user found");
			return;
		}
	}


	@SuppressWarnings("rawtypes")
	protected boolean userHasRole(Collection collection, String[] s, Profile profile) {
		if (collection != null) {
			for (Iterator iterator = collection.iterator(); iterator.hasNext(); ) {
				Role role = (Role) iterator.next();
				if (role != null) {
					for (int i = 0; i < s.length; i++) {
						if (!StringUtils.isBlank((String) profile.getPropertyValue(CPSConstants.CSR_USER_ID))) {
							if (s[i].trim().equalsIgnoreCase(CPSConstants.ROLE_CSR_ADMIN)) {
								return true;
							}
						}
						if (role.getName().equalsIgnoreCase(s[i].trim())) {
							return true;
						}
					}
				}
			}
			if (!StringUtils.isBlank((String) profile.getPropertyValue(CPSConstants.CSR_USER_ID))) {
				for (int i = 0; i < s.length; i++) {
					if (s[i].trim().equalsIgnoreCase(CPSConstants.ROLE_CSR_ADMIN)) {
						return true;
					}
				}
			}
		} else {
			if (!StringUtils.isBlank((String) profile.getPropertyValue(CPSConstants.CSR_USER_ID))) {
				for (int i = 0; i < s.length; i++) {
					if (s[i].trim().equalsIgnoreCase(CPSConstants.ROLE_CSR_ADMIN)) {
						return true;
					}
				}
			}
		}
		return false;
	}


	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}


}