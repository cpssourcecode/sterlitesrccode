package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.cps.seo.SeoTools;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Droplet used to get dimension seo item.
 * 
 */
public class DimensionSeoItemDroplet extends DynamoServlet {

	/** The Constant OUTPUT_OPARAM. */
	private static final String OUTPUT_OPARAM = "output";

	/** The Constant EMPTY_OPARAM. */
	private static final String EMPTY_OPARAM = "empty";

	/** The dimval item. */
	private static final String DIMVAL_ITEM = "dimValItem";

	/** The dimval name input param. */
	private static final String DIMVAL_NAME_PARAM = "dimValName";

	/**
	 * Dimension value cache tools
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools = null;

	/**
	 * Maximum category count limit
	 */
	private int mMaximumCount = 400;

	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		DimensionValueCacheObject dvco = null;
		String dimValName = pRequest.getParameter(DIMVAL_NAME_PARAM);
		if (!StringUtils.isBlank(dimValName)) {
			String dimValSeoName = SeoTools.seoName(dimValName);
			if (!StringUtils.isBlank(dimValSeoName)) {
				dvco = getDimensionValueCacheTools().get(dimValSeoName, null);
			}
		}
		
		//logDebug("CategoryNavigation droplet results: RootCats - "+rootCatsResult+" | SubCats - "+subCatsResult);
		if ((null != dvco)) {
			pRequest.setParameter(DIMVAL_ITEM, dvco);
			pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		}
	}

	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		this.mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	public int getMaximumCount() {
		return mMaximumCount;
	}

	public void setMaximumCount(int pMaximumCount) {
		this.mMaximumCount = pMaximumCount;
	}

}