package com.cps.droplet;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;

import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.RepositoryUtils;

import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * @author Vignesh 
 * this droplet used to fectch all user who are not completed
 * there confirmation Process
 *
 */
public class CPSConfirmationPendingUsersDroplet extends DynamoServlet {
	private static final String INACTIVE_USERS = "inactiveUsers";
	private static final String EMAIL = "email";
	private static final String USERS_LIST = "usersList";
	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final ParameterName OPARAM_EMPTY = ParameterName.getParameterName("empty");
	private CPSProfileTools profileTools;
	private RepositoryUtils repositoryUtils;
	private String confirmationPendingQuery;
	private String searchProfileQuery;
	private String longInactiveUserQuery;
	private int inactiveUserDateTimeFrom;
	

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("Confirmation Pending Users droplet");
		String email = (String) pRequest.getParameter(EMAIL);
		String inactiveUsers = (String) pRequest.getParameter(INACTIVE_USERS);
		Repository userProfile = getProfileTools().getProfileRepository();
		RepositoryItem[] userItems = null;
		try {
			if(!StringUtils.isBlank(email)){
				Object[] login = new Object[1];
				login[0]= email;
				userItems=getRepositoryUtils().executeQuery(CPSConstants.USER_ITEM_DESCRIPTOR,
						userProfile, getSearchProfileQuery(),login);
			vlogDebug("email is not null or empty");
			}else if (!StringUtils.isBlank(inactiveUsers)) {
				Object[] inactiveUser = getRqlQueryInactiveUserParams();
				userItems=getRepositoryUtils().executeQuery(CPSConstants.USER_ITEM_DESCRIPTOR,
						userProfile, getLongInactiveUserQuery(),inactiveUser);
			}
			else{
			userItems = getRepositoryUtils().executeQuery(CPSConstants.USER_ITEM_DESCRIPTOR,
					userProfile, getConfirmationPendingQuery());
			vlogDebug("email is null or empty");
			}
		} catch (RepositoryException e) {
			vlogError("Error in processing Profile {0}", e);
		}
		if (userItems != null) {
			pRequest.setParameter(USERS_LIST, userItems);
			pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);

		} else {
			pRequest.serviceLocalParameter(OPARAM_EMPTY, pRequest, pResponse);
		}
	}
	/**
	 * @return date param 
	 */
	private Object[] getRqlQueryInactiveUserParams() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, getInactiveUserDateTimeFrom());  
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		Date date = calendar.getTime();
		Object params[] = new Object[1];
		params[0] = date;
		return params;
	}

	/**
	 * @return the profileTools
	 */
	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	/**
	 * @param profileTools
	 *            the profileTools to set
	 */
	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	/**
	 * @return the repositoryUtils
	 */
	public RepositoryUtils getRepositoryUtils() {
		return repositoryUtils;
	}

	/**
	 * @param repositoryUtils
	 *            the repositoryUtils to set
	 */
	public void setRepositoryUtils(RepositoryUtils repositoryUtils) {
		this.repositoryUtils = repositoryUtils;
	}

	/**
	 * @return the searchProfileQuery
	 */
	public String getSearchProfileQuery() {
		return searchProfileQuery;
	}

	/**
	 * @param searchProfileQuery the serchProfileQuery to set
	 */
	public void setSearchProfileQuery(String searchProfileQuery) {
		this.searchProfileQuery = searchProfileQuery;
	}

	/**
	 * @return the confirmationPendingQuery
	 */
	public String getConfirmationPendingQuery() {
		return confirmationPendingQuery;
	}

	/**
	 * @param confirmationPendingQuery the confirmationPendingQuery to set
	 */
	public void setConfirmationPendingQuery(String confirmationPendingQuery) {
		this.confirmationPendingQuery = confirmationPendingQuery;
	}

	/**
	 * @return the longInactiveUserQuery
	 */
	public String getLongInactiveUserQuery() {
		return longInactiveUserQuery;
	}

	/**
	 * @param longInactiveUserQuery the longInactiveUserQuery to set
	 */
	public void setLongInactiveUserQuery(String longInactiveUserQuery) {
		this.longInactiveUserQuery = longInactiveUserQuery;
	}

	/**
	 * @return the inactiveUserDateTimeFrom
	 */
	public int getInactiveUserDateTimeFrom() {
		return inactiveUserDateTimeFrom;
	}

	/**
	 * @param inactiveUserDateTimeFrom the inactiveUserDateTimeFrom to set
	 */
	public void setInactiveUserDateTimeFrom(int inactiveUserDateTimeFrom) {
		this.inactiveUserDateTimeFrom = inactiveUserDateTimeFrom;
	}

}