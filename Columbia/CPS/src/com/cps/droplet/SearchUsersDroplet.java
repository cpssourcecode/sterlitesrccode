package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;
import atg.userprofiling.Profile;

import com.cps.userprofiling.SortableUserInfo;
import com.cps.util.CPSConstants;

public class SearchUsersDroplet extends DynamoServlet {

	/**
	 * Profile repository include
	 */
	private Repository profileRepository;
	/**
	 * User directory include
	 */
	private UserDirectory userDirectory;
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("SearchUsersDroplet.service start");
		// List of users to return
		List<SortableUserInfo> users = new ArrayList<SortableUserInfo>();
		double total = 0;
		double numPages = 1;
		
		// Get passed in values
		String searchValue = pRequest.getParameter(DropletConstants.SEARCH_TERM);
		String role = pRequest.getParameter(DropletConstants.ROLE);
		String companyName = pRequest.getParameter(DropletConstants.COMPANY_NAME);
		String sort = pRequest.getParameter(DropletConstants.SORT);
		String order = pRequest.getParameter(DropletConstants.ORDER);
		int page = 1;
		try {
			page = Integer.parseInt(pRequest.getParameter(DropletConstants.PAGE));
		} catch (NumberFormatException e){
			vlogError(e, "Error");
			page = 1;
		}

		vlogDebug("Entered values: SearchValue - "+searchValue+" | Role - "+role+" | CompanyName - "+
				companyName+" | Sort - "+sort+" | Order - "+order+" | Page - "+page);

		RepositoryItem parentOrg = null;
		Set<RepositoryItem> notifyUsers = null;
		Profile profile = getProfile();
		if (profile != null) {
			// Get the parent org to pull users off of
			parentOrg = (RepositoryItem)getProfile().getPropertyValue(DropletConstants.PARENT_ORG);
			notifyUsers = (Set<RepositoryItem>) getProfile().getPropertyValue(DropletConstants.NOTIFICATION_ENABLED_USERS);
			vlogDebug("ParentOrg: "+parentOrg);
		}

		if (parentOrg != null){
			Set<RepositoryItem> members = (Set<RepositoryItem>) parentOrg.getPropertyValue(DropletConstants.MEMBERS);
			vlogDebug("Members: "+members);

			// Need to add users to a list for sorting purposes 
			for (RepositoryItem member : members) {
				if (shouldDisplayMember(member)){
					users.add(createSortableUserInfo(member));
				}
			}
			
			if (users != null && !users.isEmpty()) {
				if (!StringUtils.isBlank(searchValue)) {
					// This search filter is only for name and email
					users = filterListBySearch(users, searchValue);
					if (users.isEmpty()) {
						pRequest.setParameter(DropletConstants.EMPTY, true);
					}
				}
				if (!StringUtils.isBlank(role)) {
					users = filterListByRole(users, role);
					if (users.isEmpty()) {
						pRequest.setParameter(DropletConstants.EMPTY, true);
					}
				}
				if (!StringUtils.isBlank(companyName)) {
					users = filterListByCompany(users, companyName);
					if (users.isEmpty()) {
						pRequest.setParameter(DropletConstants.EMPTY, true);
					}
				}
				vlogDebug("Users after filters: "+users);

				// Sort
				sortUsers(users, sort, order);
				
				// Set items on correct page to list for display
				total = users.size();
				numPages = Math.ceil((total / DropletConstants.NUM_PER_PAGE_AMOUNT));
				
				users = getSubsetList(users, page);
				vlogDebug("Users after subset: "+users);
			}
		}
		Set<String> notifyUsersId=new HashSet<>();
		if(notifyUsers!=null){			
			for (RepositoryItem user : notifyUsers) {
				notifyUsersId.add(user.getRepositoryId());
			}
		}

		
		// Set return values
		pRequest.setParameter("notifyUsers", notifyUsers);
		pRequest.setParameter("notifyUsersId", notifyUsersId);
		pRequest.setParameter(DropletConstants.LIST, users);
		pRequest.setParameter(DropletConstants.TOTAL, (int)total);
		pRequest.setParameter(DropletConstants.NUM_PER_PAGE, (int)DropletConstants.NUM_PER_PAGE_AMOUNT);
		pRequest.setParameter(DropletConstants.NUM_PAGES, (int)numPages);
		pRequest.serviceLocalParameter(DropletConstants.OUTPUT, pRequest, pResponse);

		vlogDebug("SearchUsersDroplet.service end");
	}
	
	protected List<SortableUserInfo> filterListBySearch(List<SortableUserInfo> users, String searchTerm) {
		List<SortableUserInfo> filteredList = new ArrayList<SortableUserInfo>();
		
		// Loop through each list and add to new list if search term found
		for (SortableUserInfo item : users){
			List<String> values = new ArrayList<String>();
			values.add((String)item.getFirstName());
			values.add((String)item.getLastName());
			values.add((String)item.getEmail());
			if (checkItem(searchTerm, values)){
				filteredList.add(item);
			}
		}
		
		return filteredList;
	}
	
	protected List<SortableUserInfo> filterListByRole (List<SortableUserInfo> users, String role) {
		List<SortableUserInfo> filteredList = new ArrayList<SortableUserInfo>();
		
		for (SortableUserInfo user : users){
			try {
				RepositoryItem userItem = getProfileRepository().getItem(user.getId(), CPSConstants.USER_ITEM_DESCRIPTOR);
				List<String> values = new ArrayList<String>();
				values.add((String)user.getRole());
				if (checkUserRole(userItem, role)){
					filteredList.add(user);
				}
			} catch (Exception e){
				vlogError(e, "Error");
			}
		}
		
		return filteredList;
	}
	
	protected List<SortableUserInfo> filterListByCompany (List<SortableUserInfo> users, String companyName) {
		List<SortableUserInfo> filteredList = new ArrayList<SortableUserInfo>();
		
		for (SortableUserInfo user : users){
			List<String> values = new ArrayList<String>();
			values.add((String)user.getCompany());
			if (checkItem(companyName, values)){
				filteredList.add(user);
			}
		}
		
		return filteredList;
	}
	
	protected boolean checkItem(String searchTerm, List<String> values) {
		boolean match = false;
		
		int count = 0;
		
		while (!match && count < values.size()) {
			if (!StringUtils.isBlank(values.get(count))){
				match = values.get(count).toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
			}
			count++;
		}
		
		return match;
	}
	
	private SortableUserInfo createSortableUserInfo (RepositoryItem userItem) {
		SortableUserInfo user = new SortableUserInfo();
		
		user.setId(userItem.getRepositoryId());
		user.setFirstName((String)userItem.getPropertyValue(CPSConstants.FIRST_NAME));
		user.setLastName((String)userItem.getPropertyValue(CPSConstants.LAST_NAME));
		user.setEmail((String)userItem.getPropertyValue(CPSConstants.EMAIL));
		user.setCompany(getUserCompany(userItem));
		user.setRole(getUserRole(userItem, true));
		boolean showLimits=checkUserRole(userItem,CPSConstants.ROLE_BUYER);
		if(showLimits){
			if (userItem.getPropertyValue(CPSConstants.ORDER_LIMIT) != null){
				user.setSpendingLimit(((Double)userItem.getPropertyValue(CPSConstants.ORDER_LIMIT)).toString());
				if (userItem.getPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY) != null){
					user.setFrequency((String)userItem.getPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY));
				}
			} else {
				// User order limit null, use default from org
				RepositoryItem parentOrg = (RepositoryItem)userItem.getPropertyValue(CPSConstants.PARENT_ORG);
				if (parentOrg.getPropertyValue(CPSConstants.ORDER_LIMIT) != null){
					user.setSpendingLimit(((Double)parentOrg.getPropertyValue(CPSConstants.ORDER_LIMIT)).toString());
					if (parentOrg.getPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY) != null){
						user.setFrequency((String)parentOrg.getPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY));
					}
				}
			}
		}
		else{
			user.setSpendingLimit("NA");
			user.setFrequency("NA");
		}
		user.setStatus(getUserStatus(userItem));
		RepositoryItem parentOrganization = (RepositoryItem)userItem.getPropertyValue(CPSConstants.PARENT_ORG);
		RepositoryItem superParentOrganization = (RepositoryItem) parentOrganization.getPropertyValue(CPSConstants.PARENT_ORG);
        
		Set billAccount = (Set) userItem.getPropertyValue("billingAccounts");
		if(superParentOrganization == null && billAccount.size() < 1){
			user.setBillingAccount("false");
		}else{
			user.setBillingAccount("true");
		}
		return user;
	}
	
	private String getUserCompany (RepositoryItem userItem) {
		String companyName = "";
		RepositoryItem parentOrg = (RepositoryItem)userItem.getPropertyValue(DropletConstants.PARENT_ORG);
		if (parentOrg != null) {
			companyName = (String)parentOrg.getPropertyValue(CPSConstants.NAME);
		}
		
		return companyName;
	}
	
	private String getUserRole (RepositoryItem userItem, boolean returnDisplay) {
		String userRole = "";
		User user = getUserDirectory().findUserByPrimaryKey(userItem.getRepositoryId());
		if (user != null){
			Collection collection = user.getAssignedRoles();
			if (collection != null && !collection.isEmpty()){
				Iterator iterator = collection.iterator();
				Role assignedRole = (Role)iterator.next();
				
				if (returnDisplay){
					if (collection.size() > 1){
						userRole = "MULTI";
					} else {
						userRole = getRoleDisplayValue(assignedRole.getName());
					}
				} else {
					userRole = assignedRole.getName();
				}
			}
		}
		
		return userRole;
	}
	
	private boolean checkUserRole (RepositoryItem userItem, String roleName){
		boolean userHasRole = false;
		
		User user = getUserDirectory().findUserByPrimaryKey(userItem.getRepositoryId());
		if (user != null){
			Collection collection = user.getAssignedRoles();
			if (collection != null && !collection.isEmpty()){
				for (Object roleObj : collection){
					Role role = (Role)roleObj;
					if (roleName.equalsIgnoreCase(role.getName())){
						userHasRole = true;
						break;
					}
				}
			}
		}
		
		return userHasRole;
	}
	
	private String getUserStatus (RepositoryItem userItem) {
		String userStatus = "";
		
		//Boolean onHold = (Boolean)userItem.getPropertyValue(CPSConstants.ON_HOLD);
		Boolean isActive = (Boolean)userItem.getPropertyValue(CPSConstants.IS_ACTIVE);
		if (isActive == null){
			isActive = false;
		}
		
		if (isActive){
			userStatus = "Active";
		} else {
			userStatus = "Deactivated";
		}
		
		return userStatus;
	}
	
	private String getRoleDisplayValue(String role){
		String roleDisplay = "";
		switch (role){
		case CPSConstants.ROLE_ACCOUNT_ADMIN:
			roleDisplay = CPSConstants.ROLE_ACCOUNT_ADMIN_DISPLAY;
			break;
		case CPSConstants.ROLE_APPROVER:
			roleDisplay = CPSConstants.ROLE_APPROVER_DISPLAY;
			break;
		case CPSConstants.ROLE_FINANCE:
			roleDisplay = CPSConstants.ROLE_FINANCE_FULL_DISPLAY;
			break;
		case CPSConstants.ROLE_FINANCE_LIMITED:
			roleDisplay = CPSConstants.ROLE_FINANCE_LIMITED_DISPLAY;
			break;
		case CPSConstants.ROLE_BUYER:
			roleDisplay = CPSConstants.ROLE_BUYER_DISPLAY;
			break;
		default: 
			roleDisplay = CPSConstants.ROLE_APPRAISER_DISPLAY;
		}
		
		return roleDisplay;
	}
	
	private void sortUsers (List<SortableUserInfo> users, final String sort, final String order){
		Collections.sort(users, new Comparator<SortableUserInfo>() {
			public int compare(SortableUserInfo item1, SortableUserInfo item2){
				int returnValue = 0;
				
				switch (sort){
				case DropletConstants.SORT_LAST_NAME:
					if ((item1.getLastName() != null) && (item2.getLastName() != null)){
						returnValue = (item1.getLastName().toString().toLowerCase())
								.compareTo(item2.getLastName().toString().toLowerCase());
					}
					break;
				case DropletConstants.SORT_ROLE:
					if ((item1.getRole() != null) && (item2.getRole() != null)){
						returnValue = (item1.getRole().toString().toLowerCase())
								.compareTo(item2.getRole().toString().toLowerCase());
					}
					break;
				case DropletConstants.SORT_EMAIL:
					if ((item1.getEmail() != null) && (item2.getEmail() != null)){
						returnValue = (item1.getEmail().toString().toLowerCase())
								.compareTo(item2.getEmail().toString().toLowerCase());
					}
					break;
				case DropletConstants.SORT_COMPANY:
					if ((item1.getCompany() != null) && (item2.getCompany() != null)){
						returnValue = (item1.getCompany().toString().toLowerCase())
								.compareTo(item2.getCompany().toString().toLowerCase());
					}
					break;
				case DropletConstants.SORT_SPENDING:
					if ((item1.getSpendingLimit() != null) && (item2.getSpendingLimit() != null)){
						returnValue = (item1.getSpendingLimit().toString().toLowerCase())
								.compareTo(item2.getSpendingLimit().toString().toLowerCase());
					}
					break;
				case DropletConstants.SORT_FREQUENCY:
					if ((item1.getFrequency() != null) && (item2.getFrequency() != null)){
						returnValue = (item1.getFrequency().toString().toLowerCase())
								.compareTo(item2.getFrequency().toString().toLowerCase());
					}
					break;
				case DropletConstants.SORT_STATUS:
					if ((item1.getStatus() != null) && (item2.getStatus() != null)){
						returnValue = (item1.getStatus().toString().toLowerCase())
								.compareTo(item2.getStatus().toString().toLowerCase());
					}
					break;
				default:
					if ((item1.getFirstName() != null) && (item2.getFirstName() != null)){
						returnValue = (item1.getFirstName().toString().toLowerCase())
								.compareTo(item2.getFirstName().toString().toLowerCase());
					}
					break;
				}
				if (DropletConstants.ORDER_DESCENDING.equalsIgnoreCase(order)){
					returnValue *= -1;
				}
				return returnValue;
			}
		});
	}
	
	private List<SortableUserInfo> getSubsetList(List<SortableUserInfo> list, int page){
		List<SortableUserInfo> subsetList = new ArrayList<SortableUserInfo>();
		int max = page*(int)DropletConstants.NUM_PER_PAGE_AMOUNT;
		int min = max-(int)DropletConstants.NUM_PER_PAGE_AMOUNT;
		
		// No one likes IndexOutOfBounds Exceptions
		if (max > list.size()){
			max = list.size();
		}
		
		// Only include users that should display on the current page
		for (int i=min; i<max; i++){
			subsetList.add(list.get(i));
		}
		
		return subsetList;
	}
	
	private boolean shouldDisplayMember(RepositoryItem member){
		boolean shouldDisplay = true;
		vlogDebug("Should show user - "+member.getPropertyValue(CPSConstants.EMAIL));
		// Don't csr users that are accessing the account

		vlogDebug("Check user role - "+getUserRole(member, false));
		if (checkUserRole(member, CPSConstants.ROLE_CSR_ADMIN) || checkUserRole(member, "regularAdmin")) {
			shouldDisplay = false;
		}

		vlogDebug("Should display user? "+shouldDisplay);

		return shouldDisplay;
	}

	public Profile getProfile() {
		return (Profile) ServletUtil.getCurrentUserProfile();
	}

	public UserDirectory getUserDirectory() {
		return userDirectory;
	}

	public void setUserDirectory(UserDirectory userDirectory) {
		this.userDirectory = userDirectory;
	}

	public Repository getProfileRepository() {
		return profileRepository;
	}

	public void setProfileRepository(Repository profileRepository) {
		this.profileRepository = profileRepository;
	}
}