package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * QuickOrderErrorDroplet
 *
 * @author David Mednikov
 */
public class QuickOrderErrorDroplet extends DynamoServlet {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/droplet/QuickOrderErrorDroplet";
    public static ParameterName INVALID_SKU_LIST = ParameterName.getParameterName("invalidSkuList");
    public static ParameterName DUPLICATE_PRODUCTS = ParameterName.getParameterName("duplicateProducts");
    public static ParameterName HAS_DUPLICATES = ParameterName.getParameterName("hasDuplicates"); // null check for duplicateProducts passed from
                                                                                                  // cartModifierFormHandler

    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        ArrayList<String> invalidSkuList = (ArrayList<String>) req.getObjectParameter(INVALID_SKU_LIST);
        if (invalidSkuList != null && invalidSkuList.size() > 0) {
            req.serviceLocalParameter("outputStartInvalidSearchInput", req, res);
            for (int ind = 0; ind < invalidSkuList.size(); ind++) {
                req.setParameter("invalidInput", invalidSkuList.get(ind));
                req.serviceLocalParameter("outputInvalidSearchInput", req, res);
            }
            req.serviceLocalParameter("outputEndInvalidSearchInput", req, res);
        }

        if ((Boolean) req.getObjectParameter(HAS_DUPLICATES)) {
            req.serviceLocalParameter("outputStartDuplicatesDisplay", req, res);
            HashMap<String, HashMap<Integer, ArrayList<String>>> duplicateProducts = (HashMap<String, HashMap<Integer, ArrayList<String>>>) req
                            .getObjectParameter(DUPLICATE_PRODUCTS);
            int rowNumber = 0;
            for (String searchInput : duplicateProducts.keySet()) {
                vlogDebug("SEE ME IN THE LOGS EASIERIER: SEARCHINPUT" + searchInput);
                for (Integer qty : duplicateProducts.get(searchInput).keySet()) {
                    ArrayList<String> duplicates = duplicateProducts.get(searchInput).get(qty);
                    if (duplicates != null && duplicates.size() > 0) {
                        vlogDebug("SEE ME IN THE LOGS EASIERIER: QUANTITY=" + qty);
                        req.serviceLocalParameter("outputStartDuplicateRow", req, res);
                        for (int ind = 0; ind < duplicates.size(); ind++) {
                            vlogDebug("SEE ME IN LOGS:" + "qty:" + qty + ",dupId:" + duplicates.get(ind));
                            req.setParameter("rowQuantity", qty + ""); // quantity to be add to cart for whichever selection is made in this row
                            req.setParameter("duplicateId", duplicates.get(ind));
                            req.setParameter("rowNumber", rowNumber);
                            req.serviceLocalParameter("outputDuplicateCol", req, res);
                        }
                        rowNumber++;
                        req.serviceLocalParameter("outputEndDuplicateRow", req, res);
                    }
                }
            }
            req.serviceLocalParameter("outputEndDuplicatesDisplay", req, res);
            req.serviceLocalParameter("outputDuplicatesFooter", req, res); // this is only a separate call for clarity NOT necessary to be separate from above
                                                                           // though
        } else {
            // no duplicates to display, show okay button footer
            req.serviceLocalParameter("outputInvalidOnlyFooter", req, res);
        }
    }

    public static String getComponentName() {
        return COMPONENT_NAME;
    }

}