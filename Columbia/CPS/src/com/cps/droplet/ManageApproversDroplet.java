package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import atg.servlet.ServletUtil;
import com.cps.commerce.approval.ApprovalManager;

import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

public class ManageApproversDroplet extends DynamoServlet {

	public Profile getProfile() {
		return (Profile) ServletUtil.getCurrentUserProfile();
	}

	private ApprovalManager mApprovalManager;

	public ApprovalManager getApprovalManager() {
		return mApprovalManager;
	}

	public void setApprovalManager(ApprovalManager pApprovalManager) {
		mApprovalManager = pApprovalManager;
	}
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		List<RepositoryItem> approvers = new ArrayList<RepositoryItem>();

		Profile profile = getProfile();
		if (profile != null) {
			// Get all approvers from org to display
			RepositoryItem parentOrg = (RepositoryItem)profile.getPropertyValue(DropletConstants.PARENT_ORG);

			if (parentOrg != null){
				Set<RepositoryItem> members = (Set<RepositoryItem>) parentOrg.getPropertyValue(DropletConstants.MEMBERS);
				for (RepositoryItem member : members) {
					if (getApprovalManager().isApprover(member)){
						approvers.add(member);
					}
				}
			}
		}

		pRequest.setParameter(DropletConstants.APPROVERS, approvers);
		pRequest.serviceLocalParameter(DropletConstants.OUTPUT, pRequest, pResponse);
	}
	
}