package com.cps.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.cps.service.SixLengthIdService;


/**
 * @author shebi 
 * this droplet fetch the message from MTR list DisplayName and Description.
 */
public class MTRListDisplayNameDroplet  extends DynamoServlet{

    private static final String ITEM_NUMBER = "itemNumber";	   
    private Repository catalog;    
    private String itemDescriptor;
    private String itemNumber;
    private String findByIdQuery;
    private SixLengthIdService sixLengthIdService;

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
    private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        String itemNumber = pRequest.getParameter(ITEM_NUMBER);       
        RepositoryItem [] result=null;
        String description=null;
        String displayName=null;
        try {
        			Object[] params = new Object[1];
        			params[0] = itemNumber;        	
        			Map<String, String> itemDetails = null ;
        			RepositoryView view = getCatalog().getView(getItemDescriptor());
        			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(getItemNumber());
        			result = rqlStatement.executeQuery(view, params);
        			if(result != null){
        				description=(String) result[0].getPropertyValue("invoice_description");
        				displayName=(String) result[0].getPropertyValue("displayName");
        				 pRequest.setParameter("displayName", displayName);
        			}
        			else{
        				itemDetails = new HashMap<>();
        				itemDetails = getItemDetails(itemNumber,result,itemDetails);
        				if(!itemDetails.isEmpty()){
        					description = itemDetails.get("description");
        					displayName=itemDetails.get("displayName");
        				}
        			}
        	}
         catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (StringUtils.isBlank(description) || StringUtils.isBlank(displayName)) {
            pRequest.setParameter(ITEM_NUMBER,"Not valid item number");
            pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
        } else {
            pRequest.setParameter("description", description);
            pRequest.setParameter("displayName", displayName);
            pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
        }

    }
    
    private Map<String, String> getItemDetails(String itemNumber, RepositoryItem[] result, Map<String, String> itemDetails)throws ServletException,IOException, RepositoryException {
		
         itemNumber = getSixLengthIdService().removeFirstZeros(itemNumber);
         Object[] params = new Object[1];
			params[0] = itemNumber;        			
			RepositoryView view = getCatalog().getView(getItemDescriptor());
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(getFindByIdQuery());
			result = rqlStatement.executeQuery(view, params);
			if(result != null){
				String description=(String) result[0].getPropertyValue("invoice_description");
				String displayName=(String) result[0].getPropertyValue("displayName");
				itemDetails.put("description", description);
				itemDetails.put("displayName", displayName);
			}
         
    	return itemDetails;	
	}

	/**
	 * @return the catalog
	 */
	public Repository getCatalog() {
		return catalog;
	}

	/**
	 * @param catalog the catalog to set
	 */
	public void setCatalog(Repository catalog) {
		this.catalog = catalog;
	}

	/**
	 * @return the itemDescriptor
	 */
	public String getItemDescriptor() {
		return itemDescriptor;
	}

	/**
	 * @param itemDescriptor the itemDescriptor to set
	 */
	public void setItemDescriptor(String itemDescriptor) {
		this.itemDescriptor = itemDescriptor;
	}
	 /**
		 * @return the itemNumber
		 */
		public String getItemNumber() {
			return itemNumber;
		}

		/**
		 * @param itemNumber the itemNumber to set
		 */
		public void setItemNumber(String itemNumber) {
			this.itemNumber = itemNumber;
		}

		/**
		 * @return the findByIdQuery
		 */
		public String getFindByIdQuery() {
			return findByIdQuery;
		}

		/**
		 * @param findByIdQuery the findByIdQuery to set
		 */
		public void setFindByIdQuery(String findByIdQuery) {
			this.findByIdQuery = findByIdQuery;
		}

		/**
		 * @return the sixLengthIdService
		 */
		public SixLengthIdService getSixLengthIdService() {
			return sixLengthIdService;
		}

		/**
		 * @param sixLengthIdService the sixLengthIdService to set
		 */
		public void setSixLengthIdService(SixLengthIdService sixLengthIdService) {
			this.sixLengthIdService = sixLengthIdService;
		}


}
