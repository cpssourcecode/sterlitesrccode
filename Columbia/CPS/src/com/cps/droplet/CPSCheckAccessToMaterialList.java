package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.cps.commerce.gifts.CPSGiftlistManager;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

/**
 * 
 * @author VSG
 *
 */
public class CPSCheckAccessToMaterialList extends DynamoServlet {

	public static final String INPUT_GIFTLIST_ID = "giftlistId";
	public static final String INPUT_PROFILE = "profile";
	public static final String OUTPUT_HAVE_ACCESS = "isHaveAccess";
	public static final String OUTPUT_PARAM = "output";
	private CPSGiftlistManager mGiftlistManager;
	
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		String giftlistId = getGiftlistId(pRequest);
		Profile profile = getProfile(pRequest);
		boolean haveAccess = false;
		if(StringUtils.isNotBlank(giftlistId) && profile != null) {
			haveAccess = getGiftlistManager().isHaveAccessToGiftlist(giftlistId, profile);
		}
		
		pRequest.setParameter(OUTPUT_HAVE_ACCESS, haveAccess);
		pRequest.serviceLocalParameter(OUTPUT_PARAM, pRequest, pResponse);
	}

	private Profile getProfile(DynamoHttpServletRequest pRequest) {
		Profile profile = null;
		Object obj = pRequest.getLocalParameter(INPUT_PROFILE);
		if(obj instanceof Profile) {
			profile = (Profile) obj;
		}
		return profile;
	}
	
	private String getGiftlistId(DynamoHttpServletRequest pRequest) {
		String giftlistId = null;
		Object obj = pRequest.getLocalParameter(INPUT_GIFTLIST_ID);
		if(obj instanceof String) {
			giftlistId = (String) obj;
		}
		return giftlistId;
	}
	
	public CPSGiftlistManager getGiftlistManager() {
		return mGiftlistManager;
	}

	public void setGiftlistManager(CPSGiftlistManager pGiftlistManager) {
		mGiftlistManager = pGiftlistManager;
	}
	
}

