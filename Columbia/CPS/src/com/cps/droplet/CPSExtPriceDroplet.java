package com.cps.droplet;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Takes in user id and role and checks if user has role assigned
 * Returns true if user has role, false otherwise
 * @author root
 *
 */
public class CPSExtPriceDroplet extends DynamoServlet {
	
	private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

	public static ParameterName SKU = ParameterName.getParameterName("sku");
	public static ParameterName PRICE_LIST = ParameterName.getParameterName("priceList");
	public static ParameterName OUTPUT = ParameterName.getParameterName("output");

	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		ParameterName renderParam = EMPTY_OPARAM;
		String sku = pRequest.getParameter(SKU);

		CPSSessionPriceList sessionPriceList = (CPSSessionPriceList)pRequest.getObjectParameter(PRICE_LIST);

		Double correctPrice = 0.0000;
		if (isLoggingDebug()) {
			logDebug("Inside ExtPrice Droplet" + sessionPriceList);
		}

		if (sessionPriceList != null && !(sessionPriceList.getValues().isEmpty())) {
		
			if (isLoggingDebug()) {
				logDebug("Looping each item" + sessionPriceList.getValues().toString());
			}
			Map<String,Double> priceList = sessionPriceList.getValues();
			
			for (Entry<String,Double> price : priceList.entrySet()){
				if(sku.equalsIgnoreCase(price.getKey())){
					correctPrice = price.getValue();
					if (isLoggingDebug()) {
						logDebug(" Price " + correctPrice );
					}
					pRequest.setParameter("element", correctPrice);
					renderParam = OUTPUT;
				}
			}
			if (correctPrice == 0.0000) {
				if (isLoggingDebug()) {
					logDebug("setting Price no price found");
				}
				pRequest.setParameter("element", correctPrice);
				renderParam = OUTPUT;
			}
			
			pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
		}
		
	}

}

	
