package com.cps.droplet;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.cps.userprofiling.CPSProfileTools;

import javax.servlet.ServletException;
import java.io.IOException;

public class GuestCheckoutAllowedDroplet extends DynamoServlet {

    private static final String OPARAM_TRUE = "true";
    private static final String OPARAM_FALSE = "false";

    private CPSProfileTools mProfileTools;

    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {

        CPSProfileTools profileTools = getProfileTools();

        boolean isAllowed = false;

        isAllowed = profileTools.isAllowGuestCheckout();

        if (isAllowed) {
            req.serviceLocalParameter(OPARAM_TRUE, req, res);
        } else {
            req.serviceLocalParameter(OPARAM_FALSE, req, res);
        }
    }

    public CPSProfileTools getProfileTools() {
        return mProfileTools;
    }

    public void setProfileTools(CPSProfileTools pProfileTools) {
        mProfileTools = pProfileTools;
    }
}
