package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import com.cps.util.CPSConstants;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.droplet.ForEach;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Droplet used to got associated products
 */
public class AssociatedProductsDroplet extends DynamoServlet implements CPSConstants {

	/**
	 * The Constant OUTPUT_OPARAM.
	 */
	private static final String OUTPUT_OPARAM = "output";

	/**
	 * The Constant EMPTY_OPARAM.
	 */
	private static final String EMPTY_OPARAM = "empty";

	private static final String PRODUCTS_NUMBER = "productsNumber";

	private static final String PRODUCT = "product";

	private static final String PRODUCT_ID = "productId";

	private Integer mDefaultProductsNumber = 3;

	private String mAssociatedProductsProductPropertyName = "fixedRelatedProducts";
	private String mAssociatedProductsCategoryPropertyName = "associatedProducts";

	private Repository mProductCatalog;

	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		RepositoryItem product = (RepositoryItem) pRequest.getObjectParameter(PRODUCT);
		if (product == null) {
			String productId = (String) pRequest.getObjectParameter(PRODUCT_ID);
			if (null != productId) {
				try {
					product = getProductCatalog().getItem(productId, PRODUCT_ITEM_DESCRIPTOR);
				} catch (RepositoryException re) {
					logError(re);
				}
			}
		}

		if (product != null) {
			String productsNumberStr = pRequest.getParameter(PRODUCTS_NUMBER);

			Integer productsNumber = getDefaultProductsNumber();
			if (StringUtils.isNotEmpty(productsNumberStr)) {
				try {
					productsNumber = Integer.parseInt(productsNumberStr);
				} catch (NumberFormatException e) {
					vlogError("AssociatedProductsDroplet: incorrect number format. Use default products number");
				}
			}

			Set<RepositoryItem> resultSet = new LinkedHashSet<RepositoryItem>(productsNumber);

			List<RepositoryItem> relatedProducts = (List<RepositoryItem>) product.getPropertyValue(getAssociatedProductsProductPropertyName());

			if (relatedProducts != null && !relatedProducts.isEmpty()) {
				Integer count = relatedProducts.size() > productsNumber ? productsNumber : relatedProducts.size();
				for (int i = 0; i < count; i++) {
					RepositoryItem currentAssociated = relatedProducts.get(i);
					Boolean useAsAssociated = (Boolean) currentAssociated.getPropertyValue(USE_AS_ASSOCIATED_PRODUCT);
					if (useAsAssociated != null && useAsAssociated && !product.getRepositoryId().equals(currentAssociated.getRepositoryId())) {
						resultSet.add(currentAssociated);
					}

				}
			}

			if (resultSet.size() < productsNumber) {
				Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) product.getPropertyValue(PRODUCT_PARENT_CATEGORIES);
				if (parentCategories != null && !parentCategories.isEmpty()) {
					RepositoryItem parentCategory = parentCategories.iterator().next();
					addProductsFromParentCategories(parentCategory, resultSet, productsNumber, product.getRepositoryId());
				}
			}

			if (resultSet.isEmpty()) {
				pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
			} else {
				pRequest.setParameter(ASSOCIATED_PRODUCTS, resultSet);
				pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
			}

		} else {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		}

	}

	private void addProductsFromParentCategories(RepositoryItem pParentCategory, Set<RepositoryItem> pResultList, Integer pProductsNumber, String currentProductId) {
		if (pParentCategory != null && pResultList != null && pProductsNumber != null) {
			List<RepositoryItem> associatedProducts = (List<RepositoryItem>) pParentCategory.getPropertyValue(getAssociatedProductsCategoryPropertyName());
			if (associatedProducts != null) {
				if(!associatedProducts.isEmpty()) {
					for (RepositoryItem ap : associatedProducts) {
						Boolean useAsAssociated = (Boolean) ap.getPropertyValue(USE_AS_ASSOCIATED_PRODUCT);
						if (pResultList.size() < pProductsNumber && useAsAssociated != null && useAsAssociated && !currentProductId.equals(ap.getRepositoryId())) {
							pResultList.add(ap);
						} else {
							break;
						}
					}
				}
				if (associatedProducts.size() < pProductsNumber) {
					Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) pParentCategory.getPropertyValue(FIXED_PARENT_CATEGORIES);
					if (parentCategories != null && !parentCategories.isEmpty()) {
						RepositoryItem parentCategory = parentCategories.iterator().next();
						addProductsFromParentCategories(parentCategory, pResultList, pProductsNumber, currentProductId);
					}
				}
			}
		}
	}

	public Integer getDefaultProductsNumber() {
		return mDefaultProductsNumber;
	}

	public void setDefaultProductsNumber(Integer pDefaultProductsNumber) {
		this.mDefaultProductsNumber = pDefaultProductsNumber;
	}


	public String getAssociatedProductsProductPropertyName() {
		return mAssociatedProductsProductPropertyName;
	}

	public void setAssociatedProductsProductPropertyName(String pAssociatedProductsProductPropertyName) {
		mAssociatedProductsProductPropertyName = pAssociatedProductsProductPropertyName;
	}

	public String getAssociatedProductsCategoryPropertyName() {
		return mAssociatedProductsCategoryPropertyName;
	}

	public void setAssociatedProductsCategoryPropertyName(String pAssociatedProductsCategoryPropertyName) {
		mAssociatedProductsCategoryPropertyName = pAssociatedProductsCategoryPropertyName;
	}

	public Repository getProductCatalog() {
		return mProductCatalog;
	}

	public void setProductCatalog(Repository pProductCatalog) {
		mProductCatalog = pProductCatalog;
	}
}