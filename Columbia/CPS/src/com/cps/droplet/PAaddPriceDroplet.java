package com.cps.droplet;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.cps.commerce.order.purchase.CPSPriceAvailability;
import com.cps.util.CPSConstants;
import com.cps.util.CPSSessionBean;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Takes in user id and role and checks if user has role assigned
 * Returns true if user has role, false otherwise
 * @author root
 *
 */
public class PAaddPriceDroplet extends DynamoServlet {

	static final ParameterName SESSION_BEAN = ParameterName.getParameterName("sessionBean");
	static final ParameterName PRICE_AVAILABILITY_PRICES = ParameterName.getParameterName("priceAvailabilityPrices");
	static final ParameterName PRICE_AVAILABILITY_PRICE_VALUES = ParameterName.getParameterName("priceAvailabilityPriceValues");
	static final ParameterName SELECTED_CS = ParameterName.getParameterName("selectedCS");

	public static ParameterName OUTPUT = ParameterName.getParameterName("output");

	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		CPSSessionBean sessionBean = (CPSSessionBean) pRequest.getObjectParameter(SESSION_BEAN);
		List <CPSPriceAvailability> itemList = (List) pRequest.getObjectParameter(PRICE_AVAILABILITY_PRICES);
		Map<String,Double> priceList = (Map<String, Double>) pRequest.getObjectParameter(PRICE_AVAILABILITY_PRICE_VALUES);
		RepositoryItem  selectedCS = (RepositoryItem) pRequest.getObjectParameter(SELECTED_CS);

		if (!itemList.isEmpty()) {
			vlogDebug("check post login user");
			for (int i=0;i<itemList.size();i++) {
				vlogDebug("looping post login user");
				sessionBean.getPalist().get(i).setCsid((String)selectedCS.getPropertyValue(CPSConstants.ID));
			}
		}

		vlogDebug("Inside PA Droplet");

		for (CPSPriceAvailability item : itemList) {
			vlogDebug("Looping each item");
			String sku = item.getSku();
			for(Entry<String,Double> price : priceList.entrySet()){
				if(sku.equalsIgnoreCase(price.getKey())){
					vlogDebug("setting Price");
					item.setUnitprice(price.getValue());
					double itemAmount = price.getValue()*item.getQty();
					itemAmount = (double) Math.round(itemAmount * 100);
					itemAmount = itemAmount / 100;
					item.setAmount(itemAmount);
					break;
				}
			}
		}
	}

}


