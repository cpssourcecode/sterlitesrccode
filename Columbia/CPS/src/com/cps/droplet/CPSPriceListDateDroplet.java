package com.cps.droplet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class CPSPriceListDateDroplet extends DynamoServlet {

    public static ParameterName DATED_PRICE_LISTS = ParameterName.getParameterName("datedPriceLists");

    private String dateFormat = "yyyy MMMM d";

    private static final String PARAM_SORTED_PRICE_LISTS = "sortedPriceLists";
    private static final String OPARAM_OUTPUT = "output";
    private static final String OPARAM_EMPTY = "empty";

    @Override
    public void service(DynamoHttpServletRequest request, DynamoHttpServletResponse response) throws ServletException, IOException {
        Map<String, String> items = (Map<String, String>) request.getObjectParameter(DATED_PRICE_LISTS);

        if (items != null && items.size() > 0) {
            vlogDebug("items not null, sorting and servicing output oparam");
            request.setParameter("sortedPriceLists", sortDatedPriceLists(items));
            request.serviceLocalParameter(OPARAM_OUTPUT, request, response);
        } else {
            vlogDebug("items null or empty, servicing empty oparam");
            request.serviceLocalParameter(OPARAM_EMPTY, request, response);
        }
    }

    private SortedMap<String, String> sortDatedPriceLists(Map<String, String> inputMap) {
        SortedMap<String, String> returnMap = new TreeMap<String, String>(new CPSPriceListDateComparator(getDateFormat()));

        for (String key : inputMap.keySet()) {
            returnMap.put(key, inputMap.get(key));
        }

        return returnMap;
    }

    private class CPSPriceListDateComparator implements Comparator<String> {
        private final SimpleDateFormat dateFormat;

        public CPSPriceListDateComparator(String dateFormatString) {
            this.dateFormat = new SimpleDateFormat(dateFormatString);
        }

        @Override
        public int compare(String o1, String o2) {
            Date d1 = parseDate(o1);
            Date d2 = parseDate(o2);

            if (d1 == null || d2 == null) {
                // Fall back to string comparison, one or both items are not parsable dates
                // We also want non-conforming dates to the bottom
                return o1.compareTo(o2);
            } else {
                if (d1.before(d2)) {
                    return 1; // More recent comes first
                } else if (d1.after(d2)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }

        private Date parseDate(String dateString) {
            Date parsedDate = null;

            if (dateString != null) {
                try {
                    parsedDate = dateFormat.parse(dateString);
                } catch (ParseException e) {
                    // We expect ParseException if the item isn't a date, so only log it if we're debugging
                    vlogDebug("Could not parse <" + dateString + "> into a date");
                    vlogError(e, "Error");
                } catch (Exception e) {
                    vlogError(e, "Error");
                }
            }

            return parsedDate;
        }
    }

    public String getDateFormat() {
        return this.dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

}