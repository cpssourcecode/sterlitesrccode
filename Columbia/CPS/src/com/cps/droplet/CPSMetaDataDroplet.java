package com.cps.droplet;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import javax.servlet.ServletException;
import java.io.IOException;

public class CPSMetaDataDroplet extends DynamoServlet {
    private static ParameterName CATALOG_URL = ParameterName.getParameterName("catalogURL");
    public static ParameterName OUTPUT = ParameterName.getParameterName("output");

    private static final String CATEGORY = "category";
    private static final String META_DESCRIPTION = "metaDescription";
    private static final String META_KEYWORDS = "metaKeywords";
    private static final String RQL_SELECT_BY_CATEGORY_ID = "id = ?0";
    private static final String SLASH = "/";

    private DimensionValueCacheTools mDimensionValueCacheTools;
    private Repository mRepository;

    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        String catalogURL = pRequest.getParameter(CATALOG_URL);

        if (catalogURL != null && !StringUtils.isBlank(catalogURL)) {
            String categoryId = parseRepositoryId(catalogURL);

            if (categoryId != null && !StringUtils.isBlank(categoryId)) {
                try {
                    RepositoryView repositoryView = getRepository().getView(CATEGORY);
                    RqlStatement statement = RqlStatement.parseRqlStatement(RQL_SELECT_BY_CATEGORY_ID);
                    Object params[] = {categoryId};
                    RepositoryItem[] items = statement.executeQuery(repositoryView, params);

                    if (items != null && items.length > 0) {
                        String metaDescription = (String) items[0].getPropertyValue(META_DESCRIPTION);
                        String metaKeywords = (String) items[0].getPropertyValue(META_KEYWORDS);

                        pRequest.setParameter(META_DESCRIPTION, metaDescription);
                        pRequest.setParameter(META_KEYWORDS, metaKeywords);
                    }
                } catch (RepositoryException e) {
                    vlogError(e, "Repository exception during querying Category by Category ID.");
                }
            }
        }

        pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
    }

    private String parseRepositoryId(String catalogURL) {
        String categoryId = null;

        String[] values = catalogURL.split(SLASH);
        for (int i = values.length - 1; i >= 0; i--) {
            if (StringUtils.isNotBlank(values[i])) {
                categoryId = values[i].toUpperCase();
                break;
            }
        }

        return categoryId;
    }

    public DimensionValueCacheTools getDimensionValueCacheTools() {
        return mDimensionValueCacheTools;
    }

    public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
        mDimensionValueCacheTools = pDimensionValueCacheTools;
    }

    public Repository getRepository() {
        return mRepository;
    }

    public void setRepository(Repository pRepository) {
        this.mRepository = pRepository;
    }
}
