package com.cps.droplet;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoServlet;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoHttpServletRequest;


/**
 * GetPriceFromMapDroplet
 *
 * @author Andy Porter
 */
public class GetPriceFromMapDroplet extends DynamoServlet {

	/**
	 * component name for debugging
	 */
	private final static String COMPONENT_NAME = "/cps/droplet/GetPriceFromMapDroplet";
	public final static ParameterName PRODUCT_ID = ParameterName.getParameterName("productId");
	public final static ParameterName ITEM_PRICES = ParameterName.getParameterName("itemPrices");
	public final static ParameterName PROFILE = ParameterName.getParameterName("profile");


	private static final String EMPTY = "emptyPrice";
	private static final String ITEM_PRICE = "itemPrice";
	private static final String OUTPUT_PRICE = "outputPrice";


	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {

		Map<String, Double> itemPrices = (Map<String, Double>) req.getObjectParameter(ITEM_PRICES);
		Object oProductId = req.getObjectParameter(PRODUCT_ID);

		String productId = null;
		if (null != oProductId) {
			productId = oProductId.toString();
		}

		Double itemPrice = null;
		if (null != itemPrices && null != productId) {
			itemPrice = itemPrices.get(productId);
		}


		if (null != itemPrice) {
			req.setParameter(ITEM_PRICE, itemPrice);
			req.serviceLocalParameter(OUTPUT_PRICE, req, res);
		} else {
			//hide price and add to cart
			req.serviceLocalParameter(EMPTY, req, res);
		}
	}


}