package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;


import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class RememberMeDroplet extends DynamoServlet{
	
	private static String OUTPUT = "output";
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		String name = "";
		String email = "";
		String cValue = pRequest.getCookieParameter(CPSConstants.USER_EMAIL_NAME_COOKIE_NAME);
		if (!StringUtils.isEmpty(cValue)){
			String[] arr = cValue.replace("\"", "").split(",");
			if (arr[0] != null && !((String)arr[0]).equals("")){
				email = (String)arr[0];
			}
			if (arr[1] != null && !((String)arr[1]).equals("")){
				name = (String)arr[1];
			}
		}
		if (!StringUtils.isBlank(email))
			pRequest.setParameter(CPSConstants.IS_REMEMBER, true);
		else
			pRequest.setParameter(CPSConstants.IS_REMEMBER, false);
		pRequest.setParameter(CPSConstants.EMAIL, email);
		pRequest.setParameter(CPSConstants.NAME, name);
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
	}
}