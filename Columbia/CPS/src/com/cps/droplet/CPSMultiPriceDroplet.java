package com.cps.droplet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.pricing.service.CPSMultiPriceService;
import com.cps.util.CPSSessionBean;
import com.endeca.infront.cartridge.model.Record;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

/**
 * CPSMultiPriceDroplet
 *
 * @author Andy Porter
 */
public class CPSMultiPriceDroplet extends DynamoServlet {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/droplet/CPSMultiPriceDroplet";
    public final static ParameterName RECORDS = ParameterName.getParameterName("records");
    public final static ParameterName PRODUCT_ID = ParameterName.getParameterName("productId");
    public final static ParameterName QTY = ParameterName.getParameterName("quantity");
    public final static ParameterName CUSTOMER_ID = ParameterName.getParameterName("customerId");
    public final static ParameterName PROFILE = ParameterName.getParameterName("profile");
    public final static ParameterName SHOPPING_CART = ParameterName.getParameterName("shoppingCart");
    private static final String PARAM_NAME_ORDER = "order";

    private static final String EMPTY = "emptyPrice";
    private static final String ITEM_PRICES = "itemPrices";
    private static final String OUTPUT_PRICE = "outputPrice";

    private CPSMultiPriceService mMultiPriceService;

    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        // check pricing client enabled call it otherwise return empty
        if (getMultiPriceService().isPricingWebserviceEnable()) {
        	CPSSessionBean sessionBean = (CPSSessionBean) ServletUtil.getCurrentRequest().resolveName(getMultiPriceService().getProductPricingService().getSessionBeanPath());
            Profile profile = (Profile) req.getObjectParameter(PROFILE);
            List<Record> records = (List<Record>) req.getObjectParameter(RECORDS);
            Order order = (Order) req.getObjectParameter(PARAM_NAME_ORDER);
            if (order != null) {
            	sessionBean.getProductPricesCache().clear();
                getMultiPriceService().updateFullOrderPrices(order, profile);
                req.serviceLocalParameter(EMPTY, req, res);
            } else if (records != null) {
            	sessionBean.getProductPricesCache().clear();
                OrderHolder orderHolder = (OrderHolder) req.getObjectParameter(SHOPPING_CART);
                Map<String, Double> itemPrices = getMultiPriceService().priceItems(records, profile, (CPSOrderImpl) orderHolder.getCurrent());

                req.setParameter(ITEM_PRICES, itemPrices);
                req.serviceLocalParameter(OUTPUT_PRICE, req, res);
            }
        } else {
            // hide price and add to cart
            req.serviceLocalParameter(EMPTY, req, res);
        }
    }

    public CPSMultiPriceService getMultiPriceService() {
        return mMultiPriceService;
    }

    public void setMultiPriceService(CPSMultiPriceService pMultiPriceService) {
        mMultiPriceService = pMultiPriceService;
    }

}