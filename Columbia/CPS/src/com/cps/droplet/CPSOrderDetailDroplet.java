package com.cps.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import atg.nucleus.naming.ParameterName;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.integrations.CPSOrderDetailBean;
import com.cps.integrations.CPSOrderStatusConnectionManager;
import com.cps.reorders.model.AutoOrder;
import com.cps.util.CPSSessionBean;

import static com.cps.util.CPSConstants.SESSION_BEAN_PATH;

public class CPSOrderDetailDroplet extends DynamoServlet{
	private static final String RQL_GET_ORDER_BY_ID = "id = ?0";
	private static final String ORDER = "order";
	private static final String AUTO_ORDER_ID = "autoOrderId";

	public static ParameterName ORDER_NUMBER = ParameterName.getParameterName("orderId"); //input orderId returned from admin order headers webservice

	public static ParameterName OUTPUT = ParameterName.getParameterName("output");

	public static ParameterName EMPTY = ParameterName.getParameterName("empty");

	private CPSOrderStatusConnectionManager mOrderStatusConnectionManager;

	private MutableRepository mRepository;

	public void setOrderStatusConnectionManager(CPSOrderStatusConnectionManager pOrderStatusConnectionManager){
		mOrderStatusConnectionManager = pOrderStatusConnectionManager;
	}

	public CPSOrderStatusConnectionManager getOrderStatusConnectionManager(){
		return mOrderStatusConnectionManager;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException{

		CPSOrderDetailBean orderDetail = null;
		String orderId = pRequest.getParameter(ORDER_NUMBER);
		try {
			orderDetail = getOrderStatusConnectionManager().requestOrderStatus(Integer.parseInt(orderId));
		} catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		if (orderDetail != null) {
			CPSSessionBean sessionBean = (CPSSessionBean)pRequest.resolveName(SESSION_BEAN_PATH);
			if (sessionBean != null) {
				sessionBean.getOrderDetailBeans().put(orderId, orderDetail);
			}
			String autoOrderId = getAutoOrderId(orderId);
			pRequest.setParameter("autoOrderId", autoOrderId);

			pRequest.setParameter("result", orderDetail);
			pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		}

	}

	private String getAutoOrderId(String orderId) {
		String autoOrderId = null;
		try {
			RepositoryView repositoryView = getRepository().getView(ORDER);
			RqlStatement statement = RqlStatement.parseRqlStatement(RQL_GET_ORDER_BY_ID);
			Object params[] = {orderId};
			RepositoryItem[] items = statement.executeQuery(repositoryView, params);

			if (items != null && items.length > 0) {
				autoOrderId = (String) items[0].getPropertyValue(AUTO_ORDER_ID);
			}
		} catch (RepositoryException e) {
			vlogError(e, "Repository exception during querying Auto Orders ID from Order ID.");
		}

		return autoOrderId;
	}

	public MutableRepository getRepository() {
		return mRepository;
	}

	public void setRepository(MutableRepository pRepository) {
		mRepository = pRepository;
	}
}