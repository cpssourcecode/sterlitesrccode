package com.cps.droplet;

import static com.cps.util.CPSConstants.REFINEMENTS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.endeca.infront.cartridge.model.Refinement;

/**
 * droplet to sort refinements 
 *
 */
public class RefinementsSorterDroplet extends DynamoServlet {
	
	/** repositoryItems oparam */
	private final static String SORTED_REFINEMENTS = "sortedRefinements";
	/** The Constant OUTPUT_OPARAM. */
	private static final String OUTPUT_OPARAM = "output";
	/** The Constant EMPTY_OPARAM. */
	private static final String EMPTY_OPARAM = "empty";
	
	public static final ParameterName PARAM_REFINEMENTS = ParameterName.getParameterName(REFINEMENTS);
	
	/**
	 * Droplet service method.
	 *
	 * @param pRequest
	 *            - request
	 * @param pResponse
	 *            - response
	 * @throws javax.servlet.ServletException
	 *             - if cannot make output or empty oparam
	 * @throws java.io.IOException
	 *             - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		
		List<Refinement> refinements = (List<Refinement>) pRequest.getObjectParameter(PARAM_REFINEMENTS);
		
		if (refinements != null && refinements.size() > 0) {
			
			Collections.sort(refinements, new Comparator<Refinement>() {
				@Override
				public int compare(final Refinement object1, final Refinement object2) {
					if(object1.getLabel() != null && object2.getLabel() != null){
						return object1.getLabel().compareTo(object2.getLabel());
					} else {
						return 0;
					}
					
				}
			});
			
			pRequest.setParameter(SORTED_REFINEMENTS, createAlphabeticMap(refinements));
			pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
			
		} else {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		}
	}
	
	/**
	 * create alphabetic map from sorted refinements list
	 * @param pRefinements - sorted refinements list
	 * @return alphabetic map
	 */
	private Map<String, List<Refinement>> createAlphabeticMap(List<Refinement> pRefinements){
		Map<String, List<Refinement>> alphabeticMap = new TreeMap<String, List<Refinement>>();
		
		String firstLetter = pRefinements.get(0).getLabel().substring(0, 1);
		List<Refinement> letterList = new ArrayList<Refinement>();
		
		for(Refinement refinement : pRefinements){
			String currentFirstLetter = refinement.getLabel().substring(0, 1);
			if(!currentFirstLetter.equals(firstLetter)){
				alphabeticMap.put(firstLetter, letterList);
				letterList = new ArrayList<Refinement>();
				letterList.add(refinement);
				firstLetter = currentFirstLetter;
			} else {
				letterList.add(refinement);
			}
		}
		
		alphabeticMap.put(firstLetter, letterList);
		return alphabeticMap;
	}
	
}
