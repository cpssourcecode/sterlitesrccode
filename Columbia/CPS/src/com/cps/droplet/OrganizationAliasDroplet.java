package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.cps.service.OrganizationAliasService;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

/**
 * This droplet returns true if the organization of the logged in profile has alias else returns false.
 */

public class OrganizationAliasDroplet extends DynamoServlet {

    public static ParameterName INPUT_PROFILE = ParameterName.getParameterName("profile");
    public static ParameterName TRUE = ParameterName.getParameterName("true");
    public static ParameterName FALSE = ParameterName.getParameterName("false");

    private OrganizationAliasService mOrganizationAliasService;

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        Profile profile = (Profile) pRequest.getObjectParameter(INPUT_PROFILE);
        vlogDebug("input profile :: {0} ", profile);
        if (profile != null) {
            if (!profile.isTransient()) {
                if (getOrganizationAliasService().isOraganizationHasAlias(profile)) {
                    pRequest.serviceLocalParameter(TRUE, pRequest, pResponse);
                } else {
                    pRequest.serviceLocalParameter(FALSE, pRequest, pResponse);
                }
            }
        }
    }

    public OrganizationAliasService getOrganizationAliasService() {
        return mOrganizationAliasService;
    }

    public void setOrganizationAliasService(OrganizationAliasService pOrganizationAliasService) {
        this.mOrganizationAliasService = pOrganizationAliasService;
    }

}