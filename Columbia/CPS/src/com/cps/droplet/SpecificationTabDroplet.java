package com.cps.droplet;

import atg.adapter.gsa.ChangeAwareMap;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SpecificationTabDroplet extends DynamoServlet {
	public static final ParameterName PRODUCT = ParameterName.getParameterName("product"); //sent product returns each resource (links) to display 
	public static final ParameterName HIDE_TAB_TEST=ParameterName.getParameterName("hideTabTest"); //use droplet to hide tab if this is set and nothing else
	public static final String MFR_CAT_NUM="mfr_cat_num"; //property that must be populated for TRADESERVICE DATA TO EXIST, otherwise don't even bother looking for it
	
	//TRADESERVICE PROPERTIES
	public static final String PKG_TYPE="pkg_type";
	public static final String INDV_WEIGHT="indv_weight";
	public static final String INDV_WEIGHT_UOM="indv_weight_uom";
	public static final String INDV_WEIGHT_QTY="indv_weight_qty";
	public static final String INDV_WEIGHT_QTY_UOM="indv_weight_qty_uom";
	public static final String LOW_LEAD_COMPLIANT_FLAG="low_lead_compliant_flag";
	public static final String MERCURY_FREE_FLAG="mercury_free_flag";
	public static final String WATER_SENSE_COMPLIANT_FLAG="water_sense_compliant_flag";
	public static final String GREEN_COMPLIANT_FLAG="green_compliant_flag";
	//TRADESERVICE KEYS FOR WebAppResources
	public static final String TS_PKG_TYPE="Package Type";//"ts.pkg.type";
	public static final String TS_INDV_WEIGHT="Individual Weight";
	public static final String TS_INDV_WEIGHT_UOM="Individual Weight Unit of Measure";
	public static final String TS_INDV_WEIGHT_QTY="Individual Weight Qty";
	public static final String TS_INDV_WEIGHT_QTY_UOM="Individual Weight Qty Unit of Measure";
	public static final String TS_LOW_LEAD_COMPLIANT_FLAG="Low Lead Compliant";//"ts.low.lead.compliant.flag";
	public static final String TS_MERCURY_FREE_FLAG="Mercury Free";//"ts.mercury.free.flag";
	public static final String TS_WATER_SENSE_COMPLIANT_FLAG="Water Sense Compliant";//"ts.water.sense.compliant.flag";
	public static final String TS_GREEN_COMPLIANT_FLAG="Green Compliant";//"ts.green.compliant.flag";
	
	//unilog data map property
	public static final String ATTRIBUTE_VALUE_MAP="attributeValueMap";

	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException,
			IOException {
		RepositoryItem theProduct = (RepositoryItem) req.getObjectParameter(PRODUCT);
		Map<String,String> specs=new HashMap<String,String>(); //key,value = property name,property value
		if(theProduct.getPropertyValue(MFR_CAT_NUM)!=null && !((String)(theProduct.getPropertyValue(MFR_CAT_NUM))).isEmpty()){
			//USE TRADESERVICE DATA
			vlogDebug("USER TRADESERVICE DATA");
			//Double pkgQty=(Double)theProduct.getPropertyValue(PKG_QTY);
			Double indvWeight=(Double)theProduct.getPropertyValue(INDV_WEIGHT);
			String indvWeightUom=(String)theProduct.getPropertyValue(INDV_WEIGHT_UOM);
			Double indvWeightQty=(Double)theProduct.getPropertyValue(INDV_WEIGHT_QTY);
			String indvWeightQtyUom=(String)theProduct.getPropertyValue(INDV_WEIGHT_QTY_UOM);
			Boolean lowLead=null;
			if(theProduct.getPropertyValue(LOW_LEAD_COMPLIANT_FLAG)!=null){
				vlogDebug("LOW LEAD="+(Boolean)theProduct.getPropertyValue(LOW_LEAD_COMPLIANT_FLAG)+","+theProduct.getPropertyValue(LOW_LEAD_COMPLIANT_FLAG));
				lowLead=(Boolean)theProduct.getPropertyValue(LOW_LEAD_COMPLIANT_FLAG);
			}
			Boolean mercuryFree=null;
			if(theProduct.getPropertyValue(MERCURY_FREE_FLAG)!=null){
				vlogDebug("MERCURY FREE="+(Boolean)theProduct.getPropertyValue(MERCURY_FREE_FLAG)+","+theProduct.getPropertyValue(MERCURY_FREE_FLAG));
				mercuryFree=(Boolean)theProduct.getPropertyValue(MERCURY_FREE_FLAG);
			}
			Boolean waterSense=null;
			if(theProduct.getPropertyValue(WATER_SENSE_COMPLIANT_FLAG)!=null){
				vlogDebug("WATER SENSE="+(Boolean)theProduct.getPropertyValue(WATER_SENSE_COMPLIANT_FLAG)+","+theProduct.getPropertyValue(WATER_SENSE_COMPLIANT_FLAG));
				waterSense=(Boolean)theProduct.getPropertyValue(WATER_SENSE_COMPLIANT_FLAG);
			}
			Boolean greenFlag=null; 
			if(theProduct.getPropertyValue(GREEN_COMPLIANT_FLAG)!=null){
				vlogDebug("GREEN FLAG="+(Boolean)theProduct.getPropertyValue(GREEN_COMPLIANT_FLAG)+","+theProduct.getPropertyValue(GREEN_COMPLIANT_FLAG));
				greenFlag=(Boolean)theProduct.getPropertyValue(GREEN_COMPLIANT_FLAG);
			}
			//if(pkgType!=null){specs.put(TS_PKG_TYPE, pkgType);}
			if(indvWeight!=null){specs.put(TS_INDV_WEIGHT, ""+indvWeight);}
			if(indvWeightUom!=null){specs.put(TS_INDV_WEIGHT_UOM, indvWeightUom);}
			if(indvWeightQty!=null){specs.put(TS_INDV_WEIGHT_QTY, ""+indvWeightQty);}
			if(indvWeightQtyUom!=null){specs.put(TS_INDV_WEIGHT_QTY_UOM, indvWeightQtyUom);}
			if(lowLead!=null && lowLead){specs.put(TS_LOW_LEAD_COMPLIANT_FLAG, "Low Lead Compliant");}
			if(mercuryFree!=null){
				if(mercuryFree){
					specs.put(TS_MERCURY_FREE_FLAG, "Yes");
				}
				else if(!mercuryFree){
					specs.put(TS_MERCURY_FREE_FLAG, "No");
				}
			}
			if(lowLead!=null){
				if(lowLead){
					specs.put(TS_LOW_LEAD_COMPLIANT_FLAG, "Yes");
				}
				else if(!lowLead){
					specs.put(TS_LOW_LEAD_COMPLIANT_FLAG,"No");
				}
			}
			if(waterSense!=null){
				if(waterSense){
					specs.put(TS_WATER_SENSE_COMPLIANT_FLAG, "Yes");
				}
				else if(!waterSense){
					specs.put(TS_WATER_SENSE_COMPLIANT_FLAG,"No");
				}
			}
			if(greenFlag!=null){
				if(greenFlag){
					specs.put(TS_GREEN_COMPLIANT_FLAG, "Yes");
				}
				else if(!greenFlag){
					specs.put(TS_GREEN_COMPLIANT_FLAG, "No");
				}
			}
		}
		else{
			//USE UNILOG DATA
			vlogDebug("USE UNILOG DATA");
			ChangeAwareMap attributeValueMap=(ChangeAwareMap)theProduct.getPropertyValue("attributeValueMap");
			vlogDebug("attributeValueMap=" + attributeValueMap);
			ChangeAwareMap attributeUomMap=(ChangeAwareMap)theProduct.getPropertyValue("attributeUomMap");
			vlogDebug("attributeUomMap=" + attributeUomMap);
			if ( null != attributeValueMap ) {
				for ( Object okey: attributeValueMap.keySet() ) {
					try {
						String key = (String) okey;
						String value = (String) attributeValueMap.get(key);
						if(attributeUomMap != null){
							String uom = (String) attributeUomMap.get(key);
							if(StringUtils.isNotBlank(uom) && value != null){
								value += " " + uom;
							}
						}
						if ( StringUtils.isNotBlank(key) && StringUtils.isNotBlank(value) ) {
							specs.put(key, value);
						}
					} catch (Exception e) {
						vlogError(e, "Could not read product attribute");
					}
				}
			}
		}
		String hideTabTest= (String) req.getObjectParameter(HIDE_TAB_TEST);
		if(hideTabTest!=null && !hideTabTest.isEmpty()){
			if(specs.keySet().size()>0){
				req.serviceLocalParameter("outputTabHeader",req,res);
			}
			//else hide tab
		}
		else if(specs.keySet().size()>0){
			//show tab
			req.serviceLocalParameter("openSpecTab", req, res);
			Iterator<Map.Entry<String,String>> ispecs = specs.entrySet().iterator();
			String key1 = "", key2 = "", value1 = "";
			while( ispecs.hasNext() ) {
				Map.Entry<String,String> entry = ispecs.next();
				if ( "".equals(key1) ) {
					key1 = entry.getKey();
					value1 = entry.getValue();
				} else if( "".equals(key2) ) {
					key2 = entry.getKey();
					req.setParameter("key1", key1);
					setParameterYesNoCheck(req, res, "value1", value1);
					req.setParameter("key2", key2);
					setParameterYesNoCheck(req, res, "value2", entry.getValue());
					req.serviceLocalParameter("outputSpecPair", req, res);
					key1 = key2 = value1 = "";
				}
			}
			//check if odd number of properties
			if( "".equals(key2) && !"".equals(key1) ) {
				//odd one out left to print
				req.setParameter("key1", key1);
				setParameterYesNoCheck(req, res, "value1", value1);
				req.serviceLocalParameter("outputLastSpec", req, res);
			}
			req.serviceLocalParameter("closeSpecTab",req,res);
		}
		else{
			//no specs, output empty, suppress tab
			req.serviceLocalParameter("outputNothing",req,res);
		}
	}
	public void setParameterYesNoCheck(DynamoHttpServletRequest req, DynamoHttpServletResponse res, String key, String value) throws ServletException,
	IOException {
		if(value.equals("Y")){
			req.setParameter(key, "Yes");
		}
		else if(value.equals("N")){
			req.setParameter(key,"No");
		}
		else if(value!=null && !value.equals("") && !value.equals("Y") && !value.equals("N")){
			req.setParameter(key, value);
		}
	}
}
