package com.cps.droplet;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;

import com.cps.util.CPSGlobalProperties;
import com.endeca.infront.assembler.ContentItem;
import com.endeca.infront.cartridge.model.DimensionSearchGroup;
import com.endeca.infront.cartridge.model.DimensionSearchValue;

import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * This droplet is used to iterate through the suggestion terms returned by the endeca and normalize it as well as weed out the duplicates
 * Also, this will trim the search string to 40 characters and displayable string to 32 characters or as configured in the CPSGlobalProperties
 * to suit the typeahead display screen
 * @author R.Srinivasan
 *
 */

public class FetchUniqueEndecaSuggestionsDroplet extends DynamoServlet {
	
	private static final String CONTENT_ITEM_NAME="contentItem";
	private static final ParameterName CONTENT_ITEM=ParameterName.getParameterName(CONTENT_ITEM_NAME);
	private static final ParameterName OUTPUT=ParameterName.getParameterName("output");
	private static final String DMIENSION_SEARCH_GROUPS="dimensionSearchGroups";
	private static final String FILTERED_ENDECA_DIMENSIONS="filteredEndecaDimensions";
	
	private CPSGlobalProperties globalProperties;
		
	public CPSGlobalProperties getGlobalProperties() {
		return globalProperties;
	}

	public void setGlobalProperties(CPSGlobalProperties globalProperties) {
		this.globalProperties = globalProperties;
	}

	@Override
	public void service(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws ServletException, IOException {
		 
			ContentItem suggestions=(ContentItem)request.getObjectParameter(CONTENT_ITEM);
			List<DimensionSearchGroup> dimSearchGroups=(List)suggestions.get(DMIENSION_SEARCH_GROUPS);
			vlogDebug("dimSearch groups are - {0}",dimSearchGroups);
			Set<String> sortedSuggestions=new LinkedHashSet<>();			
			if(dimSearchGroups!=null) {
				for(DimensionSearchGroup dimSearchGroup: dimSearchGroups) {
					vlogDebug("one dimSearch group is - {0}",dimSearchGroup);
					List <DimensionSearchValue> dimensionSearchValues=dimSearchGroup.getDimensionSearchValues();
					
						if(dimensionSearchValues!=null) {
							for(DimensionSearchValue dimSearchValue: dimensionSearchValues) {
								 String dimLabel=dimSearchValue.getLabel();
								 vlogDebug("unescaped dim label is - {0}", dimLabel);
								 dimLabel=dimLabel.toUpperCase().replaceAll("[^\\p{ASCII}]", "");
								 dimLabel=dimLabel.replaceAll("[^a-zA-Z0-9_-]", " ");
								 dimLabel=dimLabel.trim();
								 dimLabel=dimLabel.replaceAll("( )+", " ");								 
								 if(dimLabel.length() > getGlobalProperties().getSearchTermsMaxLength()) {
									 dimLabel=dimLabel.substring(0, getGlobalProperties().getSearchTermsMaxLength());
								 }								 
								 sortedSuggestions.add(dimLabel);
							}
						}					
				}
			}			 
			vlogDebug("sorted and filtered suggestions are - {0}",sortedSuggestions);
			request.setParameter(FILTERED_ENDECA_DIMENSIONS, sortedSuggestions);
			request.serviceLocalParameter(OUTPUT, request, response);
		
		
	}
	
}
