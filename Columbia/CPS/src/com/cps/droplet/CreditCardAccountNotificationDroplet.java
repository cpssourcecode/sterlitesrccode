package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.cps.util.CPSConstants;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

/**
 * Credit card only account notification modal will be displayed based on the user's address 
 * @author Manikandan
 *
 */
public class CreditCardAccountNotificationDroplet extends DynamoServlet {

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final ParameterName OPARAM_EMPTY = ParameterName.getParameterName("empty");
	private static final ParameterName PROFILE = ParameterName.getParameterName("profile");
	
	@Override
	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res)
			throws ServletException, IOException {
		Profile profile = (Profile) req.getObjectParameter(PROFILE);
		if (profile != null) {
			RepositoryItem billingAddress = (RepositoryItem) profile.getPropertyValue(CPSConstants.BILLING_ADDRESS);
			RepositoryItem cartSelectedCS  = (RepositoryItem) profile.getPropertyValue(CPSConstants.CART_SELECTED_CS);
			RepositoryItem selectedCS  = (RepositoryItem) profile.getPropertyValue(CPSConstants.SELECTED_CS);
			if(cartSelectedCS == null && selectedCS != null){
				String selectedCSPayInstRYIN = (String) selectedCS.getPropertyValue(CPSConstants.PAY_INST_RYIN);
				if(selectedCSPayInstRYIN != null){
					cartSelectedCS = selectedCS;
				}
			}
			
			boolean isCCNotificationAccepted = (boolean) profile.getPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED);
			boolean isCCNotificationShown = (boolean) profile.getPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION);
			
			String billPayInstRYIN=null;
			String cartCSPayInstRYIN=null;
			if(billingAddress != null || cartSelectedCS != null ) {
				billPayInstRYIN = (String) billingAddress.getPropertyValue(CPSConstants.PAY_INST_RYIN);
				if(cartSelectedCS != null){
					cartCSPayInstRYIN = (String) cartSelectedCS.getPropertyValue(CPSConstants.PAY_INST_RYIN);
				}				
				if ((billPayInstRYIN != null && billPayInstRYIN.equals("?")) || (cartCSPayInstRYIN != null && cartCSPayInstRYIN.equals("?"))) {
					if(isCCNotificationShown && !isCCNotificationAccepted){
						profile.setPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED, false);
						profile.setPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION, true);
						req.setParameter("isCCNotificationAccepted", profile.getPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED));
						req.setParameter("isCCNotificationShown", profile.getPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION));
					}
				} else {
					profile.setPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED, true);
					profile.setPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION, false);
					req.setParameter("isCCNotificationAccepted", profile.getPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED));
					req.setParameter("isCCNotificationShown", profile.getPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION));
				}
				req.serviceLocalParameter(OUTPUT_OPARAM, req, res);
			} else {
				req.serviceLocalParameter(OPARAM_EMPTY, req, res);
			}
			
		}
		
	}

}
