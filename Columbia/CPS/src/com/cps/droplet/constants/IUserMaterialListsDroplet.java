package com.cps.droplet.constants;

/**
 * @author Dmitry Golubev
 **/
public interface IUserMaterialListsDroplet extends IDropletConstants {
	String ID = "id";
	String NAME = "eventName";
	String DATE = "creationDate";
	String ITEMS = "items";
	String TERM = "term";
	String GIFT_LIST_ITEMS = "giftlistItems";
}
