package com.cps.droplet;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSAvailabilityManager;
import com.cps.util.CPSGlobalProperties;
import com.cps.util.CPSSessionBean;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class CPSPricingServiceAvailableDroplet extends DynamoServlet {

    private static final String OPARAM_OUTPUT = "output";
    private static final String PARAM_AVAILABLE = "available";

    private String mSessionBeanPath;
    private CPSAvailabilityManager mAvailabilityManager;

    private long mCheckIntervalInMinutes;

    private boolean mAvailablePricesWebService = false;
    private Date mLastCheckDate;
    private CPSGlobalProperties cpsGlobalProperties;

    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        if (mLastCheckDate == null) {
            mLastCheckDate = new Date(0);
        }
        long diffInMillis = Math.abs(mLastCheckDate.getTime() - new Date().getTime());
        long diff = TimeUnit.MINUTES.convert(diffInMillis, TimeUnit.MILLISECONDS);
        CPSSessionBean sessionBean = ((CPSSessionBean) req.resolveName(getSessionBeanPath()));
        if(!getCpsGlobalProperties().isTestMode()){
        	if (getCheckIntervalInMinutes() > 0 && diff > getCheckIntervalInMinutes()) {
                mLastCheckDate = new Date();
                mAvailablePricesWebService = getAvailabilityManager().isJdeAvailable();
                if (sessionBean != null) {
                    sessionBean.setAvailablePricesWebService(mAvailablePricesWebService);
                }
            } else if (sessionBean != null && sessionBean.getAvailablePricesWebService() != mAvailablePricesWebService) {
                mAvailablePricesWebService = getAvailabilityManager().isJdeAvailable();
                sessionBean.setAvailablePricesWebService(mAvailablePricesWebService);
            }
            if (mAvailablePricesWebService) {
                req.setParameter(PARAM_AVAILABLE, true);
            } else {
                req.setParameter(PARAM_AVAILABLE, false);
            }
        }else{
        	req.setParameter(PARAM_AVAILABLE, true);
        	sessionBean.setAvailablePricesWebService(true);
        }        
        req.serviceLocalParameter(OPARAM_OUTPUT, req, res);
    }

    public String getSessionBeanPath() {
        return mSessionBeanPath;
    }

    public void setSessionBeanPath(String pSessionBeanPath) {
        this.mSessionBeanPath = pSessionBeanPath;
    }


    public long getCheckIntervalInMinutes() {
        return mCheckIntervalInMinutes;
    }

    public void setCheckIntervalInMinutes(long pCheckIntervalInMinutes) {
        this.mCheckIntervalInMinutes = pCheckIntervalInMinutes;
    }

    public CPSAvailabilityManager getAvailabilityManager() {
        return mAvailabilityManager;
    }

    public void setAvailabilityManager(CPSAvailabilityManager pAvailabilityManager) {
        mAvailabilityManager = pAvailabilityManager;
    }

	/**
	 * @return the cpsGlobalProperties
	 */
	public CPSGlobalProperties getCpsGlobalProperties() {
		return cpsGlobalProperties;
	}

	/**
	 * @param cpsGlobalProperties the cpsGlobalProperties to set
	 */
	public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
		this.cpsGlobalProperties = cpsGlobalProperties;
	}
    
}
