package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.messages.RepositoryMessagesTools;

/**
 * @author vignesh 
 * this droplet fetch the message from messageRepository.
 */
public class RepositoryMessagesLookupDroplet extends DynamoServlet {
    private static final String MESSAGE = "message";
    private RepositoryMessagesTools repositoryMessagesTools;
    private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
    private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        String messageKey = pRequest.getParameter("messageKey");
        String messageValue = null;
        try {
            messageValue = getRepositoryMessagesTools().getMessageValue(messageKey);
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (StringUtils.isBlank(messageValue)) {
            pRequest.setParameter(MESSAGE,"enter the valid key");
            pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
        } else {
            pRequest.setParameter(MESSAGE, messageValue);
            pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
        }

    }

    /**
     * @return the repositoryMessagesTools
     */
    public RepositoryMessagesTools getRepositoryMessagesTools() {
        return repositoryMessagesTools;
    }

    /**
     * @param repositoryMessagesTools
     *            the repositoryMessagesTools to set
     */
    public void setRepositoryMessagesTools(RepositoryMessagesTools repositoryMessagesTools) {
        this.repositoryMessagesTools = repositoryMessagesTools;
    }

}
