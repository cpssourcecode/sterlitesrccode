package com.cps.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import com.cps.commerce.gifts.CPSGiftlistManager;
import com.cps.commerce.order.purchase.CPSPriceAvailability;
import com.cps.util.CPSConstants;
import com.cps.util.CPSSessionBean;
import com.cps.util.security.CriptoUtils;

import atg.commerce.CommerceException;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

/**
 * 
 * @author VSG
 * if user goes from shared material list email link to material list mark all items in the list for availability and proce check.
 */
public class CPSMaterialListPriceAvailabilityCheckDroplet extends DynamoServlet {

	public static final String INPUT_PROFILE = "profile";
	public static final String INPUT_GIFTLIST_ID = "giftlistId";
	public static final String INPUT_SESSION_BEAN = "sessionBean";
	public static final String OUTPUT_SHOW_PA = "showItemPA";
	public static final String OUTPUT = "output";
	private CPSGiftlistManager mGiftlistManager;
	
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		String wishListIdParam = pRequest.getParameter(CPSConstants.WISHLIST_ID_PARAM);
		String profileIdParam = pRequest.getParameter(CPSConstants.PROFILE_ID_PARAM);
		Profile profile = getProfile(pRequest);
		String giftlistId = getGiftlistId(pRequest);
		CPSSessionBean sessionBean = getSessionBean(pRequest);
		boolean showItemPa = false;
		if (profile != null && StringUtils.isNotBlank(giftlistId) && StringUtils.isNotBlank(wishListIdParam)
				&& StringUtils.isNotBlank(profileIdParam) && sessionBean != null) {
			String wishlistId = decrypt(wishListIdParam);
			String profileId = decrypt(profileIdParam);
			if (profileId != null && wishlistId != null &&
					profileId.equals(profile.getRepositoryId()) && wishlistId.equals(giftlistId) &&
					getGiftlistManager().isHaveAccessToGiftlist(giftlistId, profile)) {
				synchronized (sessionBean) {
					createPriceAvailabilityItems(profile, giftlistId, sessionBean);
					showItemPa = true;
				}
			}
		}
		pRequest.setParameter(OUTPUT_SHOW_PA, showItemPa);
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
	}
	
	private void createPriceAvailabilityItems(Profile pProfile, String pGiftlistId, CPSSessionBean pSessionBean) {
		try {
			String cs = getCsObject(pProfile);
			RepositoryItem giftlist = getGiftlistManager().getGiftlist(pGiftlistId);
			List<RepositoryItem> giftlistItems = (List) giftlist.getPropertyValue(CPSConstants.GIFTLIST_ITEMS);
			if(giftlistItems != null && !giftlistItems.isEmpty()) {
				Map<String,Long> itemsForPA = new HashMap<String,Long>();
				for(RepositoryItem giftListItem: giftlistItems) {
					CPSPriceAvailability priceAvailability = new CPSPriceAvailability();
					long qty = getGiftlistManager().getGiftlistItemQuantityDesired(giftListItem.getRepositoryId());
					String sku = getGiftlistManager().getGiftlistItemCatalogRefId(giftListItem.getRepositoryId());
					String productId = getGiftlistManager().getGiftlistItemProductId(giftListItem.getRepositoryId());
					priceAvailability.setCsid(cs);
					priceAvailability.setQty(qty);
					priceAvailability.setSku(sku);
					priceAvailability.setPaproductid(productId);
					if(StringUtils.isNotBlank(productId)) {
						itemsForPA.put(productId, qty);
					}
					pSessionBean.getPalist().add(priceAvailability);
				}
				pSessionBean.getItemIdsPA().clear();
				if (itemsForPA.size() > 0) {
					pSessionBean.setItemIdsPA(itemsForPA);
				}
			}
		} catch (CommerceException e) {
			vlogError(e, "Error in CPSMaterialListPriceAvailabilityCheckDroplet.createPriceAvailabilityItems");
		}
	}

	private String getCsObject(Profile pProfile) {
		RepositoryItem csObject = (RepositoryItem) pProfile.getPropertyValue(CPSConstants.SELECTED_CS);
		String cs = null;
		if(csObject != null) {
			cs = (String) csObject.getPropertyValue(CPSConstants.ID);
		}
		return cs;
	}
	
	private String decrypt(String pParam) {
		String result = null;
		try {
			result = CriptoUtils.aesDecrypt(pParam);
		} catch (Exception e) {
			vlogError(e, "Error in CPSProfileRequest");
		}
		return result;
	}
	
	private Profile getProfile(DynamoHttpServletRequest pRequest) {
		Profile profile = null;
		Object obj = pRequest.getLocalParameter(INPUT_PROFILE);
		if(obj instanceof Profile) {
			profile = (Profile) obj;
		}
		return profile;
	}

	private String getGiftlistId(DynamoHttpServletRequest pRequest) {
		String giftlistId = null;
		Object obj = pRequest.getLocalParameter(INPUT_GIFTLIST_ID);
		if(obj instanceof String) {
			giftlistId = (String) obj;
		}
		return giftlistId;
	}
	
	private CPSSessionBean getSessionBean(DynamoHttpServletRequest pRequest) {
		CPSSessionBean sessionBean = null;
		Object obj = pRequest.getLocalParameter(INPUT_SESSION_BEAN);
		if(obj instanceof CPSSessionBean) {
			sessionBean = (CPSSessionBean) obj;
		}
		return sessionBean;
	}

	public CPSGiftlistManager getGiftlistManager() {
		return mGiftlistManager;
	}

	public void setGiftlistManager(CPSGiftlistManager pGiftlistManager) {
		mGiftlistManager = pGiftlistManager;
	}
	
}
