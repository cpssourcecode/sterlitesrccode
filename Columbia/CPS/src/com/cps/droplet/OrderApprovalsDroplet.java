package com.cps.droplet;


import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderBean;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.commerce.order.CPSOrderTools;
import com.cps.util.CPSConstants;

import javax.servlet.ServletException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class OrderApprovalsDroplet extends DynamoServlet {

	private static final String COMPONENT_NAME = "/cps/droplet/OrderApprovalsDroplet";

	private static final String ORDER_STATE_PENDING_APPROVAL = "PENDING_APPROVAL";
	//private static final String ORDER_STATE_FAILED_APPROVAL = "FAILED_APPROVAL";

	private static final ParameterName PROFILE = ParameterName.getParameterName("profile");

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

	//private static final String RETRIEVE_ORDERS_RQL_QUERY = "state =?0 AND submittedDate >=?1 AND organizationId =?2";
	private static final String RETRIEVE_ORDERS_RQL_QUERY = "state =?0 AND submittedDate >=?1 AND authorizedApproverIds INCLUDES ?2";

	private OrderManager mOrderManager;

	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	private int mDays = 7;

	public int getDays() {
		return mDays;
	}

	public void setDays(int pDays) {
		mDays = pDays;
	}

	private RepositoryItem getUser(String pProfileId) {
		RepositoryItem user = null;
		try {
			user = getOrderManager().getOrderTools().getProfileTools().getProfileItem(pProfileId);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return user;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - start").toString());
		}

		List<Order> orders = null;
		Profile profile = (Profile) pRequest.getObjectParameter(PROFILE);
		if (profile != null) {
			orders = retrieveOrders(profile);
		}

		ParameterName renderParam = OUTPUT_OPARAM;
		if (orders != null && orders.size() > 0) {
			List<CPSOrderBean> orderInfos = new ArrayList<CPSOrderBean>();
			for (Order order : orders) {
				CPSOrderBean orderBean = new CPSOrderBean();
				orderBean.setId(order.getId());
				orderBean.setWebOrderId(((CPSOrderImpl) order).getWebOrderId());
				try {
					orderBean.setOrderTotal(order.getPriceInfo().getTotal());
				} catch (Exception ex) {
					logError(ex);
				}
				RepositoryItem user = getUser(order.getProfileId());
				if (user != null) {
					String firstName = (String) user.getPropertyValue(CPSConstants.FIRST_NAME);
					String lastName = (String) user.getPropertyValue(CPSConstants.LAST_NAME);
					StringBuilder userName = new StringBuilder();
					if(StringUtils.isNotBlank(firstName)){
						userName.append(firstName);
					}
					if(StringUtils.isNotBlank(lastName)){
						userName.append(" ").append(lastName);
					}
					orderBean.setProfileName(userName.toString());
					Double spendingLimit = (Double) user.getPropertyValue(CPSConstants.ORDER_LIMIT);
					if (spendingLimit != null) {
						orderBean.setSpendingLimit(spendingLimit);
					}
				}
				orderBean.setProfileId(order.getProfileId());
				orderBean.setSubmittedDate(order.getSubmittedDate());
				orderInfos.add(orderBean);
			}

			pRequest.setParameter("orders", orderInfos);
			//pRequest.setParameter("orders", orders);
			pRequest.setParameter("ordersSize", orderInfos.size());
		} else {
			renderParam = EMPTY_OPARAM;
		}
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - end").toString());
		}

	}

	private List<Order> retrieveOrders(Profile pProfile) {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".retrieveOrders").append(" - start").toString());
		}

		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DAY_OF_YEAR, -getDays());

		List<Order> orderList = null;

		try {
			RepositoryItem parentOrganization = ((CPSOrderManager) getOrderManager()).getProfileTools().getParentOrganization(pProfile);
			if (parentOrganization != null) {
				Object[] params = new Object[3];
				params[0] = ORDER_STATE_PENDING_APPROVAL;
				params[1] = new Timestamp(currentDate.getTime().getTime());
				params[2] = pProfile.getRepositoryId();
				//params[2] = parentOrganization.getRepositoryId();

				orderList = ((CPSOrderManager) getOrderManager()).retrieveOrders(params, RETRIEVE_ORDERS_RQL_QUERY);
			}
		} catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".retrieveOrders").append(" - exit").toString());
		}
		return orderList;

	}


}