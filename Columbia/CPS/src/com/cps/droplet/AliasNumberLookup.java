package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.adapter.gsa.ChangeAwareSet;
import atg.commerce.catalog.CatalogTools;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

import com.cps.service.OrganizationAliasService;

public class AliasNumberLookup extends DynamoServlet {
	
	/**
	 * This droplet takes in the sku id and gets the profile and returns 
	 * an alias for that product if one exists in output oparam. If none 
	 * exists, return nothing in empty oparam.
	 */
	
	public static ParameterName SKU_ID = ParameterName.getParameterName("skuId");
	
	public static ParameterName EMPTY = ParameterName.getParameterName("empty");
	public static ParameterName OUTPUT = ParameterName.getParameterName("output");
	
	private CatalogTools catalogTools;
	
	private OrganizationAliasService mOrganizationAliasService;
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		// Get the profile and prodId
		Profile profile = (Profile)ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/Profile");
		String prodId = pRequest.getParameter(SKU_ID);
		
		// Get the Sku Object from passed in information
		RepositoryItem sku = null;
		try {
			sku = getCatalogTools().findSKU(prodId);
		} catch (RepositoryException e) {
			vlogError(e, "Error finding product with passed sku id: "+prodId);
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			return;
		}
		vlogDebug("Profile Check "+ profile );
		if (sku != null && profile != null){
			if(!profile.isTransient()){
				ChangeAwareSet parentProducts = (ChangeAwareSet) (sku.getPropertyValue("parentProducts"));
				if (parentProducts != null){
					RepositoryItem product = (RepositoryItem) parentProducts.iterator().next();
					String productAlias = getOrganizationAliasService().findAlias(profile, product);
					
					if (productAlias != null){
						pRequest.setParameter("alias", productAlias.replaceAll(",", ", "));
						pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
						return;
					}else{
						pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
						return;
					}
				}
			}else{
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
				return;
			}
		}
		pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		return;
	}
	
	public OrganizationAliasService getOrganizationAliasService() {
		return mOrganizationAliasService;
	}

	public void setOrganizationAliasService(
			OrganizationAliasService pOrganizationAliasService) {
		this.mOrganizationAliasService = pOrganizationAliasService;
	}

	public CatalogTools getCatalogTools() {
		return catalogTools;
	}

	public void setCatalogTools(CatalogTools catalogTools) {
		this.catalogTools = catalogTools;
	}
}