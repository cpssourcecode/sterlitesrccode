package com.cps.droplet;

import static com.cps.util.CPSConstants.SHOPPING_CART_PATH;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.inventory.InventoryAvailabilityInfo;
import com.cps.service.CPSExternalProductService;

import atg.commerce.order.OrderHolder;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import vsg.common.message.IMessageUtils;
import vsg.messages.MessagesRepositoryConstants;

/**
 *
 * @author Steve Neverov
 *
 */
public class CPSInventoryDroplet extends DynamoServlet {

    public static ParameterName PRODUCT_ID = ParameterName.getParameterName("productId");
    public static ParameterName QTY = ParameterName.getParameterName("qty");
    public static ParameterName CUSTOMER_ID = ParameterName.getParameterName("customerId");
    public static ParameterName ALL = ParameterName.getParameterName("all");
    public static ParameterName REQUEST_CURRENT_STORE = ParameterName.getParameterName("isRequestCurrentStore");
    public static ParameterName IS_EMPTY_CART = ParameterName.getParameterName("isEmptyCart");

    
    private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
    private static final ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");

    private static final String QTY_AVAILABLE = "quantityAvailable";
    private static final String LEAD_TIME = "leadTime";
    private static final String INFOS = "inventoryAvailabilityInfos";
    private static final String ERROR_MESSAGE = "errorMessage";
    private static final String DEFAULT_LEAD_TIME_BRANCH = "200";
    private static final String DEFAULT_BRANCH_QTY = "defaultBranchQuantity";

    private CPSExternalProductService mExternalProductService;
    private IMessageUtils mMessageUtils;
    private int mLeadTimeThreshold;

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        // check pricing client enabled call it otherwise call dummy
        if (getExternalProductService().isInventoryWebserviceEnable()) {
            String productId = (String) pRequest.getObjectParameter(PRODUCT_ID);
            String qty = (String) pRequest.getObjectParameter(QTY);
            long productQty = 1;
            if (!StringUtils.isBlank(qty)) {
                productQty = Long.valueOf(qty);
            }
            String customerId = (String) pRequest.getObjectParameter(CUSTOMER_ID);
            String all = (String) pRequest.getObjectParameter(ALL);
            String isRequestCurrentStore = (String) pRequest.getObjectParameter(REQUEST_CURRENT_STORE);   
            Boolean isEmptyCart = Boolean.valueOf(pRequest.getParameter(IS_EMPTY_CART));
            if ("true".equalsIgnoreCase(isRequestCurrentStore)) {
                requestStoreInventory(productId, productQty, pRequest, pResponse);
            } else if ("true".equalsIgnoreCase(all)) {
                requestAllInventory(customerId, productId, productQty, pRequest, pResponse);
            } else {
                requestInventory(customerId, productId, productQty, pRequest, pResponse);
            }

        } else {
            pRequest.setParameter(QTY_AVAILABLE, 0);
            pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
        }

    }

    private void requestInventory(String pCustomerId, String pProductId, long pOrderedQty, DynamoHttpServletRequest pRequest,
                    DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        try {
            int availableQty = 0;
            int leadTime = 0;
            int defaultBranchQuantity = 0;
            List<Integer> infoLeadTimes = new ArrayList<Integer>();
            List<InventoryAvailabilityInfo> infos = getExternalProductService().requestAllInventory(pCustomerId, pProductId, pOrderedQty);
            if (!infos.isEmpty()) {
                for (InventoryAvailabilityInfo info : infos) {
                    availableQty += info.getQuantity();
                    infoLeadTimes.add(info.getLeadTime());
                    if(info.isBranchDefault()){
                    defaultBranchQuantity = (int) info.getQuantity();
                    }
                }
                if (availableQty == 0) {
                	Collections.sort(infoLeadTimes);
                	leadTime=infoLeadTimes.get(0);
                }
            	vlogDebug("leadTime {0}",leadTime);
            }

            pRequest.setParameter(QTY_AVAILABLE, availableQty);
            pRequest.setParameter(LEAD_TIME, leadTime);
            pRequest.setParameter(DEFAULT_BRANCH_QTY, defaultBranchQuantity);

            pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            pRequest.setParameter(ERROR_MESSAGE, getMessageUtils().getMessage(MessagesRepositoryConstants.CHECK_AVAILABILITY_ERROR_MESSAGE));
            pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
        }
    }

    private void requestAllInventory(String pCustomerId, String pProductId, long pOrderedQty, DynamoHttpServletRequest pRequest,
                    DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        try {
            OrderHolder shoppingCart = (OrderHolder) pRequest.resolveName(SHOPPING_CART_PATH);
            List<InventoryAvailabilityInfo> infos = StringUtils.isNotBlank(pCustomerId)
                            ? getExternalProductService().requestAllInventory(pCustomerId, pProductId, pOrderedQty)
                            : getExternalProductService().requestAllInventory(pProductId, getProfile(), (CPSOrderImpl) shoppingCart.getCurrent(), pOrderedQty);
            pRequest.setParameter(INFOS, infos);
            pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
        }
    }

    private void requestStoreInventory(String pProductId, long pOrderedQty, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
                    throws ServletException, IOException {
        try {
            OrderHolder shoppingCart = (OrderHolder) pRequest.resolveName(SHOPPING_CART_PATH);
            List<InventoryAvailabilityInfo> infos = getExternalProductService().requestStoreInventoryFromPDP(pProductId, getProfile(),
                            (CPSOrderImpl) shoppingCart.getCurrent(), pOrderedQty);
            pRequest.setParameter(INFOS, infos);
            pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
        }
    }

    public Profile getProfile() {
        return (Profile) ServletUtil.getCurrentUserProfile();
    }

    public CPSExternalProductService getExternalProductService() {
        return mExternalProductService;
    }

    public void setExternalProductService(CPSExternalProductService pExternalProductService) {
        mExternalProductService = pExternalProductService;
    }

    public IMessageUtils getMessageUtils() {
        return mMessageUtils;
    }

    public void setMessageUtils(IMessageUtils pMessageUtils) {
        mMessageUtils = pMessageUtils;
    }

    public int getLeadTimeThreshold() {
        return mLeadTimeThreshold;
    }

    public void setLeadTimeThreshold(int pLeadTimeThreshold) {
        mLeadTimeThreshold = pLeadTimeThreshold;
    }

}
