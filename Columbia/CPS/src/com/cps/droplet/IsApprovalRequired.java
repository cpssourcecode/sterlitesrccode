package com.cps.droplet;

import atg.commerce.order.Order;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.cps.commerce.approval.ApprovalManager;

import javax.servlet.ServletException;
import java.io.IOException;

public class IsApprovalRequired extends DynamoServlet {

	private static final String COMPONENT_NAME = "/cps/droplet/IsApprovalRequired";

	private static final ParameterName ORDER = ParameterName.getParameterName("order");

	private static final ParameterName TRUE_OPARAM = ParameterName.getParameterName("true");
	private static final ParameterName FALSE_OPARAM = ParameterName.getParameterName("false");

	public Profile getProfile() {
		return (Profile) ServletUtil.getCurrentUserProfile();
	}

	private ApprovalManager mApprovalManager;

	public ApprovalManager getApprovalManager() {
		return mApprovalManager;
	}

	public void setApprovalManager(ApprovalManager pApprovalManager) {
		mApprovalManager = pApprovalManager;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - start").toString());
		}

		ParameterName renderParam = FALSE_OPARAM;
		Order order = (Order)pRequest.getObjectParameter(ORDER);
		Profile profile = getProfile();
		if (profile != null && order != null && getApprovalManager().isApprovalRequired(order, profile)) {
			renderParam = TRUE_OPARAM;
		}
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - end").toString());
		}

	}

}