package com.cps.droplet;

import static com.cps.util.CPSConstants.*;
import static vsg.constants.VSGConstants.MAP_KEY_VALUE_DELIMITER;
import static vsg.constants.VSGConstants.MAP_PAIR_DELIMITER;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

import com.cps.userprofiling.CPSProfileTools;
import com.endeca.infront.cartridge.model.StringAttribute;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

public class AliasNumberLookupFromIndex extends DynamoServlet {

	/**
	 * input parameters.
	 */
	public static final ParameterName PROD_ALIAS_MAP_PARAMETR = ParameterName.getParameterName(PRODUCT_ALIAS_MAP);
//	public static final ParameterName USER_ID_PARAMETR = ParameterName.getParameterName(USER_ID);


	private static final String ALIAS = "alias";

	private CPSProfileTools mProfileTools;

	/**
	 * output.
	 */
	public static final ParameterName OUTPUT = ParameterName.getParameterName("output");

	/**
	 * EMPTY
	 */
	public static final ParameterName EMPTY = ParameterName.getParameterName("empty");

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		// Get the profile
		Profile profile = (Profile) ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/Profile");
		if (profile.isTransient()) { // only logged user can get product alias map
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			return;
		}

		Map<String, String> prodAliasMap = getProductAliasMap(pRequest);

		if (null != prodAliasMap) {
			String productAlias = getProductAlias(profile, prodAliasMap);

			if (!StringUtils.isEmpty(productAlias)) {
				productAlias = productAlias.replaceAll(",", ", ");
			}
			pRequest.setParameter(ALIAS, productAlias);
		}
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
	}

	/**
	 * @param pRequest request
	 * @return product alias map
	 */
	private Map<String, String> getProductAliasMap(DynamoHttpServletRequest pRequest) {
		// Get PRODUCT_ALIAS_MAP
		String prodAliasMapStr = null;
		Object oProdAliasMapStrAtr = pRequest.getObjectParameter(PROD_ALIAS_MAP_PARAMETR);

		if (oProdAliasMapStrAtr instanceof StringAttribute) {
			Object oProdAliasMapStr = ((StringAttribute) oProdAliasMapStrAtr).get(0);
			if (oProdAliasMapStr != null) {
				prodAliasMapStr = (String) oProdAliasMapStr;
			}
		}
		return getValue(prodAliasMapStr);
	}

	/**
	 * @param pProfile      profile
	 * @param pProdAliasMap product alias map
	 * @return product alias
	 */
	private String getProductAlias(Profile pProfile, Map<String, String> pProdAliasMap) {
		String productAlias = null;
		String orgId = null;

		RepositoryItem organization = getProfileTools().getParentOrganization(pProfile);
		if (organization != null) {
			orgId = organization.getRepositoryId();
		}

		if (orgId != null) {
			productAlias = pProdAliasMap.get(orgId);
		}

		return productAlias;
	}

	/**
	 * return map of strings
	 *
	 * @param pValueObj passed value
	 * @return map of strings
	 */
	public Map<String, String> getValue(String pValueObj) {
		Map<String, String> resultValues = null;

		if (null != pValueObj) {
			resultValues = new HashMap<>();
			StringTokenizer st = new StringTokenizer(pValueObj, MAP_PAIR_DELIMITER);
			while (st.hasMoreTokens()) {
				String keyValuePair = st.nextToken();
				vlogDebug("keyValuePair={0}", keyValuePair);

				StringTokenizer pairTokenize = new StringTokenizer(keyValuePair, MAP_KEY_VALUE_DELIMITER);
				String key = null;
				String value = null;
				if (pairTokenize.hasMoreTokens()) {
					key = pairTokenize.nextToken();
				}
				if (pairTokenize.hasMoreTokens()) {
					value = pairTokenize.nextToken();
				}
				if ((null != key) && (null != value)) {
					resultValues.put(key, value);
				}

			}

		}
		return resultValues;
	}

	//------------------------------------------------------------------------------------------------------------------

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}


}