package com.cps.droplet;

import atg.commerce.order.*;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import com.cps.commerce.order.CPSCommerceItemImpl;

import javax.servlet.ServletException;
import java.io.IOException;

public class CPSCartCommerceItemDroplet extends DynamoServlet {

    private static final String PARAM_NAME_ORDER = "order";
    public static ParameterName ITEM_ID = ParameterName.getParameterName("itemId");

    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        Order order = (Order) req.getObjectParameter(PARAM_NAME_ORDER);
        String itemId = (String) req.getObjectParameter(ITEM_ID);
        if (itemId != null && !itemId.isEmpty()) {
            try {
                CommerceItem commerceItem = order.getCommerceItem(itemId);
                CPSCommerceItemImpl commerceItemImpl = (CPSCommerceItemImpl) order.getCommerceItem(itemId);
                req.setParameter("commerceItem", commerceItemImpl);
                req.serviceLocalParameter("output", req, res);
            } catch (CommerceItemNotFoundException | InvalidParameterException e) {
                if (isLoggingError()) {
                    logError(e);
                }
                req.serviceLocalParameter("error", req, res);
            }
        }
        req.serviceLocalParameter("empty", req, res);
    }

}
