package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.cps.userprofiling.CPSInvoiceConnectionManager;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * @author Steve Neverov
 */
public class RequestInvoiceStatus extends DynamoServlet {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/droplet/RequestInvoiceStatus";

    /**
     * Input - Invoice Id being requested
     */
    public static final ParameterName INVOICE_ID_PARAM = ParameterName.getParameterName("invoiceId");

    /**
     * Output - status.
     */
    public static final String OUTPUT_STATUS = "status";

    /**
     * Output parameter name.
     */
    private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");

    /**
     * Empty parameter name.
     */
    private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

    /**
     * Error parameter name.
     */
    private static final ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");

    /**
     * CPSInvoiceConnectionManager property
     */
    private CPSInvoiceConnectionManager mConnectionManager;

    // -------------------------------------
    // Methods
    // -------------------------------------

    /**
     * Returns the CPSInvoiceConnectionManager property
     *
     * @return
     */
    public CPSInvoiceConnectionManager getConnectionManager() {
        return mConnectionManager;
    }

    /**
     * Sets the CPSInvoiceConnectionManager property
     *
     * @param pConnectionManager
     */
    public void setConnectionManager(CPSInvoiceConnectionManager pConnectionManager) {
        mConnectionManager = pConnectionManager;
    }

    /**
     * Renders the <code>output</code>
     *
     * Renders the <code>empty</code> oparam if the map of items or values is empty.
     *
     * Renders the <code>error</code> oparam if an exception is thrown while trying to retrieve/render the credit card.
     *
     * @param pRequest
     *            a <code>DynamoHttpServletRequest</code> value
     * @param pResponse
     *            a <code>DynamoHttpServletResponse</code> value
     * @exception javax.servlet.ServletException
     *                if an error occurs
     * @exception java.io.IOException
     *                if an error occurs
     */
    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - start").toString());
        }

        try {
            // output parameter to render
            ParameterName renderParam;

            String invoiceId = (String) pRequest.getObjectParameter(INVOICE_ID_PARAM);

            if (!StringUtils.isBlank(invoiceId)) {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append("Looking for Invoice:").append(invoiceId).toString());
                }

                String status = getConnectionManager().requestInvoiceStatus(invoiceId);
                if (!StringUtils.isBlank(status)) {
                    renderParam = OUTPUT_OPARAM;
                    pRequest.setParameter(OUTPUT_STATUS, status);
                } else {
                    renderParam = EMPTY_OPARAM;
                }

            } else {
                if (isLoggingDebug()) {
                    logDebug("No invoice id to request.");
                }
                renderParam = EMPTY_OPARAM;
            }
            // render parameter back to JSP
            pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
        } catch (Exception exc) {
            if (isLoggingError()) {
                logError(exc);
            }
            pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - exit").toString());
        }
    }

}