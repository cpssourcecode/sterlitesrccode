package com.cps.droplet;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;

import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.security.CriptoUtils;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;

public class NewUserLogin extends DynamoServlet {
	public static final ParameterName TRUE = ParameterName.getParameterName("true");
	public static final ParameterName ERROR = ParameterName.getParameterName("error");
	
	private final static String PARAM_LN = "ln";
	private final static String PARAM_PR = "pr";
	private final static String PARAM_DT = "dt";
    private final static String PARAM_TP = "tp";
	
	private CPSProfileTools profileTools;
	
    // private String decryptedLogin;
    // private String decryptedProfileId;
    // private String decryptedDate;
    // private String decryptedTempPassword;
	
	private long profileDateExpiration = (86400000 * 2); // 1 day = 86400000 milliseconds
	
	private long profileFridayDateExpiration = (86400000 * 3); // 1 day = 86400000 milliseconds
	
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
			throws ServletException, IOException {
		vlogDebug("New User Login droplet");
		
		// Get url params to decrypt
		String ln = pRequest.getParameter(PARAM_LN);
		String pr = pRequest.getParameter(PARAM_PR);
		String dt = pRequest.getParameter(PARAM_DT);
        String tp = pRequest.getParameter(PARAM_TP);
		
		// Decrypt params
        String decryptedLogin = CriptoUtils.aesDecrypt(ln);
        String decryptedProfileId = CriptoUtils.aesDecrypt(pr);
        String decryptedTimeStamp = CriptoUtils.aesDecrypt(dt);

        String decryptedTempPassword = null;
        boolean userAddedByCPSAdmin = false;
        if (tp != null) {
            decryptedTempPassword = CriptoUtils.aesDecrypt(tp);
            userAddedByCPSAdmin = true;

        }
		
		if(!isCurrentUserTransient()) {
			serviceError(pRequest, pResponse, CPSErrorCodes.ERR_TEMPORARY_USER_ALREADY_LOGGED_IN);
        } else if (StringUtils.isNotBlank(decryptedProfileId) && StringUtils.isNotBlank(decryptedLogin)) {
			// If we have values, validate
			// Get profile item
            RepositoryItem profile = getProfileTools().getProfileForTemporaryPassword(decryptedProfileId, decryptedLogin);
			vlogDebug("Profile for new user login: "+profile);
			if (profile != null) {
				// User found
				boolean isGeneratedPass = (Boolean) profile.getPropertyValue(CPSConstants.GENERATED_PSWD);
				if (isGeneratedPass){
					// Password is generated, check time
					Timestamp lastPasswordUpdate = (Timestamp)profile.getPropertyValue(CPSConstants.LAST_PSWD_UPDATE);
                    if (isValidDate(lastPasswordUpdate) && !userAddedByCPSAdmin) {
						// Date is valid
                        serviceSuccess(pRequest, pResponse, decryptedLogin, decryptedTempPassword);
                    } else if (userAddedByCPSAdmin) { // dont check the time validity, since the user is added by CPS admin via BCC
                        serviceSuccess(pRequest, pResponse, decryptedLogin, decryptedTempPassword);
                    } else {
						// Date over 2 days, error
						serviceError(pRequest, pResponse, CPSErrorCodes.ERR_TEMPORARY_EXPIRED);
					}
				} else {
					// Password has already been changed
					serviceError(pRequest, pResponse, CPSErrorCodes.ERR_PSWD_CHANGED);
				}
			}
		} else {
			// No profile found, url invalid
			serviceError(pRequest, pResponse, CPSErrorCodes.ERR_TEMPORARY_INVALID_URL);
		}
	}
	
    private void serviceSuccess(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String decryptedLogin, String decryptedTempPass) {
		try {
            vlogDebug("Service success: - {0}" + decryptedLogin);
            pRequest.setParameter(CPSConstants.EMAIL, decryptedLogin);
            pRequest.setParameter(CPSConstants.TEMPORARY_PSWD, decryptedTempPass);
			pRequest.serviceLocalParameter(TRUE, pRequest, pResponse);
		} catch (Exception e) {
			vlogError(e, "Error");
		} 
	}
	
	private void serviceError (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String errorMessage) {
		try {
			vlogDebug("Service error: "+errorMessage);
			pRequest.setParameter("errorMessage", errorMessage);
			pRequest.serviceLocalParameter(ERROR, pRequest, pResponse);
		} catch (Exception e) {
			vlogError(e, "Error");
		}
	}
	
    // private void decrypt(String ln, String pr, String dt){
    // try {
    // String login = CriptoUtils.aesDecrypt(ln);
    // String profileId = CriptoUtils.aesDecrypt(pr);
    // String date = CriptoUtils.aesDecrypt(dt);
    //
    // setDecryptedLogin(new String(login));
    // setDecryptedProfileId(new String(profileId));
    // setDecryptedDate(new String(date));
    //
    // } catch (Exception e) {
    // vlogError(e, "Error");
    // }
    // }
	
	private boolean isValidDate (Timestamp pDate) {
		boolean isValid = false;
		Calendar calendar = Calendar.getInstance();

		if (pDate != null){
			calendar.setTime(pDate);
			int day = calendar.get(Calendar.DAY_OF_WEEK); 
			Date current = new Date();
			if (day == Calendar.FRIDAY) {
				isValid = !(current.getTime() - pDate.getTime() > getProfileFridayDateExpiration());
			} else {
				isValid = !(current.getTime() - pDate.getTime() > getProfileDateExpiration());
			}
		}
		vlogDebug("Is date valid? "+isValid);
		return isValid;
	}

	private boolean isCurrentUserTransient() {
		RepositoryItem profile = ServletUtil.getCurrentUserProfile();
		return profile != null && profile.isTransient();
	}
	
    // public String getDecryptedLogin() {
    // return decryptedLogin;
    // }
    //
    // public void setDecryptedLogin(String decryptedLogin) {
    // this.decryptedLogin = decryptedLogin;
    // }
    //
    // public String getDecryptedProfileId() {
    // return decryptedProfileId;
    // }
    //
    // public void setDecryptedProfileId(String decryptedProfileId) {
    // this.decryptedProfileId = decryptedProfileId;
    // }
    //
    // public String getDecryptedDate() {
    // return decryptedDate;
    // }
    //
    // public void setDecryptedDate(String decryptedDate) {
    // this.decryptedDate = decryptedDate;
    // }

	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	public long getProfileDateExpiration() {
		return profileDateExpiration;
	}

	public void setProfileDateExpiration(long profileDateExpiration) {
		this.profileDateExpiration = profileDateExpiration;
	}

	public long getProfileFridayDateExpiration() {
		return profileFridayDateExpiration;
	}

	public void setProfileFridayDateExpiration(long profileFridayDateExpiration) {
		this.profileFridayDateExpiration = profileFridayDateExpiration;
	}
	
}