package com.cps.droplet;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;

import com.cps.util.DataLayerConstants;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.json.JSONArray;
import atg.json.JSONObject;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Helper droplet to convert the data structure to JSON for Data Layer push for checkout Order Complete
 * 
 * @author vignesh
 * 
 */
public class OrderCompleteDataLayerDroplet extends DynamoServlet {
    private static final String SLASH = "/";
	private static final String DESCRIPTION = "description";
	private static final String PARENT_CATEGORIES = "parentCategories";
	private static final String DISPLAY_NAME = "displayName";
	private String affiliation;
    private static final ParameterName OUTPUT = ParameterName.getParameterName("output");
    private static final String OUTPUT_OBJECT = "outputObject";

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        JSONArray productsListJSON = new JSONArray();
        JSONObject actionFieldObj = new JSONObject();
        try {
            Order order = (Order) pRequest.getObjectParameter(DataLayerConstants.ORDER);
            if (order != null) {
                BigDecimal totalAmount = new BigDecimal(order.getPriceInfo().getAmount());
                totalAmount = totalAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
                double total = totalAmount.doubleValue();
                double estimatedTax = order.getPriceInfo().getTax();
                String orderId = order.getId();
                double shipping = order.getPriceInfo().getShipping();
                List<CommerceItem> commerceItems = order.getCommerceItems();
                if (commerceItems != null) {
                    for (CommerceItem commerceItem : commerceItems) {
                        RepositoryItem catalogRef = (RepositoryItem) commerceItem.getAuxiliaryData().getCatalogRef();
                        RepositoryItem productRef = (RepositoryItem) commerceItem.getAuxiliaryData().getProductRef();
                        Set<RepositoryItem> productCategory =  (Set<RepositoryItem>) productRef.getPropertyValue(PARENT_CATEGORIES);
                        StringBuilder subCategorys= new StringBuilder();
                        if(productCategory != null){
                            for (RepositoryItem repositoryItem : productCategory) {
                               	subCategorys.append(repositoryItem.getPropertyValue(DISPLAY_NAME));
                            	subCategorys.append(SLASH);
    						}
                        }
                        if (StringUtils.endsWith(subCategorys.toString(), SLASH)) {
                        	subCategorys.deleteCharAt(subCategorys.length() - 1);
        				}
                        String skuId = catalogRef.getRepositoryId();
                        String ProductName = (String) catalogRef.getPropertyValue(DESCRIPTION);
                        long skuQuantity = commerceItem.getQuantity();
                        String category=subCategorys.toString();
                        double skuPrice = commerceItem.getPriceInfo().getAmount();
                        BigDecimal unitPrice = BigDecimal.ZERO;
						BigDecimal quantity;
						if (commerceItem.getPriceInfo() != null) {
							unitPrice = new BigDecimal(commerceItem.getPriceInfo().getAmount());
							quantity = new BigDecimal(skuQuantity);
							unitPrice = unitPrice.divide(quantity, 2, BigDecimal.ROUND_HALF_UP);
						}
                        double price = unitPrice.doubleValue();
                        JSONObject productJsonObj = new JSONObject();
                        productJsonObj.put(DataLayerConstants.SKU, skuId);
                        productJsonObj.put(DataLayerConstants.NAME, ProductName);
                        productJsonObj.put(DataLayerConstants.CATEGORY, category);
                        productJsonObj.put(DataLayerConstants.PRICE, price);
                        productJsonObj.put(DataLayerConstants.QUANTITY, skuQuantity);
                        productsListJSON.add(productJsonObj);
                    }
                    actionFieldObj.put(DataLayerConstants.TRANSACTION_ID, orderId);
                    actionFieldObj.put(DataLayerConstants.TRANSACTION_AFFILIATION, getAffiliation());
                    actionFieldObj.put(DataLayerConstants.TRANSACTION_TOATAL, total);
                    actionFieldObj.put(DataLayerConstants.TRANSACTION_TAX, estimatedTax);
                    actionFieldObj.put(DataLayerConstants.TRANSACTION_SHIPPING, shipping);
                    actionFieldObj.put(DataLayerConstants.TRANSACTION_PRODUCTS, productsListJSON);
                }
            }

        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
        }
        pRequest.setParameter(OUTPUT_OBJECT, actionFieldObj.toString());
        pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
    }

    /**
     * @return the affiliation
     */
    public String getAffiliation() {
        return affiliation;
    }

    /**
     * @param affiliation the affiliation to set
     */
    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

}