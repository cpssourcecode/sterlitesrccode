package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.multisite.SiteWrapper;
import atg.nucleus.naming.ParameterName;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class ServicePagesDroplet extends DynamoServlet {
	
	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	
	public static final String STATIC_PAGE_TEMPLATE = "staticPageTemplate";
	public static final String SITES = "sites";
	public static final String PARENT_PAGES = "parentPages";
	public static final String SERVICE_PAGE_URI = "/services/default";
	public static final String PAGE_URL = "pageURL";
	
	private Repository mRepository;
	
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
			throws IOException, ServletException {
		vlogDebug("ServicePagesDroplet.start");

		RepositoryItem[] result = null;
		Repository repository = getRepository();
		SiteWrapper site = new SiteWrapper();
		
		if (repository != null && site != null){
			try{
				String siteId = site.getId();
				if (!StringUtils.isBlank(siteId)){
					vlogDebug("Site: "+siteId);
					RepositoryItem servicesDefaultPage = getServiceDefaultPage(siteId, repository);
					vlogDebug("servicesDefaultPage: "+servicesDefaultPage);
					if (servicesDefaultPage != null){
						RepositoryView view = repository.getView(STATIC_PAGE_TEMPLATE);
						QueryBuilder qb = view.getQueryBuilder();
						List<Query> queries = new ArrayList<Query>();
						QueryExpression valueSetExp = qb.createConstantQueryExpression(servicesDefaultPage);
						QueryExpression propExp = qb.createPropertyQueryExpression(PARENT_PAGES);
						queries.add(qb.createPatternMatchQuery(propExp, valueSetExp, QueryBuilder.CONTAINS, true));
						
						valueSetExp = qb.createConstantQueryExpression(siteId);
						propExp = qb.createPropertyQueryExpression(SITES);
						queries.add(qb.createPatternMatchQuery(propExp,  valueSetExp, QueryBuilder.CONTAINS, true));
						Query resultQuerry = qb.createAndQuery(queries.toArray(new Query[queries.size()]));
						result = view.executeQuery(resultQuerry);
						vlogDebug("Result set: "+Arrays.toString(result));
					}
					
				}
			}catch (RepositoryException e){
				if (isLoggingError()){
					logError(e);
				}
			}
		}
		
		pRequest.setParameter("pages", result);
		pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
		vlogDebug("ServicePagesDroplet.end");
	}
	
	private RepositoryItem getServiceDefaultPage(String siteId, Repository repository){
		RepositoryItem result = null;
		try{
			RepositoryView view = repository.getView(STATIC_PAGE_TEMPLATE);
			QueryBuilder qb = view.getQueryBuilder();
			List<Query> queries = new ArrayList<Query>();
			QueryExpression valueSetExp = qb.createConstantQueryExpression(SERVICE_PAGE_URI);
			QueryExpression propExp = qb.createPropertyQueryExpression(PAGE_URL);
			queries.add(qb.createPatternMatchQuery(propExp, valueSetExp, QueryBuilder.EQUALS, true));
			
			valueSetExp = qb.createConstantQueryExpression(siteId);
			propExp = qb.createPropertyQueryExpression(SITES);
			queries.add(qb.createPatternMatchQuery(propExp,  valueSetExp, QueryBuilder.CONTAINS, true));
			Query resultQuerry = qb.createAndQuery(queries.toArray(new Query[queries.size()]));
			RepositoryItem[] items = view.executeQuery(resultQuerry);
			
			if ((items != null) && (items.length > 0)){
				result = items[0];
			}
		}catch (Exception e){
			vlogError(e, "Error");
		}
		return result;
	}

	public Repository getRepository() {
		return mRepository;
	}

	public void setRepository(Repository mRepository) {
		this.mRepository = mRepository;
	}
}