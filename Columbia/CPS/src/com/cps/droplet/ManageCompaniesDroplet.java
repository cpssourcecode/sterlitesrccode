package com.cps.droplet;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;

import atg.adapter.gsa.query.Builder;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;

import com.cps.userprofiling.SortableCompanyInfo;
import com.cps.util.CPSConstants;
import com.cps.util.CPSSessionBean;

import static com.cps.util.CPSConstants.SESSION_BEAN_PATH;

public class ManageCompaniesDroplet extends DynamoServlet {

	public static final ParameterName LETTER_FILTER = ParameterName.getParameterName("letter");

	private static final double PER_PAGE = 5.0;
	private static final String ITEM_DESCRIPTOR_ORGANIZATION = "organization";
	private static final String BILLING_ADDRESS = "billingAddress";
	private static final String ACCOUNT_NUMBER = "billingAddress.jdeAddressNumber";
	private static final String DEFAULT_SORT = "companyName";
	private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
	private static final String UNDEFINED = "undefined";

	private Repository profileRepository;

	private UserDirectory userDirectory;

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("ManageCompaniesDroplet.start");

		List<SortableCompanyInfo> companyList = new ArrayList<>();
		List<RepositoryItem> cList = new ArrayList<>();
		Map<String, Boolean> letterMap = null;
		double total = 0;
		double numPages = 1;

		String searchValue = pRequest.getParameter(DropletConstants.SEARCH_TERM);
		if ("".equals(searchValue)) {
			searchValue = null;
		}
		String letterFilter = pRequest.getParameter(LETTER_FILTER);
		String sort = pRequest.getParameter(DropletConstants.SORT);
		if (StringUtils.isBlank(sort)) {
			sort = DEFAULT_SORT;
		}
		String order = pRequest.getParameter(DropletConstants.ORDER);
		int page = 1;
		try {
			String p = pRequest.getParameter(DropletConstants.PAGE);
			if (StringUtils.isNotBlank(p) && !UNDEFINED.equals(p)) {
				page = Integer.parseInt(p);
			}
		} catch (Exception e) {
			vlogError(e, "Error");
			page = 1;
		}
		long startTime = System.nanoTime();
		if (searchValue == null) {
			cList = getOrgs(letterFilter, sort, order);
		}
		long endTime = System.nanoTime();
		vlogDebug("getOrgs Method duration: " + ((endTime - startTime) / 1000000));

		CPSSessionBean sessionBean = (CPSSessionBean)pRequest.resolveName(SESSION_BEAN_PATH);
		sessionBean.setCompanies(cList);

		if (cList != null) { //&& !cList.isEmpty()
			startTime = System.nanoTime();
			if (StringUtils.isNotBlank(searchValue)) {
				cList = filterListBySearch(searchValue, letterFilter, sort, order);
				if (cList.isEmpty()) {
					pRequest.setParameter(DropletConstants.EMPTY, true);
				}
			}
			endTime = System.nanoTime();
			vlogDebug("filterListBySearch Method duration: " + ((endTime - startTime) / 1000000));

			startTime = System.nanoTime();
			if (StringUtils.isNotBlank(letterFilter)) {
				cList = filterListByLetter(letterFilter, sort, order);
			}
			endTime = System.nanoTime();
			vlogDebug("filterListByLetter Method duration: " + ((endTime - startTime) / 1000000));

			// Letter Map
			startTime = System.nanoTime();
			letterMap = generateLetterMap(cList, sessionBean);
			endTime = System.nanoTime();
			vlogDebug("generateLetterMap Method duration: " + ((endTime - startTime) / 1000000));

			// Set items on correct page to list for display
			total = cList.size();
			numPages = Math.ceil((total / PER_PAGE));

			startTime = System.nanoTime();
			companyList = getSubsetList(cList, page);
			endTime = System.nanoTime();
			vlogDebug("getSubsetList Method duration: " + ((endTime - startTime) / 1000000));
		}

		pRequest.setParameter(DropletConstants.LIST, companyList);
		pRequest.setParameter(DropletConstants.LETTER_MAP, letterMap);
		pRequest.setParameter(DropletConstants.TOTAL, (int) total);
		pRequest.setParameter(DropletConstants.NUM_PER_PAGE, (int) PER_PAGE);
		pRequest.setParameter(DropletConstants.NUM_PAGES, (int) numPages);
		pRequest.serviceLocalParameter(DropletConstants.OUTPUT, pRequest, pResponse);
	}
	
	// Get org repository items
	private List<RepositoryItem> getOrgs(String letterFilter, String fieldSort, String order) {
		List<RepositoryItem> orgs = new ArrayList<>();
		String sort = getSorting(fieldSort, order);
		try {
			String rqlQuery;
			Object params[] = new Object[1];
			if (letterFilter == null) {
				rqlQuery = "ALL ORDER BY " + sort;
			} else {
				params[0] = letterFilter;
				rqlQuery = "name STARTS WITH IGNORECASE ?0 ORDER BY " + sort;
			}

				RepositoryView rpView = getProfileRepository().getView(ITEM_DESCRIPTOR_ORGANIZATION);
				RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);

			RepositoryItem[] results = stmt.executeQuery(rpView, params);

				if (results != null && results.length > 0) {
					// Locations found
					orgs.addAll(Arrays.asList(results));
				}
			} catch (Exception e) {
				vlogError(e, "Error");
			}

		return orgs;
	}

	private List<SortableCompanyInfo> createSortableCompanies(List<SortableCompanyInfo> companyList, List<RepositoryItem> orgs) {

		for (RepositoryItem org : orgs) {
			SortableCompanyInfo company = new SortableCompanyInfo();
			RepositoryItem companyBilling = (RepositoryItem) org.getPropertyValue(BILLING_ADDRESS);
			company.setId((String) org.getPropertyValue(CPSConstants.ID));
			company.setCompanyName((String) org.getPropertyValue(CPSConstants.NAME));

			if (companyBilling != null) {
				String accountNumber = (String) companyBilling.getPropertyValue(CPSConstants.JDE_ADDRESS_NUM);
				String address1 = (String) companyBilling.getPropertyValue(CPSConstants.ADDRESS1);
				String address2 = (String) companyBilling.getPropertyValue(CPSConstants.ADDRESS2);
				String city = (String) companyBilling.getPropertyValue(CPSConstants.CITY);
				String state = (String) companyBilling.getPropertyValue(CPSConstants.STATE);
				String postalCode = (String) companyBilling.getPropertyValue(CPSConstants.POSTAL_CODE);
				if (StringUtils.isNotBlank(address1)) {
					company.setAddress1(address1);
				}
				if (StringUtils.isNotBlank(address2)) {
					company.setAddress2(address2);
				}
				if (StringUtils.isNotBlank(city)) {
					company.setCity(city);
				}
				if (StringUtils.isNotBlank(state)) {
					company.setState(state);
				}
				if (StringUtils.isNotBlank(postalCode)) {
					company.setPostalCode(postalCode);
				}
				if (StringUtils.isNotBlank(accountNumber)) {
					company.setAccountNumber(accountNumber);
				}
			}
			
			if (StringUtils.isBlank(company.getAccountNumber())) {
				company.setAccountNumber((String) org.getPropertyValue(CPSConstants.ID));
			}

			if (companyHasAdmin(org)) {
				company.setNeedsSetup(false);
			} else {
				company.setNeedsSetup(true);
			}
			companyList.add(company);
		}

		return companyList;
	}

	private boolean companyHasAdmin(RepositoryItem org) {
		boolean hasAdmin = false;
		Set<RepositoryItem> members = (Set<RepositoryItem>) org.getPropertyValue(DropletConstants.MEMBERS);
		if (members != null) {
			for (RepositoryItem member : members) {
				if (isContainUserRoleAccAdmin(member)) {
					hasAdmin = true;
					break;
				}
			}
		}
		return hasAdmin;
	}

	private boolean isContainUserRoleAccAdmin(RepositoryItem userItem) {
		User user = getUserDirectory().findUserByPrimaryKey(userItem.getRepositoryId());
		if (user != null) {
			Collection collection = user.getAssignedRoles();
			if (collection != null && !collection.isEmpty()) {
				for (Object role : collection) {
					Role assignedRole = (Role) role;
					String userRole = assignedRole.getName();
					if (CPSConstants.ROLE_ACCOUNT_ADMIN.equalsIgnoreCase(userRole)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	// Method to filter current list by entered search term, Developed to perform search on the DB side and increase performance
	private List<RepositoryItem> filterListBySearch(String searchTerm, String letterFilter, String fieldSort, String order) {
		vlogDebug("Filtering by searchTerm: " + searchTerm);
		List<RepositoryItem> filteredList = new ArrayList<>();
		String rqlQuery;
		RepositoryItem[] results;
		String sort = getSorting(fieldSort, order);

		// Object params[] = new Object[]{letterFilter, searchTerm, sort, descending};
		Object params[] = new Object[]{letterFilter, searchTerm};
		try {
			RepositoryView rpView = getProfileRepository().getView(ITEM_DESCRIPTOR_ORGANIZATION);
			if (StringUtils.isNotBlank(letterFilter)) {
				rqlQuery = "name STARTS WITH ?0 AND (name CONTAINS IGNORECASE ?1 OR billingAddress.jdeAddressNumber CONTAINS IGNORECASE ?1) ORDER BY " + sort;
			} else {
				rqlQuery = "name CONTAINS IGNORECASE ?1 OR billingAddress.jdeAddressNumber CONTAINS IGNORECASE ?1 ORDER BY " + sort;
			}
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
			results = stmt.executeQuery(rpView, params);

			if (results != null && results.length > 0) {
				// Locations found
				filteredList.addAll(Arrays.asList(results));
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}

		return filteredList;
	}

	private List<RepositoryItem> filterListByLetter(String letterFilter, String fieldSort, String order) {
		vlogDebug("Filtering by letter: " + letterFilter);
		List<RepositoryItem> filteredList = new ArrayList<>();
		String rqlQuery;
		RepositoryItem[] results;
		String sort = getSorting(fieldSort, order);
		
		Object params[] = new Object[]{ letterFilter };
		try {
			RepositoryView rpView = getProfileRepository().getView(ITEM_DESCRIPTOR_ORGANIZATION);
			rqlQuery = "name STARTS WITH ?0 ORDER BY " + sort;
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
			results = stmt.executeQuery(rpView, params);

			if (results != null && results.length > 0) {
				// Locations found
				filteredList.addAll(Arrays.asList(results));
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}

		return filteredList;
	}
	
	private String getSorting(String sort, String order) {
		String descending;
		if (DropletConstants.ORDER_DESCENDING.equals(order)) {
			descending = "DESC";
		} else {
			descending = "ASC";
		}

		switch (sort) {
			case DropletConstants.SORT_ACCOUNT_NUMBER: sort = ACCOUNT_NUMBER + " SORT " + descending;
				break;
			case DropletConstants.SORT_ADDRESS:
				sort = BILLING_ADDRESS + "." + CPSConstants.ADDRESS1 + " SORT " + descending + ", "
						+ BILLING_ADDRESS + "." + CPSConstants.ADDRESS2 + " SORT " + descending + ", "
						+ BILLING_ADDRESS + "." + CPSConstants.CITY + " SORT " + descending + ", "
						+ BILLING_ADDRESS + "." + CPSConstants.STATE + " SORT " + descending + ", "
						+ BILLING_ADDRESS + "." + CPSConstants.POSTAL_CODE + " SORT " + descending;
				break;
			default: sort = CPSConstants.NAME + " SORT " + descending;
				break;
		}
		return sort;
	}

	private Map<String, Boolean> generateLetterMap(List<RepositoryItem> companyList, CPSSessionBean pSessionBean) {
		Map<String, Boolean> letterMap = new LinkedHashMap<>();
		Map<String, Boolean> letterMap1 = pSessionBean.getLetterMap();
		List<RepositoryItem> allCompanyNames = new ArrayList<>();

		if (letterMap1 == null) {
			char[] alphabet = ALPHABET.toCharArray();

			try {
				String rqlQuery = "SELECT DISTINCT name FROM dps_organization";
				RepositoryView rpView = getProfileRepository().getView(ITEM_DESCRIPTOR_ORGANIZATION);
				Builder builder = (Builder) rpView.getQueryBuilder();
				RepositoryItem[] results = rpView.executeQuery (builder.createSqlPassthroughQuery(rqlQuery, null));
				if (results != null && results.length > 0) {
					// Locations found
					allCompanyNames.addAll(Arrays.asList(results));
				}
			} catch (Exception e) {
				vlogError(e, "Error");
			}

			for (char letter : alphabet) {
				Boolean contains = false;
				if (listContainsLetter(allCompanyNames, letter)) {
					contains = true;
				}
				letterMap.put(Character.toString(letter).toUpperCase(), contains);
			}
			pSessionBean.setLetterMap(letterMap);
		} else {
			letterMap = pSessionBean.getLetterMap();
		}
		return letterMap;
	}

	private boolean listContainsLetter(List<RepositoryItem> companyList, char letter) {
		boolean contains = false;

		for (RepositoryItem company : companyList) {
			if (StringUtils.isNotBlank(company.getRepositoryId())) {
				if ((company.getRepositoryId()).toLowerCase().startsWith(
						Character.toString(letter).toLowerCase())) {
					contains = true;
					break;
				}
			}
		}

		return contains;
	}

	private List<SortableCompanyInfo> getSubsetList(List<RepositoryItem> list, int page) {
		List<RepositoryItem> subsetList = new ArrayList<>();
		List<SortableCompanyInfo> subset = new ArrayList<>();
		int max = page * (int) PER_PAGE;
		int min = max - (int) PER_PAGE;

		// No one likes IndexOutOfBounds Exceptions
		if (max > list.size()) {
			max = list.size();
		}

		// Only include users that should display on the current page
		for (int i = min; i < max; i++) {
			subsetList.add(list.get(i));
		}

		createSortableCompanies(subset, subsetList);

		return subset;
	}

	public Repository getProfileRepository() {
		return profileRepository;
	}

	public void setProfileRepository(Repository profileRepository) {
		this.profileRepository = profileRepository;
	}

	public UserDirectory getUserDirectory() {
		return userDirectory;
	}

	public void setUserDirectory(UserDirectory userDirectory) {
		this.userDirectory = userDirectory;
	}
}