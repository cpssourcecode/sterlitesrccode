package com.cps.droplet;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import com.cps.service.OrganizationAliasService;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

public class CustomerAliasDroplet extends DynamoServlet {
	public static final ParameterName PRODUCT = ParameterName.getParameterName("product"); //sent product returns each resource (links) to display
	public static ParameterName PROFILE = ParameterName.getParameterName("profile");
	private CPSProfileTools mProfileTools;
	private OrganizationAliasService mOrganizationAliasService;

	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException,
			IOException {
		RepositoryItem theProduct = (RepositoryItem) req.getObjectParameter(PRODUCT);
		Profile profile = (Profile) ServletUtil.getCurrentRequest().resolveName("/atg/userprofiling/Profile");
		String customerAlias = "";
		String orgId = null;

		if (!profile.isTransient()) {
			RepositoryItem organization = getProfileTools().getParentOrganization(profile);
			if (organization != null) {
				orgId = organization.getRepositoryId();
			}

			if (orgId != null) {
				customerAlias = getOrganizationAliasService().findAliasByOrganizationId(theProduct, orgId);
			}

			if (customerAlias != null && !"".equals(customerAlias)) {
				req.setParameter("customerAlias", customerAlias.replaceAll(",", ", "));
				req.serviceLocalParameter("outputAlias", req, res);
			} else {
				req.serviceLocalParameter("outputEmpty", req, res);
			}
		} else {
			req.serviceLocalParameter("outputEmpty", req, res);
		}
	}


	//------------------------------------------------------------------------------------------------------------------

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public OrganizationAliasService getOrganizationAliasService() {
		return mOrganizationAliasService;
	}

	public void setOrganizationAliasService(OrganizationAliasService pOrganizationAliasService) {
		this.mOrganizationAliasService = pOrganizationAliasService;
	}


}
