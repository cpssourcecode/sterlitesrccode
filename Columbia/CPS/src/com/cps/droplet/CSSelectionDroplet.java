package com.cps.droplet;

import static com.cps.util.CPSConstants.ADDRESS1;
import static com.cps.util.CPSConstants.ADDRESS2;
import static com.cps.util.CPSConstants.ASSOCIATED_CS;
import static com.cps.util.CPSConstants.ASSOCIATED_CS_DATA;
import static com.cps.util.CPSConstants.BILLING_ACCOUNTS;
import static com.cps.util.CPSConstants.CHILD_ORGS;
import static com.cps.util.CPSConstants.CITY;
import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;
import static com.cps.util.CPSConstants.ORGANIZATION;
import static com.cps.util.CPSConstants.PARENT_ORG;
import static com.cps.util.CPSConstants.POSTAL_CODE;
import static com.cps.util.CPSConstants.SECONDARY_ADDRESSES;
import static com.cps.util.CPSConstants.STATE;

import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.userprofiling.util.UserAssociatedCB;
import com.cps.userprofiling.util.UserAssociatedCS;
import com.cps.util.CSAddress;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupImpl;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import atg.userprofiling.address.AddressTools;


public class CSSelectionDroplet extends DynamoServlet {

	private static final ParameterName SEARCH_TERM = ParameterName.getParameterName("filter");
	private static final ParameterName ORG_ID = ParameterName.getParameterName("orgId");
	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final String EMPTY_RESULT = "emptyResult";
	private static final String SHOW_BILLING_ACCOUNT = "showBillingAccount";
	
	private static final String SORTED_ORG_LISTS = "sortedOrgLists";
	private static final String ORG_LIST = "orgList";
	private static final String BILL_TOS = "billTos";

	private CPSProfileTools mProfileTools;
	private OrderManager orderManager;
	private Repository profileRepository;
	private RqlStatement profileOrderRql;
	private RqlStatement orgOrderRql;
	

	//------------------------------------------------------------------------------------------------------------------


	


	

	


	


	/**
	 * ***********************************************
	 * Return a list of contact info's do display in a
	 * cs selector.
	 * ***********************************************
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		boolean showDeletedAddress=Boolean.parseBoolean(pRequest.getParameter("showDeletedAddress"));
		boolean emptyResult = false;
		boolean showBillingAccount = false;
		List<RepositoryItem> csList = new ArrayList<RepositoryItem>();
		Object[] params=null;
		RqlStatement orderRql=null;

		
		//CLMB0206-56 CR
		Set<UserAssociatedCB> orgsList = new HashSet<UserAssociatedCB>();
		Set<RepositoryItem> billTos = new HashSet<RepositoryItem>();
		List<UserAssociatedCS> csListData = new ArrayList<UserAssociatedCS>();

		String orgId = pRequest.getParameter(ORG_ID);
			
		vlogDebug("Selected Org ID : " +orgId);

		Profile profile = null;
		String userId = pRequest.getParameter("id");
		if (StringUtils.isNotBlank(userId)) {
			try {
				profile = new Profile();
				profile.setDataSource(getProfileTools().getProfileItem(userId));
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		}
		if (profile == null || profile.getDataSource() == null) {
			profile = getProfile();
		}
		
		if (profile != null && !profile.isTransient()) {
			String profileId=profile.getRepositoryId();  
			RepositoryItem org=(RepositoryItem) profile.getPropertyValue(PARENT_ORG);
			String parentOrgId=org.getRepositoryId();
			if (StringUtils.isNotBlank(orgId)){
				getProfileTools().initAddressesDataByOrganization(profile, csListData, orgsList, billTos);
				orderRql=getOrgOrderRql();
				params=new Object[]{parentOrgId};
			} else {
				Set<RepositoryItem> billingAccounts = (Set<RepositoryItem>) profile.getPropertyValue(BILLING_ACCOUNTS);
				if (billingAccounts != null && billingAccounts.size() > 0) {
					getProfileTools().initAddressesDataByOrganization(profile, csListData, orgsList, billTos);
					orderRql=getOrgOrderRql();
					params=new Object[]{parentOrgId};
				} else {
					if (!getProfileTools().initAddressesDataByProfile(profile, csListData)) {
						getProfileTools().initAddressesDataByOrganization(profile, csListData, orgsList, billTos);
						orderRql=getOrgOrderRql();
						params=new Object[]{parentOrgId};

					}else{
						orderRql=getProfileOrderRql();
						params=new Object[]{profileId};

					}
				}
			}

			filterAddresses(csListData, pRequest);

			RepositoryItem parentOrg = (RepositoryItem) profile.getPropertyValue(PARENT_ORG);
			if (parentOrg != null) {
				Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) parentOrg.getPropertyValue(CHILD_ORGS);
				if (childOrgs != null && childOrgs.size() > 0) {
					showBillingAccount = true;
				}
			}
		}
		List<CSAddress> csAddressList=new ArrayList<CSAddress>();
		List<String> csAddrIds=new ArrayList<String>();
		Set<String> addressIds=new HashSet<String>();
		List<String> shipTos=new ArrayList<String>();
		for (UserAssociatedCS cs : csListData) {
			csList.add(cs.getAddressItem());
			CSAddress csAddress=new CSAddress();
			try {
				AddressTools.copyAddress(cs.getAddressItem(),csAddress);
				csAddress.setAddressId(cs.getAddressItem().getRepositoryId());
				csAddressList.add(csAddress);
				csAddrIds.add(cs.getAddressItem().getRepositoryId());
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<UserAssociatedCS> userOrderAddresses = new ArrayList<UserAssociatedCS>();
		
		Set<CSAddress> shipAddresses=new HashSet<CSAddress>();
		Set<String> addrIds=new HashSet<String>();
		
		if(showDeletedAddress) {
			List<CSAddress> addressList=new ArrayList<CSAddress>();
			try {
				// TODO use RepositoryUtils
				Repository orderRepository = getOrderManager().getOrderTools().getOrderRepository();
				RepositoryView orderView = orderRepository.getView(ITEM_DESCRIPTOR_ORDER);
				RepositoryItem[] orderItems= orderRql.executeQuery(orderView, params);
				if(orderItems != null) {
					for(RepositoryItem orderItem : orderItems) {
						String orderId=orderItem.getRepositoryId();
						Order order=getOrderManager().loadOrder(orderId);
						List<ShippingGroup> shippingGroups=order.getShippingGroups();
						if(shippingGroups != null) {
							ShippingGroup shipGroup=shippingGroups.get(0);
							ShippingGroupImpl sgImpl=(ShippingGroupImpl) shipGroup;
							String jdeAddress=(String) sgImpl.getPropertyValue("jdeAddressNumber");
							CSAddress csAddr=new CSAddress();
							if(shipGroup instanceof CPSHardgoodShippingGroup) {
								CPSHardgoodShippingGroup hsg = (CPSHardgoodShippingGroup) shipGroup;
								if(!csAddrIds.contains(jdeAddress) && !addrIds.contains(jdeAddress)) {
									AddressTools.copyAddress(hsg.getShippingAddress(),csAddr);
									csAddr.setAddressId(jdeAddress);
									addressList.add(csAddr);
									addrIds.add(jdeAddress);
								}
							}
						}
					}
				}
	
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError(re);
				}
			} catch (CommerceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IntrospectionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			shipAddresses.addAll(csAddressList);
			shipAddresses.addAll(addressList);
			addressIds.addAll(addrIds);
		}else{
			shipAddresses.addAll(csAddressList);
		}
		addressIds.addAll(csAddrIds);
		
		shipTos.addAll(addressIds);
		profile.setPropertyValue("csShipTos", shipTos);
		vlogDebug("CS Addresses {0}",csAddrIds);
		vlogDebug("Order addresses {0}",addrIds);
		vlogDebug("CS Addresses size {0}",csAddrIds.size());
		vlogDebug("Order addresses size {0}",addrIds.size());
		vlogDebug("Total addresses {0}",addressIds.size());

		List<CSAddress> shipAddressesList = sortAddresses(shipAddresses);
		if(orgsList.size()>0 && !orgsList.isEmpty()){
			Set<UserAssociatedCB> sortedOrgLists = new TreeSet<UserAssociatedCB>(orgsList);			
			pRequest.setParameter(SORTED_ORG_LISTS, sortedOrgLists);
		}
		
		pRequest.setParameter(ORG_LIST, orgsList);
		pRequest.setParameter(BILL_TOS, billTos);
		pRequest.setParameter(ASSOCIATED_CS, csList);
		pRequest.setParameter("associatedCSAddr", shipAddressesList);
		pRequest.setParameter(ASSOCIATED_CS_DATA, csListData);
		if (csList.isEmpty()) {
			emptyResult = true;
		}
		pRequest.setParameter(SHOW_BILLING_ACCOUNT, showBillingAccount);
		pRequest.setParameter(EMPTY_RESULT, emptyResult);
		pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
	}


	/**
	 * Sort Addresses
	 * The results should display using Alphabetical Sort Order on fields  1) Address Line1  2) City
	 *
	 * @param pAddressesList list rep items
	 * @return sorted list
	 */
	private List<CSAddress> sortAddresses(Set<CSAddress> pAddressesList) {
		Object[] addressesArray = pAddressesList.toArray();
		Arrays.sort(addressesArray, new Comparator<Object>() {
					public int compare(Object addr1, Object addr2) {
						int result = 0;

						if ((addr1 instanceof CSAddress) && (addr2 instanceof CSAddress)) {
							String addr1Address1 = (String) ((CSAddress)addr1).getAddress1();
							String addr2Address1 = (String) ((CSAddress) addr2).getAddress1();
							if ((null != addr1Address1) && (null != addr2Address1)) {
								result = addr1Address1.compareTo(addr2Address1);
							}

							if (result == 0) {
								addr1Address1 = (String) ((CSAddress) addr1).getAddress2();
								addr2Address1 = (String) ((CSAddress) addr2).getAddress2();
								if ((null != addr1Address1) && (null != addr2Address1)) {
									result = addr1Address1.compareTo(addr2Address1);
								}
							}

							if (result == 0) {
								addr1Address1 = (String) ((CSAddress) addr1).getCity();
								addr2Address1 = (String) ((CSAddress) addr2).getCity();
								if ((null != addr1Address1) && (null != addr2Address1)) {
									result = addr1Address1.compareTo(addr2Address1);
								}
							}
						}
						return result;
					}
				}
		);

		List<CSAddress> addressesResults = new ArrayList<CSAddress>();
		for (Object address : addressesArray) {
			if (address instanceof CSAddress) {
				addressesResults.add((CSAddress) address);
			}
		}

		return addressesResults;
	}


	/**
	 * filter addresses according to specified filter
	 *
	 * @param pAddresses list of addresses
	 * @param pRequest   request
	 */
	private void filterAddresses(List<UserAssociatedCS> pAddresses, DynamoHttpServletRequest pRequest) {
		String userFilter = pRequest.getParameter(SEARCH_TERM);
		String orgId = pRequest.getParameter(ORG_ID);

		if (StringUtils.isBlank(userFilter) && StringUtils.isBlank(orgId)) {
			return;
		}
		List<UserAssociatedCS> addressesToRemove = new ArrayList<UserAssociatedCS>();
		for (UserAssociatedCS address : pAddresses) {
			if (!isAddressSuitable(address.getAddressItem(), userFilter, orgId)) {
				addressesToRemove.add(address);
			}
		}
		pAddresses.removeAll(addressesToRemove);
	}

	/**
	 * https://vachiosolutions.atlassian.net/wiki/display/CPSC/Manage+Addresses
	 * Shipping Address Name, Shipping Address Line, Shipping City, Shipping State, Shipping Zipcode
	 *
	 * @param pAddress address item to check
	 * @param pFilter  filter input
	 * @param orgId - selected org id
	 * @return true if address is suitable
	 */
	private boolean isAddressSuitable(RepositoryItem pAddress, String pFilter, String orgId) {
		String address1 = (String) pAddress.getPropertyValue(ADDRESS1);
		String address2 = (String) pAddress.getPropertyValue(ADDRESS2);
		String city = (String) pAddress.getPropertyValue(CITY);
		String state = (String) pAddress.getPropertyValue(STATE);
		String postalCode = (String) pAddress.getPropertyValue(POSTAL_CODE);

		RepositoryItem orgItem = null;
		if (StringUtils.isNotBlank(orgId)) {
			try {
				orgItem = getProfileTools().getProfileRepository().getItem(orgId, ORGANIZATION);
				if (orgItem != null) {
					// RepositoryItem billAddr = (RepositoryItem)
					// orgItem.getPropertyValue(CPSConstants.BILLING_ADDRESS);
					// if(billAddr != null ){
					//
					// }
					@SuppressWarnings("unchecked")
					Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) orgItem
							.getPropertyValue(SECONDARY_ADDRESSES);
					if (secondaryAddresses != null && !secondaryAddresses.containsValue(pAddress)) {
						vlogDebug("Address does not belong to selected bill to");
						return false;
					}
				}
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		} else if (StringUtils.isBlank(pFilter)) {
			return true;
		}

		return doStringMatch(address1, pFilter) || doStringMatch(address2, pFilter) || doStringMatch(city, pFilter)
				|| doStringMatch(state, pFilter) || doStringMatch(postalCode, pFilter);
	}

	/**
	 * @param pStr1 string to check
	 * @param pStr2 filter string
	 * @return true if 1st string contains 2nd string
	 */
	private boolean doStringMatch(String pStr1, String pStr2) {
		if (pStr1 != null && pStr2 != null) {
			return pStr1.toLowerCase().contains(pStr2.trim().toLowerCase());
		}
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------

	public Profile getProfile() {
		return (Profile) ServletUtil.getCurrentUserProfile();
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
	
	
	public OrderManager getOrderManager() {
		return orderManager;
	}


	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}
	
	
	public Repository getProfileRepository() {
		return profileRepository;
	}


	public void setProfileRepository(Repository profileRepository) {
		this.profileRepository = profileRepository;
	}


	public RqlStatement getProfileOrderRql() {
		return profileOrderRql;
	}


	public void setProfileOrderRql(RqlStatement profileOrderRql) {
		this.profileOrderRql = profileOrderRql;
	}


	public RqlStatement getOrgOrderRql() {
		return orgOrderRql;
	}


	public void setOrgOrderRql(RqlStatement orgOrderRql) {
		this.orgOrderRql = orgOrderRql;
	}


	
}