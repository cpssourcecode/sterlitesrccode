package com.cps.droplet;

import static com.cps.util.CPSConstants.PRODUCT_ITEM_DESCRIPTOR;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.QueryOptions;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.SortDirective;
import atg.repository.SortDirectives;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import vsg.messages.MessagesRepositoryConstants;


public class CPSProductSpecificMessagesDroplet extends DynamoServlet {

    private static final String PARAM_PRODUCT_ID = "productId";
    private static final String PARAM_PRODUCT = "product";
    private static final String PARAM_CATEGORY_ID = "categoryId";
    private static final String PARAM_TYPE = "type";
    private static final String ALL_TYPE = "all";
    private static final String ONE_TYPE = "one";
    private static final String OPARAM_MESSAGE = "message";
    private static final String OPARAM_MESSAGES = "messages";
    private static final String OUTPUT_OPARAM = "output";
    private static final String ERROR_OPARAM = "error";
    private static final String SEAMLESS_FLAG = "seamless_pipe_flag";
    private static final String SUBS_BRAND_PROPERTY = "substituteBrandMessage";


    private Repository mProductCatalog;
    private List<String> mDefaultCategories;
    private Repository mMessagesRepository;
    private CPSCatalogTools catalogTools;

    /**
     * @return the catalogTools
     */
    public CPSCatalogTools getCatalogTools() {
        return catalogTools;
    }

    /**
     * @param catalogTools
     *            the catalogTools to set
     */
    public void setCatalogTools(CPSCatalogTools catalogTools) {
        this.catalogTools = catalogTools;
    }

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        String pProductId = pRequest.getParameter(PARAM_PRODUCT_ID);
        String pCategoryId = pRequest.getParameter(PARAM_CATEGORY_ID);
        RepositoryItem product = (RepositoryItem) pRequest.getObjectParameter(PARAM_PRODUCT);
        if (StringUtils.isNotBlank(pProductId) && product == null) {
            try {
                product = getProductCatalog().getItem(pProductId, PRODUCT_ITEM_DESCRIPTOR);
            } catch (RepositoryException re) {
                logError(re);
            }
        }
        if (product != null) {
            String type = pRequest.getParameter(PARAM_TYPE);
            if (ALL_TYPE.equals(type)) {
                processMessagesForAll(pRequest, pResponse, product, pCategoryId);
            } else {
                processMessagesForOne(pRequest, pResponse, product, pCategoryId);
            }
        } else {
            pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
        }
    }

    private void processMessagesForAll(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
                                       RepositoryItem pProduct, String pCategoryId) throws ServletException, IOException {
        List<String> messages = new ArrayList<>();
        addDefaultMessages(messages,pProduct,pCategoryId);
        RepositoryItem[] items = getSortedMessages(messages);
        pRequest.setParameter(OPARAM_MESSAGES, items);
        pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
    }

    private void processMessagesForOne(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
                                       RepositoryItem pProduct, String pCategoryId) throws ServletException, IOException {
        List<String> messages = new ArrayList<>();
        addDefaultMessages(messages,pProduct,pCategoryId);
        if(isObsolete(pProduct)){
            messages.add(CPSConstants.OBSOLETE_INFO_MESSAGE);
        }
        RepositoryItem substituteBrandMessage = (RepositoryItem) pProduct.getPropertyValue(SUBS_BRAND_PROPERTY);
        if(substituteBrandMessage != null){
            String key = (String) substituteBrandMessage.getPropertyValue("keyValue");
            if(key != null){
                messages.add(key);
            }
        }
        RepositoryItem[] items = getSortedMessages(messages);
        if(items != null && items.length > 0 ) {
            String message = (String) items[0].getPropertyValue("message");
            pRequest.setParameter(OPARAM_MESSAGE, message);
        }
        pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
    }

    private void addDefaultMessages(List<String> pMessages, RepositoryItem pProduct, String pCategoryId){
        boolean isSeamless = (boolean) pProduct.getPropertyValue(SEAMLESS_FLAG);
        if (isSeamless) {
            pMessages.add(CPSConstants.SEAMLESS_PIPE_INFO_MESSAGE);
        }

        boolean founded;
        if (StringUtils.isNotBlank(pCategoryId)) {
            founded = getCatalogTools().isProductInCategory(pProduct, pCategoryId);
        } else {
            founded = getCatalogTools().isProductInCategories(pProduct, getDefaultCategories());
        }
        if (founded) {
            pMessages.add(CPSConstants.CUT_TO_ORDER_INFO_MESSAGE);
        }
    }

    private RepositoryItem[] getSortedMessages(List<String> pMessageKeys) {
        RepositoryItem[] result = null;
        try {
            RepositoryView view = getMessagesRepository().getView(MessagesRepositoryConstants.ITEM_DESCR_PRODUCT_SPECIFIC_MESSAGE);
            final QueryBuilder queryBuilder = view.getQueryBuilder();
            final QueryExpression propertyExpression = queryBuilder.createPropertyQueryExpression("keyValue");
            final QueryExpression valueExpression = queryBuilder.createConstantQueryExpression(pMessageKeys);
            final Query employeeQuery = queryBuilder.createIncludesQuery(valueExpression, propertyExpression);
            SortDirectives sortDirectives = new SortDirectives();
            sortDirectives.addDirective(new SortDirective("priority", SortDirective.DIR_ASCENDING));
            QueryOptions queryOptions = new QueryOptions(-1, -1, sortDirectives, null);
            result = view.executeQuery(employeeQuery,queryOptions);
        } catch (RepositoryException e) {
            logError(e);
        }
        return result;
    }


    private boolean isObsolete(RepositoryItem pProduct){
        String productStockingCode = (String) pProduct.getPropertyValue(CPSConstants.STOCKING_TYPE);
        return CPSConstants.STOCKING_TYPE_OBSOLETE.equals(productStockingCode)
                || CPSConstants.STOCKING_TYPE_USEUP.equals(productStockingCode)
                || CPSConstants.STOCKING_TYPE_K.equals(productStockingCode)
                || CPSConstants.STOCKING_TYPE_X.equals(productStockingCode)
                || CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR.equals(productStockingCode);
    }


    public List<String> getDefaultCategories() {
        return mDefaultCategories;
    }

    public void setDefaultCategories(List<String> pDefaultCategories) {
        mDefaultCategories = pDefaultCategories;
    }

    public Repository getProductCatalog() {
        return mProductCatalog;
    }

    public void setProductCatalog(Repository pProductCatalog) {
        mProductCatalog = pProductCatalog;
    }

    public Repository getMessagesRepository() {
        return mMessagesRepository;
    }

    public void setMessagesRepository(Repository pMessagesRepository) {
        mMessagesRepository = pMessagesRepository;
    }

}
