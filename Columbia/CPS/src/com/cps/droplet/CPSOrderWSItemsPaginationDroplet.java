package com.cps.droplet;

import atg.core.util.StringUtils;
import atg.droplet.ForEach;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.integrations.CPSOrderDetailBean;
import com.cps.integrations.CPSOrderLineDetailBean;
import com.cps.util.CPSSessionBean;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static com.cps.util.CPSConstants.SESSION_BEAN_PATH;

public class CPSOrderWSItemsPaginationDroplet extends ForEach {

	private static final String PARAM_PAGE_NUMBER = "pageNumber";
	private static final String PARAM_ORDER_ID = "orderId";

    private int mItemsPerPage;

	public int getItemsPerPage() {
		return mItemsPerPage;
	}

	public void setItemsPerPage(int pItemsPerPage) {
		mItemsPerPage = pItemsPerPage;
	}

    public int extractPageNumber(DynamoHttpServletRequest pReq) {
        String pageNumber = pReq.getParameter(PARAM_PAGE_NUMBER);
        try {
            return StringUtils.isNotBlank(pageNumber) ? Integer.parseInt(pageNumber) : 1;
        } catch(NumberFormatException ex) {
            return 1;
        }
    }

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		CPSOrderDetailBean orderBean = null;
		String orderId = pRequest.getParameter(PARAM_ORDER_ID);
		if (!StringUtils.isBlank(orderId)) {
			CPSSessionBean sessionBean = (CPSSessionBean)pRequest.resolveName(SESSION_BEAN_PATH);
			if (sessionBean != null) {
				orderBean = sessionBean.getOrderDetailBeans().get(orderId);
			}
		}

		if (orderBean != null) {
			int commerceItemsCount = orderBean.getNumberOfItems() == null ? 0 : orderBean.getNumberOfItems();
			int pageCount = (int) Math.ceil((double)commerceItemsCount/getItemsPerPage());
			pRequest.setParameter("commerceItemsCount", commerceItemsCount);
			pRequest.setParameter("itemsPerPage", getItemsPerPage());
			pRequest.setParameter("pageCount", pageCount);

			if (commerceItemsCount == 0) {
				pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
			} else {
				int pageNumber = extractPageNumber(pRequest);
				int startItemIndex = getItemsPerPage() * (pageNumber-1);
				int endItemIndex = startItemIndex - 1 + getItemsPerPage();
				List<CPSOrderLineDetailBean> items = new ArrayList<CPSOrderLineDetailBean>();
				ListIterator<CPSOrderLineDetailBean> listIterator = orderBean.getLineDetailBeans().listIterator(startItemIndex);
				for (int count = startItemIndex; listIterator.hasNext() && count <= endItemIndex; ++count) {
					items.add(listIterator.next());
				}
				setElementParameter(pRequest, "items", items);
				pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
			}
        }

    }

}
