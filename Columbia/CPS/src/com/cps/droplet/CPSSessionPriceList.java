package com.cps.droplet;

import java.util.HashMap;

import atg.nucleus.GenericService;

/**
 * CPSSessionPriceList
 *
 * <h4>Description</h4> This session object will hold a map of skuIds to prices for use in the price availability view and the cart.
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class CPSSessionPriceList extends GenericService {

    /**
     * Values map.
     */
    private HashMap mValues;

    /**
     * A map containing price list session values.
     * 
     * @return values.
     */
    public HashMap getValues() {
        if (mValues == null) {
            mValues = new HashMap();
        }
        return mValues;
    }

    /**
     * @param pValues
     */
    public void setValues(HashMap pValues) {
        mValues = pValues;
    }
}
