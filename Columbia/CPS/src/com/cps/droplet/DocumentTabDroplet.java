package com.cps.droplet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class DocumentTabDroplet extends DynamoServlet {
	public static final ParameterName PRODUCT = ParameterName.getParameterName("product"); //sent product returns each resource (links) to display
	public static final ParameterName HIDE_TAB_TEST=ParameterName.getParameterName("hideTabTest"); //use droplet to hide tab if this is set and nothing else
	public static final String MFR_CAT_NUM="mfr_cat_num"; //property that must be populated for TRADESERVICE DATA TO EXIST, otherwise don't even bother looking for it
	
	//TRADESERVICE PROPERTIES
	//public static final String CATALOG_PAGE_URL="catalog_page_url";
	public static final String MSDS_SHEET_URL="msds_sheet_url";
	public static final String INSTALL_INSTRUCTION_URL="install_instruction_url";
	public static final String SPECIFICATION_SHEET_URL="specification_sheet_url";
	
	//TRADESERVICE KEYS FOR WebAppResources
	//public static final String TS_CATALOG_PAGE_URL="Catalog Page";//"ts.catalog.page.url";
	public static final String TS_MSDS_SHEET_URL="MSDS SHEET"; //"ts.msds.sheet.url";
	public static final String TS_INSTALL_INSTRUCTION_URL="Installation Instruction"; //ts.install.instruction.url";
	public static final String TS_SPECIFICATION_SHEET_URL="Specification Sheet"; //ts.specification.sheet.url";
	
	public static final String ITEM_DOCUMENT_NAME_1="item_document_name_1";
	public static final String ITEM_DOCUMENT_NAME_2="item_document_name_2";
	public static final String ITEM_DOCUMENT_NAME_3="item_document_name_3";
	public static final String ITEM_DOCUMENT_NAME_4="item_document_name_4";
	public static final String ITEM_DOCUMENT_NAME_5="item_document_name_5";
	public static final String ITEM_DOCUMENT_NAME_6="item_document_name_6";
	private String unilogBaseUrl="";


	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException,
			IOException {
		RepositoryItem theProduct = (RepositoryItem) req.getObjectParameter(PRODUCT);
		Map<String,String> docs=new HashMap<String,String>(); //key,value = property name,property value
		if(theProduct.getPropertyValue(MFR_CAT_NUM)!=null && !((String)(theProduct.getPropertyValue(MFR_CAT_NUM))).isEmpty()){
			//Use Tradeservice Data
			String msds=(String)theProduct.getPropertyValue(MSDS_SHEET_URL);
			String install=(String)theProduct.getPropertyValue(INSTALL_INSTRUCTION_URL);
			String spec=(String)theProduct.getPropertyValue(SPECIFICATION_SHEET_URL);
			if(msds!=null){docs.put(TS_MSDS_SHEET_URL, msds);}
			if(install!=null){docs.put(TS_INSTALL_INSTRUCTION_URL, install);}
			if(spec!=null){docs.put(TS_SPECIFICATION_SHEET_URL, spec);}
		}
		else{
			//Use Unilog Data
			String name1=(String)theProduct.getPropertyValue(ITEM_DOCUMENT_NAME_1);
			String name2=(String)theProduct.getPropertyValue(ITEM_DOCUMENT_NAME_2);
			String name3=(String)theProduct.getPropertyValue(ITEM_DOCUMENT_NAME_3);
			String name4=(String)theProduct.getPropertyValue(ITEM_DOCUMENT_NAME_4);
			String name5=(String)theProduct.getPropertyValue(ITEM_DOCUMENT_NAME_5);
			String name6=(String)theProduct.getPropertyValue(ITEM_DOCUMENT_NAME_6);
			if(name1!=null){docs.put(name1, unilogBaseUrl+name1);}
			if(name2!=null){docs.put(name2, unilogBaseUrl+name2);}
			if(name3!=null){docs.put(name3, unilogBaseUrl+name3);}
			if(name4!=null){docs.put(name4, unilogBaseUrl+name4);}
			if(name5!=null){docs.put(name5, unilogBaseUrl+name5);}
			if(name6!=null){docs.put(name6, unilogBaseUrl+name6);}
		}
		String hideTabTest= (String) req.getObjectParameter(HIDE_TAB_TEST);
		if(hideTabTest!=null && !hideTabTest.isEmpty()){
			if(docs.keySet().size()>0){
				req.serviceLocalParameter("outputTabHeader",req,res);
			}
			//else hide tab
		}
		else if(docs.keySet().size()>0){
			//show tab
			req.serviceLocalParameter("openDocumentTab",req,res);

		for(String docName: docs.keySet()){
			req.setParameter("pdfUrl",docs.get(docName));
			req.setParameter("docName",docName);
			req.serviceLocalParameter("outputDocument",req,res);
		}
			//not empty, output close documentTab
			req.serviceLocalParameter("closeDocumentTab",req,res);
		}
		/**String contentId=(String)theContent.getPropertyValue("id");
		//ET,HT,EU - _new window
		//ED, HD - stay on page (Direct DL)
		// HU - redirect in same window
		if(contentId.startsWith("ET") || contentId.startsWith("HT") || contentId.startsWith("EU")){
			req.setParameter("targetValue","_new" );
			req.serviceLocalParameter("outputResource", req, res);
		}
		else if(contentId.startsWith("ED") || contentId.startsWith("HD")|| contentId.startsWith("HU")){
			req.setParameter("targetValue","_blank"); //should open blank new window but closes if it is a download link
			req.serviceLocalParameter("outputResource",req,res);
			
		}
		else{
			//HU redirect in same window
			req.setParameter("targetValue","_new");
			req.serviceLocalParameter("outputResource",req,res);
		}**/
	}


	public String getUnilogBaseUrl() {
		return unilogBaseUrl;
	}


	public void setUnilogBaseUrl(String unilogBaseUrl) {
		this.unilogBaseUrl = unilogBaseUrl;
	}
}
