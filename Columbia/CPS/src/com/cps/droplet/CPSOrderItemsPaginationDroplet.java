package com.cps.droplet;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.core.util.StringUtils;
import atg.droplet.ForEach;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class CPSOrderItemsPaginationDroplet extends ForEach {

    private OrderManager orderManager;
    private int itemsPerPage;

    private static final String PARAM_PAGE_NUMBER = "pageNumber";
    private static final String PARAM_ORDER_ID = "orderId";

    public int extractPageNumber(DynamoHttpServletRequest pReq) {
        String pageNumber = pReq.getParameter(PARAM_PAGE_NUMBER);
        try {
            return StringUtils.isNotBlank(pageNumber) ? Integer.parseInt(pageNumber) : 1;
        } catch(NumberFormatException ex) {
            return 1;
        }
    }

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        String orderId = pRequest.getParameter(PARAM_ORDER_ID);
        if (StringUtils.isNotBlank(orderId)) {
            OrderManager orderManager = getOrderManager();
            try {
                if(orderManager.orderExists(orderId)) {
                    Order order = orderManager.loadOrder(orderId);
                    int commerceItemsCount = order.getCommerceItemCount();
                    int pageCount = (int) Math.ceil((double)commerceItemsCount/getItemsPerPage());
                    pRequest.setParameter("commerceItemsCount", commerceItemsCount);
                    pRequest.setParameter("itemsPerPage", getItemsPerPage());
                    pRequest.setParameter("pageCount", pageCount);

                    if(commerceItemsCount == 0) {
                        pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
                    } else {
//                        pRequest.serviceLocalParameter(OUTPUT_START, pRequest, pResponse);
                        int pageNumber = extractPageNumber(pRequest);
                        int startItemIndex = getItemsPerPage() * (pageNumber-1);
                        int endItemIndex = startItemIndex - 1 + getItemsPerPage();
                        List<CommerceItem> items = new ArrayList<CommerceItem>();
                        ListIterator<CommerceItem> listIterator = order.getCommerceItems().listIterator(startItemIndex);
                        for(int count = startItemIndex; listIterator.hasNext() && count <= endItemIndex; ++count) {
                            items.add(listIterator.next());
                        }
                        this.setElementParameter(pRequest, "items", items);
                        pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
//                        pRequest.serviceLocalParameter(OUTPUT_END, pRequest, pResponse);
                    }
                }
            } catch (CommerceException e) {
                vlogError(e, "Error");
            }
        }

    }









    public OrderManager getOrderManager() {
        return orderManager;
    }

    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

}
