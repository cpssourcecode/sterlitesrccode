package com.cps.droplet;

import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import atg.userprofiling.address.AddressTools;
import com.cps.core.util.CPSContactInfo;
import com.cps.reorders.model.AutoOrder;
import com.cps.reorders.model.AutoOrderItem;
import com.cps.service.CPSExternalProductService;

import javax.servlet.ServletException;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.*;

public class CPSActiveAutoOrdersDroplet extends DynamoServlet {
    private static ParameterName PROFILE = ParameterName.getParameterName("profile");
    private static ParameterName LOAD_LIST = ParameterName.getParameterName("loadList");
    private static ParameterName OUTPUT = ParameterName.getParameterName("output");

    private static final String ACTIVE_AUTO_ORDERS = "activeAutoOrders";
    private static final String AUTO_ORDER = "autoOrder";
    private static final String AUTO_ORDER_LIST = "autoOrderList";
    private static final String CONTACT_INFO = "contactInfo";
    private static final String ENABLED = "enabled";
    private static final String HAS_AUTO_ORDERS = "hasAutoOrders";
    private static final String ID = "id";

    private static final String RQL_AUTO_ORDERS_BY_USER_ID = "submitterId = ?0";
    private static final String RQL_CONTACT_INFO_BY_ID = "id = ?0";

    private CPSExternalProductService mExternalProductService;

    private Repository mOrderRepository;
    private Repository mProfileRepository;

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        Profile profile = (Profile) pRequest.getObjectParameter(PROFILE);

        int activeAutoOrders = 0;
        boolean hasAutoOrders = false;
        if (profile != null) {
            String userId = (String) profile.getPropertyValue(ID);

            try {
                RepositoryView repositoryView = getOrderRepository().getView(AUTO_ORDER);
                RqlStatement statement = RqlStatement.parseRqlStatement(RQL_AUTO_ORDERS_BY_USER_ID);
                Object params[] = {userId};
                RepositoryItem[] items = statement.executeQuery(repositoryView, params);

                if (items != null && items.length > 0) {
                    activeAutoOrders = getActiveAutoOrdersCount(items);
                    hasAutoOrders = true;

                    Boolean isListRequested = Boolean.valueOf((String) pRequest.getObjectParameter(LOAD_LIST));
                    if (isListRequested) {
                        List<AutoOrder> autoOrderList = getAutoOrderList(items);
                        pRequest.setParameter(AUTO_ORDER_LIST, autoOrderList);
                    }
                }
            } catch (RepositoryException e) {
                vlogError(e, "Repository exception during querying Auto Orders by User ID.");
            }
        }

        pRequest.setParameter(ACTIVE_AUTO_ORDERS, activeAutoOrders);
        pRequest.setParameter(HAS_AUTO_ORDERS, hasAutoOrders);
        pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
    }

    private int getActiveAutoOrdersCount(RepositoryItem[] items) {
        int activeAutoOrders = 0;
        for (RepositoryItem item : items) {
            if ((Boolean) item.getPropertyValue(ENABLED)) {
                activeAutoOrders++;
            }
        }
        return activeAutoOrders;
    }

    private List<AutoOrder> getAutoOrderList(RepositoryItem[] items) {
        List<AutoOrder> autoOrderList = new ArrayList<>();
        for (RepositoryItem item : items) {
            AutoOrder autoOrder = new AutoOrder(item);
            if (autoOrder.isEnabled()) {
            	CPSContactInfo contactInfo = getAutoOrderShippingAddress(autoOrder);

                if (contactInfo != null) {
                    autoOrder.setShipToAddress(contactInfo.getAddress1());

                    double currentTotalAmount = priceAutoOrderItem(autoOrder, contactInfo.getJdeAddressNumber(), contactInfo.getBusinessUnit());
                    autoOrder.setCurrentTotalAmount(currentTotalAmount);
                }

                autoOrderList.add(autoOrder);
            }
        }
        return autoOrderList;
    }

    //TODO this method can be REMOVED
    /**
     * @deprecated
     * @param autoOrder
     * @return
     */
    private CPSContactInfo getAutoOrderContactInfo(AutoOrder autoOrder) {
        try {
            String jdeAddressNumber = autoOrder.getShippingInfo().getJDEAddressNumber();

            RepositoryView repositoryView = getProfileRepository().getView(CONTACT_INFO);
            RqlStatement statement = RqlStatement.parseRqlStatement(RQL_CONTACT_INFO_BY_ID);
            RepositoryItem[] items = statement.executeQuery(repositoryView, new Object[]{jdeAddressNumber});

            CPSContactInfo address = new CPSContactInfo();
            if (items != null && items.length > 0) {
                AddressTools.copyAddress(items[0], address);
            }

            return address;
        } catch (RepositoryException | IntrospectionException e) {
            vlogError(e, "An exception occurred during getting Ship To Address from Auto Order.");
        }

        return null;
    }
    private CPSContactInfo getAutoOrderShippingAddress(AutoOrder autoOrder) {
        try {
            String jdeAddressNumber = autoOrder.getShippingInfo().getJDEAddressNumber();

            RepositoryView repositoryView = getOrderRepository().getView("autoOrderAddress");
            RqlStatement statement = RqlStatement.parseRqlStatement("jdeAddressNumber = ?0");
            Object params[] = {jdeAddressNumber};
            RepositoryItem[] items = statement.executeQuery(repositoryView, params);

            CPSContactInfo address = new CPSContactInfo();
            if (items != null && items.length > 0) {
                AddressTools.copyAddress(items[0], address);
            }

            return address;
        } catch (RepositoryException | IntrospectionException e) {
            vlogError(e, "An exception occurred during getting Ship To Address from Auto Order.");
        }

        return null;
    }

    private Map<String, Long> getProductsMapFromAutoOrder(AutoOrder autoOrder) {
        List<AutoOrderItem> autoOrderItems = autoOrder.getItems();

        Map<String, Long> products = new HashMap<>();
        for (AutoOrderItem autoOrderItem : autoOrderItems) {
            products.put(autoOrderItem.getProductId(), autoOrderItem.getQuantity());
        }

        return products;
    }


    private double priceAutoOrderItem(AutoOrder autoOrder, String jdeAddressNumber, String businessUnit) {
        Map<String, Long> autoOrderMap = getProductsMapFromAutoOrder(autoOrder);
        Set<String> products = autoOrderMap.keySet();
        Map<String, Double> itemPrices = mExternalProductService.getCustomerItemPrice(products, jdeAddressNumber, businessUnit);

        double currentTotalAmount = 0;
        if (itemPrices != null) {
            Set<String> keySet = itemPrices.keySet();
            for (String key : keySet) {
                Double itemPrice = itemPrices.get(key);
                Long quantity = autoOrderMap.get(key);

                currentTotalAmount += itemPrice * quantity;
            }
        }

        return currentTotalAmount;
    }

    public void setExternalProductService(CPSExternalProductService pExternalProductService) {
        mExternalProductService = pExternalProductService;
    }

    public CPSExternalProductService getExternalProductService() {
        return mExternalProductService;
    }

    public Repository getOrderRepository() {
        return mOrderRepository;
    }

    public void setOrderRepository(Repository pOrderRepository) {
        mOrderRepository = pOrderRepository;
    }

    public void setProfileRepository(Repository pProfileRepository) {
        mProfileRepository = pProfileRepository;
    }

    public Repository getProfileRepository() {
        return mProfileRepository;
    }
}
