package com.cps.droplet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * This droplet returns list of stores
 * @author Jason Robert Juvingo
 * @version 1.0
 * 
 * Usage Example:
 * 
 *   <dsp:droplet name="StoreLookupDroplet">
 *   
 *    <dsp:param name="storeId" value="${storeId}"/>
 *    
 *    OR
 *    
 *    <dsp:param name="minutes" value="30"/>
 *   
 *    OR
 *    
 *    //No param
 *    
 *    <dsp:oparam name="output">
 *    ...
 *    </dsp:oparam>
 *    <dsp:oparam name="empty">
 *    ...
 *    </dsp:oparam>
 *  </dsp:droplet>
 */
public class StoreLookupDroplet extends DynamoServlet {	
	
	/**
	 * Stores Id to lookup.
	 */
	private final static String STORE_ID = "storeId";
	
	/**
	 * Time param
	 */
	private final static String TIME = "time";
	
	/**
	 * Stores Repository
	 */
	private Repository mStoresRepository;
	
	/**
	 * item descriptor for rewards redemption levels.
	 */
	public static final String ITEM_DESCRIPTOR_STORES = "stores";
	
	public static final String ID = "id";
	public static final String AREA = "area";
	public static final String ADDRESS = "address";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String ZIP = "zip";
	public static final String PHONE = "phone";
	public static final String OPEN_TIME = "openTime";
	public static final String CLOSE_TIME = "closeTime";
	public static final String TIMEZONE = "timezone";
	public static final String DESCRIPTION = "description";
	public static final String IMAGE = "store_images";
	public static final String LAT = "lat";
	public static final String LNG = "lng";
	
	
	/**
	 * Empty parameter name.
	 */
	private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");
	
	/**
	 * @return
	 */
	public Repository getStoresRepository() {
		return mStoresRepository;
	}

	/**
	 * 
	 * @param pStoresRepository
	 */
	public void setStoresRepository(Repository pStoresRepository) {
		this.mStoresRepository = pStoresRepository;
	}

	/**
	 * Output parameter name.
	 */
	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	
	@Override
	/**
	 * Service call to load store data.  Three possible outcomes based on inputs.
	 * @param pRequest - request object.
	 * @param pResponse - response object.
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) { logDebug("StoreLookupDroplet() invoked -------------->"); }
		
		ParameterName renderParam = EMPTY_OPARAM;
		ArrayList<Map<String, Object>> dataMap = new ArrayList<Map<String, Object>>();
		String storeId = pRequest.getParameter(STORE_ID);
		String time = pRequest.getParameter(TIME);
		
		// store is passed and no time parameter.  This is a single store lookup on the repository.
		if ( storeId != null 
				&& storeId.length() != 0 
				&& time == null
				) { // Return Specific Store
			if (isLoggingDebug()) { logDebug("Looking up a single store: <" + storeId + ">" ); }
			
			try {
				String rqlQuery = "ID = " + storeId;
				RepositoryView rpView = getStoresRepository().getView(StoreLookupDroplet.ITEM_DESCRIPTOR_STORES);
				RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
				RepositoryItem[] results = stmt.executeQuery(rpView, null);
				if ( results == null || results.length == 0 ) {
					if (isLoggingDebug()) { logDebug("Store does not Exist"); }
					renderParam = EMPTY_OPARAM;
				} else {
					if (isLoggingDebug()) {
						logDebug(Arrays.toString(results));
					}
					dataMap = loadMap(results);
					if (isLoggingDebug()) { logDebug(new StringBuilder().append("dataMap: ").append(dataMap.toString()).toString()); }
					pRequest.setParameter("element", dataMap);
					renderParam = OUTPUT_OPARAM;
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		
		// store is passed and time paramter is passed.  Returns a breakdown of the store times.
		} else if ( storeId != null 
						&& storeId.length() != 0 
						&& time != null 
						&& time.length() != 0 ) {
			if (isLoggingDebug()) { logDebug("Getting store: <" + storeId + ">'s Time Breakdown" ); }
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm a"); 
			
			int timeIncrement = Integer.parseInt(time);
			
			try {
				ArrayList data = new ArrayList();
				String rqlQuery = "ID = " + storeId;
				String timezone = null;
				RepositoryView rpView = getStoresRepository().getView(StoreLookupDroplet.ITEM_DESCRIPTOR_STORES);
				RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
				RepositoryItem[] results = stmt.executeQuery(rpView, null);
				if ( results == null || results.length == 0 ) {
					if (isLoggingDebug()) { logDebug("Store does not Exist"); }
					renderParam = EMPTY_OPARAM;
				} else {
					if (isLoggingDebug()) {
						logDebug(Arrays.toString(results));
					}
					for ( RepositoryItem item : results ) {
						
						timezone = (String) item.getPropertyValue(StoreLookupDroplet.TIMEZONE);
						
						Calendar openCal = Calendar.getInstance();
						openCal.setTime((Date) item.getPropertyValue(StoreLookupDroplet.OPEN_TIME));
						Calendar closeCal = Calendar.getInstance();
						closeCal.setTime((Date) item.getPropertyValue(StoreLookupDroplet.CLOSE_TIME));
						do {
							String startTime = simpleDateFormat.format(openCal.getTime());
							openCal.add(Calendar.MINUTE, timeIncrement);
							String endTime = simpleDateFormat.format(openCal.getTime());
							data.add(new String(startTime + "-" + endTime ));
						} while (openCal.before(closeCal));
					}
					if (isLoggingDebug()) { logDebug(new StringBuilder().append("data: ").append(data.toString()).toString()); }
					pRequest.setParameter("element", data);
					pRequest.setParameter("timezone", timezone);
					renderParam = OUTPUT_OPARAM;	
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		
			// Returns all stores.
		} else { 
			if (isLoggingDebug()) { logDebug("Returning all stores!"); }
			
			try {
				String rqlQuery = "ALL ORDER BY area SORT ASC, state SORT ASC, city SORT ASC";
				RepositoryView rpView = getStoresRepository().getView(StoreLookupDroplet.ITEM_DESCRIPTOR_STORES);
				RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);

				RepositoryItem[] results = stmt.executeQuery(rpView, null);

				if ( results == null || results.length == 0 ) {
					if (isLoggingDebug()) { logDebug("No Stores available"); }
					renderParam = EMPTY_OPARAM;
				} else {
					if (isLoggingDebug()) {
						logDebug(Arrays.toString(results));
					}
					dataMap = loadMap(results);
					if (isLoggingDebug()) { logDebug(new StringBuilder().append("dataMap: ").append(dataMap.toString()).toString()); }
					pRequest.setParameter("element", dataMap);
					renderParam = OUTPUT_OPARAM;
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
		
		
		
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
		if (isLoggingDebug()) { logDebug("StoreLookupDroplet() complete -------------->"); }
	}
	
	private ArrayList<Map<String, Object>> loadMap(RepositoryItem[] results) {
		ArrayList<Map<String, Object>> dataMap = new ArrayList<Map<String, Object>>();
		for ( RepositoryItem item : results ) {
			Map<String, Object> data = new HashMap<String, Object>();
			data.put(StoreLookupDroplet.ID,item.getPropertyValue(StoreLookupDroplet.ID));
			data.put(StoreLookupDroplet.AREA,item.getPropertyValue(StoreLookupDroplet.AREA));
			data.put(StoreLookupDroplet.ADDRESS,item.getPropertyValue(StoreLookupDroplet.ADDRESS));
			data.put(StoreLookupDroplet.CITY,item.getPropertyValue(StoreLookupDroplet.CITY));
			data.put(StoreLookupDroplet.STATE,item.getPropertyValue(StoreLookupDroplet.STATE));
			data.put(StoreLookupDroplet.ZIP,item.getPropertyValue(StoreLookupDroplet.ZIP));
			data.put(StoreLookupDroplet.PHONE,item.getPropertyValue(StoreLookupDroplet.PHONE));
			data.put(StoreLookupDroplet.OPEN_TIME,item.getPropertyValue(StoreLookupDroplet.OPEN_TIME));
			data.put(StoreLookupDroplet.CLOSE_TIME,item.getPropertyValue(StoreLookupDroplet.CLOSE_TIME));
			data.put(StoreLookupDroplet.TIMEZONE,item.getPropertyValue(StoreLookupDroplet.TIMEZONE));
			data.put(StoreLookupDroplet.DESCRIPTION,item.getPropertyValue(StoreLookupDroplet.DESCRIPTION));
			data.put(StoreLookupDroplet.IMAGE,item.getPropertyValue(StoreLookupDroplet.IMAGE));
			data.put(StoreLookupDroplet.LAT,item.getPropertyValue(StoreLookupDroplet.LAT));
			data.put(StoreLookupDroplet.LNG,item.getPropertyValue(StoreLookupDroplet.LNG));
			dataMap.add(data);
		}
		return dataMap;
	}
}
