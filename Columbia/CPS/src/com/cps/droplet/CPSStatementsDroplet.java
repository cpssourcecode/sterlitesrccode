package com.cps.droplet;

import static com.cps.util.CPSConstants.SESSION_BEAN_PATH;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import javax.servlet.ServletException;

import com.cps.statement.StatementInfo;
import com.cps.userprofiling.CPSStatementConnectionManager;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;
import com.cps.util.CPSSessionBean;
import com.cps.util.security.CriptoUtils;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

/**
 *
 * @author Steve Neverov
 *
 */
public class CPSStatementsDroplet extends DynamoServlet {

    public static ParameterName CUSTOMER_ID = ParameterName.getParameterName("customerId");

    private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
    private static final ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");
    private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

    private static final String STATEMENTS = "statements";
    private static final String ERROR_MESSAGE = "errorMessage";

    public Profile getProfile() {
        return (Profile) ServletUtil.getCurrentUserProfile();
    }

    private CPSStatementConnectionManager mConnectionManager;

    public void setConnectionManager(CPSStatementConnectionManager pConnectionManager) {
        mConnectionManager = pConnectionManager;
    }

    public CPSStatementConnectionManager getConnectionManager() {
        return mConnectionManager;
    }

    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

        // check pricing client enabled call it otherwise call dummy
        if (getConnectionManager().getClient().getWebServiceConfig().getWebServiceEnabled()) {

            String customerId = (String) pRequest.getObjectParameter(CUSTOMER_ID);
            if (StringUtils.isBlank(customerId)) {
                Profile profile = getProfile();
                RepositoryItem parentOrg = null;
                if (profile != null) {
                    parentOrg = (RepositoryItem) profile.getPropertyValue(CPSConstants.PARENT_ORG);
                }
                if (parentOrg != null) {
                    customerId = parentOrg.getRepositoryId();
                    // } else {
                    // // for testing purposes
                    // customerId = "100109";
                }
            }

            try {
                List<StatementInfo> infos = getConnectionManager().requestStatements(customerId, null, null);
                if (infos != null && infos.size() > 0) {
                    TreeSet<StatementInfo> sortedInfos = new TreeSet<StatementInfo>();
                    sortedInfos.addAll(infos);

                    for (StatementInfo statementInfo : sortedInfos) {
                        statementInfo.setIdEncrypted(CriptoUtils.aesEncrypt(statementInfo.getId()));
                    }

                    pRequest.setParameter(STATEMENTS, sortedInfos);
                    pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
                } else {
                    pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
                }

                CPSSessionBean sessionBean = (CPSSessionBean) pRequest.resolveName(SESSION_BEAN_PATH);
                if (sessionBean != null) {
                    sessionBean.setStatements(infos);
                }
            } catch (Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
                pRequest.setParameter(ERROR_MESSAGE, CPSMessageUtils.getErrorMessage(CPSErrorCodes.ERR_GET_RESULTS, pRequest.getLocale()));
                pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
            }
        } else {
            pRequest.setParameter(ERROR_MESSAGE, "Service is not configured.");
            pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
        }
    }

    private Date countEndDate(Date pEndDate) {
        Long endDate;
        if (pEndDate == null) {
            Calendar cal = Calendar.getInstance();
            endDate = cal.getTimeInMillis();
        } else {
            endDate = pEndDate.getTime();
        }

        return new Date(Long.parseLong(String.valueOf(endDate)));
    }
}
