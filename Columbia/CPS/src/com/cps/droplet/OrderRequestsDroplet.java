package com.cps.droplet;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSOrderManager;

import java.io.IOException;
import java.util.List;


public class OrderRequestsDroplet extends DynamoServlet {

	private static final String COMPONENT_NAME = "/cps/droplet/OrderRequestsDroplet";

	private static final String ORDER_STATE_PENDING_APPROVAL = "PENDING_APPROVAL";
	private static final String ORDER_STATE_FAILED_APPROVAL = "FAILED_APPROVAL";

	private static final ParameterName PROFILE = ParameterName.getParameterName("profile");

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

	private static final String RETRIEVE_ORDERS_RQL_QUERY = "(state =?0 OR state =?1) AND profileId =?2";
	private static final String RETRIEVE_ORDERS_RQL_QUERY_CSR = "(state =?0 OR state =?1) AND organizationId =?2";

	private OrderManager mOrderManager;

	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - start").toString());
		}

		List<Order> orders = null;

		Profile profile = (Profile) pRequest.getObjectParameter(PROFILE);
		if (profile != null) {
			if ((((CPSOrderManager) getOrderManager()).getProfileTools()).isCSRUser(profile)) {
				orders = retrieveOrdersCSR(profile);
			} else {
				orders = retrieveOrders(profile);
			}
		}

		ParameterName renderParam = OUTPUT_OPARAM;
		if (orders != null && orders.size() > 0) {
			pRequest.setParameter("orders", orders);
			pRequest.setParameter("ordersSize", orders.size());
		} else {
			renderParam = EMPTY_OPARAM;
		}
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - end").toString());
		}

	}

	private List<Order> retrieveOrders(Profile pProfile) {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".retrieveOrders").append(" - start").toString());
		}

		Object[] params = new Object[3];
		params[0] = ORDER_STATE_FAILED_APPROVAL;
		params[1] = ORDER_STATE_PENDING_APPROVAL;
		params[2] = pProfile.getRepositoryId();

		List<Order> orderList = null;

		try {
			orderList = ((CPSOrderManager) getOrderManager()).retrieveOrders(params, RETRIEVE_ORDERS_RQL_QUERY);
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".retrieveOrders").append(" - exit").toString());
		}
		return orderList;

	}

	private List<Order> retrieveOrdersCSR(Profile pProfile) {

		List<Order> orderList = null;

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".retrieveOrdersCSR").append(" - start").toString());
		}

		RepositoryItem org = ((CPSOrderManager) getOrderManager()).getProfileTools().getParentOrganization(pProfile);

		if (null != org) {
			Object[] params = new Object[3];
			params[0] = ORDER_STATE_FAILED_APPROVAL;
			params[1] = ORDER_STATE_PENDING_APPROVAL;
			params[2] = org.getRepositoryId();

			try {
				orderList = ((CPSOrderManager) getOrderManager()).retrieveOrders(params, RETRIEVE_ORDERS_RQL_QUERY_CSR);
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError(re);
				}
			}
		}
		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".retrieveOrders").append(" - exit").toString());
		}
		return orderList;

	}

}