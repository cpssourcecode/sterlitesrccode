package com.cps.droplet;

import java.io.IOException;

import javax.servlet.ServletException;


import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class ReadOriginatingRequest extends DynamoServlet{
	
	private static ParameterName ORIGINATINGREQUEST = ParameterName.getParameterName("originatingRequest1");
	
	private static ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");
	private static ParameterName SUCCESS_OPARAM = ParameterName.getParameterName("output");
	private static ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");
	
	
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		ParameterName renderParam = EMPTY_OPARAM;
		
		if(pRequest.getAttribute("contentItem")!=null){
			vlogDebug("PRINTING OUT THE contentIem from Attribute  "+pRequest.getAttribute("contentItem"));
			pRequest.setParameter("contentItem", pRequest.getAttribute("contentItem"));
		}
			pRequest.serviceLocalParameter(SUCCESS_OPARAM, pRequest, pResponse);
	}

	
}