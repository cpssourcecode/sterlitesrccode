package com.cps.droplet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;

import com.cps.seo.SeoTools;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Droplet used to generate brand navigation menu.
 */
public class BrandNavigationDroplet extends DynamoServlet {

	/**
	 * The Constant OUTPUT_OPARAM.
	 */
	private static final String OUTPUT_OPARAM = "output";

	/**
	 * The Constant EMPTY_OPARAM.
	 */
	private static final String EMPTY_OPARAM = "empty";

	/**
	 * The catalog input param.
	 */
	private static final String CATALOG_PARAM = "catalog";

	/**
	 * The NAVIGATION_MENU.
	 */
	private static final String NAVIGATION_MENU = "navmenu";

	/**
	 * The menuList.
	 */
	private static final String MENU_LIST = "menuList";

	/**
	 * Dimension value cache tools
	 */
	private DimensionValueCacheTools mDimensionValueCacheTools = null;

	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		this.mDimensionValueCacheTools = pDimensionValueCacheTools;
	}

	/**
	 * Maximum category count limit
	 */
	private int mMaximumCount = 400;

	public int getMaximumCount() {
		return mMaximumCount;
	}

	public void setMaximumCount(int pMaximumCount) {
		this.mMaximumCount = pMaximumCount;
	}

	/**
	 * configured name of Brands menu
	 * (if id or name matches with menu folder data will be used for menu build)
	 */
	private String mBrandsMenuName = "Brands";

	public String getBrandsMenuName() {
		return mBrandsMenuName;
	}

	public void setBrandsMenuName(String pBrandsMenuName) {
		mBrandsMenuName = pBrandsMenuName;
	}

	/**
	 * configured id of Brands menu
	 * (if id or name matches with menu folder data will be used for menu build)
	 */
	private String mBrandsMenuId = "brands";

	public String getBrandsMenuId() {
		return mBrandsMenuId;
	}

	public void setBrandsMenuId(String pBrandsMenuId) {
		mBrandsMenuId = pBrandsMenuId;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		List<List<NavigationMenuItem>> brandsMenu = new ArrayList<>();
		Object catalogParam = pRequest.getObjectParameter(CATALOG_PARAM);
		if (catalogParam instanceof RepositoryItem && null != getDimensionValueCacheTools()) {
			RepositoryItem catalog = (RepositoryItem) catalogParam;
			List<RepositoryItem> navmenu = (List<RepositoryItem>) catalog.getPropertyValue(NAVIGATION_MENU);
			if (null != navmenu) {
				RepositoryItem brands = null;
				if (navmenu != null) {
					for (RepositoryItem folder : navmenu) {
						String name = (String) folder.getPropertyValue("name");
						if (getBrandsMenuName().equals(name) || getBrandsMenuId().equals(folder.getRepositoryId())) {
							brands = folder;
							break;
						}
					}
				}
				if (null != brands) {
					List<RepositoryItem> subfolders = (List<RepositoryItem>) brands.getPropertyValue("subfolders");
					if (null != subfolders && !subfolders.isEmpty()) {
						for (RepositoryItem subfolder : subfolders) {
							List<NavigationMenuItem> columnBrandMenu = new ArrayList<NavigationMenuItem>();
							List<RepositoryItem> submenu = (List<RepositoryItem>) subfolder.getPropertyValue("submenu");
							if (null != submenu && !submenu.isEmpty()) {
								for (RepositoryItem menu : submenu) {
									columnBrandMenu.add(getBrandMenuItem(menu));
								}
							}
							brandsMenu.add(columnBrandMenu);
						}
					}
				}
			}
		}

		if (brandsMenu.isEmpty()) {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		} else {
			pRequest.setParameter(MENU_LIST, brandsMenu);
			pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
	}

	private NavigationMenuItem getBrandMenuItem(final RepositoryItem pMenu) {
		String link = (String) pMenu.getPropertyValue("link");
		String name = (String) pMenu.getPropertyValue("name");

		String seoName = SeoTools.seoName(name);
		List<DimensionValueCacheObject> listCacheObject = getDimensionValueCacheTools().get(seoName);
		if (listCacheObject != null && listCacheObject.size() > 0) {
			String seoUrl = ("/" + seoName);
			if (!StringUtils.isBlank(seoUrl)) {
				link = seoUrl;
			}
		}

		if (StringUtils.isBlank(name)) {
			RepositoryItem brand = null;
			try {
				brand = (RepositoryItem) pMenu.getPropertyValue("brand");
			} catch (Exception e) {
				brand = null;
			}
			if (null != brand) {
				name = (String) brand.getPropertyValue("name");
			}
		}
		return new NavigationMenuItem(link, name);
	}

	public class NavigationMenuItem {

		private String mURL;

		private String mName;

		public NavigationMenuItem(final String pURL, final String pName) {
			mURL = pURL;
			mName = pName;
		}

		public String getURL() {
			return mURL;
		}

		public void setURL(final String pURL) {
			mURL = pURL;
		}

		public String getName() {
			return mName;
		}

		public void setName(final String pName) {
			mName = pName;
		}

		private BrandNavigationDroplet getOuterType() {
			return BrandNavigationDroplet.this;
		}

		@Override
		public String toString() {
			return "NavigationMenuItem [mURL=" + mURL + ", mName=" + mName + "]";
		}
	}
}