package com.cps.droplet;


import static com.cps.util.CPSConstants.ADDRESS;
import static com.cps.util.CPSConstants.ADDRESS_ID;
import static com.cps.util.CPSConstants.BUSINESS_UNIT;
import static com.cps.util.CPSConstants.CART;
import static com.cps.util.CPSConstants.CART_SELECTED_CS;
import static com.cps.util.CPSConstants.CART_SELECTED_DELIVERY_METHOD;
import static com.cps.util.CPSConstants.CART_SELECTED_STORE;
import static com.cps.util.CPSConstants.CHECKOUT_DELIVERY_METHOD;
import static com.cps.util.CPSConstants.CONTACT_INFO;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_PICK_UP;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_SHIPPED;
import static com.cps.util.CPSConstants.ORDER;
import static com.cps.util.CPSConstants.PAGE;
import static com.cps.util.CPSConstants.PICKUP;
import static com.cps.util.CPSConstants.PROFILE;
import static com.cps.util.CPSConstants.REVIEW;
import static com.cps.util.CPSConstants.SELECTED_CS;
import static com.cps.util.CPSConstants.STORE_BRANCH_ID_PRTY;
import static com.cps.util.CPSConstants.STORE_ITEM_ID;
import static com.cps.util.CPSConstants.STORE_ITEM_TYPE;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.commerce.order.CPSOrderImpl;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.ShippingGroup;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;


/**
 * Checks cart settings and define delivery method, cart selected address or cart selected location based on this.
 *
 * @author Andy Porter
 */
public class CartAddressDroplet extends DynamoServlet {

	public static final ParameterName ORDER_PARAM = ParameterName.getParameterName(ORDER);
	public static final ParameterName PROFILE_PARAM = ParameterName.getParameterName(PROFILE);
	public static final ParameterName PAGE_PARAM = ParameterName.getParameterName(PAGE);

	/**
	 * output.
	 */
	public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");

	/**
	 * EMPTY
	 */
	public static final ParameterName EMPTY = ParameterName.getParameterName("empty");


	/**
	 * The Constant DELIVERY_METHOD.
	 */
//	private static final String DELIVERY_METHOD = "deliveryMethod";


	/**
	 * ProfileRepository
	 */
	private Repository mProfileRepository;

	/**
	 * LocationRepository
	 */
	private Repository mLocationRepository;


	@SuppressWarnings("rawtypes")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		String deliveryMethod = null;
		Object address = null;
		String addressId = null;
		RepositoryItem store = null;
		String storeId = null;

		CPSOrderImpl order = (CPSOrderImpl) pRequest.getObjectParameter(ORDER_PARAM);
		Profile profile = (Profile) pRequest.getObjectParameter(PROFILE_PARAM);
		String page = (String) pRequest.getObjectParameter(PAGE_PARAM);

		RepositoryItem selectedCS = (RepositoryItem) profile.getPropertyValue(SELECTED_CS);
		RepositoryItem cartSelectedCS = (RepositoryItem) profile.getPropertyValue(CART_SELECTED_CS);
		String cartSelectedStoreId = (String) profile.getPropertyValue(CART_SELECTED_STORE);
		String cartSelectedDeliveryMethod = (String) profile.getPropertyValue(CART_SELECTED_DELIVERY_METHOD);

		if(!PICKUP.equals(page)) {
			if (CART.equals(page) && StringUtils.isNotBlank(cartSelectedDeliveryMethod) &&
					(StringUtils.isNotBlank(cartSelectedStoreId) || null != cartSelectedCS)) {
				deliveryMethod = cartSelectedDeliveryMethod;   // DELEVERY_METHOD_SHIPPED / DELEVERY_METHOD_PICK_UP

			} else if ((null != order) && (order.getShippingGroupCount() > 0)) {   // get data from order
				List<ShippingGroup> shippingGroups = order.getShippingGroups();
				for (ShippingGroup shippingGroup : shippingGroups) {
					if (shippingGroup instanceof InStorePickupShippingGroup) {
						deliveryMethod = DELEVERY_METHOD_PICK_UP;
						InStorePickupShippingGroup inStorePickupShippingGroup = (InStorePickupShippingGroup) shippingGroup;
						String locationId = inStorePickupShippingGroup.getLocationId();    // will save there branchId, so we need to get Store-by-branch-ID
						String id = inStorePickupShippingGroup.getId();
						store = getRepositoryItem(mLocationRepository, locationId, STORE_ITEM_TYPE);
						if (null != store) {
							storeId = store.getRepositoryId();
						}
						System.out.println("getting location from order :: " + storeId);
					} else if (shippingGroup instanceof HardgoodShippingGroup) {
						deliveryMethod = DELEVERY_METHOD_SHIPPED;
						CPSHardgoodShippingGroup hardgoodShippingGroup = (CPSHardgoodShippingGroup) shippingGroup;
						String csId = hardgoodShippingGroup.getJdeAddressNumber();
						RepositoryItem addressItem = getRepositoryItem(mProfileRepository, csId, CONTACT_INFO);
						if (null != addressItem) {
							addressId = addressItem.getRepositoryId();
							address = addressItem;
						} else {
							address = hardgoodShippingGroup.getShippingAddress();
						}
					}
				}
			} else { // take default selectedCS from profile
				deliveryMethod = DELEVERY_METHOD_SHIPPED;
				address = selectedCS;
				if (null != selectedCS) {
					addressId = selectedCS.getRepositoryId();
				}
			}
		}

        if (StringUtils.isBlank(storeId) && !REVIEW.equals(page)) { // additional check that pickup within order
			if (StringUtils.isNotBlank(cartSelectedStoreId)) {  // Store #3
				store = getRepositoryItem(mLocationRepository, cartSelectedStoreId, STORE_ITEM_TYPE);
				storeId = store.getRepositoryId();
			} else if (null != selectedCS) { // Store#2
				String branchId = (String) selectedCS.getPropertyValue(BUSINESS_UNIT);
				store = getStore(branchId);
				if (null != store) {
					storeId = store.getRepositoryId();
				}
			}
		}

		if (StringUtils.isBlank(addressId) && !REVIEW.equals(page)) { // addional check that pickup within order
			if (null != cartSelectedCS) {
				address = cartSelectedCS;
				addressId = cartSelectedCS.getRepositoryId();
			} else if (null != selectedCS) {
				address = selectedCS;
				addressId = selectedCS.getRepositoryId();
			}
		}

		pRequest.setParameter(CHECKOUT_DELIVERY_METHOD, deliveryMethod);
		pRequest.setParameter(ADDRESS, address);
		pRequest.setParameter(ADDRESS_ID, addressId);
		pRequest.setParameter(STORE_ITEM_TYPE, store);
		pRequest.setParameter(STORE_ITEM_ID, storeId);

		pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);

	}


	private RepositoryItem getRepositoryItem(Repository pRepository, String pRepItemId, String pRepItemType) {
		RepositoryItem repItem = null;
		try {
			if (!StringUtils.isBlank(pRepItemId)) {
				repItem = pRepository.getItem(pRepItemId, pRepItemType);
			}
		} catch (RepositoryException re) {
			vlogError("getRepositoryItem() RepositoryException", re);
		}
		return repItem;
	}

	private RepositoryItem getStore(String pBranchId) {
		RepositoryItem store = null;

		try {
			String rqlQuery = STORE_BRANCH_ID_PRTY + "=" + pBranchId;
			RepositoryView rpView = getLocationRepository().getView(STORE_ITEM_TYPE);
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);

			RepositoryItem[] results = stmt.executeQuery(rpView, null);

			if (results != null && results.length == 1) {
				store = results[0];
			}
		} catch (Exception e) {
			vlogError(e, "LocationByBranchLookupDroplet.getStore:");
		}

		return store;
	}


	// Getter and Setter methods

	/**
	 * Sets new repository.
	 *
	 * @param pProfileRepository New value of repository.
	 */
	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}

	/**
	 * Gets repository.
	 *
	 * @return Value of repository.
	 */
	public Repository getProfileRepository() {
		return mProfileRepository;
	}


	/**
	 * Gets LocationRepository.
	 *
	 * @return Value of LocationRepository.
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * Sets new LocationRepository.
	 *
	 * @param pLocationRepository New value of LocationRepository.
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}
}
