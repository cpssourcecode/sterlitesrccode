package com.cps.droplet;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import java.io.IOException;

public class LastVisitedListingPage extends DynamoServlet {
    /**
     * input param last visited page to store
     */
    private final static String INPUT_PAGE_TO_STORE = "pageToStore";
    /**
     * output param page last visited
     */
    private final static String OUTPUT_PAGE_LAST_VISITED = "pageLastVisited";
    /**
     * output service local parameter
     */
    private final static String OUTPUT_OUTPUT = "output";
    /**
     * last visited page
     */
    private String mLastVisitedPage = "/";
    /**
     * loadSection
     */
    private final static String LOAD_SECTION = "loadSection";

    /**
     * retrieve and init last visited page
     * @param pRequest request
     * @param pResponse response
     * @throws ServletException if exception occurs
     * @throws IOException if exception occurs
     */
    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        String pageToStore = pRequest.getParameter(INPUT_PAGE_TO_STORE);
        if(!StringUtils.isBlank(pageToStore)) {
            if (pageToStore.contains(LOAD_SECTION)) {
                pageToStore = pageToStore.split(LOAD_SECTION)[0];
                mLastVisitedPage = pageToStore.substring(0, pageToStore.length() - 1);
            } else {
                mLastVisitedPage = pageToStore;
            }
        }
        pRequest.setParameter(OUTPUT_PAGE_LAST_VISITED, mLastVisitedPage);
        pRequest.serviceLocalParameter(OUTPUT_OUTPUT, pRequest, pResponse);
    }
}
