package com.cps.droplet;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;


import static com.cps.util.CPSConstants.*;

/**
 * Takes in user id and role and checks if user has role assigned
 * Returns true if user has role, false otherwise
 *
 * @author root
 */
public class UsersAccountCBLookupDroplet extends DynamoServlet {

	public static final ParameterName USER_ID = ParameterName.getParameterName("userId");
	public static final ParameterName TRUE = ParameterName.getParameterName("true");
	public static final ParameterName FALSE = ParameterName.getParameterName("false");
	public static final ParameterName OUTPUT = ParameterName.getParameterName("output");


	/**
	 * output.
	 */
	public static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");

	/**
	 * EMPTY
	 */
	public static final ParameterName EMPTY = ParameterName.getParameterName("empty");

	/**
	 * The Constant RESULT_MAP.
	 */
	private static final String RESULT_MAP = "resultMap";


	/**
	 * repository
	 */
	private Repository mRepository;

	/** roles array which should be */
	private String[] mRoles;

	@SuppressWarnings("rawtypes")
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		Map<String, String> resultMap = new LinkedHashMap<String, String>();
		String currentUserId = pRequest.getParameter(USER_ID);
		vlogDebug("userId: " + currentUserId);

		boolean result = false;
		RepositoryItem currentUser = getRepositoryItem(currentUserId, USER);
		if (null != currentUser) {
//			accountCB = parentOrganization
			RepositoryItem accountCB = (RepositoryItem) currentUser.getPropertyValue(PARENT_ORG);
			if (null != accountCB) {
				Set members = (Set) accountCB.getPropertyValue(ORGANIZATION_MEMBERS_PRTY);
				if (!members.isEmpty()) {
					for (Object oUser : members) {
						if (oUser instanceof RepositoryItem) {
							RepositoryItem user = (RepositoryItem) oUser;
							String userId = user.getRepositoryId();

							if (!currentUserId.equals(userId) && checkUserRole(user)) {
								String userEmail = (String) user.getPropertyValue(EMAIL);
								String firstName = (String) user.getPropertyValue(FIRST_NAME);
								String lastName = (String) user.getPropertyValue(LAST_NAME);
								if (!StringUtils.isBlank(userEmail)) {
									resultMap.put(userId, firstName + " " + lastName);
								}
							}
						}
					}
					result = true;
				}
			}
		}

		if (result) {
			pRequest.setParameter(RESULT_MAP, resultMap);
			pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);

		} else {
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		}

	}

	private boolean checkUserRole(RepositoryItem user) {
		boolean result = false;
		if(user != null) {
			Set<RepositoryItem> orgMemberRoles = (Set) user.getPropertyValue(CPSConstants.ROLES_PRPTY);
			if (orgMemberRoles != null) {
				for (RepositoryItem role : orgMemberRoles) {
					String roleName = (String) role.getPropertyValue(CPSConstants.NAME);
					if (getRoles() != null && getRoles().length > 0) {
						for(String accessRole: getRoles()) {
							if(accessRole.equals(roleName)) {
								result = true;
							}
						}
					} else {
						result = true;
					}
				}
			}
		}
		return result;
	}
	
	private RepositoryItem getRepositoryItem(String pRepItemId, String pRepItemType) {
		RepositoryItem repItem = null;
		try {
			if (!StringUtils.isBlank(pRepItemId)) {
				repItem = mRepository.getItem(pRepItemId, pRepItemType);
			}
		} catch (RepositoryException re) {
			vlogError("getRepositoryItem() RepositoryException", re);
		}
		return repItem;
	}

	// Getter and Setter methods

	/**
	 * return specified repository
	 *
	 * @return specified repository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * init item's repository
	 *
	 * @param pRepository item's repository
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	public String[] getRoles() {
		return mRoles;
	}

	public void setRoles(String[] pRoles) {
		mRoles = pRoles;
	}

}