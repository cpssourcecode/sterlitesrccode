package com.cps.droplet;

import static com.cps.util.CPSConstants.BILLING_ACCOUNTS;
import static com.cps.util.CPSConstants.CHILD_ORGS;
import static com.cps.util.CPSConstants.SECONDARY_ADDRESSES;
import static com.cps.util.CPSConstants.SESSION_BEAN_PATH;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.userprofiling.CPSProfileTools;
import com.cps.userprofiling.util.UserAssociatedCS;
import com.cps.util.CPSConstants;
import com.cps.util.CPSSessionBean;

public class ManageUsersShipping extends DynamoServlet {

	private Repository profileRepository;

	private static final ParameterName COMPANY_ID = ParameterName.getParameterName("companyId");
	private static final ParameterName USER_ID = ParameterName.getParameterName("userId");
	private static final ParameterName ADDRESS_LIST = ParameterName.getParameterName("addressList");
	private static final ParameterName DEFAULT_ID = ParameterName.getParameterName("defaultId");
	private static final ParameterName REMOVE_ID = ParameterName.getParameterName("removeId");
	private static final ParameterName IS_MODAL = ParameterName.getParameterName("isModal");
	private static final ParameterName SEARCH_TERM = ParameterName.getParameterName("searchTerm");
	private static final ParameterName COMPANY_IDS = ParameterName.getParameterName("companyIds");

	private static final String CURRENT_LIST = "currentList";
	private static final String SHOW_BILLING_ACCOUNT = "showBillingAccount";
	private static final String DEFAULT_CS = "defaultCS";

	private CPSProfileTools mProfileTools;

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("ManageUserShipping.start");
		ParameterName outputName = DropletConstants.OUTPUT;
		String companyId = pRequest.getParameter(COMPANY_ID);
		String userId = pRequest.getParameter(USER_ID);
		String addressList = pRequest.getParameter(ADDRESS_LIST);
		String defaultId = pRequest.getParameter(DEFAULT_ID);
		String removeId = pRequest.getParameter(REMOVE_ID);
		String isModal = pRequest.getParameter(IS_MODAL);
		String searchTerm = pRequest.getParameter(SEARCH_TERM);
		String companyIds = pRequest.getParameter(COMPANY_IDS);
		CPSSessionBean sessionBean = (CPSSessionBean) pRequest.resolveName(SESSION_BEAN_PATH);

		vlogDebug("Params: companyId - " + companyId + " | userId - " + userId + " | addressList - " + addressList +
				" | defaultId - " + defaultId + " | removeId - " + removeId + " | isModal: " + isModal +
				" | searchTerm: " + searchTerm + " | companyIds: " + companyIds);

		List<UserAssociatedCS> currentList = null;
		List<UserAssociatedCS> displayList = new ArrayList<UserAssociatedCS>();
		RepositoryItem editUser = null;
		boolean showBillingAccount = false;

		if (StringUtils.isNotBlank(isModal)) {
			// Get list to display on modal
			vlogDebug("IsModal");
			if (StringUtils.isNotBlank(userId)) {
				editUser = getItemById(CPSConstants.B2B_ITEM_DESCRIPTOR, userId);
			}

			if (editUser != null) {
				currentList = new ArrayList<UserAssociatedCS>();
				RepositoryItem parentOrg = (RepositoryItem) editUser.getPropertyValue(CPSConstants.PARENT_ORG);

				//Map<String, RepositoryItem> csMap = (Map<String, RepositoryItem>)parentOrg.getPropertyValue(CPSConstants.SECONDARY_ADDRESSES);
				Map<String, RepositoryItem> csMap = getOrgsShippingAddresses(parentOrg);
				for (Entry<String, RepositoryItem> address : csMap.entrySet()) {
					UserAssociatedCS newcs = getProfileTools().createAssociatedCS(address.getValue());
					if (csNotInUserList(newcs, sessionBean)) {
						populateOrgValues(newcs, address.getKey(), parentOrg);
						currentList.add(newcs);
					}
				}

				Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) parentOrg.getPropertyValue(CHILD_ORGS);
				if (childOrgs != null && childOrgs.size() > 0) {
					Set<RepositoryItem> billingAccounts = (Set<RepositoryItem>) editUser.getPropertyValue(BILLING_ACCOUNTS);
					// check for selected billing accounts for this user (organizations)
					currentList = filterListByBillingAccounts(currentList, billingAccounts);
					showBillingAccount = true;
				}

				// Filter current list for searchTerm here
				if (StringUtils.isNotBlank(searchTerm)) {
					currentList = filterListBySearch(currentList, searchTerm);
				}

				vlogDebug("Current List: {0}", currentList);
				displayList = currentList;
			}
		} else {
			// Get current list of cs if available
			if (StringUtils.isNotBlank(userId) && userId.equalsIgnoreCase(sessionBean.getUserShippingId())) {
				currentList = sessionBean.getUsersShipping();
			} else {
				sessionBean.setUsersShipping(null);
			}
			vlogDebug("CurrentList: {0}", currentList);
			if (currentList == null) {
				currentList = new ArrayList<UserAssociatedCS>();
			}

			// Check if userId is passed from edit user page
			if (StringUtils.isNotBlank(userId)) {
				// It is, get the user
				editUser = getItemById(CPSConstants.B2B_ITEM_DESCRIPTOR, userId);
			}
			vlogDebug("Edit User: {0}", editUser);

			// If current list not populated, populate with default values
			RepositoryItem org = null;
			if (editUser != null) {
				org = (RepositoryItem) editUser.getPropertyValue(CPSConstants.PARENT_ORG);
			} else {
				org = getItemById(CPSConstants.ORGANIZATION, companyId);
			}

			if (currentList.isEmpty()) {
				// If edit user is found, populate from current set associated cs values
				if (editUser != null) {
					vlogDebug("Populate list for user");
					currentList = populateList(editUser, CPSConstants.SECONDARY_ADDRESSES);
				} else {
					// Otherwise populate from passed company
					vlogDebug("Populate list from org");
					currentList = populateList(org, CPSConstants.SECONDARY_ADDRESSES);
				}
			} else {
				// check if orgs in list are populated
				for (UserAssociatedCS userAssociatedCS : currentList) {
					if (userAssociatedCS.getOrganization() == null) {
						populateOrgValues(userAssociatedCS, null, org);
					}
				}

			}

			vlogDebug("CurrentList after populate: {0}", currentList);

			// Check if addresses are entered from modal
			if (StringUtils.isNotBlank(addressList)) {
				// Entered address ids from modal, update currentList with values
				currentList = updateListFromModal(currentList, addressList);
			}

			// If removal id passed, remove from current list
			if (StringUtils.isNotBlank(removeId)) {
				currentList = removeFromList(currentList, removeId);
			}

			// Set the correct default address
			if (StringUtils.isNotBlank(defaultId)) {
				currentList = updateDefaultCS(currentList, defaultId);
			}

			vlogDebug("CurrentList after filters: {0}", currentList);

			// Update session bean with latest values
			sessionBean.setUsersShipping(currentList);
			sessionBean.setUserShippingId(userId);

			UserAssociatedCS defaultCS = findDefaultCS(editUser, currentList);
			pRequest.setParameter(DEFAULT_CS, defaultCS);
			vlogDebug("Default: {0}", defaultCS);

			if (org != null) {
				Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) org.getPropertyValue(CHILD_ORGS);
				if (childOrgs != null && childOrgs.size() > 0) {

					currentList = filterListByCompanyIds(currentList, companyIds);
//					if (editUser != null) {
//						Set<RepositoryItem> billingAccounts = (Set<RepositoryItem>) editUser.getPropertyValue(BILLING_ACCOUNTS);
//						// check for selected billing accounts for this user (organizations)
//						currentList = filterListByBillingAccounts(currentList, billingAccounts);
//					}
					showBillingAccount = true;
				}
			}

			// If search term entered, filter by search
			if (StringUtils.isNotBlank(searchTerm)) {
				displayList = filterListBySearch(currentList, searchTerm);
			} else {
				displayList = currentList;
			}
		}

		// Return output param
		pRequest.setParameter(CURRENT_LIST, displayList);
		pRequest.setParameter(SHOW_BILLING_ACCOUNT, showBillingAccount);
		pRequest.serviceLocalParameter(outputName, pRequest, pResponse);
	}

	/**
	 * Populate initial associated cs list from passed repositoryItem
	 *
	 * @param ri - either user or organization
	 * @return associatedCS list
	 */
	protected List<UserAssociatedCS> populateList(RepositoryItem ri, String property) {
		vlogDebug("populateList.start - repositoryItem: " + ri);
		List<UserAssociatedCS> associatedCS = new ArrayList<UserAssociatedCS>();
		Map<String, RepositoryItem> csMap = null;
		RepositoryItem shippingAddress = null;
		RepositoryItem parentOrg = null;
		try {
			if (ri != null && CPSConstants.B2B_ITEM_DESCRIPTOR.equalsIgnoreCase(ri.getItemDescriptor().getItemDescriptorName())) {
				vlogDebug("Populate list from user item");
				shippingAddress = (RepositoryItem) ri.getPropertyValue(CPSConstants.SHIPPING_ADDRESS);
				csMap = (Map<String, RepositoryItem>) ri.getPropertyValue(property);
				parentOrg = (RepositoryItem) ri.getPropertyValue(CPSConstants.PARENT_ORG);
				if (csMap == null || (csMap != null && csMap.isEmpty())) {
					csMap = getOrgsShippingAddresses(parentOrg);
//					if (parentOrg != null){
//						csMap = (Map<String, RepositoryItem>)parentOrg.getPropertyValue(CPSConstants.SECONDARY_ADDRESSES);
//					}
				}
			} else {
				vlogDebug("Get secondary address map from org");
				//csMap = (Map<String, RepositoryItem>)ri.getPropertyValue(CPSConstants.SECONDARY_ADDRESSES);
				csMap = getOrgsShippingAddresses(ri);
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}
		vlogDebug("AssociatedCS map: " + csMap);
		if (csMap != null && !csMap.isEmpty()) {
			for (Entry<String, RepositoryItem> address : csMap.entrySet()) {
				UserAssociatedCS newcs = getProfileTools().createAssociatedCS(address.getValue());
				if (shippingAddress != null) {
					if (newcs.getId().equalsIgnoreCase(shippingAddress.getRepositoryId())) {
						newcs.setIsDefault(true);
					}
				}
				populateOrgValues(newcs, address.getKey(), parentOrg);
				associatedCS.add(newcs);
			}
		}

		vlogDebug("Returning: " + associatedCS);
		return associatedCS;
	}

	/**
	 * Populates org values in address bean
	 *
	 * @param pCS        pCS
	 * @param pKey       pKey
	 * @param pParentOrg pParentOrg
	 */
	private void populateOrgValues(UserAssociatedCS pCS, String pKey, RepositoryItem pParentOrg) {

		RepositoryItem organization = null;
		if (pKey != null) {
			String[] nickname = pKey.split("-");
			if (nickname != null && nickname.length == 2) {
				organization = getItemById(CPSConstants.ORGANIZATION, nickname[0]);
			}
		}
		if (organization == null && pParentOrg != null) {
			organization = getProfileTools().findOrgFromShippingAddress(pParentOrg, pCS.getAddressItem());
		}
		if (organization != null) {
			pCS.setOrganization(organization);
		}

	}

	/**
	 * Gets the all orgs shipping addresses.
	 *
	 * @param parentOrg the parent org
	 * @return the orgs shipping addresses
	 */
	private Map<String, RepositoryItem> getOrgsShippingAddresses(RepositoryItem parentOrg) {
		Map<String, RepositoryItem> map = new HashMap<String, RepositoryItem>();
		if (parentOrg != null) {
			Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) parentOrg.getPropertyValue(CHILD_ORGS);
			if (childOrgs != null) {
				for (RepositoryItem org : childOrgs) {
					Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) org
							.getPropertyValue(SECONDARY_ADDRESSES);

					if (secondaryAddresses != null) {
						map.putAll(secondaryAddresses);
					}
				}
			}
			Map<String, RepositoryItem> parentSecondaryAddresses = (Map<String, RepositoryItem>) parentOrg.getPropertyValue(SECONDARY_ADDRESSES);
			if (parentSecondaryAddresses != null) {
				map.putAll(parentSecondaryAddresses);
			}
		}
		return map;
	}


	/**
	 * Create new associated cs list from passed ids
	 *
	 * @param addressList - address ids to populate new list
	 * @return associatedCS list
	 */
	protected List<UserAssociatedCS> updateListFromModal(List<UserAssociatedCS> currentList, String addressList) {
		vlogDebug("updateListFromModal.start - addressList: " + addressList);
		String[] addressIds = addressList.split(",");

		for (String addressId : addressIds) {
			RepositoryItem address = getItemById(CPSConstants.CONTACT_INFO, addressId);
			if (address != null) {
				currentList.add(getProfileTools().createAssociatedCS(address));
			}
		}

		vlogDebug("associatedCS list: " + currentList);
		return currentList;
	}

	/**
	 * Removes address from current list
	 *
	 * @param currentList - current list
	 * @param removeId    - id of address to remove
	 * @return updated current list
	 */
	protected List<UserAssociatedCS> removeFromList(List<UserAssociatedCS> currentList, String removeId) {
		vlogDebug("removeFromList.start - removeId: " + removeId);
		for (UserAssociatedCS cs : currentList) {
			if (cs.getId().equals(removeId)) {
				cs.setIsActive(!cs.getIsActive());
			}
		}

		return currentList;
	}

	/**
	 * Set correct address to default
	 *
	 * @param currentList - current list
	 * @param defaultId   - id to set to default
	 * @return updated current list
	 */
	protected List<UserAssociatedCS> updateDefaultCS(List<UserAssociatedCS> currentList, String defaultId) {
		vlogDebug("updateDefaultCS.start - defaultId: " + defaultId);

		for (UserAssociatedCS cs : currentList) {
			if (cs.getId().equals(defaultId)) {
				vlogDebug("Address found, set to default");
				cs.setIsDefault(true);
			} else {
				cs.setIsDefault(false);
			}
		}

		return currentList;
	}

	protected List<UserAssociatedCS> filterListBySearch(List<UserAssociatedCS> list, String searchTerm) {
		List<UserAssociatedCS> filteredList = new ArrayList<UserAssociatedCS>();

		// Loop through each list and add to new list if search term found
		for (UserAssociatedCS item : list) {
			List<String> values = new ArrayList<String>();
			values.add((String) item.getAddressItem().getPropertyValue(CPSConstants.ADDRESS1));
			values.add((String) item.getAddressItem().getPropertyValue(CPSConstants.ADDRESS2));
			values.add((String) item.getAddressItem().getPropertyValue(CPSConstants.ADDRESS3));
			if (checkItem(searchTerm, values)) {
				filteredList.add(item);
			}
		}

		return filteredList;
	}

	protected List<UserAssociatedCS> filterListByBillingAccounts(List<UserAssociatedCS> list, Set<RepositoryItem> billingAccounts) {
		List<UserAssociatedCS> filteredList = new ArrayList<UserAssociatedCS>();
		if (billingAccounts != null) {
			// Loop through each list and add to new list if search term found
			for (UserAssociatedCS item : list) {
				if (item.getOrganization() != null && billingAccounts.contains(item.getOrganization())) {
					filteredList.add(item);
				}
			}
		}
		return filteredList;
	}

	protected List<UserAssociatedCS> filterListByCompanyIds(List<UserAssociatedCS> list, String companyIdList) {
		vlogDebug("filterListByCompanyIds.start - companyIds: " + companyIdList);
		List<UserAssociatedCS> filteredList = new ArrayList<UserAssociatedCS>();
		if (!StringUtils.isBlank(companyIdList)) {
			String[] companyIdsArray = companyIdList.split(",");
			Set<String> companyIds = new HashSet<String>(Arrays.asList(companyIdsArray));
			// Loop through each list and add to new list if search term found
			for (UserAssociatedCS item : list) {
				RepositoryItem org = item.getOrganization();
				if (org != null && companyIds.contains(org.getRepositoryId())) {
					filteredList.add(item);
				}
			}
		}
		vlogDebug("filterListByCompanyIds list: " + filteredList);
		return filteredList;
	}

	protected boolean checkItem(String searchTerm, List<String> values) {
		boolean match = false;

		int count = 0;

		while (!match && count < values.size()) {
			if (!StringUtils.isBlank(values.get(count))) {
				match = values.get(count).toLowerCase().indexOf(searchTerm.toLowerCase()) != -1;
			}
			count++;
		}

		return match;
	}

	private RepositoryItem getItemById(String itemDescriptor, String userId) {
		RepositoryItem[] item = null;
		try {
			final RepositoryItemDescriptor roleDesc = getProfileRepository().getItemDescriptor(itemDescriptor);
			final RepositoryView view = roleDesc.getRepositoryView();
			final QueryBuilder builder = view.getQueryBuilder();
			final QueryExpression name = builder.createPropertyQueryExpression(CPSConstants.ID);
			final QueryExpression itemId = builder.createConstantQueryExpression(userId);
			final Query queryItem = builder.createComparisonQuery(name, itemId, QueryBuilder.EQUALS);
			item = view.executeQuery(queryItem);
		} catch (final RepositoryException e) {
			vlogError(e, "getItem()");
		} catch (final NullPointerException ne) {
			vlogError(ne, "getItem()");
		}
		return item != null ? item[0] : null;
	}


	private UserAssociatedCS findDefaultCS(RepositoryItem user, List<UserAssociatedCS> currentList) {
		UserAssociatedCS defaultCS = null;

		for (UserAssociatedCS cs : currentList) {
			if (cs.getIsDefault()) {
				defaultCS = cs;
			}
		}

		return defaultCS;
	}

	private boolean csNotInUserList(UserAssociatedCS userCS, CPSSessionBean pSessionBean) {
		boolean isCSInList = false;

		if (pSessionBean.getUsersShipping() != null) {
			for (UserAssociatedCS cs : pSessionBean.getUsersShipping()) {
				if (userCS.getId().equalsIgnoreCase(cs.getId())) {
					isCSInList = true;
				}
			}
		}

		return !isCSInList;
	}

	public Repository getProfileRepository() {
		return profileRepository;
	}

	public void setProfileRepository(Repository profileRepository) {
		this.profileRepository = profileRepository;
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

}