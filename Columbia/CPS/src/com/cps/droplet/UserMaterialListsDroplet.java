package com.cps.droplet;

import atg.adapter.gsa.ChangeAwareList;
import atg.commerce.gifts.GiftlistManager;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;
import com.cps.droplet.constants.IUserMaterialListsDroplet;
import com.cps.service.GiftListService;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.CPSSessionBean;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;

import static com.cps.util.CPSConstants.*;

public class UserMaterialListsDroplet extends DynamoServlet implements IUserMaterialListsDroplet {

	private static final ParameterName SEARCH_TERM = ParameterName.getParameterName(TERM);
	private static final ParameterName SEARCH_CS = ParameterName.getParameterName(CS);

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName(OUTPUT);

	private GiftlistManager giftlistManager;

	/**
	 * gift list service
	 */
	private GiftListService mGiftListService;

	//------------------------------------------------------------------------------------------------------------------

	//	testing
	private boolean mAfter = false;

	public boolean isAfter() {
		return mAfter;
	}

	public void setAfter(boolean pAfter) {
		mAfter = pAfter;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("Search Reorder list Droplet");

		boolean emptyResult = false;
		List<RepositoryItem> giftlists = new ArrayList<RepositoryItem>();
		List<RepositoryItem> filteredList = new ArrayList<RepositoryItem>();
		RepositoryItem profileOrg = null;

		ParameterName renderParam = OUTPUT_OPARAM;
		String searchTerm = pRequest.getParameter(SEARCH_TERM);
		String searchCS = pRequest.getParameter(SEARCH_CS);

		Profile profile = getProfile();
		if (profile != null) {
			giftlists = (List) profile.getPropertyValue(GIFTLISTS);
			vlogDebug("Term - " + searchTerm);
			vlogDebug("CS - " + searchCS);
			profileOrg = ((CPSProfileTools)getGiftlistManager().getProfileTools()).getParentOrganization(profile);
			filteredList = getFilteredGiftLists(giftlists, profileOrg, searchCS, searchTerm);
		}
		vlogDebug("Is list empty? - " + filteredList.isEmpty());

		if (filteredList.isEmpty()) {
			filteredList.addAll(giftlists); // add all when epmty ????
			if (!StringUtils.isBlank(searchTerm) || !StringUtils.isBlank(searchCS)) {
				emptyResult = true;
			}
		}

		List<LinkedHashMap<String, Object>> sortableLists = getSortedMapOfGiftList(filteredList, profileOrg);
		vlogDebug("Sortable list of maps - " + sortableLists.toString());

		CPSSessionBean sessionBean = (CPSSessionBean)pRequest.resolveName(SESSION_BEAN_PATH);
		if (sessionBean != null) {
			if (!sortableLists.isEmpty()) {
				sessionBean.setSortableReorderLists(sortableLists);
			} else {
				sessionBean.setSortableReorderLists(null);
			}
		}

		sortSearchResultsList(filteredList);

		pRequest.setParameter("giftlists", filteredList);
		pRequest.setParameter("emptyResult", emptyResult);
		pRequest.serviceLocalParameter(renderParam, pRequest, pResponse);
	}

	/**
	 * @param pGiftLists  user gift lists
	 * @param pProfileOrg profile org
	 * @param pSearchCS   cs input
	 * @param pSearchTerm search input
	 * @return sorted list of giftlist data
	 */
	private List<RepositoryItem> getFilteredGiftLists(
			List pGiftLists,
			RepositoryItem pProfileOrg,
			String pSearchCS,
			String pSearchTerm
	) {
		List<RepositoryItem> filteredList = new ArrayList<RepositoryItem>();
		String orgId = pProfileOrg != null ? (String) pProfileOrg.getPropertyValue(ID) : null;
		for (Object giftlistObj : pGiftLists) {
			RepositoryItem giftlist = (RepositoryItem) giftlistObj;
//			if (isGiftListOrgMatchesUserOrg(giftlist, orgId)) {
			boolean matchToSearch = true;
			boolean matchToCS = true;

			if (!StringUtils.isBlank(pSearchCS)) {
				matchToCS = isGiftlistMatchesCS(giftlist, pSearchCS);
			}
			if (!StringUtils.isBlank(pSearchTerm)) {
//					matchToSearch = isGiftlistMatchesSearch(giftlist, pSearchTerm) || isGiftlistItemsMatchesSearch(giftlist, pSearchTerm);
				matchToSearch = isGiftlistMatchesSearch(giftlist, pSearchTerm);
			}

			if (matchToCS && matchToSearch) {
				filteredList.add(giftlist);
			}
//			}
		}
		return filteredList;
	}

	/**
	 * @param pFilteredGiftLists filtered gift lists
	 * @param pProfileOrg        profile org
	 * @return sorted list of giftlist data
	 */
	private List<LinkedHashMap<String, Object>> getSortedMapOfGiftList(List<RepositoryItem> pFilteredGiftLists, RepositoryItem pProfileOrg) {
		Map<String, RepositoryItem> userParentOrgSecondAddresses = null;
		if (pProfileOrg != null) {
			userParentOrgSecondAddresses = (Map<String, RepositoryItem>) pProfileOrg.getPropertyValue(SECONDARY_ADDRESSES);
		}

		List<LinkedHashMap<String, Object>> sortableLists = new ArrayList<LinkedHashMap<String, Object>>();
		for (RepositoryItem giftlist : pFilteredGiftLists) {
			if (giftlist != null) {
				vlogDebug("Setting sortable map for list - " + giftlist.toString());
				LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
				map.put(ID, giftlist.getPropertyValue(ID));
				map.put(NAME, giftlist.getPropertyValue(EVENT_NAME));
				map.put(DATE, giftlist.getPropertyValue(CREATION_DATE));
				map.put(DESCRIPTION, giftlist.getPropertyValue(DESCRIPTION));
				map.put(ITEMS, getDesiredQty(giftlist));
				map.put(GIFT_LIST_ITEMS, getGiftListItems(giftlist));
				map.put(CS, getCSAddress1(giftlist, userParentOrgSecondAddresses));

				sortableLists.add(map);
			}
		}

		sortSearchResultsHashMap(sortableLists);
		return sortableLists;
	}

	private ChangeAwareList getGiftListItems(RepositoryItem giftlist) {
		vlogDebug("Getting contents of giftlist: " + giftlist);
		if (giftlist != null) {
			ChangeAwareList giftlistItems = (ChangeAwareList) giftlist.getPropertyValue(GIFTLIST_ITEMS);
			vlogDebug("Contents of giftlist: " + giftlistItems);
			int items = 0;

			List<RepositoryItem> itemsToRemove = new ArrayList<>();

			for (Object itemObj : giftlistItems) {
				RepositoryItem item = (RepositoryItem) itemObj;

				if (getGiftListService().isItemShouldBeExcluded(item)) {
					vlogDebug("Removing item <" + item + "> from returned giftlist");
					itemsToRemove.add(item);
				}
			}

			if(!itemsToRemove.isEmpty()) {
				giftlistItems.removeAll(itemsToRemove);
			}

			vlogDebug("Final giftlist contents: " + giftlistItems);
			return giftlistItems;
		}

		return null;
	}

	/**
	 * @param giftlist user giftlist
	 * @return number of desired items
	 */
	private int getDesiredQty(RepositoryItem giftlist) {
		ChangeAwareList giftlistItems = (ChangeAwareList) giftlist.getPropertyValue(GIFTLIST_ITEMS);
		int items = 0;
		for (Object itemObj : giftlistItems) {
			RepositoryItem item = (RepositoryItem) itemObj;

			String productId = (String) item.getPropertyValue(PRODUCT_ID);
			RepositoryItem catalogProduct = null;
			String stockingType = null;

			try {
				if (productId != null) {
					catalogProduct = getGiftlistManager().getCatalogTools().findProduct(productId);

					if (catalogProduct != null) {
						stockingType = (String) catalogProduct.getPropertyValue(STOCKING_TYPE);
					}
				}
			} catch (Exception e) {
				logError("Exception raised when trying to look up productId <" + productId + "> in catalog");
			}

			if (catalogProduct == null || !isUnavailableByStocking(stockingType)) {
				items += (Long) item.getPropertyValue(QUANTITY_DESIRED);
			}
		}
		return items;
	}

	private boolean isUnavailableByStocking(String pStockingType){
		return CPSConstants.STOCKING_TYPE_OBSOLETE.equals(pStockingType)
				|| CPSConstants.STOCKING_TYPE_USEUP.equals(pStockingType)
				|| CPSConstants.STOCKING_TYPE_K.equals(pStockingType)
				|| CPSConstants.STOCKING_TYPE_X.equals(pStockingType)
				|| CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR.equalsIgnoreCase(pStockingType);
	}



	/**
	 * @param giftlist                      gitlist
	 * @param pUserParentOrgSecondAddresses parent organization addresses
	 * @return address 1 if address if found
	 */
	private String getCSAddress1(RepositoryItem giftlist, Map<String, RepositoryItem> pUserParentOrgSecondAddresses) {
		if (pUserParentOrgSecondAddresses != null) {
			String giftlistCSId = (String) giftlist.getPropertyValue(CS);
			if (!StringUtils.isBlank(giftlistCSId)) {
				for (Entry<String, RepositoryItem> address : pUserParentOrgSecondAddresses.entrySet()) {
					String addressId = (String) address.getValue().getPropertyValue(ID);
					if (addressId.equals(giftlistCSId)) {
						return (String) address.getValue().getPropertyValue(ADDRESS1);
					}
				}
			}
		}
		return "";
	}

	/**
	 * Sort the relevant Material Lists in most recent created date Order
	 *
	 * @param pListSearchResults list rep items
	 */
	private void sortSearchResultsList(List<RepositoryItem> pListSearchResults) {
		Collections.sort(
				pListSearchResults,
				new Comparator<Object>() {
					public int compare(Object pItem1, Object pItem2) {
						int result = 0;

						if ((pItem1 instanceof RepositoryItem) && (pItem2 instanceof RepositoryItem)) {
							Date date1 = (Date) ((RepositoryItem) pItem1).getPropertyValue(CREATION_DATE);
							Date date2 = (Date) ((RepositoryItem) pItem2).getPropertyValue(CREATION_DATE);

							result = compareDates(date1, date2);
						}
						return result;
					}
				}
		);
	}

	/**
	 * Sort the relevant Material Lists in most recent created date Order
	 *
	 * @param pListSearchResults list rep items
	 */
	private void sortSearchResultsHashMap(List<LinkedHashMap<String, Object>> pListSearchResults) {
		Collections.sort(pListSearchResults, new Comparator<Object>() {
			public int compare(Object pItem1, Object pItem2) {
				int result = 0;

				if ((pItem1 instanceof LinkedHashMap) && (pItem2 instanceof LinkedHashMap)) {
					Date date1 = (Date) ((LinkedHashMap) pItem1).get(CREATION_DATE);
					Date date2 = (Date) ((LinkedHashMap) pItem2).get(CREATION_DATE);

					result = compareDates(date1, date2);
				}
				return result;
			}
		});
	}

	/**
	 * @param date1 date 1
	 * @param date2 date 2
	 * @return -1 if date1 before date 2, otherwise 1
	 */
	private int compareDates(Date date1, Date date2) {
		if (isAfter()) {
			if (date1.after(date2)) {
				return 1;
			} else {
				return -1;
			}
		} else {
			if (date1.before(date2)) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	/**
	 * @param pGiftList giftlist item
	 * @param pOrgId    org id
	 * @return true if org is the same as on giftlist
	 */
	private boolean isGiftListOrgMatchesUserOrg(RepositoryItem pGiftList, String pOrgId) {
		return pOrgId != null && pOrgId.equalsIgnoreCase((String) pGiftList.getPropertyValue(ORG));
	}

	/**
	 * @param pGiftList giftlist
	 * @param pCS       CS input
	 * @return true if there are items in gift list matching the search
	 */
	private boolean isGiftlistMatchesCS(RepositoryItem pGiftList, String pCS) {
		String cs = (String) pGiftList.getPropertyValue(CS);
		if (StringUtils.isBlank(cs)) {
			return false;
		}
		return cs.toLowerCase().equals(pCS.toLowerCase());
	}

	/**
	 * @param pGiftList giftlist
	 * @param pSearch   search input
	 * @return true if there are items in gift list matching the search
	 */
	private boolean isGiftlistMatchesSearch(RepositoryItem pGiftList, String pSearch) {
		String giftListName = (String) pGiftList.getPropertyValue(EVENT_NAME);
		return (isStringContainsString(giftListName, pSearch));
	}

	/**
	 * @param pGiftList giftlist
	 * @param pSearch   search input
	 * @return true if there are items in gift list matching the search
	 */
	private boolean isGiftlistItemsMatchesSearch(RepositoryItem pGiftList, String pSearch) {
		ChangeAwareList giftlistItems = (ChangeAwareList) pGiftList.getPropertyValue(GIFTLIST_ITEMS);
		for (Object giftItem : giftlistItems) {
			RepositoryItem item = (RepositoryItem) giftItem;
			if (isItemSuitableForSearch(item, pSearch)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * @param pGiftItem gift item
	 * @param pSearch   search input
	 * @return true if suites for search and other settings
	 */
	private boolean isItemSuitableForSearch(RepositoryItem pGiftItem, String pSearch) {
		boolean isItemMatchesSearchResult = isItemMatchesSearchResult(pGiftItem, pSearch);
		boolean isItemShouldBeExcluded = getGiftListService().isItemShouldBeExcluded(pGiftItem);

		return isItemMatchesSearchResult && !isItemShouldBeExcluded;
	}

	/**
	 * @param pGiftItem gift item
	 * @param pSearch   search input
	 * @return true if items properties matchs
	 */
	private boolean isItemMatchesSearchResult(RepositoryItem pGiftItem, String pSearch) {
		try {
			String productId = (String) pGiftItem.getPropertyValue(PRODUCT_ID);
			String catalogRefId = (String) pGiftItem.getPropertyValue(CATALOG_REF_ID);
			String giftItemName = (String) pGiftItem.getPropertyValue(DISPLAY_NAME);
			String giftItemDesc = (String) pGiftItem.getPropertyValue(DESCRIPTION);

			RepositoryItem product = getGiftlistManager().getCatalogTools().findProduct(productId);

			String productName = (String) product.getPropertyValue(DISPLAY_NAME);
			String productDescription = (String) product.getPropertyValue(DESCRIPTION);

			return (
					isStringContainsString(giftItemName, pSearch) ||
							isStringContainsString(giftItemDesc, pSearch) ||
							isStringContainsString(productId, pSearch) ||
							isStringContainsString(catalogRefId, pSearch) ||
							isStringContainsString(productName, pSearch) ||
							isStringContainsString(productDescription, pSearch)
			);

		} catch (Exception e) {
			vlogError(e, "Unable to check gift item for search input");
		}
		return false;
	}

	/**
	 * @param pStr1 string 1
	 * @param pStr2 string 2
	 * @return true if string 1 contains string 2
	 */
	private boolean isStringContainsString(String pStr1, String pStr2) {
		if (StringUtils.isBlank(pStr1) || StringUtils.isBlank(pStr2)) {
			return false;
		}
		return pStr1.toLowerCase().contains(pStr2.toLowerCase());
	}


	//------------------------------------------------------------------------------------------------------------------

	public Profile getProfile() {
		return (Profile) ServletUtil.getCurrentUserProfile();
	}

	public GiftlistManager getGiftlistManager() {
		return giftlistManager;
	}

	public void setGiftlistManager(GiftlistManager giftlistManager) {
		this.giftlistManager = giftlistManager;
	}

	public GiftListService getGiftListService() {
		return mGiftListService;
	}

	public void setGiftListService(GiftListService pGiftListService) {
		mGiftListService = pGiftListService;
	}
}