package com.cps.droplet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.util.CPSConstants;

public class UserContainsApproverDroplet extends DynamoServlet {
	public static ParameterName USER_ID = ParameterName.getParameterName("userId");
	public static ParameterName APPROVER_ID = ParameterName.getParameterName("approverId");
	
	public static ParameterName TRUE = ParameterName.getParameterName("true");
	public static ParameterName FALSE = ParameterName.getParameterName("false");
	
	private Repository profileRepository;
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("UserContainsApproverDroplet.start");

		ParameterName outputParam = FALSE;
		String userId = pRequest.getParameter(USER_ID);
		String approverId = pRequest.getParameter(APPROVER_ID);

		vlogDebug("UserId - "+userId+" | ApproverId - "+approverId);

		RepositoryItem user = getItemById(CPSConstants.B2B_ITEM_DESCRIPTOR, userId);

		vlogDebug("User - "+user);

		if (user != null && approverId != null) {
			List<RepositoryItem> approvers = (List<RepositoryItem>) user.getPropertyValue(CPSConstants.APPROVERS);
			vlogDebug("List of users approvers: "+approvers);

			if (approvers != null) {
				for (RepositoryItem approver : approvers) {
					if (approverId.equalsIgnoreCase(approver.getRepositoryId())) {
						outputParam = TRUE;
						break;
					}
				}
			}
		}
		
		pRequest.serviceLocalParameter(outputParam, pRequest, pResponse);
		return;
	}
	
	private RepositoryItem getItemById(String itemDescriptor, String userId) {
		RepositoryItem [] item=null;
		try {
			final RepositoryItemDescriptor roleDesc = getProfileRepository().getItemDescriptor(itemDescriptor);
			final RepositoryView view = roleDesc.getRepositoryView();
			final QueryBuilder builder = view.getQueryBuilder();
			final QueryExpression name = builder.createPropertyQueryExpression(CPSConstants.ID);
			final QueryExpression itemId = builder.createConstantQueryExpression(userId);
			final Query queryItem =builder.createComparisonQuery(name, itemId, QueryBuilder.EQUALS);
			item = view.executeQuery(queryItem );
		} catch (final RepositoryException e) {
			logError("getItem(): "+e);
		} catch(final NullPointerException ne){
			logError("getItem(): "+ne);
		}
		return item!=null?item[0]:null;
	}

	public Repository getProfileRepository() {
		return profileRepository;
	}

	public void setProfileRepository(Repository profileRepository) {
		this.profileRepository = profileRepository;
	}
}