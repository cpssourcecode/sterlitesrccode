package com.cps.droplet;
import java.io.IOException;

import javax.servlet.ServletException;

import atg.nucleus.naming.ParameterName;
import atg.repository.MutableRepository;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.userprofiling.CPSProfileTools;
public class CPSProductFromIdentifierDroplet extends DynamoServlet{
	private MutableRepository productCatalog;
	private CPSProfileTools profileTools;
	//private MutableRepository profileAdapterRepository;
	//private MutableRepository productCatalog;
	//private PNShippingGroupManager shippingGroupManager;
	public static final ParameterName IDENTIFIER_ITEM=ParameterName.getParameterName("idItem"); //identifier2NdItem from orderStatus webservice, use to search for product
	//public static final ParameterName ACCOUNT_NUMBER=ParameterName.getParameterName("orgId"); //orgId for this admin
	//use to call query order lines webservice for all data and create order in ATG
	public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException{
		String idItem=(String)req.getObjectParameter(IDENTIFIER_ITEM);
		Object[] params={""+idItem.trim()};
		String query="thirdItemNumber=?0"; //always has value
		//String query2="item_num=?0"; //seems to always be null

		RepositoryItem[] foundProduct=profileTools.queryRepository(params,query,"product",getProductCatalog());
		//RepositoryItem[] foundProduct2=profileTools.queryRepository(params,query2,"product",getProductCatalog());
		if(foundProduct!=null && foundProduct.length>0){
			req.setParameter("product", foundProduct[0]);
			req.serviceLocalParameter("outputProduct", req, res);
		}
		//if(foundProduct2!=null && foundProduct2.length>0){
		//	req.setParameter("product2", foundProduct2[0]);
		//	req.serviceLocalParameter("outputProduct2",req,res);
		//}
	}
	public MutableRepository getProductCatalog() {
		return productCatalog;
	}
	public void setProductCatalog(MutableRepository productCatalog) {
		this.productCatalog = productCatalog;
	}
	public CPSProfileTools getProfileTools(){
		return profileTools;
	}
	public void setProfileTools(CPSProfileTools profileTools){
		this.profileTools=profileTools;
	}
}