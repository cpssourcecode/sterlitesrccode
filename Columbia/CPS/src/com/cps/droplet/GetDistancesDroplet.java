package com.cps.droplet;

import static com.cps.util.CPSConstants.POSTAL_CODE;
import static com.cps.util.CPSConstants.SELECTED_CS;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import com.cps.util.StoreTools;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

/**
 * 
 * @author Steve Neverov
 *
 */
public class GetDistancesDroplet extends DynamoServlet {

	private static final ParameterName OUTPUT_OPARAM = ParameterName.getParameterName("output");
	private static final ParameterName ERROR_OPARAM = ParameterName.getParameterName("error");
	private static final ParameterName EMPTY_OPARAM = ParameterName.getParameterName("empty");

	private static final String DISTANCES = "distances";

	public Profile getProfile() {
		return (Profile) ServletUtil.getCurrentUserProfile();
	}

	private StoreTools mStoreTools;

	public StoreTools getStoreTools() {
		return mStoreTools;
	}

	public void setStoreTools(StoreTools pStoreTools) {
		mStoreTools = pStoreTools;
	}

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		Profile profile = getProfile();
		if (profile != null) {
			RepositoryItem selectedCS = (RepositoryItem) profile.getPropertyValue(SELECTED_CS);
			if (selectedCS != null) {
				String postalCode = (String) selectedCS.getPropertyValue(POSTAL_CODE);
				if (!StringUtils.isBlank(postalCode)) {
					try {
						Map<String, Float> distances = getStoreTools().findSortedStoreDistances(postalCode);
						if (distances != null && distances.size() > 0) {
							pRequest.setParameter(DISTANCES, distances);
							pRequest.serviceLocalParameter(OUTPUT_OPARAM, pRequest, pResponse);
						} else {
							pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
						}
					} catch (RepositoryException e) {
						if (isLoggingError()) {
							logError(e);
						}
						pRequest.serviceLocalParameter(ERROR_OPARAM, pRequest, pResponse);
					}
				}
			}
		}

	}

}
