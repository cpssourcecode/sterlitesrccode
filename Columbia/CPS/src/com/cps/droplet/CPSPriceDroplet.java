package com.cps.droplet;

import static com.cps.util.CPSConstants.SHOPPING_CART_PATH;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.pricing.service.CPSMultiPriceService;
import com.cps.service.CPSExternalProductService;
import com.cps.userprofiling.CPSProfileTools;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import cps.commerce.pricing.priceLists.CPSPriceListManager;

/**
 * CPSPriceDroplet
 *
 * @author David Mednikov
 */
public class CPSPriceDroplet extends DynamoServlet {

    /**
     * component name for debugging
     */
    public static ParameterName PRODUCT_ID = ParameterName.getParameterName("productId");
    public static ParameterName QTY = ParameterName.getParameterName("quantity");
    public static ParameterName CUSTOMER_ID = ParameterName.getParameterName("customerId");
    public static ParameterName PROFILE = ParameterName.getParameterName("profile");
    private static final String PARAM_NAME_ORDER = "order";

    private TransactionManager transactionManager;
    private CPSMultiPriceService mMultiPriceService;
    private CPSExternalProductService externalProductService;
    private CPSProfileTools profileTools;
    private CPSPriceListManager priceListManager;

    /**
     * @return the profileTools
     */
    public CPSProfileTools getProfileTools() {
        return profileTools;
    }

    /**
     * @param profileTools
     *            the profileTools to set
     */
    public void setProfileTools(CPSProfileTools profileTools) {
        this.profileTools = profileTools;
    }

    /**
     * @return the priceListManager
     */
    public CPSPriceListManager getPriceListManager() {
        return priceListManager;
    }

    /**
     * @param priceListManager
     *            the priceListManager to set
     */
    public void setPriceListManager(CPSPriceListManager priceListManager) {
        this.priceListManager = priceListManager;
    }

    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        // check pricing client enabled call it otherwise call dummy
        if (externalProductService.isPricingWebserviceEnable()) {
            Profile profile = (Profile) req.getObjectParameter(PROFILE);
            Order order = (Order) req.getObjectParameter(PARAM_NAME_ORDER);
            if (order != null) {
                getMultiPriceService().updateFullOrderPrices(order, profile);
                req.serviceLocalParameter("emptyPrice", req, res);
            } else {
                priceOneItem(req, res);
            }
        } else {
            // hide price and add to cart
            req.serviceLocalParameter("emptyPrice", req, res);
        }
    }

    private void priceOneItem(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        // Long qty = (Long)req.getObjectParameter(QTY);
        // Profile profile = (Profile) req.getObjectParameter(PROFILE);
        Profile profile = (Profile) req.resolveName("/atg/userprofiling/Profile");
        String productId = req.getParameter(PRODUCT_ID);
        String customerId = req.getParameter(CUSTOMER_ID);
        try {
            OrderHolder shoppingCart = (OrderHolder) req.resolveName(SHOPPING_CART_PATH);
            CPSOrderImpl order = (CPSOrderImpl) shoppingCart.getCurrent();
            double unitPrice = 0.0;
            // if the user is either anonymous or does not contain price overrides, then get the price from the PriceList and not webservice
            String priceMatrix = getProfileTools().getPriceMatrixLevelForGivenProfileAddress(profile, order);
            if (priceMatrix != null) {
                // the profile qualifies for fetching prices from the ATG PriceList Repository.

                String skuId = productId; // in CPS, both the productId and SKUid have one-to-one mapping

                unitPrice = getPriceListManager().getOnePrice(productId, skuId, profile, priceMatrix, order);
            } else {
                unitPrice = StringUtils.isNotBlank(customerId)
                                ? externalProductService.getCustomerItemPrice(productId, customerId, profile, (CPSOrderImpl) shoppingCart.getCurrent())
                                : externalProductService.getCustomerItemPrice(productId, profile, (CPSOrderImpl) shoppingCart.getCurrent());
            }
            if (unitPrice > 0) {
                req.setParameter("currentPrice", unitPrice);
                req.serviceLocalParameter("outputPrice", req, res);
            } else {
                // hide price and add to cart
                req.serviceLocalParameter("emptyPrice", req, res);
            }

        } catch (Exception ex) {
            if (isLoggingError()) {
                logError(ex);
            }
            req.serviceLocalParameter("emptyPrice", req, res);
        }
    }

    public CPSExternalProductService getExternalProductService() {
        return externalProductService;
    }

    public void setExternalProductService(CPSExternalProductService externalProductService) {
        this.externalProductService = externalProductService;
    }

    public TransactionManager getTransactionManager() {
        return transactionManager;
    }

    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public CPSMultiPriceService getMultiPriceService() {
        return mMultiPriceService;
    }

    public void setMultiPriceService(CPSMultiPriceService pMultiPriceService) {
        mMultiPriceService = pMultiPriceService;
    }

}