package com.cps.version;

/**
 * This class store build version for JS/CSS assets.
 * 
 * @author Vasili Ivus
 * 
 */
public class CPSBuild {

	private String mVersion;

	public String getVersion() {
		return (null == mVersion) ? "" : mVersion;
	}

	public void setVersion(final String pVersion) {
		mVersion = pVersion;
	}
}