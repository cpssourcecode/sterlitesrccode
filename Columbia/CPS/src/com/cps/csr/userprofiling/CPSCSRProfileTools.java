package com.cps.csr.userprofiling;

import atg.commerce.CommerceException;
import atg.commerce.order.OrderHolder;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;
import atg.userprofiling.Profile;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;

import java.util.Collection;
import java.util.Iterator;

public class CPSCSRProfileTools extends CPSProfileTools {

	private boolean mMergeCSROrders;
	private UserDirectory mUserDirectory;

	public RepositoryItem findByIdOnly(String userId) {
		RepositoryItem user = null;
		try {
			user = getProfileRepository().getItem(userId, "user");
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		return user;
	}

	public void loadShoppingCarts(RepositoryItem pProfile, OrderHolder pShoppingCart) throws CommerceException {
		if (pProfile != null && pShoppingCart != null) {
			CPSOrderImpl srcOrder = (CPSOrderImpl) pShoppingCart.getCurrent(false);
			if (srcOrder != null && srcOrder.isUseProfileOrganization()) {
				srcOrder.setOrganizationId((String) pProfile.getPropertyValue(CPSConstants.CSR_SELECTED_ORG));
			}
		}
		super.loadShoppingCarts(pProfile, pShoppingCart);
	}

	public boolean isCSRUser(Profile pProfile) {
		boolean isCSRUser = false;

		User user = null;

		if (getUserDirectory() != null) {
			user = getUserDirectory().findUserByPrimaryKey(pProfile.getRepositoryId());
		}

		if (user != null) {
			Collection collection = user.getAssignedRoles();
			if (collection != null) {
				for (Iterator iterator = collection.iterator(); iterator.hasNext(); ) {
					Role role = (Role) iterator.next();
					if (role != null) {
						if (role.getName().equalsIgnoreCase(CPSConstants.ROLE_CSR_ADMIN)
								|| role.getName().equalsIgnoreCase(CPSConstants.ROLE_SITE_ADMIN)) {
							isCSRUser = true;
							break;
						}
					}
				}
			}
		}

		return isCSRUser;
	}

	public UserDirectory getUserDirectory() {
		return mUserDirectory;
	}

	public void setUserDirectory(UserDirectory pUserDirectory) {
		mUserDirectory = pUserDirectory;
	}

	public boolean isMergeCSROrders() {
		return mMergeCSROrders;
	}

	public void setMergeCSROrders(boolean pMergeCSROrders) {
		mMergeCSROrders = pMergeCSROrders;
	}


}