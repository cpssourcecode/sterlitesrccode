package com.cps.csr.userprofiling;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.util.RepeatingRequestMonitor;
import com.cps.userprofiling.CPSProfileFormHandler;
import com.cps.csr.util.CPSCSRLoggingManager;
import com.cps.csr.util.CPSCSRSessionBean;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSFormUtils;

import atg.adapter.gsa.ChangeAwareSet;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;
import atg.userprofiling.ProfileSwapEvent;

public class CPSCSRProfileFormHandler extends CPSProfileFormHandler {

	private CPSCSRSessionBean csrSessionBean;
	private CPSCSRProfileTools csrProfileTools;
	private CPSCSRLoggingManager csrLoggingManager;
	private UserDirectory userDirectory;

	private String changeAccountSuccessURL;
	private String selectAccountSuccessURL;
	private String changeUserSuccessURL;
	private String logoutCSRSuccessURL;
	private String impersonateUserSuccessURL;

	private String orgId;
	private String searchEmail;
	private String searchLastName;
	private String searchJDEAcct;
	private String impersonatedUserId;
	private String csrUserId;

	/**
	 * Called when salesRep user is going to select a new organization to link
	 * to
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleChangeAccount(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        vlogDebug("handleChangeAccount.start");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSCSRProfileFormHandler.handleChangeAccount";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				getCsrLoggingManager().logAction(6, 2, (RepositoryItem) getProfile().getPropertyValue(CPSConstants.SELECTED_ORG), getProfile().getRepositoryId(), null, "Disconnected with organization.");

				// Set the selected org to null
				getProfile().setPropertyValue(CPSConstants.SELECTED_ORG, null);

				// Reset correct cs
				RepositoryItem parentOrg = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
				if (parentOrg != null) {
					Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) parentOrg.getPropertyValue(CPSConstants.SECONDARY_ADDRESSES);
					if (secondaryAddresses != null) {
                        vlogDebug("org has secondary addresses: " + secondaryAddresses.size() + " " + secondaryAddresses);
                        if (secondaryAddresses.size() > 1) {
							if (!StringUtils.isBlank(getSelectAccountSuccessURL())) {
								if (!getSelectAccountSuccessURL().contains("?")) {
									setSelectAccountSuccessURL(getSelectAccountSuccessURL().concat(getLoginURLParamQuestion()));
								} else {
									setSelectAccountSuccessURL(getSelectAccountSuccessURL().concat(getLoginURLParamAnd()));
								}
							}
						}
						for (Entry<String, RepositoryItem> csEntry : secondaryAddresses.entrySet()) {
							getProfile().setPropertyValue(CPSConstants.SELECTED_CS, csEntry.getValue());
							break;
						}
					}
				} else {
					getProfile().setPropertyValue(CPSConstants.SELECTED_CS, null);
				}

				// Seem to be having trouble passing in params, so if null, try to get
				// from request
				if (getChangeAccountSuccessURL() == null) {
					setChangeAccountSuccessURL(pRequest.getParameter("changeAccountSuccessURL"));
				}
			} catch (Exception e) {
				if(isLoggingError()){
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e){
						if(isLoggingError()){
							logError(e);
						}
					}
				}
			}
		}

		return checkFormRedirect(getChangeAccountSuccessURL(), null, pRequest, pResponse);
	}

	/**
	 * This links a newly selected org to the sales rep
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleSelectAccount(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        vlogDebug("handleSelectAccount.start");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSCSRProfileFormHandler.handleSelectAccount";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				// Try to get org item to connect to
				RepositoryItem selectedOrg = null;
				if (getOrgId() == null) {
					setOrgId(pRequest.getParameter("orgId"));
				}
				if (getSelectAccountSuccessURL() == null) {
					setSelectAccountSuccessURL(pRequest.getParameter("selectAccountSuccessURL"));
				}
                vlogDebug("passed in org id: " + getOrgId());
                vlogDebug("Profile: " + getProfile());
                if (!StringUtils.isBlank(getOrgId())) {
					// Loop through ancestor organizations and match ids to passed in
					// org
					ChangeAwareSet ancestors = (ChangeAwareSet) getProfile().getPropertyValue(CPSConstants.ORG_LIST);
                    vlogDebug("users ancestor orgs: " + ancestors);
                    for (Object orgObj : ancestors) {
						RepositoryItem org = (RepositoryItem) orgObj;
						if (getOrgId().equalsIgnoreCase((String) org.getPropertyValue(CPSConstants.ID))) {
							// Org matched, set and break out of loop
							getProfile().setPropertyValue(CPSConstants.SELECTED_ORG, org);
							selectedOrg = org;
							break;
						}
					}
                    vlogDebug("selected org: " + selectedOrg);
                    if (selectedOrg != null) {
						// Found an org, see if need to display cs modal
						Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) selectedOrg.getPropertyValue(CPSConstants.SECONDARY_ADDRESSES);
						if (secondaryAddresses != null) {
                            vlogDebug("org has secondary addresses: " + secondaryAddresses.size() + " " + secondaryAddresses);
                            if (secondaryAddresses.size() > 1) {
								if (!StringUtils.isBlank(getSelectAccountSuccessURL())) {
									if (!getSelectAccountSuccessURL().contains("?")) {
										setSelectAccountSuccessURL(getSelectAccountSuccessURL().concat(getLoginURLParamQuestion()));
									} else {
										setSelectAccountSuccessURL(getSelectAccountSuccessURL().concat(getLoginURLParamAnd()));
									}
								}
							}
							for (Entry<String, RepositoryItem> csEntry : secondaryAddresses.entrySet()) {
								getProfile().setPropertyValue(CPSConstants.SELECTED_CS, csEntry.getValue());
								break;
							}
						}
					}
				}

				getCsrLoggingManager().logAction(5, 2, selectedOrg, getProfile().getRepositoryId(), null, "Connected with organization.");
			} catch (TransactionDemarcationException e) {
				if(isLoggingError()){
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null){
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if(isLoggingError()){
							logError(e);
						}
					}
				}
			}
		}

        vlogDebug("redirect url: " + getSelectAccountSuccessURL());
        return checkFormRedirect(getSelectAccountSuccessURL(), null, pRequest, pResponse);
	}

	/**
	 * Handles the logout action taken by a csrAdmin to logout of the
	 * impersonated user and resume csr user
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws IOException
	 */
	public boolean handleLogoutImpersonatedUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSCSRProfileFormHandler.handleLogoutImpersonatedUser";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			// Time to logout impersonated user
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				RepositoryItem preLogoutDataSource = getCurrentDataSource();
				// Get the csr id to log back in with
				String csrUser = (String) preLogoutDataSource.getPropertyValue("CSRUserId");
				// Set the csr id to null of the user object so that user won't be
				// considered a csrAdmin
				((MutableRepositoryItem) preLogoutDataSource).setPropertyValue(CPSConstants.CSR_USER_ID, null);
				setCsrUserId(csrUser);
				// call regular pre logout
				preLogoutUser(pRequest, pResponse);

				sendProfileSwapEvent(ProfileSwapEvent.LOGOUT, preLogoutDataSource, getCurrentDataSource());

				// call post logout
				postLogoutUser(pRequest, pResponse);

				// This should've been an ajax submission, so return ajax response
				try {
					if (!getFormError()) {
						vlogDebug("no errors, return success");
                        addAjaxSuccessResponse(pResponse, csrUser);
					} else {
                        vlogDebug("errors, return errors for display");
                        Set<String> errorProperties = new HashSet<String>();
						for (int i = 0; i < getFormExceptions().size(); i++) {
							DropletException exc = (DropletException) getFormExceptions().get(i);
							errorProperties.add(exc.getErrorCode());
						}
						CPSFormUtils.addAjaxErrorResponse(this, pResponse, errorProperties);
					}
				} catch (JSONException e) {
					vlogError(e, "Error");
				}
				getCsrLoggingManager().logAction(4, 1, (RepositoryItem) preLogoutDataSource.getPropertyValue(CPSConstants.PARENT_ORG), getCsrUserId(), preLogoutDataSource.getRepositoryId(),
						"Logged out of impersonated user.");
				return false;
			} catch (Exception e) {
				vlogError(e, "Error");
			} finally {
				try {
					if (tm != null) {
						td.end();
					}
				} catch (TransactionDemarcationException e) {
					vlogError(e, "Error");
				}

				if(rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return false;
	}

	/**
	 * Called when csrAdmin searches for users
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleSearchUsers(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        vlogDebug("Searching for users");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSCSRProfileFormHandler.handleSearchUsers";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				getCsrSessionBean().setSearchResults(null);
				if (StringUtils.isBlank(getSearchEmail())) {
					setSearchEmail(pRequest.getParameter("searchEmail"));
				}
				if (StringUtils.isBlank(getSearchLastName())) {
					setSearchLastName(pRequest.getParameter("searchLastName"));
				}
				if (StringUtils.isBlank(getSearchJDEAcct())) {
					setSearchJDEAcct(pRequest.getParameter("searchJDEAcct"));
				}

				// Make sure we have entries
				validateSearchEntries(pRequest);

				// If we have entries
				if (!getFormError()) {
                    vlogDebug("Search params entered");
                    // Gather search parameters
					Map<String, String> params = new HashMap<String, String>();
					if (!StringUtils.isBlank(getSearchEmail())) {
						params.put(CPSConstants.EMAIL, getSearchEmail());
					}
					if (!StringUtils.isBlank(getSearchLastName())) {
						params.put(CPSConstants.LAST_NAME, getSearchLastName());
					}
					if (!StringUtils.isBlank(getSearchJDEAcct())) {
						params.put(CPSConstants.PARENT_ORG + "." + CPSConstants.JDE_ACCOUNT_NUM, getSearchJDEAcct());
					}
					// Query the user repository for users with the matched search
					// params
					RepositoryItem[] users = getCsrProfileTools().queryRepository("user", params, "AND");
                    vlogDebug("Search results: " + Arrays.toString(users));
                    // For each user found, add to list and store in session bean
					if (users != null && users.length > 0) {
						List<Map<String, String>> searchResults = new ArrayList<Map<String, String>>();
						for (int i = 0; i < users.length; i++) {
							if ((Integer) users[i].getPropertyValue(CPSConstants.USER_TYPE) != 3) {
                                vlogDebug("adding to list: " + users[i]);
                                searchResults.add(createUserMap(users[i]));
							}
						}
						if (!searchResults.isEmpty()) {
							getCsrSessionBean().setSearchResults(searchResults);
						} else {
							createError(CPSErrorCodes.ERR_CSR_NO_RESULTS, null, pRequest);
							getCsrSessionBean().setSearchResults(null);
						}
					} else {
						// No search results found, return error
						createError(CPSErrorCodes.ERR_CSR_NO_RESULTS, null, pRequest);
						getCsrSessionBean().setSearchResults(null);
					}
				}
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		// This is an ajax request, return ajax response
		try {
			if (!getFormError()) {
                vlogDebug("no errors, return success");
                CPSFormUtils.addAjaxSuccessResponse(pResponse);
			} else {
                vlogDebug("errors, return errors for display");
                Set<String> errorProperties = new HashSet<String>();
				for (int i = 0; i < getFormExceptions().size(); i++) {
					DropletException exc = (DropletException) getFormExceptions().get(i);
					errorProperties.add(exc.getErrorCode());
				}
				CPSFormUtils.addAjaxErrorResponse(this, pResponse, errorProperties);
			}
		} catch (JSONException e) {
			logError(e);
		}

		return false;
	}

	private Map<String, String> createUserMap(RepositoryItem profile) {
		Map<String, String> userMap = new HashMap<String, String>();

		userMap.put(CPSConstants.FIRST_NAME, (String) profile.getPropertyValue(CPSConstants.FIRST_NAME));
		userMap.put(CPSConstants.LAST_NAME, (String) profile.getPropertyValue(CPSConstants.LAST_NAME));
		userMap.put(CPSConstants.EMAIL, (String) profile.getPropertyValue(CPSConstants.EMAIL));
		userMap.put(CPSConstants.ID, (String) profile.getRepositoryId());

        vlogDebug("User Directory: " + getUserDirectory());

        User user = getUserDirectory().findUserByPrimaryKey((String) profile.getRepositoryId());
		String rolesString = "";
		if (user != null) {
			Collection collection = user.getAssignedRoles();
			for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
				Role role = (Role) iterator.next();
				if (role != null) {
                    vlogDebug("Role name: " + role.getName());
                    if (StringUtils.isBlank(rolesString)) {
						rolesString = formatRole(role.getName());
					} else {
						rolesString += ", " + formatRole(role.getName());
					}
				}
			}
		}

		userMap.put(CPSConstants.ROLES_PRPTY, rolesString);

		return userMap;
	}

	protected String formatRole(String role) {
		String formatedRole = role;
		if (!StringUtils.isBlank(role)) {
			if (role.equals("salesRep")) {
				formatedRole = "Sales Rep";
			}
			if (role.equals("csrAdmin")) {
				formatedRole = "CSR";
			}
			if (role.equals("finance")) {
				formatedRole = "Finance";
			}
			if (role.equals("buyerWithoutSL")) {
				formatedRole = "Buyer Without Spending Limit";
			}
			if (role.equals("buyerWithSL")) {
				formatedRole = "Buyer With Spending Limit";
			}
		}

		return formatedRole;
	}

	/**
	 * If no search entries were entered, add error
	 *
	 * @param pRequest
	 */
	private void validateSearchEntries(DynamoHttpServletRequest pRequest) {
		if (StringUtils.isBlank(getSearchEmail()) && StringUtils.isBlank(getSearchLastName()) && StringUtils.isBlank(getSearchJDEAcct())) {
			createError(CPSErrorCodes.ERR_CSR_MISSING_SEARCH, null, pRequest);
		}
	}

	/**
	 * Override ajax response to add csrId to data object, used to log csr back
	 * in
	 *
	 * @param pResponse
	 * @param pUserParam
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxSuccessResponse(DynamoHttpServletResponse pResponse, String pUserParam) throws IOException, JSONException {
		pResponse.setContentType("application/json");

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put("error", "false");
		responseJson.put("user", pUserParam);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}

	protected RepositoryItem getCurrentDataSource() {
		if (getProfile() != null)
			return getProfile().getDataSource();
		return getProfileItem();
	}

	public String getChangeAccountSuccessURL() {
		return changeAccountSuccessURL;
	}

	public void setChangeAccountSuccessURL(String changeAccountSuccessURL) {
		this.changeAccountSuccessURL = changeAccountSuccessURL;
	}

	public String getSelectAccountSuccessURL() {
		return selectAccountSuccessURL;
	}

	public void setSelectAccountSuccessURL(String selectAccountSuccessURL) {
		this.selectAccountSuccessURL = selectAccountSuccessURL;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getChangeUserSuccessURL() {
		return changeUserSuccessURL;
	}

	public void setChangeUserSuccessURL(String changeUserSuccessURL) {
		this.changeUserSuccessURL = changeUserSuccessURL;
	}

	public String getLogoutCSRSuccessURL() {
		return logoutCSRSuccessURL;
	}

	public void setLogoutCSRSuccessURL(String logoutCSRSuccessURL) {
		this.logoutCSRSuccessURL = logoutCSRSuccessURL;
	}

	public String getImpersonateUserSuccessURL() {
		return impersonateUserSuccessURL;
	}

	public void setImpersonateUserSuccessURL(String impersonateUserSuccessURL) {
		this.impersonateUserSuccessURL = impersonateUserSuccessURL;
	}

	public String getSearchEmail() {
		return searchEmail;
	}

	public void setSearchEmail(String searchEmail) {
		this.searchEmail = searchEmail;
	}

	public String getSearchLastName() {
		return searchLastName;
	}

	public void setSearchLastName(String searchLastName) {
		this.searchLastName = searchLastName;
	}

	public String getSearchJDEAcct() {
		return searchJDEAcct;
	}

	public void setSearchJDEAcct(String searchJDEAcct) {
		this.searchJDEAcct = searchJDEAcct;
	}

	public CPSCSRSessionBean getCsrSessionBean() {
		return csrSessionBean;
	}

	public void setCsrSessionBean(CPSCSRSessionBean csrSessionBean) {
		this.csrSessionBean = csrSessionBean;
	}

	public CPSCSRProfileTools getCsrProfileTools() {
		return csrProfileTools;
	}

	public void setCsrProfileTools(CPSCSRProfileTools csrProfileTools) {
		this.csrProfileTools = csrProfileTools;
	}

	public String getImpersonatedUserId() {
		return impersonatedUserId;
	}

	public void setImpersonatedUserId(String impersonatedUserId) {
		this.impersonatedUserId = impersonatedUserId;
	}

	public String getCsrUserId() {
		return csrUserId;
	}

	public void setCsrUserId(String csrUserId) {
		this.csrUserId = csrUserId;
	}

	public CPSCSRLoggingManager getCsrLoggingManager() {
		return csrLoggingManager;
	}

	public void setCsrLoggingManager(CPSCSRLoggingManager csrLoggingManager) {
		this.csrLoggingManager = csrLoggingManager;
	}

	public UserDirectory getUserDirectory() {
		return userDirectory;
	}

	public void setUserDirectory(UserDirectory userDirectory) {
		this.userDirectory = userDirectory;
	}
}