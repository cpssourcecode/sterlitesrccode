package com.cps.csr.util;

import java.util.Calendar;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

public class CPSCSRLoggingManager extends GenericService{
	
	/**
	 * This class handles the logging events that get triggered by csrs or sales reps
	 * Will create new repository item and store in the logging repository
	 */
	
	private MutableRepository csrLoggingRepository;
	private String csrLogType;
	private MutableRepository profileRepository;
	
	private static String TIME_LOGGED = "timeLogged";
	private static String ACTION_TYPE = "actionType";
	private static String ACTION_DETAIL = "actionDetail";
	private static String USER_TYPE = "userType";
	private static String ACCOUNT = "account";
	private static String USER = "user";
	private static String IMPERSONATED = "impersonated";
	
	public void logAction(int actionType, int userType, RepositoryItem org, String userId, String impersonatedUserId, String message){
		
		if (!StringUtils.isBlank(message)){
			try {
				MutableRepository repository = getCsrLoggingRepository();
				MutableRepositoryItem logItem = repository.createItem(getCsrLogType());
				
				Calendar c = Calendar.getInstance();
				
				MutableRepositoryItem user = null;
				if (!StringUtils.isBlank(userId)){
					user = getProfileRepository().getItemForUpdate(userId, "user");
				}
				MutableRepositoryItem impersonatedUser = null;
				if (!StringUtils.isBlank(impersonatedUserId)){
					impersonatedUser = getProfileRepository().getItemForUpdate(impersonatedUserId, "user");
				}
				
				// log time, action type, and message
				logItem.setPropertyValue(TIME_LOGGED, c.getTime());
				logItem.setPropertyValue(ACTION_TYPE, actionType);
				logItem.setPropertyValue(ACTION_DETAIL, message);
				logItem.setPropertyValue(USER_TYPE, userType);
				logItem.setPropertyValue(ACCOUNT, org);
				logItem.setPropertyValue(USER, user);
				logItem.setPropertyValue(IMPERSONATED, impersonatedUser);
				
				repository.addItem(logItem);
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		}else{
			logError("Missing required message! Passed info: actionType - "+actionType+" userType - "+userType+" org - "+org+
					" user - "+userId+" impersonatedUser - "+impersonatedUserId+" message - "+message);
		}
		
	}

	public String getCsrLogType() {
		return csrLogType;
	}

	public void setCsrLogType(String csrLogType) {
		this.csrLogType = csrLogType;
	}

	public MutableRepository getCsrLoggingRepository() {
		return csrLoggingRepository;
	}

	public void setCsrLoggingRepository(MutableRepository csrLoggingRepository) {
		this.csrLoggingRepository = csrLoggingRepository;
	}

	public MutableRepository getProfileRepository() {
		return profileRepository;
	}

	public void setProfileRepository(MutableRepository profileRepository) {
		this.profileRepository = profileRepository;
	}
}