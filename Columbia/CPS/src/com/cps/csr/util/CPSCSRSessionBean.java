package com.cps.csr.util;

import java.util.List;
import java.util.Map;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

public class CPSCSRSessionBean extends GenericService {
	
	private String impersonatedUser;
	private List<Map<String, String>> searchResults;
	private String csrUser;
	
	
	public String getImpersonatedUser() {
		return impersonatedUser;
	}
	public void setImpersonatedUser(String impersonatedUser) {
		this.impersonatedUser = impersonatedUser;
	}
	public List<Map<String, String>> getSearchResults() {
		return searchResults;
	}
	public void setSearchResults(List<Map<String, String>> searchResults) {
		this.searchResults = searchResults;
	}
	public String getCSRUser() {
		return csrUser;
	}
	public void setCSRUser(String csrUser) {
		this.csrUser = csrUser;
	}
}