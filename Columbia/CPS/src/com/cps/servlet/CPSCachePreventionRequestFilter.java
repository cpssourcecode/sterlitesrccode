package com.cps.servlet;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletRequestFilter;

import java.util.List;

public class CPSCachePreventionRequestFilter extends GenericService implements DynamoHttpServletRequestFilter {

	private List<String> mPreventCacheUris;

	public void setPreventCacheUris(List<String> pRequestFilter) {
		mPreventCacheUris = pRequestFilter;
	}

	public List<String> getPreventCacheUris() {
		return mPreventCacheUris;
	}

	public boolean accept(DynamoHttpServletRequest pRequest) {

		if (isLoggingDebug()) {
			logDebug("request URI: " + pRequest.getRequestURI());
		}

		List<String> preventCacheUris = getPreventCacheUris();
		String requestURI = pRequest.getRequestURI();
		if (preventCacheUris != null && !StringUtils.isBlank(requestURI)) {
			for (String uri : preventCacheUris) {
				if (requestURI.startsWith(uri)) {
					return true;
				}
			}
		}

		return false;

	}

}
