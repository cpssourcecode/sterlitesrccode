package com.cps.servlet;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import atg.multisite.SiteWrapper;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;

public class CPSPagesJumpServlet extends CPSJumpServlet {

	public static final String PAGE_URL = "pageURL";
	public static final String SITES = "sites";


	/**
	 * Gets and writes the jump url from the corresponding template items
	 **/
	protected String getJumpLink(DynamoHttpServletRequest pRequest) {
		String result = null;
		String uri = pRequest.getRequestURI();
		try {
			if (null != uri) {
				RepositoryItem itemPage = findPage(uri);
				if (null != itemPage) {
					result = clearUri(pRequest, getTemplateURL());
					pRequest.setAttribute(CPS_SITE_PAGE, itemPage);
				}
			}
		} catch (Exception e) {
			logError(e);
		}
		return result;
	}

	/**
	 * Returns the template item by the URL
	 */
	protected RepositoryItem findPage(String uri) {
		RepositoryItem result = null;
		Repository repository = getRepository();
		SiteWrapper site = new SiteWrapper();

		if (isLoggingDebug()) {
			logDebug("Finding page...");
		}

		if (repository != null && site != null && !StringUtils.isBlank(uri)) {
			try {
				String siteId = site.getId();
				if (!StringUtils.isBlank(siteId)) {
					if (isLoggingDebug()) {
						logDebug("Site: " + siteId);
					}
					RepositoryView view = repository.getView(STATIC_PAGE_TEMPLATE);
					QueryBuilder qb = view.getQueryBuilder();
					List<Query> queries = new ArrayList<Query>();
					QueryExpression valueSetExp = qb.createConstantQueryExpression(uri);
					QueryExpression propExp = qb.createPropertyQueryExpression(PAGE_URL);
					queries.add(qb.createPatternMatchQuery(propExp, valueSetExp, QueryBuilder.EQUALS, true));

					valueSetExp = qb.createConstantQueryExpression(siteId);
					propExp = qb.createPropertyQueryExpression(SITES);
					queries.add(qb.createPatternMatchQuery(propExp, valueSetExp, QueryBuilder.CONTAINS, true));
					Query resultQuerry = qb.createAndQuery(queries.toArray(new Query[queries.size()]));
					RepositoryItem[] items = view.executeQuery(resultQuerry);

					if ((items != null) && (items.length > 0)) {
						result = items[0];
						if (isLoggingDebug()) {
							logDebug("Found an item: " + result);
						}
					} else if (isLoggingDebug()) {
						logDebug("No items were returned by query.");
					}
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
		return result;
	}

}