package com.cps.servlet;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.ServletException;

import org.apache.commons.lang.RandomStringUtils;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.HeadPipelineServlet;

public class CPSHeadPipelineServlet extends HeadPipelineServlet {

    private static final String STATIC_CONTENT_PREFIX = "staticContentPrefix";
    private static final String SITE_CONFIGURATION = "siteConfiguration";
    private boolean mContentSecurityPoliceHeaderEnabled;
    private Repository SiteRepository;
    private final String defaultSiteId = "cps";
    @Override
    public String getContentSecurityPolicyHeader() {
        if(isContentSecurityPoliceHeaderEnabled()) {
            return super.getContentSecurityPolicyHeader();
        }
        return null;
    }

    private void updateContentSecurityPolicyHeader(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pRespons){
        String staticContentPrefix = "";
        try {
            RepositoryItem siteItem = getSiteRepository().getItem(defaultSiteId, SITE_CONFIGURATION);
            if (siteItem != null) {
                staticContentPrefix = (String) siteItem.getPropertyValue(STATIC_CONTENT_PREFIX);
            }
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        String header = getContentSecurityPolicyHeader();
        if(isContentSecurityPoliceHeaderEnabled() && (header != null)){
            String nonce = RandomStringUtils.randomAlphanumeric(16);
            String nonceString = String.format("'nonce-%s'", nonce);
            pRespons.setHeader("Content-Security-Policy", MessageFormat.format(header, nonceString, staticContentPrefix));
            pRequest.setAttribute("nonce", nonce);
        }
    }

    @Override
    public void passRequest(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        updateContentSecurityPolicyHeader(pRequest, pResponse);
        super.passRequest(pRequest, pResponse);
    }

    public boolean isContentSecurityPoliceHeaderEnabled() {
        return mContentSecurityPoliceHeaderEnabled;
    }

    public void setContentSecurityPoliceHeaderEnabled(boolean pContentSecurityPoliceHeaderEnabled) {
        mContentSecurityPoliceHeaderEnabled = pContentSecurityPoliceHeaderEnabled;
    }

    /**
     * @return the siteRepository
     */
    public Repository getSiteRepository() {
        return SiteRepository;
    }

    /**
     * @param siteRepository the siteRepository to set
     */
    public void setSiteRepository(Repository siteRepository) {
        SiteRepository = siteRepository;
    }
}
