package com.cps.servlet;

import atg.adapter.gsa.ChangeAwareSet;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * @author Stolbunov Dmitry.
 */
public class FindSubscriberServlet extends DynamoServlet {
    private static final ParameterName OUTPUT = ParameterName.getParameterName("output");
    private static final ParameterName ERROR = ParameterName.getParameterName("error");
    private static final String ID = "id";
    private static final String DESCRIPTION = "description";
    private static final String STATUS = "status";
    private static final String CHECKED = "checked";
    private Repository mRepository;
    private String mSubsDescriptor;
    private String mOptionsDescriptor;

    @Override
    public void service(DynamoHttpServletRequest req, DynamoHttpServletResponse res) throws ServletException, IOException {
        try {
            String email = req.getParameter("email");
            if (StringUtils.isNotBlank(email)) {
                RepositoryItem item = getSubscriberByEmail(email);
                if(item != null){
                    if (req.getParameter("userId").equals(item.getPropertyValue("user_id"))){
                        List<HashMap<String, Object>> options = getMapOfOptionsList(getItems(getOptionsDescriptor(), "status = 1", new Object[0]));
                        addChecked((ChangeAwareSet) item.getPropertyValue("options"), options);
                        req.setParameter("isNew", "false");
                        req.setParameter("subscriber_id", item.getPropertyValue("id"));
                        req.setParameter("options", options);
                        req.serviceLocalParameter(OUTPUT, req, res);
                    } else {
                        req.setParameter("message", "This email is used already");
                        req.serviceLocalParameter(ERROR, req, res);
                    }
                } else {
                    req.setParameter("options", getMapOfOptionsList(getItems(getOptionsDescriptor(), "status = 1", new Object[0])));
                    req.setParameter("isNew", "true");
                    req.serviceLocalParameter(OUTPUT, req, res);
                }
            }
        } catch (RepositoryException e) {
            vlogError(e, "Error");
        }
    }

    private RepositoryItem getSubscriberByEmail(String email) throws RepositoryException {
        Object params[] = new Object[1];
        params[0] = email;
        RepositoryItem[] items = getItems(getSubsDescriptor(), "login = ?0", params);
        if (null != items && items.length > 0) {
            return items[0];
        }
        return null;
    }


    private RepositoryItem[] getItems(String descriptor, String filter, Object[] params) throws RepositoryException {
        RepositoryView view = getRepository().getView(descriptor);
        RqlStatement statement = RqlStatement.parseRqlStatement(filter);
        return statement.executeQuery(view, params);
    }

    private List<HashMap<String, Object>> getMapOfOptionsList(RepositoryItem[] items) {
        List<HashMap<String, Object>> optionsList = new ArrayList<>();
        if (items.length > 0) {
            optionsList = new ArrayList<>(items.length);
            for (RepositoryItem item : items) {
                HashMap<String, Object> option = new HashMap<>();
                option.put(ID, item.getPropertyValue(ID));
                option.put(DESCRIPTION, item.getPropertyValue(DESCRIPTION));
                option.put(STATUS, item.getPropertyValue(STATUS));
                option.put(CHECKED, "true");
                optionsList.add(option);
            }
        }
        return optionsList;
    }

    private void addChecked(ChangeAwareSet set, List<HashMap<String, Object>> options){
        for (HashMap<String, Object> option : options) {
            option.put(CHECKED, "false");
            Object id = option.get(ID);
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                Object savedId = iterator.next();
                if (id.equals(savedId)){
                    option.put(CHECKED, "true");
                    break;
                }
            }
        }
    }

    public Repository getRepository() {
        return mRepository;
    }

    public void setRepository(Repository pRepository) {
        mRepository = pRepository;
    }

    public String getSubsDescriptor() {
        return mSubsDescriptor;
    }

    public void setSubsDescriptor(String pItemDescriptor) {
        mSubsDescriptor = pItemDescriptor;
    }

    public String getOptionsDescriptor() {
        return mOptionsDescriptor;
    }

    public void setOptionsDescriptor(String pOptionsDescriptor) {
        mOptionsDescriptor = pOptionsDescriptor;
    }
}
