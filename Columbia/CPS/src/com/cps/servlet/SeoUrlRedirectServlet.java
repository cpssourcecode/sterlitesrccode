package com.cps.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.seo.SeoTools;

import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.multisite.SiteManager;
import atg.nucleus.ServiceException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

public class SeoUrlRedirectServlet extends InsertableServletImpl {

	public static final String SITE_IDS = "siteIds";
	public static final String ADDITIONAL_PRODUCTION_URLS = "additionalProductionURLs";

	private String idRegex;
	private boolean enabled;
	private String urlRegex;
	private Pattern idPattern;
	private SeoTools seoTools;
	private Pattern urlPattern;
	private String pdpForwardUrl;
	private boolean forwardToPdp;
    // private String siteIdForRedirect;
	private SiteManager siteManager;
	private ArrayList<String> brandSiteIds;
    // private RepositoryItem repositoryItemForLink;

	public void doStartService() throws ServiceException {
		super.doStartService();
		initialize();
	}

	/**
	 * Performs regex initialization on doStart service.
	 */
	protected void initialize() {
		idPattern = Pattern.compile(getIdRegex());
		urlPattern = Pattern.compile(getUrlRegex());
	}

	/**
	 * Proceeds to the correct seo link
	 * 
	 * @param request
	 *            - the request to be processed
	 * @param response
	 *            - the response object for this request
	 * @throws IOException
	 *             - an application specific error occurred processing this
	 *             request
	 * @throws ServletException
	 *             - an error occurred reading data from the request or writing
	 *             data to the response.
	 */
	public void service(DynamoHttpServletRequest request,
			DynamoHttpServletResponse response) throws IOException,
			ServletException {
		String currentSiteId = SiteContextManager.getCurrentSiteId();
		if (isEnabled() && checkMatchUrl(request)) {
			String supposedID = extractId(request);
			try {
				if (!StringUtils.isBlank(supposedID)
						&& isIdReferToProduct(supposedID)) {
					RepositoryItem possibleItem = findPossibleItem(supposedID);
                    // setRepositoryItemForLink(possibleItem);
                    String siteIdFromPossibleItem = extractSiteFromItem(possibleItem);
                    // setSiteIdForRedirect(extractSiteFromItem(possibleItem));
                    // String correctSEOLink = getSeoTools().makeSeoLink(
                    // getRepositoryItemForLink(), currentSiteId);
                    String correctSEOLink = getSeoTools().makeSeoLink(possibleItem, currentSiteId);

                    if (!validatePath(request, correctSEOLink, siteIdFromPossibleItem)) {
						if (isForwardToPdp()) {
							vlogDebug("Forwarding this item to pdp: "
									+ supposedID);
							forwardResponseToPDP(supposedID, request, response);
							return;
						}
					}
					vlogDebug("No redirect needed. Passing request further.");
				}
			} catch (RepositoryException e) {
				vlogError("Can not find Repository Item by the given ID", e);
			}
		}
		if (getNextServlet() != null) {
			passRequest(request, response);
		} else {
			response.sendError(DynamoHttpServletResponse.SC_NOT_FOUND);
		}
	}

	/**
	 * Gets the site object from the id, that was set to "siteIdForRedirect".
	 * Then retrieves additional production urls property from site object and
	 * thus - detect production url.
	 */
    // private String getBaseProductUrl() throws RepositoryException {
    // String result = null;
    // RepositoryItem site = getSiteManager().getSite(getSiteIdForRedirect());
    // if (site != null) {
    // String[] siteUrlSet = (String[]) site
    // .getPropertyValue(ADDITIONAL_PRODUCTION_URLS);
    // result = siteUrlSet[0];
    // } else {
    // vlogError("Can't find site assosiated with id:"
    // + getSiteIdForRedirect());
    // }
    // return result;
    // }

	/**
	 * Forward current request to pdp page. Pdp path is set to the component.
	 */
	private void forwardResponseToPDP(String itemId,
			DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (!StringUtils.isEmpty(getPdpForwardUrl())) {
			RequestDispatcher dispatcher = pRequest
					.getRequestDispatcher(getPdpForwardUrl().replaceAll(
							"\\{.*\\}", itemId));
			dispatcher.forward(pRequest, pResponse);
		} else {
			vlogError("pdpForwardUrl is not set");
		}
	}

	/**
	 * Check whether the given id corresponds to the product or not
	 */
	private boolean isIdReferToProduct(String productId)
			throws RepositoryException {
		return findPossibleItem(productId) != null;
	}

	/**
	 * Finds repository item - product - for the given id. If nothing was found
	 * - return null.
	 */
	private RepositoryItem findPossibleItem(String productId)
			throws RepositoryException {
		return getCatalogTools().findProduct(productId);
	}

	/**
	 * Extracts site id for the given repository item
	 */
	private String extractSiteFromItem(RepositoryItem possibleItem) {
		String result = SiteContextManager.getCurrentSiteId();
		Set<String> siteIds = (Set<String>) possibleItem
				.getPropertyValue(SITE_IDS);
		if (siteIds != null && !siteIds.contains(result)) {
			String siteId = chooseBrandSiteId(siteIds);
			if (!StringUtils.isBlank(siteId)) {
				result = siteId;
			} else {
				vlogDebug("Can't retrieve site id for repository item:"
						+ possibleItem.getItemDisplayName());
			}
		}
		return result;
	}

	/**
	 * Choose the first siteId match from the given siteIds set among the
	 * brandSiteIds array, which was set in component.
	 */
	private String chooseBrandSiteId(Set<String> siteIds) {
		String result = null;
		ArrayList<String> brandSites = (ArrayList<String>) getBrandSiteIds();
		if (siteIds != null) {
			for (String siteId : brandSites) {
				if (siteIds.contains(siteId)) {
					result = siteId;
					break;
				}
			}
		}
		return result;
	}

	private boolean isItemFromCurrentSite(String itemSiteId) {
		return SiteContextManager.getCurrentSiteId().equals(itemSiteId);
	}

	private boolean isSeoUrlAlreadyCorrect(DynamoHttpServletRequest request,
			String redirectSEOLink) {
		return request.getRequestURI().equalsIgnoreCase(redirectSEOLink);
	}

	/**
	 * Validates url and set detects what will be the further path for request.
	 * If seo url is incorrect and item's siteId matches the current site - sets
	 * forward request to pdp flag to true. If SeoUrl is correct - the result of
	 * validation will be false if item is not from current site, true
	 * otherwise.
	 */
	private boolean validatePath(DynamoHttpServletRequest request,
                    String correctSEOLink, String siteIdForRedirect) {
		boolean result = true;
		setForwardToPdp(false);
		if (!StringUtils.isBlank(correctSEOLink)) {
			if (isSeoUrlAlreadyCorrect(request, correctSEOLink)) {
				vlogDebug("Seo URL is already correct: " + correctSEOLink);
                result = isItemFromCurrentSite(siteIdForRedirect);
			} else {
				result = false;
                if (isItemFromCurrentSite(siteIdForRedirect)) {
					setForwardToPdp(true);
				}
			}
			vlogDebug("The item from the URL is from the current site: "
					+ result);
		}
		return result;
	}

	/**
	 * Extracts possible item id from the requests
	 */
	protected String extractId(DynamoHttpServletRequest request) {
		String uri = request.getRequestURI();
		Matcher matcher = idPattern.matcher(uri);
		return matcher.find() ? matcher.group(1) : null;
	}

	/**
	 * Check whether url from requests matches the urlPattern, that was set in
	 * the component.
	 */
	protected boolean checkMatchUrl(DynamoHttpServletRequest request) {
		String uri = request.getRequestURI();
		Matcher matcher = urlPattern.matcher(uri);
		return matcher.matches();
	}

	public CPSCatalogTools getCatalogTools() {
		return getSeoTools().getCatalogTools();
	}

	public SeoTools getSeoTools() {
		return seoTools;
	}

	public void setSeoTools(SeoTools mSeoTools) {
		this.seoTools = mSeoTools;
	}

    /*
     * public RepositoryItem getRepositoryItemForLink() { return repositoryItemForLink; }
     * 
     * public void setRepositoryItemForLink(RepositoryItem repositoryItemForLink) { this.repositoryItemForLink = repositoryItemForLink; }
     */
    /*
     * public String getSiteIdForRedirect() { return siteIdForRedirect; }
     * 
     * public void setSiteIdForRedirect(String siteIdForRedirect) { this.siteIdForRedirect = siteIdForRedirect; vlogDebug("Site id for redirect is set to: " +
     * siteIdForRedirect); }
     */
	public Pattern getIdPattern() {
		return idPattern;
	}

	public void setIdPattern(Pattern idPattern) {
		this.idPattern = idPattern;
	}

	public String getIdRegex() {
		return idRegex;
	}

	public void setIdRegex(String idRegex) {
		this.idRegex = idRegex;
	}

	public SiteManager getSiteManager() {
		return siteManager;
	}

	public void setSiteManager(SiteManager siteManager) {
		this.siteManager = siteManager;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getUrlRegex() {
		return urlRegex;
	}

	public void setUrlRegex(String urlRegex) {
		this.urlRegex = urlRegex;
	}

	public boolean isForwardToPdp() {
		return forwardToPdp;
	}

	public void setForwardToPdp(boolean forwardToPdp) {
		this.forwardToPdp = forwardToPdp;
	}

	public String getPdpForwardUrl() {
		return pdpForwardUrl;
	}

	public void setPdpForwardUrl(String pdpForwardUrl) {
		this.pdpForwardUrl = pdpForwardUrl;
	}

	public ArrayList<String> getBrandSiteIds() {
		return brandSiteIds;
	}

	public void setBrandSiteIds(ArrayList<String> brandSiteIds) {
		this.brandSiteIds = brandSiteIds;
	}
}