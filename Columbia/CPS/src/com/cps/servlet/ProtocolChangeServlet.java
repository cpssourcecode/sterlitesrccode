package com.cps.servlet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.cps.service.server.SecureServerService;

import atg.nucleus.ServiceException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

public class ProtocolChangeServlet extends InsertableServletImpl {
	
	private final static String SEARCH_RESULTS = "search-results.jsp";

    /** Is access control enabled? **/
    boolean mEnabled = false;

    /** List of URL's that need to be secure **/
    String[] mSecurePageList = null;


    /**
     * Is the access controller enabled?
     * @return boolean
     *
     **/
    public boolean isEnabled() {

        return mEnabled;
    }


     /**
     * Sets whether or not access controller is enabled.
     * @param pEnabled boolean
     **/
    public void setEnabled(boolean pEnabled) {

        mEnabled = pEnabled;
    }


    /**
     * Sets the list of secure pages
     * @param pSecurePageList
     **/
    public void setSecurePageList(String[] pSecurePageList) {

        mSecurePageList = pSecurePageList;
    }


    /**
     * Gets the list of secure pages
     * @return  String[]
     **/
    public String[] getSecurePageList() {

        return mSecurePageList;
    }


    /**
     * Services a DynamoHttpServletRequest/Response pair.
     * @exception ServletException if an error occurred while
     * processing the servlet request
     * @exception IOException if an error occurred while reading or
     * writing the servlet request
     **/
    @Override
    public void service(DynamoHttpServletRequest pRequest,
              DynamoHttpServletResponse pResponse)
        throws IOException, ServletException {

        // determine whether access should be allowed or denied
        if (!mEnabled || mSecurePageList == null) {
            passRequest(pRequest, pResponse);
            return;
        }

        if (pRequest.getMimeType() == null) {
            passRequest(pRequest, pResponse);
            return;
        }

        String path = pRequest.getRequestURIWithQueryString();
        // for ajax request validation parameters to make sue we not redirecting if modal windows loaded under secure schema
        String requestType = pRequest.getParameter("modal");
        boolean isModal = requestType!=null && requestType.equalsIgnoreCase("true");
        boolean isAjax = isAjaxRequest(pRequest);
        if (isLoggingDebug()) {
        	logDebug("Request using modal " + isModal + " is ajax post"+isAjax);
        	logDebug("Performing access control on " + path +
                " pRequest.getScheme()=" + pRequest.getScheme() + " pRequest.getServerPort()=" + pRequest.getServerPort() +
                " pRequest.getServerName()=" + pRequest.getServerName());
       }

        boolean bNeedSecure = currentPathInSecurePageList(path);

        // Determine what the current scheme is.
        boolean bIsSecure = false;
        if (pRequest.getScheme() != null && pRequest.getScheme().equalsIgnoreCase("https")) {
            bIsSecure = true;
        }
        if (pRequest.getServerPort() == 443) {
            pRequest.setScheme("https");
            bIsSecure = true;
        }


        // We don't want to redirect if we have post data
        boolean bPost = false;
        if( isLoggingDebug() ) {
			logDebug("Path is: " + path);
		}
        if (pRequest.getMethod() != null &&
              pRequest.getMethod().equalsIgnoreCase("post")) {
            bPost = true;
        }

        if (bNeedSecure && !bIsSecure && !isModal && !isAjax) {
            /**
             * If we need to be on a secure page and we currently on an in-secure
             * server, then redirect to the secure server
             */
            String redirectURL = SecureServerService.secureServerURL(pRequest, pResponse, path);
            if (isLoggingDebug()) {
                logDebug("bNeedSecure && !bIsSecure");
                logDebug("Redirecting to: " + redirectURL);
                logDebug("Encoded URL: " + pResponse.encodeRedirectURL(redirectURL));
            }

            /**
             * If the request is to change servers, but the page has post data,
             * then print a warning and do not redirect or you'll lose the data.
             */
            if (bPost && (path.indexOf(SEARCH_RESULTS) < 0)) {
                if (isLoggingWarning()) {
                    logWarning("Request method is \"POST\", not redirecting");
                }
                passRequest(pRequest, pResponse);
            } else {
				if( path.indexOf(SEARCH_RESULTS) >= 0 ) {
					String question = pRequest.getParameter("question");
					if( isLoggingDebug() ) {
						logDebug("search-results question is: " + question);
						logDebug("redirectUrl is: " + redirectURL);
					}
					pResponse.sendRedirect(pResponse.encodeRedirectURL(redirectURL+"&question="+question));
				} else {
                	pResponse.sendRedirect(pResponse.encodeRedirectURL(redirectURL));
				}
            }
        } else if (!bNeedSecure && bIsSecure && !isModal && !isAjax) {
            /**
             * If we need to be on an in-secure page and we currently on a secure
             * server, then redirect to the in-secure server
             */
            String redirectURL = SecureServerService.inSecureServerURL(pRequest, pResponse, path);

            logDebug("no need to use secure schema set it back to HTTP");
            pRequest.setScheme("http");

            if (isLoggingDebug()) {
                logDebug("!bNeedSecure && bIsSecure");
                logDebug("Redirecting to: " + redirectURL);
                logDebug("Encoded URL: " + pResponse.encodeRedirectURL(redirectURL));
            }

            /**
             * If the request is to change servers, but the page has post data,
             * then print a warning and do not redirect or you'll lose the data.
             */
            if (bPost && (path.indexOf(SEARCH_RESULTS) < 0)) {
                if (isLoggingWarning()) {
                    logWarning("Request method is \"POST\", not redirecting");
                }
                passRequest(pRequest, pResponse);
            } else {
				if( path.indexOf(SEARCH_RESULTS) >= 0 ) {
					String question = pRequest.getParameter("question");
					if( isLoggingDebug() ) {
						logDebug("search-results question is: " + question);
						logDebug("redirectUrl is: " + redirectURL);
					}
					pResponse.sendRedirect(pResponse.encodeRedirectURL(redirectURL+"&question="+question));
				} else {
                	pResponse.sendRedirect(pResponse.encodeRedirectURL(redirectURL));
				}
            }
        } else {
            if (isLoggingDebug()) {
                logDebug("bNeedSecure == bIsSecure");
                logDebug("No need to change");
            }

            passRequest(pRequest, pResponse);
        }
    }


    /** Checks to see if path is in the secur page list
     * @return boolean
     * @param mPath - url path
     **/
    boolean currentPathInSecurePageList(String mPath) {

        if (mSecurePageList == null) {
            return false;
        }

        for (int i = 0; i < mSecurePageList.length; i++) {
            if (mPath.startsWith(mSecurePageList[i])) {
                return true;
            }
        }

        return false;
    }

    private static boolean isAjaxRequest(DynamoHttpServletRequest request) {
   	 	String acceptHeader = request.getHeader("Accept");
   	 	if(acceptHeader!=null)
   	 	{
   	 		return (acceptHeader.indexOf("text/json") != -1 || acceptHeader.indexOf("application/json") != -1);
   	 	}else{
   	 		return false;
   	 	}
    }

    /**
     * Called after the service has been created, placed into the
     * naming hierarchy, and initialized with its configured property
     * values.  Makes sure all the required properties have been set
     * in the .properties file.
     *
     * Could put the list into a hash table
     *
     * @exception ServiceException if the service had a problem
     * starting up
     **/
    @Override
    public void doStartService() throws ServiceException {

        if (isLoggingDebug()) {
            if (mSecurePageList != null) {
                for (int i = 0; i < mSecurePageList.length; i++) {
                    logDebug("SecurePageList[" + i + "] = " + mSecurePageList[i]);
                }
            } else {
                logDebug("SecurePageList is null");
            }
        }

        super.doStartService();
    }


}