package com.cps.servlet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletRequestFilter;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.CachePreventionServlet;

/**
 * CachePreventionServlet extension. Modified http response header "cache-control" attribute to serve browser's back button
 * and cache invalidation.
 *
 * @author Steve Neverov
 */
public class CPSCachePreventionServlet extends CachePreventionServlet {

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		DynamoHttpServletRequestFilter[] filter = getRequestFilter();
		boolean accept = false;
		if ((filter != null) && (filter.length > 0)) {
			for (int fItr = 0; fItr < filter.length; ++fItr) {
				if (filter[fItr].accept(pRequest)) {
					accept = true;
					break;
				}
			}
		}

		if (accept) {
			if (isLoggingDebug()) {
				logDebug("Prevent cache of " + pRequest.getRequestURI());
			}
			pResponse.setHeader("Pragma", "no-cache");
			pResponse.addDateHeader("Expires", 0L);
			pResponse.setHeader("Cache-Control", "max-age=0, no-cache, no-store");
		}

		passRequest(pRequest, pResponse);

	}

}
