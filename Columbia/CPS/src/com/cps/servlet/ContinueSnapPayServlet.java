package com.cps.servlet;

import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.PaymentGroup;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.servlet.HttpServletService;
import atg.service.lockmanager.ClientLockManager;
import atg.service.lockmanager.DeadlockException;
import atg.service.pipeline.PipelineResult;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.TransactionManager;

import com.cps.commerce.order.CPSCreditCard;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.commerce.payment.SnapPayConnectionManager;
import com.cps.email.CommonEmailSender;
import com.cps.multisite.CPSSiteURLManager;

import java.io.IOException;

import static com.cps.util.CPSConstants.REQUEST_NUMBER;

/**
 * @author VSG
 *
 */
public class ContinueSnapPayServlet extends HttpServletService {

    /** Logging Debug parameter to get from web.xml */
    private static final String PARAM_LOGGING_DEBUG = "loggingDebug";

    private static final String RETRIEVE_ORDER_BY_REQUEST_NUMBER_RQL_QUERY = "requestNumber =?0";

    /** ConnectionManager property */
    private SnapPayConnectionManager mConnectionManager;

    /** order manager property */
    private CPSOrderManager mOrderManager;

    /** TransactionManager property */
    private TransactionManager mTransactionManager;

    /** shoppingCart property */
    private String mShoppingCart;

    /** profile property */
    private String mProfile;

    /** siteURLManager property */
    private CPSSiteURLManager mSiteURLManager;

    private ClientLockManager mLocalLockManager;

    private boolean mUseShoppingCart = true;

    private CommonEmailSender mEmailSender;

    /**
     * The initializing method
     *
     * @throws ServletException exception
     */
    public void init() throws ServletException {
        // Get the value of an initialization parameter
        String value = getServletConfig().getInitParameter(PARAM_LOGGING_DEBUG);
        if (!StringUtils.isBlank(value)) {
            setLoggingDebug(Boolean.parseBoolean(value));
        }
    }

    /**
     * Gets the request number parameter.
     *
     * @param pRequest the request
     * @return the token parameter
     */
    private String getRequestNumberParameter(HttpServletRequest pRequest) {
        String token = pRequest.getParameter(REQUEST_NUMBER);
        vlogDebug("Token from PayPal: {0}", token);
        return token;
    }

    /**
     * This method is the service call for this servlet. It will redirect the
     * user to the order review step after selecting payment options at PayPal.
     * The token will have been previously added to the PayPal payment group.
     *
     * @param pRequest  request
     * @param pResponse response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    public void service(HttpServletRequest pRequest, HttpServletResponse pResponse)
            throws ServletException, IOException {

        vlogDebug("{0}.service - start", getAbsoluteName());

        Profile currentProfile = getCurrentProfile(pRequest);
        boolean acquireLock = false;
        String profileId = "";
        CPSOrderManager orderManager = getOrderManager();
        try {
            if (currentProfile != null) {
                profileId = currentProfile.getRepositoryId();
                acquireLock = !getLocalLockManager().hasWriteLock(profileId, Thread.currentThread());
                if (acquireLock) {
                    getLocalLockManager().acquireWriteLock(profileId, Thread.currentThread());
                }
            }
            TransactionManager transactionManager = getTransactionManager();
            TransactionDemarcation transactionDemarcation = new TransactionDemarcation();
            boolean shouldRollback = false;
            try {
                transactionDemarcation.begin(transactionManager);

                CPSOrderImpl order = null;
                OrderHolder shoppingCart = null;
                if (isUseShoppingCart()) {
                    shoppingCart = getCurrentShoppingCart(pRequest);
                    order = (CPSOrderImpl)shoppingCart.getCurrent();
                } else {
                    String requestNumber = getRequestNumberParameter(pRequest);
                    if (!StringUtils.isBlank(requestNumber)) {
                        order = (CPSOrderImpl)orderManager.retrieveOrder(RETRIEVE_ORDER_BY_REQUEST_NUMBER_RQL_QUERY, new Object[]{requestNumber});
                    }
                }
                if (isRequiredDataValid(order)) {
                    synchronized (order) {
                        order.setOrderTotal(order.getPriceInfo().getTotal());
                        PaymentGroup pg = order.getPaymentGroup();
                        if (pg != null && pg instanceof CPSCreditCard) {
                        	((CPSCreditCard) pg).setPONumber(order.getPONumber());
                        }
                        PipelineResult pipelineResult = orderManager.processOrder(order, orderManager.getProcessOrderMap(null, null));
                        if (pipelineResult.hasErrors()) {
                            shouldRollback = true;
                            if (isLoggingError()) {
                                Object pipelineErrors[] = pipelineResult.getErrors();
                                logError("ContinueSnapPayServlet.service: errors processing order - " + order.getId());
                                for (int i = 0; i < pipelineErrors.length; i++) {
                                    logError(pipelineErrors[i].toString());
                                }
                            }
                        } else {
                            if (shoppingCart != null) {
                                shoppingCart.setLast(order);
                                shoppingCart.setCurrent(null);
                            }

                            getEmailSender().sendOrderConfirmationEmail(currentProfile, order, order.getSiteId());
                        }
                        redirectToContinueUrl(pRequest, pResponse);
                    }
                }
            } catch (Exception e) {
                vlogError(e, "Error occurred while processing callback data from SnapPay");
                shouldRollback = true;
            } finally {
                endTransaction(transactionDemarcation, shouldRollback);
                if (shouldRollback) {
                    vlogDebug("Redirecting to cancel URL as errors occurred while processing SnapPay callback.");
                    pResponse.sendRedirect(getConnectionManager().getCancelUrl());
                }
            }
        } catch (DeadlockException e) {
            vlogError(e, "DeadLockException");
        } finally {
            try {
                if (currentProfile != null && acquireLock) {
                    getLocalLockManager().releaseWriteLock(profileId, Thread.currentThread(), true);
                }
            } catch (Throwable e) {
                vlogError(e, "Error");
            }
        }

        vlogDebug("{0}.service - exit", getAbsoluteName());
    }

    /**
     * Redirects user to the continue page.
     *
     * @param pRequest     request
     * @param pResponse    response
     * @throws ServletException ServletException
     * @throws IOException      IOException
     */
    private void redirectToContinueUrl(HttpServletRequest pRequest, HttpServletResponse pResponse)
            throws ServletException, IOException {
        String siteUrl = getSiteURLManager().getSiteUrl();
        String processingUrl = getConnectionManager().getConfirmationUrl();
        vlogDebug("Redirect to ContinueURI {0}", siteUrl + processingUrl);
        pResponse.sendRedirect(siteUrl + processingUrl);

    }

    /**
     * Required data in place boolean.
     *
     * @param pOrder the current order
     * @return the boolean
     */
    private boolean isRequiredDataValid(Order pOrder) {
        boolean valid = true;
        if (pOrder == null) {
            vlogError("Cannot get current order from session.");
            valid = false;
        }
        return valid;
    }

    /**
     * Ends transaction.
     *
     * @param pTransactionDemarcation the transaction demarcation
     * @param pShouldRollback         the should rollback
     */
    private void endTransaction(TransactionDemarcation pTransactionDemarcation, boolean pShouldRollback) {
        try {
            pTransactionDemarcation.end(pShouldRollback);
        } catch (TransactionDemarcationException e) {
            vlogError(e, "Error in ending transaction.");
        }
    }

    /**
     * @param pRequest request
     * @return current OrderHolder
     * @throws ServletException ServletException
     */
    protected OrderHolder getCurrentShoppingCart(HttpServletRequest pRequest) throws ServletException {
        return getCurrentShoppingCart(ServletUtil.getDynamoRequest(pRequest));
    }

    /**
     * @param pRequest request
     * @return current OrderHolder
     * @throws ServletException ServletException
     */
    protected OrderHolder getCurrentShoppingCart(DynamoHttpServletRequest pRequest) throws ServletException {
        Object shoppingCart = pRequest.resolveName(getShoppingCart());
        if (shoppingCart instanceof OrderHolder) {
            return (OrderHolder) shoppingCart;
        } else {
            throw new ServletException(getAbsoluteName() + ": ShoppingCart configuration is not an OrderHolder.");
        }
    }

    /**
     * @param pRequest request
     * @return current profile
     * @throws ServletException ServletException
     */
    protected Profile getCurrentProfile(HttpServletRequest pRequest) throws ServletException {
        return getCurrentProfile(ServletUtil.getDynamoRequest(pRequest));
    }

    /**
     * @param pRequest request
     * @return current profile
     * @throws ServletException ServletException
     */
    protected Profile getCurrentProfile(DynamoHttpServletRequest pRequest) throws ServletException {
        Object profile = pRequest.resolveName(getProfile());
        if (profile instanceof Profile) {
            return (Profile) profile;
        } else {
            throw new ServletException(getAbsoluteName() + ": Profile configuration is not an Profile item.");
        }
    }

    public void setConnectionManager(SnapPayConnectionManager pConnectionManager) {
        mConnectionManager = pConnectionManager;
    }

    public SnapPayConnectionManager getConnectionManager() {
        return mConnectionManager;
    }

    public String getShoppingCart() {
        return mShoppingCart;
    }

    public void setShoppingCart(String pShoppingCart) {
        mShoppingCart = pShoppingCart;
    }

    public CPSOrderManager getOrderManager() {
        return mOrderManager;
    }

    public void setOrderManager(CPSOrderManager pOrderManager) {
        mOrderManager = pOrderManager;
    }

    public TransactionManager getTransactionManager() {
        return mTransactionManager;
    }

    public void setTransactionManager(TransactionManager pTransactionManager) {
        mTransactionManager = pTransactionManager;
    }

    public void setProfile(String pProfile) {
        mProfile = pProfile;
    }

    public String getProfile() {
        return mProfile;
    }

    public CPSSiteURLManager getSiteURLManager() {
        return mSiteURLManager;
    }

    public void setSiteURLManager(CPSSiteURLManager pSiteURLManager) {
        mSiteURLManager = pSiteURLManager;
    }

    public void setLocalLockManager(ClientLockManager pLocalLockManager) {
        mLocalLockManager = pLocalLockManager;
    }

    public ClientLockManager getLocalLockManager() {
        return mLocalLockManager;
    }

    public boolean isUseShoppingCart() {
        return mUseShoppingCart;
    }

    public void setUseShoppingCart(boolean pUseShoppingCart) {
        mUseShoppingCart = pUseShoppingCart;
    }

    public CommonEmailSender getEmailSender() {
        return mEmailSender;
    }

    public void setEmailSender(CommonEmailSender pEmailSender) {
        mEmailSender = pEmailSender;
    }

}
