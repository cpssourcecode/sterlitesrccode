package com.cps.servlet;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import vsg.crypto.EncryptorException;
import vsg.util.UrlDownload;
import atg.core.util.StringUtils;
import atg.nucleus.servlet.HttpServletService;
import atg.servlet.ServletUtil;

import com.cps.userprofiling.CPSStatementConnectionManager;
import com.cps.statement.StatementInfo;
import com.cps.util.CPSSessionBean;
import com.cps.util.security.CriptoUtils;

/**
 * StatementServlet
 * 
 * @author Steve Neverov
 * 
 */
public class StatementServlet extends HttpServletService {

	// Constants
	// ----------------------------------------------------------------------------------

	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(StatementServlet.class);

	/**
	 * component name for debugging
	 */
	private final static String COMPONENT_NAME = "/cps/servlet/StatementServlet";
	/** Logging Debug parameter to get from web.xml */
	private static final String PARAM_LOGGING_DEBUG = "loggingDebug";

	private static final String STATEMENT_SERVLET_PATH = "/statement/";
	private static final String URL_PARAM_SEPARATOR = "&";
	private static final String ASSIGN = "=";

	private static final String STATEMENT_ID_PARAM = "si";

	/**
	 * Content Disposition property
	 */
	private String mContentDisposition;

	public String getContentDisposition(){
		return mContentDisposition;
	}

	public void setContentDisposition(String pContentDisposition){
		mContentDisposition = pContentDisposition;
	}

	/**
	 * Default Constructor
	 */
	public StatementServlet() {
	}

	private CPSStatementConnectionManager mConnectionManager;

	public void setConnectionManager(CPSStatementConnectionManager pConnectionManager){
		mConnectionManager = pConnectionManager;
	}

	public CPSStatementConnectionManager getConnectionManager(){
		return mConnectionManager;
	}

	// Actions
	// ------------------------------------------------------------------------------------
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	// Helpers (can be refactored to public utility class)
	// ----------------------------------------

	private static void close(Closeable pResource) {
		if (pResource != null) {
			try {
				pResource.close();
			} catch (IOException e) {
				// Do your thing with the exception. Print it, log it or mail
				// it.
				mLogging.logError(e);
			}
		}
	}

	/**
	 * The initializing method
	 *
	 * @throws javax.servlet.ServletException
	 */
	public void init() throws ServletException {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".init").append(" - start").toString());
		}
		// Get the value of an initialization parameter
		String value = getServletConfig().getInitParameter(PARAM_LOGGING_DEBUG);
		if (!StringUtils.isBlank(value)) {
			setLoggingDebug(Boolean.parseBoolean(value));
		}
		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".init").append(" - start").toString());
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see atg.nucleus.servlet.HttpServletService#service(javax.servlet.http.
	 * HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void service(HttpServletRequest pRequest, HttpServletResponse pResponse) throws ServletException, IOException {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".service").append(" - start").toString());
		}

		try {

			CPSSessionBean sessionBean = (CPSSessionBean) ServletUtil.getDynamoRequest(pRequest).resolveName("/cps/util/CPSSessionBean");

			Map<String, String> urlParams = getURLParams(pRequest.getRequestURL().toString());
			String id = urlParams.get(STATEMENT_ID_PARAM);

			String path = null;
			if (!StringUtils.isBlank(id) && sessionBean.getStatements() != null) {
				for (StatementInfo statementInfo : sessionBean.getStatements()) {
					if (id.equals(statementInfo.getId())) {
						path = statementInfo.getUrl();
					}
				}
			}

			if (!StringUtils.isBlank(path)) {
				String url = new StringBuilder()
						.append(getConnectionManager().getClient().getWebServiceConfig().getDocumentExtractionServiceAddress())
						.append(path).toString();

				if (!StringUtils.isBlank(url)) {
					byte[] statementPDF = UrlDownload.getFile(url, "application/pdf");
					extractStatement(pResponse, statementPDF);
				} else {
					printError(pResponse);
				}
			} else {
				printError(pResponse);
			}

		} catch (Exception e) {
			if (isLoggingError()) {
				logError(e);
			}
			pResponse.sendError(HttpServletResponse.SC_NOT_FOUND); // 404.
			return;
		}
	}

	private Map<String, String> getURLParams(String pUrl) throws EncryptorException {

		Map<String, String> urlParams = new HashMap<String, String>();
		String[] urlSplit = pUrl.split(STATEMENT_SERVLET_PATH);
		if (urlSplit.length > 1) {
			String paramValue = CriptoUtils.aesDecrypt(urlSplit[1]);
			urlParams.put(STATEMENT_ID_PARAM, paramValue);

//			String[] urlParamsSplit = urlSplit[1].split(URL_PARAM_SEPARATOR);
//			for (String urlParam : urlParamsSplit) {
//				String[] paramSplit = urlParam.split(ASSIGN);
//				if (paramSplit.length > 1) {
//					String paramName = paramSplit[0];
//					String paramValue = getDESEncryptor().decrypt(paramSplit[1]);
//					urlParams.put(STATEMENT_ID_PARAM, paramValue);
//				}
//			}
		}
		return urlParams;

	}

	private void extractStatement(HttpServletResponse pResponse, byte[] pStatementPDF) throws IOException {
		OutputStream os = pResponse.getOutputStream();
		pResponse.setContentType("application/pdf");
		pResponse.setHeader("Content-Disposition", getContentDisposition() + ";filename=Statement.pdf");
		os.write(pStatementPDF);
		os.flush();
		os.close();
	}

	private void printError(HttpServletResponse pResponse) throws IOException {
		PrintWriter out = pResponse.getWriter();
		out.print("Error extracting pdf file.");
		out.close();
	}

}