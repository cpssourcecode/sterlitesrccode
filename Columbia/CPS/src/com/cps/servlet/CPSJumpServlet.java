package com.cps.servlet;

import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.multisite.SiteWrapper;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.util.CurrentDate;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

public class CPSJumpServlet extends InsertableServletImpl {
	public final static int FALSE = 0;
	public final static int TRUE = 1;

	static final String STATIC_PAGE_TEMPLATE = "staticPageTemplate";
	static final String RQL_STATEMENT = "pageURL = ?0 AND sites CONTAINS ?1";
	static final String CPS_SITE_PAGE = "cpsSitePage";
	static final String SLASH = "/";
	static final String START_DATE = "startDate";
	static final String END_DATE = "endDate";
	static final String TEMPLATE = "template";

	/**
	 * property Repository
	 */
	private Repository mRepository;

	/**
	 * true if the servlet should be use
	 */
	private boolean mEnabled = false;

	/**
	 * list of url context processed by the servlet
	 */
	private String[] mJumpedContexts;

	/**
	 * jsp page path for the template
	 */
	private String mTemplateURL = "/global/page-template.jsp";

	/**
	 * current date
	 */
	private CurrentDate mCurrentDate = null;

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {

		if (isEnabled()) {
			String url = getJumpLink(pRequest);
			if (null != url && 0 < url.trim().length()) {
				RequestDispatcher dispatcher = pRequest.getRequestDispatcher(url);
				dispatcher.forward(pRequest, pResponse);
			} else {
				if (getNextServlet() != null) {
					passRequest(pRequest, pResponse);
				} else {
					pResponse.sendError(DynamoHttpServletResponse.SC_NOT_FOUND);
				}
			}
		} else {
			if (getNextServlet() != null) {
				passRequest(pRequest, pResponse);
			} else {
				pResponse.sendError(DynamoHttpServletResponse.SC_NOT_FOUND);
			}
		}
	}

	/**
	 * Gets and writes the jump url from the corresponding template items
	 */
	protected String getJumpLink(DynamoHttpServletRequest pRequest) {

		String result = null;
		String uri = pRequest.getRequestURI();
		try {
			if (null != uri) {
				RepositoryItem itemPage = findPage(uri);
				if (null != itemPage) {
					String template = (String) itemPage.getPropertyValue(TEMPLATE);
					if (!StringUtils.isBlank(template)) {
						result = clearUri(pRequest, template);
					} else {
						result = clearUri(pRequest, getTemplateURL());
					}
					pRequest.setAttribute(CPS_SITE_PAGE, itemPage);
				}
			}
		} catch (Exception e) {
			logError(e);
		}
		return result;
	}

	/**
	 * Returns the template item by the URL
	 */
	protected RepositoryItem findPage(String uri) {
		RepositoryItem result = null;
		Repository repository = getRepository();
		SiteWrapper site = new SiteWrapper();

		if (null != repository && null != site && null != getCurrentDate()) {
			try {
				String siteId = site.getId();
				if (!StringUtils.isBlank(siteId)) {
					RepositoryView view = repository.getView(STATIC_PAGE_TEMPLATE);
					RqlStatement statement = null;
					statement = RqlStatement.parseRqlStatement(RQL_STATEMENT);
					Object[] params = new Object[2];
					params[0] = uri;
					params[1] = siteId;

					RepositoryItem[] items = statement.executeQuery(view, params);
					if ((items != null) && (items.length > 0)) {
						for (RepositoryItem item : items) {
							if (isValidDatePage(item)) {
								result = item;
								break;
							}
						}

					}
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
		return result;
	}

	/**
	 * Checks the current date against the Start and End date interval of the
	 * page item
	 *
	 * @param pPage
	 * @return
	 */
	boolean isValidDatePage(RepositoryItem pPage) {
		boolean result = false;

		if (null != pPage) {
			Timestamp date = getCurrentDate().getTimeAsTimestamp();
			Timestamp startDate = (Timestamp) pPage.getPropertyValue(START_DATE);
			Timestamp endDate = (Timestamp) pPage.getPropertyValue(END_DATE);
			if ((null == endDate) || (date.before(endDate))) {
				if ((null == startDate) || (date.after(startDate))) {
					result = true;
				}

			}
		}
		return result;
	}

	/**
	 * Clear request uri from context and last slash.
	 *
	 * @param pRequest - request.
	 * @param uri      - uri.
	 * @return request uri except context and last slash.
	 */
	protected String clearUri(DynamoHttpServletRequest pRequest, String uri) {
		String result = uri;
		String context = pRequest.getContextPath();
		if (null != result) {
			result = result.trim();
			if (result.startsWith(context)) {
				result = result.substring(context.length());
			}
			if (result.length() > 1 && result.endsWith(SLASH)) {
				result = result.substring(0, result.length() - 1);
			}
		}
		return result;
	}

	/**
	 * @return the mRepository
	 */
	public Repository getRepository() {
		return mRepository;
	}

	/**
	 * @param pRepository the mRepository to set
	 */
	public void setRepository(Repository pRepository) {
		mRepository = pRepository;
	}

	/**
	 * @return the mEnabled
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * @param pEnabled the mEnabled to set
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * @return the mJumpedContexts
	 */
	public String[] getJumpedContexts() {
		return mJumpedContexts;
	}

	/**
	 * @param pJumpedContexts the mJumpedContexts to set
	 */
	public void setJumpedContexts(String[] pJumpedContexts) {
		this.mJumpedContexts = pJumpedContexts;
	}

	/**
	 * @return the mTemplateURL
	 */
	public String getTemplateURL() {
		return mTemplateURL;
	}

	/**
	 * @param pTemplateURL the mTemplateURL to set
	 */
	public void setTemplateURL(String pTemplateURL) {
		this.mTemplateURL = pTemplateURL;
	}

	/**
	 * @return the mCurrentDate
	 */
	public CurrentDate getCurrentDate() {
		return mCurrentDate;
	}

	/**
	 * @param pCurrentDate the mCurrentDate to set
	 */
	public void setCurrentDate(CurrentDate pCurrentDate) {
		mCurrentDate = pCurrentDate;
	}
}