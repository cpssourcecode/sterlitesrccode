package com.cps.servlet;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import atg.core.util.StringUtils;
import atg.nucleus.ServiceException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

import com.cps.seo.SeoTools;
import com.cps.seo.SeoTools.SeoItem;

public class CPSSeoJumpServlet extends InsertableServletImpl {
	
	private static final String PAGE = "page";
	
	private Pattern urlPattern;
	private Pattern idPattern;
	private String mUrlRegex;
	private String mExtUrlRegex;
	private String mIdRegex;
	private String mPdpForwardUrl;
	private SeoTools mSeoTools;
	
	private boolean mEnabled = true;
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
			throws ServletException, IOException {
		vlogDebug("CPSSeoJumpServlet.service start - "+isEnabled());
		if (isEnabled()){
			//if (checkMatchUrl(pRequest)) {
				String[] ids = null;
				String requestUri = pRequest.getRequestURI();
			vlogDebug("RequestUri: "+requestUri);
			ids = extractIds(pRequest);
				String strIds = "";
				if (ids != null && ids.length > 0){
					for(String id : ids){
						strIds += id + ",";
					}
				}
			vlogDebug("Extracted Ids: " + ids);
			SeoItem seoItem = getSeoTools().determineItemsByIds(ids);
			vlogDebug("SeoItem: "+seoItem);
			if (seoItem != null) {
					if (SeoItem.PRODUCT == seoItem && ids != null && ids.length > 0) {
						forwardToPDP(ids[ids.length-1], pRequest, pResponse);
					}else{
						// Mark request, AssemblerPipelineServlet will take
						// care about it.
						pRequest.setAttribute(SeoTools.ENDECA_SEO_REQUEST_ITEM, seoItem);
						//String categoryId = getSeoTools().getCategoryId(id);
						pRequest.setAttribute(SeoTools.SEO_REQUEST_ITEMS_IDS, strIds);
					}
				} else if(requestUri.contains(PAGE)){
					pRequest.setAttribute(SeoTools.ENDECA_SEO_REQUEST_ITEM, seoItem);
					pRequest.setAttribute(SeoTools.SEO_REQUEST_ITEMS_IDS, strIds);
				} else {
					if (isLoggingWarning()) {
						logWarning("Extracted Id does not belong to any SEO supporeted item, passing request");
					}
				}
			//}
		}
		if (getNextServlet() != null) {
			passRequest(pRequest, pResponse);
		} else {
			pResponse.sendError(DynamoHttpServletResponse.SC_NOT_FOUND);
		}
	}
	
	private void forwardToPDP(String pProductId, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (!StringUtils.isEmpty(getPdpForwardUrl())) {
			// forward to PDP
			RequestDispatcher dispatcher = pRequest.getRequestDispatcher(getPdpForwardUrl().replaceAll("\\{.*\\}",
					pProductId));
			dispatcher.forward(pRequest, pResponse);
		} else {
			if (isLoggingError()) {
				logError("pdpForwardUrl is not set, pass request");
			}
		}
	}
	
	protected boolean checkMatchUrl(DynamoHttpServletRequest pRequest) {
		String uri = pRequest.getRequestURI();
		vlogDebug("CheckMatchUrl: "+uri);
		vlogDebug("URLPattern: "+urlPattern);
		Matcher matcher = urlPattern.matcher(uri);
		vlogDebug("Matches - "+matcher.matches());
		return matcher.matches();
	}

	protected String[] extractIds(DynamoHttpServletRequest pRequest) {
		String uri = pRequest.getRequestURI().toUpperCase();
		return uri.split("/");
	}
	
	protected void initialize() {
		urlPattern = Pattern.compile(getUrlRegex());
		//extUrlPattern = Pattern.compile(getExtUrlRegex());
		idPattern = Pattern.compile(getIdRegex());
	}
	
	public void doInitialize() {
		initialize();
	}

	public void doStartService() throws ServiceException {
		super.doStartService();
		initialize();
	}
	
	public SeoTools getSeoTools() {
		return mSeoTools;
	}

	public void setSeoTools(SeoTools pSeoTools) {
		mSeoTools = pSeoTools;
	}
	
	public String getUrlRegex() {
		return mUrlRegex;
	}

	public void setUrlRegex(String pUrlRegex) {
		mUrlRegex = pUrlRegex;
	}
	
	public String getExtUrlRegex() {
		return mExtUrlRegex;
	}

	public void setExtUrlRegex(String pExtUrlRegex) {
		mExtUrlRegex = pExtUrlRegex;
	}
	
	public String getIdRegex() {
		return mIdRegex;
	}

	public void setIdRegex(String pIdRegex) {
		mIdRegex = pIdRegex;
	}
	
	public String getPdpForwardUrl() {
		return mPdpForwardUrl;
	}

	public void setPdpForwardUrl(String pPdpForwardUrl) {
		mPdpForwardUrl = pPdpForwardUrl;
	}
	
	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}
}