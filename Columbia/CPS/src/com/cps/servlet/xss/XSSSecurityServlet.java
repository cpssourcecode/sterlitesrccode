package com.cps.servlet.xss;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.SecurityServlet;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * The Class XSSSecurityServlet.
 * Extends SecurityServlet logic to not stop request
 * if any suspicious params was founded (make redirect to error page instead).
 *
 */
public class XSSSecurityServlet extends SecurityServlet {

	List<String> exclude = Arrays.asList("&","=","!","@","#","$","%","?","<",">","{","}","[","]");

	/** The m error page. */
	private String mErrorPage;

	/** The mUrisToExcludeFromRedirect. */
	private  List<String> mUrisToExclude;


	@Override
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {

		boolean stopFlow = false;
		if (!(allowRequest(pRequest, pResponse))) {
			String reqUri = pRequest.getRequestURI();
			boolean shouldMakeRedirect = true;
			if (reqUri != null) {
				for (String urlToExclude : exclude) {
					if (reqUri.contains(urlToExclude)) {
						shouldMakeRedirect = false;
						break;
					}
				}
			}

			if (shouldMakeRedirect && !StringUtils.isBlank(getErrorPage())) {
				pResponse.sendLocalRedirect( getErrorPage(), pRequest);
			}
			stopFlow = true;
		}

		if ( !stopFlow ) {
			passRequest(pRequest, pResponse);
		}
	}

	public String getErrorPage() {
		return mErrorPage;
	}

	public void setErrorPage(String mErrorPage) {
		this.mErrorPage = mErrorPage;
	}

	public List<String> getmUrisToExclude() {
		return mUrisToExclude;
	}

	public void setmUrisToExclude(List<String> pUrisToExclude) {
		this.mUrisToExclude = pUrisToExclude;
	}
}
