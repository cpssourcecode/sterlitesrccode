package com.cps.servlet;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

import atg.core.util.StringUtils;
import atg.nucleus.servlet.ServletService;
import atg.servlet.GenericServletService;
import atg.servlet.ServletUtil;

/**
 * ServletBridge
 * 
 * <h4>Description</h4> 
 * Bridge to convert to Dynamo Request and
 * Response objects
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 *
 */
public class ServletBridge extends GenericServletService {

	/** Logging Debug parameter to get from web.xml */
	private static final String PARAM_LOGGING_DEBUG = "loggingDebug";

	/**
	 * Destination Servlet property
	 */
	private ServletService mDestinationServlet;
	
    /**
     * The initializing method
     * @throws ServletException
     */
    public void init() throws ServletException {
        // Get the value of an initialization parameter
        String value = getServletConfig().getInitParameter(PARAM_LOGGING_DEBUG);
        if (!StringUtils.isBlank(value)){
        	setLoggingDebug(Boolean.parseBoolean(value));
        }
    }

	/**
	 * 
	 */
	public ServletBridge() {
	}

	/* (non-Javadoc)
	 * @see atg.servlet.GenericServletService#service(javax.servlet.ServletRequest, javax.servlet.ServletResponse)
	 */
	public void service(ServletRequest pServletRequest,
			ServletResponse pServletResponse) throws IOException,
			ServletException {
		if (isLoggingDebug())
			logDebug("ServletBridge.service:starting method...");
		if (isLoggingDebug() && (pServletRequest instanceof HttpServletRequest)) {
			HttpServletRequest httpRequest = (HttpServletRequest) pServletRequest;
			if (isLoggingDebug())
				logDebug((new StringBuilder()).append(
						"ServletBridge.service:Request data:: URI:").append(
						httpRequest.getRequestURI()).append(" URL:").append(
						httpRequest.getRequestURL()).append(" queryString:")
						.append(httpRequest.getQueryString()).toString());
		}
		atg.servlet.DynamoHttpServletRequest dynRequest = ServletUtil
				.getDynamoRequest(getServletContext(), pServletRequest,
						pServletResponse);
		atg.servlet.DynamoHttpServletResponse dynResponse = ServletUtil
				.getDynamoResponse(dynRequest, pServletResponse);
		if (isLoggingDebug())
			logDebug((new StringBuilder())
					.append(
							"ServletBridge.service:passing request to destination servlet: ")
					.append(getDestinationServlet().getAbsoluteName())
					.toString());
		getDestinationServlet().service(dynRequest, dynResponse);
		if (isLoggingDebug())
			logDebug("ServletBridge.service:ending method.");
	}

	/**
	 * @return
	 */
	public ServletService getDestinationServlet() {
		return mDestinationServlet;
	}

	/**
	 * @param pDestinationServlet
	 */
	public void setDestinationServlet(ServletService pDestinationServlet) {
		mDestinationServlet = pDestinationServlet;
	}
}