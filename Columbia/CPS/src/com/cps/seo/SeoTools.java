package com.cps.seo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import atg.adapter.gsa.GSAItem;
import atg.commerce.endeca.cache.DimensionValueCacheObject;
import atg.commerce.endeca.cache.DimensionValueCacheTools;
import atg.core.util.StringUtils;
import atg.multisite.SiteContextManager;
import atg.multisite.SiteManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.util.CPSConstants;
import com.endeca.navigation.DimLocation;
import com.endeca.navigation.DimLocationList;
import com.endeca.navigation.DimVal;
import com.endeca.soleng.urlformatter.NavStateUrlParam;
import com.endeca.soleng.urlformatter.UrlFormatException;
import com.endeca.soleng.urlformatter.UrlParam;
import com.endeca.soleng.urlformatter.UrlState;
import com.endeca.soleng.urlformatter.seo.SeoUrlParam;

import static com.cps.util.CPSConstants.PRODUCT_PARENT_CATEGORIES;

public class SeoTools extends GenericService {

	public enum SeoItem {
		PRODUCT, CATEGORY, HOMEPAGE
	};

	public static final String ENDECA_SEO_REQUEST_ITEM = "endeca_seo_request_item";
	public static final String SEO_REQUEST_ITEMS_IDS = "seo_request_items_repositoryIds";

	private static final String CATEGORY_PAGE_PATH = "/pages/browse";
	private static final String HOME_PAGE_PATH = "/pages/homepage";
	private static final String SPACE = " ";
	private static final String AMP = "\\&";
	private static final String QUESTION = "\\?";
	private static final String DOUBLE_SPACE = "  ";
	private static final String SLASH = "/";
	private static final String URL_REGEX = "[^A-Za-z0-9]+";

	private static final String CPS_SITE_ID = "cps";

	/**
	 * special chars pattern
	 */
	protected static Pattern mSpecCharsPattern = Pattern.compile("[\\W_&&[^\\u00C0-\\u00FF]]+");

	/**
	 * special chars replacement
	 */
	protected static String mSpecCharsReplacement = "-";

	private CPSCatalogTools catalogTools;
	private SiteManager siteManager;

	private String mCategoryPagePath = CATEGORY_PAGE_PATH;
	private String mHomePagePath = HOME_PAGE_PATH;

	private String mSeparator = "+";
	private String mSpaceReplacer = "-";

	private Set<String> mCategoryCache = new HashSet<String>(4096 * 2);

	private Map<String, String> mSeoDimensionsMap = new HashMap<String, String>();

	private Map<String, String> mCpsSeoDimensionsMap = new HashMap<String, String>();

	private DimensionValueCacheTools mDimensionValueCacheTools;

	public boolean isSeoRequest(HttpServletRequest pRequest) {
		return pRequest.getAttribute(SeoTools.ENDECA_SEO_REQUEST_ITEM) != null;
	}

	public String getPagePath(ServletRequest pRequest) {
		return getPagePath((SeoItem) pRequest.getAttribute(ENDECA_SEO_REQUEST_ITEM));
	}

	public String getPagePath(SeoItem pSeoItem) {
		if (pSeoItem != null) {
			switch (pSeoItem) {
			case CATEGORY:
				return getCategoryPagePath();
			case HOMEPAGE:
				return getHomePagePath();
			}
		}
		return null;
	}

	public SeoItem determineItemsByIds(String[] pIds) {
		if (pIds == null || pIds.length == 0){
			vlogDebug("determineItemById(): id is null or empty");
			return null;
		}

		/*if (isCategoryInCache(pId)){
			return SeoItem.CATEGORY;
		}*/

		for(String id : pIds){
			RepositoryItem item = null;
			try {
				// check for product
				if(StringUtils.isNotEmpty(id)){
					item = getCatalogTools().findProduct(id);
					if (item != null) {
						return SeoItem.PRODUCT;
					}
					// check for category
					item = getCatalogTools().findCategory(id);
					if (item != null) {
						addCategoryIdToCache(id);
						return SeoItem.CATEGORY;
					}

					List dvcoList = getDimensionValueCacheTools().get(id);
					if(dvcoList != null && !dvcoList.isEmpty()){
						return SeoItem.CATEGORY;
					}
				}
			} catch (RepositoryException e) {
				if (isLoggingDebug()) {
					logDebug(e);
				}
			}
		}

		// found nothing
		return null;
	}

	public String getCategoryId(String name){
		try {
			RepositoryItem item = getCatalogTools().findCategoryByName(name);
			if (item != null) {
				return item.getRepositoryId();
			}
		}catch (Exception e){
			vlogError(e, "Error");
		}
		return name;
	}

	public List<DimensionValueCacheObject> getSeoItemCacheEntries(HttpServletRequest pRequest) {
		List<DimensionValueCacheObject> result = new ArrayList<DimensionValueCacheObject>();
		SeoItem seoItem = (SeoItem) pRequest.getAttribute(SeoTools.ENDECA_SEO_REQUEST_ITEM);
		String repositoryIds = (String) pRequest.getAttribute(SeoTools.SEO_REQUEST_ITEMS_IDS);
		if(StringUtils.isNotBlank(repositoryIds)){
			String[] idsArray = repositoryIds.split(",");
			for(String repositoryId : idsArray){
				if (seoItem != null && StringUtils.isNotEmpty(repositoryId)) {
					List<DimensionValueCacheObject> entries = getDimensionValueCacheTools().get(repositoryId);
					if (entries != null && !entries.isEmpty()) {
						result.add(entries.get(0));
					}
				}
			}
		}
		return result;
	}

	public String getFormattedURLString(UrlState pUrlState, boolean pAppendEncodedKey, boolean pSkipFirstDimVal)
			throws UrlFormatException {
		StringBuilder buffer = new StringBuilder();

		if (null != pUrlState) {
			UrlParam nUrlParam = pUrlState.getUrlParam("N");
			if (nUrlParam instanceof NavStateUrlParam && nUrlParam instanceof SeoUrlParam) {
				SeoUrlParam seoUrlParam = (SeoUrlParam) nUrlParam;
				if (pAppendEncodedKey) {
					buffer.append('/');
					buffer.append(nUrlParam.getEncodedKey());
					buffer.append("-");
				}
				if (seoUrlParam.getIsPathParam()) {
					NavStateUrlParam nNavStateUrlParam = (NavStateUrlParam) nUrlParam;
					DimLocationList dimLocList = nNavStateUrlParam.getDimLocationList();
					if (dimLocList != null && !dimLocList.isEmpty()) {
						DimLocation dimLoc = null;
						DimVal dimVal = null;
						boolean isFirst = true;
						boolean isSkippedFirst = false;
						for (Object dimLocObj : dimLocList) {
							if (dimLocObj instanceof DimLocation) {
								dimLoc = (DimLocation) dimLocObj;
								dimVal = dimLoc.getDimValue();
								if (null != dimVal) {
									if (!pSkipFirstDimVal || isSkippedFirst) {
										if (isFirst) {
											isFirst = false;
										} else {
											buffer.append(getSeparator());
										}
										if (getSeoDimensionsMap().containsKey(dimVal.getDimensionName())) {
											buffer.append(getSeoDimensionsMap().get(dimVal.getDimensionName()))
													.append(getSeparator())
													.append(cleanDimensionName(dimVal.getName()));
										} else {
											buffer.append(dimVal.getId());
										}
									} else {
										isSkippedFirst = true;
									}
								}
							}
						}
					}
				}
			}
		}

		return buffer.toString();
	}

	public String cleanDimensionName(String pDimensionName) {
		String result = pDimensionName;

		if (!StringUtils.isBlank(result)) {
			result = result.replaceAll(QUESTION, SPACE).replaceAll(AMP, SPACE)
					.replaceAll(DOUBLE_SPACE, getSpaceReplacer()).replaceAll(SLASH, SPACE)
					.replaceAll(SPACE, getSpaceReplacer());
		}

		return result;
	}

	public String makeSeoLink(RepositoryItem pItem, String pSiteId) {
		return makeSisterSeoLink(pItem);
	}

	public String makeSisterSeoLink(RepositoryItem pItem) {
		String result = "";
		try {
			if (pItem != null) {
				String itemName = pItem.getItemDescriptor().getItemDescriptorName();
				StringBuilder sb = new StringBuilder(100);
				String name = "";
				RepositoryItem parentCategory = (RepositoryItem)pItem.getPropertyValue(CPSConstants.PARENT_CATEGORY);
				if ("product".equalsIgnoreCase(itemName)){
					if (parentCategory != null){
						name = (String)parentCategory.getPropertyValue(CPSConstants.DISPLAY_NAME);
						name = name.replaceAll(URL_REGEX, getSpaceReplacer());
					} else {
						Set parentCategories = (Set) pItem.getPropertyValue(PRODUCT_PARENT_CATEGORIES);
						if (!parentCategories.isEmpty()) {
							RepositoryItem productParentCategory = (RepositoryItem) parentCategories.iterator().next();
							name = (String) productParentCategory.getPropertyValue(CPSConstants.DISPLAY_NAME);
							name = name.replaceAll(URL_REGEX, getSpaceReplacer());
						}
					}
					if (StringUtils.isNotBlank(name)) {
						sb.append(SLASH).append(name).append(SLASH).append(pItem.getRepositoryId());
					} else {
						sb.append(SLASH).append(pItem.getRepositoryId());
					}
				} else if ("category".equalsIgnoreCase(itemName)){
					name = "";
					String begin = "";
					while (parentCategory != null) {
						name = (String)parentCategory.getPropertyValue(CPSConstants.DISPLAY_NAME);
						name = name.replaceAll(URL_REGEX, getSpaceReplacer());
						begin = SLASH + name + begin;
						parentCategory = (RepositoryItem)parentCategory.getPropertyValue(CPSConstants.PARENT_CATEGORY);
					}
					name = (String)pItem.getPropertyValue(CPSConstants.DISPLAY_NAME);
					name = name.replaceAll(URL_REGEX, getSpaceReplacer());
					sb.append(begin).append(SLASH).append(name).append(SLASH).append(pItem.getRepositoryId());
				}
				result = sb.toString();
				result = result.replaceAll("--+", getSpaceReplacer());
				return result.toLowerCase();
			} else {
				if (isLoggingError()) {
					logError("makeSeoLink(): pItem is null");
					return result;
				}
			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(e);
			}

		}
		
		return result.toLowerCase();
	}
	
	/**
	 * get seo name from value
	 * 
	 * @param pValue - value
	 * @return seo name
	 */
	public static String seoName(final String pValue) {
		return StringUtils.isBlank(pValue) ? ""
				: mSpecCharsPattern.matcher(pValue).replaceAll(mSpecCharsReplacement).toUpperCase();
	}
	
	public String getSeoDimensionName(String pDimensionName) {
		if (!StringUtils.isBlank(pDimensionName)) {
			return getSeoDimensionsMap().get(pDimensionName);
		}
		return null;
	}

	protected boolean addCategoryIdToCache(String pId) {
		return mCategoryCache.add(pId);
	}

	protected boolean isCategoryInCache(String pId) {
		return mCategoryCache.contains(pId);
	}
	
	public String getCategoryPagePath() {
		return mCategoryPagePath;
	}

	public void setCategoryPagePath(String pCategoryPagePath) {
		mCategoryPagePath = pCategoryPagePath;
	}
	
	public String getHomePagePath() {
		return mHomePagePath;
	}

	public void setHomePagePath(String pHomePagePath) {
		mHomePagePath = pHomePagePath;
	}

	public CPSCatalogTools getCatalogTools() {
		return catalogTools;
	}

	public void setCatalogTools(CPSCatalogTools catalogTools) {
		this.catalogTools = catalogTools;
	}
	
	public DimensionValueCacheTools getDimensionValueCacheTools() {
		return mDimensionValueCacheTools;
	}

	public void setDimensionValueCacheTools(DimensionValueCacheTools pDimensionValueCacheTools) {
		mDimensionValueCacheTools = pDimensionValueCacheTools;
	}
	
	public String getSeparator() {
		return mSeparator;
	}

	public void setSeparator(String pSeparator) {
		this.mSeparator = pSeparator;
	}
	
	public Map<String, String> getSeoDimensionsMap() {
		String currentSite=SiteContextManager.getCurrentSiteId();
		if(currentSite!=null && currentSite.equalsIgnoreCase(CPS_SITE_ID)){
			return mCpsSeoDimensionsMap;
		} else {
			return mSeoDimensionsMap;
		}
	}
	
	public void setSeoDimensionsMap(Map<String, String> pSeoDimensionsMap) {
		this.mSeoDimensionsMap = pSeoDimensionsMap;
	}
	
	public void setCpsSeoDimensionsMap(Map<String, String> pCpsSeoDimensionsMap) {
		this.mCpsSeoDimensionsMap = pCpsSeoDimensionsMap;
	}
	
	public String getSpaceReplacer() {
		return mSpaceReplacer;
	}

	public void setSpaceReplacer(String pSpaceReplacer) {
		this.mSpaceReplacer = pSpaceReplacer;
	}

	public SiteManager getSiteManager() {
		return siteManager;
	}

	public void setSiteManager(SiteManager siteManager) {
		this.siteManager = siteManager;
	}
}