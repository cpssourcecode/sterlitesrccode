package com.cps.seo;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.catalog.CatalogTools;
import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

public class ProductSeoUrlDroplet extends DynamoServlet {
	
	private SeoTools seoTools;
	private CatalogTools catalogTools;
	
	public static ParameterName PRODUCT_ID = ParameterName.getParameterName("prodId");
	public static ParameterName OUTPUT = ParameterName.getParameterName("output");
	
	public static final String PRODUCT_URL = "url";
	
	public void service (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		String seoURL = "";
		String productId = pRequest.getParameter(PRODUCT_ID);
		
		try {
			if (StringUtils.isNotBlank(productId)){
				RepositoryItem product = getCatalogTools().findProduct(productId);
				
				if (product != null) {
					seoURL = getSeoTools().makeSisterSeoLink(product);
				}
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}
		
		pRequest.setParameter(PRODUCT_URL, seoURL);
		pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
	}

	public SeoTools getSeoTools() {
		return seoTools;
	}

	public void setSeoTools(SeoTools seoTools) {
		this.seoTools = seoTools;
	}

	public CatalogTools getCatalogTools() {
		return catalogTools;
	}

	public void setCatalogTools(CatalogTools catalogTools) {
		this.catalogTools = catalogTools;
	}
}