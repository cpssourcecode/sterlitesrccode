package com.cps.seo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A java class to contain various SEO tag values like title, h1,h2 etc.,
 * @author Vignesh
 *
 */
public class SEOTagHolder {

    private Map<String, String> seoTags = new HashMap<String, String>(); 

    private List<String> seoMetaTagNameAttributeValues;

    private List<String> seoTagAttributeValues;

    private String productURL;

    private String productId;

    private String imageURL;

    /**
     * @return the imageURL
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * @param imageURL
     *            the imageURL to set
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    /**
     * @return the productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * @param productId
     *            the productId to set
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * @return the productURL
     */
    public String getProductURL() {
        return productURL;
    }

    /**
     * @param productURL
     *            the productURL to set
     */
    public void setProductURL(String productURL) {
        this.productURL = productURL;
    }

    /**
     * @return the seoTags
     */
    public Map<String, String> getSeoTags() {
        return seoTags;
    }

    /**
     * @param seoTags
     *            the seoTags to set
     */
    public void setSeoTags(Map<String, String> seoTags) {
        this.seoTags = seoTags;
    }

    /**
     *
     * @return
     */
    public List<String> getSeoMetaTagNameAttributeValues() {
        return seoMetaTagNameAttributeValues;
    }

    /**
     *
     * @param seoMetaTagNameAttributeValues
     */
    public void setSeoMetaTagNameAttributeValues(List<String> seoMetaTagNameAttributeValues) {
        this.seoMetaTagNameAttributeValues = seoMetaTagNameAttributeValues;
    }

    /**
     *
     * @return
     */
    public List<String> getSeoTagAttributeValues() {
        return seoTagAttributeValues;
    }

    /**
     *
     * @param seoTagAttributeValues
     */
    public void setSeoTagAttributeValues(List<String> seoTagAttributeValues) {
        this.seoTagAttributeValues = seoTagAttributeValues;
    }



}
