package com.cps.abandoned;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import com.cps.email.CommonEmailSender;
import com.cps.util.CPSGlobalProperties;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.abandoned.OrderAbandoned;
import atg.dms.patchbay.MessageSink;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/**
*
* @author Vignesh
*
*/
public class AbandonedOrderEmailSender extends GenericService implements MessageSink {
   private static final String ORDER_ABANDONED_MSG_TYPE = "atg.commerce.order.abandoned.OrderAbandoned";
   private boolean enabled;
   private CPSGlobalProperties globalProperties;
   private CommonEmailSender emailInfo;

   /**
    * @return the enabled
    */
   public boolean isEnabled() {
       return enabled;
   }

   /**
    * @param enabled
    *            the enabled to set
    */
   public void setEnabled(boolean enabled) {
       this.enabled = enabled;
   }

   /**
    * @return the globalProperties
    */
   public CPSGlobalProperties getGlobalProperties() {
       return globalProperties;
   }

   /**
    * @param globalProperties
    *            the globalProperties to set
    */
   public void setGlobalProperties(CPSGlobalProperties globalProperties) {
       this.globalProperties = globalProperties;
   }

   /**
    * @return the emailInfo
    */
   public CommonEmailSender getEmailInfo() {
       return emailInfo;
   }

   /**
    * @param emailInfo
    *            the emailInfo to set
    */
   public void setEmailInfo(CommonEmailSender emailInfo) {
       this.emailInfo = emailInfo;
   }

   @Override
   public void receiveMessage(String pPort, Message pMessage) throws JMSException {
       vlogDebug("pPort :: {0}", pPort);
       vlogDebug("pMessage :: {0}", ToStringBuilder.reflectionToString(pMessage, ToStringStyle.MULTI_LINE_STYLE));
       if (!isEnabled()) {
           return;
       }
       String type = pMessage.getJMSType();
       if (type.equals(ORDER_ABANDONED_MSG_TYPE)) { // ignoring when the incoming message is of a different type
           ObjectMessage objectMessage = (ObjectMessage) pMessage;
           OrderAbandoned orderAbandonedMsg = (OrderAbandoned) objectMessage.getObject();
           String profileId = orderAbandonedMsg.getProfileId();
           String orderId = orderAbandonedMsg.getOrderId();
           Order order = getOrder(orderId);
           if (order != null) { 
               RepositoryItem profileItem = null;
               try {
                   profileItem = getGlobalProperties().getProfileTools().getProfileForOrder(order);
               } catch (RepositoryException re) {
                   vlogError(re, "Exception occured trying to fetch profile with id :: {0}", profileId);
               }
               if (profileItem != null) { 
                   getEmailInfo().sendAbandonedCartEmail(profileItem);
               } else {
                   vlogError("Could not find profile with id :: {0}, for order with id :: {1}, not sending email.", profileId, orderId);
               }
           } else {
               vlogError("Could not find order with id :: {0}, for profile with id :: {1}, not sending email.", orderId, profileId);
           }
       }
   }

   /**
    * @param pOrderId
    * @return
    */
   private Order getOrder(String pOrderId) {
       Order order = null;
       try {
           order = getGlobalProperties().getOrderManager().loadOrder(pOrderId);
       } catch (CommerceException ce) {
           vlogError(ce, "Exception occured trying to load order with id :: {0}", pOrderId);
       }
       return order;
   }

}
