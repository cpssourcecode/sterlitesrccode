package com.cps.abandoned.jms.config;

import com.cps.email.CommonEmailSender;

/**
 * The type Schedule abandoned order email config.
 */
public class ScheduleAbandonedOrderEmailConfig {

	/**
	 * commonEmailSender to send emails.
	 */
	private CommonEmailSender mCommonEmailSender;
	/**
	 * The M logging debug.
	 */
	private boolean mLoggingDebug;
	/**
	 * The M logging error.
	 */
	private boolean mLoggingError;

	/**
	 * Is logging debug boolean.
	 *
	 * @return the boolean
	 */
	public boolean isLoggingDebug() {
		return mLoggingDebug;
	}

	/**
	 * Sets logging debug.
	 *
	 * @param pLoggingDebug the p logging debug
	 */
	public void setLoggingDebug(boolean pLoggingDebug) {
		mLoggingDebug = pLoggingDebug;
	}

	/**
	 * Is logging error boolean.
	 *
	 * @return the boolean
	 */
	public boolean isLoggingError() {
		return mLoggingError;
	}

	/**
	 * Sets logging error.
	 *
	 * @param pLoggingError the p logging error
	 */
	public void setLoggingError(boolean pLoggingError) {
		mLoggingError = pLoggingError;
	}

	/**
	 * Sets new commonEmailSender to send emails..
	 *
	 * @param mCommonEmailSender New value of commonEmailSender to send emails..
	 */
	public void setCommonEmailSender(CommonEmailSender pCommonEmailSender) {
		mCommonEmailSender = pCommonEmailSender;
	}

	/**
	 * Gets commonEmailSender to send emails..
	 *
	 * @return Value of commonEmailSender to send emails..
	 */
	public CommonEmailSender getCommonEmailSender() {
		return mCommonEmailSender;
	}

}
