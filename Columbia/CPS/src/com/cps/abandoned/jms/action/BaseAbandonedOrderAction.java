package com.cps.abandoned.jms.action;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.process.ProcessException;
import atg.process.action.ActionImpl;
import com.cps.email.CommonEmailSender;
import com.cps.abandoned.jms.config.ScheduleAbandonedOrderEmailConfig;


/**
 * The type Base abandoned order action.
 */
public abstract class BaseAbandonedOrderAction extends ActionImpl {

	/**
	 * commonEmailSender to send emails.
	 */
	private CommonEmailSender mCommonEmailSender;
	/**
	 * The M logger.
	 */
	private ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(OnAbandonedOrderAction.class);

	/**
	 * Configure.
	 *
	 * @param pConfiguration the p configuration
	 * @throws ProcessException              the process exception
	 * @throws UnsupportedOperationException the unsupported operation exception
	 */
	@Override
	public void configure(Object pConfiguration) throws ProcessException, UnsupportedOperationException {
		ScheduleAbandonedOrderEmailConfig config = (ScheduleAbandonedOrderEmailConfig) pConfiguration;
		mCommonEmailSender = config.getCommonEmailSender();
		if (mLogger != null) {
			mLogger.setLoggingDebug(config.isLoggingDebug());
			mLogger.setLoggingError(config.isLoggingError());
		}
	}

	/**
	 * Log debug.
	 *
	 * @param pMessage the p message
	 */
	protected void logDebug(String pMessage) {
		if (mLogger != null && mLogger.isLoggingDebug()) {
			mLogger.logDebug(pMessage);
		}
	}

	/**
	 * Log error.
	 *
	 * @param pMessage   the p message
	 * @param pThrowable the p throwable
	 */
	protected void logError(String pMessage, Throwable pThrowable) {
		if (mLogger != null && mLogger.isLoggingError()) {
			mLogger.logError(pMessage, pThrowable);
		}
	}

	/**
	 * Log error.
	 *
	 * @param pThrowable the p throwable
	 */
	protected void logError(Throwable pThrowable) {
		if (mLogger != null && mLogger.isLoggingError()) {
			mLogger.logError(pThrowable);
		}
	}

	/**
	 * Gets commonEmailSender to send emails..
	 *
	 * @return Value of commonEmailSender to send emails..
	 */
	public CommonEmailSender getCommonEmailSender() {
		return mCommonEmailSender;
	}
}
