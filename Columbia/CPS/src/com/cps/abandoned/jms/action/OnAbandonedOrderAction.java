package com.cps.abandoned.jms.action;

import atg.core.util.StringUtils;
import atg.process.ProcessException;
import atg.process.ProcessExecutionContext;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import static com.cps.abandoned.AbandonedOrderConstants.*;

import vsg.userprofiling.VSGProfileTools;

import java.util.Map;

/**
 * The type On abandoned order action.
 */
public class OnAbandonedOrderAction extends BaseAbandonedOrderAction {

	/**
	 * Initialize.
	 *
	 * @param pParameters the p parameters
	 * @throws ProcessException the process exception
	 */
	@Override
	public void initialize(final Map pParameters) throws ProcessException {
		logDebug("Init OnAbandonedOrderAction");
		storeRequiredParameter(pParameters, JMS_PARAM_ORDER_ID, String.class);
		storeOptionalParameter(pParameters, JMS_PARAM_PROFILE_ID, String.class);
	}

	/**
	 * Execute action.
	 *
	 * @param pProcessExecutionContext the p process execution context
	 * @throws ProcessException the process exception
	 */
	@Override
	protected void executeAction(ProcessExecutionContext pProcessExecutionContext) throws ProcessException {
		logDebug("Action start");
		VSGProfileTools profileTools = getCommonEmailSender().getProfileTools();
		RepositoryItem user = pProcessExecutionContext.getSubject();
		if (profileTools.isRegisteredUser(user)) {
			String orderId = (String) getParameterValue(JMS_PARAM_ORDER_ID, pProcessExecutionContext);
			logDebug("Execute action with orderId = " + orderId);
			String profileId = (String) getParameterValue(JMS_PARAM_PROFILE_ID, pProcessExecutionContext);
			try {
				RepositoryItem profile = profileTools.getProfileItem(profileId);
				logDebug("Execute action with profileId = " + profileId);
				if (StringUtils.isNotBlank(orderId) && StringUtils.isNotBlank(profileId)) {
					getCommonEmailSender().sendAbandonedCartEmail(profile);
				}
			} catch (RepositoryException e) {
				logError("Error creation email schedule for order: " + orderId + ", with profile: " + profileId, e);
			}

		}
		logDebug("Action end");
	}

}
