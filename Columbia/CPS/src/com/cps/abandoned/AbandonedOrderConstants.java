package com.cps.abandoned;

/**
 * The interface Abandoned order constants.
 */
public interface AbandonedOrderConstants {

	/**
	 * The constant ITEM_DESCRIPTOR.
	 */
	String ITEM_DESCRIPTOR = "abandonedEmailSchedule";
	/**
	 * The constant ITEM_FIELD_ID.
	 */
	String ITEM_FIELD_ID = "id";
	/**
	 * The constant ITEM_FIELD_NEXT_EMAIL_DATE.
	 */
	String ITEM_FIELD_NEXT_EMAIL_DATE = "nextEmailDate";
	/**
	 * The constant ITEM_FIELD_PROFILE_ID.
	 */
	String ITEM_FIELD_PROFILE_ID = "profileId";
	/**
	 * The constant ITEM_FIELD_ORDER_ID.
	 */
	String ITEM_FIELD_ORDER_ID = "orderId";
	/**
	 * The constant ITEM_FIELD_STEP.
	 */
	String ITEM_FIELD_STEP = "step";

	/**
	 * The constant EMAIL_STEP_FIRST.
	 */
	String EMAIL_STEP_FIRST = "first";
	/**
	 * The constant EMAIL_STEP_SECOND.
	 */
	String EMAIL_STEP_SECOND = "second";
	/**
	 * The constant EMAIL_STEP_THIRD.
	 */
	String EMAIL_STEP_THIRD = "third";
	/**
	 * The constant EMAIL_STEP_DONE.
	 */
	String EMAIL_STEP_DONE = "done";

	/* JMS Constants */

	/**
	 * The constant JMS_PARAM_ORDER_ID.
	 */
	String JMS_PARAM_ORDER_ID = "order_id";
	/**
	 * The constant JMS_PARAM_PROFILE_ID.
	 */
	String JMS_PARAM_PROFILE_ID = "profile_id";

}
