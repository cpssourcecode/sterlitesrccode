package com.cps.abandoned.schedulable;

import atg.commerce.order.abandoned.AbandonedOrderService;
import atg.commerce.order.abandoned.AbandonedOrderTools;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.RepositoryException;
import atg.repository.RepositoryView;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * The type CPS abandoned order service.
 */
public class CPSAbandonedOrderService extends AbandonedOrderService {

	/**
	 * The M enabled.
	 */
	private boolean mEnabled = false;
	/**
	 * The M test mode.
	 */
	private boolean mTestMode = false;


	/**
	 * count of minutes until abandoned
	 */
	private int mTestIdleMinutesUntilAbandoned;

	/**
	 * count of minutes until lost
	 */
	private int mTestIdleMinutesUntilLost;

	/**
	 * The M test schedule.
	 */
	private Schedule mTestSchedule;

	/**
	 * Do start service.
	 *
	 * @throws ServiceException the service exception
	 */
	@Override
	public void doStartService() throws ServiceException {
		if (isTestMode()) {
			doTestModeServiceStart();
		} else {
			super.doStartService();
		}
	}

	/**
	 * Do stop service.
	 *
	 * @throws ServiceException the service exception
	 */
	@Override
	public void doStopService() throws ServiceException {
		if (isTestMode()) {
			doTestModeServiceStop();
		}
		super.doStopService();
	}

	/**
	 * Do restart service.
	 *
	 * @throws ServiceException the service exception
	 */
	public void doRestartService() throws ServiceException {
		doStopService();
		doStartService();
	}

	/**
	 * Do scheduled task.
	 *
	 * @param pScheduler    the p scheduler
	 * @param pScheduledJob the p scheduled job
	 */
	@Override
	public void doScheduledTask(Scheduler pScheduler, ScheduledJob pScheduledJob) {

		vlogDebug("START service" + (isTestMode() ? "  in test mode." : "."));

		if (isEnabled() || isTestMode()) {
			super.doScheduledTask(pScheduler, pScheduledJob);
		} else {
			vlogDebug("Service is disabled");
		}

		vlogDebug("END service");
	}

	/**
	 * Gets date query for abandoned orders.
	 *
	 * @return the date query for abandoned orders
	 * @throws RepositoryException the repository exception
	 */
	@Override
	public Query getDateQueryForAbandonedOrders() throws RepositoryException {
		if (isTestMode()) {
			Query dateQuery = generateDateQueryForTest(getTestIdleMinutesUntilAbandoned());
			setDateQueryForAbandonedOrders(dateQuery);
			return dateQuery;
		} else {
			return super.getDateQueryForAbandonedOrders();
		}
	}

	/**
	 * Gets date query for lost orders.
	 *
	 * @return the date query for lost orders
	 * @throws RepositoryException the repository exception
	 */
	@Override
	public Query getDateQueryForLostOrders() throws RepositoryException {
		if (isTestMode()) {
			Query dateQuery = generateDateQueryForTest(getTestIdleMinutesUntilLost());
			setDateQueryForAbandonedOrders(dateQuery);
			return dateQuery;
		} else {
			return super.getDateQueryForLostOrders();
		}
	}

	/**
	 * Generate date query for test query.
	 *
	 * @param pIdleMinutes the p idle minutes
	 * @return the query
	 * @throws RepositoryException the repository exception
	 */
	protected Query generateDateQueryForTest(int pIdleMinutes) throws RepositoryException {
		AbandonedOrderTools abandonedOrderTools = getAbandonedOrderTools();
		MutableRepository orderRepository = abandonedOrderTools.getOrderRepository();
		RepositoryView orderView = orderRepository.getView(abandonedOrderTools.getOrderItemName());

		QueryBuilder queryBuilder = orderView.getQueryBuilder();

		QueryExpression datePropertyExpression = queryBuilder.createPropertyQueryExpression(getDateQueryPropertyName());

		Date currentDate = new Date(System.currentTimeMillis());
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(currentDate);
		calendar.add(Calendar.MINUTE, -pIdleMinutes);
		Date thresholdDate = calendar.getTime();

		QueryExpression thresholdTimeExpression = queryBuilder.createConstantQueryExpression(thresholdDate);
		return queryBuilder.createComparisonQuery(datePropertyExpression, thresholdTimeExpression, 6);
	}

	/**
	 * Do test mode service start.
	 */
	protected synchronized void doTestModeServiceStart() {
		setLoggingDebug(true);
		getAbandonedOrderTools().setLoggingDebug(true);
		vlogDebug("Starting service in test mode: " + getJobName());
		vlogDebug("Test params: " + "[idleMinutesUntilAbandoned]: " + mTestIdleMinutesUntilAbandoned
				+ ",\n[idleMinutesUntilLost]: " + mTestIdleMinutesUntilLost
				+ ",\n[schedule]: " + mTestSchedule);
		if (getScheduler() != null && getTestSchedule() != null && mJobId == -1) {
			ScheduledJob job = new ScheduledJob(getJobName(), getJobDescription(),
					getAbsoluteName(), getTestSchedule(), this, getThreadMethod(), isTransactional());
			mJobId = getScheduler().addScheduledJob(job);
			vlogDebug("Service started. [jobId]: " + mJobId);
		} else {
			vlogDebug("NOT starting. test mode scheduler: " + this.getScheduler() +
					", schedule: " + getTestSchedule() + ", job id: " + mJobId);
			doTestModeServiceStop();
		}
	}

	/**
	 * Do test mode service stop.
	 */
	protected void doTestModeServiceStop() {
		setLoggingDebug(false);
		getAbandonedOrderTools().setLoggingDebug(false);
	}

	/**
	 * Is enabled boolean.
	 *
	 * @return the boolean
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets enabled.
	 *
	 * @param pEnabled the p enabled
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	/**
	 * Is test mode boolean.
	 *
	 * @return the boolean
	 */
	public boolean isTestMode() {
		return mTestMode;
	}

	/**
	 * Sets test mode.
	 *
	 * @param pTestMode the p test mode
	 */
	public void setTestMode(boolean pTestMode) {
		mTestMode = pTestMode;
	}

	/**
	 * Gets test idle minutes until abandoned.
	 *
	 * @return the test idle minutes until abandoned
	 */
	public int getTestIdleMinutesUntilAbandoned() {
		return mTestIdleMinutesUntilAbandoned;
	}

	/**
	 * Sets test idle minutes until abandoned.
	 *
	 * @param pTestIdleMinutesUntilAbandoned the p test idle minutes until abandoned
	 */
	public void setTestIdleMinutesUntilAbandoned(int pTestIdleMinutesUntilAbandoned) {
		mTestIdleMinutesUntilAbandoned = pTestIdleMinutesUntilAbandoned;
	}

	/**
	 * Gets test idle minutes until lost.
	 *
	 * @return the test idle minutes until lost
	 */
	public int getTestIdleMinutesUntilLost() {
		return mTestIdleMinutesUntilLost;
	}

	/**
	 * Sets test idle minutes until lost.
	 *
	 * @param pTestIdleMinutesUntilLost the p test idle minutes until lost
	 */
	public void setTestIdleMinutesUntilLost(int pTestIdleMinutesUntilLost) {
		mTestIdleMinutesUntilLost = pTestIdleMinutesUntilLost;
	}

	/**
	 * Gets test schedule.
	 *
	 * @return the test schedule
	 */
	public Schedule getTestSchedule() {
		return mTestSchedule;
	}

	/**
	 * Sets test schedule.
	 *
	 * @param pTestSchedule the p test schedule
	 */
	public void setTestSchedule(Schedule pTestSchedule) {
		mTestSchedule = pTestSchedule;
	}

}
