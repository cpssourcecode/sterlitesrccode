package com.cps.multisite;

import atg.core.util.StringUtils;
import atg.multisite.Site;
import atg.multisite.SiteContextException;
import atg.multisite.SiteContextManager;
import atg.multisite.SiteURLManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import static com.cps.util.CPSConstants.ADDITIONAL_PRODUCTION_URLS;
import static com.cps.util.CPSConstants.HTTPS_PROTOCOL;
import static com.cps.util.CPSConstants.PRODUCTION_URL;

public class CPSSiteURLManager extends SiteURLManager {

	private String mSiteId;

	public String getSiteId() {
		return mSiteId;
	}

	public void setSiteId(String pSiteId) {
		mSiteId = pSiteId;
	}

	protected void appendSlashIfNeeded(StringBuffer pURLBuffer) {
	}

	public String getSiteUrl() {
		return getSiteUrl(SiteContextManager.getCurrentSite());
	}

	public String getSiteUrl(RepositoryItem pSite) {
		String protocol = HTTPS_PROTOCOL;
		String siteUrl = "";
		String[] urls = null;
		if (null != pSite) {
			urls = (String[]) pSite.getPropertyValue(ADDITIONAL_PRODUCTION_URLS);
		} else {
			try {
				RepositoryItem[] allSites = SiteContextManager.getSiteContextManager().getSiteManager().getAllSites();
				RepositoryItem repositoryItem = allSites[0];
				urls = (String[]) repositoryItem.getPropertyValue(ADDITIONAL_PRODUCTION_URLS);
			} catch (RepositoryException sce) {
				if (isLoggingError()) {
					logError(sce);
				}
			}
		}

		if (urls != null && urls.length > 0) {
			siteUrl = protocol + urls[0];
		} else if (pSite != null){
			siteUrl = protocol + (String) pSite.getPropertyValue(PRODUCTION_URL);
		}
		vlogDebug("siteUrl=" + siteUrl);

		return siteUrl;
	}

	/**
	 * Returns site url
	 *
	 * @param pSiteId default value
	 * @return
	 */
	public String getSiteUrl(String pSiteId) {
		Site site = SiteContextManager.getCurrentSite();
		if (null == site) {
			String siteId = pSiteId;
			try {
				if (StringUtils.isBlank(pSiteId)) {
					siteId = getSiteId();
				}
				site = SiteContextManager.getSiteContextManager().getSite(siteId);
			} catch (SiteContextException sce) {
				if (isLoggingError()) {
					logError(sce);
				}
			}
		}
		return getSiteUrl(site);
	}

}