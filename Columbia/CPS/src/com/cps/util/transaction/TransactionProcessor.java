package com.cps.util.transaction;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;

import javax.transaction.TransactionManager;

/**
 * @author Dmitry Golubev
 **/
public class TransactionProcessor extends GenericService {
	/**
	 * transaction manager
	 */
	TransactionManager mTransactionManager;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * call TransactionCommand.execute() in transaction
	 * @param pTransactionCommand transaction command
	 * @return true - error, false - ok
	 */
	public boolean doInTransaction(TransactionCommand pTransactionCommand) {
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();

		boolean shouldRollback = false;
		try {
			if (tm != null) {
				td.begin(tm, TransactionDemarcation.REQUIRED);
			}
			pTransactionCommand.execute();
		} catch (Exception e) {
			shouldRollback = true;
			vlogError(e, "Unable to execute operation in transaction");
		} finally {
			try {
				if (null != tm) {
					td.end(shouldRollback);
				}
			} catch (TransactionDemarcationException e) {
				vlogError(e, "Error in end transaction.");
			}
		}
		return shouldRollback;
	}

	//------------------------------------------------------------------------------------------------------------------


	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}
}
