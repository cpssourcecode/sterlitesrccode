package com.cps.util;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import atg.userprofiling.Profile;

import com.cps.email.CommonEmailSender;

import com.cps.util.validator.SharePageValidator;

import com.cps.userprofiling.CPSProfileTools;

import java.util.HashMap;
import java.util.Map;

import vsg.util.ajax.AjaxUtils;

public class SharePageFormHandler extends GenericFormHandler {
	private Profile profile;
	private CPSProfileTools profileTools;
	private CommonEmailSender commonEmailSender;
	private SharePageValidator sharePageValidator;

	// Form fields
	private String recipients;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String message;
	private Boolean sendCopyToSender;
	private String sharedUrl;

	/**
	 * request monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public void preSharePage(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("preSharePage - start");

		getSharePageValidator().validateRecipientInformation(getRecipients(), this, request);
		getSharePageValidator().validateSenderInformation(getFirstName(), getLastName(), getEmailAddress(), this, request);
		getSharePageValidator().validateSharedUrl(getSharedUrl(), this, request);

		vlogDebug("preSharePage - end");
	}

	public boolean handleSharePage(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("handleSharePage - start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "SharePageFormHandler.handleSharePage";
		boolean sendResult = false;

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {

				// Check if we need to grab the shared URL from the incoming request
				// It seems like this value in particular is often set to null
				if (null == getSharedUrl()) {
					setSharedUrl(request.getParameter("/cps/util/SharePageFormHandler.sharedUrl"));
					vlogDebug("handleSharePage - shared url null, had to grab value <" + getSharedUrl() + "> from request");
				}

				if (null == getMessage()) {
					setMessage(request.getParameter("/cps/util/SharePageFormHandler.message"));
					vlogDebug("handleSharePage - message null, had to grab value <" + getMessage() + "> from request");
				}

				vlogDebug("  url=" + getSharedUrl());
				vlogDebug("  recipients=" + getRecipients() + "; copyToSender=" + getSendCopyToSender());
				vlogDebug("  firstName=" + getFirstName() + "; lastName=" + getLastName() + "; email=" + getEmailAddress());
				vlogDebug("  message=" + getMessage());

				preSharePage(request, response);

				if (!getFormError()) {
					vlogDebug("Share Page calling e-mail sender...");
					if(getMessage().trim() != null && !getMessage().trim().isEmpty()){
						String message = new StringBuilder().append("Message : ")
								.append(getMessage()).toString();
						setMessage(message);
					}
					sendResult = getCommonEmailSender().sendSharePageEmail(getEmailAddress(), getFirstName(), getLastName(), getRecipients(), getMessage(), getSharedUrl());

					if (getSendCopyToSender() == true) {
						vlogDebug("Share Page calling e-mail sender for copy to user...");
						sendResult = sendResult && getCommonEmailSender().sendSharePageEmail(getEmailAddress(), getFirstName(), getLastName(), getEmailAddress(), getMessage(), getSharedUrl());
					}
				}

				postSharePage(request, response);
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		vlogDebug("handleSharePage - end, send result=" + sendResult);

		if (AjaxUtils.isAjaxRequest(request)) {
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("sent", sendResult);
			AjaxUtils.addAjaxResponse(this, response, result);
			return false;
		}
		else {
			return (!getFormError());
		}
	}

	public void postSharePage(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("postSharePage - start");

		vlogDebug("postSharePage - end");
	}

	/*********************************************************/

	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	public SharePageValidator getSharePageValidator() {
		return sharePageValidator;
	}

	public void setSharePageValidator(SharePageValidator sharePageValidator) {
		this.sharePageValidator = sharePageValidator;
	}

	// Form fields
	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	public String getRecipients() {
		return this.recipients;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public void setSendCopyToSender(Boolean sendCopyToSender) {
		this.sendCopyToSender = sendCopyToSender;
	}

	public Boolean getSendCopyToSender() {
		return this.sendCopyToSender;
	}

	public void setSharedUrl(String sharedUrl) {
		this.sharedUrl = sharedUrl;
	}

	public String getSharedUrl() {
		return this.sharedUrl;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

}