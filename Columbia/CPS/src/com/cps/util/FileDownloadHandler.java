package com.cps.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cps.droplet.DropletConstants;
import com.cps.scheduler.manager.RegisteredUserReportManager;
import com.cps.scheduler.manager.SalesOrderReportManager;
import com.cps.userprofiling.CPSProfileTools;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;
import atg.userprofiling.Profile;
import vsg.util.ajax.AjaxUtils;

public class FileDownloadHandler extends GenericFormHandler {

	private FilePermissionsHelper filePermissionsHelper;

	private CPSSessionBean sessionBean;

	private String createUserSpreadsheetSuccessURL;

	private String createUserSpreadsheetErrorURL;

	private Profile profile;

	private UserDirectory userDirectory;

	private CPSProfileTools mProfileTools;

	private RegisteredUserReportManager registeredUserReportManager;
	private List<String> headerColumns;
	private String reportQuery;
	private String reportName;
	private String sheetName;
	private Map<String,String> activeUsersQueryMap;
	private SalesOrderReportManager salesOrderReportManager;
	private String reportDay;
	private String salesReportSuccessURL;
	private String salesReportErrorURL;

	/**
	 * request monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public boolean handleCreateSpreadsheet(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("handleCreateSpreadsheet.start");

		// List<CPSInvoiceBean> invoices = getSessionBean().getInvoices();
		//
		// if (invoices != null) {
		// try {
		// logDebug("Write invoices to spreadsheet: " + invoices.toString());
		// HSSFWorkbook workbook = new HSSFWorkbook();
		// HSSFName name;
		//
		// name = workbook.createName();
		// name.setNameName("Invoices");
		// HSSFSheet worksheet = workbook.createSheet("Invoices");
		// HSSFCellStyle cellStyle = workbook.createCellStyle();
		// HSSFDataFormat df = workbook.createDataFormat();
		// cellStyle.setDataFormat(df.getFormat("$#,##0.00"));
		//
		// HSSFRow titleRow = worksheet.createRow(0);
		// HSSFCell titleCell1 = titleRow.createCell(0);
		// HSSFCell titleCell2 = titleRow.createCell(1);
		// HSSFCell titleCell3 = titleRow.createCell(2);
		// HSSFCell titleCell4 = titleRow.createCell(3);
		// HSSFCell titleCell5 = titleRow.createCell(4);
		// titleCell1.setCellValue("ORDER #");
		// titleCell2.setCellValue("INVOICED");
		// titleCell3.setCellValue("DUE");
		// titleCell4.setCellValue("CS");
		// titleCell5.setCellValue("AMOUNT");
		//
		// int count = 1;
		// for (CPSInvoiceBean invoice : invoices) {
		// HSSFRow itemRow = worksheet.createRow(count++);
		// HSSFCell itemCell1 = itemRow.createCell(0);
		// HSSFCell itemCell2 = itemRow.createCell(1);
		// HSSFCell itemCell3 = itemRow.createCell(2);
		// HSSFCell itemCell4 = itemRow.createCell(3);
		// HSSFCell itemCell5 = itemRow.createCell(4);
		// itemCell1.setCellValue(invoice.getInvoiceNumber());
		// itemCell2.setCellValue(new
		// SimpleDateFormat("MM-dd-yyyy").format(invoice.getInvoicedDate().getTime()));
		// Calendar status = invoice.getDueDate();
		// if (status != null) {
		// itemCell3.setCellValue(new
		// SimpleDateFormat("MM-dd-yyyy").format(status.getTime()));
		// } else {
		// itemCell3.setCellValue("Paid");
		// }
		// itemCell4.setCellValue(invoice.getInvoiceCS());
		// itemCell5.setCellValue(invoice.getInvoiceAmount());
		// itemCell5.setCellStyle(cellStyle);
		// }
		//
		// worksheet.autoSizeColumn(0);
		// worksheet.autoSizeColumn(1);
		// worksheet.autoSizeColumn(2);
		// worksheet.autoSizeColumn(3);
		// worksheet.autoSizeColumn(4);
		//
		// logDebug("Write file");
		// pResponse.setContentType("application/vnd.ms-excel");
		// pResponse.setHeader("Content-Disposition",
		// "download;filename=\"Invoices.xls\"");
		// ServletOutputStream out = pResponse.getOutputStream();
		// workbook.write(out);
		// out.close();
		// } catch (Exception e) {
		// vlogError(e, "Error");
		// }
		// } else {
		// vlogDebug("No invoices to write");
		// }

		return false;
	}
	
	/**
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleExportBulkUsers(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("HandleExportBulkUsers.start");
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "FileDownloadHandler.handleExportBulkUsers";
		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if(!getSessionBean().getBccImportedUsersMap().isEmpty() && getSessionBean().getBccImportedUsersMap() != null){
					String url = CreateBulkUsersSpreadSheet(getSessionBean().getBccImportedUsersMap());
					if (StringUtils.isNotEmpty(url)) {
						pResponse.setContentType("application/vnd.ms-excel");
						pResponse.setHeader("Content-Disposition", "attachment; filename=\"BulkUsersList.xls\"");
						pResponse.sendRedirect(url, true);
					}
				}else{
					vlogInfo("There is no users to import");
				}
				getSessionBean().getBccImportedUsersMap().clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		
		return false;
	}

	/**
	 * 
	 * @param bccImportedUsersMap
	 * @return
	 */
	private String CreateBulkUsersSpreadSheet(Map<String, String> bccImportedUsersMap) {
		if (!bccImportedUsersMap.isEmpty() && bccImportedUsersMap != null) {
			vlogDebug("Total number of users : {0}", bccImportedUsersMap.size());
			Set<String> userEmails = bccImportedUsersMap.keySet();
			for (String email : userEmails) {
				vlogDebug("user mail : {0} PreRegistration Link {1}", email, bccImportedUsersMap.get(email));
			}

			Workbook workbook = new XSSFWorkbook();
			// Create a Sheet
			Sheet sheet = workbook.createSheet("BulkUsers");

			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setFontHeightInPoints((short) 10);
			headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			// Create a Row
			Row headerRow = sheet.createRow(0);
			for (int i = 0; i < getHeaderColumns().size(); i++) {
				createHeaderCell(getHeaderColumns(), headerCellStyle, headerRow, i);
			}

			int rowNum = 1;
			for (String email : userEmails) {
				vlogDebug("user mail : {0} PreRegistration Link {1}", email, bccImportedUsersMap.get(email));
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(email);
				row.createCell(1).setCellValue(bccImportedUsersMap.get(email));
			}
			for (int i = 0; i < getHeaderColumns().size(); i++) {
				sheet.autoSizeColumn(i);
			}
			try {
				// Write the output to a file
				FileOutputStream fileOut = null;
				StringBuilder localFilePath = new StringBuilder();
				StringBuilder filePath = new StringBuilder();
				Timestamp currentDate = new Timestamp(System.currentTimeMillis());

				String fileLocation = filePath.append(CPSConstants.HTTP_PROTOCOL)
						.append(getRegisteredUserReportManager().getActiveUserExportHostName())
						.append("/reports/BulkUsersList").append(currentDate).append(".xlsx").toString();
				String localFileLocation = localFilePath.append("/apps/feeds/reports/BulkUsersList").append(currentDate)
						.append(".xlsx").toString();
				fileLocation = fileLocation.replaceAll("\\s", "_");
				localFileLocation = localFileLocation.replaceAll("\\s", "_");
				fileOut = new FileOutputStream(localFileLocation);
				workbook.write(fileOut);
				fileOut.close();
				getFilePermissionsHelper().setFilePermissions(localFileLocation);

				return fileLocation;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	/**
	 * 
	 * @param headerColumns
	 * @param headerCellStyle
	 * @param headerRow
	 * @param i
	 */
	private void createHeaderCell(List<String> headerColumns, CellStyle headerCellStyle, Row headerRow, int i) {
		Cell cell = headerRow.createCell(i);
		cell.setCellValue((String) headerColumns.get(i));
		cell.setCellStyle(headerCellStyle);
	}

	public boolean handleExportUserSpreadsheet(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("HandleExportUserSpreadsheet.start");
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "FileDownloadHandler.handleExportUserSpreadsheet";
		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				String  activeUsersExport = getRegisteredUserReportManager().generateActiveUserReport(getActiveUsersQueryMap().get(getReportQuery()) ,getReportName(),getSheetName());
				if (!StringUtils.isBlank(activeUsersExport)) {
					vlogDebug("Exporting members");
					try {
						pResponse.setContentType("application/vnd.ms-excel");
						pResponse.setHeader("Content-Disposition", "attachment; filename=\"ExportActiveUsers.xls\"");
						pResponse.sendRedirect(activeUsersExport ,true);
					} catch (Exception e) {
						vlogError(e, "Error");
					}
					return false;
					
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return checkFormRedirect(getCreateUserSpreadsheetSuccessURL(), getCreateUserSpreadsheetErrorURL(), pRequest,
				pResponse);
	}

	public boolean handleCreateUserSpreadsheet(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("HandleCreateUserSpreadsheet.start");
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "FileDownloadHandler.handleCreateUserSpreadsheet";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				RepositoryItem parentOrg = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
				Map<String, Object> result = new HashMap<String, Object>();

				if (parentOrg != null) {
					Set<RepositoryItem> members = (Set<RepositoryItem>) parentOrg.getPropertyValue(DropletConstants.MEMBERS);

					if (members != null && !members.isEmpty()) {
						vlogDebug("Exporting members");
						try {
							HSSFWorkbook workbook = createUserSpreadsheet(members);
							pResponse.setContentType("application/vnd.ms-excel");
							pResponse.setHeader("Content-Disposition", "attachment; filename=\"ManageUsers.xls\"");
							ServletOutputStream out = pResponse.getOutputStream();
							workbook.write(out);
							out.close();
						} catch (Exception e) {
							vlogError(e, "Error");
						}
						return false;
					}
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return checkFormRedirect(getCreateUserSpreadsheetSuccessURL(), getCreateUserSpreadsheetErrorURL(), pRequest,
				pResponse);
	}
	
	/**
	 * @param req
	 * @param res
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleGenerateSalesReport(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("HandleGenerateSalesReport.start");
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "FileDownloadHandler.handleGenerateSalesReport";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (StringUtils.isBlank(getReportDay())) {
					CPSMessageUtils.addFormException(this, CPSErrorCodes.ERR_SALES_REPORT_SELECT_OPTION,
							pRequest.getLocale(), null);
				} else {
					int reportDate = -1;
					if (getReportDay().equalsIgnoreCase("currentDay")) {
						reportDate = 0;
					}
					RepositoryItem[] results = getSalesOrderReportManager().getRepositoryUtils().executeQuery(
							CPSConstants.ITEM_DESCRIPTOR_ORDER, getSalesOrderReportManager().getOrderRepository(),
							getSalesOrderReportManager().getRqlQuerySalesOrder(),
							getSalesOrderReportManager().getRqlQueryParams(reportDate));
					if (results != null && results.length > 0) {
						Workbook workbook = getSalesOrderReportManager().createWorkbook(results,
								getSalesOrderReportManager().getSalesOrderReportFileName(),
								getSalesOrderReportManager().getSalesOrderReportSheetName());
						if (null != workbook) {
							if (CPSConstants.FILE_EXT_XLS.equals(getSalesOrderReportManager().getFileExtension())) {
								pResponse.setContentType("application/vnd.ms-excel");
								pResponse.setHeader("Content-Disposition",
										"download;filename=\""
												+ getSalesOrderReportManager().getSalesOrderReportFileName()
												+ CPSConstants.FILE_EXT_XLS + "\"");
								ServletOutputStream out = pResponse.getOutputStream();
								workbook.write(out);
								out.close();
							} else if (CPSConstants.FILE_EXT_XLSX
									.equals(getSalesOrderReportManager().getFileExtension())) {
								pResponse.setContentType("application/vnd.ms-excel");
								pResponse.setHeader("Content-Disposition",
										"download;filename=\""
												+ getSalesOrderReportManager().getSalesOrderReportFileName()
												+ CPSConstants.FILE_EXT_XLSX + "\"");
								ServletOutputStream out = pResponse.getOutputStream();
								workbook.write(out);
								out.close();
							} else {
								CPSMessageUtils.addFormException(this, "There is no file format", pRequest.getLocale(), null);
							}
						}
					} else {
						CPSMessageUtils.addFormException(this, "No records found", pRequest.getLocale(), null);
					}
				}
			} catch (RepositoryException e) {
				e.printStackTrace();
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return checkFormRedirect(getSalesReportSuccessURL(), getSalesReportErrorURL(), pRequest, pResponse);
	}

	private HSSFWorkbook createUserSpreadsheet(Set<RepositoryItem> members) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFName name = workbook.createName();
		name.setNameName("Manage_Users");
		HSSFSheet worksheet = workbook.createSheet("Users");
		HSSFCellStyle cellStyle = workbook.createCellStyle();
		HSSFDataFormat df = workbook.createDataFormat();
		cellStyle.setDataFormat(df.getFormat("$#,##0.00"));
		HSSFRow titleRow = worksheet.createRow(0);
		HSSFCell titleCell1 = titleRow.createCell(0);
		HSSFCell titleCell2 = titleRow.createCell(1);
		HSSFCell titleCell3 = titleRow.createCell(2);
		HSSFCell titleCell4 = titleRow.createCell(3);
		HSSFCell titleCell5 = titleRow.createCell(4);
		HSSFCell titleCell6 = titleRow.createCell(5);
		HSSFCell titleCell7 = titleRow.createCell(6);
		HSSFCell titleCell8 = titleRow.createCell(7);
		titleCell1.setCellValue("First Name");
		titleCell2.setCellValue("Last Name");
		titleCell3.setCellValue("Email Address");
		titleCell4.setCellValue("Role");
		titleCell5.setCellValue("Company");
		titleCell6.setCellValue("Spend Limit");
		titleCell7.setCellValue("Frequency");
		titleCell8.setCellValue("Status");

		createUserRows(worksheet, members);

		for (int i = 0; i < 8; i++) {
			worksheet.autoSizeColumn(i);
		}

		return workbook;
	}

	private void createUserRows(HSSFSheet worksheet, Set<RepositoryItem> members) {
		int count = 1;
		for (RepositoryItem userItem : members) {
			HSSFRow itemRow = worksheet.createRow(count++);
			HSSFCell itemCell1 = itemRow.createCell(0);
			HSSFCell itemCell2 = itemRow.createCell(1);
			HSSFCell itemCell3 = itemRow.createCell(2);
			HSSFCell itemCell4 = itemRow.createCell(3);
			HSSFCell itemCell5 = itemRow.createCell(4);
			HSSFCell itemCell6 = itemRow.createCell(5);
			HSSFCell itemCell7 = itemRow.createCell(6);
			HSSFCell itemCell8 = itemRow.createCell(7);
			String firstName = (String) userItem.getPropertyValue(CPSConstants.FIRST_NAME);
			if (StringUtils.isNotBlank(firstName)) {
				itemCell1.setCellValue(firstName);
			}
			String lastName = (String) userItem.getPropertyValue(CPSConstants.LAST_NAME);
			if (StringUtils.isNotBlank(lastName)) {
				itemCell2.setCellValue(lastName);
			}
			String email = (String) userItem.getPropertyValue(CPSConstants.EMAIL);
			if (StringUtils.isNotBlank(email)) {
				itemCell3.setCellValue(email);
			}
			itemCell4.setCellValue(getUserRoles(userItem));
			itemCell5.setCellValue(getUserCompany(userItem));
			if (userItem.getPropertyValue(CPSConstants.ORDER_LIMIT) != null) {
				itemCell6.setCellValue(((Double) userItem.getPropertyValue(CPSConstants.ORDER_LIMIT)).toString());
			}
			String frequency = (String) userItem.getPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY);
			if (StringUtils.isNotBlank(frequency)) {
				itemCell7.setCellValue(frequency);
			}
			if (userItem.getPropertyValue(CPSConstants.ON_HOLD) != null) {
				itemCell8.setCellValue(getUserStatus(userItem));
			}
		}
	}

	private String getUserRoles(RepositoryItem userItem) {
		String roles = "";

		String userId = userItem.getRepositoryId();

		User user = null;

		if (getUserDirectory() != null) {
			user = getUserDirectory().findUserByPrimaryKey(userId);
		}

		if (user != null) {
			// logDebug("User found, check roles");
			Collection collection = user.getAssignedRoles();
			for (Iterator iterator = collection.iterator(); iterator.hasNext();) {
				Role role = (Role) iterator.next();
				if (role != null) {
					// logDebug("Role name: "+role.getName());
					if (StringUtils.isBlank(roles)) {
						roles = getProfileTools().formatRole(role.getName());
					} else {
						roles += ", " + getProfileTools().formatRole(role.getName());
					}
				}
			}
		}

		return roles;
	}

	private String getUserCompany(RepositoryItem user) {
		String companyName = "";

		RepositoryItem parentOrg = (RepositoryItem) user.getPropertyValue(CPSConstants.PARENT_ORG);
		if (parentOrg != null) {
			companyName = (String) parentOrg.getPropertyValue(CPSConstants.NAME);
		}

		return companyName;
	}

	private String getUserStatus(RepositoryItem user) {
		String userStatus = "";
		Boolean isActive = (Boolean) user.getPropertyValue(CPSConstants.IS_ACTIVE);
		if (isActive == null) {
			isActive = false;
		}
		if (isActive) {
			userStatus = "Active";
		} else {
			userStatus = "Deactivated";
		}

		return userStatus;
	}

	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (AjaxUtils.isAjaxRequest(pRequest)) {
			// add ajax response in handle method
			return false;
		} else {
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}
	}

	public CPSSessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(CPSSessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getCreateUserSpreadsheetSuccessURL() {
		return createUserSpreadsheetSuccessURL;
	}

	public void setCreateUserSpreadsheetSuccessURL(String createUserSpreadsheetSuccessURL) {
		this.createUserSpreadsheetSuccessURL = createUserSpreadsheetSuccessURL;
	}

	public String getCreateUserSpreadsheetErrorURL() {
		return createUserSpreadsheetErrorURL;
	}

	public void setCreateUserSpreadsheetErrorURL(String createUserSpreadsheetErrorURL) {
		this.createUserSpreadsheetErrorURL = createUserSpreadsheetErrorURL;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public UserDirectory getUserDirectory() {
		return userDirectory;
	}

	public void setUserDirectory(UserDirectory userDirectory) {
		this.userDirectory = userDirectory;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	/**
	 * @return the registeredUserReportManager
	 */
	public RegisteredUserReportManager getRegisteredUserReportManager() {
		return registeredUserReportManager;
	}

	/**
	 * @param registeredUserReportManager
	 *            the registeredUserReportManager to set
	 */
	public void setRegisteredUserReportManager(RegisteredUserReportManager registeredUserReportManager) {
		this.registeredUserReportManager = registeredUserReportManager;
	}

	/**
	 * @return the headerColumns
	 */
	public List<String> getHeaderColumns() {
		return headerColumns;
	}

	/**
	 * @param headerColumns the headerColumns to set
	 */
	public void setHeaderColumns(List<String> headerColumns) {
		this.headerColumns = headerColumns;
	}

	/**
	 * @return the filePermissionsHelper
	 */
	public FilePermissionsHelper getFilePermissionsHelper() {
		return filePermissionsHelper;
	}

	/**
	 * @param filePermissionsHelper the filePermissionsHelper to set
	 */
	public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
		this.filePermissionsHelper = filePermissionsHelper;
	}

	/**
	 * @return the reportQuery
	 */
	public String getReportQuery() {
		return reportQuery;
	}

	/**
	 * @param reportQuery the reportQuery to set
	 */
	public void setReportQuery(String reportQuery) {
		this.reportQuery = reportQuery;
	}
	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * @param reportName the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @return the activeUsersQueryMap
	 */
	public Map<String,String> getActiveUsersQueryMap() {
		return activeUsersQueryMap;
	}

	/**
	 * @param activeUsersQueryMap the activeUsersQueryMap to set
	 */
	public void setActiveUsersQueryMap(Map<String,String> activeUsersQueryMap) {
		this.activeUsersQueryMap = activeUsersQueryMap;
	}

	/**
	 * @return the sheetName
	 */
	public String getSheetName() {
		return sheetName;
	}

	/**
	 * @param sheetName the sheetName to set
	 */
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	/**
	 * @return the salesOrderReportManager
	 */
	public SalesOrderReportManager getSalesOrderReportManager() {
		return salesOrderReportManager;
	}

	/**
	 * @param salesOrderReportManager the salesOrderReportManager to set
	 */
	public void setSalesOrderReportManager(SalesOrderReportManager salesOrderReportManager) {
		this.salesOrderReportManager = salesOrderReportManager;
	}

	/**
	 * @return the reportDay
	 */
	public String getReportDay() {
		return reportDay;
	}

	/**
	 * @param reportDay the reportDay to set
	 */
	public void setReportDay(String reportDay) {
		this.reportDay = reportDay;
	}

	/**
	 * @return the salesReportSuccessURL
	 */
	public String getSalesReportSuccessURL() {
		return salesReportSuccessURL;
	}

	/**
	 * @param salesReportSuccessURL the salesReportSuccessURL to set
	 */
	public void setSalesReportSuccessURL(String salesReportSuccessURL) {
		this.salesReportSuccessURL = salesReportSuccessURL;
	}

	/**
	 * @return the salesReportErrorURL
	 */
	public String getSalesReportErrorURL() {
		return salesReportErrorURL;
	}

	/**
	 * @param salesReportErrorURL the salesReportErrorURL to set
	 */
	public void setSalesReportErrorURL(String salesReportErrorURL) {
		this.salesReportErrorURL = salesReportErrorURL;
	}

}