/**
 * 
 */
package com.cps.util;

import static java.nio.file.FileVisitResult.CONTINUE;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import atg.nucleus.GenericService;

/**
 * A class that is used to set the file permissions on the files that are created by the application. needed, because the application puts only the user and the
 * group permissions and for others it would be none.
 * 
 * @author R.Srinivasan
 *
 */
public class FilePermissionsHelper extends GenericService implements FileVisitor<Path> {

    private boolean groupRead;
    private boolean groupWrite;
    private boolean groupExecute;

    private boolean othersRead;
    private boolean othersWrite;
    private boolean othersExecute;

    /**
     * @return the groupRead
     */
    public boolean isGroupRead() {
        return groupRead;
    }

    /**
     * @param groupRead
     *            the groupRead to set
     */
    public void setGroupRead(boolean groupRead) {
        this.groupRead = groupRead;
    }

    /**
     * @return the groupWrite
     */
    public boolean isGroupWrite() {
        return groupWrite;
    }

    /**
     * @param groupWrite
     *            the groupWrite to set
     */
    public void setGroupWrite(boolean groupWrite) {
        this.groupWrite = groupWrite;
    }

    /**
     * @return the groupExecute
     */
    public boolean isGroupExecute() {
        return groupExecute;
    }

    /**
     * @param groupExecute
     *            the groupExecute to set
     */
    public void setGroupExecute(boolean groupExecute) {
        this.groupExecute = groupExecute;
    }

    /**
     * @return the othersRead
     */
    public boolean isOthersRead() {
        return othersRead;
    }

    /**
     * @param othersRead
     *            the othersRead to set
     */
    public void setOthersRead(boolean othersRead) {
        this.othersRead = othersRead;
    }

    /**
     * @return the othersWrite
     */
    public boolean isOthersWrite() {
        return othersWrite;
    }

    /**
     * @param othersWrite
     *            the othersWrite to set
     */
    public void setOthersWrite(boolean othersWrite) {
        this.othersWrite = othersWrite;
    }

    /**
     * @return the othersExecute
     */
    public boolean isOthersExecute() {
        return othersExecute;
    }

    /**
     * @param othersExecute
     *            the othersExecute to set
     */
    public void setOthersExecute(boolean othersExecute) {
        this.othersExecute = othersExecute;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        setFilePermissions(dir);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        setFilePermissions(file);
        return CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        if (exc != null)
            System.err.println("WARNING: " + exc);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println("WARNING: " + exc);
        return CONTINUE;
    }

    /**
     * sets the POSIX File permissions. will not work on Windows OS
     * 
     * @param destFilePath
     * @throws IOException
     */
    public void setFilePermissions(Path destFilePath) {
        Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
        // add owners permission
        perms.add(PosixFilePermission.OWNER_READ);
        perms.add(PosixFilePermission.OWNER_WRITE);
        perms.add(PosixFilePermission.OWNER_EXECUTE);
        // add group permissions
        if (isGroupRead()) {
            perms.add(PosixFilePermission.GROUP_READ);
        }
        if (isGroupWrite()) {
            perms.add(PosixFilePermission.GROUP_WRITE);
        }
        if (isGroupExecute()) {
            perms.add(PosixFilePermission.GROUP_EXECUTE);
        }
        // add others permissions
        if (isOthersRead()) {
            perms.add(PosixFilePermission.OTHERS_READ);
        }
        if (isOthersWrite()) {
            perms.add(PosixFilePermission.OTHERS_WRITE);
        }
        if (isOthersExecute()) {
            perms.add(PosixFilePermission.OTHERS_EXECUTE);
        }

        try {
            Files.setPosixFilePermissions(destFilePath, perms);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * sets the POSIX File permissions. will not work on Windows OS
     * 
     * @param destinationFile
     * @throws IOException
     */
    public void setFilePermissions(String destinationFile) throws IOException {
        File destFile = new File(destinationFile);
        Path destPath = destFile.toPath();
        setFilePermissions(destPath);
    }

    public void setFilePermissionsRecursivelyOnDir(Path dirPath) throws IOException {

        int maxDepth = Integer.MAX_VALUE;
        Set<FileVisitOption> opts = Collections.emptySet();
        Files.walkFileTree(dirPath, opts, maxDepth, this);

    }

    public void setFilePermissionsRecursivelyOnDir(String dirPathStr) throws IOException {

        int maxDepth = Integer.MAX_VALUE;
        Set<FileVisitOption> opts = Collections.emptySet();
        Files.walkFileTree(new File(dirPathStr).toPath(), opts, maxDepth, this);

    }

}
