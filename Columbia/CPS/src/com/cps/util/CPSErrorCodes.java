package com.cps.util;

public interface CPSErrorCodes {

    // ***********************************************************************
    // ERROR CODES THAT WILL BE USED TO PULL MESSAGES FROM MESSAGE REPOSITORY
    // ***********************************************************************
	
	// Generic Error Message
    String ERR_GENERIC = "errGeneric";

    // Registration Errors
    String ERR_REG_MISSING_FIRST_NAME = "errRegMissFirstName";
    String ERR_REG_INVALID_FIRST_NAME = "errRegInvFirstName";
    String ERR_REG_MISSING_LAST_NAME = "errRegMissLastName";
    String ERR_REG_INVALID_LAST_NAME= "errRegInvLastName";
    String ERR_REG_MISSING_EMAIL = "errRegMissEmail";
    String ERR_REG_INVALID_EMAIL = "errRegInvEmail";
    String ERR_REG_EMAIL_EXISTS = "errRegEmailExists";
    String ERR_REG_MISSING_CONFIRM_EMAIL = "errRegMissConfEmail";
    String ERR_REG_EMAIL_MISMATCH = "errRegEmailMismatch";
    String ERR_REG_MISSING_PSWD = "errRegMissPass";
    String ERR_REG_INVALID_PSWD = "errRegInvPass";
    String ERR_REG_MISSING_CONFIRM_PSWD = "errRegMissConfPass";
    String ERR_REG_PSWD_MISMATCH = "errRegPasswordMismatch";
    String ERR_REG_MISSING_SECURITY_QUESTION = "errRegMissSecQues";
    String ERR_REG_MISSING_SECURITY_ANSWER = "errRegMissSecAns";
    String ERR_REG_INVALID_SECURITY_ANSWER = "errRegInvSecAns";
    String ERR_REG_MISSING_ACCOUNT = "errRegMissAcc";
    String ERR_REG_INVALID_ACCOUNT = "errRegInvAcc";
    String ERR_REG_MISSING_ZIP = "errRegMissZip";
    String ERR_REG_INVALID_ZIP = "errRegInvZip";
    String ERR_REG_INVALID_ACCOUNT_ZIP_COMB = "errRegInvAccZipComb";
    String ERR_REG_MISSING_SUPERVISOR_NAME = "errRegMissSupervisorName";
    String ERR_REG_INVALID_SUPERVISOR_NAME= "errRegInvSupervisorName";
    String ERR_REG_MISSING_SUPERVISOR_EMAIL = "errRegMissSupervisorEmail";
    String ERR_REG_SUPERVISOR_EMAIL_EXISTS = "errRegInvSupervisorEmailExists";
    String ERR_REG_INVALID_SUPERVISOR_EMAIL = "errRegInvSupervisorEmail";
    String ERR_INVALID_ORG_SUPERVISOR_EMAIL = "errInvalidOrgSupervisorEmail";


    String ERR_REG_MISSING_COMPANY = "errRegMissCompany";
    String ERR_REG_INVALID_COMPANY = "errRegInvCompany";
    String ERR_REG_INVALID_TITLE = "errRegInvTitle";
    String ERR_REG_MISSING_ADDRESS1 = "errRegMissAdd1";
    String ERR_REG_INVALID_ADDRESS1 = "errRegInvAdd1";
    String ERR_REG_INVALID_ADDRESS2 = "errRegInvAdd2";
    String ERR_REG_MISSING_CITY = "errRegMissCity";
    String ERR_REG_INVALID_CITY = "errRegInvCity";
    String ERR_REG_MISSING_STATE = "errRegMissState";
    String ERR_REG_MISSING_POSTAL_CODE = "errRegMissPostCode";
    String ERR_REG_INVALID_POSTAL_CODE = "errRegInvPostCode";
    String ERR_REG_MISSING_PRIMARY_PHONE = "errRegMissPrimPhone";
    String ERR_REG_INVALID_PRIMARY_PHONE = "errRegInvPrimePhone";
    String ERR_REG_INVALID_PRIMARY_PHONE_EXT = "errRegInvPrimPhoneExt";
    String ERR_REG_INVALID_ALTERNATE_PHONE = "errRegInvAltPhone";
    String ERR_REG_INVALID_ALTERNATE_PHONE_EXT = "errRegInvAltPhoneExt";
    String ERR_REG_INVALID_FAX = "errRegInvFax";

    String ERR_REG_MISSING_ACTION = "errRegMissAction";

    // Login Errors
    String ERR_LOGIN_MISSING_LOGIN = "errLoginMissLogin";
    String ERR_LOGIN_MISSING_PSWD = "errLoginMissPass";
    String ERR_LOGIN_INVALID_EMAIL = "errLoginInvalidEmail";
    String ERR_LOGIN_INVALID_PSWD = "errLoginInvalidPassword";
    String ERR_LOGIN_INVALID_CREDENTIALS = "err_invalid_email_password";
    String ERR_LOGIN_INV_CAPTCHA = "errLoginInvCaptcha";
    String ERR_LOGIN_SESSION_LOCKED = "err_captcha_fail";
    String ERR_LOGIN_TO_CHECK = "errLoginToCheckout";
    String ERR_LOGIN_DEACTIVATED_USER = "err_profile_deactivate";
    String ERR_LOGIN_EMAIL_NOT_FOUND = "err_invalid_email";
    String ERR_LOGIN_ACCOUNT_INACTIVE = "errLoginAccountInactive";

    // Profile Update Errors
    String ERR_UPDATE_MISSING_FIRST_NAME = "errUpdMissFirstName";
    String ERR_UPDATE_INVALID_FIRST_NAME = "errUpdInvFirstName";
    String ERR_UPDATE_MISSING_LAST_NAME = "errUpdMissLastName";
    String ERR_UPDATE_INVALID_LAST_NAME = "errUpdInvLastName";
    String ERR_UPDATE_MISSING_EMAIL = "errUpdMissEmail";
    String ERR_UPDATE_INVALID_EMAIL = "errUpdInvEmail";
    String ERR_UPDATE_MISSING_CONFIRM_EMAIL = "errUpdMissConfEmail";
    String ERR_UPDATE_EMAIL_MISMATCH = "errUpdEmailMismatch";
    String ERR_UPDATE_MISSING_PSWD = "errUpdMissPass";
    String ERR_UPDATE_INVALID_PSWD = "errUpdInvPass";
    String ERR_UPDATE_MISSING_CONFIRM_PSWD = "errUpdMissConfPass";
    String ERR_UPDATE_PSWD_MISMATCH = "errUpdPassMismatch";
    String ERR_UPDATE_MISSING_SECURITY_QUESTION = "errUpdMissSecQues";
    String ERR_UPDATE_MISSING_SECURITY_ANSWER = "errUpdMissSecAns";
    String ERR_UPDATE_INVALID_SECURITY_ANSWER = "errUpdInvSecAns";
    String ERR_UPDATE_ACCOUNT_EXISTS = "errUpdAccExists";
    String ERR_UPDATE_MISSING_TITLE = "errUpdMissTitle";
    String ERR_UPDATE_INVALID_TITLE = "errUpdInvTitle";
    String ERR_UPDATE_MISSING_PRIMARY_PHONE = "errUpdMissPrimPhone";
    String ERR_UPDATE_INVALID_PRIMARY_PHONE = "errUpdInvPrimPhone";
    String ERR_UPDATE_MISSING_ALTERNATE_PHONE = "errUpdMissAltPhone";
    String ERR_UPDATE_INVALID_ALTERNATE_PHONE = "errUpdInvAltPhone";
    String ERR_UPDATE_MISSING_ADDRESS1 = "errUpdMissAddress1";
    String ERR_UPDATE_INVALID_ADDRESS1 = "errUpdInvAddress1";
    String ERR_UPDATE_MISSING_ADDRESS2 = "errUpdMissAddress2";
    String ERR_UPDATE_INVALID_ADDRESS2 = "errUpdInvAddress2";
    String ERR_UPDATE_MISSING_CITY = "errUpdMissCity";
    String ERR_UPDATE_INVALID_CITY = "errUpdInvCity";
    String ERR_UPDATE_MISSING_STATE = "errUpdMissState";
    String ERR_UPDATE_INVALID_STATE = "errUpdInvState";
    String ERR_UPDATE_MISSING_POSTAL_CODE = "errUpdMissPostalCode";
    String ERR_UPDATE_INVALID_POSTAL_CODE = "errUpdInvPostalCode";
    String ERR_UPDATE_MISSING_CONFIRM = "info_confirm_password";
    String ERR_UPDATE_INVALID_CONFIRM = "err_confirm_password";
    String ERR_UPDATE_MISSING_OLD_PSWD = "info_enter_old_password";
    String ERR_UPDATE_INVALID_OLD_PSWD = "err_old_password";
    String ERR_UPDATE_MISSING_APPROVER = "errUpdMissApprover";
    String ERR_UPDATE_MISSING_PARENT_ORG = "errUpdMissParentOrg";
    String ERR_UPDATE_INVALID_PARENT_ORG = "errInvalidParentOrg";

    // Forgot Password Errors
    String ERR_FORGOT_PASS_MISSING_EMAIL = "errForgPassMissEmail";
    String ERR_FORGOT_PASS_NO_USER = "errForgPassNoUser";
    String ERR_FORGOT_PASS_MISSING_SECURITY_QUESTION = "errForgPassMissSecQues";
    String ERR_FORGOT_PASS_MISSING_SECURITY_ANSWER = "errForgPassMissSecAns";
    String ERR_FORGOT_PASS_INVALID_COMBINATION = "errForgPassInvCombination";
    String ERR_FORGOT_PASS_ALREADY_CHANGED = "errForgPasswordAlreadyChanged";
    String ERR_FORGOT_PASS_LINK_EXPIRED = "errForgPasswordLinkExpired";
    String ERR_FORGOT_PASS_USER_ALREADY_LOGGED_IN = "errForgPasswordUserAlreadyLoggedIn";
    String INFO_ENTER_EMAIL = "err_email"; // CPS-198

    // Credit App Errors
    String ERR_CREDIT_APP_GENERAL_ERROR = "errCreditAppGenErr";
    String ERR_CREDIT_APP_MISSING_YOUR_NAME = "errCreditAppMissYourName";
    String ERR_CREDIT_APP_INVALID_YOUR_NAME = "errCreditAppInvYourName";
    String ERR_CREDIT_APP_MISSING_BUS_NAME = "errCreditAppMissBusName";
    String ERR_CREDIT_APP_INVALID_BUS_NAME = "errCreditAppInvBusName";
    String ERR_CREDIT_APP_MISSING_TRADE_NAME = "errCreditAppMissTradeName";
    String ERR_CREDIT_APP_INVALID_TRADE_NAME = "errCreditAppInvTradeName";
    String ERR_CREDIT_APP_MISSING_ORG_TYPE = "errCreditAppMissOrgType";
    String ERR_CREDIT_APP_MISSING_BUS_PHONE = "errCreditAppMissBusPhone";
    String ERR_CREDIT_APP_INVALID_BUS_PHONE = "errCreditAppInvBusPhone";
    String ERR_CREDIT_APP_INVALID_BUS_PHONE_EXT = "errCreditAppInvBusPhoneExt";
    String ERR_CREDIT_APP_INVALID_BUS_FAX = "errCreditAppInvBusFax";

    String ERR_CREDIT_APP_MISSING_BUS_TYPE = "errCreditAppMissBusType";
    String ERR_CREDIT_APP_INVALID_BUS_TYPE = "errCreditAppInvBusType";
    String ERR_CREDIT_APP_INVALID_CON_LICENSE_NUM = "errCreditAppInvConLicenseNum";
    String ERR_CREDIT_APP_MISSING_YEAR_EST = "errCreditAppMissYearEst";
    String ERR_CREDIT_APP_INVALID_YEAR_EST = "errCreditAppInvYearEst";
    String ERR_CREDIT_APP_MISSING_CUSTOMER_TYPE = "errCreditAppMissCusType";
    String ERR_CREDIT_APP_MISSING_PO_REQUIRED = "errCreditAppMissPO";
    String ERR_CREDIT_APP_MISSING_TAX_EXEMPT = "errCreditAppMissTaxExempt";

    String ERR_CREDIT_APP_MISSING_BUS_ADDR_1 = "errCreditAppMissBusAddr1";
    String ERR_CREDIT_APP_INVALID_BUS_ADDR_1 = "errCreditAppInvBusAddr1";
    String ERR_CREDIT_APP_INVALID_BUS_ADDR_2 = "errCreditAppInvBusAddr2";
    String ERR_CREDIT_APP_MISSING_BUS_CITY = "errCreditAppMissBusCity";
    String ERR_CREDIT_APP_INVALID_BUS_CITY = "errCreditAppInvBusCity";
    String ERR_CREDIT_APP_MISSING_BUS_STATE = "errCreditAppMissBusState";
    String ERR_CREDIT_APP_MISSING_BUS_ZIP = "errCreditAppMissBusZip";
    String ERR_CREDIT_APP_INVALID_BUS_ZIP = "errCreditAppInvBusZip";

    String ERR_CREDIT_APP_MISSING_BIL_ADDR_1 = "errCreditAppMissBilAddr1";
    String ERR_CREDIT_APP_INVALID_BIL_ADDR_1 = "errCreditAppInvBilAddr1";
    String ERR_CREDIT_APP_INVALID_BIL_ADDR_2 = "errCreditAppInvBilAddr2";
    String ERR_CREDIT_APP_MISSING_BIL_CITY = "errCreditAppMissBilCity";
    String ERR_CREDIT_APP_INVALID_BIL_CITY = "errCreditAppInvBilCity";
    String ERR_CREDIT_APP_MISSING_BIL_STATE = "errCreditAppMissBilState";
    String ERR_CREDIT_APP_MISSING_BIL_ZIP = "errCreditAppMissBilZip";
    String ERR_CREDIT_APP_INVALID_BIL_ZIP = "errCreditAppInvBilZip";

    String ERR_CREDIT_APP_MISSING_PRIN1_NAME = "errCreditAppMissPrin1Name";
    String ERR_CREDIT_APP_INVALID_PRIN1_NAME = "errCreditAppInvPrin1Name";
    String ERR_CREDIT_APP_MISSING_PRIN1_TITLE = "errCreditAppMissPrin1Title";
    String ERR_CREDIT_APP_INVALID_PRIN1_TITLE = "errCreditAppInvPrin1Title";
    String ERR_CREDIT_APP_MISSING_PRIN1_ADDR = "errCreditAppMissPrin1Addr1";
    String ERR_CREDIT_APP_INVALID_PRIN1_ADDR = "errCreditAppInvPrin1Addr1";
    String ERR_CREDIT_APP_MISSING_PRIN1_CITY = "errCreditAppMissPrin1City";
    String ERR_CREDIT_APP_INVALID_PRIN1_CITY = "errCreditAppInvPrin1City";
    String ERR_CREDIT_APP_MISSING_PRIN1_STATE = "errCreditAppMissPrin1State";
    String ERR_CREDIT_APP_MISSING_PRIN1_ZIP = "errCreditAppMissPrin1Zip";
    String ERR_CREDIT_APP_INVALID_PRIN1_ZIP = "errCreditAppInvPrin1Zip";
    String ERR_CREDIT_APP_MISSING_PRIN1_PHONE = "errCreditAppMissPrin1Phone";
    String ERR_CREDIT_APP_INVALID_PRIN1_PHONE = "errCreditAppInvPrin1Phone";
    String ERR_CREDIT_APP_INVALID_PRIN1_PHONE_EXT = "errCreditAppInvPrin1PhoneExt";

    String ERR_CREDIT_APP_INVALID_PRIN2_NAME = "errCreditAppInvPrin2Name";
    String ERR_CREDIT_APP_INVALID_PRIN2_TITLE = "errCreditAppMissPrin2Title";
    String ERR_CREDIT_APP_INVALID_PRIN2_ADDR = "errCreditAppInvPrin2Addr1";
    String ERR_CREDIT_APP_INVALID_PRIN2_CITY = "errCreditAppInvPrin2City";
    String ERR_CREDIT_APP_INVALID_PRIN2_ZIP = "errCreditAppInvPrin2Zip";
    String ERR_CREDIT_APP_INVALID_PRIN2_PHONE = "errCreditAppInvPrin2Phone";
    String ERR_CREDIT_APP_INVALID_PRIN2_PHONE_EXT = "errCreditAppInvPrin2PhoneExt";

    String ERR_CREDIT_APP_MISSING_REF1_NAME = "errCreditAppMissRef1Name";
    String ERR_CREDIT_APP_INVALID_REF1_NAME = "errCreditAppInvRef1Name";
    String ERR_CREDIT_APP_MISSING_REF1_ADDR = "errCreditAppMissRef1Addr1";
    String ERR_CREDIT_APP_INVALID_REF1_ADDR = "errCreditAppInvRef1Addr1";
    String ERR_CREDIT_APP_MISSING_REF1_CITY = "errCreditAppMissRef1City";
    String ERR_CREDIT_APP_INVALID_REF1_CITY = "errCreditAppInvRef1City";
    String ERR_CREDIT_APP_MISSING_REF1_STATE = "errCreditAppMissRef1State";
    String ERR_CREDIT_APP_MISSING_REF1_ZIP = "errCreditAppMissRef1Zip";
    String ERR_CREDIT_APP_INVALID_REF1_ZIP = "errCreditAppInvRef1Zip";
    String ERR_CREDIT_APP_MISSING_REF1_PHONE = "errCreditAppMissRef1Phone";
    String ERR_CREDIT_APP_INVALID_REF1_PHONE = "errCreditAppInvRef1Phone";
    String ERR_CREDIT_APP_INVALID_REF1_PHONE_EXT = "errCreditAppInvRef1PhoneExt";
    String ERR_CREDIT_APP_INVALID_REF1_FAX = "errCreditAppMissRef1Fax";

    String ERR_CREDIT_APP_MISSING_REF2_NAME = "errCreditAppMissRef2Name";
    String ERR_CREDIT_APP_INVALID_REF2_NAME = "errCreditAppInvRef2Name";
    String ERR_CREDIT_APP_MISSING_REF2_ADDR = "errCreditAppMissRef2Addr1";
    String ERR_CREDIT_APP_INVALID_REF2_ADDR = "errCreditAppInvRef2Addr1";
    String ERR_CREDIT_APP_MISSING_REF2_CITY = "errCreditAppMissRef2City";
    String ERR_CREDIT_APP_INVALID_REF2_CITY = "errCreditAppInvRef2City";
    String ERR_CREDIT_APP_MISSING_REF2_STATE = "errCreditAppMissRef2State";
    String ERR_CREDIT_APP_MISSING_REF2_ZIP = "errCreditAppMissRef2Zip";
    String ERR_CREDIT_APP_INVALID_REF2_ZIP = "errCreditAppInvRef2Zip";
    String ERR_CREDIT_APP_MISSING_REF2_PHONE = "errCreditAppMissRef2Phone";
    String ERR_CREDIT_APP_INVALID_REF2_PHONE = "errCreditAppInvRef2Phone";
    String ERR_CREDIT_APP_INVALID_REF2_PHONE_EXT = "errCreditAppInvRef2PhoneExt";
    String ERR_CREDIT_APP_INVALID_REF2_FAX = "errCreditAppMissRef2Fax";

    String ERR_CREDIT_APP_MISSING_BANK_NAME = "errCreditAppMissBankName";
    String ERR_CREDIT_APP_INVALID_BANK_NAME = "errCreditAppInvBankName";
    String ERR_CREDIT_APP_MISSING_BANK_ADDR = "errCreditAppMissBankAddr1";
    String ERR_CREDIT_APP_INVALID_BANK_ADDR = "errCreditAppInvBankAddr1";
    String ERR_CREDIT_APP_MISSING_BANK_CITY = "errCreditAppMissBankCity";
    String ERR_CREDIT_APP_INVALID_BANK_CITY = "errCreditAppInvBankCity";
    String ERR_CREDIT_APP_MISSING_BANK_STATE = "errCreditAppMissBankState";
    String ERR_CREDIT_APP_MISSING_BANK_ZIP = "errCreditAppMissBankZip";
    String ERR_CREDIT_APP_INVALID_BANK_ZIP = "errCreditAppInvBankZip";
    String ERR_CREDIT_APP_MISSING_BANK_PHONE = "errCreditAppMissBankPhone";
    String ERR_CREDIT_APP_INVALID_BANK_PHONE = "errCreditAppInvBankPhone";
    String ERR_CREDIT_APP_INVALID_BANK_PHONE_EXT = "errCreditAppInvBankPhoneExt";
    String ERR_CREDIT_APP_MISSING_BANK_ACCOUNT_NUM = "errCreditAppMissBankAccNum";
    String ERR_CREDIT_APP_INVALID_BANK_ACCOUNT_NUM = "errCreditAppInvBankAccNum";
    String ERR_CREDIT_APP_MISSING_BANK_ACCOUNT_OFF = "errCreditAppMissBankAccOff";
    String ERR_CREDIT_APP_INVALID_BANK_ACCOUNT_OFF = "errCreditAppInvBankAccOff";

    String ERR_CREDIT_APP_INVALID_EXP_PURCH_AMT = "errCreditAppInvExpPurchAmt";

    String ERR_CREDIT_APP_INVALID_ORDERER1_NAME = "errCreditAppInvOrderer1Name";
    String ERR_CREDIT_APP_INVALID_ORDERER1_TITLE = "errCreditAppInvOrderer1Title";
    String ERR_CREDIT_APP_INVALID_ORDERER1_PHONE = "errCreditAppInvOrderer1Phone";
    String ERR_CREDIT_APP_INVALID_ORDERER1_PHONE_EXT = "errCreditAppInvOrderer1PhoneExt";
    String ERR_CREDIT_APP_INVALID_ORDERER2_NAME = "errCreditAppInvOrderer2Name";
    String ERR_CREDIT_APP_INVALID_ORDERER2_TITLE = "errCreditAppInvOrderer2Title";
    String ERR_CREDIT_APP_INVALID_ORDERER2_PHONE = "errCreditAppInvOrderer2Phone";
    String ERR_CREDIT_APP_INVALID_ORDERER2_PHONE_EXT = "errCreditAppInvOrderer2PhoneExt";
    String ERR_CREDIT_APP_INVALID_ORDERER3_NAME = "errCreditAppInvOrderer3Name";
    String ERR_CREDIT_APP_INVALID_ORDERER3_TITLE = "errCreditAppInvOrderer3Title";
    String ERR_CREDIT_APP_INVALID_ORDERER3_PHONE = "errCreditAppInvOrderer3Phone";
    String ERR_CREDIT_APP_INVALID_ORDERER3_PHONE_EXT = "errCreditAppInvOrderer3PhoneExt";
    String ERR_CREDIT_APP_INVALID_ORDERER4_NAME = "errCreditAppInvOrderer4Name";
    String ERR_CREDIT_APP_INVALID_ORDERER4_TITLE = "errCreditAppInvOrderer4Title";
    String ERR_CREDIT_APP_INVALID_ORDERER4_PHONE = "errCreditAppInvOrderer4Phone";
    String ERR_CREDIT_APP_INVALID_ORDERER4_PHONE_EXT = "errCreditAppInvOrderer4PhoneExt";

    // Reorder List Errors
    String ERR_REORDER_DELETE = "errReorderDelete";
    String ERR_REORDER_MISSING_NAME = "errReorderMissName";
    String ERR_REORDER_LIST_EXISTS = "errReorderListExists";
    String ERR_REORDER_MISSING_GIFTLIST = "errReorderMissGiftlist";
    String ERR_REORDER_MISSING_EMAIL = "errReorderMissEmail";
    String ERR_REORDER_INVALID_EMAIL = "errReorderInvEmail";
    String ERR_REORDER_NO_LISTS = "errReorderNoLists";
    String ERR_REORDER_NO_ITEMS = "errReorderNoItems";
    String ERR_REORDER_IMPORT_FILE = "errReorderImportMissFile";
    String ERR_REORDER_MISSING_ORG = "errReorderMissOrg";

    //	Material list errors
    String ERR_NEW_MAT_LIST_MISSING= "err_new_mat_list_missing";
    String ERR_MAT_NO_ACCESS= "err_mat_no_access";

    // Order Management Errors
    String ERR_ORDERS_MISSING_ORG = "errOrdersMissOrg";

    // Invoice Management Errors
    String ERR_INVOICES_MISSING_ORG = "errInvoiceMissOrg";
    String ERR_BILLING_ADDR_MISSING = "errMissBilState";

    // CSR Errors
    String ERR_CSR_MISSING_SEARCH = "errCSRMissSearch";
    String ERR_CSR_NO_RESULTS = "errCSRNoResults";

    //Cart PA Errors
    String ERR_PA_CHECK_ITEMS ="errPaCheckItems";
    String ERR_PA_NO_QTY = "errPaNoQty";
    String ERR_PA_NO_ITEM_REMOVE = "errPaNoItemRemove";
    String ERR_PA_NO_ITEM = "errPaNoItem";
    String ERR_PA_ADD_ITEM = "errPaAddItem";
    String ERR_CART_NO_QTY = "errCartNoQty";
    String ERR_CART_ADD_ITEM = "errCartAddItem";
    String ERR_CART_LOGIN_NEEDED = "errCartLoginNeeded";
    String ERR_CART_ITEMS_REMOVE = "errCartItemsRemove";
    String ERR_PA_INVALID_SKU ="errPaInvalidSku";
    String ERR_ITEM_INVENTORY = "errorItemInventoryValidation";
    String WARNING_ITEM_REMOVED = "warningItemRemoved";
    String WARNING_ITEM_UPDATED = "warningItemUpdated";

    //QuickOrder Errors
    String ERR_Quick_Order_Mult_Item_Validation ="errQuickOrderMultItemValidation";
    String ERR_Quick_Item_Validation ="errQuickOrderItemValidation";
    String ERR_PRODUCT_DOES_NOT_EXIST = "errNoProductExists";
    String ERR_NO_PRODUCTS = "errNoProducts";
    String ERR_ITEMS_NOT_FOUND = "err_item_not_found";
    String ERR_PRODUCT_NOT_PURCHASABLE = "errProductNotPurchasable";
    String ERR_QUICK_ORDER_GENERAL = "errQuickOrderGeneral";

    // Checkout Errors
    String ERR_CHECKOUT_PICKUP_BRANCH_EMPTY = "errCheckoutBranchEmpty";
    String ERR_CHECKOUT_PICKUP_DATE_EMPTY = "errCheckoutPickupDateEmpty";
    String ERR_CHECKOUT_PICKUP_TIME_EMPTY = "errCheckoutPickupTimeEmpty";
    String ERR_CHECKOUT_CARRIER_EMPTY = "errCheckoutCarrierEmpty";
    String ERR_CHECKOUT_ACCOUNT_NUMBER_EMPTY = "errCheckoutAccountNumberEmpty";
    String ERR_CHECKOUT_UNKNOWN_DELIVERY_TYPE = "errCheckoutUnknownDeliveryType";
    String ERR_CHECKOUT_CREDIT_CARD_TYPE_EMPTY = "errCheckoutCCTypeEmpty";
    String ERR_CHECKOUT_CREDIT_CARD_FIRST_NAME_EMPTY = "errCheckoutCCFirstNameEmpty";
    String ERR_CHECKOUT_CREDIT_CARD_LAST_NAME_EMPTY = "errCheckoutCCLastNameEmpty";
    String ERR_CHECKOUT_CREDIT_CARD_NUMBER_EMPTY = "errCheckoutCCNumberEmpty";
    String ERR_CHECKOUT_CREDIT_CARD_EXP_MONTH_EMPTY = "errCheckoutCCMonthEmpty";
    String ERR_CHECKOUT_CREDIT_CARD_EXP_YEAR_EMPTY = "errCheckoutCCYearEmpty";
    String ERR_CHECKOUT_CREDIT_CARD_ZIPCODE_EMPTY = "errCheckoutCCZipcodeEmpty";
    String ERR_CHECKOUT_PO_REFERENCE_EMPTY = "errCheckoutPOEmpty";
    String ERR_CHECKOUT_UNKNOWN_PAYMENT_TYPE = "errCheckoutUnknownPaymentType";
    String ERR_CHECKOUT_NONE_AVAILABLE = "errCheckoutNoneAvailable";
    String ERR_CHECKOUT_WILLCALL_ISSUE = "errCheckoutWillCallIssue";
    String ERR_CHECKOUT_EMPTY_SHIPPING_ADDRESS ="errEmptyShippingAddress";
    String ERR_CHECKOUT_EMPTY_PICKUP_LOCATION_ADDRESS ="errEmptyPickupLocationAddress";
    String ERR_CHECKOUT_EMPTY_BILLING_ADDRESS ="errEmptyBillingAddress";
    String ERR_CHECKOUT_INVALID_DELIVERY_METHOD ="errInvalidDeliveryMethod";
    String ERR_CHECKOUT_EMPTY_PO_NUMBER ="errEmptyPoNumber";
    String ERR_CHECKOUT_TOO_LONG_PO_NUMBER ="errTooLongPoNumber";
    String ERR_CHECKOUT_EMPTY_ITEMS_MAP ="errEmptyItemsMap";
    String ERR_CHECKOUT_EMPTY_ORDER ="errEmptyOrder";
    String ERR_CHECKOUT_ERROR_MODIFYING_ORDER ="errModifyingOrder";

    String CC_EXPIRED = "errCheckoutCCExpired";
    String CC_INVALID_CHARS = "errCheckoutCCInvalidChars";
    String CC_INVALID_TYPE_NUM_MISMATCH = "errCheckoutCCInvalidTypeNumMisMatch";
    String CC_INVALID_TYPE = "errCheckoutCCInvalidType";
    String CC_NUMBER_SHORT = "errCheckoutCCNumberShort";
    String CC_NUMBER_WRONG = "errCheckoutCCNumberWrong";
    String CC_INFO_WRONG = "errCheckoutCCInfoWrong";
    String CC_EXP_DATE_WRONG = "errCheckoutCCExpDateWrong";
    String ERR_CREDIT_HOLD = "err_credit_hold";
    String ERR_INVALID_ONSITE_NAME = "errInvalidOnsiteName";
    String ERR_INVALID_ONSITE_PHONE = "errInvalidOnsitePhone";
    String ERR_INVALID_JOB_NAME = "errInvalidJobName";

    //contact us form
    String ERR_CONTACT_INVALID_SUBJECT = "errContactUsSubject";
    String ERR_CONTACT_INVALID_COMMENTS = "errContactUsComments";
    String INFO_ENTER_NAME = "err_name"; // CPS-200
    String INFO_COMP_NAME = "err_company_name"; // CPS-200
    String INFO_ACC_NUM = "err_account"; // CPS-200
    String INFO_SUBJECT = "err_subject"; // CPS-200
    String INFO_PHONE_NUM = "err_phone"; // CPS-200
    String INFO_METHOD = "err_method"; // CPS-200
    String INFO_COMMENTS = "err_comments"; // CPS-200
    String INVALID_EMAIL = "invalid_email"; // CPS_200
    String INVALID_PHONE = "invalid_phone"; // CPS-200
    String INVALID_COMMENTS = "invalid_comments"; // CPS-200

    // material list form
    String ML_EMPTY_NAME = "errMaterialList_EmptyName";
    String ML_WRONG_NAME = "errMaterialList_Name";
    String ML_WRONG_DESCRIPTION = "errMaterialList_Description";
    String ML_WRONG_GIFTLIST = "err_no_ML";
    String ML_NAME_EXIST = "errMaterialListExist";
    String ML_ERROR_EDDING_TO_LIST = "errMaterialList_UnableToAdd";
    String ML_WRONG_QUANTITY = "Qty_Err_";
    String ML_WRONG_HEADER = "errMaterialListHeader";

    //Material Test Reports
    String ERR_MTR_NO_HEAT_NUMBER = "errMTRNoHeatNumber";
    String ERR_MTR_NO_RESULTS = "errMTRNoResults";

    String ERR_INCORRECT_FILE = "err_incorrect_file";
    String ERR_GET_RESULTS = "err_get_results";
    String ERR_REQUEST_QUOTE_FILE_FORMAT="errRequestQuoteFileFormat";
    String ERR_REQUEST_QUOTE_EMPTY_ROWS="errRequestQuoteEmptyRows";
    String ERR_REQUEST_QUOTE_INCORRECT_ROWS="errRequestQuoteIncorrectRows";
    String ERR_NO_FILE = "errNoFile";
    String ERR_REQUEST_QUOTE_EMPTY_FILE = "errRequestQuoteEmptyFile";
    String ERR_REQUEST_QUOTE_FILE_LENGTH = "errRequestQuoteFileLength";
    String ERR_REQUEST_QUOTE_FILE_SIZE = "errRequestQuoteFileSize";

    //Order Pad Messages
    String ERR_INVALID_FILE_FORMAT="invalid_file_format";
    String ERR_INVALID_FIELDS_FORMAT="invalid_fields_format";
    String ERR_BLANK_QUICK_ORDER_FORM="blank_quick_order_form";
    String ERR_QUICK_ORDER_QTY="err_qty"; // for incorrect quick order input
    String ERR_QUICK_ORDER_DUPE="err_dupe_not_selected"; // for duplicate modal checkbox not selected
    String ERR_MATERIAL_LIST_DUPE="err_not_sel";//Please Select One Item in each row to add to your Material List.
    String ERR_UPDATE_MISSING_ROLE = "errManageMissingRole";
    String ERR_UPDATE_INVALID_ROLE = "errManageInvalidRole";
    String ERR_UPDATE_SPEND_LIMIT = "errManageInvSpendingLimit";
    String ERR_UPDATE_FREQUENCY = "errManageMissingFrequency";
    String ERR_UPDATE_SPEND_LIMIT_CSR = "errManageSpendingGreater";

    // Share Page Functionality
    String ERR_SHARE_MISSING_RECIPIENT = "errShareMissingRecipient";
    String ERR_SHARE_INVALID_RECIPIENT = "errShareInvalidRecipient";
    String ERR_SHARE_MISSING_FIRST_NAME = "errShareMissingFirstName";
    String ERR_SHARE_INVALID_FIRST_NAME = "errShareInvalidFirstName";
    String ERR_SHARE_MISSING_LAST_NAME = "errShareMissingLastName";
    String ERR_SHARE_INVALID_LAST_NAME = "errShareInvalidLastName";
    String ERR_SHARE_MISSING_EMAIL = "errShareMissingEmail";
    String ERR_SHARE_INVALID_EMAIL = "errShareInvalidEmail";
    String ERR_SHARE_MISSING_MESSAGE = "errShareMissingMessage";
    String ERR_SHARE_INVALID_MESSAGE = "errShareInvalidMessage";
    String ERR_SHARE_INVALID_URL = "errShareInvalidUrl";

    String ERR_TESTIMONIALS_FIRST_NAME = "errTestimonialFirstName";
    String ERR_TESTIMONIALS_LAST_NAME = "errTestimonialLastName";
    String ERR_TESTIMONIALS_COMPANY = "errTestimonialCompany";
    String ERR_TESTIMONIALS_MESSAGE = "errTestimonialMessage";
    String ERR_TESTIMONIALS_EMAIL = "errTestimonialEmail"; 
    
    String ERR_PL_CONTACT_NAME = "errPriceListUpdatesName";
    String ERR_PL_EMAIL = "errPriceListUpdatesEmail";
    String ERR_PL_INVALID_EMAIL = "errPriceListUpdatesInvalidEmail";

    String ERR_TEMPORARY_EXPIRED = "errTemporaryPasswordExpired";
    String ERR_PSWD_CHANGED = "errTemporaryPasswordChanged";
    String ERR_TEMPORARY_INVALID_URL = "errTemporaryPasswordInvalidURL";
    String ERR_INVALID_TEMPORARY_PSWD = "errTemporaryPasswordInvalid";
    String ERR_TEMPORARY_USER_ALREADY_LOGGED_IN = "errTemporaryUserAlreadyLoggedIn";
    String ERR_LOGIN_RESET_PSWD = "errLoginResetPassword";

    String ERR_REG_MISSING_APPROVER = "errRegMissApprover";

    String ERR_UPDATE_MISSING_DEFAULT_SHIPPING = "errUpdMissDefaultShip";

    // Credit application form
    String ERR_CREDIT_APPLICATION = "errRequiredFieldsCreditApplication";

    // PDP / PLP add to cart
    String ERR_MIN_ORDER_QTY = "errMinOrderQty";
    String ERR_ORDER_QTY_INTERVAL = "errOrderQtyInterval";
    String ERR_QTY_ERROR = "err_qty_err";

    // Shipping Address on checkout
    String ERR_SHIPPING_MISSING_FIRST_NAME = "errShipMissFirstName";
    String ERR_SHIPPING_INVALID_FIRST_NAME = "errShipInvFirstName";
    String ERR_SHIPPING_MISSING_LAST_NAME = "errShipMissLastName";
    String ERR_SHIPPING_INVALID_LAST_NAME = "errShipInvLastName";
    String ERR_SHIPPING_MISSING_EMAIL = "errShipMissEmail";
    String ERR_SHIPPING_INVALID_EMAIL = "errShipInvEmail";
    String ERR_SHIPPING_MISSING_ADDRESS1 = "errShipMissAddress1";
    String ERR_SHIPPING_INVALID_ADDRESS1 = "errShipInvAddress1";
    String ERR_SHIPPING_MISSING_ADDRESS2 = "errShipMissAddress2";
    String ERR_SHIPPING_INVALID_ADDRESS2 = "errShipInvAddress2";
    String ERR_SHIPPING_MISSING_CITY = "errShipMissCity";
    String ERR_SHIPPING_INVALID_CITY = "errShipInvCity";
    String ERR_SHIPPING_MISSING_STATE = "errShipMissState";
    String ERR_SHIPPING_INVALID_STATE = "errShipInvState";
    String ERR_SHIPPING_MISSING_ZIP_CODE = "errShipMissPostalCode";
    String ERR_SHIPPING_INVALID_ZIP_CODE = "errShipInvPostalCode";
    String ERR_SHIPPING_MISSING_COMPANY = "errShipMissCompany";
    String ERR_SHIPPING_INVALID_COMPANY = "errShipInvCompany";
    String ERR_SHIPPING_EMAIL_EXISTS = "errShipEmailExists";
    
    //PDP Error form    
    String PDP_ERR_MISSING_FIRST_NAME = "pdpErrorMissingFirstName";    
    String PDP_ERR_INVALID_FIRST_NAME = "pdpErrorInvalidFirstName";
    String PDP_ERR_MISSING_EMAIL = "pdpErrorMissingEmail";	
    String PDP_ERR_INVALID_EMAIL = "pdpErrorInvalidEmail";
    
    String ERR_UPLOADING_FILE = "errBulkUserUploadedFile";
    String ERR_INVALID_DATA_IN_TEMPLATE = "err_invalid_data_in_template";
    
    String ERR_HOUSE_ORGANIZATION = "errHouseOrganization";
    
    String ERR_REMOVE_APPROVER_ROLE = "errRemoveApproverRole";
    
    //Sales order report
    String ERR_SALES_REPORT_SELECT_OPTION = "errSalesOrderReportSelectOption";

}
