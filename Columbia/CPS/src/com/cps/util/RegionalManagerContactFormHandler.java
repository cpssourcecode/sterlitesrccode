package com.cps.util;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;

import atg.repository.RepositoryItem;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import atg.userprofiling.Profile;

import com.cps.content.service.ContentPagesService;

import com.cps.email.CommonEmailSender;

import com.cps.util.validator.RegionalManagerContactValidator;

import com.cps.userprofiling.CPSProfileTools;

import java.util.HashMap;
import java.util.Map;

import vsg.util.ajax.AjaxUtils;

public class RegionalManagerContactFormHandler extends GenericFormHandler {
	private Profile profile;
	private CPSProfileTools profileTools;
	private CommonEmailSender commonEmailSender;
	private ContentPagesService contentPagesService;
	private RegionalManagerContactValidator regionalManagerContactValidator;

	// Form fields
	private String regionalManager;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String message;

	/**
	 * request monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public void preRegionalManagerContactUs(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("preRegionalManagerContactUs - start");

		getRegionalManagerContactValidator().validateSenderInformation(getFirstName(), getLastName(), getEmailAddress(), this, request);
		getRegionalManagerContactValidator().validateMessageLength(getMessage(), this, request);

		vlogDebug("preRegionalManagerContactUs - end");
	}

	public boolean handleRegionalManagerContactUs(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("handleRegionalManagerContactUs - start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "RegionalManagerContactFormHandler.handleRegionalManagerContactUs";
		boolean sendResult = false;

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (null == getMessage()) {
					setMessage(request.getParameter("/cps/util/RegionalManagerContactFormHandler.message"));
					vlogDebug("handleRegionalManagerContactUs - message null, had to grab value <" + getMessage() + "> from request");
				}

				vlogDebug("  firstName=" + getFirstName() + "; lastName=" + getLastName() + "; email=" + getEmailAddress() + "; mgr=" + getRegionalManager());
				vlogDebug("  message=" + getMessage());

				preRegionalManagerContactUs(request, response);

				if (!getFormError()) {
					vlogDebug("Looking up associated regional manager to send to...");
					RepositoryItem regionalManager = null;
					if (getContentPagesService() != null) {
						regionalManager = getContentPagesService().getRegionalManager(getRegionalManager());
					}

					if (regionalManager != null) {
						vlogDebug("Regional Manager Contact Us calling e-mail sender...");
						sendResult = getCommonEmailSender().sendCPSRegionalManagerContactUsEmail(regionalManager, getFirstName(), getLastName(), getEmailAddress(), getMessage());
					} else {
						logError("RegionalManagerContactFormHandler - no regional manager found with ID " + getRegionalManager());
					}
				}

				postRegionalManagerContactUs(request, response);

				vlogDebug("handleRegionalManagerContactUs - end, send result=" + sendResult);
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		if (AjaxUtils.isAjaxRequest(request)) {
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("sent", sendResult);
			AjaxUtils.addAjaxResponse(this, response, result);
			return false;
		}
		else {
			return (!getFormError());
		}
	}

	public void postRegionalManagerContactUs(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("postRegionalManagerContactUs - start");

		vlogDebug("postRegionalManagerContactUs - end");
	}

	/*********************************************************/

	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	public RegionalManagerContactValidator getRegionalManagerContactValidator() {
		return regionalManagerContactValidator;
	}

	public void setRegionalManagerContactValidator(RegionalManagerContactValidator regionalManagerContactValidator) {
		this.regionalManagerContactValidator = regionalManagerContactValidator;
	}

	public ContentPagesService getContentPagesService() {
		return contentPagesService;
	}

	public void setContentPagesService(ContentPagesService contentPagesService) {
		this.contentPagesService = contentPagesService;
	}

	// Form fields
	public void setRegionalManager(String regionalManager) {
		this.regionalManager = regionalManager;
	}

	public String getRegionalManager() {
		return this.regionalManager;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

}