package com.cps.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.cps.email.CPSEmailConstants;
import com.cps.email.CommonEmailSender;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.util.ajax.AjaxUtils;

/**
 * 
 * @author Manikandan
 *
 */
public class PDPFoundAnErrorFormHandler extends GenericFormHandler {

	private RepeatingRequestMonitor mRepeatingRequestMonitor;
	private CommonEmailSender commonEmailSender;

	private String emailAddress;
	private String name;
	private String typeOfError;
	private String additionalInfo;

	private String displayName;
	private String brandName;
	private String vendorItem;
	private String pricesvcUPC;
	private String productDescription;
	private Map<String, String> typesOfError = new HashMap<String, String>();

	public boolean handleSendPDPError(DynamoHttpServletRequest request, DynamoHttpServletResponse response)
			throws IOException {

		vlogDebug("handleSendPDPError.start");
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String handleMethodName = "CPSPdpPageErrorFormHandler.handleSendPDPError";
		boolean sendResult = false;
		if ((rrm == null) || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				vlogDebug("Product Details : {0} : {1} : {2} : {3} : {4} : {5}", getDisplayName(), getBrandName(), getVendorItem(),
						getPricesvcUPC(), getProductDescription(), getTypesOfError());
				preSendPDPError(request, response);
				StringBuilder errorTypes = new StringBuilder();
				for (Entry<String, String> errorType : getTypesOfError().entrySet()) {					
					if(errorType.getValue() != CPSConstants.FALSE) {
						errorTypes.append(errorType.getValue()).append(", ");
					}
				}
				if(errorTypes.length()>0){
					typeOfError = errorTypes.substring(0,errorTypes.length()-2);
				}				
				if (!getFormError()) {
					HashMap<String, Object> emailParams = new HashMap<String, Object>();
					
					emailParams.put(CPSEmailConstants.PARAM_USER_FIRST_NAME, getName().trim());
					emailParams.put(CPSEmailConstants.PARAM_USER_EMAIL, getEmailAddress().trim());
					emailParams.put(CPSEmailConstants.PARAM_ERROR_TYPE, getTypeOfError());
					emailParams.put(CPSEmailConstants.PARAM_USER_MESSAGE, getAdditionalInfo());
					emailParams.put(CPSEmailConstants.PARAM_DISPLAY_NAME, getDisplayName());
					emailParams.put(CPSEmailConstants.PARAM_BRAND_NAME, getBrandName());
					emailParams.put(CPSEmailConstants.PARAM_VENDOR_ITEM, getVendorItem());
					emailParams.put(CPSEmailConstants.PARAM_UPC, getPricesvcUPC());
					emailParams.put(CPSEmailConstants.PARAM_PRODUCT_DESCRIPTION, getProductDescription());
					emailParams.put(CPSEmailConstants.EMAIL_TYPE, CPSEmailConstants.PDP_ERROR_EMAIL);
					vlogDebug("email params : {0}", emailParams);
					sendResult = getCommonEmailSender().sendPDPErrorEmail(emailParams);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		if (AjaxUtils.isAjaxRequest(request)) {
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("sent", sendResult);
			AjaxUtils.addAjaxResponse(this, response, result);
			return false;
		} else {
			return (!getFormError());
		}
	}

	public void preSendPDPError(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		
		if (!StringUtils.isBlank(getName()) && !getName().matches(CPSConstants.REG_EXP_NAME)) {

			vlogDebug("First name < {0} > does not match regular expression", getName());
			CPSMessageUtils.addFormException(this, CPSErrorCodes.PDP_ERR_INVALID_FIRST_NAME, request.getLocale(),
					CPSConstants.PDP_ERR_FIRST_NAME);

		}
		if (!StringUtils.isBlank(getEmailAddress()) && !getEmailAddress().matches(CPSConstants.REG_EXP_EMAIL)) {
			CPSMessageUtils.addFormException(this, CPSErrorCodes.PDP_ERR_INVALID_EMAIL, request.getLocale(),
					CPSConstants.PDP_ERR_EMAIL_ADDRESS);
		}
	}

	/**
	 * @return the mRepeatingRequestMonitor
	 */
	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * @param mRepeatingRequestMonitor
	 *            the mRepeatingRequestMonitor to set
	 */
	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		this.mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the typeOfError
	 */
	public String getTypeOfError() {
		return typeOfError;
	}

	/**
	 * @param typeOfError
	 *            the typeOfError to set
	 */
	public void setTypeOfError(String typeOfError) {
		this.typeOfError = typeOfError;
	}

	/**
	 * @return the additionalInfo
	 */
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * @param additionalInfo
	 *            the additionalInfo to set
	 */
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	/**
	 * @return the commonEmailSender
	 */
	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	/**
	 * @param commonEmailSender
	 *            the commonEmailSender to set
	 */
	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName
	 *            the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the brandName
	 */
	public String getBrandName() {
		return brandName;
	}

	/**
	 * @param brandName
	 *            the brandName to set
	 */
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	/**
	 * @return the vendorItem
	 */
	public String getVendorItem() {
		return vendorItem;
	}

	/**
	 * @param vendorItem
	 *            the vendorItem to set
	 */
	public void setVendorItem(String vendorItem) {
		this.vendorItem = vendorItem;
	}

	/**
	 * @return the pricesvcUPC
	 */
	public String getPricesvcUPC() {
		return pricesvcUPC;
	}

	/**
	 * @param pricesvcUPC
	 *            the pricesvcUPC to set
	 */
	public void setPricesvcUPC(String pricesvcUPC) {
		this.pricesvcUPC = pricesvcUPC;
	}

	/**
	 * @return the productDescription
	 */
	public String getProductDescription() {
		return productDescription;
	}

	/**
	 * @param productDescription
	 *            the productDescription to set
	 */
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	/**
	 * @return the typesOfError
	 */
	public Map<String, String> getTypesOfError() {
		return typesOfError;
	}

	/**
	 * @param typesOfError the typesOfError to set
	 */
	public void setTypesOfError(Map<String, String> typesOfError) {
		this.typesOfError = typesOfError;
	}	

}
