package com.cps.util;

public interface CPSConstants {

    String CPS_LOGIN_PREFIX = "cps_";
    String TRUE = "true";
    String FALSE = "false";

    String ERR_MESSAGES_REPOSITORY = "/cps/registry/repository/MessagesRepository";
    String SHOPPING_CART_PATH = "/atg/commerce/ShoppingCart";
    String SESSION_BEAN_PATH = "/cps/util/CPSSessionBean";

    String USER_ITEM_DESCRIPTOR = "user";
    String B2B_ITEM_DESCRIPTOR = "b2b-user";

    // Regular Expressions
    String REG_EXP_ALPHA = "^[a-zA-Z- ]+$";
    String REG_EXP_NUMERIC = "^[0-9]+$";
    String REG_EXP_ZIPCODE = "^[0-9]{5}(?:-[0-9]{4})?$";
    String REG_EXP_ALPHA_NUMERIC = "^[A-Za-z0-9 _]*$";
    String REG_EXP_EMAIL = "^[A-Za-z0-9._%+-]+@(?:[A-Za-z0-9-]+\\.)+[A-Za-z]{2,6}$";
    String REG_EXP_PSWD = "((?=.*\\d)(?=.*[a-zA-Z])(?=.*[A-Z])(?=.*[\\_\\@\\#\\$\\%\\\\/\\.\\,\\<\\>\\!\\^\\&\\*\\(\\)\\|\\~]).{6,15})";
    String REG_EXP_PHONE = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
    String REG_EXP_NAME = "^[a-zA-Z][a-zA-Z-'\\.\\s]*$";
    String REG_EXP_SPECIAL_CHAR = "^[a-zA-Z0-9@,:;\\.'_\\(\\)\\/#\\&\\s\\-]+$";
    String FILTER_SPEC_SIMBOLS = "[^\\w\\s]";
    String COMMA=",";

    // Login/Registration Profile
    String ID = "id";
    String USER_TYPE = "userType";
    int USER_TYPE_B2B_SALES_REP = 3;
    int USER_TYPE_B2B_USER = 2;
    String NAME = "name";
    String FIRST_NAME = "firstName";
    String LAST_NAME = "lastName";
    String FIRST = "First";
    String LAST = "Last";
    String EMAIL = "email";
    String VALID = "valid";
    String EMAIL_STATUS = "emailStatus";
    String CURRENT_EMAIL = "currentEmail";
    String LOGIN = "login";
    String CONFIRM_EMAIL = "confirmEmail";
    String PSWD = "password";
    String GENERATED_PSWD = "generatedPassword";
    String FORGOT_PSWD = "forgotPassword";
    String ELOQUA_ID = "eloquaId";
    String EXPORTED_WITH_ELOQUA_ID = "exportedWithEloquaId";
    String CONFIRM_PSWD = "confirmPassword";
    String SECURITY_QUESTION = "securityQuestion";
    String SECURITY_ANSWER = "securityQuestionAnswer";
    String ASSOCIATED_CS = "associatedCS";
    String ASSOCIATED_CS_DATA = "associatedCSData";
    String FORGOT_PSWD_STEP = "step";
    String OLD_PSWD = "oldPassword";
    String NEW_PSWD = "newPassword";
    String CUSTOMER_NUMBER = "customerNumber";
    String PARENT_CATEGORY = "parentCategory";
    String PARENT_PRODUCTS = "parentProducts";
    String AUTO_LOGIN = "autoLogin";
    String FIRST_LOGIN = "firstLogin";
    String KDF = "passwordKeyDerivationFunction";
    String PROMO_EMAIL_SIGN_UP = "promoEmailSignUp";
    String DEACTIVATED_DATE = "deactivatedDate";
    String SUPERVISOR_NAME = "supervisorName";
    String SUPERVISOR_EMAIL = "supervisorEmail";

    String COMPANY = "company";
    String COMPANY_NAME = "companyName";
    String TITLE = "title";
    String JOB_TITLE = "jobTitle";
    String ADDRESS1 = "address1";
    String ADDRESS2 = "address2";
    String ADDRESS3 = "address3";
    String CITY = "city";
    String CITY_SELECT = "Select...";
    String STATE = "state";
    String STATE_ADDRESS = "stateAddress";
    String POSTAL_CODE = "postalCode";
    String ZIP = "zip";
    String ZIP_CODE = "zipCode";
    String COUNTRY = "country";
    String PRIMARY_PHONE = "primaryPhone";
    String PRIMARY_PHONE_EXT = "primaryPhoneExt";
    String ALTERNATE_PHONE = "alternatePhone";
    String ALTERNATE_PHONE_EXT = "alternatePhoneExt";
    String PHONE = "phone";
    String PHONE_NUMBER = "phoneNumber";
    String PHONE_NUMBER_EXT = "phoneNumberExt";
    String ALTERNATE_PHONE_NUMBER = "alternatePhoneNumber";
    String ALTERNATE_PHONE_NUMBER_EXT = "alternatePhoneNumberExt";
    String FAX = "fax";
    String FAX_NUMBER = "faxNumber";
    String EXT = "Ext.";
    String HOME_ADDRESS = "homeAddress";
    String EMAIL_ADDRESS = "emailAddress";
    String ADDRESS_NUMBER = "addressNumber";

    String REMEMBER_ME = "rememberMe";
    String IS_REMEMBER = "isRemember";
    String USER_EMAIL_NAME_COOKIE_NAME = "CPS_EMAIL_NAME";
    String CAPTCHA = "captcha";
    String SESSION_LOCKED = "timeLocked";
    String LOGIN_COOKIE_NAME = "LOGIN_COOKIE";

    String REDIRECT = "redirect";
    String LANDING = "landing";
    String PROFILE = "profile";
    String ADDRESSES = "addresses";
    String REORDER = "reorder";
    String ORDERS = "orders";
    String APPROVALS = "approvals";
    String CART = "cart";

    String SELECT_ONE = "Select One...";

    String BILLING_ADDRESS = "derivedBillingAddress";
    String CONTACT_INFO = "contactInfo";
    String CREDIT_APPROVAL_REQUIRED = "creditApprovalRequired";
    String PAY_INST_RYIN = "payInstRYIN";
    String CC_ACCOUNT_ACCEPTED = "isCCAccountAccepted";
    String SHOW_CC_NOTIFICATION = "showCCNotification";

    String CREDIT_APP_FLAG = "creditAppFlag";
    String PARENT_ORG = "parentOrganization";
    String SECONDARY_ADDRESSES = "secondaryAddresses";
    String SELECTED_CS = "selectedCS";
    String CART_SELECTED_CS = "cartSelectedCS";
    String CART_SELECTED_STORE = "cartSelectedStore";
    String CART_SELECTED_DELIVERY_METHOD = "cartSelectedDeliveryMethod";
    String CHILD_ORGS = "childOrganizations";
    String BILLING_ACCOUNTS = "billingAccounts";

    String BUSINESS_UNIT = "businessUnit";
    String SELECTED_ORG = "selectedOrg";
    String USER_ROLES = "userRoles";
    String ORG_LIST = "orgList";
    String CSR_USER_ID = "CSRUserId";
    String JDE_ACCOUNT_NUM = "jdeAccountNumber";
    String ON_HOLD = "isOnHold";

    String CONTACT_INFO_JDE_ADDRESS_NUMBER = "jdeAddressNumber";
    String ORGANIZATION = "organization";
    String ORGANIZATION_MEMBERS_PRTY = "members";
    String IS_ACTIVE = "isActive";
    String CREATE = "create";
    String UPDATE = "update";
    String USER = "user";
    String B2B_SALE_REP_NAME = "b2b-sales-rep";
    String REP_CODE_NAME = "outsideRepCode";
    String ADDR_TYPE_NAME = "addressType";
    String SEGMENT_NAME = "segmentName";
    String SEGMENT_CODE = "segmentCode";
    String ONE = "1";
    String TWO = "2";
    String THREE = "3";
    String FOUR = "4";
    String FIVE = "5";

    // Roles
    String ROLE_PRPTY = "role";
    String ROLES_PRPTY = "roles";
    String ROLE_SITE_ADMIN = "regularAdmin";
    String ROLE_CSR_ADMIN = "superAdmin";
    String ROLE_SALES_REP = "salesRep";
    String ROLE_FINANCE = "finance";
    String ROLE_FINANCE_LIMITED = "financeLight";
    String ROLE_ACCOUNT_ADMIN = "custAccAdmin";
    String ROLE_APPROVER = "approver";
    String ROLE_BUYER = "buyer";
    String ROLE_ADMIN = "admin";
    String ROLE_APPRAISER = "appraiser";

    int ORG_TYPE_GUEST = 6;

    String ROLE_ACCOUNT_ADMIN_DISPLAY = "Account Admin";
    String ROLE_FINANCE_DISPLAY = "Finance";
    String ROLE_APPROVER_DISPLAY = "Approver";
    String ROLE_BUYER_DISPLAY = "Buyer";
    String ROLE_FINANCE_FULL_DISPLAY = "Finance";
    String ROLE_FINANCE_LIMITED_DISPLAY = "Finance - Limited";
    String ROLE_APPRAISER_DISPLAY = "Appraiser";

    // Credit Application
    String CREDIT_APPLICATION_VIEW_NAME = "appCredit";
    String ADDRESS_VIEW_NAME = "appCreditAddress";
    String BANK_REFERENCE_VIEW_NAME = "appCreditBankReference";
    String TAX_EXEMPTION_CERTIFICATE_VIEW_NAME = "appCreditTax";
    String RESALE_CERTIFICATE_VIEW_NAME = "appCreditResale";
    String PRINCIPAL_VIEW_NAME = "appCreditPrincipal";
    String TRADE_REFERENCE_VIEW_NAME = "appCreditTradeReference";
    String CUSTOMER_VIEW_NAME = "appCreditCustomer";

    String ORGANIZATION_ID = "organizationId";
    String LEGAL_BUSINESS_NAME = "legalBusinessName";
    String TYPE_ORGANIZATION = "organizationType";
    String BUSINESS_YEAR_ESTABLISHED = "businessYearEstablished";
    String BUSINESS_LICENCE_NUMBER = "businessLicenseNumber";
    String REQUIRED_PO = "requiredPO";
    String TAX_EXEMPT = "taxExempt";
    String TAX_NUMBER = "taxNumber";
    String ADDRESS_BUSINESS = "businessAddress";
    String ADDRESS_BILLING = "billingAddress";
    String BANK_REFERENCE = "bankReference";
    String EXPECTED_ANNUAL_PURCHASES = "expectedAnnualPurchases";
    String ORIGINAL_CREDIT_REQUEST = "originatedCreditRequest";
    String PERSONAL_GUARANTY = "personalGuaranty";
    String PERSONAL_GUARANTY_DEBTOR = "personalGuarantyDebtor";
    String CUSTOMER = "customer";
    String TAX_EXEMPTION_CERTIFICATE = "taxExemptionCertificate";
    String RESALE_CERTIFICATE = "resaleCertificate";
    String PRODUCT_TYPE_LIST = "productTypeList";
    String REGISTERED_STATES = "registeredStates";
    String PURCHASE_TYPE = "purchaseType";
    String TAX_EXEMPT_STATUS= "taxExemptStatus";
    String FIRM_NAME = "firmName";
    String REGISTERED_TYPE = "registeredType";
    String SPECIFY_REGISTERED_TYPE = "specifyRegisteredType";
    String BUYER_ACTIVITY_TYPE = "buyerActivityType";
    String SPECIFY_BUYER_ACTIVITY_TYPE = "specifyBuyerActivityType";
    String BUSINESS_FOLLOWING = "businessFollowing";
    String GENERAL_DESCRIPTION_PRODUCTS = "generalDescriptionProducts";
    String SELLER_NAME = "sellerName";
    String SELLER_ADDRESS = "sellerAddress";
    String PURCHASER_NAME = "purchaserName";
    String PURCHASER_ADDRESS = "purchaserAddress";
    String PURCHASER_REGISTERED_TYPE = "purchaserRegisteredType";
    String REGISTRATION_NUMBER = "registrationNumber";
    String DESCRIBE_PROPERTY_RESALE = "describePropertyResale";
    String PURCHASES_RESALE_TYPE = "purchasesResaleType";
    String PERCENTAGE_PURCHASES_RESALE = "percentagePurchasesResale";
    String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
    

    String PRINCIPALS = "principals";
    String REFERENCES = "references";
    String CUSTOMERS = "customers";

    String ADDRESS_STREET = "street";
    String ADDRESS_CITY = "city";
    String ADDRESS_STATE = "state";
    String ADDRESS_ZIP_CODE = "zipCode";

    String YOUR_NAME = "yourName";
    String BUSINESS_NAME = "businessName";
    String TRADE_NAME = "tradeName";
    String ORGANIZATION_TYPE = "orgType";
    String BUSINESS_PHONE = "busPhone";
    String BUSINESS_PHONE_EXT = "busPhoneExt";
    String BUSINESS_FAX = "busFax";
    String BUSINESS_TYPE = "busType";
    String CONTRACTOR_LICENSE_NUMBER = "contractorLicenseNumber";
    String YEAR_ESTABLISHED = "yearEstablished";
    String PO = "po";
    String TAX = "tax";

    String CUSTOMER_TYPE = "customerType";
    String TYPE_CONTRACTOR = "typeContractor";
    String TYPE_MECHANICAL = "typeMechanical";
    String TYPE_PLUMBING = "typePlumbing";
    String TYPE_INDUSTRIAL = "typeIndustrial";
    String TYPE_INSTITUTIONAL = "typeInstitutional";
    String TYPE_WHOLESALER = "typeWholesaler";
    String TYPE_OTHER = "typeOther";

    String BUSINESS_ADDRESS1 = "busAddress1";
    String BUSINESS_ADDRESS2 = "busAddress2";
    String BUSINESS_CITY = "busCity";
    String BUSINESS_STATE = "busState";
    String BUSINESS_ZIP = "busZip";
    String BILLING_ADDRESS1 = "bilAddress1";
    String BILLING_ADDRESS2 = "bilAddress2";
    String BILLING_CITY = "bilCity";
    String BILLING_STATE = "bilState";
    String BILLING_ZIP = "bilZip";

    String PRINCIPAL1_NAME = "prin1Name";
    String PRINCIPAL1_TITLE = "prin1Title";
    String PRINCIPAL1_ADDRESS = "prin1Address";
    String PRINCIPAL1_CITY = "prin1City";
    String PRINCIPAL1_STATE = "prin1State";
    String PRINCIPAL1_ZIP = "prin1Zip";
    String PRINCIPAL1_PHONE = "prin1Phone";
    String PRINCIPAL1_PHONE_EXT = "prin1PhoneExt";

    String PRINCIPAL2_NAME = "prin2Name";
    String PRINCIPAL2_TITLE = "prin2Title";
    String PRINCIPAL2_ADDRESS = "prin2Address";
    String PRINCIPAL2_CITY = "prin2City";
    String PRINCIPAL2_STATE = "prin2State";
    String PRINCIPAL2_ZIP = "prin2Zip";
    String PRINCIPAL2_PHONE = "prin2Phone";
    String PRINCIPAL2_PHONE_EXT = "prin2PhoneExt";

    String REFERENCE1_NAME = "ref1Name";
    String REFERENCE1_ADDRESS = "ref1Address";
    String REFERENCE1_CITY = "ref1City";
    String REFERENCE1_STATE = "ref1State";
    String REFERENCE1_ZIP = "ref1Zip";
    String REFERENCE1_PHONE = "ref1Phone";
    String REFERENCE1_PHONE_EXT = "ref1PhoneExt";
    String REFERENCE1_FAX = "ref1Fax";

    String REFERENCE2_NAME = "ref2Name";
    String REFERENCE2_ADDRESS = "ref2Address";
    String REFERENCE2_CITY = "ref2City";
    String REFERENCE2_STATE = "ref2State";
    String REFERENCE2_ZIP = "ref2Zip";
    String REFERENCE2_PHONE = "ref2Phone";
    String REFERENCE2_PHONE_EXT = "ref2PhoneExt";
    String REFERENCE2_FAX = "ref2Fax";

    String BANK_NAME = "bankName";
    String BANK_ADDRESS = "bankAddress";
    String BANK_CITY = "bankCity";
    String BANK_STATE = "bankState";
    String BANK_ZIP = "bankZip";
    String BANK_PHONE = "bankPhone";
    String BANK_PHONE_EXT = "bankPhoneExt";
    String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
    String BANK_ACCOUNT_OFFICER = "bankAccountOfficer";

    String PRODUCT_LIST_PVC = "productListPVC";
    String PRODUCT_LIST_PLUMBING = "productListPlumbing";
    String PRODUCT_LIST_HANGARS = "productListHangars";
    String PRODUCT_LIST_AUTOMATED = "productListAutomated";
    String PRODUCT_LIST_HVAC = "productListHVAC";
    String EXPECTED_ANNUAL_PURCHASE_AMOUNT = "expectedAnnualPurchAmt";

    String ORDER1_NAME = "order1Name";
    String ORDER1_TITLE = "order1Title";
    String ORDER1_PHONE = "order1Phone";
    String ORDER1_PHONE_EXT = "order1PhoneExt";

    String ORDER2_NAME = "order2Name";
    String ORDER2_TITLE = "order2Title";
    String ORDER2_PHONE = "order2Phone";
    String ORDER2_PHONE_EXT = "order2PhoneExt";

    String ORDER3_NAME = "order3Name";
    String ORDER3_TITLE = "order3Title";
    String ORDER3_PHONE = "order3Phone";
    String ORDER3_PHONE_EXT = "order3PhoneExt";

    String ORDER4_NAME = "order4Name";
    String ORDER4_TITLE = "order4Title";
    String ORDER4_PHONE = "order4Phone";
    String ORDER4_PHONE_EXT = "order4PhoneExt";

    String USER_ID = "userId";
    String USER_NAME = "userName";
    String BUSINESS_FAX_PROPERTY = "businessFax";
    String BUSINESS_NAME_PROPERTY = "businessName";
    String BUSINESS_PHONE_PROPERTY = "businessPhone";
    String BUSINESS_PHONE_EXT_PROPERTY = "businessPhoneExt";
    String BUSINESS_TYPE_PROPERTY = "businessType";
    String CONTRACTOR_LICENSE_NUMBER_PROPERTY = "contractorsLicenseNum";
    String EXPECTED_ANNUAL_PURCHASE_AMOUNT_PROPERTY = "expAnnPurchAmt";
    String ACCOUNT_NUMBER = "accountNumber";
    String ACCOUNT_OFFICER = "accountOfficer";
    String PO_PROPERTY = "poRequired";
    String TAX_PROPERTY = "taxExempt";
    String CREDIT_APP_CONTACT_PROPERTY = "creditAppAddresses";
    String CREDIT_APP_ORDERER_PROPERTY = "creditAppPlaceOrders";
    String CREDIT_APP_PRODUCT_LIST_PROPERTY = "productList";

    String CREDIT_APP_BUSINESS_ADDRESS = "creditAppBusinessAddress";
    String CREDIT_APP_BILLING_ADDRESS = "creditAppBillingAddress";
    String CREDIT_APP_PRINCIPAL_1 = "creditAppPrincipal1Address";
    String CREDIT_APP_PRINCIPAL_2 = "creditAppPrincipal2Address";
    String CREDIT_APP_REFERENCE_1 = "creditAppReference1Address";
    String CREDIT_APP_REFERENCE_2 = "creditAppReference2Address";
    String CREDIT_APP_BANK_ADDRESS = "creditAppBankAddress";

    // Reorder lists
    String EVENT_NAME = "eventName";
    String EVENT_DATE = "eventDate";
    String CREATION_DATE = "creationDate";
    String CS = "cs";
    String ORG = "org";
    String DESCRIPTION = "description";
    String DESCRIPTION_LINE2 = "descriptionLine2";
    String ALIAS_NUMBER = "aliasNumber";
    String GIFTLISTS = "giftlists";
    String GIFT_LIST_ITEM_TYPE = "gift-list";
    String GIFTLIST_ITEMS = "giftlistItems";
    String DISPLAY_NAME = "displayName";
    String PRODUCT_ID = "productId";
    String CATALOG_REF_ID = "catalogRefId";
    String QUANTITY = "Quantity";
    String QUANTITY_DESIRED = "quantityDesired";
    String QUANTITY_PURCHASED = "quantityPurchased";
    String UPC = "pricesvc_upc";
    String MFG_NAME = "mfr_fullname";
    String MFG_PART_NUMBER = "part_number";
    String UNIT_PRICE = "Unit Price";
    String EXTENDED_AMOUNT = "Unit Total";
    public static final String QUOTE_ACKNOWLEDGEMENT = "QUOTE_ACKNOWLEDGEMENT";
    String FILE_EXT_CSV = ".csv";
    String FILE_EXT_XLS = ".xls";
    String FILE_EXT_XLSX = ".xlsx";

    // Checkout fields
    String CHECKOUT_DELIVERY_METHOD = "deliveryMethod";
    String CHECKOUT_PICKUP_BRANCH = "branch";
    String CHECKOUT_PICKUP_DATE = "pickupDate";
    String CHECKOUT_PICKUP_TIME = "pickupTime";
    String CHECKOUT_DISPLAY_PICKUP_TIME = "pickUpTime";
    String CHECKOUT_COLLECT_CARRIER = "carrier";
    String CHECKOUT_COLLECT_ACCOUNT_NUMBER = "carrierAcctNumber";
    String CHECKOUT_MTR = "mtrRequested";
    String CHECKOUT_PAYMENT_TYPE = "paymentType";
    String CHECKOUT_PO_REFERENCE = "poReferenceNumber";
    String CHECKOUT_CREDIT_CARD_TYPE = "creditCardType";
    String CHECKOUT_CREDIT_CARD_FIRST_NAME = "firstName";
    String CHECKOUT_CREDIT_CARD_LAST_NAME = "lastName";
    String CHECKOUT_CREDIT_CARD_NUMBER = "creditCardNumber";
    String CHECKOUT_CREDIT_CARD_EXP_MONTH = "creditCardExpMonth";
    String CHECKOUT_CREDIT_CARD_EXP_YEAR = "creditCardExpYear";
    String CHECKOUT_CREDIT_CARD_ZIPCODE = "creditCardBillingZipcode";
    String CHECKOUT_JDE_ADDRESS_NUMBER = "jdeAddressNumber";
    String CHECKOUT_ONSITE_CONTACT = "onsiteContact";
    String CHECKOUT_JOB_NAME = "jobName";
    String CHECKOUT_CONCATENATED_PO_JOB_NAME = "concatenatedPoJobName";

    // Checkout values
    String CHECKOUT_PAYMENT_PO = "checkout-payment-po";
    String CHECKOUT_PAYMENT_CC = "checkout-payment-cc";
    String CHECKOUT_DELIVERY_METHOD_STANDARD_GROUND = "checkout-delivery-ground";
    String CHECKOUT_DELIVERY_METHOD_PICKUP = "checkout-delivery-pickup";
    String CHECKOUT_DELIVERY_METHOD_COLLECT = "checkout-delivery-collect";
    String CHECKOUT_METHOD_STANDARD_GROUND = "Ground Delivery";
    String CHECKOUT_METHOD_PICKUP = "Pickup";
    String CHECKOUT_METHOD_COLLECT = "Collect Delivery";
    String CHECKOUT_ON_ACCOUNT = "On Account";

    // Checkout values
    String QUICKORDER_SKU = "itemNumP";

    // Checkout payment group types
    String PAYMENT_TYPE_CC = "creditCard";
    String PAYMENT_TYPE_INVOICE = "invoiceRequest";
    String SHIPPPING_TYPE_HARDGOOD = "hardgoodShippingGroup";
    String SHIPPPING_TYPE_IN_STORE_PICKUP = "inStorePickupShippingGroup";

    // Order History
    String ORDER_SEARCH_TERM = "orderSearchTerm";
    String ORDER_SEARCH_TIME = "orderSearchTime";
    String ORDER_SEARCH_CS = "orderSearchCS";
    String ORDER_SEARCH_BY_PO = "orderSearchByPO";
    String ORDER_SEARCH_FIELD = "orderSearchFieldName";
    String ORDER_SEARCH_PAGE_SIZE = "orderSearchPageSize";
    String ORDER_SEARCH_START_INDEX = "orderSearchStartIndex";
    String ORDER_SEARCH_SORT_FIELD = "orderSearchSortField";
    String ORDER_SEARCH_SORT_OPTION = "orderSearchSortOption";
    String ORDER_SEARCH_PO = "poNumber";
    String ORDER_SEARCH_ORDER_ID = "orderId";
    String ORDER_SEARCH_SALES_ID = "salesOrderId";

    // Invoice History
    String INVOICE_SEARCH_CS = "invoiceSearchCS";
    String INVOICE_SEARCH_ORDER_NUMBER = "invoiceSearchOrderNumber";
    String INVOICE_SEARCH_BY_PO = "invoiceSearchByPO";
    String INVOICE_SEARCH_VALUE = "invoiceSearchValue";
    String INVOICE_SEARCH_PAGE_SIZE = "invoiceSearchPageSize";
    String INVOICE_SEARCH_START_INDEX = "invoiceSearchStartIndex";
    String INVOICE_SEARCH_TIME = "invoiceSearchTime";
    String INVOICE_SEARCH_SORT_FIELD = "invoiceSearchSortField";
    String INVOICE_SEARCH_SORT_OPTION = "invoiceSearchSortOption";
    String PAID = "paid";
    String OPEN = "open";
    String STATUS = "status";
    String DATE = "date";
    String TOTAL = "total";
    String INVOICE_NUMBER = "invoiceNumber";
    String ORDER_NUMBER = "orderNumber";
    String PO_NUMBER = "poNumber";
    String PO_NUMBER_PROPERTY = "PONumber";

    // Packing Slips
    String PACKING_SEARCH_CS = "packingSearchCS";
    String PACKING_SEARCH_TERM = "packingSearchTerm";
    String PACKING_SEARCH_OPTION = "packingSearchOption";
    String PACKING_SEARCH_TIME = "packingSearchTime";
    String PACKING_SEARCH_BY_PO = "packingSearchByPO";
    String PACKING_SEARCH_IS_SEARCH_BUTTON = "isSearchButton";
    String PACKING_SEARCH_PAGE_SIZE = "packingSearchPageSize";
    String PACKING_SEARCH_START_INDEX = "packingSearchStartIndex";
    String PACKING_SEARCH_SORT_FIELD = "packingSearchSortField";
    String PACKING_SEARCH_SORT_OPTION = "packingSearchSortOption";

    // order properties
    String COUPON_CODES = "couponCodes";
    String ALLOW_CHECKOUT = "allowCheckout";
    String ORDER_TOTAL = "orderTotal";
    String USE_PROFILE_ORGANIZATION = "useProfileOrganization";
    String PAYMENT_UPDATED = "paymentUpdated";
    String SUBMITTED_DATE ="submittedDate";
    String PROFILE_EMAIL = "profileEmail";
    String LAST_MODIFIED_DATE = "lastModifiedDate";

    // contact us form
    String SUBJECT = "subject";
    String COMMENTS = "comments";
    String ACCOUNT = "account";
    String PREFERRED_METHOD = "preferred_method"; // CPS-200

    String SHIPPING_GROUP_COMMERCE_ITEM_CL_TYPE = "shippingGroupCommerceItem";
    String PAYMENT_GROUP_ORDER_CL_TYPE = "paymentGroupOrder";

    // Material Test Reports
    String HEAT_NUMBER = "heatNumber";

    String PRODUCT_ALIAS_MAP = "prodAliasMap";
    
    String PRODUCT_PURCHASABLE = "productPurchasable";

    String RECOVER_PSWD_DATE_FORMAT = "MM/dd/yyyy HH:mm";

    // Category
    String ECOMMERCE_DISPLAY = "eCommerceDisplay";
    String CATEGORY_FACETS = "categoryFacets";
    String ASSOCIATED_PRODUCTS = "associatedProducts";
    String FIXED_PARENT_CATEGORIES = "fixedParentCategories";

    // Product
    String PRODUCT_PARENT_CATEGORIES = "parentCategories";
    String FIXED_RELATED_PRODUCTS = "fixedRelatedProducts";
    String USE_AS_ASSOCIATED_PRODUCT = "useAsAssociatedProduct";
    String MINIMUM_ORDER_QTY = "minimum_order_qty";
    String ORDER_QTY_INTERVAL = "order_qty_interval";

    String PRODUCT_REP_ITEM_STOCKING_TYPE = "stockingType";
    String PRODUCT_REP_ITEM_STOCKING_TYPE_O = "O";
    String PRODUCT_REP_ITEM_STOCKING_TYPE_U = "U";
    String PRODUCT_REP_ITEM_STOCKING_TYPE_K = "K";
    String PRODUCT_REP_ITEM_STOCKING_TYPE_X = "X";
    String PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR = "4";


    // GiftlistItem
    String ECOMMERCE_UNAVAILABLE = "eCommerceUnavailable";

    // Manage Pages Constants
    String SPENDING_LIMIT = "spendLimit";
    String DERIVED_ORDER_LIMIT = "derivedOrderPriceLimit";
    String ORDER_LIMIT = "orderPriceLimit";
    String ORDER_LIMIT_FREQUENCY = "spFrequency";
    String CSR_ORDER_LIMIT = "cpsAdminSpendingLimit";

    String ORDER_LIMIT_FREQUENCY_ORDER = "ORDER";
    String ORDER_LIMIT_FREQUENCY_DAY = "DAY";
    String ORDER_LIMIT_FREQUENCY_WEEK = "WEEK";
    String ORDER_LIMIT_FREQUENCY_MONTH = "MONTH";

    String ROLE_NAME_PRPTY = "name";

    String REGION = "region";

    // Commerce Items
    String INVENTORY_QTY = "inventoryQuantity";
    String AVAILABLE_QTY = "availableQuantity";
    String DEFAULT_BRANCH_QTY = "defaultBranchQty";
    String LEAD_TIME = "leadTime";
    String STOCKING_TYPE = "stockingType";
    String STOCKING_TYPE_OBSOLETE = "O";
    String STOCKING_TYPE_USEUP = "U";
    String STOCKING_TYPE_K = "K";
    String STOCKING_TYPE_X = "X";
    String SCHEDULE_DAYS = "scheduleDays";
    String IS_SCHEDULED_ITEM = "isScheduledItem";

    String APPROVERS = "approvers";
    String PRODUCT_ITEM_DESCRIPTOR = "product";
    String PRODUCT_ITEM_DESCRIPTOR_ALIAS_MAP = "prodAliasMap";
    String PRODUCT_ITEM_DESCRIPTOR_KEY_ORGS = "keyOrgs";

    String SHIPPING_ADDRESS = "shippingAddress";
    String DERIVED_SHIPPING_ADDRESS = "derivedShippingAddress";

    String IS_PRICED = "isPriced";
    String IS_AVAILABILITY_CHECKED = "isAvailabilityChecked";
    int RESET_AVAILABLE_QTY = -1;

    String JDE_ADDRESS_NUM = "jdeAddressNumber";

    String CSR_SELECTED_ORG = "CSRSelectedOrg";
    String CURRENT_ORG = "currentOrganization";
    String CSR_ACCESS_MESSAGE = "csrDisplayAccessWarning";

    // Share Page
    String SHARE_FIRST_NAME = "shareModalFirstName";
    String SHARE_LAST_NAME = "shareModalLastName";
    String SHARE_EMAIL_ADDRESS = "shareModalEmailAddress";
    String SHARE_RECIPIENT = "shareModalRecipients";
    String SHARE_MESSAGE = "shareModalMessage";

    // Services Contact Us
    String SERVICES_CONTACT_FIRST_NAME = "services-contact-firstName";
    String SERVICES_CONTACT_LAST_NAME = "services-contact-lastName";
    String SERVICES_CONTACT_EMAIL_ADDRESS = "services-contact-emailAddress";
    String SERVICES_CONTACT_MESSAGE = "services-contact-message";

    // Regional Managers Contact Us
    String REGIONAL_MANAGER_CONTACT_FIRST_NAME = "rm-contact-firstName";
    String REGIONAL_MANAGER_CONTACT_LAST_NAME = "rm-contact-lastName";
    String REGIONAL_MANAGER_CONTACT_EMAIL_ADDRESS = "rm-contact-emailAddress";
    String REGIONAL_MANAGER_CONTACT_MESSAGE = "rm-contact-message";

    // Checkout
    String DELEVERY_METHOD_SHIPPED = "shipped";
    String DELEVERY_METHOD_PICK_UP = "pick-up";
    String STORE_ITEM_TYPE = "store";
    String STORE_ITEM_ID = "storeId";
    String ADDRESS = "address";
    String ADDRESS_ID = "addressId";
    String WEB_ORDER_ID = "webOrderId";
    String AUTO_ORDER_ID = "autoOrderId";
    String REQUEST_NUMBER = "requestNumber";
    public static final String SC_SECURE_TOKEN="scSecureToken";
    public static final String SECURE_TOKEN="secure_token"; 
    public static final String ORDER_NOTIFICATION_EMAILS = "orderNotificationEmails";
    public static final String JDE_SUCCESS = "jdeSuccess";


    String ORDER = "order";
    String PAGE = "page";
    String REVIEW = "review";
    String PICKUP = "pickup"; // modal page
    public static final String CB="cb";

    // Location / Store
    String LOCATION = "location";
    String STORE_BRANCH_ID_PRTY = "branchId";

    String ITEM_DESCRIPTOR_ORDER = "order";

    String ISPU_LOCATION_ID_PRTY = "locationId";
    String LOCATION_DEFAULT_BRANCH_ID = "100";

    String REFINEMENTS = "refinements";

    String SIMPLE_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss:SSS";
    String LAST_PSWD_UPDATE = "lastPasswordUpdate";
    String TEMPORARY_PSWD = "temporaryPassword";

    String RELATED_MEDIA_CONTENT = "relatedMediaContent";
    String URL = "url";

    String ACTIVE = "active";

    String DIM_PRODUCT_CATEGORY = "product.category";
    String DIM_PRODUCT_MANUFACTURER = "product.mfr_fullname";

    // Request a quote
    String IMPORT_FIELD_ID = "import";

    String BRAND_NAME = "brand_name";
    String VENDOR_NAME = "vendorName";
    String VENDOR_ITEM = "vendorItem";
    String MFR_FULL_NAME = "mfr_fullname";

    String SELECTED_BILLTO = "selectedBillTo";

    String WISHLIST_ID_PARAM = "wd";
    String PROFILE_ID_PARAM = "pd";

    String SITE_ACCESS_COUNT = "accessCountBeforeModalShowed";
    String SITE_ACCESS_COUNT_INTERVAL = "accessCountInterval";

    String GUEST_LAST_ACCESS_COOKIE_NAME = "guestLastAccess";
    String GUEST_ACCESS_COUNT_COOKIE_NAME = "guestAccessCount";

    String AUTO_ORDER_VIEW_NAME = "autoOrder";
    String AUTO_ORDER_ITEM_VIEW_NAME = "autoOrderItem";
    String AUTO_ORDER_PAYMENT_INFO_VIEW_NAME = "autoOrderPaymentInfo";
    String AUTO_ORDER_PAYMENT_INFO_ON_ACCOUNT_VIEW_NAME = "autoOrderPaymentInfoOnAccount";
    String AUTO_ORDER_SHIPPING_INFO_VIEW_NAME = "autoOrderShippingInfo";
    String AUTO_ORDER_ADDRESS_VIEW_NAME = "autoOrderAddress";

    String DEFAULT_CURRENCY_CODE = "USD";

    String COMPATIBILITY_MODE_ON = "compatibilityModeOn";
    String USER_AGENT = "User-Agent";
    String BROWSER_IE = "IE";
    String COMPATIBLE = "compatible";

    String CUT_TO_ORDER_INFO_MESSAGE = "cutToOrderProducts";

    String SEAMLESS_PIPE_INFO_MESSAGE = "seamlessPipeProducts";

    String OBSOLETE_INFO_MESSAGE = "seamlessPipeProducts";

    /**
     * All query
     */
    String QUERY_ALL = "ALL";

    // site

    String ADDITIONAL_PRODUCTION_URLS = "additionalProductionURLs";
    String PRODUCTION_URL = "productionURL";
    String HTTP_PROTOCOL = "http://";
    String HTTPS_PROTOCOL = "https://";
    String SITE_ID = "siteId";
    String IS_ITEMS_FROM_MT = "itemsFromMaterialList";

    String APPROVAL_FIRST_NAME = "approverFirstName";
    String APPROVAL_LAST_NAME = "approverLastName";
    String APPROVAL_PHONE_NUMBER = "approverPhoneNumber";
    String APPROVAL_EMAIL = "approverEmail";
    String ROLES = "roles";

    // Address Overrides Constants

    String ADDRESS_OVERRIDES_Y_OR_N = "Address_Overrides_Y_or_N";
    String BILLTO_OVERRIDES_Y_OR_N = "BillTo_Overrides_Y_or_N";
    String PARENT_OVERRIDES_Y_OR_N = "Parent_Overrides_Y_or_N";
    String ABAC06_BASE_MATRIX = "ABAC06_Base_Matrix";

    String PDP_ERR_FIRST_NAME = "pdpErrorModalFirstName";
    String PDP_ERR_EMAIL_ADDRESS = "pdpErrorModalEmailAddress";

    String NOMINAL_SIZE_FILTER_NAME = "NOMINAL SIZE";
    String OUTSIDE_DIAMETER_FILTER_NAME = "OUTSIDE DIAMETER";
    String INSIDE_DIAMETER_FILTER_NAME = "INSIDE DIAMETER";
    String ROD_SIZE_FILTER_NAME = "ROD SIZE";
    String PIPE_SIZE_FILTER_NAME = "PIPE SIZE";
    String CENTER_DISTANCE_FILTER_NAME = "CENTER DISTANCE";
    String FLOW_RATE_FILTER_NAME = "FLOW RATE";
    String PRESSURE_CLASS_FILTER_NAME = "PRESSURE CLASS";
    String THICKNESS_FILTER_NAME = "THICKNESS";

    String ATTRIBUTE_VALUE_MAP = "attributeValueMap";
    String ATTRIBUTE_UOM_MAP = "attributeUomMap";
    
    String WORKGROUP_NUMBER = "workGroupId";
    String ENABLE_DOWNLOAD_LINK="enableDownloadLink";
    String DOWNLOAD_FILE_LINK = "downloadFileLink";
    String HOST_NAME = "hostName";
    
    //Testimonial Email
    String TESTIMONIAL_EMAIL = "testimonialEmail";
    
    String NOTIFICATION_ENABLED_USERS = "notificationEnabledUsers";
	public static final String PROFILE_ID = "profileId";
// Address fields
    public static final String OUTSIDE_REP_DESCRIPTION = "outsideRepDescription";
	public static final String OUTSIDE_REP = "outsideRep";
	public static final String REGIONAL_MANAGER_DESCRIPTION = "regionalManagerDescription";
	public static final String REGIONAL_MANAGER = "regionalManager";

	//sitemap
	public static final String SLASH="/"; 
	public static final String QUESTION_MARK="?";
	
	public static final String LAST_ACTIVITY ="lastActivity";
	public static final String REGISTRATION_DATE ="registrationDate";
	
	public static final String LAST_LOGOUT="lastLogout";
	public static final String ORDER_NOTIFICATION_EMAIL="OrderNotificationEmailSent";
	public static final String WORKGROUP_EMAILS="workGroupMails";
	public static final String FIRST_ITEM_TO_CURRENT_CART="firstItemAddedToCurrentCart";

	String ENABLED_ONE_DAY_REPORT = "enabledOneDayReport";
	//Credit Card info
	public static final String PROV_TRANS_ID = "provTransID";
	public static final String ACCT_REF_TOKEN = "acctRefToken";
	public static final String PNREF = "pnref";
	public static final String AUTH_CODE = "authCode";
	
	public static final String HANDLING_EXEMPT ="Cat_Hnd_Crd";
	public static final String SHIPPING_TAX_EXEMPT="TxExempt";
	public static final String TAX_EXEMPT_TYPE="E";
	public static final String ZERO_STRING="0";
	public static final String ZERO_DECIMAL_STRING="0.00";
	public static final String IS_ACTIVE_ORGANIZATION="isActiveOrganization";
}
