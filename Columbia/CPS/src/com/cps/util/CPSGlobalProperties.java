package com.cps.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;

import atg.commerce.order.OrderManager;
import atg.commerce.profile.CommerceProfileTools;
import atg.nucleus.GenericService;

/**
 * A base class containing common properties for usage across the application.
 *
 * @author Manikandan
 *
 */
public class CPSGlobalProperties extends GenericService {

    private boolean testMode;
    private boolean onHoldValueTestMode;
    private boolean ajaxEnabledForPricingServiceAvailabilityCheck;
    private boolean enableAutoSuggestion;
    private boolean enableCustomerAlsoBought;
    // configure the LRUMap size of the CPSSessionBean
    private int priceCacheSizePerUser;
    private boolean enableAnonymousUserToRequestAccess;
    private boolean showOrderShippingAddressForSession = true;
    private CommerceProfileTools profileTools;
    private OrderManager orderManager;
    private int maxEndecaSuggestions;
    private boolean lazyLoadEnabled;
    private int searchTermsMaxLength;
    private int displayableSearchTermsMaxLength;
    private FilePermissionsHelper filePermissionsHelper;
    private String searchDropdownShade;
    private int maxMLItemsPerPage;
    private int sessionTimeoutInMinutes;
    private int lazyLoadViewPortImages;
    private HashMap<string, String> dimensionSortMap;
    private String defaultDimensionSort;
    private boolean enableSizeCustomSorting;
    private Map<String, String> customSortingEnabledMap;
    private int maximumAttachedFiles;
    private int limitOfUploadedFileSize;
    private int welcomeModalExpiry;
    private boolean enableSupervisorEmailValidation;
    private boolean hideSavedCart;
    private boolean enableDefaultShipTo;
    private boolean invalidteCacheEnabled;
    private List<String> skippedItems;
    private Set<String> stockingTypesNotPurchasable;
    protected Map<String,List<String>> errorProducts = new HashMap<String,List<String>>();
    private String errorAddressLoaderFile;
    /**
     * @return the maxMLItemsPerPage
     */
    public int getMaxMLItemsPerPage() {
        return maxMLItemsPerPage;
    }

    /**
     * @param maxMLItemsPerPage
     *            the maxMLItemsPerPage to set
     */
    public void setMaxMLItemsPerPage(int maxMLItemsPerPage) {
        this.maxMLItemsPerPage = maxMLItemsPerPage;
    }

    /**
     * @return the filePermissionsHelper
     */
    public FilePermissionsHelper getFilePermissionsHelper() {
        return filePermissionsHelper;
    }

    /**
     * @param filePermissionsHelper
     *            the filePermissionsHelper to set
     */
    public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
        this.filePermissionsHelper = filePermissionsHelper;
    }

    public int getSearchTermsMaxLength() {
        return searchTermsMaxLength;
    }

    public void setSearchTermsMaxLength(int searchTermsMaxLength) {
        this.searchTermsMaxLength = searchTermsMaxLength;
    }

    public int getDisplayableSearchTermsMaxLength() {
        return displayableSearchTermsMaxLength;
    }

    public void setDisplayableSearchTermsMaxLength(int displayableSearchTermsMaxLength) {
        this.displayableSearchTermsMaxLength = displayableSearchTermsMaxLength;
    }

    public int getMaxEndecaSuggestions() {
        return maxEndecaSuggestions;
    }

    public void setMaxEndecaSuggestions(int maxEndecaSuggestions) {
        this.maxEndecaSuggestions = maxEndecaSuggestions;
    }

    /**
     * @return the priceCacheSizePerUser
     */
    public int getPriceCacheSizePerUser() {
        return priceCacheSizePerUser;
    }

    /**
     * @param priceCacheSizePerUser
     *            the priceCacheSizePerUser to set
     */
    public void setPriceCacheSizePerUser(int priceCacheSizePerUser) {
        this.priceCacheSizePerUser = priceCacheSizePerUser;
    }

    /**
     * @return the testMode
     */
    public boolean isTestMode() {
        return testMode;
    }

    /**
     * @param testMode
     *            the testMode to set
     */
    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    /**
     * @return the onHoldValueTestMode
     */
    public boolean isOnHoldValueTestMode() {
        return onHoldValueTestMode;
    }

    /**
     * @param onHoldValueTestMode
     *            the onHoldValueTestMode to set
     */
    public void setOnHoldValueTestMode(boolean onHoldValueTestMode) {
        this.onHoldValueTestMode = onHoldValueTestMode;
    }

    /**
     * @return the ajaxEnabledForPricingServiceAvailabilityCheck
     */
    public boolean isAjaxEnabledForPricingServiceAvailabilityCheck() {
        return ajaxEnabledForPricingServiceAvailabilityCheck;
    }

    /**
     * @param ajaxEnabledForPricingServiceAvailabilityCheck
     *            the ajaxEnabledForPricingServiceAvailabilityCheck to set
     */
    public void setAjaxEnabledForPricingServiceAvailabilityCheck(boolean ajaxEnabledForPricingServiceAvailabilityCheck) {
        this.ajaxEnabledForPricingServiceAvailabilityCheck = ajaxEnabledForPricingServiceAvailabilityCheck;
    }

    /**
     * @return the enableAutoSuggestion
     */
    public boolean isEnableAutoSuggestion() {
        return enableAutoSuggestion;
    }

    /**
     * @param enableAutoSuggestion
     *            the enableAutoSuggestion to set
     */
    public void setEnableAutoSuggestion(boolean enableAutoSuggestion) {
        this.enableAutoSuggestion = enableAutoSuggestion;
    }

    /**
     * @return the enableCustomerAlsoBought
     */
    public boolean isEnableCustomerAlsoBought() {
        return enableCustomerAlsoBought;
    }

    /**
     * @param enableCustomerAlsoBought
     *            the enableCustomerAlsoBought to set
     */
    public void setEnableCustomerAlsoBought(boolean enableCustomerAlsoBought) {
        this.enableCustomerAlsoBought = enableCustomerAlsoBought;
    }

    /**
     * @return the showOrderShippingAddressForSession
     */
    public boolean isShowOrderShippingAddressForSession() {
        return showOrderShippingAddressForSession;
    }

    /**
     * @param showOrderShippingAddressForSession
     *            the showOrderShippingAddressForSession to set
     */
    public void setShowOrderShippingAddressForSession(boolean showOrderShippingAddressForSession) {
        this.showOrderShippingAddressForSession = showOrderShippingAddressForSession;
    }

    /**
     * @return the orderManager
     */
    public OrderManager getOrderManager() {
        return orderManager;
    }

    /**
     * @param orderManager
     *            the orderManager to set
     */
    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    /**
     * @return the profileTools
     */
    public CommerceProfileTools getProfileTools() {
        return profileTools;
    }

    /**
     * @param profileTools
     *            the profileTools to set
     */
    public void setProfileTools(CommerceProfileTools profileTools) {
        this.profileTools = profileTools;
    }

    /**
     * @return the enableAnonymousUserToRequestAccess
     */
    public boolean isEnableAnonymousUserToRequestAccess() {
        return enableAnonymousUserToRequestAccess;
    }

    /**
     * @param enableAnonymousUserToRequestAccess
     *            the enableAnonymousUserToRequestAccess to set
     */
    public void setEnableAnonymousUserToRequestAccess(boolean enableAnonymousUserToRequestAccess) {
        this.enableAnonymousUserToRequestAccess = enableAnonymousUserToRequestAccess;
    }

    /**
     * @return the lazyLoadEnabled
     */
    public boolean isLazyLoadEnabled() {
        return lazyLoadEnabled;
    }

    /**
     * @param lazyLoadEnabled
     *            the lazyLoadEnabled to set
     */
    public void setLazyLoadEnabled(boolean lazyLoadEnabled) {
        this.lazyLoadEnabled = lazyLoadEnabled;
    }

    /**
     * @return the searchDropdownShade
     */
    public String getSearchDropdownShade() {
        return searchDropdownShade;
    }

    /**
     * @param searchDropdownShade
     *            the searchDropdownShade to set
     */
    public void setSearchDropdownShade(String searchDropdownShade) {
        this.searchDropdownShade = searchDropdownShade;
    }

    /**
     * @return the sessionTimeoutInMinutes
     */
    public int getSessionTimeoutInMinutes() {
        return sessionTimeoutInMinutes;
    }

    /**
     * @param sessionTimeoutInMinutes
     *            the sessionTimeoutInMinutes to set
     */
    public void setSessionTimeoutInMinutes(int sessionTimeoutInMinutes) {
        this.sessionTimeoutInMinutes = sessionTimeoutInMinutes;
    }

    /**
     * @return the lazyLoadViewPortImages
     */
    public int getLazyLoadViewPortImages() {
        return lazyLoadViewPortImages;
    }

    /**
     * @param lazyLoadViewPortImages
     *            the lazyLoadViewPortImages to set
     */
    public void setLazyLoadViewPortImages(int lazyLoadViewPortImages) {
        this.lazyLoadViewPortImages = lazyLoadViewPortImages;
    }

    /**
     * @return the dimensionSortMap
     */
    public HashMap<string, String> getDimensionSortMap() {
        return dimensionSortMap;
    }

    /**
     * @param dimensionSortMap
     *            the dimensionSortMap to set
     */
    public void setDimensionSortMap(HashMap<string, String> dimensionSortMap) {
        this.dimensionSortMap = dimensionSortMap;
    }

    /**
     * @return the defaultDimensionSort
     */
    public String getDefaultDimensionSort() {
        return defaultDimensionSort;
    }

    /**
     * @param defaultDimensionSort
     *            the defaultDimensionSort to set
     */
    public void setDefaultDimensionSort(String defaultDimensionSort) {
        this.defaultDimensionSort = defaultDimensionSort;
    }

    /**
     * @return the enableSizeCustomSorting
     */
    public boolean isEnableSizeCustomSorting() {
        return enableSizeCustomSorting;
    }

    /**
     * @param enableSizeCustomSorting
     *            the enableSizeCustomSorting to set
     */
    public void setEnableSizeCustomSorting(boolean enableSizeCustomSorting) {
        this.enableSizeCustomSorting = enableSizeCustomSorting;
    }

    /**
     * @return the maximumAttachedFiles
     */
    public int getMaximumAttachedFiles() {
        return maximumAttachedFiles;
    }

    /**
     * @param maximumAttachedFiles
     *            the maximumAttachedFiles to set
     */
    public void setMaximumAttachedFiles(int maximumAttachedFiles) {
        this.maximumAttachedFiles = maximumAttachedFiles;
    }

    /**
     * @return the limitOfUploadedFileSize
     */
    public int getLimitOfUploadedFileSize() {
        return limitOfUploadedFileSize;
    }

    /**
     * @param limitOfUploadedFileSize
     *            the limitOfUploadedFileSize to set
     */
    public void setLimitOfUploadedFileSize(int limitOfUploadedFileSize) {
        this.limitOfUploadedFileSize = limitOfUploadedFileSize;
    }

    /**
     * @return the welcomeModalExpiry
     */
    public int getWelcomeModalExpiry() {
        return welcomeModalExpiry;
    }

    /**
     * @param welcomeModalExpiry
     *            the welcomeModalExpiry to set
     */
    public void setWelcomeModalExpiry(int welcomeModalExpiry) {
        this.welcomeModalExpiry = welcomeModalExpiry;
    }

    /**
     * @return the enableSupervisorEmailValidation
     */
    public boolean isEnableSupervisorEmailValidation() {
        return enableSupervisorEmailValidation;
    }

    /**
     * @param enableSupervisorEmailValidation
     *            the enableSupervisorEmailValidation to set
     */
    public void setEnableSupervisorEmailValidation(boolean enableSupervisorEmailValidation) {
        this.enableSupervisorEmailValidation = enableSupervisorEmailValidation;
    }

    /**
     * @return the customSortingEnabledMap
     */
    public Map<String, String> getCustomSortingEnabledMap() {
        return customSortingEnabledMap;
    }

    /**
     * @param customSortingEnabledMap
     *            the customSortingEnabledMap to set
     */
    public void setCustomSortingEnabledMap(Map<String, String> customSortingEnabledMap) {
        this.customSortingEnabledMap = customSortingEnabledMap;
    }

    /**
     * @return the hideSavedCart
     */
    public boolean isHideSavedCart() {
        return hideSavedCart;
    }

    /**
     * @param hideSavedCart
     *            the hideSavedCart to set
     */
    public void setHideSavedCart(boolean hideSavedCart) {
        this.hideSavedCart = hideSavedCart;
    }

	/**
	 * @return the enableDefaultShipTo
	 */
	public boolean isEnableDefaultShipTo() {
		return enableDefaultShipTo;
	}

	/**
	 * @param enableDefaultShipTo the enableDefaultShipTo to set
	 */
	public void setEnableDefaultShipTo(boolean enableDefaultShipTo) {
		this.enableDefaultShipTo = enableDefaultShipTo;
	}

	public boolean isInvalidteCacheEnabled() {
		return invalidteCacheEnabled;
	}

	public void setInvalidteCacheEnabled(boolean invalidteCacheEnabled) {
		this.invalidteCacheEnabled = invalidteCacheEnabled;
	}

	/**
	 * @return the skippedItems
	 */
	public List<String> getSkippedItems() {
		return skippedItems;
	}

	/**
	 * @param skippedItems the skippedItems to set
	 */
	public void setSkippedItems(List<String> skippedItems) {
		this.skippedItems = skippedItems;
	}

	public Set<String> getStockingTypesNotPurchasable() {
		return stockingTypesNotPurchasable;
	}

	public void setStockingTypesNotPurchasable(Set<String> stockingTypesNotPurchasable) {
		this.stockingTypesNotPurchasable = stockingTypesNotPurchasable;
	}


	public Map<String, List<String>> getErrorProducts() {
	    return errorProducts;
	}

	public void setErrorProducts(Map<String, List<String>> errorProducts) {
	    this.errorProducts = errorProducts;
	}

	public String getErrorAddressLoaderFile() {
	    return errorAddressLoaderFile;
	}

	public void setErrorAddressLoaderFile(String errorAddressLoaderFile) {
	    this.errorAddressLoaderFile = errorAddressLoaderFile;
	}

}
