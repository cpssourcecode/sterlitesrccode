package com.cps.util;

import java.util.ArrayList;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;


public class PoiUtils {

	/**
	 * The default separator to use if none is supplied to the constructor.
	 */
	public static final String DEFAULT_SEPARATOR = ",";

	/**
	 * Identifies that the CSV file should obey Excel's formatting conventions
	 * with regard to escaping certain embedded characters - the field separator,
	 * speech mark and end of line (EOL) character
	 */
	public static final int EXCEL_STYLE_ESCAPING = 0;

	/**
	 * Identifies that the CSV file should obey UNIX formatting conventions
	 * with regard to escaping certain embedded characters - the field separator
	 * and end of line (EOL) character
	 */
	public static final int UNIX_STYLE_ESCAPING = 1;

	public static String separator = DEFAULT_SEPARATOR;


	/**
	 * Called to convert the contents of the currently opened workbook into
	 * a CSV file.
	 */
	public static ArrayList<ArrayList<String>> convertToCSV(Workbook workbook) {
		int maxRowWidth = 0;
		Sheet sheet = null;
		Row row = null;
		int lastRowNum = 0;
		ArrayList<ArrayList<String>> csvData = new ArrayList<ArrayList<String>>();

		int numSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numSheets; i++) {

			sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				lastRowNum = sheet.getLastRowNum();
				for (int j = 0; j <= lastRowNum; j++) {
					row = sheet.getRow(j);
					rowToCSV(row, csvData, workbook, maxRowWidth);
				}
			}
		}

		return csvData;
	}


	/**
	 * Called to convert a row of cells into a line of data that can later be
	 * output to the CSV file.
	 *
	 * @param row An instance of either the HSSFRow or XSSFRow classes that
	 *            encapsulates information about a row of cells recovered from
	 *            an Excel workbook.
	 */
	private static int rowToCSV(Row row, ArrayList<ArrayList<String>> csvData, Workbook workbook, int maxRowWidth) {
		DataFormatter formatter = new DataFormatter(true);
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();

		Cell cell = null;
		int lastCellNum = 0;
		ArrayList<String> csvLine = new ArrayList<String>();
		if (row != null) {
			lastCellNum = row.getLastCellNum();
			for (int i = 0; i <= lastCellNum; i++) {
				cell = row.getCell(i);
				if (cell == null) {
					csvLine.add("");
				} else {
					if (cell.getCellType() != Cell.CELL_TYPE_FORMULA) {
						csvLine.add(formatter.formatCellValue(cell));
					} else {
						csvLine.add(formatter.formatCellValue(cell, evaluator));
					}
				}
			}
			if (lastCellNum > maxRowWidth) {
				maxRowWidth = lastCellNum;
			}
		}
		csvData.add(csvLine);
		return maxRowWidth;
	}


	/**
	 * @param field An instance of the String class encapsulating the formatted
	 *              contents of a cell on an Excel worksheet.
	 * @return A String that encapsulates the formatted contents of that
	 * Excel worksheet cell but with any embedded separator, EOL or
	 * speech mark characters correctly escaped.
	 */
	public static String escapeEmbeddedCharacters(String field) {
		int formattingConvention = 0;
		StringBuffer buffer = null;

		if (formattingConvention == EXCEL_STYLE_ESCAPING) {
			if (field.contains("\"")) {
				buffer = new StringBuffer(field.replaceAll("\"", "\\\"\\\""));
				buffer.insert(0, "\"");
				buffer.append("\"");
			} else {
				buffer = new StringBuffer(field);
				if ((buffer.indexOf(separator)) > -1 ||
						(buffer.indexOf("\n")) > -1) {
					buffer.insert(0, "\"");
					buffer.append("\"");
				}
			}
			return (buffer.toString().trim());
		} else {
			if (field.contains(separator)) {
				field = field.replaceAll(separator, ("\\\\" + separator));
			}
			if (field.contains("\n")) {
				field = field.replaceAll("\n", "\\\\\n");
			}
			return (field);
		}
	}

}
