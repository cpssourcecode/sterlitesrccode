package com.cps.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.concurrent.ConcurrentHashMap;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import vsg.util.VSGMessageUtils;

import atg.adapter.gsa.GSARepository;
import atg.droplet.DropletFormException;
import atg.droplet.GenericFormHandler;
import atg.nucleus.Nucleus;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.dynamo.LangLicense;

public class CPSMessageUtils extends VSGMessageUtils{

	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(CPSMessageUtils.class);

	public static int REPOSITORY_BUNDLE = 0;
	public static int RESOURCE_BUNDLE = 1;

	public static String getErrorMessage(String pLookUpKey, Locale pLocale){
		GSARepository messageRepository = (GSARepository) getRepositoryBundle(CPSConstants.ERR_MESSAGES_REPOSITORY, pLocale);
		return CPSMessageUtils.getMessageText(messageRepository, pLookUpKey);
	}

	public static void addFormException(GenericFormHandler pHandler, String pExceptionRepositoryName, String pLookUpKey, Locale pLocale){
		GSARepository messageRepository = (GSARepository) getRepositoryBundle(pExceptionRepositoryName, pLocale);
		String errorMessage = CPSMessageUtils.getMessageText(messageRepository, pLookUpKey);
		pHandler.addFormException(new DropletFormException(errorMessage, null));
	}
	
	public static void addFormException(GenericFormHandler pHandler, String pLookUpKey, Locale pLocale, String pControlName){
		GSARepository messageRepository = (GSARepository) getRepositoryBundle(CPSConstants.ERR_MESSAGES_REPOSITORY, pLocale);
		String errorMessage = CPSMessageUtils.getMessageText(messageRepository, pLookUpKey);
		pHandler.addFormException(new DropletFormException(errorMessage, pControlName));
	}
	
	public static void addFormException(GenericFormHandler pHandler, String pExceptionRepositoryName, String pLookUpKey, Locale pLocale, String errorCode){
		GSARepository messageRepository = (GSARepository) getRepositoryBundle(pExceptionRepositoryName, pLocale);
		String errorMessage = CPSMessageUtils.getMessageText(messageRepository, pLookUpKey);
//		pHandler.addFormException(new DropletFormException(errorMessage, pHandler.getAbsoluteName(), errorCode));
		pHandler.addFormException(new DropletFormException(errorMessage, errorCode));
	}

	public static void addFormException (GenericFormHandler pHandler, String pExceptionRepositoryName, String pLookUpKey, String path, Locale pLocale){
		GSARepository messageRepository = (GSARepository) getRepositoryBundle(pExceptionRepositoryName, pLocale);
		String errorMessage = CPSMessageUtils.getMessageText(messageRepository, pLookUpKey);
		pHandler.addFormException(new DropletFormException(errorMessage, generatePropertyPath(pHandler, path)));
	}
	
	public static final String generatePropertyPath(final GenericFormHandler pHandler, final String pKey){
		return pHandler.getAbsoluteName() + ".value." + pKey;
	}
	
	/**
	 * Add the form exception to the form handler with formated errormessage with dynamic values
	 * @param pHandler
	 * @param pExceptionRepositoryName
	 * @param pLookUpKey
	 * @param pLocale
	 * @param errorCode
	 * @param pParams
	 */
	public static void addFormException(GenericFormHandler pHandler, String pExceptionRepositoryName, String pLookUpKey, Locale pLocale, String errorCode, Object[] pParams){
		GSARepository messageRepository = (GSARepository) getRepositoryBundle(pExceptionRepositoryName, pLocale);
		String errorMessage = CPSMessageUtils.getMessageText(messageRepository, pLookUpKey);
		String formatedMessage = MessageFormat.format(errorMessage, pParams);
		pHandler.addFormException(new DropletFormException(formatedMessage, pHandler.getAbsoluteName(), errorCode));
	}
	
	/**
	 * Method responsible to give the formated message with dynamic values
	 * @param pExceptionRepositoryName
	 * @param pLookUpKey
	 * @param pLocale
	 * @param pParams
	 * @return String - formated message
	 */
	public static String getFormattedMessage(String pExceptionRepositoryName, String pLookUpKey, Locale pLocale, Object[] pParams){
		GSARepository messageRepository = (GSARepository) getRepositoryBundle(pExceptionRepositoryName, pLocale);
		String message = CPSMessageUtils.getMessageText(messageRepository, pLookUpKey);
		String formatedMessage = MessageFormat.format(message,  pParams);
		return formatedMessage;
	}
	
	public static String getMessageText(Repository mExceptionRepository, String pLookUpKey){
		RepositoryView view = null;
		String message = pLookUpKey;
		try{
			view = mExceptionRepository.getView("userMessage");
			RqlStatement statement = RqlStatement.parseRqlStatement("keyValue=?0");
			Object params[] = new Object[1];
			params[0] = pLookUpKey;
			RepositoryItem[] items = statement.executeQuery(view, params);
			if (items != null && items.length > 0){
				message = (String) items[0].getPropertyValue("message");
			}
		}catch (RepositoryException e){
			mLogging.logError(e);
		}
		return message;
	}
	
	protected static Object getRepositoryBundle(String pBaseName, Locale pLocale){
		Object repository = null;
		
		Map bundleNames = (Map)sBundles.get(pLocale);
		if (bundleNames == null){
			repository = (Repository)Nucleus.getGlobalNucleus().resolveName(pBaseName);
			bundleNames = new ConcurrentHashMap();
			bundleNames.put(pBaseName, repository);
			sBundles.put(pLocale, bundleNames);
		}else{
			repository = (Repository)bundleNames.get(pBaseName);
			if (repository == null){
				repository = (Repository)Nucleus.getGlobalNucleus().resolveName(pBaseName);
				bundleNames.put(pBaseName, repository);
			}
		}
		return repository;
	}
	
	public static Object getBundle(String pBaseName, int pBundleType, Locale pLocale) throws MissingResourceException{
		if (pBaseName == null)
			throw new MissingResourceException("(null)", pBaseName, null);
		if (pLocale == null)
			pLocale = LangLicense.getLicensedDefault();
		Object bundle = null;
		
		switch (pBundleType){
		case 0:
			//resolve repository bundle
			bundle = getRepositoryBundle(pBaseName, pLocale);
			break;
		case 1:
			//resolve resource bundle
			bundle = getResourceBundle(pBaseName, pLocale);
			break;
		default:
			break;
		}
		
		return bundle;
	}
}