package com.cps.util;

import java.util.List;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;

/**
 * 
 * @author David Mednikov
 *
 */
public class ManageSessionBean extends GenericService{
	/**
	 * @isSessionLocale flag to use session locale over profile local
	 */
	private boolean isSessionLocale=false;
	
	/**
	 * @users users for manage-users page
	 */
	private List<RepositoryItem> users;
	/**
	 * 
	 */
	private Integer loginErrorCount;
	/**
	 * 
	 */
	private Integer loginErrorWithCaptchaCount;
	/**
	 * 
	 */
	private boolean loginError;
	
	public List<RepositoryItem> getUsers() {
		return users;
	}
	public void setUsers(List<RepositoryItem> users) {
		this.users = users;
	}
	public Integer getLoginErrorCount() {
		return loginErrorCount;
	}
	public void setLoginErrorCount(Integer loginErrorCount) {
		this.loginErrorCount = loginErrorCount;
	}
	public Integer getLoginErrorWithCaptchaCount() {
		return loginErrorWithCaptchaCount;
	}
	public void setLoginErrorWithCaptchaCount(Integer loginErrorWithCaptchaCount) {
		this.loginErrorWithCaptchaCount = loginErrorWithCaptchaCount;
	}
	public boolean isLoginError() {
		return loginError;
	}
	public void setLoginError(boolean loginError) {
		this.loginError = loginError;
	}
	public boolean isSessionLocale() {
		return isSessionLocale;
	}
	public void setSessionLocale(boolean isSessionLocale) {
		this.isSessionLocale = isSessionLocale;
	}

}
