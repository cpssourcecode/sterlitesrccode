package com.cps.util;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

/**
 * This class has a bunch of utility methods to access the repository which are not there in ATG OOTB
 *
 * @author Srilakshmi Sampath
 *
 */
public class RepositoryUtils extends GenericService {

    /**
     *
     * @param pItemDescriptorName
     * @param pRepository
     * @param pRQLStatement
     * @param pQueryParams
     * @returnO
     * @throws RepositoryException
     */
    public int executeCountQuery(String pItemDescriptorName, Repository pRepository, String pRQLStatement, Object... pQueryParams) throws RepositoryException {
        RepositoryItem[] items = executeQuery(pItemDescriptorName, pRepository, pRQLStatement, pQueryParams);
        return ArrayUtils.getLength(items);
    }

    /**
     *
     * @param pItemDescriptorName
     * @param pRepository
     * @param pRQLStatement
     * @param pQueryParams
     * @return
     * @throws RepositoryException
     */
    public int executeCountQuery(String pItemDescriptorName, Repository pRepository, RqlStatement pRQLStatement, Object... pQueryParams)
                    throws RepositoryException {
        RepositoryItem[] items = executeQuery(pItemDescriptorName, pRepository, pRQLStatement, pQueryParams);
        return ArrayUtils.getLength(items);
    }

    /**
     *
     * @param pItemDescriptorName
     * @param pRepository
     * @param pRQLStatement
     * @param pQueryParams
     * @return
     * @throws RepositoryException
     */
    public RepositoryItem[] executeQuery(String pItemDescriptorName, Repository pRepository, RqlStatement pRQLStatement, Object... pQueryParams)
                    throws RepositoryException {
        RepositoryView repositoryView = pRepository.getView(pItemDescriptorName);
        vlogDebug("Repository View :: {0}", repositoryView);
        vlogDebug("RQL Statement ::  :: {0}", pRQLStatement);
        vlogDebug("Query Parameter :: {0}", pQueryParams);
        RepositoryItem[] items = pRQLStatement.executeQuery(repositoryView, pQueryParams);
        if (!ArrayUtils.isEmpty(items)) {
            vlogDebug("Found {0} items for {1}", items, pRQLStatement);
        }

        return items;
    }

    /**
     *
     * @param pRQLStatement
     * @return
     * @throws RepositoryException
     */
    public RqlStatement getRqlStatement(String pRQLStatement) throws RepositoryException {
        return RqlStatement.parseRqlStatement(pRQLStatement);
    }

    /**
     *
     * @param pItemDescriptorName
     * @param pRepository
     * @param pRQLStatement
     * @param pQueryParams
     * @returnO
     * @throws RepositoryException
     */
    public RepositoryItem[] executeQuery(String pItemDescriptorName, Repository pRepository, String pRQLStatement, Object... pQueryParams)
                    throws RepositoryException {
        RqlStatement rqlStatement = getRqlStatement(pRQLStatement);
        return executeQuery(pItemDescriptorName, pRepository, rqlStatement, pQueryParams);
    }

    /**
     *
     * @param property
     * @param valuesList
     * @return
     */
    public String buildRql(String property, List<String> valuesList) {
        StringBuilder rqlStatement = new StringBuilder(property);
        for (int i = 0; i < valuesList.size(); i++) {
            if (i != 0) {
                rqlStatement.append(property);
            }
            rqlStatement.append("=\"").append(valuesList.get(i)).append('"');
            if (i != valuesList.size() - 1) {
                rqlStatement.append(" OR ");
            }
        }
        return rqlStatement.toString();
    }
}
