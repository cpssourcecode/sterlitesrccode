package com.cps.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.servlet.DynamoHttpServletResponse;
import vsg.util.VSGFormUtils;

public class CPSFormUtils extends VSGFormUtils {

	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(CPSFormUtils.class);

	/** Regular expression bundle. */
	private static final String REG_EXP = "cps.util.RegularExpressions";
	
	/** Resource bundle. */
	final static ResourceBundle regularExpBundle = ResourceBundle.getBundle(REG_EXP);
	public static void addAjaxErrorResponse(GenericFormHandler pFormHandler, DynamoHttpServletResponse pResponse,Set<String> propertyPath, boolean showCaptcha)
			throws IOException, JSONException {

		pResponse.setContentType("application/json");

		JSONObject responseJson=new JSONObject();
		responseJson.put("error", "true");

		JSONArray errorArray = new JSONArray();

		//JSONObject errorArray[] = new JSONObject[getFormExceptions().size()];
		JSONArray errorPropertiesArray = new JSONArray(); // new JSONObject[getFormExceptions().size()];

		for(int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
			DropletException error = (DropletException) pFormHandler.getFormExceptions().get(i);
			errorArray.add(error.getMessage());
		}

		for(String property: propertyPath){
			errorPropertiesArray.add(property);
		}
		/*Iterator<String> it = propertyPath.keySet().iterator();
		while (it.hasNext()) {
			errorPropertiesArray.add(it.next());
		}*/
		
		if (showCaptcha){
			responseJson.put("captcha", true);
		}

		responseJson.put("errors", errorArray);
		responseJson.put("properties", errorPropertiesArray);

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}
	/**
	 * Generic method that takes a value, boolean if the property
	 * is required, and regular expression to match the value too. 
	 * 
	 * regEx values are managed in resources.jlg.util.RegularExpressions. 
	 * 
	 * Default regEx = addrSpecialCharRegExp
	 * 
	 * @param value value to check
	 * @param isRequired boolean value for is required
	 * @param regEx - alphaRegExp, numRegExp, alphaNumRegExp, emailRegExp,
	 * 		pwdRegExp, nameRegExp, addrSpecialCharRegExp, zipcodeRegExp, phoneRegExp,
	 * 		canadaPostalRegEx
	 * @return
	 * 		true if value matches regEx and fits required flag, false otherwise
	 */
	public static boolean checkProperty (String value, boolean isRequired, String regEx){
		String regExp = null;
		try {
			// Try to pull regEx from resource bundle
			regExp = ResourceUtils.getMsgResource(regEx, REG_EXP, regularExpBundle);
		}catch(Exception e){
			mLogging.logError(e);
		}
		if (StringUtils.isBlank(regExp)){
			regExp = ResourceUtils.getMsgResource(CPSConstants.REG_EXP_SPECIAL_CHAR, REG_EXP, regularExpBundle);
		}
		if (StringUtils.isBlank(value) && isRequired){
			return false;
		} else if (!StringUtils.isBlank(value)) {
			Pattern p = Pattern.compile(regExp);
			Matcher m = p.matcher(value);
			return m.matches();
		}
		
		return true;
	}
	
	/**
	  * Generic method that takes a value, boolean if the property
	 * is required, and regular expression to match the value too. 
	 * 
	 * regEx values are managed in resources.jlg.util.RegularExpressions. 
	 * 
	 * Default regEx = addrSpecialCharRegExp
	 * 
	 * @param value value to check
	 * @param isRequired boolean value for is required
	 * @param regEx - alphaRegExp, numRegExp, alphaNumRegExp, emailRegExp,
	 * 		pwdRegExp, nameRegExp, addrSpecialCharRegExp, zipcodeRegExp, phoneRegExp,
	 * 		canadaPostalRegEx
	 * @param emptyError - error message to return if empty
	 * @param invalidError - error message to return if invalid
	 * @return
	 * 		error message to display if invalid
	 */
	public static String checkProperty (String value, boolean isRequired, String regEx, String emptyError, String invalidError){
		String regExp = null;
		try {
			// Try to pull regEx from resource bundle
			regExp = ResourceUtils.getMsgResource(regEx, REG_EXP, regularExpBundle);
		}catch(Exception e){
			mLogging.logError(e);
		}
		if (StringUtils.isBlank(regExp)){
			regExp = ResourceUtils.getMsgResource(CPSConstants.REG_EXP_SPECIAL_CHAR, REG_EXP, regularExpBundle);
		}
		if (StringUtils.isBlank(value) && isRequired){
			return emptyError;
		} else if (!StringUtils.isBlank(value)) {
			Pattern p = Pattern.compile(regExp);
			Matcher m = p.matcher(value);
			if (!m.matches()){
				return invalidError;
			}
		}
		
		return null;
	}
	/**
	 * Adding Success Json response.
	 *
	 * @param pResponse
	 * @throws IOException
	 * @throws JSONException
	 */
	public static void addAjaxSuccessResponse(Boolean showCS, DynamoHttpServletResponse pResponse)
			throws IOException, JSONException {
		pResponse.setContentType("application/json");

		JSONObject responseJson;
		responseJson = new JSONObject();

		responseJson.put("error", "false");
		if (showCS)
			responseJson.put("showCS", "true");

		PrintWriter out = pResponse.getWriter();
		out.print(responseJson);
		out.flush();
	}
	
	public static boolean checkMaxLength(String s, int maxLength){
		if (s != null) {
			try {
				return s.getBytes("UTF-8").length <= maxLength;
			} catch (UnsupportedEncodingException e) {
				mLogging.logError(e);
			}
		}
		return false;
	}
}