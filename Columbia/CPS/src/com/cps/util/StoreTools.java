package com.cps.util;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * StoreTools
 * 
 */
public class StoreTools extends GenericService {
	
	/**
	 * component name for debugging
	 */
	private final static String COMPONENT_NAME = "/cps/util/StoreTools";

	public static final String ITEM_DESCRIPTOR_STORE = "store";
	public static final String ITEM_DESCRIPTOR_ZIP_TO_STORE_DISTANCE = "zipToStoreDistance";

	public static final String POSTAL_CODE_PRPTY = "postalCode";
	public static final String DISTANCE_PRPTY = "distance";

	public static final String STORE_ID = "storeId";
	public static final String LOCATION_ID = "locationId";
	public static final String LATITUDE = "latitude";
	public static final String LONGITUDE = "longitude";

	/**
	 * XML Response Elements
	 */
	private static final String RESPONSE = "Response";
	private static final String PLACEMARK = "Placemark";
	private static final String POINT = "Point";
	private static final String COORDINATES = "coordinates";
	private static final String ROW = "row";
	private static final String DISTANCE = "distance";
	
	private static final String MODE = "driving";
	private static final String SENSOR = "false";
	private static final String UNITS = "imperial";
	private static final String URL_ENCODING = "UTF-8";
	
	/**
	 * 
	 */
	private GoogleSignatureGenerator mGoogleSignatureGenerator;
	
	/**
	 * @return
	 */
	public GoogleSignatureGenerator getGoogleSignatureGenerator(){
		return mGoogleSignatureGenerator;
	}
	
	/**
	 * @param pGoogleSignatureGenerator
	 */
	public void setGoogleSignatureGenerator(GoogleSignatureGenerator pGoogleSignatureGenerator){
		mGoogleSignatureGenerator = pGoogleSignatureGenerator;
	}
	
	/**
	 * 
	 */
	private String mDistanceMatrixUrl;
	
	/**
	 * @return
	 */
	public String getDistanceMatrixUrl(){
		return mDistanceMatrixUrl;
	}
	
	/**
	 * @param pDistanceMatrixUrl
	 */
	public void setDistanceMatrixUrl(String pDistanceMatrixUrl){
		mDistanceMatrixUrl = pDistanceMatrixUrl;
	}
	
	/**
	 * 
	 */
	public String mGoogleClientId;
	
	/**
	 * @return
	 */
	public String getGoogleClientId(){
		return mGoogleClientId;
	}
	
	/**
	 * @param pGoogleClientId
	 */
	public void setGoogleClientId(String pGoogleClientId){
		mGoogleClientId = pGoogleClientId;
	}

	public String mGoogleApiKey;

	/**
	 * @return
	 */
	public String getGoogleApiKey(){
		return mGoogleApiKey;
	}

	/**
	 * @param pGoogleApiKey
	 */
	public void setGoogleApiKey(String pGoogleApiKey){
		mGoogleApiKey = pGoogleApiKey;
	}

	/**
	 * Buffersize to read from the input.
	 */
	private int  mBufferSize;
	
	public int getBufferSize() {
		return mBufferSize;
	}

	public void setBufferSize(int pBufferSize) {
		mBufferSize = pBufferSize;
	}

	/**
	 * Timeout in seconds.
	 */
	private int  mTimeOut;

	public int getTimeOut() {
		return mTimeOut;
	}

	public void setTimeOut(int pTimeOut) {
		mTimeOut = pTimeOut;
	}

	private Repository mLocationRepository;

	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}

	private Repository mDistanceRepository;

	public Repository getDistanceRepository() {
		return mDistanceRepository;
	}

	public void setDistanceRepository(Repository pDistanceRepository) {
		mDistanceRepository = pDistanceRepository;
	}

	public boolean mUseClientId;

	public boolean isUseClientId(){
		return mUseClientId;
	}

	public void setUseClientId(boolean pUseClientId){
		mUseClientId = pUseClientId;
	}

	public boolean mUseApiKey;

	public boolean isUseApiKey(){
		if (isUseClientId()) {
			return false;
		}
		return mUseApiKey;
	}

	public void setUseApiKey(boolean pUseApiKey){
		mUseApiKey = pUseApiKey;
	}

	/**
	 * Adds an xml element to a DOM document
	 * @param pDocument
	 * @param pElement
	 * @param pValue
	 * @return a node
	 */
	public Node addXMLElement(Document pDocument, String pElement, String pValue) {
		Element element = pDocument.createElement(pElement);
		if (pValue != null) {
			element.setTextContent(pValue.trim());
		}
		return element;
	}
	
	/**
	 * finds an element in the response xml and returns the contents.
	 * @param pDocument
	 * @return a string of the response.
	 */
	public List<String> getDistanceElement(Document pDocument) {
		List<String> distanceList = new ArrayList<String>();
		NodeList nList = pDocument.getElementsByTagName(ROW);
		Node nNode = nList.item(0);
		if ( nNode.getNodeType() == Node.ELEMENT_NODE) {
			NodeList nDistanceList = pDocument.getElementsByTagName(DISTANCE);
			for (int x = 0; x < nDistanceList.getLength(); x++) {
				Node nDistanceNode = nDistanceList.item(x);
				if (nDistanceNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eDistanceElement = (Element) nDistanceNode;
					distanceList.add(getTagValue("text", eDistanceElement));
				}
			}
		}
		return distanceList;
	}
	
	/**
	 * @param sTag
	 * @param eElement
	 * @return
	 */
	private String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
        Node nValue = nlList.item(0);
		return nValue.getNodeValue();
	  }
	
	/**
	 * finds an element in the response xml and returns the contents.
	 * @param pDocumnet
	 * @param pElement
	 * @return a string of the response.
	 */
	public String getElement(Document pDocumnet, String pElement) {
	    NodeList nodeList = pDocumnet.getElementsByTagName(pElement); 
	    String returnString = ""; 
	    if (nodeList != null && nodeList.getLength() > 0) { 
	        nodeList = nodeList.item(0).getChildNodes(); 
	        if (nodeList != null && nodeList.getLength() > 0) { 
				returnString = nodeList.item(0).getNodeValue();
	        }
	    } 
	    return returnString;
	}
	
	/**
	 * Parses an XML file and returns the DOM document.
	 * @param pXML 
	 * @return a Document with the results.
	 * @throws Exception
	 */
	public Document parseXML(String pXML) throws Exception {
	    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		return documentBuilder.parse(new ByteArrayInputStream(pXML.getBytes()));
	}

	/**
	 * Reads the response from a connection and and loads the results into a String.
	 * @param pURLConnection
	 * @return a string of xml data.
	 * @throws Exception
	 */
	public String readInput(URLConnection pURLConnection) throws Exception{
		
		InputStreamReader reader = new InputStreamReader(pURLConnection.getInputStream());
		StringBuilder buf = new StringBuilder();
		char[] cbuf = new char[ getBufferSize() ];
		int num;
		while ( -1 != (num=reader.read( cbuf ))) {
			buf.append( cbuf, 0, num );
		}
		return buf.toString();
	}
	
	/**
	 * Makes call to google to get distances from passed in zip
	 * to all stores in store repository
	 * 
	 * @param pPostalCode
	 * @return
	 * @throws atg.repository.RepositoryException
	 * @throws java.io.IOException
	 */
	public Map<String, Float> googleCall(String pPostalCode) throws RepositoryException, IOException{
		// query all stores from the repository ordered by state
		List<RepositoryItem> allStores = findAllStoresWithCoordinates();
		if (allStores != null) {
			RepositoryItem [] allStoresArray = new RepositoryItem [allStores.size()];
			allStoresArray = allStores.toArray(allStoresArray);
			//Generate url string for google call
			return googleCall(pPostalCode, allStoresArray);
		}

		return null;
	}

	/**
	 * Makes call to google to get distances from passed in zip
	 * to stores repository items passed in
	 *
	 * @param pPostalCode
	 * @param pStores
	 * @return
	 * @throws atg.repository.RepositoryException
	 * @throws java.io.IOException
	 */
	public Map<String, Float> googleCall(String pPostalCode, RepositoryItem[] pStores) throws RepositoryException, IOException{

		vlogDebug("{0}.googleCall - start", COMPONENT_NAME);

		Map<String, Float> storeDistances = null;

		//Generate url string for google call
		// example:
		// http://maps.googleapis.com/maps/api/distancematrix/xml?origins=61910&destinations=40.292108,-74.299684&mode=driving&sensor=false&units=imperial&client=YOUR_CLIENT_ID&signature=YOUR_URL_SIGNATURE
		// origin is zip code
		// destination is coordinates of stores
		StringBuilder urlString = new StringBuilder().append(getDistanceMatrixUrl());
		// add origins
		urlString.append("?origins=").append(pPostalCode);
		// add destinations
		urlString.append("&destinations=");

		// get store names and coordinates from list of stores
		Double longitude = 0.0d;
		Double latitude = 0.0d;
		List<String> locationIds = new ArrayList<String>();
		String urlDestinations="";
		int count = 0;
		for (RepositoryItem store : pStores) {
			longitude = (Double) store.getPropertyValue(LONGITUDE);
			latitude = (Double) store.getPropertyValue(LATITUDE);

			vlogDebug("Longitude: {0} \n Latitude: {1}", longitude, latitude);

			if (longitude != 0.0 && latitude != 0.0){
				locationIds.add(store.getRepositoryId());

				if (count == 0)
					urlDestinations += String.valueOf(latitude) + "," + String.valueOf(longitude);
				else
					urlDestinations += "|" + String.valueOf(latitude) + "," + String.valueOf(longitude);
				count++;
			}
		}

		urlString.append(urlDestinations);
		// add mode
		urlString.append("&mode=").append(MODE);
		// add sensor
		urlString.append("&sensor=").append(SENSOR);
		// add units
		urlString.append("&units=").append(UNITS);
		if (isUseClientId()) {
			// add client
			urlString.append("&client=").append(getGoogleClientId());
		} else if (isUseApiKey()) {
			// add key
			urlString.append("&key=").append(getGoogleApiKey());
		}

		vlogDebug("Google URL before signature: {0}", urlString);

		String fullUrlString = urlString.toString();
		if (isUseClientId()) {
			try {
				// URL encode url string
				// The URL shown in these examples must be already
				// URL-encoded. In practice, you will likely have code
				// which assembles your URL from user or web service input
				// and plugs those values into its parameters.
				// get signature
				//fullUrlString = getGoogleSignatureGenerator().requestSignature(URLEncoder.encode(urlString.toString(), URL_ENCODING));
				fullUrlString = getGoogleSignatureGenerator().requestSignature(urlString.toString());
			} catch (Exception e){
				if (isLoggingError()) {
					logError("Error getting url signature", e);
				}
				return null;
			}
		}

		if (!StringUtils.isBlank(fullUrlString)){
			vlogDebug("Make call to google with full URL: {0}", fullUrlString);

			// generate url and make call
			URL url = new URL(fullUrlString);
			URLConnection urlConnection = url.openConnection();

			// specify that we will accept input
			urlConnection.setDoInput(true);
			urlConnection.setDoOutput(false);
			urlConnection.setConnectTimeout( getTimeOut() );  // long timeout, but not infinite
			urlConnection.setReadTimeout( getTimeOut() );
			urlConnection.setUseCaches (false);
			urlConnection.setDefaultUseCaches (false);
			urlConnection.setRequestProperty ( "Content-Type", "text/xml" );

			// Get the distances from the response
			List<String> distanceList;
			String response = "";
			try {
				response = readInput(urlConnection);

				Document responseDocument = parseXML(response);

				distanceList = getDistanceElement(responseDocument);

			} catch(Exception e) {
				vlogError("Error google response: {0}",  response);
				return null;
			}

			// Add both the store name and the distance to the
			// storeDistances for each in lists
			if (!distanceList.isEmpty()){
				storeDistances = new HashMap<String, Float>();
				for (int i=0; i < locationIds.size(); i++){
					// convert string distance to float
					Float d = convertDistance(distanceList.get(i));
					storeDistances.put(locationIds.get(i), d);
				}
			} else {
				vlogError("Distance list is empty.");
			}
		} else {
			vlogError("No url to send to google for distances.");
		}

		vlogDebug("{0}.googleCall - exit", COMPONENT_NAME);

		return storeDistances;
	}

	/**
	 * @param distance
	 * @return
	 */
	public float convertDistance(String distance){
		float d = 9999999f;

		// Remove everything except numbers and .
		distance = distance.replaceAll("[^\\d.]", "");
		vlogDebug("Distance to convert: {0}", distance);
		try {
			d = Float.valueOf(distance.trim()).floatValue();
		}catch (NumberFormatException nfe) {
			logError(nfe.getMessage());
		}

		return d;
	}

	/**
	 * @deprecated Using repository to store distance information
	 *
	 * Method called to find the 3 nearest stores for the entered in zip
	 *
	 * @param zip
	 *
	 * @author Chris
	 * @throws java.io.IOException
	 * @throws atg.repository.RepositoryException
	 */
	public Map<String, Float> findNearStores(String zip) throws IOException, RepositoryException{
		/*
		// Get the coordinates
		String coordinates = findCoordinates(zip);

		// Parse coordinates to lat and lon
		String latitude = coordinates.substring(0, coordinates.indexOf(",")).trim();
		String longitude = coordinates.substring(coordinates.indexOf(",")+1, coordinates.length()).trim();
		*/
		Map<String, Float> storeDistances = googleCall(zip);
		if (storeDistances != null){
			Float d;
			String storeId;
			Map<String, Float> validStoreDistances = new LinkedHashMap<String, Float>();
			Map<String, Float> sortedStoreDistances = new LinkedHashMap<String, Float>();
			// Only return stores within 30 mile
			for (Entry<String, Float> distance:storeDistances.entrySet()){
				d = distance.getValue();
				storeId = distance.getKey();
				if (d <= 30){
					//storeDistances.remove(storeId);
					validStoreDistances.put(storeId, d);
				}
			}
			// Sort store by distance
			sortedStoreDistances = sortByValue(validStoreDistances);

			List<String> stores = new ArrayList<String>();
			List<String> distances = new ArrayList<String>();
			for (Entry<String, Float> entry:sortedStoreDistances.entrySet()){
				stores.add(entry.getKey());
				distances.add(entry.getValue().toString());
			}

//			getSessionBean().setStoresList(stores);
//			getSessionBean().setDistancesList(distances);
			return sortedStoreDistances;
		}

		return null;
	}

	/**
	 * Method called to find the nearest stores for the entered in zip
	 *
	 * @param pPostalCode
	 * @return
	 * @throws java.io.IOException
	 * @throws atg.repository.RepositoryException
	 */
	public Map<String, Float> findNearestStores(String pPostalCode) throws IOException, RepositoryException{

		vlogDebug("{0}.findNearestStores - start", COMPONENT_NAME);


		Map<String, Float> validStoreDistances = null;

		// query all stores from the repository ordered by state
		List<RepositoryItem> allStores = findAllStoresWithCoordinates();
		if (allStores != null) {

			vlogDebug("Found stores in the repository, now check for stores by distance.");

			Map<String, Float> storeDistances = findStoreDistances(pPostalCode, allStores);

			if (storeDistances != null && storeDistances.size() > 0) {
				Float d;
				String storeId;
				validStoreDistances = new LinkedHashMap<String, Float>();
				Map<String, Float> sortedStoreDistances = new LinkedHashMap<String, Float>();
				// Only return stores within the near store distance (miles)
				for (Entry<String, Float> distance:storeDistances.entrySet()){
					d = distance.getValue();
					storeId = distance.getKey();
					if (d <= getGoogleSignatureGenerator().getNearStoreDistance()){
						//storeDistances.remove(storeId);
						validStoreDistances.put(storeId, d);
					}
				}
				// Sort stores by distance
				sortedStoreDistances = sortByValue(validStoreDistances);
				// build lists of store names & distances
				List<String> stores = new ArrayList<String>();
				List<String> distances = new ArrayList<String>();
				for (Entry<String, Float> entry:sortedStoreDistances.entrySet()){
					stores.add(entry.getKey());
					distances.add(entry.getValue().toString());
				}
				// add lists to session, this is what is important in this method
//				getSessionBean().setStoresList(stores);
//				getSessionBean().setDistancesList(distances);
				// this value is not really used
				return sortedStoreDistances;
			}
		} else {
			vlogError("No stores in the repository, this should be fixed.");
		}

		vlogDebug("{0}.findNearestStores - exit", COMPONENT_NAME);

		return validStoreDistances;
	}

	/**
	 * Method called to find the nearest stores for Longitude and Latitude
	 *
	 * @param pLongitude        Longitude
	 * @param pLatitude          Latitude
	 * @return        Map<String, Double>
	 * @throws java.io.IOException
	 * @throws atg.repository.RepositoryException
	 */

	public Map<String, Double> findNearestStoresByCoordinates(String pLongitude, String pLatitude) throws IOException, RepositoryException {

		vlogDebug("{0}.findNearestStores - start", COMPONENT_NAME);

		Map<String, Double> validStoreDistances = null;

		// query all stores from the repository ordered by state
		List<RepositoryItem> allStores = findAllStoresWithCoordinates();
		if (allStores != null) {
			vlogDebug("Found stores in the repository, now check for stores by distance.");

			Map<String, Double> storeDistances = new LinkedHashMap<String, Double>();
			for (RepositoryItem store : allStores) {
				String storeLatitudeStr = (String) store.getPropertyValue("latitude");
				String storeLongitudeStr = (String) store.getPropertyValue("longitude");

				double storeLatitude = Double.valueOf(storeLatitudeStr);
				double storeLongitude = Double.valueOf(storeLongitudeStr);

				double userLatitude = Double.valueOf(pLatitude);
				double userLongitude = Double.valueOf(pLongitude);

				Double distance = distFrom(userLatitude, userLongitude, storeLatitude, storeLongitude);
				String storeId = store.getRepositoryId();
				storeDistances.put(storeId, distance);

				vlogDebug("Store: {0}. Distance: {1}", storeId, distance);
			}


			if (storeDistances != null && storeDistances.size() > 0) {
				Double d;
				String storeId;
				validStoreDistances = new LinkedHashMap<String, Double>();
				Map<String, Double> sortedStoreDistances = new LinkedHashMap<String, Double>();
				// Only return stores within the near store distance (miles)
				for (Entry<String, Double> distance:storeDistances.entrySet()){
					d = distance.getValue();
					storeId = distance.getKey();
					if (d <= getGoogleSignatureGenerator().getNearStoreDistance()){
						//storeDistances.remove(storeId);
						validStoreDistances.put(storeId, d);
					}
				}
				// Sort stores by distance
				sortedStoreDistances = sortByValue(validStoreDistances);
				// build lists of store names & distances
				List<String> stores = new ArrayList<String>();
				List<String> distances = new ArrayList<String>();
				for (Entry<String, Double> entry:sortedStoreDistances.entrySet()){
					stores.add(entry.getKey());
					String distanceStr = new BigDecimal(entry.getValue()).setScale(1, RoundingMode.HALF_UP).toString();
					distances.add(distanceStr);
				}
				// add lists to session, this is what is important in this method
//				getSessionBean().setStoresList(stores);
//				getSessionBean().setDistancesList(distances);
				// this value is not really used
				return sortedStoreDistances;
			}
		} else {
			vlogError("No stores in the repository, this should be fixed.");
		}

		vlogDebug("{0}.findNearestStores - exit", COMPONENT_NAME);

		return validStoreDistances;
	}


	/**
	 * Java implementation of Haversine formula. It calculates distance in miles between lat/longs.
	 *
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return
	 */
	public static double distFrom(double lat1, double lng1, double lat2, double lng2) {
		double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
				* Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;

		return dist;
	}



	/**
	 * This method will add new distance data to the
	 * repository. This data is from google
	 *
	 * @param pDistanceItems
	 * @param pPostalCode
	 */
	private void addNewDistancesToRepository(Map<String, Float> pDistanceItems, String pPostalCode) {

		vlogDebug("{0}.addNewDistancesToRepository - start", COMPONENT_NAME);


		if (pDistanceItems != null && !StringUtils.isBlank(pPostalCode)) {
			MutableRepository repository = (MutableRepository) getDistanceRepository();
			MutableRepositoryItem distance = null;
			RepositoryItem distanceItem = null;
			String locationId = null;
			Float distanceFloat;

			Iterator it = pDistanceItems.entrySet().iterator();
			for (Entry pairs : pDistanceItems.entrySet()) {
				try {
					locationId = (String) pairs.getKey();
					RepositoryItem storeItem = findStoreByLocationId(locationId);
					// check for distance item in the repository based
					// on the postalCode and store repositoryItem
					distanceItem = findDistancesFromZipAndStore(pPostalCode, storeItem);
					if (distanceItem == null) {
						distance = repository.createItem(ITEM_DESCRIPTOR_ZIP_TO_STORE_DISTANCE);
						distance.setPropertyValue(POSTAL_CODE_PRPTY, pPostalCode);
						distance.setPropertyValue(STORE_ID, storeItem.getRepositoryId());
						distance.setPropertyValue(DISTANCE_PRPTY, pairs.getValue());
						repository.addItem(distance);
					} else {
						distanceFloat = (Float) distanceItem.getPropertyValue("distance");
						if (distanceFloat != null && distanceFloat < 0.0) {
							distance = repository.getItemForUpdate(ITEM_DESCRIPTOR_ZIP_TO_STORE_DISTANCE, distanceItem.getRepositoryId());
							distance.setPropertyValue(DISTANCE_PRPTY, pairs.getValue());
							repository.updateItem(distance);
						}
					}
				} catch (RepositoryException e) {
					if (isLoggingError()) {
						logError(e);
					}
				}
			}
		}

		vlogDebug("{0}.addNewDistancesToRepository - exit", COMPONENT_NAME);

	}

	/**
	 * This method will look for a distance item in the repository based
	 * on the postalCode and store repositoryItem
	 *
	 * @param pPostalCode
	 * @param pStore
	 * @return
	 */
	private RepositoryItem findDistancesFromZipAndStore(String pPostalCode, RepositoryItem pStore) {

		vlogDebug("{0}.findDistancesFromZipAndStore - start", COMPONENT_NAME);
		RepositoryItem result = null;
		if (!StringUtils.isBlank(pPostalCode)){
			RepositoryView view;
			try {
				view = getDistanceRepository().getView(ITEM_DESCRIPTOR_ZIP_TO_STORE_DISTANCE);
				RqlStatement statement = RqlStatement.parseRqlStatement("postalCode = ?0 AND storeId = ?1");
				Object params[] = new Object[2];
				params[0] = pPostalCode;
				params[1] = pStore.getRepositoryId();
				RepositoryItem [] stores = statement.executeQuery (view, params);
				if (stores != null && stores.length > 0) {
					result = stores[0];
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(e.getMessage());
				}
			}
		}

		vlogDebug("{0}.findDistancesFromZipAndStore - exit", COMPONENT_NAME);

		return result;
	}


	/**
	 * This method will find the sorted stores distances from the repository or by
	 * calling google
	 *
	 * @param pPostalCode
	 * @return
	 */
	public Map<String, Float> findSortedStoreDistances(String pPostalCode)
			throws IOException, RepositoryException {

		return findSortedStoreDistances(pPostalCode, findAllStoresWithCoordinates());

	}

	/**
	 * This method will find the sorted stores distances from the repository or by
	 * calling google
	 *
	 * @param pPostalCode
	 * @param pStores
	 * @return
	 */
	public Map<String, Float> findSortedStoreDistances(String pPostalCode, List<RepositoryItem> pStores)
			throws IOException, RepositoryException {

		vlogDebug("{0}.findSortedStoreDistances - start", COMPONENT_NAME);

		if (pStores != null) {
			Map<String, Float> storeDistances = findStoreDistances(pPostalCode, pStores);
			if (storeDistances != null && storeDistances.size() > 0) {
				Map<String, Float> sortedStoreDistances = sortByValue(storeDistances);
				vlogDebug("{0}.findSortedStoreDistances - exit", COMPONENT_NAME);
				return sortedStoreDistances;
			}
		} else {
			vlogError("No stores in the repository, this should be fixed.");
		}

		vlogDebug("{0}.findSortedStoreDistances - exit", COMPONENT_NAME);

		return null;
	}

	/**
	 * This method will find the stores distances from the repository
	 * or by calling google
	 *
	 * @param pPostalCode
	 * @param pStores
	 * @return
	 */
	private Map<String, Float> findStoreDistances(String pPostalCode, List<RepositoryItem> pStores) throws IOException, RepositoryException{

		vlogDebug("{0}.findStoreDistances - start", COMPONENT_NAME);

		Map<String, Float> storeDistances = new LinkedHashMap<String, Float>();
		List<RepositoryItem> newStoresNeedDistances = new ArrayList<RepositoryItem>();
		// check for distance items in the repository based on the postal code
		List<RepositoryItem> distanceItems = findDistancesFromZip(pPostalCode);
		if (distanceItems != null) {
			vlogDebug("Found store distances in the repository, check if all stores count: {0} is same as distance item count: {1} and build list.",
					pStores.size(), distanceItems.size());

			RepositoryItem storeItem = null;
			Float distance;

			// Check if list sizes match, otherwise there is a new distance to find for a store
			if (distanceItems.size() != pStores.size()) {
				for (RepositoryItem store : pStores) {
					boolean foundStore = false;
					for (RepositoryItem distanceItem : distanceItems) {
						vlogDebug("Check distanceItem: {0}  for postalCode: {1}", distanceItem.getRepositoryId(), pPostalCode);

						// get store repository item based on storeId on distanceItem
						storeItem = findStoreById((String) distanceItem.getPropertyValue(STORE_ID));

						if (storeItem != null) {
							if (store.getRepositoryId() == storeItem.getRepositoryId()) {
								// if store is found, no need to look up at google
								distance = (Float) distanceItem.getPropertyValue("distance");
								// add found store name & distance to map to return
								storeDistances.put(storeItem.getRepositoryId(), distance);

								vlogDebug("Found store: {0} in repository with distance: {1} from postal code: {2} \n",
										storeItem.getRepositoryId(), distance, pPostalCode);

								foundStore = true;
								break;
							}
						}
					} // end for loop for distanceItems
					if (!foundStore) {
						vlogDebug("Store: {0} not found in distance repository for postal code: {1}.  Add to list to request data from Google.",
								store.getRepositoryId(), pPostalCode);
						newStoresNeedDistances.add(store);
					} // end if not foundStore
				} // end for loop for stores
			} else {
				// distances size matches up to store count, add all distances to output
				for (RepositoryItem distanceItem : distanceItems) {
					if (distanceItem != null) {
						// get store repository item based on storeId on distanceItem
						storeItem = findStoreById((String) distanceItem.getPropertyValue(STORE_ID));
						if (storeItem != null) {
								distance = (Float) distanceItem.getPropertyValue("distance");
								storeDistances.put(storeItem.getRepositoryId(), distance);
							vlogDebug("Found store: {0} in repository with distance: {1}. from postal code: {2} \n",
									storeItem.getRepositoryId(), distance, pPostalCode);
						} else {
							vlogError("Store not found based on repository distanceItem: {0} with store value: {1}",
									distanceItem.getRepositoryId(),  distanceItem.getPropertyValue(STORE_ID));
						}
					}
				}
			} // end if/else store size & distanceItems size matches

		} else {
			vlogDebug("No distances found for postalCode: {0} , add all the stores to list to query at Google.", pPostalCode);
			newStoresNeedDistances.addAll(pStores);
		}

		if (!newStoresNeedDistances.isEmpty()) {
			RepositoryItem [] newStoresNeedDistancesArray = new RepositoryItem [newStoresNeedDistances.size()];
			newStoresNeedDistancesArray = newStoresNeedDistances.toArray(newStoresNeedDistancesArray);
			Map<String, Float> newStoreDistances = googleCall(pPostalCode, newStoresNeedDistancesArray);
			if (newStoreDistances != null) {
				// retrieved new store distances from google, add them to the repository
				addNewDistancesToRepository(newStoreDistances, pPostalCode);
				// add new store distances to output
				storeDistances.putAll(newStoreDistances);
			}
		}

		vlogDebug("{0}.findStoreDistances - exit", COMPONENT_NAME);

		return storeDistances;
	}

	/**
	 * This method will return all stores that have
	 * a State and coordinate values set ascending by state and postal code
	 *
	 * @return
	 */
	private List<RepositoryItem> findAllStoresWithCoordinates() {

		vlogDebug("{0}.findAllStoresWithCoordinates - start", COMPONENT_NAME);

		List<RepositoryItem> result = null;
		RepositoryView view;
		try {
			view = getLocationRepository().getView(ITEM_DESCRIPTOR_STORE);
			RqlStatement statement = RqlStatement.parseRqlStatement("not ( stateAddress is null ) AND not (longitude is null) AND not (latitude is null) ORDER BY stateAddress SORT ASC, postalCode");
			Object params[] = new Object[0];
			RepositoryItem [] stores = statement.executeQuery (view, params);
			if (stores != null && stores.length > 0) {
				result = Arrays.asList(stores);
			}
		} catch (RepositoryException e) {
			if (isLoggingError())
				logError(e.getMessage());
		}

		vlogDebug("{0}.findAllStoresWithCoordinates - exit", COMPONENT_NAME);

		return result;
	}

	/**
	 * @param pPostalCode
	 * @return
	 */
	private List<RepositoryItem> findDistancesFromZip(String pPostalCode) {

		vlogDebug("{0}.findDistancesFromZip - start", COMPONENT_NAME);

		List<RepositoryItem> result = null;
		if (!StringUtils.isBlank(pPostalCode)){
			vlogDebug("Look for entries based on postalCode: {0}", pPostalCode);

			RepositoryView view;
			try {
				view = getDistanceRepository().getView(ITEM_DESCRIPTOR_ZIP_TO_STORE_DISTANCE);
				RqlStatement statement = RqlStatement.parseRqlStatement("postalCode = ?0");
				Object params[] = new Object[1];
				params[0] = pPostalCode;
				RepositoryItem [] distanceItems = statement.executeQuery (view, params);
				if (distanceItems != null && distanceItems.length > 0) {
					result = Arrays.asList(distanceItems);
				} else {
					vlogDebug("No stores found in repository based on postal code: {0}", pPostalCode);
				}
			} catch (RepositoryException e) {
				if (isLoggingError()) {
					logError(e.getMessage());
				}
			}
		}
		vlogDebug("{0}.findDistancesFromZip - exit", COMPONENT_NAME);

		return result;
	}

	/**
	 * @param map
	 * @return
	 */
	private Map sortByValue(Map map) {
	     List<Entry> list = new LinkedList<Entry>(map.entrySet());
	     Collections.sort(list, new Comparator() {
	          public int compare(Object o1, Object o2) {
	               return ((Comparable) ((Entry) (o1)).getValue())
	              .compareTo(((Entry) (o2)).getValue());
	          }
	     });

	    Map result = new LinkedHashMap();
		for (Entry entry : list) {
	        result.put(entry.getKey(), entry.getValue());
	    }
	    return result;
	}

    /**
     * @param pLocationId
     * @return
     */
    public RepositoryItem findStoreByLocationId(String pLocationId) {

		vlogDebug("Looking for store with location id: {0}.", pLocationId);

        String rqlQuery = new StringBuffer().append(LOCATION_ID).append("=?0").toString();
        Object[] params = new Object[1];
        params[0] = pLocationId;
        RepositoryItem[] items = null;
        RepositoryItem storeItem = null;
        try {
            RepositoryView view = getLocationRepository().getView(ITEM_DESCRIPTOR_STORE);
            RqlStatement rqlStatement = RqlStatement.parseRqlStatement(rqlQuery);
            items = rqlStatement.executeQuery(view, params);
            if(items != null && items.length > 0){
                storeItem = items[0];
            }

        } catch (RepositoryException re) {
            if (isLoggingError()) {
                logError(re);
            }
        }
        return storeItem;
    }
	
    /**
     * @param pStoreId
     * @return
     */
    public RepositoryItem findStoreById(String pStoreId) {

		vlogDebug("{0}.findStoreById - start", COMPONENT_NAME);

		vlogDebug("Looking for store with id: {0}", pStoreId);

        String rqlQuery = new StringBuffer().append(LOCATION_ID).append("=?0").toString();
        Object[] params = new Object[1];
        params[0] = pStoreId;
        RepositoryItem[] items = null;
        RepositoryItem storeItem = null;
        try {
            RepositoryView view = getLocationRepository().getView(ITEM_DESCRIPTOR_STORE);
            RqlStatement rqlStatement = RqlStatement.parseRqlStatement(rqlQuery);
            items = rqlStatement.executeQuery(view, params);
            if(items != null && items.length > 0){
                storeItem = items[0];
            }

        } catch (RepositoryException re) {
            if (isLoggingError()) {
                logError(re);
            }
        }

		vlogDebug("{0}.findStoreById - exit", COMPONENT_NAME);

        return storeItem;
    }
    
}