package com.cps.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.cps.userprofiling.CPSProfileTools;

import atg.commerce.profile.CommercePropertyManager;
import atg.droplet.GenericFormHandler;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

public class ProfileExportManager extends GenericFormHandler {

	private final static String ITEM_DESCRIPTOR_PATH = "/atg/userprofiling/ProfileAdapterRepository:b2b-user";
	private final static String TIME_FORMAT = "TIMEFORMAT=M/d/yyyy h:mm a";
	private final static String FILE_TIME_STAMP = "MM_dd_yyyy_HH_mm_ss";
	private final static String COLLECTION_FORMAT = "FORMAT=itemized";
	private final static String EXPORTED_FLAG = "exportedFlag";
	private final static String EXPORTED_DATE = "exportedDate";
	private final static String AMPERSAND = "&";
	private final static String COMMA = ",";
	private final static String IGNORE = " IGNORE";


	//Credit App 
	private final static String CREDIT_APP_HEADER = "Credit Application for Profile: ";
	private final static String CREDIT_APP = "CreditApp";
	private final static String CREDIT_APP_SHEET = "Credit App sheet";
	private final static String CREDIT_APP_ITEM_DESCRIPTOR = "creditApp";
	private final static String CREDIT_APP_QUERY_STMT = "userId = ?0";
	private final static String EMPTY_STRING = "";
	private final static String UNDERSCORE = "_";

	public void createSpreadsheetForExport(RepositoryItem[] rItems) {

		try {
			if (isLoggingDebug()) {
				logDebug("Begining write file.............");
			}

			HSSFWorkbook workbook = new HSSFWorkbook();
			Calendar date = Calendar.getInstance();
			Boolean hasCreditApp = false;
			Date d = new Date();
			date.setTime(d);
			Timestamp timeStamp = new Timestamp(d.getTime());
			try (FileOutputStream out = new FileOutputStream(getSpreadsheetName());) {
				HSSFName name;
				name = workbook.createName();
				name.setNameName("Profiles");
				HSSFSheet worksheet = workbook.createSheet("Profiles sheet");

				// Create Rows.
				HSSFRow row1 = worksheet.createRow(0);
				HSSFRow row2 = worksheet.createRow(1);
				row1.createCell(0).setCellValue(ITEM_DESCRIPTOR_PATH);
				row1.createCell(1).setCellValue(TIME_FORMAT);
				//row1.createCell(2).setCellValue(COLLECTION_FORMAT);

				//creating header row.
				createHeader(row2);

				int count = 2;

				for (RepositoryItem item : rItems) {
					HSSFRow itemRow = worksheet.createRow(count++);
					String id = item.getRepositoryId();
					hasCreditApp = (Boolean) item.getPropertyValue(CPSConstants.CREDIT_APPROVAL_REQUIRED);

					if (isLoggingDebug()) {
						logDebug("id............." + id);
						logDebug("id............." + id);
					}

					//creating row with cells for each profile
					String[] props = getStringProperties();
					if (props != null && props.length > 0) {
						for (int i = 0; i < props.length; i++) {
							Object propertyValue = item.getPropertyValue(props[i]);
							if (propertyValue instanceof String) {
								itemRow.createCell(i).setCellValue((String) propertyValue);
							} else if (propertyValue != null) {
								itemRow.createCell(i).setCellValue(propertyValue.toString());
							}

						}
					}
					String[] repItemsProps = getRepItemsProperties();
					if (repItemsProps != null && repItemsProps.length > 0) {
						for (int j = 0; j < repItemsProps.length; j++) {
							int k = j + getArrayLength(props);
							itemRow.createCell(k).setCellValue((RepositoryItem) item.getPropertyValue(repItemsProps[j]) == null ? EMPTY_STRING : ((RepositoryItem) item.getPropertyValue(repItemsProps[j])).getRepositoryId());
						}
					}
					String[] booleanProperties = getBooleanProperties();
					if (booleanProperties != null && booleanProperties.length > 0) {
						for (int l = 0; l < booleanProperties.length; l++) {
							int booleans = l + getArrayLength(props) + getArrayLength(repItemsProps);
							Object propValue = item.getPropertyValue(booleanProperties[l]);
							String value = EMPTY_STRING;
							if (propValue != null) {
								value = propValue.toString();
							}
							itemRow.createCell(booleans).setCellValue(value);
						}
					}

					String[] collectionItemsProperties = getCollectionItemsProperties();
					if (collectionItemsProperties != null && collectionItemsProperties.length > 0) {
						for (int m = 0; m < collectionItemsProperties.length; m++) {
							int collections = m + getArrayLength(props) + getArrayLength(repItemsProps) + getArrayLength(booleanProperties);
							StringBuilder iDs = new StringBuilder();
							Set<RepositoryItem> collection = (Set<RepositoryItem>) item.getPropertyValue(collectionItemsProperties[m]);
							if (collection != null) {
								for (RepositoryItem collItem : collection) {
									if (collItem != null) {
										String itemId = collItem.getRepositoryId();
										iDs.append(itemId).append(COMMA);
									}
								}
							}
							String result = EMPTY_STRING;
							if (iDs.length() > 0) {
								result = iDs.substring(0, iDs.length() - 1);
							}
							itemRow.createCell(collections).setCellValue(result);
						}
					}

					String[] stringPropertiesToIgnore = getStringPropertiesToIgnore();
					if (stringPropertiesToIgnore != null && stringPropertiesToIgnore.length > 0) {
						for (int p = 0; p < stringPropertiesToIgnore.length; p++) {
							int stringPropsToIgnore = p + getArrayLength(props) + getArrayLength(repItemsProps) + getArrayLength(booleanProperties) + getArrayLength(collectionItemsProperties);
							Object propToIgnoreValue = item.getPropertyValue(stringPropertiesToIgnore[p]);
							if (propToIgnoreValue instanceof String) {
								itemRow.createCell(stringPropsToIgnore).setCellValue((String) propToIgnoreValue);
							} else if (propToIgnoreValue != null) {
								itemRow.createCell(stringPropsToIgnore).setCellValue(propToIgnoreValue.toString());
							}
						}
					}

					String[] billAddrProperties = getProfileBillAddrProperties();
					RepositoryItem billAddrItem = (RepositoryItem) item.getPropertyValue(getPropertyManager().getBillingAddressPropertyName());
					if (billAddrProperties != null && billAddrProperties.length > 0) {
						for (int n = 0; n < billAddrProperties.length; n++) {
							int collections = n + getArrayLength(props) + getArrayLength(repItemsProps) + getArrayLength(booleanProperties) + getArrayLength(collectionItemsProperties)
									+ getArrayLength(stringPropertiesToIgnore);
							Object propertyValue = billAddrItem == null ? "" : billAddrItem.getPropertyValue(billAddrProperties[n]);
							if (propertyValue instanceof String) {
								itemRow.createCell(collections).setCellValue((String) propertyValue);
							} else if (propertyValue != null) {
								itemRow.createCell(collections).setCellValue(propertyValue.toString());
							}
						}
					}

					if (hasCreditApp != null && hasCreditApp) {
						RepositoryItem creditAppItem = lookupCreditApp(item.getRepositoryId());
						if (creditAppItem != null) {
							//create credit app spreadsheet
							createSpreadsheetForCreditApp(item, creditAppItem);
						}
					}

					//Setting flag on profile indicating that profile was exported
					try {
						getProfileTools().updateProperty(EXPORTED_FLAG, true, item);
						getProfileTools().updateProperty(EXPORTED_DATE, timeStamp, item);
					} catch (RepositoryException e) {
						logError(e);
					}
				}

				workbook.write(out);
				out.close();
			}
		} catch (FileNotFoundException e) {
			vlogError(e, "Error");
		} catch (IOException e) {
			vlogError(e, "Error");
		}
	}


	private String getSpreadsheetName() {
		SimpleDateFormat fileDateFormat = new SimpleDateFormat(FILE_TIME_STAMP);
		return new StringBuffer().append(getFilePath()).append(fileDateFormat.format(new Date())).append(getFileName()).toString();
	}

	private String getCreditAppSpreadsheetName(String id) {
		return new StringBuffer().append(getFilePath()).append(getCreditAppFileNamePrefix()).append(id).append(UNDERSCORE).append(getCreditAppFileName()).toString();
	}

	/**
	 * @param row2 creates header with properties names for profile spreadsheet
	 */
	public void createHeader(HSSFRow row2) {
		String[] props = getStringProperties();
		if (props != null && props.length > 0) {
			for (int i = 0; i < props.length; i++) {
				row2.createCell(i).setCellValue(props[i]);
			}
		}

		String[] repItemsProps = getRepItemsProperties();
		if (repItemsProps != null && repItemsProps.length > 0) {
			for (int j = 0; j < repItemsProps.length; j++) {
				int k = j + getArrayLength(props);
				row2.createCell(k).setCellValue(repItemsProps[j]);
			}
		}

		String[] booleanProperties = getBooleanProperties();
		if (booleanProperties != null && booleanProperties.length > 0) {
			for (int l = 0; l < booleanProperties.length; l++) {
				int booleans = l + getArrayLength(props) + getArrayLength(repItemsProps);
				row2.createCell(booleans).setCellValue(booleanProperties[l]);
			}
		}

		String[] collectionItemsProperties = getCollectionItemsProperties();
		if (collectionItemsProperties != null && collectionItemsProperties.length > 0) {
			for (int m = 0; m < collectionItemsProperties.length; m++) {
				int collections = m + getArrayLength(props) + getArrayLength(repItemsProps) + getArrayLength(booleanProperties);
				row2.createCell(collections).setCellValue(AMPERSAND + collectionItemsProperties[m]);
			}
		}

		String[] stringPropertiesToIgnore = getStringPropertiesToIgnore();
		if (stringPropertiesToIgnore != null && stringPropertiesToIgnore.length > 0) {
			for (int p = 0; p < stringPropertiesToIgnore.length; p++) {
				int stringPropsToIgnore = p + getArrayLength(props) + getArrayLength(repItemsProps) + getArrayLength(booleanProperties)
						+ getArrayLength(collectionItemsProperties);
				//row2.createCell(stringPropsToIgnore).setCellValue(stringPropertiesToIgnore[p]+IGNORE);
				row2.createCell(stringPropsToIgnore).setCellValue(stringPropertiesToIgnore[p]);
			}
		}

		String[] billAddrProperties = getProfileBillAddrProperties();
		if (billAddrProperties != null && billAddrProperties.length > 0) {
			for (int n = 0; n < billAddrProperties.length; n++) {
				int billAddr = n + getArrayLength(props) + getArrayLength(repItemsProps) + getArrayLength(booleanProperties)
						+ getArrayLength(collectionItemsProperties) + getArrayLength(stringPropertiesToIgnore);
				//row2.createCell(billAddr).setCellValue(billAddrProperties[n]+IGNORE);
				row2.createCell(billAddr).setCellValue(IGNORE);
			}
		}
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	public void createSpreadsheetForCreditApp(RepositoryItem profile, RepositoryItem creditAppItem) {
		try {
			if (isLoggingDebug()) {
				logDebug("Begining write file for credit app.............");
			}
			String profileId = profile.getRepositoryId();
			HSSFWorkbook workbook = new HSSFWorkbook();
			try (FileOutputStream out = new FileOutputStream(getCreditAppSpreadsheetName(profileId));) {

				HSSFName name;
				name = workbook.createName();
				name.setNameName(CREDIT_APP);
				HSSFSheet worksheet = workbook.createSheet(CREDIT_APP_SHEET);

				// Create Rows.
				HSSFRow row1 = worksheet.createRow(0);
				HSSFRow row2 = worksheet.createRow(1);
				row1.createCell(0).setCellValue(CREDIT_APP_HEADER + profileId);
				//creating header row.
				createCreditAppHeader(row2);
				int count = 2;
				//for (RepositoryItem item : rItems){
				HSSFRow itemRow = worksheet.createRow(count++);
				String id = profile.getRepositoryId();

				LinkedHashMap propMap = getCreditAppPropertiesMap();
				LinkedHashMap appBusinessAddress = getCreditAppBusinessAddress();
				LinkedHashMap appBillingAddress = getCreditAppBillingAddress();
				LinkedHashMap appPrincipal1Address = getCreditAppPrincipal1Address();
				LinkedHashMap appPrincipal2Address = getCreditAppPrincipal2Address();
				LinkedHashMap appReference1Address = getCreditAppReference1Address();
				LinkedHashMap appReference2Address = getCreditAppReference2Address();
				LinkedHashMap appBankAddress = getCreditAppBankAddress();
				//Map appProductUsed = getProductsUsed();
				LinkedHashMap appPlaceOrders = getCreditAppPlaceOrders();

				int cellNum = 0;

				cellNum = exportValues(propMap, cellNum, creditAppItem, itemRow);

				Map creditAppAddresses = (Map) creditAppItem.getPropertyValue(CPSConstants.CREDIT_APP_CONTACT_PROPERTY);
				if (creditAppAddresses != null) {
					RepositoryItem businessAddr = (RepositoryItem) creditAppAddresses.get(CPSConstants.CREDIT_APP_BUSINESS_ADDRESS);
					//if(businessAddr!=null){
					cellNum = exportValues(appBusinessAddress, cellNum, businessAddr, itemRow);
					//}

					RepositoryItem billingAddr = (RepositoryItem) creditAppAddresses.get(CPSConstants.CREDIT_APP_BILLING_ADDRESS);
					//if(billingAddr!=null){
					cellNum = exportValues(appBillingAddress, cellNum, billingAddr, itemRow);
					//}

					RepositoryItem pricipalAddr1 = (RepositoryItem) creditAppAddresses.get(CPSConstants.CREDIT_APP_PRINCIPAL_1);
					//if(pricipalAddr1!=null){
					cellNum = exportValues(appPrincipal1Address, cellNum, pricipalAddr1, itemRow);
					//}

					RepositoryItem pricipalAddr2 = (RepositoryItem) creditAppAddresses.get(CPSConstants.CREDIT_APP_PRINCIPAL_2);
					//if(pricipalAddr2!=null){
					cellNum = exportValues(appPrincipal2Address, cellNum, pricipalAddr2, itemRow);
					//}


					RepositoryItem reference1 = (RepositoryItem) creditAppAddresses.get(CPSConstants.CREDIT_APP_REFERENCE_1);
					//if(reference1!=null){
					cellNum = exportValues(appReference1Address, cellNum, reference1, itemRow);
					//}

					RepositoryItem reference2 = (RepositoryItem) creditAppAddresses.get(CPSConstants.CREDIT_APP_REFERENCE_2);
					//if(reference2!=null){
					cellNum = exportValues(appReference2Address, cellNum, reference2, itemRow);
					//}

					RepositoryItem bankAdd = (RepositoryItem) creditAppAddresses.get(CPSConstants.CREDIT_APP_BANK_ADDRESS);
					//if(bankAdd!=null){
					cellNum = exportValues(appBankAddress, cellNum, bankAdd, itemRow);
					//}

				}

				Set<String> productsUsed = (Set<String>) creditAppItem.getPropertyValue(CPSConstants.CREDIT_APP_PRODUCT_LIST_PROPERTY);
				cellNum++;
				if (productsUsed != null && !productsUsed.isEmpty()) {
					itemRow.createCell(cellNum).setCellValue(Arrays.toString(productsUsed.toArray()));
				}

				Set<RepositoryItem> buyers = (Set<RepositoryItem>) creditAppItem.getPropertyValue(CPSConstants.CREDIT_APP_ORDERER_PROPERTY);
				if (buyers != null && !buyers.isEmpty()) {
					int buyerNumber = 1;
					for (RepositoryItem buyer : buyers) {
						cellNum = exportValuesForBuyers(appPlaceOrders, cellNum, buyer, itemRow, buyerNumber);
						buyerNumber++;
					}
				}

				workbook.write(out);
				out.close();
			}
		} catch (FileNotFoundException e) {
			vlogError(e, "Error");
		} catch (IOException e) {
			vlogError(e, "Error");
		}
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	private int exportValues(LinkedHashMap propMap, int cellNum, RepositoryItem creditAppItem, HSSFRow itemRow) {
		Set propKeySet = propMap.keySet();
		Iterator<String> iter = propKeySet.iterator();
		while (iter.hasNext()) {
			String property = iter.next();
			Object propValue = creditAppItem == null ? EMPTY_STRING : creditAppItem.getPropertyValue(property);
			cellNum++;
			if (propValue != null) {
				if (propValue instanceof String) {
					itemRow.createCell(cellNum).setCellValue(propValue.toString());
				} else if (propValue instanceof Boolean) {
					itemRow.createCell(cellNum).setCellValue(((Boolean) propValue).booleanValue());
				} else {
					try {
						itemRow.createCell(cellNum).setCellValue(propValue.toString());
					} catch (Exception ex) {
						logError(ex);
					}
				}
			} else {
				itemRow.createCell(cellNum).setCellValue(EMPTY_STRING);
			}
		}
		return cellNum;
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	private int exportValuesForBuyers(LinkedHashMap propMap, int cellNum, RepositoryItem creditAppItem, HSSFRow itemRow, int buyerNumber) {
		Set propKeySet = propMap.keySet();
		Iterator<String> iter = propKeySet.iterator();
		while (iter.hasNext()) {
			String property = iter.next();
			Object propValue = creditAppItem.getPropertyValue(property);
			cellNum++;
			if (propValue != null) {
				if (propValue instanceof String) {
					itemRow.createCell(cellNum).setCellValue(propValue.toString());
				} else if (propValue instanceof Boolean) {
					itemRow.createCell(cellNum).setCellValue(((Boolean) propValue).booleanValue());
				}
			} else {
				itemRow.createCell(cellNum).setCellValue(EMPTY_STRING);
			}
		}
		return cellNum;
	}


	/**
	 * @param row2 creates header with properties names for profile spreadsheet
	 */
	@SuppressWarnings("rawtypes")
	public void createCreditAppHeader(HSSFRow row2) {
		LinkedHashMap propMap = getCreditAppPropertiesMap();
		LinkedHashMap appBusinessAddress = getCreditAppBusinessAddress();
		LinkedHashMap appBillingAddress = getCreditAppBillingAddress();
		LinkedHashMap appPrincipal1Address = getCreditAppPrincipal1Address();
		LinkedHashMap appPrincipal2Address = getCreditAppPrincipal2Address();
		LinkedHashMap appReference1Address = getCreditAppReference1Address();
		LinkedHashMap appReference2Address = getCreditAppReference2Address();
		LinkedHashMap appBankAddress = getCreditAppBankAddress();
		LinkedHashMap productsUsed = getProductsUsed();
		LinkedHashMap appPlaceOrders = getCreditAppPlaceOrders();

		int cellNum = 0;
		cellNum = addToHeader(cellNum, propMap, row2);
		cellNum = addToHeader(cellNum, appBusinessAddress, row2);
		cellNum = addToHeader(cellNum, appBillingAddress, row2);
		cellNum = addToHeader(cellNum, appPrincipal1Address, row2);
		cellNum = addToHeader(cellNum, appPrincipal2Address, row2);
		cellNum = addToHeader(cellNum, appReference1Address, row2);
		cellNum = addToHeader(cellNum, appReference2Address, row2);
		cellNum = addToHeader(cellNum, appBankAddress, row2);
		cellNum = addToHeader(cellNum, productsUsed, row2);
		cellNum = addToHeaderForBuyers(cellNum, appPlaceOrders, row2, "1 ");
		cellNum = addToHeaderForBuyers(cellNum, appPlaceOrders, row2, "2 ");
		cellNum = addToHeaderForBuyers(cellNum, appPlaceOrders, row2, "3 ");
		cellNum = addToHeaderForBuyers(cellNum, appPlaceOrders, row2, "4 ");

		if (isLoggingDebug()) {
			logDebug("createCreditAppHeader......END.....");
		}

	}

	private RepositoryItem lookupCreditApp(String profileId) {
		RepositoryView rpView;
		RepositoryItem creditApp = null;
		try {
			rpView = getCreditAppRepository().getView(CREDIT_APP_ITEM_DESCRIPTOR);
			RqlStatement statement = RqlStatement.parseRqlStatement(CREDIT_APP_QUERY_STMT);
			Object params[] = new Object[1];
			params[0] = profileId;
			RepositoryItem[] rItems = statement.executeQuery(rpView, params);
			if (rItems != null && rItems.length > 0) {
				creditApp = rItems[0];
			}
		} catch (RepositoryException e) {
			logError(e);
		}

		return creditApp;
	}

	@SuppressWarnings({"rawtypes", "unchecked"})
	private int addToHeader(int cellNum, LinkedHashMap properties, HSSFRow row) {

		Set<String> propertiesSet = properties.keySet();
		Collection propertiesValues = properties.values();
		Iterator<String> iter = propertiesSet.iterator();
		while (iter.hasNext()) {
			cellNum++;
			String property = iter.next();
			row.createCell(cellNum).setCellValue((String) properties.get(property));
		}

		return cellNum;
	}

	private int addToHeaderForBuyers(int cellNum, LinkedHashMap properties, HSSFRow row, String buyerNumber) {

		Set<String> propertiesSet = properties.keySet();
		Collection propertiesValues = properties.values();
		Iterator<String> iter = propertiesSet.iterator();

		while (iter.hasNext()) {
			cellNum++;
			String property = iter.next();
			row.createCell(cellNum).setCellValue(buyerNumber + (String) properties.get(property));
		}

		return cellNum;
	}

	private static int getArrayLength(Object[] array) {
		return array == null ? 0 : array.length;
	}

	private String fileName;
	private String filePath;
	private String[] stringProperties;
	private String[] stringPropertiesToIgnore;
	private String[] repItemsProperties;
	private String[] collectionItemsProperties;
	private String[] booleanProperties;
	private String[] profileBillAddrProperties;
	private CommercePropertyManager propertyManager;
	private CPSProfileTools profileTools;


	//Credit application related information
	private String creditAppFileName;
	private String creditAppFileNamePrefix;
	private LinkedHashMap<String, String> creditAppPropertiesMap;
	private LinkedHashMap<String, String> creditAppBusinessAddress;
	private LinkedHashMap<String, String> creditAppBillingAddress;
	private LinkedHashMap<String, String> creditAppPrincipal1Address;
	private LinkedHashMap<String, String> creditAppPrincipal2Address;
	private LinkedHashMap<String, String> creditAppReference1Address;
	private LinkedHashMap<String, String> creditAppReference2Address;
	private LinkedHashMap<String, String> creditAppBankAddress;
	private LinkedHashMap<String, String> creditAppPlaceOrders;
	private LinkedHashMap<String, String> productsUsed;
	private Repository creditAppRepository;

	public LinkedHashMap<String, String> getCreditAppBillingAddress() {
		return creditAppBillingAddress;
	}


	public void setCreditAppBillingAddress(
			LinkedHashMap<String, String> creditAppBillingAddress) {
		this.creditAppBillingAddress = creditAppBillingAddress;
	}


	public LinkedHashMap<String, String> getCreditAppPrincipal1Address() {
		return creditAppPrincipal1Address;
	}


	public void setCreditAppPrincipal1Address(
			LinkedHashMap<String, String> creditAppPrincipal1Address) {
		this.creditAppPrincipal1Address = creditAppPrincipal1Address;
	}


	public LinkedHashMap<String, String> getCreditAppPrincipal2Address() {
		return creditAppPrincipal2Address;
	}


	public void setCreditAppPrincipal2Address(
			LinkedHashMap<String, String> creditAppPrincipal2Address) {
		this.creditAppPrincipal2Address = creditAppPrincipal2Address;
	}


	public LinkedHashMap<String, String> getCreditAppReference1Address() {
		return creditAppReference1Address;
	}


	public void setCreditAppReference1Address(
			LinkedHashMap<String, String> creditAppReference1Address) {
		this.creditAppReference1Address = creditAppReference1Address;
	}


	public LinkedHashMap<String, String> getCreditAppReference2Address() {
		return creditAppReference2Address;
	}


	public void setCreditAppReference2Address(
			LinkedHashMap<String, String> creditAppReference2Address) {
		this.creditAppReference2Address = creditAppReference2Address;
	}


	public LinkedHashMap<String, String> getCreditAppPlaceOrders() {
		return creditAppPlaceOrders;
	}


	public void setCreditAppPlaceOrders(LinkedHashMap<String, String> creditAppPlaceOrders) {
		this.creditAppPlaceOrders = creditAppPlaceOrders;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public CommercePropertyManager getPropertyManager() {
		return propertyManager;
	}

	public void setPropertyManager(CommercePropertyManager propertyManager) {
		this.propertyManager = propertyManager;
	}

	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	public String[] getStringProperties() {
		return stringProperties;
	}

	public void setStringProperties(String[] stringProperties) {
		this.stringProperties = stringProperties;
	}

	public String[] getRepItemsProperties() {
		return repItemsProperties;
	}

	public void setRepItemsProperties(String[] repItemsProperties) {
		this.repItemsProperties = repItemsProperties;
	}

	public String[] getCollectionItemsProperties() {
		return collectionItemsProperties;
	}

	public void setCollectionItemsProperties(String[] collectionItemsProperties) {
		this.collectionItemsProperties = collectionItemsProperties;
	}

	public String[] getBooleanProperties() {
		return booleanProperties;
	}

	public void setBooleanProperties(String[] booleanProperties) {
		this.booleanProperties = booleanProperties;
	}

	public LinkedHashMap<String, String> getCreditAppPropertiesMap() {
		return creditAppPropertiesMap;
	}


	public void setCreditAppPropertiesMap(LinkedHashMap<String, String> creditAppPropertiesMap) {
		this.creditAppPropertiesMap = creditAppPropertiesMap;
	}


	public LinkedHashMap<String, String> getCreditAppBusinessAddress() {
		return creditAppBusinessAddress;
	}


	public void setCreditAppBusinessAddress(LinkedHashMap<String, String> creditAppBusinessAddress) {
		this.creditAppBusinessAddress = creditAppBusinessAddress;
	}


	public LinkedHashMap<String, String> getCreditAppBankAddress() {
		return creditAppBankAddress;
	}


	public void setCreditAppBankAddress(LinkedHashMap<String, String> creditAppBankAddress) {
		this.creditAppBankAddress = creditAppBankAddress;
	}


	public String getCreditAppFileName() {
		return creditAppFileName;
	}


	public void setCreditAppFileName(String creditAppFileName) {
		this.creditAppFileName = creditAppFileName;
	}

	public String getCreditAppFileNamePrefix() {
		return creditAppFileNamePrefix;
	}


	public void setCreditAppFileNamePrefix(String creditAppFileNamePrefix) {
		this.creditAppFileNamePrefix = creditAppFileNamePrefix;
	}


	public LinkedHashMap<String, String> getProductsUsed() {
		return productsUsed;
	}


	public void setProductsUsed(LinkedHashMap<String, String> productsUsed) {
		this.productsUsed = productsUsed;
	}


	public Repository getCreditAppRepository() {
		return creditAppRepository;
	}


	public void setCreditAppRepository(Repository creditAppRepository) {
		this.creditAppRepository = creditAppRepository;
	}


	public String[] getProfileBillAddrProperties() {
		return profileBillAddrProperties;
	}


	public void setProfileBillAddrProperties(String[] profileBillAddrProperties) {
		this.profileBillAddrProperties = profileBillAddrProperties;
	}


	public String[] getStringPropertiesToIgnore() {
		return stringPropertiesToIgnore;
	}


	public void setStringPropertiesToIgnore(String[] stringPropertiesToIgnore) {
		this.stringPropertiesToIgnore = stringPropertiesToIgnore;
	}

}