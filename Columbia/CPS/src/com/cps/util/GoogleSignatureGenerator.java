package com.cps.util;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * GoogleSignatureGenerator
 * 
 */
public class GoogleSignatureGenerator extends GenericService {

	/**
	 * component name for debugging
	 */
	private final static String COMPONENT_NAME = "/cps/util/GoogleSignatureGenerator";
	
	/**
	 * 
	 */
	private String mGoogleKey;
	
	/**
	 * @return
	 */
	public String getGoogleKey(){
		return mGoogleKey;
	}
	
	/**
	 * @param pGoogleKey
	 */
	public void setGoogleKey(String pGoogleKey){
		mGoogleKey = pGoogleKey;
	}

	/**
	 *
	 */
	private float mNearStoreDistance;

	/**
	 * @return
	 */
	public float getNearStoreDistance() {
		return mNearStoreDistance;
	}

	/**
	 * @param pNearStoreDistance
	 */
	public void setNearStoreDistance(float pNearStoreDistance) {
		mNearStoreDistance = pNearStoreDistance;
	}


	// This variable stores the binary key, which is computed from the string
	// (Base64) key
	private static byte[] key;

	/**
	 * 
	 */
	public GoogleSignatureGenerator() {
	}
	
	/* (non-Javadoc)
	 * @see atg.nucleus.GenericService#doStartService()
	 */
	public void doStartService() throws ServiceException {

		vlogDebug("{0}.doStartService - start", COMPONENT_NAME);

		// Convert the key from 'web safe' base 64 to binary
		if (StringUtils.isBlank(getGoogleKey())) {
			StringBuilder errorMessage = new StringBuilder().append("Google Key is not set for signature generator.  This is an exception that will stop the integration from working.");
			if (isLoggingError())
				logError(errorMessage.toString());
			throw new ServiceException(errorMessage.toString());
		}
		try {
			String keyString = getGoogleKey().replace('-', '+');
			keyString = keyString.replace('_', '/');
			this.key = Base64.decode(keyString);
		} catch (IOException ioe) {
			if (isLoggingError())
				logError(ioe);
			throw new ServiceException(ioe);
		}
	}
	
	/**
	 * @param pUrlString
	 * @return
	 * @throws java.io.IOException
	 * @throws java.security.InvalidKeyException
	 * @throws java.security.NoSuchAlgorithmException
	 * @throws java.net.URISyntaxException
	 */
	public String requestSignature(String pUrlString) throws IOException,
			InvalidKeyException, NoSuchAlgorithmException, URISyntaxException {

		vlogDebug("{0}.requestSignature - start", COMPONENT_NAME);

		// Convert the string to a URL so we can parse it
		URL url = new URL(pUrlString);

		// sign original request
		String request = signRequest(url.getPath(), url.getQuery());

		// build full signed url
		StringBuilder signedUrl = new StringBuilder().append(url.getProtocol())
				.append("://")
				.append(url.getHost())
				.append(request);

		vlogDebug("Signed URL : {0}", signedUrl);

		vlogDebug("{0}.requestSignature - exit", COMPONENT_NAME);

		return signedUrl.toString();
	}

	/**
	 * @param pPath
	 * @param pQuery
	 * @return
	 * @throws java.security.NoSuchAlgorithmException
	 * @throws java.io.UnsupportedEncodingException
	 * @throws java.net.URISyntaxException
	 * @throws java.security.InvalidKeyException
	 */
	private String signRequest(String pPath, String pQuery)
			throws NoSuchAlgorithmException,
			UnsupportedEncodingException, URISyntaxException, InvalidKeyException {

		if (isLoggingDebug()) { vlogDebug("{0}.signRequest - start", COMPONENT_NAME); }

		// Retrieve the proper URL components to sign
		String resource = pPath + '?' + pQuery;

		// Get an HMAC-SHA1 signing key from the raw key bytes
		SecretKeySpec sha1Key = new SecretKeySpec(key, "HmacSHA1");

		// Get an HMAC-SHA1 Mac instance and initialize it with the HMAC-SHA1
		// key
		Mac mac = Mac.getInstance("HmacSHA1");
		mac.init(sha1Key);

		// compute the binary signature for the request
		byte[] sigBytes = mac.doFinal(resource.getBytes());

		// base 64 encode the binary signature
		String signature = Base64.encodeBytes(sigBytes);

		if (!StringUtils.isBlank(signature)){
			// convert the signature to 'web safe' base 64
			signature = signature.replace('+', '-');
			signature = signature.replace('/', '_');
			resource += "&signature=" + signature;
		}

		if (isLoggingDebug()) { vlogDebug("{0}.signRequest - exit", COMPONENT_NAME); }

		return resource;
	}
}
