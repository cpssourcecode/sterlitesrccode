package com.cps.util;

import atg.core.util.Address;

public class CSAddress extends Address {
	private String addressId;

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

}
