package com.cps.util.validator;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.SharePageFormHandler;

import com.cps.userprofiling.validator.BaseValidator;

public class SharePageValidator extends BaseValidator {
	private final int MAX_RECIPIENTS_LENGTH = 1000;
	private final int MAX_NAME_LENGTH = 30;
	private final int MAX_EMAIL_LENGTH = 100;
	private final int MAX_MESSAGE_LENGTH = 250;

	public void validateRecipientInformation(String recipients, SharePageFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("SharePageValidator.validateRecipientInformation - start");

		if (StringUtils.isBlank(recipients)) {
			vlogDebug("SharePageValidator.validateRecipientInformation - recipient field empty");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_RECIPIENT, CPSConstants.SHARE_RECIPIENT, request);
		}
		else {
			// Validate max length
			if (recipients.length() > MAX_RECIPIENTS_LENGTH) {
				vlogDebug("SharePageValidator.validateRecipientInformation - recipient field exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_RECIPIENT, CPSConstants.SHARE_RECIPIENT, request);
				return;
			}

			// Validate recipient e-mail addresses individually
			String[] splitRecipients = recipients.split(",");

			for (String recipient : splitRecipients) {
				if (!StringUtils.isBlank(recipient) && !recipient.matches(CPSConstants.REG_EXP_EMAIL)) {
					vlogDebug("SharePageValidator.validateRecipientInformation - recipient <" + recipient + "> invalid");
					addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_RECIPIENT, CPSConstants.SHARE_RECIPIENT, request);
				}
			}
		}

		vlogDebug("SharePageValidator.validateRecipientInformation - end");
	}

	public void validateSharedUrl(String url, SharePageFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("SharePageValidator.validateSharedUrl - start");

		if (StringUtils.isBlank(url)) {
			vlogDebug("SharePageValidator.validateSharedUrl - shared URL empty");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_URL, "", request);
		}
		else if (!url.startsWith("/")) {
			vlogDebug("SharePageValidator.validateSharedUrl - shared URL <" + url + "> does not start with /");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_URL, "", request);
		}

		vlogDebug("SharePageValidator.validateSharedUrl - end");
	}

	public void validateMessageLength(String message, SharePageFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("SharePageValidator.validateMessageLength - start");

		if (!StringUtils.isBlank(message) && message.length() > MAX_MESSAGE_LENGTH) {
			vlogDebug("SharePageValidator.validateMessageLength - message exceeds max length");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_MESSAGE, CPSConstants.SHARE_MESSAGE, request);
		}

		vlogDebug("SharePageValidator.validateMessageLength - end");
	}

	public void validateSenderInformation(String firstName, String lastName, String emailAddress, SharePageFormHandler formHandler, DynamoHttpServletRequest request) {
		isValidFirstName(firstName, true, formHandler, request);
		isValidLastName(lastName, true, formHandler, request);
		isValidEmail(emailAddress, true, formHandler, request);
	}

	private void isValidEmail (String email, boolean isRequired, SharePageFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("SharePageValidator.isValidEmail - start");

		if (isRequired && StringUtils.isBlank(email)) {
			vlogDebug("SharePageValidator.isValidEmail - email missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_EMAIL, CPSConstants.SHARE_EMAIL_ADDRESS, request);
		}
		else if (!StringUtils.isBlank(email)) {
			if (email.length() > MAX_EMAIL_LENGTH) {
				vlogDebug("SharePageValidator.isValidEmail - email field exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_EMAIL, CPSConstants.SHARE_EMAIL_ADDRESS, request);
			}
			else if (!email.matches(CPSConstants.REG_EXP_EMAIL)) {
				vlogDebug("SharePageValidator.isValidEmail - email address <" + email + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_EMAIL, CPSConstants.SHARE_EMAIL_ADDRESS, request);
			}
		}

		vlogDebug("SharePageValidator.isValidEmail - end");
	}

	private void isValidFirstName(String firstName, boolean isRequired, SharePageFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("SharePageValidator.isValidFirstName - start");

		if (isRequired && StringUtils.isBlank(firstName)) {
			vlogDebug("SharePageValidator.isValidFirstName - first name missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_FIRST_NAME, CPSConstants.SHARE_FIRST_NAME, request);
		} else if (!StringUtils.isBlank(firstName)) {
			if (firstName.length() > MAX_NAME_LENGTH) {
				vlogDebug("SharePageValidator.isValidFirstName - first name exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_FIRST_NAME, CPSConstants.SHARE_FIRST_NAME, request);
			}
			else if (!firstName.matches(CPSConstants.REG_EXP_NAME)) {
				vlogDebug("SharePageValidator.isValidFirstName - first name <" + firstName + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_FIRST_NAME, CPSConstants.SHARE_FIRST_NAME, request);
			}
		}

		vlogDebug("SharePageValidator.isValidFirstName - end");
	}

	private void isValidLastName(String lastName, boolean isRequired, SharePageFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("SharePageValidator.isValidLastName - start");

		if (isRequired && StringUtils.isBlank(lastName)) {
			vlogDebug("SharePageValidator.isValidLastName - last name missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_LAST_NAME, CPSConstants.SHARE_LAST_NAME, request);
		} else if (!StringUtils.isBlank(lastName)) {
			if (lastName.length() > MAX_NAME_LENGTH) {
				vlogDebug("SharePageValidator.isValidLastName - last name exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_LAST_NAME, CPSConstants.SHARE_LAST_NAME, request);
			}
			else if (!lastName.matches(CPSConstants.REG_EXP_NAME)) {
				vlogDebug("SharePageValidator.isValidLastName - last name <" + lastName + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_LAST_NAME, CPSConstants.SHARE_LAST_NAME, request);
			}
		}

		vlogDebug("SharePageValidator.isValidLastName - start");
	}

}