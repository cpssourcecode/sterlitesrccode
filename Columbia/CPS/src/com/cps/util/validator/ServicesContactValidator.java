package com.cps.util.validator;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.ServicesContactFormHandler;

import com.cps.userprofiling.validator.BaseValidator;

public class ServicesContactValidator extends BaseValidator {
	private final int MAX_NAME_LENGTH = 30;
	private final int MAX_EMAIL_LENGTH = 100;
	private final int MAX_MESSAGE_LENGTH = 500;

	public void validateMessageLength(String message, ServicesContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("ServicesContactValidator.validateMessageLength - start");

		if (StringUtils.isBlank(message)) {
			vlogDebug("ServicesContactValidator.validateMessageLength - message is empty");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_MESSAGE, CPSConstants.SERVICES_CONTACT_MESSAGE, request);
		}
		else if (message.length() > MAX_MESSAGE_LENGTH) {
			vlogDebug("ServicesContactValidator.validateMessageLength - message exceeds max length");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_MESSAGE, CPSConstants.SERVICES_CONTACT_MESSAGE, request);
		}

		vlogDebug("ServicesContactValidator.validateMessageLength - end");
	}

	public void validateSenderInformation(String firstName, String lastName, String emailAddress, ServicesContactFormHandler formHandler, DynamoHttpServletRequest request) {
		isValidFirstName(firstName, true, formHandler, request);
		isValidLastName(lastName, true, formHandler, request);
		isValidEmail(emailAddress, true, formHandler, request);
	}

	private void isValidEmail (String email, boolean isRequired, ServicesContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("ServicesContactValidator.isValidEmail - start");

		if (isRequired && StringUtils.isBlank(email)) {
			vlogDebug("ServicesContactValidator.isValidEmail - email missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_EMAIL, CPSConstants.SERVICES_CONTACT_EMAIL_ADDRESS, request);
		}
		else if (!StringUtils.isBlank(email)) {
			if (email.length() > MAX_EMAIL_LENGTH) {
				vlogDebug("ServicesContactValidator.isValidEmail - email field exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_EMAIL, CPSConstants.SERVICES_CONTACT_EMAIL_ADDRESS, request);
			}
			else if (!email.matches(CPSConstants.REG_EXP_EMAIL)) {
				vlogDebug("ServicesContactValidator.isValidEmail - email address <" + email + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_EMAIL, CPSConstants.SERVICES_CONTACT_EMAIL_ADDRESS, request);
			}
		}

		vlogDebug("ServicesContactValidator.isValidEmail - end");
	}

	private void isValidFirstName(String firstName, boolean isRequired, ServicesContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("ServicesContactValidator.isValidFirstName - start");

		if (isRequired && StringUtils.isBlank(firstName)) {
			vlogDebug("ServicesContactValidator.isValidFirstName - first name missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_FIRST_NAME, CPSConstants.SERVICES_CONTACT_FIRST_NAME, request);
		} else if (!StringUtils.isBlank(firstName)) {
			if (firstName.length() > MAX_NAME_LENGTH) {
				vlogDebug("ServicesContactValidator.isValidFirstName - first name exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_FIRST_NAME, CPSConstants.SERVICES_CONTACT_FIRST_NAME, request);
			}
			else if (!firstName.matches(CPSConstants.REG_EXP_NAME)) {
				vlogDebug("ServicesContactValidator.isValidFirstName - first name <" + firstName + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_FIRST_NAME, CPSConstants.SERVICES_CONTACT_FIRST_NAME, request);
			}
		}

		vlogDebug("ServicesContactValidator.isValidFirstName - end");
	}

	private void isValidLastName(String lastName, boolean isRequired, ServicesContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("ServicesContactValidator.isValidLastName - start");

		if (isRequired && StringUtils.isBlank(lastName)) {
			vlogDebug("ServicesContactValidator.isValidLastName - last name missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_LAST_NAME, CPSConstants.SERVICES_CONTACT_LAST_NAME, request);
		} else if (!StringUtils.isBlank(lastName)) {
			if (lastName.length() > MAX_NAME_LENGTH) {
				vlogDebug("ServicesContactValidator.isValidLastName - last name exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_LAST_NAME, CPSConstants.SERVICES_CONTACT_LAST_NAME, request);
			}
			else if (!lastName.matches(CPSConstants.REG_EXP_NAME)) {
				vlogDebug("ServicesContactValidator.isValidLastName - last name <" + lastName + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_LAST_NAME, CPSConstants.SERVICES_CONTACT_LAST_NAME, request);
			}
		}

		vlogDebug("ServicesContactValidator.isValidLastName - start");
	}

}