package com.cps.util.validator;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.RegionalManagerContactFormHandler;

import com.cps.userprofiling.validator.BaseValidator;

public class RegionalManagerContactValidator extends BaseValidator {
	private final int MAX_NAME_LENGTH = 30;
	private final int MAX_EMAIL_LENGTH = 100;
	private final int MAX_MESSAGE_LENGTH = 500;

	public void validateMessageLength(String message, RegionalManagerContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("RegionalManagerContactValidator.validateMessageLength - start");

		if (StringUtils.isBlank(message)) {
			vlogDebug("RegionalManagerContactValidator.validateMessageLength - message is empty");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_MESSAGE, CPSConstants.REGIONAL_MANAGER_CONTACT_MESSAGE, request);
		}
		else if (message.length() > MAX_MESSAGE_LENGTH) {
			vlogDebug("RegionalManagerContactValidator.validateMessageLength - message exceeds max length");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_MESSAGE, CPSConstants.REGIONAL_MANAGER_CONTACT_MESSAGE, request);
		}

		vlogDebug("RegionalManagerContactValidator.validateMessageLength - end");
	}

	public void validateSenderInformation(String firstName, String lastName, String emailAddress, RegionalManagerContactFormHandler formHandler, DynamoHttpServletRequest request) {
		isValidFirstName(firstName, true, formHandler, request);
		isValidLastName(lastName, true, formHandler, request);
		isValidEmail(emailAddress, true, formHandler, request);
	}

	private void isValidEmail (String email, boolean isRequired, RegionalManagerContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("RegionalManagerContactValidator.isValidEmail - start");

		if (isRequired && StringUtils.isBlank(email)) {
			vlogDebug("RegionalManagerContactValidator.isValidEmail - email missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_EMAIL, CPSConstants.REGIONAL_MANAGER_CONTACT_EMAIL_ADDRESS, request);
		}
		else if (!StringUtils.isBlank(email)) {
			if (email.length() > MAX_EMAIL_LENGTH) {
				vlogDebug("RegionalManagerContactValidator.isValidEmail - email field exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_EMAIL, CPSConstants.REGIONAL_MANAGER_CONTACT_EMAIL_ADDRESS, request);
			}
			else if (!email.matches(CPSConstants.REG_EXP_EMAIL)) {
				vlogDebug("RegionalManagerContactValidator.isValidEmail - email address <" + email + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_EMAIL, CPSConstants.REGIONAL_MANAGER_CONTACT_EMAIL_ADDRESS, request);
			}
		}

		vlogDebug("RegionalManagerContactValidator.isValidEmail - end");
	}

	private void isValidFirstName(String firstName, boolean isRequired, RegionalManagerContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("RegionalManagerContactValidator.isValidFirstName - start");

		if (isRequired && StringUtils.isBlank(firstName)) {
			vlogDebug("RegionalManagerContactValidator.isValidFirstName - first name missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_FIRST_NAME, CPSConstants.REGIONAL_MANAGER_CONTACT_FIRST_NAME, request);
		} else if (!StringUtils.isBlank(firstName)) {
			if (firstName.length() > MAX_NAME_LENGTH) {
				vlogDebug("RegionalManagerContactValidator.isValidFirstName - first name exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_FIRST_NAME, CPSConstants.REGIONAL_MANAGER_CONTACT_FIRST_NAME, request);
			}
			else if (!firstName.matches(CPSConstants.REG_EXP_NAME)) {
				vlogDebug("RegionalManagerContactValidator.isValidFirstName - first name <" + firstName + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_FIRST_NAME, CPSConstants.REGIONAL_MANAGER_CONTACT_FIRST_NAME, request);
			}
		}

		vlogDebug("RegionalManagerContactValidator.isValidFirstName - end");
	}

	private void isValidLastName(String lastName, boolean isRequired, RegionalManagerContactFormHandler formHandler, DynamoHttpServletRequest request) {
		vlogDebug("RegionalManagerContactValidator.isValidLastName - start");

		if (isRequired && StringUtils.isBlank(lastName)) {
			vlogDebug("RegionalManagerContactValidator.isValidLastName - last name missing");
			addError(formHandler, CPSErrorCodes.ERR_SHARE_MISSING_LAST_NAME, CPSConstants.REGIONAL_MANAGER_CONTACT_LAST_NAME, request);
		} else if (!StringUtils.isBlank(lastName)) {
			if (lastName.length() > MAX_NAME_LENGTH) {
				vlogDebug("RegionalManagerContactValidator.isValidLastName - last name exceeds max length");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_LAST_NAME, CPSConstants.REGIONAL_MANAGER_CONTACT_LAST_NAME, request);
			}
			else if (!lastName.matches(CPSConstants.REG_EXP_NAME)) {
				vlogDebug("RegionalManagerContactValidator.isValidLastName - last name <" + lastName + "> does not match regular expression");
				addError(formHandler, CPSErrorCodes.ERR_SHARE_INVALID_LAST_NAME, CPSConstants.REGIONAL_MANAGER_CONTACT_LAST_NAME, request);
			}
		}

		vlogDebug("RegionalManagerContactValidator.isValidLastName - start");
	}

}