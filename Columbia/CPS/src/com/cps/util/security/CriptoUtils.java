package com.cps.util.security;

import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import sun.misc.CharacterDecoder;
import sun.misc.CharacterEncoder;
import sun.misc.UCDecoder;
import sun.misc.UCEncoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class CriptoUtils {

    private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(CriptoUtils.class);

    private static final String DEFAULT_AES_KEY = "7b*c&4e9!-@5$3%="; // 128 bit key
    private static final String DEFAULT_AES_INIT_VECTOR = "A6%6!_=++0JgtFd%"; // 16 bytes IV
    private static final String ENCODING_UTF_8 = "UTF-8";
    private static final String ALGORITHM_AES = "AES";
    private static final String AES_CBC_PKC_PADDING = "AES/CBC/PKCS5PADDING";

    private CriptoUtils(){}

    public static String aesEncrypt(String value) {
        return aesEncrypt(DEFAULT_AES_KEY, extractInitVector(DEFAULT_AES_KEY), value);
    }

    public static String aesEncrypt(String key, String value) {
        return aesEncrypt(DEFAULT_AES_KEY, extractInitVector(key), value);
    }

    public static String aesEncrypt(String key, String initVector, String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(ENCODING_UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(ENCODING_UTF_8), ALGORITHM_AES);

            Cipher cipher = Cipher.getInstance(AES_CBC_PKC_PADDING);
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            CharacterEncoder strEncoder = new UCEncoder();
            return strEncoder.encode(encrypted);
        } catch (Exception ex) {
            mLogging.logError(ex);
        }
        return "";
    }

    public static String aesDecrypt(String value) {
        return aesDecrypt(DEFAULT_AES_KEY, extractInitVector(DEFAULT_AES_KEY), value);
    }

    public static String aesDecrypt(String key, String value) {
        return aesDecrypt(DEFAULT_AES_KEY, extractInitVector(key), value);
    }

    public static String aesDecrypt(String key, String initVector, String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes(ENCODING_UTF_8));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(ENCODING_UTF_8), ALGORITHM_AES);

            Cipher cipher = Cipher.getInstance(AES_CBC_PKC_PADDING);
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            CharacterDecoder strDecoder = new UCDecoder();
            byte[] encoded = strDecoder.decodeBuffer(value);
            byte[] decoded = cipher.doFinal(encoded);
            return new String(decoded);
        } catch (Exception ex) {
            mLogging.logError(ex);
        }
        return "";
    }

    private static String extractInitVector(String key) {
        return StringUtils.isNotBlank(key) ? md5(key).substring(0, 16) : DEFAULT_AES_INIT_VECTOR;
    }

    public static String md5(String value){
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] passBytes = value.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i = 0; i < digested.length; i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            mLogging.logError(ex);
        }
        return "";
    }

    public static String sha1(String value){
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] passBytes = value.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i = 0; i < digested.length; i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            mLogging.logError(ex);
        }
        return "";
    }

}
