package com.cps.util;

import atg.adapter.gsa.GSARepository;
import atg.commerce.order.OrderHolder;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

import com.cps.commerce.gifts.CPSGiftlistManager;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.userprofiling.CPSProfileTools;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import static com.cps.util.CPSConstants.*;

public class MaterialListDownloadHandler extends GenericFormHandler {

	
	private static final String PRICE_DISCLAIMER_MESSAGE = "priceDisclaimerMessage";
	private String mGiftlistId;
	private GSARepository mGiftlistsRepository;
	private CPSGiftlistManager mGiftlistManager;
	private OrderHolder shoppingCart;
	private Profile profile;
	private CPSProfileTools profileTools;
	private boolean quoteAcknowldge;
	private String cIds;
	private List<String> cIdList;
	/**
	 * request monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	private void removeUnavailableItems(RepositoryItem pGiftList) {
		if (pGiftList != null) {
			List<RepositoryItem> gItems = (List) pGiftList.getPropertyValue(CPSConstants.GIFTLIST_ITEMS);
			if (gItems != null && !gItems.isEmpty()) {
				for (RepositoryItem gItem : gItems) {
					Boolean unavailable = (Boolean) gItem.getPropertyValue(CPSConstants.ECOMMERCE_UNAVAILABLE);
					if (unavailable == null) {
						unavailable = false;
					}

					String productId = (String) gItem.getPropertyValue(PRODUCT_ID);
					RepositoryItem catalogProduct = null;
					String stockingType = null;

					try {
						if (productId != null) {
							catalogProduct = getGiftlistManager().getCatalogTools().findProduct(productId);

							if (catalogProduct != null) {
								stockingType = (String) catalogProduct.getPropertyValue(STOCKING_TYPE);
							}
						}
					} catch (Exception e) {
						logError("Exception raised when trying to look up productId <" + productId + "> in catalog");
					}

					if (unavailable || (stockingType != null && isUnavailableByStocking(stockingType))) {
						try {
							//logDebug("removing item: " + gItem.getRepositoryId());
							getGiftlistManager().removeItemFromGiftlist(pGiftList.getRepositoryId(), gItem.getRepositoryId());
						} catch (Exception e) {
							if (isLoggingError()) {
								logError(e);
							}
						}
					}
				}
			}
		}
	}

	private boolean isUnavailableByStocking(String pStockingType){
		return CPSConstants.STOCKING_TYPE_OBSOLETE.equals(pStockingType)
				|| CPSConstants.STOCKING_TYPE_USEUP.equals(pStockingType)
				|| CPSConstants.STOCKING_TYPE_K.equals(pStockingType)
				|| CPSConstants.STOCKING_TYPE_X.equals(pStockingType)
				|| CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR.equals(pStockingType);
	}
public void lookupCids(){
		if (!StringUtils.isBlank(getcIds())) {
			String[] userIds = getcIds().split(",");
			List<String> ids=new ArrayList<String>();
			for (String id : userIds) {
				ids.add(id);
			}
			setcIdList(ids);
		}
	}

	public boolean handleCreateSpreadsheet(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("handleCreateSpreadsheet.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListDownloadHandler.handleCreateSpreadsheet";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (!StringUtils.isBlank(getGiftlistId()) || isQuoteAcknowldge()) {
					RepositoryItem giftList = null;
					CPSGiftlistManager giftlistManager = getGiftlistManager();
					String giftListName = null;
					String priceDisclaimerMessage=getPriceDisclaimerMessage(pRequest);
					if(!isQuoteAcknowldge()){
						giftList = getRepositoryItem(getGiftlistId(), GIFT_LIST_ITEM_TYPE);
						giftListName = giftlistManager.getGiftlistFileName(giftList);
						// Remove items that are not currently available
						removeUnavailableItems(giftList);
						setcIdList(null);
					}else{
						lookupCids();
					}
					
					HSSFWorkbook workbook = giftlistManager.createWorkbook(giftList, giftListName, getProfile(),
					getProfileTools(), (CPSOrderImpl) getShoppingCart().getCurrent(),priceDisclaimerMessage,getcIdList());
					if (giftListName == null) {
						giftListName=CPSConstants.QUOTE_ACKNOWLEDGEMENT;
					}
					if (null != workbook) {
							vlogDebug("Write file");
							if (FILE_EXT_XLS.equals(giftlistManager.getFileExt())) {
								pResponse.setContentType("application/vnd.ms-excel");
								pResponse.setHeader("Content-Disposition", "download;filename=\"" + giftListName + FILE_EXT_XLS + "\"");
								ServletOutputStream out = pResponse.getOutputStream();
								workbook.write(out);
								out.close();
							} else if (FILE_EXT_CSV.equals(giftlistManager.getFileExt())) {
								pResponse.setContentType("application/vnd.ms-excel");
								pResponse.setHeader("Content-Disposition", "download;filename=\"" + giftListName + FILE_EXT_CSV + "\"");
								ServletOutputStream out = pResponse.getOutputStream();
								ArrayList<ArrayList<String>> csvData = PoiUtils.convertToCSV(workbook);
								try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out);) {
									BufferedWriter bw = new BufferedWriter(outputStreamWriter);
									giftlistManager.writeCsvFile(bw, csvData);
								}
							}
						}
					
				} else {
					vlogDebug("No data to write");
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}

	/**
	 * To get price message from messages repository
	 * @param pRequest
	 */
	private String getPriceDisclaimerMessage(DynamoHttpServletRequest pRequest) {
		GSARepository messageRepository = (GSARepository) CPSMessageUtils.getRepositoryBundle(CPSConstants.ERR_MESSAGES_REPOSITORY, pRequest.getLocale());
		String message = CPSMessageUtils.getMessageText(messageRepository, PRICE_DISCLAIMER_MESSAGE);
		return message;
	}

	private RepositoryItem getRepositoryItem(String pRepItemId, String pRepItemType) {
		RepositoryItem repItem = null;
		try {
			if (!StringUtils.isBlank(pRepItemId)) {
				repItem = getGiftlistsRepository().getItem(pRepItemId, pRepItemType);
			}
		} catch (RepositoryException re) {
			vlogError(re, "getRepositoryItem() RepositoryException");
		}
		return repItem;
	}

	// Getters and Setters

	/**
	 * Sets new mGiftlistId.
	 *
	 * @param pGiftlistId New value of mGiftlistId.
	 */
	public void setGiftlistId(String pGiftlistId) {
		mGiftlistId = pGiftlistId;
	}


	/**
	 * Gets mGiftlistId.
	 *
	 * @return Value of mGiftlistId.
	 */
	public String getGiftlistId() {
		return mGiftlistId;
	}


	/**
	 * Gets mGiftlistsRepository.
	 *
	 * @return Value of mGiftlistsRepository.
	 */
	public GSARepository getGiftlistsRepository() {
		return mGiftlistsRepository;
	}

	/**
	 * Sets new mGiftlistsRepository.
	 *
	 * @param pGiftlistsRepository New value of mGiftlistsRepository.
	 */
	public void setGiftlistsRepository(GSARepository pGiftlistsRepository) {
		mGiftlistsRepository = pGiftlistsRepository;
	}

	public CPSGiftlistManager getGiftlistManager() {
		return mGiftlistManager;
	}

	public void setGiftlistManager(CPSGiftlistManager pGiftlistManager) {
		mGiftlistManager = pGiftlistManager;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	/**
	 * @return the shoppingCart
	 */
	public OrderHolder getShoppingCart() {
		return shoppingCart;
	}

	/**
	 * @param shoppingCart the shoppingCart to set
	 */
	public void setShoppingCart(OrderHolder shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	/**
	 * @return the profile
	 */
	public Profile getProfile() {
		return profile;
	}

	/**
	 * @param profile the profile to set
	 */
	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	/**
	 * @return the profileTools
	 */
	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	/**
	 * @param profileTools the profileTools to set
	 */
	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	public boolean isQuoteAcknowldge() {
		return quoteAcknowldge;
	}

	public void setQuoteAcknowldge(boolean quoteAcknowldge) {
		this.quoteAcknowldge = quoteAcknowldge;
	}

	public String getcIds() {
		return cIds;
	}

	public void setcIds(String cIds) {
		this.cIds = cIds;
	}

	public List<String> getcIdList() {
		return cIdList;
	}

	public void setcIdList(List<String> cIdList) {
		this.cIdList = cIdList;
	}
	
}
