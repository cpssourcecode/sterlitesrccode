package com.cps.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cps.email.CommonEmailSender;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.UploadedFile;
import atg.userprofiling.Profile;

public class RequestAQuoteHandler extends GenericFormHandler {
    /**
     * E-mail sender (see property file)
     */
    private CommonEmailSender commonEmailSender;
    /**
     * Include profile
     */
    private Profile profile;
    /**
     * Request a Quote upload success url string
     */
    private String requestAQuoteSuccessURL;
    /**
     * Request a Quote upload fail url string
     */
    private String requestAQuoteFailURL;
    /**
     * File to read in for request a quote
     */
    private Object requestQuoteFile;
    /**
     * String (JSON) containing personal info from JSP
     */
    private String personalInfoJSON;

    /**
     * String (JSON) containing all data rows info
     */
    private String quoteInfoJSON;

    /**
     * request monitor
     */
    private RepeatingRequestMonitor mRepeatingRequestMonitor;
    private CPSGlobalProperties cpsGlobalProperties;
    boolean isFileError = false;
    

    // Acceptable file extensions .xls, .xlsx .doc, pdf, .xls, .jpg, .png, .tif,
    // or .gif.
    private static final String XLS_EXTENSION = "xls";
    private static final String XLSX_EXTENSION = "xlsx";
    private static final String DOC_EXTENSION = "doc";
    private static final String DOCX_EXTENSION = "docx";
    private static final String PDF_EXTENSION = "pdf";
    private static final String JPG_EXTENSION = "jpg";
    private static final String PNG_EXTENSION = "png";
    private static final String TIF_EXTENSION = "tif";
    private static final String GIF_EXTENSION = "gif";

    private static final String DEFAULT_ATTACHMENT_NAME = "attachment.";
    private static final String SHEET_NAME = "Quote Info";

    private static final String PARAM_YOUR_PART = "yourPart";
    private static final String PARAM_CPS_PART = "cpsPart";
    private static final String PARAM_QTY = "qty";
    private static final String HEADER_CHECK_USER = "First Name";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_SIZE = "size";
    private static final String PARAM_BRAND = "brand";
    private static final String PARAM_COMMENTS = "comments";
    /**
     * Method to handle Request a QUote Without Attachment (Manual quote
     * information input)
     * 
     * @param pRequest
     * @param pResponse
     * @return
     * @throws IOException
     * @throws ServletException
     */
    public boolean handleRequestAQuote(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        vlogDebug("handleRequestAQuote.start");
        vlogDebug("JSON: " + getQuoteInfoJSON());
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "RequestAQuoteHandler.handleRequestAQuote";

        if ((rrm == null) || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {            	
            	//Form validation in pre-method  
            	preRequestAQuote(pRequest, pResponse);
                if (!getFormError()) {

            		UploadedFile uploadedFile = null;
            		UploadedFile[] uploadedFiles = null;
                	// Getting input quote parameters as map
                    Map<String, Object> quoteInfoMap = getQuoteInfoMap();
                    File[] attachments = new File[0];
                    boolean isEmptyRow=false;
                    
                    if (getRequestQuoteFile() instanceof UploadedFile[]) {
                      //Reading in UploadedFiles                      
                      uploadedFiles = (UploadedFile[]) getRequestQuoteFile();
                      File destFile = null;
                      for (int i=0; i<uploadedFiles.length;i++) {
                    	  if (uploadedFiles[i] != null && uploadedFiles[i].getInputFile() != null) {
      						destFile = new File(uploadedFiles[i].getFilename());
      						copyInputStreamToFile(uploadedFiles[i].getInputStream(), destFile);
      						attachments = Arrays.copyOf(attachments, i + 1);
  							attachments[i] = destFile;      						
      					}
                      }
                    }else if (getRequestQuoteFile() instanceof UploadedFile){
                    	//Reading in UploadedFile
                    	uploadedFile = (UploadedFile) getRequestQuoteFile();
    					if (uploadedFile != null && uploadedFile.getInputFile() != null) {
    						File destFile = new File(uploadedFile.getFilename());
    						copyInputStreamToFile(uploadedFile.getInputStream(), destFile);
    						if (attachments == null || attachments.length == 0) {
    							attachments = Arrays.copyOf(attachments, attachments.length + 1);
    							attachments[attachments.length - 1] = destFile;
    						}
    					}
    					
                    } else {
                    	vlogError("RequestAQuoteHandler received an Object which is neither an UploadedFile or an UploadedFile[].");
                    }
                    
                    //Reading and validating rows
                    isEmptyRow = ValidateEmptyRow(quoteInfoMap);
                    if(!isEmptyRow){
                    	attachments = Arrays.copyOf(attachments, attachments.length+1);
                		// Creating XLSX file from Map with input parameters
                		attachments[attachments.length-1] = createFileFromRows(quoteInfoMap);
                    }
                    int totalFilesUploaded = attachments.length;
                    if(totalFilesUploaded<=getCpsGlobalProperties().getMaximumAttachedFiles()){
                        // Sending an email
                        getCommonEmailSender().sendQuoteRequestedEmail(getProfile(), getPersonalInfoMap(), attachments);
                    }else{
                    	//create an error - attached files cannot be exceeded more than given file size
                    	createError(CPSErrorCodes.ERR_REQUEST_QUOTE_FILE_LENGTH, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
                    }                    
                }
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        vlogDebug("handleRequestAquote.end");
        return checkFormRedirect(getRequestAQuoteSuccessURL(), getRequestAQuoteFailURL(), pRequest, pResponse);
    }
    
    /**
     * 
     * @param pRequest
     * @param pResponse
     * @throws ServletException
     */
    private void preRequestAQuote(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException {
    	
		UploadedFile[] uploadedFiles = null;		
		if (getRequestQuoteFile() instanceof UploadedFile[]) {
            uploadedFiles = (UploadedFile[]) getRequestQuoteFile();
            int uploadedFileSize=0;
            //validate maximum uploaded file
            if(uploadedFiles.length>getCpsGlobalProperties().getMaximumAttachedFiles()){
//            	create an error - Uploaded file length cannot be exceeded more than given file size
            	vlogDebug("Uploaded Files cannot be exceeded more than {0} files.",getCpsGlobalProperties().getMaximumAttachedFiles());
            	createError(CPSErrorCodes.ERR_REQUEST_QUOTE_FILE_LENGTH, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
            }
            for (UploadedFile upFile : uploadedFiles) {
				uploadedFileSize=uploadedFileSize+ upFile.getFileSize();
				validateUploadedFile(pRequest, upFile);
				if(isFileError()){
					break;
				}
			}
            //Limit of uploaded file size
            if(uploadedFileSize>getCpsGlobalProperties().getLimitOfUploadedFileSize()){
//            	create an error - Uploaded file size cannot be exceeded more than given file's size 
            	vlogDebug("File size cannot be exceeded more than {0} bytes",getCpsGlobalProperties().getLimitOfUploadedFileSize());
            	createError(CPSErrorCodes.ERR_REQUEST_QUOTE_FILE_SIZE, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
            }         
         } else if(getRequestQuoteFile() instanceof UploadedFile) {
        	 //Validation for only one file uploaded
        	 validateUploadedFile(pRequest, (UploadedFile) getRequestQuoteFile());
         } else {
        	 vlogError("RequestAQuoteHandler received a NULL file.");
         }
		
    }

	/**
	 * @param pRequest
	 * @param uploadedFile
	 */
	private void validateUploadedFile(DynamoHttpServletRequest pRequest, UploadedFile uploadedFile) {
		boolean isFileEmpty = false;
		boolean isRowEmpty = false;
		boolean isValidRow = true;
		if (uploadedFile.getFilename()!=null && !uploadedFile.getFilename().isEmpty()) {
		    // Check file format
		    isFileEmpty = isEmptyFileUploaded(uploadedFile);
		    if(!checkFileExtensionForRequestAQuote(uploadedFile)){
		    	// Not acceptable file extension, add error
		        createError(CPSErrorCodes.ERR_REQUEST_QUOTE_FILE_FORMAT, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
		        isFileError = true;
		    } else if (isFileEmpty) {
		        // Empty File has been uploaded
		        vlogDebug("Empty file has been uploaded.");
		        createError(CPSErrorCodes.ERR_REQUEST_QUOTE_EMPTY_FILE, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
		        isFileError = true;
		    } /*else {
		    	isValidRow = validateIncorrectRows(getQuoteInfoMap());
		    	if(!isValidRow){
		    		//error for incorrect rows
		    		createError(CPSErrorCodes.ERR_REQUEST_QUOTE_INCORRECT_ROWS, null, pRequest.getLocale());
		    		isFileError = true;
		    	}
		    }*/
		} else {
			//validation for incorrect and empty row
			//isValidRow = validateIncorrectRows(getQuoteInfoMap());
			isRowEmpty = ValidateEmptyRow(getQuoteInfoMap());
			/*if(!isValidRow){
				//error for incorrect rows
				createError(CPSErrorCodes.ERR_REQUEST_QUOTE_INCORRECT_ROWS, null, pRequest.getLocale());
			}*/
		    if ((uploadedFile == null || uploadedFile.getInputFile() == null && isRowEmpty)) {
		    	//error for no file uploaded
		    	createError(CPSErrorCodes.ERR_NO_FILE, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
			}
		}
	}
    
    /**
     * Method to validate whether the row is empty or not
     * 
     * @param quoteInfoMap
     * @return
     */
    private boolean ValidateEmptyRow(Map<String, Object> quoteInfoMap) {
    	boolean isEmptyRow = false;
    	int count = 0;
        for (Entry<String, Object> entry : getQuoteInfoMap().entrySet()) {            
            HashMap<String, String> rowMap = (HashMap<String, String>) entry.getValue();
            boolean isQtyEmpty = rowMap.get(PARAM_QTY).isEmpty();
            boolean isCpsPartEmpty = rowMap.get(PARAM_CPS_PART).isEmpty();
            boolean isYourPartEmpty = rowMap.get(PARAM_YOUR_PART).isEmpty();
            boolean isDescriptionEmpty = rowMap.get(PARAM_DESCRIPTION).isEmpty();
            boolean isSizeEmpty = rowMap.get(PARAM_SIZE).isEmpty();
            boolean isBrandEmpty = rowMap.get(PARAM_BRAND).isEmpty();
            boolean isCommentsEmpty = rowMap.get(PARAM_COMMENTS).isEmpty();
            // If row is empty - increase count
            if (isQtyEmpty && isCpsPartEmpty && isYourPartEmpty && isDescriptionEmpty && isSizeEmpty && isBrandEmpty && isCommentsEmpty) {
            	count++;
            }
        }
        if (count == getQuoteInfoMap().size()) {
        	isEmptyRow = true;
        }
		return isEmptyRow;
	}

    /**
     * Method to validate the incorrect rows 
     * 
     * @param mapWithRows
     * @return
     */
	private boolean validateIncorrectRows(Map<String, Object> mapWithRows) {    	
		boolean isValidRow = true;    	
        for (Entry<String, Object> entry : mapWithRows.entrySet()) {
            // Get the inner Map<TDNAME:TDVALUE>
            HashMap<String, String> rowMap = (HashMap<String, String>) entry.getValue();
            boolean isQtyEmpty = rowMap.get(PARAM_QTY).isEmpty();
            boolean isCpsPartEmpty = rowMap.get(PARAM_CPS_PART).isEmpty();
            boolean isYourPartEmpty = rowMap.get(PARAM_YOUR_PART).isEmpty();
            boolean isDescriptionEmpty = rowMap.get(PARAM_DESCRIPTION).isEmpty();
            // If qty given but no number or number given but not qty - adding
            // error
            if ((!isQtyEmpty && (isCpsPartEmpty && isYourPartEmpty && isDescriptionEmpty)) || (isQtyEmpty && (!isCpsPartEmpty || !isYourPartEmpty || !isDescriptionEmpty))) {
            	isValidRow = false;
            } 
        }
		return isValidRow;
    }

    /**
     * Method to handle request a quote with file provided
     * 
     * @param pRequest
     * @param pResponse
     * @return
     * @throws IOException
     * @throws ServletException
     */
    public boolean handleRequestAQuoteWithFile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        vlogDebug("handleRequestAQuoteWithFile.start");
        // Get the file
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "RequestAQuoteHandler.handleRequestAQuoteWithFile";

        if ((rrm == null) || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                UploadedFile uploadedFile = (UploadedFile) getRequestQuoteFile();
                vlogDebug("File: " + uploadedFile);
                boolean isFileEmpty = false;
                // Process upload
                if ((uploadedFile != null) && (uploadedFile.getInputFile() != null) && uploadedFile.getInputFile().exists()) {
                    // Check file format
                    isFileEmpty = isEmptyFileUploaded(uploadedFile);
                    if (checkFileExtensionForRequestAQuote(uploadedFile) && !isFileEmpty) {
                        // send email with file attached and body message
                        File[] attachments = new File[1];
                        String filename = uploadedFile.getFilename();
                        String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
                        File destFile = new File(DEFAULT_ATTACHMENT_NAME + extension);
                        copyInputStreamToFile(uploadedFile.getInputStream(), destFile);
                        attachments[0] = destFile;
                        getCommonEmailSender().sendQuoteRequestedEmail(getProfile(), getPersonalInfoMap(), attachments);
                    } else {
                        if (isFileEmpty) {
                            // Empty File has been uploaded
                            vlogDebug("Empty file has been uploaded.");
                            createError(CPSErrorCodes.ERR_REQUEST_QUOTE_EMPTY_FILE, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
                        } else {
                            // Not acceptable file extension, add error
                            createError(CPSErrorCodes.ERR_REQUEST_QUOTE_FILE_FORMAT, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());
                        }
                    }
                } else {
                    // No file given, create error
                    createError(CPSErrorCodes.ERR_NO_FILE, CPSConstants.IMPORT_FIELD_ID, pRequest.getLocale());

                }
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        vlogDebug("handleRequestAquoteWithFile.end");
        return checkFormRedirect(getRequestAQuoteSuccessURL(), getRequestAQuoteFailURL(), pRequest, pResponse);
    }

    /**
     * Method to validate empty file uploaded or not
     * 
     * @param uploadedFile
     * @return
     */
    private boolean isEmptyFileUploaded(UploadedFile uploadedFile) {
        boolean isEmptyFileUploaded = false;
        try {
            String filename = uploadedFile.getFilename();
            String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
            if (XLS_EXTENSION.equalsIgnoreCase(extension) || XLSX_EXTENSION.equalsIgnoreCase(extension)) {
                Workbook wb = WorkbookFactory.create(uploadedFile.getInputStream());
                Sheet sheet = wb.getSheetAt(0);
                if (sheet.getRow(sheet.getLastRowNum()) == null) {
                    vlogDebug("There is empty row in the excel file");
                    isEmptyFileUploaded = true;
                    return isEmptyFileUploaded;
                }
            }
            if (DOC_EXTENSION.equalsIgnoreCase(extension)) {
                HWPFDocument document = new HWPFDocument(uploadedFile.getInputStream());
                WordExtractor extractor = new WordExtractor(document);
                String[] fileData = extractor.getParagraphText();
                for (String element : fileData) {
                    if (element != null) {
                        isEmptyFileUploaded = element.trim().isEmpty();
                    }
                }
                return isEmptyFileUploaded;
            }

        } catch (InvalidFormatException e) {
            vlogError("Invalid Format Exception  {0}", e.getMessage());
        } catch (IOException e) {
            vlogError("IOException {0}", e.getMessage());
        }
        return isEmptyFileUploaded;

    }

    /**
     * Method to Get an input JSON String as A HashMap with following form: HashMap<ROW#, HashMap<TD_NAME, TD_VALUE>>
     * 
     * @return
     */
    private Map<String, Object> getQuoteInfoMap() {
        Map<String, Object> rowsMap = new HashMap<>();
        try {
            // Parsing string with JSON to JSONObject
            JSONObject rowsJSONObject = new JSONObject(getQuoteInfoJSON());
            // Parsing JSONObject to map
            rowsMap = jsonToMap(rowsJSONObject);
        } catch (JSONException e) {
            vlogError(e.getMessage());
        }
        return rowsMap;
    }

    /**
     * Method to create XLSX file from a given Map
     * 
     * @param mapWithRows
     * @return
     */
    @SuppressWarnings("unchecked")
    private File createFileFromRows(Map<String, Object> mapWithRows) {
        // Create blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        // Create a blank sheet
        XSSFSheet spreadsheet = workbook.createSheet(SHEET_NAME);
        // Create row object
        XSSFRow row;
        int rowCount = 0;
        // Loop thru Map(ROW# : Map<TDNAME:TDVALUE>)
        for (Entry<String, Object> entry : mapWithRows.entrySet()) {
            // Get the inner Map<TDNAME:TDVALUE>
            HashMap<String, Object> mapRow = (HashMap<String, Object>) entry.getValue();
            // get row names
            if (rowCount == 0) {
                row = spreadsheet.createRow(rowCount);
                int titleCellCount = 0;
                for (Entry<String, Object> rowEntry : mapRow.entrySet()) {
                    XSSFCell cell = row.createCell(titleCellCount++);
                    cell.setCellValue(rowEntry.getKey());
                }
                rowCount++;
            }
            row = spreadsheet.createRow(rowCount);
            int cellCount = 0;
            // Populate row with data
            for (Entry<String, Object> rowEntry : mapRow.entrySet()) {
                XSSFCell cell = row.createCell(cellCount++);
                cell.setCellValue((String) rowEntry.getValue());
            }
            rowCount++;
        }

        File destFile = new File(DEFAULT_ATTACHMENT_NAME + XLSX_EXTENSION);
        try (FileOutputStream output = FileUtils.openOutputStream(destFile);) {
            workbook.write(output);
            output.close();
        } catch (IOException e) {
            vlogError(e.getMessage());
        }
        vlogDebug("File written succesfully");

        return destFile;
    }

    /**
     * Method to retrieve personal information from JSON String
     * 
     * @return
     */
    private Map<String, Object> getPersonalInfoMap() {
        Map<String, Object> personalInfoMap = new HashMap<String, Object>();
        try {
            JSONObject valuesJSONObject = new JSONObject(getPersonalInfoJSON());
            personalInfoMap = jsonToMap(valuesJSONObject);
        } catch (JSONException e) {
            vlogError(e.getMessage());
        }
        return personalInfoMap;
    }

    protected boolean checkFileExtensionForRequestAQuote(UploadedFile file) {
        String filename = file.getFilename();
        String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
        return XLS_EXTENSION.equalsIgnoreCase(extension) || XLSX_EXTENSION.equalsIgnoreCase(extension) || DOC_EXTENSION.equalsIgnoreCase(extension) || DOCX_EXTENSION.equalsIgnoreCase(extension)
                        || PDF_EXTENSION.equalsIgnoreCase(extension) || JPG_EXTENSION.equalsIgnoreCase(extension) || PNG_EXTENSION.equalsIgnoreCase(extension) || TIF_EXTENSION.equalsIgnoreCase(extension)
                        || GIF_EXTENSION.equalsIgnoreCase(extension);
    }

    /**
     * Method to validate rows from quote request form (If QTY given - YOUR PART
     * or CPS PART should be provided and visa versa)
     * 
     * @param mapWithRows
     * @param pRequest
     */
    @SuppressWarnings("unchecked")
    private void validateRowMap(Map<String, Object> mapWithRows, DynamoHttpServletRequest pRequest) {
        // Count of empty rows
        int count = 0;
        for (Entry<String, Object> entry : mapWithRows.entrySet()) {
            // Get the inner Map<TDNAME:TDVALUE>
            HashMap<String, String> rowMap = (HashMap<String, String>) entry.getValue();
            boolean isQtyEmpty = rowMap.get(PARAM_QTY).isEmpty();
            boolean isCpsPartEmpty = rowMap.get(PARAM_CPS_PART).isEmpty();
            boolean isYourPartEmpty = rowMap.get(PARAM_YOUR_PART).isEmpty();
            boolean isDescriptionEmpty = rowMap.get(PARAM_DESCRIPTION).isEmpty();
            // If qty given but no number or number given but not qty - adding
            // error
            if ((!isQtyEmpty && (isCpsPartEmpty && isYourPartEmpty && isDescriptionEmpty)) || (isQtyEmpty && (!isCpsPartEmpty || !isYourPartEmpty && !isDescriptionEmpty))) {
                createError(CPSErrorCodes.ERR_REQUEST_QUOTE_INCORRECT_ROWS, null, pRequest.getLocale());
                // If row is empty - increase count
            } else if (isQtyEmpty && isCpsPartEmpty && isYourPartEmpty) {
                count++;
            }
        }
        // If all rows are empty add error
        if (count == mapWithRows.size()) {
        	vlogInfo("empty rows : {0}",mapWithRows.isEmpty());
            //createError(CPSErrorCodes.ERR_REQUEST_QUOTE_EMPTY_ROWS, null, pRequest.getLocale());
        }
    }

    private Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    private void copyInputStreamToFile(InputStream source, File destination) throws IOException {
        try {
            FileOutputStream output = FileUtils.openOutputStream(destination);

            try {
                IOUtils.copy(source, output);
                output.close();
            } finally {
                IOUtils.closeQuietly(output);
            }
        } finally {
            IOUtils.closeQuietly(source);
        }
    }

    protected void createError(String msg, String pty, Locale locale) {
        CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, locale, pty);
    }

    public CommonEmailSender getCommonEmailSender() {
        return commonEmailSender;
    }

    public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
        this.commonEmailSender = commonEmailSender;
    }

    public Object getRequestQuoteFile() {
        return requestQuoteFile;
    }

    public void setRequestQuoteFile(Object requestQuoteFile) {
        this.requestQuoteFile = requestQuoteFile;
    }

    public String getRequestAQuoteSuccessURL() {
        return requestAQuoteSuccessURL;
    }

    public void setRequestAQuoteSuccessURL(String requestAQuoteSuccessURL) {
        this.requestAQuoteSuccessURL = requestAQuoteSuccessURL;
    }

    public String getRequestAQuoteFailURL() {
        return requestAQuoteFailURL;
    }

    public void setRequestAQuoteFailURL(String requestAQuoteFailURL) {
        this.requestAQuoteFailURL = requestAQuoteFailURL;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getPersonalInfoJSON() {
        return personalInfoJSON;
    }

    public void setPersonalInfoJSON(String personalInfoJSON) {
        this.personalInfoJSON = personalInfoJSON;
    }

    public String getQuoteInfoJSON() {
        return quoteInfoJSON;
    }

    public void setQuoteInfoJSON(String quoteInfoJSON) {
        this.quoteInfoJSON = quoteInfoJSON;
    }

    public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
        mRepeatingRequestMonitor = pRepeatingRequestMonitor;
    }

    public RepeatingRequestMonitor getRepeatingRequestMonitor() {
        return mRepeatingRequestMonitor;
    }

	/**
	 * @return the cpsGlobalProperties
	 */
	public CPSGlobalProperties getCpsGlobalProperties() {
		return cpsGlobalProperties;
	}

	/**
	 * @param cpsGlobalProperties the cpsGlobalProperties to set
	 */
	public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
		this.cpsGlobalProperties = cpsGlobalProperties;
	}

	/**
	 * @return the isFileError
	 */
	public boolean isFileError() {
		return isFileError;
	}

	/**
	 * @param isFileError the isFileError to set
	 */
	public void setFileError(boolean isFileError) {
		this.isFileError = isFileError;
	}
    
}