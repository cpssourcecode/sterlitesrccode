package com.cps.util;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import atg.userprofiling.Profile;

import com.cps.email.CommonEmailSender;

import com.cps.util.validator.ServicesContactValidator;

import com.cps.userprofiling.CPSProfileTools;

import java.util.HashMap;
import java.util.Map;

import vsg.util.ajax.AjaxUtils;

public class ServicesContactFormHandler extends GenericFormHandler {
	private Profile profile;
	private CPSProfileTools profileTools;
	private CommonEmailSender commonEmailSender;
	private ServicesContactValidator servicesContactValidator;

	// Form fields
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String message;
	private String serviceName;

	/**
	 * request monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public void preServicesContactUs(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("preServicesContactUs - start");

		getServicesContactValidator().validateSenderInformation(getFirstName(), getLastName(), getEmailAddress(), this, request);
		getServicesContactValidator().validateMessageLength(getMessage(), this, request);

		vlogDebug("preServicesContactUs - end");
	}

	public boolean handleServicesContactUs(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("handleServicesContactUs - start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "ServicesContactFormHandler.handleServicesContactUs";
		boolean sendResult = false;

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (null == getMessage()) {
					setMessage(request.getParameter("/cps/util/ServicesContactFormHandler.message"));
					vlogDebug("handleServicesContactUs - message null, had to grab value <" + getMessage() + "> from request");
				}

				vlogDebug("  firstName=" + getFirstName() + "; lastName=" + getLastName() + "; email=" + getEmailAddress() + "; service=" + getServiceName());
				vlogDebug("  message=" + getMessage());

				preServicesContactUs(request, response);

				if (!getFormError()) {
					vlogDebug("Services Contact Us calling e-mail sender...");
					sendResult = getCommonEmailSender().sendCPSServicesContactUsEmail(getFirstName(), getLastName(), getEmailAddress(), getServiceName(), getMessage());
				}

				postServicesContactUs(request, response);

				vlogDebug("handleServicesContactUs - end, send result=" + sendResult);
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		if (AjaxUtils.isAjaxRequest(request)) {
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("sent", sendResult);
			AjaxUtils.addAjaxResponse(this, response, result);
			return false;
		}
		else {
			return (!getFormError());
		}
	}

	public void postServicesContactUs(DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
		vlogDebug("postServicesContactUs - start");

		vlogDebug("postServicesContactUs - end");
	}

	/*********************************************************/

	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	public ServicesContactValidator getServicesContactValidator() {
		return servicesContactValidator;
	}

	public void setServicesContactValidator(ServicesContactValidator servicesContactValidator) {
		this.servicesContactValidator = servicesContactValidator;
	}

	// Form fields
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

}