package com.cps.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.LRUMap;

import com.cps.commerce.order.purchase.CPSPriceAvailability;
import com.cps.integrations.CPSOrderDetailBean;
import com.cps.invoice.InvoiceInfo;
import com.cps.mtr.MTRInfo;
import com.cps.packingslip.PackingSlipRecord;
import com.cps.statement.StatementInfo;
import com.cps.userprofiling.util.UserAssociatedCS;
import com.csp.order.OrderInfo;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;


public class CPSSessionBean extends GenericService {
	
	/**
	 * Session Bean to store values
	 */
	
	// Variables
	private List<CPSPriceAvailability> palist = new ArrayList<CPSPriceAvailability>();
	private List<Integer> qtys;
	private List<String> itemIds;
	private ArrayList<String> invalidItemList = new ArrayList<String>();
	private List<String> mInvalidMaterialItemsList = new ArrayList<String>();
	private HashMap<String,HashMap<Integer,ArrayList<String>>> quickOrderDuplicates=null;
	private HashMap<String,HashMap<Integer,ArrayList<String>>> mMaterialDuplicates=null;
	private boolean hasQuickOrderDuplicates=false;
	private boolean mHasMaterialDuplicates=false;
	private boolean hasAddedNewProductToShoppingCart = false;
	private boolean availablePricesWebService = true;
	private boolean mShowErrorDivs = true;
	private boolean mMaterialListSuccessfullyUpdated;
	private boolean differentShipto=true;
	private Map<String, Long> itemIdsPA = new HashMap<String,Long>();
	private Long totalpaitems;
	
	private Integer loginErrorCount;
	private Integer loginErrorWithCaptchaCount;
	
	private List<CommerceItem> addedProductInfos;
	private List<String> notAddedProductIds;
	private List<AddCommerceItemInfo> notAddedProductInfos;
	private List<AddCommerceItemInfo> itemsAddToCartInfos;
	private boolean setByFile = false;
	private List<OrderInfo> mOrders;
	private List<InvoiceInfo> mInvoices;
	private List<PackingSlipRecord> mPackingSlipRecords;
	private String mComparisonReferer;
	private String mPreRegisterPage;
    private CPSGlobalProperties globalProperties;

    // use LRUMap to store then retrieved Price values per productIds

    private Map<String, Double> productPricesCache;
    private Map<String, String> bccImportedUsersMap;

    /**
	 * @return the bccImportedUsersMap
	 */
	public Map<String, String> getBccImportedUsersMap() {
		return bccImportedUsersMap;
	}

	/**
	 * @param bccImportedUsersMap the bccImportedUsersMap to set
	 */
	public void setBccImportedUsersMap(Map<String, String> bccImportedUsersMap) {
		this.bccImportedUsersMap = bccImportedUsersMap;
	}

	/**
     * @return the globalProperties
     */
    public CPSGlobalProperties getGlobalProperties() {
        return globalProperties;
    }

    /**
     * @param globalProperties
     *            the globalProperties to set
     */
    public void setGlobalProperties(CPSGlobalProperties globalProperties) {
        this.globalProperties = globalProperties;
    }

    /**
     * @return the productPricesCache
     */
    public Map<String, Double> getProductPricesCache() {
        // if the Map is not yet initialized, initialize it with the default size as set in GLobalProperties component
        if (productPricesCache == null) {
            productPricesCache = new LRUMap(getGlobalProperties().getPriceCacheSizePerUser());
        }
        return productPricesCache;
    }

    public int getProductPricesCacheSize() {
        int size = -1;
        if (productPricesCache != null) {
            size = productPricesCache.size();
        }
        return size;
    }

    private Map<String, CPSOrderDetailBean> mOrderDetailBeans = new HashMap<String, CPSOrderDetailBean>();

	public Map<String, CPSOrderDetailBean> getOrderDetailBeans() {
		return mOrderDetailBeans;
	}

	private Set<MTRInfo> mMaterialReports;

	public Set<MTRInfo> getMaterialReports() {
		if (mMaterialReports == null) {
			mMaterialReports = new LinkedHashSet<MTRInfo>();
		}
		return mMaterialReports;
	}

	public void setMaterialReports(Set<MTRInfo> pMaterialReports) {
		mMaterialReports = pMaterialReports;
	}

	private Set<String> mFoundHeatNums;

	public Set<String> getFoundHeatNums() {
		if (mFoundHeatNums == null) {
			mFoundHeatNums = new HashSet<String>();
		}
		return mFoundHeatNums;
	}

	public void setFoundHeatNums(Set<String> pFoundHeatNums) {
		mFoundHeatNums = pFoundHeatNums;
	}

	private String userShippingId;
	private List<UserAssociatedCS> usersShipping;
	List<RepositoryItem> companies;
	private Map<String, Boolean> letterMap;

	private Integer mOrdersTotal;

	public Integer getOrdersTotal() {
		return mOrdersTotal;
	}
	public void setOrdersTotal(Integer pOrdersTotal) {
		mOrdersTotal = pOrdersTotal;
	}

	private Integer mInvoicesTotal;

	public Integer getInvoicesTotal() {
		return mInvoicesTotal;
	}
	public void setInvoicesTotal(Integer pInvoicesTotal) {
		mInvoicesTotal = pInvoicesTotal;
	}

	private Integer mMTRTotal;

	public Integer getMTRTotal() {
		return mMTRTotal;
	}
	public void setMTRTotal(Integer pMTRTotal) {
		mMTRTotal = pMTRTotal;
	}

	private Integer mPackingSlipsTotal;

	public Integer getPackingSlipsTotal() {
		return mPackingSlipsTotal;
	}
	public void setPackingSlipsTotal(Integer pPackingSlipsTotal) {
		mPackingSlipsTotal = pPackingSlipsTotal;
	}

	private List<StatementInfo> mStatements = new ArrayList<StatementInfo>();;

	public List<StatementInfo> getStatements() {
		return mStatements;
	}
	public void setStatements(List<StatementInfo> pStatements) {
		mStatements = pStatements;
	}

	private int invoicesPending;
	private List<LinkedHashMap<String, Object>> sortableReorderLists;
	// Getters & Setters
	public Map<String, Long> getItemIdsPA() {
		return itemIdsPA;
	}
	public void setItemIdsPA(Map<String, Long> itemIdsPA) {
		this.itemIdsPA = itemIdsPA;
	}
	public Integer getLoginErrorCount() {
		return loginErrorCount;
	}
	public void setLoginErrorCount(Integer loginErrorCount) {
		this.loginErrorCount = loginErrorCount;
	}
	public Integer getLoginErrorWithCaptchaCount() {
		return loginErrorWithCaptchaCount;
	}
	public void setLoginErrorWithCaptchaCount(Integer loginErrorWithCaptchaCount) {
		this.loginErrorWithCaptchaCount = loginErrorWithCaptchaCount;
	}
	public List<CommerceItem> getAddedProductInfos() {
		return addedProductInfos;
	}
	public void setAddedProductInfos(List<CommerceItem> addedProductInfos) {
		this.addedProductInfos = addedProductInfos;
	}
	public void setItemsAddToCartInfos(List<AddCommerceItemInfo> itemsAddToCartInfos) {
		this.itemsAddToCartInfos = itemsAddToCartInfos;
	}
	
	public List<String> getItemIds() {
		return itemIds;
	}
	public void setItemIds(ArrayList<String> itemIds) {
		this.itemIds = itemIds;
	}
	public List<Integer> getQtys() {
		return qtys;
	}
	public void setQtys(ArrayList<Integer> qtys) {
		this.qtys = qtys;
	}
	public List<OrderInfo> getOrders() {
		return mOrders;
	}
	public void setOrders(List<OrderInfo> pOrders) {
		mOrders = pOrders;
	}
	public List<LinkedHashMap<String, Object>> getSortableReorderLists() {
		return sortableReorderLists;
	}
	public void setSortableReorderLists(List<LinkedHashMap<String, Object>> sortableReorderLists) {
		this.sortableReorderLists = sortableReorderLists;
	}

	public int getInvoicesPending() {
		return invoicesPending;
	}


	public void setInvoicesPending(int invoicesPending) {
		this.invoicesPending = invoicesPending;
	}
	public List<InvoiceInfo> getInvoices() {
		return mInvoices;
	}
	public void setInvoices(List<InvoiceInfo> pInvoices) {
		mInvoices = pInvoices;
	}
	public List<CPSPriceAvailability> getPalist() {

		return palist;
	}
	public void setPalist(List<CPSPriceAvailability> palist) {
		this.palist = palist;
	}
	public List<PackingSlipRecord> getPackingSlipRecords() {
		return mPackingSlipRecords;
	}
	public void setPackingSlipRecords(List<PackingSlipRecord> pPackingSlipRecords) {
		mPackingSlipRecords = pPackingSlipRecords;
	}
	public Long getTotalpaitems() {
		return totalpaitems;
	}
	public void setTotalpaitems(Long totalpaitems) {
		this.totalpaitems = totalpaitems;
	}
	public boolean isSetByFile() {
		return setByFile;
	}
	public void setSetByFile(boolean setByFile) {
		this.setByFile = setByFile;
	}
	public List<UserAssociatedCS> getUsersShipping() {
		return usersShipping;
	}
	public void setUsersShipping(List<UserAssociatedCS> usersShipping) {
		this.usersShipping = usersShipping;
	}
	public String getUserShippingId() {
		return userShippingId;
	}
	public void setUserShippingId(String userShippingId) {
		this.userShippingId = userShippingId;
	}
	public void setInvalidItemList(ArrayList<String> invalidItemList){
		this.invalidItemList=invalidItemList;
	}
	public ArrayList<String> getInvalidItemList(){
		return invalidItemList;
	}
	public void setQuickOrderDuplicates(HashMap<String,HashMap<Integer,ArrayList<String>>> quickOrderDuplicates){
		this.quickOrderDuplicates=quickOrderDuplicates;
	}
	public HashMap<String,HashMap<Integer,ArrayList<String>>> getQuickOrderDuplicates(){
		return quickOrderDuplicates;
	}
	public boolean isHasQuickOrderDuplicates() {
		return hasQuickOrderDuplicates;
	}
	public void setHasQuickOrderDuplicates(boolean hasQuickOrderDuplicates) {
		this.hasQuickOrderDuplicates = hasQuickOrderDuplicates;
	}

	public boolean isHasAddedNewProductToShoppingCart() {
		return hasAddedNewProductToShoppingCart;
	}

	public void setHasAddedNewProductToShoppingCart(boolean hasAddedNewProductToShoppingCart) {
		this.hasAddedNewProductToShoppingCart = hasAddedNewProductToShoppingCart;
	}

	public boolean getAvailablePricesWebService() {
		return availablePricesWebService;
	}

	public void setAvailablePricesWebService(boolean availablePricesWebService) {
		this.availablePricesWebService = availablePricesWebService;
	}

	///
	public void setInvalidMaterialItemsList(List<String> pInvalidMaterialItemsList){
		this.mInvalidMaterialItemsList=pInvalidMaterialItemsList;
	}
	public List<String> getInvalidMaterialItemsList(){
		return mInvalidMaterialItemsList;
	}
	public void setMaterialDuplicates(HashMap<String,HashMap<Integer,ArrayList<String>>> pMaterialDuplicates){
		this.mMaterialDuplicates=pMaterialDuplicates;
	}
	public HashMap<String,HashMap<Integer,ArrayList<String>>> getMaterialDuplicates(){
		return mMaterialDuplicates;
	}
	public boolean isHasMaterialDuplicates() {
		return mHasMaterialDuplicates;
	}
	public void setHasMaterialDuplicates(boolean pHasMaterialDuplicates) {
		this.mHasMaterialDuplicates = pHasMaterialDuplicates;
	}
	///
	public List<RepositoryItem> getCompanies() {
		return companies;
	}
	public void setCompanies(List<RepositoryItem> companies) {
		this.companies = companies;
	}

	public void setClearMtr(boolean pClear) {
		//if (pClear) {
			getMaterialReports().clear();
			setMTRTotal(0);
		//}
	}

	public Map<String, Boolean> getLetterMap() {
		return letterMap;
	}

	public void setLetterMap(Map<String, Boolean> letterMap) {
		this.letterMap = letterMap;
	}

	public String getComparisonReferer() {
		return mComparisonReferer;
	}

	public void setComparisonReferer(String pComparisonReferer) {
		mComparisonReferer = pComparisonReferer;
	}

	public String getPreRegisterPage() {
		return mPreRegisterPage;
	}

	public void setPreRegisterPage(String pPreRegisterPage) {
		mPreRegisterPage = pPreRegisterPage;
	}

	public boolean getShowErrorDivs() {
		return mShowErrorDivs;
	}

	public void setShowErrorDivs(boolean pShowErrorDivs) {
		mShowErrorDivs = pShowErrorDivs;
	}

	public boolean getMaterialListSuccessfullyUpdated() {
		return mMaterialListSuccessfullyUpdated;
	}

	public void setMaterialListSuccessfullyUpdated(boolean pMaterialListSuccessfullyUpdated) {
		mMaterialListSuccessfullyUpdated = pMaterialListSuccessfullyUpdated;
	}

	/**
	 * @return the differentShipto
	 */
	public boolean isDifferentShipto() {
		return differentShipto;
	}

	/**
	 * @param differentShipto the differentShipto to set
	 */
	public void setDifferentShipto(boolean differentShipto) {
		this.differentShipto = differentShipto;
	}

	
}