package com.cps.util.model;

public class UserImport {
    /**
     * User first name
     */
    private String mFirstName;
    /**
     * User last name
     */
    private String mLastName;
    /**
     * User email
     */
    private String mEmail;
    /**
     * User phone
     */
    private String mPhone;
    /**
     * User isActive
     */
    private boolean mIsActive;
    /**
     * User role
     */
    private String mRole;
    /**
     * User spending limit
     */
    private String mSpendingLimit;
    /**
     * User frequency
     */
    private String mSpendingLimitFrequency;
    /**
     * User ship address
     */
    private String mShipAddrId;

    private String parentOrganizationId;

    /**
     * User import constructor
     *
     * @param pFirstName - first name
     * @param pLastName  - last name
     * @param pEmail     - email
     * @param pRole      - role
     */
    public UserImport(String pFirstName, String pLastName, String pEmail, String pPhone, String pIsActive,
                      String pRole, String pSpendingLimit, String pSpendingLimitFrequency, String pShipAddrId) {
        mFirstName = pFirstName;
        mLastName = pLastName;
        mEmail = pEmail;
        mPhone = pPhone;
        mIsActive = Boolean.parseBoolean(pIsActive);
        mRole = pRole;
        mSpendingLimit = pSpendingLimit;
        mSpendingLimitFrequency = pSpendingLimitFrequency;
        mShipAddrId = pShipAddrId;
    }

    public UserImport(String pFirstName, String pLastName, String pEmail, String pPhone, String pIsActive, String pRole) {
        mFirstName = pFirstName;
        mLastName = pLastName;
        mEmail = pEmail;
        mPhone = pPhone;
        mIsActive = Boolean.parseBoolean(pIsActive);
        mRole = pRole;

    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPhone() {
        return mPhone;
    }

    public boolean isActive() {
        return mIsActive;
    }

    public void setActive(boolean pIsActive) {
        this.mIsActive = pIsActive;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String pRole) {
        this.mRole = pRole;
    }

    public String getSpendingLimit() {
        return mSpendingLimit;
    }

    public String getSpendingLimitFrequency() {
        return mSpendingLimitFrequency;
    }

    public String getShipAddrId() {
        return mShipAddrId;
    }

    public void setShipAddrId(String pShipAddrId) {
        mShipAddrId = pShipAddrId;
    }

    /**
     * @return the parentOrganizationId
     */
    public String getParentOrganizationId() {
        return parentOrganizationId;
    }

    /**
     * @param parentOrganizationId
     *            the parentOrganizationId to set
     */
    public void setParentOrganizationId(String parentOrganizationId) {
        this.parentOrganizationId = parentOrganizationId;
    }

    /**
     * Create toString method for logging
     *
     * @return properties
     */
    public String toString() {
        return new StringBuilder("First Name - ").append(mFirstName)
                .append(" | ").append("Last Name - ").append(mLastName)
                .append(" | ").append("Email - ").append(mEmail)
                .append(" | ").append("isActive - ").append(mIsActive)
                .append(" | ").append("Role - ").append(mRole)
                .append(" | ").append("Phone - ").append(mPhone)
                .append(" | ").append("Spending Limit - ").append(mSpendingLimit)
                .append(" | ").append("Frequency - ").append(mSpendingLimitFrequency)
                .append(" | ").append("ShipAddrId - ").append(mShipAddrId)
                        .append(" | ").append("parentOrganizationId - ").append(parentOrganizationId)
                .toString();
    }
}
