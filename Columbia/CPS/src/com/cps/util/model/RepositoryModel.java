package com.cps.util.model;

import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;

import java.io.Serializable;

/**
 * @author Alex Turchynovich.
 */
public class RepositoryModel implements Serializable{

	private RepositoryItem mSource;
	private MutableRepositoryItem mTarget;

	public RepositoryModel(RepositoryItem pSource, MutableRepositoryItem pTarget){
		mSource = pSource;
		mTarget = pTarget;
	}

	public RepositoryModel(RepositoryItem pSource){
		mSource = pSource;
		if(pSource instanceof MutableRepositoryItem){
			mTarget = (MutableRepositoryItem) pSource;
		}
	}

	public Object getPropertyValue(String key){
		return mSource.getPropertyValue(key);
	}

	public void setPropertyValue(String key, Object value){
		if(mTarget == null){
			throw new IllegalStateException("Cannot set value to read only Repository Item");
		}
		mTarget.setPropertyValue(key, value);
	}

	public RepositoryItem getSource() {
		return mSource;
	}

	public MutableRepositoryItem getTarget() {
		return mTarget;
	}

	@Override
	public boolean equals(Object pO) {
		if (this == pO) return true;
		if (pO == null || getClass() != pO.getClass()) return false;

		RepositoryModel that = (RepositoryModel) pO;

		return mSource != null ? mSource.equals(that.mSource) : that.mSource == null;
	}

	@Override
	public int hashCode() {
		return mSource != null ? mSource.hashCode() : 0;
	}
}
