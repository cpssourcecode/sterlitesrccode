package com.cps.util;

public interface DataLayerConstants {
    String ORDER = "order";
    String SKU = "sku";
    String NAME = "name";
    String CATEGORY = "category";
    String PRICE = "price";
    String QUANTITY = "quantity";
    String TRANSACTION_ID = "transactionId";
    String TRANSACTION_AFFILIATION = "transactionAffiliation";
    String TRANSACTION_TOATAL = "transactionTotal";
    String TRANSACTION_TAX = "transactionTax";
    String TRANSACTION_SHIPPING = "transactionShipping";
    String TRANSACTION_PRODUCTS = "transactionProducts";

}
