package com.cps.util;

import static com.cps.util.CPSConstants.EVENT_NAME;
import static com.cps.util.CPSConstants.GIFTLISTS;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.cps.commerce.gifts.CPSGiftlistFormHandler;
import com.cps.commerce.gifts.CPSGiftlistTools;
import com.cps.commerce.order.purchase.CPSCartModifierFormHandler;
import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.CPSMultiUserAddFormHandler;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.userprofiling.validator.MaterialListValidator;
import com.cps.util.model.UserImport;
import com.cps.util.transaction.TransactionProcessor;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepository;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.UploadedFile;
import atg.userprofiling.MultiUserAddFormHandler;
import atg.userprofiling.Profile;
import atg.userprofiling.UserListItem;
import vsg.util.ajax.AjaxUtils;

public class FileUploadHandler extends GenericFormHandler {
    private static final String THIS_EMAIL_IS_ALREADY_EXISTS = "This email already exists. So, It's not processed.";
	private GiftlistFormHandler giftlistFormHandler;
    private String readFileSuccessURL;
    private String readFileErrorURL;
    private String giftlistId;
    private Object importFile;
    private MutableRepository productCatalog;
    private CPSCartModifierFormHandler cartForm;
    private CommonEmailSender commonEmailSender;
    private boolean mGiftlistCheckAvailability = false;
    private String emptyFileErrorMessage;
    
    /**
     * Include profile
     */
    private Profile profile;
    /**
     * Include profileTools
     */
    private CPSProfileTools profileTools;
    /**
     * Include MultiUserAddFormHandler
     */
    private MultiUserAddFormHandler multiUserAddFormHandler;
    private String productsForCart = ""; //products for quick order
    private String quantitiesForCart = ""; //quantities for quick order

    private ArrayList<String> productIds = new ArrayList<String>();
    private ArrayList<Long> qtys = new ArrayList<Long>();

    private ArrayList<String> localInvalidItems = new ArrayList<>();

    /**
     * Constant value for checking header row in user gear import sheet
     */
    private static final String HEADER_CHECK_QUICK_ORDER = "Item #/Short Code/Customer Alias\nExample: sku10001";
    private static final String HEADER_CHECK_MATERIAL_LIST = "Item #/Short Code/Customer Alias\nExample: sku10001";


    private static final String HEADER_AJAX_FILE_UPLOAD = "Ajax-File-Upload";
    private static final String HEADER_PARAMETER_TRUE = "true";
    /**
     * Constant value for file extension xls
     */
    private static final String XLS_EXTENSION = "xls";
    /**
     * Constant value for file extension xlsx
     */
    private static final String XLSX_EXTENSION = "xlsx";
    private static final String CSV_EXTENSION = "csv";
    // .doc, pdf, .xls, .jpg, .png, .tif, or .gif.
    private static final String DOC_EXTENSION = "doc";
    private static final String DOCX_EXTENSION = "docx";
    private static final String PDF_EXTENSION = "pdf";
    private static final String JPG_EXTENSION = "jpg";
    private static final String PNG_EXTENSION = "png";
    private static final String TIF_EXTENSION = "tif";
    private static final String GIF_EXTENSION = "gif";

//    private static final String LIST_NAME_KEY = "/cps/userprofiling/MaterialListHandler.eventName";
//    private static final String LIST_DESCRIPTION_KEY = "/cps/userprofiling/MaterialListHandler.description";
    /**
     * File to read in for quick order
     */
    private Object quickOrderFile;
    /**
     * QuickOrder upload success url string
     */
    private String readQuickOrderFileSuccessURL;
    /**
     * QuickOrder upload fail url string
     */
    private String readQuickOrderFileFailURL;
    /**
     * QuickOrder duplicate fail url string
     */
    private String readQuickOrderInvalidURL;
    /**
     * File to read in for import users
     */
    private Object userFile;
    /**
     * List of users to be imported
     */
    private List<UserImport> usersToImport = new ArrayList<UserImport>();
    /**
     * User upload success url string
     */
    private String readUserFileSuccessURL;

    private static final String HEADER_CHECK_USER = "First Name";
    /**
     * File to read in for request a quote
     */
    private Object requestQuoteFile;
    /**
     * Request a Quote upload success url string
     */
    private String requestAQuoteSuccessURL;
    /**
     * Request a Quote upload fail url string
     */
    private String requestAQuoteFailURL;
    /**
     * Request a Quote email body optional message
     */
    private String requestQuoteMessage;

    private boolean bulkUploadFromBcc;

    private List<String> emailsImportedInThisRun = new ArrayList<>();


    /**
     * Error message map holder
     */
    private Map<String, List<String>> errorMap = new HashMap<String, List<String>>();

    /**
     * request monitor
     */
    private RepeatingRequestMonitor mRepeatingRequestMonitor;
    private TransactionProcessor mTransactionProcessor;
    private MaterialListValidator mValidator;
    private TransactionManager mTransactionManager;
    private String mListName;
    private String mListDescription;

    
 //////////////////////////import user/////////////////////////////////////////////
    /**
     * Handle method to import users from xls file
     *
     * @param pRequest  - request
     * @param pResponse - response
     * @return false
     * @throws IOException
     * @throws ServletException
     */
    public boolean handleReadUserFile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws IOException, ServletException {
        vlogDebug("handleReadUserFile.start");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "FileUploadHandler.handleReadUserFile";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                // Get the file
                UploadedFile uploadedFile = (UploadedFile) getUserFile();
                vlogDebug("File: {0}", uploadedFile);

                // Process upload
                if (uploadedFile != null && uploadedFile.getInputFile() != null && uploadedFile.getInputFile().exists()) {
                    // Check file format
                    if (checkFileExtensionXls(uploadedFile)) {
                        readUpUser(uploadedFile, pRequest, pResponse);
                    } else {
                        // Not xls or xlsx, add error
                        createError("err_template_upload_fail", "import", pRequest.getLocale());
                    }
                } else {
                    // No file entered, add form exception
                    createError("err_template_upload_fail", "import", pRequest.getLocale());
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        vlogDebug("handleReadUserFile.end");
        vlogDebug("imported users are - {0}", ((CPSMultiUserAddFormHandler) getMultiUserAddFormHandler()).getBccImportedUsers());
        emailsImportedInThisRun.clear();
        return checkFormRedirect(getReadUserFileSuccessURL(), null, pRequest, pResponse);
    }

    /**
     * Read the file and extract new users to add
     *
     * @param pFile     - import list
     * @param pRequest  - request
     * @param pResponse - response
     */
    protected void readUpUser(UploadedFile pFile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
        vlogDebug("readUpUser.start");

        if (pFile != null) {
            InputStream inp = null;
            try {
                inp = pFile.getInputStream();

                // Create workbook
                Workbook wb = WorkbookFactory.create(inp);
                Sheet sheet = wb.getSheetAt(0);
                CPSMultiUserAddFormHandler multiUserFH = (CPSMultiUserAddFormHandler) getMultiUserAddFormHandler();
                multiUserFH.setBulkUploadFromBcc(bulkUploadFromBcc);
                // Read user data
                for (Row row : sheet) {
                    processUserRow(row, pRequest.getLocale());
                }
                multiUserFH.getSessionBean().setBccImportedUsersMap(multiUserFH.getBccImportedUsers());

                if (inp != null) {
                    inp.close();
                }
                checkImportErrors(pRequest.getLocale());

                // Import the users
                vlogDebug("Users to Import: {0}", usersToImport);
                if (!getFormError() && usersToImport != null && !usersToImport.isEmpty()) {
                    vlogDebug("no validation errors. going to add users");
                    multiUserFH.addUsersFromImport(usersToImport, pRequest, pResponse);
                }

                if (multiUserFH.getFormError()) {
                    if (multiUserFH.getUsers() != null) {
                        for (UserListItem userListItem : (List<UserListItem>) multiUserFH.getUsers()) {
                            if (userListItem.getFormError()) {
                                vlogError("Errors in User Import :: userListItem.getFormError() :: {0}", userListItem.getFormExceptions());
                            }
                        }
                    }
                    vlogError("Errors in User Import :: multiUserFH.getFormExceptions() :: {0}", multiUserFH.getFormExceptions());
                }

            } catch (Exception e) {
                vlogError("Error: {0}",e.getMessage());
                e.getStackTrace();
                // Add error
            } finally {
                try {
                    if (inp != null) {
                        inp.close();
                    }
                } catch (IOException e) {
                    vlogError("IOException Error: {0}",e.getMessage());
                }
            }
        }

        vlogDebug("readUpUser.end");
    }

    /**
     * Read the row and extract user import info
     *
     * @param row    - row to read
     * @param locale - request locale
     */
    protected void processUserRow(Row row, Locale locale) {
        vlogDebug("processUserRow.start - Reading Row: {0}", row.getRowNum());

        // Check for header row
        if (row.getRowNum() == 0 && isHeaderRow(row, HEADER_CHECK_USER)) {
            vlogDebug("Header row, return");
            return;
        }

        // Not header row, proceed
         UserImport user = new UserImport(readCell(row, 0), readCell(row, 1), readCell(row, 2), readCell(row, 3),
                readCell(row, 4), readCell(row, 5), readCell(row, 6), readCell(row, 7), readCell(row, 8)); 
        if(isBulkUploadFromBcc()) {
            //if it is  a bulk upload from BCC, then the columns 9 would be there with the parentOrganizationId
            user.setParentOrganizationId(readCell(row, 9));
        }

        vlogDebug("User values: - {0}", user.toString());

        if (!isRowEmpty(row)) {
            if (isUserValid(user, locale, Integer.toString(row.getRowNum()))) {
                // let us check whether the user is already there and the process is BCC bulk import. if yes, we will ignore that user
                // also check whether the email is not duplicated in the same file. this will also cause problems
                RepositoryItem existingUser = getProfileTools().getItemFromEmail(user.getEmail().toLowerCase());
                if (existingUser == null && !emailsImportedInThisRun.contains(user.getEmail())) {
                    usersToImport.add(user);
                    emailsImportedInThisRun.add(user.getEmail());
                } else {
                    CPSMultiUserAddFormHandler multiFH = (CPSMultiUserAddFormHandler) getMultiUserAddFormHandler();
                    multiFH.getBccImportedUsers().put(user.getEmail(), THIS_EMAIL_IS_ALREADY_EXISTS);
                }
            }
        }

        vlogDebug("processUserRow.end");
    }

    public static boolean isRowEmpty(Row row) {
        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
                return false;
        }
        return true;
    }

    /**
     * check whether the user is valid.
     * 
     * @param user
     * @param locale
     * @param rowNum
     * @return
     */

    private boolean isUserValid(UserImport user, Locale locale, String rowNum) {
        vlogDebug("Checking if user - {0} is a valid user", user);

        Map<String, String> errors = new HashMap<String, String>();

        if (!StringUtils.isNotBlank(user.getRole())) {
            if (!isBulkUploadFromBcc()) {
            user.setRole(CPSConstants.ROLE_ACCOUNT_ADMIN + "," + CPSConstants.ROLE_APPROVER + "," +
                    CPSConstants.ROLE_BUYER + "," + CPSConstants.ROLE_FINANCE + "," + CPSConstants.ROLE_FINANCE_LIMITED);
            } else {
                // if it is imported from BCC< just set the buyer role as the default role
                user.setRole(CPSConstants.ROLE_BUYER);
            }
        }

        String[] roles = user.getRole().split(",");
        Map<String, String> userRoles = new HashMap<String, String>();
        for (String role : roles) {
            userRoles.put(role, "true");
        }
        errors = ((CPSMultiUserAddFormHandler) getMultiUserAddFormHandler()).
                validateNewUser(user.getFirstName(), user.getLastName(),
                        user.getEmail(), user.getPhone(), userRoles,
                                        user.getSpendingLimit(), user.getSpendingLimitFrequency(), null, user.getParentOrganizationId(), true);

        if (!validRoles(roles)) {
            errors.put(CPSErrorCodes.ERR_UPDATE_INVALID_ROLE, CPSConstants.ROLE_PRPTY);
        }

        if (errors.isEmpty()) {
            RepositoryItem userItem = null;
            if (!StringUtils.isBlank(user.getEmail())) {
                userItem = getMultiUserAddFormHandler().getProfileTools().getItemFromEmail(user.getEmail().toLowerCase());
            }
            if (!isBulkUploadFromBcc()) {
            if (userItem != null) {
                if ((Boolean) userItem.getPropertyValue(CPSConstants.ON_HOLD)) {
                    errors.put("err_email_exist_deactive", CPSConstants.EMAIL);
                } else {
                    errors.put("err_email_already_exits", CPSConstants.EMAIL);
                }
            }
            }
        }

        if (errors != null && !errors.isEmpty()) {
            vlogDebug("Errors: {0} ", errors);
            List<String> errorList = new ArrayList<String>();
            for (Entry<String, String> error : errors.entrySet()) {
                errorList.add(CPSMessageUtils.getErrorMessage(error.getKey(), locale));
            }
            getErrorMap().put(rowNum, errorList);
            return false;
        }
        return true;
    }

    private boolean validRoles(String[] roles) {
        boolean rolesAreValid = true;
        String validRoles = CPSConstants.ROLE_ACCOUNT_ADMIN + "," + CPSConstants.ROLE_APPROVER + "," +
                CPSConstants.ROLE_BUYER + "," + CPSConstants.ROLE_FINANCE + "," + CPSConstants.ROLE_FINANCE_LIMITED + "," + CPSConstants.ROLE_APPRAISER;

        vlogDebug("Roles to add: {0} ", Arrays.toString(roles));
        vlogDebug("Valid Roles: {0} ", validRoles);

        for (String role : roles) {
            vlogDebug("Role - {0} is valid? - {1} ", role, validRoles.contains(role));
            if (!validRoles.contains(role)) {
                // Role doesn't match a valid role, not valid
                rolesAreValid = false;
            }
        }
        if (!rolesAreValid || roles.length < 5) {
            roles = validRoles.split(",");
        }

        return rolesAreValid;
    }

    private void checkImportErrors(Locale locale) {
        if (getErrorMap() != null && !getErrorMap().isEmpty()) {
            // There are errors
            createError(CPSErrorCodes.ERR_INVALID_DATA_IN_TEMPLATE, "import", locale);

            String errorMsg = null;
            createError(CPSErrorCodes.ERR_UPLOADING_FILE, "import", locale);
            for (Entry<String, List<String>> error : getErrorMap().entrySet()) {
                if (error.getValue() != null && !error.getValue().isEmpty()) {
                    for (String e : error.getValue()) {
                        errorMsg = System.lineSeparator()+"Row: " + error.getKey() + " - " + e;
                        createError(errorMsg, "import", locale);
                    }
                }
            }
            vlogDebug("ErrorMsg: {0}", errorMsg);
        }
    }
//////////////////////////end import user/////////////////////////////////////////////
    
//////////////////////////import quick order/////////////////////////////////////////////
    /**
     * Handle method to import gears from xls file
     *
     * @param pRequest  - request
     * @param pResponse - response
     * @return false
     * @throws IOException
     * @throws ServletException
     */
    public boolean handleReadQuickOrderFile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws IOException, ServletException {
        vlogDebug("handleReadQuickOrderFile.start");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "FileUploadHandler.handleReadQuickOrderFile";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                // Get the file
                UploadedFile uploadedFile = (UploadedFile) getQuickOrderFile();
                vlogDebug("File: {0}", uploadedFile);
                if (uploadedFile != null && uploadedFile.getInputFile() == null) {
                    byte[] buf = new byte[uploadedFile.getFileSize()];
                    uploadedFile.getInputStream().read(buf);
                    uploadedFile = new UploadedFile(uploadedFile.getFilename(), uploadedFile.getContentType(), uploadedFile.getFileSize(), buf);
                }

                // Process upload
                if (uploadedFile != null && uploadedFile.getInputStream() != null/* && uploadedFile.getInputFile().exists()*/) {
                    // Check file format
                    if (checkFileExtensionXlsCsv(uploadedFile)) {
                        return readUpQuickOrder(uploadedFile, pRequest, pResponse);
                    } else {
                        // Not xls or xlsx, add error
                        createError(CPSErrorCodes.ERR_INVALID_FILE_FORMAT, "import", pRequest.getLocale());
                    }
                } else {
                    // No file entered, add form exception
                    createError(CPSErrorCodes.ERR_INVALID_FILE_FORMAT, "import", pRequest.getLocale());
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        vlogDebug("handleReadQuickOrderFile.end");
        return checkFormRedirect(getReadQuickOrderFileSuccessURL(), getReadQuickOrderFileFailURL(), pRequest, pResponse);
    }

    public boolean handleQuickOrderPadReadQFile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        vlogDebug("handleQuickOrderPadReadQFile.start");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "FileUploadHandler.handleQuickOrderPadReadQFile";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                // Get the file
                UploadedFile uploadedFile = (UploadedFile) getQuickOrderFile();
                vlogDebug("File: {0} ", uploadedFile);
                if (uploadedFile != null && uploadedFile.getInputFile() == null) {
                    byte[] buf = new byte[uploadedFile.getFileSize()];
                    uploadedFile.getInputStream().read(buf);
                    uploadedFile = new UploadedFile(uploadedFile.getFilename(), uploadedFile.getContentType(), uploadedFile.getFileSize(), buf);
                }

                // Process upload
                if (uploadedFile != null && uploadedFile.getInputStream() != null/* && uploadedFile.getInputFile().exists()*/) {
                    // Check file format
                    if (checkFileExtensionXlsCsv(uploadedFile)) {
                        cartForm.getSessionBean().setInvalidItemList(null);
                        readUpQuickOrder(uploadedFile, pRequest, pResponse);
                        if (cartForm.getSessionBean().getInvalidItemList() != null && !cartForm.getSessionBean().getInvalidItemList().isEmpty()) {
                            createError(CPSErrorCodes.ERR_MIN_ORDER_QTY, CPSErrorCodes.ERR_MIN_ORDER_QTY, pRequest.getLocale());
                        }
                    } else {
                        // Not xls or xlsx, add error
                        createError(CPSErrorCodes.ERR_INVALID_FILE_FORMAT, "import", pRequest.getLocale());
                    }
                } else {
                    // No file entered, add form exception
                    createError(CPSErrorCodes.ERR_INVALID_FILE_FORMAT, "import", pRequest.getLocale());
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        vlogDebug("handleQuickOrderPadReadQFile.end");
        return response(pResponse);
    }

    /**
     * Read the file and extract new gears to add
     *
     * @param pFile     - import list
     * @param pRequest  - request
     * @param pResponse - response
     */
    protected boolean readUpQuickOrder(UploadedFile pFile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws IOException, ServletException {
        vlogDebug("readUpQuickOrder.start");

        if (pFile != null) {
            InputStream inp = null;
            try {
                inp = pFile.getInputStream();
                String fileName = pFile.getFilename();
                String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();

                switch (fileExt) {
                    case CSV_EXTENSION:
                        try (BufferedReader br = new BufferedReader(new InputStreamReader(inp))) {
                            String row;
//                            String headerRow = br.readLine();
//                            if (StringUtils.isNotBlank(headerRow)) {
//                                String firstColumn = headerRow.split(",")[0];
//                                if (!firstColumn.equalsIgnoreCase(HEADER_CHECK_MATERIAL_LIST)) {
//                                    vlogDebug("wrong first column name: " + firstColumn + " is not equals to " + HEADER_CHECK_MATERIAL_LIST);
//                                }
//                            }
                            while ((row = br.readLine()) != null) {
                                processQuickOrderRowFromCSV(row, pRequest.getLocale());
                                if (getFormError()) {
                                	if (!isAjaxFileUpload(pRequest)) {
    		                            return checkFormRedirect(getReadQuickOrderFileSuccessURL(), getReadQuickOrderFileFailURL(), pRequest, pResponse);
    		                        } else {
    		                            return false;
    		                        }
                                }
                            }
                        }
                        break;
                    case XLS_EXTENSION:
                    case XLSX_EXTENSION:
		                // Create workbook
		                Workbook wb = WorkbookFactory.create(inp);
		                Sheet sheet = wb.getSheetAt(0);
		
		                // Read user data
		                for (Row row : sheet) {
		                    processQuickOrderRow(row, pRequest.getLocale());
		                    if (getFormError()) {
		                        if (!isAjaxFileUpload(pRequest)) {
		                            return checkFormRedirect(getReadQuickOrderFileSuccessURL(), getReadQuickOrderFileFailURL(), pRequest, pResponse);
		                        } else {
		                            return false;
		                        }
		                    }
		                }
                    default:
                        break;
                }
                
                // Import the lineItems
                vlogDebug("Products for Cart: " + productsForCart);
                if (getProductsForCart() != null && !getProductsForCart().isEmpty() && getQuantitiesForCart() != null && !getQuantitiesForCart().isEmpty()) {
                    vlogDebug("productsForCart:" + getProductsForCart());
                    vlogDebug("quantitiesForCart:" + getQuantitiesForCart());

                    cartForm.setQtyList(getQuantitiesForCart());
                    cartForm.setSkuIdsList(getProductsForCart());
                    cartForm.setQuickOrderAddItemsToOrderFromListErrorURL(getReadQuickOrderFileFailURL());
                    cartForm.setQuickOrderAddItemsToOrderFromListSuccessURL(getReadQuickOrderFileSuccessURL());
                    cartForm.setQuickOrderAddItemsToOrderFromListInvalidURL(getReadQuickOrderInvalidURL());
                    return cartForm.handleQuickOrderAddItemsToOrderFromList(pRequest, pResponse);
                } else {
                    //products and quantities don't match, cant do anything.
                    createError(CPSErrorCodes.ERR_INVALID_FILE_FORMAT, "import", pRequest.getLocale());
                    if (!isAjaxFileUpload(pRequest)) {
                        return checkFormRedirect(getReadQuickOrderFileSuccessURL(), getReadQuickOrderFileFailURL(), pRequest, pResponse);
                    }
                }

            } catch (Exception e) {
                vlogError(e, "Error");
                // Add error
            } finally {
                try {
                    if (inp != null) {
                        inp.close();
                    }
                } catch (IOException e) {
                    vlogError(e, "Error");
                }
            }
        }

        vlogDebug("readUpQuickOrder.end");
        createError(CPSErrorCodes.ERR_INVALID_FILE_FORMAT, "import", pRequest.getLocale());
        if (!isAjaxFileUpload(pRequest)) {
            return checkFormRedirect(getReadQuickOrderFileSuccessURL(), getReadQuickOrderFileFailURL(), pRequest, pResponse);
        } else {
            return false;
        }
    }
    
    /**
     * Read the row and extract user import info
     *
     * @param row    - row to read
     * @param locale - request locale
     */
    protected void processQuickOrderRow(Row row, Locale locale) {
        vlogDebug("processQuickOrderRow.start - Reading Row: " + row.getRowNum());

        // Check for header row
        // skip first row as header
//        if (row.getRowNum() == 0/* && isHeaderRow(row, HEADER_CHECK_QUICK_ORDER)*/) {
//            vlogDebug("Header row, return");
//            return;
//        }
        //PAUSE
        // Not header row, proceed
        ArrayList<String> quickOrderProperties = new ArrayList<String>();
        quickOrderProperties.add(readCell(row, 0)); // CPS ITEM # or CUSTOMER ALIAS # (FROM prodAliasMap on product)
        quickOrderProperties.add(readCell(row, 1)); // quantity for this product
        isQuickOrderValid(quickOrderProperties, locale); //checks quickOrder Line data, if valid adds to productsForCart Map
        vlogDebug("processUserRow.end");
    }
    
    /**
     * Read the row and extract material list import info
     *
     * @param row - row to read
     */
    protected void processQuickOrderRowFromCSV(String row, Locale locale) {
        vlogDebug("processQuickOrderRowFromCSV.start - Reading Row: " + row);
        if (StringUtils.isBlank(row)) {
            return;
        }
        String[] rowData = row.split(",");
        if (rowData.length < 2) {
            vlogDebug("Row is too short");
            return;
        }

        ArrayList<String> quickOrderProperties = new ArrayList<String>();
        quickOrderProperties.add(rowData[0]); // CPS ITEM # or CUSTOMER ALIAS # (FROM prodAliasMap on product)
        quickOrderProperties.add(rowData[1]); // quantity for this product
        isQuickOrderValid(quickOrderProperties, locale); //checks quickOrder Line data, if valid adds to productsForCart Map

        vlogDebug("processQuickOrderRowFromCSV.end");
    }
    
    /**
     * checks if cps # or customer alias # exists, need to check available quantity?
     *
     * @param properties Properties properties from spreadsheet row in  list
     * @param locale     request locale
     * @return if values are valid
     */
    private boolean isQuickOrderValid(ArrayList<String> properties, Locale locale) {
        Map<String, String> errors = validateNewLineItem(properties);

        if (errors != null && !errors.isEmpty()) {
            if (!getFormError()) {
                createError(CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, "import", locale);
            }
            return false;
        }
        return true;
    }
    
    /**
     * Method that takes in values for new line item and validates
     *
     * @param values new values
     * @return any errors
     */
    public Map<String, String> validateNewLineItem(ArrayList<String> values) {
        //0-cpsItem#/customerAlias#,1-quantity
        vlogDebug("validateNewLineItem.start - values: " + values);
        Map<String, String> errors = new HashMap<String, String>();
        if (values == null || values.size() <= 0) {
            return errors;
        } else if (values != null) {
            boolean allNull = true;
            for (int ind = 0; ind < values.size(); ind++) {
                if (values.get(ind) != null && !values.get(ind).equals("null") && !values.get(ind).trim().equals("")) {
                    allNull = false;
                    break; //should actually check this row, not just all blank
                }
            }
            if (allNull) {
                vlogDebug("validateNewLineItem-EMPTY ROW, IGNORE IT");
                return errors; //Don't bother checking this row, everything is NULL or BLANK, probably last row
            }
        }
        // Check Properties
        String error = null;
        error = CPSFormUtils.checkProperty((values.get(1) != null ? values.get(1) : null), false, CPSConstants.REG_EXP_NUMERIC, null, "INVALID CHARACTERS");
        if ((values.get(1) == null || values.get(1).length() > 5)) {
            errors.put("QUANTITY BADNESS", error);
        }
        if (errors.isEmpty() && values.get(0) != null) {
            vlogDebug("adding to products=" + values.get(0) + ", adding to quantities=" + Integer.parseInt(values.get(1)));
            productsForCart += "" + values.get(0) + ",";
            quantitiesForCart += "" + values.get(1) + ",";
        }
        vlogDebug("validateNewLineItem.end - returning: " + errors);
        return errors;
    }

////////////////////////end import quick order/////////////////////////////////////////////
    
////////////////////////import material list/////////////////////////////////////////////
    /**
     * Handle method to import gears from xls file
     *
     * @param pRequest  - request
     * @param pResponse - response
     * @return false
     * @throws IOException
     * @throws ServletException
     */
    public boolean handleReadImportMaterialFile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws IOException, ServletException {
        vlogDebug("handleReadImportMaterialFile.start");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "FileUploadHandler.handleReadImportMaterialFile";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                // Get the file
                UploadedFile uploadedFile = (UploadedFile) getImportFile();
                vlogDebug("File: " + uploadedFile);
                if (uploadedFile != null && uploadedFile.getInputFile() == null) {
                    byte[] buf = new byte[uploadedFile.getFileSize()];
                    uploadedFile.getInputStream().read(buf);
                    uploadedFile = new UploadedFile(uploadedFile.getFilename(), uploadedFile.getContentType(), uploadedFile.getFileSize(), buf);
                }
                // Process upload
                if (uploadedFile != null && uploadedFile.getInputStream() != null) {
                    // Check file format
                    if (checkFileExtensionXlsCsv(uploadedFile)) {
                        return readUpMaterialList(uploadedFile, pRequest, pResponse);
                    } else {
                        // Not xls or xlsx, add error
                        createError(CPSErrorCodes.ERR_INCORRECT_FILE, "import", pRequest.getLocale());
                    }
                } else {
                    // No file entered, add form exception
                    createError(CPSErrorCodes.ERR_INCORRECT_FILE, "import", pRequest.getLocale());
                }
            } finally {
                if (rrm != null) {
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        vlogDebug("handleReadImportMaterialFile.end");

        String successUrl = ((CPSGiftlistFormHandler) getGiftlistFormHandler()).getAddItemsToMaterialListSuccessUrl() + getGiftlistId() + (isGiftlistCheckAvailability() ? "&ca=true" : "");
        String errorUrl = ((CPSGiftlistFormHandler) getGiftlistFormHandler()).getAddItemsToMaterialListErrorUrl() + getGiftlistId() + (isGiftlistCheckAvailability() ? "&ca=true" : "");

        return checkFormRedirect(successUrl, errorUrl, pRequest, pResponse);
    }

    public boolean handleCreateMaterialListFromFile(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "FileUploadHandler.handleCreateMaterialListFromFile";

        if (null == rrm || rrm.isUniqueRequestEntry(handleMethodName)) {
            TransactionManager tm = getTransactionManager();
            TransactionDemarcation td = new TransactionDemarcation();
            boolean rollback = true;
            try {
                if (tm != null) {
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }

                if (validateNewMaterialList(pRequest)) {
                    String newGiftListId = saveGiftlist();
                    setGiftlistId(newGiftListId);

                    UploadedFile uploadedFile = (UploadedFile) getImportFile();
                    vlogDebug("File: " + uploadedFile);
                    if (uploadedFile != null && uploadedFile.getInputFile() == null) {
                        byte[] buf = new byte[uploadedFile.getFileSize()];
                        uploadedFile.getInputStream().read(buf);
                        uploadedFile = new UploadedFile(uploadedFile.getFilename(), uploadedFile.getContentType(), uploadedFile.getFileSize(), buf);
                    }
                    // Process upload
                    if (uploadedFile != null && uploadedFile.getInputStream() != null) {
                        // Check file format
                        if (checkFileExtensionXlsCsv(uploadedFile)) {
                            rollback = readUpMaterialListNoRedirect(uploadedFile, pRequest, pResponse);
                        } else {
                            // Not xls or xlsx, add error
                            createError(CPSErrorCodes.ERR_INCORRECT_FILE, "import", pRequest.getLocale());
                        }
                    } else {
                        // No file entered, add form exception
                        createError(CPSErrorCodes.ERR_INCORRECT_FILE, "import", pRequest.getLocale());
                    }
                }
            } catch (final Exception e) {
                if (isLoggingError()) {
                    logError(e);
                }
            } finally {
                if (null != rrm) {
                    rrm.removeRequestEntry(handleMethodName);
                }
                if (tm != null) {
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException pE) {
                        if (isLoggingError()) {
                            logError(pE);
                        }
                    }
                }
            }
        }

        return response(pResponse);
    }

    public boolean validateNewMaterialList(DynamoHttpServletRequest pRequest) {
        boolean valid = true;
        String listName = getListName();
        String listDescription = getListDescription();

        if (StringUtils.isBlank(listName)) {
            valid = false;
            createError(CPSErrorCodes.ML_EMPTY_NAME, "import", pRequest.getLocale());
        } else {
            if (listName.length() > 64) {
                valid = false;
                createError(CPSErrorCodes.ML_WRONG_NAME, "import", pRequest.getLocale());
            }
        }
        if (!StringUtils.isBlank(listDescription) && listDescription.length() > 250) {
            valid = false;
            createError(CPSErrorCodes.ML_WRONG_DESCRIPTION, "import", pRequest.getLocale());
        }
        if (valid) {
            List<RepositoryItem> usersGiftlists = (List<RepositoryItem>) getProfile().getPropertyValue(GIFTLISTS);
            if (usersGiftlists != null && usersGiftlists.size() > 0) {
                List<String> listNames = new ArrayList<String>();
                for (RepositoryItem list : usersGiftlists) {
                    listNames.add((String) list.getPropertyValue(EVENT_NAME));
                }
                CPSGiftlistTools tools = (CPSGiftlistTools) giftlistFormHandler.getGiftlistManager().getGiftlistTools();
                if (!tools.validateNewGiftlistNameExist(listName, listNames)) {
                    valid = false;
                    createError(CPSErrorCodes.ML_NAME_EXIST, "import", pRequest.getLocale());
                }
            }
        }

        return valid;
    }
    
    public String saveGiftlist() throws CommerceException {
        giftlistFormHandler.setEventName(mListName);
        giftlistFormHandler.setDescription(mListDescription);
        return giftlistFormHandler.saveGiftlist(getProfile().getRepositoryId());
    }
    
    /**
     * Read the file and extract new gears to add
     *
     * @param pFile    - import list
     * @param pRequest - request
     */
    protected boolean readUpMaterialList(UploadedFile pFile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        vlogDebug("readUpMaterialList.start");

        String successUrl = ((CPSGiftlistFormHandler) getGiftlistFormHandler()).getAddItemsToMaterialListSuccessUrl() + getGiftlistId() + (isGiftlistCheckAvailability() ? "&ca=true" : "");
        String errorUrl = ((CPSGiftlistFormHandler) getGiftlistFormHandler()).getAddItemsToMaterialListErrorUrl() + getGiftlistId() + "&im=true" + (isGiftlistCheckAvailability() ? "&ca=true" : "");

        if (pFile != null) {
            InputStream inp = null;
            try {
                inp = pFile.getInputStream();
                String fileName = pFile.getFilename();
                String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();

                switch (fileExt) {
                    case CSV_EXTENSION:
                        try (BufferedReader br = new BufferedReader(new InputStreamReader(inp))) {
                            String row;
//                            String headerRow = br.readLine();
//                            if (StringUtils.isNotBlank(headerRow)) {
//                                String firstColumn = headerRow.split(",")[0];
//                                if (!firstColumn.equalsIgnoreCase(HEADER_CHECK_MATERIAL_LIST)) {
//                                    vlogDebug("wrong first column name: " + firstColumn + " is not equals to " + HEADER_CHECK_MATERIAL_LIST);
//                                }
//                            }
                            while ((row = br.readLine()) != null) {
                                processMaterialListRowFromCSV(row);
                                if (getFormError()) {
                                    return checkFormRedirect(successUrl, errorUrl, pRequest, pResponse); //stop wasting time and show error
                                }
                            }
                        }
                        break;
                    case XLS_EXTENSION:
                    case XLSX_EXTENSION:
                        // Create workbook
                        Workbook wb = WorkbookFactory.create(inp);
                        Sheet sheet = wb.getSheetAt(0);

                        // Read user data
                        for (Row row : sheet) {
                            processMaterialListRow(row, pRequest.getLocale());
                            if (getFormError()) {
                                return checkFormRedirect(successUrl, errorUrl, pRequest, pResponse); //stop wasting time and show error
                            }
                        }
                        break;
                    default:
                        break;
                }
                // Import the lineItems
                vlogDebug("Products for Material List: " + productIds);

                CPSSessionBean sessionBean = ((CPSGiftlistFormHandler) giftlistFormHandler).getSessionBean();
                cleanUpMaterialListErrors(sessionBean);

                if (!productIds.isEmpty()) {
                    ((CPSGiftlistFormHandler) getGiftlistFormHandler()).setQtyList(qtys.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                    ((CPSGiftlistFormHandler) getGiftlistFormHandler()).setSkuIdsList(productIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                    ((CPSGiftlistFormHandler) getGiftlistFormHandler()).addMaterialListItemsFromFile(productIds, qtys, pRequest, pResponse, getGiftlistId(), isGiftlistCheckAvailability());
                }

                List invalidMLItems = sessionBean.getInvalidMaterialItemsList();
                if (invalidMLItems == null && !localInvalidItems.isEmpty()) {
                    sessionBean.setInvalidMaterialItemsList(localInvalidItems);
                }
                if (invalidMLItems != null) {
                    sessionBean.getInvalidMaterialItemsList().addAll(localInvalidItems);
                }
                if (sessionBean.getInvalidMaterialItemsList() != null && !sessionBean.getInvalidMaterialItemsList().isEmpty()) {
                    createError(CPSErrorCodes.ERR_REORDER_IMPORT_FILE, CPSErrorCodes.ERR_REORDER_IMPORT_FILE, pRequest.getLocale());
                    sessionBean.setShowErrorDivs(false);
                }

                if (!getFormError() && productIds.isEmpty() && localInvalidItems.isEmpty()) {
                    createError(CPSErrorCodes.ERR_REORDER_IMPORT_FILE, CPSErrorCodes.ERR_REORDER_IMPORT_FILE, pRequest.getLocale());
                    sessionBean.setShowErrorDivs(true);
                }

                if (!getFormError()) {
                    sessionBean.setMaterialListSuccessfullyUpdated(true);
                }

                return checkFormRedirect(successUrl, errorUrl, pRequest, pResponse);
            } catch (Exception e) {
                createError(CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, null, pRequest.getLocale());
                if (isLoggingError()) {
                    logError(e);
                }
            } finally {
                try {
                    if (inp != null) {
                        inp.close();
                    }
                } catch (IOException e) {
                    if (isLoggingError()) {
                        logError(e);
                    }
                }
            }
        }

        vlogDebug("readUpMaterialList.end");

        return checkFormRedirect(successUrl, errorUrl, pRequest, pResponse);
    }

    private boolean readUpMaterialListNoRedirect(UploadedFile pFile, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        vlogDebug("readUpMaterialListNoRedirect.start");

        boolean error = true;

        if (pFile != null) {
            InputStream inp = null;
            try {
                inp = pFile.getInputStream();
                String fileName = pFile.getFilename();
                String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();

                switch (fileExt) {
                    case CSV_EXTENSION:
                        try (BufferedReader br = new BufferedReader(new InputStreamReader(inp))) {
                            String row;
//                            String headerRow = br.readLine();
//                            if (StringUtils.isNotBlank(headerRow)) {
//                                String firstColumn = headerRow.split(",")[0];
//                                if (!firstColumn.equalsIgnoreCase(HEADER_CHECK_MATERIAL_LIST)) {
//                                	//skip first row as header
//                                    vlogDebug("wrong first column name: " + firstColumn + " is not equals to " + HEADER_CHECK_MATERIAL_LIST);
////                                    createError(CPSErrorCodes.ML_WRONG_HEADER, CPSErrorCodes.ML_WRONG_HEADER, pRequest.getLocale());
//                                }
//                            }
                            while ((row = br.readLine()) != null) {
                                if (getFormError()) {
                                    //stop wasting time and show error
                                    break;
                                } else {
                                    processMaterialListRowFromCSV(row);
                                }
                            }
                        }
                        break;
                    case XLS_EXTENSION:
                    case XLSX_EXTENSION:
                        // Create workbook
                        Workbook wb = WorkbookFactory.create(inp);
                        Sheet sheet = wb.getSheetAt(0);

                        // Read user data
                        for (Row row : sheet) {
                            if (getFormError()) {
                                //stop wasting time and show error
                                break;
                            } else {
                                processMaterialListRow(row, pRequest.getLocale());
                            }
                        }
                        break;
                    default:
                        break;
                }
                // Import the lineItems
                vlogDebug("Products for Material List: " + productIds);

                if (!productIds.isEmpty()) {
                    CPSSessionBean sessionBean = ((CPSGiftlistFormHandler) giftlistFormHandler).getSessionBean();
                    cleanUpMaterialListErrors(sessionBean);

                    ((CPSGiftlistFormHandler) getGiftlistFormHandler()).setQtyList(qtys.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                    ((CPSGiftlistFormHandler) getGiftlistFormHandler()).setSkuIdsList(productIds.toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", ""));
                    error = ((CPSGiftlistFormHandler) getGiftlistFormHandler()).addMaterialListItemsFromFile(productIds, qtys, pRequest, pResponse, getGiftlistId(), isGiftlistCheckAvailability());

                    List invalidMLItems = sessionBean.getInvalidMaterialItemsList();
                    if (invalidMLItems == null) {
                        sessionBean.setInvalidMaterialItemsList(localInvalidItems);
                    } else {
                        sessionBean.getInvalidMaterialItemsList().addAll(localInvalidItems);
                    }

                    if (!sessionBean.getInvalidMaterialItemsList().isEmpty()) {
                        createError(CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, pRequest.getLocale());
                    }
                } else {
                    if (!getFormError()) {
                        createError(CPSErrorCodes.ERR_REORDER_IMPORT_FILE, null, pRequest.getLocale());
                    }
                    CPSSessionBean sessionBean = ((CPSGiftlistFormHandler) giftlistFormHandler).getSessionBean();
                    cleanUpMaterialListErrors(sessionBean);
                    List invalidMLItems = sessionBean.getInvalidMaterialItemsList();
                    if (invalidMLItems == null) {
                        sessionBean.setInvalidMaterialItemsList(localInvalidItems);
                    } else {
                        sessionBean.getInvalidMaterialItemsList().addAll(localInvalidItems);
                    }

                    if (!sessionBean.getInvalidMaterialItemsList().isEmpty() && sessionBean.getInvalidMaterialItemsList()!=null) {
                        createError(CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, pRequest.getLocale());
                    }
                }

            } catch (Exception e) {
                createError(CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, CPSErrorCodes.ERR_INVALID_FIELDS_FORMAT, pRequest.getLocale());
                if (isLoggingError()) {
                    logError(e);
                }
            } finally {
                try {
                    if (inp != null) {
                        inp.close();
                    }
                } catch (IOException e) {
                    if (isLoggingError()) {
                        logError(e);
                    }
                }
            }
        }

        vlogDebug("readUpMaterialListNoRedirect.end");

        return error;
    }
    
    /**
     * Read the row and extract material list import info
     *
     * @param row    - row to read
     * @param locale - request locale
     */
    protected void processMaterialListRow(Row row, Locale locale) {
        vlogDebug("processMaterialListRow.start - Reading Row: " + row.getRowNum());

        // Check for header row
        // skip first row as header
//        if (row.getRowNum() == 0) {
////            if (!isHeaderRow(row, HEADER_CHECK_MATERIAL_LIST)) {
////                createError(CPSErrorCodes.ML_WRONG_HEADER, CPSErrorCodes.ML_WRONG_HEADER, locale);
////            }
//            vlogDebug("Checking header row.");
//            return;
//        }
        //PAUSE
        // Not header row, proceed
        ArrayList<String> props = new ArrayList<String>();
        props.add(readCell(row, 0)); // CPS ITEM # or CUSTOMER ALIAS # (FROM prodAliasMap on product)
        props.add(readCell(row, 1)); // quantity for this product

        try {
            Long qty = Long.parseLong(props.get(1)); // quantity for this product
			if (qty <= 0 || qty == null) { // If qty is null or <= 0 in the uploaded file, Its added to invalid
            	localInvalidItems.add(props.get(0));
            } else {
                productIds.add(props.get(0));            //CPS ITEM # or CUSTOMER ALIAS # (FROM prodAliasMap on product)
                qtys.add(qty);
            }
        } catch (NumberFormatException e) {
            localInvalidItems.add(props.get(0));
            vlogError("Can't parse value " + props.get(1));
        }

        vlogDebug("processMaterialListRow.end");
    }

    /**
     * Read the row and extract material list import info
     *
     * @param row - row to read
     */
    protected void processMaterialListRowFromCSV(String row) {
        vlogDebug("processMaterialListRowFromCSV.start - Reading Row: " + row);
        if (StringUtils.isBlank(row)) {
            return;
        }
        String[] rowData = row.split(",");
        if (rowData.length < 2) {
            if (rowData.length == 1) {
                localInvalidItems.add(rowData[0]);
            }

            vlogDebug("Row is too short");
            return;
        }

        try {
            Long qty = Long.parseLong(rowData[1]); // quantity for this product
			if (qty <= 0 || qty == null) {	// If qty is null or <= 0 in the uploaded file, Its added to invalid
            	localInvalidItems.add(rowData[0]);
            } else {
                productIds.add(rowData[0]);            //CPS ITEM # or CUSTOMER ALIAS # (FROM prodAliasMap on product)
                qtys.add(qty);            	
            }
        } catch (NumberFormatException e) {
            localInvalidItems.add(rowData[0]);
            vlogError("Can't parse value " + rowData[1]);
        }

        vlogDebug("processMaterialListRowFromCSV.end");
    }
    
//////////////////////end import material list/////////////////////////////////////////////    

    private boolean isAjaxFileUpload(DynamoHttpServletRequest pRequest) {
        return HEADER_PARAMETER_TRUE.equalsIgnoreCase(pRequest.getHeader(HEADER_AJAX_FILE_UPLOAD));
    }

    private boolean response(DynamoHttpServletResponse pResponse) {
        AjaxUtils.addAjaxResponse(this, pResponse, null);
        return false;
    }

    /**
     * Checks files extension to verify correct file type
     *
     * @param file uploaded file
     * @return is valid
     */
    protected boolean checkFileExtensionXls(UploadedFile file) {
        String filename = file.getFilename();
        String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());

        return XLS_EXTENSION.equalsIgnoreCase(extension) || XLSX_EXTENSION.equalsIgnoreCase(extension);
    }

    /**
     * Checks files extension to verify correct file type for import material list
     *
     * @param file uploaded file
     * @return is valid
     */
    protected boolean checkFileExtensionXlsCsv(UploadedFile file) {
        String filename = file.getFilename();
        String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());

        return XLS_EXTENSION.equalsIgnoreCase(extension) || XLSX_EXTENSION.equalsIgnoreCase(extension) || CSV_EXTENSION.equalsIgnoreCase(extension);
    }

    /**
     * Checks files extension to verify correct file type for request a quote
     *
     * @param file uploaded file
     * @return is valid
     */
    protected boolean checkFileExtensionForRequestAQuote(UploadedFile file) {
        String filename = file.getFilename();
        String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
        return XLS_EXTENSION.equalsIgnoreCase(extension) || XLSX_EXTENSION.equalsIgnoreCase(extension) || DOC_EXTENSION.equalsIgnoreCase(extension) || DOCX_EXTENSION.equalsIgnoreCase(extension) || PDF_EXTENSION.equalsIgnoreCase(extension) || JPG_EXTENSION.equalsIgnoreCase(extension) || PNG_EXTENSION.equalsIgnoreCase(extension) || TIF_EXTENSION.equalsIgnoreCase(extension) || GIF_EXTENSION.equalsIgnoreCase(extension);
    }

    public static void copyInputStreamToFile(InputStream source, File destination) throws IOException {
        try {
            FileOutputStream output = FileUtils.openOutputStream(destination);

            try {
                IOUtils.copy(source, output);
                output.close();
            } finally {
                IOUtils.closeQuietly(output);
            }
        } finally {
            IOUtils.closeQuietly(source);
        }
    }

    /**
     * Reads the value of desired cell and returns string value
     *
     * @param row - row to pull cell from
     * @param c   - cell to read
     * @return cell value
     */
    private String readCell(Row row, int c) {
        Cell cell = row.getCell(c);
        if (cell != null) {
            vlogDebug("readCell.start - reading cell: {0}", cell.getColumnIndex());
            if (cell.getCellType() == Cell.CELL_TYPE_STRING || cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
                //return checkScientific(cell.getStringCellValue());
                return cell.getStringCellValue().trim();
            } else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                return checkScientific(((Double) cell.getNumericCellValue()).toString().trim());
            } else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
                return ((Boolean) cell.getBooleanCellValue()).toString().trim();
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * @param cellValue string cell value could be scientific notation
     * @return cell value hopefully corrected
     */
    private String checkScientific(String cellValue) {
        vlogDebug("CELL VALUE FOR SCIENCE:" + cellValue);
        String digitRegex = "\\d";
        try {
            String valueFormatted = (new BigDecimal(cellValue)).toPlainString();
            vlogDebug("ValueFormatted: " + valueFormatted);
            valueFormatted = valueFormatted.replaceAll("\\.0", "");
            vlogDebug("FOR SCIENCE:" + valueFormatted);
            return valueFormatted;
        } catch (NumberFormatException nfe) {
            vlogDebug("Someone is Trying to Break the World with Scientific Notation and Letters");
        }
        return cellValue;
    }

    /**
     * Check if first cell in row has header from template,
     * if so, return true, otherwise not a header row
     *
     * @param row         - row to check
     * @param headerCheck - String to check against
     * @return - if is header row
     */
    private boolean isHeaderRow(Row row, String headerCheck) {
        return (headerCheck.equalsIgnoreCase(row.getCell(0).getStringCellValue()));
    }

    /**
     * Loops through list of errors and adds for formExceptions
     *
     * @param errors - errors to add
     * @param locale - locale
     */
//    protected void checkErrorMessages(Map<String, String> errors, Locale locale) {
//        vlogDebug("Adding errors");
//        List<String> addedExceptions = new ArrayList<String>();
//
//        if (errors != null && !errors.isEmpty()) {
//            for (Entry<String, String> error : errors.entrySet()) {
//                if (!addedExceptions.contains(error.getValue())) {
//                    vlogDebug("New error: " + error.getKey() + " - " + error.getValue());
//                    createError(error.getValue(), error.getKey(), locale);
//                    addedExceptions.add(error.getValue());
//                } else {
//                    vlogDebug("Existing error: " + error.getKey() + " - " + error.getValue());
//                    addFormException(new DropletException("", error.getKey()));
//                }
//            }
//        }
//    }

    /**
     * Adds error to formErrors
     *
     * @param msg    message to add
     * @param pty    property to highlight
     * @param locale request locale
     */
    private void createError(String msg, String pty, Locale locale) {
        CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, locale, pty);
    }
    
    private void cleanUpMaterialListErrors(CPSSessionBean sessionBean) {
        sessionBean.setMaterialDuplicates(null);
        sessionBean.setInvalidMaterialItemsList(null);
        sessionBean.setHasMaterialDuplicates(false);
    }
    
    /**
     * To cleanup material lists which is stored in the session bean
     * 
     * @param pRequest
     * @param pResponse
     * @return
     * @throws IOException
     * @throws ServletException
     */
    public boolean handleCleanUpMaterialList(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws IOException, ServletException {
    	CPSSessionBean sessionBean = ((CPSGiftlistFormHandler) giftlistFormHandler).getSessionBean();
        cleanUpMaterialListErrors(sessionBean);
    	return false;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public CPSProfileTools getProfileTools() {
        return profileTools;
    }

    public void setProfileTools(CPSProfileTools profileTools) {
        this.profileTools = profileTools;
    }

    public Object getQuickOrderFile() {
        return quickOrderFile;
    }

    public void setQuickOrderFile(Object quickOrderFile) {
        this.quickOrderFile = quickOrderFile;
    }

    public String getReadQuickOrderFileSuccessURL() {
        return readQuickOrderFileSuccessURL;
    }

    public void setReadQuickOrderFileSuccessURL(
            String readQuickOrderFileSuccessURL) {
        this.readQuickOrderFileSuccessURL = readQuickOrderFileSuccessURL;
    }

    public String getReadQuickOrderFileFailURL() {
        return readQuickOrderFileFailURL;
    }

    public void setReadQuickOrderFileFailURL(String readQuickOrderFileFailURL) {
        this.readQuickOrderFileFailURL = readQuickOrderFileFailURL;
    }

    public String getReadQuickOrderInvalidURL() {
        return readQuickOrderInvalidURL;
    }

    public void setReadQuickOrderInvalidURL(String readQuickOrderInvalidURL) {
        this.readQuickOrderInvalidURL = readQuickOrderInvalidURL;
    }

    public String getProductsForCart() {
        return productsForCart;
    }

    public void setProductsForCart(String productsForCart) {
        this.productsForCart = productsForCart;
    }

    public String getQuantitiesForCart() {
        return quantitiesForCart;
    }

    public void setQuantitiesForCart(String quantitiesForCart) {
        this.quantitiesForCart = quantitiesForCart;
    }

    public void setProductCatalog(MutableRepository productCatalog) {
        this.productCatalog = productCatalog;
    }

    public MutableRepository getProductCatalog() {
        return productCatalog;
    }

    public String getGiftlistId() {
        return giftlistId;
    }

    public void setGiftlistId(String giftlistId) {
        this.giftlistId = giftlistId;
    }

    public GiftlistFormHandler getGiftlistFormHandler() {
        return giftlistFormHandler;
    }

    public void setGiftlistFormHandler(GiftlistFormHandler giftlistFormHandler) {
        this.giftlistFormHandler = giftlistFormHandler;
    }

    public String getReadFileSuccessURL() {
        return readFileSuccessURL;
    }

    public void setReadFileSuccessURL(String readFileSuccessURL) {
        this.readFileSuccessURL = readFileSuccessURL;
    }

    public String getReadFileErrorURL() {
        return readFileErrorURL;
    }

    public void setReadFileErrorURL(String readFileErrorURL) {
        this.readFileErrorURL = readFileErrorURL;
    }

    public Object getImportFile() {
        return importFile;
    }

    public void setImportFile(Object importFile) {
        this.importFile = importFile;
    }

    public Object getUserFile() {
        return userFile;
    }

    public void setUserFile(Object userFile) {
        this.userFile = userFile;
    }

    public String getReadUserFileSuccessURL() {
        return readUserFileSuccessURL;
    }

    public void setReadUserFileSuccessURL(String readUserFileSuccessURL) {
        this.readUserFileSuccessURL = readUserFileSuccessURL;
    }

    public MultiUserAddFormHandler getMultiUserAddFormHandler() {
        return multiUserAddFormHandler;
    }

    public void setMultiUserAddFormHandler(MultiUserAddFormHandler multiUserAddFormHandler) {
        this.multiUserAddFormHandler = multiUserAddFormHandler;
    }

    public CPSCartModifierFormHandler getCartForm() {
        return cartForm;
    }

    public void setCartForm(CPSCartModifierFormHandler cartForm) {
        this.cartForm = cartForm;
    }

    public Object getRequestQuoteFile() {
        return requestQuoteFile;
    }

    public void setRequestQuoteFile(Object requestQuoteFile) {
        this.requestQuoteFile = requestQuoteFile;
    }

    public String getRequestAQuoteSuccessURL() {
        return requestAQuoteSuccessURL;
    }

    public void setRequestAQuoteSuccessURL(String requestAQuoteSuccessURL) {
        this.requestAQuoteSuccessURL = requestAQuoteSuccessURL;
    }

    public String getRequestAQuoteFailURL() {
        return requestAQuoteFailURL;
    }

    public void setRequestAQuoteFailURL(String requestAQuoteFailURL) {
        this.requestAQuoteFailURL = requestAQuoteFailURL;
    }

    public String getRequestQuoteMessage() {
        return requestQuoteMessage;
    }

    public void setRequestQuoteMessage(String requestQuoteMessage) {
        this.requestQuoteMessage = requestQuoteMessage;
    }

    public CommonEmailSender getCommonEmailSender() {
        return commonEmailSender;
    }

    public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
        this.commonEmailSender = commonEmailSender;
    }

    public Map<String, List<String>> getErrorMap() {
        return errorMap;
    }

    public void setErrorMap(Map<String, List<String>> errorMap) {
        this.errorMap = errorMap;
    }

    public boolean isGiftlistCheckAvailability() {
        return mGiftlistCheckAvailability;
    }

    public void setGiftlistCheckAvailability(boolean pGiftlistCheckAvailability) {
        this.mGiftlistCheckAvailability = pGiftlistCheckAvailability;
    }

    public String getListName() {
        return mListName;
    }

    public void setListName(String pListName) {
        mListName = pListName;
    }

    public String getListDescription() {
        return mListDescription;
    }

    public void setListDescription(String pListDescription) {
        mListDescription = pListDescription;
    }

    public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
        mRepeatingRequestMonitor = pRepeatingRequestMonitor;
    }

    public RepeatingRequestMonitor getRepeatingRequestMonitor() {
        return mRepeatingRequestMonitor;
    }

    public TransactionProcessor getTransactionProcessor() {
        return mTransactionProcessor;
    }

    public void setTransactionProcessor(TransactionProcessor pTransactionProcessor) {
        mTransactionProcessor = pTransactionProcessor;
    }

    public MaterialListValidator getValidator() {
        return mValidator;
    }

    public void setValidator(MaterialListValidator pValidator) {
        mValidator = pValidator;
    }

    public void setTransactionManager(TransactionManager pTransactionManager) {
        this.mTransactionManager = pTransactionManager;
    }

    public TransactionManager getTransactionManager() {
        return this.mTransactionManager;
    }

	/**
	 * @return the emptyFileErrorMessage
	 */
	public String getEmptyFileErrorMessage() {
		return emptyFileErrorMessage;
	}

	/**
	 * @param emptyFileErrorMessage the emptyFileErrorMessage to set
	 */
	public void setEmptyFileErrorMessage(String emptyFileErrorMessage) {
		this.emptyFileErrorMessage = emptyFileErrorMessage;
	}

    /**
     * @return the bulkUploadFromBcc
     */
    public boolean isBulkUploadFromBcc() {
        return bulkUploadFromBcc;
    }

    /**
     * @param bulkUploadFromBcc
     *            the bulkUploadFromBcc to set
     */
    public void setBulkUploadFromBcc(boolean bulkUploadFromBcc) {
        this.bulkUploadFromBcc = bulkUploadFromBcc;
    }

}