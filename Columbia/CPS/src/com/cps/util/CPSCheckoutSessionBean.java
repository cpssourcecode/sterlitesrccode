package com.cps.util;

import java.util.ArrayList;

import atg.nucleus.GenericService;

public class CPSCheckoutSessionBean extends GenericService {
	
	private boolean orderPlaced;
	
	public boolean isOrderPlaced() {
		return orderPlaced;
	}

	public void setOrderPlaced(boolean orderPlaced) {
		this.orderPlaced = orderPlaced;
	}

}
