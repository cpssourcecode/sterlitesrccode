package com.cps.util;

/**
 * Utility class for retry logic
 */
public final class Retry {

    private static final int RETRIES_NUMBER = 5;
    private static final int WAIT_TIME = 3000;

    private int mRetriesNumberLeft;
    private int mWaitTime;

    public Retry() {
        this(RETRIES_NUMBER, WAIT_TIME);
    }

    public Retry(int pRetriesNumber, int pWaitTime) {
        mRetriesNumberLeft = pRetriesNumber;
        mWaitTime = pWaitTime;
    }

    public int getRetriesNumberLeft() {
        return mRetriesNumberLeft;
    }

    public void setRetriesNumberLeft(int pRetriesNumberLeft) {
        mRetriesNumberLeft = pRetriesNumberLeft;
    }

    /**
     * @return true if there are tries left
     */
    public boolean shouldRetry() {
        return mRetriesNumberLeft > 0;
    }

    public void errorOccurred() {
        mRetriesNumberLeft--;
        waitUntilNextTry();
    }

    public void cancel() {
        setRetriesNumberLeft(0);
    }

    public long getWaitTime() {
        return mWaitTime;
    }

    private void waitUntilNextTry() {
        try {
            Thread.sleep(getWaitTime());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

}
