package com.cps.scheduler;

import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.states.OrderStates;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;
import com.cps.commerce.approval.ApprovalManager;
import com.cps.email.CommonEmailSender;

import javax.transaction.TransactionManager;
import java.util.Date;
import java.util.List;

public class ApprovalRequestReminderService extends SingletonSchedulableService {

	private static final String ORDER_STATE_PENDING_APPROVAL = "PENDING_APPROVAL";

	private CommonEmailSender mEmailSender;

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	private ApprovalManager mApprovalManager;

	public ApprovalManager getApprovalManager() {
		return mApprovalManager;
	}

	public void setApprovalManager(ApprovalManager pApprovalManager) {
		mApprovalManager = pApprovalManager;
	}

	private OrderStates mOrderStates;

	public OrderStates getOrderStates() {
		return mOrderStates;
	}

	public void setOrderStates(OrderStates pOrderStates) {
		mOrderStates = pOrderStates;
	}

	private TransactionManager mTransactionManager;

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	private boolean mEnabled;

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	private String mOrderId;

	public String getOrderId() {
		return mOrderId;
	}

	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	public OrderManager getOrderManager() {
		return getApprovalManager().getOrderManager();
	}

	@Override
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {

		if (isEnabled()) {
			vlogDebug(new StringBuilder("Start: ").append(new Date()).toString());
			processApprovalRequests();
			vlogDebug(new StringBuilder("End: ").append(new Date()).toString());
		}

	}

	/**
	 * force run job once
	 */
	public void runJobOnce() {
		doScheduledTask(null, null);
	}

	private void sendReminder(Order pOrder) {

		List<String> approverIds = pOrder.getAuthorizedApproverIds();
		for (String id : approverIds) {
			try {
				RepositoryItem approver = getOrderManager().getOrderTools().getProfileTools().getProfileItem(id);
				RepositoryItem buyer = getOrderManager().getOrderTools().getProfileTools().getProfileItem(pOrder.getProfileId());
				if (approver != null) {
					getEmailSender().sendOrderApprovalPendingEmail(approver, buyer, pOrder);
				}
			} catch (RepositoryException re) {
				if (isLoggingError()) {
					logError(re);
				}
			}
		}

	}

	private void processApprovalRequest(RepositoryItem pApprovalRequest) throws Exception {
		Order order = getOrderManager().loadOrder(pApprovalRequest.getRepositoryId());
		if (order != null) {
			if (ORDER_STATE_PENDING_APPROVAL.equalsIgnoreCase(getOrderStates().getStateString(order.getState()))) {
				sendReminder(order);
				getApprovalManager().updateApprovalRequest(pApprovalRequest);
			} else {
			    getApprovalManager().deleteApprovalRequest(pApprovalRequest.getRepositoryId());
			}
		}
	}

	private boolean processApprovalRequestTransaction(RepositoryItem pApprovalRequest) {

		boolean result = false;
		try {
			TransactionManager tm = getTransactionManager();
			if (null != tm) {
				TransactionDemarcation td = new TransactionDemarcation();
				boolean rollback = true;
				try {
					td.begin(tm, TransactionDemarcation.REQUIRED);
					processApprovalRequest(pApprovalRequest);
					rollback = false;
				} finally {
					td.end(rollback);
				}
			} else {
				processApprovalRequest(pApprovalRequest);
			}
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("processApprovalRequest: Exception while processing order", e);
			}
		}
		return result;

	}

	public void processApprovalRequests() {
		RepositoryItem[] items = getApprovalManager().getApprovalRequests();
		if (items != null && items.length > 0) {
			for (RepositoryItem item: items) {
				processApprovalRequestTransaction(item);
			}
		}
	}

	public void processApprovalRequestById() {
		if (isEnabled() && !StringUtils.isBlank(getOrderId())) {
			try {
				RepositoryItem request = getApprovalManager().getOrderRepository().getItem(getOrderId(), "approvalRequest");
				if (request != null) {
					processApprovalRequest(request);
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
	}

}
