package com.cps.scheduler;

import java.util.HashMap;
import java.util.List;

import com.cps.scheduler.manager.SalesOrderReportManager;
import com.cps.util.CPSConstants;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

/**
 * @author Manikandan
 *
 */
public class SalesOrderReportScheduler extends GenericService implements Schedulable {

	private int jobId;
	private String jobName;
	private boolean enabled;

	private Scheduler scheduler;
	private Schedule schedule;

	private SalesOrderReportManager salesOrderReportManager;
	private List<String> salesOrderReportEmailsTo;
	private String environment;
	private String reportHostName;
	private boolean enableDownloadLink;
	private boolean enabledOneDayReport;

	@Override
	public void doStartService() throws ServiceException {
		ScheduledJob scheduledJob = new ScheduledJob(getJobName(), getJobName(), getAbsoluteName(), getSchedule(), this,
				ScheduledJob.REUSED_THREAD);
		jobId = getScheduler().addScheduledJob(scheduledJob);
	}

	@Override
	public void doStopService() throws ServiceException {
		getScheduler().removeScheduledJob(jobId);
	}

	@Override
	public void performScheduledTask(Scheduler var1, ScheduledJob var2) {
		if (isEnabled()) {
			vlogDebug("Schedulable Service is started for sales report of order");
			generateSalesOrderReport();
			vlogDebug("Schedulable Service is completed for sales report of order");
		}

	}

	/**
	 * Generating sales order report
	 */
	public void generateSalesOrderReport() {

		HashMap<String, Object> emailParams = new HashMap<String, Object>();
		emailParams.put(CPSConstants.EMAIL, getSalesOrderReportEmailsTo());
		emailParams.put(CPSConstants.ENABLE_DOWNLOAD_LINK, isEnableDownloadLink());
		emailParams.put(CPSConstants.HOST_NAME, getReportHostName());
		emailParams.put(CPSConstants.ENABLED_ONE_DAY_REPORT, isEnabledOneDayReport());
		getSalesOrderReportManager().generateSalesOrderReport(emailParams, getEnvironment());

	}
	
	/**
	 * Generating sales order report for one day
	 */
	public void generateSalesOrderReportForOneDay() {
		HashMap<String, Object> emailParams = new HashMap<String, Object>();
		emailParams.put(CPSConstants.EMAIL, getSalesOrderReportEmailsTo());
		emailParams.put(CPSConstants.ENABLE_DOWNLOAD_LINK, isEnableDownloadLink());
		emailParams.put(CPSConstants.HOST_NAME, getReportHostName());
		emailParams.put(CPSConstants.ENABLED_ONE_DAY_REPORT, true);
		getSalesOrderReportManager().generateSalesOrderReport(emailParams, getEnvironment());
	}

	/**
	 * @return the jobId
	 */
	public int getJobId() {
		return jobId;
	}

	/**
	 * @param jobId
	 *            the jobId to set
	 */
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName
	 *            the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the scheduler
	 */
	public Scheduler getScheduler() {
		return scheduler;
	}

	/**
	 * @param scheduler
	 *            the scheduler to set
	 */
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	/**
	 * @return the schedule
	 */
	public Schedule getSchedule() {
		return schedule;
	}

	/**
	 * @param schedule
	 *            the schedule to set
	 */
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	/**
	 * @return the salesOrderReportManager
	 */
	public SalesOrderReportManager getSalesOrderReportManager() {
		return salesOrderReportManager;
	}

	/**
	 * @param salesOrderReportManager
	 *            the salesOrderReportManager to set
	 */
	public void setSalesOrderReportManager(SalesOrderReportManager salesOrderReportManager) {
		this.salesOrderReportManager = salesOrderReportManager;
	}

	/**
	 * @return the salesOrderReportEmailsTo
	 */
	public List<String> getSalesOrderReportEmailsTo() {
		return salesOrderReportEmailsTo;
	}

	/**
	 * @param salesOrderReportEmailsTo
	 *            the salesOrderReportEmailsTo to set
	 */
	public void setSalesOrderReportEmailsTo(List<String> salesOrderReportEmailsTo) {
		this.salesOrderReportEmailsTo = salesOrderReportEmailsTo;
	}

	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}

	/**
	 * @param environment
	 *            the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	/**
	 * @return the reportHostName
	 */
	public String getReportHostName() {
		return reportHostName;
	}

	/**
	 * @param reportHostName
	 *            the reportHostName to set
	 */
	public void setReportHostName(String reportHostName) {
		this.reportHostName = reportHostName;
	}

	/**
	 * @return the enableDownloadLink
	 */
	public boolean isEnableDownloadLink() {
		return enableDownloadLink;
	}

	/**
	 * @param enableDownloadLink
	 *            the enableDownloadLink to set
	 */
	public void setEnableDownloadLink(boolean enableDownloadLink) {
		this.enableDownloadLink = enableDownloadLink;
	}

	/**
	 * @return the enabledOneDayReport
	 */
	public boolean isEnabledOneDayReport() {
		return enabledOneDayReport;
	}

	/**
	 * @param enabledOneDayReport the enabledOneDayReport to set
	 */
	public void setEnabledOneDayReport(boolean enabledOneDayReport) {
		this.enabledOneDayReport = enabledOneDayReport;
	}

}
