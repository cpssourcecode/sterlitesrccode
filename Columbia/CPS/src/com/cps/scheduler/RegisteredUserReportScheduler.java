package com.cps.scheduler;

import java.util.HashMap;
import java.util.List;

import com.cps.scheduler.manager.RegisteredUserReportManager;
import com.cps.util.CPSConstants;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

/**
 * @author Manikandan
 *
 */
public class RegisteredUserReportScheduler extends GenericService implements Schedulable {

	private int jobId;
	private String jobName;
	private boolean enabled;

	private Scheduler scheduler;
	private Schedule schedule;
	private RegisteredUserReportManager registeredUserReportManager;
	
	private String reportHostName;
	private List<String> registeredUserReportEmail;
    private String inactiveUserReportEmail;
    private boolean enableDownloadLink;
    private String environment;

	@Override
	public void doStartService() throws ServiceException {

		ScheduledJob scheduledJob = new ScheduledJob(getJobName(), getJobName(), getAbsoluteName(), getSchedule(), this,
				ScheduledJob.REUSED_THREAD);

		jobId = getScheduler().addScheduledJob(scheduledJob);
	}

	@Override
	public void doStopService() throws ServiceException {

		getScheduler().removeScheduledJob(jobId);
	}

	@Override
	public void performScheduledTask(Scheduler scheduler, ScheduledJob job) {
		if (isEnabled()) {
			vlogDebug("Schedulable Service starts for registered users report...");
			generateNewUsersReport();
			generateNotActivatedNewUsersReport();
			generateReminderEmailForInactiveUsers();
			generateRegisteredUserCSV();
			vlogDebug("Schedulable Service end for registered users report...");
		} else {
			vlogInfo("{0} service is not enabled", getJobName());
		}
	}

	public void generateRegisteredUserCSV() {
	    getRegisteredUserReportManager().registeredOrganizationReport();
	    
	}

	public void generateReminderEmailForInactiveUsers() {
		getRegisteredUserReportManager().generateEmailReminderLink();
	}
	
	/**
	 * To generate new users report
	 */
	public void generateNewUsersReport() {
		HashMap<String, Object> emailParams = new HashMap<String, Object>();
		emailParams.put(CPSConstants.EMAIL, getRegisteredUserReportEmail());
		emailParams.put(CPSConstants.ENABLE_DOWNLOAD_LINK, isEnableDownloadLink());
		emailParams.put(CPSConstants.HOST_NAME, getReportHostName());
		getRegisteredUserReportManager().generateRegisteredNewUsersReport(emailParams, getEnvironment());
	}

	/**
	 * To generate not activated new users report 
	 */
	public void generateNotActivatedNewUsersReport() {
		HashMap<String, Object> emailParams = new HashMap<String, Object>();
		emailParams.put(CPSConstants.EMAIL, getInactiveUserReportEmail());
		emailParams.put(CPSConstants.ENABLE_DOWNLOAD_LINK, isEnableDownloadLink());
		emailParams.put(CPSConstants.HOST_NAME, getReportHostName());
		getRegisteredUserReportManager().generateNotActivatedUsersReport(emailParams, getEnvironment());
	}

	/**
	 * @return the jobId
	 */
	public int getJobId() {
		return jobId;
	}

	/**
	 * @param jobId
	 *            the jobId to set
	 */
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName
	 *            the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the scheduler
	 */
	public Scheduler getScheduler() {
		return scheduler;
	}

	/**
	 * @param scheduler
	 *            the scheduler to set
	 */
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	/**
	 * @return the schedule
	 */
	public Schedule getSchedule() {
		return schedule;
	}

	/**
	 * @param schedule
	 *            the schedule to set
	 */
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	/**
	 * @return the registeredUserReportManager
	 */
	public RegisteredUserReportManager getRegisteredUserReportManager() {
		return registeredUserReportManager;
	}

	/**
	 * @param registeredUserReportManager
	 *            the registeredUserReportManager to set
	 */
	public void setRegisteredUserReportManager(RegisteredUserReportManager registeredUserReportManager) {
		this.registeredUserReportManager = registeredUserReportManager;
	}

	

	/**
	 * @return the reportHostName
	 */
	public String getReportHostName() {
		return reportHostName;
	}

	/**
	 * @param reportHostName the reportHostName to set
	 */
	public void setReportHostName(String reportHostName) {
		this.reportHostName = reportHostName;
	}

	/**
	 * @return the registeredUserReportEmail
	 */
	public List<String> getRegisteredUserReportEmail() {
		return registeredUserReportEmail;
	}

	/**
	 * @param registeredUserReportEmail the registeredUserReportEmail to set
	 */
	public void setRegisteredUserReportEmail(List<String> registeredUserReportEmail) {
		this.registeredUserReportEmail = registeredUserReportEmail;
	}

	/**
	 * @return the inactiveUserReportEmail
	 */
	public String getInactiveUserReportEmail() {
		return inactiveUserReportEmail;
	}

	/**
	 * @param inactiveUserReportEmail the inactiveUserReportEmail to set
	 */
	public void setInactiveUserReportEmail(String inactiveUserReportEmail) {
		this.inactiveUserReportEmail = inactiveUserReportEmail;
	}

	/**
	 * @return the enableDownloadLink
	 */
	public boolean isEnableDownloadLink() {
		return enableDownloadLink;
	}

	/**
	 * @param enableDownloadLink the enableDownloadLink to set
	 */
	public void setEnableDownloadLink(boolean enableDownloadLink) {
		this.enableDownloadLink = enableDownloadLink;
	}

	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}

	/**
	 * @param environment the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}

}
