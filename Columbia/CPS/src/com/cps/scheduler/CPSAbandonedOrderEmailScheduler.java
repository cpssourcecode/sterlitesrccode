package com.cps.scheduler;

import java.util.Date;

import com.cps.commerce.approval.ApprovalManager;
import com.cps.email.CommonEmailSender;

import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

/**
 * @author Vignesh
 *
 */
public class CPSAbandonedOrderEmailScheduler extends SingletonSchedulableService {
	private CommonEmailSender mEmailSender;
	private boolean mEnabled;
	private ApprovalManager mApprovalManager;
	private String mPendingAbandentOrderQuery;
	private int mEmailInterval;

	@Override
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {
		if (isEnabled()) {
			vlogDebug(new StringBuilder("Start: ").append(new Date()).toString());
			processAbandonedOrderEmail();
			vlogDebug(new StringBuilder("End: ").append(new Date()).toString());
		}
	}

	/**
	 * Process abandoned order email 
	 */
	public void processAbandonedOrderEmail() {
		getApprovalManager().getAbandonedOrderEmails(getPendingAbandentOrderQuery(), getEmailInterval());
	}

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender mEmailSender) {
		this.mEmailSender = mEmailSender;
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean mEnabled) {
		this.mEnabled = mEnabled;
	}

	public ApprovalManager getApprovalManager() {
		return mApprovalManager;
	}

	public void setApprovalManager(ApprovalManager mApprovalManager) {
		this.mApprovalManager = mApprovalManager;
	}

	public String getPendingAbandentOrderQuery() {
		return mPendingAbandentOrderQuery;
	}

	public void setPendingAbandentOrderQuery(String mPendingAbandentOrderQuery) {
		this.mPendingAbandentOrderQuery = mPendingAbandentOrderQuery;
	}

	public int getEmailInterval() {
		return mEmailInterval;
	}

	public void setEmailInterval(int mEmailInterval) {
		this.mEmailInterval = mEmailInterval;
	}

}
