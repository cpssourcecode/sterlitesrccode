package com.cps.scheduler;

import atg.commerce.order.Order;
import atg.commerce.states.OrderStates;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;
import com.cps.commerce.approval.ApprovalManager;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.email.CommonEmailSender;

import javax.transaction.TransactionManager;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class RejectApprovalRequestsService extends SingletonSchedulableService {

	private static final String RETRIEVE_ORDERS_FOR_PROCESSING_RQL_QUERY = "state =?0 AND submittedDate <=?1";
	private static final String ORDER_STATE_PENDING_APPROVAL = "PENDING_APPROVAL";
	private static final String ORDER_STATE_FAILED_APPROVAL = "FAILED_APPROVAL";

	private ApprovalManager mApprovalManager;

	public ApprovalManager getApprovalManager() {
		return mApprovalManager;
	}

	public void setApprovalManager(ApprovalManager pApprovalManager) {
		mApprovalManager = pApprovalManager;
	}

	private CommonEmailSender mEmailSender;

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	private OrderStates mOrderStates;

	public OrderStates getOrderStates() {
		return mOrderStates;
	}

	public void setOrderStates(OrderStates pOrderStates) {
		mOrderStates = pOrderStates;
	}

	private TransactionManager mTransactionManager;

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	private CPSOrderManager mOrderManager;

	public CPSOrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(CPSOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	private boolean mEnabled;

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	private String mOrderId;

	public String getOrderId() {
		return mOrderId;
	}

	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	private int mDays = 7;

	public int getDays() {
		return mDays;
	}

	public void setDays(int pDays) {
		mDays = pDays;
	}

	@Override
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {

		if (isEnabled()) {
			vlogDebug(new StringBuilder("Start: ").append(new Date()).toString());
			processOrders();
			vlogDebug(new StringBuilder("End: ").append(new Date()).toString());
		}

	}

	/**
	 * force run job once
	 */
	public void runJobOnce() {
		doScheduledTask(null, null);
	}

	public void rejectOrderById() {
		if (isEnabled() && !StringUtils.isBlank(getOrderId())) {
			try {
				final Order order = getOrderManager().loadOrder(getOrderId());
				synchronized (order) {
					rejectOrder(order);
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
	}

	private void processOrders() {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DAY_OF_YEAR, -getDays());

		Object[] params = new Object[2];
		params[0] = ORDER_STATE_PENDING_APPROVAL;
		params[1] = new Timestamp(currentDate.getTime().getTime());
		RepositoryItem[] orders = getOrderManager().retrieveOrders(RETRIEVE_ORDERS_FOR_PROCESSING_RQL_QUERY, params);
		if (orders != null && orders.length > 0) {
			for (RepositoryItem order: orders) {
				processOrderInTransaction(order);
			}
		}
	}

	private void sendRejectEmail(Order pOrder) {
		try {
			RepositoryItem profile = getOrderManager().getOrderTools().getProfileTools().getProfileItem(pOrder.getProfileId());
			if (profile != null) {
				getEmailSender().sendOrderApprovalExpiredEmail(profile, pOrder);
			}
		} catch (RepositoryException re) {
			if (isLoggingError()) {
				logError(re);
			}
		}
	}

	private boolean processOrderInTransaction(RepositoryItem pOrder) {
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = true;
		try {
			final Order order = getOrderManager().loadOrder(pOrder.getRepositoryId());
			if (null != tm) {
				td.begin(tm, TransactionDemarcation.REQUIRED);
			}
			synchronized (order) {
				rejectOrder(order);
				rollback = false;
			}
			// send reject email
			sendRejectEmail(order);
		} catch (Exception e) {
			if (isLoggingError()) {
				logError("processOrder: Exception while processing order", e);
			}
		} finally {
			try {
				td.end(rollback);
			} catch (TransactionDemarcationException pE) {
				if (isLoggingError()){
					logError(pE);
				}
			}
		}

		return !rollback;
	}

	private void rejectOrder(Order pOrder) throws Exception {
		pOrder.getApproverMessages().add("The order was rejected automatically.");
		pOrder.setState(getOrderStates().getStateValue(ORDER_STATE_FAILED_APPROVAL.toLowerCase()));
		pOrder.setStateDetail("The order was rejected automatically.");
		getApprovalManager().resetAuthorization(pOrder);
		getOrderManager().updateOrder(pOrder);
	}

}
