package com.cps.scheduler;

import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

/**
 * @author Alexey Meleshko.
 */
public class SavedCartReminderScheduler extends GenericService implements Schedulable {
    private static final String SCHEDULER_NAME = "Saved Cart Scheduler";
    private static final String ORDER_STATE_INCOMPLETE = "INCOMPLETE";
    private static final String INCOMPLETE_ORDERS_RQL_QUERY = "state =?0 AND lastModifiedDate >= ?1 AND lastModifiedDate <= ?2 AND COUNT (commerceItems) > 0 AND explicitlySaved = true";

    private boolean mEnabled;
    private int jobId;
    private int savedCartDateTimeFrom;

    private Scheduler mScheduler;
    private Schedule mSchedule;

    /**
     * email sender
     */
    private CommonEmailSender mEmailSender;
    private MutableRepository mOrderRepository;
    private CPSProfileTools mProfileTools;
    private OrderManager mOrderManager;

    @Override
    public void doStartService() throws ServiceException {
        ScheduledJob scheduledJob = new ScheduledJob(SCHEDULER_NAME, SCHEDULER_NAME,
                        getAbsoluteName(), getSchedule(), this,
                        ScheduledJob.REUSED_THREAD);

        jobId = getScheduler().addScheduledJob(scheduledJob);
    }

    @Override
    public void doStopService() throws ServiceException {
        getScheduler().removeScheduledJob(jobId);
    }

    @Override
    public void performScheduledTask(Scheduler scheduler, ScheduledJob scheduledJob) {
        try {
            if (isLoggingDebug()) {
                logDebug("SavedCartReminderScheduler - start");
            }

            if (isEnabled()) {
                processSavedCarts();
            } else {
                if (isLoggingDebug()) {
                    logDebug("SavedCartReminderScheduler doesn't process Saved Carts");
                }
            }

            if (isLoggingDebug()) {
                logDebug("SavedCartReminderScheduler - end");
            }
        } catch (Exception ex) {
            logError(ex);
        }
    }

    private void processSavedCarts() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, getSavedCartDateTimeFrom());
        Date dateTimeFrom = calendar.getTime();

        calendar.add(Calendar.HOUR_OF_DAY, -1);
        Date dateTimeTo = calendar.getTime();

        Object[] params = new Object[3];
        params[0] = ORDER_STATE_INCOMPLETE;
        params[1] = dateTimeTo;
        params[2] = dateTimeFrom;
        Map<String, String> profiles = new HashMap<>();
        try {
            RepositoryView orderView = getOrderRepository().getView(ITEM_DESCRIPTOR_ORDER);
            RqlStatement rqlStatement = RqlStatement.parseRqlStatement(INCOMPLETE_ORDERS_RQL_QUERY);
            RepositoryItem[] results = rqlStatement.executeQueryUncached(orderView, params);
            if ((results != null) && (results.length > 0)) {
                // instantiate arrayList object to add to
                for (RepositoryItem orderRepositoryItem : results) {
                    try {
                        Order order = getOrderManager().loadOrder(orderRepositoryItem.getRepositoryId());
                        MutableRepositoryItem profileItem = getProfileTools().getProfileItem(order.getProfileId());
                        String email = (String) profileItem.getPropertyValue(CPSConstants.EMAIL);
                        String firstName = (String) profileItem.getPropertyValue(CPSConstants.FIRST_NAME);
                        String lastName = (String) profileItem.getPropertyValue(CPSConstants.LAST_NAME);
                        if (lastName != null) {
                            profiles.put(email, firstName + " " + lastName);
                        } else {
                            profiles.put(email, firstName);
                        }
                    } catch (CommerceException ce) {
                        if (isLoggingError()) {
                            logError(ce);
                        }
                    }
                }
                getEmailSender().sendSavedCartReminder(profiles);
            }
        } catch (RepositoryException re) {
            if (isLoggingError()) {
                logError(re);
            }
        }
    }

    public void runJobOnce() {
        performScheduledTask(null, null);
    }

    public void processOrderOnce() {
        try {
            processSavedCarts();
        } catch (Exception e) {
            logError(e);
        }
    }

    public boolean isEnabled() {
        return mEnabled;
    }

    public boolean getEnabled() {
        return isEnabled();
    }

    public void setEnabled(boolean pEnabled) {
        mEnabled = pEnabled;
    }

    public Scheduler getScheduler() {
        return mScheduler;
    }

    public void setScheduler(Scheduler pScheduler) {
        mScheduler = pScheduler;
    }

    public Schedule getSchedule() {
        return mSchedule;
    }

    public void setSchedule(Schedule pSchedule) {
        mSchedule = pSchedule;
    }

    public CommonEmailSender getEmailSender() {
        return mEmailSender;
    }

    public void setEmailSender(CommonEmailSender pEmailSender) {
        mEmailSender = pEmailSender;
    }

    public MutableRepository getOrderRepository() {
        return mOrderRepository;
    }

    public void setOrderRepository(MutableRepository pOrderRepository) {
        mOrderRepository = pOrderRepository;
    }

    public CPSProfileTools getProfileTools() {
        return mProfileTools;
    }

    public void setProfileTools(CPSProfileTools pProfileTools) {
        mProfileTools = pProfileTools;
    }

    public OrderManager getOrderManager() {
        return mOrderManager;
    }

    public void setOrderManager(OrderManager pOrderManager) {
        mOrderManager = pOrderManager;
    }

    /**
     * @return the savedCartDateTimeFrom
     */
    public int getSavedCartDateTimeFrom() {
        return savedCartDateTimeFrom;
    }

    /**
     * @param savedCartDateTimeFrom the savedCartDateTimeFrom to set
     */
    public void setSavedCartDateTimeFrom(int savedCartDateTimeFrom) {
        this.savedCartDateTimeFrom = savedCartDateTimeFrom;
    }

}
