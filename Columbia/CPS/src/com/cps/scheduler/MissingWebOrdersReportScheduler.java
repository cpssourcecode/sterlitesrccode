package com.cps.scheduler;

import java.util.HashMap;
import java.util.List;

import com.cps.scheduler.manager.MissingWebOrdersReportManager;
import com.cps.util.CPSConstants;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.service.scheduler.Schedulable;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

public class MissingWebOrdersReportScheduler extends GenericService implements Schedulable {

	private int jobId;
	private String jobName;
	private boolean enabled;

	private Scheduler scheduler;
	private Schedule schedule;
	private String environment;

	private List<String> emailsTo;
	private boolean enableDownloadLink;
	private String reportHostName;

	private MissingWebOrdersReportManager webOrdersReportManager;

	@Override
	public void doStartService() throws ServiceException {
		ScheduledJob scheduledJob = new ScheduledJob(getJobName(), getJobName(), getAbsoluteName(), getSchedule(), this,
				ScheduledJob.REUSED_THREAD);

		jobId = getScheduler().addScheduledJob(scheduledJob);
	}

	@Override
	public void doStopService() throws ServiceException {
		getScheduler().removeScheduledJob(jobId);
	}

	@Override
	public void performScheduledTask(Scheduler var1, ScheduledJob var2) {
		if (isEnabled()) {
			vlogDebug("Schedulable Service starts for missing orders...");
			generateMissingWebordersReport();
			vlogDebug("Schedulable Service end for missing orders...");
		} else {
			vlogInfo("{0} service is not enabled", getJobName());
		}

	}

	public void generateMissingWebordersReport() {
		vlogDebug("Generating Missing Weborders");
		
		HashMap<String, Object> emailParams = new HashMap<String, Object>();
		emailParams.put(CPSConstants.EMAIL, getEmailsTo());
		emailParams.put(CPSConstants.ENABLE_DOWNLOAD_LINK, isEnableDownloadLink());
		emailParams.put(CPSConstants.HOST_NAME, getReportHostName());
		
		getWebOrdersReportManager().generateMissingWebOrdersReport(emailParams, getEnvironment());
		vlogDebug("Generated Missing Weborders");
	}

	/**
	 * @return the jobId
	 */
	public int getJobId() {
		return jobId;
	}

	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(int jobId) {
		this.jobId = jobId;
	}

	/**
	 * @return the jobName
	 */
	public String getJobName() {
		return jobName;
	}

	/**
	 * @param jobName the jobName to set
	 */
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * @return the scheduler
	 */
	public Scheduler getScheduler() {
		return scheduler;
	}

	/**
	 * @param scheduler the scheduler to set
	 */
	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	/**
	 * @return the schedule
	 */
	public Schedule getSchedule() {
		return schedule;
	}

	/**
	 * @param schedule the schedule to set
	 */
	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}

	/**
	 * @param environment the environment to set
	 */
	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	/**
	 * @return the emailsTo
	 */
	public List<String> getEmailsTo() {
		return emailsTo;
	}

	/**
	 * @param emailsTo the emailsTo to set
	 */
	public void setEmailsTo(List<String> emailsTo) {
		this.emailsTo = emailsTo;
	}

	/**
	 * @return the webOrdersReportManager
	 */
	public MissingWebOrdersReportManager getWebOrdersReportManager() {
		return webOrdersReportManager;
	}

	/**
	 * @param webOrdersReportManager the webOrdersReportManager to set
	 */
	public void setWebOrdersReportManager(MissingWebOrdersReportManager webOrdersReportManager) {
		this.webOrdersReportManager = webOrdersReportManager;
	}

	/**
	 * @return the enableDownloadLink
	 */
	public boolean isEnableDownloadLink() {
		return enableDownloadLink;
	}

	/**
	 * @param enableDownloadLink the enableDownloadLink to set
	 */
	public void setEnableDownloadLink(boolean enableDownloadLink) {
		this.enableDownloadLink = enableDownloadLink;
	}

	/**
	 * @return the reportHostName
	 */
	public String getReportHostName() {
		return reportHostName;
	}

	/**
	 * @param reportHostName the reportHostName to set
	 */
	public void setReportHostName(String reportHostName) {
		this.reportHostName = reportHostName;
	}

}
