package com.cps.scheduler.manager;

import static com.cps.util.CPSConstants.ITEM_DESCRIPTOR_ORDER;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.FilePermissionsHelper;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvoiceRequest;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.util.CurrentDate;

public class MissingWebOrdersReportManager extends GenericService {

	private MutableRepository orderRepository;
	private CPSProfileTools profileTools;
	private OrderManager orderManager;
	private CommonEmailSender commonEmailSender;
	private FilePermissionsHelper filePermissionsHelper;
	private CurrentDate currentDate;

	private String missingWebOrdersRQLQuery;
	private String fileName;
	private String sheetName;
	private List<String> headerColumns;
	private List<String> errorEmailTo;
	private String localDirectory;
	private String fileExtension;
	private String fileLocationDirectory;

	/**
	 * 
	 * @param emailParams
	 * @param environment
	 */
	public void generateMissingWebOrdersReport(HashMap<String, Object> emailParams, String environment) {

		try {
			RepositoryView orderView = getOrderRepository().getView(ITEM_DESCRIPTOR_ORDER);
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(getMissingWebOrdersRQLQuery());
			RepositoryItem[] orderItems = rqlStatement.executeQuery(orderView, null);
			if (orderItems != null && orderItems.length > 0) {
				File[] attachments = new File[1];
				attachments = createAndWriteFile(orderItems, getFileName(), getSheetName(), emailParams, attachments);

				// Send email with attachment
				 boolean success = getCommonEmailSender().sendMissingWebOrderReportEmail(attachments, emailParams, environment);

			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param pOrderItems
	 * @param pFileName
	 * @param pSheetName
	 * @param emailParams
	 * @param attachments
	 * @return
	 */
	private File[] createAndWriteFile(RepositoryItem[] pOrderItems, String pFileName, String pSheetName,
			HashMap<String, Object> emailParams, File[] attachments) {
		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for
												// generating `.xls` file

		// Create a Sheet
		Sheet sheet = workbook.createSheet(pSheetName);

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create header cells
		for (int i = 0; i < getHeaderColumns().size(); i++) {
			createHeaderCell(getHeaderColumns(), headerCellStyle, headerRow, i);
		}
		// Create Other rows and cells with newly registered user data
		int rowNum = 1;
		for (RepositoryItem orderItem : pOrderItems) {
			Row row = sheet.createRow(rowNum++);
			String orderId = (String) orderItem.getRepositoryId();
			
			String profileEmail = (String) orderItem.getPropertyValue("profileEmail");
			String organizationId = (String) orderItem.getPropertyValue("organizationId");
			String cbNumbar = null;
			
			row.createCell(0).setCellValue(orderId);

			try {
			    	MutableRepository orderRepository = (MutableRepository) getOrderManager().getOrderTools().getOrderRepository();
				MutableRepositoryItem mutableOrderItem = orderRepository.getItemForUpdate(orderId,"order");
				mutableOrderItem.setPropertyValue("isExported", true);
				orderRepository.updateItem(mutableOrderItem);
				RepositoryItem profile = getProfileTools().getProfileForOrder(orderId);
				if (profile != null) {
					// RepositoryItem organization =
					// getProfileTools().getParentOrganization(profile);
					String firstName = (String) profile.getPropertyValue("firstName");
					String lastName = (String) profile.getPropertyValue("lastName");					
					row.createCell(1).setCellValue(firstName + " " + lastName);
					row.createCell(2).setCellValue(profileEmail);
					row.createCell(3).setCellValue(organizationId);
					RepositoryItem organization = (RepositoryItem) profile.getPropertyValue("parentOrganization");
					RepositoryItem billingAddress = null;
					if(organization != null){
						billingAddress = (RepositoryItem) organization.getPropertyValue("billingAddress");
					}else{
						billingAddress = (RepositoryItem) profile.getPropertyValue("derivedBillingAddress");
					}
					cbNumbar = billingAddress.getRepositoryId();
				}
			} catch (RepositoryException | CommerceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Order order = getOrder(orderId);
			vlogDebug("Load Order -------- {0}", order);
			List<CommerceItem> commerceItems = order.getCommerceItems();
			if (commerceItems != null && commerceItems.size() > 0) {
				vlogDebug("Commerce Items : {0}", commerceItems);
				StringBuilder cItem = new StringBuilder();
				for (CommerceItem commerceItem : commerceItems) {
					vlogDebug("Catalog Ref Id : {0} -- Qty : {1} -- Price : {2}", commerceItem.getCatalogRefId(),
							commerceItem.getQuantity(), commerceItem.getPriceInfo().getAmount());
//					cItem.append(commerceItem.getCatalogRefId()).append("-").append(commerceItem.getQuantity()).append("-").append(commerceItem.getPriceInfo().getAmount()).append("\n");
					cItem.append("\nItem Number : ").append(commerceItem.getCatalogRefId()).append("\n").append("Qty : ").append(commerceItem.getQuantity()).append("\n").append("Price : $").append(commerceItem.getPriceInfo().getAmount());
				}
				row.createCell(4).setCellValue(cItem.toString());
				row.setHeightInPoints(120);
			}

			Double orderTotal = (Double) orderItem.getPropertyValue("orderTotal");
			if(orderTotal==null){
				orderTotal = 0.0;
			}
			vlogDebug("Order Total {0}", orderTotal);
			row.createCell(5).setCellValue(orderTotal.doubleValue());
			row.createCell(6).setCellValue(cbNumbar);
			
			//getting Delivery Method and CS Number
			String csNumber = null;
			String deliveryMethod = null;
			List<ShippingGroup> shippingGroups = order.getShippingGroups();
			for (ShippingGroup shippingGroup : shippingGroups) {
				if (shippingGroup instanceof InStorePickupShippingGroup) {
					deliveryMethod = CPSConstants.DELEVERY_METHOD_PICK_UP;
					InStorePickupShippingGroup inStorePickupShippingGroup = (InStorePickupShippingGroup) shippingGroup;
					csNumber = inStorePickupShippingGroup.getLocationId();
				} else if (shippingGroup instanceof HardgoodShippingGroup) {
					deliveryMethod = CPSConstants.DELEVERY_METHOD_SHIPPED;
					CPSHardgoodShippingGroup hardgoodShippingGroup = (CPSHardgoodShippingGroup) shippingGroup;
					csNumber = hardgoodShippingGroup.getJdeAddressNumber();
				}
				vlogDebug("delivery Method :", deliveryMethod);
				vlogDebug("CS Number :", csNumber);
			}
									
			// Getting Payment Method
			String paymentType = null ;
			List<PaymentGroup> pg = order.getPaymentGroups();
			for (PaymentGroup paymentGroup : pg) {
				if (paymentGroup instanceof InvoiceRequest) {
					InvoiceRequest invoiceRequest = (InvoiceRequest) paymentGroup;
					paymentType = invoiceRequest.getPaymentMethod();
				}else if (paymentGroup instanceof CreditCard){
					CreditCard creditCard = (CreditCard) paymentGroup;
					paymentType = creditCard.getPaymentMethod();
				}
				vlogDebug("payment Type :", paymentType);
			}
			
			String webOrderNumber = (String) orderItem.getPropertyValue("webOrderId");
			vlogDebug("webOrderNumber :", webOrderNumber);
			
			String onsiteContact = (String) orderItem.getPropertyValue("onsiteContact");
			String contactName = null;
			String contactPhoneNumber = null;
			if(onsiteContact != null){
				String[] onsiteContactArray = onsiteContact.split("\\|");
				contactName = onsiteContactArray[0];
				contactPhoneNumber = onsiteContactArray[1];
			}
			vlogDebug("contactName :", contactName);
			vlogDebug("contactPhoneNumber :", contactPhoneNumber);
			
			String poNumber = null;
			String checkPoNumber = (String) orderItem.getPropertyValue("concatenatedPOJobName");
			String[] poNumberArray = checkPoNumber.split("-");
			if(checkPoNumber != null && poNumberArray.length > 0){
				poNumber = poNumberArray[0];
			}else{
				poNumber = " ";
			}
			vlogDebug("poNumber :", poNumber);
			
			String jobName = null ;
			String checkJobName = (String) orderItem.getPropertyValue("jobName");
			if(checkJobName != null ){
				jobName = checkJobName;
			}else{
				jobName = " ";
			}
			vlogDebug("jobName :", jobName);
			row.createCell(7).setCellValue(csNumber);
			row.createCell(8).setCellValue(webOrderNumber);
			row.createCell(9).setCellValue(poNumber);
			row.createCell(10).setCellValue(contactName);
			row.createCell(11).setCellValue(contactPhoneNumber);
			row.createCell(12).setCellValue(jobName);
			row.createCell(13).setCellValue(paymentType);
			row.createCell(14).setCellValue(deliveryMethod);
		}
		
		// Resize all columns to fit the content size
		for (int i = 0; i < getHeaderColumns().size(); i++) {
			sheet.autoSizeColumn(i);
		}
		// Write the output to a file
		FileOutputStream fileOut = null;
		try {
			StringBuilder localFilePath = new StringBuilder();
			StringBuilder filePath = new StringBuilder();
			Timestamp currentDate = getCurrentDate().getTimeAsTimestamp();
			makeDirectory();
			String fileLocation = filePath.append(getFileLocationDirectory()).append(pFileName).append(currentDate)
					.append(getFileExtension()).toString();
			String localFileLocation = localFilePath.append(getLocalDirectory()).append(pFileName).append(currentDate)
					.append(getFileExtension()).toString();
			fileLocation = fileLocation.replaceAll("\\s", "_");
			localFileLocation = localFileLocation.replaceAll("\\s", "_");
			fileOut = new FileOutputStream(localFileLocation);
			workbook.write(fileOut);
			fileOut.close();

			getFilePermissionsHelper().setFilePermissions(localFileLocation);
			String downloadFileLink;
			File destFile = new File(localFileLocation);
			attachments[0] = destFile;
			if (emailParams != null) {
				downloadFileLink = new StringBuilder(CPSConstants.HTTP_PROTOCOL)
						.append(emailParams.get(CPSConstants.HOST_NAME)).append(fileLocation).toString();
				emailParams.put(CPSConstants.DOWNLOAD_FILE_LINK, downloadFileLink);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return attachments;
	}

	/**
	 * 
	 * @param pOrderId
	 * @return
	 */
	private Order getOrder(String pOrderId) {
		Order order = null;
		try {
			order = getOrderManager().loadOrder(pOrderId);
		} catch (CommerceException ce) {
			vlogError(ce, "Exception occured trying to load order with id :: {0}", pOrderId);
		}
		return order;
	}

	/**
	 * To make directory if directory is not exist
	 */
	private void makeDirectory() {
		File files = new File(getLocalDirectory());
		if (!files.exists()) {
			files.mkdirs();
		}
	}

	/**
	 * 
	 * @param headerColumns
	 * @param headerCellStyle
	 * @param headerRow
	 * @param i
	 */
	private void createHeaderCell(List<String> headerColumns, CellStyle headerCellStyle, Row headerRow, int i) {
		Cell cell = headerRow.createCell(i);
		cell.setCellValue((String) headerColumns.get(i));
		cell.setCellStyle(headerCellStyle);
	}

	/**
	 * @return the orderRepository
	 */
	public MutableRepository getOrderRepository() {
		return orderRepository;
	}

	/**
	 * @param orderRepository
	 *            the orderRepository to set
	 */
	public void setOrderRepository(MutableRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	/**
	 * @return the profileTools
	 */
	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	/**
	 * @param profileTools
	 *            the profileTools to set
	 */
	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	/**
	 * @return the orderManager
	 */
	public OrderManager getOrderManager() {
		return orderManager;
	}

	/**
	 * @param orderManager
	 *            the orderManager to set
	 */
	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	/**
	 * @return the commonEmailSender
	 */
	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	/**
	 * @param commonEmailSender
	 *            the commonEmailSender to set
	 */
	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}

	/**
	 * @return the filePermissionsHelper
	 */
	public FilePermissionsHelper getFilePermissionsHelper() {
		return filePermissionsHelper;
	}

	/**
	 * @param filePermissionsHelper
	 *            the filePermissionsHelper to set
	 */
	public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
		this.filePermissionsHelper = filePermissionsHelper;
	}

	/**
	 * @return the currentDate
	 */
	public CurrentDate getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate
	 *            the currentDate to set
	 */
	public void setCurrentDate(CurrentDate currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the missingWebOrdersRQLQuery
	 */
	public String getMissingWebOrdersRQLQuery() {
		return missingWebOrdersRQLQuery;
	}

	/**
	 * @param missingWebOrdersRQLQuery
	 *            the missingWebOrdersRQLQuery to set
	 */
	public void setMissingWebOrdersRQLQuery(String missingWebOrdersRQLQuery) {
		this.missingWebOrdersRQLQuery = missingWebOrdersRQLQuery;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the sheetName
	 */
	public String getSheetName() {
		return sheetName;
	}

	/**
	 * @param sheetName
	 *            the sheetName to set
	 */
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	/**
	 * @return the headerColumns
	 */
	public List<String> getHeaderColumns() {
		return headerColumns;
	}

	/**
	 * @param headerColumns
	 *            the headerColumns to set
	 */
	public void setHeaderColumns(List<String> headerColumns) {
		this.headerColumns = headerColumns;
	}

	/**
	 * @return the localDirectory
	 */
	public String getLocalDirectory() {
		return localDirectory;
	}

	/**
	 * @param localDirectory
	 *            the localDirectory to set
	 */
	public void setLocalDirectory(String localDirectory) {
		this.localDirectory = localDirectory;
	}

	/**
	 * @return the fileExtension
	 */
	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * @param fileExtension
	 *            the fileExtension to set
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/**
	 * @return the fileLocationDirectory
	 */
	public String getFileLocationDirectory() {
		return fileLocationDirectory;
	}

	/**
	 * @param fileLocationDirectory
	 *            the fileLocationDirectory to set
	 */
	public void setFileLocationDirectory(String fileLocationDirectory) {
		this.fileLocationDirectory = fileLocationDirectory;
	}
	public List<String> getErrorEmailTo() {
	    return errorEmailTo;
	}
	public void setErrorEmailTo(List<String> errorEmailTo) {
	    this.errorEmailTo = errorEmailTo;
	}

}
