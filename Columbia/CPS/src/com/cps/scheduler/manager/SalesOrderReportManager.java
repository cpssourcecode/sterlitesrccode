package com.cps.scheduler.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.FilePermissionsHelper;
import com.cps.util.RepositoryUtils;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CreditCard;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.InvoiceRequest;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.PaymentGroup;
import atg.commerce.order.ShippingGroup;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.util.CurrentDate;

public class SalesOrderReportManager extends GenericService {

	private static final Object ORDER_STATE_SUBMITTED = "SUBMITTED";
	private Repository orderRepository;
	private Repository productRepository;
	private CPSProfileTools profileTools;
	private OrderManager orderManager;
	private CurrentDate currentDate;
	private CommonEmailSender commonEmailSender;
	private RepositoryUtils repositoryUtils;
	private FilePermissionsHelper filePermissionsHelper;

	private String localDirectory;
	private String fileLocationDirectory;
	private String fileExtension;
	private String rqlQuerySalesOrder;
	private String salesOrderReportFileName;
	private String salesOrderReportSheetName;
	private int salesReportDate;
	private String rqlQueryOneDaySalesOrder;

	private List<String> headerColumns;

	/**
	 * Generating sales order report
	 * 
	 * @param emailParams
	 * @param environment
	 */
	public void generateSalesOrderReport(HashMap<String, Object> emailParams, String environment) {
		vlogDebug("Generating sales order report...");
		try {
			boolean enableOneDayReport = (boolean) emailParams.get("enabledOneDayReport");
			RepositoryItem[] results = null;
			if(!enableOneDayReport){
				results = getRepositoryUtils().executeQuery(CPSConstants.ITEM_DESCRIPTOR_ORDER,
						getOrderRepository(), getRqlQuerySalesOrder(), getRqlQueryParams(getSalesReportDate()));
			}else {
				results = getRepositoryUtils().executeQuery(CPSConstants.ITEM_DESCRIPTOR_ORDER,
						getOrderRepository(), getRqlQueryOneDaySalesOrder(), getOneDayRqlQueryParams(getSalesReportDate()));
			}			
			if (results != null && results.length > 0) {
				// Creating xlsx file
				Workbook workbook = createWorkbook(results, getSalesOrderReportFileName(), getSalesOrderReportSheetName());
				File[] attachments = new File[1];
				attachments[0] = writeFile(workbook, getSalesOrderReportFileName(), emailParams);
				// Send email with attachment
				getCommonEmailSender().sendSalesOrderReportEmail(attachments, emailParams, environment);

			} else {
				vlogDebug("No record(s) found for sales order report.");
				getCommonEmailSender().sendSalesOrderReportEmail(null, emailParams, environment);
			}
		} catch (RepositoryException rex) {
			vlogError("There is an issue while generating the reports.");
			vlogError("RepositoryException {0}", rex.getMessage());
		}

	}

	/**
	 * @param pOrderItems
	 * @param pFileName
	 * @param pSheetName
	 * @return
	 */
	public Workbook createWorkbook(RepositoryItem[] pOrderItems, String pFileName, String pSheetName) {

		Workbook workbook = new XSSFWorkbook();
		// Create a Sheet
		Sheet sheet = workbook.createSheet(pSheetName);

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Create a Row
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < getHeaderColumns().size(); i++) {
			createHeaderCell(getHeaderColumns(), headerCellStyle, headerRow, i);

		}
		// Create Other rows and cells for sales order
		int rowNum = 1;
		for (RepositoryItem orderItem : pOrderItems) {
			Row row = sheet.createRow(rowNum++);

			String orderId = (String) orderItem.getRepositoryId();
			String profileEmail = (String) orderItem.getPropertyValue(CPSConstants.PROFILE_EMAIL);
			String organizationId = (String) orderItem.getPropertyValue(CPSConstants.ORGANIZATION_ID);

			row.createCell(0).setCellValue(orderId);
			row.createCell(1).setCellValue(profileEmail);
			row.createCell(2).setCellValue(organizationId);

			try {
				RepositoryItem profile = getProfileTools().getProfileForOrder(orderId);
				if (profile != null) {
					RepositoryItem organization = getProfileTools().getParentOrganization(profile);
					if (organization != null) {
						row.createCell(3).setCellValue((String) organization.getPropertyValue(CPSConstants.NAME));
					}
				}
			} catch (RepositoryException | CommerceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Get the order object by passing order id
			Order order = getOrder(orderId);
			vlogDebug("Loaded Order : {0}", order);
			if (order != null) {
				if (order.getShippingGroups().size() > 0) {
					// Order shipping groups
					List<ShippingGroup> shippingGroups = order.getShippingGroups();
					for (ShippingGroup shippingGroup : shippingGroups) {
						if (shippingGroup instanceof InStorePickupShippingGroup) {
							InStorePickupShippingGroup inStorePickupShippingGroup = (InStorePickupShippingGroup) shippingGroup;
							String locationId = inStorePickupShippingGroup.getLocationId();
							vlogDebug("Location ID : {0} ", locationId);
							row.createCell(4).setCellValue(locationId);
						}
						if (shippingGroup instanceof HardgoodShippingGroup) {
							CPSHardgoodShippingGroup hardgoodShippingGroup = (CPSHardgoodShippingGroup) shippingGroup;
							String csNumber = hardgoodShippingGroup.getJdeAddressNumber();
							row.createCell(4).setCellValue(csNumber);
							vlogDebug("Shipping Group ID : {0}", csNumber);
						}
					}

				}
				// Order commerce items
				List<CommerceItem> commerceItems = order.getCommerceItems();
				if (commerceItems != null && commerceItems.size() > 0) {
					vlogDebug("Commerce Items : {0}", commerceItems);
					StringBuilder cItem = new StringBuilder();
					for (CommerceItem commerceItem : commerceItems) {
						try {
							RepositoryItem productItem = getProductRepository().getItem(commerceItem.getCatalogRefId(),
									CPSConstants.PRODUCT_ITEM_DESCRIPTOR);
							String displayName = (String) productItem.getPropertyValue(CPSConstants.DISPLAY_NAME);
							vlogDebug("Item Number : {0} -- Qty : {1} -- List Price : {2} -- Amount : {3}",
									productItem.getPropertyValue(CPSConstants.DISPLAY_NAME), commerceItem.getQuantity(),
									commerceItem.getPriceInfo().getListPrice(),
									commerceItem.getPriceInfo().getAmount());
							BigDecimal unitPrice = new BigDecimal(commerceItem.getPriceInfo().getListPrice());
							unitPrice = unitPrice.setScale(2, RoundingMode.HALF_UP);
							cItem.append("\nItem Number : ")
									.append(productItem.getPropertyValue(CPSConstants.DISPLAY_NAME)).append("\n")
									.append("Qty : ").append(commerceItem.getQuantity()).append("\n")
									.append("Unit Price : $").append(unitPrice).append("\n").append("Amount : $")
									.append(commerceItem.getPriceInfo().getAmount());
						} catch (RepositoryException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					row.createCell(5).setCellValue(cItem.toString());
					row.setHeightInPoints(120);
				}
			}

			Double orderTotal = (Double) orderItem.getPropertyValue(CPSConstants.ORDER_TOTAL);
			if (orderTotal == null) {
				orderTotal = 0.0;
			}
			vlogDebug("Order Total {0}", orderTotal);
			row.createCell(6).setCellValue(orderTotal.doubleValue());
			row.createCell(7).setCellValue(orderItem.getPropertyValue(CPSConstants.SUBMITTED_DATE).toString());
			
			//Payment Method
			List<PaymentGroup> pg = order.getPaymentGroups();
			String paymentType = null ;
			for (PaymentGroup paymentGroup : pg) {
				if (paymentGroup instanceof InvoiceRequest) {
					InvoiceRequest invoiceRequest = (InvoiceRequest) paymentGroup;
					paymentType = invoiceRequest.getPaymentMethod();
				}else if (paymentGroup instanceof CreditCard){
					CreditCard creditCard = (CreditCard) paymentGroup;
					paymentType = creditCard.getPaymentMethod();
				}
				vlogDebug("payment Type", paymentType);
			}
			row.createCell(8).setCellValue(paymentType);
		}
		// Resize all columns to fit the content size
		for (int i = 0; i < getHeaderColumns().size(); i++) {
			sheet.autoSizeColumn(i);
		}

		return workbook;
	}

	/**
	 * @param workbook
	 * @param pFileName
	 * @param emailParams
	 * @return
	 */
	private File writeFile(Workbook workbook, String pFileName, HashMap<String, Object> emailParams) {
		FileOutputStream fileOut = null;
		File salesReportFile = null;
		try {
			StringBuilder localFilePath = new StringBuilder();
			StringBuilder filePath = new StringBuilder();
			Timestamp currentDate = getCurrentDate().getTimeAsTimestamp();
			// make a directory
			makeDirectory();
			String fileLocation = filePath.append(getFileLocationDirectory()).append(pFileName).append(currentDate)
					.append(getFileExtension()).toString();
			String localFileLocation = localFilePath.append(getLocalDirectory()).append(pFileName).append(currentDate)
					.append(getFileExtension()).toString();
			fileLocation = fileLocation.replaceAll("\\s", "_");
			localFileLocation = localFileLocation.replaceAll("\\s", "_");

			fileOut = new FileOutputStream(localFileLocation);
			workbook.write(fileOut);
			fileOut.close();

			// Set permission for the file
			getFilePermissionsHelper().setFilePermissions(localFileLocation);
			String downloadFileLink;
			salesReportFile = new File(localFileLocation);

			if (emailParams != null) {
				downloadFileLink = new StringBuilder(CPSConstants.HTTP_PROTOCOL)
						.append(emailParams.get(CPSConstants.HOST_NAME)).append(fileLocation).toString();
				emailParams.put(CPSConstants.DOWNLOAD_FILE_LINK, downloadFileLink);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return salesReportFile;

	}

	/**
	 * To make directory if directory is not exist and set the permission to the
	 * directory
	 */
	private void makeDirectory() {
		File files = new File(getLocalDirectory());
		if (!files.exists()) {
			files.mkdirs();
			try {
				// Set permission for the directory
				getFilePermissionsHelper().setFilePermissionsRecursivelyOnDir(getLocalDirectory());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * To load the order by passing order id
	 * 
	 * @param pOrderId
	 * @return
	 */
	private Order getOrder(String pOrderId) {
		Order order = null;
		try {
			order = getOrderManager().loadOrder(pOrderId);
		} catch (CommerceException ce) {
			vlogError(ce, "Exception occured trying to load order with id : {0}", pOrderId);
		}
		return order;
	}

	/**
	 * To create header cell to the sales order report
	 * 
	 * @param headerColumns
	 * @param headerCellStyle
	 * @param headerRow
	 * @param i
	 */
	private void createHeaderCell(List<String> headerColumns, CellStyle headerCellStyle, Row headerRow, int i) {
		Cell cell = headerRow.createCell(i);
		cell.setCellValue((String) headerColumns.get(i));
		cell.setCellStyle(headerCellStyle);
	}
	
	/**
	 * @param pSalesReportDate
	 * @return
	 */
	private Object[] getOneDayRqlQueryParams(int pSalesReportDate) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, pSalesReportDate);
		Date pastOneDay = cal.getTime();
		
		Object params[] = new Object[2];
		params[0] = ORDER_STATE_SUBMITTED;
		params[1] = pastOneDay;
		
		return params;
	}
	
	/**
	 * @return RQL Query
	 */
	public Object[] getRqlQueryParams(int pSalesReportDate) {
		Calendar startDateTime = Calendar.getInstance();
		startDateTime.add(Calendar.DATE, pSalesReportDate);
		startDateTime.set(Calendar.HOUR_OF_DAY, 0);
		startDateTime.set(Calendar.MINUTE, 0);
		startDateTime.set(Calendar.SECOND, 0);
		startDateTime.set(Calendar.MILLISECOND, 0);
		Date dateTimeFrom = startDateTime.getTime();

		Calendar endDateTime = Calendar.getInstance();
		endDateTime.add(Calendar.DATE, pSalesReportDate);
		endDateTime.set(Calendar.HOUR_OF_DAY, 23);
		endDateTime.set(Calendar.MINUTE, 59);
		endDateTime.set(Calendar.SECOND, 59);
		endDateTime.set(Calendar.MILLISECOND, 999);
		Date dateTimeTo = endDateTime.getTime();

		Object params[] = new Object[3];
		params[0] = ORDER_STATE_SUBMITTED;
		params[1] = dateTimeFrom;
		params[2] = dateTimeTo;
		return params;
	}

	/**
	 * @return the orderRepository
	 */
	public Repository getOrderRepository() {
		return orderRepository;
	}

	/**
	 * @param orderRepository
	 *            the orderRepository to set
	 */
	public void setOrderRepository(Repository orderRepository) {
		this.orderRepository = orderRepository;
	}

	/**
	 * @return the productRepository
	 */
	public Repository getProductRepository() {
		return productRepository;
	}

	/**
	 * @param productRepository
	 *            the productRepository to set
	 */
	public void setProductRepository(Repository productRepository) {
		this.productRepository = productRepository;
	}

	/**
	 * @return the profileTools
	 */
	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	/**
	 * @param profileTools
	 *            the profileTools to set
	 */
	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	/**
	 * @return the orderManager
	 */
	public OrderManager getOrderManager() {
		return orderManager;
	}

	/**
	 * @param orderManager
	 *            the orderManager to set
	 */
	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}

	/**
	 * @return the currentDate
	 */
	public CurrentDate getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate
	 *            the currentDate to set
	 */
	public void setCurrentDate(CurrentDate currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the commonEmailSender
	 */
	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	/**
	 * @param commonEmailSender
	 *            the commonEmailSender to set
	 */
	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}

	/**
	 * @return the repositoryUtils
	 */
	public RepositoryUtils getRepositoryUtils() {
		return repositoryUtils;
	}

	/**
	 * @param repositoryUtils
	 *            the repositoryUtils to set
	 */
	public void setRepositoryUtils(RepositoryUtils repositoryUtils) {
		this.repositoryUtils = repositoryUtils;
	}

	/**
	 * @return the filePermissionsHelper
	 */
	public FilePermissionsHelper getFilePermissionsHelper() {
		return filePermissionsHelper;
	}

	/**
	 * @param filePermissionsHelper
	 *            the filePermissionsHelper to set
	 */
	public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
		this.filePermissionsHelper = filePermissionsHelper;
	}

	/**
	 * @return the localDirectory
	 */
	public String getLocalDirectory() {
		return localDirectory;
	}

	/**
	 * @param localDirectory
	 *            the localDirectory to set
	 */
	public void setLocalDirectory(String localDirectory) {
		this.localDirectory = localDirectory;
	}

	/**
	 * @return the fileLocationDirectory
	 */
	public String getFileLocationDirectory() {
		return fileLocationDirectory;
	}

	/**
	 * @param fileLocationDirectory
	 *            the fileLocationDirectory to set
	 */
	public void setFileLocationDirectory(String fileLocationDirectory) {
		this.fileLocationDirectory = fileLocationDirectory;
	}

	/**
	 * @return the fileExtension
	 */
	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * @param fileExtension
	 *            the fileExtension to set
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/**
	 * @return the rqlQuerySalesOrder
	 */
	public String getRqlQuerySalesOrder() {
		return rqlQuerySalesOrder;
	}

	/**
	 * @param rqlQuerySalesOrder
	 *            the rqlQuerySalesOrder to set
	 */
	public void setRqlQuerySalesOrder(String rqlQuerySalesOrder) {
		this.rqlQuerySalesOrder = rqlQuerySalesOrder;
	}

	/**
	 * @return the salesOrderReportFileName
	 */
	public String getSalesOrderReportFileName() {
		return salesOrderReportFileName;
	}

	/**
	 * @param salesOrderReportFileName
	 *            the salesOrderReportFileName to set
	 */
	public void setSalesOrderReportFileName(String salesOrderReportFileName) {
		this.salesOrderReportFileName = salesOrderReportFileName;
	}

	/**
	 * @return the salesOrderReportSheetName
	 */
	public String getSalesOrderReportSheetName() {
		return salesOrderReportSheetName;
	}

	/**
	 * @param salesOrderReportSheetName
	 *            the salesOrderReportSheetName to set
	 */
	public void setSalesOrderReportSheetName(String salesOrderReportSheetName) {
		this.salesOrderReportSheetName = salesOrderReportSheetName;
	}

	/**
	 * @return the salesReportDate
	 */
	public int getSalesReportDate() {
		return salesReportDate;
	}

	/**
	 * @param salesReportDate
	 *            the salesReportDate to set
	 */
	public void setSalesReportDate(int salesReportDate) {
		this.salesReportDate = salesReportDate;
	}

	/**
	 * @return the headerColumns
	 */
	public List<String> getHeaderColumns() {
		return headerColumns;
	}

	/**
	 * @param headerColumns
	 *            the headerColumns to set
	 */
	public void setHeaderColumns(List<String> headerColumns) {
		this.headerColumns = headerColumns;
	}

	/**
	 * @return the rqlQueryOneDaySalesOrder
	 */
	public String getRqlQueryOneDaySalesOrder() {
		return rqlQueryOneDaySalesOrder;
	}

	/**
	 * @param rqlQueryOneDaySalesOrder the rqlQueryOneDaySalesOrder to set
	 */
	public void setRqlQueryOneDaySalesOrder(String rqlQueryOneDaySalesOrder) {
		this.rqlQueryOneDaySalesOrder = rqlQueryOneDaySalesOrder;
	}

}
