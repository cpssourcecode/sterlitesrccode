package com.cps.scheduler.manager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.FilePermissionsHelper;
import com.cps.util.RepositoryUtils;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.util.CurrentDate;
import vsg.crypto.AESEncryptor;
import vsg.crypto.EncryptorException;

/**
 * @author Manikandan, Vignesh
 *
 */
public class RegisteredUserReportManager extends GenericService {

	private static final String LAST_LOGIN = "LastLogin";
	private CPSProfileTools profileTools;
	private CommonEmailSender commonEmailSender;
	private RepositoryUtils repositoryUtils;
	private FilePermissionsHelper filePermissionsHelper;
	private SimpleDateFormat mDateFormat = new SimpleDateFormat("MM-dd-yyyy--hh-mm-ss");
	
	private String inactiveOrganizatioQuery;
	private String userOrganizationQuery;
	private String inactiveOrganizationFileName;
	private String newUserFileName;
	private String newUserSheetName;
	private String inactiveUserFileName;
	private String inactiveUserSheetName;
	private String localDirectory;
	private String fileLocationDirectory;
	private String fileExtension;
	private String rqlQueryNewUser;
	private String rqlQueryOverThreeDays;
	private String inactiveUserDateRangeQuery;
	private String activationUrl;
	private String activeUserGeneratedUrl;
	private String activeUserExportHostName;
	private String ftpClient;
	private String ftpUser;
	private String ftpPassword;
	private String ftpLocation;
	private String ftpFileExtension;
	private String knownHost;
	private String ftpLocationDirectory;
	private String localFtpDirectory;
	
	private boolean ftpEnabled;

	private int ftpPort;
	private int inactiveUserDateTimeFrom;
	private int startDurationForFirstReminderMail;
	private int endDurationForFirstReminderMail;
	private int startDurationForSecondReminderMail;
	private int endDurationForSecondReminderLink;
	
	/*private String localFileLocation;
	private String fileLocation;*/

	private List<String> headerColumns;
	private List<String> activeUserHeaderColumns;
	private CurrentDate currentDate;
	
	private AESEncryptor encryptor;
	private String email;
	private List<String> reportsShouldHavelastLogin;
	private boolean enableLastLoginForAllReports;

	/**
	 * To generate report for daily registered user
	 * @param emailParams
	 */
	public void generateRegisteredNewUsersReport(HashMap<String, Object> emailParams,String environment) {

		vlogDebug("Registered new users report generation is started...");
		try {
			Repository userProfile = getProfileTools().getProfileRepository();
			Object[] params = getRqlQueryParams();
			RepositoryItem[] userItems = getRepositoryUtils().executeQuery(CPSConstants.USER_ITEM_DESCRIPTOR,
					userProfile, getRqlQueryNewUser(), params);

			if (userItems != null && userItems.length > 0) {
				// Creating xlsx file
				File[] attachments = new File[1];
				attachments = createAndWriteFile(userItems, getNewUserFileName(), getNewUserSheetName(), emailParams, attachments , false);				
				// Send email with attachment
				getCommonEmailSender().sendRegisteredUserReportEmail(attachments, emailParams, environment);

			} else {
				vlogDebug("There is no user registered.");
				getCommonEmailSender().sendRegisteredUserReportEmail(null, emailParams, environment);
			}
		} catch (RepositoryException rex) {
			vlogError("There is an issue while generating the reports. ");
			vlogError("RepositoryException {0}", rex.getMessage());
		}
	}

	/**
	 * To generate report for registered user but not activated certain periods
	 * @param emailParams
	 */
	public void generateNotActivatedUsersReport(HashMap<String, Object> emailParams, String environment) {

		try {
			Repository userProfile = getProfileTools().getProfileRepository();
			Object[] inActiveUserParams = getRqlQueryInactiveUserParams();
			RepositoryItem[] inActiveUserItems = getRepositoryUtils().executeQuery(CPSConstants.USER_ITEM_DESCRIPTOR,
					userProfile, getRqlQueryOverThreeDays(), inActiveUserParams);

			if (inActiveUserItems != null && inActiveUserItems.length > 0) {
				// Creating xlsx file
				File[] attachments = new File[1];
				attachments = createAndWriteFile(inActiveUserItems, getInactiveUserFileName(), getInactiveUserSheetName(), emailParams, attachments,false);				
				// Send email
				getCommonEmailSender().sendNotActivatedUsersReportEmail(attachments, emailParams, environment);
			} else {
				vlogDebug("There is no inactive user.");
				getCommonEmailSender().sendNotActivatedUsersReportEmail(null, emailParams, environment);
			}
		} catch (RepositoryException rex) {
			vlogError("There is an issue while generating the reports. ");
			vlogError("RepositoryException {0}", rex.getMessage());
		}

	}
	public void registeredOrganizationReport(){
	    
	    try {
		List<String> newRegisterdOrganization = new ArrayList<String>();
		Repository userProfile = getProfileTools().getProfileRepository();
		Object[] inActiveOrganizationParams = getOrganizationParams();
		RepositoryItem[] inActiveOrganizationItems = getRepositoryUtils().executeQuery(CPSConstants.ORGANIZATION,
			userProfile, getInactiveOrganizatioQuery(), inActiveOrganizationParams);
		if (inActiveOrganizationItems != null && inActiveOrganizationParams.length > 0) {
		    for (RepositoryItem inActiveOrganizationItem : inActiveOrganizationItems) {
			Object param[] = new Object[1];
			param[0]=(String)inActiveOrganizationItem.getRepositoryId();
			int userCount = getRepositoryUtils().executeCountQuery(CPSConstants.USER_ITEM_DESCRIPTOR, userProfile,getUserOrganizationQuery(), param);
			if(userCount > 0){
			    newRegisterdOrganization.add((String)inActiveOrganizationItem.getRepositoryId());
			    MutableRepository repository= (MutableRepository) userProfile;
			    MutableRepositoryItem organization= repository.getItemForUpdate((String)inActiveOrganizationItem.getRepositoryId(),CPSConstants.ORGANIZATION);
			    organization.setPropertyValue(CPSConstants.IS_ACTIVE_ORGANIZATION, true);
			    
			}
		    }
		}
		if(newRegisterdOrganization.isEmpty()){
		    vlogInfo("No new Organization registered in Site");
		}else{
		    generateCsvFile(newRegisterdOrganization);
		}
		
	    } catch (RepositoryException e) {
		vlogError("error occured while querying Organization ", e);
	    }
	}
	
	private void generateCsvFile(List<String> newRegisterdOrganization) {
	    StringBuilder localFilePath = new StringBuilder();
		StringBuilder filePath = new StringBuilder();
	        Date currentDate = Calendar.getInstance().getTime();
		makeDirectory();
		String fileLocation = filePath.append(getFtpLocationDirectory()).append(getInactiveOrganizationFileName()).append(mDateFormat.format(currentDate)).append(getFtpFileExtension()).toString();			
		String localFileLocation = localFilePath.append(getLocalFtpDirectory()).append(getInactiveOrganizationFileName()).append(mDateFormat.format(currentDate)).append(getFtpFileExtension()).toString();
		fileLocation = fileLocation.replaceAll("\\s", "_");
		localFileLocation = localFileLocation.replaceAll("\\s", "_");
		
		try {
		    FileWriter cname = new FileWriter(localFileLocation);
		    getFilePermissionsHelper().setFilePermissions(localFileLocation);
		    cname.append("Organization");
		    cname.append("\n");
		    for (String orgId : newRegisterdOrganization) {
			cname.append(orgId);
			cname.append("\n");
		    }
		    cname.close();
		    if(getFtpEnabled()){
			File fis =new File(localFileLocation);
			pushFileToFTP(fis);
			
		    }
		 
		} catch (IOException e) {
		    vlogError("error while creating file", e);
		}
	}
	private void pushFileToFTP(File file){
	    Session session = null;
	    Channel channel = null;
	    ChannelSftp channelSftp = null;
	    try {
		JSch jsch = new JSch();
		jsch.setKnownHosts(getKnownHost());
		session = jsch.getSession(getFtpUser(), getFtpClient(),  getFtpPort());
		session.setPassword(getFtpPassword());
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();
		channel = session.openChannel("sftp");
		channel.connect();
		channelSftp = (ChannelSftp) channel;
		channelSftp.cd(getFtpLocation());
		channelSftp.put(new FileInputStream(file), file.getName());
		 
	    } catch (IOException ioe) {
		vlogError("IOException occured-----{0}", ioe);
		ioe.printStackTrace();
	    } catch (JSchException je) {
		vlogError("JSch Exception occured -----{0}", je);
		je.printStackTrace();
	    } catch (SftpException se) {
		vlogError("Sftp Exception ------{0}", se);
		se.printStackTrace();
	    }finally {
		channelSftp.exit();
	        channel.disconnect();
	        session.disconnect();
	    }
	}

	private Object[] getOrganizationParams() {
	    Object param[] = new Object[1];
	    param[0]=false;
	    return param;
	}

	public void generateEmailReminderLink() {
		try {
			Repository userProfile = getProfileTools().getProfileRepository();
			for (int i = 1; i <= 2; i++) {
				Object[] inActiveUserParams = getDateRange(i);
				RepositoryItem[] inActiveUserItems = getRepositoryUtils().executeQuery(
						CPSConstants.USER_ITEM_DESCRIPTOR, userProfile, getInactiveUserDateRangeQuery(), inActiveUserParams);
				if (inActiveUserItems != null && inActiveUserItems.length > 0) {
					for (RepositoryItem inActiveUserItem : inActiveUserItems) {
						String outgoingActivationToken = generateActivationToken(inActiveUserItem);
						getCommonEmailSender().sendEmailConfirmationReminder(inActiveUserItem,outgoingActivationToken);
					}
				}
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
	
	public String generateActivationToken(RepositoryItem inActiveUserItem) {
		String activationToken = null;
		try {
			getEncryptor().initialization();
			String email = (String) inActiveUserItem.getPropertyValue("login");
			activationToken = new String(getEncryptor().encrypt(email.toLowerCase().getBytes()));
			vlogDebug("Encrypted email: {0} and activationToken : {1}",  email, activationToken);
			activationToken = URLEncoder.encode(activationToken, "UTF-8");
			activationToken = activationUrl + activationToken;

		} catch (EncryptorException | UnsupportedEncodingException e) {
			vlogError("Could create activation token {0}", e.getMessage());
		}
		return activationToken;
	}
	public String generateActiveUserReport(String reportQuery, String reportName,String sheetName){
		File[] attachments = new File[1];
		try {
			Repository userProfile = getProfileTools().getProfileRepository();
			RepositoryItem[] activeUserItems = getRepositoryUtils().executeQuery(CPSConstants.USER_ITEM_DESCRIPTOR,
					userProfile, reportQuery);

			if (activeUserItems != null && activeUserItems.length > 0) {
				// Creating xlsx file
				attachments = createAndWriteFile(activeUserItems, reportName, sheetName ,null , attachments,true);				
				return getActiveUserGeneratedUrl();
			} else {
				vlogDebug("There is no active user.");
			}
		} catch (RepositoryException rex) {
			vlogError("There is an issue while generating the Active-User reports. ");
			vlogError("RepositoryException {0}", rex.getMessage());
		}

		return getActiveUserGeneratedUrl();
	}
	/**
	 * 
	 * @return
	 */
	private Object[] getRqlQueryParams() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -1);
		Date dateTimeFrom = cal.getTime();

		Calendar currentDate = Calendar.getInstance();
		Date dateTimeTo = currentDate.getTime();

		Object params[] = new Object[2];
		params[0] = dateTimeFrom;
		params[1] = dateTimeTo;
		return params;
	}
	
	private Object[] getDateRange(int reminderCount) {
		Object params[] = new Object[2];
		if (reminderCount == 1 ){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, getStartDurationForFirstReminderMail());
		Date dateTimeFrom = cal.getTime();

		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DATE, getEndDurationForFirstReminderMail());
		Date dateTimeTo = currentDate.getTime();


		params[0] = dateTimeFrom;
		params[1] = dateTimeTo;
		
		}
		if(reminderCount == 2){
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, getStartDurationForSecondReminderMail());
			Date dateTimeFrom = cal.getTime();

			Calendar currentDate = Calendar.getInstance();
			currentDate.add(Calendar.DATE, getEndDurationForSecondReminderLink());
			Date dateTimeTo = currentDate.getTime();
			params[0] = dateTimeFrom;
			params[1] = dateTimeTo;
		}
		
		return params;
	}

	/**
	 * @return
	 */
	private Object[] getRqlQueryInactiveUserParams() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, getInactiveUserDateTimeFrom());  
		Date date = calendar.getTime();

		Object params[] = new Object[1];
		params[0] = date;
		return params;
	}

	/**
	 * @param pUserItems
	 * @param pFileName
	 * @param pSheetName
	 * @param emailParams
	 * @param attachments
	 * @return
	 */
	private File[] createAndWriteFile(RepositoryItem[] pUserItems, String pFileName, String pSheetName, HashMap<String, Object> emailParams, File[] attachments,boolean activeUserReport) {
		// Create a Workbook
		Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for
											// generating `.xls` file
		boolean addLastLogin =false;
		if(getReportsShouldHavelastLogin().contains(pFileName) || isEnableLastLoginForAllReports()){
			addLastLogin=true;
			getHeaderColumns().add(LAST_LOGIN);
			getActiveUserHeaderColumns().add(LAST_LOGIN);
		}
		// Create a Sheet
		Sheet sheet = workbook.createSheet(pSheetName);

		// Create a Font for styling header cells
		Font headerFont = workbook.createFont();
		headerFont.setFontHeightInPoints((short) 10);
		headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		// Create a Row
		Row headerRow = sheet.createRow(0);

		// Create header cells
		if(!activeUserReport){
			for (int i = 0; i < getHeaderColumns().size(); i++) {
					createHeaderCell(getHeaderColumns(), headerCellStyle, headerRow, i);	
			
			}
		}else{
			for (int i = 0; i < getActiveUserHeaderColumns().size(); i++) {
				createHeaderCell(getActiveUserHeaderColumns(), headerCellStyle, headerRow, i);
			}	
		}
		

		// Create Other rows and cells with newly registered user data
		int rowNum = 1;
		for (RepositoryItem userItem : pUserItems) {
			Timestamp ts = (Timestamp) userItem.getPropertyValue(CPSConstants.LAST_ACTIVITY);
			Date date = new Date();
			String lastlogin ="";
			if(ts != null){
			date.setTime(ts.getTime());
			lastlogin = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(date);
			}
			Timestamp timeStamp = (Timestamp) userItem.getPropertyValue(CPSConstants.REGISTRATION_DATE);
			String registrationDate ="";
			if(timeStamp != null){
				date.setTime(timeStamp.getTime());
				registrationDate = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(date);
			}
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue((String) userItem.getPropertyValue(CPSConstants.FIRST_NAME));
			row.createCell(1).setCellValue((String) userItem.getPropertyValue(CPSConstants.LAST_NAME));
			if(!activeUserReport){
				RepositoryItem billingAddressItem = (RepositoryItem) userItem
						.getPropertyValue(CPSConstants.BILLING_ADDRESS);
				if (billingAddressItem != null) {
					String accountNumber = (String) billingAddressItem
							.getPropertyValue(CPSConstants.CONTACT_INFO_JDE_ADDRESS_NUMBER);
					String zipCode = (String) billingAddressItem.getPropertyValue(CPSConstants.POSTAL_CODE);

					row.createCell(2).setCellValue(accountNumber);
					row.createCell(3).setCellValue(zipCode);
				}
				row.createCell(4).setCellValue((String) userItem.getPropertyValue(CPSConstants.EMAIL));
				row.createCell(5).setCellValue((String) userItem.getPropertyValue(CPSConstants.SUPERVISOR_NAME));
				row.createCell(6).setCellValue((String) userItem.getPropertyValue(CPSConstants.SUPERVISOR_EMAIL));

				RepositoryItem parentOrg = (RepositoryItem) userItem.getPropertyValue(CPSConstants.PARENT_ORG);
				if(parentOrg != null) {
					RepositoryItem billingAddress = (RepositoryItem) parentOrg.getPropertyValue(CPSConstants.ADDRESS_BILLING);
					if(billingAddress != null){
						row.createCell(7).setCellValue((String) billingAddress.getPropertyValue( CPSConstants.PHONE_NUMBER));
						row.createCell(8).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.REGIONAL_MANAGER));
						row.createCell(9).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.REGIONAL_MANAGER_DESCRIPTION));
						row.createCell(10).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.OUTSIDE_REP));
						row.createCell(11).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.OUTSIDE_REP_DESCRIPTION));
						RepositoryItem workGroupItem = (RepositoryItem) billingAddress.getPropertyValue(CPSConstants.WORKGROUP_NUMBER);
						if(workGroupItem != null) {
							vlogDebug("Work Group Number : {0}",workGroupItem.getRepositoryId());
							row.createCell(12).setCellValue((String) workGroupItem.getRepositoryId());
							row.createCell(13).setCellValue((String) workGroupItem.getPropertyValue(CPSConstants.DESCRIPTION));
						}
						row.createCell(14).setCellValue(registrationDate);
						if(addLastLogin){
							row.createCell(getHeaderColumns().size()-1).setCellValue(lastlogin);
						}
					}
				}
			}else{
				row.createCell(2).setCellValue((String) userItem.getPropertyValue(CPSConstants.EMAIL));
				Set<RepositoryItem> roles=(Set<RepositoryItem>) userItem.getPropertyValue(CPSConstants.ROLES);
				StringBuilder userRoles = new StringBuilder();
				for (RepositoryItem role : roles) {
					userRoles.append(role.getRepositoryId()).append(CPSConstants.COMMA);
				}
				if(StringUtils.endsWith(userRoles.toString(), CPSConstants.COMMA)){
					userRoles.deleteCharAt(userRoles.length() - 1);
				}
				row.createCell(3).setCellValue(userRoles.toString());
				RepositoryItem currentOrganization = (RepositoryItem) userItem
						.getPropertyValue(CPSConstants.CURRENT_ORG);
				if (currentOrganization != null){
					String organizationName= (String) currentOrganization.getPropertyValue(CPSConstants.NAME);
					String organizationNumber=(String) currentOrganization.getRepositoryId();
					row.createCell(4).setCellValue(organizationName);
					row.createCell(5).setCellValue(organizationNumber);

				}
				RepositoryItem parentOrg = (RepositoryItem) userItem.getPropertyValue(CPSConstants.PARENT_ORG);
				if(parentOrg != null) {
					RepositoryItem billingAddress = (RepositoryItem) parentOrg.getPropertyValue(CPSConstants.ADDRESS_BILLING);
					if(billingAddress != null){
						row.createCell(6).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.PHONE_NUMBER));
						row.createCell(7).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.REGIONAL_MANAGER));
						row.createCell(8).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.REGIONAL_MANAGER_DESCRIPTION));
						row.createCell(9).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.OUTSIDE_REP));
						row.createCell(10).setCellValue((String) billingAddress.getPropertyValue(CPSConstants.OUTSIDE_REP_DESCRIPTION));
						RepositoryItem workGroupItem = (RepositoryItem) billingAddress.getPropertyValue(CPSConstants.WORKGROUP_NUMBER);
						if(workGroupItem != null) {
							vlogDebug("Work Group Number : {0}",workGroupItem.getRepositoryId());
							row.createCell(11).setCellValue((String) workGroupItem.getRepositoryId());
							row.createCell(12).setCellValue((String) workGroupItem.getPropertyValue(CPSConstants.DESCRIPTION));
						}
						
					}
				}
				row.createCell(13).setCellValue((String) userItem.getRepositoryId());
				row.createCell(14).setCellValue(registrationDate);
				if(addLastLogin){
				row.createCell(getActiveUserHeaderColumns().size()-1).setCellValue(lastlogin);
				}
			}


		}

		// Resize all columns to fit the content size
		for (int i = 0; i < getHeaderColumns().size(); i++) {
			sheet.autoSizeColumn(i);
		}

		// Write the output to a file
		FileOutputStream fileOut = null;
		try {
			StringBuilder localFilePath = new StringBuilder();
			StringBuilder filePath = new StringBuilder();
			Timestamp currentDate = getCurrentDate().getTimeAsTimestamp();
			makeDirectory();
			String fileLocation = filePath.append(getFileLocationDirectory()).append(pFileName).append(currentDate).append(getFileExtension()).toString();			
			String localFileLocation = localFilePath.append(getLocalDirectory()).append(pFileName).append(currentDate).append(getFileExtension()).toString();
			fileLocation = fileLocation.replaceAll("\\s", "_");
			localFileLocation = localFileLocation.replaceAll("\\s", "_");
			fileOut = new FileOutputStream(localFileLocation);
			workbook.write(fileOut);
			fileOut.close();
			
			getFilePermissionsHelper().setFilePermissions(localFileLocation);
			String downloadFileLink;
			File destFile = new File(localFileLocation);
			attachments[0] = destFile;
			if(emailParams != null){		
				downloadFileLink = new StringBuilder(CPSConstants.HTTP_PROTOCOL).append(emailParams.get(CPSConstants.HOST_NAME)).append(fileLocation).toString();
				emailParams.put(CPSConstants.DOWNLOAD_FILE_LINK, downloadFileLink);	
			}else{
				 downloadFileLink = new StringBuilder(CPSConstants.HTTP_PROTOCOL).append(getActiveUserExportHostName()).append(fileLocation).toString();
			}
			setActiveUserGeneratedUrl(downloadFileLink);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (getHeaderColumns().contains(LAST_LOGIN) || getActiveUserHeaderColumns().contains(LAST_LOGIN)) {
			getHeaderColumns().remove(LAST_LOGIN);
			getActiveUserHeaderColumns().remove(LAST_LOGIN);
		}
		return attachments;
	}

	/**
	 * To make directory if directory is not exist
	 */
	private void makeDirectory() {
		File files = new File(getLocalDirectory());
		if (!files.exists()) {	        	
		    files.mkdirs();
		}
	}

	/**
	 * @param headerColumns
	 * @param headerCellStyle
	 * @param headerRow
	 * @param i
	 */
	private void createHeaderCell(List<String> headerColumns, CellStyle headerCellStyle, Row headerRow, int i) {
		Cell cell = headerRow.createCell(i);
		cell.setCellValue((String) headerColumns.get(i));
		cell.setCellStyle(headerCellStyle);
	}

	/**
	 * @return the profileTools
	 */
	public CPSProfileTools getProfileTools() {
		return profileTools;
	}

	/**
	 * @param profileTools
	 *            the profileTools to set
	 */
	public void setProfileTools(CPSProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	/**
	 * @return the commonEmailSender
	 */
	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	/**
	 * @param commonEmailSender
	 *            the commonEmailSender to set
	 */
	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}

	/**
	 * @return the repositoryUtils
	 */
	public RepositoryUtils getRepositoryUtils() {
		return repositoryUtils;
	}

	/**
	 * @param repositoryUtils
	 *            the repositoryUtils to set
	 */
	public void setRepositoryUtils(RepositoryUtils repositoryUtils) {
		this.repositoryUtils = repositoryUtils;
	}

	/**
	 * @return the filePermissionsHelper
	 */
	public FilePermissionsHelper getFilePermissionsHelper() {
		return filePermissionsHelper;
	}

	/**
	 * @param filePermissionsHelper the filePermissionsHelper to set
	 */
	public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
		this.filePermissionsHelper = filePermissionsHelper;
	}

	/**
	 * @return the newUserFileName
	 */
	public String getNewUserFileName() {
		return newUserFileName;
	}

	/**
	 * @param newUserFileName
	 *            the newUserFileName to set
	 */
	public void setNewUserFileName(String newUserFileName) {
		this.newUserFileName = newUserFileName;
	}

	/**
	 * @return the newUserSheetName
	 */
	public String getNewUserSheetName() {
		return newUserSheetName;
	}

	/**
	 * @param newUserSheetName
	 *            the newUserSheetName to set
	 */
	public void setNewUserSheetName(String newUserSheetName) {
		this.newUserSheetName = newUserSheetName;
	}

	/**
	 * @return the inactiveUserFileName
	 */
	public String getInactiveUserFileName() {
		return inactiveUserFileName;
	}

	/**
	 * @param inactiveUserFileName
	 *            the inactiveUserFileName to set
	 */
	public void setInactiveUserFileName(String inactiveUserFileName) {
		this.inactiveUserFileName = inactiveUserFileName;
	}

	/**
	 * @return the inactiveUserSheetName
	 */
	public String getInactiveUserSheetName() {
		return inactiveUserSheetName;
	}

	/**
	 * @param inactiveUserSheetName
	 *            the inactiveUserSheetName to set
	 */
	public void setInactiveUserSheetName(String inactiveUserSheetName) {
		this.inactiveUserSheetName = inactiveUserSheetName;
	}

	/**
	 * @return the localDirectory
	 */
	public String getLocalDirectory() {
		return localDirectory;
	}

	/**
	 * @param localDirectory
	 *            the localDirectory to set
	 */
	public void setLocalDirectory(String localDirectory) {
		this.localDirectory = localDirectory;
	}

	/**
	 * @return the fileLocationDirectory
	 */
	public String getFileLocationDirectory() {
		return fileLocationDirectory;
	}

	/**
	 * @param fileLocationDirectory
	 *            the fileLocationDirectory to set
	 */
	public void setFileLocationDirectory(String fileLocationDirectory) {
		this.fileLocationDirectory = fileLocationDirectory;
	}

	/**
	 * @return the fileExtension
	 */
	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * @param fileExtension
	 *            the fileExtension to set
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/**
	 * @return the rqlQueryNewUser
	 */
	public String getRqlQueryNewUser() {
		return rqlQueryNewUser;
	}

	/**
	 * @param rqlQueryNewUser
	 *            the rqlQueryNewUser to set
	 */
	public void setRqlQueryNewUser(String rqlQueryNewUser) {
		this.rqlQueryNewUser = rqlQueryNewUser;
	}

	/**
	 * @return the rqlQueryOverThreeDays
	 */
	public String getRqlQueryOverThreeDays() {
		return rqlQueryOverThreeDays;
	}

	/**
	 * @param rqlQueryOverThreeDays
	 *            the rqlQueryOverThreeDays to set
	 */
	public void setRqlQueryOverThreeDays(String rqlQueryOverThreeDays) {
		this.rqlQueryOverThreeDays = rqlQueryOverThreeDays;
	}

	

	/**
	 * @return the headerColumns
	 */
	public List<String> getHeaderColumns() {
		return headerColumns;
	}

	/**
	 * @param headerColumns
	 *            the headerColumns to set
	 */
	public void setHeaderColumns(List<String> headerColumns) {
		this.headerColumns = headerColumns;
	}

	/**
	 * @return the currentDate
	 */
	public CurrentDate getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate
	 *            the currentDate to set
	 */
	public void setCurrentDate(CurrentDate currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the encryptor
	 */
	public AESEncryptor getEncryptor() {
		return encryptor;
	}

	/**
	 * @param encryptor the encryptor to set
	 */
	public void setEncryptor(AESEncryptor encryptor) {
		this.encryptor = encryptor;
	}

	/**
	 * @return the activationUrl
	 */
	public String getActivationUrl() {
		return activationUrl;
	}

	/**
	 * @param activationUrl the activationUrl to set
	 */
	public void setActivationUrl(String activationUrl) {
		this.activationUrl = activationUrl;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the inactiveUserDateTimeFrom
	 */
	public int getInactiveUserDateTimeFrom() {
		return inactiveUserDateTimeFrom;
	}

	/**
	 * @param inactiveUserDateTimeFrom the inactiveUserDateTimeFrom to set
	 */
	public void setInactiveUserDateTimeFrom(int inactiveUserDateTimeFrom) {
		this.inactiveUserDateTimeFrom = inactiveUserDateTimeFrom;
	}

	/**
	 * @return the inactiveUserDateRangeQuery
	 */
	public String getInactiveUserDateRangeQuery() {
		return inactiveUserDateRangeQuery;
	}

	/**
	 * @param inactiveUserDateRangeQuery the inactiveUserDateRangeQuery to set
	 */
	public void setInactiveUserDateRangeQuery(String inactiveUserDateRangeQuery) {
		this.inactiveUserDateRangeQuery = inactiveUserDateRangeQuery;
	}

	/**
	 * @return the startDurationForFirstReminderMail
	 */
	public int getStartDurationForFirstReminderMail() {
		return startDurationForFirstReminderMail;
	}

	/**
	 * @param startDurationForFirstReminderMail the startDurationForFirstReminderMail to set
	 */
	public void setStartDurationForFirstReminderMail(int startDurationForFirstReminderMail) {
		this.startDurationForFirstReminderMail = startDurationForFirstReminderMail;
	}

	/**
	 * @return the endDurationForFirstReminderMail
	 */
	public int getEndDurationForFirstReminderMail() {
		return endDurationForFirstReminderMail;
	}

	/**
	 * @param endDurationForFirstReminderMail the endDurationForFirstReminderMail to set
	 */
	public void setEndDurationForFirstReminderMail(int endDurationForFirstReminderMail) {
		this.endDurationForFirstReminderMail = endDurationForFirstReminderMail;
	}
 
	/**
	 * @return the startDurationForSecondReminderMail
	 */
	public int getStartDurationForSecondReminderMail() {
		return startDurationForSecondReminderMail;
	}

	/**
	 * @param startDurationForSecondReminderMail the startDurationForSecondReminderMail to set
	 */
	public void setStartDurationForSecondReminderMail(int startDurationForSecondReminderMail) {
		this.startDurationForSecondReminderMail = startDurationForSecondReminderMail;
	}

	/**
	 * @return the endDurationForSecondReminderLink
	 */
	public int getEndDurationForSecondReminderLink() {
		return endDurationForSecondReminderLink;
	}

	/**
	 * @param endDurationForSecondReminderLink the endDurationForSecondReminderLink to set
	 */
	public void setEndDurationForSecondReminderLink(int endDurationForSecondReminderLink) {
		this.endDurationForSecondReminderLink = endDurationForSecondReminderLink;
	}

	/**
	 * @return the activeUserGeneratedUrl
	 */
	public String getActiveUserGeneratedUrl() {
		return activeUserGeneratedUrl;
	}

	/**
	 * @param activeUserGeneratedUrl the activeUserGeneratedUrl to set
	 */
	public void setActiveUserGeneratedUrl(String activeUserGeneratedUrl) {
		this.activeUserGeneratedUrl = activeUserGeneratedUrl;
	}

	/**
	 * @return the activeUserExportHostName
	 */
	public String getActiveUserExportHostName() {
		return activeUserExportHostName;
	}

	/**
	 * @param activeUserExportHostName the activeUserExportHostName to set
	 */
	public void setActiveUserExportHostName(String activeUserExportHostName) {
		this.activeUserExportHostName = activeUserExportHostName;
	}

	/**
	 * @return the activeUserHeaderColumns
	 */
	public List<String> getActiveUserHeaderColumns() {
		return activeUserHeaderColumns;
	}

	/**
	 * @param activeUserHeaderColumns the activeUserHeaderColumns to set
	 */
	public void setActiveUserHeaderColumns(List<String> activeUserHeaderColumns) {
		this.activeUserHeaderColumns = activeUserHeaderColumns;
	}

	public boolean isEnableLastLoginForAllReports() {
		return enableLastLoginForAllReports;
	}

	public void setEnableLastLoginForAllReports(boolean enableLastLoginForAllReports) {
		this.enableLastLoginForAllReports = enableLastLoginForAllReports;
	}

	public List<String> getReportsShouldHavelastLogin() {
		return reportsShouldHavelastLogin;
	}

	public void setReportsShouldHavelastLogin(List<String> reportsShouldHavelastLogin) {
		this.reportsShouldHavelastLogin = reportsShouldHavelastLogin;
	}
	public String getInactiveOrganizatioQuery() {
	    return inactiveOrganizatioQuery;
	}

	public void setInactiveOrganizatioQuery(String inactiveOrganizatioQuery) {
	    this.inactiveOrganizatioQuery = inactiveOrganizatioQuery;
	}

	public String getUserOrganizationQuery() {
	    return userOrganizationQuery;
	}

	public void setUserOrganizationQuery(String userOrganizationQuery) {
	    this.userOrganizationQuery = userOrganizationQuery;
	}

	public String getInactiveOrganizationFileName() {
	    return inactiveOrganizationFileName;
	}

	public void setInactiveOrganizationFileName(String inactiveOrganizationFileName) {
	    this.inactiveOrganizationFileName = inactiveOrganizationFileName;
	}

	public String getFtpClient() {
	    return ftpClient;
	}

	public void setFtpClient(String ftpClient) {
	    this.ftpClient = ftpClient;
	}

	public int getFtpPort() {
	    return ftpPort;
	}

	public void setFtpPort(int ftpPort) {
	    this.ftpPort = ftpPort;
	}

	public String getFtpUser() {
	    return ftpUser;
	}

	public void setFtpUser(String ftpUser) {
	    this.ftpUser = ftpUser;
	}

	public String getFtpPassword() {
	    return ftpPassword;
	}

	public void setFtpPassword(String ftpPassword) {
	    this.ftpPassword = ftpPassword;
	}

	public String getFtpLocation() {
	    return ftpLocation;
	}

	public void setFtpLocation(String ftpLocation) {
	    this.ftpLocation = ftpLocation;
	}

	public boolean getFtpEnabled() {
	    return ftpEnabled;
	}

	public void setFtpEnabled(boolean ftpEnabled) {
	    this.ftpEnabled = ftpEnabled;
	}

	public String getFtpFileExtension() {
	    return ftpFileExtension;
	}

	public void setFtpFileExtension(String ftpFileExtension) {
	    this.ftpFileExtension = ftpFileExtension;
	}

	public String getKnownHost() {
	    return knownHost;
	}

	public void setKnownHost(String knownHost) {
	    this.knownHost = knownHost;
	}

	public String getFtpLocationDirectory() {
	    return ftpLocationDirectory;
	}

	public void setFtpLocationDirectory(String ftpLocationDirectory) {
	    this.ftpLocationDirectory = ftpLocationDirectory;
	}

	public String getLocalFtpDirectory() {
	    return localFtpDirectory;
	}

	public void setLocalFtpDirectory(String localFtpDirectory) {
	    this.localFtpDirectory = localFtpDirectory;
	}
	
	
}
