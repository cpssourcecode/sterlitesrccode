package com.cps.scheduler;

import atg.commerce.order.Order;
import atg.commerce.states.OrderStates;
import atg.commerce.states.StateDefinitions;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryItem;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.commerce.payment.SnapPayConnectionManager;
import com.cps.email.CommonEmailSender;

import javax.transaction.TransactionManager;

import static com.cps.util.CPSConstants.PAYMENT_TYPE_CC;

public class SnapPayOrderUpdateService extends SingletonSchedulableService {

	private static final String RETRIEVE_ORDERS_FOR_PROCESSING_RQL_QUERY = "state =?0 AND paymentGroups INCLUDES ITEM (paymentGroupClassType =?1) AND isExported = true AND paymentUpdated = false";
	private static final String ORDER_STATE_SUBMITTED = "SUBMITTED";

	private CommonEmailSender mEmailSender;

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	private OrderStates mOrderStates;

	public OrderStates getOrderStates() {
		return mOrderStates;
	}

	public void setOrderStates(OrderStates pOrderStates) {
		mOrderStates = pOrderStates;
	}

	private TransactionManager mTransactionManager;

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	private CPSOrderManager mOrderManager;

	public CPSOrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(CPSOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	private SnapPayConnectionManager mSnapPayConnectionManager;

	public void setSnapPayConnectionManager(SnapPayConnectionManager pSnapPayConnectionManager) {
		mSnapPayConnectionManager = pSnapPayConnectionManager;
	}

	public SnapPayConnectionManager getSnapPayConnectionManager() {
		return mSnapPayConnectionManager;
	}

	private boolean mEnabled;

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	private String mOrderId;

	public String getOrderId() {
		return mOrderId;
	}

	public void setOrderId(String pOrderId) {
		mOrderId = pOrderId;
	}

	/* The email to */
	private String mEmailTo;

	public void setEmailTo(String pEmailTo) {
		mEmailTo = pEmailTo;
	}

	public String getEmailTo() {
		return mEmailTo;
	}

	@Override
	public void doScheduledTask(Scheduler pParamScheduler, ScheduledJob pParamScheduledJob) {

		if (isEnabled()) {
			vlogDebug("Scheduled task start");
			processOrders();
			vlogDebug("Scheduled task end");
		}

	}

	/**
	 * force run job once
	 */
	public void runJobOnce() {
		doScheduledTask(null, null);
	}

	public void updateOrderById() {
		if (isEnabled() && !StringUtils.isBlank(getOrderId())) {
			try {
				processOrder(getOrderManager().loadOrder(getOrderId()));
			} catch (Exception e) {
				vlogError(e, "Error");
			}
		}
	}

	private void processOrders() {
		Object[] params = new Object[2];
		params[0] = ORDER_STATE_SUBMITTED;
		params[1] = PAYMENT_TYPE_CC;
		RepositoryItem[] orders = getOrderManager().retrieveOrders(RETRIEVE_ORDERS_FOR_PROCESSING_RQL_QUERY, params);
		if (orders != null && orders.length > 0) {
			for (RepositoryItem order: orders) {
				boolean result = processOrderInTransaction(order);
				if (result) {
					vlogDebug("External Order Update: order was processed successfully. order: {0}", order.getRepositoryId());
				} else {
					vlogWarning("External Order Update: order was not processed. order: {0}", order.getRepositoryId());
				}
			}
		}
	}

	private void sendFailureEmail(Order pOrder) {
		getEmailSender().sendOrderUpdateFailureEmail(getEmailTo(), pOrder);
	}

	private boolean processOrderInTransaction(RepositoryItem pOrder) {

		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = true;
		try {
			final Order order = getOrderManager().loadOrder(pOrder.getRepositoryId());
			if (null != tm) {
				td.begin(tm, TransactionDemarcation.REQUIRED);
			}
			boolean result;
			synchronized (order) {
				result = processOrder(order);
				rollback = false;
			}
			if (!result) {
				sendFailureEmail(order);
			}
		} catch (Exception e) {
			vlogError(e, "processOrder: Exception while processing order");
		} finally {
			try {
				td.end(rollback);
			} catch (TransactionDemarcationException e) {
				vlogError(e, "Error");
			}
		}

		return !rollback;

	}

	/**
	 * Processes order
	 *
	 * @param pOrder pOrder
	 * @return true if successful, false otherwise.
	 */
	private boolean processOrder(Order pOrder) throws Exception {
		CPSOrderImpl order = (CPSOrderImpl)pOrder;
		boolean result = getSnapPayConnectionManager().externalOrderUpdate(order);
		synchronized (order) {
			if (result) {
				order.setPaymentUpdated(true);
			} else {
				pOrder.setState(StateDefinitions.ORDERSTATES.getStateValue(OrderStates.PENDING_MERCHANT_ACTION));
			}
			getOrderManager().updateOrder(pOrder);
		}
		return result;
	}

}
