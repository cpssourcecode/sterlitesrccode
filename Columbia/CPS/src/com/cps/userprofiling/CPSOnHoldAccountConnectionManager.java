package com.cps.userprofiling;

import java.util.HashMap;
import java.util.Map;

import com.cps.integrations.onholdaccount.CPSOnHoldAccountClient;
import com.cps.integrations.onholdaccount.CPSOnHoldAccountConstants;

import atg.nucleus.GenericService;
import vsg.exception.WebServiceException;

/**
 * CPSOnHoldAccountConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Steve Neverov
 * 
 */
public class CPSOnHoldAccountConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/userprofiling/CPSOnHoldAccountConnectionManager";
    private boolean testMode;
    private boolean defaultOnHoldValue; // for testing purposes. IF test mode is true and this value is true, the user is 'OnHold'

    /**
     * @return the testMode
     */
    public boolean isTestMode() {
        return testMode;
    }

    /**
     * @param testMode
     *            the testMode to set
     */
    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    /**
     * @return the defaultOnHoldValue
     */
    public boolean isDefaultOnHoldValue() {
        return defaultOnHoldValue;
    }

    /**
     * @param defaultOnHoldValue
     *            the defaultOnHoldValue to set
     */
    public void setDefaultOnHoldValue(boolean defaultOnHoldValue) {
        this.defaultOnHoldValue = defaultOnHoldValue;
    }

    /** Client property */
	private CPSOnHoldAccountClient mClient;

	/**
	 * @return
	 */
	public CPSOnHoldAccountClient getClient() {
		return mClient;
	}

	/**
	 * @param pClient
	 */
	public void setClient(CPSOnHoldAccountClient pClient) {
		mClient = pClient;
	}

	public boolean isAccountOnHold(String pCustomerNumber) {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".isAccountOnHold").append(" - start").toString());
		}

        if (isTestMode()) {
            return isDefaultOnHoldValue();
		}

		boolean isAccountOnHold = true;

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CPSOnHoldAccountConstants.CUSTOMER_NUMBER, pCustomerNumber);
		try {
			Map<String, Object> reply = getClient().requestOnHoldStatus(params);
			if (reply != null && reply.size() > 0) {
				isAccountOnHold = (Boolean)reply.get(CPSOnHoldAccountConstants.IS_ON_HOLD);
			}
		} catch (WebServiceException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".isAccountOnHold").append(" - exit").toString());
		}

		return isAccountOnHold;

	}

}
