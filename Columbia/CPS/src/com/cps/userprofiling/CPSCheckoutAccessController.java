package com.cps.userprofiling;

import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;
import com.cps.util.CPSConstants;

/**
 * Allow access based on user role and selectedCS
 */
public class CPSCheckoutAccessController extends GuestCheckoutAccessController {
	
	@Override
	public boolean allowAccess(Profile pProfile, DynamoHttpServletRequest pRequest) {
		if (isEnabled()) {
			final boolean userHasNoCS = !pProfile.isTransient() && pProfile.getPropertyValue(CPSConstants.SELECTED_CS) == null;
			if (!super.allowAccess(pProfile, pRequest) || userHasNoCS) {
				return false;
			}
		}
		return true;
	}
	
}
