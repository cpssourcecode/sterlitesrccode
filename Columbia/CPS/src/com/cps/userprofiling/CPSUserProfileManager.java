package com.cps.userprofiling;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.TransactionManager;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import com.cps.util.CPSMessageUtils;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.RepositoryException;
import atg.userprofiling.ProfileTools;
import vsg.load.loader.CommonSettings;

/**
 * @author Vignesh 
 * Manager Class for user profile Related Activity
 *
 */
public class CPSUserProfileManager extends GenericService{
	private ProfileTools profileTools;
	private CommonSettings commonSettings;
	public List<String> removeUserItems(String [] userList){
		List<String> errorUser = new ArrayList<String>();
		for (String id : userList) {
			 TransactionManager tm = getCommonSettings().getTransactionManager();
	 		 TransactionDemarcation td = new TransactionDemarcation();
		try{
			td.begin(tm, TransactionDemarcation.REQUIRES_NEW);
			getProfileTools().getProfileRepository().removeItem(id, "user");
		} catch (RepositoryException re) {
			vlogError("Error in processing Profile {0}", re);
			errorUser.add(id);
		} catch (TransactionDemarcationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			 try { if (tm != null) td.end(); }
	           catch (TransactionDemarcationException e) {
	        	   vlogError("Error Removing From DB transaction Error {0}", e);
	           }
		}

		}
		return errorUser;
	}

	/**
	 * @return the profileTools
	 */
	public ProfileTools getProfileTools() {
		return profileTools;
	}

	/**
	 * @param profileTools the profileTools to set
	 */
	public void setProfileTools(ProfileTools profileTools) {
		this.profileTools = profileTools;
	}

	/**
	 * @return the commonSettings
	 */
	public CommonSettings getCommonSettings() {
		return commonSettings;
	}

	/**
	 * @param commonSettings the commonSettings to set
	 */
	public void setCommonSettings(CommonSettings commonSettings) {
		this.commonSettings = commonSettings;
	}

}
