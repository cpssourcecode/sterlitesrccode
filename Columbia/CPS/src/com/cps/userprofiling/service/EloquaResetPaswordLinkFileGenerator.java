package com.cps.userprofiling.service;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.*;
import atg.repository.rql.RqlStatement;
import com.cps.email.CommonEmailSender;
import vsg.load.file.IWriter;
import vsg.load.file.csv.CSVWriter;
import vsg.load.tools.impl.ImportTools;

import java.io.File;


import static com.cps.email.CPSEmailConstants.EMAIL_FIELD;
import static com.cps.util.CPSConstants.*;
import static vsg.load.file.csv.CSVBase.NO_QUOTE_CHARACTER;

/**
 * @author Andy Porter
 */
public class EloquaResetPaswordLinkFileGenerator extends GenericService {

	/**
	 * ProfileRepository
	 */
	private Repository mProfileRepository;

	private ImportTools mImportTools;
	private ForgotPasswordService mForgotPasswordService;
	private String mEloquaFilePath;
	private CommonEmailSender mEmailSender;

	private static final String RETRIEVE_USERS_RQL_QUERY = "NOT eloquaId IS NULL";


	private RepositoryItem[] retrieveUsersWithEloquaID() throws RepositoryException {
		Repository orderRepository = getProfileRepository();
		RepositoryView userView = orderRepository.getView(USER_ITEM_DESCRIPTOR);
		RqlStatement rqlStatement = RqlStatement.parseRqlStatement(RETRIEVE_USERS_RQL_QUERY);
		RepositoryItem[] users = rqlStatement.executeQuery(userView, new Object[]{});
		return users;
	}


	private IWriter getCsvWriter(File pFile) {
		CSVWriter csvWriter = (CSVWriter) getImportTools().getWriter(pFile);
		csvWriter.setSeparator(getImportTools().getFileSeparator());
		csvWriter.setQuoteChar(NO_QUOTE_CHARACTER);
		return csvWriter;
	}


	/**
	 * check if file exist
	 *
	 * @param pFileName file name
	 * @return file exist
	 */
	private static boolean isFileExist(String pFileName) {
		try {
			File pFile = new File(pFileName);
			return pFile.exists();
		} catch (Exception e) {
			return false;
		}
	}

	public void ExportResetPaswordLinkFile() {

		IWriter iWriter = null;
		// check if file exist
		boolean isExist = isFileExist(getEloquaFilePath());
		if (isExist) {
			isExist = !((new File(getEloquaFilePath())).delete());
		}
		if (!isExist) {
			File addFile = new File(getEloquaFilePath());
			iWriter = getCsvWriter(addFile);
		}
		if (null != iWriter) {
			RepositoryItem[] users = null;
			try {
				users = retrieveUsersWithEloquaID();
			} catch (RepositoryException re) {
				vlogError("process(): users == null, no users will be exported into .csv file", re);
			}
			if (null == users) {
				return;
			}
			for (RepositoryItem user : users) {
				String password = (String) user.getPropertyValue(PSWD);
				if (StringUtils.isBlank(password) || password.length() < 15) {
					// we export user with generated passwords only in case that is brandnew users with blank password and ELOQUA_ID set

					String repositoryId = user.getRepositoryId();
					String eloquaId = (String) user.getPropertyValue(ELOQUA_ID);
					String email = (String) user.getPropertyValue(EMAIL_FIELD);
					String forgotPasswordLink = "";
					forgotPasswordLink = getForgotPasswordService().generateNonExpireLink(forgotPasswordLink, user);
					forgotPasswordLink = getEmailSender().getSiteUrl() + forgotPasswordLink;
					((MutableRepositoryItem) user).setPropertyValue(FORGOT_PSWD, true);
					((MutableRepositoryItem) user).setPropertyValue(EXPORTED_WITH_ELOQUA_ID, true);

					String[] processedFieldsArr = new String[4];
					processedFieldsArr[0] = repositoryId;
					processedFieldsArr[1] = eloquaId;
					processedFieldsArr[2] = email;
					processedFieldsArr[3] = forgotPasswordLink;
					iWriter.writeNext(processedFieldsArr);
				}
			}
			iWriter.close();
		}
	}


	// Getters and Setters

	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}

	public ForgotPasswordService getForgotPasswordService() {
		return mForgotPasswordService;
	}

	public void setForgotPasswordService(ForgotPasswordService pForgotPasswordService) {
		mForgotPasswordService = pForgotPasswordService;
	}

	public String getEloquaFilePath() {
		return mEloquaFilePath;
	}

	public void setEloquaFilePath(String pEloquaFilePath) {
		mEloquaFilePath = pEloquaFilePath;
	}

	public ImportTools getImportTools() {
		return mImportTools;
	}

	public void setImportTools(ImportTools pImportTools) {
		mImportTools = pImportTools;
	}

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}
}
