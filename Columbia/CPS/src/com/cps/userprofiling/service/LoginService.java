package com.cps.userprofiling.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.cps.csr.userprofiling.CPSCSRProfileTools;
import com.cps.userprofiling.CPSOnHoldAccountConnectionManager;
import com.cps.userprofiling.util.LoginSessionInfo;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.security.CriptoUtils;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;
import vsg.crypto.EncryptorException;

/**
 * @author Dmitry Golubev
 */
public class LoginService extends GenericService {
	/**
	 * profile tools
	 */
	private ProfileTools mProfileTools;
	/**
	 * On Hold Account Connection manager
	 */
	private CPSOnHoldAccountConnectionManager mOnHoldAccountConnectionManager;
	

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * checks if user locked and unlocks it if lock time is expired
	 * @param pUser user
	 * @return true if locked
	 */
	public boolean isUserActive(RepositoryItem pUser) {
		Boolean isActiveUser = (Boolean) pUser.getPropertyValue(CPSConstants.IS_ACTIVE);
		return isActiveUser!=null && isActiveUser;
	}
	
	public boolean isUserOnHold(RepositoryItem pUser) {
		Boolean isOnHold = (Boolean) pUser.getPropertyValue(CPSConstants.ON_HOLD);
		if (isOnHold == null) {
			isOnHold = false;
		}
		return isOnHold;
	}

	/**
	 * checks if user locked and unlocks it if lock time is expired
	 * @param pUser user
	 * @return true if locked
	 */
	public boolean isUserLocked(RepositoryItem pUser, LoginSessionInfo pLoginSessionInfo) {
		if(isDateBefore(new Date(), (Date) pUser.getPropertyValue(CPSConstants.SESSION_LOCKED))) {
			unLockUser((MutableRepositoryItem) pUser, pLoginSessionInfo);
			return false;
		} else {
			return true;
		}
	}
	
	public boolean isAccountInactive(RepositoryItem pUser, LoginSessionInfo pLoginSessionInfo) {
		RepositoryItem  parentOrganization=(RepositoryItem) pUser.getPropertyValue(CPSConstants.PARENT_ORG);
		if(parentOrganization == null) {
			return true;
		}else{
			return false;
		}
	}

	/**
	 * locks user for 30 min
	 *
	 * @param pProfile   current profile
	 * @param pUserLogin user login
	 */
	public void lockUser(Profile pProfile, String pUserLogin, LoginSessionInfo pLoginSessionInfo) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, 30);

		// NT: Commented per CPS-1180 - Don't lock the browser session, only lock the account (if exists)
		//pProfile.setPropertyValue(CPSConstants.SESSION_LOCKED, c.getTime());

		MutableRepositoryItem userRepItem = (MutableRepositoryItem) getProfileTools().getItem(pUserLogin, null);
		if (userRepItem != null) {
			userRepItem.setPropertyValue(CPSConstants.SESSION_LOCKED, c.getTime());
		} else {
			vlogError("Unable to find user by login {0}", pUserLogin);
		}

		//pLoginSessionInfo.resetLoginErrors();
		//pLoginSessionInfo.setUserLocked(true);
	}

	/**
	 * unlocks user
	 * @param pProfile current profile
	 */
	public void unLockUser(MutableRepositoryItem pProfile, LoginSessionInfo pLoginSessionInfo) {
		pProfile.setPropertyValue(CPSConstants.SESSION_LOCKED, null);
		pLoginSessionInfo.setUserLocked(false);
	}

	/**
	 * check default CS for user
	 * @param pProfile user profile
	 */
	public void initSelectedCS(Profile pProfile, LoginSessionInfo pLoginSessionInfo) {
		pLoginSessionInfo.setShouldSelectCS(true);
		if(! initSelectedCSByAssociatedCS(pProfile, pLoginSessionInfo) ) {
			initSelectedCSByOrg(pProfile, pLoginSessionInfo);
		}
	}

	/**
	 * @param pProfile profile
	 * @return true there are associated CSs
	 */
	private boolean initSelectedCSByAssociatedCS(Profile pProfile, LoginSessionInfo pLoginSessionInfo) {
		boolean inited = false;
		Map<String, RepositoryItem> associatedCS = new HashMap<String, RepositoryItem>();
		Integer userType = (Integer) pProfile.getPropertyValue(CPSConstants.USER_TYPE);
		vlogDebug("User type: "+userType);
		if (userType!=null && userType != 3) {
			associatedCS = (Map<String, RepositoryItem>)pProfile.getPropertyValue(CPSConstants.SECONDARY_ADDRESSES);
		}
		vlogDebug("Users associated CS: "+associatedCS);

		if (associatedCS != null && !associatedCS.isEmpty()) {
			inited = true;
			RepositoryItem cs = (RepositoryItem)pProfile.getPropertyValue(CPSConstants.SHIPPING_ADDRESS);
			vlogDebug("User Profile Default CS: "+cs);
			if (cs != null){
				pProfile.setPropertyValue(CPSConstants.SELECTED_CS, cs);
				pLoginSessionInfo.setShouldSelectCS(true);
			}
			if (associatedCS.size() == 1){
				pLoginSessionInfo.setShouldSelectCS(false);
			}
		} else {
			if(associatedCS == null || associatedCS.isEmpty()) {
				pLoginSessionInfo.setShouldSelectCS(false); // nothing to select
			}
		}
		return inited;
	}

	/**
	 * @param pProfile profile
	 * @param pLoginSessionInfo login session info
	 * @return true if parent organization has addresses
	 */
	private boolean initSelectedCSByOrg(Profile pProfile, LoginSessionInfo pLoginSessionInfo) {
		boolean inited = false;
		if (pProfile.getPropertyValue(CPSConstants.PARENT_ORG) != null) {
			RepositoryItem parentOrg = (RepositoryItem) pProfile.getPropertyValue(CPSConstants.PARENT_ORG);
			Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) parentOrg.getPropertyValue(
                            CPSConstants.SECONDARY_ADDRESSES);
			vlogDebug("Secondary addresses org: "+secondaryAddresses);
			if (secondaryAddresses != null && !secondaryAddresses.isEmpty()) {
				inited = true;
				RepositoryItem cs = (RepositoryItem)pProfile.getPropertyValue(CPSConstants.SHIPPING_ADDRESS);
				vlogDebug("User Profile Default CS: "+cs);
				pProfile.setPropertyValue(CPSConstants.SELECTED_CS, cs);
				pLoginSessionInfo.setShouldSelectCS(true);
				if (secondaryAddresses.size() == 1){
					pLoginSessionInfo.setShouldSelectCS(false);
				}
			} else {
				if (secondaryAddresses == null || secondaryAddresses.isEmpty()) {
					pLoginSessionInfo.setShouldSelectCS(false);
				}
			}
		}
		return inited;
	}

	/**
	 * @param pCurrentDate date to check before
	 * @param pCheckDate check date
	 * @return true if check date is before current date
	 */
	private boolean isDateBefore(Date pCurrentDate, Date pCheckDate) {
		return pCheckDate == null || pCheckDate.before( pCurrentDate );
	}

	/**
	 * checks if user is on hold
	 * @param pCustomerId pCustomerId
	 * @return true if is on hold
	 */
	public boolean isUserOnHold(String pCustomerId) {
		return getOnHoldAccountConnectionManager().isAccountOnHold(pCustomerId);
	}

	/**
	 * checks if user is on hold and update appropriate property
	 * @param pProfile profile
	 * @return true if locked
	 */
	public void checkUserOnHold(Profile pProfile, LoginSessionInfo pLoginSessionInfo) {

		RepositoryItem parentOrg = (RepositoryItem) pProfile.getPropertyValue(CPSConstants.PARENT_ORG);
		if (parentOrg != null) {
			boolean isUserOnHold = isUserOnHold(parentOrg.getRepositoryId());
			pProfile.setPropertyValue(CPSConstants.ON_HOLD, isUserOnHold);
			pLoginSessionInfo.setOnHold(isUserOnHold);
		}

	}
	
	/**
	 * Check user after login, if csr user, 
	 * remove parent org that may be left over from previous session
	 * if user didn't stop access
	 * @param profile
	 * @param loginSessionInfo
	 */
	public boolean checkCSRUser(Profile profile, LoginSessionInfo loginSessionInfo) {
		if (((CPSCSRProfileTools)getProfileTools()).isCSRUser(profile)){
			profile.setPropertyValue(CPSConstants.CSR_SELECTED_ORG, null);
			profile.setPropertyValue(CPSConstants.PARENT_ORG, null);
			loginSessionInfo.setShouldRedirect(true);
			return true;
		}
		return false;
	}
	

	public String encryptLinkParams(String pProfileId, String pLogin) {
		StringBuilder result = new StringBuilder();
		DateFormat dateFormat = new SimpleDateFormat(CPSConstants.SIMPLE_DATE_FORMAT);
		Date date = new Date();
		String dateString = dateFormat.format(date);
		if (!StringUtils.isBlank(pLogin) && !StringUtils.isBlank(pProfileId)) {
			try {
				result.append("?ln=").append(cryptString(pLogin));
				result.append("&pr=").append(cryptString(pProfileId));
				result.append("&dt=").append(cryptString(dateString));
			} catch (EncryptorException e) {
				logError(e);
			}
		}
		return result.toString();
	}
	
    public String encryptLinkParams(String pProfileId, String pLogin, String tempPassword) {
        String encryptedString = encryptLinkParams(pProfileId, pLogin);

        if (org.apache.commons.lang3.StringUtils.isNotBlank(tempPassword)) {
            try {
                encryptedString = encryptedString.concat("&tp=").concat(cryptString(tempPassword));
        } catch (EncryptorException e) {
            logError(e);
        }
        }
            return encryptedString;

    }

	private String cryptString(String pParam) throws EncryptorException {
		String result = null;
		if (!StringUtils.isBlank(pParam)) {
			result = CriptoUtils.aesEncrypt(pParam);
		}
		return result;
	}

	//------------------------------------------------------------------------------------------------------------------


	public ProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public CPSOnHoldAccountConnectionManager getOnHoldAccountConnectionManager() {
		return mOnHoldAccountConnectionManager;
	}

	public void setOnHoldAccountConnectionManager(CPSOnHoldAccountConnectionManager pOnHoldAccountConnectionManager) {
		mOnHoldAccountConnectionManager = pOnHoldAccountConnectionManager;
	}


}
