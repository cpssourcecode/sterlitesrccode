package com.cps.userprofiling.service;


import static com.cps.util.CPSConstants.CART_SELECTED_CS;
import static com.cps.util.CPSConstants.CART_SELECTED_DELIVERY_METHOD;
import static com.cps.util.CPSConstants.CART_SELECTED_STORE;
import static com.cps.util.CPSConstants.CONTACT_INFO;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_PICK_UP;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_SHIPPED;
import static com.cps.util.CPSConstants.IS_AVAILABILITY_CHECKED;
import static com.cps.util.CPSConstants.IS_PRICED;
import static com.cps.util.CPSConstants.RESET_AVAILABLE_QTY;

import java.util.List;

import javax.transaction.TransactionManager;

import com.cps.commerce.order.CPSCommerceItemImpl;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.core.util.CPSContactInfo;
import com.cps.service.CPSExternalProductService;
import com.cps.userprofiling.ProfileDefaultShippingAddressFormHandler;
import com.cps.util.CPSConstants;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemImpl;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.OrderManager;
import atg.commerce.profile.CommerceProfileTools;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;

/**
 * @author Dmitry Golubev
 */
public class ProfileDefaultShippingAddressService extends GenericService {
	/**
	 * profile repository
	 */
	private Repository mProfileRepository;

	// property: orderManager
	OrderManager mOrderManager;

    // private OrderHolder mShoppingCart;
    private String shoppingCartPath;


	private TransactionManager mTransactionManager;

	private CPSExternalProductService mExternalProductService;

	private PropertyManager mPropertyManager;
	
	/**
	 * profile tools
	 */
	private ProfileTools mProfileTools;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pFormHandler profile form
	 * @param pRequest     request
	 * @param pResponse    response
	 */
	public void initProfileDefaultShippingAddress(ProfileDefaultShippingAddressFormHandler pFormHandler, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			RepositoryItem address = getProfileRepository().getItem(pFormHandler.getSelectedId(), CONTACT_INFO);
			pFormHandler.getProfile().setPropertyValue(CPSConstants.SHIPPING_ADDRESS, address);
		} catch (Exception e) {
			vlogError("Unable to init default shipping address", e);
			pFormHandler.addFormException(
					new DropletException("Unable to init default shipping address")
			);
		}
	}

	/**
	 * @param pFormHandler profile form
	 * @param pRequest     request
	 * @param pResponse    response
	 */
	public void initProfileShippingAddress(ProfileDefaultShippingAddressFormHandler pFormHandler, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			RepositoryItem address = getProfileRepository().getItem(pFormHandler.getSelectedId(), CONTACT_INFO);
			pFormHandler.getProfile().setPropertyValue(CPSConstants.SELECTED_CS, address);
            // SM-118. Clear the cartSelectedCS, in case, the user selects the shippingAddress from the modal for the session. otherwise, in the cart page, the
            // cartSelectedCS is stick, leading to confusion
            pFormHandler.getProfile().setPropertyValue(CPSConstants.CART_SELECTED_CS, null);
            pFormHandler.getSessionBean().getProductPricesCache().clear();
		} catch (Exception e) {
			vlogError("Unable to init session shipping address", e);
			pFormHandler.addFormException(
					new DropletException("Unable to init session shipping address")
			);
		}
	}

	public void populateGuestProfileAddress(MutableRepositoryItem pProfile, CPSContactInfo pAddress){
		final String email = pAddress.getEmail();
		final String firstName = pAddress.getFirstName();
		final String lastName = pAddress.getLastName();

		final PropertyManager propertyManager = getPropertyManager();

		pProfile.setPropertyValue(propertyManager.getEmailAddressPropertyName(), email);
		pProfile.setPropertyValue(propertyManager.getFirstNamePropertyName(), firstName);
		pProfile.setPropertyValue(propertyManager.getLastNamePropertyName(), lastName);
	}

	/**
	 * @param pFormHandler profile form
	 * @param pRequest     request
	 * @param pResponse    response
	 */
	public void setProfileShippingAddressOnCart(ProfileDefaultShippingAddressFormHandler pFormHandler, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			Profile profile = pFormHandler.getProfile();
			profile.setPropertyValue(CART_SELECTED_DELIVERY_METHOD, DELEVERY_METHOD_SHIPPED);

			if(!profile.isTransient()) {
				String selectedAddrId = pFormHandler.getSelectedId();
				RepositoryItem address = getProfileRepository().getItem(selectedAddrId, CONTACT_INFO);

				RepositoryItem previousAddress = null;
                OrderHolder shoppingCart = (OrderHolder) pRequest.resolveName(getShoppingCartPath());
                String jdeAddressNumber = mExternalProductService.getProfileTools().getCustomerId(profile, (CPSOrderImpl) shoppingCart.getCurrent());
				if (StringUtils.isNotBlank(jdeAddressNumber)) {
					previousAddress = mProfileRepository.getItem(jdeAddressNumber, CONTACT_INFO);
				}

				String previousCartSelectedDeliveryMethod = (String) profile.getPropertyValue(CART_SELECTED_DELIVERY_METHOD);

				profile.setPropertyValue(CART_SELECTED_CS, address);

				if ((null == previousAddress) ||
						((null != address) && !selectedAddrId.equals(previousAddress.getRepositoryId()))) {
//				DO repricing only when change CS, in case of changing PickupLocation check availability only
					throwFlagsOnCommerceItemsOnOrder(pFormHandler.getOrder(), true);
				} else if (DELEVERY_METHOD_PICK_UP.equals(previousCartSelectedDeliveryMethod)) {
					throwFlagsOnCommerceItemsOnOrder(pFormHandler.getOrder(), false);
				}
			}
		} catch (Exception e) {
			vlogError(e, "Unable to init default shipping address");
			pFormHandler.addFormException(new DropletException("Unable to init default shipping address"));
		}

	}

	/**
	 * @param pFormHandler profile form
	 * @param pRequest     request
	 * @param pResponse    response
	 */

	public void setPickupLocationOnCart(ProfileDefaultShippingAddressFormHandler pFormHandler, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			String selectedPickUpLocationId = pFormHandler.getSelectedId();

			String previousCartSelectedStoreId = (String) pFormHandler.getProfile().getPropertyValue(CART_SELECTED_STORE);
			String previousCartSelectedDeliveryMethod = (String) pFormHandler.getProfile().getPropertyValue(CART_SELECTED_DELIVERY_METHOD);
			
			Profile profile = pFormHandler.getProfile();
			
			if(profile != null){
				MutableRepositoryItem repoProfile = (MutableRepositoryItem) profile.getDataSource();
				
				repoProfile.setPropertyValue(CART_SELECTED_STORE, selectedPickUpLocationId);
				repoProfile.setPropertyValue(CART_SELECTED_DELIVERY_METHOD, DELEVERY_METHOD_PICK_UP);
				
				((MutableRepository) getProfileRepository()).updateItem(repoProfile);
				
			}

			if (!DELEVERY_METHOD_PICK_UP.equals(previousCartSelectedDeliveryMethod) ||
					((null != selectedPickUpLocationId) && (null != previousCartSelectedStoreId) && !selectedPickUpLocationId.equals(previousCartSelectedStoreId))) {
				throwFlagsOnCommerceItemsOnOrder(pFormHandler.getOrder(), false);
			}

		} catch (Exception e) {
			vlogError(e, "Unable to init default shipping address");
			pFormHandler.addFormException(
					new DropletException("Unable to init default shipping address")
			);
		}
	}
	
	/**
	 * clears isAvailabilityChecked flag on commerce item
	 */
	public void clearCIAvailabilityCheck(Order pOrder){
		if (null != pOrder) {
			Order order = pOrder;
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				try {
					td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
					synchronized (order) {
						List<CommerceItem> commerceItems = order.getCommerceItems();

						for (CommerceItem commerceItem : commerceItems) {
							CommerceItemImpl commerceItem1 = (CommerceItemImpl) commerceItem;
							commerceItem1.setPropertyValue(IS_AVAILABILITY_CHECKED, false);
						}
						((CommerceProfileTools) getProfileTools()).getOrderManager().updateOrder(order);
					}
				} catch (Exception e) {
					rollback = true;
					if (isLoggingError()) {
						logError(e);
					}
				} finally {
					td.end(rollback);
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}

		}
	}
	
	private void throwFlagsOnCommerceItemsOnOrder(Order pOrder, boolean pIsShipped) {
		CPSOrderImpl order = (CPSOrderImpl) pOrder;
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;
		try {
			try {
				td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
				synchronized (order) {
					List<CommerceItem> commerceItems = order.getCommerceItems();
					for (CommerceItem commerceItem : commerceItems) {
						CPSCommerceItemImpl itemImpl = (CPSCommerceItemImpl) commerceItem;
						if (pIsShipped) {
							itemImpl.setPropertyValue(IS_PRICED, false);
						}
						itemImpl.setPropertyValue(IS_AVAILABILITY_CHECKED, false);
						itemImpl.setLeadTime(0);
						itemImpl.setAvailableQuantity(RESET_AVAILABLE_QTY);
					}
					// will be called in methods invoking chain
					//((CommerceProfileTools) getProfileTools()).getOrderManager().updateOrder(order);
				}
			} catch (Exception e) {
				rollback = true;
				if (isLoggingError()) {
					logError(e);
				}
			} finally {
				td.end(rollback);
			}
		} catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}

	/**
	 * Sets the property orderManager which points to the
	 * DCS order manager nucleus component.
	 *
	 * @param pOrderManager the OrderManager component. Usually /atg/commerce/order/OrderManager
	 */
	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	/**
	 * Returns the orderManager property which is a component that is
	 * useful for many order management functions.
	 */
	public OrderManager getOrderManager() {
		return mOrderManager;
	}

    //
    // public void setShoppingCart(OrderHolder pShoppingCart) {
    // mShoppingCart = pShoppingCart;
    // }
    //
    // public OrderHolder getShoppingCart() {
    // return mShoppingCart;
    // }

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}


	/**
	 * Gets mExternalProductService.
	 *
	 * @return Value of mExternalProductService.
	 */
	public CPSExternalProductService getExternalProductService() {
		return mExternalProductService;
	}


	/**
	 * Sets new mExternalProductService.
	 *
	 * @param pExternalProductService New value of mExternalProductService.
	 */
	public void setExternalProductService(CPSExternalProductService pExternalProductService) {
		mExternalProductService = pExternalProductService;
	}
	
	public void setProfileTools(ProfileTools pProfileTools) {
		this.mProfileTools = pProfileTools;
	}

	public ProfileTools getProfileTools() {
		return this.mProfileTools;
	}

	public PropertyManager getPropertyManager() {
		return mPropertyManager;
	}

	public void setPropertyManager(PropertyManager pPropertyManager) {
		mPropertyManager = pPropertyManager;
	}

    /**
     * @return the shoppingCartPath
     */
    public String getShoppingCartPath() {
        return shoppingCartPath;
    }

    /**
     * @param shoppingCartPath
     *            the shoppingCartPath to set
     */
    public void setShoppingCartPath(String shoppingCartPath) {
        this.shoppingCartPath = shoppingCartPath;
    }

}
