package com.cps.userprofiling.service;

import atg.nucleus.GenericService;

public class AccountLookupSessionInfo extends GenericService {

	/**
	 * count of error attempts
	 */
	private int accountLookupCount;

	/**
	 * reset errors count
	 */
	public void resetAccountLookupAttemptsCount() {
		setAccountLookupCount(0);
	}

	/**
	 * increments account lookup error count
	 */
	public void incrementAccountLookupCount() {
		accountLookupCount++;
	}

	/**
	 * @return true if there are more than 3 errors attempts
	 */
	public boolean isShowCaptcha() {
		return getAccountLookupCount() > 2;
	}

	public int getAccountLookupCount() {
		return accountLookupCount;
	}

	public void setAccountLookupCount(int accountLookupCount) {
		this.accountLookupCount = accountLookupCount;
	}
}