package com.cps.userprofiling.service;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;

import com.cps.util.CPSConstants;

public class ProfileUpdateService extends GenericService {
	/**
	 * profile tools
	 */
	private ProfileTools profileTools;

	public void updateProfilePersonalInfo (Profile profile, String firstName, String lastName, String title) throws RepositoryException {
		MutableRepository profileRepository = getProfileTools().getProfileRepository();
		//if (!StringUtils.isBlank(firstName)) {
			profile.setPropertyValue(CPSConstants.FIRST_NAME, firstName);
		//}
		//if (!StringUtils.isBlank(lastName)) {
			profile.setPropertyValue(CPSConstants.LAST_NAME, lastName);
		//}
		//if (!StringUtils.isBlank(title)) {
			MutableRepositoryItem homeAddress = (MutableRepositoryItem) profile.getPropertyValue(CPSConstants.HOME_ADDRESS);
			homeAddress.setPropertyValue(CPSConstants.JOB_TITLE, title);
			profileRepository.updateItem(homeAddress);
			profile.setPropertyValue(CPSConstants.HOME_ADDRESS, homeAddress);
		//}
	}
	
	public void updateProfileContactInfo (Profile profile, String email, String mobilePhone, String directPhone, 
				String address1, String address2, String city, String state, String postalCode) throws RepositoryException {
		MutableRepository profileRepository = getProfileTools().getProfileRepository();
		MutableRepositoryItem homeAddress = (MutableRepositoryItem) profile.getPropertyValue(CPSConstants.HOME_ADDRESS);
		if (homeAddress == null){
			// Profile somehow doesn't have a home address
			createHomeAddressForUser(profile);
			homeAddress = (MutableRepositoryItem) profile.getPropertyValue(CPSConstants.HOME_ADDRESS);
		}
		if (!StringUtils.isBlank(email)) {
			profile.setPropertyValue(CPSConstants.EMAIL, email.toLowerCase());
		}
		//if (!StringUtils.isBlank(mobilePhone)) {
			homeAddress.setPropertyValue(CPSConstants.PHONE_NUMBER, mobilePhone);
		//}
		//if (!StringUtils.isBlank(directPhone)) {
			homeAddress.setPropertyValue(CPSConstants.ALTERNATE_PHONE_NUMBER, directPhone);
		//}
		//if (!StringUtils.isBlank(address1)) {
			homeAddress.setPropertyValue(CPSConstants.ADDRESS1, address1);
		//}
		//if (!StringUtils.isBlank(address2)) {
			homeAddress.setPropertyValue(CPSConstants.ADDRESS2, address2);
		//}
		//if (!StringUtils.isBlank(city)) {
			homeAddress.setPropertyValue(CPSConstants.CITY, city);
		//}
		//if (!StringUtils.isBlank(state)) {
			homeAddress.setPropertyValue(CPSConstants.STATE, state);
		//}
		//if (!StringUtils.isBlank(postalCode)) {
			homeAddress.setPropertyValue(CPSConstants.POSTAL_CODE, postalCode);
		//}
		profileRepository.updateItem(homeAddress);
		profile.setPropertyValue(CPSConstants.HOME_ADDRESS, homeAddress);
	}
	
	private void createHomeAddressForUser (MutableRepositoryItem user) {
		try {
			MutableRepositoryItem homeAddress = getProfileTools().getProfileRepository().createItem(CPSConstants.CONTACT_INFO);
			getProfileTools().getProfileRepository().addItem(homeAddress);
			user.setPropertyValue(CPSConstants.HOME_ADDRESS, homeAddress);
			getProfileTools().getProfileRepository().updateItem(user);
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
	}

	public ProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(ProfileTools profileTools) {
		this.profileTools = profileTools;
	}
}