package com.cps.userprofiling.service;

import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

import java.util.*;

/**
 * @author Alex Turchynovich.
 */
public class ProfileRecentlyViewedService extends GenericService {

    private static final String PROPERTY_RECENTLY_VIEWED_PRODUCTS = "recentlyViewedProducts";

    private final List<RepositoryItem> mRecentlyViewed = new ArrayList<>();

    private Profile mProfile;
    private int mProductsLimit;


    /**
     * add product to collection and remove first if limit({@link #mProductsLimit}) is exceeded
     * @param pCollection
     * @param pProduct
     */
    private void addProduct(Collection<RepositoryItem> pCollection, RepositoryItem pProduct) {
        if(!pCollection.contains(pProduct)) {
            if(pCollection.size() >= mProductsLimit){
                pCollection.remove(pCollection.iterator().next());
            }
            pCollection.add(pProduct);
        }
    }

    /**
     * add product to recently viewed of current profile.
     * @param pProduct
     */
    @SuppressWarnings("unchecked")
    public void addRecentlyViewed(RepositoryItem pProduct){
        Profile profile = getProfile();
        if(profile == null || profile.isTransient()){
            addProduct(mRecentlyViewed, pProduct);
        }
        else{
            List<RepositoryItem> list = (List<RepositoryItem>)profile.getPropertyValue(PROPERTY_RECENTLY_VIEWED_PRODUCTS);
            addProduct(list, pProduct);
            profile.setPropertyValue(PROPERTY_RECENTLY_VIEWED_PRODUCTS, list);
        }
    }

    /**
     * put all recently viewed products from transient field to profile
     */
    @SuppressWarnings("unchecked")
    public void mergeTransient(){
        Profile profile = getProfile();
        if(profile != null && !profile.isTransient()){
            synchronized (mRecentlyViewed) {
                List<RepositoryItem> profileRecentlyViewed = (List<RepositoryItem>) profile.getPropertyValue(PROPERTY_RECENTLY_VIEWED_PRODUCTS);
                for (RepositoryItem item : mRecentlyViewed) {
                    addProduct(profileRecentlyViewed, item);
                }
                mRecentlyViewed.clear();
                profile.setPropertyValue(PROPERTY_RECENTLY_VIEWED_PRODUCTS, profileRecentlyViewed);
            }
        }
    }

    /**
     * get all recently viewed products for current profile
     */
    @SuppressWarnings("unchecked")
    public List<RepositoryItem> getRecentlyViewed(){
        Profile profile = getProfile();
        if(profile == null || profile.isTransient()){
            return Collections.unmodifiableList(mRecentlyViewed);
        }
        else{
            return (List<RepositoryItem>)profile.getPropertyValue(PROPERTY_RECENTLY_VIEWED_PRODUCTS);
        }
    }

    public Profile getProfile() {
        return mProfile;
    }

    public void setProfile(Profile pProfile) {
        mProfile = pProfile;
    }

    public int getProductsLimit() {
        return mProductsLimit;
    }

    public void setProductsLimit(int pProductsLimit) {
        mProductsLimit = pProductsLimit;
    }
}
