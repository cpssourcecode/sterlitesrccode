package com.cps.userprofiling.service;

import atg.nucleus.GenericService;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileTools;
import com.cps.email.CommonEmailSender;
import com.cps.util.CPSConstants;
import com.cps.util.security.CriptoUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Dmitry Golubev
 */
public class ForgotPasswordService extends GenericService {
	/**
	 * email sender
	 */
	private CommonEmailSender mEmailSender;
	/**
	 * reset password path
	 */
	private String mResetPasswordPath;
	/**
	 * profile tools
	 */
	private ProfileTools mProfileTools;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * send forgot password email
	 * @param pUserEmail user email
	 * @param pRequest request
	 */
	public void sendForgotPasswordEmail(String pUserEmail, DynamoHttpServletRequest pRequest) {
		MutableRepositoryItem user = (MutableRepositoryItem)getProfileTools().getItemFromEmail(pUserEmail);
		if (user != null){
			user.setPropertyValue(CPSConstants.FORGOT_PSWD, true);

			String url = "";

			String forgotPasswordLink = generateLink(url, user);
			vlogDebug(forgotPasswordLink);

			//String siteId = SiteContextManager.getCurrentSiteId();
			//getEmailSender().sendForgotPasswordEmail(user, forgotPasswordLink, siteId);
			getEmailSender().sendPasswordResetLinkEmail(user, forgotPasswordLink);
		} else {
			vlogError("No user found by email: "+pUserEmail);
		}
	}

	/**
	 * @param url url
	 * @param user user
	 * @return forgot password link
	 */
	public String generateLink(String url, RepositoryItem user) {
		Date currentDate = Calendar.getInstance().getTime();
		return generateLinkForDate(url,user,currentDate);
	}

	public String generateNonExpireLink(String url, RepositoryItem user) {
		Calendar c  = Calendar.getInstance();
		c.add(Calendar.YEAR, 1000);
		return generateLinkForDate(url,user,c.getTime());
	}

	private String generateLinkForDate(String url, RepositoryItem user, Date date) {
		String link = url;
		String profileId = user.getRepositoryId();
		String login = (String) user.getPropertyValue(CPSConstants.LOGIN);
		DateFormat dfr = new SimpleDateFormat(CPSConstants.RECOVER_PSWD_DATE_FORMAT);
		String currentDateStr = dfr.format(date);
		try {
			String sEncryptedLogin = CriptoUtils.aesEncrypt(login);
			String sEncryptedProfileId = CriptoUtils.aesEncrypt(profileId);
			String sEncryptedCurrentDate = CriptoUtils.aesEncrypt(currentDateStr);

			String ln = "?ln=" + sEncryptedLogin;
			String pr = "&pr=" + sEncryptedProfileId;
			String rd = "&rd=" + sEncryptedCurrentDate;
			String eParams = ln + pr + rd;
			link = url + getResetPasswordPath() + eParams;
		} catch (Exception e) {
			vlogDebug("error encrypting", e);
		}
		return link;
	}


	//------------------------------------------------------------------------------------------------------------------

	public String getResetPasswordPath() {
		return mResetPasswordPath;
	}

	public void setResetPasswordPath(String pResetPasswordPath) {
		mResetPasswordPath = pResetPasswordPath;
	}

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	public ProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
}
