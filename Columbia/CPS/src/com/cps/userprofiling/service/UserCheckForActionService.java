package com.cps.userprofiling.service;

import atg.nucleus.GenericService;
import atg.userprofiling.Profile;

/**
 * @author Dmitry Golubev
 **/
public class UserCheckForActionService extends GenericService {
	/**
	 * @param pProfile user profile
	 * @return true if user allowed to add item to material list
	 */
	public boolean checkAddItemToMaterialList(Profile pProfile) {
		if(pProfile == null) {
			return false;
		}
		return !isUserTransient(pProfile);
	}

	/**
	 * @param pProfile profile
	 * @return true if user is transient
	 */
	private boolean isUserTransient(Profile pProfile) {
		return pProfile.isTransient();
	}
}
