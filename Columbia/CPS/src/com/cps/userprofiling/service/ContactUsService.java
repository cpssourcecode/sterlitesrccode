package com.cps.userprofiling.service;

import atg.droplet.DropletException;
import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.ContactUsFormHandler;

/**
 * @author Dmitry Golubev
 */
public class ContactUsService extends GenericService {
	/**
	 * email sender
	 */
	private CommonEmailSender mEmailSender;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * change user password
	 * @param pFormHandler profile form
	 * @param pRequest request
	 * @param pResponse response
	 */
	public void sendContactUsMessage(ContactUsFormHandler pFormHandler, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse){
		try{
			String siteId = SiteContextManager.getCurrentSiteId();
			boolean success = getEmailSender().sendContactUsEmail(pFormHandler.getValue(), siteId);
			if(!success) {
				vlogDebug("'Contact Us' email is not sent");
			}
		}catch(Exception e){
			vlogError("Unable to send message", e);
			pFormHandler.addFormException(
				new DropletException("Unable to send message")
			);
		}
	}

	//------------------------------------------------------------------------------------------------------------------


	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}
}
