package com.cps.userprofiling.service;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.security.PasswordHasher2Adapter;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.ProfileForm;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;
import com.cps.email.CommonEmailSender;
import com.cps.util.CPSConstants;

/**
 * @author Dmitry Golubev
 */
public class ResetPasswordService extends GenericService {

	/**
	 * profile tools
	 */
	private ProfileTools mProfileTools;
	/**
	 * cookie service
	 */
	private CookieService mCookieService;
	/**
	 * email sender
	 */
	private CommonEmailSender mEmailSender;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * change user password
	 * @param pProfileForm profile form
	 * @param pRequest request
	 * @param pResponse response
	 */
	public void changeUserPassword (ProfileForm pProfileForm, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws RepositoryException {
		MutableRepositoryItem currentProfile = (MutableRepositoryItem) getProfileTools().getItemFromEmail(
			(String)pProfileForm.getValue().get(CPSConstants.EMAIL)
		);
		PropertyManager pmgr = getProfileTools().getPropertyManager();
		String newPswd = (String)pProfileForm.getValue().get(CPSConstants.PSWD);
		String pswdValue = "";
		String pswdSalt = (String)pProfileForm.getValue().get(pmgr.getPasswordSaltPropertyName());
		if (StringUtils.isBlank(pswdSalt)){
			pswdSalt = (String)currentProfile.getPropertyValue(pmgr.getPasswordSaltPropertyName());
			if (StringUtils.isBlank(pswdSalt)){
				pswdSalt = ((PasswordHasher2Adapter)pmgr.getPasswordHasher()).generateSalt();
				currentProfile.setPropertyValue(pmgr.getPasswordSaltPropertyName(), pswdSalt);
				if(pswdSalt != null){
					String passwordHasher = pmgr.getPasswordHasher().getPwdHasherComponentPath();
					currentProfile.setPropertyValue(pmgr.getUserPasswordHasherPropertyName(),  pmgr.getCryptoAgilityPasswordHasherCode(passwordHasher));
				}
			}
		}
		if ( (pswdValue = pmgr.generatePassword(pswdSalt, newPswd)) != null) {
			newPswd = pswdValue;
		}
		if (!StringUtils.isBlank(newPswd) && !StringUtils.isBlank(pswdValue)){
			currentProfile.setPropertyValue(CPSConstants.PSWD, newPswd);
			currentProfile.setPropertyValue(CPSConstants.GENERATED_PSWD, false);
			currentProfile.setPropertyValue(CPSConstants.FORGOT_PSWD, false);
			getProfileTools().getProfileRepository().updateItem(currentProfile);

			getEmailSender().sendPasswordResetConfirmationEmail(currentProfile);

			getCookieService().removeUserCookies( pRequest, pResponse );
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	public ProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public CookieService getCookieService() {
		return mCookieService;
	}

	public void setCookieService(CookieService pCookieService) {
		mCookieService = pCookieService;
	}

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}
}
