package com.cps.userprofiling.service;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.CookieManager;
import atg.userprofiling.ProfileTools;
import com.cps.userprofiling.CPSCookieManager;
import com.cps.util.CPSConstants;

import javax.servlet.http.Cookie;

/**
 * @author Dmitry Golubev
 */
public class CookieService extends GenericService {
	/**
	 * profile tools
	 */
	private ProfileTools mProfileTools;
	/**
	 * cookie manager
	 */
	private CookieManager mCookieManager;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * remove user cookies
	 * @param pRequest request
	 * @param pResponse response
	 */
	public void removeUserCookies(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		Cookie[] cookies = pRequest.getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase(CPSConstants.USER_EMAIL_NAME_COOKIE_NAME)) {
				((CPSCookieManager) getProfileTools().getCookieManager()).rememberMeCookie("", "", pRequest, pResponse);
			}
		}

	}

	/**
	 * init user 'remember me' cookie
	 * @param email user email
	 * @param name user name
	 * @param pResponse response
	 */
	public void setUserCookies(String email, String name, DynamoHttpServletResponse pResponse){
		Cookie cookie = null;
		if (email != null){
			cookie = generateUserEmailAndNameCookie(email, name);
			if (cookie != null){
				pResponse.addCookie(cookie);
			}
		}
	}

	/**
	 * @param email email
	 * @param name name
	 * @return generate cookie
	 */
	private Cookie generateUserEmailAndNameCookie(String email, String name){
		String combinedString = "";
		String QUOTE = "\"";
		if (!StringUtils.isBlank(email)) {
			if(StringUtils.isBlank(name)) {
				combinedString = QUOTE +email+ QUOTE;
			} else {
				combinedString = QUOTE +email+","+name+ QUOTE;
			}
		}else
		if (!StringUtils.isBlank(name)) {
			combinedString = QUOTE +","+name+ QUOTE;
		}

		Cookie cookie = new Cookie(CPSConstants.USER_EMAIL_NAME_COOKIE_NAME, combinedString);
		String domain = getCookieManager().getProfileCookieDomain();
		if (domain != null)
			cookie.setDomain(domain);
		String path = getCookieManager().getProfileCookiePath();
		if (path != null)
			cookie.setPath(path);
		cookie.setSecure(false);
		cookie.setMaxAge(getCookieManager().getProfileCookieMaxAge());
		String YES = "Yes";
		cookie.setComment(YES);
		return cookie;
	}

	//------------------------------------------------------------------------------------------------------------------

	public ProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public CookieManager getCookieManager() {
		return mCookieManager;
	}

	public void setCookieManager(CookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}
}
