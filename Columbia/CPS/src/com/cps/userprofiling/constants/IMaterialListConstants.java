package com.cps.userprofiling.constants;

/**
 * @author Dmitry Golubev
 **/
public interface IMaterialListConstants {
	String CONTROL_NAME = "control-ml-name";
	String CONTROL_DESCRIPTION = "control-ml-description";
	String CONTROL_GIFTLIST = "control-ml-giftlist";

}
