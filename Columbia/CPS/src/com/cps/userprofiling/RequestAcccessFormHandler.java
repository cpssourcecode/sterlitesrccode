package com.cps.userprofiling;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;

import com.cps.email.CommonEmailSender;
import com.cps.util.CPSConstants;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;
import vsg.util.ajax.AjaxUtils;

/**
 * 
 * @author Olga Jirkevich
 *
 */
public class RequestAcccessFormHandler extends GenericFormHandler {
    private Profile currentProfile;
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;

	private Map<String, String> role = new HashMap<String, String>();

	private String approverFirstName;  
	private String approverLastName;
	private String approverPhoneNumber;
	private String approverEmail;
	
	private String profileId;

	private String requestAccessSuccessURL;
	private String requestAccessErrorURL;

	private RepeatingRequestMonitor mRepeatingRequestMonitor;
	private CommonEmailSender emailSender;
	
	private ProfileTools profileTools;
	private String learnVideo;

	public boolean handleRequestAccess(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("handleRequestAccess.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "RequestAcccessFormHandler.handleRequestAccess";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				Map<String, String> emailParams = new HashMap<>();
				emailParams.put(CPSConstants.FIRST_NAME, getFirstName());
				emailParams.put(CPSConstants.LAST_NAME, getLastName());
				emailParams.put(CPSConstants.PHONE_NUMBER, getPhoneNumber());
				emailParams.put(CPSConstants.EMAIL, getEmail());
				
				emailParams.put(CPSConstants.APPROVAL_FIRST_NAME, getApproverFirstName());
				emailParams.put(CPSConstants.APPROVAL_LAST_NAME, getApproverLastName());
				emailParams.put(CPSConstants.APPROVAL_PHONE_NUMBER, getApproverPhoneNumber());
				emailParams.put(CPSConstants.APPROVAL_EMAIL, getApproverEmail());
				
				String roles = "";
				for (Map.Entry<String, String> role : getRole().entrySet()) {
					if(role.getValue().equals(CPSConstants.TRUE)){
						roles += role.getKey() + ",";						
					}
				}
				updateUserRequestAccess(roles);
				emailParams.put(CPSConstants.ROLES, roles.toUpperCase());
				if(!getCurrentProfile().isTransient()){
				    String companyName = getCompanyName();
	                emailParams.put(CPSConstants.COMPANY_NAME, companyName);    
				}
								
				getEmailSender().sendRequestAccessEmail(emailParams);
				
				AjaxUtils.addAjaxResponse(this, pResponse, null);
			} catch (RepositoryException e) {
				vlogError("Error", e);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		
		return false;
	}

	private void updateUserRequestAccess(String userRole) {
		try {
			if(!userRole.isEmpty()){
				String[] userRoles = userRole.split(",");
				RepositoryItem profile = getProfileTools().getProfileRepository().getItem(getProfileId(),CPSConstants.USER);
				if(profile!=null){
					MutableRepository repository = getProfileTools().getProfileRepository();
					MutableRepositoryItem userItem = repository.getItemForUpdate(profile.getRepositoryId(), CPSConstants.USER);
					for (String usrRole : userRoles) {
						if(usrRole.equals(CPSConstants.ROLE_FINANCE)) {
							userItem.setPropertyValue("requestFinanceAccess", Boolean.TRUE);
						}
						if(usrRole.equals(CPSConstants.ROLE_ADMIN)) {
							userItem.setPropertyValue("requestAdminAccess", Boolean.TRUE);
						}
						if(usrRole.equals(CPSConstants.ROLE_BUYER)) {
							userItem.setPropertyValue("requestBuyerAccess", Boolean.TRUE);
						}
					}										
					repository.updateItem(userItem);
				}
			}			
		} catch (RepositoryException e) {
			vlogError("Profile Repository Exception : {0}", e.getMessage());
			e.printStackTrace();
		}
	}

	private String getCompanyName() throws RepositoryException {
        RepositoryItem profile = (RepositoryItem) getProfileTools().getProfileRepository().getItem(getProfileId(), CPSConstants.USER);
        if (profile != null) {
			RepositoryItem organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(profile);
			if (organization != null) {
				return (String) organization.getPropertyValue(CPSConstants.NAME);
			}
        }
		
		return "";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Map<String, String> getRole() {
		return role;
	}

	public void setRole(Map<String, String> role) {
		this.role = role;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phone) {
		this.phoneNumber = phone;
	}

	public String getRequestAccessSuccessURL() {
		return requestAccessSuccessURL;
	}

	public void setEditSuccessURL(String requestAccessSuccessURL) {
		this.requestAccessSuccessURL = requestAccessSuccessURL;
	}

	public String getRequestAccessErrorURL() {
		return requestAccessErrorURL;
	}

	public void setRequestAccessErrorURL(String requestAccessErrorURL) {
		this.requestAccessErrorURL = requestAccessErrorURL;
	}

	public String getApproverFirstName() {
		return approverFirstName;
	}

	public void setApproverFirstName(String pApproverFirstName) {
		approverFirstName = pApproverFirstName;
	}

	public String getApproverLastName() {
		return approverLastName;
	}

	public void setApproverLastName(String pApproverLastName) {
		approverLastName = pApproverLastName;
	}

	public String getApproverPhoneNumber() {
		return approverPhoneNumber;
	}

	public void setApproverPhoneNumber(String pApproverPhoneNumber) {
		approverPhoneNumber = pApproverPhoneNumber;
	}

	public String getApproverEmail() {
		return approverEmail;
	}

	public void setApproverEmail(String pApproverEmail) {
		approverEmail = pApproverEmail;
	}

	public CommonEmailSender getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		emailSender = pEmailSender;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String pProfile) {
		profileId = pProfile;
	}

	public ProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(ProfileTools pProfileTools) {
		profileTools = pProfileTools;
	}

	public String getLearnVideo() {
		return learnVideo;
	}

	public void setLearnVideo(String pLearnVideo) {
		learnVideo = pLearnVideo;
	}

    /**
     * @return the currentProfile
     */
    public Profile getCurrentProfile() {
        return currentProfile;
    }

    /**
     * @param currentProfile the currentProfile to set
     */
    public void setCurrentProfile(Profile currentProfile) {
        this.currentProfile = currentProfile;
    }
}