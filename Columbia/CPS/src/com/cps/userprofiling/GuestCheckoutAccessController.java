package com.cps.userprofiling;

import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;

public class GuestCheckoutAccessController extends CPSRoleAccessController {

    private CPSProfileTools mProfileTools;

    @Override
    public boolean allowAccess(Profile pProfile, DynamoHttpServletRequest pRequest) {
        boolean isAllowed = super.allowAccess(pProfile, pRequest);

        CPSProfileTools profileTools = getProfileTools();

        return isAllowed || (pProfile.isTransient() && profileTools.isAllowGuestCheckout());
    }

    public CPSProfileTools getProfileTools() {
        return mProfileTools;
    }

    public void setProfileTools(CPSProfileTools pProfileTools) {
        mProfileTools = pProfileTools;
    }
}
