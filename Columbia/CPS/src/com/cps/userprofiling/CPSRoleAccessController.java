package com.cps.userprofiling;

import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.userdirectory.Role;
import atg.userdirectory.User;
import atg.userdirectory.UserDirectory;
import atg.userprofiling.AccessController;
import atg.userprofiling.Profile;

/**
 * Allow access based on user role
 */
public class CPSRoleAccessController extends GenericService implements AccessController {
	
	/** The Denied access url. */
	private String mDeniedAccessURL;
	
	/** The Enabled flag. */
	private boolean mEnabled = true;
	
	/** permitted roles */
	private String[] mRolesPermitted;
	
	/** user directory */
	private UserDirectory mUserDirectory;
	
	@Override
	public boolean allowAccess(Profile pProfile, DynamoHttpServletRequest pRequest) {
		if (isEnabled()) {
			User user = getUserDirectory().findUserByPrimaryKey(pProfile.getRepositoryId());
			return userHasPermission(user);
		}
		return true;
	}
	
	/**
	 * check if user has permission
	 * @param pUser - user
	 * @return - true if user has permission
	 */
	private boolean userHasPermission (User pUser){
		if(getRolesPermitted() != null && getRolesPermitted().length > 0){
			for(String role : getRolesPermitted()){
				Role currentRole = getUserDirectory().findRoleByPrimaryKey(role);
				if(currentRole != null && pUser.hasAssignedRole(currentRole)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Gets the denied access url.
	 *
	 * @param pProfile the profile
	 * @return the denied access url
	 * @see atg.userprofiling.AccessController#getDeniedAccessURL(atg.userprofiling.Profile)
	 */
	@Override
	public String getDeniedAccessURL(Profile pProfile) {
		return getDeniedAccessURL();
	}
	
	public String getDeniedAccessURL() {
		return mDeniedAccessURL;
	}

	public void setDeniedAccessURL(String pDeniedAccessURL) {
		mDeniedAccessURL = pDeniedAccessURL;
	}
	
	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	public String[] getRolesPermitted() {
		return mRolesPermitted;
	}

	public void setRolesPermitted(String[] pRolesPermitted) {
		this.mRolesPermitted = pRolesPermitted;
	}

	public UserDirectory getUserDirectory() {
		return mUserDirectory;
	}

	public void setUserDirectory(UserDirectory pUserDirectory) {
		this.mUserDirectory = pUserDirectory;
	}
	
}
