package com.cps.userprofiling;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.AccessController;
import atg.userprofiling.Profile;

import com.cps.userprofiling.util.CPSUserAgentCacheParser;
import com.cps.util.CPSConstants;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.UserAgentType;

import java.util.Properties;

public class BrowserAccessController extends GenericService implements AccessController {
	private static UserAgentStringParser parser = new CPSUserAgentCacheParser();

    /** The Denied access url. */
    private String mDeniedAccessURL;

    public String getDeniedAccessURL() {
        return mDeniedAccessURL;
    }

    public void setDeniedAccessURL(String pDeniedAccessURL) {
        mDeniedAccessURL = pDeniedAccessURL;
    }

    /** The Enabled flag. */
    private boolean mEnabled = true;

    public boolean isEnabled() {
        return mEnabled;
    }

    public void setEnabled(boolean pEnabled) {
        mEnabled = pEnabled;
    }

    /** The strict check flag. */
    private boolean mStrictCheck = false;

    public boolean isStrictCheck() {
        return mStrictCheck;
    }

    public void setStrictCheck(boolean pStrictCheck) {
        mStrictCheck = pStrictCheck;
    }

    public String getDeniedAccessURL(Profile pProfile) {
        return getDeniedAccessURL();
    }

    private Properties mAllowedBrowsers;

    public Properties getAllowedBrowsers() {
        return mAllowedBrowsers;
    }

    public void setAllowedBrowsers(Properties pAllowedBrowsers) {
        mAllowedBrowsers = pAllowedBrowsers;
    }
    
    private String mIEengines;
    
    public String getIEengines() {
        return mIEengines;
    }

    public void setIEengines(String pIEengines) {
    	mIEengines = pIEengines;
    }

    @Override
    public boolean allowAccess(Profile pProfile, DynamoHttpServletRequest pRequest) {

        boolean allowAccess = true;
        String uri = pRequest.getRequestURI();
        if (isEnabled() && pProfile != null && !StringUtils.isBlank(uri) && !uri.contains(getDeniedAccessURL())) {
            String userAgentHeader = pRequest.getHeader(CPSConstants.USER_AGENT);
            if (StringUtils.isNotBlank(userAgentHeader)) {
	            ReadableUserAgent agent = parser.parse(userAgentHeader);
	            // perform check
	            vlogDebug(new StringBuilder().append("AccessControl: Checking User-Agent: ").append(agent).toString());
	            if (!UserAgentType.ROBOT.equals(agent.getType())) {
	            	String browser = agent.getName();
	                String allowedVersion = getAllowedBrowsers().getProperty(browser);
	                if (!StringUtils.isBlank(allowedVersion)) {
	                	try {
		                	if (!browser.equalsIgnoreCase(CPSConstants.BROWSER_IE)) {
		                		allowAccess = checkVersion(agent, allowedVersion);
		                	} else {
		                		allowAccess = checkIE(userAgentHeader, agent, allowedVersion, pProfile);
		                	}
	                	} catch (Exception e) {
	                        vlogError(e, "Error");
	                    }
	                } else if (isStrictCheck()) {
	                    allowAccess = false;
	                }
	            }
            }
        }
        return allowAccess;
    }
    
    private boolean checkIE(String userAgentHeader, ReadableUserAgent agent, String allowedVersion, Profile pProfile) {
    	String ieEngine = getIEengines();
    	if (!checkVersion(agent, allowedVersion) && userAgentHeader.matches(ieEngine)) {
    		pProfile.setPropertyValue(CPSConstants.COMPATIBILITY_MODE_ON, true);
    		return false;
    	}
    	
    	return true;
    }
    
    private boolean checkVersion(ReadableUserAgent agent, String allowedVersion) {
        String browserVersion = agent.getVersionNumber().getMajor();
        if(browserVersion == null || browserVersion.isEmpty()){
        	return false;
        }else{
        	int version = Integer.valueOf(browserVersion).intValue();
        	return !(Integer.valueOf(allowedVersion).intValue() > version);
        }
    }
}
