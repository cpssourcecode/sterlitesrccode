package com.cps.userprofiling;

import static com.cps.util.CPSConstants.*;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.email.CPSEmailConstants;
import com.cps.integrations.pricing.CPSPricingConstants;
import com.cps.userprofiling.util.UserAssociatedCB;
import com.cps.userprofiling.util.UserAssociatedCS;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSFormUtils;

import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.profile.CommercePropertyManager;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.json.JSONException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import vsg.userprofiling.VSGProfileTools;

public class CPSProfileTools extends VSGProfileTools {

	private boolean mAllowGuestCheckout;
	private boolean mAllowCreditCardOnApproval;
	private boolean mCreditCardEnabled;
	private boolean mAllowNewUserSelfRegistration;
	private boolean mShowPaymentMethod;
    private String defaultCustomerId;
    private String defaultBranch;
    private Repository locationRepository;
    private boolean makeJDECallsAlwaysForEMPMatrix = true;
    private boolean disablePriceMatrixPricingForLoggedInUsers; // setting this to true will disable the priceMatrix solution for loggedIn users
    private static final String YES_VALUE = "Y";

	public String getCustomerId(RepositoryItem pProfile) {
		String customerId = null;
		RepositoryItem org = getParentOrganization(pProfile);
		if (org != null) {
			RepositoryItem billingAddress = (RepositoryItem) org.getPropertyValue(BILLING_ADDRESS);
			if (billingAddress != null) {
				customerId = (String) billingAddress.getPropertyValue(CPSConstants.JDE_ADDRESS_NUM);
			}
			if (StringUtils.isBlank(customerId)) {
				customerId = org.getRepositoryId();
			}
		}
		return customerId;
	}

	public RepositoryItem getParentOrganization(RepositoryItem pProfile) {
		RepositoryItem organization = null;
		if (pProfile != null) {
			if ((Integer) pProfile.getPropertyValue(USER_TYPE) != 3) {
				organization = (RepositoryItem) pProfile.getPropertyValue(PARENT_ORG);
			} else if (pProfile.getPropertyValue(SELECTED_ORG) != null) {
				organization = (RepositoryItem) pProfile.getPropertyValue(SELECTED_ORG);
			} else {
				organization = (RepositoryItem) pProfile.getPropertyValue(PARENT_ORG);
			}
		}

		return organization;
	}

	public List<String> getProfileAddresses(Profile pProfile) {
		List<String> shipTo = new ArrayList<String>();
		List<RepositoryItem> csList = new ArrayList<RepositoryItem>();
		if (!initAddressesByProfile(pProfile, csList)) {
			initAddressesByOrganization(pProfile, csList);
		}
		for (RepositoryItem cs : csList) {
			shipTo.add(cs.getRepositoryId());
		}
		return shipTo;
	}


	public Map<String, Object> validateCreditApp(Dictionary values) {
		vlogDebug("Validate Credit App Start");
		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		if (values != null) {
			errors.putAll(validateBusinessDetails(values));
			errors.putAll(validateBusinessBillingAddress(values));
			errors.putAll(validatePrincipals(values));
			errors.putAll(validateReferences(values));
			errors.putAll(validateCustomerProfile(values));
		}

		vlogDebug("Validate Credit App End, returning errors: " + errors.toString());
		return errors;
	}

	private Map<String, Object> validateBusinessDetails(Dictionary values) {
		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		// Validate Your Name
		if (StringUtils.isBlank((String) values.get(YOUR_NAME))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_YOUR_NAME, YOUR_NAME);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(YOUR_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_YOUR_NAME, YOUR_NAME);
		}
		// Validate Legal Business Name
		if (StringUtils.isBlank((String) values.get(BUSINESS_NAME))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BUS_NAME, BUSINESS_NAME);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_NAME, BUSINESS_NAME);
		}
		// Validate Trade Name
		if (StringUtils.isBlank((String) values.get(TRADE_NAME))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_TRADE_NAME, TRADE_NAME);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(TRADE_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_TRADE_NAME, TRADE_NAME);
		}
		// Validate Type of Organization
		if (StringUtils.isBlank((String) values.get(ORGANIZATION_TYPE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_ORG_TYPE, ORGANIZATION_TYPE);
		}
		// Validate Phone
		if (StringUtils.isBlank((String) values.get(BUSINESS_PHONE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BUS_PHONE, BUSINESS_PHONE);
		} else if (!checkValid((String) values.get(BUSINESS_PHONE), REG_EXP_PHONE)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_PHONE, BUSINESS_PHONE);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_PHONE, BUSINESS_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(BUSINESS_PHONE_EXT)) && !((String) values.get(BUSINESS_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(BUSINESS_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_PHONE_EXT, BUSINESS_PHONE_EXT);
		}
		// Validate Fax
		if ((!StringUtils.isBlank((String) values.get(BUSINESS_FAX)) && !checkValid((String) values.get(BUSINESS_FAX), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_FAX), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_FAX, BUSINESS_FAX);
		}
		// Validate Type of Business
		if (StringUtils.isBlank((String) values.get(BUSINESS_TYPE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BUS_TYPE, BUSINESS_TYPE);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_TYPE), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_TYPE, BUSINESS_TYPE);
		}
		// Validate Contractors License #
		// Validate Year Established
		try {
			Calendar c = Calendar.getInstance();
			Integer currentYear = c.get(Calendar.YEAR);
			if (StringUtils.isBlank((String) values.get(YEAR_ESTABLISHED))) {
				errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_YEAR_EST, YEAR_ESTABLISHED);
			} else if (Integer.parseInt((String) values.get(YEAR_ESTABLISHED)) > currentYear) {
				errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_YEAR_EST, YEAR_ESTABLISHED);
			} else if (!CPSFormUtils.checkMaxLength((String) values.get(YEAR_ESTABLISHED), 4)) {
				errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_YEAR_EST, YEAR_ESTABLISHED);
			}
		} catch (Exception e) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_YEAR_EST, YEAR_ESTABLISHED);
		}
		// Type of Customer
		if (StringUtils.isBlank((String) values.get(CUSTOMER_TYPE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_CUSTOMER_TYPE, CUSTOMER_TYPE);
		}
		// PO Required / Tax Exempt
		if (StringUtils.isBlank((String) values.get(PO))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PO_REQUIRED, PO);
		}
		if (StringUtils.isBlank((String) values.get(TAX))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_TAX_EXEMPT, TAX);
		}

		return errors;
	}

	private Map<String, Object> validateBusinessBillingAddress(Dictionary values) {
		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		// Validate Business Address 1
		if (StringUtils.isBlank((String) values.get(BUSINESS_ADDRESS1))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BUS_ADDR_1, BUSINESS_ADDRESS1);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_ADDRESS1), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_ADDR_1, BUSINESS_ADDRESS1);
		}
		if (!StringUtils.isBlank((String) values.get(BUSINESS_ADDRESS2)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_ADDRESS2), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_ADDR_2, BUSINESS_ADDRESS2);
		}
		// Validate Business City
		if (StringUtils.isBlank((String) values.get(BUSINESS_CITY))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BUS_CITY, BUSINESS_CITY);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_CITY), 30)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_CITY, BUSINESS_CITY);
		}
		// Validate Business State
		if (StringUtils.isBlank((String) values.get(BUSINESS_STATE)) ||
				(!StringUtils.isBlank((String) values.get(BUSINESS_STATE)) &&
						((String) values.get(BUSINESS_STATE)).equalsIgnoreCase(CITY_SELECT))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BUS_STATE, BUSINESS_STATE);
		}
		// Validate Business Zip
		if (StringUtils.isBlank((String) values.get(BUSINESS_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BUS_ZIP, BUSINESS_ZIP);
		} else if (!validZip((String) values.get(BUSINESS_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_ZIP, BUSINESS_ZIP);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BUSINESS_ZIP), 10)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BUS_ZIP, BUSINESS_ZIP);
		}
		// Validate Billing Address 1
		if (StringUtils.isBlank((String) values.get(BILLING_ADDRESS1))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BIL_ADDR_1, BILLING_ADDRESS1);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BILLING_ADDRESS1), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BIL_ADDR_1, BILLING_ADDRESS1);
		}
		// Validate Billing City
		if (StringUtils.isBlank((String) values.get(BILLING_CITY))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BIL_CITY, BILLING_CITY);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BILLING_CITY), 30)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BIL_CITY, BILLING_CITY);
		}
		// Validate Billing State
		if (StringUtils.isBlank((String) values.get(BILLING_STATE)) || (!StringUtils.isBlank((String) values.get(BILLING_STATE)) &&
				((String) values.get(BILLING_STATE)).equalsIgnoreCase(CITY_SELECT))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BIL_STATE, BILLING_STATE);
		}
		// Validate Billing Zip
		if (StringUtils.isBlank((String) values.get(BILLING_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BIL_ZIP, BILLING_ZIP);
		} else if (!validZip((String) values.get(BILLING_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BIL_ZIP, BILLING_ZIP);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BILLING_ZIP), 10)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BIL_ZIP, BILLING_ZIP);
		}

		return errors;
	}

	private Map<String, Object> validatePrincipals(Dictionary values) {
		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		// Validate Name
		if (StringUtils.isBlank((String) values.get(PRINCIPAL1_NAME))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PRIN1_NAME, PRINCIPAL1_NAME);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL1_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_NAME, PRINCIPAL1_NAME);
		}
		// Validate Title
		if (StringUtils.isBlank((String) values.get(PRINCIPAL1_TITLE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PRIN1_TITLE, PRINCIPAL1_TITLE);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL1_TITLE), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_TITLE, PRINCIPAL1_TITLE);
		}
		// Validate Address
		if (StringUtils.isBlank((String) values.get(PRINCIPAL1_ADDRESS))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PRIN1_ADDR, PRINCIPAL1_ADDRESS);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL1_ADDRESS), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_ADDR, PRINCIPAL1_ADDRESS);
		}
		// Validate City, State, Zip
		if (StringUtils.isBlank((String) values.get(PRINCIPAL1_CITY))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PRIN1_CITY, PRINCIPAL1_CITY);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL1_CITY), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_CITY, PRINCIPAL1_CITY);
		}
		if (StringUtils.isBlank((String) values.get(PRINCIPAL1_STATE)) ||
				(!StringUtils.isBlank((String) values.get(PRINCIPAL1_STATE)) &&
						((String) values.get(PRINCIPAL1_STATE)).equalsIgnoreCase(CITY_SELECT))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PRIN1_STATE, PRINCIPAL1_STATE);
		}
		if (StringUtils.isBlank((String) values.get(PRINCIPAL1_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PRIN1_ZIP, PRINCIPAL1_ZIP);
		} else if (!validZip((String) values.get(PRINCIPAL1_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_ZIP, PRINCIPAL1_ZIP);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL1_ZIP), 10)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_ZIP, PRINCIPAL1_ZIP);
		}
		// Validate Phone, Ext
		if (StringUtils.isBlank((String) values.get(PRINCIPAL1_PHONE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_PRIN1_PHONE, PRINCIPAL1_PHONE);
		} else if (!checkValid((String) values.get(PRINCIPAL1_PHONE), REG_EXP_PHONE)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_PHONE, PRINCIPAL1_PHONE);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL1_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_PHONE, PRINCIPAL1_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(PRINCIPAL1_PHONE_EXT)) &&
				!((String) values.get(PRINCIPAL1_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(PRINCIPAL1_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL1_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN1_PHONE_EXT, PRINCIPAL1_PHONE_EXT);
		}
		// PRIN 2
		if (!StringUtils.isBlank((String) values.get(PRINCIPAL2_NAME)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL2_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN2_NAME, PRINCIPAL2_NAME);
		}
		if (!StringUtils.isBlank((String) values.get(PRINCIPAL2_TITLE)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL2_TITLE), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN2_TITLE, PRINCIPAL2_TITLE);
		}
		if (!StringUtils.isBlank((String) values.get(PRINCIPAL2_ADDRESS)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL2_ADDRESS), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN2_ADDR, PRINCIPAL2_ADDRESS);
		}
		// Validate City, State, Zip
		if (!StringUtils.isBlank((String) values.get(PRINCIPAL2_CITY)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL2_CITY), 30)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN2_CITY, PRINCIPAL2_CITY);
		}
		if ((!StringUtils.isBlank((String) values.get(PRINCIPAL2_ZIP)) &&
				!validZip((String) values.get(PRINCIPAL2_ZIP))) ||
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL2_ZIP), 10)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN2_ZIP, PRINCIPAL2_ZIP);
		}
		// Validate Phone, Ext
		if ((!StringUtils.isBlank((String) values.get(PRINCIPAL2_PHONE)) &&
				!checkValid((String) values.get(PRINCIPAL2_PHONE), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL2_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN2_PHONE, PRINCIPAL2_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(PRINCIPAL2_PHONE_EXT)) &&
				!((String) values.get(PRINCIPAL2_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(PRINCIPAL2_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(PRINCIPAL2_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_PRIN2_PHONE_EXT, PRINCIPAL2_PHONE_EXT);
		}

		return errors;
	}

	private Map<String, Object> validateReferences(Dictionary values) {
		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		// Validate Name
		if (StringUtils.isBlank((String) values.get(REFERENCE1_NAME))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF1_NAME, REFERENCE1_NAME);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE1_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_NAME, REFERENCE1_NAME);
		}
		// Validate Address
		if (StringUtils.isBlank((String) values.get(REFERENCE1_ADDRESS))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF1_ADDR, REFERENCE1_ADDRESS);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE1_ADDRESS), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_ADDR, REFERENCE1_ADDRESS);
		}
		// Validate City, State, Zip
		if (StringUtils.isBlank((String) values.get(REFERENCE1_CITY))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF1_CITY, REFERENCE1_CITY);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE1_CITY), 30)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_CITY, REFERENCE1_CITY);
		}
		if (StringUtils.isBlank((String) values.get(REFERENCE1_STATE)) ||
				(!StringUtils.isBlank((String) values.get(REFERENCE1_STATE)) &&
						((String) values.get(REFERENCE1_STATE)).equalsIgnoreCase(CITY_SELECT))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF1_STATE, REFERENCE1_STATE);
		}
		if (StringUtils.isBlank((String) values.get(REFERENCE1_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF1_ZIP, REFERENCE1_ZIP);
		} else if (!validZip((String) values.get(REFERENCE1_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_ZIP, REFERENCE1_ZIP);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE1_ZIP), 10)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_ZIP, REFERENCE1_ZIP);
		}
		// Validate Phone, Ext
		if (StringUtils.isBlank((String) values.get(REFERENCE1_PHONE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF1_PHONE, REFERENCE1_PHONE);
		} else if (!checkValid((String) values.get(REFERENCE1_PHONE), REG_EXP_PHONE)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_PHONE, REFERENCE1_PHONE);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE1_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_PHONE, REFERENCE1_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(REFERENCE1_PHONE_EXT)) &&
				!((String) values.get(REFERENCE1_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(REFERENCE1_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE1_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_PHONE_EXT, REFERENCE1_PHONE_EXT);
		}
		// Validate Fax
		if ((!StringUtils.isBlank((String) values.get(REFERENCE1_FAX)) &&
				!checkValid((String) values.get(REFERENCE1_FAX), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE1_FAX), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF1_FAX, REFERENCE1_FAX);
		}

		// Validate Name
		if (StringUtils.isBlank((String) values.get(REFERENCE2_NAME))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF2_NAME, REFERENCE2_NAME);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE2_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_NAME, REFERENCE2_NAME);
		}
		// Validate Address
		if (StringUtils.isBlank((String) values.get(REFERENCE2_ADDRESS))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF2_ADDR, REFERENCE2_ADDRESS);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE2_ADDRESS), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_ADDR, REFERENCE2_ADDRESS);
        }
		// Validate City, State, Zip
		if (StringUtils.isBlank((String) values.get(REFERENCE2_CITY))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF2_CITY, REFERENCE2_CITY);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE2_CITY), 30)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_CITY, REFERENCE2_CITY);
		}
		if (StringUtils.isBlank((String) values.get(REFERENCE2_STATE)) ||
				(!StringUtils.isBlank((String) values.get(REFERENCE2_STATE)) &&
						((String) values.get(REFERENCE2_STATE)).equalsIgnoreCase(CITY_SELECT))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF2_STATE, REFERENCE2_STATE);
		}
		if (StringUtils.isBlank((String) values.get(REFERENCE2_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF2_ZIP, REFERENCE2_ZIP);
		} else if (!validZip((String) values.get(REFERENCE2_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_ZIP, REFERENCE2_ZIP);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE2_ZIP), 10)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_ZIP, REFERENCE2_ZIP);
		}
		// Validate Phone, Ext
		if (StringUtils.isBlank((String) values.get(REFERENCE2_PHONE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_REF2_PHONE, REFERENCE2_PHONE);
		} else if (!checkValid((String) values.get(REFERENCE2_PHONE), REG_EXP_PHONE)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_PHONE, REFERENCE2_PHONE);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE2_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_PHONE, REFERENCE2_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(REFERENCE2_PHONE_EXT)) &&
				!((String) values.get(REFERENCE2_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(REFERENCE2_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE2_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_PHONE_EXT, REFERENCE2_PHONE_EXT);
		}
		// Validate Fax
		if ((!StringUtils.isBlank((String) values.get(REFERENCE2_FAX)) &&
				!checkValid((String) values.get(REFERENCE2_FAX), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(REFERENCE2_FAX), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_REF2_FAX, REFERENCE2_FAX);
		}

		// Validate Bank Name
		if (StringUtils.isBlank((String) values.get(BANK_NAME))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_NAME, BANK_NAME);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BANK_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_NAME, BANK_NAME);
		}
		// Validate Address
		if (StringUtils.isBlank((String) values.get(BANK_ADDRESS))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_ADDR, BANK_ADDRESS);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BANK_ADDRESS), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_ADDR, BANK_ADDRESS);
		}
		// Validate City, State, Zip
		if (StringUtils.isBlank((String) values.get(BANK_CITY))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_CITY, BANK_CITY);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BANK_CITY), 30)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_CITY, BANK_CITY);
		}
		if (StringUtils.isBlank((String) values.get(BANK_STATE)) ||
				(!StringUtils.isBlank((String) values.get(BANK_STATE)) &&
						((String) values.get(BANK_STATE)).equalsIgnoreCase(CITY_SELECT))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_STATE, BANK_STATE);
		}
		if (StringUtils.isBlank((String) values.get(BANK_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_ZIP, BANK_ZIP);
		} else if (!validZip((String) values.get(BANK_ZIP))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_ZIP, BANK_ZIP);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BANK_ZIP), 10)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_ZIP, BANK_ZIP);
		}
		// Validate Phone, Ext
		if (StringUtils.isBlank((String) values.get(BANK_PHONE))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_PHONE, BANK_PHONE);
		} else if (!checkValid((String) values.get(BANK_PHONE), REG_EXP_PHONE)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_PHONE, BANK_PHONE);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BANK_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_PHONE, BANK_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(BANK_PHONE_EXT)) &&
				!((String) values.get(BANK_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(BANK_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(BANK_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_PHONE_EXT, BANK_PHONE_EXT);
		}
		if (StringUtils.isBlank((String) values.get(BANK_ACCOUNT_NUMBER))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_ACCOUNT_NUM, BANK_ACCOUNT_NUMBER);
		} else if (!checkValid((String) values.get(BANK_ACCOUNT_NUMBER), REG_EXP_NUMERIC)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_ACCOUNT_NUM, BANK_ACCOUNT_NUMBER);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BANK_ACCOUNT_NUMBER), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_ACCOUNT_NUM, BANK_ACCOUNT_NUMBER);
		}
		if (StringUtils.isBlank((String) values.get(BANK_ACCOUNT_OFFICER))) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_MISSING_BANK_ACCOUNT_OFF, BANK_ACCOUNT_OFFICER);
		} else if (!CPSFormUtils.checkMaxLength((String) values.get(BANK_ACCOUNT_OFFICER), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_BANK_ACCOUNT_OFF, BANK_ACCOUNT_OFFICER);
		}

		return errors;
	}

	private Map<String, Object> validateCustomerProfile(Dictionary values) {
		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		if ((!StringUtils.isBlank((String) values.get(EXPECTED_ANNUAL_PURCHASE_AMOUNT)) &&
				!checkValid((String) values.get(EXPECTED_ANNUAL_PURCHASE_AMOUNT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(EXPECTED_ANNUAL_PURCHASE_AMOUNT), 13)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_EXP_PURCH_AMT, EXPECTED_ANNUAL_PURCHASE_AMOUNT);
		}
		// ORDERER 1
		if (!StringUtils.isBlank((String) values.get(ORDER1_NAME)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER1_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER1_NAME, ORDER1_NAME);
		}
		if (!StringUtils.isBlank((String) values.get(ORDER1_TITLE)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER1_TITLE), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER1_TITLE, ORDER1_TITLE);
		}
		if ((!StringUtils.isBlank((String) values.get(ORDER1_PHONE)) &&
				!checkValid((String) values.get(ORDER1_PHONE), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER1_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER1_PHONE, ORDER1_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(ORDER1_PHONE_EXT)) &&
				!((String) values.get(ORDER1_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(ORDER1_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER1_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER1_PHONE_EXT, ORDER1_PHONE_EXT);
		}

		// ORDERER 2
		if (!StringUtils.isBlank((String) values.get(ORDER2_NAME)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER2_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER2_NAME, ORDER2_NAME);
		}
		if (!StringUtils.isBlank((String) values.get(ORDER2_TITLE)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER2_TITLE), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER2_TITLE, ORDER2_TITLE);
		}
		if ((!StringUtils.isBlank((String) values.get(ORDER2_PHONE)) &&
				!checkValid((String) values.get(ORDER2_PHONE), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER2_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER2_PHONE, ORDER2_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(ORDER2_PHONE_EXT)) &&
				!((String) values.get(ORDER2_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(ORDER2_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER2_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER2_PHONE_EXT, ORDER2_PHONE_EXT);
		}
		// ORDERER 3
		if (!StringUtils.isBlank((String) values.get(ORDER3_NAME)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER3_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER3_NAME, ORDER3_NAME);
		}
		if (!StringUtils.isBlank((String) values.get(ORDER3_TITLE)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER3_TITLE), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER3_TITLE, ORDER3_TITLE);
		}
		if ((!StringUtils.isBlank((String) values.get(ORDER3_PHONE)) &&
				!checkValid((String) values.get(ORDER3_PHONE), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER3_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER3_PHONE, ORDER3_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(ORDER3_PHONE_EXT)) &&
				!((String) values.get(ORDER3_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(ORDER3_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER3_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER3_PHONE_EXT, ORDER3_PHONE_EXT);
		}
		// ORDERER 4
		if (!StringUtils.isBlank((String) values.get(ORDER4_NAME)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER4_NAME), 80)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER4_NAME, ORDER4_NAME);
		}
		if (!StringUtils.isBlank((String) values.get(ORDER4_TITLE)) &&
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER4_TITLE), 50)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER4_TITLE, ORDER4_TITLE);
		}
		if ((!StringUtils.isBlank((String) values.get(ORDER4_PHONE)) &&
				!checkValid((String) values.get(ORDER4_PHONE), REG_EXP_PHONE)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER4_PHONE), 14)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER4_PHONE, ORDER4_PHONE);
		}
		if (((!StringUtils.isBlank((String) values.get(ORDER4_PHONE_EXT)) &&
				!((String) values.get(ORDER4_PHONE_EXT)).equalsIgnoreCase(EXT)) &&
				!checkValid((String) values.get(ORDER4_PHONE_EXT), REG_EXP_NUMERIC)) ||
				!CPSFormUtils.checkMaxLength((String) values.get(ORDER4_PHONE_EXT), 8)) {
			errors.put(CPSErrorCodes.ERR_CREDIT_APP_INVALID_ORDERER4_PHONE_EXT, ORDER4_PHONE_EXT);
		}

		return errors;
	}


	/**
	 * Take in the passed values and validate them, return any errors
	 *
	 * @param values
	 * @return
	 */
	public Map<String, Object> validateContactUs(Dictionary values) {
		vlogDebug("Validate validateContactUs Start");
		Map<String, Object> errors = new LinkedHashMap<String, Object>();
		if (values != null) {
			if (StringUtils.isBlank((String) values.get(FIRST_NAME)) || (values.get(FIRST_NAME) != null && ((String) values.get(FIRST_NAME)).equalsIgnoreCase(FIRST)))
				errors.put(CPSErrorCodes.ERR_REG_MISSING_FIRST_NAME, FIRST_NAME);
			if (StringUtils.isBlank((String) values.get(LAST_NAME)) || (values.get(LAST_NAME) != null && ((String) values.get(LAST_NAME)).equalsIgnoreCase(LAST)))
				errors.put(CPSErrorCodes.ERR_REG_MISSING_LAST_NAME, LAST_NAME);
			if (StringUtils.isBlank((String) values.get(EMAIL)))
				errors.put(CPSErrorCodes.ERR_REG_MISSING_EMAIL, EMAIL);
			else if (!checkValid((String) values.get(EMAIL), REG_EXP_EMAIL))
				errors.put(CPSErrorCodes.ERR_REG_INVALID_EMAIL, EMAIL);
			if (StringUtils.isBlank((String) values.get(SUBJECT)))
				errors.put(CPSErrorCodes.ERR_CONTACT_INVALID_SUBJECT, SUBJECT);
			if (StringUtils.isBlank((String) values.get(COMMENTS)))
				errors.put(CPSErrorCodes.ERR_CONTACT_INVALID_COMMENTS, COMMENTS);


//			if (StringUtils.isBlank((String)values.get(COMPANY)))
//				errors.put(CPSErrorCodes.ERR_REG_MISSING_COMPANY, COMPANY);
//			if (StringUtils.isBlank((String)values.get(PRIMARY_PHONE)))
//				errors.put(CPSErrorCodes.ERR_REG_MISSING_PRIMARY_PHONE, PRIMARY_PHONE);
//			else if (!checkValid((String)values.get(PRIMARY_PHONE), REG_EXP_PHONE))
//				errors.put(CPSErrorCodes.ERR_REG_INVALID_PRIMARY_PHONE, PRIMARY_PHONE);
//			if ((!StringUtils.isBlank((String)values.get(PRIMARY_PHONE_EXT)) && !((String)values.get(PRIMARY_PHONE_EXT)).equalsIgnoreCase(EXT)) && 
//					!checkValid((String)values.get(PRIMARY_PHONE_EXT), REG_EXP_NUMERIC))
//				errors.put(CPSErrorCodes.ERR_REG_INVALID_PRIMARY_PHONE_EXT, PRIMARY_PHONE_EXT);

		}

		vlogDebug("Validate contact us End, returning errors: " + errors.toString());
		return errors;
	}

	public void repriceShoppingCarts(RepositoryItem pProfile, OrderHolder pShoppingCart, PricingModelHolder pUserPricingModels, Locale pLocale, String pPricingOperation)
			throws CommerceException {
		vlogDebug("Old Date loaded");
		((CPSOrderImpl) pShoppingCart.getCurrent()).setPrevLastModifiedDate(pShoppingCart.getCurrent().getLastModifiedDate());
		super.repriceShoppingCarts(pProfile, pShoppingCart, pUserPricingModels, pLocale, pPricingOperation);

	}


	public boolean checkValid(String value, String regExp) {
		if (value != null)
			return value.matches(regExp);
		return false;
	}

	public boolean validZip(String zip) {
		if (zip != null) {
			zip = zip.replaceAll("[-\\(\\) ]", "");
			if (checkValid(zip, REG_EXP_NUMERIC)) {
				if (zip.length() == 5 || zip.length() == 9) {
					return true;
				}
			}
		}
		return false;
	}

	public RepositoryItem[] queryRepository(String repository, Map<String, String> params, String condition) {
		String rqlQuery = "";
		final Object[] p = new Object[params.size()];

		// generate the query statement
		int count = 0;
		for (Entry<String, String> param : params.entrySet()) {
			if (!StringUtils.isBlank(condition) && condition.equalsIgnoreCase("STARTS WITH")) {
				rqlQuery = param.getKey() + " " + condition + " ?" + count;
			} else {
				if (rqlQuery == "") {
					rqlQuery = param.getKey() + " =?" + count;
				} else {
					rqlQuery += " " + condition + " " + param.getKey() + " =?" + count;
				}
			}
			p[count] = param.getValue();
			count++;
		}

		vlogDebug("Generated Query: " + rqlQuery);
		for (int i = 0; i < p.length; i++) {
			vlogDebug("Params: " + p[i]);
		}

		RepositoryItem[] item = null;
		if (!StringUtils.isBlank(rqlQuery) && !StringUtils.isBlank(repository)) {
			try {
				final RepositoryView view = getProfileRepository().getView(repository);
				final RqlStatement rqlStatement = RqlStatement.parseRqlStatement(rqlQuery);
				item = rqlStatement.executeQuery(view, p);
			} catch (final RepositoryException e) {
				logError("queryRepository(): " + e);
			}
		}

		return item;
	}

	public RepositoryItem getRoleItem(final String accountType) {
		RepositoryItem[] roleItem = null;
		try {
			final RepositoryItemDescriptor roleDesc = getProfileRepository().getItemDescriptor(ROLE_PRPTY);
			final RepositoryView roleView = roleDesc.getRepositoryView();
			final QueryBuilder roleBuilder = roleView.getQueryBuilder();
			final QueryExpression name = roleBuilder.createPropertyQueryExpression(NAME);
			final QueryExpression acctType = roleBuilder.createConstantQueryExpression(accountType);
			final Query queryItem = roleBuilder.createComparisonQuery(name, acctType, QueryBuilder.EQUALS);
			roleItem = roleView.executeQuery(queryItem);

		} catch (final RepositoryException e) {
			vlogError("getRoleItem(): " + e);
		} catch (final NullPointerException ne) {
			vlogError("getRoleItem(): " + ne);
		}
		return roleItem != null ? roleItem[0] : null;
	}


	private Calendar generateDate(Calendar start, Calendar end) {
		if (end.getTimeInMillis() < start.getTimeInMillis()) {
			// if end date lower than start, swap parameters.
			Calendar tmp = start;
			start = end;
			end = tmp;
		}

		Calendar date = Calendar.getInstance();
		Random r = new Random();
		long range = end.getTimeInMillis() - start.getTimeInMillis() + 1;
		long fraction = (long) (range * r.nextDouble());
		date.setTimeInMillis(fraction + start.getTimeInMillis());

		return date;
	}


	private int mLogoutlength;


	/**
	 * This will create record in db for email batch job
	 *
	 * @param pProfile
	 * @param emailName
	 * @param templateURL
	 * @param forgotPasswordLink
	 * @param order
	 * @param siteId
	 */
	public void createMailingItem(RepositoryItem pProfile, String emailName, String templateURL, String forgotPasswordLink, Order order, String siteId) {

		if (isLoggingDebug()) {
			vlogDebug("createMailingItem START");
		}

		CommercePropertyManager pm = (CommercePropertyManager) getPropertyManager();
		try {
			MutableRepository repository = (MutableRepository) pProfile.getRepository();
			MutableRepositoryItem mItem = repository.createItem(CPSEmailConstants.EMAIL_MAILING);

			mItem.setPropertyValue(CPSEmailConstants.EMAIL_CREATED_DATE, new Date());
			mItem.setPropertyValue(CPSEmailConstants.EMAIL_NAME, emailName);
			mItem.setPropertyValue(CPSEmailConstants.EMAIL_SUBJECT, emailName);
			mItem.setPropertyValue(CPSEmailConstants.EMAIL_TEMPLATE, templateURL);
			mItem.setPropertyValue(CPSConstants.SITE_ID, siteId);
			mItem.setPropertyValue(CPSEmailConstants.EMAIL_FROM, CPSEmailConstants.EMAIL_ADDRESS_FROM);

			//if forgot password email - saving forgotPasswordLink 
			if (!StringUtils.isEmpty(forgotPasswordLink)) {
				mItem.setPropertyValue(CPSEmailConstants.EMAIL_FORGOT_PSWD_LINK, forgotPasswordLink);
			}

			if (order != null) {
				mItem.setPropertyValue(CPSEmailConstants.ORDER_ID, order.getId());
			}

			repository.addItem(mItem);

			Set<RepositoryItem> mailings = (Set<RepositoryItem>) pProfile.getPropertyValue(CPSEmailConstants.EMAIL_CPS_MAILINGS);
			if (mailings != null) {
				if (!mailings.contains(mItem)) {
					mailings.add(mItem);
					((MutableRepositoryItem) pProfile).setPropertyValue(CPSEmailConstants.EMAIL_CPS_MAILINGS, mailings);
				}
			} else {
				Set<RepositoryItem> newMalings = new HashSet<RepositoryItem>();
				newMalings.add(mItem);
				((MutableRepositoryItem) pProfile).setPropertyValue(CPSEmailConstants.EMAIL_CPS_MAILINGS, newMalings);
			}
			//setting flag on profile for EmailSchedulableService
			((MutableRepositoryItem) pProfile).setPropertyValue(CPSEmailConstants.EMAIL_IS_SEND_EMAIL, true);

		} catch (RepositoryException e) {
			logError(e);
		}
	}



    /**
     * @return the locationRepository
     */
    public Repository getLocationRepository() {
        return locationRepository;
    }

    /**
     * @param locationRepository
     *            the locationRepository to set
     */
    public void setLocationRepository(Repository locationRepository) {
        this.locationRepository = locationRepository;
    }

    /**
     * @return the defaultCustomerId
     */
    public String getDefaultCustomerId() {
        return defaultCustomerId;
    }

    /**
     * @param defaultCustomerId
     *            the defaultCustomerId to set
     */
    public void setDefaultCustomerId(String defaultCustomerId) {
        this.defaultCustomerId = defaultCustomerId;
    }

    /**
     * @return the defaultBranch
     */
    public String getDefaultBranch() {
        return defaultBranch;
    }

    /**
     * @param defaultBranch
     *            the defaultBranch to set
     */
    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public int getLogoutlength() {
		return mLogoutlength;
	}

	public void setLogoutlength(int pLogoutlength) {
		mLogoutlength = pLogoutlength;
	}

	/**
	 * @param params         params for query
	 * @param rql            rql string of query
	 * @param itemDescriptor itemDescriptor to query for
	 * @param mRepository    repository to query
	 * @return return query results
	 */
	public RepositoryItem[] queryRepository(Object params[], String rql, String itemDescriptor, MutableRepository mRepository) {
		RepositoryView rpView;
		RepositoryItem[] rItems = null;
		try {
			rpView = mRepository.getView(itemDescriptor);
			RqlStatement statement = RqlStatement.parseRqlStatement(rql);
			vlogDebug("itemDescriptor:" + itemDescriptor);
			vlogDebug("statement:" + statement);
			rItems = statement.executeQuery(rpView, params);
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError("RepositoryException " + e);
			}
		}
		return rItems;
		//if(rItems!=null){ return rItems;}
		//RepositoryItem[] nothing=new RepositoryItem[0];
		//return nothing;
	}

	/**
	 * init addresses list by user associated address
	 *
	 * @param pAddresses address list to fill
	 */
	public boolean initAddressesByProfile(Profile pProfile, List<RepositoryItem> pAddresses) {
		Integer userType = (Integer) pProfile.getPropertyValue(USER_TYPE);

		if (userType != null && userType != USER_TYPE_B2B_SALES_REP) {
			Map<String, RepositoryItem> associatedCS = (Map<String, RepositoryItem>) pProfile.getPropertyValue(SECONDARY_ADDRESSES);
			if ((associatedCS != null) && (associatedCS.size() != 0)) {
				pAddresses.addAll(associatedCS.values());
				return true;
			}
		}
		return false;
	}

	public UserAssociatedCS createAssociatedCS(RepositoryItem pCS) {
		UserAssociatedCS o = new UserAssociatedCS();
		o.setAddressItem(pCS);
		o.setId(pCS.getRepositoryId());
		o.setIsDefault(false);
		o.setIsActive(true);
		return o;
	}

	/**
	 * Find address in org
	 *
	 * @param pParentOrg the parent org
	 * @param pAddress   pAddress
	 * @return the orgs shipping addresses
	 */
	public RepositoryItem findOrgFromShippingAddress(RepositoryItem pParentOrg, RepositoryItem pAddress) {
		if (pParentOrg != null) {
			Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) pParentOrg.getPropertyValue(CHILD_ORGS);
			if (childOrgs != null && childOrgs.size() > 0) {
				for (RepositoryItem org : childOrgs) {
					Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) org
							.getPropertyValue(SECONDARY_ADDRESSES);
					if (secondaryAddresses != null) {
						for (RepositoryItem address : secondaryAddresses.values()) {
							if (address.getRepositoryId().equals(pAddress.getRepositoryId())) {
								return org;
							}
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * init addresses list by user associated address
	 *
	 * @param pAddresses address list to fill
	 */
	public boolean initAddressesDataByProfile(Profile pProfile, List<UserAssociatedCS> pAddresses) {
		Integer userType = (Integer) pProfile.getPropertyValue(USER_TYPE);
		if (userType != null && userType != USER_TYPE_B2B_SALES_REP) {
			Map<String, RepositoryItem> associatedCS = (Map<String, RepositoryItem>) pProfile.getPropertyValue(SECONDARY_ADDRESSES);
			if (associatedCS != null && associatedCS.size() > 0) {
				for (RepositoryItem address : associatedCS.values()) {
					UserAssociatedCS newCS = createAssociatedCS(address);
					newCS.setOrganization(findOrgFromShippingAddress((RepositoryItem) pProfile.getPropertyValue(PARENT_ORG), address));
					pAddresses.add(newCS);
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * init addresses list by user associated address
	 *
	 * @param pAddresses address list to fill
	 */
	public void initAddressesByOrganization(Profile pProfile, List<RepositoryItem> pAddresses) {
		RepositoryItem parentOrg = (RepositoryItem) pProfile.getPropertyValue(PARENT_ORG);
		if (parentOrg != null) {
			pAddresses.addAll(getShipTosFromChildOrgs(parentOrg));
		}
	}

	private List<RepositoryItem> getShipTosFromChildOrgs(RepositoryItem parentOrg) {
		List<RepositoryItem> addresses = new ArrayList<RepositoryItem>();
		if (parentOrg != null) {
			Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) parentOrg.getPropertyValue(CHILD_ORGS);
			if (childOrgs != null) {
				for (RepositoryItem org : childOrgs) {
					Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) org
							.getPropertyValue(SECONDARY_ADDRESSES);
					if (secondaryAddresses != null) {
						addresses.addAll(secondaryAddresses.values());
					}
				}
			}
			Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) parentOrg.getPropertyValue(SECONDARY_ADDRESSES);
			if (secondaryAddresses != null) {
				addresses.addAll(secondaryAddresses.values());
			}
		}
		return addresses;
	}

	private List<UserAssociatedCS> getShipTosDataFromChildOrgs(RepositoryItem parentOrg, Set<RepositoryItem> billingAccounts) {
		List<UserAssociatedCS> addresses = new ArrayList<UserAssociatedCS>();
		if (parentOrg != null) {
			Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) parentOrg.getPropertyValue(CHILD_ORGS);
			if (childOrgs != null && childOrgs.size() > 0) {
				for (RepositoryItem org : childOrgs) {
					if (billingAccounts.contains(org)) {
						Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) org.getPropertyValue(SECONDARY_ADDRESSES);
						if (secondaryAddresses != null && secondaryAddresses.size() > 0) {
							for (RepositoryItem address : secondaryAddresses.values()) {
								UserAssociatedCS newCS = createAssociatedCS(address);
								newCS.setOrganization(org);
								addresses.add(newCS);
							}
						}
					}
				}
			} else {
				Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) parentOrg.getPropertyValue(SECONDARY_ADDRESSES);
				if (secondaryAddresses != null && secondaryAddresses.size() > 0) {
					for (RepositoryItem address : secondaryAddresses.values()) {
						addresses.add(createAssociatedCS(address));
					}
				}
			}
		}
		return addresses;
	}


	/**
	 * Inits the addresses by organization.
	 *
	 * @param pProfile   the profile
	 * @param pAddresses the addresses
	 * @param orgs       the orgs
	 * @param billTos    the bill tos
	 */
	public void initAddressesDataByOrganization(Profile pProfile, List<UserAssociatedCS> pAddresses,
												Set<UserAssociatedCB> orgs, Set<RepositoryItem> billTos) {
		RepositoryItem parentOrg = (RepositoryItem) pProfile.getPropertyValue(PARENT_ORG);
		Set<RepositoryItem> billingAccounts = (Set<RepositoryItem>) pProfile.getPropertyValue(BILLING_ACCOUNTS);
		if (parentOrg != null) {
			pAddresses.addAll(getShipTosDataFromChildOrgs(parentOrg, billingAccounts));
			Set<RepositoryItem> childOrgs = (Set<RepositoryItem>) parentOrg.getPropertyValue(CHILD_ORGS);
			if (childOrgs != null) {
				for (RepositoryItem org : childOrgs) {
					RepositoryItem billAddr = (RepositoryItem) org.getPropertyValue(BILLING_ADDRESS);
					if (billAddr != null) {
						//adding only if there is billing address
						UserAssociatedCB userAssociatedCB = new UserAssociatedCB();
						userAssociatedCB.setOrganization(org);
						if (billingAccounts != null) {
							userAssociatedCB.setSelected(billingAccounts.contains(org));
						}
						orgs.add(userAssociatedCB);
						billTos.add(billAddr);
					}
				}
			}
		}
	}

	public RepositoryItem getProfileForTemporaryPassword(String profileId, String login) {
		RepositoryItem profile = null;

		try {
			if (StringUtils.isNotBlank(profileId) && StringUtils.isNotBlank(login)) {
				RepositoryItem profileItem = getProfileRepository().getItem(profileId, getDefaultProfileType());
				if (profileItem != null) {
					String mUserLogin = (String) profileItem.getPropertyValue("login");
					if (mUserLogin.equalsIgnoreCase(login)) {
						profile = profileItem;
					}
				}
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}

		return profile;
	}

	/**
	 * Checks if user wasn't activated before.
	 *
	 * @param user User
	 * @return true - if wasn't activated before, false otherwise.
	 */
	public boolean isUserNotActivatedBefore(RepositoryItem user) {
		Boolean isActive = (Boolean) user.getPropertyValue(IS_ACTIVE);
		Date deactivatedDate = (Date) user.getPropertyValue(DEACTIVATED_DATE);

		return (isActive == null || !isActive) && deactivatedDate == null;
	}


	public void checkErrors(DynamoHttpServletResponse pResponse, GenericFormHandler pFormHandler) throws IOException {
		try {
			if (!pFormHandler.getFormError()) {
				pFormHandler.vlogDebug("no errors, return success");
				CPSFormUtils.addAjaxSuccessResponse(pResponse);
			} else {
				pFormHandler.vlogDebug("errors, return errors for display");
				Set<String> errorProperties = new HashSet<String>();
				for (int i = 0; i < pFormHandler.getFormExceptions().size(); i++) {
					DropletException exc = (DropletException) pFormHandler.getFormExceptions().get(i);
					errorProperties.add(exc.getErrorCode());
				}
				CPSFormUtils.addAjaxErrorResponse(pFormHandler, pResponse, errorProperties);
			}
		} catch (JSONException e) {
			if (pFormHandler.isLoggingError()) {
				pFormHandler.logError(e);
			}
		}
	}

	public String formatRole(String role) {
		String formatedRole = role;
		if (!StringUtils.isBlank(role)) {
			if (role.equals(ROLE_CSR_ADMIN)) {
				formatedRole = "Super Admin";
			}
			if (role.equals(ROLE_FINANCE)) {
				formatedRole = "Finance";
			}
			if (role.equals(ROLE_FINANCE_LIMITED)) {
				formatedRole = "Finance Light";
			}
			if (role.equals(ROLE_BUYER)) {
				formatedRole = "Buyer";
			}
			if (role.equals(ROLE_ACCOUNT_ADMIN)) {
				formatedRole = "Account Admin";
			}
			if (role.equals(ROLE_APPROVER)) {
				formatedRole = "Approver";
			}
		}

		return formatedRole;
	}


	/**
	 * pread file
	 *
	 * @param path     - path
	 * @param encoding -encoding
	 * @return file content
	 * @throws IOException - I/O exception
	 */
	public String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	/**
	 * get items by query
	 *
	 * @return
	 */
	public RepositoryItem[] queryOrganizations() {
		RepositoryItem[] items = null;
		try {
			RepositoryView view = getProfileRepository().getView(ORGANIZATION);
			RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_ALL);
			Object params[] = new Object[0];
			items = statement.executeQuery(view, params);
		} catch (RepositoryException e) {
			vlogDebug("Repository exception during quering organizations");
		}
		return items;
	}

    /**
     * This method will check whether the prices have to be got from the ATG PriceLists or from the Webservice based on whether the profile is anonymous or the
     * profile does not have any overrides in the JE.
     * 
     * @param profile
     * @return
     */
    public String getPriceMatrixLevelForGivenProfileAddress(RepositoryItem profile, CPSOrderImpl order) {

        if (profile == null || profile.isTransient()) {

            return CPSPricingConstants.ANONYMOUS_USER_PRICE_MATRIX;
        }
        
        if (isDisablePriceMatrixPricingForLoggedInUsers()) {
            // no need to check any priceMatrix Availability. return null, since we dont want to enable the final pricing solution.
            return null;
        }
        String customerId = getCustomerId(profile, order);
        String branch = getBranch(profile, order);
        if (customerId == getDefaultCustomerId() && branch == getDefaultBranch()) {
            // this means, the user has not selected any shipping address and there is no default ShippingAddress as well in the profile.
            // Also, the user has not opted for any pickup and hence the branch is also null
            // so check whether the user has got override at their billing address or the parent. if anyone of them is yes, then hit the JD

            RepositoryItem billingAddress = (RepositoryItem) profile.getPropertyValue("derivedBillingAddress");
            if (billingAddress != null) {
                String addressOverridesYorN = (String) billingAddress.getPropertyValue(CPSConstants.ADDRESS_OVERRIDES_Y_OR_N);
                String billToOverridesYorN = (String) billingAddress.getPropertyValue(CPSConstants.BILLTO_OVERRIDES_Y_OR_N);
                String parentOverridesYorN = (String) billingAddress.getPropertyValue(CPSConstants.PARENT_OVERRIDES_Y_OR_N);
                if (YES_VALUE.equalsIgnoreCase(addressOverridesYorN) || YES_VALUE.equalsIgnoreCase(billToOverridesYorN)
                                || YES_VALUE.equalsIgnoreCase(parentOverridesYorN)) {
                    return null; // cannot get price matrix for this address since there are overrrides
                } else {
                    String priceMatrixLevel = (String) billingAddress.getPropertyValue(ABAC06_BASE_MATRIX);
                    return priceMatrixLevel;
                }
            }

            return CPSPricingConstants.ANONYMOUS_USER_PRICE_MATRIX;
        }
        // If the order is an Instore pickup order, then dont try to get the price from matrix on the ATG side. Simply return a null matrix.

        if (order.isInStorePickupOrder()) {
            return null;
        }
        // if the customerId is a default customerId, that means, there is no selected shipAddress at all.
        // so, in that case, we dont know about any overrides. so return null matrix so that the call will be to JDE

        // TODO

        // now check whether there are NO price overrides for the given customerId. if there are NO overrides, then
        // we need to get the price from the matrix level pricing assigned to that address

        RepositoryItem contactInfo = getRepositoryItem(getProfileRepository(), customerId, CPSConstants.CONTACT_INFO);
        if(contactInfo==null) {
            //not possible, but no such address. hence return false. let JDE handle this.
            return null;
        }
        String addressOverridesYorN=(String)contactInfo.getPropertyValue(CPSConstants.ADDRESS_OVERRIDES_Y_OR_N);
        String billToOverridesYorN=(String)contactInfo.getPropertyValue(CPSConstants.BILLTO_OVERRIDES_Y_OR_N);
        String parentOverridesYorN=(String)contactInfo.getPropertyValue(CPSConstants.PARENT_OVERRIDES_Y_OR_N);
        
        if (org.apache.commons.lang3.StringUtils.equalsIgnoreCase(addressOverridesYorN, YES_VALUE)) {
            //there is an override at this address level itself. so no need to check for anything else.
            //need to talk to JDE
            vlogDebug("there are overrides at the shipping address level itself. hence need to talk to JDE");
            return null;
        }
        // here means there is no override at this address level. check the billTO and the Parent. if neither of them have got 'Y', then we can safely get price
        // from ATG
        
        if (org.apache.commons.lang3.StringUtils.equalsIgnoreCase(billToOverridesYorN, "N")
                        && org.apache.commons.lang3.StringUtils.equalsIgnoreCase(parentOverridesYorN, "N")) {
            String priceMatrixLevel = (String) contactInfo.getPropertyValue(ABAC06_BASE_MATRIX);
            if (isMakeJDECallsAlwaysForEMPMatrix() && "lpEMP".equals(priceMatrixLevel)) {
                vlogDebug("This is a EMP Price matrix.. hence need to talk to JDE");
                return null;
            }
            vlogDebug("no overrides at any level. so returning the base matrix assigned to the address - {0}", priceMatrixLevel);
            return priceMatrixLevel;
        }
        return null;
    }

    /**
     * Given the Profile and the Cart, find the correct JDE addressNu9mber for which the pricing has to be computed
     * 
     * @param profile
     * @param pOrder
     * @return
     */

    // return JdeAddressNumber
    public String getCustomerId(RepositoryItem profile, CPSOrderImpl pOrder) {
        if (profile != null) {
            RepositoryItem selectedCS = (RepositoryItem) profile.getPropertyValue(SELECTED_CS); // contactInfo ShipTO address
            RepositoryItem cartSelectedCS = (RepositoryItem) profile.getPropertyValue(CART_SELECTED_CS);

            String jdeAddressNumber = null;
            if (null != cartSelectedCS) { // if cartSelectedCS!=null get jdeAddressNumber from cartSelectedCS
                // if we selected CS on cart level that get jdeAddressNumber from cartSelectedCS
                jdeAddressNumber = (String) cartSelectedCS.getPropertyValue(CONTACT_INFO_JDE_ADDRESS_NUMBER);
            } else if ((null != pOrder) && (pOrder.getShippingGroupCount() > 0)) { // else try to get data from order
                // if we DO NOT select CS on cart level than get jdeAddressNumber from ORDER if hardGoodShipGroup
                List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
                for (ShippingGroup shippingGroup : shippingGroups) {
                    // if ((shippingGroup instanceof InStorePickupShippingGroup) && (null != selectedCS)) {
                    // jdeAddressNumber = (String) selectedCS.getPropertyValue(CONTACT_INFO_JDE_ADDRESS_NUMBER);
                    // } else
                    if (shippingGroup instanceof HardgoodShippingGroup) {
                        CPSHardgoodShippingGroup hardgoodShippingGroup = (CPSHardgoodShippingGroup) shippingGroup;
                        if (null != hardgoodShippingGroup) {
                            jdeAddressNumber = hardgoodShippingGroup.getJdeAddressNumber();
                        }
                    }
                }
            }
            if (StringUtils.isBlank(jdeAddressNumber) && (null != selectedCS)) {
                jdeAddressNumber = (String) selectedCS.getPropertyValue(CONTACT_INFO_JDE_ADDRESS_NUMBER);
            }
            // update getDefaultCustomerId() to get default CS for customer
            return StringUtils.isNotBlank(jdeAddressNumber) ? jdeAddressNumber : getDefaultCustomerId();
        }
        return getDefaultCustomerId();
    }
    
    public String getBranch(RepositoryItem profile, CPSOrderImpl pOrder) {
        String branchId = null;
        if (profile != null) {
            RepositoryItem selectedCS = (RepositoryItem) profile.getPropertyValue(SELECTED_CS); // contactInfo ShipTO address
            RepositoryItem cartSelectedCS = (RepositoryItem) profile.getPropertyValue(CART_SELECTED_CS);
            String cartSelectedStoreId = (String) profile.getPropertyValue(CART_SELECTED_STORE);

            RepositoryItem store = null;
            if (null != cartSelectedStoreId) {// If we select store on cart then get branchId from this store
                store = getRepositoryItem(getLocationRepository(), cartSelectedStoreId, STORE_ITEM_TYPE);
                if (null != store) {
                    branchId = (String) store.getPropertyValue(STORE_BRANCH_ID_PRTY);
                }
            } else if ((null != pOrder) && (pOrder.getShippingGroupCount() > 0)) { // get data from order
                // if we DO NOT select store on cart level than get branch id from pickupShipGroup, if HGSH then get CS from order and get it BusinessUnitId
                // (BranchId)
                List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
                for (ShippingGroup shippingGroup : shippingGroups) {
                    if (shippingGroup instanceof InStorePickupShippingGroup) {
                        String storeId = ((InStorePickupShippingGroup) shippingGroup).getLocationId();
                        store = getRepositoryItem(getLocationRepository(), storeId, STORE_ITEM_TYPE);
                        if (null != store) {
                            branchId = (String) store.getPropertyValue(STORE_BRANCH_ID_PRTY);
                        }
                    } else if (shippingGroup instanceof HardgoodShippingGroup) {
                        CPSHardgoodShippingGroup hardgoodShippingGroup = (CPSHardgoodShippingGroup) shippingGroup;
                        String jdeAddressNumber = hardgoodShippingGroup.getJdeAddressNumber();
                        RepositoryItem address = getRepositoryItem(getProfileRepository(), jdeAddressNumber, CONTACT_INFO);
                        if (null != address) {
                            branchId = (String) address.getPropertyValue(BUSINESS_UNIT);
                        }
                    }
                }
            }
            if (StringUtils.isBlank(branchId) && (null != cartSelectedCS)) {// If we DO NOT select store on cart, BUT select CS on cart then get branchId from
                                                                            // this CS
                branchId = (String) cartSelectedCS.getPropertyValue(BUSINESS_UNIT);
            }
            if (StringUtils.isBlank(branchId) && (null != selectedCS)) {
                branchId = (String) selectedCS.getPropertyValue(BUSINESS_UNIT);
            }

        }
        return StringUtils.isNotBlank(branchId) ? branchId : getDefaultBranch();
    }

    

    private RepositoryItem getRepositoryItem(Repository pRepository, String pRepItemId, String pRepItemType) {
        RepositoryItem repItem = null;
        try {
            if (!StringUtils.isBlank(pRepItemId)) {
                repItem = pRepository.getItem(pRepItemId, pRepItemType);
            }
        } catch (RepositoryException re) {
            vlogError("getRepositoryItem() RepositoryException", re);
        }
        return repItem;
    }


    public boolean isAllowGuestCheckout() {
		return mAllowGuestCheckout;
	}

	public void setAllowGuestCheckout(boolean pAllowGuestCheckout) {
		mAllowGuestCheckout = pAllowGuestCheckout;
	}

	public boolean isAllowCreditCardOnApproval() {
		return mAllowCreditCardOnApproval;
	}

	public void setAllowCreditCardOnApproval(boolean pAllowCreditCardOnApproval) {
		mAllowCreditCardOnApproval = pAllowCreditCardOnApproval;
	}

	public boolean isCreditCardEnabled() {
		return mCreditCardEnabled;
	}

	public void setCreditCardEnabled(boolean pCreditCardEnabled) {
		mCreditCardEnabled = pCreditCardEnabled;
	}

	public boolean isAllowNewUserSelfRegistration() {
		return mAllowNewUserSelfRegistration;
	}

	public void setAllowNewUserSelfRegistration(boolean pAllowNewUserSelfRegistration) {
		mAllowNewUserSelfRegistration = pAllowNewUserSelfRegistration;
	}

	public boolean isShowPaymentMethod() {
		return mShowPaymentMethod;
	}

	public void setShowPaymentMethod(boolean pShowPaymentMethod) {
		mShowPaymentMethod = pShowPaymentMethod;
	}

    /**
     * @return the makeJDECallsAlwaysForEMPMatrix
     */
    public boolean isMakeJDECallsAlwaysForEMPMatrix() {
        return makeJDECallsAlwaysForEMPMatrix;
    }

    /**
     * @param makeJDECallsAlwaysForEMPMatrix
     *            the makeJDECallsAlwaysForEMPMatrix to set
     */
    public void setMakeJDECallsAlwaysForEMPMatrix(boolean makeJDECallsAlwaysForEMPMatrix) {
        this.makeJDECallsAlwaysForEMPMatrix = makeJDECallsAlwaysForEMPMatrix;
    }

    /**
     * @return the disablePriceMatrixPricingForLoggedInUsers
     */
    public boolean isDisablePriceMatrixPricingForLoggedInUsers() {
        return disablePriceMatrixPricingForLoggedInUsers;
    }

    /**
     * @param disablePriceMatrixPricingForLoggedInUsers
     *            the disablePriceMatrixPricingForLoggedInUsers to set
     */
    public void setDisablePriceMatrixPricingForLoggedInUsers(boolean disablePriceMatrixPricingForLoggedInUsers) {
        this.disablePriceMatrixPricingForLoggedInUsers = disablePriceMatrixPricingForLoggedInUsers;
    }

}