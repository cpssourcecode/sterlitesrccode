package com.cps.userprofiling;

/**
 * @author Dmitry Golubev
 **/
public interface IContactUsFormHandler {
	/**
	 * input name
	 */
	public String NAME = "name";
	/**
	 * input email
	 */
	public String EMAIL = "email";
	/**
	 * input company
	 */
	public String COMPANY = "company";
	/**
	 * input accountNumber
	 */
	public String ACCOUNT_NUMBER = "accountNumber";
	/**
	 * input phone
	 */
	public String PHONE = "phone";
	/**
	 * input subject
	 */
	public String SUBJECT = "subject";
	/**
	 * input method
	 */
	public String METHOD = "method";
	/**
	 * input comments
	 */
	public String COMMENTS = "comments";
}
