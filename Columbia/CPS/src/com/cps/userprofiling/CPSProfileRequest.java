package com.cps.userprofiling;

import atg.userprofiling.ProfileRequest;

/**
 * 
 * @author VSG
 *
 */
public class CPSProfileRequest extends ProfileRequest {

	public void setRequestSource(int pRequestSource) {
		mRequestSource = pRequestSource;
	}
}
