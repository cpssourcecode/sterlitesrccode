package com.cps.userprofiling;

import static com.cps.util.CPSConstants.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.cps.commerce.approval.ApprovalManager;
import com.cps.commerce.order.CPSHardgoodShippingGroup;
import com.cps.commerce.order.CPSInStorePickupShippingGroup;
import com.cps.commerce.order.CPSOrderConnectionManager;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.packingslip.CPSPackingSlipConnectionManager;
import com.cps.csr.util.CPSCSRLoggingManager;
import com.cps.droplet.DropletConstants;
import com.cps.email.CommonEmailSender;
import com.cps.invoice.InvoiceInfo;
import com.cps.packingslip.PackingSlipRecord;
import com.cps.reorders.AutoOrderManager;
import com.cps.scheduler.manager.RegisteredUserReportManager;
import com.cps.userprofiling.service.AccountLookupSessionInfo;
import com.cps.userprofiling.service.CookieService;
import com.cps.userprofiling.service.LoginService;
import com.cps.userprofiling.service.ProfileRecentlyViewedService;
import com.cps.userprofiling.service.ProfileUpdateService;
import com.cps.userprofiling.service.ResetPasswordService;
import com.cps.userprofiling.util.LoginSessionInfo;
import com.cps.userprofiling.validator.LoginValidator;
import com.cps.userprofiling.validator.RegistrationValidator;
import com.cps.userprofiling.validator.ResetPasswordValidator;
import com.cps.userprofiling.validator.UpdateValidator;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSGlobalProperties;
import com.cps.util.CPSMessageUtils;
import com.cps.util.CPSSessionBean;
import com.csp.order.OrderInfo;

import atg.beans.DynamicBeans;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemImpl;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.ShippingGroup;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.multisite.SiteContextManager;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.security.PasswordHasher;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.CookieManager;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;
import vsg.constants.VSGMessageConstants;
import vsg.crypto.AESEncryptor;
import vsg.crypto.EncryptorException;
import vsg.userprofiling.VSGProfileFormHandler;
import vsg.util.ajax.AjaxUtils;

public class CPSProfileFormHandler extends VSGProfileFormHandler {

	private static final String CSR_LOGIN_SUCCESS_URL = "/account/manage-companies.jsp";
    private static final String ZIP_SEPARATOR = "-";

	/**
	 * reset password validator
	 */
	private ResetPasswordValidator mResetPasswordValidator;
	/**
	 * reset password service
	 */
	private ResetPasswordService mResetPasswordService;

	/**
	 * Cookie service
	 */
	private CookieService mCookieService;
	/**
	 * login validator
	 */
	private LoginValidator mLoginValidator;
	/**
	 * login service
	 */
	private LoginService mLoginService;
	/**
	 * session bean
	 */
	private LoginSessionInfo mLoginSessionInfo;

	/**
	 * recently viewed service
	 */
	private ProfileRecentlyViewedService mRecentlyViewedService;

	private Order mOrder;

	private CookieManager mCookieManager;

	/**
	 * update validator
	 */
	private UpdateValidator mUpdateValidator;

	/**
	 * Profile update service
	 */
	private ProfileUpdateService mProfileUpdateService;

	/**
	 * email sender
	 */
	private CommonEmailSender mEmailSender;

	/**
	 * Variable to show success message on successful update
	 */
	private boolean profileUpdateSuccessful;

	private boolean contactUpdateSuccessful;

	private boolean securityUpdateSuccessful;

	/**
	 * Holder for errors in contact section
	 */
	private Vector personalFormExceptions;

	/**
	 * Holder for errors in contact section
	 */
	private Vector contactFormExceptions;

	/**
	 * Holder for error in security section
	 */
	private Vector securityFormExceptions;

	private Map<String, String> registrationAddress = new LinkedHashMap<String, String>();
	private Boolean showCS = false;

	private String creditAppSuccessURL;
	private String creditAppErrorURL;
	private String creditAppFlag;

	private CPSSessionBean sessionBean;
	private MutableRepository creditAppRepository;
	private String creditAppType;
	private String creditAppContactType;
	private String creditAppURL;
	private CommonEmailSender emailSender;
	private CPSCSRLoggingManager csrLoggingManager;
	private CPSInvoiceConnectionManager invoiceConnectionManager;
	private CPSOrderConnectionManager orderConnectionManager;
	private CPSPackingSlipConnectionManager packingSlipConnectionManager;

	private String loginURLParamQuestion;
	private String loginURLParamAnd;
	private String contactusSuccessURL;
	private String contactusErrorURL;
	private String mPreRegisterPage;

	private Properties mRedirectURLMap;

	private static final String ORDERS_RQL_QUERY = "jdeOrderNumber =?0";

	private OrderManager mOrderManager;
	private AutoOrderManager mAutoOrderManager;

	private String pFailureURL;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private String confirmedPassword;
	private String organizationID;
	private String accountNumber;
	private String zipCode;
	private String organizationViewName;
	private String findOrganizationQuery;
	private String activationUrl;
	private String activationToken;
	private boolean isBuyer;
	private AESEncryptor encryptor;
	private boolean showCaptchaReCaptcha;
	private String activationSuccessURL;
	private String registrationSuccessURL;
	private ApprovalManager approvalManager;
	private RepositoryItem createdUser;
	private AccountLookupSessionInfo accountLookupSessionInfo;
	private String supervisorName;
	private String supervisorEmail;
	private boolean isValidAdmin;
	
	private String userId;
	private boolean removeUserId;
	private Set<RepositoryItem> userIds = new HashSet<RepositoryItem>();

	private RegistrationValidator registrationValidator;
	private Map<String, String> role = new HashMap<String, String>();
    private boolean preventLoginImmediatelyUponRegistration = true;

	private RepeatingRequestMonitor mRepeatingRequestMonitor;
	private CPSGlobalProperties cpsGlobalProperties;




	/**
	 * handles user login
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occures
	 * @throws IOException      if error occures
	 */
	@Override
	public boolean handleLogin(DynamoHttpServletRequest pRequest,
							   DynamoHttpServletResponse pResponse) throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleLogin";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				super.handleLogin(pRequest, pResponse);

				Map<String, Object> result = new HashMap<String, Object>();
				//result.put("showCS", getLoginSessionInfo().isShouldSelectCS());
				result.put("showCaptcha", getLoginSessionInfo().isShowCaptcha());
				result.put("onHold", getLoginSessionInfo().isOnHold());
				result.put("redirect", getLoginSessionInfo().isShouldRedirect());
				if (getLoginSessionInfo().isShouldRedirect()) {
					result.put("redirectUrl", getLoginSuccessURL());
				}
				AjaxUtils.addAjaxResponse(this, pResponse, result);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		if (AjaxUtils.isAjaxRequest(pRequest)) {
			return false;
		} else {
			return super.checkFormRedirect(getLoginSuccessURL(), getLoginErrorURL(), pRequest, pResponse);
		}

	}

	/**
	 * Pre Login User, validates entries
	 *
	 * @throws java.io.IOException
	 * @throws javax.servlet.ServletException
	 */
	@Override
	public void preLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		vlogDebug("preLoginUser - Errors: " + getLoginSessionInfo().getLoginErrorCount() + ", Captcha Errors: " + getLoginSessionInfo().getLoginErrorWithCaptchaCount());

		if (getLoginValidator().isCurrentUserAllowedToLogin(this, pRequest, getLoginSessionInfo())) {
			if (getLoginValidator().validateLoginFormForEmptyInputs(this, pRequest)) {
				if (getLoginValidator().validateCaptcha(this, getLoginSessionInfo(), pRequest)) {
					// Check to trim
					String userLogin = (String) getValue().get(LOGIN);
					if (!StringUtils.isBlank(userLogin)) {
						userLogin = userLogin.trim().toLowerCase();
					}
					getValue().put(LOGIN, userLogin);

					String userPassword = (String) getValue().get(PSWD);
					RepositoryItem user = getLoginValidator().isUserExist(this, userLogin, userPassword, pRequest);
					if (user != null) {
						getLoginValidator().isUserAllowedToLogin(this, user, pRequest, getLoginSessionInfo());
					} else {
						vlogDebug("Incrementing due to invalid captcha, request " + pRequest.hashCode());
						incrementLoginError(pRequest);
					}
				} else {
					vlogDebug("Incrementing due to empty inputs, request " + pRequest.hashCode());
					incrementLoginError(pRequest);
				}
			} else {
				vlogDebug("Incrementing due to user not allowed to login, request " + pRequest.hashCode());
				incrementLoginError(pRequest);
			}
		}
	}

	/**
	 * increments login errors and locks user
	 */
	private void incrementLoginError(DynamoHttpServletRequest pRequest) {
		getLoginSessionInfo().incrementLoginError();
		if (getLoginSessionInfo().isLockUser()) {
			lockUser(pRequest);
		}
		vlogDebug("Incrementing Login Error, now " + getLoginSessionInfo().getLoginErrorCount());
	}

	/**
	 * check remember me and init selected CS, merge recently viewed products
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	@Override
	public void postLoginUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

        Profile profile = getProfile();
		if (!getFormError()) {
			//SM-438 updating last activity
			profile.setPropertyValue(CPSConstants.LAST_ACTIVITY, new Date());
			profile.setPropertyValue(FIRST_LOGIN, true);
			// CPS-1180 - Login Errors Persist After Successful Login
			getLoginSessionInfo().resetLoginErrors();
			
			profile.setPropertyValue(SELECTED_CS, profile.getPropertyValue(CPSConstants.SHIPPING_ADDRESS));
            // just in case, clear the PriceCache in the sessionBean to start with a clean slate
            getSessionBean().getProductPricesCache().clear();
			checkRememberMe(pRequest, pResponse);
			//checkRedirectUrl(pRequest);
			boolean isCSRUser = getLoginService().checkCSRUser(profile, getLoginSessionInfo());
			if (isCSRUser) {
				setLoginSuccessURL(CSR_LOGIN_SUCCESS_URL);
			}
            // getLoginService().initSelectedCS(profile, getLoginSessionInfo());
			getLoginService().checkUserOnHold(profile, getLoginSessionInfo());

			ProfileRecentlyViewedService recentlyViewedService = getRecentlyViewedService();
			if (recentlyViewedService != null) {
				recentlyViewedService.mergeTransient();
			}

			String redirect = (String) getValue().get(REDIRECT);
			if (!StringUtils.isBlank(redirect)) {
				String contextRoot = pRequest.getContextPath();
				String redirectUrl = getRedirectURLMap().getProperty(redirect);
				if (!StringUtils.isBlank(redirectUrl)) {
					setLoginSuccessURL(contextRoot + redirectUrl);
					getLoginSessionInfo().setShouldRedirect(true);
				}
				vlogDebug("New Success URL: " + getLoginSuccessURL());
			}

		}
		super.postLoginUser(pRequest, pResponse);
		/*String originalURL=pRequest.getParameter("originalURL");
		if(StringUtils.isNotBlank(originalURL)){
			setLoginSuccessURL(originalURL);
			vlogDebug("New Success URL: {0}", getLoginSuccessURL());
		}*/
        getLoginService().initSelectedCS(profile, getLoginSessionInfo());
        // check whether there is a valid Current Order and that order also contains a HardGoodShippingGroup with a valid shipAddr
        // if yes, check whether that shipAddr is available in the contactInfo. if not, remove it

        TransactionManager tm = getTransactionManager();
        TransactionDemarcation td = getTransactionDemarcation();
        boolean shouldRollback = false;
        try {
            if (tm != null) {
                td.begin(tm, TransactionDemarcation.REQUIRED);
            }
            CPSOrderImpl currentOrder = (CPSOrderImpl) getShoppingCart().getCurrent();

            synchronized (currentOrder) {
                List<ShippingGroup> shipGroups = currentOrder.getShippingGroups();
                if (shipGroups != null) {
                    for (ShippingGroup sGroup : shipGroups) {
                        if (sGroup instanceof CPSHardgoodShippingGroup) {
                            CPSHardgoodShippingGroup hgShpGrp = (CPSHardgoodShippingGroup) sGroup;
                            String addressJdeNumber = hgShpGrp.getJdeAddressNumber();
                            if (addressJdeNumber != null) {
                            try {
                                RqlStatement getContactInfoForJdeNumberStmt = RqlStatement.parseRqlStatement("jdeAddressNumber=?0");
                                    RepositoryView rView = getProfileTools().getProfileRepository().getView(CPSConstants.CONTACT_INFO);
                                Object[] params = new Object[] { addressJdeNumber };
                                RepositoryItem[] addresses = getContactInfoForJdeNumberStmt.executeQuery(rView, params);
                                if (addresses == null) {
                                    vlogDebug("address with jdeAddressNumber - {0} is not there in the database. so reset the shipping address",
                                                    addressJdeNumber);
                                    hgShpGrp.setShippingAddress(new Address());
                                    hgShpGrp.setJdeAddressNumber(null);
                                    } else {
                                        // it is a valid address.
                                    	if(getCpsGlobalProperties().isEnableDefaultShipTo() && currentOrder.getCommerceItemCount()==0){
                                        	getProfile().setPropertyValue(CPSConstants.CART_SELECTED_CS, profile.getPropertyValue(CPSConstants.DERIVED_SHIPPING_ADDRESS));
                                        	getProfile().setPropertyValue(CPSConstants.SELECTED_CS, profile.getPropertyValue(CPSConstants.DERIVED_SHIPPING_ADDRESS));
                                        }else{
                                        	vlogDebug("address with jdenumber - {0} is valid. setting it to cartSelectedCS", addressJdeNumber);
                                            getProfile().setPropertyValue(CPSConstants.CART_SELECTED_CS, addresses[0]);
                                            getProfile().setPropertyValue(CPSConstants.SELECTED_CS, addresses[0]);
                                            getProfile().setPropertyValue(CPSConstants.CART_SELECTED_DELIVERY_METHOD,CPSConstants.DELEVERY_METHOD_SHIPPED);
                                        }
                                        
                                }
                            } catch (RepositoryException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }else{
                        	getProfile().setPropertyValue(CPSConstants.CART_SELECTED_CS, profile.getPropertyValue(CPSConstants.SHIPPING_ADDRESS));
                            getProfile().setPropertyValue(CPSConstants.SELECTED_CS, profile.getPropertyValue(CPSConstants.SHIPPING_ADDRESS));
                        }
                        }else {
                        	String locationId=null;
                        	if (sGroup instanceof CPSInStorePickupShippingGroup) {
                                String storeId = ((CPSInStorePickupShippingGroup) sGroup).getLocationId();
                                RepositoryItem store = getRepositoryItem(((CPSProfileTools) getProfileTools()).getLocationRepository(), storeId, STORE_ITEM_TYPE);
                                if (null != store) {
                                    locationId = (String) store.getRepositoryId();
                                    getProfile().setPropertyValue(CPSConstants.CART_SELECTED_STORE, locationId);
                                    getProfile().setPropertyValue(CPSConstants.CART_SELECTED_DELIVERY_METHOD,CPSConstants.DELEVERY_METHOD_PICK_UP);
                                }
                            }
                        }
                    }
                }
                getOrderManager().updateOrder(currentOrder);
                
                RepositoryItem billingAddress = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.BILLING_ADDRESS);
                RepositoryItem cartSelectedCS  = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.CART_SELECTED_CS);
                RepositoryItem defaultShippingAddress  = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.DERIVED_SHIPPING_ADDRESS);
                
                if(billingAddress!=null || cartSelectedCS != null || defaultShippingAddress !=null){
                	String billAddressPayInstRYIN = null;
                	String cartAddressPayInstRYIN = null;
                	String defaultShipAddressPayInstRYIN = null;
                	if(billingAddress!=null){
                		billAddressPayInstRYIN = (String) billingAddress.getPropertyValue(CPSConstants.PAY_INST_RYIN);
                	}
                	if(cartSelectedCS!=null){
                		cartAddressPayInstRYIN = (String) cartSelectedCS.getPropertyValue(CPSConstants.PAY_INST_RYIN);
                	}
                	if(defaultShippingAddress!=null){
                		defaultShipAddressPayInstRYIN = (String) defaultShippingAddress.getPropertyValue(CPSConstants.PAY_INST_RYIN);
                	}
                	
					if ((billAddressPayInstRYIN != null && billAddressPayInstRYIN.equals("?"))
							|| (cartAddressPayInstRYIN != null && cartAddressPayInstRYIN.equals("?"))
							|| (defaultShipAddressPayInstRYIN != null && defaultShipAddressPayInstRYIN.equals("?"))) {
						getProfile().setPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED, false);
						getProfile().setPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION, true);
					} else {
						getProfile().setPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED, true);
						getProfile().setPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION, false);
					}
                }
                
            }
            getAutoOrderManager().updateSavedCart(currentOrder);
        } catch (final Exception e) {
            shouldRollback = true;
            vlogError("Error clearing shipping address - {0}", e.getMessage());
            logError(e);
        } finally {
            try {
                td.end(shouldRollback);
            } catch (TransactionDemarcationException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

	}
	
	/**
	 * 
	 * @param pRepository
	 * @param pRepItemId
	 * @param pRepItemType
	 * @return
	 */
	
	private RepositoryItem getRepositoryItem(Repository pRepository, String pRepItemId, String pRepItemType) {
        RepositoryItem repItem = null;
        try {
            if (!StringUtils.isBlank(pRepItemId)) {
                repItem = pRepository.getItem(pRepItemId, pRepItemType);
            }
        } catch (RepositoryException re) {
            vlogError("getRepositoryItem() RepositoryException", re);
        }
        return repItem;
    }

	/**
	 * lock user and add form error
	 */
	private void lockUser(DynamoHttpServletRequest pRequest) {
		getLoginService().lockUser(getProfile(), (String) getValue().get(LOGIN), getLoginSessionInfo());

		getLoginSessionInfo().resetLoginErrors();

		getFormExceptions().clear();

		CPSMessageUtils.addFormException(this, ERR_MESSAGES_REPOSITORY,
				CPSErrorCodes.ERR_LOGIN_SESSION_LOCKED, pRequest.getLocale(), null);
	}

	/**
	 * checks if remember me is selected or not and process cookie
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 */
	private void checkRememberMe(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String rememberMeValue = (String) getValue().get(REMEMBER_ME);
		boolean rememberMe = TRUE.equalsIgnoreCase(rememberMeValue);
		if (rememberMe) {
			getCookieService().setUserCookies(
					(String) getProfile().getPropertyValue(LOGIN),
					(String) getProfile().getPropertyValue(FIRST_NAME),
					pResponse
			);
		} else {
			getCookieService().removeUserCookies(pRequest, pResponse);
		}
	}

	@Override
	public boolean handleLogout(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleLogout";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				super.handleLogout(pRequest, pResponse);
				AjaxUtils.addAjaxResponse(this, pResponse, null);

			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		if (AjaxUtils.isAjaxRequest(pRequest)) {
			return false;
		} else {
			return super.checkFormRedirect(getLogoutSuccessURL(), getLogoutErrorURL(), pRequest, pResponse);
		}
	}

	/**
	 * clear selected cs and selected org
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	@Override
	public void preLogoutUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		// These were somehow getting stored, make sure they are set to null before logging out
		Profile profile = getProfile();
		Integer userType = (Integer) profile.getPropertyValue(USER_TYPE);
		if (userType != null && userType == 3) {
			profile.setPropertyValue(SELECTED_ORG, null);
		}
		profile.setPropertyValue(SELECTED_CS, null);
		profile.setPropertyValue(SESSION_LOCKED, null);
		profile.setPropertyValue(CART_SELECTED_CS, null);
		profile.setPropertyValue(CART_SELECTED_STORE, null);
		profile.setPropertyValue(CPSConstants.LAST_LOGOUT, new Date());

		getCookieManager().expireProfileCookies(profile, pRequest, pResponse);

		Order order = getOrder();
		if (null != order) {
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				try {
					td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
					synchronized (order) {
						List<CommerceItem> commerceItems = order.getCommerceItems();

						for (CommerceItem commerceItem : commerceItems) {
							CommerceItemImpl commerceItem1 = (CommerceItemImpl) commerceItem;
							commerceItem1.setPropertyValue(IS_PRICED, false);
							commerceItem1.setPropertyValue(IS_AVAILABILITY_CHECKED, false);
						}
						((CommerceProfileTools) getProfileTools()).getOrderManager().updateOrder(order);
					}
				} catch (Exception e) {
					rollback = true;
					if (isLoggingError()) {
						logError(e);
					}
				} finally {
					td.end(rollback);
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}

		}

		super.preLogoutUser(pRequest, pResponse);
	}

	@Override
	public void postLogoutUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		super.postLogoutUser(pRequest, pResponse);

		// CPS-1180 - To make sure the new anonymous session that should be created doesn't lock new users out,
		// especially if they've been idling recently.
		getProfile().setPropertyValue(SESSION_LOCKED, null);

		if (getLoginSessionInfo() != null) {
			getLoginSessionInfo().resetLoginErrors();
		}
	}

	//************************************************************************************************************
	// START UPDATE PERSONAL INFO SECTION
	//************************************************************************************************************

	/**
	 * Handles update user personal info from view profile page
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleUpdatePersonalInfo(DynamoHttpServletRequest pRequest,
											DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		vlogDebug("ProfileFormHandler.handleUpdatePersonalInfo.start");

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleUpdatePersonalInfo";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				preUpdatePersonalInfo(pRequest, pResponse);

				if (!getFormError()) {
					updatePersonalInfo(pRequest, pResponse);
				}

				postUpdatePersonalInfo(pRequest, pResponse);
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}
		vlogDebug("ProfileFormHandler.handleUpdatePersonalInfo.end");
		return checkFormRedirect(getUpdateSuccessURL(), getUpdateErrorURL(), pRequest, pResponse);
	}

	/**
	 * Validates entries for personal update
	 *
	 * @param pRequest
	 * @param pResponse
	 */
	public void preUpdatePersonalInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("ProfileFormHandler.preUpdatePersonalInfo.start");

		String firstName = (String) getValue().get(FIRST_NAME);
		String lastName = (String) getValue().get(LAST_NAME);
		String title = (String) getValue().get(JOB_TITLE);

		vlogDebug("Entered values to update: " + firstName + ", " + lastName + ", " + title);

		getUpdateValidator().validateProfilePersonalInfo(this, firstName, lastName, title, pRequest);

		vlogDebug("ProfileFormHandler.preUpdatePersonalInfo.end");
	}

	/**
	 * Update the users personal info
	 *
	 * @param pRequest
	 * @param pResponse
	 */
	public void updatePersonalInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws RepositoryException {
		vlogDebug("ProfileFormHandler.updatePersonaInfo.start");

		String firstName = (String) getValue().get(FIRST_NAME);
		String lastName = (String) getValue().get(LAST_NAME);
		String title = (String) getValue().get(JOB_TITLE);

		getProfileUpdateService().updateProfilePersonalInfo(getProfile(), firstName, lastName, title);

		vlogDebug("ProfileFormHandler.updatePersonalInfo.end");
	}

	/**
	 * Holder method for post update actions
	 *
	 * @param pRequest
	 * @param pResponse
	 */
	public void postUpdatePersonalInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("Form Exceptions: " + getFormExceptions());
		setProfileUpdateSuccessful(!getFormError());

		if (!getFormError()) {
			// Send notification e-mail, profile was updated
			getEmailSender().sendUserProfileUpdatedEmail(getProfile());
		}
		setPersonalFormExceptions(getFormExceptions());
	}

	//************************************************************************************************************
	// END UPDATE PERSONAL INFO SECTION
	//************************************************************************************************************

	//************************************************************************************************************
	// START UPDATE CONTACT INFO SECTION
	//************************************************************************************************************

	/**
	 * Handles updating user contact info from view profile page
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleUpdateContactInfo(DynamoHttpServletRequest pRequest,
										   DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		vlogDebug("ProfileFormHandler.handleUpdateContactInfo.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleUpdateContactInfo";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				preUpdateContactInfo(pRequest, pResponse);

				if (!getFormError()) {
					updateContactInfo(pRequest, pResponse);
				}

				postUpdateContactInfo(pRequest, pResponse);
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}

		vlogDebug("ProfileFormHandler.handleUpdateContactInfo.end");
		return checkFormRedirect(getUpdateSuccessURL(), getUpdateErrorURL(), pRequest, pResponse);
	}

	public void preUpdateContactInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("ProfileFormHandler.preUpdateContantInfo.start");

		String email = (String) getValue().get(EMAIL);
		String mobilePhone = (String) getValue().get(PHONE_NUMBER);
		String directPhone = (String) getValue().get(ALTERNATE_PHONE_NUMBER);
		String address1 = (String) getValue().get(ADDRESS1);
		String address2 = (String) getValue().get(ADDRESS2);
		String city = (String) getValue().get(CITY);
		String state = (String) getValue().get(STATE);
		String postalCode = (String) getValue().get(POSTAL_CODE);

		vlogDebug("Entered values to update: " + email + ", " + mobilePhone + ", " + directPhone + ", "
				+ address1 + ", " + address2 + ", " + city + ", " + state + ", " + postalCode);

		getUpdateValidator().validateProfileContactInfo(this, email, mobilePhone, directPhone,
				address1, address2, city, state, postalCode, pRequest);

		vlogDebug("ProfileFormHandler.preUpdateContactInfo.end");
	}

	public void updateContactInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws RepositoryException {
		vlogDebug("ProfileFormHandler.updateContactInfo.start");

		String email = (String) getValue().get(EMAIL);
		String mobilePhone = (String) getValue().get(PHONE_NUMBER);
		String directPhone = (String) getValue().get(ALTERNATE_PHONE_NUMBER);
		String address1 = (String) getValue().get(ADDRESS1);
		String address2 = (String) getValue().get(ADDRESS2);
		String city = (String) getValue().get(CITY);
		String state = (String) getValue().get(STATE);
		String postalCode = (String) getValue().get(POSTAL_CODE);

		getProfileUpdateService().updateProfileContactInfo(getProfile(), email, mobilePhone, directPhone,
				address1, address2, city, state, postalCode);

		vlogDebug("ProfileFormHandler.updateContactInfo.end");
	}

	public void postUpdateContactInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("ProfileFormHandler.postUpdateContactInfo.start");

		setContactUpdateSuccessful(!getFormError());

		if (!getFormError()) {
			// Send notification e-mail, profile was updated
			getEmailSender().sendUserProfileUpdatedEmail(getProfile());
		}

		setContactFormExceptions(getFormExceptions());

		vlogDebug("ProfileFormHandler.postUpdateContactInfo.end");
	}


	//************************************************************************************************************
	// END UPDATE CONTACT INFO SECTION
	//************************************************************************************************************

	//************************************************************************************************************
	// START UPDATE SECURITY INFO SECTION
	//************************************************************************************************************
	public boolean handleUpdateSecurityInfo(DynamoHttpServletRequest pRequest,
											DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		vlogDebug("ProfileFormHandler.handleUpdateSecurityInfo.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleUpdateSecurityInfo";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				preUpdateSecurityInfo(pRequest, pResponse);

				if (!getFormError()) {
					updateSecurityInfo(pRequest, pResponse);
				}

				postUpdateSecurityInfo(pRequest, pResponse);
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (Exception e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}

		vlogDebug("ProfileFormHandler.handleUpdateSecurityInfo.end");
		return checkFormRedirect(getUpdateSuccessURL(), getUpdateErrorURL(), pRequest, pResponse);
	}

	public void preUpdateSecurityInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("ProfileFormHandler.preUpdateSecurityInfo.start");

		String currentPassword = (String) getProfile().getPropertyValue(PSWD);
		String oldPassword = (String) getValue().get(OLD_PSWD);
		String oldPasswordValue = getOldPasswordValue(oldPassword);
		String newPassword = (String) getValue().get(NEW_PSWD);
		String confirmPassword = (String) getValue().get(CONFIRM_PSWD);

		getUpdateValidator().validateProfileSecurityInfo(this, currentPassword, oldPasswordValue, newPassword, confirmPassword, pRequest);

		vlogDebug("ProfileFormHandler.preUpdateSecurityInfo.end");
	}

	public void updateSecurityInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		vlogDebug("ProfileFormHandler.updateSecurityInfo.start");

		String newPassword = (String) getValue().get(NEW_PSWD);

		ProfileTools ptools = getProfileTools();
		PropertyManager pmgr = ptools.getPropertyManager();
		String passwordPropertyName = pmgr.getPasswordPropertyName();
		getValue().put(passwordPropertyName, getValue().get(NEW_PSWD));
		getValue().put("OLDPASSWORD", getValue().get(OLD_PSWD));

		changePassword(pRequest, pResponse);

		vlogDebug("ProfileFormHandler.updateSecurityInfo.end");
	}

	public void postUpdateSecurityInfo(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("ProfileFormHandler.postUpdateSecurityInfo.start");

		setSecurityUpdateSuccessful(!getFormError());

		if (!getFormError()) {
			// Send notification e-mail, profile was updated
			getEmailSender().sendUserProfileUpdatedEmail(getProfile());
		}

		setSecurityFormExceptions(getFormExceptions());

		vlogDebug("ProfileFormHandler.postUpdateSecurityInfo.end");
	}

	private String getOldPasswordValue(String oldPassword) {
		String oldPasswordValue = oldPassword;
		try {
			PropertyManager pmgr = getProfileTools().getPropertyManager();
			String loginPropertyName = pmgr.getLoginPropertyName();
			MutableRepository repository = (MutableRepository) getProfile().getRepository();
			String profileId = getProfile().getRepositoryId();
			String typename = getProfile().getItemDescriptor().getItemDescriptorName();
			MutableRepositoryItem mutableItem = repository.getItemForUpdate(profileId, typename);
			String login = (String) DynamicBeans.getPropertyValue(mutableItem, loginPropertyName);
			String passwordSaltPropertyName = pmgr.getPasswordSaltPropertyName();

			String salt = (String) DynamicBeans.getPropertyValue(mutableItem, passwordSaltPropertyName);

			PasswordHasher hasher = getProfileTools().getPasswordHasherForUserItem(mutableItem);
			oldPasswordValue = pmgr.generatePassword(salt, oldPassword, hasher);

		} catch (Exception e) {
			vlogError(e, "Error");
		}
		return oldPasswordValue;
	}

	//************************************************************************************************************
	// END UPDATE SECURITY INFO SECTION
	//************************************************************************************************************

	//************************************************************************************************************
	// START CREATE USER SECTION
	//************************************************************************************************************

	@Override
	public boolean handleCreate(DynamoHttpServletRequest pRequest,
								DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleCreate";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
//				TODO check if user is already created and with elouqua_id and was exported
				RepositoryItem user = getProfileTools().getItem(getEmail(), (String) null, this.getLoginProfileType());
				if (null != user) {
					String eloquaId = (String) user.getPropertyValue(ELOQUA_ID);
					Object oExportedWithEloquaId = user.getPropertyValue(EXPORTED_WITH_ELOQUA_ID);
					boolean exportedWithEloquaId = false;
					if (null != oExportedWithEloquaId) {
						exportedWithEloquaId = (boolean) oExportedWithEloquaId;
					}
					if (!StringUtils.isBlank(eloquaId) && exportedWithEloquaId) {
						setRepositoryId(user.getRepositoryId());
						handleDelete(pRequest, pResponse);
					}
				}

				super.handleCreate(pRequest, pResponse);

				AjaxUtils.addAjaxResponse(this, pResponse, null);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		if (AjaxUtils.isAjaxRequest(pRequest)) {
			return false;
		} else {
			return super.checkFormRedirect(getCreateSuccessURL(), getCreateErrorURL(), pRequest, pResponse);
		}
	}

	@Override
	public void preCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		vlogDebug(":::*********** Pre Create User ***********:::");
		validateRegistrationInfo(pRequest);
		if (!getFormError()) {
			vlogDebug("Fields validated succesfully");
			populateUserParameters();
		} else {
			vlogDebug("Errors occured while validating fields");
		}
	}

	protected void validateRegistrationInfo(DynamoHttpServletRequest pRequest) {
		vlogDebug("first name: " + getFirstName() + " " + "last name: " + getLastName() + " " + "email: " + getEmail() + " " + "account number: "
				+ getAccountNumber() + " " + "zip code: " + getZipCode()/* + " " + "newsletter sign up: " + isNewsletterSignUp()*/);
		
		getRegistrationValidator().validateRegistrationInfo(this, getFirstName(), getLastName(), getEmail(), getPassword(), getConfirmedPassword(),
				getAccountNumber(), getZipCode(),getSupervisorName(),getSupervisorEmail(), pRequest);
		if(getCpsGlobalProperties().isEnableSupervisorEmailValidation()){
			isValidAdmin = isValidSupervisorEmail();
			vlogInfo("Valid Supervisor : {0}",isValidAdmin);
			getRegistrationValidator().validateSupervisorEmail(this,isValidAdmin, pRequest);
		}
	}
	
	/**
	 * This method is used to validate whether the supervisor email is valid or not
	 * 
	 * @param pRequest
	 * @return
	 */
	private boolean isValidSupervisorEmail(){
		try {
			String rqlQuery = "currentOrganization="+getAccountNumber();
			RepositoryView rpView = getProfileTools().getProfileRepository().getView(USER);
			RqlStatement stmt = RqlStatement.parseRqlStatement(rqlQuery);
			RepositoryItem[] orgProfileItems = stmt.executeQuery(rpView, null);			
			if(orgProfileItems!=null && orgProfileItems.length>0){
				Set roles=null;
				for (RepositoryItem orgProfileItem : orgProfileItems) {
					roles = (Set) orgProfileItem.getPropertyValue(ROLES);
					if(roles.toString().contains(ROLE_ACCOUNT_ADMIN)){
						vlogInfo("Email : {0} Roles : {1}", orgProfileItem.getPropertyValue(EMAIL),orgProfileItem.getPropertyValue(ROLES));
						if(getSupervisorEmail().equals(orgProfileItem.getPropertyValue(EMAIL))){
							isValidAdmin = true;
						}						
					}					
				}
			}			
			
		} catch (RepositoryException e) {
			vlogError("Repository Exception {0}", e.getMessage());
		}
		
		return isValidAdmin;
	}

	protected MutableRepositoryItem createProfileItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		MutableRepositoryItem userMutableItem = null;
		if (!getFormError()) {
			RepositoryItem organization = getOrganization(pRequest);
			if (organization != null) {
				boolean isHouseOrganization = (boolean) organization.getPropertyValue("isHouseOrganization");
				if (!isHouseOrganization) {
					vlogDebug("Found organization: " + organization.getItemDisplayName());
					vlogDebug("Creating USER");
					userMutableItem = super.createProfileItem(pRequest, pResponse);
					userMutableItem.setPropertyValue(PARENT_ORG, organization);
					userMutableItem.setPropertyValue(USER_TYPE, USER_TYPE_B2B_USER);
					userMutableItem = determineNewUserRole(userMutableItem);
					userMutableItem.setPropertyValue(GENERATED_PSWD, false);
					userMutableItem.setPropertyValue(FORGOT_PSWD, false);
				}else {
					createError(CPSErrorCodes.ERR_HOUSE_ORGANIZATION, JDE_ACCOUNT_NUM, pRequest);
				}
			}
		}
		return userMutableItem;
	}

	protected RepositoryItem addUser(MutableRepositoryItem pUser, MutableRepository pProfileRepository, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws RepositoryException, ServletException, IOException {
		createdUser = super.addUser(pUser, pProfileRepository, pRequest, pResponse);
		getAccountLookupSessionInfo().resetAccountLookupAttemptsCount();
		setShowCaptchaReCaptcha(false);
		vlogDebug("Successfully created user: item=" + createdUser);
		return createdUser;
	}

	protected RepositoryItem getOrganization(DynamoHttpServletRequest pRequest) {
		vlogDebug("Searching for organization");
		RepositoryItem organization = findOrganizationByAccountNumberAndZipCode(getAccountNumber(), getZipCode());
		if (organization == null) {
			getAccountLookupSessionInfo().incrementAccountLookupCount();
			vlogDebug("Organization not found, or ZIP is incorrect. Incrementing AccountLookupAttempts, now " + getAccountLookupSessionInfo().getAccountLookupCount());
			createError(CPSErrorCodes.ERR_REG_INVALID_ACCOUNT_ZIP_COMB, JDE_ACCOUNT_NUM, pRequest);
			if (getAccountLookupSessionInfo().isShowCaptcha()) {
				vlogDebug("Showing captcha");
				setShowCaptchaReCaptcha(true);
			}
		}
		return organization;
	}

	@Override
	public void postCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		vlogDebug(":::************ Post Create User ************:::");
		if (!(getFormError()) && createdUser != null) {
			sendRegisterEmailConfirmation();
		}
		// AD-HOC: Setting null so user would not log in before activation
		//getProfile().setDataSource(null);
		if (!AjaxUtils.isAjaxRequest(pRequest)) {
			checkFormRedirect(registrationSuccessURL, null, pRequest, pResponse);
		}
        super.postCreateUser(pRequest, pResponse);

        if (isPreventLoginImmediatelyUponRegistration()) { // if the flag is true, then revoke the security identity so that user cannot login unless they
            // accept the confirmation email link - SM-75

            getProfile().setDataSource(null);
            getProfile().setPropertyValue("securityStatus", 0);
        }

	}

    // protected void superPostCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
    // throws ServletException, IOException {
    // super.postCreateUser(pRequest, pResponse);
    // }

	protected void sendRegisterEmailConfirmation() {
		vlogDebug("Sending an email  with confirmation link to user");
		String outgoingActivationToken = generateActivationToken();
		getEmailSender().sendRegisterEmailConfirmation(createdUser, outgoingActivationToken);
		if (isBuyer) {
			vlogDebug("Notifiing user organization admin(s)");
			notifyAdmins((RepositoryItem) createdUser.getPropertyValue(PARENT_ORG));
		} else {
			vlogDebug("Sending an email about account activation to CPS admin");
			getEmailSender().sendAccountActivated(createdUser);
		}
	}

	/**
	 * Method send notifications about new user registration ti CustAccAdmins of
	 * given organization
	 *
	 * @param organization
	 */
	private void notifyAdmins(RepositoryItem organization) {
		Set<RepositoryItem> organizationAdmins = getAccountAdminsForOrganization(organization);
		if (organizationAdmins != null) {
			getEmailSender().sendNewUserRegistered(createdUser, organizationAdmins);
		}
	}

	/**
	 * Method to find CustAccAdmins of given organization
	 *
	 * @param organization
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	private Set<RepositoryItem> getAccountAdminsForOrganization(RepositoryItem organization) {
		Set<RepositoryItem> accountAdmins = new HashSet<RepositoryItem>();
		if (organization != null) {
			Set organizationMembers = (Set) organization.getPropertyValue(ORGANIZATION_MEMBERS_PRTY);

			for (Object memberObject : organizationMembers) {
				if (memberObject instanceof RepositoryItem) {
					RepositoryItem organizationMember = (RepositoryItem) memberObject;

					Set<RepositoryItem> orgMemberRoles = (Set) organizationMember.getPropertyValue(ROLES_PRPTY);
					if (orgMemberRoles != null) {
						for (RepositoryItem r : orgMemberRoles) {
							if (ROLE_ACCOUNT_ADMIN.equalsIgnoreCase((String) r.getPropertyValue(NAME))) {
								accountAdmins.add(organizationMember);
								break;
							}
						}
					}
				}
			}
		}

		return accountAdmins;
	}

	/**
	 * Account lookup method. Search for organization by given accoun number. If
	 * found and given ZIP code corresponds to found one - return organization.
	 *
	 * @param accountNumber
	 * @param zipCode
	 * @return
	 */
	private RepositoryItem findOrganizationByAccountNumberAndZipCode(String accountNumber, String zipCode) {
        RepositoryItem organization = null;
		try {
			Repository pRepository = getProfileTools().getProfileRepository();
            RepositoryView view = pRepository.getView(getOrganizationViewName());
            // get the orgn by its billingAddress JdeNumber. then check whether the orgn has got a 5 or 9 digit zip code and compare the passed
            // zipcode with the persisted zipcode.
            RqlStatement rqlStatement = RqlStatement.parseRqlStatement(getFindOrganizationQuery());
            RepositoryItem[] items = rqlStatement.executeQuery(view, new Object[] { getAccountNumber() });
			if (items != null && items.length > 0) {
                // one orgn is available with that JdeNumber for billingAddress. now check its zipCode
                RepositoryItem foundOrgn = items[0];
                RepositoryItem orgnBillAddr = (RepositoryItem) foundOrgn.getPropertyValue("billingAddress");
                if (orgnBillAddr != null) {
                    String billingZipCode = (String) orgnBillAddr.getPropertyValue("postalCode");
                    if (billingZipCode == null) {
                        // cannot happen. but still checking.
                        return null;
                    }
                    if (billingZipCode.equals(getZipCode())) {
                        vlogDebug("exact match for the zipcode. return the organization");
                        organization = foundOrgn;
                    } else {
                        // meaning the passed zip and the billing zip are not matching.checking here whether the billing zipcode is having pattern 5-4 and
                        // passed zip code is just 5
                        // in that case, if the first 5 numbers match, then it is okay.
                        String[] billingZipParts = billingZipCode.split(ZIP_SEPARATOR);
                        String passedZipCode = getZipCode();
                        String[] passedZipCodeParts = passedZipCode.split(ZIP_SEPARATOR);

                        if (passedZipCodeParts[0].equals(billingZipParts[0])) {
                            // meaning the user passed zipcode is just 5 digits
                            vlogDebug("passed zip code matches the first 5 digits of the billing zipcode. should be enough");
                            organization = foundOrgn;
                        }
                    }
                }

			}
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}
		return organization;
	}

	/**
	 * Method to activate user after he clicked confirmation link.
	 *
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 * @throws IOException
	 */
	public void handleActivateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleActivateUser";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				String token = getActivationToken();
				vlogDebug("Decripting activation token " + getActivationToken());
				if (token != null && !StringUtils.isEmpty(token)) {
					String decryptedEmail = "";
					try {
						getEncryptor().initialization();
						decryptedEmail = new String(getEncryptor().decrypt(token.getBytes()));
					} catch (EncryptorException e) {
						logError("Could not decrypt user activation token " + e);
					}
					vlogDebug("Searching for user with e-mail:" + decryptedEmail);
					MutableRepositoryItem user = (MutableRepositoryItem) getProfileTools().getItemFromEmail(decryptedEmail);
					String emailStatus = (String) user.getPropertyValue(EMAIL_STATUS);
					boolean isUserActivated = true;
					if (user != null && !emailStatus.equals(VALID)) {
						user.setPropertyValue(IS_ACTIVE, true);
						user.setPropertyValue(EMAIL_STATUS, VALID);
						vlogDebug("User activated");
						loginActivatedUser(pRequest, pResponse, user);
						vlogDebug("Logging user in");
					} else if (user != null && emailStatus.equals(VALID)) {
						isUserActivated = false;
						Map<String, Object> params = new HashMap<>();
						params.put("userActivated", true);
						AjaxUtils.addAjaxResponse(this, pResponse, params);						
						vlogDebug("User already activated. Redirecting to home page");
					} else {
						vlogDebug("User not found. Redirecting to home page");
					}
					if (isUserActivated) {
						checkFormRedirect(activationSuccessURL, null, pRequest, pResponse);
					}
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}
	}

	/**
	 * Method to find whether new user should be assigned BUYER role or
	 * CustAccAdmin. It depends on whether his parentOrg contains any user or
	 * not.
	 *
	 * @param userMutableItem
	 * @return
	 */
	private MutableRepositoryItem determineNewUserRole(MutableRepositoryItem userMutableItem) {
		getRole().put(ROLE_BUYER, TRUE);
		getRole().put(ROLE_APPROVER, FALSE);
		getRole().put(ROLE_FINANCE, FALSE);
		getRole().put(ROLE_ACCOUNT_ADMIN, FALSE);

		if (isBuyer = hasMembers()) {
//			logDebug("Organization has users. Setting new user BUYER Role");
//			getRole().put(ROLE_BUYER, TRUE);
			userMutableItem.setPropertyValue(APPROVERS, lookupApprover());

		} else {
			vlogDebug("Organization has got no users. Setting new user CustAccAdmin Role");
//			getRole().put(ROLE_ACCOUNT_ADMIN, TRUE);
//			getRole().put(ROLE_BUYER, TRUE);
		}

		Set<RepositoryItem> roleSet = new HashSet<RepositoryItem>();
		for (Entry<String, String> roleOption : getRole().entrySet()) {
			if (roleOption.getValue().equalsIgnoreCase(TRUE)) {
				String roleName = roleOption.getKey();
				roleSet.add(getRoleItem(roleName));
			}
		}
		if (!roleSet.isEmpty()) {
			userMutableItem.setPropertyValue(ROLES_PRPTY, roleSet);
		}
		return userMutableItem;
	}

	/**
	 * Method to set global parameters to ProfileFormHandler
	 */
	@SuppressWarnings("unchecked")
	protected void populateUserParameters() {
		getValue().put(FIRST_NAME, getFirstName());
		getValue().put(LAST_NAME, getLastName());
		getValue().put(EMAIL, getEmail().toLowerCase());
		String login = StringUtils.removeWhiteSpace(getEmail()).toLowerCase();
		getValue().put(LOGIN, login);
		getValue().put(PSWD, getPassword());
		getValue().put(SUPERVISOR_NAME, getSupervisorName());
		getValue().put(SUPERVISOR_EMAIL, getSupervisorEmail());
	}

	/**
	 * Method to log user in. (c) LoginFormHandler
	 *
	 * @param pRequest
	 * @param pResponse
	 * @param user
	 */
	private void loginActivatedUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, MutableRepositoryItem user) {
		TransactionManager tm = this.getTransactionManager();
		TransactionDemarcation td = this.getTransactionDemarcation();

		try {
			if (tm != null) {
				td.begin(tm, 3);
			}
			RepositoryItem authenticatedUser = user;
			/*
			 * RepositoryItem preLoginDataSource = this.getCurrentDataSource();
			 */
			ProfileTools ptools = this.getProfileTools();
			PropertyManager pmgr = ptools.getPropertyManager();
			String loginPropertyName = pmgr.getLoginPropertyName();
			String login = this.getStringValueProperty(loginPropertyName);
			if (authenticatedUser != null) {

				/*
				 * MutableRepository repository =
				 * this.getProfileTools().getProfileRepository(); Profile guest
				 * = this.getProfile();
				 */
				/*
				 * this.copyPropertiesOnLogin(guest, authenticatedUser);
				 * this.addPropertiesOnLogin(guest, authenticatedUser);
				 */
				this.setRepositoryId(authenticatedUser.getRepositoryId());
				pRequest.setParameter("HANDLE_LOGIN", HANDLE_SUCCESS);
				if (this.getAuthenticationMessageTrigger() != null) {
					this.getAuthenticationMessageTrigger().sendAuthenticationSuccessMessage(login, pRequest.getRemoteAddr());
				}

			} else {
				pRequest.setParameter("HANDLE_LOGIN", HANDLE_FAILURE);
				if (this.getAuthenticationMessageTrigger() != null) {
					this.getAuthenticationMessageTrigger().sendAuthenticationFailureMessage(login, pRequest.getRemoteAddr());
				}
			}

		} catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError(e);
			}
		} finally {
			try {
				if (tm != null) {
					td.end();
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
		}
		return;
	}

	/**
	 * Method to generate activation token from user email
	 *
	 * @return
	 */
	protected String generateActivationToken() {
		String activationToken = null;
		try {
			getEncryptor().initialization();
			activationToken = new String(getEncryptor().encrypt(email.toLowerCase().getBytes()));
			vlogDebug("Encrypted email: " + email + " :: " + activationToken);
			activationToken = URLEncoder.encode(activationToken, "UTF-8");
			activationToken = activationUrl + activationToken;

		} catch (EncryptorException | UnsupportedEncodingException e) {
			logError("Could create activation token" + e.getMessage());
		}
		return activationToken;
	}

	/**
	 * Method to search for approver of current user. If multiple approvers
	 * found - setting first one.
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<RepositoryItem> lookupApprover() {
		vlogDebug("Looking for approvers");
		List<RepositoryItem> approvers = new ArrayList<RepositoryItem>();

		// Get all approvers from org to display
		RepositoryItem parentOrg = (RepositoryItem) getProfile().getPropertyValue(DropletConstants.PARENT_ORG);

		if (parentOrg != null) {
			Set<RepositoryItem> members = (Set<RepositoryItem>) parentOrg.getPropertyValue(DropletConstants.MEMBERS);
			for (RepositoryItem member : members) {
				if (getApprovalManager().isApprover(member)) {
					vlogDebug("Approver found: " + member);
					approvers.add(member);
					// Setting first best approver. No need to search on.
					break;
				}
			}
		}
		return approvers;
	}

	/**
	 * Method to check if current org has members
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private boolean hasMembers() {
		boolean hasMembers = false;
		RepositoryItem parentOrg = (RepositoryItem) getProfile().getPropertyValue(DropletConstants.PARENT_ORG);
		if (parentOrg != null) {
			Set<RepositoryItem> members = (Set<RepositoryItem>) parentOrg.getPropertyValue(DropletConstants.MEMBERS);
			hasMembers = (members != null && !members.isEmpty());
		}
		return hasMembers;
	}

	private RepositoryItem getRoleItem(final String accountType) {
		RepositoryItem[] roleItem = null;
		try {
			final RepositoryItemDescriptor roleDesc = getProfileTools().getProfileRepository().getItemDescriptor(ROLE_PRPTY);
			final RepositoryView roleView = roleDesc.getRepositoryView();
			final QueryBuilder roleBuilder = roleView.getQueryBuilder();
			final QueryExpression name = roleBuilder.createPropertyQueryExpression(ROLE_NAME_PRPTY);
			final QueryExpression acctType = roleBuilder.createConstantQueryExpression(accountType);
			final Query queryItem = roleBuilder.createComparisonQuery(name, acctType, QueryBuilder.EQUALS);
			roleItem = roleView.executeQuery(queryItem);
		} catch (final RepositoryException e) {
			logError("getRoleItem(): " + e);
		} catch (final NullPointerException ne) {
			logError("getRoleItem(): " + ne);
		}
		return roleItem != null ? roleItem[0] : null;
	}

	//************************************************************************************************************
	// END CREATE USER SECTION
	//************************************************************************************************************

	/**
	 * handles the credit app form submission, validates then stores info
	 * then completed registration
	 *
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 * @throws RepositoryException
	 */
	public boolean handleCreditApp(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, RepositoryException {
		vlogDebug("handleCreditApp.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleCreditApp";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				// Validate form (have fun..)
				preCreditApp(pRequest, pResponse);

				if (!getFormError()) {
					postCreditApp(pRequest, pResponse);
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}

		vlogDebug("handleCreditApp.end");
		return checkFormRedirect(getCreditAppSuccessURL(), getCreditAppErrorURL(), pRequest, pResponse);
	}

	private void preCreditApp(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("preCreditApp.start");

		Map<String, Object> errors = new LinkedHashMap<String, Object>();

		vlogDebug("Passed values: " + getValue().toString());
		// Lets do this!!! Leeeerrrooooyyyyy Jeeeennnkkiiinsssss!!!!
		errors = ((CPSProfileTools) getProfileTools()).validateCreditApp(getValue());

		if (errors != null) {
			// We have errors, deal with them
			processCreditAppErrors(errors, pRequest);
		}

		vlogDebug("preCreditApp.end");
	}

	private void postCreditApp(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws RepositoryException {
		vlogDebug("postCreditApp.start");

		// Store entered credit app information
		MutableRepository repository = getCreditAppRepository();

		RepositoryItem creditApp = repository.createItem(getCreditAppType());

		vlogDebug("Setting values for credit app");
		((MutableRepositoryItem) creditApp).setPropertyValue(USER_ID, getProfile().getPropertyValue(ID));
		((MutableRepositoryItem) creditApp).setPropertyValue(USER_NAME, getValue().get(YOUR_NAME));
		((MutableRepositoryItem) creditApp).setPropertyValue(BUSINESS_FAX_PROPERTY, getValue().get(BUSINESS_FAX));
		((MutableRepositoryItem) creditApp).setPropertyValue(BUSINESS_NAME_PROPERTY, getValue().get(BUSINESS_NAME));
		((MutableRepositoryItem) creditApp).setPropertyValue(BUSINESS_PHONE_PROPERTY, getValue().get(BUSINESS_PHONE));
		((MutableRepositoryItem) creditApp).setPropertyValue(BUSINESS_PHONE_EXT_PROPERTY, getValue().get(BUSINESS_PHONE_EXT));
		((MutableRepositoryItem) creditApp).setPropertyValue(BUSINESS_TYPE_PROPERTY, getValue().get(BUSINESS_TYPE));
		((MutableRepositoryItem) creditApp).setPropertyValue(CONTRACTOR_LICENSE_NUMBER_PROPERTY, getValue().get(CONTRACTOR_LICENSE_NUMBER));
		((MutableRepositoryItem) creditApp).setPropertyValue(CUSTOMER_TYPE, getValue().get(CUSTOMER_TYPE));
		((MutableRepositoryItem) creditApp).setPropertyValue(EXPECTED_ANNUAL_PURCHASE_AMOUNT_PROPERTY, getValue().get(EXPECTED_ANNUAL_PURCHASE_AMOUNT));
		((MutableRepositoryItem) creditApp).setPropertyValue(ORGANIZATION_TYPE, getValue().get(ORGANIZATION_TYPE));
		((MutableRepositoryItem) creditApp).setPropertyValue(PO_PROPERTY, Boolean.parseBoolean((String) getValue().get(PO)));
		((MutableRepositoryItem) creditApp).setPropertyValue(TAX_PROPERTY, Boolean.parseBoolean((String) getValue().get(TAX)));
		((MutableRepositoryItem) creditApp).setPropertyValue(TRADE_NAME, getValue().get(TRADE_NAME));
		((MutableRepositoryItem) creditApp).setPropertyValue(YEAR_ESTABLISHED, getValue().get(YEAR_ESTABLISHED));
		((MutableRepositoryItem) creditApp).setPropertyValue(CREDIT_APP_CONTACT_PROPERTY, setCreditAppContactMap(getValue()));
		((MutableRepositoryItem) creditApp).setPropertyValue(CREDIT_APP_ORDERER_PROPERTY, setCreditAppOrdererSet(getValue()));
		((MutableRepositoryItem) creditApp).setPropertyValue(CREDIT_APP_PRODUCT_LIST_PROPERTY, setCreditAppProductListSet(getValue()));

		vlogDebug("Property values set, add creditApp Item to repository");

		repository.addItem((MutableRepositoryItem) creditApp);

		vlogDebug("postCreditApp.end");
	}

	@SuppressWarnings("rawtypes")
	private Map<String, RepositoryItem> setCreditAppContactMap(Dictionary values) {
		Map<String, RepositoryItem> contactAddresses = new HashMap<String, RepositoryItem>();
		// Holy shit, look at this monster
		vlogDebug("Generating map of contacts");
		try {
			MutableRepository repository = getCreditAppRepository();

			// These should most likely be populated, since they are required...
			if (!StringUtils.isBlank((String) values.get(BUSINESS_ADDRESS1)) || !StringUtils.isBlank((String) values.get(BUSINESS_ADDRESS2)) ||
					!StringUtils.isBlank((String) values.get(BUSINESS_CITY)) || !StringUtils.isBlank((String) values.get(BUSINESS_STATE)) ||
					!StringUtils.isBlank((String) values.get(BUSINESS_ZIP))) {
				vlogDebug("Setting business address contact info");
				MutableRepositoryItem businessAddress = repository.createItem(getCreditAppContactType());
				businessAddress.setPropertyValue(ADDRESS1, values.get(BUSINESS_ADDRESS1));
				if (!StringUtils.isBlank((String) values.get(BUSINESS_ADDRESS2))) {
					businessAddress.setPropertyValue(ADDRESS2, values.get(BUSINESS_ADDRESS2));
				}
				businessAddress.setPropertyValue(CITY, values.get(BUSINESS_CITY));
				businessAddress.setPropertyValue(STATE, values.get(BUSINESS_STATE));
				businessAddress.setPropertyValue(ZIP, values.get(BUSINESS_ZIP));
				repository.addItem(businessAddress);
				contactAddresses.put(CREDIT_APP_BUSINESS_ADDRESS, businessAddress);
			}

			// Same with these...
			if (!StringUtils.isBlank((String) values.get(BILLING_ADDRESS1)) || !StringUtils.isBlank((String) values.get(BILLING_ADDRESS2)) ||
					!StringUtils.isBlank((String) values.get(BILLING_CITY)) || !StringUtils.isBlank((String) values.get(BILLING_STATE)) ||
					!StringUtils.isBlank((String) values.get(BILLING_ZIP))) {
				vlogDebug("Setting billing address contact info");
				MutableRepositoryItem billingAddress = repository.createItem(getCreditAppContactType());
				billingAddress.setPropertyValue(ADDRESS1, values.get(BILLING_ADDRESS1));
				if (!StringUtils.isBlank((String) values.get(BILLING_ADDRESS2))) {
					billingAddress.setPropertyValue(ADDRESS2, values.get(BILLING_ADDRESS2));
				}
				billingAddress.setPropertyValue(CITY, values.get(BILLING_CITY));
				billingAddress.setPropertyValue(STATE, values.get(BILLING_STATE));
				billingAddress.setPropertyValue(ZIP, values.get(BILLING_ZIP));
				repository.addItem(billingAddress);
				contactAddresses.put(CREDIT_APP_BILLING_ADDRESS, billingAddress);
			}

			// This one also
			if (!StringUtils.isBlank((String) values.get(PRINCIPAL1_NAME)) || !StringUtils.isBlank((String) values.get(PRINCIPAL1_TITLE)) ||
					!StringUtils.isBlank((String) values.get(PRINCIPAL1_ADDRESS)) || !StringUtils.isBlank((String) values.get(PRINCIPAL1_CITY)) ||
					!StringUtils.isBlank((String) values.get(PRINCIPAL1_STATE)) || !StringUtils.isBlank((String) values.get(PRINCIPAL1_ZIP)) ||
					!StringUtils.isBlank((String) values.get(PRINCIPAL1_PHONE)) || (!StringUtils.isBlank((String) values.get(PRINCIPAL1_PHONE_EXT)) &&
					!((String) values.get(PRINCIPAL1_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				vlogDebug("Setting principal1 address contact info");
				MutableRepositoryItem principal1Address = repository.createItem(getCreditAppContactType());
				principal1Address.setPropertyValue(DISPLAY_NAME, values.get(PRINCIPAL1_NAME));
				principal1Address.setPropertyValue(TITLE, values.get(PRINCIPAL1_TITLE));
				principal1Address.setPropertyValue(ADDRESS1, values.get(PRINCIPAL1_ADDRESS));
				principal1Address.setPropertyValue(CITY, values.get(PRINCIPAL1_CITY));
				principal1Address.setPropertyValue(STATE, values.get(PRINCIPAL1_STATE));
				principal1Address.setPropertyValue(ZIP, values.get(PRINCIPAL1_ZIP));
				principal1Address.setPropertyValue(PHONE_NUMBER, values.get(PRINCIPAL1_PHONE));
				if (!StringUtils.isBlank((String) values.get(PRINCIPAL1_PHONE_EXT)) &&
						!((String) values.get(PRINCIPAL1_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					principal1Address.setPropertyValue(PHONE_NUMBER_EXT, values.get(PRINCIPAL1_PHONE_EXT));
				}
				repository.addItem(principal1Address);
				contactAddresses.put(CREDIT_APP_PRINCIPAL_1, principal1Address);
			}

			// This is probably the only one of these if statements that is necessary, since this one is not required
			if (!StringUtils.isBlank((String) values.get(PRINCIPAL2_NAME)) || !StringUtils.isBlank((String) values.get(PRINCIPAL2_TITLE)) ||
					!StringUtils.isBlank((String) values.get(PRINCIPAL2_ADDRESS)) || !StringUtils.isBlank((String) values.get(PRINCIPAL2_CITY)) ||
					!StringUtils.isBlank((String) values.get(PRINCIPAL2_STATE)) || !StringUtils.isBlank((String) values.get(PRINCIPAL2_ZIP)) ||
					!StringUtils.isBlank((String) values.get(PRINCIPAL2_PHONE)) || (!StringUtils.isBlank((String) values.get(PRINCIPAL2_PHONE_EXT)) &&
					!((String) values.get(PRINCIPAL2_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				vlogDebug("Setting principal2 address contact info");
				MutableRepositoryItem principal2Address = repository.createItem(getCreditAppContactType());
				principal2Address.setPropertyValue(DISPLAY_NAME, values.get(PRINCIPAL2_NAME));
				principal2Address.setPropertyValue(TITLE, values.get(PRINCIPAL2_TITLE));
				principal2Address.setPropertyValue(ADDRESS1, values.get(PRINCIPAL2_ADDRESS));
				principal2Address.setPropertyValue(CITY, values.get(PRINCIPAL2_CITY));
				principal2Address.setPropertyValue(STATE, values.get(PRINCIPAL2_STATE));
				principal2Address.setPropertyValue(ZIP, values.get(PRINCIPAL2_ZIP));
				principal2Address.setPropertyValue(PHONE_NUMBER, values.get(PRINCIPAL2_PHONE));
				if (!StringUtils.isBlank((String) values.get(PRINCIPAL2_PHONE_EXT)) &&
						!((String) values.get(PRINCIPAL2_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					principal2Address.setPropertyValue(PHONE_NUMBER_EXT, values.get(PRINCIPAL2_PHONE_EXT));
				}
				repository.addItem(principal2Address);
				contactAddresses.put(CREDIT_APP_PRINCIPAL_2, principal2Address);
			}

			// And back to required items
			if (!StringUtils.isBlank((String) values.get(REFERENCE1_NAME)) || !StringUtils.isBlank((String) values.get(REFERENCE1_ADDRESS)) ||
					!StringUtils.isBlank((String) values.get(REFERENCE1_CITY)) || !StringUtils.isBlank((String) values.get(REFERENCE1_STATE)) ||
					!StringUtils.isBlank((String) values.get(REFERENCE1_ZIP)) || !StringUtils.isBlank((String) values.get(REFERENCE1_PHONE)) ||
					!StringUtils.isBlank((String) values.get(REFERENCE1_FAX)) || (!StringUtils.isBlank((String) values.get(REFERENCE1_PHONE_EXT)) &&
					!((String) values.get(REFERENCE1_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				vlogDebug("Setting reference1 address contact info");
				MutableRepositoryItem reference1Address = repository.createItem(getCreditAppContactType());
				reference1Address.setPropertyValue(DISPLAY_NAME, values.get(REFERENCE1_NAME));
				reference1Address.setPropertyValue(ADDRESS1, values.get(REFERENCE1_ADDRESS));
				reference1Address.setPropertyValue(CITY, values.get(REFERENCE1_CITY));
				reference1Address.setPropertyValue(STATE, values.get(REFERENCE1_STATE));
				reference1Address.setPropertyValue(ZIP, values.get(REFERENCE1_ZIP));
				reference1Address.setPropertyValue(PHONE_NUMBER, values.get(REFERENCE1_PHONE));
				if (!StringUtils.isBlank((String) values.get(REFERENCE1_PHONE_EXT)) &&
						!((String) values.get(REFERENCE1_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					reference1Address.setPropertyValue(PHONE_NUMBER_EXT, values.get(REFERENCE1_PHONE_EXT));
				}
				reference1Address.setPropertyValue(FAX, values.get(REFERENCE1_FAX));
				repository.addItem(reference1Address);
				contactAddresses.put(CREDIT_APP_REFERENCE_1, reference1Address);
			}

			// Even the second reference is required, harsh
			if (!StringUtils.isBlank((String) values.get(REFERENCE2_NAME)) || !StringUtils.isBlank((String) values.get(REFERENCE2_ADDRESS)) ||
					!StringUtils.isBlank((String) values.get(REFERENCE2_CITY)) || !StringUtils.isBlank((String) values.get(REFERENCE2_STATE)) ||
					!StringUtils.isBlank((String) values.get(REFERENCE2_ZIP)) || !StringUtils.isBlank((String) values.get(REFERENCE2_PHONE)) ||
					!StringUtils.isBlank((String) values.get(REFERENCE2_FAX)) || (!StringUtils.isBlank((String) values.get(REFERENCE2_PHONE_EXT)) &&
					!((String) values.get(REFERENCE2_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				vlogDebug("Setting reference2 address contact info");
				MutableRepositoryItem reference2Address = repository.createItem(getCreditAppContactType());
				reference2Address.setPropertyValue(DISPLAY_NAME, values.get(REFERENCE2_NAME));
				reference2Address.setPropertyValue(ADDRESS1, values.get(REFERENCE2_ADDRESS));
				reference2Address.setPropertyValue(CITY, values.get(REFERENCE2_CITY));
				reference2Address.setPropertyValue(STATE, values.get(REFERENCE2_STATE));
				reference2Address.setPropertyValue(ZIP, values.get(REFERENCE2_ZIP));
				reference2Address.setPropertyValue(PHONE_NUMBER, values.get(REFERENCE2_PHONE));
				if (!StringUtils.isBlank((String) values.get(REFERENCE2_PHONE_EXT)) &&
						!((String) values.get(REFERENCE2_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					reference2Address.setPropertyValue(PHONE_NUMBER_EXT, values.get(REFERENCE2_PHONE_EXT));
				}
				reference2Address.setPropertyValue(FAX, values.get(REFERENCE2_FAX));
				repository.addItem(reference2Address);
				contactAddresses.put(CREDIT_APP_REFERENCE_2, reference2Address);
			}

			// Bank part, almost done
			if (!StringUtils.isBlank((String) values.get(BANK_NAME)) || !StringUtils.isBlank((String) values.get(BANK_ADDRESS)) ||
					!StringUtils.isBlank((String) values.get(BANK_CITY)) || !StringUtils.isBlank((String) values.get(BANK_STATE)) ||
					!StringUtils.isBlank((String) values.get(BANK_ZIP)) || !StringUtils.isBlank((String) values.get(BANK_PHONE)) ||
					!StringUtils.isBlank((String) values.get(BANK_ACCOUNT_NUMBER)) || (!StringUtils.isBlank((String) values.get(BANK_PHONE_EXT)) &&
					!((String) values.get(BANK_PHONE_EXT)).equalsIgnoreCase(EXT)) || !StringUtils.isBlank((String) values.get(BANK_ACCOUNT_OFFICER))) {
				vlogDebug("Setting bank address contact info");
				MutableRepositoryItem bankAddress = repository.createItem(getCreditAppContactType());
				bankAddress.setPropertyValue(DISPLAY_NAME, values.get(BANK_NAME));
				bankAddress.setPropertyValue(ADDRESS1, values.get(BANK_ADDRESS));
				bankAddress.setPropertyValue(CITY, values.get(BANK_CITY));
				bankAddress.setPropertyValue(STATE, values.get(BANK_STATE));
				bankAddress.setPropertyValue(ZIP, values.get(BANK_ZIP));
				bankAddress.setPropertyValue(PHONE_NUMBER, values.get(BANK_PHONE));
				if (!StringUtils.isBlank((String) values.get(BANK_PHONE_EXT)) &&
						!((String) values.get(BANK_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					bankAddress.setPropertyValue(PHONE_NUMBER_EXT, values.get(BANK_PHONE_EXT));
				}
				bankAddress.setPropertyValue(ACCOUNT_NUMBER, values.get(BANK_ACCOUNT_NUMBER));
				bankAddress.setPropertyValue(ACCOUNT_OFFICER, values.get(BANK_ACCOUNT_OFFICER));
				repository.addItem(bankAddress);
				contactAddresses.put(CREDIT_APP_BANK_ADDRESS, bankAddress);
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}
		// That, was, large
		vlogDebug("Map of contacts: " + contactAddresses.toString());
		return contactAddresses;
	}

	@SuppressWarnings("rawtypes")
	private Set<RepositoryItem> setCreditAppOrdererSet(Dictionary values) {
		Set<RepositoryItem> orderers = new HashSet<RepositoryItem>();
		// Create orderer contact information if it was entered
		vlogDebug("Generating set of orderers");
		try {
			MutableRepository repository = getCreditAppRepository();

			// Was anything entered for orderer 1
			if (!StringUtils.isBlank((String) values.get(ORDER1_NAME)) || !StringUtils.isBlank((String) values.get(ORDER1_TITLE)) ||
					!StringUtils.isBlank((String) values.get(ORDER1_PHONE)) || (!StringUtils.isBlank((String) values.get(ORDER1_PHONE_EXT)) &&
					!((String) values.get(ORDER1_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				// Look at that, there was
				vlogDebug("Setting order 1 contact info");
				MutableRepositoryItem orderer1 = repository.createItem(getCreditAppContactType());
				orderer1.setPropertyValue(DISPLAY_NAME, values.get(ORDER1_NAME));
				orderer1.setPropertyValue(TITLE, values.get(ORDER1_TITLE));
				orderer1.setPropertyValue(PHONE_NUMBER, values.get(ORDER1_PHONE));
				if (!StringUtils.isBlank((String) values.get(ORDER1_PHONE_EXT)) &&
						!((String) values.get(ORDER1_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					orderer1.setPropertyValue(PHONE_NUMBER_EXT, values.get(ORDER1_PHONE_EXT));
				}
				// Set data and add to repository and add to set to return
				repository.addItem(orderer1);
				orderers.add(orderer1);
			}

			//What about orderer 2?
			if (!StringUtils.isBlank((String) values.get(ORDER2_NAME)) || !StringUtils.isBlank((String) values.get(ORDER2_TITLE)) ||
					!StringUtils.isBlank((String) values.get(ORDER2_PHONE)) || (!StringUtils.isBlank((String) values.get(ORDER2_PHONE_EXT)) &&
					!((String) values.get(ORDER2_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				// Cool, do same thing as above but for new item
				vlogDebug("Setting order 2 contact info");
				MutableRepositoryItem orderer2 = repository.createItem(getCreditAppContactType());
				orderer2.setPropertyValue(DISPLAY_NAME, values.get(ORDER2_NAME));
				orderer2.setPropertyValue(TITLE, values.get(ORDER2_TITLE));
				orderer2.setPropertyValue(PHONE_NUMBER, values.get(ORDER2_PHONE));
				if (!StringUtils.isBlank((String) values.get(ORDER2_PHONE_EXT)) &&
						!((String) values.get(ORDER2_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					orderer2.setPropertyValue(PHONE_NUMBER_EXT, values.get(ORDER2_PHONE_EXT));
				}
				repository.addItem(orderer2);
				orderers.add(orderer2);
			}

			// Nothing new here
			if (!StringUtils.isBlank((String) values.get(ORDER3_NAME)) || !StringUtils.isBlank((String) values.get(ORDER3_TITLE)) ||
					!StringUtils.isBlank((String) values.get(ORDER3_PHONE)) || (!StringUtils.isBlank((String) values.get(ORDER3_PHONE_EXT)) &&
					!((String) values.get(ORDER3_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				// Surprise
				vlogDebug("Setting order 3 contact info");
				MutableRepositoryItem orderer3 = repository.createItem(getCreditAppContactType());
				orderer3.setPropertyValue(DISPLAY_NAME, values.get(ORDER3_NAME));
				orderer3.setPropertyValue(TITLE, values.get(ORDER3_TITLE));
				orderer3.setPropertyValue(PHONE_NUMBER, values.get(ORDER3_PHONE));
				if (!StringUtils.isBlank((String) values.get(ORDER3_PHONE_EXT)) &&
						!((String) values.get(ORDER3_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					orderer3.setPropertyValue(PHONE_NUMBER_EXT, values.get(ORDER3_PHONE_EXT));
				}
				repository.addItem(orderer3);
				orderers.add(orderer3);
			}

			// And, last one
			if (!StringUtils.isBlank((String) values.get(ORDER4_NAME)) || !StringUtils.isBlank((String) values.get(ORDER4_TITLE)) ||
					!StringUtils.isBlank((String) values.get(ORDER4_PHONE)) || (!StringUtils.isBlank((String) values.get(ORDER4_PHONE_EXT)) &&
					!((String) values.get(ORDER4_PHONE_EXT)).equalsIgnoreCase(EXT))) {
				// This is terrible
				vlogDebug("Setting order 4 contact info");
				MutableRepositoryItem orderer4 = repository.createItem(getCreditAppContactType());
				orderer4.setPropertyValue(DISPLAY_NAME, values.get(ORDER4_NAME));
				orderer4.setPropertyValue(TITLE, values.get(ORDER4_TITLE));
				orderer4.setPropertyValue(PHONE_NUMBER, values.get(ORDER4_PHONE));
				if (!StringUtils.isBlank((String) values.get(ORDER4_PHONE_EXT)) &&
						!((String) values.get(ORDER4_PHONE_EXT)).equalsIgnoreCase(EXT)) {
					orderer4.setPropertyValue(PHONE_NUMBER_EXT, values.get(ORDER4_PHONE_EXT));
				}
				repository.addItem(orderer4);
				orderers.add(orderer4);
			}
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		// Annnd, done.
		vlogDebug("Set of orderers: " + orderers.toString());
		return orderers;
	}

	@SuppressWarnings("rawtypes")
	private Set<String> setCreditAppProductListSet(Dictionary values) {
		Set<String> products = new HashSet<String>();
		// Add products to set that user chose.
		vlogDebug("Generating set of products selected");
		if (!StringUtils.isBlank((String) values.get(PRODUCT_LIST_PVC)) &&
				((String) values.get(PRODUCT_LIST_PVC)).equalsIgnoreCase(TRUE)) {
			products.add("PVC");
		}
		if (!StringUtils.isBlank((String) values.get(PRODUCT_LIST_PLUMBING)) &&
				((String) values.get(PRODUCT_LIST_PLUMBING)).equalsIgnoreCase(TRUE)) {
			products.add("Plumbing");
		}
		if (!StringUtils.isBlank((String) values.get(PRODUCT_LIST_HANGARS)) &&
				((String) values.get(PRODUCT_LIST_HANGARS)).equalsIgnoreCase(TRUE)) {
			products.add("Hangers");
		}
		if (!StringUtils.isBlank((String) values.get(PRODUCT_LIST_AUTOMATED)) &&
				((String) values.get(PRODUCT_LIST_AUTOMATED)).equalsIgnoreCase(TRUE)) {
			products.add("Automated");
		}
		if (!StringUtils.isBlank((String) values.get(PRODUCT_LIST_HVAC)) &&
				((String) values.get(PRODUCT_LIST_HVAC)).equalsIgnoreCase(TRUE)) {
			products.add("HVAC");
		}
		// That was exciting
		vlogDebug("Set of products: " + products.toString());
		return products;
	}

	private void processCreditAppErrors(Map<String, Object> errors, DynamoHttpServletRequest pRequest) {
		if (errors != null) {
			if (errors.size() > 1) {
				int count = 0;
				for (Entry<String, Object> error : errors.entrySet()) {
					if (count == 0) {
						createError(CPSErrorCodes.ERR_CREDIT_APP_GENERAL_ERROR, error.getValue().toString(), pRequest);
					} else {
						addFormException(new DropletFormException("", getAbsoluteName(), error.getValue().toString()));
					}
					count++;
				}
			} else {
				checkErrorMessages(errors, pRequest);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public boolean handleSelectCS(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleSelectCS";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				vlogDebug("Setting cs to what user selected");
				String csIdSelected = (String) getValue().get(SELECTED_CS);
				vlogDebug("User selected: " + csIdSelected);
				vlogDebug("User type: " + getProfile().getPropertyValue(USER_TYPE));
				vlogDebug("User parent organization: " + getProfile().getPropertyValue(PARENT_ORG));

				RepositoryItem org = ((CPSProfileTools) getProfileTools()).getParentOrganization(getProfile());

				if (org != null) {
					vlogDebug("Parent/selected organization found");
					Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) org.getPropertyValue(SECONDARY_ADDRESSES);
					if (secondaryAddresses != null) {
						vlogDebug("Secondary addresses found");
						for (Entry<String, RepositoryItem> csEntry : secondaryAddresses.entrySet()) {
							vlogDebug("secondary address value id: " + csEntry.getValue().getPropertyValue(ID));
							if (csIdSelected.equals((String) csEntry.getValue().getPropertyValue(ID))) {
								vlogDebug("CS found, set it");
								getProfile().setPropertyValue(SELECTED_CS, csEntry.getValue());
                                // since the shipping addresses being changed, clear the priceCache from the SessionBean.
                                // since the newly selected address may be having a different override in the backend.
                                getSessionBean().getProductPricesCache().clear();
								break;
							}
						}
					}
				}

				((CPSProfileTools) getProfileTools()).checkErrors(pResponse, this);

				if (getProfile().getPropertyValue(CSR_USER_ID) != null) {
					// CSR Impersonating User, log action
					getCsrLoggingManager().logAction(1, 1, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
							(String) getProfile().getPropertyValue(CSR_USER_ID), getProfile().getRepositoryId(), "CSR selected new CS for imperonated user.");
				} else if ((Integer) getProfile().getPropertyValue(USER_TYPE) == 3) {
					// Sales Rep action, log it
					getCsrLoggingManager().logAction(1, 2, (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG),
							getProfile().getRepositoryId(), null, "Sales Rep changing CS of current organization.");
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * Override change password
	 */
	public boolean handleChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleChangePassword";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				// Validate new password
				preChangePassword(pRequest, pResponse);

				if (!getFormError()) {
					getResetPasswordService().changeUserPassword(this, pRequest, pResponse);
				}

				postChangePassword(pRequest, pResponse);

				try {
					AjaxUtils.addAjaxResponse(this, pResponse, null);
				} catch (Exception e) {
					vlogError("Unable to change password.", e);
				}

			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}

		return false;
	}

	/**
	 * validates change password form
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 */
	@Override
	protected void preChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (getResetPasswordValidator().validateResetPasswordForm(this, pRequest)) {
			vlogDebug("Validation passed.");
		}
	}

	/**
	 * send email
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 */
	@Override
	protected void postChangePassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (!getFormError()) {
			checkRememberMe(pRequest, pResponse);
		}
	}

	public boolean handleNewUserResetPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleNewUserResetPassword";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				preNewUserResetPassword(pRequest, pResponse);

				if (!getFormError()) {
					getResetPasswordService().changeUserPassword(this, pRequest, pResponse);
				}

				postNewUserResetPassword(pRequest, pResponse);

				try {
					AjaxUtils.addAjaxResponse(this, pResponse, null);
				} catch (Exception e) {
					vlogError("Unable to change password.", e);
				}
			} catch (Exception e) {
				if (isLoggingError()) {
					logError(e);
				}
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}
		return false;
	}

	protected void preNewUserResetPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (getResetPasswordValidator().validateNewUserResetPasswordForm(this, pRequest)) {
			vlogDebug("Validation passed.");
		}
	}

	protected void postNewUserResetPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		if (!getFormError()) {
			checkRememberMe(pRequest, pResponse);
			try {
				getValue().put(LOGIN, getValue().get(EMAIL));
				super.handleLogin(pRequest, pResponse);
			} catch (Exception e) {
				vlogError(e, "Error");
			}
		}
	}

	public boolean handleOrderSearch(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleOrderSearch";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				getSessionBean().setOrders(null);
				getSessionBean().setOrdersTotal(0);
				RepositoryItem organization = null;
				if (null != getProfile()) {
					organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(getProfile());
				} else {
					logError("handleOrderSearch(): getProfile()==NULL ");
				}

				if (organization != null) {
					String searchNumber = (String) getValue().get(ORDER_SEARCH_TERM);
					String orderSearchCS = (String) getValue().get(ORDER_SEARCH_CS);
					String searchField = (String) getValue().get(ORDER_SEARCH_FIELD);

					Boolean isSearchButton = Boolean.valueOf((String) getValue().get(PACKING_SEARCH_IS_SEARCH_BUTTON));

					if (isSearchButton != null && isSearchButton) {
						validateSearch(searchNumber, searchField, orderSearchCS, (String) getValue().get(ORDER_SEARCH_TIME),
								pRequest.getLocale(), VSGMessageConstants.E_SEARCH_ORDER);
					}

					if (!getFormError()) {
						Integer pageSize = Integer.valueOf((String) getValue().get(ORDER_SEARCH_PAGE_SIZE));
						Integer startIndex = Integer.valueOf((String) getValue().get(ORDER_SEARCH_START_INDEX));

						String sortField = (String) getValue().get(ORDER_SEARCH_SORT_FIELD);
						String sortOption = (String) getValue().get(ORDER_SEARCH_SORT_OPTION);

						String poNumber = null;
						String orderNumber = null;
						String salesOrderNumber = null;
						if (ORDER_SEARCH_PO.equals(searchField)) {
							poNumber = searchNumber;
						} else if (ORDER_SEARCH_ORDER_ID.equals(searchField)) {
							orderNumber = searchNumber;
						} else if (ORDER_SEARCH_SALES_ID.equals(searchField)) {
							salesOrderNumber = searchNumber;
						}

						List<String> shipTo = new ArrayList<String>();
						if (StringUtils.isBlank(orderSearchCS)) {
							List<String> csShipTos=(List<String>) getProfile().getPropertyValue("csShipTos");
							shipTo.addAll(csShipTos);
						} else {
							shipTo.add(orderSearchCS);
						}
						vlogDebug("Ship tos {0}",shipTo);
						vlogDebug("Ship tos size {0}", shipTo.size());

						Calendar start = Calendar.getInstance();
						Calendar end = Calendar.getInstance();

						String customerNumber = organization.getRepositoryId();
                        String selectedBillTo = (String) getValue().get(SELECTED_BILLTO);
                        RepositoryItem profile = getProfile();
						RepositoryItem parentOrganization = (RepositoryItem)profile.getPropertyValue(CPSConstants.PARENT_ORG);
						RepositoryItem superParentOrganization = (RepositoryItem) parentOrganization.getPropertyValue(CPSConstants.PARENT_ORG);
                        if(StringUtils.isEmpty(selectedBillTo) && superParentOrganization == null){
    						createError(CPSErrorCodes.ERR_BILLING_ADDR_MISSING, null, pRequest);
    					}
                        if(StringUtils.isNotBlank(selectedBillTo)){
                            customerNumber = selectedBillTo;
                        }
						//String customerNumber = (String)organization.getPropertyValue(JDE_ACCOUNT_NUM);

						String searchTimeString = (String) getValue().get(ORDER_SEARCH_TIME);
						start = setStartCalendar(start, searchTimeString);
						try {
							Map<String, Object> result = null;
							if (StringUtils.isNotBlank(searchNumber) && StringUtils.isNotBlank(searchTimeString)) {
								result = getOrderConnectionManager().requestOrders(customerNumber, start, end,
										orderNumber, poNumber, salesOrderNumber, shipTo, pageSize, startIndex, sortField, sortOption);
							} else if (StringUtils.isBlank(searchTimeString)) {
								result = getOrderConnectionManager().requestOrders(customerNumber, null, null,
										orderNumber, poNumber, salesOrderNumber, shipTo, pageSize, startIndex, sortField, sortOption);
							} else {
								result = getOrderConnectionManager().requestOrders(customerNumber, start, end,
										orderNumber, poNumber, salesOrderNumber, shipTo, pageSize, startIndex, sortField, sortOption);
							}

							if (result != null && result.size() > 0) {
								Integer numFound = (Integer) result.get("numFound");
								List<OrderInfo> orders = (List<OrderInfo>) result.get("orders");

								for (OrderInfo orderInfo : orders) {
									Object[] objects = new Object[1];
									objects[0] = orderInfo.getOrderNumber();//"1651275";//
									Repository orderRepository = getOrderManager().getOrderTools().getOrderRepository();
									RepositoryView orderView = orderRepository.getView(ITEM_DESCRIPTOR_ORDER);
									RqlStatement rqlStatement = RqlStatement.parseRqlStatement(ORDERS_RQL_QUERY);
									RepositoryItem[] repositoryItems = rqlStatement.executeQuery(orderView, objects);

									if (repositoryItems != null && repositoryItems.length > 0) {
										for (RepositoryItem repositoryItem : repositoryItems) {
											Order order = getOrderManager().loadOrder(repositoryItem.getRepositoryId());
											CPSOrderImpl orderImpl = (CPSOrderImpl) order;
											String autoOrderId = orderImpl.getAutoOrderId();
											if (autoOrderId != null && getAutoOrderManager().loadAutoOrder(autoOrderId) != null && getAutoOrderManager().loadAutoOrder(autoOrderId).isEnabled()) {
												orderInfo.setAutoOrderId(autoOrderId);
											}
										}
									}
								}

								getSessionBean().setOrders(orders);
								getSessionBean().setOrdersTotal(numFound);
							}

						} catch (Exception e) {
							if (isLoggingError()) {
								logError(e);
							}
						}

					}

				} else {
					createError(CPSErrorCodes.ERR_ORDERS_MISSING_ORG, null, pRequest);
				}

				((CPSProfileTools) getProfileTools()).checkErrors(pResponse, this);

			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}
	
	public boolean handleInvoiceSearch(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException {

		vlogDebug("handleInvoiceSearch.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleInvoiceSearch";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				getSessionBean().setInvoices(null);
				getSessionBean().setInvoicesTotal(0);

				RepositoryItem organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(getProfile());
				if (organization != null) {
					String invoiceSearchCS = (String) getValue().get(INVOICE_SEARCH_CS);
					String selectedBillTo = (String) getValue().get(SELECTED_BILLTO);
					RepositoryItem profile = getProfile();
					RepositoryItem parentOrganization = (RepositoryItem)profile.getPropertyValue(CPSConstants.PARENT_ORG);
					RepositoryItem superParentOrganization = (RepositoryItem) parentOrganization.getPropertyValue(CPSConstants.PARENT_ORG);
                    if(StringUtils.isEmpty(selectedBillTo) && superParentOrganization == null){
						createError(CPSErrorCodes.ERR_BILLING_ADDR_MISSING, null, pRequest);
					}
					String searchNumber = (String) getValue().get(INVOICE_SEARCH_ORDER_NUMBER);
					String searchValue = (String) getValue().get(INVOICE_SEARCH_VALUE);

					Boolean isSearchButton = Boolean.valueOf((String) getValue().get(PACKING_SEARCH_IS_SEARCH_BUTTON));

					if (isSearchButton != null && isSearchButton) {
						validateSearch(searchNumber, searchValue, invoiceSearchCS, (String) getValue().get(INVOICE_SEARCH_TIME),
								pRequest.getLocale(), VSGMessageConstants.E_SEARCH_INVOICE);
					}

					if (!getFormError()) {
						Integer pageSize = Integer.valueOf((String) getValue().get(INVOICE_SEARCH_PAGE_SIZE));
						Integer startIndex = Integer.valueOf((String) getValue().get(INVOICE_SEARCH_START_INDEX));

						String sortField = (String) getValue().get(INVOICE_SEARCH_SORT_FIELD);
						String sortOption = (String) getValue().get(INVOICE_SEARCH_SORT_OPTION);

						String invoiceNumber = null;
						String orderNumber = null;
						String poNumber = null;
						if (INVOICE_NUMBER.equals(searchValue)) {
							invoiceNumber = searchNumber;
						} else if (ORDER_NUMBER.equals(searchValue)) {
							orderNumber = searchNumber;
						} else if (PO_NUMBER.equals(searchValue)) {
							poNumber = searchNumber;
						}

						List<String> shipTo = new ArrayList<String>();
						if (StringUtils.isBlank(invoiceSearchCS)) {
							List<RepositoryItem> csList = new ArrayList<RepositoryItem>();
							if (!((CPSProfileTools) getProfileTools()).initAddressesByProfile(getProfile(), csList)) {
								((CPSProfileTools) getProfileTools()).initAddressesByOrganization(getProfile(), csList);
							}
							for (RepositoryItem cs : csList) {
								shipTo.add(cs.getRepositoryId());
							}
						} else {
							shipTo.add(invoiceSearchCS);
						}

						Calendar start = Calendar.getInstance();
						Calendar end = Calendar.getInstance();

						String customerNumber = organization.getRepositoryId();
						//String customerNumber = (String)organization.getPropertyValue(JDE_ACCOUNT_NUM);

						String searchTimeString = (String) getValue().get(INVOICE_SEARCH_TIME);
						start = setStartCalendar(start, searchTimeString);
						try {
							Map<String, Object> result = null;
							if (StringUtils.isNotBlank(searchNumber) && StringUtils.isNotBlank(searchTimeString)) {
								result = getInvoiceConnectionManager().requestInvoices(customerNumber, start, end,
										invoiceNumber, orderNumber, poNumber, shipTo, selectedBillTo, pageSize, startIndex, sortField, sortOption);
							} else if (StringUtils.isBlank(searchTimeString)) {
								result = getInvoiceConnectionManager().requestInvoices(customerNumber, null, null,
										invoiceNumber, orderNumber, poNumber, shipTo, selectedBillTo, pageSize, startIndex, sortField, sortOption);
							} else {
								result = getInvoiceConnectionManager().requestInvoices(customerNumber, start, end,
										invoiceNumber, orderNumber, poNumber, shipTo, selectedBillTo, pageSize, startIndex, sortField, sortOption);
							}

							if (result != null && result.size() > 0) {
								Integer numFound = (Integer) result.get("numFound");
								List<InvoiceInfo> invoices = (List<InvoiceInfo>) result.get("invoices");

								getSessionBean().setInvoices(invoices);
								getSessionBean().setInvoicesTotal(numFound);
							}

						} catch (Exception e) {
							if (isLoggingError()) {
								logError(e);
							}
						}
						vlogDebug("Invoices: " + getSessionBean().getInvoices());
						vlogDebug("CS Search Term: " + invoiceSearchCS);

						// Filter returned results by entered criteria
						//filterInvoiceResults(invoiceSearchCS, invoiceSearchStatus);
					}

				} else {
					createError(CPSErrorCodes.ERR_INVOICES_MISSING_ORG, null, pRequest);
				}

				//int count = 0;
//		for (CPSInvoiceBean invoice : getSessionBean().getInvoices()){
//			if (invoice.getDueDate() != null){
//				count++;
//			}
//		}
//
//		getSessionBean().setInvoicesPending(count);

				((CPSProfileTools) getProfileTools()).checkErrors(pResponse, this);

			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}


	private Calendar setStartCalendar(Calendar pStart, String pSearchTimeString) {
		if (!StringUtils.isBlank(pSearchTimeString)) {
			Integer searchTime;
			try {
				searchTime = Integer.parseInt(pSearchTimeString);
			} catch (NumberFormatException e) {
				searchTime = 1;
				if (isLoggingError()) {
					logError(e);
				}
			}
			vlogDebug("search time: " + searchTime);
			switch (searchTime) {
				case 1:
					pStart.add(Calendar.DAY_OF_MONTH, -30);
					break;
				case 2:
					pStart.add(Calendar.DAY_OF_MONTH, -60);
					break;
				case 3:
					pStart.add(Calendar.DAY_OF_MONTH, -90);
					break;
				case 4:
					pStart.add(Calendar.DAY_OF_MONTH, -120);
					break;
			}
		} else {
			pStart.add(Calendar.DAY_OF_MONTH, -120);
		}
		return pStart;
	}


	private void validateSearch(String pSearchValue, String pSearchOption, String pSearchCS, String pSearchTimeString, Locale pLocale, String pMsg) {

		if ((StringUtils.isEmpty(pSearchValue) || StringUtils.isEmpty(pSearchOption)) && StringUtils.isEmpty(pSearchCS) && StringUtils.isEmpty(pSearchTimeString)) {
			CPSMessageUtils.addFormException(this, ERR_MESSAGES_REPOSITORY, pMsg, pLocale, pMsg);
		} else if ((StringUtils.isEmpty(pSearchOption) && StringUtils.isNotEmpty(pSearchValue)) 
				&& (StringUtils.isNotEmpty(pSearchCS) || StringUtils.isNotEmpty(pSearchTimeString))) {
			CPSMessageUtils.addFormException(this, ERR_MESSAGES_REPOSITORY, pMsg, pLocale, pMsg);
		}
	}

	public boolean handlePackingSearch(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException {

		vlogDebug("handlePackingSearch.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handlePackingSearch";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				getSessionBean().setPackingSlipRecords(null);
				getSessionBean().setPackingSlipsTotal(0);

				RepositoryItem organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(getProfile());

				if (organization != null) {

					String searchNumber = (String) getValue().get(PACKING_SEARCH_TERM);
					String packingSearchCS = (String) getValue().get(PACKING_SEARCH_CS);
					String searchOption = (String) getValue().get(PACKING_SEARCH_OPTION);

					Boolean isSearchButton = Boolean.valueOf((String) getValue().get(PACKING_SEARCH_IS_SEARCH_BUTTON));

					if (isSearchButton != null && isSearchButton) {
						validateSearch(searchNumber, searchOption, packingSearchCS, (String) getValue().get(PACKING_SEARCH_TIME),
								pRequest.getLocale(), VSGMessageConstants.E_SEARCH_PACKINGSLIP);
					}

					if (!getFormError()) {
						Integer pageSize = Integer.valueOf((String) getValue().get(PACKING_SEARCH_PAGE_SIZE));
						Integer startIndex = Integer.valueOf((String) getValue().get(PACKING_SEARCH_START_INDEX));

						String sortField = (String) getValue().get(PACKING_SEARCH_SORT_FIELD);
						String sortOption = (String) getValue().get(PACKING_SEARCH_SORT_OPTION);

						Calendar start = Calendar.getInstance();
						Calendar end = Calendar.getInstance();

						String customerNumber = organization.getRepositoryId();
						RepositoryItem profile = getProfile();
						RepositoryItem parentOrganization = (RepositoryItem)profile.getPropertyValue(CPSConstants.PARENT_ORG);
						RepositoryItem superParentOrganization = (RepositoryItem) parentOrganization.getPropertyValue(CPSConstants.PARENT_ORG);	
                        String selectedBillTo = (String) getValue().get(SELECTED_BILLTO);
                        if(StringUtils.isEmpty(selectedBillTo) && superParentOrganization == null){
    						createError(CPSErrorCodes.ERR_BILLING_ADDR_MISSING, null, pRequest);
    					}
                        if(StringUtils.isNotBlank(selectedBillTo)){
                            customerNumber = selectedBillTo;
                        }
						//String customerNumber = (String)organization.getPropertyValue(JDE_ACCOUNT_NUM);

						String orderNumber = null;
						String poNumber = null;

						if (ORDER_NUMBER.equals(searchOption)) {
							orderNumber = searchNumber;
						} else if (PO_NUMBER.equals(searchOption)) {
							poNumber = searchNumber;
						}

						List<String> shipTo = new ArrayList<String>();
						if (StringUtils.isBlank(packingSearchCS)) {
							List<RepositoryItem> csList = new ArrayList<RepositoryItem>();
							if (!((CPSProfileTools) getProfileTools()).initAddressesByProfile(getProfile(), csList)) {
								((CPSProfileTools) getProfileTools()).initAddressesByOrganization(getProfile(), csList);
							}
							for (RepositoryItem cs : csList) {
								shipTo.add(cs.getRepositoryId());
							}
						} else {
							shipTo.add(packingSearchCS);
						}

						String searchTimeString = (String) getValue().get(PACKING_SEARCH_TIME);
						start = setStartCalendar(start, searchTimeString);
						try {

							Map<String, Object> result =
									getPackingSlipConnectionManager().requestPackingSlips(customerNumber, start, end,
											orderNumber, poNumber, shipTo, pageSize, startIndex, sortField, sortOption);

							if (result != null && result.size() > 0) {
								Integer numFound = (Integer) result.get("numFound");
								List<PackingSlipRecord> packingSlipRecords = (List<PackingSlipRecord>) result.get("packingSlips");
								getSessionBean().setPackingSlipRecords(packingSlipRecords);
								getSessionBean().setPackingSlipsTotal(numFound);
							}

						} catch (Exception e) {
							if (isLoggingError()) {
								logError(e);
							}
						}
					}

				}

				vlogDebug("Packing slips: " + getSessionBean().getPackingSlipRecords());

				((CPSProfileTools) getProfileTools()).checkErrors(pResponse, this);

			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}

	/**
	 * Takes in a map of error messages and properties to display and highlight
	 * Use this if you have a list of errors to display
	 *
	 * @param errors
	 * @param pRequest
	 */
	public void checkErrorMessages(Map<String, Object> errors, DynamoHttpServletRequest pRequest) {
		for (Entry<String, Object> error : errors.entrySet())
			createError(error.getKey(), (String) error.getValue(), pRequest);
	}

	/**
	 * This takes a single error key and property and displays the error and adds the
	 * property to be highlighted. Use this if only adding one error.
	 *
	 * @param msg
	 * @param pty
	 * @param pRequest
	 */
	protected void createError(String msg, String pty, DynamoHttpServletRequest pRequest) {
		CPSMessageUtils.addFormException(this, ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
	}

	/**
	 * override to escape response redirect
	 *
	 * @param pErrorURL error url
	 * @param pRequest  request
	 * @param pResponse response
	 * @return 1 - error, 0 - ok
	 */
	@Override
	protected int checkFormError(String pErrorURL, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		return super.checkFormError(AjaxUtils.isAjaxRequest(pRequest) ? null : pErrorURL, pRequest, pResponse);
	}

	/**
	 * override to escape response redirect
	 *
	 * @param pSuccessURL success url
	 * @param pRequest    request
	 * @param pResponse   response
	 * @return false - redirect, true - stay
	 */
	@Override
	protected boolean checkFormSuccess(String pSuccessURL, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		return super.checkFormSuccess(AjaxUtils.isAjaxRequest(pRequest) ? null : pSuccessURL, pRequest, pResponse);
	}

	public boolean handleContactUs(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException, RepositoryException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handlePackingSearch";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				CPSProfileTools pTools = (CPSProfileTools) getProfileTools();
				Map<String, Object> errors = pTools.validateContactUs(getValue());
				if (errors != null && !errors.isEmpty()) {
					checkErrorMessages(errors, pRequest);
				} else {
					String siteId = SiteContextManager.getCurrentSiteId();
					getEmailSender().sendContactUsFormEmail(getValue(), siteId);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return checkFormRedirect(getContactusSuccessURL(), getContactusErrorURL(), pRequest, pResponse);
	}

	public boolean handleGoToRegistrationPage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException, RepositoryException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSProfileFormHandler.handleLinkToRegisterPage";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				CPSSessionBean sessionBean = getSessionBean();
				sessionBean.setPreRegisterPage(getPreRegisterPage());
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return response(pResponse);
	}

	private boolean response(DynamoHttpServletResponse pResponse) {
		AjaxUtils.addAjaxResponse(this, pResponse, null);
		return false;
	}
	
	public boolean handleUserOrderNotification(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException {
		MutableRepositoryItem userItem = null;
		Profile profile = getProfile();
		lookupUserIds();
		try {
			userItem = getProfileTools().getProfileRepository().getItemForUpdate(
					profile.getRepositoryId(), CPSConstants.USER_ITEM_DESCRIPTOR);
			userItem.setPropertyValue(NOTIFICATION_ENABLED_USERS, getUserIds());
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		return true;
		
	}
    
	
	private void lookupUserIds() {
		if (!StringUtils.isBlank(getUserId())) {
			String[] userIds = getUserId().split(",");
			for (String id : userIds) {
				RepositoryItem orderNotifiedUser = findUserItem(id);
				if (orderNotifiedUser != null) {
					getUserIds().add(orderNotifiedUser);
				}
			}
		}
		
	}
	
	public RepositoryItem findUserItem(String pId) {

		RepositoryItem notifyUserItem = null;
		try {
			notifyUserItem = getProfileTools().getProfileRepository().getItem(pId, CPSConstants.USER_ITEM_DESCRIPTOR);
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		return notifyUserItem;

	}

	public CPSSessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(CPSSessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public String getCreditAppSuccessURL() {
		return creditAppSuccessURL;
	}

	public void setCreditAppSuccessURL(String creditAppSuccessURL) {
		this.creditAppSuccessURL = creditAppSuccessURL;
	}

	public String getCreditAppErrorURL() {
		return creditAppErrorURL;
	}

	public void setCreditAppErrorURL(String creditAppErrorURL) {
		this.creditAppErrorURL = creditAppErrorURL;
	}

	public String getCreditAppFlag() {
		return creditAppFlag;
	}

	public void setCreditAppFlag(String creditAppFlag) {
		this.creditAppFlag = creditAppFlag;
	}

	public MutableRepository getCreditAppRepository() {
		return creditAppRepository;
	}

	public void setCreditAppRepository(MutableRepository creditAppRepository) {
		this.creditAppRepository = creditAppRepository;
	}

	public String getCreditAppType() {
		return creditAppType;
	}

	public void setCreditAppType(String creditAppType) {
		this.creditAppType = creditAppType;
	}

	public String getCreditAppContactType() {
		return creditAppContactType;
	}

	public void setCreditAppContactType(String creditAppContactType) {
		this.creditAppContactType = creditAppContactType;
	}

	public CommonEmailSender getEmailSender() {
		return emailSender;
	}

	public void setEmailSender(CommonEmailSender emailSender) {
		this.emailSender = emailSender;
	}

	public CPSCSRLoggingManager getCsrLoggingManager() {
		return csrLoggingManager;
	}

	public void setCsrLoggingManager(CPSCSRLoggingManager csrLoggingManager) {
		this.csrLoggingManager = csrLoggingManager;
	}

	public CPSInvoiceConnectionManager getInvoiceConnectionManager() {
		return invoiceConnectionManager;
	}

	public void setInvoiceConnectionManager(CPSInvoiceConnectionManager invoiceConnectionManager) {
		this.invoiceConnectionManager = invoiceConnectionManager;
	}

	public CPSOrderConnectionManager getOrderConnectionManager() {
		return orderConnectionManager;
	}

	public void setOrderConnectionManager(CPSOrderConnectionManager orderConnectionManager) {
		this.orderConnectionManager = orderConnectionManager;
	}

	public CPSPackingSlipConnectionManager getPackingSlipConnectionManager() {
		return packingSlipConnectionManager;
	}

	public void setPackingSlipConnectionManager(
			CPSPackingSlipConnectionManager packingSlipConnectionManager) {
		this.packingSlipConnectionManager = packingSlipConnectionManager;
	}

	public String getLoginURLParamQuestion() {
		return loginURLParamQuestion;
	}

	public void setLoginURLParamQuestion(String loginURLParamQuestion) {
		this.loginURLParamQuestion = loginURLParamQuestion;
	}

	public String getLoginURLParamAnd() {
		return loginURLParamAnd;
	}

	public void setLoginURLParamAnd(String loginURLParamAnd) {
		this.loginURLParamAnd = loginURLParamAnd;
	}

	public String getContactusSuccessURL() {
		return contactusSuccessURL;
	}

	public void setContactusSuccessURL(String contactusSuccessURL) {
		this.contactusSuccessURL = contactusSuccessURL;
	}

	public String getContactusErrorURL() {
		return contactusErrorURL;
	}

	public void setContactusErrorURL(String contactusErrorURL) {
		this.contactusErrorURL = contactusErrorURL;
	}

	public String getCreditAppURL() {
		return creditAppURL;
	}

	public void setCreditAppURL(String creditAppURL) {
		this.creditAppURL = creditAppURL;
	}

	public OrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(OrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	public AutoOrderManager getAutoOrderManager() {
		return mAutoOrderManager;
	}

	public void setAutoOrderManager(AutoOrderManager mAutoOrderManager) {
		this.mAutoOrderManager = mAutoOrderManager;
	}

	public CookieService getCookieService() {
		return mCookieService;
	}

	public void setCookieService(CookieService pCookieService) {
		mCookieService = pCookieService;
	}

	public LoginSessionInfo getLoginSessionInfo() {
		return mLoginSessionInfo;
	}

	public void setLoginSessionInfo(LoginSessionInfo pLoginSessionInfo) {
		mLoginSessionInfo = pLoginSessionInfo;
	}

	public LoginService getLoginService() {
		return mLoginService;
	}

	public void setLoginService(LoginService pLoginService) {
		mLoginService = pLoginService;
	}

	public LoginValidator getLoginValidator() {
		return mLoginValidator;
	}

	public void setLoginValidator(LoginValidator pLoginValidator) {
		mLoginValidator = pLoginValidator;
	}

	public ProfileRecentlyViewedService getRecentlyViewedService() {
		return mRecentlyViewedService;
	}

	public void setRecentlyViewedService(ProfileRecentlyViewedService pRecentlyViewedService) {
		mRecentlyViewedService = pRecentlyViewedService;
	}

	/**
	 * Sets new mOrder.
	 *
	 * @param pOrder New value of mOrder.
	 */
	public void setOrder(Order pOrder) {
		mOrder = pOrder;
	}

	/**
	 * Gets mOrder.
	 *
	 * @return Value of mOrder.
	 */
	public Order getOrder() {
		return mOrder;
	}

	public CookieManager getCookieManager() {
		return mCookieManager;
	}

	public void setCookieManager(CookieManager pCookieManager) {
		mCookieManager = pCookieManager;
	}

	public UpdateValidator getUpdateValidator() {
		return mUpdateValidator;
	}

	public void setUpdateValidator(UpdateValidator mUpdateValidator) {
		this.mUpdateValidator = mUpdateValidator;
	}

	public ProfileUpdateService getProfileUpdateService() {
		return mProfileUpdateService;
	}

	public void setProfileUpdateService(ProfileUpdateService mProfileUpdateService) {
		this.mProfileUpdateService = mProfileUpdateService;
	}

	public boolean isProfileUpdateSuccessful() {
		return profileUpdateSuccessful;
	}

	public void setProfileUpdateSuccessful(boolean profileUpdateSuccessful) {
		this.profileUpdateSuccessful = profileUpdateSuccessful;
	}

	public boolean isContactUpdateSuccessful() {
		return contactUpdateSuccessful;
	}

	public void setContactUpdateSuccessful(boolean contactUpdateSuccessful) {
		this.contactUpdateSuccessful = contactUpdateSuccessful;
	}

	public boolean isSecurityUpdateSuccessful() {
		return securityUpdateSuccessful;
	}

	public void setSecurityUpdateSuccessful(boolean securityUpdateSuccessful) {
		this.securityUpdateSuccessful = securityUpdateSuccessful;
	}

	public Vector getContactFormExceptions() {
		return contactFormExceptions;
	}

	public void setContactFormExceptions(Vector contactFormExceptions) {
		this.contactFormExceptions = contactFormExceptions;
	}

	public Vector getSecurityFormExceptions() {
		return securityFormExceptions;
	}

	public void setSecurityFormExceptions(Vector securityFormExceptions) {
		this.securityFormExceptions = securityFormExceptions;
	}

	public Vector getPersonalFormExceptions() {
		return personalFormExceptions;
	}

	public void setPersonalFormExceptions(Vector personalFormExceptions) {
		this.personalFormExceptions = personalFormExceptions;
	}

	public Properties getRedirectURLMap() {
		return mRedirectURLMap;
	}

	public void setRedirectURLMap(Properties pRedirectURLMap) {
		mRedirectURLMap = pRedirectURLMap;
	}

	public RegistrationValidator getRegistrationValidator() {
		return registrationValidator;
	}

	public void setRegistrationValidator(RegistrationValidator registrationValidator) {
		this.registrationValidator = registrationValidator;
	}

	public Map<String, String> getRole() {
		return role;
	}

	public void setRole(Map<String, String> role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmedPassword() {
		return confirmedPassword;
	}

	public void setConfirmedPassword(String confirmPassword) {
		this.confirmedPassword = confirmPassword;
	}

	public String getOrganizationID() {
		return organizationID;
	}

	public void setOrganizationID(String organizationID) {
		this.organizationID = organizationID;
	}

	public ApprovalManager getApprovalManager() {
		return approvalManager;
	}

	public void setApprovalManager(ApprovalManager approvalManager) {
		this.approvalManager = approvalManager;
	}

	public String getActivationToken() {
		return activationToken;
	}

	public void setActivationToken(String activationToken) {
		this.activationToken = activationToken;
	}

	public String getpFailureURL() {
		return pFailureURL;
	}

	public void setpFailureURL(String pFailureURL) {
		this.pFailureURL = pFailureURL;
	}

	public String getRegistrationSuccessURL() {
		return registrationSuccessURL;
	}

	public void setRegistrationSuccessURL(String registrationSuccessURL) {
		this.registrationSuccessURL = registrationSuccessURL;
	}

	public String getActivationSuccessURL() {
		return activationSuccessURL;
	}

	public void setActivationSuccessURL(String activationSuccessURL) {
		this.activationSuccessURL = activationSuccessURL;
	}

	public AESEncryptor getEncryptor() {
		return encryptor;
	}

	public void setEncryptor(AESEncryptor encryptor) {
		this.encryptor = encryptor;
	}

	public boolean isAdmin() {
		return isBuyer;
	}

	public void setAdmin(boolean isAdmin) {
		this.isBuyer = isAdmin;
	}

	public boolean isBuyer() {
		return isBuyer;
	}

	public void setBuyer(boolean isBuyer) {
		this.isBuyer = isBuyer;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getOrganizationViewName() {
		return organizationViewName;
	}

	public void setOrganizationViewName(String organizationViewName) {
		this.organizationViewName = organizationViewName;
	}

	public String getFindOrganizationQuery() {
		return findOrganizationQuery;
	}

	public void setFindOrganizationQuery(String findOrganizationQuery) {
		this.findOrganizationQuery = findOrganizationQuery;
	}

	public AccountLookupSessionInfo getAccountLookupSessionInfo() {
		return accountLookupSessionInfo;
	}

	public void setAccountLookupSessionInfo(AccountLookupSessionInfo accountLookupSessionInfo) {
		this.accountLookupSessionInfo = accountLookupSessionInfo;
	}

	public boolean isShowCaptchaReCaptcha() {
		return showCaptchaReCaptcha;
	}

	public void setShowCaptchaReCaptcha(boolean showCaptchaReCaptcha) {
		this.showCaptchaReCaptcha = showCaptchaReCaptcha;
	}

	public String getActivationUrl() {
		return activationUrl;
	}

	public RepositoryItem getCreatedUser() {
		return createdUser;
	}

	public void setCreatedUser(RepositoryItem createdUser) {
		this.createdUser = createdUser;
	}

	public void setActivationUrl(String activationUrl) {
		this.activationUrl = activationUrl;
	}

	public ResetPasswordValidator getResetPasswordValidator() {
		return mResetPasswordValidator;
	}

	public void setResetPasswordValidator(ResetPasswordValidator pResetPasswordValidator) {
		mResetPasswordValidator = pResetPasswordValidator;
	}

	public ResetPasswordService getResetPasswordService() {
		return mResetPasswordService;
	}

	public void setResetPasswordService(ResetPasswordService pResetPasswordService) {
		mResetPasswordService = pResetPasswordService;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public String getPreRegisterPage() {
		return mPreRegisterPage;
	}

	public void setPreRegisterPage(String pPreRegisterPage) {
		mPreRegisterPage = pPreRegisterPage;
	}

    /**
     * @return the preventLoginImmediatelyUponRegistration
     */
    public boolean isPreventLoginImmediatelyUponRegistration() {
        return preventLoginImmediatelyUponRegistration;
    }

    /**
     * @param preventLoginImmediatelyUponRegistration
     *            the preventLoginImmediatelyUponRegistration to set
     */
    public void setPreventLoginImmediatelyUponRegistration(boolean preventLoginImmediatelyUponRegistration) {
        this.preventLoginImmediatelyUponRegistration = preventLoginImmediatelyUponRegistration;
    }

	/**
	 * @return the supervisorName
	 */
	public String getSupervisorName() {
		return supervisorName;
	}

	/**
	 * @param supervisorName the supervisorName to set
	 */
	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	/**
	 * @return the supervisorEmail
	 */
	public String getSupervisorEmail() {
		return supervisorEmail;
	}

	/**
	 * @param supervisorEmail the supervisorEmail to set
	 */
	public void setSupervisorEmail(String supervisorEmail) {
		this.supervisorEmail = supervisorEmail;
	}

	/**
	 * @return the isValidAdmin
	 */
	public boolean isValidAdmin() {
		return isValidAdmin;
	}

	/**
	 * @param isValidAdmin the isValidAdmin to set
	 */
	public void setValidAdmin(boolean isValidAdmin) {
		this.isValidAdmin = isValidAdmin;
	}

	/**
	 * @return the cpsGlobalProperties
	 */
	public CPSGlobalProperties getCpsGlobalProperties() {
		return cpsGlobalProperties;
	}

	/**
	 * @param cpsGlobalProperties the cpsGlobalProperties to set
	 */
	public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
		this.cpsGlobalProperties = cpsGlobalProperties;
	}
	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userIds
	 */
	public Set<RepositoryItem> getUserIds() {
		return userIds;
	}

	/**
	 * @param userIds the userIds to set
	 */
	public void setUserIds(Set<RepositoryItem> userIds) {
		this.userIds = userIds;
	}

	/**
	 * @return the removeUserId
	 */
	public boolean isRemoveUserId() {
		return removeUserId;
	}

	/**
	 * @param removeUserId the removeUserId to set
	 */
	public void setRemoveUserId(boolean removeUserId) {
		this.removeUserId = removeUserId;
	}

}