package com.cps.userprofiling;

import java.util.Calendar;

/**
 * CPSInvoiceBean
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Tim Harshbarger
 *
 */
public class CPSInvoiceBean {

	/**  Amount property */
	private double mInvoiceAmount;
	
	/**  Link property */
	private String mInvoiceLink;
	
	/**  Invoice Number property */
	private String mInvoiceNumber;
	
	/**  Invoice CS property */
	private String mInvoiceCS;
	
	/**  Invoiced Date property */
	private Calendar mInvoicedDate;
	
	/**  Due Date property */
	private Calendar mDueDate;
		
	/**
	 * 
	 * @return
	 */
	public double getInvoiceAmount(){
		return mInvoiceAmount;
	}
	/**
	 * 
	 * @param pInvoiceAmount
	 */
	public void setInvoiceAmount(double pInvoiceAmount){
		mInvoiceAmount = pInvoiceAmount;
	}
	/**
	 * @return
	 */
	public String getInvoiceLink(){
		return mInvoiceLink;
	}
	/**
	 * @param pInvoiceLink
	 */
	public void setInvoiceLink(String pInvoiceLink){
		mInvoiceLink = pInvoiceLink;
	}
	/**
	 * @return
	 */
	public String getInvoiceNumber(){
		return mInvoiceNumber;
	}
	/**
	 * @param pInvoiceNumber
	 */
	public void setInvoiceNumber(String pInvoiceNumber){
		mInvoiceNumber = pInvoiceNumber;
	}
	/**
	 * @return
	 */
	public String getInvoiceCS(){
		return mInvoiceCS;
	}
	/**
	 * @param pInvoiceCS
	 */
	public void setInvoiceCS(String pInvoiceCS){
		mInvoiceCS = pInvoiceCS;
	}
	/**
	 * @return
	 */
	public Calendar getInvoicedDate(){
		return mInvoicedDate;
	}
	/**
	 * @param pInvoicedDate
	 */
	public void setInvoicedDate(Calendar pInvoicedDate){
		mInvoicedDate = pInvoicedDate;
	}
	/**
	 * @return
	 */
	public Calendar getDueDate(){
		return mDueDate;
	}
	/**
	 * @param pDueDate
	 */
	public void setDueDate(Calendar pDueDate){
		mDueDate = pDueDate;
	}

	public String toString() {
		return (new StringBuilder()).append("CPS Invoice Bean:").
				append("\n").append("Invoice Number:")
				.append(getInvoiceNumber())
				.append("\n").append("Invoice Link:")
				.append(getInvoiceLink())
				.append("\n").append("Invoice Amount:")
				.append(getInvoiceAmount())
				.append("\n").append("Invoice CS:")
				.append(getInvoiceCS())
				.append("\n").append("Invoiced Date:")
				.append(getInvoicedDate())
				.append("\n").append("Due Date:")
				.append(getDueDate())
				.toString();
	}
}
