package com.cps.userprofiling;

/**
 * 
 * @author Chris
 *
 */
public class SortableUserInfo {
	
	/**
	 * Parameter definition for id
	 */
	private String id;
	/**
	 * Parameter definition for firstName
	 */
	private String firstName;
	/**
	 * Parameter definition for lastName
	 */
	private String lastName;
	/**
	 * Parameter definition for role
	 */
	private String role;
	/**
	 * Parameter definition for email
	 */
	private String email;
	/**
	 * Parameter definition for company
	 */
	private String company;
	/**
	 * Parameter definition for spending limit
	 */
	private String spendingLimit;
	/**
	 * Parameter definition for spending limit frequency
	 */
	private String frequency;
	/**
	 * Parameter definition for status
	 */
	private String status;
	/**
	 * Parameter definition for billingAccount
	 */
	private String billingAccount;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCompany() {
		return company;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	public String getSpendingLimit() {
		return spendingLimit;
	}
	
	public void setSpendingLimit(String spendingLimit) {
		this.spendingLimit = spendingLimit;
	}
	
	public String getFrequency() {
		return frequency;
	}
	
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the billingAccount
	 */
	public String getBillingAccount() {
		return billingAccount;
	}

	/**
	 * @param billingAccount the billingAccount to set
	 */
	public void setBillingAccount(String billingAccount) {
		this.billingAccount = billingAccount;
	}

	public String toString() {
		return new StringBuilder().append("SortableUserInfo - Id: ").append(getId())
				.append(" | FirstName: ").append(getFirstName()).append(" | LastName: ").append(getLastName())
				.append(" | Role: ").append(getRole()).append(" | Email: ").append(getEmail())
				.append(" | Company: ").append(getCompany()).append(" | Spending Limit: ").append(getSpendingLimit())
				.append(" | Frequency: ").append(getFrequency()).append(" | Status: ").append(getStatus())
				.append(" | Billing Account: ").append(getBillingAccount()).toString();
	}
}