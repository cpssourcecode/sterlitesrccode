package com.cps.userprofiling;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistFormHandler;
import atg.commerce.gifts.GiftlistManager;
import atg.commerce.gifts.InvalidDateException;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.json.JSONArray;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.cps.commerce.gifts.CPSGiftlistManager;
import com.cps.userprofiling.validator.MaterialListValidator;
import com.cps.util.transaction.TransactionCommand;
import com.cps.util.transaction.TransactionProcessor;

import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.cps.util.CPSConstants.*;

/**
 * @author Dmitry Golubev
 *         based on /atg/commerce/gifts/GiftlistFormHandler;
 **/
public class MaterialListFormHandler extends GiftlistFormHandler {
	/**
	 * /cps/util/transaction/TransactionProcessor
	 */
	private TransactionProcessor mTransactionProcessor;
	/**
	 * id of created gift item
	 */
	private String mNewGiftItemId;

	private Map<String, String> addedSkuIdsWithQty = new HashMap<>();
	
	private Map<String, String> itemsToUpdate = new HashMap<>();

	private long newGiftItemsQty;
	/**
	 * new quantity of item
	 */
	private String mNewItemQty;
	/**
	 * id of item to update
	 */
	private String mItemToUpdate;

	private Object[] newGiftItemIds;

	private RepeatingRequestMonitor mRepeatingRequestMonitor;
	/**
	 * contact us validator
	 */
	private MaterialListValidator mValidator;


	//------------------------------------------------------------------------------------------------------------------

	/**
	 * adds item to newly created gift-list
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleSaveGiftlist(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
		boolean failed = true;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListFormHandler.handleSaveGiftlist";
		final GiftlistFormHandler pFormHandler = this;

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				failed = getTransactionProcessor().doInTransaction(
						new TransactionCommand() {
							@Override
							public void execute() throws Exception {
								String pProfileId = (String) getProfile().getPropertyValue("id");
								try {
									((CPSGiftlistManager) getGiftlistManager()).validatePreCreateListAddItem(pRequest, getProfile(), pFormHandler);
									if (!getFormError()) {
										String giftlistId = saveGiftlist(pProfileId);
										setGiftlistId(giftlistId);
									}
								} catch (InvalidDateException ide) {
									processException(ide, "invalidEventDate", pRequest, pResponse);
								} catch (CommerceException ce) {
									processException(ce, "errorSavingGiftlist", pRequest, pResponse);
								}
								postSaveGiftlist(pRequest, pResponse);
							}
						}
				);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return responseSaveGiftlist(failed, pRequest, pResponse);
	}


	/**
	 * adds item to newly created gift-list
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleAddItem(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
		boolean failed = true;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListFormHandler.handleAddItem";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				failed = getTransactionProcessor().doInTransaction(
						new TransactionCommand() {
							@Override
							public void execute() throws Exception {
								getValidator().checkGiftlistAccess(MaterialListFormHandler.this, pRequest);
								if (!getFormError() && getValidator().validateAddItem(MaterialListFormHandler.this, pRequest)
										&& getValidator().validateAddingItems(MaterialListFormHandler.this, pRequest, false)
										&& validateGiftlistId(pRequest, pResponse)) {
									addItemToGiftlist(pRequest, pResponse);
								}
								if (!getFormError()) {
									setNewGiftItemId(
											getGiftlistManager().getGiftlistItemId(getGiftlistId(), getCatalogRefIds()[0], getProductId(), getSiteId())
									);
								}
							}
						}
				);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return responseAddedNewItem(failed, pRequest, pResponse);
	}

	/**
	 * updates giftlist quantity
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleUpdateGiftlist(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
		boolean failed = true;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListFormHandler.handleUpdateGiftlist";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				failed = getTransactionProcessor().doInTransaction(
						new TransactionCommand() {
							@Override
							public void execute() throws Exception {
								getValidator().checkGiftlistAccess(MaterialListFormHandler.this, pRequest);
								if (!getFormError() && getValidator().validateUpdateGiftlist(MaterialListFormHandler.this, pRequest) && validateGiftlistId(pRequest, pResponse)) {
									updateGiftlistItem(pRequest, pResponse);
								}
							}
						}
				);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return responseUpdateGiftlist(failed, pRequest, pResponse);
	}

	/**
	 * update material list item quantity
	 *
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @throws ServletException - the servlet exception
	 * @throws IOException      - the I/O exception
	 */
	public void updateGiftlistItem(final DynamoHttpServletRequest pRequest,
								   final DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		final RepositoryItem giftItem = findGiftItem(getItemToUpdate(), getGiftlistId());
		if (giftItem != null) {
			int quantity = Integer.parseInt(getNewItemQty());
			if (quantity >= 0) {
				if (getGiftlistManager().getGiftlistItemQuantityDesired(getItemToUpdate()) != quantity) {
					getGiftlistManager().setGiftlistItemQuantityDesired(getGiftlistId(), getItemToUpdate(), quantity);
				}
			}
		}
	}

	/**
	 * find material list item by pGiftitemId, pGiftlistId
	 *
	 * @param pGiftitemId - item id
	 * @param pGiftlistId - giftlist id
	 * @return giftlist item
	 */
	@SuppressWarnings("unchecked")
	public RepositoryItem findGiftItem(final String pGiftitemId, final String pGiftlistId) {
		RepositoryItem giftItem = null;
		if (!StringUtils.isBlank(pGiftlistId)) {
			final Collection<RepositoryItem> giftlistItems = getGiftlistManager().getGiftlistItems(pGiftlistId);
			if (giftlistItems != null && !giftlistItems.isEmpty()) {
				for (final RepositoryItem item : giftlistItems) {
					if (item != null && pGiftitemId.equals(item.getRepositoryId())) {
						giftItem = item;
						break;
					}
				}
			}
		}
		return giftItem;
	}
	
	public boolean handleUpdateGiftlistItems(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		boolean failed = true;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListFormHandler.handleUpdateGiftlistItems";
		if(rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				failed = getTransactionProcessor().doInTransaction(new TransactionCommand(){
					@Override
					public void execute() throws Exception {
						getValidator().checkGiftlistAccess(MaterialListFormHandler.this, pRequest);
						boolean isValidGiftlistItems = getValidator().validateUpdateGiftlistItems(MaterialListFormHandler.this, pRequest);
						boolean isValidGiftlistId = validateGiftlistId(pRequest, pResponse);
						if (!getFormError() && isValidGiftlistItems && isValidGiftlistId) {
							
							Map<String, String> updatingItems = getItemsToUpdate();
							for (Map.Entry<String, String> entry : updatingItems.entrySet()) {
								RepositoryItem giftItem = findGiftItem(entry.getKey(), getGiftlistId());								
								if (giftItem != null) {
									int quantity = Integer.parseInt(entry.getValue());
									if (quantity > 0 && getGiftlistManager().getGiftlistItemQuantityDesired(entry.getKey()) != quantity) {
										getGiftlistManager().setGiftlistItemQuantityDesired(getGiftlistId(), entry.getKey(), quantity);
									}
								}
							}
							
						}
					}
				});
			}  finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return responseUpdateGiftlist(failed, pRequest, pResponse);
	}
	
	public boolean handleAddItems(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
		boolean failed = true;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListFormHandler.handleAddItems";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				failed = getTransactionProcessor().doInTransaction(
						new TransactionCommand() {
							@Override
							public void execute() throws Exception {
								getValidator().checkGiftlistAccess(MaterialListFormHandler.this, pRequest);
								if (getValidator().validateAddItem(MaterialListFormHandler.this, pRequest) 
										&& getValidator().validateAddingItems(MaterialListFormHandler.this, pRequest, true)
										&& validateGiftlistId(pRequest, pResponse)) {
									addItemsToGiftlist();
								}
							}
						}
				);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return responseAddedNewItems(failed, pRequest, pResponse);
	}

	/**
	 * adds item to newly created gift-list
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleCreateListAddItem(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
		boolean failed = true;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListFormHandler.handleCreateListAddItem";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				failed = getTransactionProcessor().doInTransaction(
						new TransactionCommand() {
							@Override
							public void execute() throws Exception {
								if (getValidator().validateCreateListAddItem(MaterialListFormHandler.this, pRequest)) {
									String newGiftListId = saveGiftlist(getProfile().getRepositoryId());
									setGiftlistId(newGiftListId);
									addItemToGiftlist(pRequest, pResponse);
									if (!getFormError()) {
										setNewGiftItemId(
												getGiftlistManager().getGiftlistItemId(getGiftlistId(), getCatalogRefIds()[0], getProductId(), getSiteId())
										);
									}
								}
							}
						}
				);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return responseAddedNewItem(failed, pRequest, pResponse);
	}

	public boolean handleCreateListAddItems(final DynamoHttpServletRequest pRequest, final DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {
		boolean failed = true;
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MaterialListFormHandler.handleCreateListAddItems";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				failed = getTransactionProcessor().doInTransaction(
						new TransactionCommand() {
							@Override
							public void execute() throws Exception {
								if (getValidator().validateCreateListAddItem(MaterialListFormHandler.this, pRequest)) {
									//preSaveGiftlist(pRequest, pResponse);
									String newGiftListId = saveGiftlist(getProfile().getRepositoryId());
									setGiftlistId(newGiftListId);
									postSaveGiftlist(pRequest, pResponse);
									addItemsToGiftlist();
								}
							}
						}
				);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return responseAddedNewItems(failed, pRequest, pResponse);
	}

	private void addItemsToGiftlist() throws CommerceException {
		long generalQty = 0L;
		List<String> gItemsids = new ArrayList<>();
		GiftlistManager giftlistManager = getGiftlistManager();
		String giftListId = getGiftlistId();
		for (Map.Entry<String, String> entry : getAddedSkuIdsWithQty().entrySet()) {
			long quantity = Long.parseLong(entry.getValue());
			if (quantity <= 0) {
				quantity = 1L;
			}
			giftlistManager.addCatalogItemToGiftlist(entry.getKey(), entry.getKey(), giftListId, getSiteId(), quantity);
			String gItemId = getGiftlistManager().getGiftlistItemId(getGiftlistId(), entry.getKey(), entry.getKey(), getSiteId());
			gItemsids.add(gItemId + "=" + quantity);
			generalQty += quantity;
		}
		if (!getFormError()) {
			setNewGiftItemsQty(generalQty);
			setNewGiftItemIds(gItemsids.toArray());
		}
	}

	private void setCustomProperties() {
		try {
			MutableRepositoryItem newGiftlist = (MutableRepositoryItem) getGiftlistManager().getGiftlist(getGiftlistId());

			String currentOrgId = "";
			RepositoryItem organization = ((CPSProfileTools) getProfileTools()).getParentOrganization(getProfile());
			if (organization != null) {
				currentOrgId = organization.getRepositoryId();
				newGiftlist.setPropertyValue(ORG, currentOrgId);
			} else {
				vlogDebug("User does not have organization to link list to. User: " + getProfile().getPropertyValue(ID));
			}

		} catch (CommerceException e) {
			vlogError(e, "Error");
		}
	}

	@Override
	public void postSaveGiftlist(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		if (!getFormError()) {
			setCustomProperties();
		}
	}


	/**
	 * puts result params to response
	 *
	 * @param pFailed   failed status
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	private boolean responseAddedNewItem(boolean pFailed,
										 DynamoHttpServletRequest pRequest,
										 DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		if (pFailed) {
			String msg = this.formatUserMessage("errorAddingToGiftlist", pRequest, pResponse);
			addFormException(new DropletException(msg));
		} else {
			params.put("giftItemId", getNewGiftItemId());
			params.put("giftListId", getGiftlistId());
		}
		return response(pResponse, params);
	}

	private boolean responseAddedNewItems(boolean pFailed,
										  DynamoHttpServletRequest pRequest,
										  DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		Map<String, Object> params = new HashMap<String, Object>();
		if (pFailed) {
			String msg = this.formatUserMessage("errorAddingToGiftlist", pRequest, pResponse);
			addFormException(new DropletException(msg));
		} else {
			if (!getFormError()) {
				params.put("giftItemIds", new JSONArray(getNewGiftItemIds()));
				params.put("itemsCount", getNewGiftItemsQty());
				params.put("giftListId", getGiftlistId());
			}
		}
		return response(pResponse, params);
	}

	/**
	 * processes responce
	 *
	 * @param pFailed   is transaction failed flag
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @return false
	 * @throws ServletException - the servlet exception
	 * @throws IOException      - the I/O exception
	 */
	private boolean responseUpdateGiftlist(boolean pFailed,
										   DynamoHttpServletRequest pRequest,
										   DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		if (pFailed) {
			String msg = this.formatUserMessage("errorUpdatingGiftlist",
					pRequest, pResponse);
			addFormException(new DropletException(msg));
		}
		return response(pResponse);
	}

	/**
	 * processes responce
	 *
	 * @param pFailed   is transaction failed flag
	 * @param pRequest  - request
	 * @param pResponse - response
	 * @return false
	 * @throws ServletException - the servlet exception
	 * @throws IOException      - the I/O exception
	 */
	private boolean responseSaveGiftlist(boolean pFailed, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (pFailed) {
			String msg = this.formatUserMessage("errorSavingGiftlist", pRequest, pResponse);
			addFormException(new DropletException(msg));
		}
		return response(pResponse);
	}

	/**
	 * @param pResponse response
	 * @return return false
	 */
	private boolean response(DynamoHttpServletResponse pResponse) {
		AjaxUtils.addAjaxResponse(this, pResponse, null);
		return false;
	}

	/**
	 * @param pResponse response
	 * @param pParams   additional params
	 * @return return false
	 */
	private boolean response(DynamoHttpServletResponse pResponse, Map<String, Object> pParams) {
		AjaxUtils.addAjaxResponse(this, pResponse, pParams);
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------

	public TransactionProcessor getTransactionProcessor() {
		return mTransactionProcessor;
	}

	public void setTransactionProcessor(TransactionProcessor pTransactionProcessor) {
		mTransactionProcessor = pTransactionProcessor;
	}

	private String getNewGiftItemId() {
		return mNewGiftItemId;
	}

	private void setNewGiftItemId(String pNewGiftItemId) {
		mNewGiftItemId = pNewGiftItemId;
	}

	public Map<String, String> getAddedSkuIdsWithQty() {
		return addedSkuIdsWithQty;
	}

	public void setAddedSkuIdsWithQty(Map<String, String> addedSkuIdsWithQty) {
		this.addedSkuIdsWithQty = addedSkuIdsWithQty;
	}

	/**
	 * @return the itemsToUpdate
	 */
	public Map<String, String> getItemsToUpdate() {
		return itemsToUpdate;
	}

	/**
	 * @param itemsToUpdate
	 *            the itemsToUpdate to set
	 */
	public void setItemsToUpdate(Map<String, String> itemsToUpdate) {
		this.itemsToUpdate = itemsToUpdate;
	}

	public long getNewGiftItemsQty() {
		return newGiftItemsQty;
	}

	public void setNewGiftItemsQty(long newGiftItemsQty) {
		this.newGiftItemsQty = newGiftItemsQty;
	}

	public Object[] getNewGiftItemIds() {
		return newGiftItemIds;
	}

	public void setNewGiftItemIds(Object[] newGiftItemIds) {
		this.newGiftItemIds = newGiftItemIds;
	}

	public String getNewItemQty() {
		return mNewItemQty;
	}

	public void setNewItemQty(String pNewItemQty) {
		this.mNewItemQty = pNewItemQty;
	}

	public String getItemToUpdate() {
		return mItemToUpdate;
	}

	public void setItemToUpdate(String pItemToUpdate) {
		this.mItemToUpdate = pItemToUpdate;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public MaterialListValidator getValidator() {
		return mValidator;
	}

	public void setValidator(MaterialListValidator pValidator) {
		mValidator = pValidator;
	}

}