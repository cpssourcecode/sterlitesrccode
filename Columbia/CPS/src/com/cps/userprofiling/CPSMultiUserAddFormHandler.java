package com.cps.userprofiling;

import static com.cps.util.CPSConstants.BILLING_ACCOUNTS;
import static com.cps.util.CPSConstants.PARENT_ORG;
import static com.cps.util.CPSConstants.ROLE_ACCOUNT_ADMIN;
import static com.cps.util.CPSConstants.ROLE_APPROVER;
import static com.cps.util.CPSConstants.ROLE_BUYER;
import static com.cps.util.CPSConstants.ROLE_FINANCE;
import static com.cps.util.CPSConstants.TRUE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.cps.commerce.approval.ApprovalManager;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.commerce.order.CPSOrderManager;
import com.cps.csr.userprofiling.CPSCSRProfileTools;
import com.cps.email.CommonEmailSender;
import com.cps.rmi.CacheInvalidationManager;
import com.cps.scheduler.manager.RegisteredUserReportManager;
import com.cps.userprofiling.service.ForgotPasswordService;
import com.cps.userprofiling.service.LoginService;
import com.cps.userprofiling.util.UserAssociatedCS;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSGlobalProperties;
import com.cps.util.CPSMessageUtils;
import com.cps.util.CPSSessionBean;
import com.cps.util.model.UserImport;

import atg.commerce.order.OrderHolder;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepositoryItem;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryItemDescriptor;
import atg.repository.RepositoryView;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.MultiUserAddFormHandler;
import atg.userprofiling.PropertyManager;
import atg.userprofiling.UserDirectoryUserListItem;
import atg.userprofiling.UserListItem;

public class CPSMultiUserAddFormHandler extends MultiUserAddFormHandler {

	private String firstName;

	private String lastName;

	private String email;

	private Map<String, String> role = new HashMap<String, String>();
	private Map<String, String> approversMap = new HashMap<String, String>();

	private String phone;

	private String company;

	private String status;

	private String spendingLimit;

	private String frequency;

	private String newUserCompletionLink;

	private boolean isImport = false;
	private boolean isAddAdmin = false;

	private static final String COUNT = "count";

	private static final String ORG_LIMIT_NONE = "nospending";

	private static final String ORG_LIMIT_HAS = "defaultspending";

	private String editSuccessURL;

	private String editErrorURL;

	private String editId;

	private String updateSpendingLimitSuccessURL;

	private String updateSpendingLimitErrorURL;

	private String addAdminSuccessURL;

	private String addAdminErrorURL;

	private String accessAccountSuccessURL;

	private String accessAccountErrorURL;

	private String orgLimit;

	private String approvers;

	private List<RepositoryItem> orderApprovers = new ArrayList<RepositoryItem>();

	private String billingAccountIds;

	private Set<RepositoryItem> billingAccounts = new HashSet<RepositoryItem>();

	private CPSSessionBean sessionBean;

	private CommonEmailSender mEmailSender;

	private boolean addAdminSuccessful;

	private LoginService loginService;

	private boolean userDeactivated = false;

	private boolean isUserActivatedFirstTime = false;

	private Map<String, String> importedPhones = new HashMap<String, String>();

	private boolean sendUserUpdateEmail = false;

	private String warningMessage;

	private ForgotPasswordService mForgotPasswordService;
	private String resendInviteSuccessURL;
	private String resendInviteErrorURL;

	private CPSOrderManager mOrderManager;

	private Repository siteRepository;
	private String defaultSiteId;
	private boolean bulkUploadFromBcc;
	private Map<String, String> bccImportedUsers = new LinkedHashMap<String, String>();
	private RegisteredUserReportManager registeredUserReportManager;
	private String userId;
	private Set<RepositoryItem> userIds = new HashSet<RepositoryItem>();
	private CPSUserProfileManager cpsUserProfileManager;
	private ApprovalManager approvalManager;
	private PropertyManager propertyManager;
	private CPSGlobalProperties cpsGlobalProperties;
	private String invalidateCacheRepositoryPath;
	private CacheInvalidationManager cacheInvalidationManager;

	public CPSOrderManager getOrderManager() {
		return mOrderManager;
	}

	public void setOrderManager(CPSOrderManager pOrderManager) {
		mOrderManager = pOrderManager;
	}

	private OrderHolder mShoppingCart;

	public OrderHolder getShoppingCart() {
		return mShoppingCart;
	}

	public void setShoppingCart(OrderHolder pShoppingCart) {
		mShoppingCart = pShoppingCart;
	}

	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	@Override
	public boolean handleCreate(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("handleCreate.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleCreate";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (StringUtils.isBlank(getCompany())) {
					RepositoryItem org = ((RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG));
					if (org != null) {
						setCompany(org.getRepositoryId());
					}
				}

				if (StringUtils.isBlank(getApprovers())) {
					StringBuilder apprFromMap = new StringBuilder("");
					for (Entry<String, String> appVal : getApproversMap().entrySet()) {
						if ("true".equals(appVal.getValue())) {
							apprFromMap.append(appVal.getKey()).append(",");
						}
					}
					setApprovers(apprFromMap.toString());
				}
				lookupApprovers();
				lookupBillingAccounts();

				vlogDebug("Values: FirstName - " + getFirstName() + " | LastName - " + getLastName() + " | Email - "
						+ getEmail() + " | Roles - " + getRole() + " | Phone - " + getPhone() + " | Status - "
						+ getStatus() + " | Spending Limit - " + getSpendingLimit() + " | Company - " + getCompany()
						+ " | Approvers - " + getOrderApprovers() + " | BillingAccounts - " + getBillingAccounts());

				setCreateSuccessURL("/account/manage-users.jsp?a=s&invite=" + isActive());

				return super.handleCreate(pRequest, pResponse);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return false;
	}

	@Override
	public void preCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("preCreateUser.start");

		if (!isAddAdmin) {
			RepositoryItem org = getParentOrganization();
			if (org != null) {
				setOrganizationId(org.getRepositoryId());
			}

			vlogDebug("isImport: " + isImport);
			if (!isImport) {
				Map<String, String> error = new HashMap<String, String>();
				String login = getEmail().toLowerCase();

				// Set user values
				getCurrentUser().getValue().put("LOGIN", login);
				getCurrentUser().getValue().put(CPSConstants.EMAIL, getEmail().toLowerCase());
				getCurrentUser().getValue().put(CPSConstants.FIRST_NAME, getFirstName());
				getCurrentUser().getValue().put(CPSConstants.LAST_NAME, getLastName());
				getCurrentUser().getValue().put(CPSConstants.ON_HOLD, false);
				getCurrentUser().getValue().put(CPSConstants.IS_ACTIVE, isActive());

				// getCurrentUser().getValue().put(CPSConstants.APPROVERS,
				// getOrderApprovers());
				if (StringUtils.isNotBlank(getSpendingLimit())) {
					getCurrentUser().getValue().put(CPSConstants.ORDER_LIMIT, getSpendingLimit());
					if (StringUtils.isNotBlank(getFrequency())) {
						getCurrentUser().getValue().put(CPSConstants.ORDER_LIMIT_FREQUENCY, getFrequency());
					}
				}

				// Check if user exists
				RepositoryItem user = null;
				if (!StringUtils.isBlank(getEmail())) {
					user = getProfileTools().getItemFromEmail(getEmail());
				}

				// User is found, throw error
				if (user != null) {
					// createError(JLGErrorConstants.errRegisterEmailExists,
					// JLGConstants.EMAIL, pRequest.getLocale());
					getCurrentUser().getValue().put(CPSConstants.LOGIN, getEmail().toLowerCase());
				}

				// if errors, add them
				if (error != null && !error.isEmpty()) {
					checkErrorMessages(error, pRequest.getLocale());
				}
			}
		}

		vlogDebug("Values: {0}", getCurrentUser().getValue());
		vlogDebug("preCreateUser.end");
	}

	private Boolean isActive() {
		return "active".equals(getStatus()) || "".equals(getStatus());
	}

	@Override
	public void preCreate(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

		if (!isImport && !isAddAdmin) {
			if (getCount() == 0) {
				try {
					setCount(Integer.parseInt(pRequest.getParameter(COUNT)));
				} catch (NumberFormatException e) {
					vlogError(e, "Error");
					setCount(1);
				}
			}

			Map<String, String> errors = validateNewUser(getFirstName(), getLastName(), getEmail(), getPhone(),
					getRole(), getSpendingLimit(), getFrequency(), null, null, false);

			if (errors.isEmpty()) {
				RepositoryItem user = null;
				if (!StringUtils.isBlank(getEmail())) {
					user = getProfileTools().getItemFromEmail(getEmail());
				}
				if (user != null) {
					// Error user exists
					if ((Boolean) user.getPropertyValue(CPSConstants.ON_HOLD)) {
						errors.put("err_email_exist_deactive", CPSConstants.EMAIL);
					} else {
						errors.put("err_email_already_exits", CPSConstants.EMAIL);
					}
				} else {
					// Check if buyer role, approvers exist
					if (getRole().get(ROLE_BUYER) != null) {
						if (getRole().get(ROLE_BUYER).equalsIgnoreCase(CPSConstants.TRUE)) {
							// User has buyer role, check if approvers exist
							/*if (!"0".equals(getSpendingLimit()) && (getOrderApprovers() == null
									|| (getOrderApprovers() != null && getOrderApprovers().isEmpty()))) {
								*/
							if(StringUtils.isNotBlank(getSpendingLimit()) ){
								if (!"0".equals(getSpendingLimit()) && (getOrderApprovers() == null
										|| getOrderApprovers().isEmpty())) {
									errors.put(CPSErrorCodes.ERR_REG_MISSING_APPROVER, CPSConstants.APPROVERS);
								}
							}
						}
					}
				}
			}

			if (errors != null && !errors.isEmpty()) {
				checkErrorMessages(errors, pRequest.getLocale());
			}
		}
	}

	private RepositoryItem getParentOrganization() {
		RepositoryItem org = null;
		if (!isBulkUploadFromBcc()) {
			org = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);
			vlogDebug("fetching organization - {0} from profile", org);
		} else {
			String orgnId = ((UserDirectoryUserListItem) getCurrentUser()).getOrganizationId();
			try {
				org = getProfileTools().getProfileRepository().getItem(orgnId, "organization");
				vlogDebug("fetching organization - {0} from currentUser", org);
			} catch (RepositoryException e) {
				vlogError("error in retrieving organization - {1}", e.getMessage());
				logError(e);
			}

		}
		return org;
	}

	@Override
	public void postCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		RepositoryItem newUser = null;
		if (isImport) {
			newUser = getProfileTools().getItemFromEmail((String) getCurrentUser().getValue().get(CPSConstants.EMAIL));
			String phoneNumber = getImportedPhones().get(getCurrentUser().getValue().get(CPSConstants.EMAIL));
			vlogDebug("Imported User, set phone from map: " + phoneNumber);
			setPhone(phoneNumber);
			try {
				if (!getInvalidateCacheRepositoryPath().isEmpty()) {
					vlogDebug("now going to invalidate the cache of - {0}", getInvalidateCacheRepositoryPath());
					getCacheInvalidationManager().invalidateCache(getInvalidateCacheRepositoryPath());
				} else {
					vlogInfo("error-repository path is missing");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			newUser = getProfileTools().getItemFromEmail(getEmail().toLowerCase());
		}

		MutableRepositoryItem userItem = null;
		String newPass = "";

		vlogDebug("User: " + String.valueOf(newUser));

		if (newUser != null) {
			try {
				userItem = getProfileTools().getProfileRepository().getItemForUpdate(newUser.getRepositoryId(),
						CPSConstants.USER_ITEM_DESCRIPTOR);
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}

			if (userItem != null) {
				RepositoryItem org = null;
				if (!isAddAdmin) {
					org = getParentOrganization();
					userItem.setPropertyValue(CPSConstants.PARENT_ORG, org);
					userItem.setPropertyValue(CPSConstants.APPROVERS, getOrderApprovers());
					userItem.setPropertyValue(CPSConstants.BILLING_ACCOUNTS, getBillingAccounts());
				}

				try {
					newPass = getProfileTools().generateNewPasswordForProfile(userItem);
				} catch (RepositoryException e) {
					vlogError(e, "Error");
				}

				Map<String, RepositoryItem> userShippingAddresses = new HashMap<String, RepositoryItem>();

				// Set users shipto addresses
				vlogDebug("Users specific ship to addresses: " + getSessionBean().getUsersShipping());
				if (getSessionBean().getUsersShipping() != null) {
					for (UserAssociatedCS cs : getSessionBean().getUsersShipping()) {
						if (cs.getIsActive()) {
							if (cs.getIsDefault()) {
								userItem.setPropertyValue(CPSConstants.SHIPPING_ADDRESS, cs.getAddressItem());
							}
							userShippingAddresses.put(cs.getId(), cs.getAddressItem());
						}
					}
				}
				MutableRepositoryItem homeAddress = (MutableRepositoryItem) userItem
						.getPropertyValue(CPSConstants.HOME_ADDRESS);
				if (homeAddress == null) {
					// Profile somehow doesn't have a home address
					createHomeAddressForUser(userItem);
					homeAddress = (MutableRepositoryItem) userItem.getPropertyValue(CPSConstants.HOME_ADDRESS);
				}
				// Add phone to home address
				homeAddress.setPropertyValue(CPSConstants.PHONE_NUMBER, getPhone());

				// Add roles
				if (!isImport && !isAddAdmin) {
					Set<RepositoryItem> roleSet = new HashSet<RepositoryItem>();
					for (Entry<String, String> roleOption : getRole().entrySet()) {
						if (roleOption.getValue().equalsIgnoreCase(CPSConstants.TRUE)) {
							String roleName = roleOption.getKey();
							roleSet.add(getRoleItem(roleName));
							if (ROLE_ACCOUNT_ADMIN.equals(roleName)) {
								roleSet.add(getRoleItem(ROLE_BUYER));
							}
						}
					}
					if (!roleSet.isEmpty()) {
						userItem.setPropertyValue(CPSConstants.ROLES_PRPTY, roleSet);
					}
				} else if (!isAddAdmin && StringUtils
						.isNotBlank((String) getCurrentUser().getValue().get(CPSConstants.SHIPPING_ADDRESS))) {
					String shipAddressId = (String) getCurrentUser().getValue().get(CPSConstants.SHIPPING_ADDRESS);
					try {
						RepositoryItem defShipAddress = (RepositoryItem) getProfileTools().getProfileRepository()
								.getItem(shipAddressId, CPSConstants.CONTACT_INFO);
						if (defShipAddress != null) {
							userItem.setPropertyValue(CPSConstants.SHIPPING_ADDRESS, defShipAddress);

							if (org != null) {
								UserAssociatedCS newcs = createAssociatedCS(defShipAddress);
								newcs.setOrganization(org);
								userShippingAddresses.put(newcs.getId(), newcs.getAddressItem());
							}
						}
					} catch (RepositoryException e) {
						vlogError(e, "Error");
					}
				} else {
					if (org != null) {
						if (!isBulkUploadFromBcc()) {
							// SM-31 - importing users from BCC
							// dont think we need to set the shippingAddress
							// explicitly as the secondaryAddress for the user.
							// if we do that, then it would be
							// the only address, the user will see.
							// but, the existing code is like that and hence
							// eliminating that logic only for BCC bulk update.
							RepositoryItem orgBillingAddress = (RepositoryItem) org
									.getPropertyValue(CPSConstants.BILLING_ADDRESS);
							vlogDebug("Org Billing set to imports shipping: " + orgBillingAddress);
							if (orgBillingAddress != null) {
								userItem.setPropertyValue(CPSConstants.SHIPPING_ADDRESS, orgBillingAddress);
							}
						}
					}
				}
				if (!isBulkUploadFromBcc()) {
					// SM-31 - importing users from BCC
					// dont think we need to set the shippingAddress explicitly
					// as the secondaryAddress for the user. if we do that, then
					// it would be the only
					// address, the user will see.
					// but, the existing code is like that and hence eliminating
					// that logic only for BCC bulk update.
					userItem.setPropertyValue(CPSConstants.SECONDARY_ADDRESSES, userShippingAddresses);
				}
				getSessionBean().setUsersShipping(null);

				try {
					getProfileTools().getProfileRepository().updateItem(homeAddress);
					getProfileTools().getProfileRepository().updateItem(userItem);

					// Notify user
					String encryptedParams = getLoginService().encryptLinkParams(userItem.getRepositoryId(),
							(String) userItem.getPropertyValue(CPSConstants.LOGIN), newPass);

					if (!isBulkUploadFromBcc()) { // send emails if it is not
													// from BCC uploads
						try {
							// logDebug("TESTING: "+newPass+" |
							// "+encryptedParams);
							getEmailSender().sendNewUserWelcomeEmail(userItem);
							Boolean isActive = (Boolean) userItem.getPropertyValue(CPSConstants.IS_ACTIVE);
							if (isActive != null && isActive) {
								getEmailSender().sendTemporaryPasswordEmail(userItem, newPass, encryptedParams);
							}

						} catch (Exception e) {
							vlogError(e, "Error");
						}
					} else {

						RepositoryItem siteItem = getSiteRepository().getItem(defaultSiteId, "siteConfiguration");
						String urlPrefix = "";
						if (siteItem != null) {
							String[] productionURLs = (String[]) siteItem.getPropertyValue("additionalProductionURLs");
							if (productionURLs != null) {
								urlPrefix = urlPrefix.concat(productionURLs[0]);
								if (!urlPrefix.startsWith("https")) {
									urlPrefix = "https://" + urlPrefix;
								}
							}
						}
						encryptedParams = urlPrefix.concat(encryptedParams);
						// getSessionBean().getBccImportedUsersMap().clear();
						getBccImportedUsers().put((String) userItem.getPropertyValue(CPSConstants.LOGIN),
								encryptedParams);
						getSessionBean().setBccImportedUsersMap(getBccImportedUsers());
					}
					if (!isBulkUploadFromBcc()) {
						// Notify account admins
						if (!isAddAdmin && org != null) {
							Set organizationAdmins = getAccountAdminsForOrganization(org);

							if (organizationAdmins != null) {
								for (Object orgAdminObj : organizationAdmins) {
									if (orgAdminObj instanceof RepositoryItem) {
										RepositoryItem organizationAdmin = (RepositoryItem) orgAdminObj;

										// CPS-1142 - Don't send admin alert to
										// the added user
										if (!organizationAdmin.equals(userItem)) {
											try {
												// account admin, deactivated
												// profile
												getEmailSender().sendAdminAddedUserEmail(organizationAdmin, userItem);
											} catch (Exception e) {
												vlogError(e, "Error");
											}
										}
									}
								}
							}
						}
					}

				} catch (RepositoryException e) {
					vlogError(e, "Error");
				}
			}
		}
	}

	private UserAssociatedCS createAssociatedCS(RepositoryItem cs) {
		UserAssociatedCS o = new UserAssociatedCS();

		o.setAddressItem(cs);
		o.setId(cs.getRepositoryId());
		o.setIsDefault(true);
		o.setIsActive(true);

		return o;
	}

	public boolean handleEdit(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("HandleEdit.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleEdit";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				lookupApprovers();
				lookupBillingAccounts();

				preEditUser(pRequest, pResponse);

				if (!getFormError()) {
					editUser(pRequest, pResponse);

					postEditUser(pRequest, pResponse);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return checkFormRedirect(getEditSuccessURL(), getEditErrorURL(), pRequest, pResponse);
	}

	public void preEditUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

		RepositoryItem user = null;
		Double csrSetSpendingLimit = null;
		if (!StringUtils.isBlank(getEmail())) {
			user = getProfileTools().getItemFromEmail(getEmail());
		}
		if (user != null) {
			csrSetSpendingLimit = (Double) user.getPropertyValue(CPSConstants.CSR_ORDER_LIMIT);
		}
		Map<String, String> errors = validateNewUser(getFirstName(), getLastName(), getEmail(), getPhone(), getRole(),
				getSpendingLimit(), getFrequency(), csrSetSpendingLimit, null, false);

		if (errors.isEmpty()) {
			if (user != null) {
				vlogDebug("Edit Id: " + getEditId() + " | Email Id: " + user.getRepositoryId());
				if (!getEditId().equalsIgnoreCase(user.getRepositoryId())) {
					// Error user exists
					if ((Boolean) user.getPropertyValue(CPSConstants.ON_HOLD)) {
						errors.put("err_email_exist_deactive", CPSConstants.EMAIL);
					} else {
						errors.put("err_email_already_exits", CPSConstants.EMAIL);
					}
				}
			}
		}

		if (errors != null && !errors.isEmpty()) {
			checkErrorMessages(errors, pRequest.getLocale());
		}
	}

	public void editUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

		MutableRepositoryItem user = null;
		try {
			user = ((CommerceProfileTools) getProfileTools()).getProfileItem(getEditId());
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}

		vlogDebug("Edit id: " + getEditId());
		vlogDebug("Editing user: " + user);

		if (user != null) {
			// Added for CPS-1130, only send email if admin updates anything
			// except default ship to
			Map<String, Object> propertiesMap = createPropertiesMap();
			sendUserUpdateEmail = checkUserPropertiesUpdates(user, propertiesMap);
			/*--------------------------------------------------------------------*/
			// Update users properties

			if (getStatus().equalsIgnoreCase(CPSConstants.ACTIVE)) {
				isUserActivatedFirstTime = ((CPSProfileTools) getProfileTools()).isUserNotActivatedBefore(user);
				user.setPropertyValue(CPSConstants.IS_ACTIVE, true);
			} else if (isAllowDeactivate(user)) {
				Boolean isActivateNow = (Boolean) user.getPropertyValue(CPSConstants.IS_ACTIVE);
				if (isActivateNow != null && isActivateNow) {
					user.setPropertyValue(CPSConstants.DEACTIVATED_DATE, Calendar.getInstance().getTime());
					user.setPropertyValue(CPSConstants.IS_ACTIVE, false);
					userDeactivated = true;
				}
			}

			user.setPropertyValue(CPSConstants.FIRST_NAME, getFirstName());
			user.setPropertyValue(CPSConstants.LAST_NAME, getLastName());
			user.setPropertyValue(CPSConstants.EMAIL, getEmail().toLowerCase());
			user.setPropertyValue(CPSConstants.APPROVERS, getOrderApprovers());
			user.setPropertyValue(CPSConstants.BILLING_ACCOUNTS, getBillingAccounts());

			Set<RepositoryItem> roleSet = new HashSet<RepositoryItem>();
			for (Entry<String, String> roleOption : getRole().entrySet()) {
				if (roleOption.getValue().equalsIgnoreCase(CPSConstants.TRUE)) {
					String roleName = roleOption.getKey();
					roleSet.add(getRoleItem(roleName));
					if (ROLE_ACCOUNT_ADMIN.equals(roleName)) {
						roleSet.add(getRoleItem(ROLE_BUYER));
					}
				}
			}

			vlogDebug("Roles: " + getRole() + " | RoleSet: " + roleSet);
			user.setPropertyValue(CPSConstants.ROLES_PRPTY, roleSet);
			if (((CPSCSRProfileTools) getProfileTools()).isCSRUser(getProfile())
					&& StringUtils.isNotBlank(getSpendingLimit())) {
				try {
					user.setPropertyValue(CPSConstants.CSR_ORDER_LIMIT, Double.parseDouble(getSpendingLimit()));
				} catch (NumberFormatException nfe) {
					vlogError(nfe, "Error");
				}
			}
			if (StringUtils.isNotBlank(getSpendingLimit())) {
				user.setPropertyValue(CPSConstants.ORDER_LIMIT, Double.parseDouble(getSpendingLimit()));
				if (StringUtils.isNotBlank(getFrequency())) {
					user.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, getFrequency());
				}
			} else {
				user.setPropertyValue(CPSConstants.ORDER_LIMIT, null);
				if (StringUtils.isBlank(getFrequency())) {
					user.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, null);
				} else {
					user.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, null);
				}

			}

			// Set users shipto addresses
			Map<String, RepositoryItem> userShippingAddresses = new HashMap<String, RepositoryItem>();
			vlogDebug("Users specific ship to addresses: " + getSessionBean().getUsersShipping());
			for (UserAssociatedCS cs : getSessionBean().getUsersShipping()) {
				if (cs.getIsActive()) {
					if (cs.getIsDefault()) {
						user.setPropertyValue(CPSConstants.SHIPPING_ADDRESS, cs.getAddressItem());
					}
					userShippingAddresses.put(cs.getId(), cs.getAddressItem());
				}
			}

			RepositoryItem parentOrg = (RepositoryItem) getProfile().getPropertyValue(PARENT_ORG);
			Set<RepositoryItem> billingAccounts = (Set<RepositoryItem>) getProfile().getPropertyValue(BILLING_ACCOUNTS);
			if (parentOrg == null && (billingAccounts.size() == 0 || billingAccounts.isEmpty())) {
				userShippingAddresses = new HashMap<String, RepositoryItem>();
				user.setPropertyValue(CPSConstants.SHIPPING_ADDRESS, null);
				user.setPropertyValue(CPSConstants.SELECTED_CS, null);
				user.setPropertyValue(CPSConstants.CART_SELECTED_CS, null);
			}
			user.setPropertyValue(CPSConstants.SECONDARY_ADDRESSES, userShippingAddresses);
			getSessionBean().setUsersShipping(null);

			MutableRepositoryItem homeAddress = (MutableRepositoryItem) user
					.getPropertyValue(CPSConstants.HOME_ADDRESS);
			if (homeAddress == null) {
				createHomeAddressForUser(user);
				homeAddress = (MutableRepositoryItem) user.getPropertyValue(CPSConstants.HOME_ADDRESS);
			}
			if (homeAddress != null) {
				homeAddress.setPropertyValue(CPSConstants.PHONE_NUMBER, getPhone());
			}

			try {
				getProfileTools().getProfileRepository().updateItem(homeAddress);
				getProfileTools().getProfileRepository().updateItem(user);
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		}
	}

	/**
	 * check if user is not last admin in organizarion
	 *
	 * @param user
	 *            - user
	 * @return true if user is not last admin in organizarion
	 */
	private boolean isAllowDeactivate(RepositoryItem user) {
		RepositoryItem organization = (RepositoryItem) user.getPropertyValue("parentOrganization");
		if (organization != null) {
			Set<RepositoryItem> allMembers = (Set<RepositoryItem>) organization.getPropertyValue("allMembers");
			if (allMembers != null && !allMembers.isEmpty()) {
				String userLogin = (String) user.getPropertyValue("login");
				for (RepositoryItem member : allMembers) {
					String memberLogin = (String) member.getPropertyValue("login");
					Boolean memberStatus = (Boolean) member.getPropertyValue(CPSConstants.IS_ACTIVE);
					if (!userLogin.equals(memberLogin)) {
						Set<RepositoryItem> roles = (Set<RepositoryItem>) member.getPropertyValue("roles");
						if (roles != null && !roles.isEmpty()) {
							for (RepositoryItem role : roles) {
								if ((role.getRepositoryId().equals("custAccAdmin")
										|| role.getRepositoryId().equals("regularAdmin"))
										&& (memberStatus != null && memberStatus)) {
									return true;
								}
							}
						}
					}
				}
			}
		}

		return false;
	}

	public boolean handleResendInvite(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleResendInvite";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				String userEmail = getEmail();
				vlogDebug("Resending invite to " + userEmail);
				RepositoryItem user = getProfileTools().getItemFromEmail(getEmail().toLowerCase());
				if (user != null) {
					getEmailSender().sendNewUserWelcomeEmail(user);
					if (!isUserActivatedFirstTime) {
						String newPass = getProfileTools().generateNewPasswordForProfile(user);
						String encryptedParams = getLoginService().encryptLinkParams(user.getRepositoryId(),
								(String) user.getPropertyValue(CPSConstants.LOGIN));
						getEmailSender().sendTemporaryPasswordEmail(user, newPass, encryptedParams);
					}
				}
			} catch (RepositoryException e) {
				vlogError("Error: ", e);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return checkFormRedirect(getResendInviteSuccessURL(), getResendInviteErrorURL(), pRequest, pResponse);
	}

	public boolean handleRemoveUsers(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		vlogDebug("handleRemoveUsers.start");
		String url = pRequest.getRequestURI();
		String successURL = null;
		String errorURL = null;
		List<String> errorUser = null;
		if (url.contains(CPSConstants.QUESTION_MARK)) {
			successURL = url + "&error=false";
			errorURL = url + "&error=true";
		} else {
			successURL = url + "?error=false";
			errorURL = url + "?error=true";
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MultiUserAddFormHandler.handleRemoveUsers";
		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (!StringUtils.isBlank(getUserId())) {
					String[] userIds = getUserId().split(",");
					errorUser = getCpsUserProfileManager().removeUserItems(userIds);
					if (!errorUser.isEmpty()) {
						successURL = errorURL;
					}
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return super.checkFormRedirect(successURL, errorURL, pRequest, pResponse);

	}

	public boolean handleConformationEmailResend(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws IOException, ServletException {
		vlogDebug("handleConformationEmailResend.start");
		String url = pRequest.getRequestURI();
		String successURL = null;
		String errorURL = null;
		if (url.contains(CPSConstants.QUESTION_MARK)) {
			successURL = url + "&error=false";
			errorURL = url + "&error=true";
		} else {
			successURL = url + "?error=false";
			errorURL = url + "?error=true";
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "MultiUserAddFormHandler.handleConformationEmailResend";
		lookupUserIds();
		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (!getUserIds().isEmpty()) {
					for (RepositoryItem user : getUserIds()) {
						String ActivationLink = getRegisteredUserReportManager().generateActivationToken(user);
						getRegisteredUserReportManager().getCommonEmailSender().sendEmailConfirmationReminder(user,
								ActivationLink);
					}
				}

			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return super.checkFormRedirect(successURL, errorURL, pRequest, pResponse);

	}

	private void lookupUserIds() {
		if (!StringUtils.isBlank(getUserId())) {
			String[] userIds = getUserId().split(",");
			for (String id : userIds) {
				RepositoryItem orderNotifiedUser = findUserItem(id);
				if (orderNotifiedUser != null) {
					getUserIds().add(orderNotifiedUser);
				}
			}
		}

	}

	public RepositoryItem findUserItem(String pId) {

		RepositoryItem notifyUserItem = null;
		try {
			notifyUserItem = getProfileTools().getProfileRepository().getItem(pId, CPSConstants.USER_ITEM_DESCRIPTOR);
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		return notifyUserItem;

	}

	private Map<String, Object> createPropertiesMap() {
		Map<String, Object> propertiesMap = new HashMap<String, Object>();

		propertiesMap.put(CPSConstants.IS_ACTIVE, getStatus());
		propertiesMap.put(CPSConstants.FIRST_NAME, getFirstName());
		propertiesMap.put(CPSConstants.LAST_NAME, getLastName());
		propertiesMap.put(CPSConstants.EMAIL, getEmail());
		propertiesMap.put(CPSConstants.APPROVERS, getOrderApprovers());
		propertiesMap.put(CPSConstants.BILLING_ACCOUNTS, getBillingAccounts());
		propertiesMap.put(CPSConstants.ROLES_PRPTY, getRole());
		propertiesMap.put(CPSConstants.ORDER_LIMIT, getSpendingLimit());
		propertiesMap.put(CPSConstants.ORDER_LIMIT_FREQUENCY, getFrequency());
		propertiesMap.put(CPSConstants.PHONE_NUMBER, getPhone());

		return propertiesMap;
	}

	private boolean checkUserPropertiesUpdates(RepositoryItem user, Map<String, Object> propertiesMap) {
		vlogDebug("Checking user property updates: " + propertiesMap);
		// This checks properties passed in map against properties assigned to
		// user and returns true
		// if a property in the map is different meaning the user was updated
		boolean userUpdated = false;

		try {
			for (Entry<String, Object> property : propertiesMap.entrySet()) {
				Object userProperty = null;
				if (!property.getKey().equalsIgnoreCase(CPSConstants.PHONE_NUMBER)) {
					userProperty = user.getPropertyValue(property.getKey());
				} else {
					// Phone number property
					userProperty = ((RepositoryItem) user.getPropertyValue(CPSConstants.HOME_ADDRESS))
							.getPropertyValue(CPSConstants.PHONE_NUMBER);
				}
				vlogDebug("Property: " + property.getKey() + " | CurrentUser - " + userProperty + " | NewValue - "
						+ property.getValue());
				if (userProperty != null) {
					if (userProperty instanceof String) {
						// Validates for First/last name, email, frequency, and
						// phone
						userUpdated = !((String) userProperty).equals(property.getValue());
						vlogDebug("Property is instanceof String, userUpdated? " + userUpdated);
					} else if (userProperty instanceof Boolean) {
						// Validates for is active
						userUpdated = (((String) property.getValue())
								.equalsIgnoreCase(CPSConstants.ACTIVE)) != (Boolean) userProperty;
						vlogDebug("Property is instanceof Boolean, userUpdated? " + userUpdated);
					} else if (userProperty instanceof Double) {
						// Validate for spending limit
						userUpdated = ((Double) userProperty) != Double.parseDouble((String) property.getValue());
						vlogDebug("Property is instanceof Double, userUpdated? " + userUpdated);
					} else {
						// Validate for role and approvers
						if (property.getKey().equalsIgnoreCase(CPSConstants.APPROVERS)) {
							vlogDebug("Checking changes in approver list");
							List<RepositoryItem> currentApprovers = (List<RepositoryItem>) userProperty;
							List<RepositoryItem> newApprovers = (List<RepositoryItem>) property.getValue();
							vlogDebug("CurrentApprovers - " + currentApprovers);
							vlogDebug("New Approvers - " + newApprovers);
							if (currentApprovers != null && newApprovers != null) {
								// Both lists populated, check size
								if (currentApprovers.size() == newApprovers.size()) {
									// Same size, check contents
									for (RepositoryItem approver : newApprovers) {
										if (!currentApprovers.contains(approver)) {
											userUpdated = true;
											break;
										}
									}
								} else {
									// different size = change
									userUpdated = true;
								}
							} else {
								if (currentApprovers != null || newApprovers != null) {
									// One list null, but another is not, that
									// means is different
									userUpdated = true;
								}
							}
							vlogDebug("UserUpdated? " + userUpdated);
						} else {
							// Not approvers, validate for role change
							vlogDebug("Checking changes in role set");
							Set<RepositoryItem> currentRoles = (Set<RepositoryItem>) userProperty;
							Set<RepositoryItem> roleSet = new HashSet<RepositoryItem>();

							if (property.getValue() instanceof Map) {
								for (Entry<String, String> roleOption : ((Map<String, String>) property.getValue())
										.entrySet()) {
									if (roleOption.getValue().equalsIgnoreCase(CPSConstants.TRUE)) {
										String roleName = roleOption.getKey();
										RepositoryItem roleItem = getRoleItem(roleName);
										if (null != roleItem) {
											roleSet.add(roleItem);
										}
									}
								}
							}

							if (property.getValue() instanceof Set) {
								Set valueSet = (Set) property.getValue();
								Iterator iter = valueSet.iterator();
								while (iter.hasNext()) {
									String roleName = null;
									Object oRoleName = iter.next();
									if (oRoleName instanceof String) {
										roleName = (String) oRoleName;
										if (StringUtils.isNotBlank(roleName)) {
											RepositoryItem roleItem = getRoleItem(roleName);
											if (null != roleItem) {
												roleSet.add(roleItem);
											}
										}
									}
								}
							}

							vlogDebug("Current Roles - " + currentRoles);
							vlogDebug("New Roles - " + roleSet);
							if (currentRoles != null && roleSet != null) {
								// Both lists populated, check size
								if (currentRoles.size() == roleSet.size()) {
									// Same size, check contents
									for (RepositoryItem role : roleSet) {
										if (!currentRoles.contains(role)) {
											userUpdated = true;
											break;
										}
									}
								} else {
									// different size = change
									userUpdated = true;
								}
							} else {
								if (currentRoles != null || roleSet != null) {
									// One list null, but another is not, that
									// means is different
									userUpdated = true;
								}
							}
							vlogDebug("UserUpdated? " + userUpdated);
						}
					}
				}
				if (userUpdated) {
					break;
				}
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}

		vlogDebug("End user property update check, was userUpdated? " + userUpdated);

		return userUpdated;
	}

	public void postEditUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		vlogDebug("postEditUser.start");

		MutableRepositoryItem user = null;
		try {
			user = ((CommerceProfileTools) getProfileTools()).getProfileItem(getEditId());
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		// Send user information updated by admin email to user
		try {
			sendUserUpdateEmail = sendUserUpdateEmail && !isUserActivatedFirstTime;
			if (sendUserUpdateEmail) {
				getEmailSender().sendUserProfileUpdatedEmail(user);
			}
		} catch (Exception e) {
			vlogError(e, "Error");
		}

		// Trigger e-mail to a disabled customer user's related account
		// administrators
		if (user != null && userDeactivated) {
			// Iterate over each of the user's account's admins and send e-mail
			// to them
			RepositoryItem org = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.PARENT_ORG);

			if (org != null) {
				Set organizationAdmins = getAccountAdminsForOrganization(org);
				vlogDebug("Org admins: " + organizationAdmins);
				if (organizationAdmins != null) {
					for (Object orgAdminObj : organizationAdmins) {
						if (orgAdminObj instanceof RepositoryItem) {
							RepositoryItem organizationAdmin = (RepositoryItem) orgAdminObj;
							// account admin, deactivated profile
							try {
								getEmailSender().sendCPSDeactivatedUserEmail(organizationAdmin, user);
							} catch (Exception e) {
								vlogError(e, "Error");
							}
						}
					}
				}
			}
		}

		if (isUserActivatedFirstTime) {
			try {
				String newPass = getProfileTools().generateNewPasswordForProfile(user);
				String encryptedParams = getLoginService().encryptLinkParams(user.getRepositoryId(),
						(String) user.getPropertyValue(CPSConstants.LOGIN));
				getEmailSender().sendTemporaryPasswordEmail(user, newPass, encryptedParams);
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		}

		vlogDebug("postEditUser.end");
	}

	public boolean handleUpdateSpendingLimit(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("HandleUpdateSpendingLimit.start");

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleUpdateSpendingLimit";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				preUpdateSpendingLimit(pRequest, pResponse);

				if (!getFormError()) {
					updateSpendingLimit(pRequest, pResponse);
				}
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return checkFormRedirect(getUpdateSpendingLimitSuccessURL(), getUpdateSpendingLimitErrorURL(), pRequest,
				pResponse);
	}

	public void preUpdateSpendingLimit(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		MutableRepositoryItem parentOrg = (MutableRepositoryItem) getProfile()
				.getPropertyValue(CPSConstants.PARENT_ORG);
		Double csrSetSpendingLimit = null;
		if (parentOrg != null) {
			csrSetSpendingLimit = (Double) parentOrg.getPropertyValue(CPSConstants.CSR_ORDER_LIMIT);
		}
		// If no spending limit radio button selected, set spending limit to 0
		if (!ORG_LIMIT_HAS.equalsIgnoreCase(getOrgLimit())) {
			setSpendingLimit("0");
		}
		Map<String, String> errors = validateSpendingLimit(getSpendingLimit(), getFrequency(), csrSetSpendingLimit);

		if (errors != null && !errors.isEmpty()) {
			checkErrorMessages(errors, pRequest.getLocale());
		}
	}

	public void updateSpendingLimit(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		MutableRepositoryItem parentOrg = (MutableRepositoryItem) getProfile()
				.getPropertyValue(CPSConstants.PARENT_ORG);

		if (parentOrg != null) {
			if (ORG_LIMIT_HAS.equalsIgnoreCase(getOrgLimit())) {
				if (StringUtils.isNotBlank(getSpendingLimit())) {
					if (((CPSCSRProfileTools) getProfileTools()).isCSRUser(getProfile())) {
						parentOrg.setPropertyValue(CPSConstants.CSR_ORDER_LIMIT,
								Double.parseDouble(getSpendingLimit()));
					}
					parentOrg.setPropertyValue(CPSConstants.ORDER_LIMIT, Double.parseDouble(getSpendingLimit()));
					if (StringUtils.isNotBlank(getFrequency())) {
						parentOrg.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, getFrequency());
					}

					updateMembersSpendingLimits(parentOrg, getSpendingLimit(), getFrequency());
				}
			} else {
				// No default limit selected, clear default
				parentOrg.setPropertyValue(CPSConstants.ORDER_LIMIT, null);
				parentOrg.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, null);
				updateMembersSpendingLimits(parentOrg, "0", null);
			}
			try {
				getProfileTools().getProfileRepository().updateItem(parentOrg);
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		}
	}

	private void updateMembersSpendingLimits(RepositoryItem parentOrg, String spendingLimit, String frequency) {
		Set<RepositoryItem> organizationMembers = (Set<RepositoryItem>) parentOrg
				.getPropertyValue(CPSConstants.ORGANIZATION_MEMBERS_PRTY);

		for (RepositoryItem organizationMember : organizationMembers) {
			MutableRepositoryItem user;
			try {
				user = getProfileTools().getProfileRepository().getItemForUpdate(organizationMember.getRepositoryId(),
						organizationMember.getItemDescriptor().getItemDescriptorName());

				if (userIsBuyer(organizationMember)) {
					if (StringUtils.isNotBlank(spendingLimit)) {
						user.setPropertyValue(CPSConstants.ORDER_LIMIT, Double.parseDouble(spendingLimit));
						if (StringUtils.isNotBlank(frequency)) {
							user.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, frequency);
						} else {
							user.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, null);
						}
					} else {
						user.setPropertyValue(CPSConstants.ORDER_LIMIT, "0");
						user.setPropertyValue(CPSConstants.ORDER_LIMIT_FREQUENCY, null);
					}

					getProfileTools().getProfileRepository().updateItem(user);
				}
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		}
	}

	public void addUsersFromImport(List<UserImport> users, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		isImport = true;

		setCount(users.size());

		for (int i = 0; i < getCount(); i++) {
			String login = users.get(i).getEmail().toLowerCase();

			((UserListItem) getUsers().get(i)).getValue().put("LOGIN", login);
			((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.EMAIL,
					users.get(i).getEmail().toLowerCase());
			((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.FIRST_NAME, users.get(i).getFirstName());
			((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.LAST_NAME, users.get(i).getLastName());
			((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.IS_ACTIVE, users.get(i).isActive());
			if (StringUtils.isNotBlank(users.get(i).getShipAddrId())) {
				((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.SHIPPING_ADDRESS,
						users.get(i).getShipAddrId());
			}
			// ((UserListItem)getUsers().get(i)).getValue().put(CPSConstants.HOME_ADDRESS+"."+CPSConstants.PHONE_NUMBER,
			// users.get(i).getPhone());
			getImportedPhones().put(users.get(i).getEmail().toLowerCase(), users.get(i).getPhone());

			String[] roles = users.get(i).getRole().split(",");
			String[] roleIds = getRoleIds(roles);
			((UserDirectoryUserListItem) getUsers().get(i)).setRoleIds(roleIds);
			((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.ON_HOLD, true);
			if (StringUtils.isNotBlank(users.get(i).getSpendingLimit())) {
				((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.ORDER_LIMIT,
						Double.parseDouble(users.get(i).getSpendingLimit()));
				if (StringUtils.isNotBlank(users.get(i).getSpendingLimitFrequency())) {
					((UserListItem) getUsers().get(i)).getValue().put(CPSConstants.ORDER_LIMIT_FREQUENCY,
							users.get(i).getSpendingLimitFrequency());
				}
			}
			if (StringUtils.isNotBlank(users.get(i).getParentOrganizationId())) {
				((UserDirectoryUserListItem) getUsers().get(i))
						.setOrganizationId(users.get(i).getParentOrganizationId());
			}
		}
		vlogDebug("populated the user list into MultiProfileFormHandler. going to invoke handleCreate ");
		super.handleCreate(pRequest, pResponse);
	}

	public boolean handleAddAdmin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("handleAddAdmin.start");

		vlogDebug("Values: " + getFirstName() + " | " + getLastName() + " | " + getEmail() + " | " + getPhone() + " | "
				+ getCompany());

		vlogDebug("Success URL: " + getAddAdminSuccessURL());

		vlogDebug("Error URL: " + getAddAdminErrorURL());

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleAddAdmin";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				preAddAdmin(pRequest, pResponse);

				if (!getFormError()) {
					addAdmin(pRequest, pResponse);
				}

				postAddAdmin(pRequest, pResponse);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return checkFormRedirect(getAddAdminSuccessURL(), getAddAdminErrorURL(), pRequest, pResponse);
	}

	private void preAddAdmin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		// Validate admin form fields
		Map<String, String> roleMap = new HashMap<String, String>();
		roleMap.put(ROLE_ACCOUNT_ADMIN, TRUE);
		roleMap.put(ROLE_BUYER, TRUE);
		roleMap.put(ROLE_APPROVER, TRUE);
		roleMap.put(ROLE_FINANCE, TRUE);

		Map<String, String> errors = validateNewUser(getFirstName(), getLastName(), getEmail(), getPhone(), roleMap,
				"0", null, null, null, false);

		if (errors != null && !errors.isEmpty()) {
			checkErrorMessages(errors, pRequest.getLocale());
		}
	}

	private void addAdmin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		// Create admin profile and assign to company

		setCount(1);

		isAddAdmin = true;

		String login = getEmail().toLowerCase();

		((UserListItem) getUsers().get(0)).getValue().put("LOGIN", login);
		((UserListItem) getUsers().get(0)).getValue().put(CPSConstants.EMAIL, getEmail().toLowerCase());
		((UserListItem) getUsers().get(0)).getValue().put(CPSConstants.FIRST_NAME, getFirstName());
		((UserListItem) getUsers().get(0)).getValue().put(CPSConstants.LAST_NAME, getLastName());
		String[] roles = new String[4];
		roles[0] = ROLE_ACCOUNT_ADMIN;
		roles[1] = ROLE_BUYER;
		roles[2] = ROLE_APPROVER;
		roles[3] = ROLE_FINANCE;
		String[] roleIds = getRoleIds(roles);
		((UserDirectoryUserListItem) getUsers().get(0)).setRoleIds(roleIds);
		((UserListItem) getUsers().get(0)).getValue().put(CPSConstants.ON_HOLD, false);
		((UserListItem) getUsers().get(0)).getValue().put(CPSConstants.PARENT_ORG, findOrg(getCompany()));
		((UserListItem) getUsers().get(0)).getValue().put(CPSConstants.IS_ACTIVE, true);

		super.handleCreate(pRequest, pResponse);
	}

	private void postAddAdmin(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		setAddAdminSuccessful(!getFormError());
		RepositoryItem newUser = getProfileTools().getItemFromEmail(getEmail().toLowerCase());
		MutableRepositoryItem userItem = null;
		if (!getFormError()) {
			if (newUser != null) {
				try {
					userItem = getProfileTools().getProfileRepository().getItemForUpdate(newUser.getRepositoryId(),
							CPSConstants.USER_ITEM_DESCRIPTOR);
				} catch (RepositoryException e) {
					vlogError(e, "Error");
				}
				RepositoryItem org = findOrg(getCompany());

				if (userItem != null) {
					userItem.setPropertyValue(CPSConstants.PARENT_ORG, org);

					try {
						getProfileTools().getProfileRepository().updateItem(userItem);
					} catch (RepositoryException e) {
						vlogError(e, "Error");
					}
				}
			}
		}
	}

	public boolean handleAccessAccount(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug("Handle Access Account, accessing account - " + getCompany());
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleAccessAccount";

		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollback = false;

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				RepositoryItem org = findOrg(getCompany());

				if (org != null) {
					String dontShowMessage = getWarningMessage();
					vlogDebug("Stop showing message? " + dontShowMessage);
					if (CPSConstants.TRUE.equalsIgnoreCase(dontShowMessage)) {
						getProfile().setPropertyValue(CPSConstants.CSR_ACCESS_MESSAGE, false);
					}

					getProfile().setPropertyValue(CPSConstants.CSR_SELECTED_ORG, org);
					getProfile().setPropertyValue(CPSConstants.PARENT_ORG, org);

					Map<String, RepositoryItem> secondaryAddresses = (Map<String, RepositoryItem>) org
							.getPropertyValue(CPSConstants.SECONDARY_ADDRESSES);
					if (secondaryAddresses != null) {
						for (Entry<String, RepositoryItem> csEntry : secondaryAddresses.entrySet()) {
							getProfile().setPropertyValue(CPSConstants.SELECTED_CS, csEntry.getValue());
							break;
						}
					}

					// getProfile().setPropertyValue(CPSConstants.SELECTED_CS,
					// null);

					// get current order before resetting parent org to see if
					// there are any items
					// if products in cart then items should be merged into
					// current order when parent org is set
					CPSOrderImpl currentOrder = (CPSOrderImpl) getShoppingCart().getCurrent();
					if (currentOrder != null) {
						synchronized (currentOrder) {
							boolean mergeOrders = ((CPSCSRProfileTools) getProfileTools()).isMergeCSROrders();
							if (mergeOrders) {
								mergeOrders = currentOrder.getCommerceItemCount() > 0;
							}
							vlogDebug("mergeOrders - " + mergeOrders);

							// do not merge just handle current order
							if (!mergeOrders) {
								currentOrder.setOrganizationId(org.getRepositoryId());
							} else {
								currentOrder.setUseProfileOrganization(true);
							}
							getOrderManager().updateOrder(currentOrder);
						}
					}
				} else {
					createError("errorGeneric", "errorGeneric", pRequest.getLocale());
				}
			} catch (Exception e) {
				vlogError(e, "Error");
				rollback = true;
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
				if (tm != null) {
					try {
						td.end(rollback);
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError(e);
						}
					}
				}
			}
		}

		return checkFormRedirect(getAccessAccountSuccessURL(), getAccessAccountErrorURL(), pRequest, pResponse);
	}

	public boolean handleStopAccess(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleStopAccess";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				getProfile().setPropertyValue(CPSConstants.CSR_SELECTED_ORG, null);
				getProfile().setPropertyValue(CPSConstants.PARENT_ORG, null);
			} finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return checkFormRedirect(null, null, pRequest, pResponse);
	}

	public RepositoryItem findOrg(String pId) {

		RepositoryItem org = null;
		try {
			org = getProfileTools().getProfileRepository().getItem(pId, "organization");
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		return org;

	}

	private RepositoryItem getRoleItem(final String accountType) {
		RepositoryItem[] roleItem = null;
		try {
			final RepositoryItemDescriptor roleDesc = getProfileTools().getProfileRepository()
					.getItemDescriptor(CPSConstants.ROLE_PRPTY);
			final RepositoryView roleView = roleDesc.getRepositoryView();
			final QueryBuilder roleBuilder = roleView.getQueryBuilder();
			final QueryExpression name = roleBuilder.createPropertyQueryExpression(CPSConstants.ROLE_NAME_PRPTY);
			final QueryExpression acctType = roleBuilder.createConstantQueryExpression(accountType);
			final Query queryItem = roleBuilder.createComparisonQuery(name, acctType, QueryBuilder.EQUALS);
			roleItem = roleView.executeQuery(queryItem);
		} catch (final RepositoryException e) {
			logError("getRoleItem(): " + e);
		} catch (final NullPointerException ne) {
			logError("getRoleItem(): " + ne);
		}
		return roleItem != null ? roleItem[0] : null;
	}

	private String[] getRoleIds(String[] roles) {
		List<String> roleIds = new ArrayList<>();

		for (int i = 0; i < roles.length; i++) {
			RepositoryItem roleItem = getRoleItem(getRolePropertyName(roles[i]));
			if (roleItem != null) {
				roleIds.add(roleItem.getRepositoryId());
			}
		}

		return roleIds.toArray(new String[0]);
	}

	private String getRolePropertyName(String roleDisplayName) {
		String role = roleDisplayName;

		if (CPSConstants.ROLE_ACCOUNT_ADMIN_DISPLAY.equalsIgnoreCase(roleDisplayName)) {
			role = ROLE_ACCOUNT_ADMIN;
		}
		if (CPSConstants.ROLE_APPROVER_DISPLAY.equalsIgnoreCase(roleDisplayName)) {
			role = CPSConstants.ROLE_APPROVER;
		}
		if (CPSConstants.ROLE_BUYER_DISPLAY.equalsIgnoreCase(roleDisplayName)) {
			role = ROLE_BUYER;
		}
		if (CPSConstants.ROLE_FINANCE_DISPLAY.equalsIgnoreCase(roleDisplayName)) {
			role = CPSConstants.ROLE_FINANCE;
		}

		return role;
	}

	private void lookupApprovers() {
		String[] approverIds = getApprovers().split(",");

		for (String id : approverIds) {
			RepositoryItem user = findByIdOnly(id);
			if (user != null) {
				getOrderApprovers().add(user);
			}
		}
	}

	private void lookupBillingAccounts() {
		if (!StringUtils.isBlank(getBillingAccountIds())) {
			String[] billingAccountIds = getBillingAccountIds().split(",");
			for (String id : billingAccountIds) {
				RepositoryItem organization = findOrg(id);
				if (organization != null) {
					getBillingAccounts().add(organization);
				}
			}
		}
	}

	public RepositoryItem findByIdOnly(String userId) {
		try {
			Repository pRepository = getProfileTools().getProfileRepository();
			RepositoryView view = pRepository.getView("user");
			QueryBuilder qb = view.getQueryBuilder();
			QueryExpression qe1 = qb.createPropertyQueryExpression("Id");
			QueryExpression qe2 = qb.createConstantQueryExpression(userId);
			Query query = qb.createComparisonQuery(qe1, qe2, QueryBuilder.EQUALS);

			RepositoryItem items[] = view.executeQuery(query);

			if (items != null && items.length > 0) {
				return items[0];
			}
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		return null;
	}

	public Map<String, String> validateNewUser(String firstName, String lastName, String email, String phone,
			Map<String, String> roles, String spendingLimit, String frequency, Double userCSRSpendingLimit,
			String parentOrganizationId, boolean isImport) {
		Map<String, String> errors = new HashMap<String, String>();

		// Validate first name
		if (StringUtils.isBlank(firstName)) {
			errors.put(CPSErrorCodes.ERR_UPDATE_MISSING_FIRST_NAME, CPSConstants.FIRST_NAME);
		} else {
			if (!firstName.matches(CPSConstants.REG_EXP_NAME)) {
				errors.put(CPSErrorCodes.ERR_UPDATE_INVALID_FIRST_NAME, CPSConstants.FIRST_NAME);
			}
		}
		// Validate last name
		if (StringUtils.isBlank(lastName)) {
			errors.put(CPSErrorCodes.ERR_UPDATE_MISSING_LAST_NAME, CPSConstants.LAST_NAME);
		} else {
			if (!lastName.matches(CPSConstants.REG_EXP_NAME)) {
				errors.put(CPSErrorCodes.ERR_UPDATE_INVALID_LAST_NAME, CPSConstants.LAST_NAME);
			}
		}
		// Validate email
		if (StringUtils.isBlank(email)) {
			errors.put(CPSErrorCodes.ERR_UPDATE_MISSING_EMAIL, CPSConstants.EMAIL);
		} else {
			if (!email.matches(CPSConstants.REG_EXP_EMAIL)) {
				errors.put(CPSErrorCodes.ERR_UPDATE_INVALID_EMAIL, CPSConstants.EMAIL);
			}
		}
		// Validate phone
		if (!isBulkUploadFromBcc()) {
			if (!phone.matches(CPSConstants.REG_EXP_PHONE) && (!StringUtils.isBlank(phone))) {
				errors.put(CPSErrorCodes.ERR_UPDATE_INVALID_PRIMARY_PHONE, CPSConstants.PRIMARY_PHONE);
			}
		}
		// Validate role
		if (roles == null || roles.isEmpty()) {
			errors.put(CPSErrorCodes.ERR_UPDATE_MISSING_ROLE, CPSConstants.ROLE_PRPTY);
		}
		RepositoryItem approver = null;
		if(StringUtils.isNotBlank(getEditId())){
		try {
			approver = ((CommerceProfileTools) getProfileTools()).getProfileItem(getEditId());
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
		

		for (Entry<String, String> roleOption : getRole().entrySet()) {
			if (roleOption.getKey().equals(CPSConstants.ROLE_APPROVER)
					&& roleOption.getValue().equals(CPSConstants.FALSE)) {
				List<RepositoryItem> buyers = getBuyersForApprover(approver);
				List<String> buyersList = new ArrayList<String>();
				boolean hasErrors = false;
				for (RepositoryItem buyer : buyers) {
					boolean approverHasBuyer = approverHasOnlyOneBuyer(buyer, approver);
					if (approverHasBuyer) {
						String buyerFirstName = (String) buyer
								.getPropertyValue(getPropertyManager().getFirstNamePropertyName());
						String buyerLastName = (String) buyer
								.getPropertyValue(getPropertyManager().getLastNamePropertyName());
						buyersList.add(buyerFirstName + " " + buyerLastName);
						hasErrors = true;
					}
				}
				if (hasErrors) {
					StringBuilder sb = new StringBuilder();
					for (String string : buyersList) {
						sb.append(string).append(", ");
					}
					int index = sb.lastIndexOf(",");
					if (index > 0) {
						sb = sb.deleteCharAt(index);
					}
					errors.put(
							"This is the only approver for user(s) " + sb
									+ ". You must assign another approver to user(s) or remove their spending limit.",
							CPSConstants.ROLE_PRPTY);
				}
			}
		}
		}

		if (!isBulkUploadFromBcc()) {
			if (roles != null && roles.get(ROLE_BUYER) != null
					&& roles.get(ROLE_BUYER).equalsIgnoreCase(CPSConstants.TRUE)) {
				errors.putAll(validateSpendingLimit(spendingLimit, frequency, userCSRSpendingLimit));
			}
		}
		if (!isImport && getSessionBean().getUsersShipping() != null
				&& !getSessionBean().getUsersShipping().isEmpty()) {
			boolean userHasDefault = false;
			for (UserAssociatedCS cs : getSessionBean().getUsersShipping()) {
				if (cs.getIsDefault()) {
					userHasDefault = true;
				}
			}
		}
		// if it is a bulk upload from BCC,then the parentOrganization must be a
		// required field

		if (isBulkUploadFromBcc()) {
			if (StringUtils.isBlank(parentOrganizationId)) {
				errors.put(CPSErrorCodes.ERR_UPDATE_MISSING_PARENT_ORG, CPSConstants.PARENT_ORG);
			} else {
				try {
					RepositoryItem orgn = getProfileTools().getProfileRepository().getItem(parentOrganizationId,
							"organization");
					if (orgn == null) {
						errors.put(CPSErrorCodes.ERR_UPDATE_INVALID_PARENT_ORG, CPSConstants.PARENT_ORG);
					}
				} catch (RepositoryException e) {
					vlogError("error fetching organization for the given ID - {0} - {1}", parentOrganizationId,
							e.getMessage());
					logError(e);
				}
			}
		}
		return errors;
	}

	public Map<String, String> validateSpendingLimit(String spendingLimit, String frequency,
			Double csrSetSpendingLimit) {
		Map<String, String> errors = new HashMap<String, String>();

		if (StringUtils.isNotBlank(spendingLimit)) {
			try {
				Double limit = Double.parseDouble(spendingLimit);

				vlogDebug("Spending Limit: " + spendingLimit);

				if (limit < 0) {
					// Spending limit can't be negative
					errors.put(CPSErrorCodes.ERR_UPDATE_SPEND_LIMIT, CPSConstants.SPENDING_LIMIT);
				} else if (limit > Math.floor(limit)) {
					// Spending limit has decimal, return error
					errors.put(CPSErrorCodes.ERR_UPDATE_SPEND_LIMIT, CPSConstants.SPENDING_LIMIT);
				} else {
					// Spending limit value is valid, check if greater than
					// super admin set
					if (!((CPSCSRProfileTools) getProfileTools()).isCSRUser(getProfile())) {
						// Not csr user setting limit, check users csr set
						// spending limit
						if (csrSetSpendingLimit != null) {
							if (limit > csrSetSpendingLimit) {
								errors.put(CPSErrorCodes.ERR_UPDATE_SPEND_LIMIT_CSR, CPSConstants.SPENDING_LIMIT);
							}
						}
					}
				}
				if (StringUtils.isBlank(frequency)) {
					if (!"0".equalsIgnoreCase(spendingLimit)) {
						errors.put(CPSErrorCodes.ERR_UPDATE_FREQUENCY, CPSConstants.ORDER_LIMIT_FREQUENCY);
					}
				}
				// Check if buyer role, approvers exist
				if (getRole().get(ROLE_BUYER) != null) {
					if (getRole().get(ROLE_BUYER).equalsIgnoreCase(CPSConstants.TRUE)) {
						// User has buyer role, check if approvers exist
						if (!"0".equals(getSpendingLimit()) && (getOrderApprovers() == null
								|| (getOrderApprovers() != null && getOrderApprovers().isEmpty()))) {
							errors.put(CPSErrorCodes.ERR_REG_MISSING_APPROVER, CPSConstants.APPROVERS);
						}
					}
				}
			} catch (Exception e) {
				vlogError(e, "Error");
				errors.put(CPSErrorCodes.ERR_UPDATE_SPEND_LIMIT, CPSConstants.SPENDING_LIMIT);
			}
		}

		return errors;
	}

	protected void checkErrorMessages(Map<String, String> errors, Locale locale) {
		vlogDebug("Adding errors");
		List<String> addedExceptions = new ArrayList<String>();

		if (errors != null && !errors.isEmpty()) {
			for (Entry<String, String> error : errors.entrySet()) {
				if (!addedExceptions.contains(error.getValue())) {
					vlogDebug("New error: " + error.getKey() + " - " + error.getValue());
					// createError(error.getValue(), error.getKey(), locale);
					createError(error.getKey(), error.getValue(), locale);
					addedExceptions.add(error.getValue());
				} else {
					vlogDebug("Existing error: " + error.getKey() + " - " + error.getValue());
					addFormException(new DropletException("", error.getKey()));
				}
			}
		}
	}

	protected void createError(String msg, String pty, Locale locale) {
		// CPSMessageUtils.addFormException(this,
		// CPSConstants.ERR_MESSAGES_REPOSITORY, msg, locale, pty);
		CPSMessageUtils.addFormException(this, msg, locale, pty);
	}

	private Set<RepositoryItem> getAccountAdminsForOrganization(RepositoryItem organization) {
		Set<RepositoryItem> accountAdmins = new HashSet<RepositoryItem>();
		Set organizationMembers = (Set) organization.getPropertyValue(CPSConstants.ORGANIZATION_MEMBERS_PRTY);

		for (Object memberObject : organizationMembers) {
			if (memberObject instanceof RepositoryItem) {
				RepositoryItem organizationMember = (RepositoryItem) memberObject;

				Set<RepositoryItem> orgMemberRoles = (Set) organizationMember
						.getPropertyValue(CPSConstants.ROLES_PRPTY);
				if (orgMemberRoles != null) {
					for (RepositoryItem r : orgMemberRoles) {
						if (ROLE_ACCOUNT_ADMIN.equalsIgnoreCase((String) r.getPropertyValue(CPSConstants.NAME))) {
							accountAdmins.add(organizationMember);
							break;
						}
					}
				}
			}
		}

		return accountAdmins;
	}

	private boolean userIsBuyer(RepositoryItem user) {
		boolean isBuyer = false;

		if (user != null) {
			Set<RepositoryItem> orgMemberRoles = (Set) user.getPropertyValue(CPSConstants.ROLES_PRPTY);
			if (orgMemberRoles != null) {
				for (RepositoryItem r : orgMemberRoles) {
					if (ROLE_BUYER.equalsIgnoreCase((String) r.getPropertyValue(CPSConstants.NAME))) {
						isBuyer = true;
						break;
					}
				}
			}
			vlogDebug("User " + user.getPropertyValue(CPSConstants.EMAIL) + " is buyer? " + isBuyer);
		}

		return isBuyer;
	}

	private void createHomeAddressForUser(MutableRepositoryItem user) {
		try {
			MutableRepositoryItem homeAddress = getProfileTools().getProfileRepository()
					.createItem(CPSConstants.CONTACT_INFO);
			getProfileTools().getProfileRepository().addItem(homeAddress);
			user.setPropertyValue(CPSConstants.HOME_ADDRESS, homeAddress);
			getProfileTools().getProfileRepository().updateItem(user);
		} catch (RepositoryException e) {
			vlogError(e, "Error");
		}
	}

	private boolean approverHasOnlyOneBuyer(RepositoryItem buyer, RepositoryItem approver) {
		boolean approverHasOnlyOneBuyer = false;
		List<RepositoryItem> approvers = getApprovalManager().getApproversForUser(buyer);
		Double spendingLimit = (Double) buyer.getPropertyValue(CPSConstants.ORDER_LIMIT);
		if (approvers != null && spendingLimit != null) {
			if (approvers.size() == 1 && approvers.contains(approver)) {
				approverHasOnlyOneBuyer = true;
			}
		}
		return approverHasOnlyOneBuyer;
	}

	public List<RepositoryItem> getBuyersForApprover(RepositoryItem pProfile) {

		List<RepositoryItem> buyers = null;
		RepositoryItem parentOrganization = (RepositoryItem) pProfile.getPropertyValue(CPSConstants.PARENT_ORG);
		if (parentOrganization != null) {
			buyers = new ArrayList<RepositoryItem>();
			Set<RepositoryItem> organizationMembers = (Set<RepositoryItem>) parentOrganization
					.getPropertyValue(CPSConstants.ORGANIZATION_MEMBERS_PRTY);
			for (RepositoryItem organizationMember : organizationMembers) {
				if (userIsBuyer(organizationMember)) {
					buyers.add(organizationMember);
				}
			}
		}
		return buyers;

	}

	public boolean handleInvalidateCache(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	throws ServletException, IOException {
		vlogDebug("HandleInvalidateCache.start");
		String url = pRequest.getRequestURI();
		String successURL = null;
		String errorURL = null;
		if (url.contains(CPSConstants.QUESTION_MARK)) {
			successURL = url + "&error=false";
			errorURL = url + "&error=true";
		} else {
			successURL = url + "?error=false";
			errorURL = url + "?error=true";
		}
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "CPSMultiUserAddFormHandler.handleInvalidateCache";
		if ((rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) && getCpsGlobalProperties().isInvalidteCacheEnabled()) {
			try {
					if(!getInvalidateCacheRepositoryPath().isEmpty()){
					vlogDebug("now going to invalidate the cache of - {0}", getInvalidateCacheRepositoryPath());
					getCacheInvalidationManager().invalidateCache(getInvalidateCacheRepositoryPath());
					}else{
						vlogInfo("error-repository path is missing");
					}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			finally {
				if (rrm != null) {
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
	
		return checkFormRedirect(successURL,errorURL , pRequest,pResponse);
	
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNewUserCompletionLink() {
		return newUserCompletionLink;
	}

	public void setNewUserCompletionLink(String newUserCompletionLink) {
		this.newUserCompletionLink = newUserCompletionLink;
	}

	public Map<String, String> getRole() {
		return role;
	}

	public void setRole(Map<String, String> role) {
		this.role = role;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSpendingLimit() {
		return spendingLimit;
	}

	public void setSpendingLimit(String spendingLimit) {
		this.spendingLimit = spendingLimit;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getEditSuccessURL() {
		return editSuccessURL;
	}

	public void setEditSuccessURL(String editSuccessURL) {
		this.editSuccessURL = editSuccessURL;
	}

	public String getEditErrorURL() {
		return editErrorURL;
	}

	public void setEditErrorURL(String editErrorURL) {
		this.editErrorURL = editErrorURL;
	}

	public String getEditId() {
		return editId;
	}

	public void setEditId(String editId) {
		this.editId = editId;
	}

	public String getUpdateSpendingLimitSuccessURL() {
		return updateSpendingLimitSuccessURL;
	}

	public void setUpdateSpendingLimitSuccessURL(String updateSpendingLimitSuccessURL) {
		this.updateSpendingLimitSuccessURL = updateSpendingLimitSuccessURL;
	}

	public String getUpdateSpendingLimitErrorURL() {
		return updateSpendingLimitErrorURL;
	}

	public void setUpdateSpendingLimitErrorURL(String updateSpendingLimitErrorURL) {
		this.updateSpendingLimitErrorURL = updateSpendingLimitErrorURL;
	}

	public String getOrgLimit() {
		return orgLimit;
	}

	public void setOrgLimit(String orgLimit) {
		this.orgLimit = orgLimit;
	}

	public String getApprovers() {
		return approvers;
	}

	public void setApprovers(String approvers) {
		this.approvers = approvers;
	}

	public List<RepositoryItem> getOrderApprovers() {
		return orderApprovers;
	}

	public String getBillingAccountIds() {
		return billingAccountIds;
	}

	public void setBillingAccountIds(String billingAccountIds) {
		this.billingAccountIds = billingAccountIds;
	}

	public Set<RepositoryItem> getBillingAccounts() {
		return billingAccounts;
	}

	public void setOrderApprovers(List<RepositoryItem> orderApprovers) {
		this.orderApprovers = orderApprovers;
	}

	public CPSSessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(CPSSessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

	public String getAddAdminSuccessURL() {
		return addAdminSuccessURL;
	}

	public void setAddAdminSuccessURL(String addAdminSuccessURL) {
		this.addAdminSuccessURL = addAdminSuccessURL;
	}

	public String getAddAdminErrorURL() {
		return addAdminErrorURL;
	}

	public void setAddAdminErrorURL(String addAdminErrorURL) {
		this.addAdminErrorURL = addAdminErrorURL;
	}

	public String getAccessAccountSuccessURL() {
		return accessAccountSuccessURL;
	}

	public void setAccessAccountSuccessURL(String accessAccountSuccessURL) {
		this.accessAccountSuccessURL = accessAccountSuccessURL;
	}

	public String getAccessAccountErrorURL() {
		return accessAccountErrorURL;
	}

	public void setAccessAccountErrorURL(String accessAccountErrorURL) {
		this.accessAccountErrorURL = accessAccountErrorURL;
	}

	public boolean isAddAdminSuccessful() {
		return addAdminSuccessful;
	}

	public void setAddAdminSuccessful(boolean addAdminSuccessful) {
		this.addAdminSuccessful = addAdminSuccessful;
	}

	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}

	public Map<String, String> getImportedPhones() {
		return importedPhones;
	}

	public void setImportedPhones(Map<String, String> importedPhones) {
		this.importedPhones = importedPhones;
	}

	public String getWarningMessage() {
		return warningMessage;
	}

	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}

	public ForgotPasswordService getForgotPasswordService() {
		return mForgotPasswordService;
	}

	public void setForgotPasswordService(ForgotPasswordService mForgotPasswordService) {
		this.mForgotPasswordService = mForgotPasswordService;
	}

	public String getResendInviteSuccessURL() {
		return resendInviteSuccessURL;
	}

	public void setResendInviteSuccessURL(String resendInviteSuccessURL) {
		this.resendInviteSuccessURL = resendInviteSuccessURL;
	}

	public String getResendInviteErrorURL() {
		return resendInviteErrorURL;
	}

	public void setResendInviteErrorURL(String resendInviteErrorURL) {
		this.resendInviteErrorURL = resendInviteErrorURL;
	}

	public Map<String, String> getApproversMap() {
		return approversMap;
	}

	public void setApproversMap(Map<String, String> pApproversMap) {
		approversMap = pApproversMap;
	}

	/**
	 * @return the bulkUploadFromBcc
	 */
	public boolean isBulkUploadFromBcc() {
		return bulkUploadFromBcc;
	}

	/**
	 * @param bulkUploadFromBcc
	 *            the bulkUploadFromBcc to set
	 */
	public void setBulkUploadFromBcc(boolean bulkUploadFromBcc) {
		this.bulkUploadFromBcc = bulkUploadFromBcc;
	}

	/**
	 * @return the bccImportedUsers
	 */
	public Map<String, String> getBccImportedUsers() {
		return bccImportedUsers;
	}

	/**
	 * @param bccImportedUsers
	 *            the bccImportedUsers to set
	 */
	public void setBccImportedUsers(Map<String, String> bccImportedUsers) {
		this.bccImportedUsers = bccImportedUsers;
	}

	/**
	 * @return the siteRepository
	 */
	public Repository getSiteRepository() {
		return siteRepository;
	}

	/**
	 * @param siteRepository
	 *            the siteRepository to set
	 */
	public void setSiteRepository(Repository siteRepository) {
		this.siteRepository = siteRepository;
	}

	/**
	 * @return the defaultSiteId
	 */
	public String getDefaultSiteId() {
		return defaultSiteId;
	}

	/**
	 * @param defaultSiteId
	 *            the defaultSiteId to set
	 */
	public void setDefaultSiteId(String defaultSiteId) {
		this.defaultSiteId = defaultSiteId;
	}

	/**
	 * @return the registeredUserReportManager
	 */
	public RegisteredUserReportManager getRegisteredUserReportManager() {
		return registeredUserReportManager;
	}

	/**
	 * @param registeredUserReportManager
	 *            the registeredUserReportManager to set
	 */
	public void setRegisteredUserReportManager(RegisteredUserReportManager registeredUserReportManager) {
		this.registeredUserReportManager = registeredUserReportManager;
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the userIds
	 */
	public Set<RepositoryItem> getUserIds() {
		return userIds;
	}

	/**
	 * @param userIds
	 *            the userIds to set
	 */
	public void setUserIds(Set<RepositoryItem> userIds) {
		this.userIds = userIds;
	}

	/**
	 * @return the cpsUserProfileManager
	 */
	public CPSUserProfileManager getCpsUserProfileManager() {
		return cpsUserProfileManager;
	}

	/**
	 * @param cpsUserProfileManager
	 *            the cpsUserProfileManager to set
	 */
	public void setCpsUserProfileManager(CPSUserProfileManager cpsUserProfileManager) {
		this.cpsUserProfileManager = cpsUserProfileManager;
	}

	/**
	 * @return the approvalManager
	 */
	public ApprovalManager getApprovalManager() {
		return approvalManager;
	}

	/**
	 * @param approvalManager
	 *            the approvalManager to set
	 */
	public void setApprovalManager(ApprovalManager approvalManager) {
		this.approvalManager = approvalManager;
	}

	/**
	 * @return the propertyManager
	 */
	public PropertyManager getPropertyManager() {
		return propertyManager;
	}

	/**
	 * @param propertyManager
	 *            the propertyManager to set
	 */
	public void setPropertyManager(PropertyManager propertyManager) {
		this.propertyManager = propertyManager;
	}

	public CPSGlobalProperties getCpsGlobalProperties() {
		return cpsGlobalProperties;
	}

	public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
		this.cpsGlobalProperties = cpsGlobalProperties;
	}

	public String getInvalidateCacheRepositoryPath() {
		return invalidateCacheRepositoryPath;
	}

	public void setInvalidateCacheRepositoryPath(String invalidateCacheRepositoryPath) {
		this.invalidateCacheRepositoryPath = invalidateCacheRepositoryPath;
	}

	public CacheInvalidationManager getCacheInvalidationManager() {
		return cacheInvalidationManager;
	}

	public void setCacheInvalidationManager(CacheInvalidationManager cacheInvalidationManager) {
		this.cacheInvalidationManager = cacheInvalidationManager;
	}

}