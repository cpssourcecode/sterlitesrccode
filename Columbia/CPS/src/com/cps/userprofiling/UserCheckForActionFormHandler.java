package com.cps.userprofiling;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import com.cps.userprofiling.constants.IUserCheckForActionConstants;
import com.cps.userprofiling.service.UserCheckForActionService;
import org.apache.commons.lang3.StringUtils;
import vsg.util.ajax.AjaxUtils;

import java.util.Map;

/**
 * @author Dmitry Golubev
 **/
public class UserCheckForActionFormHandler extends GenericFormHandler implements IUserCheckForActionConstants {
	/**
	 * current profile
	 */
	private Profile mProfile;
	/**
	 * action name
	 */
	private String mAction;
	/**
	 * user actions check service
	 */
	private UserCheckForActionService mActionCheckService;

	/**
	 * request monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * checkes if user has rights to perform an action
	 * @param pRequest request
	 * @param pResponse response
	 * @return false to proceed request
	 */
	public boolean handleCheckForAction(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)  {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "UserCheckForActionFormHandler.handleCheckForAction";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (StringUtils.isBlank(getAction())) {
					addFormException(new DropletException("No action is specified to check."));
				} else {
					if (ADD_TO_MATERIAL_LIST.equals(getAction())) {
						if (!getActionCheckService().checkAddItemToMaterialList(getProfile())) {
							addFormException(new DropletException("User has no rights to add an item to material list"));
						}
					} else {
						addFormException(new DropletException("No suitable action is defined: " + getAction()));
					}
				}
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return response(pResponse, null);
	}

	/**
	 * @param pResponse response
	 * @param pParams additional params
	 * @return return false
	 */
	private boolean response(DynamoHttpServletResponse pResponse, Map<String, Object> pParams) {
		AjaxUtils.addAjaxResponse(this, pResponse, pParams);
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------

	public Profile getProfile() {
		return mProfile;
	}

	public void setProfile(Profile pProfile) {
		mProfile = pProfile;
	}

	public String getAction() {
		return mAction;
	}

	public void setAction(String pAction) {
		mAction = pAction;
	}

	public UserCheckForActionService getActionCheckService() {
		return mActionCheckService;
	}

	public void setActionCheckService(UserCheckForActionService pActionCheckService) {
		mActionCheckService = pActionCheckService;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

}
