package com.cps.userprofiling.validator;

import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import com.cps.userprofiling.ContactUsFormHandler;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;

/**
 * @author Dmitry Golubev
 */
public class BaseValidator extends GenericService {
	/**
	 * Takes the key for error message and adds to error repository
	 *
	 * @author Bruce Bui
	 * @param pFormHandler ContactUsFormhandler
	 * @param msg key value error code
	 * @param pty property
	 * @param pRequest
	 */
	protected void createError(GenericFormHandler pFormHandler, String msg, String pty, DynamoHttpServletRequest pRequest){
		CPSMessageUtils.addFormException(pFormHandler, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
	}

	/**
	 * This takes a single error key and property and displays the error and adds the
	 * property to be highlighted. Use this if only adding one error.
	 *
	 * @param pHandler  generic handler
	 * @param pMessage  message code
	 * @param pProperty property
	 * @param pRequest  request
	 */
	protected static void addError(GenericFormHandler pHandler, String pMessage, String pProperty, DynamoHttpServletRequest pRequest) {
		CPSMessageUtils.addFormException(pHandler, pMessage, pRequest.getLocale(), pProperty);
	}

	protected void validateRequired(String value, String propertyName, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest){
		if (StringUtils.isBlank(value)) {
			addError(pFormHandler, errorCode, propertyName, pRequest);
		}
	}
}
