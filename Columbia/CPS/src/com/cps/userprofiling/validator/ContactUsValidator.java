package com.cps.userprofiling.validator;

import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.servlet.DynamoHttpServletRequest;
import atg.droplet.GenericFormHandler;

import com.cps.userprofiling.ContactUsFormHandler;
import com.cps.userprofiling.IContactUsFormHandler;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;

/**
 * @author Dmitry Golubev
 */
public class ContactUsValidator extends BaseValidator implements IContactUsFormHandler {

	//------------------------------------------------------------------------------------------------------------------

	/** Validates contact us page form
	 * 
	 * @author Bruce Bui
	 * @param pFormHandler contactUsFormHandler
	 * @param pRequest     request
	 * @return true if all form entries are valid
	 */
	public boolean validateContactUsForm(ContactUsFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean valid = true;
		String firstName = (String) pFormHandler.getValue().get(CPSConstants.FIRST_NAME);
		String lastName = (String) pFormHandler.getValue().get(CPSConstants.LAST_NAME);
		String email = (String) pFormHandler.getValue().get(CPSConstants.EMAIL);
		String company = (String) pFormHandler.getValue().get(COMPANY);
		String accountNum = (String) pFormHandler.getValue().get(ACCOUNT_NUMBER);
		String phoneNum = (String) pFormHandler.getValue().get(PHONE);
		String comments = (String) pFormHandler.getValue().get(COMMENTS);
		String method = (String) pFormHandler.getValue().get(METHOD);
		String contactSubject = (String) pFormHandler.getValue().get(CPSConstants.SUBJECT);
		// validates phone number
		if (!StringUtils.isBlank(phoneNum)) {
			String phoneString = pFormHandler.getValue().get(PHONE);
			if (isLoggingDebug()) {
				logDebug("!!!PHONE field reads as phoneNum: " + phoneNum);
				logDebug("!!!PHONE field reads as: phoneString" + phoneString);
			}
			valid = checkForCorrect(phoneString, "phone");
			if (!valid) {
				createError(pFormHandler, CPSErrorCodes.INVALID_PHONE, "cu_input_phone", pRequest);
			}
		}	
		// validate email address
		if (!StringUtils.isBlank(email)) {
			// String emailString = pFormHandler.getValue().get(EMAIL);
			// logDebug("!!!EMAIL field reads as emailString: " + emailString);
			if (isLoggingDebug()) {
				logDebug("!!!EMAIL field reads as email: " + email);
			}
			valid = checkForCorrect(email, "email");
			if (!valid) {
				createError(pFormHandler, CPSErrorCodes.INVALID_EMAIL, "cu_input_email", pRequest);
			}
		}
		if(StringUtils.isBlank(firstName)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_REG_MISSING_FIRST_NAME, "cu_input_fname", pRequest);
			//pFormHandler.addFormException(new DropletFormException(CPSErrorCodes.INFO_ENTER_NAME, "cu_input_name"));
			//addError(pFormHandler, CPSErrorCodes.INFO_ENTER_NAME, "cu_input_name", pRequest);

		}
		if(StringUtils.isBlank(lastName)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_REG_MISSING_LAST_NAME, "cu_input_lname", pRequest);
			//pFormHandler.addFormException(new DropletFormException(CPSErrorCodes.INFO_ENTER_NAME, "cu_input_name"));
			//addError(pFormHandler, CPSErrorCodes.INFO_ENTER_NAME, "cu_input_name", pRequest);

		}
		if(StringUtils.isBlank(email)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.INFO_ENTER_EMAIL, "cu_input_email", pRequest);
		}
		
		if(StringUtils.isBlank(company)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.INFO_COMP_NAME, "cu_input_company", pRequest);
		}
		/*
		if(StringUtils.isBlank(accountNum)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.INFO_ACC_NUM, "cu_input_accountNumber", pRequest);
		}
		*/
		if(StringUtils.isBlank(phoneNum)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.INFO_PHONE_NUM, "cu_input_phone", pRequest);
		}
		
		if(StringUtils.isBlank(method)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.INFO_METHOD, "cu_input_method", pRequest);
		}
		if(StringUtils.isBlank(comments)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.INFO_COMMENTS, "cu_input_comments", pRequest);
			/*// checks for commends < 500 chars
			boolean commentsEntered = true;
			if(commentsEntered) {
				String commentsString = pFormHandler.getValue().get(COMMENTS);
				valid = checkForCorrect(commentsString, "comments");
				if(!valid) {
					createError(pFormHandler, CPSErrorCodes.INVALID_COMMENTS, "invalid comments", pRequest);
				}
			}*/
		}
		if(StringUtils.isBlank(contactSubject)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.INFO_SUBJECT, "cu_input_subject", pRequest);
		}
		/*
		if( valid ) {
			valid = checkForCorrect( pFormHandler );
		}
		 */
		return valid;
	}
	

	/**
	 * @param pFormHandler contactUsFormHandler
	 * @param pInput input name
	 * @param pMsgPart part for error message
	 * @return true if input is filled
	 */
	/*
	private boolean isValid(ContactUsFormHandler pFormHandler, String pInput, String pMsgPart) {
		if (StringUtils.isBlank(pFormHandler.getValue().get(pInput))) {
			pFormHandler.addFormException(
				new DropletFormException("The "+pMsgPart+" is not specified.", "cu_input_"+pInput)
			);
			return false;
		}
		return true;
	}
	*/
	
	/**
	 * @param pFormHandler contact us form handler
	 * @return true if all mandatory fields are filled
	 */
	/*
	private boolean checkForEmpty(ContactUsFormHandler pFormHandler) {
		boolean valid = true;

		//if(!isValid(pFormHandler, NAME, "name")) { valid = false; }
		//if(!isValid(pFormHandler, EMAIL, "email address")) { valid = false; }
		//if(!isValid(pFormHandler, COMPANY, "company")) { valid = false; }
		//if(!isValid(pFormHandler, ACCOUNT_NUMBER, "account number")) { valid = false; }
		//if(!isValid(pFormHandler, PHONE, "phone number")) { valid = false; }
		//if(!isValid(pFormHandler, SUBJECT, "subject")) { valid = false; }
		//if(!isValid(pFormHandler, METHOD, "preferred method")) { valid = false; }
		//if(!isValid(pFormHandler, COMMENTS, "comments")) { valid = false; }

		return valid;
	}
	*/
	/** Checks for valid email and phone with reg exp and for comment length
	 * 
	 * @author Bruce Bui
	 * @param pFormHandler ContactUsFormHandler
	 * @param entry string that the field contains
	 * @param field the form field name
	 * @param pRequest DynamoHttpServletRequest
	 * @return true if entry is valid
	 */
	private boolean checkForCorrect(String entry, String field) {
		boolean valid = true;
		vlogDebug("!! inside checkForCorrect method");
		if(field.equals("email")){
			vlogDebug("!!validating email..");
			if(!entry.matches(CPSConstants.REG_EXP_EMAIL)) {
				vlogDebug("!!email is not valid");
				
				valid = false;
			}
		}
		if(field.equals("phone")) {
			vlogDebug("!!validating phone..");
			if(!entry.matches(CPSConstants.REG_EXP_PHONE)) {
				vlogDebug("!!phone is not valid");
				valid = false;
			}
		}
		if(field.equals("comments")) {
			vlogDebug("!!validating phone..");
			if(entry.length() > 500) {
				vlogDebug("!!comments is not valid");
				valid = false;
			}
		}
		return valid;
	}
	/**
	 * @param pFormHandler contact us form handler
	 * @return true if fields are filled correct
	 */
	/*
	private boolean checkForCorrect(ContactUsFormHandler pFormHandler) {

		boolean valid = true;
		String email = pFormHandler.getValue().get(EMAIL);
		logDebug("Checking for valid email..");
		if (!email.matches(CPSConstants.REG_EXP_EMAIL)) {
			pFormHandler.addFormException(
				new DropletFormException("The provided email is not valid.", "cu_input_"+EMAIL)
			);
			logDebug("checked for valid email");
			valid = false;
		}
		String comment = pFormHandler.getValue().get(COMMENTS);
		logDebug("Checking for valid comments..");
		if(comment.length() > 500) {
			pFormHandler.addFormException(
				new DropletFormException("The provided comments are too long. Allowed length is above 500 symbols.", "cu_input_"+COMMENTS)
			);
			logDebug("Checked for valid comments");
			valid = false;
		}
		String phone = pFormHandler.getValue().get(PHONE);
		logDebug("Checking for valid phone..");
		if(!phone.matches(CPSConstants.REG_EXP_PHONE)) {
			logDebug("checked for valid phone..");
			pFormHandler.addFormException(
				new DropletFormException("The provided phone number is not valid.", "cu_input_" + PHONE));
			valid = false;
		}
		return valid;
	}
	*/
	//------------------------------------------------------------------------------------------------------------------

}
