package com.cps.userprofiling.validator;

import static com.cps.util.CPSConstants.EVENT_NAME;
import static com.cps.util.CPSConstants.GIFTLISTS;
import static com.cps.util.CPSConstants.MINIMUM_ORDER_QTY;
import static com.cps.util.CPSConstants.ORDER_QTY_INTERVAL;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.commerce.gifts.CPSGiftlistTools;
import com.cps.userprofiling.MaterialListFormHandler;
import com.cps.userprofiling.constants.IMaterialListConstants;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;
import com.cps.util.CPSMessageUtils;

import atg.commerce.CommerceException;
import atg.commerce.gifts.GiftlistFormHandler;
import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;

/**
 * @author Dmitry Golubev
 **/
public class MaterialListValidator extends BaseValidator implements IMaterialListConstants {
	
	private CPSCatalogTools catalogTools;
    public CPSCatalogTools getCatalogTools() {
		return catalogTools;
	}

	public void setCatalogTools(CPSCatalogTools catalogTools) {
		this.catalogTools = catalogTools;
	}

	/**
     * Validates material list page form
     * 
     * @param pFormHandler
     *            MaterialListFormHandler
     * @param pRequest
     *            request
     * @return true if all form entries are valid
     */
    public boolean validateCreateListAddItem(GiftlistFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        boolean valid = true;

        String name = pFormHandler.getEventName();
        String description = pFormHandler.getDescription();

        if (StringUtils.isBlank(name)) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ML_EMPTY_NAME, CONTROL_NAME, pRequest);
        } else {
            if (name.length() > 64) {
                valid = false;
                createError(pFormHandler, CPSErrorCodes.ML_WRONG_NAME, CONTROL_NAME, pRequest);
            }
        }
        if (!StringUtils.isBlank(description) && description.length() > 250) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ML_WRONG_DESCRIPTION, CONTROL_DESCRIPTION, pRequest);
        }
        if (valid) {
            List<RepositoryItem> usersGiftlists = (List<RepositoryItem>) pFormHandler.getProfile().getPropertyValue(GIFTLISTS);
            if (usersGiftlists != null && usersGiftlists.size() > 0) {
                List<String> listNames = new ArrayList<String>();
                for (RepositoryItem list : usersGiftlists) {
                    listNames.add((String) list.getPropertyValue(EVENT_NAME));
                }
                CPSGiftlistTools tools = (CPSGiftlistTools) pFormHandler.getGiftlistManager().getGiftlistTools();
                if (!tools.validateNewGiftlistNameExist(name, listNames)) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ML_NAME_EXIST, CONTROL_NAME, pRequest);
                }
            }
        }
        if (valid) {
	            String productId = pFormHandler.getProductId();
	            if(productId != null) {
		            try {
		                RepositoryItem product = pFormHandler.getCatalogTools().findProduct(productId);
		            	valid = checkIfProductPurchasable(pRequest,product,pFormHandler);
		                if(valid) {
		                	valid = validateMinimumQuantityForProduct(pRequest, product, pFormHandler.getQuantity(), pFormHandler, null, false);
		                }
		
		            } catch (RepositoryException e) {
		            }
	            }else {
		            if(pFormHandler instanceof MaterialListFormHandler) {
			           	MaterialListFormHandler mFormHandler=(MaterialListFormHandler) pFormHandler;
		            	Map<String, String> addingItems = mFormHandler.getAddedSkuIdsWithQty();
		                for (Map.Entry<String, String> entry : addingItems.entrySet()) {
		                    String prodId = entry.getKey();
		                    try {
		                        RepositoryItem product = pFormHandler.getCatalogTools().findProduct(prodId);
		                    	valid = checkIfProductPurchasable(pRequest,product,pFormHandler);
		                        if(valid) {
		                        	valid = validateMinimumQuantityForProduct(pRequest, product, Long.parseLong(entry.getValue()), pFormHandler, null, true);
		                        }
		
		                    } catch (RepositoryException e) {
		                    }
		                }
		            }
            }
        }
        return valid;
    }
    
    public boolean checkIfProductPurchasable(DynamoHttpServletRequest pRequest, RepositoryItem product,GiftlistFormHandler pFormHandler) {
    	boolean isValid=false;
    	if(product != null) {
    		isValid=getCatalogTools().isProductPurchasable(product);
    		if(!isValid) {
	    		pFormHandler.addFormException(new DropletFormException((product.getItemDisplayName() + ": "+ "")
	                + CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_PRODUCT_NOT_PURCHASABLE,
	                                pRequest.getLocale(), null),
	                null));
    		}
    	}
    	return isValid;
    }

    public boolean validateCreateMaterialList(GiftlistFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        boolean valid = true;

        String name = pFormHandler.getEventName();
        String description = pFormHandler.getDescription();

        if (StringUtils.isBlank(name)) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ML_EMPTY_NAME, CONTROL_NAME, pRequest);
        } else {
            if (name.length() > 64) {
                valid = false;
                createError(pFormHandler, CPSErrorCodes.ML_WRONG_NAME, CONTROL_NAME, pRequest);
            }
        }
        if (!StringUtils.isBlank(description) && description.length() > 250) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ML_WRONG_DESCRIPTION, CONTROL_DESCRIPTION, pRequest);
        }
        if (valid) {
            List<RepositoryItem> usersGiftlists = (List<RepositoryItem>) pFormHandler.getProfile().getPropertyValue(GIFTLISTS);
            if (usersGiftlists != null && usersGiftlists.size() > 0) {
                List<String> listNames = new ArrayList<String>();
                for (RepositoryItem list : usersGiftlists) {
                    listNames.add((String) list.getPropertyValue(EVENT_NAME));
                }
                CPSGiftlistTools tools = (CPSGiftlistTools) pFormHandler.getGiftlistManager().getGiftlistTools();
                if (!tools.validateNewGiftlistNameExist(name, listNames)) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ML_NAME_EXIST, CONTROL_NAME, pRequest);
                }
            }
        }
        return valid;
    }

    /**
     * checks if product and gift-list are specified
     * 
     * @param pFormHandler
     *            MaterialListFormHandler
     * @param pRequest
     *            request
     * @return true if all form entries are valid
     */
    public boolean validateAddItem(MaterialListFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        boolean valid = true;

        if (StringUtils.isBlank(pFormHandler.getGiftlistId())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ML_WRONG_GIFTLIST, CONTROL_GIFTLIST, pRequest);
        }

        return valid;
    }

    public boolean validateAddingItems(MaterialListFormHandler pFormHandler, DynamoHttpServletRequest pRequest, boolean isList) {
        boolean valid = true;
        if (!isList) {
            String productId = pFormHandler.getProductId();

            try {
                RepositoryItem product = pFormHandler.getCatalogTools().findProduct(productId);
            	valid = checkIfProductPurchasable(pRequest,product,pFormHandler);
                if(valid) {
                	valid = validateMinimumQuantityForProduct(pRequest, product, pFormHandler.getQuantity(), pFormHandler, null, false);
                }
            } catch (RepositoryException e) {
            }
        } else {
            Map<String, String> addingItems = pFormHandler.getAddedSkuIdsWithQty();
            for (Map.Entry<String, String> entry : addingItems.entrySet()) {
                String productId = entry.getKey();
                try {
                    RepositoryItem product = pFormHandler.getCatalogTools().findProduct(productId);
                	valid = checkIfProductPurchasable(pRequest,product,pFormHandler);
                    if(valid) {
                    	valid = validateMinimumQuantityForProduct(pRequest, product, Long.parseLong(entry.getValue()), pFormHandler, null, true);
                    }

                } catch (RepositoryException e) {
                }
            }
        }

        return valid;
    }

    /**
     * checks if data for updating giftlist is valid
     * 
     * @param pFormHandler
     *            MaterialListFormHandler
     * @param pRequest
     *            request
     * @return true if valid
     */
    public boolean validateUpdateGiftlist(MaterialListFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        boolean isValid = true;

        try {
            int intValue = Integer.parseInt(pFormHandler.getNewItemQty());
            if (intValue < 1) {
                createError(pFormHandler, CPSErrorCodes.ML_WRONG_QUANTITY, "" + pFormHandler.getItemToUpdate() + "_qty", pRequest);
                isValid = false;
            }
            if (isValid) {
                String productId = null;
                RepositoryItem gift = pFormHandler.getGiftlistManager().getGiftitem(pFormHandler.getItemToUpdate());
                if (gift != null) {
                    productId = (String) gift.getPropertyValue(CPSConstants.PRODUCT_ID);
                }

                try {
                    RepositoryItem product = pFormHandler.getCatalogTools().findProduct(productId);
                	isValid = checkIfProductPurchasable(pRequest,product,pFormHandler);
                    if(isValid) {
                    	isValid = validateMinimumQuantityForProduct(pRequest, product, Integer.parseInt(pFormHandler.getNewItemQty()), pFormHandler,
                                    "" + pFormHandler.getItemToUpdate() + "_qty", false);
                    }

                } catch (RepositoryException e) {
                }
            }
        } catch (NumberFormatException | NullPointerException | CommerceException e) {
            createError(pFormHandler, CPSErrorCodes.ML_WRONG_QUANTITY, "" + pFormHandler.getItemToUpdate() + "_qty", pRequest);
            isValid = false;
        }
        return isValid;
    }
    
    /**
     * Validating giftlist items
     * @param pFormHandler
     * @param pRequest
     * @return
     */
	public boolean validateUpdateGiftlistItems(MaterialListFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean isValid = true;

		Map<String, String> updatingItems = pFormHandler.getItemsToUpdate();
		for (Map.Entry<String, String> entry : updatingItems.entrySet()) {

			RepositoryItem giftItem = pFormHandler.findGiftItem(entry.getKey(), pFormHandler.getGiftlistId());
			String productId = (String) giftItem.getPropertyValue(CPSConstants.PRODUCT_ID);

			try {
				RepositoryItem product = pFormHandler.getCatalogTools().findProduct(productId);
				if (product != null && StringUtils.isNotBlank(entry.getValue())) {
					isValid = checkIfProductPurchasable(pRequest, product, pFormHandler);
					if (isValid) {
						isValid = validateMinimumQuantityForProduct(pRequest, product, Long.parseLong(entry.getValue()),
								pFormHandler, "" + entry.getKey() + "_qty", false);
					}
				} else {
					createError(pFormHandler, CPSErrorCodes.ML_WRONG_QUANTITY, "" + entry.getKey() + "_qty", pRequest);
					isValid = false;
				}

			} catch (RepositoryException | NumberFormatException e) {
				createError(pFormHandler, CPSErrorCodes.ML_WRONG_QUANTITY, "" + entry.getKey() + "_qty", pRequest);
				isValid = false;
				if (isLoggingError()) {
					logError(e);
				}
			}
		}

		return isValid;
	}
        

    /**
     * check giftlist access
     * 
     * @param pHandler
     *            - handler
     * @param pRequest
     *            - request
     * @throws CommerceException
     *             - the commerce exception
     */
    public void checkGiftlistAccess(GiftlistFormHandler pHandler, DynamoHttpServletRequest pRequest) throws CommerceException {
        if (pHandler.isEnableSecurity() && !isGiftlistOwner(pHandler, pHandler.getGiftlistId(), pHandler.getProfile().getRepositoryId())) {
            createError(pHandler, CPSErrorCodes.ML_ERROR_EDDING_TO_LIST, CONTROL_GIFTLIST, pRequest);
        }
    }

    /**
     * check if user is giftlist owner
     * 
     * @param pHandler
     *            - handler
     * @param pGiftlistId
     *            - giftlist id
     * @param pProfileId
     *            frofile id
     * @return - true if owner
     */
    public boolean isGiftlistOwner(GiftlistFormHandler pHandler, String pGiftlistId, String pProfileId) {
        if (pGiftlistId == null || pProfileId == null) {
            return false;
        }
        String owner = pHandler.getGiftlistManager().getGiftlistOwner(pGiftlistId);
        String wishlistId = pHandler.getGiftlistManager().getWishlistId(pProfileId);

        if (owner != null && !owner.equalsIgnoreCase(pProfileId)) {
            return false;
        }
        if (owner == null && wishlistId == null) {
            return false;
        }
        return true;
    }

    public boolean validateMinimumQuantityForProduct(DynamoHttpServletRequest pRequest, RepositoryItem product, long qty, GiftlistFormHandler pFormHandler,
                    String pty, boolean withProdId) {
        boolean isValid = true;
        if (product != null) {
            Integer minOrderQty = (Integer) product.getPropertyValue(MINIMUM_ORDER_QTY);
            if (minOrderQty != null) {
                if (qty < minOrderQty) {
                    pFormHandler.addFormException(new DropletFormException((withProdId ? product.getItemDisplayName() + ": " : "")
                                    + CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_MIN_ORDER_QTY,
                                                    pRequest.getLocale(), new Object[] { minOrderQty }),
                                    pty));
                    isValid = false;
                }
            } else {
                minOrderQty = 0;
            }

            Integer orderQtyInterval = (Integer) product.getPropertyValue(ORDER_QTY_INTERVAL);
            if (orderQtyInterval != null) {
                if ((qty - orderQtyInterval) % orderQtyInterval != 0) {
                    pFormHandler.addFormException(new DropletFormException((withProdId ? product.getItemDisplayName() + ": " : "")
                                    + CPSMessageUtils.getFormattedMessage(CPSConstants.ERR_MESSAGES_REPOSITORY, CPSErrorCodes.ERR_ORDER_QTY_INTERVAL,
                                                    pRequest.getLocale(), new Object[] { orderQtyInterval }),
                                    pty));
                    isValid = false;
                }
            }
        }
        return isValid;
    }
}
