package com.cps.userprofiling.validator;

import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileTools;

public class BaseAddressValidator extends BaseValidator {

    private ProfileTools mProfileTools;

	protected void validateFirstName(String firstName, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (StringUtils.isNotBlank(firstName) && !firstName.matches(CPSConstants.REG_EXP_NAME)) {
            addError(pFormHandler, errorCode, CPSConstants.FIRST_NAME, pRequest);
        }
    }

    protected void validateLastName(String lastName, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (StringUtils.isNotBlank(lastName) && !lastName.matches(CPSConstants.REG_EXP_NAME)) {
            addError(pFormHandler, errorCode, CPSConstants.LAST_NAME, pRequest);
        }
    }

    protected void validateAccountNumber(String accountNumber, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
       if (StringUtils.isNotBlank(accountNumber) && !accountNumber.matches(CPSConstants.REG_EXP_NUMERIC)) {
            addError(pFormHandler, errorCode, CPSConstants.JDE_ACCOUNT_NUM, pRequest);
        }
    }

    protected void validateZipCode(String zipCode, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (StringUtils.isNotBlank(zipCode) && !zipCode.matches(CPSConstants.REG_EXP_ZIPCODE)) {
            addError(pFormHandler, errorCode, CPSConstants.ZIP, pRequest);
        }
    }

    protected void validateGuestZipCode(String zipCode, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(zipCode) && !zipCode.matches(CPSConstants.REG_EXP_ZIPCODE)) {
            addError(pFormHandler, errorCode, CPSConstants.ZIP_CODE, pRequest);
        }
    }

    protected void validateCompanyName(String companyName, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(companyName) && !companyName.matches(CPSConstants.REG_EXP_SPECIAL_CHAR)) {
            addError(pFormHandler, errorCode, CPSConstants.COMPANY_NAME, pRequest);
        }
    }

    protected void validatePhoneNumber(String phoneNumber, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
         if (!StringUtils.isBlank(phoneNumber) && !phoneNumber.matches(CPSConstants.REG_EXP_PHONE)) {
            addError(pFormHandler, errorCode, CPSConstants.PHONE_NUMBER, pRequest);
         }
    }

    protected void validateAddress1(String companyBillingAddress, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(companyBillingAddress) && !companyBillingAddress.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
            addError(pFormHandler, errorCode, CPSConstants.ADDRESS1, pRequest);
        }
    }

    protected void validateAddress2(String address2, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(address2) && !address2.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
            addError(pFormHandler, errorCode, CPSConstants.ADDRESS2, pRequest);
        }
    }

    protected void validateCity(String city, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(city) && !city.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
            addError(pFormHandler, errorCode, CPSConstants.CITY, pRequest);
        }
    }

    protected void validateState(String state, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(state) && !state.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
            addError(pFormHandler, errorCode, CPSConstants.STATE, pRequest);
        }
    }

    protected void validateEmail(String email, String invalidCode, String existsCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(email) && !email.matches(CPSConstants.REG_EXP_EMAIL)) {
            addError(pFormHandler, invalidCode, CPSConstants.EMAIL, pRequest);
        } else {
            // Email is valid, check if already exists
            ProfileTools profileTools = getProfileTools();
            RepositoryItem user = profileTools.getItemFromEmail(email);
            if (user != null) {
                // Email exists
                addError(pFormHandler, existsCode, CPSConstants.EMAIL, pRequest);
            }
        }
    }
    
    protected void validateSupervisorName(String supervisorName, String errorCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (StringUtils.isNotBlank(supervisorName) && !supervisorName.matches(CPSConstants.REG_EXP_NAME)) {
            addError(pFormHandler, errorCode, CPSConstants.SUPERVISOR_NAME, pRequest);
        }
    }
    
    protected void validateSupervisorEmail(String supervisorEmail, String invalidCode, String existsCode, GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        if (!StringUtils.isBlank(supervisorEmail) && !supervisorEmail.matches(CPSConstants.REG_EXP_EMAIL)) {
            addError(pFormHandler, invalidCode, CPSConstants.SUPERVISOR_EMAIL, pRequest);
        } 
    }
    
    public ProfileTools getProfileTools() {
        return mProfileTools;
    }

    public void setProfileTools(ProfileTools pProfileTools) {
        mProfileTools = pProfileTools;
    }
  
}
