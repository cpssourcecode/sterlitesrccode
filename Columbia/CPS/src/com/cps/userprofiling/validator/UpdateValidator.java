package com.cps.userprofiling.validator;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileForm;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

public class UpdateValidator extends BaseValidator {

	private boolean mValidateEmailExists;

	public void validateProfilePersonalInfo (ProfileForm pFormHandler, String firstName, String lastName,
			String title, DynamoHttpServletRequest pRequest) {
		isValidFirstName(firstName, true, pFormHandler, pRequest);
		isValidLastName(lastName, true, pFormHandler,  pRequest);
		isValidTitle(title, false, pFormHandler, pRequest);
	}
	
	public void validateProfileContactInfo (ProfileForm pFormHandler, String email, String mobilePhone, String directPhone,
			String address1, String address2, String city, String state, String postalCode, DynamoHttpServletRequest pRequest) {
		isValidEmail(email, false, pFormHandler, pRequest);
		isValidMobilPhone(mobilePhone, false, pFormHandler, pRequest);
		isValidDirectPhone(directPhone, false, pFormHandler, pRequest);
		isValidAddress1(address1, false, pFormHandler, pRequest);
		isValidAddress2(address2, false, pFormHandler, pRequest);
		isValidCity(city, false, pFormHandler, pRequest);
		isValidState(state, false, pFormHandler, pRequest);
		isValidPostalCode(postalCode, false, pFormHandler, pRequest);
	}
	
	public void validateProfileSecurityInfo (ProfileForm pFormHandler, String currentPassword, String oldPassword, 
			String newPassword, String confirmPassword, DynamoHttpServletRequest pRequest) {
		// Check if entered old password equals current password
		if (currentPassword.equals(oldPassword)) {
			// Entered Old Password Matches, check if new password is valid
			if (!StringUtils.isBlank(newPassword) && newPassword.matches(CPSConstants.REG_EXP_PSWD)) {
				// New password good, check confirm
				if (newPassword.equals(confirmPassword)) {
					vlogDebug("New password is valid");
				} else {
					// Confirm mismatch, check if blank to add correct error
					if (StringUtils.isBlank(confirmPassword)) {
						addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_CONFIRM, CPSConstants.CONFIRM_PSWD, pRequest);
					} else {
						addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_CONFIRM, CPSConstants.CONFIRM_PSWD, pRequest);
					}
				}
			} else {
				// New password invalid, check if blank to add correct error
				if (StringUtils.isBlank(newPassword)) {
					addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_PSWD, CPSConstants.NEW_PSWD, pRequest);
				} else {
					addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_PSWD, CPSConstants.NEW_PSWD, pRequest);
				}
			}
		} else {
			// Old password wrong, check if blank to add correct error
			if (StringUtils.isBlank(oldPassword)){
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_OLD_PSWD, CPSConstants.OLD_PSWD, pRequest);
			} else {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_OLD_PSWD, CPSConstants.OLD_PSWD, pRequest);
			}
		}
	}
	
	private void isValidEmail (String email, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(email)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_EMAIL, CPSConstants.EMAIL, pRequest);
		} else {
			if (!StringUtils.isBlank(email) && !email.matches(CPSConstants.REG_EXP_EMAIL)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_EMAIL, CPSConstants.EMAIL, pRequest);
			} else if (isValidateEmailExists()) {
				// Email is valid, check if already exists
				RepositoryItem[] users = pFormHandler.getProfileTools().getItemsFromEmail(email);
				if (users != null) {
					if (users.length > 1) {
						addError(pFormHandler, "err_email_already_exits", CPSConstants.EMAIL, pRequest);
					} else if (users.length == 1) {
						RepositoryItem user = users[0];
						if (user != null && !user.getRepositoryId().equals(pFormHandler.getProfile().getRepositoryId())) {
							// Email exists
							addError(pFormHandler, "err_email_already_exits", CPSConstants.EMAIL, pRequest);
						}
					}
				}
			}
		}
	}
	
	private void isValidMobilPhone (String mobilePhone, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(mobilePhone)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_PRIMARY_PHONE, CPSConstants.PRIMARY_PHONE, pRequest);
		} else {
			if (!StringUtils.isBlank(mobilePhone) && !mobilePhone.matches(CPSConstants.REG_EXP_PHONE)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_PRIMARY_PHONE, CPSConstants.PRIMARY_PHONE, pRequest);
			}
		}
	}
	private void isValidDirectPhone (String directPhone, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(directPhone)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_ALTERNATE_PHONE, CPSConstants.ALTERNATE_PHONE, pRequest);
		} else {
			if (!StringUtils.isBlank(directPhone) && !directPhone.matches(CPSConstants.REG_EXP_PHONE)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_ALTERNATE_PHONE, CPSConstants.ALTERNATE_PHONE, pRequest);
			}
		}
	}
	private void isValidAddress1 (String address1, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(address1)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_ADDRESS1, CPSConstants.ADDRESS1, pRequest);
		} else {
			if (!StringUtils.isBlank(address1) && !address1.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_ADDRESS1, CPSConstants.ADDRESS1, pRequest);
			}
		}
	}
	private void isValidAddress2 (String address2, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(address2)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_ADDRESS2, CPSConstants.ADDRESS2, pRequest);
		} else {
			if (!StringUtils.isBlank(address2) && !address2.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_ADDRESS2, CPSConstants.ADDRESS2, pRequest);
			}
		}
	}
	private void isValidCity (String city, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(city)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_CITY, CPSConstants.CITY, pRequest);
		} else {
			if (!StringUtils.isBlank(city) && !city.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_CITY, CPSConstants.CITY, pRequest);
			}
		}
	}
	private void isValidState (String state, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(state)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_STATE, CPSConstants.STATE, pRequest);
		} else {
			if (!StringUtils.isBlank(state) && !state.matches(CPSConstants.REG_EXP_ALPHA_NUMERIC)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_STATE, CPSConstants.STATE, pRequest);
			}
		}
	}
	private void isValidPostalCode (String postalCode, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(postalCode)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_POSTAL_CODE, CPSConstants.POSTAL_CODE, pRequest);
		} else {
			if (!StringUtils.isBlank(postalCode) && !postalCode.matches(CPSConstants.REG_EXP_NUMERIC)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_POSTAL_CODE, CPSConstants.POSTAL_CODE, pRequest);
			}
		}
	}
	
	private void isValidFirstName(String firstName, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(firstName)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_FIRST_NAME, CPSConstants.FIRST_NAME, pRequest);
		} else {
			if (!StringUtils.isBlank(firstName) && !firstName.matches(CPSConstants.REG_EXP_NAME)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_FIRST_NAME, CPSConstants.FIRST_NAME, pRequest);
			}
		}
	}
	
	private void isValidLastName(String lastName, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(lastName)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_LAST_NAME, CPSConstants.LAST_NAME, pRequest);
		} else {
			if (!StringUtils.isBlank(lastName) && !lastName.matches(CPSConstants.REG_EXP_NAME)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_LAST_NAME, CPSConstants.LAST_NAME, pRequest);
			}
		}
	}
	
	private void isValidTitle(String title, boolean isRequired, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		if (isRequired && StringUtils.isBlank(title)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_TITLE, CPSConstants.TITLE, pRequest);
		} else {
			if (!StringUtils.isBlank(title) && !title.matches(CPSConstants.REG_EXP_SPECIAL_CHAR)) {
				addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_TITLE, CPSConstants.TITLE, pRequest);
			}
		}
	}

	public boolean isValidateEmailExists() {
		return mValidateEmailExists;
	}

	public void setValidateEmailExists(boolean pValidateEmailExists) {
		mValidateEmailExists = pValidateEmailExists;
	}
}