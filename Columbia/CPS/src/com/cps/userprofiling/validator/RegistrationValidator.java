package com.cps.userprofiling.validator;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileForm;

public class RegistrationValidator extends BaseAddressValidator {

	public void validateRegistrationInfo(ProfileForm pFormHandler, String firstName, String lastName, String email, String password, 
			String confirmPassword, String accountNumber, String zipCode, String supervisorName, String supervisorEmail,
			DynamoHttpServletRequest pRequest) {

		validateRequired(firstName, CPSConstants.FIRST_NAME, CPSErrorCodes.ERR_REG_MISSING_FIRST_NAME, pFormHandler, pRequest);
		validateFirstName(firstName, CPSErrorCodes.ERR_REG_INVALID_FIRST_NAME, pFormHandler, pRequest);

		validateRequired(lastName, CPSConstants.LAST_NAME, CPSErrorCodes.ERR_REG_MISSING_LAST_NAME, pFormHandler, pRequest);
		validateLastName(lastName, CPSErrorCodes.ERR_REG_INVALID_LAST_NAME, pFormHandler, pRequest);

		validateRequired(email, CPSConstants.EMAIL, CPSErrorCodes.ERR_REG_MISSING_EMAIL, pFormHandler, pRequest);
		validateEmail(email, CPSErrorCodes.ERR_REG_INVALID_EMAIL, CPSErrorCodes.ERR_REG_EMAIL_EXISTS, pFormHandler, pRequest);
		isValidPassword(password, confirmPassword, pFormHandler, pRequest);

		validateRequired(accountNumber, CPSConstants.ACCOUNT_NUMBER, CPSErrorCodes.ERR_REG_MISSING_ACCOUNT, pFormHandler, pRequest);
		validateAccountNumber(accountNumber, CPSErrorCodes.ERR_REG_INVALID_ACCOUNT, pFormHandler, pRequest);

		validateRequired(zipCode, CPSConstants.ZIP, CPSErrorCodes.ERR_REG_MISSING_ZIP, pFormHandler, pRequest);
		validateZipCode(zipCode, CPSErrorCodes.ERR_REG_INVALID_ZIP, pFormHandler, pRequest);
		
		validateRequired(supervisorName, CPSConstants.SUPERVISOR_NAME, CPSErrorCodes.ERR_REG_MISSING_SUPERVISOR_NAME, pFormHandler, pRequest);
		validateSupervisorName(supervisorName, CPSErrorCodes.ERR_REG_INVALID_SUPERVISOR_NAME, pFormHandler, pRequest);

		validateRequired(supervisorEmail, CPSConstants.SUPERVISOR_EMAIL, CPSErrorCodes.ERR_REG_MISSING_SUPERVISOR_EMAIL,
				pFormHandler, pRequest);
		validateSupervisorEmail(supervisorEmail, CPSErrorCodes.ERR_REG_INVALID_SUPERVISOR_EMAIL,
				CPSErrorCodes.ERR_REG_SUPERVISOR_EMAIL_EXISTS, pFormHandler, pRequest);
		
	}
	
	public void validateGuestRegistrationInfo(ProfileForm pFormHandler, String firstName, String lastName, String email, String password,
			String confirmPassword, String companyName, String phoneNumber, String companyBillingAddress, String address2,
			String city, String state, String zipCode, DynamoHttpServletRequest pRequest) {

		validateRequired(firstName, CPSConstants.FIRST_NAME, CPSErrorCodes.ERR_REG_MISSING_FIRST_NAME, pFormHandler, pRequest);
		validateFirstName(firstName, CPSErrorCodes.ERR_REG_INVALID_FIRST_NAME, pFormHandler, pRequest);

		validateRequired(lastName, CPSConstants.LAST_NAME, CPSErrorCodes.ERR_REG_MISSING_LAST_NAME, pFormHandler, pRequest);
		validateLastName(lastName, CPSErrorCodes.ERR_REG_INVALID_LAST_NAME, pFormHandler, pRequest);

		validateRequired(companyName, CPSConstants.COMPANY_NAME, CPSErrorCodes.ERR_REG_MISSING_COMPANY, pFormHandler, pRequest);
		validateCompanyName(companyName, CPSErrorCodes.ERR_REG_INVALID_COMPANY, pFormHandler, pRequest);

		validateRequired(phoneNumber, CPSConstants.PHONE_NUMBER, CPSErrorCodes.ERR_UPDATE_MISSING_ALTERNATE_PHONE, pFormHandler, pRequest);
		validatePhoneNumber(phoneNumber, CPSErrorCodes.ERR_UPDATE_INVALID_ALTERNATE_PHONE, pFormHandler, pRequest);

		validateRequired(companyBillingAddress, CPSConstants.ADDRESS1, CPSErrorCodes.ERR_UPDATE_MISSING_ADDRESS1, pFormHandler, pRequest);
		validateAddress1(companyBillingAddress, CPSErrorCodes.ERR_UPDATE_INVALID_ADDRESS1, pFormHandler, pRequest);

		validateAddress2(address2, CPSErrorCodes.ERR_UPDATE_INVALID_ADDRESS2, pFormHandler, pRequest);
		validateCity(city, CPSErrorCodes.ERR_UPDATE_INVALID_CITY, pFormHandler, pRequest);
		validateState(state, CPSErrorCodes.ERR_UPDATE_INVALID_STATE, pFormHandler, pRequest);
		validateGuestZipCode(zipCode, CPSErrorCodes.ERR_REG_INVALID_ZIP, pFormHandler, pRequest);

		validateRequired(email, CPSConstants.EMAIL, CPSErrorCodes.ERR_REG_MISSING_EMAIL, pFormHandler, pRequest);
		validateEmail(email, CPSErrorCodes.ERR_REG_INVALID_EMAIL, CPSErrorCodes.ERR_REG_EMAIL_EXISTS, pFormHandler, pRequest);
		isValidPassword(password, confirmPassword, pFormHandler, pRequest);
	}

	public void isValidPassword(String password, String confirmPassword, ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		// Check if password is valid
		if (!StringUtils.isBlank(password) && password.matches(CPSConstants.REG_EXP_PSWD)) {
			// Password good, check confirm
			if (!password.equals(confirmPassword)) {
				// Confirm mismatch, check if blank to add correct error
				if (StringUtils.isBlank(confirmPassword)) {
					addError(pFormHandler, CPSErrorCodes.ERR_REG_MISSING_CONFIRM_PSWD, CPSConstants.CONFIRM_PSWD, pRequest);
				} else {
					addError(pFormHandler, CPSErrorCodes.ERR_REG_PSWD_MISMATCH, CPSConstants.CONFIRM_PSWD, pRequest);
				}
			}
		} else {
			// New password invalid, check if blank to add correct error
			if (StringUtils.isBlank(password)) {
				addError(pFormHandler, CPSErrorCodes.ERR_REG_MISSING_PSWD, CPSConstants.PSWD, pRequest);
			} else {
				addError(pFormHandler, CPSErrorCodes.ERR_REG_INVALID_PSWD, CPSConstants.PSWD, pRequest);
			}
		}
	}

	public void validateSupervisorEmail(ProfileForm pFormHandler,boolean isValidSupervisorEmail, DynamoHttpServletRequest pRequest) {		
		if(!isValidSupervisorEmail) {
			addError(pFormHandler, CPSErrorCodes.ERR_INVALID_ORG_SUPERVISOR_EMAIL , CPSConstants.SUPERVISOR_EMAIL , pRequest);
		}
		
	}

}
