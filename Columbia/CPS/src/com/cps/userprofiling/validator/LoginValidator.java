package com.cps.userprofiling.validator;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection; // verifyCaptcha();

import atg.json.JSONObject;
import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileForm;
import atg.userprofiling.ProfileTools;

import com.cps.userprofiling.service.LoginService;
import com.cps.userprofiling.util.LoginSessionInfo;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

/**
 * @author Dmitry Golubev
 */
public class LoginValidator extends BaseValidator {
	/**
	 * profile tools
	 */
	private ProfileTools mProfileTools;
	/**
	 * login service
	 */
	private LoginService mLoginService;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pFormHandler profile form handler
	 * @param pRequest     request
	 * @return true if current validation is passed
	 */
	public boolean validateLoginFormForEmptyInputs(
		ProfileForm pFormHandler,
		DynamoHttpServletRequest pRequest)
	{
		boolean valid = true;

		String login = (String) pFormHandler.getValue().get(CPSConstants.LOGIN);
		String password = (String) pFormHandler.getValue().get(CPSConstants.PSWD);
		// String captcha = (String) pFormHandler.getValue().get(CPSConstants.CAPTCHA);

		if (StringUtils.isBlank(login)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_MISSING_LOGIN, CPSConstants.LOGIN, pRequest);
			valid = false;
		}
		if (StringUtils.isBlank(password)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_MISSING_PSWD, CPSConstants.PSWD, pRequest);
			valid = false;
		}
		return valid;
	}


	/**
	 * @param pFormHandler profile form handler
	 * @param pRequest     request
	 * @return true if current validation is passed
	 */
	public boolean validateLoginFormForProperInputs(
		ProfileForm pFormHandler,
		DynamoHttpServletRequest pRequest)
	{
		boolean valid = true;

		String login = (String) pFormHandler.getValue().get(CPSConstants.LOGIN);
		String password = (String) pFormHandler.getValue().get(CPSConstants.PSWD);

		if (!login.matches(CPSConstants.REG_EXP_EMAIL)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_INVALID_EMAIL, CPSConstants.LOGIN, pRequest);
			valid = false;
		}
		if (StringUtils.isBlank(password)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_MISSING_PSWD, CPSConstants.PSWD, pRequest);
			valid = false;
		}
		return valid;
	}

	/**
	 * @param pFormHandler profile form handler
	 * @param pRequest     request
	 * @param pLoginSessionInfo login session info
	 * @return true if current validation is passed
	 * @throws IOException 
	 */
	public boolean validateCaptcha(ProfileForm pFormHandler, LoginSessionInfo pLoginSessionInfo, DynamoHttpServletRequest pRequest)
			throws IOException {

		boolean valid = true;
		if (pLoginSessionInfo.getLoginErrorCount() > 2) {
			vlogDebug("LoginValidator validateCaptcha start..");
			String gRecaptchaResponse = pRequest.getParameter("g-recaptcha-response");
			vlogDebug("Recaptcha response: !!" + gRecaptchaResponse);
			boolean captchaValidated = verifyCaptcha(gRecaptchaResponse, pFormHandler, pRequest);
			if (!captchaValidated) {
				valid = false;
				// addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_INV_CAPTCHA, CPSConstants.CAPTCHA, pRequest);
			}
		}
		return valid;

	}
	
	/**
	 * 
	 * @author Bruce B
	 * @param gRecaptchaResponse validated response from Google servers
	 * @param pFormHandler ProfileFormHandler
	 * @param pRequest DynamoServletRequest
	 * @return valid if Google success key is true
	 * @throws IOException
	 */
	public boolean verifyCaptcha(String gRecaptchaResponse, ProfileForm pFormHandler,
			DynamoHttpServletRequest pRequest) throws IOException {
		String url = "https://www.google.com/recaptcha/api/siteverify";
		String secret = "6LcLQAwTAAAAAO7UYqI0JQT8BBmeBE7LznEAFzL2";
		
		//if(isLoggingDebug()) {
		//	logDebug("!! LoginValidator verifyCaptcha start..");
		//}
		
		boolean valid = false;
		if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_INV_CAPTCHA, CPSConstants.CAPTCHA, pRequest);
			return valid;
		}
		try {
			URL obj = new URL(url);
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
			
			// add request header
			con.setRequestMethod("POST");
			// con.setRequestProperty("User-Agent", USER_AGENT);
			// con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
			
			String postParams = "secret=" + secret + "&response=" + gRecaptchaResponse;
			
			//send POST request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(postParams);
			wr.flush();
			wr.close();
			
			int responseCode = con.getResponseCode();
			
			//if(isLoggingDebug()) {
			//	logDebug("!! responseCode: " + responseCode);
			//}
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine = "";
			String response = "";
			
			while((inputLine = in.readLine()) != null) {
				response += inputLine + "\n";
			}
			in.close();
			
			//if(isLoggingDebug()) {
			//	logDebug("!!This is the response: " +response);
			//}
			
			JSONObject jsonResponse = new JSONObject(response);
			

			/*
			JsonReader jsonReader = Json.createReader(new StringReader(response.toString()));
			JsonObject jsonObject = JsonReader.readObject();
			jsonReader.close();
			*/
			
			
			if (jsonResponse.getBoolean("success") == true) {
				valid = true;
			} else {
				valid = false;
			}
			return valid;
				
		}catch(Exception e) {
			vlogError(e, "Error");
			return false;
		}
		
	}

	/**
	 * @param pFormHandler user login form handler
	 * @param pUserLogin user login
	 * @param pUserPassword user password
	 * @param pRequest request   @return true if user exists
	 * @return founded user
	 */
	public RepositoryItem isUserExist(ProfileForm pFormHandler,
							   String pUserLogin,
							   String pUserPassword,
							   DynamoHttpServletRequest pRequest) {

		RepositoryItem user = getProfileTools().getItem(pUserLogin, pUserPassword);
		if( user == null ) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_INVALID_CREDENTIALS, CPSConstants.LOGIN, pRequest);
		} else {
			boolean isGeneratedPass = (Boolean) user.getPropertyValue(CPSConstants.GENERATED_PSWD);
			if (isGeneratedPass){
				user = null;
				addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_RESET_PSWD, CPSConstants.LOGIN, pRequest);
			}
		}

		return user;
	}

	/**
	 * @param pFormHandler user login form handler
	 * @param pRequest request
	 * @param pLoginSessionInfo login session info
	 * @return true if user is not locked
	 */
	public boolean isCurrentUserAllowedToLogin(
		ProfileForm pFormHandler,
		DynamoHttpServletRequest pRequest,
		LoginSessionInfo pLoginSessionInfo)
	{
		boolean allow = true;

		if(getLoginService().isUserLocked(pFormHandler.getProfile(), pLoginSessionInfo)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_SESSION_LOCKED, CPSConstants.LOGIN, pRequest);
			allow = false;
		}

		vlogDebug("isCurrentUserAllowedToLogin: " + allow);

		return allow;
	}

	/**
	 * @param pFormHandler user login form handler
	 * @param pUser user founded by login
	 * @param pRequest request
	 * @param pLoginSessionInfo login session info
	 * @return true if user is not locked
	 */
	public boolean isUserAllowedToLogin(
		ProfileForm pFormHandler,
		RepositoryItem pUser,
		DynamoHttpServletRequest pRequest,
		LoginSessionInfo pLoginSessionInfo)
	{
		boolean allow = true;

		if(getLoginService().isUserLocked(pUser, pLoginSessionInfo)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_SESSION_LOCKED, CPSConstants.LOGIN, pRequest);
			allow = false;
		}

		if(!getLoginService().isUserActive(pUser)) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_DEACTIVATED_USER, CPSConstants.LOGIN, pRequest);
			allow = false;
		}
		
		if(getLoginService().isAccountInactive(pUser, pLoginSessionInfo)){
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_ACCOUNT_INACTIVE, CPSConstants.LOGIN, pRequest);
			allow = false;
		}

		return allow;
	}

	//------------------------------------------------------------------------------------------------------------------

	public ProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

	public LoginService getLoginService() {
		return mLoginService;
	}

	public void setLoginService(LoginService pLoginService) {
		mLoginService = pLoginService;
	}
}
