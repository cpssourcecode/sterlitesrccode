package com.cps.userprofiling.validator;


import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.droplet.GenericFormHandler;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.servlet.DynamoHttpServletRequest;

import atg.userprofiling.ProfileForm;
import com.cps.core.util.CPSContactInfo;
import com.cps.userprofiling.ProfileDefaultShippingAddressFormHandler;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

import static com.cps.util.CPSConstants.STORE_ITEM_TYPE;
import static com.cps.util.CPSConstants.CONTACT_INFO;


/**
 * @author Dmitry Golubev
 */
public class ProfileDefaultShippingAddressValidator extends BaseAddressValidator {
	/**
	 * profile repository
	 */
	private Repository mProfileRepository;

	/**
	 * LocationRepository
	 */
	private Repository mLocationRepository;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pFormHandler profile form handler
	 * @param pRequest     request
	 * @return true if current validation is passed
	 */
	public boolean validateCSForm(ProfileDefaultShippingAddressFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean valid = true;

		if (StringUtils.isBlank(pFormHandler.getSelectedId())) {
			pFormHandler.addFormException(
					new DropletException("The address is not chosen.")
			);
			valid = false;
		}
		try {
			if (getProfileRepository().getItem(pFormHandler.getSelectedId(), CONTACT_INFO) == null) {
				valid = false;
				pFormHandler.addFormException(
						new DropletException("Unable to find specified address")
				);
			}
		} catch (RepositoryException e) {
			valid = false;
			pFormHandler.addFormException(
					new DropletException("Unable to find specified address")
			);
		}
		return valid;
	}


	/**
	 * @param pFormHandler profile form handler
	 * @param pRequest     request
	 * @return true if current validation is passed
	 */
	public boolean validatePickupForm(ProfileDefaultShippingAddressFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean valid = true;

		if (StringUtils.isBlank(pFormHandler.getSelectedId())) {
			pFormHandler.addFormException(
					new DropletException("The address is not chosen.")
			);
			valid = false;
		}
		try {
			if (getLocationRepository().getItem(pFormHandler.getSelectedId(), STORE_ITEM_TYPE) == null) {
				valid = false;
				pFormHandler.addFormException(
						new DropletException("Unable to find specified store")
				);
			}
		} catch (RepositoryException e) {
			valid = false;
			pFormHandler.addFormException(
					new DropletException("Unable to find specified store")
			);
		}
		return valid;
	}

	/**
	 * test if address is valid
	 * @param pFormHandler Form Handler
	 * @param pAddress Contact info object
	 * @return true if valid
	 */
	public void validateShippingAddress(GenericFormHandler pFormHandler, DynamoHttpServletRequest pRequest, CPSContactInfo pAddress) {
		if(pAddress != null){
			validateRequired(pAddress.getFirstName(), CPSConstants.FIRST_NAME, CPSErrorCodes.ERR_SHIPPING_MISSING_FIRST_NAME, pFormHandler, pRequest);
			validateFirstName(pAddress.getFirstName(), CPSErrorCodes.ERR_SHIPPING_INVALID_FIRST_NAME, pFormHandler, pRequest);

			validateRequired(pAddress.getLastName(), CPSConstants.LAST_NAME, CPSErrorCodes.ERR_SHIPPING_MISSING_LAST_NAME, pFormHandler, pRequest);
			validateLastName(pAddress.getLastName(),CPSErrorCodes.ERR_SHIPPING_INVALID_LAST_NAME, pFormHandler, pRequest);

			validateRequired(pAddress.getCompanyName(), CPSConstants.COMPANY_NAME, CPSErrorCodes.ERR_SHIPPING_MISSING_COMPANY, pFormHandler, pRequest);
			validateCompanyName(pAddress.getCompanyName(), CPSErrorCodes.ERR_SHIPPING_INVALID_COMPANY, pFormHandler, pRequest);

			validateRequired(pAddress.getAddress1(), CPSConstants.ADDRESS1, CPSErrorCodes.ERR_SHIPPING_MISSING_ADDRESS1, pFormHandler, pRequest);
			validateAddress1(pAddress.getAddress1(), CPSErrorCodes.ERR_SHIPPING_INVALID_ADDRESS1, pFormHandler, pRequest);
			validateAddress2(pAddress.getAddress2(), CPSErrorCodes.ERR_SHIPPING_INVALID_ADDRESS2, pFormHandler, pRequest);

			validateRequired(pAddress.getCity(), CPSConstants.CITY, CPSErrorCodes.ERR_SHIPPING_MISSING_CITY, pFormHandler, pRequest);
			validateCity(pAddress.getCity(), CPSErrorCodes.ERR_SHIPPING_INVALID_CITY, pFormHandler, pRequest);

			validateRequired(pAddress.getPostalCode(), CPSConstants.ZIP, CPSErrorCodes.ERR_SHIPPING_MISSING_ZIP_CODE, pFormHandler, pRequest);
			validateZipCode(pAddress.getPostalCode(), CPSErrorCodes.ERR_SHIPPING_INVALID_ZIP_CODE, pFormHandler, pRequest);

			validateRequired(pAddress.getState(), CPSConstants.STATE, CPSErrorCodes.ERR_SHIPPING_MISSING_STATE, pFormHandler, pRequest);
			validateState(pAddress.getState(), CPSErrorCodes.ERR_SHIPPING_INVALID_STATE, pFormHandler, pRequest);

			validateRequired(pAddress.getEmail(), CPSConstants.EMAIL, CPSErrorCodes.ERR_SHIPPING_MISSING_EMAIL, pFormHandler, pRequest);
			validateEmail(pAddress.getEmail(), CPSErrorCodes.ERR_SHIPPING_INVALID_EMAIL, CPSErrorCodes.ERR_SHIPPING_EMAIL_EXISTS, pFormHandler, pRequest);
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}


	/**
	 * Gets LocationRepository.
	 *
	 * @return Value of LocationRepository.
	 */
	public Repository getLocationRepository() {
		return mLocationRepository;
	}

	/**
	 * Sets new LocationRepository.
	 *
	 * @param pLocationRepository New value of LocationRepository.
	 */
	public void setLocationRepository(Repository pLocationRepository) {
		mLocationRepository = pLocationRepository;
	}


}
