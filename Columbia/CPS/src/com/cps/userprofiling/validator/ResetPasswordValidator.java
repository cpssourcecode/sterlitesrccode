package com.cps.userprofiling.validator;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileForm;
import atg.userprofiling.ProfileTools;

import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

/**
 * @author Dmitry Golubev
 */
public class ResetPasswordValidator extends BaseValidator {
	
	private ProfileTools profileTools;
	
	/**
	 * @param pFormHandler profile form handler
	 * @param pRequest     request
	 * @return true if current validation is passed
	 */
	public boolean validateResetPasswordForm(ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean valid = true;
		String password = (String) pFormHandler.getValue().get(CPSConstants.PSWD);
		String passwordConfirm = (String) pFormHandler.getValue().get(CPSConstants.CONFIRM_PSWD);

		if (StringUtils.isBlank(password)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_PSWD, CPSConstants.PSWD, pRequest);
			valid = false;
		} else if (!password.matches(CPSConstants.REG_EXP_PSWD)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_INVALID_PSWD, CPSConstants.PSWD, pRequest);
			valid = false;
		} else if (StringUtils.isBlank(passwordConfirm)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_MISSING_CONFIRM_PSWD, CPSConstants.CONFIRM_PSWD, pRequest);
			valid = false;
		} else if (!passwordConfirm.equals(password)) {
			addError(pFormHandler, CPSErrorCodes.ERR_UPDATE_PSWD_MISMATCH, CPSConstants.CONFIRM_PSWD, pRequest);
			valid = false;
		}
		return valid;
	}
	
	public boolean validateNewUserResetPasswordForm(ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean valid = true;
		String email = (String)pFormHandler.getValue().get(CPSConstants.EMAIL);
		String temporaryPassword = (String)pFormHandler.getValue().get(CPSConstants.TEMPORARY_PSWD);
		
		RepositoryItem user = getProfileTools().getItemFromEmail(email);
		if (user == null) {
			addError(pFormHandler, CPSErrorCodes.ERR_LOGIN_EMAIL_NOT_FOUND, CPSConstants.LOGIN, pRequest);
			valid = false;
		} else {
			user = getProfileTools().getItem(email, temporaryPassword);
			if( user == null ) {
				addError(pFormHandler, CPSErrorCodes.ERR_INVALID_TEMPORARY_PSWD, CPSConstants.LOGIN, pRequest);
				valid = false;
			}
		}
		
		if (valid){
			validateResetPasswordForm(pFormHandler, pRequest);
		}
		
		return valid;
	}

	public ProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(ProfileTools profileTools) {
		this.profileTools = profileTools;
	}
}
