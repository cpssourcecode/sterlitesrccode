package com.cps.userprofiling.validator;

import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.userprofiling.ProfileForm;
import atg.userprofiling.ProfileTools;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

/**
 * @author Dmitry Golubev
 */
public class ForgotPasswordValidator extends BaseValidator {
	/**
	 * profile tools
	 */
	private ProfileTools mProfileTools;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pFormHandler profile form handler
	 * @param pRequest     request
	 * @return true if current validation is passed
	 */
	public boolean validateForgotPasswordForm(ProfileForm pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean valid = true;
		String step = (String) pFormHandler.getValue().get(CPSConstants.FORGOT_PSWD_STEP);
		String email = (String) pFormHandler.getValue().get(CPSConstants.EMAIL);
		String securityQuestion = (String) pFormHandler.getValue().get(CPSConstants.SECURITY_QUESTION);
		String securityAnswer = (String) pFormHandler.getValue().get(CPSConstants.SECURITY_ANSWER);

		if (!StringUtils.isBlank(email)) {
			email = email.trim().toLowerCase();
		}

		if("1".equals(step) || "2".equals(step)) {
			if (StringUtils.isBlank(email)) {
				vlogDebug("info_enter_email" + step);
				// addError(pFormHandler, CPSErrorCodes.ERR_FORGOT_PASS_MISSING_EMAIL, CPSConstants.EMAIL, pRequest);
				addError(pFormHandler, CPSErrorCodes.INFO_ENTER_EMAIL, "password-forgot-input-email", pRequest);
				valid = false;
			}
		} else
		if("2".equals(step)) { // check security question
			if (StringUtils.isBlank(securityQuestion)) {
				addError(pFormHandler, CPSErrorCodes.ERR_FORGOT_PASS_MISSING_SECURITY_QUESTION, CPSConstants.SECURITY_QUESTION, pRequest);
				valid = false;
			}
			if (StringUtils.isBlank(securityAnswer)) {
				addError(pFormHandler, CPSErrorCodes.ERR_FORGOT_PASS_MISSING_SECURITY_ANSWER, CPSConstants.SECURITY_ANSWER, pRequest);
				valid = false;
			}
		} else {
			pFormHandler.addFormException(new DropletException("Wrong step is specified."));
			valid = false;
		}
		if(!valid) {
			return false;
		}

		RepositoryItem user = getProfileTools().getItem(email, null);
		if (user == null) {
			pFormHandler.addFormException(new DropletException("No user is found with specified email"));
			// addError(pFormHandler, CPSErrorCodes.ERR_FORGOT_PASS_NO_USER, CPSConstants.EMAIL, pRequest);
			valid = false;
		}

		return valid;
	}

	//------------------------------------------------------------------------------------------------------------------


	public ProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(ProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}
}
