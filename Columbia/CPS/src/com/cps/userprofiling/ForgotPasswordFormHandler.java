package com.cps.userprofiling;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.util.RepeatingRequestMonitor;
import vsg.util.ajax.AjaxUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.ForgotPasswordHandler;

import com.cps.userprofiling.service.ForgotPasswordService;
import com.cps.userprofiling.validator.ForgotPasswordValidator;
import com.cps.util.CPSConstants;

public class ForgotPasswordFormHandler extends ForgotPasswordHandler {
	/**
	 * forgot password validator
	 */
	private ForgotPasswordValidator mForgotPasswordValidator;
	/**
	 * forgot password service
	 */
	private ForgotPasswordService mForgotPasswordService;

	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * Override handle method to validate entered information then send email with link to reset password
	 */
	public boolean handleForgotPassword(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "ForgotPasswordFormHandler.handleForgotPassword";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				preForgotPassword(pRequest, pResponse);
				if (!getFormError()) {
					postForgotPassword(pRequest, pResponse);
				}
				AjaxUtils.addAjaxResponse(this, pResponse, null);
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return checkFormRedirect(null, null, pRequest, pResponse);
	}

	/**
	 * Validate entered information
	 */
	public void preForgotPassword (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse){
		if( getForgotPasswordValidator().validateForgotPasswordForm(this, pRequest) ) {
			vlogDebug("Validation passed.");
		}
	}

	/**
	 * User entered valid information, set profile to update password, generate link, and send email
	 */
	public void postForgotPassword (DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		String emailAddress = ((String) getValue().get(CPSConstants.EMAIL)).trim().toLowerCase();
		getForgotPasswordService().sendForgotPasswordEmail(emailAddress, pRequest);
	}

	@Override
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL,
									 DynamoHttpServletRequest pRequest,
									 DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (AjaxUtils.isAjaxRequest(pRequest)) {
			// add ajax response in handle method
			return false;
		} else {
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}
	}

	//------------------------------------------------------------------------------------------------------------------

	public ForgotPasswordValidator getForgotPasswordValidator() {
		return mForgotPasswordValidator;
	}

	public void setForgotPasswordValidator(ForgotPasswordValidator pForgotPasswordValidator) {
		mForgotPasswordValidator = pForgotPasswordValidator;
	}

	public ForgotPasswordService getForgotPasswordService() {
		return mForgotPasswordService;
	}

	public void setForgotPasswordService(ForgotPasswordService pForgotPasswordService) {
		mForgotPasswordService = pForgotPasswordService;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}
}