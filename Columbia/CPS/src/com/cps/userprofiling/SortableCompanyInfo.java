package com.cps.userprofiling;

public class SortableCompanyInfo {
	/**
	 * Parameter definition for id
	 */
	private String id;
	/**
	 * Parameter definition for companyName
	 */
	private String companyName;
	/**
	 * Parameter definition for accountNumber
	 */
	private String accountNumber;
	/**
	 * Parameter definition for address1
	 */
	private String address1;
	/**
	 * Parameter definition for address2
	 */
	private String address2;
	/**
	 * Parameter definition for city
	 */
	private String city;
	/**
	 * Parameter definition for state
	 */
	private String state;
	/**
	 * Parameter definition for postalCode
	 */
	private String postalCode;
	/**
	 * Parameter definition for needsSetup
	 */
	private Boolean needsSetup;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public Boolean getNeedsSetup() {
		return needsSetup;
	}
	public void setNeedsSetup(Boolean needsSetup) {
		this.needsSetup = needsSetup;
	}
}