package com.cps.userprofiling.credit.application.pojo;

/**
 * @author Alexey Meleshko
 */
public class RegisteredState {
    private String state;
    private String stateRegistration;

    public RegisteredState(String state, String stateRegistration) {
        this.state = state;
        this.stateRegistration = stateRegistration;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateRegistration() {
        return stateRegistration;
    }

    public void setStateRegistration(String stateRegistration) {
        this.stateRegistration = stateRegistration;
    }
}
