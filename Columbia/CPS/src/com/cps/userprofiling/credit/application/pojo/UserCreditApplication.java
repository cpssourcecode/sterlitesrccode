package com.cps.userprofiling.credit.application.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Meleshko
 */
public class UserCreditApplication {
    private String organizationId;
    private String legalBusinessName;
    private String tradeName;
    private String phoneNumber;
    private String faxNumber;
    private String mail;
    private String name;
    private Address addressBusiness;
    private Address addressBilling;

    private List<PrincipalOrganization> principalOrganizationList;

    private String organizationType;

    private String businessType;
    private String businessYearEstablished;
    private String businessLicenseNumber;

    private List<BusinessTradeReference> businessTradeReferenceList;

    private BankReference bankReference;

    private String customerType;

    private String requiredPO;
    private String taxExempt;
    private String taxNumber;

    private List<String> productTypeList;
    private String expectedAnnualPurchases;

    private List<Customer> supposedCustomerList;

    private String originatedCreditRequest;

    private String personalGuaranty;
    private String personalGuarantyDebtor;

    private TaxExemptionCertificate taxExemptionCertificate;

    private ResaleCertificate resaleCertificate;

    public UserCreditApplication() {
        principalOrganizationList = new ArrayList<>();
        businessTradeReferenceList = new ArrayList<>();
        productTypeList = new ArrayList<>();
        supposedCustomerList = new ArrayList<>();
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getLegalBusinessName() {
        return legalBusinessName;
    }

    public void setLegalBusinessName(String legalBusinessName) {
        this.legalBusinessName = legalBusinessName;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public Address getAddressBusiness() {
        return addressBusiness;
    }

    public void setAddressBusiness(Address addressBusiness) {
        this.addressBusiness = addressBusiness;
    }

    public Address getAddressBilling() {
        return addressBilling;
    }

    public void setAddressBilling(Address addressBilling) {
        this.addressBilling = addressBilling;
    }

    public List<PrincipalOrganization> getPrincipalOrganizationList() {
        return principalOrganizationList;
    }

    public void setPrincipalOrganizationList(List<PrincipalOrganization> principalOrganizationList) {
        this.principalOrganizationList = principalOrganizationList;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getBusinessYearEstablished() {
        return businessYearEstablished;
    }

    public void setBusinessYearEstablished(String businessYearEstablished) {
        this.businessYearEstablished = businessYearEstablished;
    }

    public String getBusinessLicenseNumber() {
        return businessLicenseNumber;
    }

    public void setBusinessLicenseNumber(String businessLicenseNumber) {
        this.businessLicenseNumber = businessLicenseNumber;
    }

    public List<BusinessTradeReference> getBusinessTradeReferenceList() {
        return businessTradeReferenceList;
    }

    public void setBusinessTradeReferenceList(List<BusinessTradeReference> businessTradeReferenceList) {
        this.businessTradeReferenceList = businessTradeReferenceList;
    }

    public BankReference getBankReference() {
        return bankReference;
    }

    public void setBankReference(BankReference bankReference) {
        this.bankReference = bankReference;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getRequiredPO() {
        return requiredPO;
    }

    public void setRequiredPO(String requiredPO) {
        this.requiredPO = requiredPO;
    }

    public String getTaxExempt() {
        return taxExempt;
    }

    public void setTaxExempt(String taxExempt) {
        this.taxExempt = taxExempt;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public List<String> getProductTypeList() {
        return productTypeList;
    }

    public void setProductTypeList(List<String> productTypeList) {
        this.productTypeList = productTypeList;
    }

    public String getExpectedAnnualPurchases() {
        return expectedAnnualPurchases;
    }

    public void setExpectedAnnualPurchases(String expectedAnnualPurchases) {
        this.expectedAnnualPurchases = expectedAnnualPurchases;
    }

    public List<Customer> getSupposedCustomerList() {
        return supposedCustomerList;
    }

    public void setSupposedCustomerList(List<Customer> supposedCustomerList) {
        this.supposedCustomerList = supposedCustomerList;
    }

    public String getOriginatedCreditRequest() {
        return originatedCreditRequest;
    }

    public void setOriginatedCreditRequest(String originatedCreditRequest) {
        this.originatedCreditRequest = originatedCreditRequest;
    }

    public String getPersonalGuaranty() {
        return personalGuaranty;
    }

    public void setPersonalGuaranty(String personalGuaranty) {
        this.personalGuaranty = personalGuaranty;
    }

    public String getPersonalGuarantyDebtor() {
        return personalGuarantyDebtor;
    }

    public void setPersonalGuarantyDebtor(String personalGuarantyDebtor) {
        this.personalGuarantyDebtor = personalGuarantyDebtor;
    }

    public TaxExemptionCertificate getTaxExemptionCertificate() {
        return taxExemptionCertificate;
    }

    public void setTaxExemptionCertificate(TaxExemptionCertificate taxExemptionCertificate) {
        this.taxExemptionCertificate = taxExemptionCertificate;
    }

    public ResaleCertificate getResaleCertificate() {
        return resaleCertificate;
    }

    public void setResaleCertificate(ResaleCertificate resaleCertificate) {
        this.resaleCertificate = resaleCertificate;
    }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
    
}
