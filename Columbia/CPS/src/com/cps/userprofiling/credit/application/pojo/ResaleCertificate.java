package com.cps.userprofiling.credit.application.pojo;

/**
 * @author Alexey Meleshko
 */
public class ResaleCertificate {
    private String sellerName;
    private Address sellerAddress;

    private String purchaserName;
    private Address purchaserAddress;

    private String purchaserRegisteredType;
    private String registrationNumber;

    private String describePropertyResale;

    private String purchasesResaleType;
    private String percentagePurchasesResale;

    public ResaleCertificate() {
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }

    public Address getSellerAddress() {
        return sellerAddress;
    }

    public void setSellerAddress(Address sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    public String getPurchaserName() {
        return purchaserName;
    }

    public void setPurchaserName(String purchaserName) {
        this.purchaserName = purchaserName;
    }

    public Address getPurchaserAddress() {
        return purchaserAddress;
    }

    public void setPurchaserAddress(Address purchaserAddress) {
        this.purchaserAddress = purchaserAddress;
    }

    public String getPurchaserRegisteredType() {
        return purchaserRegisteredType;
    }

    public void setPurchaserRegisteredType(String purchaserRegisteredType) {
        this.purchaserRegisteredType = purchaserRegisteredType;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getDescribePropertyResale() {
        return describePropertyResale;
    }

    public void setDescribePropertyResale(String describePropertyResale) {
        this.describePropertyResale = describePropertyResale;
    }

    public String getPurchasesResaleType() {
        return purchasesResaleType;
    }

    public void setPurchasesResaleType(String purchasesResaleType) {
        this.purchasesResaleType = purchasesResaleType;
    }

    public String getPercentagePurchasesResale() {
        return percentagePurchasesResale;
    }

    public void setPercentagePurchasesResale(String percentagePurchasesResale) {
        this.percentagePurchasesResale = percentagePurchasesResale;
    }
}
