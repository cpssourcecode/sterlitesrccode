package com.cps.userprofiling.credit.application.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.credit.application.handler.CreditApplicationFormHandler;
import com.cps.userprofiling.credit.application.pojo.Address;
import com.cps.userprofiling.credit.application.pojo.BankReference;
import com.cps.userprofiling.credit.application.pojo.BusinessTradeReference;
import com.cps.userprofiling.credit.application.pojo.Customer;
import com.cps.userprofiling.credit.application.pojo.PrincipalOrganization;
import com.cps.userprofiling.credit.application.pojo.RegisteredState;
import com.cps.userprofiling.credit.application.pojo.ResaleCertificate;
import com.cps.userprofiling.credit.application.pojo.TaxExemptionCertificate;
import com.cps.userprofiling.credit.application.pojo.UserCreditApplication;
import com.cps.util.CPSConstants;
// DocuSign imports
import com.docusign.esign.api.AuthenticationApi;
import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiClient;
import com.docusign.esign.client.Configuration;
import com.docusign.esign.model.Document;
import com.docusign.esign.model.EnvelopeDefinition;
import com.docusign.esign.model.EnvelopeSummary;
import com.docusign.esign.model.LoginAccount;
import com.docusign.esign.model.LoginInformation;
import com.docusign.esign.model.RecipientViewRequest;
import com.docusign.esign.model.Recipients;
import com.docusign.esign.model.SignHere;
import com.docusign.esign.model.Signer;
import com.docusign.esign.model.Tabs;
import com.docusign.esign.model.ViewUrl;

import atg.droplet.DropletException;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

/**
 * @author Alexey Meleshko
 */
public class CreditApplicationService extends GenericService {
    /**
     * email sender
     */
    private CommonEmailSender mEmailSender;

    private MutableRepository mCreditApplicationRepository;

    /**
     * current profile
     */
    private String guestUserName;
    private String guestUserEmail;
    private String guestUserOrganisation;
    /**
     * credit application service
     */
    private String mUserName;
    private String mUserPassword;
    private String mIntegratorKey;
    private String mBaseUrl;

    private static String OTHER_SPECIFY = "Other (specify)";
    private static String PURCHASE_EXEMPT = "Exempt under direct payment permit #";
    private static String OTHER_EXPLAIN = "Other (explain)";
    private static final String RETRIEVE_CREDIT_APPLICATION_RQL_QUERY = "organizationId =?0";

    
    public void sendEmail(CreditApplicationFormHandler pFormHandler,
                          DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, UserCreditApplication mUser, File file) {
        try {
            boolean success = false;
			if (pFormHandler.getProfile().isTransient()) {
                mUser.setMail(getGuestUserEmail());
				mUser.setName(getGuestUserName());
				success = getEmailSender().sendCreditApplicationFile(mUser, file);
			} else {
				success = getEmailSender().sendCreditApplicationFile(mUser, file);
			}
            if (!success) {
                vlogDebug("Credit Application email is not sent");
            }
        } catch (Exception e) {
            vlogError("Unable to send message", e);
            pFormHandler.addFormException(
                    new DropletException("Unable to send message")
            );
        }
    }

    public UserCreditApplication createUserApplicationCredit(DynamoHttpServletRequest pRequest) {
        UserCreditApplication userCreditApplication = new UserCreditApplication();
        String businessName = pRequest.getParameter("ca_input_business_name");
        userCreditApplication.setLegalBusinessName(businessName);
        String tradeName = pRequest.getParameter("ca_input_trade_name");
        userCreditApplication.setTradeName(tradeName);
        String phone = pRequest.getParameter("ca_input_phone");
        userCreditApplication.setPhoneNumber(phone);
        String fax = pRequest.getParameter("ca_input_fax");
        userCreditApplication.setFaxNumber(fax);

        String businessStreet = pRequest.getParameter("ca_input_business_street");
        String businessCity = pRequest.getParameter("ca_input_business_city");
        String businessState = pRequest.getParameter("ca_input_business_state");
        String businessCode = pRequest.getParameter("ca_input_business_code");
        Address addressBusiness = new Address(businessStreet, businessCity, businessState, businessCode);
        userCreditApplication.setAddressBusiness(addressBusiness);

        String billingStreet = pRequest.getParameter("ca_input_billing_street");
        String billingCity = pRequest.getParameter("ca_input_billing_city");
        String billingState = pRequest.getParameter("ca_input_billing_state");
        String billingCode = pRequest.getParameter("ca_input_billing_code");
        Address addressBilling = new Address(billingStreet, billingCity, billingState, billingCode);
        userCreditApplication.setAddressBilling(addressBilling);

        String organizationType = pRequest.getParameter("ca_input_organization");
        userCreditApplication.setOrganizationType(organizationType);

        String principalNameFirst = pRequest.getParameter("ca_input_principal_name_1");
        String principalTitleFirst = pRequest.getParameter("ca_input_principal_title_1");
        String principalSocialSecurityFirst = pRequest.getParameter("ca_input_principal_social_1");
        String principalEmailFirst = pRequest.getParameter("ca_input_principal_email_1");
        String principalPhoneFirst = pRequest.getParameter("ca_input_principal_phone_1");
        String principalAddressFirst = pRequest.getParameter("ca_input_principal_address_1");
        String principalCityFirst = pRequest.getParameter("ca_input_principal_city_1");
        String principalStateFirst = pRequest.getParameter("ca_input_principal_state_1");
        String principalCodeFirst = pRequest.getParameter("ca_input_principal_code_1");
        PrincipalOrganization principalOrganizationFirst = new PrincipalOrganization(principalNameFirst, principalTitleFirst,
                principalPhoneFirst, principalSocialSecurityFirst, principalEmailFirst, new Address(principalAddressFirst,
                principalCityFirst, principalStateFirst, principalCodeFirst));

        userCreditApplication.getPrincipalOrganizationList().add(principalOrganizationFirst);

        String principalNameSecond = pRequest.getParameter("ca_input_principal_name_2");
        String principalTitleSecond = pRequest.getParameter("ca_input_principal_title_2");
        String principalSocialSecuritySecond = pRequest.getParameter("ca_input_principal_social_2");
        String principalEmailSecond = pRequest.getParameter("ca_input_principal_email_2");
        String principalPhoneSecond = pRequest.getParameter("ca_input_principal_phone_2");
        String principalAddressSecond = pRequest.getParameter("ca_input_principal_address_2");
        String principalCitySecond = pRequest.getParameter("ca_input_principal_city_2");
        String principalStateSecond = pRequest.getParameter("ca_input_principal_state_2");
        String principalCodeSecond = pRequest.getParameter("ca_input_principal_code_2");
        PrincipalOrganization principalOrganizationSecond = new PrincipalOrganization(principalNameSecond,
                principalTitleSecond, principalPhoneSecond, principalSocialSecuritySecond, principalEmailSecond,
                new Address(principalAddressSecond, principalCitySecond, principalStateSecond, principalCodeSecond));

        userCreditApplication.getPrincipalOrganizationList().add(principalOrganizationSecond);

        String businessType = pRequest.getParameter("ca_input_business_type");
        userCreditApplication.setBusinessType(businessType);
        String businessYearEstablished = pRequest.getParameter("ca_input_business_year_established");
        userCreditApplication.setBusinessYearEstablished(businessYearEstablished);
        String businessLicense = pRequest.getParameter("ca_input_business_license");
        userCreditApplication.setBusinessLicenseNumber(businessLicense);

        int number = 1;
        while (true) {
            String businessTradeName = pRequest.getParameter("ca_input_business_trade_name_" + number);
            String businessTradeAddress = pRequest.getParameter("ca_input_business_trade_address_" + number);
            String businessTradeCity = pRequest.getParameter("ca_input_business_trade_city_" + number);
            String businessTradeState = pRequest.getParameter("ca_input_business_trade_state_" + number);
            String businessTradeZip = pRequest.getParameter("ca_input_business_trade_zip_" + number);
            String businessTradePhone = pRequest.getParameter("ca_input_business_trade_phone_" + number);
            String businessTradeEmail = pRequest.getParameter("ca_input_business_trade_email_" + number);
            if (businessTradeName == null || businessTradeName.isEmpty()) {
                break;
            } else {
                userCreditApplication.getBusinessTradeReferenceList().add(new BusinessTradeReference(businessTradeName,
                        new Address(businessTradeAddress, businessTradeCity, businessTradeState, businessTradeZip), businessTradePhone, businessTradeEmail));
                number++;
            }
        }

        String bankReferenceName = pRequest.getParameter("ca_input_bank_reference_name");
        String bankReferenceAcct = pRequest.getParameter("ca_input_bank_reference_acct");
        String bankReferenceOfficer = pRequest.getParameter("ca_input_bank_reference_officer");
        String bankReferenceAddress = pRequest.getParameter("ca_input_bank_reference_address");
        String bankReferenceCity = pRequest.getParameter("ca_input_bank_reference_city");
        String bankReferenceState = pRequest.getParameter("ca_input_bank_reference_state");
        String bankReferenceZip = pRequest.getParameter("ca_input_bank_reference_zip");
        String bankReferencePhone = pRequest.getParameter("ca_input_bank_reference_phone");
        String bankReferenceEmail = pRequest.getParameter("ca_input_bank_reference_email");
        userCreditApplication.setBankReference(new BankReference(bankReferenceName, bankReferenceAcct, bankReferenceOfficer,
                new Address(bankReferenceAddress, bankReferenceCity, bankReferenceState, bankReferenceZip), bankReferencePhone, bankReferenceEmail));

        String customerType = pRequest.getParameter("ca_input_customer");
        userCreditApplication.setCustomerType(customerType);
        String requiredPO = pRequest.getParameter("ca_input_po");
        userCreditApplication.setRequiredPO(requiredPO);
        String taxExempt = pRequest.getParameter("ca_input_tax_exempt");
        userCreditApplication.setTaxExempt(taxExempt);
        String taxNumber;

        if ("Yes".equals(userCreditApplication.getTaxExempt())) {
            taxNumber = pRequest.getParameter("ca_input_tax_exempt_number");
            userCreditApplication.setTaxNumber(taxNumber);
        }

        return userCreditApplication;
    }

    public UserCreditApplication createCustomerProfile(DynamoHttpServletRequest pRequest) {
        UserCreditApplication userCreditApplication = new UserCreditApplication();
        List<String> productTypeList = new ArrayList<>();
        String[] array = new String[5];
        array[0] = pRequest.getParameter("ca_input_product_pipe");
        array[1] = pRequest.getParameter("ca_input_product_plumbing");
        array[2] = pRequest.getParameter("ca_input_product_support");
        array[3] = pRequest.getParameter("ca_input_product_automated");
        array[4] = pRequest.getParameter("ca_input_product_ventilation");

        for (String anArray : array) {
            if (anArray != null) {
                productTypeList.add(anArray);
            }
        }

        userCreditApplication.getProductTypeList().addAll(productTypeList);

        String annualPurchases = pRequest.getParameter("ca_input_annual_purchases");
        userCreditApplication.setExpectedAnnualPurchases(annualPurchases);

        int number = 1;
        while (true) {
            String profileCustomerName = pRequest.getParameter("ca_input_profile_customer_name_" + number);
            String profileCustomerTitle = pRequest.getParameter("ca_input_profile_customer_title_" + number);
            String profileCustomerPhone = pRequest.getParameter("ca_input_profile_customer_phone_" + number);
            String profileCustomerEmail = pRequest.getParameter("ca_input_profile_customer_email_" + number);
            if (profileCustomerName == null || profileCustomerName.isEmpty()) {
                break;
            } else {
                userCreditApplication.getSupposedCustomerList()
                        .add(new Customer(profileCustomerName, profileCustomerTitle, profileCustomerPhone, profileCustomerEmail));
                number++;
            }
        }

        String creditRequest = pRequest.getParameter("ca_input_credit_request");
        userCreditApplication.setOriginatedCreditRequest(creditRequest);

        String personalGuaranty = pRequest.getParameter("ca_input_personal_guaranty");
        userCreditApplication.setPersonalGuaranty(personalGuaranty);

        String personalGuarantyDebtor = pRequest.getParameter("ca_input_personal_guaranty_debtor");
        userCreditApplication.setPersonalGuarantyDebtor(personalGuarantyDebtor);

        return userCreditApplication;
    }

    public TaxExemptionCertificate createTaxExemptionCertificate(DynamoHttpServletRequest pRequest) {
        TaxExemptionCertificate taxExemptionCertificate = new TaxExemptionCertificate();

        String exemptStatus = pRequest.getParameter("ca_input_tax_exempt_status");
        taxExemptionCertificate.setTaxExemptStatus(exemptStatus);

        String taxFirmName = pRequest.getParameter("ca_input_tax_firm_name");
        taxExemptionCertificate.setFirmName(taxFirmName);

        String taxFirmAddress = pRequest.getParameter("ca_input_tax_address");
        String taxFirmState = pRequest.getParameter("ca_input_tax_state");
        String taxFirmCity = pRequest.getParameter("ca_input_tax_city");
        String taxFirmZip = pRequest.getParameter("ca_input_tax_zip");
        taxExemptionCertificate.setAddress(new Address(taxFirmAddress, taxFirmCity, taxFirmState, taxFirmZip));

        String taxEngaged = pRequest.getParameter("ca_input_tax_engaged");
        taxExemptionCertificate.setRegisteredType(taxEngaged);

        String specifyEngagedRegistered;
        if (OTHER_SPECIFY.equals(taxEngaged)) {
            specifyEngagedRegistered = pRequest.getParameter("ca_input_tax_other_engaged");
            taxExemptionCertificate.setSpecifyRegisteredType(specifyEngagedRegistered);
        }

        int number = 1;
        while (true) {
            String taxRegisteredState = pRequest.getParameter("ca_input_tax_registered_state_" + number);
            String taxRegisteredStateId = pRequest.getParameter("ca_input_tax_registered_id_" + number);
            if (taxRegisteredState == null || Objects.equals(taxRegisteredState, "State")) {
                break;
            } else {
                taxExemptionCertificate.getRegisteredStateList().add(new RegisteredState(taxRegisteredState, taxRegisteredStateId));
                number++;
            }
        }

        String buyerActivityType = pRequest.getParameter("ca_input_tax_buyer_activity");
        taxExemptionCertificate.setBuyerActivityType(buyerActivityType);

        String specifyPurchase = null;
        if (PURCHASE_EXEMPT.equals(buyerActivityType) || OTHER_EXPLAIN.equals(buyerActivityType)) {
            specifyPurchase = pRequest.getParameter("ca_input_tax_purchase_specify");
        }
        taxExemptionCertificate.setSpecifyBuyerActivityType(specifyPurchase);

        String taxAction = pRequest.getParameter("ca_input_tax_action");
        taxExemptionCertificate.setBusinessFollowing(taxAction);
        String taxProductDescription = pRequest.getParameter("ca_input_tax_product_description");
        taxExemptionCertificate.setGeneralDescriptionProducts(taxProductDescription);


        return taxExemptionCertificate;
    }

    public ResaleCertificate createResaleCertificate(DynamoHttpServletRequest pRequest) {
        ResaleCertificate resaleCertificate = new ResaleCertificate();

        String sellerName = pRequest.getParameter("ca_input_certificate_name_step_1");
        resaleCertificate.setSellerName(sellerName);
        String sellerAddress = pRequest.getParameter("ca_input_certificate_address_step_1");
        String sellerCity = pRequest.getParameter("ca_input_certificate_city_step_1");
        String sellerState = pRequest.getParameter("ca_input_certificate_state_step_1");
        String sellerZip = pRequest.getParameter("ca_input_certificate_zip_step_1");
        resaleCertificate.setSellerAddress(new Address(sellerAddress, sellerCity, sellerState, sellerZip));

        String purchaserName = pRequest.getParameter("ca_input_certificate_name_step_2");
        resaleCertificate.setPurchaserName(purchaserName);
        String purchaserAddress = pRequest.getParameter("ca_input_certificate_address_step_2");
        String purchaserCity = pRequest.getParameter("ca_input_certificate_city_step_2");
        String purchaserState = pRequest.getParameter("ca_input_certificate_state_step_2");
        String purchaserZip = pRequest.getParameter("ca_input_certificate_zip_step_2");
        resaleCertificate.setPurchaserAddress(new Address(purchaserAddress, purchaserCity, purchaserState, purchaserZip));

        String purchaserRegisteredType = pRequest.getParameter("ca_input_certificate_registered");
        resaleCertificate.setPurchaserRegisteredType(purchaserRegisteredType);
        String registeredNumber = pRequest.getParameter("ca_input_certificate_registered_number");
        resaleCertificate.setRegistrationNumber(registeredNumber);


        String describePropertyResale = pRequest.getParameter("ca_input_certificate_describe_property");
        resaleCertificate.setDescribePropertyResale(describePropertyResale);

        String resalePercentage = pRequest.getParameter("ca_input_certificate_purchases_resale_type");
        resaleCertificate.setPurchasesResaleType(resalePercentage);
        if ("partly".equals(resalePercentage)) {
            String percentagePartly = pRequest.getParameter("ca_input_certificate_percentage_partly");
            resaleCertificate.setPercentagePurchasesResale(percentagePartly);
        }

        return resaleCertificate;
    }

    public String openDocusign(File file, Profile profile) {

        // enter recipient (signer) name and email
        String recipientName = profile.getPropertyValue(CPSConstants.FIRST_NAME) + " " + profile.getPropertyValue(CPSConstants.LAST_NAME);
        String recipientEmail = (String) profile.getPropertyValue(CPSConstants.EMAIL);
        if(profile.isTransient()){
        	recipientName = getGuestUserName();
        	recipientEmail= getGuestUserEmail();
        }
        // initialize the api client for the desired environment
        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(mBaseUrl);

        // create JSON formatted auth header
        String creds = "{\"Username\":\"" +  mUserName + "\",\"Password\":\"" +  mUserPassword + "\",\"IntegratorKey\":\"" +  mIntegratorKey + "\"}";
        apiClient.addDefaultHeader("X-DocuSign-Authentication", creds);

        // assign api client to the Configuration object
        Configuration.setDefaultApiClient(apiClient);

        // create an empty list that we will populate with accounts
        List<LoginAccount> loginAccounts = null;

        try {
            // login call available off the AuthenticationApi
            AuthenticationApi authApi = new AuthenticationApi();

            // login has some optional parameters we can set
            AuthenticationApi.LoginOptions loginOps = authApi.new LoginOptions();
            loginOps.setApiPassword("true");
            loginOps.setIncludeAccountIdGuid("true");
            LoginInformation loginInfo = authApi.login(loginOps);

            // note that a given user may be a member of multiple accounts
            loginAccounts = loginInfo.getLoginAccounts();

            vlogDebug("LoginInformation: " + loginAccounts);
        } catch (com.docusign.esign.client.ApiException ex) {
            vlogError(ex, "Error");
        }

        // create a byte array that will hold our document bytes
        byte[] fileBytes = null;

        try {
            // read file from a local directory
            fileBytes = Files.readAllBytes(file.toPath());
        } catch (IOException ioExcp) {
            // handle error
            vlogError(ioExcp, "Error");
            return null;
        }

        // create an envelope that will store the document(s), tabs(s), and recipient(s)
        EnvelopeDefinition envDef = new EnvelopeDefinition();
        envDef.setEmailSubject("[Columbia Pipeline] - Please sign this doc");

        // add a document to the envelope
        Document doc = new Document();
        String base64Doc = Base64.getEncoder().encodeToString(fileBytes);
        doc.setDocumentBase64(base64Doc);
        doc.setName("CreditApplication.pdf");    // can be different from actual file name
        doc.setDocumentId("1");

        List<Document> docs = new ArrayList<Document>();
        docs.add(doc);
        envDef.setDocuments(docs);

        // add a recipient to sign the document, identified by name and email we used above
        Signer signer = new Signer();
        signer.setName(recipientName);
        signer.setEmail(recipientEmail);
        signer.setRecipientId("1");

        // to embed the recipient you must set their |clientUserId| property!
        signer.setClientUserId("1234");

        // create a signHere tab somewhere on the document for the signer to sign
        // default unit of measurement is pixels, can be mms, cms, inches also
        SignHere signHere = new SignHere();
        signHere.setDocumentId("1");
        signHere.setPageNumber("2");
        signHere.setRecipientId("1");
        signHere.setXPosition("50");
        signHere.setYPosition("720");

        // can have multiple tabs, so need to add to envelope as a single element list
        List<SignHere> signHereTabs = new ArrayList<SignHere>();
        signHereTabs.add(signHere);
        Tabs tabs = new Tabs();
        tabs.setSignHereTabs(signHereTabs);
        signer.setTabs(tabs);

        // add recipients (in this case a single signer) to the envelope
        envDef.setRecipients(new Recipients());
        envDef.getRecipients().setSigners(new ArrayList<Signer>());
        envDef.getRecipients().getSigners().add(signer);

        // send the envelope by setting |status| to "sent". To save as a draft set to "created"
        envDef.setStatus("sent");

        // accountId is needed to create the envelope and for requesting the signer view
        String accountId = null;
        String envelopeId = null;

        try {
            if (loginAccounts != null && loginAccounts.size() > 0) {
                // use the |accountId| we retrieved through the Login API to create the Envelope
                accountId = loginAccounts.get(0).getAccountId();

                // instantiate a new EnvelopesApi object
                EnvelopesApi envelopesApi = new EnvelopesApi();

                // call the createEnvelope() API to send the signature request!
                EnvelopeSummary envelopeSummary = envelopesApi.createEnvelope(accountId, envDef);

                // save the |envelopeId| that was generated and use in next API call
                envelopeId = envelopeSummary.getEnvelopeId();

                vlogDebug("EnvelopeSummary: " + envelopeSummary);
            }
        } catch (com.docusign.esign.client.ApiException ex) {
            vlogError(ex, "Error");
        }

        // instantiate a new EnvelopesApi object
        EnvelopesApi envelopesApi = new EnvelopesApi();

        // set the url where you want the recipient to go once they are done signing
        RecipientViewRequest returnUrl = new RecipientViewRequest();
        
//        returnUrl.setReturnUrl("http://local.cps.com:8080");
        returnUrl.setReturnUrl(getEmailSender().getSiteUrl(getEmailSender().getSiteId()));
        
        returnUrl.setAuthenticationMethod("email");

        // recipient information must match embedded recipient info we provided in step #2
        returnUrl.setUserName(recipientName);
        returnUrl.setEmail(recipientEmail);
        returnUrl.setRecipientId("1");
        returnUrl.setClientUserId("1234");

        try {
            // call the CreateRecipientView API then navigate to the URL to start the signing session
            ViewUrl recipientView = envelopesApi.createRecipientView(accountId, envelopeId, returnUrl);
            vlogDebug("ViewUrl: " + recipientView);
            return recipientView.getUrl();

        } catch (com.docusign.esign.client.ApiException ex) {
            vlogError(ex, "Error");
        }
        return null;
    } // end EmbeddedSigning()

    public CommonEmailSender getEmailSender() {
        return mEmailSender;
    }

    public void setEmailSender(CommonEmailSender pEmailSender) {
        mEmailSender = pEmailSender;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String pUserName) {
        this.mUserName = pUserName;
    }

    public String getUserPassword() {
        return mUserPassword;
    }

    public void setUserPassword(String pUserPassword) {
        this.mUserPassword = pUserPassword;
    }

    public String getIntegratorKey() {
        return mIntegratorKey;
    }

    public void setIntegratorKey(String pIntegratorKey) {
        this.mIntegratorKey = pIntegratorKey;
    }

    public String getBaseUrl() {
        return mBaseUrl;
    }

    public void setBaseUrl(String pBaseUrl) {
        this.mBaseUrl = pBaseUrl;
    }

    public MutableRepository getCreditApplicationRepository() {
        return mCreditApplicationRepository;
    }

    public void setCreditApplicationRepository(MutableRepository pCreditApplicationRepository) {
        this.mCreditApplicationRepository = pCreditApplicationRepository;
    }

    public void saveCreditApplication(UserCreditApplication user, Profile profile) throws RepositoryException {
        MutableRepository repository = getCreditApplicationRepository();
        MutableRepositoryItem creditApplicationRepository;
        boolean creditApplicationExist = false;
        String organizationId;
        Object[] params = new Object[1];
        if(profile.isTransient()){
        	organizationId = getGuestUserOrganisation();
            params[0] = getGuestUserOrganisation();
        }else{
      	organizationId = user.getOrganizationId();
       params[0] = user.getOrganizationId();
        }
        RepositoryView creditApplicationView = getCreditApplicationRepository().getView(CPSConstants.CREDIT_APPLICATION_VIEW_NAME);
        RqlStatement rqlStatement = RqlStatement.parseRqlStatement(RETRIEVE_CREDIT_APPLICATION_RQL_QUERY);
        RepositoryItem[] results = rqlStatement.executeQuery(creditApplicationView, params);

        if (results != null && results.length != 0) {
            creditApplicationRepository = (MutableRepositoryItem) results[0];
            creditApplicationExist = true;
        } else {
            creditApplicationRepository = repository.createItem(CPSConstants.CREDIT_APPLICATION_VIEW_NAME);
            creditApplicationRepository.setPropertyValue(CPSConstants.ORGANIZATION_ID, organizationId);
        }

        vlogDebug("Setting values for credit app");
        creditApplicationRepository.setPropertyValue(CPSConstants.LEGAL_BUSINESS_NAME, user.getLegalBusinessName());
        creditApplicationRepository.setPropertyValue(CPSConstants.TRADE_NAME, user.getTradeName());
        creditApplicationRepository.setPropertyValue(CPSConstants.TYPE_ORGANIZATION, user.getOrganizationType());
        creditApplicationRepository.setPropertyValue(CPSConstants.CUSTOMER_TYPE, user.getCustomerType());
        creditApplicationRepository.setPropertyValue(CPSConstants.PHONE_NUMBER, user.getPhoneNumber());
        creditApplicationRepository.setPropertyValue(CPSConstants.FAX_NUMBER, user.getFaxNumber());
        creditApplicationRepository.setPropertyValue(CPSConstants.BUSINESS_TYPE_PROPERTY, user.getBusinessType());
        creditApplicationRepository.setPropertyValue(CPSConstants.BUSINESS_YEAR_ESTABLISHED, user.getBusinessYearEstablished());
        creditApplicationRepository.setPropertyValue(CPSConstants.BUSINESS_LICENCE_NUMBER, user.getBusinessLicenseNumber());
        creditApplicationRepository.setPropertyValue(CPSConstants.REQUIRED_PO, user.getRequiredPO());
        creditApplicationRepository.setPropertyValue(CPSConstants.TAX_EXEMPT, user.getTaxExempt());
        creditApplicationRepository.setPropertyValue(CPSConstants.TAX_NUMBER, user.getTaxNumber());

        creditApplicationRepository.setPropertyValue(CPSConstants.EXPECTED_ANNUAL_PURCHASES, user.getExpectedAnnualPurchases());
        creditApplicationRepository.setPropertyValue(CPSConstants.ORIGINAL_CREDIT_REQUEST, user.getOriginatedCreditRequest());
        creditApplicationRepository.setPropertyValue(CPSConstants.PERSONAL_GUARANTY, user.getPersonalGuaranty());
        creditApplicationRepository.setPropertyValue(CPSConstants.PERSONAL_GUARANTY_DEBTOR, user.getPersonalGuarantyDebtor());

        MutableRepositoryItem businessAddress;
        if (creditApplicationExist) {
            businessAddress = (MutableRepositoryItem) creditApplicationRepository.getPropertyValue(CPSConstants.ADDRESS_BUSINESS);
        } else {
            businessAddress = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
        }
        businessAddress.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getAddressBusiness().getStreet());
        businessAddress.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getAddressBusiness().getCity());
        businessAddress.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getAddressBusiness().getState());
        businessAddress.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getAddressBusiness().getZipCode());
        creditApplicationRepository.setPropertyValue(CPSConstants.ADDRESS_BUSINESS, businessAddress);

        MutableRepositoryItem billingAddress;
        if (creditApplicationExist) {
            billingAddress = (MutableRepositoryItem) creditApplicationRepository.getPropertyValue(CPSConstants.ADDRESS_BILLING);
        } else {
            billingAddress = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
        }
        billingAddress.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getAddressBilling().getStreet());
        billingAddress.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getAddressBilling().getCity());
        billingAddress.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getAddressBilling().getState());
        billingAddress.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getAddressBilling().getZipCode());
        creditApplicationRepository.setPropertyValue(CPSConstants.ADDRESS_BILLING, billingAddress);

        MutableRepositoryItem bankReference;
        MutableRepositoryItem bankReferenceAddress;
        if (creditApplicationExist) {
            bankReference = (MutableRepositoryItem) creditApplicationRepository.getPropertyValue(CPSConstants.BANK_REFERENCE);
            bankReferenceAddress = (MutableRepositoryItem) bankReference.getPropertyValue(CPSConstants.ADDRESS);
        } else {
            bankReference = repository.createItem(CPSConstants.BANK_REFERENCE_VIEW_NAME);
            bankReferenceAddress = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
        }
        bankReferenceAddress.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getBankReference().getAddress().getStreet());
        bankReferenceAddress.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getBankReference().getAddress().getCity());
        bankReferenceAddress.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getBankReference().getAddress().getState());
        bankReferenceAddress.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getBankReference().getAddress().getZipCode());
        bankReference.setPropertyValue(CPSConstants.ADDRESS, bankReferenceAddress);
        bankReference.setPropertyValue(CPSConstants.NAME, user.getBankReference().getName());
        bankReference.setPropertyValue(CPSConstants.ACCOUNT_NUMBER, user.getBankReference().getAccountNumber());
        bankReference.setPropertyValue(CPSConstants.ACCOUNT_OFFICER, user.getBankReference().getAccountOfficer());
        bankReference.setPropertyValue(CPSConstants.PHONE_NUMBER, user.getBankReference().getPhoneNumber());
        bankReference.setPropertyValue(CPSConstants.EMAIL, user.getBankReference().getEmail());
        creditApplicationRepository.setPropertyValue(CPSConstants.BANK_REFERENCE, bankReference);

        if (!user.getProductTypeList().isEmpty()) {
            StringBuilder types = new StringBuilder();
            String prefix = "";
            for (String type : user.getProductTypeList()) {
                if (type != null) {
                    types.append(prefix);
                    prefix = "--";
                    types.append(type);
                }
            }
            creditApplicationRepository.setPropertyValue(CPSConstants.PRODUCT_TYPE_LIST, String.valueOf(types));
        }

        MutableRepositoryItem taxExemptionCertificate;
        MutableRepositoryItem taxExemptionCertificateAddress;
        if (creditApplicationExist) {
            taxExemptionCertificate = (MutableRepositoryItem) creditApplicationRepository.getPropertyValue(CPSConstants.TAX_EXEMPTION_CERTIFICATE);
            taxExemptionCertificateAddress = (MutableRepositoryItem) taxExemptionCertificate.getPropertyValue(CPSConstants.ADDRESS);
        } else {
            taxExemptionCertificate = repository.createItem(CPSConstants.TAX_EXEMPTION_CERTIFICATE_VIEW_NAME);
            taxExemptionCertificateAddress = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
        }
        taxExemptionCertificateAddress.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getTaxExemptionCertificate().getAddress().getStreet());
        taxExemptionCertificateAddress.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getTaxExemptionCertificate().getAddress().getCity());
        taxExemptionCertificateAddress.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getTaxExemptionCertificate().getAddress().getState());
        taxExemptionCertificateAddress.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getTaxExemptionCertificate().getAddress().getZipCode());
        taxExemptionCertificate.setPropertyValue(CPSConstants.ADDRESS, taxExemptionCertificateAddress);
        //taxExemptionCertificate.setPropertyValue(CPSConstants.PURCHASE_TYPE, user.getTaxExemptionCertificate().getPurchaseType());
        taxExemptionCertificate.setPropertyValue(CPSConstants.FIRM_NAME, user.getTaxExemptionCertificate().getFirmName());
        taxExemptionCertificate.setPropertyValue(CPSConstants.REGISTERED_TYPE, user.getTaxExemptionCertificate().getRegisteredType());
        taxExemptionCertificate.setPropertyValue(CPSConstants.SPECIFY_REGISTERED_TYPE, user.getTaxExemptionCertificate().getSpecifyRegisteredType());
        taxExemptionCertificate.setPropertyValue(CPSConstants.BUYER_ACTIVITY_TYPE, user.getTaxExemptionCertificate().getBuyerActivityType());
        taxExemptionCertificate.setPropertyValue(CPSConstants.SPECIFY_BUYER_ACTIVITY_TYPE, user.getTaxExemptionCertificate().getSpecifyBuyerActivityType());
        taxExemptionCertificate.setPropertyValue(CPSConstants.BUSINESS_FOLLOWING, user.getTaxExemptionCertificate().getBusinessFollowing());
        taxExemptionCertificate.setPropertyValue(CPSConstants.GENERAL_DESCRIPTION_PRODUCTS, user.getTaxExemptionCertificate().getGeneralDescriptionProducts());
        taxExemptionCertificate.setPropertyValue(CPSConstants.TAX_EXEMPT_STATUS, user.getTaxExemptionCertificate().getTaxExemptStatus());

        if (!user.getTaxExemptionCertificate().getRegisteredStateList().isEmpty()) {
            StringBuilder states = new StringBuilder();
            String prefix = "";
            for (RegisteredState state : user.getTaxExemptionCertificate().getRegisteredStateList()) {
                states.append(prefix);
                prefix = ",";
                states.append(state.getState()).append("--").append(state.getStateRegistration());
            }
            taxExemptionCertificate.setPropertyValue(CPSConstants.REGISTERED_STATES, String.valueOf(states));
        }
        creditApplicationRepository.setPropertyValue(CPSConstants.TAX_EXEMPTION_CERTIFICATE, taxExemptionCertificate);

        MutableRepositoryItem resaleCertificate;
        MutableRepositoryItem sellerAddress;
        MutableRepositoryItem purchaserAddress;
        if (creditApplicationExist) {
            resaleCertificate = (MutableRepositoryItem) creditApplicationRepository.getPropertyValue(CPSConstants.RESALE_CERTIFICATE);
            sellerAddress = (MutableRepositoryItem) resaleCertificate.getPropertyValue(CPSConstants.SELLER_ADDRESS);
            purchaserAddress = (MutableRepositoryItem) resaleCertificate.getPropertyValue(CPSConstants.PURCHASER_ADDRESS);
        } else {
            resaleCertificate = repository.createItem(CPSConstants.RESALE_CERTIFICATE_VIEW_NAME);
            sellerAddress = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
            purchaserAddress = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
        }
        resaleCertificate.setPropertyValue(CPSConstants.SELLER_NAME, user.getResaleCertificate().getSellerName());
        sellerAddress.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getResaleCertificate().getSellerAddress().getStreet());
        sellerAddress.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getResaleCertificate().getSellerAddress().getCity());
        sellerAddress.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getResaleCertificate().getSellerAddress().getState());
        sellerAddress.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getResaleCertificate().getSellerAddress().getZipCode());
        resaleCertificate.setPropertyValue(CPSConstants.SELLER_ADDRESS, sellerAddress);

        resaleCertificate.setPropertyValue(CPSConstants.PURCHASER_NAME, user.getResaleCertificate().getPurchaserName());
        purchaserAddress.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getResaleCertificate().getPurchaserAddress().getStreet());
        purchaserAddress.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getResaleCertificate().getPurchaserAddress().getCity());
        purchaserAddress.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getResaleCertificate().getPurchaserAddress().getState());
        purchaserAddress.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getResaleCertificate().getPurchaserAddress().getZipCode());
        resaleCertificate.setPropertyValue(CPSConstants.PURCHASER_ADDRESS, purchaserAddress);

        resaleCertificate.setPropertyValue(CPSConstants.PURCHASER_REGISTERED_TYPE, user.getResaleCertificate().getPurchaserRegisteredType());
        resaleCertificate.setPropertyValue(CPSConstants.REGISTRATION_NUMBER, user.getResaleCertificate().getRegistrationNumber());
        resaleCertificate.setPropertyValue(CPSConstants.DESCRIBE_PROPERTY_RESALE, user.getResaleCertificate().getDescribePropertyResale());
        resaleCertificate.setPropertyValue(CPSConstants.PURCHASES_RESALE_TYPE, user.getResaleCertificate().getPurchasesResaleType());
        resaleCertificate.setPropertyValue(CPSConstants.PERCENTAGE_PURCHASES_RESALE, user.getResaleCertificate().getPercentagePurchasesResale());
        creditApplicationRepository.setPropertyValue(CPSConstants.RESALE_CERTIFICATE, resaleCertificate);

        List<MutableRepositoryItem> updatedCustomerRepositoryItems = new ArrayList<>();
        List<MutableRepositoryItem> customerRepositoryItems;
        if (creditApplicationExist) {
            customerRepositoryItems = (List<MutableRepositoryItem>) creditApplicationRepository.getPropertyValue(CPSConstants.CUSTOMERS);
        } else {
            customerRepositoryItems = updatedCustomerRepositoryItems;
        }

        for (int i = 0; i < user.getSupposedCustomerList().size(); i++) {
            MutableRepositoryItem customerItem;
            if (i >= customerRepositoryItems.size()) {
                customerItem = repository.createItem(CPSConstants.CUSTOMER_VIEW_NAME);
            } else {
                customerItem = customerRepositoryItems.get(i);
            }
            customerItem.setPropertyValue(CPSConstants.NAME, user.getSupposedCustomerList().get(i).getName());
            customerItem.setPropertyValue(CPSConstants.TITLE, user.getSupposedCustomerList().get(i).getTitle());
            customerItem.setPropertyValue(CPSConstants.PHONE_NUMBER, user.getSupposedCustomerList().get(i).getPhoneNumber());
            customerItem.setPropertyValue(CPSConstants.EMAIL, user.getSupposedCustomerList().get(i).getEmail());
            if (i >= customerRepositoryItems.size()) {
                repository.addItem(customerItem);
            } else {
                repository.updateItem(customerItem);
            }
            updatedCustomerRepositoryItems.add(customerItem);
        }

        creditApplicationRepository.setPropertyValue(CPSConstants.CUSTOMERS, updatedCustomerRepositoryItems);

        List<MutableRepositoryItem> principalRepositoryItems = (List<MutableRepositoryItem>) creditApplicationRepository.getPropertyValue(CPSConstants.PRINCIPALS);
        if (!creditApplicationExist) {
            MutableRepositoryItem principalItem = repository.createItem(CPSConstants.PRINCIPAL_VIEW_NAME);
            principalRepositoryItems.add(principalItem);
            principalItem = repository.createItem(CPSConstants.PRINCIPAL_VIEW_NAME);
            principalRepositoryItems.add(principalItem);
        }

        for (int i = 0; i < 2; i++) {
            principalRepositoryItems.get(i).setPropertyValue(CPSConstants.SOCIAL_SECURITY_NUMBER, user.getPrincipalOrganizationList().get(i).getSocialSecurityNumber());
            MutableRepositoryItem address;
            MutableRepositoryItem customer;
            if (creditApplicationExist) {
                address = (MutableRepositoryItem) principalRepositoryItems.get(i).getPropertyValue(CPSConstants.ADDRESS);
                customer = (MutableRepositoryItem) principalRepositoryItems.get(i).getPropertyValue(CPSConstants.CUSTOMER);
            } else {
                address = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
                customer = repository.createItem(CPSConstants.CUSTOMER_VIEW_NAME);
            }
            address.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getPrincipalOrganizationList().get(i).getAddress().getStreet());
            address.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getPrincipalOrganizationList().get(i).getAddress().getCity());
            address.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getPrincipalOrganizationList().get(i).getAddress().getState());
            address.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getPrincipalOrganizationList().get(i).getAddress().getZipCode());
            principalRepositoryItems.get(i).setPropertyValue(CPSConstants.ADDRESS, address);

            customer.setPropertyValue(CPSConstants.NAME, user.getPrincipalOrganizationList().get(i).getName());
            customer.setPropertyValue(CPSConstants.TITLE, user.getPrincipalOrganizationList().get(i).getTitle());
            customer.setPropertyValue(CPSConstants.PHONE_NUMBER, user.getPrincipalOrganizationList().get(i).getPhoneNumber());
            customer.setPropertyValue(CPSConstants.EMAIL, user.getPrincipalOrganizationList().get(i).getEmail());
            principalRepositoryItems.get(i).setPropertyValue(CPSConstants.CUSTOMER, customer);

            if (creditApplicationExist) {
                repository.updateItem(address);
                repository.updateItem(customer);
                repository.updateItem(principalRepositoryItems.get(i));
            } else {
                repository.addItem(address);
                repository.addItem(customer);
                repository.addItem(principalRepositoryItems.get(i));
            }
        }
        creditApplicationRepository.setPropertyValue(CPSConstants.PRINCIPALS, principalRepositoryItems);


        List<MutableRepositoryItem> updatedReferenceRepositoryItems = new ArrayList<>();
        List<MutableRepositoryItem> referenceRepositoryItems;
        if (creditApplicationExist) {
            referenceRepositoryItems = (List<MutableRepositoryItem>) creditApplicationRepository.getPropertyValue(CPSConstants.REFERENCES);
        } else {
            referenceRepositoryItems = updatedReferenceRepositoryItems;
        }

        for (int i = 0; i < user.getBusinessTradeReferenceList().size(); i++) {
            MutableRepositoryItem referenceItem;
            MutableRepositoryItem addressReferenceItem;
            if (i >= referenceRepositoryItems.size()) {
                referenceItem = repository.createItem(CPSConstants.TRADE_REFERENCE_VIEW_NAME);
                addressReferenceItem = repository.createItem(CPSConstants.ADDRESS_VIEW_NAME);
            } else {
                referenceItem = referenceRepositoryItems.get(i);
                addressReferenceItem = (MutableRepositoryItem) referenceRepositoryItems.get(i).getPropertyValue(CPSConstants.ADDRESS);
            }
            referenceItem.setPropertyValue(CPSConstants.NAME, user.getBusinessTradeReferenceList().get(i).getName());
            referenceItem.setPropertyValue(CPSConstants.PHONE_NUMBER, user.getBusinessTradeReferenceList().get(i).getPhoneNumber());
            referenceItem.setPropertyValue(CPSConstants.EMAIL, user.getBusinessTradeReferenceList().get(i).getEmail());
            addressReferenceItem.setPropertyValue(CPSConstants.ADDRESS_STREET, user.getBusinessTradeReferenceList().get(i).getAddress().getStreet());
            addressReferenceItem.setPropertyValue(CPSConstants.ADDRESS_CITY, user.getBusinessTradeReferenceList().get(i).getAddress().getCity());
            addressReferenceItem.setPropertyValue(CPSConstants.ADDRESS_STATE, user.getBusinessTradeReferenceList().get(i).getAddress().getState());
            addressReferenceItem.setPropertyValue(CPSConstants.ADDRESS_ZIP_CODE, user.getBusinessTradeReferenceList().get(i).getAddress().getZipCode());
            referenceItem.setPropertyValue(CPSConstants.ADDRESS, addressReferenceItem);
            if (i >= referenceRepositoryItems.size()) {
                repository.addItem(addressReferenceItem);
                repository.addItem(referenceItem);
            } else {
                repository.updateItem(addressReferenceItem);
                repository.updateItem(referenceItem);
            }
            updatedReferenceRepositoryItems.add(referenceItem);
        }

        creditApplicationRepository.setPropertyValue(CPSConstants.REFERENCES, updatedReferenceRepositoryItems);

        vlogDebug("Property values set, add creditApp Item to repository: " + creditApplicationRepository.getRepositoryId());

        if (creditApplicationExist) {
            repository.updateItem(businessAddress);
            repository.updateItem(billingAddress);
            repository.updateItem(bankReferenceAddress);
            repository.updateItem(bankReference);
            repository.updateItem(taxExemptionCertificateAddress);
            repository.updateItem(taxExemptionCertificate);
            repository.updateItem(sellerAddress);
            repository.updateItem(purchaserAddress);
            repository.updateItem(resaleCertificate);
            repository.updateItem(creditApplicationRepository);
        } else {
            repository.addItem(businessAddress);
            repository.addItem(billingAddress);
            repository.addItem(bankReferenceAddress);
            repository.addItem(bankReference);
            repository.addItem(taxExemptionCertificateAddress);
            repository.addItem(taxExemptionCertificate);
            repository.addItem(sellerAddress);
            repository.addItem(purchaserAddress);
            repository.addItem(resaleCertificate);
            repository.addItem(creditApplicationRepository);
        }

    }

	/**
	 * @return the guestUserName
	 */
	public String getGuestUserName() {
		return guestUserName;
	}

	/**
	 * @param guestUserName the guestUserName to set
	 */
	public void setGuestUserName(String guestUserName) {
		this.guestUserName = guestUserName;
	}

	/**
	 * @return the guestUserEmail
	 */
	public String getGuestUserEmail() {
		return guestUserEmail;
	}

	/**
	 * @param guestUserEmail the guestUserEmail to set
	 */
	public void setGuestUserEmail(String guestUserEmail) {
		this.guestUserEmail = guestUserEmail;
	}

	/**
	 * @return the guestUserOrganisation
	 */
	public String getGuestUserOrganisation() {
		return guestUserOrganisation;
	}

	/**
	 * @param guestUserOrganisation the guestUserOrganisation to set
	 */
	public void setGuestUserOrganisation(String guestUserOrganisation) {
		this.guestUserOrganisation = guestUserOrganisation;
	}
}
