package com.cps.userprofiling.credit.application.service;

import atg.core.util.StringUtils;
import atg.nucleus.naming.ParameterName;
import atg.repository.*;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;
import com.cps.droplet.DropletConstants;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.userprofiling.credit.application.pojo.*;
import com.cps.util.CPSConstants;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CreditApplicationDroplet extends DynamoServlet {

	private static final ParameterName USER_CREDIT_APPLICATION = ParameterName.getParameterName("userCreditApplication");
	private static final ParameterName PROFILE = ParameterName.getParameterName("profile");

	private MutableRepository mCreditApplicationRepository;
	private Repository mProfileRepository;
	private CPSProfileTools mProfileTools;

	private static final String RETRIEVE_CREDIT_APPLICATION_RQL_QUERY = "organizationId =?0";

	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		vlogDebug("CreditApplicationDroplet.start");

		UserCreditApplication userCreditApplication = (UserCreditApplication) pRequest.getObjectParameter(USER_CREDIT_APPLICATION);
		Profile profile = (Profile) pRequest.getObjectParameter(PROFILE);
		if (profile != null && userCreditApplication != null) {
			try {
				RepositoryItem parentOrganization = getProfileTools().getParentOrganization(profile);

				String organizationId = null;
				if (parentOrganization != null) {
					organizationId = parentOrganization.getRepositoryId();
					userCreditApplication.setOrganizationId(organizationId);
				}

				Object[] params = new Object[1];
				params[0] = organizationId;

//            MutableRepositoryItem repositoryItem = getCreditApplicationRepository().getItemForUpdate(organizationId, CPSConstants.CREDIT_APPLICATION_VIEW_NAME);

				RepositoryView creditApplicationView = getCreditApplicationRepository().getView(CPSConstants.CREDIT_APPLICATION_VIEW_NAME);
				RqlStatement rqlStatement = RqlStatement.parseRqlStatement(RETRIEVE_CREDIT_APPLICATION_RQL_QUERY);
				RepositoryItem[] results = rqlStatement.executeQuery(creditApplicationView, params);

				if (results != null && results.length != 0) {
					RepositoryItem creditApplication = results[0];
					RepositoryItem repositoryItem;
					RepositoryItem addressRepositoryItem;
					RepositoryItem customerItemRepository;

					userCreditApplication.setLegalBusinessName((String) creditApplication.getPropertyValue(CPSConstants.LEGAL_BUSINESS_NAME));
					userCreditApplication.setTradeName((String) creditApplication.getPropertyValue(CPSConstants.TRADE_NAME));
					userCreditApplication.setOrganizationType((String) creditApplication.getPropertyValue(CPSConstants.TYPE_ORGANIZATION));
					userCreditApplication.setCustomerType((String) creditApplication.getPropertyValue(CPSConstants.CUSTOMER_TYPE));
					userCreditApplication.setPhoneNumber((String) creditApplication.getPropertyValue(CPSConstants.PHONE_NUMBER));
					userCreditApplication.setFaxNumber((String) creditApplication.getPropertyValue(CPSConstants.FAX_NUMBER));
					userCreditApplication.setBusinessType((String) creditApplication.getPropertyValue(CPSConstants.BUSINESS_TYPE_PROPERTY));
					userCreditApplication.setBusinessYearEstablished((String) creditApplication.getPropertyValue(CPSConstants.BUSINESS_YEAR_ESTABLISHED));
					userCreditApplication.setBusinessLicenseNumber((String) creditApplication.getPropertyValue(CPSConstants.BUSINESS_LICENCE_NUMBER));
					userCreditApplication.setRequiredPO((String) creditApplication.getPropertyValue(CPSConstants.REQUIRED_PO));
					userCreditApplication.setTaxExempt((String) creditApplication.getPropertyValue(CPSConstants.TAX_EXEMPT));
					userCreditApplication.setTaxNumber((String) creditApplication.getPropertyValue(CPSConstants.TAX_NUMBER));


					userCreditApplication.setExpectedAnnualPurchases((String) creditApplication.getPropertyValue(CPSConstants.EXPECTED_ANNUAL_PURCHASES));
					userCreditApplication.setOriginatedCreditRequest((String) creditApplication.getPropertyValue(CPSConstants.ORIGINAL_CREDIT_REQUEST));
					userCreditApplication.setPersonalGuaranty((String) creditApplication.getPropertyValue(CPSConstants.PERSONAL_GUARANTY));
					userCreditApplication.setPersonalGuarantyDebtor((String) creditApplication.getPropertyValue(CPSConstants.PERSONAL_GUARANTY_DEBTOR));

					repositoryItem = (RepositoryItem) creditApplication.getPropertyValue(CPSConstants.ADDRESS_BUSINESS);
					userCreditApplication.setAddressBusiness(new Address((String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
							(String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
							(String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE), (String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE)));

					repositoryItem = (RepositoryItem) creditApplication.getPropertyValue(CPSConstants.ADDRESS_BILLING);
					userCreditApplication.setAddressBilling(new Address((String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
							(String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
							(String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE), (String) repositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE)));

					repositoryItem = (RepositoryItem) creditApplication.getPropertyValue(CPSConstants.BANK_REFERENCE);
					addressRepositoryItem = (RepositoryItem) repositoryItem.getPropertyValue(CPSConstants.ADDRESS);
					userCreditApplication.setBankReference(new BankReference((String) repositoryItem.getPropertyValue(CPSConstants.NAME),
							(String) repositoryItem.getPropertyValue(CPSConstants.ACCOUNT_NUMBER), (String) repositoryItem.getPropertyValue(CPSConstants.ACCOUNT_OFFICER),
							new Address((String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
									(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
									(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE),
									(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE)),
							(String) repositoryItem.getPropertyValue(CPSConstants.PHONE_NUMBER), (String) repositoryItem.getPropertyValue(CPSConstants.EMAIL)));

					String productTypes = (String) creditApplication.getPropertyValue(CPSConstants.PRODUCT_TYPE_LIST);
					if (!StringUtils.isBlank(productTypes)) {
						String[] types = productTypes.split("--");
						List<String> productTypeList = new ArrayList<>();
						productTypeList.addAll(Arrays.asList(types));
						userCreditApplication.setProductTypeList(productTypeList);
					}

					TaxExemptionCertificate taxExemptionCertificate = new TaxExemptionCertificate();
					repositoryItem = (RepositoryItem) creditApplication.getPropertyValue(CPSConstants.TAX_EXEMPTION_CERTIFICATE);
					addressRepositoryItem = (RepositoryItem) repositoryItem.getPropertyValue(CPSConstants.ADDRESS);

					Address address = new Address((String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE));
					taxExemptionCertificate.setAddress(address);

					String registeredStates = (String) repositoryItem.getPropertyValue(CPSConstants.REGISTERED_STATES);
					if (!StringUtils.isBlank(registeredStates)) {
						String[] states = registeredStates.split(",");
						List<RegisteredState> stateList = new ArrayList<>();
						for (int i = 0; i < states.length; i++) {
							String[] singleState = states[i].split("--");
							stateList.add(new RegisteredState(singleState[0], singleState[1]));
						}
						taxExemptionCertificate.setRegisteredStateList(stateList);
					}
					//taxExemptionCertificate.setPurchaseType((String) repositoryItem.getPropertyValue(CPSConstants.PURCHASE_TYPE));
					taxExemptionCertificate.setTaxExemptStatus((String) repositoryItem.getPropertyValue(CPSConstants.TAX_EXEMPT_STATUS));
					taxExemptionCertificate.setFirmName((String) repositoryItem.getPropertyValue(CPSConstants.FIRM_NAME));
					taxExemptionCertificate.setRegisteredType((String) repositoryItem.getPropertyValue(CPSConstants.REGISTERED_TYPE));
					taxExemptionCertificate.setSpecifyRegisteredType((String) repositoryItem.getPropertyValue(CPSConstants.SPECIFY_REGISTERED_TYPE));
					taxExemptionCertificate.setBuyerActivityType((String) repositoryItem.getPropertyValue(CPSConstants.BUYER_ACTIVITY_TYPE));
					taxExemptionCertificate.setSpecifyBuyerActivityType((String) repositoryItem.getPropertyValue(CPSConstants.SPECIFY_BUYER_ACTIVITY_TYPE));
					taxExemptionCertificate.setBusinessFollowing((String) repositoryItem.getPropertyValue(CPSConstants.BUSINESS_FOLLOWING));
					taxExemptionCertificate.setGeneralDescriptionProducts((String) repositoryItem.getPropertyValue(CPSConstants.GENERAL_DESCRIPTION_PRODUCTS));

					userCreditApplication.setTaxExemptionCertificate(taxExemptionCertificate);

					ResaleCertificate resaleCertificate = new ResaleCertificate();
					repositoryItem = (RepositoryItem) creditApplication.getPropertyValue(CPSConstants.RESALE_CERTIFICATE);
					resaleCertificate.setSellerName((String) repositoryItem.getPropertyValue(CPSConstants.SELLER_NAME));
					addressRepositoryItem = (RepositoryItem) repositoryItem.getPropertyValue(CPSConstants.SELLER_ADDRESS);
					resaleCertificate.setSellerAddress(new Address((String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE)));

					resaleCertificate.setPurchaserName((String) repositoryItem.getPropertyValue(CPSConstants.PURCHASER_NAME));
					addressRepositoryItem = (RepositoryItem) repositoryItem.getPropertyValue(CPSConstants.PURCHASER_ADDRESS);
					resaleCertificate.setPurchaserAddress(new Address((String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE),
							(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE)));

					resaleCertificate.setPurchaserRegisteredType((String) repositoryItem.getPropertyValue(CPSConstants.PURCHASER_REGISTERED_TYPE));
					resaleCertificate.setRegistrationNumber((String) repositoryItem.getPropertyValue(CPSConstants.REGISTRATION_NUMBER));
					resaleCertificate.setDescribePropertyResale((String) repositoryItem.getPropertyValue(CPSConstants.DESCRIBE_PROPERTY_RESALE));
					resaleCertificate.setPurchasesResaleType((String) repositoryItem.getPropertyValue(CPSConstants.PURCHASES_RESALE_TYPE));
					resaleCertificate.setPercentagePurchasesResale((String) repositoryItem.getPropertyValue(CPSConstants.PERCENTAGE_PURCHASES_RESALE));

					userCreditApplication.setResaleCertificate(resaleCertificate);

					List<RepositoryItem> repositoryItems = (List<RepositoryItem>) creditApplication.getPropertyValue(CPSConstants.PRINCIPALS);
					userCreditApplication.getPrincipalOrganizationList().clear();
					for (RepositoryItem item : repositoryItems) {
						addressRepositoryItem = (RepositoryItem) item.getPropertyValue(CPSConstants.ADDRESS);
						customerItemRepository = (RepositoryItem) item.getPropertyValue(CPSConstants.CUSTOMER);
						String socialSecurityNumber = (String) item.getPropertyValue(CPSConstants.SOCIAL_SECURITY_NUMBER);
						userCreditApplication.getPrincipalOrganizationList().add(new PrincipalOrganization((String) customerItemRepository.getPropertyValue(CPSConstants.NAME),
								(String) customerItemRepository.getPropertyValue(CPSConstants.TITLE), (String) customerItemRepository.getPropertyValue(CPSConstants.PHONE_NUMBER),
								socialSecurityNumber, (String) customerItemRepository.getPropertyValue(CPSConstants.EMAIL),
								new Address((String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
										(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
										(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE),
										(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE))));
					}

					repositoryItems = (List<RepositoryItem>) creditApplication.getPropertyValue(CPSConstants.REFERENCES);
					userCreditApplication.getBusinessTradeReferenceList().clear();
					for (RepositoryItem item : repositoryItems) {
						String name = (String) item.getPropertyValue(CPSConstants.NAME);
						String phoneNumber = (String) item.getPropertyValue(CPSConstants.PHONE_NUMBER);
						String email = (String) item.getPropertyValue(CPSConstants.EMAIL);
						addressRepositoryItem = (RepositoryItem) item.getPropertyValue(CPSConstants.ADDRESS);
						userCreditApplication.getBusinessTradeReferenceList().add(new BusinessTradeReference(name,
								new Address((String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STREET),
										(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_CITY),
										(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_STATE),
										(String) addressRepositoryItem.getPropertyValue(CPSConstants.ADDRESS_ZIP_CODE)), phoneNumber, email));
					}

					repositoryItems = (List<RepositoryItem>) creditApplication.getPropertyValue(CPSConstants.CUSTOMERS);
					userCreditApplication.getSupposedCustomerList().clear();
					for (RepositoryItem item : repositoryItems) {
						userCreditApplication.getSupposedCustomerList().add(new Customer((String) item.getPropertyValue(CPSConstants.NAME),
								(String) item.getPropertyValue(CPSConstants.TITLE), (String) item.getPropertyValue(CPSConstants.PHONE_NUMBER),
								(String) item.getPropertyValue(CPSConstants.EMAIL)));
					}
				}
			} catch (RepositoryException e) {
				vlogError(e, "Error");
			}
		}

		pRequest.setParameter(DropletConstants.NUM_PAGES, null);
		pRequest.serviceLocalParameter(DropletConstants.OUTPUT, pRequest, pResponse);

		vlogDebug("CreditApplicationDroplet.end");
	}


	public MutableRepository getCreditApplicationRepository() {
		return mCreditApplicationRepository;
	}

	public void setCreditApplicationRepository(MutableRepository pCreditApplicationRepository) {
		mCreditApplicationRepository = pCreditApplicationRepository;
	}

	public Repository getProfileRepository() {
		return mProfileRepository;
	}

	public void setProfileRepository(Repository pProfileRepository) {
		mProfileRepository = pProfileRepository;
	}
	//------------------------------------------------------------------------------------------------------------------

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

}
