package com.cps.userprofiling.credit.application.pojo;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Meleshko
 */
public class TaxExemptionCertificate {
    private String taxExemptStatus;
    private String firmName;
    private Address address;

    private String registeredType;
    private String specifyRegisteredType;

    private List<RegisteredState> registeredStateList;

    private String buyerActivityType;
    private String specifyBuyerActivityType;

    private String businessFollowing;
    private String generalDescriptionProducts;

    public TaxExemptionCertificate() {
        this.registeredStateList = new ArrayList<>();
    }
    
	public String getTaxExemptStatus() {
		return taxExemptStatus;
	}

	public void setTaxExemptStatus(String taxExemptStatus) {
		this.taxExemptStatus = taxExemptStatus;
	}

	public String getFirmName() {
        return firmName;
    }

    public void setFirmName(String firmName) {
        this.firmName = firmName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRegisteredType() {
        return registeredType;
    }

    public void setRegisteredType(String registeredType) {
        this.registeredType = registeredType;
    }

    public String getSpecifyRegisteredType() {
        return specifyRegisteredType;
    }

    public void setSpecifyRegisteredType(String specifyRegisteredType) {
        this.specifyRegisteredType = specifyRegisteredType;
    }

    public List<RegisteredState> getRegisteredStateList() {
        return registeredStateList;
    }

    public void setRegisteredStateList(List<RegisteredState> registeredStateList) {
        this.registeredStateList = registeredStateList;
    }

    public String getBuyerActivityType() {
        return buyerActivityType;
    }

    public void setBuyerActivityType(String buyerActivityType) {
        this.buyerActivityType = buyerActivityType;
    }

    public String getSpecifyBuyerActivityType() {
        return specifyBuyerActivityType;
    }

    public void setSpecifyBuyerActivityType(String specifyBuyerActivityType) {
        this.specifyBuyerActivityType = specifyBuyerActivityType;
    }

    public String getBusinessFollowing() {
        return businessFollowing;
    }

    public void setBusinessFollowing(String businessFollowing) {
        this.businessFollowing = businessFollowing;
    }

    public String getGeneralDescriptionProducts() {
        return generalDescriptionProducts;
    }

    public void setGeneralDescriptionProducts(String generalDescriptionProducts) {
        this.generalDescriptionProducts = generalDescriptionProducts;
    }
}
