package com.cps.userprofiling.credit.application.validator;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import com.cps.userprofiling.credit.application.handler.CreditApplicationFormHandler;
import com.cps.userprofiling.credit.application.pojo.*;
import com.cps.userprofiling.validator.BaseValidator;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

/**
 * @author Alexey Meleshko
 */
public class CreditApplicationValidator extends BaseValidator {
    public boolean validateCreditApplicationForm(CreditApplicationFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
        boolean valid = true;
        return valid;
    }

    public boolean validateUserApplicationCredit(CreditApplicationFormHandler pFormHandler,
                                                 UserCreditApplication currentUserApplicationCredit,
                                                 DynamoHttpServletRequest pRequest) {
        boolean valid = true;

        if (StringUtils.isBlank(currentUserApplicationCredit.getLegalBusinessName())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_name", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getTradeName())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_trade_name", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getOrganizationType())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_organization", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getCustomerType())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_customer", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPhoneNumber())
                || !currentUserApplicationCredit.getPhoneNumber().matches(CPSConstants.REG_EXP_PHONE)) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_phone", pRequest);
        }
//        if (StringUtils.isBlank(currentUserApplicationCredit.getFaxNumber())
//                || !currentUserApplicationCredit.getFaxNumber().matches(CPSConstants.REG_EXP_PHONE)) {
//            valid = false;
//            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_fax", pRequest);
//        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getBusinessType())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_type", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getBusinessYearEstablished())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_year_established", pRequest);
        }
//        if (StringUtils.isBlank(currentUserApplicationCredit.getBusinessLicenseNumber())) {
//            valid = false;
//            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_license", pRequest);
//        }


        if (StringUtils.isBlank(currentUserApplicationCredit.getRequiredPO())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_po", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getTaxExempt())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_exempt", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getTaxNumber()) && "Yes".equals(currentUserApplicationCredit.getTaxExempt())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_exempt_number", pRequest);
        }


        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBusiness().getStreet())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_street", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBusiness().getCity())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_city", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBusiness().getState())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_state", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBusiness().getZipCode())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_code", pRequest);
        }


        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBilling().getStreet())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_billing_street", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBilling().getCity())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_billing_city", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBilling().getState())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_billing_state", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getAddressBilling().getZipCode())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_billing_code", pRequest);
        }


        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getName())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_name_1", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getTitle())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_title_1", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getEmail()) ||
                !currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getEmail().matches(CPSConstants.REG_EXP_EMAIL)) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_email_1", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getPhoneNumber())
                || !currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getPhoneNumber().matches(CPSConstants.REG_EXP_PHONE)) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_phone_1", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getAddress().getStreet())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_address_1", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getAddress().getCity())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_city_1", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getAddress().getState())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_state_1", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(0).getAddress().getZipCode())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_code_1", pRequest);
        }


        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getName())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_name_2", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getTitle())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_title_2", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getEmail()) ||
                !currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getEmail().matches(CPSConstants.REG_EXP_EMAIL)) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_email_2", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getPhoneNumber())
                || !currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getPhoneNumber().matches(CPSConstants.REG_EXP_PHONE)) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_phone_2", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getAddress().getStreet())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_address_2", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getAddress().getCity())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_city_2", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getAddress().getState())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_state_2", pRequest);
        }
        if (StringUtils.isBlank(currentUserApplicationCredit.getPrincipalOrganizationList().get(1).getAddress().getZipCode())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_principal_code_2", pRequest);
        }

        if (currentUserApplicationCredit.getBusinessTradeReferenceList().isEmpty()) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_name_1", pRequest);
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_address_1", pRequest);
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_city_1", pRequest);
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_state_1", pRequest);
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_zip_1", pRequest);
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_phone_1", pRequest);
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_email_1", pRequest);
        } else {
            int number = 1;
            for (BusinessTradeReference reference : currentUserApplicationCredit.getBusinessTradeReferenceList()) {
                if (StringUtils.isBlank(reference.getName())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_name_" + number, pRequest);
                }
                if (StringUtils.isBlank(reference.getAddress().getStreet())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_address_" + number, pRequest);
                }
                if (StringUtils.isBlank(reference.getAddress().getCity())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_city_" + number, pRequest);
                }
                if (StringUtils.isBlank(reference.getAddress().getState())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_state_" + number, pRequest);
                }
                if (StringUtils.isBlank(reference.getAddress().getZipCode())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_zip_" + number, pRequest);
                }
                if (StringUtils.isBlank(reference.getPhoneNumber()) || !reference.getPhoneNumber().matches(CPSConstants.REG_EXP_PHONE)) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_phone_" + number, pRequest);
                }
                if (StringUtils.isBlank(reference.getEmail()) || !reference.getEmail().matches(CPSConstants.REG_EXP_EMAIL)) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_business_trade_email_" + number, pRequest);
                }
                number++;
            }
        }

        return valid;
    }

    public boolean validateCustomerProfile(CreditApplicationFormHandler pFormHandler, UserCreditApplication currentUserApplicationCredit, DynamoHttpServletRequest pRequest) {
        boolean valid = true;
        return valid;
    }

    public boolean validateTaxExemptionCertificate(CreditApplicationFormHandler pFormHandler, TaxExemptionCertificate taxExemptionCertificate, DynamoHttpServletRequest pRequest) {
        boolean valid = true;
        if (StringUtils.isBlank(taxExemptionCertificate.getTaxExemptStatus())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_exempt_status", pRequest);
        }
        if(StringUtils.isNotBlank(taxExemptionCertificate.getTaxExemptStatus()) && taxExemptionCertificate.getTaxExemptStatus().equals("Yes")){
        if (StringUtils.isBlank(taxExemptionCertificate.getFirmName())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_firm_name", pRequest);
        }
        if (StringUtils.isBlank(taxExemptionCertificate.getAddress().getStreet())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_address", pRequest);
        }
        if (StringUtils.isBlank(taxExemptionCertificate.getAddress().getState())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_state", pRequest);
        }
        if (StringUtils.isBlank(taxExemptionCertificate.getAddress().getCity())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_city", pRequest);
        }
        if (StringUtils.isBlank(taxExemptionCertificate.getAddress().getZipCode())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_zip", pRequest);
        }
        if (StringUtils.isBlank(taxExemptionCertificate.getRegisteredType())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_engaged", pRequest);
        }

        if (taxExemptionCertificate.getRegisteredStateList().isEmpty()) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_registered_state_1", pRequest);
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_registered_id_1", pRequest);
        } else {
            int number = 1;
            for (RegisteredState registeredState : taxExemptionCertificate.getRegisteredStateList()) {
                if (StringUtils.isBlank(registeredState.getState())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_registered_state_" + number, pRequest);
                }
                if (StringUtils.isBlank(registeredState.getStateRegistration())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_tax_registered_id_" + number, pRequest);
                }
                number++;
            }
        }
        }


        return valid;
    }

    public boolean validateResaleCertificate(CreditApplicationFormHandler pFormHandler, ResaleCertificate resaleCertificate, DynamoHttpServletRequest pRequest) {
        boolean valid = true;
        if (StringUtils.isBlank(resaleCertificate.getSellerName())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_name_step_1", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getSellerAddress().getStreet())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_address_step_1", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getSellerAddress().getCity())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_city_step_1", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getSellerAddress().getState())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_state_step_1", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getSellerAddress().getZipCode())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_zip_step_1", pRequest);
        }

        if (StringUtils.isBlank(resaleCertificate.getPurchaserName())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_name_step_2", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getPurchaserAddress().getStreet())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_address_step_2", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getPurchaserAddress().getCity())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_city_step_2", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getPurchaserAddress().getState())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_state_step_2", pRequest);
        }
        if (StringUtils.isBlank(resaleCertificate.getPurchaserAddress().getZipCode())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_zip_step_2", pRequest);
        }

        if (StringUtils.isBlank(resaleCertificate.getPurchaserRegisteredType())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_registered", pRequest);
        } else {
            if ("retailer".equals(resaleCertificate.getPurchaserRegisteredType()) || "reseller".equals(resaleCertificate.getPurchaserRegisteredType())) {
                if (StringUtils.isBlank(resaleCertificate.getRegistrationNumber())) {
                    valid = false;
                    createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_registered_number", pRequest);
                }
            }
        }

        if (StringUtils.isBlank(resaleCertificate.getDescribePropertyResale())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_describe_property", pRequest);
        }

        if (StringUtils.isBlank(resaleCertificate.getPurchasesResaleType())) {
            valid = false;
            createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_purchases_resale_type", pRequest);
        } else {
            if ("partly".equals(resaleCertificate.getPurchasesResaleType()) && StringUtils.isBlank(resaleCertificate.getPercentagePurchasesResale())) {
                valid = false;
                createError(pFormHandler, CPSErrorCodes.ERR_CREDIT_APPLICATION, "ca_input_certificate_percentage_partly", pRequest);
            }
        }
        return valid;
    }
}
