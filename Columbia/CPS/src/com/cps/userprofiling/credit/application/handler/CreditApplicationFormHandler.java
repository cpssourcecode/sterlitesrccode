package com.cps.userprofiling.credit.application.handler;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.cps.userprofiling.credit.application.pdf.PDFApplicationCreditGenerator;
import com.cps.userprofiling.credit.application.pojo.ResaleCertificate;
import com.cps.userprofiling.credit.application.pojo.TaxExemptionCertificate;
import com.cps.userprofiling.credit.application.pojo.UserCreditApplication;
import com.cps.userprofiling.credit.application.service.CreditApplicationService;
import com.cps.userprofiling.credit.application.validator.CreditApplicationValidator;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import vsg.util.ajax.AjaxUtils;

/**
 * @author Alexey Meleshko
 */
public class CreditApplicationFormHandler extends GenericFormHandler {
    /**
     * values
     */
    private Map<String, String> mValue = new HashMap<>();
    /**
     * credit application service
     */
    private CreditApplicationService mService;
    /**
     * credit application validator
     */
    private CreditApplicationValidator mValidator;
    /**
     * user credit application session bean
     */
    private UserCreditApplication mUser;

    private String mSelectedOption;

    private RepeatingRequestMonitor mRepeatingRequestMonitor;

    private TransactionManager mTransactionManager;
    
    private Profile mProfile;

    //------------------------------------------------------------------------------------------------------------------

    /**
     * @param pRequest  request
     * @param pResponse response
     * @return false
     * @throws ServletException if error occurs
     * @throws IOException      if error occurs
     */
    public boolean handleSendMessage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {

        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CreditApplicationFormHandler.handleSendMessage";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                UserCreditApplication currentUserApplicationCredit = getService().createUserApplicationCredit(pRequest);

                if (getValidator().validateUserApplicationCredit(this, currentUserApplicationCredit, pRequest)) {
                    mUser.setLegalBusinessName(currentUserApplicationCredit.getLegalBusinessName());
                    mUser.setTradeName(currentUserApplicationCredit.getTradeName());
                    mUser.setPhoneNumber(currentUserApplicationCredit.getPhoneNumber());
                    mUser.setFaxNumber(currentUserApplicationCredit.getFaxNumber());
                    mUser.setAddressBusiness(currentUserApplicationCredit.getAddressBusiness());
                    mUser.setAddressBilling(currentUserApplicationCredit.getAddressBilling());
                    mUser.setPrincipalOrganizationList(currentUserApplicationCredit.getPrincipalOrganizationList());
                    mUser.setOrganizationType(currentUserApplicationCredit.getOrganizationType());
                    mUser.setBusinessType(currentUserApplicationCredit.getBusinessType());
                    mUser.setBusinessYearEstablished(currentUserApplicationCredit.getBusinessYearEstablished());
                    mUser.setBusinessLicenseNumber(currentUserApplicationCredit.getBusinessLicenseNumber());
                    mUser.setBusinessTradeReferenceList(currentUserApplicationCredit.getBusinessTradeReferenceList());
                    mUser.setBankReference(currentUserApplicationCredit.getBankReference());
                    mUser.setCustomerType(currentUserApplicationCredit.getCustomerType());
                    mUser.setRequiredPO(currentUserApplicationCredit.getRequiredPO());
                    mUser.setTaxExempt(currentUserApplicationCredit.getTaxExempt());
                    mUser.setTaxNumber(currentUserApplicationCredit.getTaxNumber());
                    mUser.setMail(getValue().get("email"));
                    mUser.setName(getValue().get("firstName"));
                }
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }

        AjaxUtils.addAjaxResponse(this, pResponse, null);
        return false;
    }

    public boolean handleCustomerProfile(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CreditApplicationFormHandler.handleCustomerProfile";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                UserCreditApplication profileUserApplicationCredit = getService().createCustomerProfile(pRequest);

                if (getValidator().validateCustomerProfile(this, profileUserApplicationCredit, pRequest)) {
                    mUser.setProductTypeList(profileUserApplicationCredit.getProductTypeList());
                    mUser.setExpectedAnnualPurchases(profileUserApplicationCredit.getExpectedAnnualPurchases());
                    mUser.setSupposedCustomerList(profileUserApplicationCredit.getSupposedCustomerList());
                    mUser.setOriginatedCreditRequest(profileUserApplicationCredit.getOriginatedCreditRequest());
                    mUser.setPersonalGuaranty(profileUserApplicationCredit.getPersonalGuaranty());
                    mUser.setPersonalGuarantyDebtor(profileUserApplicationCredit.getPersonalGuarantyDebtor());
                }
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        AjaxUtils.addAjaxResponse(this, pResponse, null);
        return false;
    }

    public boolean handleTaxExemptionCertificate(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CreditApplicationFormHandler.handleTaxExemptionCertificate";

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            try {
                TaxExemptionCertificate taxExemptionCertificate = getService().createTaxExemptionCertificate(pRequest);

                if (getValidator().validateTaxExemptionCertificate(this, taxExemptionCertificate, pRequest)) {
                    mUser.setTaxExemptionCertificate(taxExemptionCertificate);
                }
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
            }
        }
        AjaxUtils.addAjaxResponse(this, pResponse, null);
        return false;
    }

    public boolean handleResaleCertificate(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
            throws ServletException, IOException {
        final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
        final String handleMethodName = "CreditApplicationFormHandler.handleResaleCertificate";

        Map<String, Object> map = new HashMap<>();

        if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
            TransactionManager tm = getTransactionManager();
            TransactionDemarcation td = new TransactionDemarcation();
            boolean rollback = false;
            try {
                if (tm != null){
                    td.begin(tm, TransactionDemarcation.REQUIRED);
                }
                ResaleCertificate resaleCertificate = getService().createResaleCertificate(pRequest);
                if (getValidator().validateResaleCertificate(this, resaleCertificate, pRequest)) {
                    mUser.setResaleCertificate(resaleCertificate);
                    getService().saveCreditApplication(mUser, getProfile());
                    if (getSelectedOption().equals("sign")) {
                        PDFApplicationCreditGenerator generator = new PDFApplicationCreditGenerator(mUser);
                        File file = generator.createFile();
                        getService().sendEmail(this, pRequest, pResponse, mUser, file);
                    }
                }
            } catch (Exception e) {
                if (isLoggingError()){
                    logError(e);
                }
                rollback = true;
            } finally {
                if (rrm != null){
                    rrm.removeRequestEntry(handleMethodName);
                }
                if (tm != null){
                    try {
                        td.end(rollback);
                    } catch (TransactionDemarcationException e) {
                        if(isLoggingError()){
                            logError(e);
                        }
                    }
                }
            }
        }
        AjaxUtils.addAjaxResponse(this, pResponse, null);
        return false;
    }

    public Map<String, String> getValue() {
        return mValue;
    }

    public void setValue(Map<String, String> pValue) {
        this.mValue = pValue;
    }

    public CreditApplicationService getService() {
        return mService;
    }

    public void setService(CreditApplicationService pService) {
        this.mService = pService;
    }

    public CreditApplicationValidator getValidator() {
        return mValidator;
    }

    public void setValidator(CreditApplicationValidator pValidator) {
        this.mValidator = pValidator;
    }

    public UserCreditApplication getUser() {
        return mUser;
    }

    public void setUser(UserCreditApplication pUser) {
        this.mUser = pUser;
    }

    public String getSelectedOption() {
        return mSelectedOption;
    }

    public void setSelectedOption(String pSelectedOption) {
        this.mSelectedOption = pSelectedOption;
    }

    public RepeatingRequestMonitor getRepeatingRequestMonitor() {
        return mRepeatingRequestMonitor;
    }

    public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
        mRepeatingRequestMonitor = pRepeatingRequestMonitor;
    }

    public TransactionManager getTransactionManager() {
        return mTransactionManager;
    }

    public void setTransactionManager(TransactionManager pTransactionManager) {
        mTransactionManager = pTransactionManager;
    }

    /**
     * @return the profile
     */
    public Profile getProfile() {
        return mProfile;
    }

    /**
     * @param profile the profile to set
     */
    public void setProfile(Profile profile) {
        this.mProfile = profile;
    }
}
