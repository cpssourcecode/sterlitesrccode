package com.cps.userprofiling.credit.application.pdf;

import atg.core.util.StringUtils;
import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import com.cps.userprofiling.credit.application.pojo.BusinessTradeReference;
import com.cps.userprofiling.credit.application.pojo.Customer;
import com.cps.userprofiling.credit.application.pojo.RegisteredState;
import com.cps.userprofiling.credit.application.pojo.UserCreditApplication;
import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.List;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.*;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * @author Alexey Meleshko
 */
public class PDFApplicationCreditGenerator {

    private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(PDFApplicationCreditGenerator.class);


    private UserCreditApplication mUser;

    private static final Color GREEN_COLOR = new Color(0, 102, 0);
    private static final Color WHITE_COLOR = new Color(255, 255, 255);
    private static final Font NORMAL_8 = FontFactory.getFont(FontFactory.COURIER, 8);
    private static final Font NORMAL_10 = FontFactory.getFont(FontFactory.COURIER, 10);
    private static final Font BOLD_NORMAL_10 = FontFactory.getFont(FontFactory.COURIER_BOLD, 10);
    private static final Font NORMAL_12 = FontFactory.getFont(FontFactory.COURIER, 12);
    private static final Font BOLD_NORMAL_12 = FontFactory.getFont(FontFactory.COURIER_BOLD, 12);
    private static final Font ITALIC_12 = FontFactory.getFont(FontFactory.TIMES_ITALIC, 12);
    private static final Font BOLD_NORMAL_14 = FontFactory.getFont(FontFactory.COURIER_BOLD, 14);
    private static final Font CHAPTER_FONT = FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLDITALIC);
    private static final Font PARAGRAPH_FONT = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);
    private static final Font TITLE_FONT_GREEN = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.NORMAL, GREEN_COLOR);
    private static final Font TITLE_FONT_WHITE = FontFactory.getFont(FontFactory.HELVETICA, 14, Font.NORMAL, WHITE_COLOR);
    public static final String IMG = "/assets/images/header-logo.png";
    public static final Rectangle[] COLUMNS = {
            new Rectangle(36, 36, 290, 806),
            new Rectangle(305, 36, 559, 806)
    };

    SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy");
    Date localDate = Calendar.getInstance().getTime();

    public PDFApplicationCreditGenerator(UserCreditApplication mUser) {
        this.mUser = mUser;
    }

    public File createFile() {
        File file = null;
        Document document = new Document();
        try {
            file = File.createTempFile("credit-application", ".pdf");
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
            PdfPTable headerTable = new PdfPTable(1);
            headerTable.setTotalWidth(200);
            PdfPTable footerTable = new PdfPTable(1);
            footerTable.setTotalWidth(200);


//            FooterTableBuilder footerTableBuilder = new FooterTableBuilder(getModel());
//            HeaderTableBuilder headerTableBuilder = new HeaderTableBuilder(getModel());
//            LineItemsTableBuilder lineItemsTableBuilder = new LineItemsTableBuilder(getModel());
//
            //HeaderFooterApplication headerFooter = new HeaderFooterApplication(headerTable, footerTable);

            //writer.setPageEvent(headerFooter);
            document.open();
//            Image image = Image.getInstance(IMG);
//            image.setAbsolutePosition(0, 0);
//            document.add(image);


            Chunk chunk = new Chunk("COLUMBIA PIPE & SUPPLY CO.", CHAPTER_FONT);
            Chapter chapter = new Chapter(new Paragraph(chunk), 1);
            chapter.setNumberDepth(0);
            chapter.add(new Paragraph("1120 W. Pershing Road - Chicago, IL 60609", PARAGRAPH_FONT));
            chapter.add(new Paragraph("Ph: 1-800-572-1904 - Fax: 773-843-5892", PARAGRAPH_FONT));
            chapter.add(new Paragraph("Date: " + sdf.format(localDate), PARAGRAPH_FONT));

            document.add(chapter);

            PdfPTable commonInformationTable = new PdfPTable(6);
            commonInformationTable.setWidthPercentage(100);
            //commonInformationTable.setWidths(new int[]{1, 1, 1, 1, 1, 1});
            commonInformationTable.setSpacingBefore(10);

            commonInformationTable.addCell(createDoubleRowCell("Legal Business Name", mUser.getLegalBusinessName(), 3));
            commonInformationTable.addCell(createDoubleRowCell("Trade Name", mUser.getTradeName(), 3));
            commonInformationTable.addCell(createDoubleRowCell("Type of organization", mUser.getOrganizationType(), 3));
            commonInformationTable.addCell(createDoubleRowCell("Type of Customer", mUser.getCustomerType(), 3));
            commonInformationTable.addCell(createDoubleRowCell("Phone", mUser.getPhoneNumber(), 3));
            //commonInformationTable.addCell(createDoubleRowCell("Fax", mUser.getFaxNumber(), 3));
            commonInformationTable.addCell(createDoubleRowCell("Type of Business", mUser.getBusinessType(), 3));
            commonInformationTable.addCell(createDoubleRowCell("Year Established", mUser.getBusinessYearEstablished(), 1));
            commonInformationTable.addCell(createDoubleRowCell("PO Required?", mUser.getRequiredPO(), 1));
            commonInformationTable.addCell(createDoubleRowCell("Tax Exempt?", mUser.getTaxExempt(), 1));
            int colSpanForBusinessLicenseNumber = 3;
            if (StringUtils.isBlank(mUser.getTaxNumber())) {
                colSpanForBusinessLicenseNumber = 6;
            }
            commonInformationTable.addCell(createDoubleRowCell("Contractors Licence #", mUser.getBusinessLicenseNumber(), colSpanForBusinessLicenseNumber));

            if (!StringUtils.isBlank(mUser.getTaxNumber())) {
                commonInformationTable.addCell(createDoubleRowCell("List tax number", mUser.getTaxNumber(), 3));
            }
            commonInformationTable.setSpacingAfter(15);
            document.add(commonInformationTable);

//            DottedLineSeparator separator = new DottedLineSeparator();
//            separator.setPercentage(59500f / 523f);
//            Chunk linebreak = new Chunk(separator);
//            document.add(linebreak);
            //Addresses

            PdfPTable addressesTable = new PdfPTable(4);
            addressesTable.setWidthPercentage(100);

            Phrase titlePhrase = new Phrase("SHIP TO ADDRESS", BOLD_NORMAL_14);
            PdfPCell cell = new PdfPCell(titlePhrase);
            cell.setColspan(2);
            cell.setBorder(0);
            addressesTable.addCell(cell);

            titlePhrase = new Phrase("BILLING ADDRESS", BOLD_NORMAL_14);
            cell = new PdfPCell(titlePhrase);
            cell.setColspan(2);
            cell.setBorder(0);
            addressesTable.addCell(cell);

            addressesTable.addCell(createDoubleRowCell("Address", mUser.getAddressBusiness().getStreet(), 2));
            addressesTable.addCell(createDoubleRowCell("Address", mUser.getAddressBilling().getStreet(), 2));
            addressesTable.addCell(createDoubleRowCell("City", mUser.getAddressBusiness().getCity(), 2));
            addressesTable.addCell(createDoubleRowCell("City", mUser.getAddressBilling().getCity(), 2));
            addressesTable.addCell(createDoubleRowCell("State", mUser.getAddressBusiness().getState(), 1));
            addressesTable.addCell(createDoubleRowCell("Zip", mUser.getAddressBusiness().getZipCode(), 1));
            addressesTable.addCell(createDoubleRowCell("State", mUser.getAddressBilling().getState(), 1));
            addressesTable.addCell(createDoubleRowCell("Zip", mUser.getAddressBilling().getZipCode(), 1));
            addressesTable.setSpacingAfter(15);
            document.add(addressesTable);

            //document.add(linebreak);
            //Principals

            PdfPTable principalsTable = new PdfPTable(4);
            principalsTable.setWidthPercentage(100);
            principalsTable.addCell(createCellForTitle("", 4));

            titlePhrase = new Phrase("Principal", BOLD_NORMAL_14);
            cell = new PdfPCell(titlePhrase);
            cell.setColspan(2);
            cell.setBorder(0);
            principalsTable.addCell(cell);

            titlePhrase = new Phrase("Accounts Receivable Contact", BOLD_NORMAL_14);
            cell = new PdfPCell(titlePhrase);
            cell.setColspan(2);
            cell.setBorder(0);
            principalsTable.addCell(cell);

            principalsTable.addCell(createDoubleRowCell("Name", mUser.getPrincipalOrganizationList().get(0).getName(), 2));
            principalsTable.addCell(createDoubleRowCell("Name", mUser.getPrincipalOrganizationList().get(1).getName(), 2));
            principalsTable.addCell(createDoubleRowCell("Title", mUser.getPrincipalOrganizationList().get(0).getTitle(), 2));
            principalsTable.addCell(createDoubleRowCell("Title", mUser.getPrincipalOrganizationList().get(1).getTitle(), 2));
            //principalsTable.addCell(createDoubleRowCell("SSN", mUser.getPrincipalOrganizationList().get(0).getSocialSecurityNumber(), 2));
            //principalsTable.addCell(createDoubleRowCell("SSN", mUser.getPrincipalOrganizationList().get(1).getSocialSecurityNumber(), 2));
            principalsTable.addCell(createDoubleRowCell("Email", mUser.getPrincipalOrganizationList().get(0).getEmail(), 2));
            principalsTable.addCell(createDoubleRowCell("Email", mUser.getPrincipalOrganizationList().get(1).getEmail(), 2));
            principalsTable.addCell(createDoubleRowCell("Phone", mUser.getPrincipalOrganizationList().get(0).getPhoneNumber(), 2));
            principalsTable.addCell(createDoubleRowCell("Phone", mUser.getPrincipalOrganizationList().get(1).getPhoneNumber(), 2));
            principalsTable.addCell(createDoubleRowCell("Address", mUser.getPrincipalOrganizationList().get(0).getAddress().getStreet(), 2));
            principalsTable.addCell(createDoubleRowCell("Address", mUser.getPrincipalOrganizationList().get(1).getAddress().getStreet(), 2));
            principalsTable.addCell(createDoubleRowCell("City", mUser.getPrincipalOrganizationList().get(0).getAddress().getCity(), 2));
            principalsTable.addCell(createDoubleRowCell("City", mUser.getPrincipalOrganizationList().get(1).getAddress().getCity(), 2));
            principalsTable.addCell(createDoubleRowCell("State", mUser.getPrincipalOrganizationList().get(0).getAddress().getState(), 1));
            principalsTable.addCell(createDoubleRowCell("Zip", mUser.getPrincipalOrganizationList().get(0).getAddress().getZipCode(), 1));
            principalsTable.addCell(createDoubleRowCell("State", mUser.getPrincipalOrganizationList().get(1).getAddress().getState(), 1));
            principalsTable.addCell(createDoubleRowCell("Zip", mUser.getPrincipalOrganizationList().get(1).getAddress().getZipCode(), 1));
            principalsTable.setSpacingAfter(15);
            document.add(principalsTable);

            //document.add(linebreak);
            //Business Trade References

            PdfPTable referencesTable = new PdfPTable(12);
            referencesTable.setWidthPercentage(100);
            referencesTable.addCell(createCellForTitle("Business Trade References", 12));

            for (BusinessTradeReference reference : mUser.getBusinessTradeReferenceList()) {
                referencesTable.addCell(createDoubleRowCell("Name", reference.getName(), 2));
                referencesTable.addCell(createDoubleRowCell("Address", reference.getAddress().getStreet(), 2));
                referencesTable.addCell(createDoubleRowCell("City", reference.getAddress().getCity(), 2));
                referencesTable.addCell(createDoubleRowCell("State", reference.getAddress().getState(), 1));
                referencesTable.addCell(createDoubleRowCell("Zip", reference.getAddress().getZipCode(), 1));
                referencesTable.addCell(createDoubleRowCell("Phone", reference.getPhoneNumber(), 2));
                referencesTable.addCell(createDoubleRowCell("Email", reference.getEmail(), 2));
            }
            referencesTable.setSpacingAfter(15);
            document.add(referencesTable);

            //Bank Reference
            PdfPTable bankReferenceTable = new PdfPTable(12);
            bankReferenceTable.setWidthPercentage(100);
            bankReferenceTable.addCell(createCellForTitle("Bank Reference", 12));
            bankReferenceTable.addCell(createDoubleRowCell("Name", mUser.getBankReference().getName(), 4));
            bankReferenceTable.addCell(createDoubleRowCell("Account #", mUser.getBankReference().getAccountNumber(), 4));
            bankReferenceTable.addCell(createDoubleRowCell("Account Officer", mUser.getBankReference().getAccountOfficer(), 4));

            bankReferenceTable.addCell(createDoubleRowCell("Address", mUser.getBankReference().getAddress().getStreet(), 2));
            bankReferenceTable.addCell(createDoubleRowCell("City", mUser.getBankReference().getAddress().getCity(), 2));
            bankReferenceTable.addCell(createDoubleRowCell("State", mUser.getBankReference().getAddress().getState(), 2));
            bankReferenceTable.addCell(createDoubleRowCell("Zip", mUser.getBankReference().getAddress().getZipCode(), 2));
            bankReferenceTable.addCell(createDoubleRowCell("Phone", mUser.getBankReference().getPhoneNumber(), 2));
            bankReferenceTable.addCell(createDoubleRowCell("Email", mUser.getBankReference().getEmail(), 2));
            document.add(bankReferenceTable);

            document.newPage();
            Chunk chunkProfile = new Chunk("CUSTOMER PROFILE", CHAPTER_FONT);
            Chapter chapterProfile = new Chapter(new Paragraph(chunkProfile), 1);
            chapterProfile.setNumberDepth(0);
            document.add(chapterProfile);

            //document.add(new Paragraph("In order to better service your account, would you please fill out the following questionnaire?"));
            if (!mUser.getProductTypeList().isEmpty()) {
                document.add(new Paragraph("Which of the following products are used at your facility:"));
                List productList = new List(List.ORDERED);
                productList.setListSymbol("\u2022");
                for (String product : mUser.getProductTypeList()) {
                    productList.add(new ListItem(product, NORMAL_12));
                }
                document.add(productList);
            }

            if (!StringUtils.isBlank(mUser.getExpectedAnnualPurchases())) {
                Paragraph expectedAnnualPurchasesParagraph = new Paragraph("Expected Annual Purchase Amount: $" + mUser.getExpectedAnnualPurchases());
                expectedAnnualPurchasesParagraph.setSpacingBefore(10);
                expectedAnnualPurchasesParagraph.setSpacingAfter(15);
                document.add(expectedAnnualPurchasesParagraph);
            }

            if (!mUser.getSupposedCustomerList().isEmpty()) {
                PdfPTable supposedCustomerTable = new PdfPTable(4);
                supposedCustomerTable.setWidthPercentage(100);
                supposedCustomerTable.addCell(createCellForTitle("Who will place orders with Columbia Pipe in your company?", 12));

                for (Customer customer : mUser.getSupposedCustomerList()) {
                    supposedCustomerTable.addCell(createDoubleRowCell("Name", customer.getName(), 1));
                    supposedCustomerTable.addCell(createDoubleRowCell("Title", customer.getTitle(), 1));
                    supposedCustomerTable.addCell(createDoubleRowCell("Phone", customer.getPhoneNumber(), 1));
                    supposedCustomerTable.addCell(createDoubleRowCell("Email", customer.getEmail(), 1));
                }
                document.add(supposedCustomerTable);
            }

            if (!StringUtils.isBlank(mUser.getOriginatedCreditRequest())) {
                Paragraph creditRequestParagraph = new Paragraph("Who originated the credit request: " + mUser.getOriginatedCreditRequest());
                creditRequestParagraph.setSpacingBefore(10);
                creditRequestParagraph.setSpacingAfter(20);
                document.add(creditRequestParagraph);
            }

            PdfPTable personalGuarantyTable = new PdfPTable(1);
            personalGuarantyTable.setWidthPercentage(100);
            personalGuarantyTable.setSpacingBefore(10);
            PdfPCell personalGuarantyCell = createSingleRowCell("PERSONAL GUARANTY", 1, TITLE_FONT_WHITE);
            personalGuarantyCell.setBackgroundColor(GREEN_COLOR);
            personalGuarantyTable.addCell(personalGuarantyCell);

            String personalGuarantyContent = "I, " + mUser.getPersonalGuaranty() + ", for and in consideration of the extension of credit to " +
                    mUser.getPersonalGuarantyDebtor() + " (\"Debtor\") for the purchase of goods, wares and merchandise from COLUMBIA PIPE &amp; SUPPLY CO., " +
                    "an Illinois corporation (\"Columbia Pipe\"), hereby personally guarantee to Columbia Pipe, its successors and assigns, unconditionally the" +
                    "payment of the purchase price of all goods, wares and merchandise sold by Columbia Pipe to Debtor, plus accrued finance charges of 1.5% per " +
                    "month on all balances that are past due for more than 30 days and all expenses including reasonable attorneys' fees incurred by Columbia Pipe " +
                    "in collecting or attempting to collect any of the Debtor's obligations to Columbia Pipe or enforcing or attempting to enforce this Guaranty.\n\n" +
                    "I agree to abide by the terms of payment set forth by Columbia Pipe, including the requirement that all invoices to be discounted must be paid " +
                    "within the terms specified on the invoice. All invoices paid after discount terms specified on the invoice must be paid net.\n\n" +
                    "This Guaranty shall in all respects be continuing, absolute and unconditional an shall be binding upon my heirs, executors, administrators and assigns.\n\n" +
                    "Suits for the enforcement of this Guaranty may be brought against me and I hereby consent to the in personam jurisdiction and venue of any state or " +
                    "federal court within the state of Illinois. If there is a litigation based on this Guaranty, I waive all claims for set-off, counterclaims and " +
                    "defenses based on any statute of limitations and laches and waive trial by jury.\n" +
                    "I have read the above and consent to all the terms thereof.";
            personalGuarantyTable.addCell(createSingleRowCell(personalGuarantyContent, 1, NORMAL_12));
            document.add(personalGuarantyTable);
            document.newPage();

            Chunk chunkTaxCertificate = new Chunk("Multi-State Sales Tax Exemption Certificate", CHAPTER_FONT);
            Chapter chapterTaxCertificate = new Chapter(new Paragraph(chunkTaxCertificate), 1);
            chapterTaxCertificate.setNumberDepth(0);
            document.add(chapterTaxCertificate);

            List firstList = new List();
            firstList.setListSymbol(new Chunk("Issued to (seller)   "));
            firstList.add("Columbia Pipe & Supply Co.                                    Date: " + sdf.format(localDate) + "\n1120 W Pershing Road\nChicago, IL 60609");
            document.add(firstList);
            document.add(Chunk.NEWLINE);

            Chunk glue = new Chunk(new VerticalPositionMark());
            String taxExemptStatus = mUser.getTaxExemptionCertificate().getTaxExemptStatus();
            Paragraph p;
            if(taxExemptStatus.equals("Yes")){
            	p = new Paragraph("I CERTIFY THAT:");
            }else{
            	p = new Paragraph("");
            }
            p.add(new Chunk(glue));
            p.add("Do you require Tax Exempt status for multiple states? : " + mUser.getTaxExemptionCertificate().getTaxExemptStatus());
            document.add(p);

            PdfPTable certifyTable = new PdfPTable(6);
            certifyTable.setWidthPercentage(100);
            certifyTable.setSpacingBefore(15);
            if(taxExemptStatus.equals("Yes")){
            certifyTable.addCell(createDoubleRowCell("Name of Firm (buyer)", mUser.getTaxExemptionCertificate().getFirmName(), 3));
            certifyTable.addCell(createDoubleRowCell("Street Address or PO Box#", mUser.getTaxExemptionCertificate().getAddress().getStreet(), 3));
            certifyTable.addCell(createDoubleRowCell("State", mUser.getTaxExemptionCertificate().getAddress().getState(), 2));
            certifyTable.addCell(createDoubleRowCell("City", mUser.getTaxExemptionCertificate().getAddress().getCity(), 2));
            certifyTable.addCell(createDoubleRowCell("Zip", mUser.getTaxExemptionCertificate().getAddress().getZipCode(), 2));
            }
            certifyTable.setSpacingAfter(15);
            document.add(certifyTable);

            if (!StringUtils.isBlank(mUser.getTaxExemptionCertificate().getRegisteredType()) && taxExemptStatus.equals("Yes")) {
                String engagedRegisteredAnswer;
                if (!StringUtils.isBlank(mUser.getTaxExemptionCertificate().getSpecifyRegisteredType())) {
                    engagedRegisteredAnswer = mUser.getTaxExemptionCertificate().getRegisteredType() + " - " + mUser.getTaxExemptionCertificate().getSpecifyRegisteredType();
                } else {
                    engagedRegisteredAnswer = mUser.getTaxExemptionCertificate().getRegisteredType();
                }
                Paragraph engagedRegistered = new Paragraph("Is engaged as a registered? " + engagedRegisteredAnswer);
                engagedRegistered.setSpacingBefore(10);
                engagedRegistered.setSpacingAfter(20);
                document.add(engagedRegistered);
            }

            if (!mUser.getTaxExemptionCertificate().getRegisteredStateList().isEmpty() && taxExemptStatus.equals("Yes")) {
                Paragraph registeredStateParagraph = new Paragraph("And is registered with the below listed states within which your firm would deliver purchases to us:");
                registeredStateParagraph.setSpacingBefore(10);
                registeredStateParagraph.setSpacingAfter(10);
                document.add(registeredStateParagraph);
                PdfPTable registeredStateTable = new PdfPTable(8);
                registeredStateTable.setWidthPercentage(100);
                for (RegisteredState registeredState : mUser.getTaxExemptionCertificate().getRegisteredStateList()) {
                    registeredStateTable.addCell(createSingleRowCell("State", 1, NORMAL_12));
                    registeredStateTable.addCell(createSingleRowCell(registeredState.getState(), 1, NORMAL_12));
                    registeredStateTable.addCell(createSingleRowCell("STATE REGISTRATION OR ID NO.", 4, NORMAL_12));
                    registeredStateTable.addCell(createSingleRowCell(registeredState.getStateRegistration(), 2, NORMAL_12));
                }
                registeredStateTable.setSpacingAfter(20);
                document.add(registeredStateTable);
            }

            if (!StringUtils.isBlank(mUser.getTaxExemptionCertificate().getBuyerActivityType()) && taxExemptStatus.equals("Yes")){
                String anySuchPurchasesAnswer;
                if (!StringUtils.isBlank(mUser.getTaxExemptionCertificate().getSpecifyBuyerActivityType())) {
                    anySuchPurchasesAnswer = mUser.getTaxExemptionCertificate().getBuyerActivityType() + " - " + mUser.getTaxExemptionCertificate().getSpecifyBuyerActivityType();
                } else {
                    anySuchPurchasesAnswer = mUser.getTaxExemptionCertificate().getBuyerActivityType();
                }
                Paragraph anySuchPurchases = new Paragraph("And that any such purchases are: " + anySuchPurchasesAnswer);
                anySuchPurchases.setSpacingBefore(10);
                anySuchPurchases.setSpacingAfter(20);
                document.add(anySuchPurchases);
            }

            if (!StringUtils.isBlank(mUser.getTaxExemptionCertificate().getBusinessFollowing()) && taxExemptStatus.equals("Yes")) {
                Paragraph businessFollowing = new Paragraph("We are in the business of Wholesaling, Retailing, Manufacturing the following: " + mUser.getTaxExemptionCertificate().getBusinessFollowing());
                businessFollowing.setSpacingBefore(10);
                businessFollowing.setSpacingAfter(20);
                document.add(businessFollowing);
            }

            if (!StringUtils.isBlank(mUser.getTaxExemptionCertificate().getGeneralDescriptionProducts()) && taxExemptStatus.equals("Yes")) {
                Paragraph generalDescriptionProducts = new Paragraph("General description of products to be purchased from the seller: " + mUser.getTaxExemptionCertificate().getGeneralDescriptionProducts());
                generalDescriptionProducts.setSpacingBefore(10);
                generalDescriptionProducts.setSpacingAfter(20);
                document.add(generalDescriptionProducts);
            }

            document.newPage();

            Chunk chunkResaleCertificate = new Chunk("CRT-61 Certificate of Resale", CHAPTER_FONT);
            Paragraph paragraphResaleCertificate = new Paragraph(chunkResaleCertificate);
            LineSeparator line = new LineSeparator();
            line.setOffset(-2);
            paragraphResaleCertificate.add(line);
            Chapter chapterResaleCertificate = new Chapter(paragraphResaleCertificate, 1);
            chapterResaleCertificate.setNumberDepth(0);
            document.add(chapterResaleCertificate);


            PdfPTable certificateResaleTable = new PdfPTable(4);
            certificateResaleTable.setWidths(new int[]{1, 1, 1, 3});
            certificateResaleTable.setWidthPercentage(100);
            certificateResaleTable.setSpacingBefore(20);
            certificateResaleTable.addCell(createCellForTitle("Step 1: Identify the seller", 3));
            certificateResaleTable.addCell(createCellForTitle("Step 3: Describe the property", 1));

            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("2 Name", mUser.getResaleCertificate().getSellerName(), 3));
            certificateResaleTable.addCell(createSingleRowWithoutBorderCell("6 Describe the property that is being purchased for resale or list " +
                    "the invoice number and the date of purchase.", 1, 2, NORMAL_12));

            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("3 Business Address", mUser.getResaleCertificate().getSellerAddress().getStreet(), 3));

            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("City", mUser.getResaleCertificate().getSellerAddress().getCity(), 1));
            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("State", mUser.getResaleCertificate().getSellerAddress().getState(), 1));
            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("Zip", mUser.getResaleCertificate().getSellerAddress().getZipCode(), 1));
            certificateResaleTable.addCell(createSingleRowWithoutBorderCell(mUser.getResaleCertificate().getDescribePropertyResale(), 1, 1, ITALIC_12));

            addEmptyRow(certificateResaleTable, 4);

            certificateResaleTable.addCell(createCellForTitle("Step 2: Identify the purchaser", 3));
            certificateResaleTable.addCell(createCellForTitle("Step 4: Complete for blanket certificates", 1));
            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("3 Name", mUser.getResaleCertificate().getPurchaserName(), 3));

            String percentageResale;
            if (!StringUtils.isBlank(mUser.getResaleCertificate().getPercentagePurchasesResale())) {
                percentageResale = "I am the identified purchaser, and I certify that the following percentage, "
                        + mUser.getResaleCertificate().getPercentagePurchasesResale() + "%, of all of the purchases that I make from this seller are for resale.";
            } else {
                percentageResale = "I am the identified purchaser, and I certify that all of the purchases that I make from this seller are for resale.";
            }
            certificateResaleTable.addCell(createSingleRowWithoutBorderCell("7 " + percentageResale, 1, 3, NORMAL_12));

            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("4 Business Address", mUser.getResaleCertificate().getPurchaserAddress().getStreet(), 3));
            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("City", mUser.getResaleCertificate().getPurchaserAddress().getCity(), 1));
            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("State", mUser.getResaleCertificate().getPurchaserAddress().getState(), 1));
            certificateResaleTable.addCell(createDoubleRowCellWithoutBorder("Zip", mUser.getResaleCertificate().getPurchaserAddress().getZipCode(), 1));

            addEmptyRow(certificateResaleTable, 3);
            certificateResaleTable.addCell(createCellForTitle("Step 5: Purchaser’s signature", 1));

            String purchasesResale = "";
            switch (mUser.getResaleCertificate().getPurchaserRegisteredType()) {
                case "retailer": purchasesResale = "The purchaser is registered as a retailer with the Illinois Department of Revenue.\nRegistration number - " + mUser.getResaleCertificate().getRegistrationNumber(); break;
                case "reseller": purchasesResale = "The purchaser is registered as a reseller with the Illinois Department of Revenue.\nResale number - " + mUser.getResaleCertificate().getRegistrationNumber(); break;
                case "other": purchasesResale = "The purchaser is authorized to do business out-of-state and will resell and deliver property only to purchasers located outside the state of Illinois. See Line 5 instructions."; break;
            }
            certificateResaleTable.addCell(createSingleRowWithoutBorderCell("5 " + purchasesResale, 3, 1, NORMAL_12));

            certificateResaleTable.addCell(createSingleRowWithoutBorderCell("I certify that I am purchasing the property described in Step 3 " +
                    "from the stated seller for the purpose of resale.", 1, 1, NORMAL_12));
            document.add(certificateResaleTable);
            line.setOffset(-4);
            document.add(line);

//            lineItemsTableBuilder.buildLinerContent(document, getFormatterFactory());

        } catch (Exception e) {
            if(mLogging.isLoggingError()) {
                mLogging.logError( "Error document forming.", e);
            }
        } finally {
            if (document.isOpen()) {
                document.close();
            }
        }
        return file;
    }

    public PdfPCell createSingleRowCell(String value, int colSpan, Font font) {
        Phrase phrase = new Phrase(value, font);
        PdfPCell cell = new PdfPCell(phrase);
        cell.setColspan(colSpan);
        return cell;
    }

    public PdfPCell createSingleRowWithoutBorderCell(String value, int colSpan, int rowSpan, Font font) {
        Phrase phrase = new Phrase(value, font);
        PdfPCell cell = new PdfPCell(phrase);
        cell.setColspan(colSpan);
        cell.setRowspan(rowSpan);
        cell.setBorder(0);
        return cell;
    }

    public PdfPCell createDoubleRowCell(String name, String value, int colSpan) {
        Phrase phraseUp = new Phrase(name + "\n", NORMAL_8);
        Phrase phraseDown = new Phrase(value, NORMAL_12);
        phraseUp.add(phraseDown);
        PdfPCell cell = new PdfPCell(phraseUp);
        cell.setColspan(colSpan);
        return cell;
    }

    public PdfPCell createDoubleRowCellWithoutBorder(String name, String value, int colSpan) {
        Phrase phraseUp = new Phrase(name + "\n", NORMAL_8);
        Phrase phraseDown = new Phrase(value, NORMAL_12);
        phraseUp.add(phraseDown);
        PdfPCell cell = new PdfPCell(phraseUp);
        cell.setColspan(colSpan);
        cell.setBorder(0);
        return cell;
    }

    public PdfPCell createCellForTitle(String name, int colSpan) {
        PdfPCell cell = new PdfPCell(new Phrase(name, TITLE_FONT_GREEN));
        cell.setFixedHeight(20);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(colSpan);
        return cell;
    }

    private void addEmptyRow(PdfPTable pTable, int colSpan) {
        PdfPCell emptyCell = new PdfPCell();
        emptyCell.setColspan(colSpan);
        emptyCell.setBorder(0);
        emptyCell.setFixedHeight(20);
        pTable.addCell(emptyCell);
    }

    public UserCreditApplication getUser() {
        return mUser;
    }

    public void setUser(UserCreditApplication pUser) {
        this.mUser = pUser;
    }
}
