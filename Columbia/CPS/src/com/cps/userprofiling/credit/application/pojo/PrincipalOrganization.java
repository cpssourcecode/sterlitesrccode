package com.cps.userprofiling.credit.application.pojo;

/**
 * @author Alexey Meleshko
 */
public class PrincipalOrganization extends Customer {
    private String socialSecurityNumber;
    private Address address;

    public PrincipalOrganization(String name, String title, String phoneNumber, String socialSecurityNumber, String email, Address address) {
        super(name, title, phoneNumber, email);
        this.socialSecurityNumber = socialSecurityNumber;
        this.address = address;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
