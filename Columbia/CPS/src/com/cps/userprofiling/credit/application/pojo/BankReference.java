package com.cps.userprofiling.credit.application.pojo;

/**
 * @author Alexey Meleshko
 */
public class BankReference {
    private String name;
    private String accountNumber;
    private String accountOfficer;
    private Address address;
    private String phoneNumber;
    private String email;


    public BankReference(String name, String accountNumber, String accountOfficer, Address address, String phoneNumber, String email) {
        this.name = name;
        this.accountNumber = accountNumber;
        this.accountOfficer = accountOfficer;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountOfficer() {
        return accountOfficer;
    }

    public void setAccountOfficer(String accountOfficer) {
        this.accountOfficer = accountOfficer;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
