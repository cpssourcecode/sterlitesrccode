package com.cps.userprofiling.credit.application.pdf;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

/**
 * @author Alexey Meleshko
 */
public class HeaderFooterApplication extends PdfPageEventHelper {
    /**
     * header table
     */
    protected PdfPTable mHeader;
    /**
     * footer table
     */
    protected PdfPTable mFooter;
    /**
     * number of pages
     */
    private int mPageNumber = 1;
    /**
     * font
     */
    private static final Font NORMAL_8 = FontFactory.getFont(FontFactory.COURIER, 8);

    /**
     * ReportHeaderFooter constructor
     *
     * @param pHeader - header table
     * @param pFooter - footer table
     */
    public HeaderFooterApplication(PdfPTable pHeader, PdfPTable pFooter) {
        mHeader = pHeader;
        mFooter = pFooter;
    }

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        mHeader.writeSelectedRows(0, -1, 20, 585, writer.getDirectContent());
    }

    @Override
    public void onEndPage(PdfWriter pWriter, Document pDocument) {
        addPagination(pWriter);
        mPageNumber++;
    }

    /**
     * adds pagination to footer
     *
     * @param pWriter - pdf writer
     */
    private void addPagination(PdfWriter pWriter) {
        String pagination = "Page " + mPageNumber + " of " + pWriter.getPageNumber();
        PdfPTable tablePagination = new PdfPTable(1);
        tablePagination.setTotalWidth(460);
        PdfPCell cell = new PdfPCell(new Phrase(pagination, NORMAL_8));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        tablePagination.addCell(cell);
        tablePagination.writeSelectedRows(0, -1, 150, 30, pWriter.getDirectContent());
    }
}
