package com.cps.userprofiling;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.GenericFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.userprofiling.service.ContactUsService;
import com.cps.userprofiling.validator.ContactUsValidator;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Dmitry Golubev
 **/
public class ContactUsFormHandler extends GenericFormHandler {
	/**
	 * values
	 */
	private Map<String, String> mValue = new HashMap<String, String>();
	/**
	 * contact us service
	 */
	private ContactUsService mService;
	/**
	 * contact us validator
	 */
	private ContactUsValidator mValidator;

	/**
	 * Repeating Request Monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;


	//------------------------------------------------------------------------------------------------------------------

	/**
	 * saves default shipping address to user
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleSendMessage(DynamoHttpServletRequest pRequest,
													DynamoHttpServletResponse pResponse)
		throws ServletException, IOException
	{
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "ContactUsFormHandler.handleSendMessage";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				if (getValidator().validateContactUsForm(this, pRequest)) {
					getService().sendContactUsMessage(this, pRequest, pResponse);
				}
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		AjaxUtils.addAjaxResponse(this, pResponse, null);
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------

	public Map<String, String> getValue() {
		return mValue;
	}

	public void setValue(Map<String, String> pValue) {
		mValue = pValue;
	}

	public ContactUsService getService() {
		return mService;
	}

	public void setService(ContactUsService pService) {
		mService = pService;
	}

	public ContactUsValidator getValidator() {
		return mValidator;
	}

	public void setValidator(ContactUsValidator pValidator) {
		mValidator = pValidator;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}
}
