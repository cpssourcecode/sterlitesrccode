package com.cps.userprofiling;

import javax.servlet.http.Cookie;

import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.CookieManager;

public class CPSCookieManager extends CookieManager{
	
	private static String QUOTE = "\"";
	private static String YES = "Yes";
	
	public void rememberMeCookie (String email, String name, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse){
		Cookie cookie = null;
		if (email != null){
			cookie = generateUserEmailAndNameCookie(email, name);
			
			if (cookie != null){
				pResponse.addCookie(cookie);
			}
		}
	}
	
	private Cookie generateUserEmailAndNameCookie(String email, String name){
		String combinedString = "";
		if (email != null){
			StringBuilder str = new StringBuilder(email);
			if (!StringUtils.isBlank(name))
				str.append(","+name);
			
			combinedString = str.toString();
		}else if (name != null)
			combinedString = ","+name;
		
		Cookie cookie = new Cookie(CPSConstants.USER_EMAIL_NAME_COOKIE_NAME, combinedString);
		String domain = getProfileCookieDomain();
		if (domain != null)
			cookie.setDomain(domain);
		String path = getProfileCookiePath();
		if (path != null)
			cookie.setPath(path);
		cookie.setSecure(false);
		cookie.setMaxAge(getProfileCookieMaxAge());
		cookie.setComment(YES);
		return cookie;
	}
}