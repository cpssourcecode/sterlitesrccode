package com.cps.userprofiling.base;

import atg.commerce.profile.CommerceProfileFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.userprofiling.VSGProfileConstants;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;

import com.cps.util.CPSConstants;
import com.cps.util.CPSMessageUtils;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Dmitry Golubev
 **/
public class CPSAjaxBaseProfileFormHandler extends CommerceProfileFormHandler implements VSGProfileConstants {

	private Properties mRedirectURLMap;

	public Properties getRedirectURLMap() {
		return mRedirectURLMap;
	}

	public void setRedirectURLMap(Properties pRedirectURLMap) {
		mRedirectURLMap = pRedirectURLMap;
	}

	/**
	 * Extend standard checkFormRedirect for add response as Ajax ability.
	 * @param pSuccessURL - url redirect to if no form errors.
	 * @param pFailureURL - url redirect to if form errors.
	 * @param pRequest the servlet's request
	 * @param pResponse the servlet's response
	 * @return boolean
	 * @exception ServletException if there was an error while executing the code
	 * @exception IOException if there was an error with servlet io
	 */
	public boolean checkFormRedirect(String pSuccessURL, String pFailureURL, DynamoHttpServletRequest pRequest,
										 DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (AjaxUtils.isAjaxRequest(pRequest)) {
			// add ajax response in handle method
			return false;
		} else {
			return super.checkFormRedirect(pSuccessURL, pFailureURL, pRequest, pResponse);
		}
	}
	
	/**
	 * This takes a single error key and property and displays the error and adds the
	 * property to be highlighted. Use this if only adding one error.
	 *
	 * @param msg
	 * @param pty
	 * @param pRequest
	 */
	protected void createError(String msg, String pty, DynamoHttpServletRequest pRequest){
		CPSMessageUtils.addFormException(this, CPSConstants.ERR_MESSAGES_REPOSITORY, msg, pRequest.getLocale(), pty);
	}

	/**
	 * override to escape response redirect
	 * @param pErrorURL error url
	 * @param pRequest request
	 * @param pResponse response
	 * @return 1 - error, 0 - ok
	 */
	@Override
	protected int checkFormError(String pErrorURL, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		return super.checkFormError(AjaxUtils.isAjaxRequest(pRequest) ? null : pErrorURL, pRequest, pResponse);
	}

	/**
	 * override to escape response redirect
	 * @param pSuccessURL success url
	 * @param pRequest request
	 * @param pResponse response
	 * @return false - redirect, true - stay
	 */
	@Override
	protected boolean checkFormSuccess(String pSuccessURL, DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		return super.checkFormSuccess(AjaxUtils.isAjaxRequest(pRequest) ? null : pSuccessURL, pRequest, pResponse);
	}

}
