package com.cps.userprofiling;

import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import com.cps.util.CPSConstants;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GuestRegistrationFormHandler extends CPSProfileFormHandler {

	private String companyName;
	private String phoneNumber;
	private String companyBillingAddress;
	private String address2;
	private String city;
	private String state;
	private String country = "US";

	@Override
	protected void validateRegistrationInfo(DynamoHttpServletRequest pRequest) {
		vlogDebug(":::*********** Pre Create User ***********:::");
		getRegistrationValidator().validateGuestRegistrationInfo(this, getFirstName(), getLastName(),
				getEmail(), getPassword(), getConfirmedPassword(), getCompanyName(), getPhoneNumber(),
				getCompanyBillingAddress(), getAddress2(), getCity(), getState(), getZipCode(), pRequest);
	}

	@Override
	protected RepositoryItem getOrganization(DynamoHttpServletRequest pRequest) {
		return createOrganization();
	}

	@Override
	protected void sendRegisterEmailConfirmation() {
		vlogDebug("Sending an email  with confirmation link to user");
		String outgoingActivationToken = generateActivationToken();
		getEmailSender().sendRegisterEmailConfirmation(getCreatedUser(), outgoingActivationToken);

		vlogDebug("Sending an email about account activation to CPS admin");
		getEmailSender().sendAccountActivated(getCreatedUser());
	}

	private RepositoryItem createOrganization() {
		MutableRepositoryItem organization = null;
		try {
			organization = getProfileTools().getProfileRepository().createItem(CPSConstants.ORGANIZATION);

			organization.setPropertyValue(CPSConstants.NAME, getCompanyName());
			organization.setPropertyValue(CPSConstants.IS_ACTIVE, Boolean.TRUE);
			organization.setPropertyValue(CPSConstants.ORGANIZATION_TYPE, CPSConstants.ORG_TYPE_GUEST);
			organization.setPropertyValue(CPSConstants.CSR_ORDER_LIMIT, 0.0);
			organization.setPropertyValue(CPSConstants.BILLING_ADDRESS, createOrgAddress());
			RepositoryItem shippingAddress = createOrgAddress();
			organization.setPropertyValue(CPSConstants.SHIPPING_ADDRESS, shippingAddress);

			Map<String, RepositoryItem> userShippingAddresses = new HashMap<String, RepositoryItem>();
			if (shippingAddress != null) {
				userShippingAddresses.put(shippingAddress.getRepositoryId(), shippingAddress);
			}
			organization.setPropertyValue(CPSConstants.SECONDARY_ADDRESSES, userShippingAddresses);

			getProfileTools().getProfileRepository().addItem(organization);
		} catch (RepositoryException e) {
			vlogError(e, "Error in GuestRegistrationFormHandler.createOrganization");
		}
		return organization;
	}

	private RepositoryItem createOrgAddress() {
		MutableRepositoryItem orgAddress = null;
		try {
			orgAddress = getProfileTools().getProfileRepository().createItem(CPSConstants.CONTACT_INFO);
			setupAddressProperties(orgAddress);
			getProfileTools().getProfileRepository().addItem(orgAddress);
		} catch (RepositoryException e) {
			vlogError(e, "Error in GuestRegistrationFormHandler.createOrganization");
		}
		return orgAddress;
	}

	protected RepositoryItem addNewUser(MutableRepositoryItem userMutableItem,
			MutableRepository mutableRepository, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RepositoryException, ServletException, IOException {
		userMutableItem.setPropertyValue(CPSConstants.IS_ACTIVE, true);
		return addUser(userMutableItem, mutableRepository, pRequest, pResponse);
	}

	@Override
	public void postCreateUser(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		vlogDebug(":::************ Post Create User ************:::");
		setupProfileHomeAddress(pRequest, pResponse);
		//superPostCreateUser(pRequest, pResponse);
		super.postCreateUser(pRequest, pResponse);
	}

	private void setupProfileHomeAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			Boolean isCreated = (Boolean) pRequest.getObjectParameter(HANDLE_CREATE_PARAM);
			if (isCreated != null && isCreated) {
				Profile profile = getProfile();
				MutableRepositoryItem homeAddress = getProfileTools().getProfileRepository()
						.createItem(CPSConstants.CONTACT_INFO);
				setupAddressProperties(homeAddress);
				getProfileTools().getProfileRepository().addItem(homeAddress);
				profile.setPropertyValue(CPSConstants.HOME_ADDRESS, homeAddress);
			}
		} catch (RepositoryException e) {
			vlogDebug(e, "Error in GuestRegistrationFormHandler.setupProfileHomeAddress");
		}
	}

	private void setupAddressProperties(MutableRepositoryItem pAddress) {
		if (pAddress != null) {
			pAddress.setPropertyValue(CPSConstants.COMPANY_NAME, getCompanyName());
			pAddress.setPropertyValue(CPSConstants.ADDRESS1, getCompanyBillingAddress());
			pAddress.setPropertyValue(CPSConstants.ADDRESS2, getAddress2());
			pAddress.setPropertyValue(CPSConstants.COUNTRY, getCountry());
			pAddress.setPropertyValue(CPSConstants.CITY, getCity());
			pAddress.setPropertyValue(CPSConstants.STATE, getState());
			pAddress.setPropertyValue(CPSConstants.POSTAL_CODE, getZipCode());
			pAddress.setPropertyValue(CPSConstants.PHONE_NUMBER, getPhoneNumber());
		}
	}
	
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCompanyBillingAddress() {
		return companyBillingAddress;
	}

	public void setCompanyBillingAddress(String companyBillingAddress) {
		this.companyBillingAddress = companyBillingAddress;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}