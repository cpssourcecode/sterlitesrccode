package com.cps.userprofiling;

import atg.nucleus.GenericService;
import com.cps.integrations.statement.CPSStatementClient;
import com.cps.integrations.statement.CPSStatementConstants;
import com.cps.statement.StatementInfo;
import vsg.exception.WebServiceException;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * CPSStatementConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Steve Neverov
 * 
 */
public class CPSStatementConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/userprofiling/CPSStatementConnectionManager";

	private boolean mTest;

	public boolean isTest() {
		return mTest;
	}

	public void setTest(boolean pTest) {
		mTest = pTest;
	}

	/** Client property */
	private CPSStatementClient mClient;

	/**
	 * @return
	 */
	public CPSStatementClient getClient() {
		return mClient;
	}

	/**
	 * @param pClient
	 */
	public void setClient(CPSStatementClient pClient) {
		mClient = pClient;
	}

	public boolean requestStatementOnDemand(String pCustomerNumber, String pEmail) {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestStatement").append(" - start").toString());
		}

		boolean success = false;

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CPSStatementConstants.CUSTOMER_NUMBER, pCustomerNumber);
		params.put(CPSStatementConstants.EMAIL_ADDRESS, pEmail);
		try {
			Map<String, Object> reply = getClient().requestStatementOnDemand(params);
			if (reply != null && reply.size() > 0) {
				success = true;
			}
		} catch (WebServiceException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestStatement").append(" - exit").toString());
		}

		return success;

	}

	/**
	 * This method will request history statements
	 *
	 * @param pCustomerNumber
	 * @return
	 * @throws Exception
	 */
	public List<StatementInfo> requestStatements(String pCustomerNumber, Date pStartDate, Date pEndDate) throws Exception {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestStatements").append(" - start").toString());
		}

		Long startDate = null;
		if (pStartDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, -1);
			startDate = cal.getTimeInMillis();
		} else {
			startDate = pStartDate.getTime();
		}

		Long endDate = null;
		if (pEndDate == null) {
			Calendar cal = Calendar.getInstance();
			endDate = cal.getTimeInMillis();
		} else {
			endDate = pEndDate.getTime();
		}

		if (isTest()) {
			pCustomerNumber = "100109";
		}

		List<StatementInfo> infos = getClient().requestStatements(pCustomerNumber, startDate.toString(), endDate.toString());

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestStatements").append(" - exit").toString());
		}
		return infos;

	}

}
