package com.cps.userprofiling;

import atg.nucleus.GenericService;
import com.cps.integrations.invoice.CPSDummyInvoiceClient;
import com.cps.integrations.invoice.CPSInvoiceClient;
import com.cps.integrations.invoice.CPSInvoiceConstants;
import vsg.exception.WebServiceException;

import java.util.*;

/**
 * CPSInvoiceConnectionManager
 * 
 * <h4>Description</h4>
 * 
 * <h4>Notes</h4>
 * 
 * @author Steve Neverov
 * 
 */
public class CPSInvoiceConnectionManager extends GenericService {

	/** component name for debugging */
	private final static String COMPONENT_NAME = "/cps/userprofiling/CPSInvoiceConnectionManager";

	private boolean mTest;

	public boolean isTest() {
		return mTest;
	}

	public void setTest(boolean pTest) {
		mTest = pTest;
	}

	private int mDaysBefore;

	/** Client property */
	private CPSInvoiceClient mClient;

	/**
	 * @return
	 */
	public CPSInvoiceClient getClient() {
		return mClient;
	}

	/**
	 * @param pClient
	 */
	public void setClient(CPSInvoiceClient pClient) {
		mClient = pClient;
	}

	/** Dummy Client property */
	private CPSDummyInvoiceClient mDummyClient;

	/**
	 * @return
	 */
	public CPSDummyInvoiceClient getDummyClient() {
		return mDummyClient;
	}

	/**
	 * @param pDummyClient
	 */
	public void setDummyClient(CPSDummyInvoiceClient pDummyClient) {
		mDummyClient = pDummyClient;
	}

	/**
	 * This method will request invoices with the invoice client based on an
	 * start and end date and CS.
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> requestInvoices(String pCustomerNumber,
											   Calendar pStartDate,
											   Calendar pEndDate,
											   String pInvoiceNumber,
											   String pOrderNumber,
											   String pPONumber,
											   List<String> pShipTo,
											   String pBillTo,
											   int pPageSize,
											   int pStartIndex,
											   String pSortField,
											   String pSortOption) throws Exception {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoices").append(" - start").toString());
		}

		Map<String, Object> result = new HashMap<String, Object>();

		Calendar startDate = pStartDate;
		Calendar endDate = pEndDate;

		if (startDate == null || endDate == null) {
			startDate = Calendar.getInstance();
			startDate.add(Calendar.DAY_OF_MONTH, -1*getDaysBefore());
			endDate = Calendar.getInstance();
		}

		if (getClient().getWebServiceConfig().getWebServiceEnabled()) {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Service is enabled, use client to get data.").toString());
			}
			if (isTest()) {
				List<String> shipto = new ArrayList<String>();
				shipto.add("110145");
				result = getClient().requestInvoices("110144", startDate, endDate, null, null, null, shipto, pBillTo, pPageSize, pStartIndex, pSortField, pSortOption);
			} else {
				result = getClient().requestInvoices(pCustomerNumber, startDate, endDate, pInvoiceNumber, pOrderNumber, pPONumber, pShipTo, pBillTo, pPageSize, pStartIndex, pSortField, pSortOption);
			}
		} else {
			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("Service is NOT enabled!").toString());
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoices").append(" - exit").toString());
		}
		return result;

	}

	public String requestInvoiceStatus(String pInvoiceId) {

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestInvoiceStatus").append(" - start").toString());
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(CPSInvoiceConstants.INVOICE_NUMBER, pInvoiceId);
		String status = null;
		try {
			Map<String, Object> reply = getClient().requestInvoiceStatus(params);
			if (reply != null) {
				status = (String)reply.get(CPSInvoiceConstants.INVOICE_STATUS);
			}
		} catch (WebServiceException e) {
			if (isLoggingError()) {
				logError(e);
			}
		}

		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(COMPONENT_NAME)
					.append(".requestInvoiceStatus").append(" - exit").toString());
		}

		return status;

	}

	public int getDaysBefore() {
		return mDaysBefore;
	}

	public void setDaysBefore(int pDaysBefore) {
		mDaysBefore = pDaysBefore;
	}
}
