package com.cps.userprofiling;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.email.CommonEmailSender;
import com.cps.userprofiling.base.CPSAjaxBaseProfileFormHandler;
import com.cps.util.CPSConstants;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;
import java.io.IOException;

import static com.cps.email.CPSEmailConstants.FIRST_NAME;
import static com.cps.email.CPSEmailConstants.LAST_NAME;

public class RequestStatementFormHandler extends CPSAjaxBaseProfileFormHandler {

	/**
	 * CPSStatementConnectionManager property
	 */
	private CPSStatementConnectionManager mConnectionManager;

	/**
	 * email sender
	 */
	private CommonEmailSender mEmailSender;

	public CPSStatementConnectionManager getConnectionManager() {
		return mConnectionManager;
	}

	public void setConnectionManager(CPSStatementConnectionManager pConnectionManager) {
		mConnectionManager = pConnectionManager;
	}

	/**
	 * request monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}


	/**
	 * Handles request statement
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws javax.servlet.ServletException
	 * @throws java.io.IOException
	 */
	public boolean handleRequestStatement(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "RequestStatementFormHandler.handleRequestStatement";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			try {
				vlogDebug("RequestStatementFormHandler.handleRequestStatement - start");

				requestStatement(pRequest, pResponse);

				vlogDebug("RequestStatementFormHandler.handleRequestStatement - end");
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}

		return checkFormRedirect(getUpdateSuccessURL(), getUpdateErrorURL(), pRequest, pResponse);
	}

	/**
	 * Requests statement
	 * @param pRequest
	 * @param pResponse
	 */
	public void requestStatement(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

		vlogDebug("RequestStatementFormHandler.requestStatement - start");

		String customerNumber = (String) getValue().get(CPSConstants.CUSTOMER_NUMBER);
		String email = (String) getValue().get(CPSConstants.EMAIL);
		vlogDebug("email:: {0}", email);

		getConnectionManager().requestStatementOnDemand(customerNumber, email);

		postRequestStatement(email, String.valueOf(getProfile().getPropertyValue(FIRST_NAME)), String.valueOf(getProfile().getPropertyValue(LAST_NAME)),customerNumber);

		AjaxUtils.addAjaxResponse(this, pResponse, null);

		vlogDebug("RequestStatementFormHandler.requestStatement - exit");

	}

	/**
	 * Holder method for post action
	 * @param email
	 * @param firstName
	 * @param lastName
	 */
	public boolean postRequestStatement(String email, String firstName, String lastName, String customerNumber) {
		return getEmailSender().sendCurrentStatementCopyEmail(email, firstName, lastName, customerNumber);
	}

	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}

}