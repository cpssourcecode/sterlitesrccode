package com.cps.userprofiling.util;

import atg.repository.RepositoryItem;

public class UserAssociatedCS {
	private String id;

	private RepositoryItem organization;

	private RepositoryItem addressItem;
	
	private Boolean isDefault;
	
	private Boolean isActive;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public RepositoryItem getOrganization() {
		return organization;
	}

	public void setOrganization(RepositoryItem organization) {
		this.organization = organization;
	}

	public RepositoryItem getAddressItem() {
		return addressItem;
	}

	public void setAddressItem(RepositoryItem addressItem) {
		this.addressItem = addressItem;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
}