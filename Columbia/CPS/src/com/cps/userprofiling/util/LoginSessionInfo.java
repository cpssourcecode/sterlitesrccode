package com.cps.userprofiling.util;

import atg.nucleus.GenericService;

/**
 * @author Dmitry Golubev
 */
public class LoginSessionInfo extends GenericService {
	/**
	 * count of error logins
	 */
	private int mLoginErrorCount = 0;
	/**
	 * count of error logins with captcha
	 */
	private int mLoginErrorWithCaptchaCount = 0;
	/**
	 * flag to show modal for CS select
	 */
	private boolean mShouldSelectCS = false;
	/**
	 * user locked flag
	 */
	private boolean mUserLocked = false;

	/**
	 * user on hold flag
	 */
	private boolean mOnHold = false;

	/**
	 * flag to redirect
	 */
	private boolean mShouldRedirect = false;
	//------------------------------------------------------------------------------------------------------------------

	/**
	 * reset errors count
	 */
	public void resetLoginErrors() {
		setLoginErrorCount(0);
		setLoginErrorWithCaptchaCount(0);
	}

	/**
	 * increments login error count
	 */
	public void incrementLoginError() {
		mLoginErrorCount ++;
		if(getLoginErrorCount() > 3) { // more than 3 tries means that there is try with captcha
			incrementLoginErrorWithCaptcha();
		}
	}

	/**
	 * @return true if there are more than 3 errors
	 */
	public boolean isShowCaptcha() {
		return getLoginErrorCount() > 2;
	}

	/**
	 * @return true if there are more then 2 tries with captcha login
	 */
	public boolean isLockUser() {
		return getLoginErrorWithCaptchaCount() > 3;
	}

	/**
	 * increments login error with captcha count
	 */
	private void incrementLoginErrorWithCaptcha() {
		mLoginErrorWithCaptchaCount ++;
	}

	//------------------------------------------------------------------------------------------------------------------

	public int getLoginErrorCount() {
		return mLoginErrorCount;
	}

	public void setLoginErrorCount(int pLoginErrorCount) {
		mLoginErrorCount = pLoginErrorCount;
	}

	public int getLoginErrorWithCaptchaCount() {
		return mLoginErrorWithCaptchaCount;
	}

	public void setLoginErrorWithCaptchaCount(int pLoginErrorWithCaptchaCount) {
		mLoginErrorWithCaptchaCount = pLoginErrorWithCaptchaCount;
	}

	public boolean isShouldSelectCS() {
		return mShouldSelectCS;
	}

	public void setShouldSelectCS(boolean pShouldSelectCS) {
		mShouldSelectCS = pShouldSelectCS;
	}

	public boolean isUserLocked() {
		return mUserLocked;
	}

	public void setUserLocked(boolean pUserLocked) {
		mUserLocked = pUserLocked;
	}

	public boolean isOnHold() {
		return mOnHold;
	}

	public void setOnHold(boolean pOnHold) {
		mOnHold = pOnHold;
	}

	public boolean isShouldRedirect() {
		return mShouldRedirect;
	}

	public void setShouldRedirect(boolean pShouldRedirect) {
		mShouldRedirect = pShouldRedirect;
	}

}
