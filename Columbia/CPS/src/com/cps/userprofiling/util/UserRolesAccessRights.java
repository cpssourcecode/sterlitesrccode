package com.cps.userprofiling.util;

import java.util.Map;

/**
 * User Roles Access Rights
 */
public class UserRolesAccessRights {
	
	/**
	 * access rights map
	 */
	private Map<String,String> mRolesAccessRights;

	public Map<String, String> getRolesAccessRights() {
		return mRolesAccessRights;
	}

	public void setRolesAccessRights(Map<String, String> pRolesAccessRights) {
		this.mRolesAccessRights = pRolesAccessRights;
	}
	
}
