package com.cps.userprofiling.util;

import atg.repository.RepositoryItem;

public class UserAssociatedCB implements Comparable<UserAssociatedCB> {

	private RepositoryItem organization;
	
	private Boolean selected = false;
	
	public RepositoryItem getOrganization() {
		return organization;
	}

	public void setOrganization(RepositoryItem organization) {
		this.organization = organization;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	@Override
	public int compareTo(UserAssociatedCB userAssociatedCB) {
		RepositoryItem item = ((UserAssociatedCB) userAssociatedCB).getOrganization();
		return ((String) this.organization.getPropertyValue("name")).compareTo((String) item.getPropertyValue("name"));
	}

}