package com.cps.userprofiling;

import atg.droplet.DropletException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.repository.servlet.RepositoryFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Stolbunov Dmitry.
 */
public class NewsletterSignUpFormHandler extends RepositoryFormHandler {

    @Override
    protected void preCreateItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException {
        Map<String, Object> result = new HashMap<String, Object>();
        Object params[] = new Object[1];
        String email = (String) getValue().get("login");
        params[0] = email;
        if (!email.contains("@")) {
            addFormException(new DropletException("Please enter a valid email address"));
        } else {
            try {
                RepositoryView view = getRepository().getView(getItemDescriptor());
                RqlStatement statement = RqlStatement.parseRqlStatement("login = ?0");
                RepositoryItem[] items = statement.executeQuery(view, params);
                if (items != null && items.length > 0) {
                    addFormException(new DropletException("This email is subscribed already"));
                }
            } catch (RepositoryException ex) {
                if(isLoggingError()) {
                    logError(ex);
                }
            }
        }
        AjaxUtils.addAjaxResponse(this, pResponse, result);
    }
}
