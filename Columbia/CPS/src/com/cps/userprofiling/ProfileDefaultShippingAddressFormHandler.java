package com.cps.userprofiling;

import static com.cps.util.CPSConstants.CART_SELECTED_STORE;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import com.cps.commerce.order.service.CPSCartModifierService;
import com.cps.userprofiling.service.ProfileDefaultShippingAddressService;
import com.cps.userprofiling.validator.ProfileDefaultShippingAddressValidator;
import com.cps.util.CPSConstants;
import com.cps.util.CPSSessionBean;

import atg.commerce.order.Order;
import atg.commerce.order.OrderImpl;
import atg.commerce.profile.CommerceProfileFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
import vsg.util.ajax.AjaxUtils;

/**
 * @author Dmitry Golubev
 **/
public class ProfileDefaultShippingAddressFormHandler extends CommerceProfileFormHandler {
	/**
	 * validator to init selected shipping address for session
	 */
	private ProfileDefaultShippingAddressValidator mShippingAddressValidator;
	/**
	 * service to init selected shipping address for session
	 */
	private ProfileDefaultShippingAddressService mShippingAddressService;
	/**
	 * selected address id
	 */
	private String mSelectedId;
	/**
	 * current order
	 */
	private Order mOrder;

	/** cart modifier service */
	private CPSCartModifierService mCartModifierService;
	
	private String mDeliveryMethod;
		
	private boolean mUpdateShippingGroup;

    private CPSSessionBean sessionBean;

	/**
     * @return the sessionBean
     */
    public CPSSessionBean getSessionBean() {
        return sessionBean;
    }

    /**
     * @param sessionBean
     *            the sessionBean to set
     */
    public void setSessionBean(CPSSessionBean sessionBean) {
        this.sessionBean = sessionBean;
    }

    /**
     * request monitor
     */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * saves default shipping address to user
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleSaveDefaultShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		final String handleMethod = "ProfileDefaultShippingAddressFormHandler.handleSaveDefaultShippingAddress";
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (null == rrm || rrm.isUniqueRequestEntry(handleMethod)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			boolean shouldRollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				
				Order order = getOrder();
				synchronized (order) {
					if (getShippingAddressValidator().validateCSForm(this, pRequest)) {
						getShippingAddressService().initProfileShippingAddress(this, pRequest, pResponse);
						getShippingAddressService().initProfileDefaultShippingAddress(this, pRequest, pResponse);
						getCartModifierService().updateShippingGroups(getDeliveryMethod(), getSelectedId(), order, getProfile());
					}
                    ((OrderImpl) order).updateVersion();
					getOrderManager().updateOrder(order);
					//SM-356 - Credit card only account notification
					RepositoryItem selectedCS  = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.SELECTED_CS);
					if(selectedCS != null){
						String selectedCSPayInstRYIN = (String) selectedCS.getPropertyValue(CPSConstants.PAY_INST_RYIN);
						if(selectedCSPayInstRYIN != null && selectedCSPayInstRYIN.equals("?")){
							getProfile().setPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED, false);
							getProfile().setPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION, true);
						}
					}
				}
				AjaxUtils.addAjaxResponse(this, pResponse, null);

			} catch (final Exception e) {
				shouldRollback = processError(e, "Error during saving default shipping address");
				vlogError(e, "Error during saving default shipping address");
                logError(e);
			} finally {
				endTransaction(tm, td, shouldRollback || getFormError());
				if (null != rrm) {
					rrm.removeRequestEntry(handleMethod);
				}
			}
		}

		return false;
	}

	/**
	 * saves session shipping address to user
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleSaveProfileShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException{
		
		final String handleMethod = "ProfileDefaultShippingAddressFormHandler.handleSaveProfileShippingAddress";
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (null == rrm || rrm.isUniqueRequestEntry(handleMethod)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			boolean shouldRollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				
				Order order = getOrder();
				synchronized (order) {
		            getProfile().setPropertyValue(CART_SELECTED_STORE, null);
					if(getShippingAddressValidator().validateCSForm(this, pRequest)) {
						getShippingAddressService().initProfileShippingAddress(this, pRequest, pResponse);
						getCartModifierService().updateShippingGroups(getDeliveryMethod(), getSelectedId(), order, getProfile());
					}
                    ((OrderImpl) order).updateVersion();
					getOrderManager().updateOrder(order);
					//SM-356 - Credit card only account notification
					RepositoryItem selectedCS  = (RepositoryItem) getProfile().getPropertyValue(CPSConstants.SELECTED_CS);
					if(selectedCS != null){
						String selectedCSPayInstRYIN = (String) selectedCS.getPropertyValue(CPSConstants.PAY_INST_RYIN);
						if(selectedCSPayInstRYIN != null && selectedCSPayInstRYIN.equals("?")){
							getProfile().setPropertyValue(CPSConstants.CC_ACCOUNT_ACCEPTED, false);
							getProfile().setPropertyValue(CPSConstants.SHOW_CC_NOTIFICATION, true);
						}
					}
				}
				AjaxUtils.addAjaxResponse(this, pResponse, null);

			} catch (final Exception e) {
				shouldRollback = processError(e, "Error during saving shipping address");
				vlogError(e, "Error during saving shipping address");
                logError(e);
			} finally {
				endTransaction(tm, td, shouldRollback || getFormError());
				if (null != rrm) {
					rrm.removeRequestEntry(handleMethod);
				}
			}
		}
		return false;
	}
	
	/**
	 * Process error.
	 *
	 * @param e       the e
	 * @param message the message
	 * @return true, if successful
	 */
	protected boolean processError(final Exception e, final String message) {
		final String msg = e.getMessage();
		addFormException(new DropletException(StringUtils.isBlank(msg) ? message : msg));
		return true;
	}
	
	/**
	 * End transaction.
	 *
	 * @param tm             the tm
	 * @param td             the td
	 * @param shouldRollback the should rollback
	 */
	protected void endTransaction(TransactionManager tm, TransactionDemarcation td, boolean shouldRollback) {
		try {
			if (null != tm) {
				td.end(shouldRollback);
			}
		} catch (final TransactionDemarcationException e) {
			vlogError(e, "Error in end transaction.");
		}
	}
	
	/**
	 * set shipping address to user on cart
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleSetShippingAddressOnCart(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		final String handleMethod = "ProfileDefaultShippingAddressFormHandler.handleSetPickupLocationOnCart";
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (null == rrm || rrm.isUniqueRequestEntry(handleMethod)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			boolean shouldRollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				
				Order order = getOrder();
				synchronized (order) {
					Profile profile = getProfile();
		            getProfile().setPropertyValue(CART_SELECTED_STORE, null);

					if(profile.isTransient()) {
						getCartModifierService().createHardgoodShipGroupIfNotExists(order);
						getShippingAddressService().setProfileShippingAddressOnCart(this, pRequest, pResponse);
					} else {
						if (getShippingAddressValidator().validateCSForm(this, pRequest)) {
							getCartModifierService().updateShippingGroups("shipped", getSelectedId(), order, getProfile());
							getShippingAddressService().setProfileShippingAddressOnCart(this, pRequest, pResponse);
                            // clear the PricingCache from SessionBean, since the CartSelected Address changed
                            getSessionBean().getProductPricesCache().clear();
						}
					}
					if(!getFormError()) {
						getShippingAddressService().clearCIAvailabilityCheck(getOrder());
                        ((OrderImpl) order).updateVersion();
						getOrderManager().updateOrder(order);
					}
				}
				AjaxUtils.addAjaxResponse(this, pResponse, null);

			} catch (final Exception e) {
				shouldRollback = processError(e, "Error during setting shipping location");
				vlogError(e, "Error during setting shipping location");
                logError(e);
			} finally {
				endTransaction(tm, td, shouldRollback || getFormError());
				if (null != rrm) {
					rrm.removeRequestEntry(handleMethod);
				}
			}
		}
		return false;
	}



	/**
	 * set pickup location to user on cart
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleSetPickupLocationOnCart(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		
		final String handleMethod = "ProfileDefaultShippingAddressFormHandler.handleSetPickupLocationOnCart";
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if (null == rrm || rrm.isUniqueRequestEntry(handleMethod)) {
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			boolean shouldRollback = false;
			try {
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				
				Order order = getOrder();
				synchronized (order) {
					if( getShippingAddressValidator().validatePickupForm(this, pRequest) ) {
						getShippingAddressService().setPickupLocationOnCart(this, pRequest, pResponse);
						getCartModifierService().updateShippingGroups("pick-up", getSelectedId(), order, getProfile());
                        getSessionBean().getProductPricesCache().clear();
					}
					getShippingAddressService().clearCIAvailabilityCheck(getOrder());
                    ((OrderImpl) order).updateVersion();
					getOrderManager().updateOrder(order);
				}
				AjaxUtils.addAjaxResponse(this, pResponse, null);

			} catch (final Exception e) {
				shouldRollback = processError(e, "Error during setting pickup location");
				vlogError(e, "Error during setting pickup location");
                logError(e);
			} finally {
				endTransaction(tm, td, shouldRollback || getFormError());
				if (null != rrm) {
					rrm.removeRequestEntry(handleMethod);
				}
			}
		}
		return false;
		
	}

	//------------------------------------------------------------------------------------------------------------------

	public ProfileDefaultShippingAddressValidator getShippingAddressValidator() {
		return mShippingAddressValidator;
	}

	public void setShippingAddressValidator(ProfileDefaultShippingAddressValidator pShippingAddressValidator) {
		mShippingAddressValidator = pShippingAddressValidator;
	}

	public ProfileDefaultShippingAddressService getShippingAddressService() {
		return mShippingAddressService;
	}

	public void setShippingAddressService(ProfileDefaultShippingAddressService pShippingAddressService) {
		mShippingAddressService = pShippingAddressService;
	}

	public String getSelectedId() {
		return mSelectedId;
	}

	public void setSelectedId(String pSelectedId) {
		mSelectedId = pSelectedId;
	}
	
	public void setOrder(Order pOrder) {
		mOrder = pOrder;
	}

	public Order getOrder() {
		return mOrder;
	}
	
	public CPSCartModifierService getCartModifierService() {
		return mCartModifierService;
	}

	public void setCartModifierService(CPSCartModifierService pCartModifierService) {
		mCartModifierService = pCartModifierService;
	}
	
	public String getDeliveryMethod() {
		return mDeliveryMethod;
	}

	public void setDeliveryMethod(String pDeliveryMethod) {
		mDeliveryMethod = pDeliveryMethod;
	}
	
	public boolean getUpdateShippingGroup() {
		return mUpdateShippingGroup;
	}

	public void setUpdateShippingGroup(boolean pUpdateShippingGroup) {
		mUpdateShippingGroup = pUpdateShippingGroup;
	}
}
