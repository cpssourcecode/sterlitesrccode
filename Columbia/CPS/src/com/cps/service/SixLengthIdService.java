package com.cps.service;

import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

import atg.nucleus.GenericService;

/**
 * 
 * Service used to support Ids with prepended zeros
 *
 */
public class SixLengthIdService extends GenericService {
	
	/**
	 * method removes first zeros form the string
	 * @param pSkuId
	 * @return
	 */
	public String removeFirstZeros(String pSkuId){
		if(StringUtils.isNotEmpty(pSkuId) && pSkuId.length() == 6){
			for(int i = 0; i < pSkuId.length(); i++){
				if(pSkuId.startsWith("0")){
					pSkuId = pSkuId.substring(1);
					i--;
				} else {
					break;
				}
			}
		}
		return pSkuId;
	}
	
}
