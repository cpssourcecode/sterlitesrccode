package com.cps.service;

import static com.cps.util.CPSConstants.PRODUCT_ALIAS_MAP;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSConstants;

import atg.core.util.StringUtils;
import atg.endeca.index.Indexable;
import atg.endeca.index.admin.IndexingTask;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.repository.search.indexing.BulkLoaderResults;
import atg.repository.search.indexing.IncrementalLoaderResults;
import atg.repository.search.indexing.IndexingException;
import atg.repository.search.indexing.LoaderResults;
import atg.userprofiling.Profile;

/**
 * Service to clear Organization With Alias before indexing
 */
public class OrganizationAliasService extends GenericService implements Indexable {

	/**
	 * The Enabled flag.
	 */
	private boolean mEnableSearchByAliasInterface = true;
	private CPSProfileTools mProfileTools;
    private boolean filterNonIndexableProducts = true;


	/**
	 * All query
	 */
	private static final String QUERY_ALL = "ALL";
	/**
	 * All query
	 */
    private static final String QUERY_PRODUCTS_WITH_ALIASES = "NOT keyOrgs IS NULL";

    private static final String QUERY_PRODUCTS_WITH_ALIASES_AND_DISPLAYABLE = "NOT keyOrgs IS NULL and stockingType !=\"U\" and ancestorCategories INCLUDES ITEM(ecommerceDisplay=1)";

	/**
	 * All query
	 */
	private static final String DESCRIPTOR = "items";
	/**
	 * organizations property
	 */
	private static final String PROPERTY_ORGANIZATIONS = "organizations";

	/**
	 * repository
	 */
	private MutableRepository mOrgAliasRepository;
	/**
	 * repository
	 */
	private MutableRepository mProductCatalogRepository;
	/**
	 * respotiry item id
	 */
	private String mItemId = "organizations_with_aliases";

	/**
	 * clears repository and creates new item
	 */
	public void clearRepository() {
		RepositoryItem[] items = null;
		try {
			items = queryAllItems();
			removeAllItems(items);
			createNewItem();
		} catch (RepositoryException e) {
			vlogError(e, e.getMessage());
		}
	}

	/**
	 * get all items from repository
	 *
	 * @return - items
	 * @throws RepositoryException - the repository exception
	 */
	private RepositoryItem[] queryAllItems() throws RepositoryException {
		RepositoryView view = getOrgAliasRepository().getView(DESCRIPTOR);
		RqlStatement statement = RqlStatement.parseRqlStatement(QUERY_ALL);
		Object params[] = new Object[0];
		return statement.executeQuery(view, params);
	}

	/**
	 * removes items from repository
	 *
	 * @param pItems - items
	 * @throws RepositoryException - the repository exception
	 */
	private void removeAllItems(RepositoryItem[] pItems) throws RepositoryException {
		if (pItems != null) {
			for (RepositoryItem item : pItems) {
				getOrgAliasRepository().removeItem(item.getRepositoryId(), DESCRIPTOR);
			}
		}
	}

    public RepositoryItem[] queryValidProductsWithOrgnAliases() throws RepositoryException {
        RepositoryView view = getProductCatalogRepository().getView(CPSConstants.PRODUCT_ITEM_DESCRIPTOR);         
        RqlStatement statement = null;
        if(isFilterNonIndexableProducts()) {
            statement=RqlStatement.parseRqlStatement(QUERY_PRODUCTS_WITH_ALIASES_AND_DISPLAYABLE);
            vlogDebug("filtering using  the query- {0}", QUERY_PRODUCTS_WITH_ALIASES_AND_DISPLAYABLE);
        }else {
            statement=RqlStatement.parseRqlStatement(QUERY_PRODUCTS_WITH_ALIASES);  
            vlogDebug("fetching all products with alias using  the query- {0}", QUERY_PRODUCTS_WITH_ALIASES);
        }
        
        Object params[] = new Object[0];
        RepositoryItem[] items = statement.executeQuery(view, params);
        return items;
    }

    public int getValidProductsWithOrgnAliasesCount() throws RepositoryException {
        
        RepositoryItem[] items = queryValidProductsWithOrgnAliases();
        int length = 0;
        if(items==null) {
            length = -1;
        }else {
            length = items.length;
        }
        return length;
        
    }

	/**
	 * add organizations with aliases to repository
	 */
	public void addOrganizations() {
		try {

            RepositoryItem[] items = queryValidProductsWithOrgnAliases();

			Set<String> organizations = new HashSet<String>();

			for (RepositoryItem item : items) {
				Set<String> orgIds = (Set<String>) item.getPropertyValue(CPSConstants.PRODUCT_ITEM_DESCRIPTOR_KEY_ORGS);
				organizations.addAll(orgIds);
			}

			MutableRepositoryItem itemForUpdate = getOrgAliasRepository().getItemForUpdate(getItemId(), DESCRIPTOR);
			if (itemForUpdate != null) {
				itemForUpdate.setPropertyValue(PROPERTY_ORGANIZATIONS, organizations);
				getOrgAliasRepository().updateItem(itemForUpdate);
			}

		} catch (RepositoryException e) {
			vlogError(e, e.getMessage());
		}
	}

	/**
	 * check if organization contains alias
	 *
	 * @param pOrgId - organization id
	 * @return true if contains
	 */
	public boolean isOrganizationHasAlias(String pOrgId) {

		boolean result = false;
		try {
			RepositoryItem item = getOrgAliasRepository().getItem(getItemId(), DESCRIPTOR);
			if (item != null && !StringUtils.isEmpty(pOrgId)) {
				Set<String> organizations = (Set<String>) item.getPropertyValue(PROPERTY_ORGANIZATIONS);
				if (organizations.contains(pOrgId)) {
					result = true;
				}
			}
		} catch (RepositoryException e) {
			vlogError(e, e.getMessage());
		}
		return result;
	}

	/**
	 * returns set of org ids with aliases
	 *
	 * @return set of item ids
	 */
	public Set<String> getOrganizationsWithAlias() {

		Set<String> result = null;
		try {
			RepositoryItem item = getOrgAliasRepository().getItem(getItemId(), DESCRIPTOR);
			if (item != null) {
				result = (Set<String>) item.getPropertyValue(PROPERTY_ORGANIZATIONS);
			}
		} catch (RepositoryException e) {
			vlogError(e, e.getMessage());
		}
		if (result == null) {
			result = new HashSet<String>();
		}
		return result;
	}

	/**
	 * find alias by profile information
	 *
	 * @param pProfile - profile
	 * @param pProduct - product
	 * @return alias
	 */
	public String findAlias(Profile pProfile, RepositoryItem pProduct) {
		String productAlias = null;
		String orgId = null;

		RepositoryItem organization = getProfileTools().getParentOrganization(pProfile);
		if (organization != null) {
			orgId = organization.getRepositoryId();
		}

		if (orgId != null) {
			productAlias = findAliasByOrganizationId(pProduct, orgId);
		}

		return productAlias;
	}

	@SuppressWarnings("unchecked")
	public String findAliasByOrganizationId(RepositoryItem product, String orgId) {
		Map<String, String> productAliases = (Map<String, String>) product.getPropertyValue(PRODUCT_ALIAS_MAP);
		if (productAliases != null) {
			for (Entry<String, String> alias : productAliases.entrySet()) {
				if (alias.getKey().equals(orgId)) {
					return alias.getValue();
				}
			}
		}

		return null;
	}

	/**
	 * creates new item
	 *
	 * @throws RepositoryException
	 */
	private void createNewItem() throws RepositoryException {
		MutableRepositoryItem newItem = getOrgAliasRepository().createItem(getItemId(), DESCRIPTOR);
		getOrgAliasRepository().addItem(newItem);
	}

	public MutableRepository getOrgAliasRepository() {
		return mOrgAliasRepository;
	}

	public void setOrgAliasRepository(MutableRepository pOrgAliasRepository) {
		this.mOrgAliasRepository = pOrgAliasRepository;
	}

	public String getItemId() {
		return mItemId;
	}

	public void setItemId(String pItemId) {
		this.mItemId = pItemId;
	}

	@Override
	public boolean cancel(IndexingTask arg0, boolean arg1)
			throws IndexingException {
		return false;
	}

	@Override
	public Set<String> getIndexingOutputConfigPaths() {
		return Collections.emptySet();
	}

	@Override
	public boolean isForceToBaseline() {
		return false;
	}

	@Override
	public boolean isNeededForIncremental() {
		return true;
	}

	@Override
	public boolean isSupportsStatusCounts() {
		return false;
	}

	@Override
	public boolean mayNeedCleanup() {
		return false;
	}

	@Override
	public BulkLoaderResults performBaselineUpdate(IndexingTask pIndexingTask) throws IndexingException {
		clearRepository();
		addOrganizations();
		BulkLoaderResults results = new BulkLoaderResults(0);
		results.setBulkLoadUnnecessary(false);
		return results;
	}

	@Override
	public LoaderResults performPartialUpdate(IndexingTask pIndexingTask) throws IndexingException {
		clearRepository();
		addOrganizations();
		IncrementalLoaderResults results = new IncrementalLoaderResults(true, null);
		return results;
	}

	@Override
	public void postIndexingCleanup(IndexingTask arg0, boolean arg1,
									LoaderResults arg2) throws IndexingException {
	}

	public MutableRepository getProductCatalogRepository() {
		return mProductCatalogRepository;
	}

	public void setProductCatalogRepository(MutableRepository pProductCatalogRepository) {
		this.mProductCatalogRepository = pProductCatalogRepository;
	}

	public boolean isOraganizationHasAlias(RepositoryItem profile) {
		String orgId = null;
		if (profile != null && !profile.isTransient()) {
			RepositoryItem parentOrg = (RepositoryItem) profile.getPropertyValue(CPSConstants.PARENT_ORG);
			if (parentOrg != null) {
				orgId = parentOrg.getRepositoryId();
			}
		}

		if (orgId != null) {
			return isOrganizationHasAlias(orgId);
		}
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------

	public boolean isEnableSearchByAliasInterface() {
		return mEnableSearchByAliasInterface;
	}

	public void setEnableSearchByAliasInterface(boolean pEnableSearchByAliasInterface) {
		mEnableSearchByAliasInterface = pEnableSearchByAliasInterface;
	}

	public CPSProfileTools getProfileTools() {
		return mProfileTools;
	}

	public void setProfileTools(CPSProfileTools pProfileTools) {
		mProfileTools = pProfileTools;
	}

    /**
     * @return the filterNonIndexableProducts
     */
    public boolean isFilterNonIndexableProducts() {
        return filterNonIndexableProducts;
    }

    /**
     * @param filterNonIndexableProducts
     *            the filterNonIndexableProducts to set
     */
    public void setFilterNonIndexableProducts(boolean filterNonIndexableProducts) {
        this.filterNonIndexableProducts = filterNonIndexableProducts;
    }

}
