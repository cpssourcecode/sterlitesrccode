package com.cps.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import atg.adapter.gsa.ChangeAwareList;
import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.RepositoryItem;

import com.cps.commerce.order.purchase.CPSPurchaseProcessHelper;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.common.handler.DuplicatesMatchesHandlerInterface;
import com.cps.util.CPSConstants;

public class DuplicatesMatchesService extends GenericService {

    /**
     * profile tools
     */
    private CPSProfileTools mProfileTools;
    /**
     * CPS Purchase Process Helper
     */
    private CPSPurchaseProcessHelper mPurchaseProcessHelper;
    /**
     * product catalog
     */
    private MutableRepository mProductCatalog;

    public boolean makeValidSkuIdsList(RepositoryItem pProfile, DuplicatesMatchesHandlerInterface pHandler) {
        //return true if successful
        //input is quick order id entries could be item #, alias #, or cps short code
        boolean result = combineDuplicateQuickOrderEntries(pHandler); //removes duplicate input values and combines corresponding for 1 unique entry (list-->set) put into class variables quickOrderFinalSkus,quickOrderFinalQuanitites
        if (!result) {
            return false;
        }
        HashMap<String, HashMap<Integer, ArrayList<String>>> duplicatesFoundWithQuantity = new HashMap<String, HashMap<Integer, ArrayList<String>>>(); // <SearchTerm,<Qty,ListOfDupes>()
        //String[] skuList = getSkuIdsList().split(",");
        //String[] qtyList = getQtyList().split(",");
        //need to split up values that can be added and values to be displayed in duplicate modal, also eliminate values that nothing is found for on query and display those.
        ArrayList<Integer> addableQtyArrayList = new ArrayList<Integer>(); //no duplicates for these found, but something found so proceed with add to cart with these
        ArrayList<String> addableSkuArrayList = new ArrayList<String>(); //no duplicates for these found
        if (pHandler.getFinalSkus().size() > 0 && pHandler.getFinalQuantities().size() > 0 && pHandler.getFinalSkus().size() == pHandler.getFinalQuantities().size()) {
            String[] newSkuList = new String[pHandler.getFinalSkus().size()];
            for (int ind = 0; ind < pHandler.getFinalSkus().size(); ind++) {

                String currentSkuId = pHandler.getFinalSkus().get(ind).trim();
                RepositoryItem[] foundKeyProduct = findKeyProduct(currentSkuId, pProfile);

                vlogDebug("foundKeyProduct Array:" + foundKeyProduct);
                if (foundKeyProduct != null && foundKeyProduct.length == 1) {
                    vlogDebug("PRODUCT FOUND, NO DUPLICATES:" + foundKeyProduct[0]);
                    List<RepositoryItem> productChilds = (ChangeAwareList) foundKeyProduct[0].getPropertyValue("childSkus");
                    if (productChilds != null && productChilds.size() > 0) {
                        vlogDebug("CHILDS FOUND:" + productChilds);
                        vlogDebug("FIRST CHILD:" + productChilds.get(0));
                        currentSkuId = productChilds.get(0).getRepositoryId();
                        vlogDebug("INSIDE CURRENT SKU ID=" + currentSkuId);
                    }
                    addableSkuArrayList.add(currentSkuId);
                    addableQtyArrayList.add(pHandler.getFinalQuantities().get(ind)); //qty and sku should be same length
                } else if (foundKeyProduct != null && foundKeyProduct.length > 1) {
                    HashMap<Integer, ArrayList<String>> duplicates = new HashMap<Integer, ArrayList<String>>(); // <Qty,ListOfDupes>()
                    ArrayList<String> dupeSkus = new ArrayList<String>();
                    for (int dind = 0; dind < foundKeyProduct.length; dind++) {
                        vlogDebug("PRODUCT FOUND, WITH DUPLICATES:" + foundKeyProduct[dind]);
                        List<RepositoryItem> productChilds = (ChangeAwareList) foundKeyProduct[dind].getPropertyValue("childSkus");
                        if (productChilds != null && productChilds.size() > 0) {
                            vlogDebug("CHILDS FOUND:" + productChilds);
                            vlogDebug("FIRST CHILD:" + productChilds.get(0));
                            String dupeSkuId = productChilds.get(0).getRepositoryId();
                            vlogDebug("DUPE SKU ID ADDED=" + dupeSkuId);
                            dupeSkus.add(dupeSkuId);
                        }
                    }
                    duplicates.put(pHandler.getFinalQuantities().get(ind), dupeSkus); // <Qty,ListOfDupes>()
                    duplicatesFoundWithQuantity.put(pHandler.getFinalSkus().get(ind), duplicates); //<SearchSkuKey,<Qty,ListOfDupes<>>
                    //multiple options for specific search term, need to make dupe list && NOT remove these from final array
                } else {
                    //foundKeyProduct==null //add original value to addable Lists, will be set to session for invalid entry list in handleAdd method
                    vlogDebug("THIS SHOULD ERROR OUT ON TOP OF ERROR MODAL-SKU-" + currentSkuId);
                    addableSkuArrayList.add(currentSkuId);
                    addableQtyArrayList.add(pHandler.getFinalQuantities().get(ind));
                }
            }
            String addableQtys = "";
            for (int ind = 0; ind < addableQtyArrayList.size(); ind++) {
                addableQtys += addableQtyArrayList.get(ind) + ",";
            }
            String addableSkus = "";
            for (int ind = 0; ind < addableSkuArrayList.size(); ind++) {
                addableSkus += addableSkuArrayList.get(ind) + ",";
            }
            pHandler.setQtyList(addableQtys); //list with duplicates removed ready for query
            pHandler.setSkuIdsList(addableSkus); //list with duplicates removed read for query
            pHandler.setDuplicates(duplicatesFoundWithQuantity); //HashMap<SearchSkuKey,<Qty,ListOfDupes<>> to be used to display duplicates modal in session
            return true;
        } else {
            //Don't add anything to cart, input was wrong
            pHandler.setQtyList("");
            pHandler.setSkuIdsList("");
            pHandler.setDuplicates(null); //HashMap<SearchSkuKey,<Qty,ListOfDupes<>> to be used to display duplicates modal in session
            return false;
        }
    }

    public boolean combineDuplicateQuickOrderEntries(DuplicatesMatchesHandlerInterface pHandler) {
        ArrayList<String> qtyArrayList = new ArrayList<String>(Arrays.asList(pHandler.getQtyList().split(",")));
        ArrayList<Integer> qtyArrayListInteger = new ArrayList<Integer>();
        for (String qtyStr : qtyArrayList) {
            try {
                if (qtyStr != null && !qtyStr.equals("")) {
                    qtyArrayListInteger.add(Integer.parseInt(qtyStr));
                }
            } catch (NumberFormatException nfe) {
                //add form error, return or default value
                return false;
            }
        }

        ArrayList<String> skuArrayList = new ArrayList<String>(Arrays.asList(pHandler.getSkuIdsList().split(",")));
        vlogDebug("INITIAL QTYARRAYLIST:" + qtyArrayList);
        vlogDebug("INITIAL SKUARRAYLIST:" + skuArrayList);
        if (qtyArrayListInteger.size() == skuArrayList.size()) {
            for (int ind = 0; ind < skuArrayList.size(); ind++) {
                String firstEntry = skuArrayList.get(ind);

                for (int ind2 = 0; ind2 < skuArrayList.size(); ind2++) {
                    if (ind != ind2 && skuArrayList.get(ind2).equals(skuArrayList.get(ind))) { //check not to add something to itself && then if different index is duplicate value
                        skuArrayList.remove(ind2); // remove duplicate found
                        qtyArrayListInteger.set(ind, qtyArrayListInteger.get(ind) + qtyArrayListInteger.get(ind2)); // add duplicate corresponding quantity to first value
                        qtyArrayListInteger.remove(ind2); //remove duplicate quantity
                    }
                }
            }
            //String finalQtys;
            //for(int ind=0; ind<qtyArrayListInteger.size(); ind++){
            //	finalQtys+=qtyArrayListInteger.get(ind)+",";
            //}
            //String finalEntries;
            //for(int ind=0; ind<skuArrayList.size(); ind++){
            //	finalEntries+=skuArrayList.get(ind)+",";
            //}
            pHandler.setFinalSkus(skuArrayList);
            pHandler.setFinalQuantities(qtyArrayListInteger);
            vlogDebug("FINAL UNIQUE QTYARRAYLIST:" + pHandler.getFinalQuantities());
            vlogDebug("FINAL UNIQUE SKUARRAYLIST:" + pHandler.getFinalSkus());
            //setQtyList(finalQtys); //list with duplicates removed ready for query
            //setSkuIdsList(finalEntries); //list with duplicates removed read for query
        } else {
            //initial qty list and initial sku list don't have same length, missed a value on entry. DO NOTHING
            if(isLoggingDebug()){
                logDebug("initial qty list and initial sku list don't have same length, missed a value on entry");
            }
        }
        return true;
    }

    /**
     * find key product
     *
     * @param pCurrentSkuId - current sku
     * @return array of products
     */
    public RepositoryItem[] findKeyProduct(String pCurrentSkuId, RepositoryItem pProfile) {
        String orgId = "";
        RepositoryItem productFromAlias = null;
        if (pProfile != null && !pProfile.isTransient()) {
            RepositoryItem org = (RepositoryItem) pProfile.getPropertyValue(CPSConstants.PARENT_ORG);
            if (org != null) {
                orgId = org.getRepositoryId();
                productFromAlias = ((CPSPurchaseProcessHelper) getPurchaseProcessHelper()).findProductFormAlias(pCurrentSkuId, orgId);
            }
        }

        Object[] paramsForIdQuery = {pCurrentSkuId.toUpperCase(), pCurrentSkuId.toLowerCase()};
        String keyQuery = "displayName=?0 OR id=?0 OR displayName=?1 OR id=?1";
        RepositoryItem[] productsById = getProfileTools().queryRepository(paramsForIdQuery, keyQuery, "product", getProductCatalog());

        RepositoryItem[] finalFoundProducts;
        if (productsById != null && productsById.length > 0 && productFromAlias != null) {
            finalFoundProducts = new RepositoryItem[productsById.length + 1];
            for (int ind = 0; ind < productsById.length; ind++) {
                finalFoundProducts[ind] = productsById[ind];
            }
            finalFoundProducts[finalFoundProducts.length - 1] = productFromAlias;
            return finalFoundProducts;
        } else if (productFromAlias != null) {
            finalFoundProducts = new RepositoryItem[1];
            finalFoundProducts[0] = productFromAlias;
            return finalFoundProducts;
        } else {
            return productsById;
        }
    }

    public CPSProfileTools getProfileTools() {
        return mProfileTools;
    }

    public void setProfileTools(CPSProfileTools pProfileTools) {
        this.mProfileTools = pProfileTools;
    }

    public CPSPurchaseProcessHelper getPurchaseProcessHelper() {
        return mPurchaseProcessHelper;
    }

    public void setPurchaseProcessHelper(
            CPSPurchaseProcessHelper pPurchaseProcessHelper) {
        this.mPurchaseProcessHelper = pPurchaseProcessHelper;
    }

    public void setProductCatalog(MutableRepository productCatalog) {
        this.mProductCatalog = productCatalog;
    }

    public MutableRepository getProductCatalog() {
        return mProductCatalog;
    }

}
