package com.cps.service;

import java.io.IOException;

import com.cps.endeca.assembler.content.DefaultFileStoreFactory;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
import atg.service.scheduler.SingletonSchedulableService;

/**
 * Class UpdateFileStoreService make check of file store updates when changes and update
 * 
 * @author Vasili Ivus
 *
 */
public class UpdateFileStoreService extends SingletonSchedulableService {

	/**
	 * DefaultFileStoreFactory component
	 */
	private DefaultFileStoreFactory mFileStoreFactory;

	/**
	 * last rev name
	 */
	private String mLastRevName = null;

	public DefaultFileStoreFactory getFileStoreFactory() {
		return mFileStoreFactory;
	}

	public void setFileStoreFactory(DefaultFileStoreFactory pFileStoreFactory) {
		mFileStoreFactory = pFileStoreFactory;
	}

	public String getLastRevName() {
		return mLastRevName;
	}

	public void setLastRevName(String pLastRevName) {
		mLastRevName = pLastRevName;
	}

	/**
	 * Check of file store updates.
	 */
	@Override
	public void doScheduledTask(Scheduler arg0, ScheduledJob arg1) {
		final DefaultFileStoreFactory factory = getFileStoreFactory();
		if ( null != factory ) {
			String currentRevName;
			try {
				currentRevName = factory.getCurrentRevName();
			} catch (IOException e) {
				currentRevName = null;
				vlogError(e, "Could not read filestore revision.");
			}
			if ( null != currentRevName ) {
				if ( null == mLastRevName || !mLastRevName.equals(currentRevName) ) {
					vlogDebug("Call updateStore. Current revision: {0}, last revision: {1}", currentRevName, mLastRevName);
					mLastRevName = currentRevName;
					try {
						factory.updateStore();
					} catch (IOException e) {
						vlogError(e, "Unable to update store");
					}
				}
			}
		}
	}

}
