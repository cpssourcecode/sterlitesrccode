package com.cps.service.server;

import com.cps.servlet.ProtocolChangeServlet;

import atg.core.net.URLParts;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

public class SecureServerService extends GenericService {

	//-------------------------------------
	/** Class version string */
	public static final String CLASS_VERSION = "$Id: ";
	//-------------------------------------
	public final static String SECURE_SERVER_SERVICE = "/cps/service/server/SecureServerService";
	public final static String PROTOCOL_CHANGE_SERVLET = "/atg/dynamo/servlet/dafpipeline/ProtocolChangeServlet";

	//-------------------------------------
	static ProtocolChangeServlet _protocolChange = null;

	//-------------------------------------
	static SecureServerService mSecureServerService = null;


	/**
	 */
	public static final SessionServerInfo getSessionServerInfo(DynamoHttpServletRequest pRequest) {

		return SessionServerInfo.getSessionServerInfoFromRequest(pRequest);
	}


	/**
	 * This method isn't really used. It's for enabling a "per" session
	 * access to secure server.
	 */
	public static final String serverURL(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String pUrl) {

		SessionServerInfo info = getSessionServerInfo(pRequest);
		if (info == null) {
			return pUrl;
		}

		if (info.getUseSecureServer()) {
			return (secureServerURL(pRequest, pResponse, pUrl));
		} else {
			return (inSecureServerURL(pRequest, pResponse, pUrl));
		}
	}


	/**
	 */
	public static final String inSecureServerURL(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String pUrl) {

		if (isEnabled(pRequest) == false) {
			return pUrl;
		}

		SecureServerService service = getSecureServerService(pRequest);
		if (service.isLoggingDebug()) {
			service.logDebug("********** inSecureServerURL **********");
		}

		// Commented this out for testing
		if (pRequest.getScheme() != null &&
				pRequest.getScheme().equalsIgnoreCase("http")) {
			if (service.isLoggingDebug()) {
				service.logDebug("Not changing the URL: " + pUrl);
			}
			return pUrl;
		}

		String server = null;
		String port = null;
		SessionServerInfo info = getSessionServerInfo(pRequest);
		if (info != null) {
			server = info.getServer();
			port = info.getPort();
		}

		if (service.isLoggingDebug()) {
			service.logDebug("Information from SessionServerInfo");
			service.logDebug("Port = " + port);
			service.logDebug("Server = " + server);
		}

		// Sometimes the session info ends up as null, set some defaults here.
		if (server == null) {
			server = service.getInSecureServer();
			port = ((Integer)service.getInSecurePort()).toString();
		}


		if (service.isLoggingDebug()) {
			service.logDebug("Information from SessionServerService");
			service.logDebug("Port = " + port);
			service.logDebug("Server = " + server);
		}

		if (server == null) {
			server = pRequest.getServerName();
		}
		if (service.isLoggingDebug()) {
			service.logDebug("Information from servername");
			service.logDebug("Server = " + server);
		}
		//
		int intPort = -1;
		if (port != null && !StringUtils.isEmpty(port) && isNumeric(port))
			intPort = Integer.parseInt(port);

		// HttpServletRequest servReq = (HttpServletRequest) pRequest;


		StringBuffer reqURL = pRequest.getRequestURL();
		URLParts inURL = new URLParts(new URLParts(reqURL.toString()), pUrl);

		//
		inURL = new URLParts("http", server, intPort, inURL.getFile());

		// Don't encode the URL, the encode with be done elsewhere
		pUrl = inURL.toString();
		return pUrl;
	}


	/**
	 */
	public static final String secureServerURL(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse, String pUrl) {

		if (isEnabled(pRequest) == false) {
			return pUrl;
		}

		SecureServerService service = getSecureServerService(pRequest);
		if (service.isLoggingDebug()) {
			service.logDebug("********** secureServerURL **********");
		}

		String port = ((Integer)service.getSecurePort()).toString();
		String server = service.getSecureServer();

		if (service.isLoggingDebug()) {
			service.logDebug("Information from SessionServerService");
			service.logDebug("Port = " + port);
			service.logDebug("Server = " + server);
		}

		if (server == null) {
			server = pRequest.getServerName();
		}

		//
		int intPort = -1;
		if (port != null && !StringUtils.isEmpty(port) && isNumeric(port))
			intPort = Integer.parseInt(port);

		/* Otherwise, build it */

		if (pRequest.getScheme() != null &&
				pRequest.getScheme().equalsIgnoreCase("https")) {
			return pUrl;
		}

		URLParts reqURL = new URLParts(pRequest.getRequestURL().toString());
		URLParts inURL = new URLParts(reqURL, pUrl);

		// Use "http" for testing.
		inURL = new URLParts("https", server, intPort, inURL.getFile());

		// Don't encode the URL, the encode with be done elsewhere
		pUrl = inURL.toString();
		return pUrl;
	}


	/**
	 * Checks string to see if it is numeric
	 *
	 * @param s - String to test
	 * @return boolean - true for number
	 */
	public static boolean isNumeric(String s) {

		char[] cars = s.toCharArray();
		for (int i = 0; i < cars.length; i++) {
			if (!Character.isDigit(cars[i])) {
				return false;
			}
		}
		return true;
	}


	/**
	 */
	public static boolean isEnabled(DynamoHttpServletRequest pRequest) {

		if (_protocolChange == null) {
			_protocolChange = (ProtocolChangeServlet) pRequest.resolveName(PROTOCOL_CHANGE_SERVLET);
		}

		if (_protocolChange == null || _protocolChange.isEnabled() == false) {
			return false;
		}

		return true;
	}


	/**
	 */
	public static SecureServerService getSecureServerService(DynamoHttpServletRequest pRequest) {

		if (mSecureServerService == null) {
			mSecureServerService = (SecureServerService) pRequest.resolveName(SECURE_SERVER_SERVICE);
		}

		return mSecureServerService;
	}

	//-------------------------------------
	// Properties

	//-------------------------------------
	// property: SecureServer
	//

	private String mSecureServer;


	/**
	 * Gets property SecureServer
	 **/
	public String getSecureServer() {

		return mSecureServer;
	}


	/**
	 * Sets property SecureServer
	 **/
	public void setSecureServer(String pSecureServer) {

		mSecureServer = pSecureServer;
	}

	//-------------------------------------
	// property: SecurePort
	//

	private int mSecurePort;


	/**
	 * Gets property SecurePort
	 **/
	public int getSecurePort() {

		return mSecurePort;
	}


	/**
	 * Sets property SecurePort
	 **/
	public void setSecurePort(int pSecurePort) {

		mSecurePort = pSecurePort;
	}


	//-------------------------------------
	// property: InSecureServer
	//

	private String mInSecureServer;


	/**
	 * Gets property InSecureServer
	 **/
	public String getInSecureServer() {

		return mInSecureServer;
	}


	/**
	 * Sets property InSecureServer
	 **/
	public void setInSecureServer(String pInSecureServer) {

		mInSecureServer = pInSecureServer;
	}

	//-------------------------------------
	// property: InSecurePort
	//

	private int mInSecurePort;


	/**
	 * Gets property InSecurePort
	 **/
	public int getInSecurePort() {

		return mInSecurePort;
	}


	/**
	 * Sets property InSecurePort
	 **/
	public void setInSecurePort(int pInSecurePort) {

		mInSecurePort = pInSecurePort;
	}
}