package com.cps.service.server;

import atg.servlet.DynamoHttpServletRequest;

/**
 * Session-scoped bean to store information about the unsecure server
 * to which to go when switching out of the secure server.
 *
 **/


public class SessionServerInfo {

	//-------------------------------------
	// Class version string
	public static final String CLASS_VERSION = "$Id: SessionServerInfo.java";

	//-------------------------------------
	public final static String USER_SESSION_SERVICE = "/cps/service/server/SessionServerInfo";
	public final static String USER_SESSION_ATTRIBUTE = "user_session";


	/**
	 * Store this object in the session request, caching it.
	 */

	public static final SessionServerInfo getSessionServerInfoFromRequest(DynamoHttpServletRequest pRequest) {

		SessionServerInfo info = (SessionServerInfo) pRequest.getAttribute(USER_SESSION_ATTRIBUTE);
		if (info == null) {
			info = ((SessionServerInfo) pRequest.resolveName(USER_SESSION_SERVICE));
			pRequest.setAttribute(USER_SESSION_ATTRIBUTE, info);
		}

		return info;
	}

	//-------------------------------------
	// Properties

	//-------------------------------------
	// property: UseSecureServer
	//
	private boolean mUseSecureServer = true;


	/**
	 * Gets property UseSecureServer
	 **/
	public boolean getUseSecureServer() {

		return mUseSecureServer;
	}


	/**
	 * Sets property UseSecureServer
	 **/
	public void setUseSecureServer(boolean pUseSecureServer) {

		mUseSecureServer = pUseSecureServer;
	}

	//-------------------------------------
	// property: Port
	//
	private boolean mPortIsSet = false;
	private String mPort = null;


	/**
	 * Gets property Port
	 **/
	public String getPort() {

		return mPort;
	}


	/**
	 * Sets property Port
	 **/
	public void setPort(String pPort) {

		if (!mPortIsSet) {
			if ((pPort != null) && !pPort.equals("80"))
				mPort = pPort;

			mPortIsSet = true;
		}
	}

	//-------------------------------------
	// property: Server
	//
	private boolean mServerIsSet = false;
	private String mServer = null;


	/**
	 * Gets property Server
	 **/
	public String getServer() {

		return mServer;
	}


	/**
	 * Sets property Server
	 **/
	public void setServer(String pServer) {

		if (!mServerIsSet) {
			mServer = pServer;
			mServerIsSet = true;
		}
	}
}

