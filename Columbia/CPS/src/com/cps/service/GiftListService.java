package com.cps.service;

import atg.commerce.gifts.GiftlistManager;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import com.cps.util.CPSConstants;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.cps.util.CPSConstants.*;

/**
 * @author Dmitry Golubev
 **/
public class GiftListService extends GenericService {
	/**
	 * gift list manager
	 */
	private GiftlistManager giftlistManager;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * @param pGiftListItem gift list repository item
	 * @return set of available items
	 */
	public Set<RepositoryItem> getAvailableGiftListItems(RepositoryItem pGiftListItem) {
		Set<RepositoryItem> availableSet = new HashSet<RepositoryItem>();

		List<RepositoryItem> giftlistItems = (List<RepositoryItem>) pGiftListItem.getPropertyValue(GIFTLIST_ITEMS);
		if(giftlistItems!=null) {
			for(Object giftListItemObj : giftlistItems) {
				RepositoryItem giftListItem = (RepositoryItem)giftListItemObj;
				//if(!isItemShouldBeExcluded(giftListItem)) {
					availableSet.add( giftListItem );
				//}
			}
		}

		return availableSet;
	}

	/**
	 * @param pGiftItem gift item
	 * @return true if items properties matchs
	 */
	public boolean isItemShouldBeExcluded(RepositoryItem pGiftItem) {
		try {
			String productId = (String) pGiftItem.getPropertyValue(PRODUCT_ID);
			RepositoryItem product = getGiftlistManager().getCatalogTools().findProduct( productId );

			String productStockingCode = (String) product.getPropertyValue(CPSConstants.STOCKING_TYPE);
			boolean productPurchasable = (boolean) product.getPropertyValue(CPSConstants.PRODUCT_PURCHASABLE);
			vlogDebug("productPurchasable :: {0} ", productPurchasable);
			if (productStockingCode.equals(CPSConstants.STOCKING_TYPE_OBSOLETE)
					|| productStockingCode.equals(CPSConstants.STOCKING_TYPE_USEUP)
					|| productStockingCode.equals(CPSConstants.STOCKING_TYPE_K)
					|| productStockingCode.equals(CPSConstants.STOCKING_TYPE_X) 
					|| productStockingCode.equals(CPSConstants.PRODUCT_REP_ITEM_STOCKING_TYPE_FOUR)){
				return true;
			}
			if(!productPurchasable) {
				return true;
			}

			RepositoryItem parentCategory = (RepositoryItem) product.getPropertyValue(CPSConstants.PARENT_CATEGORY);
			if (parentCategory != null) {
				return !(Boolean) parentCategory.getPropertyValue(CPSConstants.ECOMMERCE_DISPLAY);
			}
		} catch (Exception e) {
			vlogError(e, "Unable to check gift item for exclude rules");
		}
		return false;
	}

	/**
	 * @param items giftlist items
	 * @return number of desired items
	 */
	public int getDesiredQty(Collection<RepositoryItem> items) {
		int totalGiftlistItems = 0;
		for (RepositoryItem item : items) {
			if (!isItemShouldBeExcluded(item)) {
				totalGiftlistItems += (Long) item.getPropertyValue(QUANTITY_DESIRED);
			}
		}
		return totalGiftlistItems;
	}

	//------------------------------------------------------------------------------------------------------------------


	public GiftlistManager getGiftlistManager() {
		return giftlistManager;
	}

	public void setGiftlistManager(GiftlistManager pGiftlistManager) {
		giftlistManager = pGiftlistManager;
	}
}
