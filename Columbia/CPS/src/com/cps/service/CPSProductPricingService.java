package com.cps.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.cps.commerce.order.CPSOrderImpl;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSSessionBean;

import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.servlet.ServletUtil;
import cps.commerce.pricing.priceLists.CPSPriceListManager;

public class CPSProductPricingService extends GenericService {

    private CPSPriceListManager priceListManager;
    private CPSProfileTools profileTools;
	private Repository productCatalog;
    private CPSExternalProductService externalProductService;
    private String sessionBeanPath;

    private boolean testMode;

    /**
     * @return the sessionBeanPath
     */
    public String getSessionBeanPath() {
        return sessionBeanPath;
    }

    /**
     * @param sessionBeanPath
     *            the sessionBeanPath to set
     */
    public void setSessionBeanPath(String sessionBeanPath) {
        this.sessionBeanPath = sessionBeanPath;
    }

    /**
     * @return the profileTools
     */
    public CPSProfileTools getProfileTools() {
        return profileTools;
    }

    /**
     * @param profileTools
     *            the profileTools to set
     */
    public void setProfileTools(CPSProfileTools profileTools) {
        this.profileTools = profileTools;
    }

    /**
     * @return the testMode
     */
	public boolean isTestMode() {
		return testMode;
	}

	/**
	 * @param testMode
	 *            the testMode to set
	 */
	public void setTestMode(boolean testMode) {
		this.testMode = testMode;
	}

	/**
	 * @return the externalProductService
	 */
	public CPSExternalProductService getExternalProductService() {
		return externalProductService;
	}

	/**
	 * @param externalProductService
	 *            the externalProductService to set
	 */
	public void setExternalProductService(CPSExternalProductService externalProductService) {
		this.externalProductService = externalProductService;
	}

	/**
	 * @return the priceListManager
	 */
    public CPSPriceListManager getPriceListManager() {
		return priceListManager;
	}

	/**
	 * @param priceListManager
	 *            the priceListManager to set
	 */
    public void setPriceListManager(CPSPriceListManager priceListManager) {
		this.priceListManager = priceListManager;
	}


	/**
	 * @return the productCatalog
	 */
	public Repository getProductCatalog() {
		return productCatalog;
	}

	/**
	 * @param productCatalog
	 *            the productCatalog to set
	 */
	public void setProductCatalog(Repository productCatalog) {
		this.productCatalog = productCatalog;
	}

	/**
	 * This method will check whether the user's price must be taken from ATG or
	 * need to make a webservice call. and call the PriceListManager or
	 * CPSExternalProductService accordingly
	 * 
	 * @param products
	 * @param profile
	 * @param order
	 * @return
	 */
	public Map<String, Double> getCustomerItemPrice(Set<String> products, RepositoryItem profile, CPSOrderImpl order) {
        Map<String, Double> priceResultsMap = null;
        // check whether to get the price from ATG
        String priceMatrix = getProfileTools().getPriceMatrixLevelForGivenProfileAddress(profile, order);
        if (priceMatrix != null || isTestMode()) {
            // the profile qualifies for fetching prices from the ATG PriceList Repository.
            vlogDebug("the profile is anonymous or the test mode is enabled or the address qualifies for ATG priceList pricing. fetching prices from ATG PriceList");
            priceResultsMap = getPriceListManager().getPricesFromATGPriceList(products, profile, priceMatrix, order);
		} else {
            priceResultsMap = new HashMap<>();
            CPSSessionBean sessionBean = (CPSSessionBean) ServletUtil.getCurrentRequest().resolveName(getSessionBeanPath());
            Map<String, Double> priceCacheMap = sessionBean.getProductPricesCache();
            vlogDebug("For this address, there is a override and Hence fetching prices from external webservice and caching them to in-memory price cache");
            Set<String> productIdsForWhichPricesToBeFetched = filteredProductIdsForWebServiceCall(products, priceResultsMap, priceCacheMap);
            if (productIdsForWhichPricesToBeFetched.size() > 0) {
                // this means there are some product prices which are not there in the cache. fetch the prices from JDE only for them.
                // need to make a web service call.
                Map pricesFromJDEMap = getExternalProductService().getCustomerItemPrice(productIdsForWhichPricesToBeFetched, profile, order);
                if (pricesFromJDEMap != null) {
                priceResultsMap.putAll(pricesFromJDEMap);
                // also add those product prices into the Cache for further use.
                priceCacheMap.putAll(pricesFromJDEMap);
                }

            }

		}
        return priceResultsMap;
	}

    private Set<String> filteredProductIdsForWebServiceCall(Set<String> products, Map<String, Double> priceResultsMap, Map<String, Double> priceCacheMap) {

        Set<String> productIdsForWhichPricesToBeFetched = new HashSet<>();
        // check whether the price for that item is not there in the cache. if it is not,then add it to the new Set to be sent to JDE
        for (String productId : products) {
            if (priceCacheMap.containsKey(productId)) {
                // add the prices from the cache into the result pricesMap.
                priceResultsMap.put(productId, priceCacheMap.get(productId));
            } else {
                productIdsForWhichPricesToBeFetched.add(productId);
            }
        }
        if (isLoggingDebug()) {
            int requestedNoOfItems = products.size();
            int itemsForWhichServiceCallToBeMade = productIdsForWhichPricesToBeFetched.size();
            int itemsFetchedFromCache = requestedNoOfItems - itemsForWhichServiceCallToBeMade;
            vlogDebug("out of requested - {0} items, fetched - {1} items from cache and made web service call for - {2} iterms", requestedNoOfItems,
                            itemsFetchedFromCache, itemsForWhichServiceCallToBeMade);
        }
        return productIdsForWhichPricesToBeFetched;
    }



}
