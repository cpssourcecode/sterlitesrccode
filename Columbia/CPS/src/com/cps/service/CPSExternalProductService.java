package com.cps.service;

import static com.cps.integrations.pricing.CPSPricingConstants.DEFAULT_QTY;
import static com.cps.util.CPSConstants.CART_SELECTED_DELIVERY_METHOD;
import static com.cps.util.CPSConstants.CART_SELECTED_STORE;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_PICK_UP;
import static com.cps.util.CPSConstants.DELEVERY_METHOD_SHIPPED;
import static com.cps.util.CPSConstants.STORE_BRANCH_ID_PRTY;
import static com.cps.util.CPSConstants.STORE_ITEM_TYPE;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import com.cps.commerce.inventory.CPSInventoryConnectionManager;
import com.cps.commerce.order.CPSAvailabilityManager;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.integrations.pricing.CPSPricingClientMultiStatic;
import com.cps.inventory.InventoryAvailabilityInfo;
import com.cps.userprofiling.CPSProfileTools;
import com.cps.util.CPSSessionBean;

import atg.beans.DynamicBeans;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.ShippingGroup;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.userprofiling.Profile;

public class CPSExternalProductService extends GenericService {

    private static final double EMPTY_PRICE = 0.0;

    private CPSPricingClientMultiStatic mPricingClientMulti;

    private CPSInventoryConnectionManager inventoryConnectionManager;
    private CPSProfileTools profileTools;

    private String defaultBranch;
    private String defaultCustomerId;

    private String mSessionBeanPath;

    private CPSAvailabilityManager mAvailabilityManager;
    // private CPSProductPricingService productPricingService;

    /**
     * LocationRepository
     */
    private Repository mLocationRepository;
    /**
     * ProfileRepository
     */
    private Repository mProfileRepository;

    private int mLeadTimeThreshold = 7;

    public double getCustomerItemPrice(String pProductId, String pCustomerId, String pBranch) {
        CPSSessionBean sessionBean = (CPSSessionBean) ServletUtil.getCurrentRequest().resolveName(getSessionBeanPath());
        sessionBean.setAvailablePricesWebService(true);

        Map<String, Double> priceCacheMap = sessionBean.getProductPricesCache();
        if (priceCacheMap != null && priceCacheMap.containsKey(pProductId)) {
            if (priceCacheMap.get(pProductId) != null) {
                vlogDebug("Price values available in Price cache in the memory. not making JDE call");
                return priceCacheMap.get(pProductId);
            }
        }

        if (isPricingWebserviceEnable()) {

            Set<String> products = new HashSet<String>();
            products.add(pProductId);
            Map<String, Double> itemPrices = new HashMap<>();
            try {
                itemPrices = getCustomerItemPrice(products, pCustomerId, pBranch);

            } catch (Exception ex) {
                if (isLoggingError()) {
                    logError(ex);
                }
            }
            Double uPrice = null;
            if (itemPrices != null) {
                uPrice = itemPrices.get(pProductId);
            }
            priceCacheMap.put(pProductId, uPrice);
            return uPrice == null ? EMPTY_PRICE : uPrice;
        }
        sessionBean.setAvailablePricesWebService(false);
        return EMPTY_PRICE;
    }

    /**
     * check whether the price is there in the cacheMap.if not, make the JDE call.update the cache and then return the price
     *
     * @param pProductId
     * @param pProfile
     * @param pOrder
     * @return
     */

    public double getCustomerItemPrice(String pProductId, RepositoryItem pProfile, CPSOrderImpl pOrder) {
        String customerId = getProfileTools().getCustomerId(pProfile, pOrder);
        String branch = getProfileTools().getBranch(pProfile, pOrder);
        return getCustomerItemPrice(pProductId, customerId, branch);
    }

    public Map<String, Double> getCustomerItemPrice(Set<String> pProducts, RepositoryItem pProfile, CPSOrderImpl pOrder) {
        String customerId = getProfileTools().getCustomerId(pProfile, pOrder);
        String branch = getProfileTools().getBranch(pProfile, pOrder);
        Map<String, Double> pricesMap = getCustomerItemPrice(pProducts, customerId, branch);
        return pricesMap;
    }

    public Map<String, Double> getCustomerItemPrice(Set<String> pProducts, String pProfileId, String pBranch) {
        boolean isDebug = isLoggingDebug();
        boolean availablePricesWebService = false;
        if (getAvailabilityManager() != null && getAvailabilityManager().isJdeAvailable()) {
            availablePricesWebService = true;
        }
        Map<String, Double> pricesMap = null;
        // boolean availablePricesWebService = getAvailabilityManager().isJdeAvailable();
        // TODO check whether the price is there in the Session PriceList for those products. if yes, dont query JDE again.

        if (availablePricesWebService) {
            try {
                if (getPricingClientMulti() != null) {
                    pricesMap = getPricingClientMulti().getCustomerItemPriceAsync(pProducts, pProfileId, DEFAULT_QTY, pBranch, isDebug);
                }
            } catch (InterruptedException e) {
                vlogError(e, "Error");
                Thread.currentThread().interrupt();
            } catch (ExecutionException e) {
                vlogError(e, "Error");
            } catch (IOException e) {
                vlogError(e, "Error");
            }
        }

        DynamoHttpServletRequest req = ServletUtil.getCurrentRequest();
        if (req != null) {
            CPSSessionBean sessionBean = (CPSSessionBean) req.resolveName(getSessionBeanPath());
            if (sessionBean != null) {
                sessionBean.setAvailablePricesWebService(availablePricesWebService);
            }
        }

        return pricesMap;
    }

    public double getCustomerItemPrice(String productId, String customerId, RepositoryItem profile, CPSOrderImpl pOrder) {
        String branch = getProfileTools().getBranch(profile, pOrder);
        return getCustomerItemPrice(productId, customerId, branch);
    }

    public InventoryAvailabilityInfo requestInventory(String pCustomerId, String pProductId, long pOrderedQty) throws Exception {
        if (isInventoryWebserviceEnable()) {
            return inventoryConnectionManager.requestInventory(pCustomerId, pProductId, pOrderedQty);
        }
        return null;
    }

    public List<InventoryAvailabilityInfo> requestInventory(String pCustomerId, String pProductId, String pBranchId, long pOrderedQty) throws Exception {
        if (isInventoryWebserviceEnable()) {
            return inventoryConnectionManager.requestInventory(pCustomerId, pProductId, pBranchId, pOrderedQty);
        }
        return null;
    }

    public InventoryAvailabilityInfo requestInventory(String pProductId, Profile profile, CPSOrderImpl pOrder, long pOrderedQty) throws Exception {
        String customerId = getProfileTools().getCustomerId(profile, pOrder);
        return this.requestInventory(customerId, pProductId, pOrderedQty);
    }

    public List<InventoryAvailabilityInfo> requestStoreInventoryFromCart(String pProductId, Profile profile, CPSOrderImpl pOrder, long pOrderedQty)
                    throws Exception {
        String customerId = getProfileTools().getCustomerId(profile, pOrder);
        String branchId = getBranchForAvailability(profile, pOrder);
        vlogDebug("requestInventoryStore. Defined branchId: " + branchId);
        if (StringUtils.isNotBlank(customerId)) {
            return this.requestInventory(customerId, pProductId, branchId, pOrderedQty);
        } else {
            return this.requestInventory(getDefaultCustomerId(), pProductId, branchId, pOrderedQty);
        }
    }

    public List<InventoryAvailabilityInfo> requestStoreInventoryFromPDP(String pProductId, Profile profile, CPSOrderImpl pOrder, long pOrderedQty)
                    throws Exception {
        String customerId = getProfileTools().getCustomerId(profile, pOrder);
        String branchId = null;
        String customerDefaultBranch  = DynamicBeans.getSubPropertyValueAsString(profile, "parentOrganization.billingAddress.businessUnit");
        if(pOrder.getCommerceItemCount() == 0) {
            // no items in cart use BU as branch id
            branchId = DynamicBeans.getSubPropertyValueAsString(profile, "parentOrganization.billingAddress.businessUnit");
        } else {
            // use cart selected branch id
            branchId = getBranchForAvailability(profile, pOrder, customerDefaultBranch);
        }
        vlogDebug("requestInventoryStore. Defined branchId: " + branchId);
        if (StringUtils.isNotBlank(customerId)) {
            return this.requestInventory(customerId, pProductId, branchId, pOrderedQty);
        } else {
            return this.requestInventory(getDefaultCustomerId(), pProductId, branchId, pOrderedQty);
        }
    }

    public List<InventoryAvailabilityInfo> requestInventoryStore(String pProductId, Profile profile, CPSOrderImpl pOrder, long pOrderedQty) throws Exception {
        String customerId = getProfileTools().getCustomerId(profile, pOrder);
        String branchId = getBranchForAvailability(profile, pOrder);
        vlogDebug("requestInventoryStore. Defined branchId: " + branchId);
        if (StringUtils.isNotBlank(customerId)) {
            return this.requestInventory(customerId, pProductId, branchId, pOrderedQty);
        } else {
            return this.requestInventory(getDefaultCustomerId(), pProductId, branchId, pOrderedQty);
        }
    }

    public List<InventoryAvailabilityInfo> requestAllInventory(String pCustomerId, String pProductId, long pOrderedQty) throws Exception {
        return inventoryConnectionManager.requestAllInventory(pCustomerId, pProductId, pOrderedQty);
    }

    public List<InventoryAvailabilityInfo> requestAllInventory(String pProductId, Profile profile, CPSOrderImpl pCurrent, long pOrderedQty) throws Exception {
        String customerId = getProfileTools().getCustomerId(profile, pCurrent);
        return this.requestAllInventory(customerId, pProductId, pOrderedQty);
    }

    public String getBranchForAvailability(Profile profile, CPSOrderImpl pOrder) { 
        return getBranchForAvailability(profile, pOrder, null);
    }
    
    public String getBranchForAvailability(Profile profile, CPSOrderImpl pOrder, String defaultCustomerBranch) { 
        // return selected store in case of pickup selected and return 100 in case shipping is selected
        String branchId = null;

        if (profile != null) {
            String cartSelectedDeliveryMethod = (String) profile.getPropertyValue(CART_SELECTED_DELIVERY_METHOD);
            if (DELEVERY_METHOD_SHIPPED.equals(cartSelectedDeliveryMethod) || null == cartSelectedDeliveryMethod) {
                return ( StringUtils.isEmpty(defaultCustomerBranch) ? getDefaultBranch() : defaultCustomerBranch);
            }

            if (DELEVERY_METHOD_PICK_UP.equals(cartSelectedDeliveryMethod)) {
                String cartSelectedStoreId = (String) profile.getPropertyValue(CART_SELECTED_STORE);

                RepositoryItem store = null;
                if (null != cartSelectedStoreId) {// If we select store on cart then get branchId from this store
                    store = getRepositoryItem(mLocationRepository, cartSelectedStoreId, STORE_ITEM_TYPE);
                    if (null != store) {
                        branchId = (String) store.getPropertyValue(STORE_BRANCH_ID_PRTY);
                    }
                } else if (null != pOrder && pOrder.getShippingGroupCount() > 0) { // get data from order
                    // if we DO NOT select store on cart level than get branch id from pickupShipGroup, if HGSH then get CS from order and get it BusinessUnitId
                    // (BranchId)
                    List<ShippingGroup> shippingGroups = pOrder.getShippingGroups();
                    for (ShippingGroup shippingGroup : shippingGroups) {
                        if (shippingGroup instanceof InStorePickupShippingGroup) {
                            String storeId = ((InStorePickupShippingGroup) shippingGroup).getLocationId();
                            store = getRepositoryItem(mLocationRepository, storeId, STORE_ITEM_TYPE);
                            if (null != store) {
                                branchId = (String) store.getPropertyValue(STORE_BRANCH_ID_PRTY);
                            }
                        } else if (shippingGroup instanceof HardgoodShippingGroup) {
                            branchId = getDefaultBranch();
                        }
                    }
                }
            }
        }
        return StringUtils.isNotBlank(branchId) ? branchId : getDefaultBranch();
    }

    private RepositoryItem getRepositoryItem(Repository pRepository, String pRepItemId, String pRepItemType) {
        RepositoryItem repItem = null;
        try {
            if (!StringUtils.isBlank(pRepItemId)) {
                repItem = pRepository.getItem(pRepItemId, pRepItemType);
            }
        } catch (RepositoryException re) {
            vlogError("getRepositoryItem() RepositoryException", re);
        }
        return repItem;
    }

    /**
     * @return the profileTools
     */
    public CPSProfileTools getProfileTools() {
        return profileTools;
    }

    /**
     * @param profileTools
     *            the profileTools to set
     */
    public void setProfileTools(CPSProfileTools profileTools) {
        this.profileTools = profileTools;
    }

    public boolean isPricingWebserviceEnable() {
        return getPricingClientMulti().isEnableWebservice();
    }

    public boolean isInventoryWebserviceEnable() {
        return getInventoryConnectionManager().getClient().getWebServiceConfig().getWebServiceEnabled();
    }

    public CPSInventoryConnectionManager getInventoryConnectionManager() {
        return inventoryConnectionManager;
    }

    public void setInventoryConnectionManager(CPSInventoryConnectionManager inventoryConnectionManager) {
        this.inventoryConnectionManager = inventoryConnectionManager;
    }

    public String getDefaultBranch() {
        return defaultBranch;
    }

    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public String getDefaultCustomerId() {
        return defaultCustomerId;
    }

    public void setDefaultCustomerId(String defaultCustomerId) {
        this.defaultCustomerId = defaultCustomerId;
    }

    /**
     * Gets LocationRepository.
     *
     * @return Value of LocationRepository.
     */
    public Repository getLocationRepository() {
        return mLocationRepository;
    }

    /**
     * Sets new LocationRepository.
     *
     * @param pLocationRepository
     *            New value of LocationRepository.
     */
    public void setLocationRepository(Repository pLocationRepository) {
        mLocationRepository = pLocationRepository;
    }

    /**
     * Sets new repository.
     *
     * @param pProfileRepository
     *            New value of repository.
     */
    public void setProfileRepository(Repository pProfileRepository) {
        mProfileRepository = pProfileRepository;
    }

    /**
     * Gets repository.
     *
     * @return Value of repository.
     */
    public Repository getProfileRepository() {
        return mProfileRepository;
    }

    public int getLeadTimeThreshold() {
        return mLeadTimeThreshold;
    }

    public void setLeadTimeThreshold(int pLeadTimeThreshold) {
        mLeadTimeThreshold = pLeadTimeThreshold;
    }

    public String getSessionBeanPath() {
        return mSessionBeanPath;
    }

    public void setSessionBeanPath(String pSessionBeanPath) {
        this.mSessionBeanPath = pSessionBeanPath;
    }

    public CPSAvailabilityManager getAvailabilityManager() {
        return mAvailabilityManager;
    }

    public void setAvailabilityManager(CPSAvailabilityManager pAvailabilityManager) {
        mAvailabilityManager = pAvailabilityManager;
    }

    /**
     * @return the mPricingClientMulti
     */
    public CPSPricingClientMultiStatic getPricingClientMulti() {
        return mPricingClientMulti;
    }

    /**
     * @param mPricingClientMulti
     *            the mPricingClientMulti to set
     */
    public void setPricingClientMulti(CPSPricingClientMultiStatic mPricingClientMulti) {
        this.mPricingClientMulti = mPricingClientMulti;
    }

}
