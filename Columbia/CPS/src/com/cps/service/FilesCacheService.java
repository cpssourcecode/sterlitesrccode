package com.cps.service;

import atg.nucleus.GenericService;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author Ihar Zharykau
 */
public class FilesCacheService extends GenericService {
	private boolean mEnabled;
	private String mCachePath;

	public InputStream getFileIfExistsByUrl(String pDownloadUrl) throws FileNotFoundException {
		if( isEnabled()) {
			String fileName = generateFileNameFromUrl(pDownloadUrl);
			if (getSavedFilesNames().contains(fileName)) {
				vlogDebug("Return file from cache for {0}", pDownloadUrl);
				return new FileInputStream(new File(mCachePath + File.separator + fileName));
			}
		}
		return null;
	}

	public void saveFile(InputStream pInputStream, String pUrl) throws IOException {
		if ( !isEnabled()){
			return;
		}
		FileOutputStream fileOutputStream = new FileOutputStream(mCachePath + File.separator  + generateFileNameFromUrl(pUrl));
		IOUtils.copy(pInputStream, fileOutputStream);
		fileOutputStream.flush();
		fileOutputStream.close();
	}

	public static String generateFileNameFromUrl(String pUrl){
		return pUrl.replaceAll("\\W+", "-") + "_" + getFilenameFromUrl(pUrl);
	}

	public static String getFilenameFromUrl(final String pUrl) {
		String result = pUrl;
		int index = result.lastIndexOf('/');
		if (index != -1) {
			result = pUrl.substring(index + 1);
		}
		return result;
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	public String getCachePath() {
		return mCachePath;
	}

	public void setCachePath(String pCachePath) {
		mCachePath = pCachePath;
	}

	public File getZipFilePath(){
		return new File(mCachePath);
	}

	public List<File> getSavedFiles(){
		File[] files = getZipFilePath().listFiles();
		return files != null ? Arrays.asList(files) : Collections.EMPTY_LIST;
	}

	public List<String> getSavedFilesNames(){
		List<String> result = new ArrayList<>();
		for ( File file : getSavedFiles()){
			result.add(file.getName());
		}
		return result;
	}
}
