package com.cps.common.handler;

import java.util.ArrayList;
import java.util.HashMap;

public interface DuplicatesMatchesHandlerInterface {
	
	public String getQtyList();

	public void setQtyList(String qtyList);
	
	public String getSkuIdsList();

	public void setSkuIdsList(String skuIdsList);
	
	public void setDuplicates(HashMap<String,HashMap<Integer,ArrayList<String>>> quickOrderDuplicates);
	
	public HashMap<String,HashMap<Integer,ArrayList<String>>> getDuplicates();
	
	public void setFinalSkus(ArrayList<String> quickOrderFinalSkus);
	
	public ArrayList<String> getFinalSkus();
	
	public void setFinalQuantities(ArrayList<Integer> quickOrderFinalQuantities);
	
	public ArrayList<Integer> getFinalQuantities();
	
	public void setAddFromErrorModal(String addFromErrorModal);
	
	public String getAddFromErrorModal();
	
}
