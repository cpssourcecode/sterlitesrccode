package com.cps.content.service;

import java.util.*;

import atg.nucleus.GenericService;
import atg.repository.MutableRepository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

import com.cps.content.IContentPagesConstants;
import com.cps.content.entity.InstructionalVideoPage;
import com.cps.content.entity.VideoPage;
import com.cps.content.entity.VideoRow;
import com.cps.email.CommonEmailSender;
import com.endeca.infront.shaded.org.apache.commons.lang.StringUtils;

/**
 * 
 * service class for content pages
 *
 */
public class ContentPagesService extends GenericService implements IContentPagesConstants{
	
	/**
	 * max number of videos on page
	 */
	private static final int ROWS_ON_PAGE = 3;
	/**
	 * max number of videos in row
	 */
	private static final int VIDEOS_IN_ROW = 3;

	private static final int INSTRUCTIONAL_VIDEOS_PER_PAGE = 9;
	
	/**
	 * email sender
	 */
	private CommonEmailSender mEmailSender;
	/**
	 * Testimonials repository
	 */
	private MutableRepository mTestimonialsRepository;
	/**
	 * Testimonial item descriptor
	 */
	private String mTestimonialDescriptor;
	/**
	 * ContentPages repository
	 */
	private MutableRepository mContentPagesRepository;
	/**
	 * yuotube preview template
	 */
	private String mYoutubePreviewTemplate;

	private String mPriceListUpdatesItemId;
	
	private String requestAccessLearnVideo;

	public String getPriceListUpdatesItemId() {
		return mPriceListUpdatesItemId;
	}

	public void setPriceListUpdatesItemId(String pPriceListUpdatesItemId) {
		mPriceListUpdatesItemId = pPriceListUpdatesItemId;
	}

	public boolean sendTestimonialEmail(Map<String,String> pValues, String userEmail){
		return getEmailSender().sendTestimonialEmail(pValues.get(FIRST_NAME_ID), pValues.get(LAST_NAME_ID),
			pValues.get(COMPANY_ID), pValues.get(MESSAGE_ID), userEmail);
	}

	public boolean sendPriceListUpdateEmail(String pName, String pEmail){
		return getEmailSender().sendPriceListUpdateConfirmationEmail(pName, pEmail);
	}

	/**
	 * get items by query
	 * @param pRepository - repository
	 * @param pDescriptor - item descriptor
	 * @param pQuery - query
	 * @return
	 */
	public RepositoryItem[] queryItems(String pQuery, String pDescriptor){
		RepositoryItem[] items = null;
		try {
			RepositoryView view = getContentPagesRepository().getView(pDescriptor);
			RqlStatement statement = RqlStatement.parseRqlStatement(pQuery);
			Object params[] = new Object[0];
			items = statement.executeQuery(view, params);
		} catch (RepositoryException e) {
			vlogDebug("Repository exception during quering items");
		}
		return items;
	}

	public RepositoryItem getRegionalManager(String regionalManagerRepositoryId) {
		RepositoryItem[] items = null;
		try {
			RepositoryView view = getContentPagesRepository().getView("regionalManager");
			RqlStatement statement = RqlStatement.parseRqlStatement("id = ?0");
			Object params[] = new Object[]{regionalManagerRepositoryId};
			items = statement.executeQuery(view, params);
		} catch (RepositoryException e) {
			vlogDebug("Repository exception during query");
		}

		if (items != null && items.length > 0) {
			return items[0];
		}
		else {
			return null;
		}
	}
	
	/**
	 * query video categories
	 * @return array of categories
	 */
	private RepositoryItem[] queryCategories(){
		RepositoryItem[] categories = null;
		try {
			RepositoryView view = getContentPagesRepository().getView("videoCategory");
			RqlStatement statement = RqlStatement.parseRqlStatement("ALL");
			Object params[] = new Object[0];
			categories =  statement.executeQuery(view, params);
		} catch (RepositoryException e) {
			vlogDebug("Repository exception during quering items");
		}
		
		return categories;
	}

	public List<RepositoryItem> getVideoLibraryAll(){
		List<RepositoryItem> result = Collections.emptyList();
		try{
			RepositoryView videoView = getContentPagesRepository().getView("video");
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement("ALL");
			result = Arrays.asList(rqlStatement.executeQuery(videoView, new Object[]{}));
		}catch (RepositoryException e){
			if(isLoggingError())
				logError(e);
		}

		return result;
	}
	
	/**
	 * configure map of page numbers and its elements
	 * @return map of page number/VideoPage
	 */
	public Map<String,VideoPage> getVideoLibraryPages(){
		RepositoryItem[] categories = queryCategories();
		Map<String,VideoPage> libraryMap = new HashMap<>();
		VideoPage page = new VideoPage();
		List<VideoRow> pageRows = new ArrayList<>();
		
		Integer pageNumber = 1;
		for (RepositoryItem category : categories){
			
			VideoRow row = new VideoRow();
			List<RepositoryItem> videoList = new ArrayList<RepositoryItem>();
			row.setCategory((String) category.getPropertyValue(PROPERTY_NAME));

			RepositoryItem[] videos = null;
			try {
				RepositoryView view = getContentPagesRepository().getView("video");
				RqlStatement statement = RqlStatement.parseRqlStatement("category = ?0");
				Object[] params = new Object[1];
				params[0] = category;

				videos = statement.executeQuery(view, params);
			}
			catch (RepositoryException e) {
				vlogError(e, "Error");
			}

			int videosFromCatetogyAdded = 0;
			if (videos != null) {
				for(RepositoryItem video : videos) {
					videoList.add(video);
					videosFromCatetogyAdded ++;
					if(videoList.size() >= VIDEOS_IN_ROW || videosFromCatetogyAdded == videos.length){
						row.setVideos(videoList);
						pageRows.add(row);
						row = new VideoRow();
						row.setCategory((String) category.getPropertyValue(PROPERTY_NAME));
						videoList = new ArrayList<RepositoryItem>();
					}
					if(pageRows.size() == ROWS_ON_PAGE){
						page.setRows(pageRows);
						libraryMap.put(pageNumber.toString(), page);
						pageNumber++;
						page = new VideoPage();
						pageRows = new ArrayList<VideoRow>();
						videoList = new ArrayList<RepositoryItem>();
					}
				}
			}
		}
		page.setRows(pageRows);
		if(!pageRows.isEmpty()){
			libraryMap.put(pageNumber.toString(), page);
		}
		return libraryMap;
	}

	public Map<String, InstructionalVideoPage> getInstructionalVideoPages(String roleId) {
		Map<String, InstructionalVideoPage> pages = new HashMap<String, InstructionalVideoPage>();

		// Get list of instructional videos that matches selected role
		RepositoryItem[] instructionalVideos = queryInstructionalVideos(roleId);

		if (instructionalVideos != null) {
			List<RepositoryItem> pageVideos = new ArrayList<RepositoryItem>();
			Long videoCount = 0l;
			Long pageNumber = 1l;
			for (RepositoryItem instructionalVideo : instructionalVideos) {

				vlogDebug("IV: " + instructionalVideo.getRepositoryId());

				// Handle creating a new page if we're filled up
				if (videoCount % INSTRUCTIONAL_VIDEOS_PER_PAGE == 0 && videoCount > 0) {
					vlogDebug("Max videos per page hit, adding new page...");
					InstructionalVideoPage newPage = new InstructionalVideoPage(pageVideos);
					pages.put(pageNumber.toString(), newPage);
					pageVideos = new ArrayList<RepositoryItem>();
					pageNumber++;
				}

				pageVideos.add(instructionalVideo);
				videoCount++;
			}

			// Add last page if necessary
			if (pageVideos.size() > 0) {
				InstructionalVideoPage newPage = new InstructionalVideoPage(pageVideos);
				pages.put(pageNumber.toString(), newPage);
			}

			vlogDebug("Role " + roleId + ", page count = " + pageNumber);
		}

		vlogDebug(pages.keySet().toString());

		return pages;
	}

	public RepositoryItem[] queryInstructionalVideos(String roleId) {
		RepositoryItem[] videos = {};
		boolean getAllVideos = true;

		if (roleId != null) {
			try {
				RepositoryView roleView = getContentPagesRepository().getView("instructional_video_role");
				RqlStatement roleStatement = RqlStatement.parseRqlStatement("id = ?0");
				Object[] roleParams = new Object[]{roleId};
				RepositoryItem[] requestedRole = roleStatement.executeQuery(roleView, roleParams);

				if (requestedRole != null && requestedRole.length > 0 && requestedRole[0].getPropertyValue("viewAll").equals(false)) {
					getAllVideos = false;
				}
			}
			catch (Exception e) {
				vlogError("Error querying instructional video role, role requested: " + roleId);
				vlogError(e, "Error");
			}
		}

		if (!getAllVideos) {
			vlogDebug("Get instructional videos for role");
			try {
				RepositoryView view = getContentPagesRepository().getView("instructional_video_item");
				RqlStatement statement = RqlStatement.parseRqlStatement("roles contains ?0 or roles includes item (viewAll=true) order by videoTitle asc");
				Object[] params = new Object[]{roleId};
				videos = statement.executeQuery(view, params);
			} catch (Exception e) {
				vlogError("Error querying instructional videos, role requested: " + roleId);
				vlogError(e, "Error");
			}
		}
		else {
			vlogDebug("Get all instructional videos");
			try {
				RepositoryView view = getContentPagesRepository().getView("instructional_video_item");
				RqlStatement statement = RqlStatement.parseRqlStatement("ALL order by videoTitle asc");
				Object[] params = new Object[]{};
				videos = statement.executeQuery(view, params);
			} catch (Exception e) {
				vlogError("Error querying instructional videos, all roles");
			}
		}

		return videos;
	}
	
	/**
	 * generate youtube preview picture from video url
	 * @param pVideoUrl - video url
	 * @return preview picture
	 */
	public String generatePreviewPicture(String pVideoUrl){
		
		String picture = null;
		
		if (!StringUtils.isEmpty(pVideoUrl)){
			char[] charArray = pVideoUrl.toCharArray();
			int startPosition = 0;
			int endPosition = pVideoUrl.length();
			boolean founded = false;
			char endChar = '?';
			if(pVideoUrl.indexOf("v=") != -1){
				startPosition = pVideoUrl.indexOf("v=") + 2;
				endChar = '&';
				founded = true;

			}else if(pVideoUrl.indexOf("youtu.be/") != -1){
				startPosition = pVideoUrl.indexOf("youtu.be/") + 9;
				founded = true;

			} else if(pVideoUrl.indexOf("v/") != -1){
				startPosition = pVideoUrl.indexOf("v/") + 2 ;
				founded = true;

			} else {
				vlogError("No video id found in url");
			}
			if(founded){
				for (int i = startPosition ; i < pVideoUrl.length() ; i++){
					if (charArray[i] == endChar){
						endPosition = i;
						break;
					}
				}
				picture = getYoutubePreviewTemplate().replace("<id>", pVideoUrl.substring(startPosition, endPosition));
			}
		} else {
			vlogError("Video url is not specified");
		}
		return picture;
	}
	
	
	public MutableRepository getTestimonialsRepository() {
		return mTestimonialsRepository;
	}

	public void setTestimonialsRepository(MutableRepository pTestimonialsRepository) {
		this.mTestimonialsRepository = pTestimonialsRepository;
	}

	public String getTestimonialDescriptor() {
		return mTestimonialDescriptor;
	}

	public void setTestimonialDescriptor(String pTestimonialDescriptor) {
		this.mTestimonialDescriptor = pTestimonialDescriptor;
	}
	
	public MutableRepository getContentPagesRepository() {
		return mContentPagesRepository;
	}

	public void setContentPagesRepository(MutableRepository pContentPagesRepository) {
		this.mContentPagesRepository = pContentPagesRepository;
	}
	
	public CommonEmailSender getEmailSender() {
		return mEmailSender;
	}

	public void setEmailSender(CommonEmailSender pEmailSender) {
		mEmailSender = pEmailSender;
	}
	
	public String getYoutubePreviewTemplate() {
		return mYoutubePreviewTemplate;
	}

	public void setYoutubePreviewTemplate(String pYoutubePreviewTemplate) {
		this.mYoutubePreviewTemplate = pYoutubePreviewTemplate;
	}

	public String getRequestAccessLearnVideo() {
		return requestAccessLearnVideo;
	}

	public void setRequestAccessLearnVideo(String pRequestAccessLearnVideo) {
		requestAccessLearnVideo = pRequestAccessLearnVideo;
	}
}
