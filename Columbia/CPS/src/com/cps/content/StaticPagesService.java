package com.cps.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.cps.endeca.assembler.content.DefaultFileStoreFactory;
import org.apache.commons.lang.StringUtils;

import atg.multisite.SiteContextManager;
import atg.nucleus.GenericService;

import com.cps.multisite.CPSSiteURLManager;
import com.cps.util.CPSConstants;
import com.endeca.infront.shaded.org.apache.commons.io.FileUtils;


/**
 * The Class StaticPagesService.
 */
public class StaticPagesService extends GenericService {

	/**
	 * The Static pages.
	 */
	private String[] mStaticPages;

	/**
	 * The Array of VSGEndecaPageInfo
	 */
	private VSGEndecaPageInfo[] mStaticPagesInfo;

	/**
	 * The Static pages prefix.
	 */
	private String mStaticPagesPrefix;

	/**
	 * The Static pages postfix.
	 */
	private String mStaticPagesPostfix;

	/**
	 * The Store factory.
	 */
	private DefaultFileStoreFactory mStoreFactory;

	/**
	 * Site URL Manager
	 */
	private CPSSiteURLManager siteURLManager;

	/**
	 * Temp folder path
	 */
	private String tempFolder;

	/**
	 * Site id
	 */
	private String siteId;


	private String beginSubstring;

	private String endSubstring;

	private String[] excludeDirectory;
	
	private List<String> excludeUrl; 

	private List<String> filePathList = new ArrayList<String>();

	/**
	 * Gets the resolved static pages.
	 *
	 * @return the resolved static pages
	 */
	public List<String> getResolvedStaticPages() {
		List<String> resolvedPages = new ArrayList<String>();

		String zipFilePath = "";
		try {
			zipFilePath = getStoreFactory().getConfigurationPath() + CPSConstants.SLASH + getStoreFactory().getCurrentRevName();
		} catch (IOException e) {
			vlogError(e, "Error");
		}
		vlogDebug("Zip File Path: " + zipFilePath);
		String tempOutputFolderPath = getTempFolder();
		vlogDebug("Temp Folder Path: " + tempOutputFolderPath);

		File unzippedArchive = unzip(zipFilePath, tempOutputFolderPath);
		File[] files = unzippedArchive.listFiles();
		for (File f : files) {
			if (f.isDirectory() && f.getName().equalsIgnoreCase("pages")) {
				File[] pagesFiles = f.listFiles();
				for (File pf : pagesFiles) {
					if (pf.isDirectory() && pf.getName().equalsIgnoreCase("100001")) {
						// Loop through this directory for listing of static pages, exclude search, browse, and home pages
						resolvedPages = getUrlsFromDirectory(pf);
					}
				}
			}
		}

		try {
			FileUtils.deleteDirectory(new File(getTempFolder()));
		} catch (IOException e) {
			vlogError("error while clearing temp dir");
		}

		return resolvedPages;
	}

	/**
	 * Gets the List of VSGEndecaPageIfno
	 *
	 * @return List<VSGEndecaPageInfo> - contains all required properties for sitemap building: (URL, changeFrequency,
	 * priority)
	 */
	public List<VSGEndecaPageInfo> getResolvedStaticPagesInfo() {

		final List<VSGEndecaPageInfo> resolvedPages = new ArrayList<VSGEndecaPageInfo>();

		final VSGEndecaPageInfo[] staticPages = getStaticPagesInfo();
		if (null != staticPages) {
			for (final VSGEndecaPageInfo staticPage : staticPages) {
				resolvedPages.add(staticPage);
			}
		}
		final DefaultFileStoreFactory storeFactory = getStoreFactory();
		if (null != storeFactory) {
			final List<VSGEndecaPageInfo> endecaPages = storeFactory.getStaticPagesEndecaInfo();
			if (null != endecaPages) {
				for (final VSGEndecaPageInfo endecaPage : endecaPages) {
					resolvedPages.add(endecaPage);
				}
			}
		}

		return resolvedPages;

	}

	/**
	 * Checks if is static seo.
	 *
	 * @param requestURI the request uri
	 * @return true, if is static seo
	 */
	public boolean isStaticSeo(String requestURI) {
		// boolean result = false;
		// String[] pages = getStaticPages();
		// if ( null != pages && null != requestURI) {
		// requestURI = requestURI.replaceAll("/", "");
		// for ( String page: pages ) {
		// if ( requestURI.equals(page) ) {
		// result = true;
		// break;
		// }
		// }
		// }
		// return result;
		return false;
	}

	public File unzip(String zipFile, String outputFolder) {
		byte[] buffer = new byte[1024];
		try {
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}

			try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));) {
				ZipEntry ze = zis.getNextEntry();
				while (ze != null) {
					String fileName = ze.getName();
					File newFile = new File(outputFolder + File.separator + fileName);
					new File(newFile.getParent()).mkdirs();
					try (FileOutputStream fos = new FileOutputStream(newFile);) {
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
						fos.close();
						ze = zis.getNextEntry();
					}
				}
				zis.closeEntry();
				zis.close();
			}
		} catch (IOException ex) {
			vlogError(ex, "Error");
		}

		return new File(outputFolder);
	}

	public List<String> getUrlsFromDirectory(File fileDirectory) {
		List<String> urls = new ArrayList<String>();

		if (fileDirectory != null && fileDirectory.isDirectory()) {
			boolean shouldRead = true;
			vlogDebug("Exclude Dirs: " + Arrays.toString(getExcludeDirectory()));
			for (String dir : getExcludeDirectory()) {
				if (dir != null && dir.equals(fileDirectory.getName())) {
					shouldRead = false;
					break;
				}
			}
			if (shouldRead) {
				urls = read(fileDirectory);
			}
			filePathList = new ArrayList<String>();
		}

		return formatUrls(urls);
	}

	public List<String> read(File file) {
		if (file.isFile()) {
			//logDebug("Adding absolute path - "+file.getAbsolutePath());
			filePathList.add(file.getAbsolutePath());
		} else if (file.isDirectory()) {
			File[] listOfFiles = file.listFiles();
			if (listOfFiles != null) {
				for (int i = 0; i < listOfFiles.length; i++) {
					for (String dir : getExcludeDirectory()) {
						if (dir != null && dir.equals(listOfFiles[i].getName())) {
						i=i+1;	
						}
					}
					read(listOfFiles[i]);
				}
			} else {
				logError("StaticPagesService.read - " + file + " [ACCESS DENIED]");
			}
		}
		return filePathList;
	}

	public List<String> formatUrls(List<String> urls) {
		List<String> formattedUrls = new ArrayList<String>();
		vlogDebug("FORMAT URLS");
		for (String url : urls) {
			if (url.endsWith(getEndSubstring())) {
				try {
					String siteId = SiteContextManager.getCurrentSiteId();
					if (siteId == null) {
						siteId = getSiteId();
					}
					String siteUrl = getSiteURLManager().getSiteUrl(siteId);
					// Need to grab what is inbetween 100001 and content.xml
					String pagePath = StringUtils.substringBetween(url, getBeginSubstring(), getEndSubstring());
					if(pagePath != null && !getExcludeUrl().contains(pagePath)){
						String fullURL = siteUrl+ CPSConstants.SLASH + pagePath;
						formattedUrls.add(fullURL);	
					}
				} catch (Exception e) {
					vlogError(e, "Error");
				}
			}
		}

		return formattedUrls;
	}

	public String[] getStaticPages() {
		return mStaticPages;
	}

	public void setStaticPages(String[] pStaticPages) {
		mStaticPages = pStaticPages;
	}

	public String getStaticPagesPrefix() {
		return mStaticPagesPrefix;
	}

	public void setStaticPagesPrefix(String pStaticPagesPrefix) {
		mStaticPagesPrefix = pStaticPagesPrefix;
	}

	public String getStaticPagesPostfix() {
		return mStaticPagesPostfix;
	}

	public void setStaticPagesPostfix(String pStaticPagesPostfix) {
		mStaticPagesPostfix = pStaticPagesPostfix;
	}

	public DefaultFileStoreFactory getStoreFactory() {
		return mStoreFactory;
	}

	public void setStoreFactory(DefaultFileStoreFactory pStoreFactory) {
		mStoreFactory = pStoreFactory;
	}

	public String getPageUrl(final String pPage) {
		return (null == pPage) ? null : getStaticPagesPrefix() + pPage.replaceAll("/", "") + getStaticPagesPostfix();
	}

	public VSGEndecaPageInfo[] getStaticPagesInfo() {
		return mStaticPagesInfo;
	}

	public void setStaticPagesInfo(VSGEndecaPageInfo[] pStaticPagesInfo) {
		mStaticPagesInfo = pStaticPagesInfo;
	}

	public String getTempFolder() {
		return tempFolder;
	}

	public void setTempFolder(String tempFolder) {
		this.tempFolder = tempFolder;
	}

	public CPSSiteURLManager getSiteURLManager() {
		return siteURLManager;
	}

	public void setSiteURLManager(CPSSiteURLManager siteURLManager) {
		this.siteURLManager = siteURLManager;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getBeginSubstring() {
		return beginSubstring;
	}

	public void setBeginSubstring(String beginSubstring) {
		this.beginSubstring = beginSubstring;
	}

	public String getEndSubstring() {
		return endSubstring;
	}

	public void setEndSubstring(String endSubstring) {
		this.endSubstring = endSubstring;
	}

	public String[] getExcludeDirectory() {
		return excludeDirectory;
	}

	public void setExcludeDirectory(String[] excludeDirectory) {
		this.excludeDirectory = excludeDirectory;
	}

	/**
	 * @return the excludeUrl
	 */
	public List<String> getExcludeUrl() {
		return excludeUrl;
	}

	/**
	 * @param excludeUrl the excludeUrl to set
	 */
	public void setExcludeUrl(List<String> excludeUrl) {
		this.excludeUrl = excludeUrl;
	}

}
