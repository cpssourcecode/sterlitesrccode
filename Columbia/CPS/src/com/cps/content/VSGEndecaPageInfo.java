package com.cps.content;

/**
 * 
 * The VSGEndecaPageInfo is responsible for VSGStaticSitemapGenerator
 * 
 * @author Pavel Kazlou
 */
public class VSGEndecaPageInfo {

	/**
	 * The changeFrequency (How frequently the page is likely to change.)
	 */
	private String mChangeFrequency;

	/**
	 * The priority (The priority of this URL relative to other URLs on your site. 
	 * Valid values range from 0.0 to 1.0)
	 */
	private String mPriority;

	/**
	 * The page URL
	 */
	private String mPageURL;

	public VSGEndecaPageInfo() {
	}

	public String getChangeFrequency() {
		return mChangeFrequency;
	}

	public void setChangeFrequency(String pChangeFrequency) {
		mChangeFrequency = pChangeFrequency;
	}

	public String getPriority() {
		return mPriority;
	}

	public void setPriority(String pPriority) {
		mPriority = pPriority;
	}

	public String getPageURL() {
		return mPageURL;
	}

	public void setPageURL(String pPageURL) {
		mPageURL = pPageURL;
	}
}
