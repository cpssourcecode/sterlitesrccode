package com.cps.content.handler;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import com.cps.content.service.ContentPagesService;
import com.cps.content.validator.ContentPagesValidator;
import vsg.util.ajax.AjaxUtils;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;
import java.io.IOException;


/**
 * Handler for submitting price list updates form
 **/
public class PriceListUpdatesFormHandler extends GenericFormHandler {

	private String mContactName;

	public String getContactName() {
		return mContactName;
	}

	public void setContactName(String pContactName) {
		mContactName = pContactName;
	}

	private String mEmail;

	public String getEmail() {
		return mEmail;
	}

	public void setEmail(String pEmail) {
		mEmail = pEmail;
	}

	private ContentPagesService mService;

	public ContentPagesService getService() {
		return mService;
	}

	public void setService(ContentPagesService pService) {
		mService = pService;
	}

	private ContentPagesValidator mValidator;

	public ContentPagesValidator getValidator() {
		return mValidator;
	}

	public void setValidator(ContentPagesValidator pValidator) {
		mValidator = pValidator;
	}

	private TransactionManager mTransactionManager;

	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		mTransactionManager = pTransactionManager;
	}

	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * saves default shipping address to user
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws javax.servlet.ServletException if error occurs
	 * @throws java.io.IOException      if error occurs
	 */
	public boolean handleSubscribe(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {

		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "PriceListUpdatesFormHandler.handleSubscribe";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				try {
					td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
					if (!StringUtils.isBlank(getEmail())) {
						setEmail(getEmail().trim());
					}
					if (getValidator().validatePriceListUpdatesForm(this, pRequest)) {
						getService().sendPriceListUpdateEmail(getContactName(), getEmail());
					}
					AjaxUtils.addAjaxResponse(this, pResponse, null);
				} catch (Exception e) {
					rollback = true;
					if (isLoggingError()) {
						logError(e);
					}
				} finally {
					td.end(rollback);
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}
			finally {
				if(rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return false;
	}

}
