package com.cps.content.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.userprofiling.Profile;
import vsg.util.ajax.AjaxUtils;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

import com.cps.content.service.ContentPagesService;
import com.cps.content.validator.ContentPagesValidator;
import com.cps.util.CPSConstants;

import static com.cps.email.CPSEmailConstants.EMAIL_FIELD;


/**
 * Handler for submitting testimonials form
 **/
public class TestimonialsFormHandler extends GenericFormHandler {
	/**
	 * values
	 */
	private Map<String, String> mValue = new HashMap<String, String>();
	/**
	 * content pages service
	 */
	private ContentPagesService mService;
	/**
	 * content pages validator
	 */
	private ContentPagesValidator mValidator;
	/**
	 * transaction manager
	 */
	private TransactionManager mTransactionManager;
	/**
	 * profile
	 */
	private Profile mProfile;

	/**
	 * Repeating Request Monitor
	 */
	private RepeatingRequestMonitor mRepeatingRequestMonitor;

	//------------------------------------------------------------------------------------------------------------------

	/**
	 * saves default shipping address to user
	 *
	 * @param pRequest  request
	 * @param pResponse response
	 * @return false
	 * @throws ServletException if error occurs
	 * @throws IOException      if error occurs
	 */
	public boolean handleSubmitTestimonial(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		final RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		final String handleMethodName = "TestimonialsFormHandler.handleSubmitTestimonial";

		if (rrm == null || rrm.isUniqueRequestEntry(handleMethodName)) {
			TransactionDemarcation td = new TransactionDemarcation();
			boolean rollback = false;
			try {
				try {
					td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);
					if (getValidator().validateTestimonialsForm(this, pRequest)) {
						String testimonialEmail = getValue().get(CPSConstants.TESTIMONIAL_EMAIL);
						String userEmail = null;
						if(userEmail == null && !testimonialEmail.isEmpty() ){
							userEmail = testimonialEmail;
						}
						getService().sendTestimonialEmail(this.getValue(), userEmail);
					}
					AjaxUtils.addAjaxResponse(this, pResponse, null);
				} catch (Exception e) {
					rollback = true;
					if (isLoggingError()) {
						logError(e);
					}
				} finally {
					td.end(rollback);
				}
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(e);
				}
			} finally {
				if (rrm != null){
					rrm.removeRequestEntry(handleMethodName);
				}
			}
		}
		return false;
	}

	//------------------------------------------------------------------------------------------------------------------

	public Map<String, String> getValue() {
		return mValue;
	}

	public void setValue(Map<String, String> pValue) {
		mValue = pValue;
	}

	public ContentPagesService getService() {
		return mService;
	}

	public void setService(ContentPagesService pService) {
		mService = pService;
	}

	public ContentPagesValidator getValidator() {
		return mValidator;
	}

	public void setValidator(ContentPagesValidator pValidator) {
		mValidator = pValidator;
	}
	
	public TransactionManager getTransactionManager() {
		return mTransactionManager;
	}

	public void setTransactionManager(TransactionManager pTransactionManager) {
		this.mTransactionManager = pTransactionManager;
	}

	public Profile getProfile() {
		return mProfile;
	}

	public void setProfile(Profile pProfile) {
		mProfile = pProfile;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return mRepeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor pRepeatingRequestMonitor) {
		mRepeatingRequestMonitor = pRepeatingRequestMonitor;
	}
}
