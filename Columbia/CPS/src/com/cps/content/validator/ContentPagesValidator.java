package com.cps.content.validator;

import atg.core.util.StringUtils;
import atg.servlet.DynamoHttpServletRequest;

import java.util.regex.Pattern;

import com.cps.content.IContentPagesConstants;
import com.cps.content.handler.PriceListUpdatesFormHandler;
import com.cps.content.handler.TestimonialsFormHandler;
import com.cps.userprofiling.validator.BaseValidator;
import com.cps.util.CPSConstants;
import com.cps.util.CPSErrorCodes;

/**
 * 
 * validator for content pages
 *
 */
public class ContentPagesValidator extends BaseValidator implements IContentPagesConstants {

	private boolean checkEmailForCorrect(String pEmail) {
		return pEmail.matches(CPSConstants.REG_EXP_EMAIL);
	}

	/** Validates contact us page form
	 * 
	 * @author Bruce Bui
	 * @param pFormHandler contactUsFormHandler
	 * @param pRequest     request
	 * @return true if all form entries are valid
	 */
	public boolean validateTestimonialsForm(TestimonialsFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {
		boolean valid = true;
		String firstName = (String) pFormHandler.getValue().get(FIRST_NAME_ID);
		String lastName = (String) pFormHandler.getValue().get(LAST_NAME_ID);
		String email = (String) pFormHandler.getValue().get(EMAIL_ID);
		String company = (String) pFormHandler.getValue().get(COMPANY_ID);
		String message = (String) pFormHandler.getValue().get(MESSAGE_ID);
				
		if(StringUtils.isBlank(firstName) || firstName.length() > 20) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_TESTIMONIALS_FIRST_NAME, FIRST_NAME_ID, pRequest);
		}
		if(StringUtils.isBlank(lastName) || lastName.length() > 20) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_TESTIMONIALS_LAST_NAME, LAST_NAME_ID, pRequest);
		}
		if(StringUtils.isBlank(company) || company.length() > 30) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_TESTIMONIALS_COMPANY, COMPANY_ID, pRequest);
		}
		if (StringUtils.isNotBlank(email) && !email.matches(CPSConstants.REG_EXP_EMAIL)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_TESTIMONIALS_EMAIL, EMAIL_ID, pRequest);
	    }
		if(StringUtils.isBlank(message) || message.length() > 250) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_TESTIMONIALS_MESSAGE, MESSAGE_ID, pRequest);
		}
		
		
		return valid;
	}

	/** Validates price list updates form
	 *
	 * @param pFormHandler PriceListUpdatesFormHandler
	 * @param pRequest     request
	 * @return true if all form entries are valid
	 */
	public boolean validatePriceListUpdatesForm(PriceListUpdatesFormHandler pFormHandler, DynamoHttpServletRequest pRequest) {

		boolean valid = true;
		String name = pFormHandler.getContactName();
		String email = pFormHandler.getEmail();

		if (StringUtils.isBlank(name)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_PL_CONTACT_NAME, CONTACT_NAME, pRequest);
		}
		if (StringUtils.isBlank(email)) {
			valid = false;
			createError(pFormHandler, CPSErrorCodes.ERR_PL_EMAIL, EMAIL, pRequest);
		} else {
			valid = checkEmailForCorrect(email);
			if(!valid) {
				createError(pFormHandler, CPSErrorCodes.ERR_PL_INVALID_EMAIL, EMAIL, pRequest);
			}
		}
		return valid;

	}

}
