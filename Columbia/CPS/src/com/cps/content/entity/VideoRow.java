package com.cps.content.entity;

import java.util.List;

import atg.repository.RepositoryItem;

/**
 * 
 * Video page row
 *
 */
public class VideoRow {
	
	/**
	 * row category
	 */
	private String category;
	/**
	 * row videos
	 */
	private List<RepositoryItem> videos;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<RepositoryItem> getVideos() {
		return videos;
	}

	public void setVideos(List<RepositoryItem> videos) {
		this.videos = videos;
	}
	
}
