package com.cps.content.entity;

import java.util.List;

import atg.repository.RepositoryItem;

public class InstructionalVideoPage {
	List<RepositoryItem> videos;

	public InstructionalVideoPage(List<RepositoryItem> videos) {
		this.videos = videos;
	}

	public List<RepositoryItem> getVideos() {
		return this.videos;
	}

	public void setVideos(List<RepositoryItem> videos) {
		this.videos = videos;
	}
}