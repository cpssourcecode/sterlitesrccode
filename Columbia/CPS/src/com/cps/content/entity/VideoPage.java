package com.cps.content.entity;

import java.util.List;

/**
 * 
 * Video library page bean
 *
 */
public class VideoPage {
	
	/**
	 * Video page rows
	 */
	private List<VideoRow> rows;

	public List<VideoRow> getRows() {
		return rows;
	}

	public void setRows(List<VideoRow> rows) {
		this.rows = rows;
	}
	
}
