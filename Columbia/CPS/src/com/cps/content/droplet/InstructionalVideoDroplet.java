package com.cps.content.droplet;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.content.entity.InstructionalVideoPage;
import com.cps.content.service.ContentPagesService;

public class InstructionalVideoDroplet extends DynamoServlet {

	private ContentPagesService service;

	private static final String PARAM_ROLE = "role";

	private static final String OPARAM_OUTPUT = "output";
	private static final String OPARAM_EMPTY = "empty";

	private static final String PAGES = "pages";

	@Override
	public void service(DynamoHttpServletRequest request, DynamoHttpServletResponse response) throws ServletException, IOException {
		String roleParam = request.getParameter(PARAM_ROLE);

		Map<String, InstructionalVideoPage> pages = getService().getInstructionalVideoPages(roleParam);

		if (pages == null || pages.isEmpty()) {
			request.serviceLocalParameter(OPARAM_EMPTY, request, response);
		}
		else {
			request.setParameter(PAGES, pages);
			request.serviceParameter(OPARAM_OUTPUT, request, response);
		}
	}

	public ContentPagesService getService() {
		return this.service;
	}

	public void setService(ContentPagesService service) {
		this.service = service;
	}
}