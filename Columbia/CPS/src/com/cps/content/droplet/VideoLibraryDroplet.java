package com.cps.content.droplet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.content.entity.VideoPage;
import com.cps.content.service.ContentPagesService;

/**
 * droplet to retrieve video library data
 *
 */
public class VideoLibraryDroplet extends DynamoServlet {
	
	/** service */
	private ContentPagesService mService;
	/** The Constant OUTPUT_OPARAM. */
	private static final String OUTPUT_OPARAM = "output";
	/** The Constant EMPTY_OPARAM. */
	private static final String EMPTY_OPARAM = "empty";
	/** pages oparam */
	private final static String PARAM_VIDEOS = "videos";
	
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		List<RepositoryItem> videos = getService().getVideoLibraryAll();
		
		if (videos == null || videos.isEmpty()) {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		} else {
			pRequest.setParameter(PARAM_VIDEOS, videos);
			pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
	}
	
	public ContentPagesService getService() {
		return mService;
	}

	public void setService(ContentPagesService pService) {
		this.mService = pService;
	}
	
}
