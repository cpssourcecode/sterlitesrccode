package com.cps.content.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.content.service.ContentPagesService;

/**
 * 
 * droplet used to query items from custom repository by specified query
 *
 */
public class QueryItemsDroplet extends DynamoServlet {

	/** repositoryItems oparam */
	private final static String REPOSITORY_ITEMS = "repositoryItems";
	/** The Constant OUTPUT_OPARAM. */
	private static final String OUTPUT_OPARAM = "output";
	/** The Constant EMPTY_OPARAM. */
	private static final String EMPTY_OPARAM = "empty";
	/** service */
	private ContentPagesService mService;
	/** query */
	private String mQuery;
	/** descriptor */
	private String mDescriptor;
	
	/**
	 * Droplet service method.
	 *
	 * @param pRequest
	 *            - request
	 * @param pResponse
	 *            - response
	 * @throws javax.servlet.ServletException
	 *             - if cannot make output or empty oparam
	 * @throws java.io.IOException
	 *             - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {

		RepositoryItem[] items = getService().queryItems(getQuery(), getDescriptor());
		
		if (items == null || items.length == 0) {
			pRequest.serviceLocalParameter(EMPTY_OPARAM, pRequest, pResponse);
		} else {
			pRequest.setParameter(REPOSITORY_ITEMS, items);
			pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
		}
	}
	
	public ContentPagesService getService() {
		return mService;
	}

	public void setService(ContentPagesService pService) {
		this.mService = pService;
	}
	
	public String getQuery() {
		return mQuery;
	}

	public void setQuery(String pQuery) {
		this.mQuery = pQuery;
	}

	public String getDescriptor() {
		return mDescriptor;
	}

	public void setDescriptor(String pDescriptor) {
		this.mDescriptor = pDescriptor;
	}
	
}
