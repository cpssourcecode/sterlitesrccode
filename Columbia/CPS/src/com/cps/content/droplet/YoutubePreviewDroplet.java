package com.cps.content.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.cps.content.service.ContentPagesService;

/**
 * 
 * droplet used to generate preview picture for youtube video
 *
 */
public class YoutubePreviewDroplet extends DynamoServlet {

	/** repositoryItems oparam */
	private final static String PREVIEW = "preview";
	/** repositoryItems oparam */
	private final static String INPUT_VIDEO_URL = "videoUrl";
	/** The Constant OUTPUT_OPARAM. */
	private static final String OUTPUT_OPARAM = "output";
	/** service */
	private ContentPagesService mService;
	
	/**
	 * Droplet service method.
	 *
	 * @param pRequest
	 *            - request
	 * @param pResponse
	 *            - response
	 * @throws javax.servlet.ServletException
	 *             - if cannot make output or empty oparam
	 * @throws java.io.IOException
	 *             - if cannot make output or empty oparam
	 */
	@Override
	public void service(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		
		String videoUrl = pRequest.getParameter(INPUT_VIDEO_URL);
		String picture = getService().generatePreviewPicture(videoUrl);
		pRequest.setParameter(PREVIEW, picture);
		pRequest.serviceParameter(OUTPUT_OPARAM, pRequest, pResponse);
		
	}
	
	public ContentPagesService getService() {
		return mService;
	}

	public void setService(ContentPagesService pService) {
		this.mService = pService;
	}
	
}
