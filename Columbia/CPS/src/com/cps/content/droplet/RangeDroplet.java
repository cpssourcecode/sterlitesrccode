package com.cps.content.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * 
 * droplet used to query a range of items from repository
 *
 */
public class RangeDroplet extends DynamoServlet {

	/** input start parameter (index number of first element we need to show)*/	
	private static final String INPUT_START = "start";
	/** input how many parameter (how many elements we need to show)*/
	private static final String INPUT_HOW_MANY = "howMany";
	/** oparam empty */		
	private static final String EMPTY = "empty";
	/** oparam output start */		
	private static final String OUTPUT_START = "outputStart";
	/** oparam output end */	
	private static final String OUTPUT_END = "outputEnd";
	/** output oparam */	
	private static final String OUTPUT = "output";
	/** output element */	
	private static final String ELEMENT = "element";
	/** size of all array of elements */	
	protected static final String OUTPUT_SIZE = "size";
	/** query range of items */	
	private static final String QUERY = "ALL RANGE ?0+?1";
	/** query all items */		
	private static final String COUNT_QUERY = "ALL";

	/** current query */	
	private String mQuery = QUERY;
	/** current statement */	
	private RqlStatement mStatement;
	/** repository for query */		
	private String mRepository;
	/** item descriptor */		
	private String mDescriptor;

	public String getQuery() {
		return mQuery;
	}

	/**
	 * setting query
	 * @param pQuery - query, we need to use as current
	 */
	public void setQuery(String pQuery) {
		try {
			mStatement = RqlStatement.parseRqlStatement(pQuery);
			mQuery = pQuery;
		} catch (RepositoryException e) {
			if (isLoggingError()) {
				vlogError(e ,"RepositoryException while setting query");
			}
		}
	}

	/**
	 * getting rql statement	
	 * @return rql statement
	 */
	public RqlStatement getStatement() {
		if (mStatement == null) {
			setQuery(QUERY);
		}
		return mStatement;
	}

	public String getRepository() {
		return mRepository;
	}

	public void setRepository(String pRepository) {
		mRepository = pRepository;
	}

	public String getDescriptor() {
		return mDescriptor;
	}

	public void setDescriptor(String pDescriptor) {
		mDescriptor = pDescriptor;
	}
	
	/**
	 * 
	 * @param pRequest - request
	 * @param pResponse - response
	 * @param outputItems - array of repository items
	 * @throws ServletException - if cannot make output, output start, output end or empty oparam
	 * @throws IOException - if cannot make output or empty oparam
	 */
	protected static void makeOutputParam(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse,
			RepositoryItem[] outputItems) throws ServletException, IOException {
		if (outputItems == null || outputItems.length == 0) {
			pRequest.serviceLocalParameter(EMPTY, pRequest, pResponse);
		} else {
			pRequest.serviceLocalParameter(OUTPUT_START, pRequest, pResponse);
			for (int i = 0; i < outputItems.length; ++i) {
				pRequest.setParameter(ELEMENT, outputItems[i]);
				pRequest.serviceLocalParameter(OUTPUT, pRequest, pResponse);
			}
			pRequest.serviceLocalParameter(OUTPUT_END, pRequest, pResponse);
		}
	}
	
		
	/**
	 * droplet service method
	 * @param pRequest - request
	 * @param pResponse - response
	 * @throws ServletException - if cannot make output, output start, output end or empty oparam
	 * @throws IOException - if cannot make output or empty oparam
	 */
	public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		int start = Integer.parseInt(pRequest.getParameter(INPUT_START));
		int howMany = Integer.parseInt(pRequest.getParameter(INPUT_HOW_MANY));
		Repository repository = (Repository)pRequest.resolveName(getRepository());
		
		try {
			rangeQuery(repository, start, howMany, pRequest, pResponse);
		} catch (RepositoryException e) {
			vlogError(e, "RepositoryException during quering renge of items");
		}
	}
	
	/**
	 * Make range query and render output.
	 *
	 * @param pRepository the repository
	 * @param start the start
	 * @param howMany the how many
	 * @param pRequest the request
	 * @param pResponse the response
	 * @throws RepositoryException the repository exception
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void rangeQuery(Repository pRepository, int start, int howMany, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RepositoryException, ServletException, IOException {
		
		RepositoryView view = pRepository.getView(getDescriptor());
		RqlStatement rs = RqlStatement.parseRqlStatement(COUNT_QUERY);	
		int count = rs.executeCountQuery(view, new Object[0]);
		pRequest.setParameter(OUTPUT_SIZE, count);
		RqlStatement statement = getStatement();
		Object params[] = new Object[2];
		if (start <= count) {
			if ((start + howMany) > count) {
				params[0] = start;
				params[1] = count - start + 1;
			} else {
				params[0] = start;
				params[1] = howMany;
			}
			RepositoryItem[] outputItems = statement.executeQuery(view, params);
			makeOutputParam(pRequest, pResponse, outputItems);
		} else {
			if (isLoggingError())
				logError("start is greater than number of items");
		}
	}
}
