package com.cps.content;

/**
 * 
 * constants for content pages
 *
 */
public interface IContentPagesConstants {
	
	/**
	 * testimonialFirstName id 
	 */
	String FIRST_NAME_ID = "testimonialFirstName";
	/**
	 * testimonialLastName id
	 */
	String LAST_NAME_ID = "testimonialLastName";
	/**
	 * testimonialCompany id 
	 */
	String COMPANY_ID = "testimonialCompany";
	/**
	 * testimonialEmail id 
	 */
	String EMAIL_ID = "testimonialEmail";
	/**
	 * testimonialMessage id
	 */
	String MESSAGE_ID = "testimonialMessage";
	/**
	 * property firstName
	 */
	String PROPERTY_FIRST_NAME = "firstName";
	/**
	 * property lastName
	 */
	String PROPERTY_LAST_NAME = "lastName";
	/**
	 * property company
	 */
	String PROPERTY_COMPANY = "company";
	/**
	 * property message
	 */
	String PROPERTY_MESSAGE = "message";
	/**
	 * property timestamp
	 */
	String PROPERTY_TIMESTAMP = "timestamp";
	/**
	 * property name
	 */
	String PROPERTY_NAME = "name";
	
	/**
	 * property videos
	 */
	String PROPERTY_VIDEOS = "videos";

	String CONTACT_NAME = "contactName";

	String EMAIL = "email";

	
}
