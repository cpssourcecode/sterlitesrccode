/**
 *
 */
package cps.commerce.pricing.priceLists;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cps.commerce.catalog.CPSCatalogTools;
import com.cps.commerce.order.CPSOrderImpl;
import com.cps.integrations.pricing.CPSPricingConstants;
import com.cps.service.CPSExternalProductService;

import atg.commerce.pricing.priceLists.PriceListException;
import atg.commerce.pricing.priceLists.PriceListManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/**
 * @author R.Srinivasan This class extends the OOTB PriceListManager and contains logic to fetch the prices for users when it is not needed to make web service
 *         calls
 *
 */
public class CPSPriceListManager extends PriceListManager {

    private CPSCatalogTools catalogTools;
    private double defaultPrice;
    private CPSExternalProductService externalProductService;

    /**
     * @return the externalProductService
     */
    public CPSExternalProductService getExternalProductService() {
        return externalProductService;
    }

    /**
     * @param externalProductService
     *            the externalProductService to set
     */
    public void setExternalProductService(CPSExternalProductService externalProductService) {
        this.externalProductService = externalProductService;
    }

    /**
     * @return the defaultPrice
     */
    public double getDefaultPrice() {
        return defaultPrice;
    }

    /**
     * @param defaultPrice
     *            the defaultPrice to set
     */
    public void setDefaultPrice(double defaultPrice) {
        this.defaultPrice = defaultPrice;
    }

    /**
     *
     * @return
     */
    public CPSCatalogTools getCatalogTools() {
        return catalogTools;
    }

    /**
     *
     * @param catalogTools
     */
    public void setCatalogTools(CPSCatalogTools catalogTools) {
        this.catalogTools = catalogTools;
    }

    /**
     * Helper method to get the current priceList based on the site or if not available get the default priceList
     *
     * @return
     */

    private RepositoryItem getCurrentPriceList() {
        RepositoryItem currentPriceList = null;
        try {
            currentPriceList = getPriceListForSite(CPSPricingConstants.DEFAULT_LISTPRICE_LIST);
        } catch (RepositoryException re) {
            vlogError(re, "Error occurred while fetching PriceLists from the Site }");
            logError(re);
        }
        if (currentPriceList == null) {
            // try to get the current price list from the PriceListManager's defaultListPriceList
            currentPriceList = getDefaultPriceList();
        }
        return currentPriceList;
    }

    /**
     * This method gets the prices from the ATG PriceLists and populates the Map
     *
     * @param products
     * @param profileId
     *
     * @return
     */

    public Map<String, Double> getPricesFromATGPriceList(Set<String> products, RepositoryItem profile, String priceMatrix, CPSOrderImpl order) {

        RepositoryItem currentPriceList = getCurrentPriceList();
        Map<String, Double> pricesMap = new HashMap<>();
        try {
            RepositoryItem productItem = null;
            for (String product : products) {
                productItem = getCatalogTools().findProduct(product);
                if (productItem != null) {
                    getSkuListPrice(pricesMap, productItem, product, currentPriceList, profile, priceMatrix, order);
                }
            }
        } catch (RepositoryException e) {
            vlogError("Repository Exception : {0}", e.getMessage());
        }
        return pricesMap;
    }

    /**
     * This method gets the price repository Item from the PriceList repository and then based on the profile property, will get the correct price value from
     * the matrix
     *
     * @param currentPriceList
     * @param productId
     * @param skuId
     * @param profile
     * @return
     */

    public Double getOnePrice(RepositoryItem currentPriceList, String productId, String skuId, RepositoryItem profile, String priceMatrix, CPSOrderImpl order) {
        // Double itemPrice = getDefaultPrice();
        Double itemPrice = null;
        try {
            RepositoryItem priceItem = getSkuPrice(currentPriceList, productId, skuId);
            if (priceItem != null) {
                if (profile == null || profile.isTransient()) {
                    itemPrice = (Double) priceItem.getPropertyValue(CPSPricingConstants.ANONYMOUS_USER_PRICE_MATRIX);
                } else {
                    itemPrice = (Double) priceItem.getPropertyValue(priceMatrix);
                    if (itemPrice == null) {
                        itemPrice = (Double) priceItem.getPropertyValue(CPSPricingConstants.ANONYMOUS_USER_PRICE_MATRIX);
                    }
                }
            }

        } catch (PriceListException ple) {
            vlogError(ple, "error fetching price for the SKU - {0}", skuId);
            logError(ple);
        }

        if (itemPrice == null) {
            // this means there is no price for that SKU in our ATG PriceLists. so make a JDE call in here.
            vlogInfo("there is no price for the SKU - {0} for the priceMatrix - {1}.Hence making a JDE call", skuId, priceMatrix);
            Set<String> productIdsForWhichPricesToBeFetched = new HashSet<>();
            productIdsForWhichPricesToBeFetched.add(skuId);
            Map<String, Double> pricesFromJDEMap = getExternalProductService().getCustomerItemPrice(productIdsForWhichPricesToBeFetched, profile, order);
            if (pricesFromJDEMap != null) {
                itemPrice = pricesFromJDEMap.get(skuId);
            }
        }

        return itemPrice != null ? itemPrice : 0.0;

    }

    /**
     * fetch the current PriceList and pass it on to the other method for price calculation
     *
     * @param productId
     * @param skuId
     * @param profile
     * @return
     */
    public Double getOnePrice(String productId, String skuId, RepositoryItem profile, String priceMatrix, CPSOrderImpl order) {
        return getOnePrice(getCurrentPriceList(), productId, skuId, profile, priceMatrix, order);

    }

    /**
     * This method iterates through the list of child skus for the given product and will fetch the price of that
     *
     * @param pricesMap
     * @param productItem
     * @param product
     * @param currentPriceList
     * @param profile
     */

    private void getSkuListPrice(Map<String, Double> pricesMap, RepositoryItem productItem, String product, RepositoryItem currentPriceList,
                    RepositoryItem profile, String priceMatrix, CPSOrderImpl order) {
        Double skuListPrice = 0.0;
        List<RepositoryItem> childSKUs = (List<RepositoryItem>) productItem.getPropertyValue("childSKUs");
        if (childSKUs != null && childSKUs.size() > 0) {
            for (RepositoryItem skuItem : childSKUs) {
                skuListPrice = getOnePrice(currentPriceList, productItem.getRepositoryId(), skuItem.getRepositoryId(), profile, priceMatrix, order);
                pricesMap.put(product, skuListPrice);
            }
        }
    }

}
