<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>

    <dsp:importbean bean="/cps/userprofiling/ContactUsFormHandler"/>
    <dsp:importbean bean="/cps/droplet/LocationByBranchLookupDroplet"/>

    <dsp:getvalueof var="transient" bean="Profile.transient"/>
    <dsp:getvalueof var="isTransient" bean="/atg/userprofiling/Profile.transient"/>
    <dsp:getvalueof var="securityStatus" bean="Profile.securityStatus"/>
    <dsp:getvalueof var="transient" value="${transient or securityStatus eq 1}"/>
    <dsp:getvalueof var="isProfileTransient" value="${transient}"/>

    <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
    <c:if test="${not empty selectedOrg}">
        <dsp:getvalueof var="csrClass" value="admin-access"/>
    </c:if>

    <cp:pageContainer page="account">

        <main id="body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ol class="breadcrumb">
                            <li class="active"><fmt:message key="account.navigation.contactUs"/></li>
                        </ol>
                    </div>
                    <dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
                        <dsp:param name="email" value="false"/>
                        <dsp:param name="print" value="false"/>
                    </dsp:include>
                </div>

                <h1 class="mb20"><fmt:message key="account.title.contactUs"/></h1>
				<div class="mb20" id="info">
                                        <dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
                                        <dsp:param name="messageKey" value="contactInfoMsg"/>
                                        <dsp:oparam name="output">
                                         <dsp:getvalueof var="message" param="message"/>
                                        </dsp:oparam>
                                        <dsp:oparam name="empty">
                                        <dsp:getvalueof var="message" param="message"/>
                                        </dsp:oparam>
                                        </dsp:droplet>
                                        <dsp:valueof param="message" valueishtml="true" />
                                        ${message}
                 </div>
			<div class="row">
                    <div class="col-sm-8">
                        <dsp:include page="${originatingRequest.contextPath}/global/gadgets/error-success-msg-divs.jsp">
                            <dsp:param name="addColClass" value="false"/>
                        </dsp:include>
                        <dsp:form action="${originatingRequest.contextPath}/account/contact-us.jsp"
                                  method="post" id="contact-us-form" formid="contact-us-form">
                            <div class="form-group">
                                <label class="control-label"><fmt:message key="account.fields.name"/>*</label>
                                <input id="cu_input_fname" name="cu_input_fname" type="text" class="form-control" maxlength="100"
                                       value="" data-event-keydown-id="global4" 
                                       data-event-keyup-id="global10" >
                            </div>
                            <div class="form-group">
                                <label class="control-label"><fmt:message key="account.fields.lastName"/>*</label>
                                <input id="cu_input_lname" name="cu_input_lname" text="text" class="form-control" maxlength="100"
                                       value="" data-event-keydown-id="global5" 
                                       data-event-keyup-id="global11" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><fmt:message key="account.fields.emailAddress"/>*</label>
                                <input id="cu_input_email" name="cu_input_email" type="email" class="form-control" maxlength="100"
                                       value="" data-event-keydown-id="global6" 
                                       data-event-keyup-id="global12" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><fmt:message key="account.fields.companyName"/>*</label>
                                <input id="cu_input_company" name="cu_input_company" type="text" class="form-control" value="" maxlength="100" data-event-keydown-id="global7"  data-event-keyup-id="global13" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><fmt:message key="account.fields.accountNumber"/></label>
                                <input id="cu_input_accountNumber" name="cu_input_accountNumber" type="text"
                                       class="form-control" value="" maxlength="100"
                                       data-event-keydown-id="global8" 
                                       data-event-keyup-id="global14" />
                            </div>
                            <div class="form-group">
                                <label class="control-label"><fmt:message key="account.fields.phone"/>*</label>
                                <input id="cu_input_phone" name="cu_input_phone" type="text" class="form-control" maxlength="15"
                                       value="" data-event-keydown-id="global9" 
                                       data-event-keyup-id="global15" />
                            </div>
                            <div class="form-group">
                                <select id="cu_input_subject" class="form-control">
                                   	   <option selected="" disabled="">Please Choose a Subject *</option>
                                       <option value="Sales Support"><fmt:message
                                               key="account.fields.support.salesSupport"/></option>
                                       <option value="Product Support"><fmt:message
                                               key="account.fields.support.productSupport"/></option>
                                       <option value="Website Support"><fmt:message
                                               key="account.fields.support.websiteSupport"/></option>
                                       <option value="Order Inquiries"><fmt:message
                                               key="account.fields.support.orderInquires"/></option>
                                       <option value="Billing Question"><fmt:message
                                               key="account.fields.support.billingQuestion"/></option>
                                       <option value="Material Test Report (MTR) Inquiries"><fmt:message
                                               key="account.fields.support.mtrInquiries"/></option>
                                       <option value="Other Inquiries"><fmt:message
                                               key="account.fields.support.other"/></option>
                                 </select>
                            </div>
                            <div class="form-group">
                                <input id="cu_input_method" type="hidden" value="email"/>
                                <label class="control-label"><fmt:message key="account.title.preferredMethodOfContact"/>*</label>
                                <div>
                                    <label class="radio-custom radio-inline active" data-initialize="radio">
                                        <input checked="checked" name="cu_input_method" type="radio" value="email" data-event-click-id="global0" >
                                        <fmt:message key="account.fields.method.email"/>
                                    </label>
                                    <label class="radio-custom radio-inline" data-initialize="radio">
                                        <input name="cu_input_method" type="radio" value="phone" data-event-click-id="global1" >
                                        <fmt:message key="account.fields.method.phone"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><fmt:message key="account.fields.message"/>*</label>
                                <textarea id="cu_input_comments" class="form-control" cols="5" rows="7" maxlength="500"></textarea>
                            </div>
					<c:choose>
						<c:when test="${isProfileTransient}">
						<div id="contact-us-form-captcha">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<div id="recaptchaContactUS"></div>
									</div>
								</div>
							</div>
						</div>
                            <p>
                                <a href="#" id="contactUsSubmit"class="btn btn-primary xs-block" data-event-click-id="global2" ><fmt:message key="account.button.submit"/></a>
                                <a href="#" class="btn btn-info xs-block" data-event-click-id="global3" ><fmt:message key="account.button.cancel"/></a>
                            </p>
						</c:when>
						<c:otherwise>
                            <p>
                                <a href="#" class="btn btn-primary xs-block" data-event-click-id="global2" ><fmt:message key="account.button.submit"/></a>
                                <a href="#" class="btn btn-info xs-block" data-event-click-id="global3" ><fmt:message key="account.button.cancel"/></a>
                            </p>
						</c:otherwise>
					</c:choose>			

                            <dsp:input id="cu_lname" type="hidden" bean="ContactUsFormHandler.value.firstName" value=""/>
                            <dsp:input id="cu_fname" type="hidden" bean="ContactUsFormHandler.value.lastName" value=""/>
                            <dsp:input id="cu_email" type="hidden" bean="ContactUsFormHandler.value.email" value=""/>
                            <dsp:input id="cu_company" type="hidden" bean="ContactUsFormHandler.value.company" value=""/>
                            <dsp:input id="cu_account" type="hidden" bean="ContactUsFormHandler.value.accountNumber" value=""/>
                            <dsp:input id="cu_phone" type="hidden" bean="ContactUsFormHandler.value.phone" value=""/>
                            <dsp:input id="cu_subject" type="hidden" bean="ContactUsFormHandler.value.subject" value=""/>
                            <dsp:input id="cu_method" type="hidden" bean="ContactUsFormHandler.value.method" value=""/>
                            <dsp:input id="cu_comments" type="hidden" bean="ContactUsFormHandler.value.comments" value=""/>

                            <dsp:input type="hidden" bean="ContactUsFormHandler.sendMessage" value="true" priority="-10"/>
                        </dsp:form>
                    </div>

                    <div class="col-sm-4">
                        <dsp:droplet name="IsNull">
                            <dsp:param name="value" bean="Profile.parentOrganization"/>
                            <dsp:oparam name="true">
                                <dsp:getvalueof var="businessUnit"
                                                bean="Profile.derivedBillingAddress.businessUnit"/>
                            </dsp:oparam>
                            <dsp:oparam name="false">
                                <dsp:getvalueof var="businessUnit"
                                                bean="Profile.parentOrganization.billingAddress.businessUnit"/>
                            </dsp:oparam>
                        </dsp:droplet>

                        <dsp:droplet name="LocationByBranchLookupDroplet">
                            <dsp:param name="branchId" value="${businessUnit}"/>
                            <dsp:oparam name="output">
                                <dsp:tomap var="location" param="store"/>
                                <c:if test="${!empty location}">
                                    <div class="panel panel-default">
                                        <div class="panel-heading"><fmt:message
                                                key="account.title.yourSalesTeam"/></div>
                                        <div class="panel-body">
                                            <address>
                                                    ${location.address1}&nbsp;${location.address2}<br/>
                                                    ${location.city},&nbsp;${location.stateAddress}&nbsp;${location.postalCode}<br/>
                                                <c:if test="${!empty location.phoneNumber}">
                                                    <strong><fmt:message key="account.title.phone"/></strong>
                                                    <dsp:valueof value="${location.phoneNumber}"/> <br>
                                                </c:if>
                                                <c:if test="${!empty location.faxNumber}">
                                                    <strong><fmt:message key="account.title.fax"/></strong>
                                                    <dsp:valueof value="${location.faxNumber}"/>
                                                </c:if>
                                            </address>
                                            <c:if test="${not empty location.hours or not empty location.willCallHours}">
                                                <h6>Normal Business Hours</h6>
                                                <p>
                                                    <c:if test="${not empty location.hours}">
                                                        Sales Office:&nbsp;<dsp:valueof value="${vsg_utils:addTimeMeridiem(location.hours)}"
                                                                                        valueishtml="true"/><br/>
                                                    </c:if>
                                                    <c:if test="${not empty location.willCallHours}">
                                                        Will Call:&nbsp;<dsp:valueof
                                                            value="${vsg_utils:addTimeMeridiem(location.willCallHours)}"
                                                            valueishtml="true"/><br/>
                                                    </c:if>
                                                </p>
                                                <p><strong>24/7 Emergency Service Available</strong></p>
                                            </c:if>
                                        </div>
                                    </div>
                                </c:if>
                            </dsp:oparam>
                        </dsp:droplet>

                        <div class="panel panel-default">
                            <div class="panel-heading"><fmt:message key="account.title.corpOffice"/></div>
                            <div class="panel-body">
                                <address>
                                    <dsp:droplet name="/atg/targeting/RepositoryLookup">
                                        <dsp:param name="id" value="corporateOfficeAddress"/>
                                        <dsp:param name="repository"
                                                   bean="/cps/registry/repository/MessagesRepository"/>
                                        <dsp:param name="itemDescriptor" value="userMessage"/>
                                        <dsp:oparam name="output">
                                            <dsp:valueof param="element.message" valueishtml="true"/>
                                        </dsp:oparam>
                                        <%--start hard code--%>
                                        <dsp:oparam name="empty">
                                                1120 West Pershing Road <br>
                                                Chicago, IL 60609 <br>
                                                <strong>Phone:</strong> 1-800-368-2709 <br>
                                                <strong>Fax:</strong> 773-927-8415
                                        </dsp:oparam>
                                        <%--end hard code--%>
                                    </dsp:droplet>
                                </address>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading"><fmt:message key="account.title.remittance"/></div>
                            <div class="panel-body">
                                <h6>Remit Payment To:</h6>
                                <address>
                                    <dsp:droplet name="/atg/targeting/RepositoryLookup">
                                        <dsp:param name="id" value="remitPaymentAddress"/>
                                        <dsp:param name="repository"
                                                   bean="/cps/registry/repository/MessagesRepository"/>
                                        <dsp:param name="itemDescriptor" value="userMessage"/>
                                        <dsp:oparam name="output">
                                            <dsp:valueof param="element.message" valueishtml="true"/>
                                        </dsp:oparam>
                                        <%--start hard code--%>
                                        <dsp:oparam name="empty">
                                            23671 Network Place <br>
                                            Chicago, IL 60673-1236
                                        </dsp:oparam>
                                        <%--end hard code--%>
                                    </dsp:droplet>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </main>

        <dsp:getvalueof var="transient" bean="Profile.transient"/>
        <c:choose>
            <c:when test="${isProfileTransient}">
                <script nonce="${requestScope.nonce}">
                    $(document).ready(
                            function () {
                                var cu_input_accountNumber = $("#cu_input_accountNumber");
                               	 //cu_input_accountNumber.attr("disabled", "");
                                $("#cu_input_phone").mask("(999) 999-9999");
                                checkContactUsSubmitButton();
                            }
                    );
                    var contact_us_captcha_success_default = false;
                    var contact_us_captcha_default = null;

                    function checkContactUsSubmitButton() {
                        var enableLoginButton = true;
                        if ($('#contact-us-form-captcha').length > 0) {
                            enableLoginButton = contact_us_captcha_success_default;
                        }

                        if (enableLoginButton) {
                            $('#contactUsSubmit').removeClass('disabled');
                        } else {
                            $('#contactUsSubmit').addClass('disabled');
                        }
                    }

                    function renderCaptchaCallback() {
                        var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
                        contact_us_captcha_default = window.grecaptcha.render(
                            'recaptchaContactUS', {
                                'sitekey' : sitekey,
                                'callback' : validateRecaptchaDefaultContactUS,
                                'expired-callback' : expireRecaptchaDefaultContactUS
                        });
                    }

                    function validateRecaptchaDefaultContactUS(captchaResponse) {
                        contact_us_captcha_success_default = true;
                        checkContactUsSubmitButton();
                    }

                    function expireRecaptchaDefaultContactUS() {
                        grecaptcha.reset(contact_us_captcha_default);
                        contact_us_captcha_success_default = false;
                        checkContactUsSubmitButton();
                    }

                </script>
            </c:when>
            <c:otherwise>
                <dsp:getvalueof var="firstName" bean="Profile.firstName"/>
                <dsp:getvalueof var="lastName" bean="Profile.lastName"/>
                <dsp:getvalueof var="fullName" value="${firstName} ${lastName}"/>

                <dsp:getvalueof var="email" bean="Profile.email"/>
                <dsp:getvalueof var="company" bean="Profile.parentOrganization.name"/>
                <dsp:getvalueof var="accountNumber" bean="Profile.selectedCS.id"/>
                <dsp:getvalueof var="phone" bean="Profile.homeAddress.phoneNumber"/>

                <script nonce="${requestScope.nonce}">
                    $(document).ready(
                            function () {
                                 $("#cu_input_phone").mask("(999) 999-9999");
                                var disabled = "disabled";
                                var cu_input_fname = $("#cu_input_fname");
                                var cu_input_lname = $("#cu_input_lname");
                                var cu_input_email = $("#cu_input_email");
                                var cu_input_company = $("#cu_input_company");
                                var cu_input_accountNumber = $("#cu_input_accountNumber");
                                var cu_input_phone = $("#cu_input_phone");

                                var cu_name = $("#cu_name");
                                var cu_email = $("#cu_email");
                                var cu_company = $("#cu_company");
                                var cu_accountNumber = $("#cu_accountNumber");
                                var cu_phone = $("cu_phone");

                                cu_input_fname.val("${firstName}");
                                cu_name.val("${fullName}");
                                cu_input_fname.attr("disabled", "");

                                cu_input_lname.val("${lastName}");
                                cu_input_lname.attr("disabled", "");

                                cu_input_email.val("${email}");
                                cu_email.val("${email}");
                                cu_input_email.attr("disabled", "");

                                cu_input_company.val("${company}");
                                cu_company.val("${company}");
                                cu_input_company.attr("disabled", "");

                                cu_input_accountNumber.val("${accountNumber}");
                                cu_accountNumber.val("${accountNumber}");
                                //cu_input_accountNumber.attr("disabled", "");

                                cu_input_phone.val("${phone}");
                                cu_phone.val("${phone}");
                                //cu_input_phone.attr("disabled", "");

                           }
                    );
                </script>
            </c:otherwise>
        </c:choose>


        <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-contact-us.jsp"/>

    </cp:pageContainer>

</dsp:page>