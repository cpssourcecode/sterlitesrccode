<%--
<dsp:page>
	<cp:pageContainer page="404">
		<jsp:body>
			<div id="body">
				<div class="center">
					<h1>404 Page not Found</h1>
					<p>Woops! This is not the page you were looking for. Use one of the links below to get back on track:</p>
					<p><a href="/index.jsp">Go to the Homepage.</a><br/><br/>
					<a href="/">Log into an account.</a><br/><br/>
					<a href="/global/contactus.jsp">Contact Us</a></p>
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</jsp:body>
	</cp:pageContainer>
</dsp:page>
--%>

<dsp:page>
	<cp:pageContainer title="404" page="404">
		<jsp:body>
			<c:set var="contentCollection" value="/content/Web/404" />
			<dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
				<dsp:param name="contentCollection" value="${contentCollection}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
					<c:forEach var="renderContent" items="${contentItem.contents}">
							<dsp:renderContentItem contentItem="${renderContent}"/>
					</c:forEach>
				</dsp:oparam>
			</dsp:droplet>
		</jsp:body>
	</cp:pageContainer>
</dsp:page>