<dsp:page>
	<dsp:setvalue param="sitePage" value="${requestScope['cpsSitePage']}"/>
	<cp:pageContainer>
		<jsp:body>
			<dsp:include src="/global/content/gadgets/breadcrumb.jsp"/>
			
			<div id="body" class="container">
				<div class="row">
					<dsp:getvalueof var="leftNavigation" param="sitePage.leftNavigation"/>
					<c:if test="${leftNavigation eq 'true'}">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 catalog-filter">
							<dsp:include src="/global/content/gadgets/left-navigation-sidebar.jsp"/>
						</div>
					</c:if>
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 page-content">
					<%--<c:choose>--%>
						<%--<c:when test="${leftNavigation eq 'true'}">--%>
							<%--<div id="content">--%>
						<%--</c:when>--%>
						<%--<c:otherwise>--%>
							<%--<div id="content" class="content-without-leftnav">--%>
						<%--</c:otherwise>--%>
					<%--</c:choose>--%>
						<dsp:getvalueof var="title" param="sitePage.pageTitle"/>
						<c:choose>
							<c:when test="${not empty title}">
								<h2><dsp:valueof value="${title}" valueishtml="true"/></h2>
							</c:when>
							<c:otherwise>
								<h2><dsp:valueof param="sitePage.displayName" valueishtml="true"/></h2>
							</c:otherwise>
						</c:choose>

						<div class="row">
							<div class="col-lg-12 block-bordered">
							</div>
							<div class="col-lg-12 block-bordered">
								<article>
									<dsp:getvalueof var="content" param="sitePage.html"/>
									<c:out value="${content}" escapeXml="false"/>
								</article>
							</div>
						</div>

					</div>
				</div>
			</div>
		</jsp:body>
	</cp:pageContainer>
</dsp:page>