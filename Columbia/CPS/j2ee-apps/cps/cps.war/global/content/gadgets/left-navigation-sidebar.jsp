<dsp:page>
	<dsp:getvalueof var="leftNav" param="sitePage.leftNavigation" />
	<c:if test="${leftNav eq 'true'}">
		<dsp:getvalueof var="title" param="sitePage.pageTitle" />
		<dsp:getvalueof var="url" param="sitePage.pageURL" />
		<c:choose>
			<c:when test="${not empty title}">
				<h4>
					<a href="${url}"><dsp:valueof value="${title}" valueishtml="true" /> </a>
				</h4>
			</c:when>
			<c:otherwise>
				<h4>
					<a href="${url}"><dsp:valueof param="sitePage.displayName" valueishtml="true" /> </a>
				</h4>
			</c:otherwise>
		</c:choose>
		<dsp:getvalueof var="childPages" param="sitePage.childPages" />
		<c:if test="${empty childPages}">
			<dsp:droplet name="/atg/dynamo/droplet/Range">
				<dsp:param name="array" param="sitePage.parentPages" />
				<dsp:param name="elementName" value="parentPage" />
				<dsp:param name="howMany" value="1" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="childPages" param="parentPage.childPages" />
				</dsp:oparam>
			</dsp:droplet>
		</c:if>
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" value="${childPages}" />
			<dsp:param name="elementName" value="childPage" />
			<dsp:param name="sortProperties" value="displayName" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="cTitle" param="childPage.pageTitle" />
				<dsp:getvalueof var="cUrl" param="childPage.pageURL" />
				<div class="panel panel-default panel-link">
					<div class="panel-heading">
						<h4 class="panel-title">
							<%--<a class="open" href="">Subnav Item</a>--%>
							<c:choose>
								<c:when test="${not empty cTitle}">
									<a href="${cUrl}"><dsp:valueof value="${cTitle}" valueishtml="true" /></a>
								</c:when>
								<c:otherwise>
									<a href="${cUrl}"><dsp:valueof param="childPage.displayName" valueishtml="true" /></a>
								</c:otherwise>
							</c:choose>
						</h4>
					</div>
				</div>
			</dsp:oparam>
			<dsp:oparam name="outputStart">
				<div class="panel-group">
			</dsp:oparam>
			<dsp:oparam name="outputEnd">
				</div>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
</dsp:page>