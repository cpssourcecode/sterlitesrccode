<dsp:page>

	<dsp:droplet name="/atg/dynamo/droplet/Range">
		<dsp:param name="array" param="page.parentPages" />
		<dsp:param name="elementName" value="parentPage" />
		<dsp:param name="howMany" value="1" />
		<dsp:oparam name="output">
			<dsp:include src="/global/content/gadgets/breadcrumb-loop.jsp">
				<dsp:param name="page" param="parentPage" />
			</dsp:include>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="title" param="page.pageTitle" />
	<dsp:getvalueof var="url" param="page.pageURL" />
	<dsp:getvalueof var="active" param="active" />

	<c:choose>
		<c:when test="${active}">
			<dsp:getvalueof var="activeClass" value="active" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="activeClass" value="" />
		</c:otherwise>
	</c:choose>

	<li class="${activeClass}">
		<c:choose>
			<c:when test="${not empty title}">
				<dsp:valueof value="${title}" valueishtml="true" />
			</c:when>
			<c:otherwise>
				<dsp:valueof param="page.displayName" valueishtml="true" />
			</c:otherwise>
		</c:choose>
	</li>

</dsp:page>