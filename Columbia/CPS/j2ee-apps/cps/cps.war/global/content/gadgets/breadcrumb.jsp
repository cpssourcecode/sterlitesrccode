<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<div class="breadcrumbs-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<ol class="breadcrumb">
						<li>
							<a href="${originatingRequest.contextPath}/index.jsp">
								<span>Home</span>
							</a>
						</li>
						<dsp:include src="/global/content/gadgets/breadcrumb-loop.jsp">
							<dsp:param name="page" param="sitePage" />
							<dsp:param name="active" value="${true}" />
						</dsp:include>

					</ol>
				</div>
			</div>
		</div>
	</div>

	<%--<div id="breadcrumb">--%>
	<%--<div class="center">--%>
	<%--<p>--%>
	<%--<a href="/">Home</a>--%>
	<%--</p>--%>
	<%--<dsp:include page="/global/content/actions.jsp" />--%>
	<%--</div>--%>
	<%--</div>--%>

</dsp:page>