<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/cps/userprofiling/RequestAccessFormHandler" />
	<dsp:importbean bean="/cps/content/service/ContentPagesService" />
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:getvalueof var="anonymousRequest" bean="CPSGlobalProperties.enableAnonymousUserToRequestAccess"/>
	<dsp:getvalueof var="video" bean="ContentPagesService.requestAccessLearnVideo" />
	<c:if test="${!anonymousRequest}">
	<dsp:droplet name="/atg/dynamo/droplet/Switch">
		<dsp:param bean="Profile.transient" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/" />
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	</c:if>
	<cp:pageContainer page="requestAccess">
		<main id="body">
			<div class="container">
				<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-success-request-access.jsp" />
			
				<h1 class="mb20">Request Access</h1>
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<div class="request-access-info">
							<h4 class="text-nocase green-text">
								Manage Users, Company Spend, and Order Frequency
							</h4>
							<cp:repositoryMessage key="requestAccessAdminDisclaimerMsg" />
						</div>
					</div>
					
					<div class="col-xs-12 col-md-6">
						<div class="request-access-info">
							<h4 class="text-nocase green-text">
								Access Invoices and Statements
							</h4>
							<cp:repositoryMessage key="requestAccessFinanceDisclaimerMsg" />
						</div>
					</div>
				</div>
			
			<section>
			<dsp:form method="post" action="/global/request-access.jsp" id="request-access-form" formid="request-access-form">	
				<div id="error-messages-request-access"></div>
				
				<dsp:getvalueof var="firstName" bean="Profile.firstName" />
				<dsp:getvalueof var="lastName" bean="Profile.lastName" />
				<dsp:getvalueof var="phoneNumber" bean="Profile.homeAddress.phoneNumber" />
				<dsp:getvalueof var="email" bean="Profile.email" />
				<dsp:getvalueof var="profileId" bean="Profile.id" />
				
				<div class="row">
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">First Name *</label>
						<dsp:input bean="RequestAccessFormHandler.firstName" 
							iclass="form-control" id="requestAccessForm_firstName" maxlength="50" value="${firstName}" />
					</div>
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">Last Name *</label>
						<dsp:input bean="RequestAccessFormHandler.lastName" 
							iclass="form-control" id="requestAccessForm_lastName" maxlength="50" value="${lastName}"/>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">Phone Number *</label>
						<dsp:input bean="RequestAccessFormHandler.phoneNumber"  
							iclass="form-control" id="requestAccessForm_phoneNumber" maxlength="50" value="${phoneNumber}"/>
					</div>
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">Email Address *</label>
						<dsp:input bean="RequestAccessFormHandler.email"  
							iclass="form-control" id="requestAccessForm_email" maxlength="50" value="${email}"/>
					</div>
				</div>
				<br>
				<h4 class="text-nocase" id="requested-permissions">
					Requested User Permission *:
				</h4>
				<!--  SM-520 New User Role -->
				<div class="checkbox" id="myCheckbox">
					<label>
						<dsp:input bean="RequestAccessFormHandler.role.buyer" type="checkbox" name="requestAccessForm_role_buyer" />
						<span class="checkbox-label">Buyer Role</span>
					</label>
				</div>
				<div class="checkbox" id="myCheckbox">
					<label>
						<dsp:input bean="RequestAccessFormHandler.role.finance" type="checkbox" name="requestAccessForm_role_finance" />
						<span class="checkbox-label">Finance Role: Access Invoices and Statements</span>
					</label>
				</div>
				<div class="checkbox" id="myCheckbox">
					<label>
						<dsp:input bean="RequestAccessFormHandler.role.admin" type="checkbox" name="requestAccessForm_role_admin" />
						<span class="checkbox-label">Admin Role: Manage Users, Company Spend and Order Frequency</span>
					</label>
				</div>
				<br>
				<h4 class="text-nocase">
					Approver Contact *:
				</h4>
				<p>
					All requests must be vetted through your organization. Please provide the contact information for your head of finance or purchasing so that we can validate your permissions.  
				</p>
				
				<div class="row">
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">Contact First Name *</label>
						<dsp:input bean="RequestAccessFormHandler.approverFirstName"  
							iclass="form-control" id="requestAccessForm_approverFirstName" maxlength="50" />
					</div>
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">Contact Last Name *</label>
						<dsp:input bean="RequestAccessFormHandler.approverLastName"  
							iclass="form-control" id="requestAccessForm_approverLastName" maxlength="50" />
					</div>
				</div>
				
				<div class="row">
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">Contact Phone Number *</label>
						<dsp:input bean="RequestAccessFormHandler.approverPhoneNumber"  
							iclass="form-control" id="requestAccessForm_approverPhoneNumber" maxlength="50" />
					</div>
					<div class="form-group col-sm-6 col-xs-12">
						<label class="control-label">Contact Email Address *</label>
						<dsp:input bean="RequestAccessFormHandler.approverEmail"  
							iclass="form-control" id="requestAccessForm_approverEmail" maxlength="50" />
					</div>
				</div>
				
				<dsp:input type="hidden" bean="RequestAccessFormHandler.profileId" value="${profileId}" priority="-10"/>
				<dsp:input type="hidden" bean="RequestAccessFormHandler.requestAccess" value="true" priority="-10"/>
			</dsp:form>
			    <c:choose>
                    <c:when test="${isTransient}">
                    <div id="request-access-form-captcha">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<div id="recaptchaRequestAccess"></div>
									</div>
								</div>
							</div>
						</div>
                   <p><button class="btn btn-lg btn-primary xs-block" id="requestAccessButton" data-event-click-id="eventRequestAccess" >Submit Request</button></p>
                    </c:when>
                    <c:otherwise>
                  <p><button class="btn btn-lg btn-primary xs-block" data-event-click-id="eventRequestAccess" >Submit Request</button></p>
                    </c:otherwise>
                    </c:choose>
			</section>
			</div>
		</main>
		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(function() {
				$("#requestAccessForm_approverPhoneNumber").mask("(999) 999-9999");
				$("#requestAccessForm_phoneNumber").mask("(999) 999-9999");
				checkRequestAccessSubmitButton();
			});
			var request_access_captcha_success_default = false;
			var request_access_captcha_default = null;

			function checkRequestAccessSubmitButton() {
			    var enableLoginButton = true;
			    if ($('#request-access-form-captcha').length > 0) {
			        enableLoginButton = request_access_captcha_success_default;
			    }

			    if (enableLoginButton) {
			        $('#requestAccessButton').removeAttr('disabled');
			    } else {
			        $('#requestAccessButton').attr('disabled', 'disabled');
			    }
			}

			function renderCaptchaCallback() {
			    var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
			    request_access_captcha_default = grecaptcha.render(
			        'recaptchaRequestAccess', {
			            'sitekey' : sitekey,
			            'callback' : validateRecaptchaDefaultRequestAccess,
			            'expired-callback' : expireRecaptchaDefaultRequestAccess
			    });
			}

			function validateRecaptchaDefaultRequestAccess(captchaResponse) {
			    request_access_captcha_success_default = true;
			    checkRequestAccessSubmitButton();
			}

			function expireRecaptchaDefaultRequestAccess() {
			    grecaptcha.reset(request_access_captcha_default);
			    request_access_captcha_success_default = false;
			    checkRequestAccessSubmitButton();
			}

		</script>
	</cp:pageContainer>
	
</dsp:page>