<div class="modal fade" id="modal-add-to-cart-error" tabindex="-1" role="dialog" aria-labelledby="modal-error-title">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
			</div>
			<div class="modal-body">
				<h4 class="modal-title" id="modal-error-title">Sorry! Something went wrong.</h4>

				<p id="add-item-error-message"></p>

			</div>
		</div>
	</div>
</div>