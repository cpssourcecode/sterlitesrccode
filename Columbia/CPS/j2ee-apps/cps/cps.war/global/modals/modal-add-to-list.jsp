<dsp:page>

	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler" />

	<div class="modal-header">
		<div class="header-symbol">
			<svg class="icon icon-paragraph"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-paragraph"></use></svg>
		</div>
		<button type="button" class="close" data-dismiss="modal">
			<span class="close-symbol" aria-hidden="true">&times;</span>
			<span class="sr-only">Close</span>
		</button>
		<h3 class="modal-title" id="myModalLabel">Add to List</h3>
	</div>
	<div class="modal-body">
		<div class="form-horizontal" role="form">
			<div class="form-group">
				<div class="col-sm-12">
					<p class="lead">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="prof-listModal-info"/>
						</dsp:include>
					</p>
					<div class="alert alert-danger" style="display:none;" id="error-messages"></div>
				</div>
			</div>

			<dsp:getvalueof var="items" param="items"/>
			<dsp:getvalueof var="citems" param="citems"/>
			<dsp:getvalueof var="indexs" param="indexs"/>
			<dsp:getvalueof var="quantities" param="quantities"/>

			<dsp:form action="/global/modals/modal-list-add.jsp?modal=true" method="post" id="list-add">
				<ul class="list-unstyled">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.giftlists"/>
						<dsp:param name="elementName" value="giftlist"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="userType" bean="Profile.userType"/>
							<dsp:getvalueof var="listOrg" param="giftlist.org"/>
							<dsp:getvalueof var="id" param="giftlist.id"/>
							<dsp:getvalueof var="date" param="giftlist.creationDate"/>
							<c:choose>
								<c:when test="${userType == 3}">
									<dsp:getvalueof var="userOrg" bean="Profile.selectedOrg.id"/>
									<c:if test="${empty userOrg}">
										<dsp:getvalueof var="userOrg" bean="Profile.parentOrganization.id"/>
									</c:if>
								</c:when>
								<c:otherwise>
									<dsp:getvalueof var="userOrg" bean="Profile.parentOrganization.id"/>
								</c:otherwise>
							</c:choose>

							<c:if test="${listOrg == userOrg}">
								<li>
									<div class="checkbox" id="myCheckbox">
										<label class="checkbox-custom checkbox-large highlight" data-initialize="checkbox">
											<input class="sr-only" type="checkbox" name="check" id="${id}">
											<span class="checkbox-label"><dsp:valueof param="giftlist.eventName"/></span>
										</label>
									</div>
									<div class="list-modal-text">
										<div class="row">
											<div class="col-sm-8">
												<p><dsp:valueof param="giftlist.description"/></p>
											</div>
											<div class="col-sm-4">
												<small class="text-muted pull-right">
													Created <fmt:formatDate pattern="MM/dd/yyyy" value="${date}"/>
												</small>
											</div>
										</div>
									</div>
								</li>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>

					<li>
						<div class="checkbox" id="myCheckbox">
							<label class="checkbox-custom checkbox-large highlight" data-initialize="checkbox">
								<input class="sr-only" type="checkbox" id="list-new" name="check" data-toggle="#checkboxToggleList">
								<span class="checkbox-label">New List</span>
							</label>
						</div>
					</li>
				</ul>

				<div class="list-modal-text hide" id="checkboxToggleList">
					<div class="well well-sm">
						<div class="form-group">
							<label for="eventName" class="col-sm-3 control-label">List Name</label>

							<div class="col-sm-9">
								<dsp:input type="text" iclass="form-control" bean="GiftlistFormHandler.eventName" id="eventName"/>
							</div>
						</div>

						<dsp:droplet name="/cps/droplet/CSSelectionDroplet">
							<dsp:oparam name="output">
								<div class="form-group">
									<label for="cs" class="col-sm-3 control-label">
										CS
									</label>

									<div class="col-sm-9">
										<dsp:select id="cs" bean="GiftlistFormHandler.cs" iclass="selectpicker">
											<dsp:option disabled="true" selected="true">Select...</dsp:option>
											<dsp:droplet name="/atg/dynamo/droplet/ForEach">
												<dsp:param name="array" param="associatedCS"/>
												<dsp:param name="elementName" value="cs"/>
												<dsp:oparam name="output">
													<dsp:getvalueof var="address1" param="cs.address1"/>
													<dsp:getvalueof var="csId" param="cs.id"/>
													<dsp:option value="${csId}">${address1}</dsp:option>
												</dsp:oparam>
											</dsp:droplet>
										</dsp:select>
										<small class="text-muted">Select a CS for this list to insure accurate pricing.</small>
									</div>

								</div>
							</dsp:oparam>
						</dsp:droplet>

						<div class="form-group">
							<label for="description" class="col-sm-3 control-label">List Description</label>

							<div class="col-sm-9">
								<dsp:textarea iclass="form-control" bean="GiftlistFormHandler.description" id="description" cols="30" rows="4"></dsp:textarea>
							</div>
						</div>
					</div>
				</div>

				<dsp:input type="hidden" bean="GiftlistFormHandler.selectedLists" value="" id="selectedLists" name="selectedLists"/>
				<dsp:input type="hidden" bean="GiftlistFormHandler.addedCommerceItems" value="${citems}" id="addedCommerceItems" name="addedCommerceItems"/>
				<dsp:input type="hidden" bean="GiftlistFormHandler.itemIdsList" value="${items}" name="itemIdsList"/>
				<dsp:input type="hidden" bean="GiftlistFormHandler.quantities" value="${quantities}" name="quantities"/>
				<dsp:input type="hidden" bean="GiftlistFormHandler.indexList" value="${indexs}" name="indexList"/>
				<dsp:input type="hidden" bean="GiftlistFormHandler.addItemsToLists" value="true" priority="-10"/>
			</dsp:form>

			<hr>
			<div class="form-group">
				<div class="col-sm-12">
					<input type="button" data-event-click-id="global180"  class="btn btn-success btn-lg pull-right" value="Save Item" id="submitSaveItem"/>
					<%--<button type="submit" class="btn btn-success btn-lg pull-right">Save Item</button>--%>
				</div>
			</div>

		</div>
	</div>


</dsp:page>