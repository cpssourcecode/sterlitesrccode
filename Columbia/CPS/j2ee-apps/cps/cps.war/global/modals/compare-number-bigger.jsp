<dsp:page>
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList"/>
	<dsp:getvalueof var="items" bean="ProductList.items"/>
	<div class="modal fade" id="biggerCompareItemNumber" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-narrow" >
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-text-title"></h4>

					<p>Please select not more than three items to compare</p>

					<button class="btn btn-info" data-dismiss="modal">Okay</button>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
