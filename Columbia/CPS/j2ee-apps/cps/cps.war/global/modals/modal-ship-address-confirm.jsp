<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<dsp:getvalueof var="isReset" param="isReset"/>

	<a href="#" data-toggle="modal" data-target="#addressConfirmChange" style="display: none;" id="link-addressConfirmChange"></a>

	<!-- confirm Modal on View Address Page -->
	<div class="modal fade" id="addressConfirmChange" tabindex="-1" role="dialog" aria-labelledby="addressConfirmChange" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-ship-change-title">Change Shipping Address</h4>

					<p><cp:repositoryMessage key="shippingAddressChangeModal"></cp:repositoryMessage></p>
					<button class="btn btn-primary" id="shipAddressSubmit" ><fmt:message key="account.address.label.confirm"/></button>
					<button class="btn btn-info" id="shipAddressCancel" data-dismiss="modal" data-event-click-id="global229" data-event-click-param0="${vsg_utils:escapeJS(isReset)}" >Cancel</button>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
