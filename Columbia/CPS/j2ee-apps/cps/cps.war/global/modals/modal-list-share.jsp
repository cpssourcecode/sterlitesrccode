<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>

	<dsp:getvalueof var="giftlistId" param="giftlistId"/>

	<div class="modal-header">
		<div class="header-symbol">
			<svg class="icon icon-write-1"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-write-1"></use></svg>
		</div>
		<button type="button" class="close" data-dismiss="modal">
			<span class="close-symbol" aria-hidden="true">&times;</span>
			<span class="sr-only">Close</span>
		</button>
		<h3 class="modal-title" id="myModalLabel">Share Reorder List</h3>
	</div>

	<div class="modal-body">
		<div class="form-horizontal" role="form">

			<div class="form-group">
				<div class="col-sm-12">
					<div class="alert alert-danger" style="display:none;" id="error-messages"></div>
				</div>
			</div>

			<dsp:form action="" method="post" id="shareList" formid="shareList">

				<dsp:input type="hidden" bean="GiftlistFormHandler.giftlistId" value="${giftlistId}"/>

				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email(s)</label>

					<div class="col-sm-10">
						<%--<dsp:input type="text" bean="GiftlistFormHandler.emailList" maxlength="1284" name="emailList" id="email"/>--%>
						<dsp:textarea iclass="form-control" bean="GiftlistFormHandler.emailList" name="emailList" id="email" maxlength="1284" cols="30" rows="3"/>
						<small class="text-muted">Separate multiple addresses with commas.</small>
					</div>
				</div>

				<div class="form-group">
					<label for="emailNotes" class="col-sm-2 control-label">Notes</label>

					<div class="col-sm-10">
						<dsp:textarea iclass="form-control" bean="GiftlistFormHandler.emailNotes" maxlength="250" name="emailNotes"/>
					</div>
				</div>
				<hr>

				<div class="form-group">
					<div class="col-sm-8 col-sm-offset-4">
						<ul class="list-inline pull-right">
							<li>
								<input type="button" data-event-click-id="global210"  class="btn btn-success btn-lg pull-right" value="Email Reorder List" id="submitReorderForm"/>
								<%--<button type="submit" class="btn btn-success btn-lg">Create List</button>--%>
							</li>
						</ul>
					</div>
				</div>

				<dsp:input type="hidden" bean="GiftlistFormHandler.shareGiftlist" value="true" priority="-10"/>

			</dsp:form>

		</div>
	</div>

	<script type="text/javascript" nonce="${requestScope.nonce}">
		// catch enter click and trigger correct button click instead of form submit
		$('#share-list-modal input').keypress(function (e) {
			if (e.which == 13) {
				$('#submitReorderForm').click();
				return false;
			}
		});
	</script>

</dsp:page>