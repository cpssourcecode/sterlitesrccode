<dsp:page>
	<a href="#" data-toggle="modal" data-target="#addAdminSuccess" style="display: none;" id="link-adminSuccess"></a>
	<div class="modal fade in" id="addAdminSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-narrow">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h2 class="modal-title">Invite sent</h2>
					<div class="row">
						<div class="col-sm-12">
							<p>Invite has been sent to new admin user.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>