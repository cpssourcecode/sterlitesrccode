<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>

<dsp:page>
	<dsp:getvalueof var="giftlistId" param="giftlist.id"/>

	<dsp:getvalueof var="eventName" param="giftlist.eventName"/>
	<dsp:getvalueof var="description" param="giftlist.description"/>

	<a href="#" data-toggle="modal" data-target="#modalEditMaterial${giftlistId}" style="display: none;" id="link-modalEditMaterial${giftlistId}"></a>

	<!-- Edit List -->
	<div class="modal fade in" id="modalEditMaterial${giftlistId}" tabindex="-1" role="dialog" 
			aria-labelledby="modalEditMaterial${giftlistId}" style="display: none; padding-right: 17px;" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<span class="modal-icon"><i class="icon icon-user-material"></i></span>
				</div>
				
				<div class="modal-body">
				<dsp:form action="" method="post" id="editListForm" formid="editListForm">
					<h4 class="modal-title" id="modal-matlist-title"><fmt:message key="account.material.material_edit_name_description"/></h4>
	
					<div class="form-group">
						<dsp:input type="text" bean="GiftlistFormHandler.eventName" value="${eventName}" iclass="form-control" id="eventName" maxlength="30"/>
					</div>
					<div class="form-group">
						<dsp:textarea bean="GiftlistFormHandler.description" id="description" iclass="form-control"
							 cols="4" rows="5" maxlength="125" style="margin-top: 0px; margin-bottom: 0px; height: 126px;">
									${description}
						</dsp:textarea>
					</div>
	
					<dsp:input type="hidden" bean="GiftlistFormHandler.giftlistId" value="${giftlistId}"/>
					<dsp:input type="hidden" bean="GiftlistFormHandler.updateGiftlist" value="true" priority="-10"/>
	
					<a href="#" class="btn btn-primary" data-dismiss="modal" data-event-click-id="global206" data-event-click-param0="${vsg_utils:escapeJS(giftlistId)}" >
						<fmt:message key="global.modal.save_edits"/>
					</a> 
					<a href="#" class="btn btn-info" data-dismiss="modal">
						<fmt:message key="global.modal.cancel"/>
					</a>
				</dsp:form>	
				</div>
			</div>
		</div>
	</div>

</dsp:page>
