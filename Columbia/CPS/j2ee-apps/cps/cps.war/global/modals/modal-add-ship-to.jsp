<dsp:page>
    <div class="modal fade in" id="addShipToAddress" tabindex="-1" role="dialog" aria-labelledby="modal-shiplist-title" style="display: none; padding-right: 17px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <dsp:getvalueof var="userId" param="userId"/>
                <dsp:getvalueof var="companyId" param="companyId"/>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="fa fa-truck"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-shipping-title">Select Ship To Addresses</h4>
                    <p>Add Ship To Addresses for this user.</p>
					<div class="alert alert-danger" style="display: none;" id="select-address-error">
						<button type="button" class="close" aria-label="Close" data-event-click-id="global176" >
							<span aria-hidden="true"> <span
								class="glyphicon glyphicon-remove-circle"></span>
							</span>
						</button>
						<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
							<dsp:param name="id" value="errNoShipToAddress" />
							<dsp:param name="elementName" value="message"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="message" param="message.message"/>
							</dsp:oparam>
							<dsp:oparam name="empty">
								<dsp:getvalueof var="message" value="No Ship to Addresses selected"/>
							</dsp:oparam>
						</dsp:droplet>
						${message}
					</div>
					<hr>
                    <form class="form">
                        <div class="row row-narrow mb20">
                            <div class="col-sm-9 mb5">
                                <div class="input-group input-group-lg">
                                    <input id="searchTerm" type="text" class="form-control input-lg" placeholder="Search for Ship To Address">
                                    <div class="input-group-btn">
                                        <button type="button" data-event-click-id="global177"  class="btn" title="Search"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3"><button id="resetBtn" type="button" data-event-click-id="global178"  class="btn btn-info btn-block btn-lg" disabled="">Reset</button>
                            </div>
                        </div>
                        <div class="modal-shipping-list">
                            <div id="shippingList">
                            </div>
                        </div>
                        <div>
                            <button type="button" data-event-click-id="global179"  class="btn btn-lg btn-primary">Confirm</button>
                            <button class="btn btn-lg btn-info" data-dismiss="modal">Cancel</button>
                        </div>
                    </form>
                    <dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
                    <script src="${staticContentPrefix}/js/add-ship-to${build_version}.js"></script>
                </div>
            </div>
        </div>
    </div>
</dsp:page>
<script nonce="${requestScope.nonce}">
	function hideErrorMessage(id){
		$('#' + id).attr('style','display:none');
	}
</script>