<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	<%--<dsp:importbean bean="/atg/userprofiling/Profile"/>--%>

	<a href="#" data-toggle="modal" data-target="#modalRejectOrders" style="display: none;"
	   id="link-modalRejectOrders">
	</a>

	<!-- Reject orders -->
  	<div class="modal fade in" id="modalRejectOrders" tabindex="-1" role="dialog" aria-labelledby="modalRejectOrders" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
				</div>
				<div id="reject-order-div">
					<div class="modal-body">
						<h4 class="modal-title" id="modal-text-title">No order has been selected.</h4>
		
						<a href="#" class="btn btn-info" data-dismiss="modal">
							Okay
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
