<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
    <dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler" />
    <dsp:importbean bean="/atg/userprofiling/Profile" />
    <dsp:getvalueof var="close" param="close" />
    <a href="#" data-toggle="modal" data-target="#selectSessionShipAddress" style="display: none;" id="link-selectSessionShipAddress" data-backdrop="static" data-keyboard="false"  data-page-name='different-shipto.jsp'></a>
    <div class="modal fade" id="selectSessionShipAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-shipAddress">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <span class="modal-icon"><i class="fa fa-truck"></i></span>
                </div>
                <div class="modal-body">
                  <dsp:getvalueof var="shippingAddressID" bean="Profile.shippingAddress.repositoryId" />
					<c:if test="${empty shippingAddressID}">
              			  <dsp:getvalueof var="shippingAddressID" bean="Profile.derivedShippingAddress.repositoryId" />
					</c:if>
               		<dsp:getvalueof var="shippingAddress" bean="Profile.shippingAddress" />
									<c:if test="${empty shippingAddress}">
										<dsp:getvalueof var="shippingAddress" bean="Profile.derivedShippingAddress" />
									</c:if>
                    <h4 class="modal-title">Select a Session Ship To Address</h4>
                    <p><cp:repositoryMessage key="selectSessionShipAddressMsg"/></p>
                    <hr>
                    <form class="form">
                        <dsp:droplet name="IsEmpty">
                            <dsp:param name="value" bean="Profile.selectedCS" />
                            <dsp:oparam name="true"></dsp:oparam>
                            <dsp:oparam name="false">
                                <h5>Cart Ship To Address</h5>
                            </dsp:oparam>
                        </dsp:droplet>
                        <div class="row row-flush">
                            <dsp:droplet name="IsEmpty">
                                <dsp:param name="value" bean="Profile.selectedCS" />
                                <dsp:oparam name="true"></dsp:oparam>
                                <dsp:oparam name="false">
                                    <dsp:getvalueof var="csId" bean="Profile.selectedCS.repositoryId" />
                                    <div class="col-xs-6">
                                        <div class="radio">
                                            <label class="active" data-initialize="radio" name="addressSelectRadio" id="myCustomRadioLabel" value="${csId}" data-event-click-id="global195" >
                                                <input type="radio" name="shipto" checked>
                                                <dsp:getvalueof var="address1" bean="Profile.selectedCS.address1" />
                                                <dsp:getvalueof var="address2" bean="Profile.selectedCS.address2" />
                                                <dsp:getvalueof var="address3" bean="Profile.selectedCS.address3" />
                                                <dsp:getvalueof var="city" bean="Profile.selectedCS.city" />
                                                <dsp:getvalueof var="state" bean="Profile.selectedCS.state" />
                                                <dsp:getvalueof var="zip" bean="Profile.selectedCS.postalCode" /> ${address1}
                                                </br>
                                                <c:if test="${not empty address2}">
                                                    ${address2}</br>
                                                </c:if>
                                                ${city},&nbsp;${state}&nbsp;${zip}
                                            </label>
                                        </div>
                                    </div>
                                    
       						<div class="col-xs-6">
                                        <button id="confirmBtn1" type="button" class="btn btn-primary btn-lg btn-block" >Confirm</button>
                                    </div> 
                                </dsp:oparam>
                            </dsp:droplet>
                        </div>
                        <hr>

							<h5>Default Ship To Address</h5>
							<div class="row row-flush">
							<div class="col-xs-6">
							<div class="radio">
								<label class="active" data-initialize="radio" name="addressSelectRadio" id="myCustomRadioLabel"	value="${shippingAddressID}" data-event-click-id="global195"> 
								 <input type="radio" name="shipto" >
								<dsp:valueof value="${vsg_utils:contactInfoAddress(shippingAddress)}" valueishtml="true" />
								</label>
								</div>
								</div>
				<%-- Removed this button for SM-385 	
							<div class="col-xs-6">
									<button id="confirmBtn" type="button" class="btn btn-primary btn-lg btn-block">Confirm</button>
								</div> --%>
							</div>
							
							<hr>


						<h5>Select an alternate Ship To Address for this session.</h5>
                        <div class="alert alert-icon">
                            <i class="fa fa-exclamation-triangle red pt10"></i>
                            <p><strong>Changing your Ship To Address may affect pricing and availability of items searched or in your cart.</strong></p>
                            <p><strong>
                            	<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="infoNeedToAddNewShipAddr"/>
									<dsp:param name="notWrap" value="true"/>
								</dsp:include>
                            <!-- Need to add a new Ship To Address? Contact your administrator. -->
                            </strong></p>
                        </div>
                        <div class="row row-slim mb20">
                            <div class="col-sm-6 mb5">
                                <div class="input-group input-group-shorten">
                                    <input type="text" class="form-control" placeholder="Search for Ship To Address" id="modal-cs-input-search">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn" title="Search" data-event-click-id="global196" data-event-click-param0="${csId}" ><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <dsp:getvalueof var="pOrganization" bean="Profile.parentOrganization" />
                            <c:if test="${not empty pOrganization}">
                                <dsp:getvalueof var="orgList" bean="Profile.parentOrganization.childOrganizations" />
                                <c:if test="${not empty orgList}">
                                    <dsp:getvalueof var="billingAccounts" bean="Profile.billingAccounts" />
                                    <div class="col-sm-4 mb5">
                                        <span class="select-wrap">
                                            <select id="selectedBillTo" class="form-control" data-event-change-id="global198" data-event-change-param0="${csId}" >
                                                <option value="" selected>
                                                    Billing Account
                                                </option>
                                                <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                                                    <dsp:param name="array" value="${billingAccounts}" />
                                                    <dsp:param name="elementName" value="org" />
                                                    <dsp:oparam name="output">
                                                        <dsp:getvalueof var="name" param="org.name" />
                                                        <dsp:getvalueof var="orgId" param="org.id" />
                                                        <dsp:getvalueof var="billToId" param="org.billingAddress.id" />
                                                        <option value="${orgId}" title="${name}" data-content="${name}">
                                                            ${name}
                                                        </option>
                                                    </dsp:oparam>
                                                </dsp:droplet>
                                            </select>
                                        </span>
                                    </div>
                                </c:if>
                            </c:if>
                            <div class="col-sm-2">
                                <button class="btn btn-info btn-block" data-event-click-id="global197" data-event-click-param0="${csId}"  type="button">Reset</button>
                            </div>
                        </div>
                        <div id="modal-cs-list-content">
                            <dsp:include page="gadgets/modal-cs-list.jsp?cs=${csId}" />
                        </div>
                        <div>
                            <button id="confirmBtn2" class="btn btn-lg btn-primary" type="button">Confirm</button>
                            <button class="btn btn-lg btn-info" data-dismiss="modal" type="button">Cancel</button>
                        </div>
                    </form>
                </div>
                <dsp:form method="post" id="modal-cs-form">
                    <dsp:input type="hidden" value="" id="selected_shipping_id" bean="ProfileDefaultShippingAddressFormHandler.selectedId" />
                    <dsp:input type="hidden" value="shipped" id="select_address_delivery_method" bean="ProfileDefaultShippingAddressFormHandler.deliveryMethod" />
                    <dsp:input type="hidden" value="false" id="select_address_update_sg" bean="ProfileDefaultShippingAddressFormHandler.updateShippingGroup" />
                    <dsp:input type="hidden" value="true" bean="ProfileDefaultShippingAddressFormHandler.saveProfileShippingAddress" />
                </dsp:form>
            </div>
        </div>
    </div>
    <c:if test="${not empty close }">
		<dsp:setvalue value="${close}" bean="/cps/util/CPSSessionBean.differentShipto"/>
	</c:if>
	<dsp:getvalueof var="sessionAddressFlag" bean="/cps/util/CPSSessionBean.differentShipto"/>
	<input type="hidden" value="${sessionAddressFlag}" id="sessionAddressFlag"/>
</dsp:page>