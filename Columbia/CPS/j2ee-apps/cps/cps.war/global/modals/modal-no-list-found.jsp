<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<dsp:getvalueof var="messageId" param="messageId"/>
	<dsp:getvalueof var="divId" param="divId"/>
	<dsp:getvalueof var="isReset" param="isReset"/>
	
	<a href="#" data-toggle="modal" data-target="#noResultsModal${divId}" style="display: none;" id="link-noResultsModal${divId}"></a>

	<!-- No List Found -->
	<div class="modal fade in" id="noResultsModal${divId}" tabindex="-1" role="dialog" aria-labelledby="noResultsModal${divId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-matnone-title">
					<c:if test="${!fn:contains(divId,'_noResultsFound')}">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="${messageId}"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include> 
						</c:if>
						<span id="noResultsModalAddText${divId}"></span>						
					</h4>	

					<c:choose>
						<c:when test="${isReset eq 'true'}">
							<div class="row">
								<div class="col-xs-12 text-left">
									<button class="btn btn-info" data-event-click-id="global216" data-event-click-param0="${vsg_utils:escapeJS(isReset)}"  type="button"
											data-dismiss="modal">
										<c:choose>
											<c:when test="${messageId eq 'err_search_fail'}">
												Close
											</c:when>
											<c:otherwise>
												<fmt:message key="global.modal.ok"/>
											</c:otherwise>
										</c:choose>
										<%-- --%>
									</button>
								</div>
							</div>
						</c:when>
						<c:otherwise>
							<a href="#" class="btn btn-info" data-dismiss="modal">
								<fmt:message key="modal.cart.button.okay"/>
							</a>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
