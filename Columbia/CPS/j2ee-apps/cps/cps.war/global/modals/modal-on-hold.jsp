<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:getvalueof var="showCS" param="showCS"/>

	<input id="showCS" type="hidden" value="${vsg_utils:escapeHtml(showCS)}"/>
	<a href="#" data-toggle="modal" data-target="#onHoldAccount" style="display: none;" id="link-onHoldAccount"></a>
	<div class="modal fade" id="onHoldAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">

					<h4 class="modal-title">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="notWrap" value="true"/>
							<dsp:param name="key" value="err_credit_hold"/>
						</dsp:include>
					</h4>

					<br/>

					<button type="button" class="btn btn-info" data-dismiss="modal" >
						<fmt:message key="global.modal.ok"/>
					</button>
				</div>
			</div>
		</div>
	</div>

</dsp:page>