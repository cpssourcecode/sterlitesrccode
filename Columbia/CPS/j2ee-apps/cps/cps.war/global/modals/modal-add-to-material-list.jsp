<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/cps/userprofiling/MaterialListHandler" />

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />

	<dsp:getvalueof var="prodId" param="prodId" />
	<dsp:getvalueof var="itemsInfo" param="itemsInfo" />
	<dsp:getvalueof var="skuId" param="skuId" />
	<dsp:getvalueof var="qty" param="qty" />
	<c:if test="${empty qty || qty == '0'}">
		<dsp:getvalueof var="qty" value="1" />
	</c:if>

	<dsp:getvalueof var="isMulti" value="${not empty itemsInfo}" />

	<a class="hidden" data-toggle="modal" href="" data-target="#add-to-material-list" id="modal-add-to-material-list-link"></a>

	<div class="modal fade" id="add-to-material-list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

		<div class="modal-dialog modal-narrow" role="document" id="modal-select">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<span class="modal-icon">
						<i class="icon icon-user-material"></i>
					</span>
				</div>
				<div class="modal-body" id="add-to-material-list-body1">
					<h4 class="modal-title" id="modal-matlistselect-title">Add to Material List</h4>

					<div class="alert alert-danger" role="alert" id="message-modal-error-div" style="display: none;">
						<p id="message-modal-error"></p>
					</div>

					<div class="form-group">
					<!-- 	<span class="select-wrap"> -->
							<select id="control-ml-giftlist" class="form-control newselect-list" data-width="100%">								<option selected="true" disabled="disabled" value="default-material-list-selection">Select List</option>
								<dsp:droplet name="/atg/dynamo/droplet/ForEach">
									<dsp:param name="array" bean="Profile.giftlists" />
									<dsp:param name="elementName" value="giftlist" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="giftlistId" param="giftlist.id" />
										<dsp:getvalueof var="giftlistName" param="giftlist.eventName" />
										<option value="${giftlistId}">${giftlistName}</option>
									</dsp:oparam>
								</dsp:droplet>
								<option value="addnew">New List...</option>
							</select>
							<!-- <span class="caret caret-select"></span>
						</span> -->
					</div>

					<p id="select-material-list-warning" style="display: none;">
						<span class="glyphicon glyphicon-alert text-danger"></span>&nbsp; <strong
							class="text-danger">IMPORTANT:</strong>&nbsp;You need to select a list.
					</p>

					<div class="newselect-addnew" style="display: none;">
						<div class="form-group">
							<input id="control-ml-name" type="text" maxlength="50" placeholder="Name"
								   class="form-control">
						</div>
						<div class="form-group">
							<textarea id="control-ml-description" maxlength="125" placeholder="Description" rows="5"
									  cols="4" class="form-control"></textarea>
						</div>
					</div>
					<input type="hidden" name="modalAddToMaterialIsMulti" id="modalAddToMaterialIsMulti"
						   value="${isMulti}"/>

					<c:choose>
						<c:when test="${isMulti}">
							<a id="add-to-list-multi" href="#" class="btn btn-primary"> Add to List </a>
						</c:when>
						<c:otherwise>
							<a id="add-to-list-single" href="#" class="btn btn-primary"> Add to List </a>
						</c:otherwise>
					</c:choose>

					<button class="btn btn-info" data-dismiss="modal">Cancel</button>
				</div>

				<div class="modal-body" id="add-to-material-list-body2" style="display: none"></div>
			</div>
		</div>
	</div>
	</div>
	<c:choose>
		<c:when test="${isMulti}">
			<dsp:form action="#" method="post" id="multi-add-to-material-list-form" formid="multi-add-to-material-list-form">
				<dsp:input id="input-ml-m-a-giftlist" bean="MaterialListHandler.giftlistId" type="hidden" value="" />
				<dsp:input id="input-ml-m-a-items-info" bean="MaterialListHandler.addedSkuIdsWithQty" converter="map" type="hidden" value="${itemsInfo}" />
				<dsp:input type="hidden" bean="MaterialListHandler.addItems" value="true" priority="-10" />
			</dsp:form>
			<dsp:form action="#" method="post" id="create-multi-add-to-material-list-form" formid="create-multi-add-to-material-list-form">
				<dsp:input id="input-ml-m-ca-giftlist" bean="MaterialListHandler.giftlistId" type="hidden" value="" />
				<dsp:input id="input-ml-m-ca-items-info" bean="MaterialListHandler.addedSkuIdsWithQty" converter="map" type="hidden" value="${itemsInfo}" />
				<dsp:input id="input-ml-m-ca-eventName" bean="MaterialListHandler.eventName" type="hidden" value="" />
				<dsp:input id="input-ml-m-ca-description" bean="MaterialListHandler.description" type="hidden" value="" />
				<dsp:input type="hidden" bean="MaterialListHandler.createListAddItems" value="true" priority="-10" />
			</dsp:form>
		</c:when>
		<c:otherwise>
			<dsp:form action="#" method="post" id="add-to-material-list-form" formid="add-to-material-list-form">
				<dsp:input id="input-ml-a-prod-id" bean="MaterialListHandler.productId" type="hidden" value="${prodId}" />
				<dsp:input id="input-ml-a-sku-id" bean="MaterialListHandler.catalogRefIds" type="hidden" value="${skuId}" />
				<dsp:input id="input-ml-a-qty" bean="MaterialListHandler.quantity" type="hidden" value="${qty}" />
				<dsp:input id="input-ml-a-giftlist" bean="MaterialListHandler.giftlistId" type="hidden" value="" />
				<dsp:input type="hidden" bean="MaterialListHandler.addItem" value="true" priority="-10" />
			</dsp:form>
			<dsp:form action="#" method="post" id="create-add-to-material-list-form" formid="create-add-to-material-list-form">
				<dsp:input id="input-ml-ca-prod-id" bean="MaterialListHandler.productId" type="hidden" value="${prodId}" />
				<dsp:input id="input-ml-ca-sku-id" bean="MaterialListHandler.catalogRefIds" type="hidden" value="${skuId}" />
				<dsp:input id="input-ml-ca-qty" bean="MaterialListHandler.quantity" type="hidden" value="${qty}" />
				<dsp:input id="input-ml-ca-eventName" bean="MaterialListHandler.eventName" type="hidden" value="" />
				<dsp:input id="input-ml-ca-description" bean="MaterialListHandler.description" type="hidden" value="" />
				<dsp:input type="hidden" bean="MaterialListHandler.createListAddItem" value="true" priority="-10" />
			</dsp:form>
		</c:otherwise>
	</c:choose>

</dsp:page>