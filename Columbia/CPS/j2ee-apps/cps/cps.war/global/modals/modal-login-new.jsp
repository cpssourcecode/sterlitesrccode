<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/cps/userprofiling/util/LoginSessionInfo"/>
	<dsp:importbean bean="/cps/userprofiling/ForgotPasswordFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileTools"/>
	<dsp:getvalueof var="allowNewUserSelfRegistration" bean="ProfileTools.allowNewUserSelfRegistration" />

	<!-- Log In -->
	<div class="modal fade in" id="logIn" tabindex="-1" role="dialog" aria-labelledby="modal-login-title" style="padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="icon icon-user"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-login-title"><fmt:message key="account.title.logInToYourAccount"/></h4>
                    <div class="alert alert-danger" style="display:none;" id="error-messages"></div>
                    <div class="alert bg-success" style="display:none;" id="modal-success-messages"></div>
					<dsp:form action="${originatingRequest.contextPath}/" method="post" id="login-form" formid="login-form">
						<dsp:droplet name="/cps/droplet/RememberMeDroplet">
							<dsp:oparam name="output">
								<dsp:getvalueof var="email" param="email"/>
								<dsp:getvalueof var="remember" param="isRemember"/>
							</dsp:oparam>
						</dsp:droplet>
						<fmt:message key="account.fields.email" var="emailText"/>
						<div class="form-group">
							<label class="control-label sr-only"><fmt:message key="account.fields.emailAddress"/></label>
							<dsp:input iclass="form-control input-lg" id="input-email"
									   bean="ProfileFormHandler.value.login" type="email" maxlength="100" value="${email}">
								<dsp:tagAttribute name="placeholder" value="${emailText}"/>
							</dsp:input>
						</div>
						<fmt:message key="account.fields.password" var="passwordText"/>
						<div class="form-group">
							<label class="control-label sr-only"><fmt:message key="account.fields.password"/></label>
							<dsp:input iclass="form-control input-lg" id="input-password"
									   bean="ProfileFormHandler.value.password" type="password" autocomplete="new-password" maxlength="15">
								<dsp:tagAttribute name="placeholder" value="${passwordText}"/>
							</dsp:input>
						</div>
						<div class="checkbox mb15">
							<label>
								<dsp:input id="input-rememberMe" bean="ProfileFormHandler.value.rememberMe" value="true" type="checkbox" checked="${remember}"/>
								<fmt:message key="account.fields.rememberMe"/>
							</label>
						</div>
                        <div id="login-form-captcha" style="display: none">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div id="recaptchaDefault"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<button class="btn btn-primary btn-lg btn-block mb15" nonce="${requestScope.nonce}" id="submitLoginForm" type="button" data-event-click-id="global211" >Log In</button>
						<p><a data-dismiss="modal" data-toggle="modal" data-target="#forgotPasswordModal"><fmt:message key="account.navigation.forgotPassword"/></a><br>
						<c:if test="${allowNewUserSelfRegistration}">
							Don't have a login? <a href data-event-click-id="eventGoToRegistrationPage"><fmt:message key="account.title.registerAccountLink"/>.</a>
						</c:if>
						<strong><br><cp:repositoryMessage key="needOrderFastMessage"/></strong>
						</p>
						<dsp:input type="hidden" bean="ProfileFormHandler.login" value="true" priority="-10"/>
					</dsp:form>
				</div>
			</div>
		</div>
	</div>


	<div style="display: none">
		<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
		<dsp:form id="modal-login-addToCart" formid="modal-login-addToCart" action="" method="post">
			<dsp:input bean="CartModifierFormHandler.addItemCount" id="modal-login-add-qty" type="hidden" value="3" priority="10"/>

			<dsp:input bean="CartModifierFormHandler.items[0].quantity" id="modal-login-qty-0" type="hidden" value="0"/>
			<dsp:input bean="CartModifierFormHandler.items[0].productId" id="modal-login-productId-0" type="hidden" value=""/>
			<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" id="modal-login-skuId-0" type="hidden" value="" />

			<dsp:input bean="CartModifierFormHandler.items[1].quantity" id="modal-login-qty-2" type="hidden" value="0"/>
			<dsp:input bean="CartModifierFormHandler.items[1].productId" id="modal-login-productId-2" type="hidden" value=""/>
			<dsp:input bean="CartModifierFormHandler.items[1].catalogRefId" id="modal-login-skuId-2" type="hidden" value="" />

			<dsp:input bean="CartModifierFormHandler.items[2].quantity" id="modal-login-qty-3" type="hidden" value="0"/>
			<dsp:input bean="CartModifierFormHandler.items[2].productId" id="modal-login-productId-3" type="hidden" value=""/>
			<dsp:input bean="CartModifierFormHandler.items[2].catalogRefId" id="modal-login-skuId-3" type="hidden" value="" />

			<dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
		</dsp:form>
	</div>

	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-password-forgot.jsp"/>
</dsp:page>

<script type="text/javascript" nonce="${requestScope.nonce}">
	$('#logIn input').on('input', function(e) {
		checkEnableLoginButton();
	});

	// catch enter click and trigger correct button click instead of form submit
	$('#logIn input').keypress(function (e) {
		if (e.which == 13) {
			$('#submitLoginForm:enabled').click();
			return false;
		}
	});
</script>