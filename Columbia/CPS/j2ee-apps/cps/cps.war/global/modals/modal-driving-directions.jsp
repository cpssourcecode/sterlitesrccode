<dsp:page>
	<!-- Driving Directions -->
	<dsp:getvalueof var="address1" param="address1" />
	<dsp:getvalueof var="city" param="city" />
	<dsp:getvalueof var="state" param="state" />
	<dsp:getvalueof var="zip" param="zip" />
	<div class="modal fade" id="drivingDirections" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<span class="modal-icon">
						<i class="fa fa-map-marker"></i>
					</span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-directions-title">Driving Directions</h4>
					<div class="alert alert-danger" id="direction-error" style="display: none;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">
								<span class="glyphicon glyphicon-remove-circle"></span>
							</span>
						</button>
						Enter a Start Address
					</div>
					<div class="modal-mapit setDirections" id="edit-starting-button">
						<div class="form-group">
							<label class="control-label">From</label>
							<input type="text" class="form-control input-lg" id="adressStart" placeholder="Start Address" maxlength="60">
						</div>
						<div class="form-group">
							<label class="control-label">To</label>
							<input type="text" class="form-control input-lg" id="adressDestination" placeholder="Destination Address" value="${address1}, ${city}, ${state}, ${zip}" disabled>
						</div>
						<button type="button" id="mapIt" data-event-click-id="global203"  class="btn btn-primary">Map It</button>
						<button class="btn btn-info" data-dismiss="modal">Cancel</button>
					</div>
					<div class="showDirections hidden">
						<div class="row">
							<div class="col-xs-12">
								<button class="toAddress btn btn-link" data-event-click-id="global204"  data-toggle="collapse"
										data-target=".modal-mapit" aria-expanded="false" aria-controls="modal-mapit">
									<i class="fa fa-angle-left"></i>
									Edit Starting Address
								</button>
							</div>
							<div class="col-xs-12">
								<div id="directions-list" class="address-list">
									<!-- <img src="${staticContentPrefix}/assets/images/directions1-3.png" alt="" /> -->
								</div>
								<div id="direction-map" class="direction-map" style="width: 260px; height: 352px;"></div>
								<button type="button" data-event-click-id="global205"  class="btn btn-primary">Print</button>
								<button class="btn btn-info" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</div>
					<script type="text/javascript" nonce="${requestScope.nonce}">
						var directionsService;
						var directionsDisplay;
						
						function editStartingAddress() {
							$('#edit-starting-button').removeClass("collapse");
                            $('#edit-starting-button').removeClass("in");
							$('#edit-starting-button').removeAttr("style")
                        }
						
						function mapDirections(){
							$("#direction-error").hide();
							var addStart = $('#adressStart').val();
							console.log("Start: "+addStart);
							if (addStart != null && addStart != ''){
								var $drivingDirections = $('#drivingDirections');
								$drivingDirections.find(".setDirections").addClass('hidden');
								$drivingDirections.find(".showDirections").removeClass('hidden');
								initMap();
								calculateAndDisplayRoute(directionsService, directionsDisplay);
							} else {
								// Missing address through error
								$("#direction-error").show();
							}
						}
					
						function initMap() {
							console.log("initMap");
							directionsService = new google.maps.DirectionsService();
							directionsDisplay = new google.maps.DirectionsRenderer();
							var map = new google.maps.Map(document.getElementById('direction-map'), {
								center: {lat: 42.601619944327965, lng: -87.51708984375},
								zoom: 6
							});
							console.log("New map created");
							directionsDisplay.setMap(null);
							directionsDisplay.setMap(map);
							document.getElementById('directions-list').innerHTML='';
							directionsDisplay.setPanel(document.getElementById('directions-list'));
						}
						function calculateAndDisplayRoute(directionsService, directionsDisplay) {
							console.log("calculate driving route");
							console.log("From: "+document.getElementById('adressStart').value);
							console.log("To: "+document.getElementById('adressDestination').value);
							directionsService.route({
								origin: document.getElementById('adressStart').value,
								destination: document.getElementById('adressDestination').value,
								travelMode: google.maps.TravelMode.DRIVING
							}, function(response, status) {
								if (status === google.maps.DirectionsStatus.OK) {
									directionsDisplay.setDirections(response);
								} else {
									window.alert('Directions request failed due to ' + status);
								}
							});
						}
						function printDirections() {
							var content = document.getElementById('directions-list').innerHTML;
							var win = window.open();
							win.document.write(content);
							win.document.close();
							win.focus();
							window.setTimeout(function(){win.print();var printerval=window.setInterval(function(){win.close();},500);},1000);
						}
					</script>
				</div>
			</div>
		</div>
	</div>
</dsp:page>