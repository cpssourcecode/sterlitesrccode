<dsp:page>

	<a href="#" data-toggle="modal" data-target="#err_search_order" style="display: none;" id="link-err_search_order"></a>

	<div class="modal fade" id="err_search_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-text-title"></h4>

					<p>
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="notWrap" value="true"/>
							<dsp:param name="key" value="err_search_order"/>
						</dsp:include>
					</p>

					<button class="btn btn-info" data-dismiss="modal" id="dismiss-err_search_order">Okay</button>
				</div>
			</div>
		</div>
		<%--<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<div class="header-symbol">
						<svg class="icon icon-missing">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-missing"></use>
						</svg>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
					</button>
					<!-- <h4 class="modal-title" id="myModalLabel">c</h4> -->
				</div>
				<div class="modal-body">
					&lt;%&ndash;<h2 class="modal-title">Alert Messsage Header</h2>&ndash;%&gt;
					<p>
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="notWrap" value="true"/>
							<dsp:param name="key" value="err_search_order"/>
						</dsp:include>
					</p>
				</div>
				<div class="modal-footer">
					<ul class="list-inline pull-left">
						<li>
							<a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal" id="dismiss-err_search_order">Okay</a>
						</li>
					</ul>
				</div>
			</div>
		</div>--%>
	</div>

</dsp:page>


