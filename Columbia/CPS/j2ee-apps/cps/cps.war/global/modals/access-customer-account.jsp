<dsp:page>

	<div class="modal fade" id="accessCustomerAccount" tabindex="-1"
		 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-narrow">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-confirmcheck-title">Access Customer Account</h4>

					<p>Are you sure you want to access this customer's account?<br>Changes
						you make may impact this customer's account.
					</p>

					<button class="btn btn-primary" href="javascript:void(0);" data-event-click-id="global174" >Proceed</button>

					<button class="btn btn-info" href="#" data-dismiss="modal" >Cancel</button>

					<div class="checkbox"><label>
						<input id="warning-check" type="checkbox" value=""> Don't show this message again</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>