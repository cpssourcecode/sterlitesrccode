<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
<dsp:importbean bean="/cps/util/FileUploadHandler"/>
<%-- <dsp:importbean bean="/cps/content/service/ContentPagesService"/>
<dsp:getvalueof var="instructionImportMT" bean="ContentPagesService.instructionImportMT" /> --%>

<dsp:getvalueof var="importModalError" param="importModalError"/>

<dsp:page>
	<dsp:getvalueof var="giftlistId" param="giftlistId"/>

	<a href="#" data-toggle="modal" data-target="#modalImportMaterial${giftlistId}" style="display: none;"
	   id="link-modalImportMaterial${giftlistId}"></a>

	<!-- Import List -->
 	 <div class="modal fade in" id="modalImportMaterial${giftlistId}" tabindex="-1" role="dialog" aria-labelledby="modalImportMaterial${giftlistId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow importMaterialListDocument" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-upload"></i></span>
				</div>
				<div class="modal-body">
					<div class="row">
		                <div class=" col-sm-12 col-xs-12 col-container importMaterialListLeft">
							<h4 class="modal-title" id="modal-import-title"><fmt:message key="account.material.import"/></h4>
			
							<!-- <p>To add [items], upload a spreadsheet in XLS format</p> -->
							<dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="FileUploadHandler.formExceptions"/>
							<c:if test="${importModalError && not empty formExceptions}">
								<div class="alert alert-danger errors">
									<p>
										<c:forEach var="formException" items="${formExceptions}">
											<dsp:param name="formException" value="${formException}"/>
											<c:if test="${formException.message != ''}">
												<dsp:valueof param="formException.message" valueishtml="true"/><br/>
											</c:if>
										</c:forEach>
									</p>
								</div>
							</c:if>
		
							<div id="errorIncorrect" style="display:none;" class="alert alert-danger errors">
								<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="err_incorrect_file"/>
									<dsp:param name="notWrap" value="true"/>
								</dsp:include>
							</div>
							
							<dsp:form enctype="multipart/form-data" id="import-material-list-form" method="post" action="">
								<div class="form-group">
									<div data-provides="fileinput">
										<dsp:input iclass="form-control" type="file" bean="FileUploadHandler.importFile" value="" name="importFile"/>
										<div style="display: none;" data-trigger="fileinput">
											<i class="glyphicon glyphicon-file fileinput-exists"></i>
											<span class="fileinput-filename"></span>
										</div>
			
										<dsp:input type="hidden" bean="FileUploadHandler.readFileSuccessURL" value="/account/material-detail.jsp?giftlistId=${giftlistId}"/>
										<dsp:input type="hidden" bean="FileUploadHandler.readFileErrorURL" value="/account/material-detail.jsp?giftlistId=${giftlistId}&importModalError=true"/>
										<dsp:input type="hidden" bean="FileUploadHandler.giftlistId" value="${giftlistId}"/>
										<dsp:getvalueof var="ca" param="ca"/>
										<c:if test="${ca}">
											<dsp:input type="hidden" bean="FileUploadHandler.giftlistCheckAvailability" value="true"/>
										</c:if>
										<dsp:input type="hidden" bean="FileUploadHandler.readImportMaterialFile" value="true" priority="-10" />
									</div>
								</div>
								<%-- <a class="btn btn-link" href="/document/MaterialListTemplate.xlsx">
									<span><i class="fa fa-download mr5"></i>
										<fmt:message key="global.modal.download_spreadsheet_template"/>
									</span>
								</a><br> --%>
								<a class="importHelpMLlink green-link" >
									<dsp:include page="/includes/gadgets/info-message.jsp">
										<dsp:param name="key" value="infoHowCanIImportFile"/>
										<dsp:param name="notWrap" value="true"/>
									</dsp:include>
								</a><br><br>

								<button type="button" class="btn btn-primary material-list-upload-btn" data-event-click-id="global208" data-giftlist-id="${giftlistId}">
									<fmt:message key="global.modal.upload"/>
								</button>
								<a data-dismiss="modal" class="btn btn-info" href="#">
									<fmt:message key="global.modal.cancel"/>
								</a>
							</dsp:form>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-container p15 well collapse green-text importHelpML" >
		                	<span>
		                		<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="infoImportFileInstruction"/>
									<dsp:param name="notWrap" value="true"/>
								</dsp:include>
								<p>
									<span class="pull-right importHelpMLlink green-link">CLOSE</span>&nbsp;
								</p>
		                	</span>
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div> 
</dsp:page>
