<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />

	<a href="#" data-toggle="modal" data-target="#sessionIdleModal" style="display: none;" id="link-sessionIdleModal"></a>

	<div class="modal fade" id="sessionIdleModal" tabindex="-1" role="dialog" aria-labelledby="sessionIdleModal" aria-hidden="true">

		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<span class="modal-icon">
						<i class="fa fa-info"></i>
					</span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-text-title">
						<fmt:message key="global.logout.session_is_idle" />
					</h4>
					<p>
						<fmt:message key="global.logout.label" />
					</p>

					<button class="btn btn-info" data-dismiss="modal" id="dismiss-err_search_order">Okay</button>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
