<dsp:page>

    <a href="#" data-toggle="modal" data-target="#productAddedToCartModal" style="display: none;" id="link-productAddedToCartModal"></a>

    <div class="modal fade" id="productAddedToCartModal" tabindex="-1" role="dialog" aria-labelledby="productAddedToCartModal" aria-hidden="true">
        <div class="modal-dialog modal-middle-wide" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-icon">
                        <i class="glyphicon glyphicon-ok"></i>
                    </span>
                </div>
                <div class="modal-body">
                    <div id="productAddedToCartModalContent"></div>
<%--
                    <button class="btn btn-info" data-dismiss="modal" id="dismiss-productAddedToCartModal">Okay</button>
--%>
                </div>
            </div>
        </div>

    </div>

</dsp:page>