<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/cps/droplet/UserMaterialListsDroplet"/>
	
	<dsp:getvalueof var="messageId" param="messageId"/>
	<dsp:getvalueof var="divId" param="divId"/>	

	<%-- <dsp:droplet name="UserMaterialListsDroplet">
		<dsp:param name="term" value="${searchTerm}" />
		<dsp:param name="cs" value="${searchCS}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="giftlists" param="giftlists" />
			<dsp:getvalueof var="emptyResult" param="emptyResult" />
			<dsp:getvalueof var="giftlistsCount" param="giftlistsCount" />

			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="CPSSessionBean.sortableReorderLists" />
				<dsp:param name="elementName" value="giftlist" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="count" param="count" />
					<c:if test="${count==1}">
						<dsp:getvalueof var="id" param="giftlist.id" />
						<dsp:getvalueof var="giftlistItems" param="giftlist.giftlistItems" />
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet> --%>

	<a href="#" data-toggle="modal" data-target="#noResultsModal${divId}" style="display: none;" id="link-noResultsModal${divId}"></a>

	<!-- No List Found -->
	<div class="modal fade in" id="noResultsModal${divId}" tabindex="-1" role="dialog" aria-labelledby="noResultsModal${divId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-check"></i></span>
				</div>				
				<div class="modal-body">
					<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-giftlist-count.jsp">
						<dsp:param name="messageId" value="${messageId}"/>
						<dsp:param name="divId" value="${divId}"/>
					</dsp:include>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
