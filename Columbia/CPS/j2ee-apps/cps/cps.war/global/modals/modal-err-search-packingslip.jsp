<dsp:page>

	<a href="#" data-toggle="modal" data-target="#err_search_packingslip" style="display: none;" id="link-err_search_packingslip"></a>

    <div class="modal fade" id="err_search_packingslip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="fa fa-info"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-text-title"></h4>
                    <p>
                        <dsp:include page="/includes/gadgets/info-message.jsp">
                            <dsp:param name="notWrap" value="true"/>
                            <dsp:param name="key" value="err_search_packingslip"/>
                        </dsp:include>
                    </p>

                    <button class="btn btn-info" data-dismiss="modal" id="dismiss-err_search_packingslip">Okay</button>
                </div>
            </div>
        </div>
    </div>

</dsp:page>


