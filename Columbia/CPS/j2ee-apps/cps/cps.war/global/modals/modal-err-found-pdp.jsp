<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/util/PDPFoundAnErrorFormHandler" />
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	
	<dsp:getvalueof var="isAnonymousUser" bean="Profile.transient"/>
	<dsp:getvalueof var="displayName" param="product.displayName"/>
	<dsp:getvalueof var="brandName" param="product.brand_name"/>
	<dsp:getvalueof var="vendorItem" param="product.vendorItem"/>
	<dsp:getvalueof var="pricesvcUPC" param="product.pricesvc_upc"/>
	<dsp:getvalueof var="productDescription" param="product.description"/>
	<dsp:getvalueof var="userEmail" bean="Profile.email" />
	<dsp:getvalueof var="firstName" bean="Profile.firstName" />
	
	<div class="modal fade" id="errorFoundOnPDPModal" tabindex="-1" role="dialog" aria-labelledby="errorFoundOnPDPModalTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<dsp:form action="${originatingRequest.requestURI}" method="POST" id="error-found-pdp-page-form" formid="error-found-pdp-page-form">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">x</span>
						</button>
						<span class="modal-icon">
							<i class="fa fa-info"></i>
						</span>
					</div>
					<div class="modal-body">
						<h4 class="modal-title" id="modal-error-found-pdp-page-title"><fmt:message key="header.error_found_pdp_page"/></h4>
						<p><fmt:message key="pdp.error.improve.website"/></p>
						<hr>
						<div>
							<table>
								<tr>
									<td class="pdp-err-modal"><fmt:message key="pdp.error.item.number" /></td>
									<td>${vsg_utils:escapeHtml(displayName)}</td>
								</tr>
								<tr>
									<td class="pdp-err-modal"><fmt:message key="pdp.error.brand"/> </td>
									<td>${vsg_utils:escapeHtml(brandName)}</td>
								</tr>
								<tr>
									<td class="pdp-err-modal"><fmt:message key="pdp.error.mfr.part"/></td>
									<td>${vsg_utils:escapeHtml(vendorItem)}</td>
								</tr>
								<c:if test="${not empty pricesvcUPC}">
									<tr>
										<td class="pdp-err-modal"><fmt:message key="pdp.error.upc"/></td>
										<td>${vsg_utils:escapeHtml(pricesvcUPC)}</td>
									</tr>
								</c:if>
								<tr>
									<td class="pdp-err-modal"><fmt:message key="pdp.error.product.description"/></td>
									<td>${productDescription}</td>
								</tr>
							</table>							
						</div>
						<hr>
						<div class="form-group">
							<h4>
								<fmt:message key="pdp.error.type" />
							</h4>
							<div class="checkbox">
								<label>
									<dsp:input type="checkbox" id="pdp-err-description" 
										bean="PDPFoundAnErrorFormHandler.typesOfError.productDescription"
										value="Product Description / Specifications" checked="true" />
									<span class="checkbox-label"><fmt:message key="pdp.error.type.description" /></span>
								</label>
							</div>
							<div class="checkbox">
								<label>
									<dsp:input type="checkbox" id="pdp-err-features" 
										bean="PDPFoundAnErrorFormHandler.typesOfError.features"
										value="Features" /> 
									<span class="checkbox-label"><fmt:message key="pdp.error.type.features" /></span>
								</label>
							</div>
							<div class="checkbox">
								<label>
									<dsp:input type="checkbox" id="pdp-err-image" 
										bean="PDPFoundAnErrorFormHandler.typesOfError.image"
										value="Image" />
									<span class="checkbox-label"><fmt:message key="pdp.error.type.image" /></span>
								</label>
							</div>
							<div class="checkbox">
								<label>
									<dsp:input type="checkbox" id="pdp-err-brochure" 
										bean="PDPFoundAnErrorFormHandler.typesOfError.specificationSheetOrBrochure"
										value="Specification Sheet or Brochure" />
									<span class="checkbox-label"><fmt:message key="pdp.error.type.brochure" /></span>
								</label>
							</div>
							<div class="checkbox">
								<label> 
									<dsp:input type="checkbox" id="pdp-err-search-nav"
										bean="PDPFoundAnErrorFormHandler.typesOfError.searchOrNavigation"
										value="Search or Navigation" /> 
									<span class="checkbox-label"><fmt:message key="pdp.error.type.search" /> </span>
								</label>
							</div>
							<div class="checkbox">
								<label> 
									<dsp:input type="checkbox" id="pdp-err-pricing" 
										bean="PDPFoundAnErrorFormHandler.typesOfError.pricing"
										value="Pricing" /> 
									<span class="checkbox-label"><fmt:message key="pdp.error.type.pricing" /></span>
								</label>
							</div>
							<div class="checkbox">
								<label>
									<dsp:input type="checkbox" id="pdp-err-others" 
										bean="PDPFoundAnErrorFormHandler.typesOfError.others"
										value="Others" />
									<span class="checkbox-label"><fmt:message key="pdp.error.type.others" /></span>
								</label>
							</div>
						</div>
						<div class="form-group">
                        	<label class="control-label"><fmt:message key="pdp.error.additional.info" /></label>
                        	<label class="control-label pdp-err-modal-info"><fmt:message key="pdp.error.additional.info.max.characters" /></label>
							<dsp:textarea iclass="form-control"
								bean="PDPFoundAnErrorFormHandler.additionalInfo"
								id="pdpErrFoundModalAdditionalInfo"
								maxlength="250" cols="5" rows="4" >
							</dsp:textarea>
						</div>
                        
						<div class="form-group">
							<fmt:message var="firstNamePlaceholder" key="pdp.error.field.firstName" />
							<label class="control-label"><fmt:message key="pdp.error.field.firstName" /></label>
							<dsp:input iclass="form-control" id="pdpErrorModalFirstName" bean="PDPFoundAnErrorFormHandler.name" type="text" maxlength="30" value="${isAnonymousUser ? '' : firstName}">
								<dsp:tagAttribute name="placeholder" value="${firstNamePlaceholder}" />
							</dsp:input>
							<small class="text-danger" style="display: none;" id="pdp-err-message-pdpErrorModalFirstName"></small>
						</div>
						
						<div class="form-group">
							<label class="control-label"><fmt:message key="pdp.error.field.emailAddress" /></label>
							<label class="control-label pdp-err-modal-info"><fmt:message key="pdp.error.field.emailAddress.contact" /></label>
							<fmt:message var="emailAddressPlaceholder" key="pdp.error.field.emailAddress.placeholder" />
							<dsp:input iclass="form-control" id="pdpErrorModalEmailAddress" bean="PDPFoundAnErrorFormHandler.emailAddress" type="text" maxlength="100" value="${isAnonymousUser ? '' : userEmail}">
								<dsp:tagAttribute name="placeholder" value="${emailAddressPlaceholder}" />
							</dsp:input>
							<small class="text-danger" style="display: none;" id="pdp-err-message-pdpErrorModalEmailAddress"></small>
							<%-- <p><span class="pdp-err-modal-required"><fmt:message key="pdp.error.required.field" /></span></p> --%>
						</div>						
						
						<dsp:input type="hidden" bean="PDPFoundAnErrorFormHandler.displayName" value="${displayName}" id="pdpErrFoundModalDisplayName" name="pdpErrFoundModalDisplayName"/>
						<dsp:input type="hidden" bean="PDPFoundAnErrorFormHandler.brandName" value="${brandName}" id="pdpErrFoundModalbrandName" name="pdpErrFoundModalbrandName"/>
						<dsp:input type="hidden" bean="PDPFoundAnErrorFormHandler.vendorItem" value="${vendorItem}" id="pdpErrFoundModalvendorItem" name="pdpErrFoundModalvendorItem"/>
						<dsp:input type="hidden" bean="PDPFoundAnErrorFormHandler.pricesvcUPC" value="${pricesvcUPC}" id="pdpErrFoundModalpricesvcUPC" name="pdpErrFoundModalpricesvcUPC" />
						<dsp:input type="hidden" bean="PDPFoundAnErrorFormHandler.productDescription" value="${productDescription}" id="pdpErrFoundModalproductDescription" name="pdpErrFoundModalproductDescription"/>
						
						<div id="pdp-found-error-form-captcha">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<div id="recaptchaPDPFoundAnError"></div>
									</div>
								</div>
							</div>
						</div>
					
						<fmt:message var="submitPlaceholder" key="pdp.error.button.submit" />
						<dsp:input type="hidden" bean="PDPFoundAnErrorFormHandler.sendPDPError" value="${submitPlaceholder}" />
						<button type="button" id="pdpFoundAnErrModalSubmit" data-event-click-id="pdpFoundAnErrModalSubmit"  class="btn btn-primary">${submitPlaceholder}</button>

						<a href="#" class="btn btn-info" data-dismiss="modal">
							<fmt:message key="pdp.error.button.cancel" />
						</a>
					</div>
				</dsp:form>
			</div>
		</div>
	</div>

	<script type="text/javascript" nonce="${requestScope.nonce}">
		$(document).ready(function() {
			checkSubmitButton();
		});
			
		var pdp_found_error_captcha_success_default = false;
		var pdp_found_error_captcha_default = null;
	
		function checkSubmitButton() {
            //console.log('inside check submit button');
            //console.log('pdp_found_error_captcha_success_default :: ' + pdp_found_error_captcha_success_default);
			var enableLoginButton = true;
			if ($('#pdp-found-error-form-captcha').length > 0) {
				enableLoginButton = pdp_found_error_captcha_success_default;
			}
	
			if (enableLoginButton) {
				$('#pdpFoundAnErrModalSubmit').removeAttr('disabled');
			} else {
				$('#pdpFoundAnErrModalSubmit').attr('disabled', 'disabled');
			}
		}
	
		function renderCaptchaCallback() {
			var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
			pdp_found_error_captcha_default = grecaptcha.render(
				'recaptchaPDPFoundAnError', {
					'sitekey' : sitekey,
					'callback' : validateRecaptchaDefault,
					'expired-callback' : expireRecaptchaDefault
			});
		}
	
		function validateRecaptchaDefault(captchaResponse) {
			pdp_found_error_captcha_success_default = true;
			checkSubmitButton();
		}
	
		function expireRecaptchaDefault() {
            //console.log('inside expireRecaptchaDefault');
            //console.log('pdp_found_error_captcha_success_default b4 :: ' + pdp_found_error_captcha_success_default);
			grecaptcha.reset(pdp_found_error_captcha_default);
			pdp_found_error_captcha_success_default = false;
			checkSubmitButton();
            //console.log('pdp_found_error_captcha_success_default after :: ' + pdp_found_error_captcha_success_default);
		}
		
	</script>

</dsp:page>