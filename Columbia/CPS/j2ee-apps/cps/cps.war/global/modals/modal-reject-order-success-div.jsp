<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	<dsp:form action="" method="post" id="rejectOrders" formid="rejectOrders">
		<dsp:input type="hidden" bean="ApprovalFormHandler.orderIds" value="" id="rejectList"/>


		<!-- Reject orders -->
		<div class="modal-body">
			<h4 class="modal-title" id="modal-accountrejectorder-title"><fmt:message key="account.approval.reject_orders"/></h4>
	
			<p><fmt:message key="account.approval.reject_orders_lb2"/></p>
	
			<div class="form-group">
				<textarea id="rejectMessTA" class="form-control" placeholder="Optional Message (125 Characters Max)" name="" maxlength="125"></textarea>
				<dsp:input type="hidden" bean="ApprovalFormHandler.approverMessage" value="" id="rejectMessage"/>
			</div>
		
			<a href="#" class="btn btn-primary" data-event-click-id="global223" >
				<fmt:message key="account.approval.confirm"/>
			</a>
			<a href="#" class="btn btn-info" data-dismiss="modal">
				<fmt:message key="account.approval.cancel"/>
			</a>
		</div>
		<dsp:input type="hidden" bean="ApprovalFormHandler.rejectOrders" value="true" priority="-10"/>


		<dsp:input type="hidden" bean="ApprovalFormHandler.rejectOrders" value="true" priority="-10"/>
	</dsp:form>

</dsp:page>