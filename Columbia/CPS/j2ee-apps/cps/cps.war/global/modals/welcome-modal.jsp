<dsp:page>
<div class="modal fade in" id="welcomeModal" tabindex="-1" role="dialog" aria-labelledby="welcome" style="padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="bg-gold bg-gold-cont">
						<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">x</span>
						</button>
                        <cp:repositoryMessage key="welcomeModalTop"></cp:repositoryMessage>
					</div>
                        <div class="modal_body">
                       <cp:repositoryMessage key="welcomeModalBottom"></cp:repositoryMessage>					
    					<button type="button" class="btn btn-secondary bg-gold"
						data-dismiss="modal">CONTINUE TO SITE</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>