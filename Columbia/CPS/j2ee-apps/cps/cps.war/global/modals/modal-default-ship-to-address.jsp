<dsp:page>
<div class="modal fade in" id="defaultShipToAddressModal" tabindex="-1" role="dialog" aria-labelledby="welcome" style="padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div class="bg-gold bg-gold-cont">
						<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">x</span>
						</button>
                        <cp:repositoryMessage key="defaultShipToAddressModalTop"></cp:repositoryMessage>
					</div>
                        <div class="modal_body">
                       <cp:repositoryMessage key="defaultShipToAddressModalBottom"></cp:repositoryMessage>
                       <a class="btn btn-secondary bg-gold" href="/account/addresses.jsp">Set Ship to Address</a>					
    					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>