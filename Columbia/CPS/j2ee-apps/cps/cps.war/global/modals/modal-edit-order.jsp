<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties" />
	<dsp:getvalueof var="hideSavedCart" bean="CPSGlobalProperties.hideSavedCart" />

	<%--<dsp:importbean bean="/atg/userprofiling/Profile"/>--%>

	<dsp:getvalueof var="commerceItemCount" bean="ShoppingCart.current.commerceItemCount"/>
	<dsp:getvalueof var="isEmptyCart" value="${commerceItemCount == 0}"/>

	<dsp:getvalueof var="orderId" param="orderId"/>

	<a href="#" data-toggle="modal" data-target="#modalEditOrder${orderId}" style="display: none;"
	   id="link-modalEditOrder${orderId}">
	</a>
	
	<!-- Edit request order -->
	<div class="modal fade in" id="modalEditOrder${orderId}" tabindex="-1" role="dialog" aria-labelledby="modalEditOrder${orderId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
				</div>
				<div class="modal-body">
					<c:choose>
						<c:when test="${isEmptyCart}">
							<h2 class="modal-title"><fmt:message key="account.approval.edit_order"/></h2>
							<p>A NEW Cart will be created with all items and details from this Order Request. You can make edits to this Cart and submit your order. </p>
						</c:when>
						<c:when test="${!isEmptyCart and hideSavedCart}">
						<h2 class="modal-title"><cp:repositoryMessage key="editRejectedCartTitle"></cp:repositoryMessage></h2>
						<cp:repositoryMessage key="editRejectedCart"></cp:repositoryMessage>
						</c:when>
						<c:otherwise>
							<h2 class="modal-title"><fmt:message key="account.approval.edit_order_items_in_cart"/></h2>
							<p>
								There are items in the active Cart. To edit this Order Request, you must first SAVE the active cart.
								<strong>When you SAVE the active Cart, it is MOVED to the Saved Carts page where you can later turn it back into an active Cart.</strong>
							</p>
						</c:otherwise>
					</c:choose>
					<c:choose>
					<c:when test="${!isEmptyCart and hideSavedCart}">
					<a href="/checkout/cart.jsp" class="btn btn-primary">Go to Cart</a>
					</c:when>
					<c:otherwise>
					<a href="#" class="btn btn-primary" data-event-click-id="global207" data-event-click-param0="${vsg_utils:escapeJS(orderId)}" >
						<fmt:message key="account.approval.confirm_and_cart"/>
					</a>
					</c:otherwise>
					</c:choose>
					
					
					<a href="#" class="btn btn-info" data-dismiss="modal">
						<fmt:message key="account.approval.cancel"/>
					</a>
				</div>
			</div>
		</div>
	</div>	
	
</dsp:page>
