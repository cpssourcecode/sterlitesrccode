<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:getvalueof var="email" param="email"/>
	<dsp:getvalueof var='tempP' param='tempP'/>
	<dsp:getvalueof var="e" param="e"/>
	<a href="#" data-toggle="modal" data-target="#activateNewAccount" style="display: none;" id="link-new-user-login"></a>
    <div class="modal fade in" id="activateNewAccount" tabindex="-1" role="dialog" aria-labelledby="modal-accountactivate-title" style="display: block; padding-right: 17px;">
        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="icon icon-user"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-accountactivate-title">Activate New Account</h4>
                    <c:choose>
                        <c:when test="${not empty e}">
                            <dsp:include page="/includes/gadgets/info-message.jsp">
                                <dsp:param name="key" value="${e}"/>
                            </dsp:include>
                        </c:when>
                        <c:otherwise>
                            <div id="error-messages" class="alert alert-danger" style="display:none;"></div>
                          	<c:choose>
                          	<c:when test='${not empty tempP}'>
                          	 <p>Enter the email associated with this account and the the new password.</p>
                          	</c:when>
                          	<c:otherwise>
                          	 <p>Enter the email associated with this account and the temporary password that was sent to you.</p>
                          	</c:otherwise>
                          	</c:choose>
                           
                          
                            <dsp:form action="/global/modal/modal-new-user-login.jsp" method="post"
                                      id="new-user-login-form" formid="new-user-login-form" iclass="form login-form">
                                <div class="form-group">
                                    <label class="control-label sr-only">Email Address</label>
                                    <dsp:input type="email" iclass="form-control input-lg" maxlength="100"
                                               id="email" value="${email}" bean="ProfileFormHandler.value.email">
                                        <dsp:tagAttribute name="placeholder" value="Email Address"/>
                                    </dsp:input>
                                </div>
                                
                                <c:choose>
                          	<c:when test='${ not empty tempP}'>
                          	  <dsp:input type="hidden"  bean="ProfileFormHandler.value.temporaryPassword"  value='${tempP}'/>
                          	</c:when>
                          	<c:otherwise>
                          	 <div class="form-group">
                                    <label class="control-label sr-only">Temporary Password</label>
                                    <dsp:input type="password" autocomplete="new-password" iclass="form-control input-lg" maxlength="15"
                                               id="exampleInputPassword1" bean="ProfileFormHandler.value.temporaryPassword">
                                        <dsp:tagAttribute name="placeholder" value="Temporary Password"/>
                                    </dsp:input>
                                </div>
                          	</c:otherwise>
                          	</c:choose>  
                                <hr>
                                <div class="form-group">
                                    <label class="control-label sr-only">New Password</label>
                                    <dsp:input iclass="form-control input-lg" id="input-password"
                                               bean="ProfileFormHandler.value.password" type="password" autocomplete="new-password" maxlength="15">
                                        <dsp:tagAttribute name="placeholder" value="New Password"/>
                                    </dsp:input>
                                </div>
                                <div class="form-group">
                                    <label class="control-label sr-only">Confirm New Password</label>
                                    <dsp:input iclass="form-control input-lg" id="input-password"
                                               bean="ProfileFormHandler.value.confirmPassword" type="password" autocomplete="new-password" maxlength="15">
                                        <dsp:tagAttribute name="placeholder" value="Confirm New Password"/>
                                    </dsp:input>
                                </div>
                                <div class="form-group collapse profile-password">
                                    <div class="well well-grey" style="padding: 14px; margin: 0px">
                                        <h5>Password rules</h5>
                                        <ul>
                                            <li>Minimum of six and maximum of 15 characters;</li>
                                            <li>Contain at least one uppercase alphabetic character;</li>
                                            <li>Contain at least one numeric character;</li>
                                            <li>Contain at least one special character (examples: !, @, #, $, %, &amp;, *, _).</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="checkbox mb15">
                                    <label>
                                        <dsp:input id="input-rememberMe" bean="ProfileFormHandler.value.rememberMe" value="true" type="checkbox"/> Remember Me
                                    </label>
                                </div>
                                <button class="btn btn-primary btn-lg btn-block mb15" type="button" data-event-click-id="global215" >Log In</button>
                                <dsp:input type="hidden" bean="ProfileFormHandler.newUserResetPassword" value="true"/>
                            </dsp:form>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</dsp:page>