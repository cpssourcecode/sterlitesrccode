<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/droplet/AliasNumberLookup"/>
	<dsp:importbean bean="/cps/droplet/CPSInventoryDroplet"/>
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
	<dsp:getvalueof var="prodId" param="prodId"/>

	<dsp:droplet name="ProductLookup">
		<dsp:param name="id" value="${prodId}"/>
		<dsp:setvalue param="product" paramvalue="element"/>
		<dsp:oparam name="output">
			<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
				<dsp:param name="prodId" value="${prodId}"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
				</dsp:oparam>
			</dsp:droplet>
			<a href="#" data-toggle="modal" data-target="#myModalCheck" style="display: none;" id="link-myModalCheck"></a>

			<!-- Check Availability -->
			<div class="modal fade" id="myModalCheck" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<!-- <span class="modal-icon"><i class="fa fa-cube"></i></span> -->
							<span class="modal-icon"><i class="glyphicon glyphicon-shopping-cart" style="top:5px;"></i></span>
						</div>
						<div class="modal-body">
							<h4 class="modal-title" id="modal-availability-title">Check Availability</h4>
							<dsp:getvalueof var="thumbnail_img" param="product.thumbnail_url"/>
							<dsp:getvalueof var="minimum_order_qty" param="product.minimum_order_qty"/>
							<dsp:getvalueof var="productPurchasable" param="product.productPurchasable"/>
							<dsp:getvalueof var="stockingType" param="product.stockingType"/>
							<dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType)}"/>
							<c:set var="thumbnail_img" value="${empty thumbnail_img ? '/assets/images/plp-placeholder.png' : thumbnail_img}"/>
		
							<div class="row row-narrow">
								<div class="col-xs-3 max-width-100">
									<cp:readerimg  iclass="media-object" src="${thumbnail_img}" alt="..." />
								</div>
								<div class="col-xs-9">
									
									<a href="${productUrl}">
										<h3 class="mt5"><dsp:valueof param="product.description" converter="valueishtml"/></h3>
									</a>
									<p>
										<span>
											<fmt:message key="listing.product.tile.itemnumber"/>&nbsp;<dsp:valueof
													param="product.displayName"/>&nbsp;<br>
											<fmt:message key="listing.product.tile.mfr"/>&nbsp;<dsp:valueof param="product.vendorItem"/>&nbsp;<br>
				
											<dsp:getvalueof var="pricesvc_upc" param="product.pricesvc_upc"/>
											<c:if test="${not empty pricesvc_upc}">
												<fmt:message key="listing.product.tile.upc"/>&nbsp; ${vsg_utils:escapeHtml(pricesvc_upc)}&nbsp;<br>
											</c:if>	
											<c:if test="${!productPurchasable || isProductPurchasableStockingType}">
                                                <br/>
												<i class="fa fa-exclamation-triangle red"></i>
												<fmt:message key="listing.product.notPurchasableOnline"/>
											</c:if>
																						
											<dsp:droplet name="/cps/droplet/AliasNumberLookup">
												<dsp:param name="skuId" value="${prodId}"/>
												<dsp:oparam name="output">
													<fmt:message key="listing.product.tile.customerAlias"/>&nbsp;
													<dsp:getvalueof var="prodAlias" param="alias"/>
                    								<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
													<dsp:valueof param="alias"/>
												</dsp:oparam>
											</dsp:droplet>
			
										</span>
									</p>
								</div>
							</div>
							<hr>
							<form>
								<div class="row row-narrow mb15">
									<div class="col-sm-4">
										<div class="radio">
											<label data-initialize="radio" class="" id="myCustomRadioLabel2">
												<input name="radioEx1" type="radio" value="haveItShip" data-event-change-id="global188" > Have it Shipped
											</label>
										</div>
										<div class="radio checked">
											<label data-initialize="radio" class="active checked" id="myCustomRadioLabel">
												<input checked="checked" name="radioEx1" type="radio" value="pickItUp" data-event-change-id="global189" > Pick it Up
											</label>
										</div>
									</div>
									<div class="col-sm-8 text-right text-left-xs mt15">
										<button class="btn btn-primary check-avail-submit xs-block" type="button" data-product-id="${vsg_utils:escapeHtml(prodId)}" data-event-click-id="global185" >Check Availability</button>
										<button id="dismiss-modal" class="btn btn-info xs-block" data-dismiss="modal">Cancel</button>
									</div>
								</div>
								<p id="haveItShipNoteText" style="display: none"><cp:repositoryMessage key="haveItShippedAvailabity"></cp:repositoryMessage></p>
								<p id="pickItUpNoteText" style="display: block"><cp:repositoryMessage key="pickItUpNote"></cp:repositoryMessage></p>



							</form>
		
							<div class="row" id="only-one-msg">
								<div class="col-lg-12">
									<br>
									<p>
										<span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
										<strong class="text-danger">IMPORTANT:</strong>&nbsp;<strong>ONLY
										ONE</strong> delivery method (<strong>Have it Shipped or Pick it Up
										</strong>) can be selected for all items in a shopping cart.
									</p>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 modal-check-collapse collapse" id="collapseCheck">
								</div>
							<c:if test="${productPurchasable && !isProductPurchasableStockingType}">
								<div id="addToCartButton" style="display: none;">
									<div class="col-sm-2 col-xs-3">
										<input type="text" class="form-control input-lg text-center" id="addToCartFromModalQty"
											   placeholder="Qty" value="${minimum_order_qty}" maxlength="5"
											   data-event-keydown-id="global187" >
									</div>
									<div class="col-sm-10 col-xs-9">
											<button class="btn btn-lg xs-block btn-primary addToCartButton" data-event-click-id="global186" data-event-click-param0="${vsg_utils:escapeJS(prodId)}">
												Add to Cart
											</button>
									</div>
								</div>
							</c:if>
						
								
								<div class="col-lg-12 modal-check-collapse collapse" id="collapsePickItUp">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<a href="#" data-toggle="collapse" data-target="#collapseCheck" style="display: none;" id="link-collapseCheck"></a>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>