<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<dsp:getvalueof var="giftlistId" param="giftlistId"/>

	<a href="#" data-toggle="modal" data-target="#modalDeleteMaterial${giftlistId}" style="display: none;"
	   id="link-modalDeleteMaterial${giftlistId}"></a>


	<!-- Delete List -->
	<div class="modal fade in" id="modalDeleteMaterial${giftlistId}" tabindex="-1" role="dialog" aria-labelledby="modalDeleteMaterial${giftlistId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-matdelete-title"><fmt:message key="account.material.delete_material_list"/></h4>
	
					<p>
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="info_del_confirm"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include>
						<%--Are you sure you want to delete this Material List?--%>
					</p>
	
					<!-- <button class="btn btn-primary">Delete</button>  -->
					<a href="#" class="btn btn-primary" data-event-click-id="global200" data-event-click-param0="${vsg_utils:escapeJS(giftlistId)}" >
						<fmt:message key="global.modal.proceed"/>
					</a>
					<button class="btn btn-info" data-dismiss="modal">Cancel</button>
					<%--<a href="#" class="btn btn-info" data-event-click-id="global201"  data-dismiss="modal">--%>
						<%--<fmt:message key="global.modal.cancel"/>--%>
					<%--</a>--%>
				</div>
			</div>
		</div>
	</div>

</dsp:page>