<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/cps/droplet/UserMaterialListsDroplet"/>
	<dsp:getvalueof var="messageId" param="messageId"/>
	<dsp:getvalueof var="divId" param="divId" />

	<dsp:droplet name="UserMaterialListsDroplet">
		<dsp:param name="term" value="${searchTerm}" />
		<dsp:param name="cs" value="${searchCS}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="giftlists" param="giftlists" />
			<dsp:getvalueof var="emptyResult" param="emptyResult" />

			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="CPSSessionBean.sortableReorderLists" />
				<dsp:param name="elementName" value="giftlist" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="count" param="count" />
					<c:if test="${count==1}">
						<dsp:getvalueof var="id" param="giftlist.id" />
						<dsp:getvalueof var="giftlistItems" param="giftlist.giftlistItems" />
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<h4 class="modal-title" id="modal-matnone-title">Item(s) Added to List</h4>
	<c:choose>
		<c:when test="${fn:length(giftlistItems)==1}">
			<dsp:getvalueof var="messageId" value="mlItemAdded"/>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="messageId" value="${messageId}" />
		</c:otherwise>
	</c:choose>
	<p>${fn:length(giftlistItems)}&nbsp;
		<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
			<dsp:param name="messageKey" value="${messageId}" />
			<dsp:oparam name="output">
				<dsp:valueof param="message" valueishtml="true" />
			</dsp:oparam>
			<dsp:oparam name="empty">
				<dsp:valueof param="message" valueishtml="true" />
			</dsp:oparam>
		</dsp:droplet>
	</p>
	<button class="btn btn-info" data-dismiss="modal">Close</button>					
	<a href="${originatingRequest.contextPath}/account/material-detail.jsp?giftlistId=${id}" class="btn btn-primary" data-event-click-id="global200" data-event-click-param0="${vsg_utils:escapeJS(giftlistId)}" >
		<fmt:message key="global.modal.view_list"/>
	</a>
</dsp:page>
