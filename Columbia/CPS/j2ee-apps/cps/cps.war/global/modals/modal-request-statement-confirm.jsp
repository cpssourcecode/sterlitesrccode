<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/Profile"/>
    <a href="#" data-toggle="modal" data-target="#requestStatementModal" style="display: none;" id="link-requestStatementModal"></a>

    <div class="modal fade in" id="requestStatementModal" tabindex="-1" role="dialog" aria-labelledby="modal-statement-title" style="display: none; padding-right: 17px;">
        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="fa fa-info"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-statement-title">Current Statement Sent</h4>

                    <p>A copy of your current statement will be sent to <dsp:valueof bean="Profile.email"/>.</p>

                    <button class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</dsp:page>