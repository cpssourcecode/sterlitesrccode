<dsp:page>
	<dsp:importbean bean="/cps/util/FileUploadHandler"/>
	<div class="modal fade in" id="modal-usersin" tabindex="-1" role="dialog"
		aria-labelledby="modal-usersin-title" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<dsp:form enctype="multipart/form-data" method="post" action="/account/manage-users.jsp" 
					id="import-users" formid="import-users">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<span class="modal-icon"><i class="fa fa-upload"></i></span>
					</div>
					<div class="modal-body">
						<h4 class="modal-title" id="modal-usersin-title">Import User(s)</h4>
						<p>To add User(s), upload a spreadsheet in .xls format.</p>

						<div class="form-group">
							<dsp:input bean="FileUploadHandler.userFile" name="userFile" id="userFile" iclass="form-control" type="file" />
						</div>

						<div class="row row-flush">
							<div class="col-sm-6 col-sm-push-6 text-right text-left-xs">
								<a href="/document/import_users_template.xlsx" class="btn btn-link"><i class="fa fa-download mr5"></i> Download Template</a>
							</div>
							<div class="col-sm-6 col-sm-pull-6">
									<dsp:input type="hidden" bean="FileUploadHandler.readUserFileSuccessURL" value="/account/manage-users.jsp?import=s"/>
									<dsp:input type="hidden" bean="FileUploadHandler.readUserFile" value="true"/>
									<button type="submit" class="btn btn-primary" id="importUploadFile">Upload</button>
									<a href="#" class="btn btn-info"
										   data-dismiss="modal">Cancel</a>
							</div>
						</div>
					</div>
				</dsp:form>
			</div>
		</div>
	</div>
</dsp:page>