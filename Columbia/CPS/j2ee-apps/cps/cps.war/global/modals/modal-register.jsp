<dsp:page>
    <div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="fa fa-info"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-text-title">We're here to help you do your job..</h4>
                    <p style="padding-top: 20px;"><strong>Register with us now so you can:</strong></p>
                    <ul>
                        <li class="fa fa-check gold" style="margin-left: -40px; padding-right: 10px;"></li>Order quickly<p></p>
                        <li class="fa fa-check gold" style="margin-left: -40px; padding-right: 10px;"></li>Save your favorite products<p></p>
                        <li class="fa fa-check gold" style="margin-left: -40px; padding-right: 10px;"></li>View and manage your orders<p></p>
                    </ul>
                    <div>
                        <dsp:droplet name="/cps/droplet/MessageLookupDroplet">
                            <dsp:param name="id" value="register-call-message"/>
                            <dsp:param name="elementName" value="message"/>
                            <dsp:oparam name="output">
                                <dsp:getvalueof var="message" param="message.message"/>
                                ${message}
                            </dsp:oparam>
                            <dsp:oparam name="empty">
                                <p></p>
                                <%--Message that encourages a user to <a href data-event-click-id="eventGoToRegistrationPage">register</a> if they keep coming to the site--%>
                            </dsp:oparam>
                        </dsp:droplet>
                    </div>
                    <div style="text-align:center; padding-top: 20px;">
                        <a href data-event-click-id="eventGoToRegistrationPage"><button class="btn btn-primary">Register Now</button></a>
                        <p style="padding: 10px;"><a href="" class="modal-open" data-dismiss="modal" style="font-size: 7pt; text-decoration: none;"><strong>No thanks, maybe later.</strong></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</dsp:page>