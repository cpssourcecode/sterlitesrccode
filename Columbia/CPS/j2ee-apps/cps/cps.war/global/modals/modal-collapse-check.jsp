<%@ include file="/includes/utils/taglibs.jspf" %>  
<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" var="profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	<dsp:importbean bean="/cps/droplet/CPSInventoryDroplet"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>

	<dsp:getvalueof var="prodId" param="prodId"/>
	<dsp:getvalueof var="qty" param="qty"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:getvalueof var="leadTimeThreshold" bean="CPSInventoryDroplet.leadTimeThreshold"/>
    <dsp:getvalueof var="commerceItemCount" bean="ShoppingCart.current.commerceItemCount"/>
    <dsp:getvalueof var="isEmptyCart" value="${commerceItemCount == 0}"/>
    <dsp:getvalueof var="defaultBranch" bean="Profile.parentOrganization.billingAddress.businessUnit"/>
    <dsp:getvalueof var="cartSelectedStore" bean="Profile.cartSelectedStore"/>
    
   
    
	<c:if test="${empty qty}">
		<dsp:getvalueof var="qty" value="1"/>
	</c:if>

	<c:if test="${not isTransient}">
		<dsp:tomap var="profile" bean="Profile.dataSource"/>
		<dsp:droplet name="/atg/dynamo/droplet/IsNull">
			<dsp:param name="value" value="${profile.selectedOrg}"/>
			<dsp:oparam name="true">
				<dsp:getvalueof var="customerId" bean="Profile.parentOrganization.id"/>
			</dsp:oparam>
			<dsp:oparam name="false">
				<dsp:getvalueof var="customerId" bean="Profile.selectedOrg.id"/>
			</dsp:oparam>
		</dsp:droplet>
		<dsp:getvalueof var="quantityAvailable" param="quantityAvailable"/>
	</c:if>
	
	<dsp:getvalueof var="haveItShipped" param="haveItShipped"/>
	
	<c:choose>
		<c:when test="${haveItShipped}">
			<dsp:droplet name="/cps/droplet/CPSInventoryDroplet">
				<dsp:param name="productId" value="${prodId}"/>
				<dsp:param name="qty" value="${qty}"/>
				<dsp:param name="customerId" value="${customerId}"/>
				<dsp:param name="isEmptyCart" value="${isEmptyCart}"/>
				<dsp:oparam name="output">
		
					<dsp:getvalueof var="quantityAvailable" param="quantityAvailable"/>
					<dsp:getvalueof var="leadTime" param="leadTime"/>
					<dsp:getvalueof var="defaultBranchQuantity" param="defaultBranchQuantity"/>
					<%--<input type="text" class="form-control input-lg text-center" id="" placeholder="Qty"  value="${quantityAvailable}">--%>
				</dsp:oparam>
				<dsp:oparam name="error">
					<dsp:getvalueof var="errorMessage" param="errorMessage"/>
				</dsp:oparam>
			</dsp:droplet>
			
			<div class="check-avail-detail" id="haveItShippedInfo">
				<c:if test="${empty errorMessage}">
					<c:choose>
						<c:when test="${quantityAvailable > 0}">
								<p class="qty-available">Qty Available</p>
								<div class="qty-info">
								<p>Now : ${defaultBranchQuantity} Unit(s)</p>
								<p>Within Days : ${quantityAvailable - defaultBranchQuantity} Unit(s)</p>
								</div>
						</c:when>
						<c:otherwise>
							<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
								<dsp:param name="id" value="modal-ships-lead-time"/>
								<dsp:param name="elementName" value="message"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="message" param="message.message"/>
								</dsp:oparam>
							</dsp:droplet>
								<p class="qty-available">Qty Available</p> 
								<div class="qty-info">
								<p>Now : ${defaultBranchQuantity} Unit(s)</p>
								<%-- <p>${fn:replace(message,"{0}",leadTime)}</p> --%>
								<p>Within Days : ${quantityAvailable} Unit(s)</p>
								</div>
							
							<c:if test="${quantityAvailable eq 0 && defaultBranchQuantity eq 0 }">
								<p><cp:repositoryMessage key="call-sales-team" /> </p>
							</c:if>
 						</c:otherwise> 
					</c:choose> 
				</c:if>
				<c:if test="${not empty errorMessage}">
					<span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
					<strong class="text-danger">${errorMessage}</strong>
				</c:if>
				<br>
				<p id="qty-important">
					<span class="glyphicon glyphicon-alert text-danger"></span>&nbsp; <strong
						class="text-danger">IMPORTANT:</strong>&nbsp;Quantity added to your
					cart can <strong>EXCEED</strong> quantity available shown. Exceeded
					quantities will be placed on backorder. You can contact your Sales
					Team for information about backorder items.
				</p>
			</div>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="/cps/droplet/GetDistancesDroplet">
				<dsp:oparam name="output">
					<dsp:getvalueof var="distances" param="distances"/>
				</dsp:oparam>
			</dsp:droplet>
			<div class="check-avail-detail" id="pickItUpInfo">
			<hr>
				<form>
					<div class="form-group">
						<dsp:droplet name="/cps/droplet/CPSInventoryDroplet">
							<dsp:param name="productId" value="${prodId}"/>
							<dsp:param name="qty" value="${qty}"/>
							<dsp:param name="isRequestCurrentStore" value="true"/>
							<dsp:param name="isEmptyCart" value="${isEmptyCart}"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="infos" param="inventoryAvailabilityInfos"/>
							</dsp:oparam>
						</dsp:droplet>
						<c:forEach items="${infos}" var="info">
							<c:if test="${info.branchDefault}">
								<dsp:getvalueof var="defaultInfo" value="${info}" />
							</c:if>
						</c:forEach>
						<h4>Pick it Up</h4>
						<br>
						<p>
							<span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
							<strong class="text-danger">IMPORTANT:</strong>&nbsp;Quantity
							added to your cart can <strong>EXCEED</strong> quantity
							available shown. Exceeded quantities will be placed on
							backorder. You can contact your Sales Team for information about
							backorder items.
						</p>
						<h5>Default Pickup Location</h5>
						<section>

							<dsp:getvalueof var="defaultBranchId" value="${defaultInfo.branchId}" />
										
							<dsp:include src="gadgets/default-location-avail-info.jsp">
								<dsp:param name="info" value="${defaultInfo}"/>
								<dsp:param name="index" value="-1"/>
								<dsp:param name="distances" value="${distances}"/>
								<dsp:param name="qty" value="${qty}"/>
								<dsp:param name="productId" value="${prodId}"/>
							</dsp:include>
									
						</section>
						<hr>
						<h5>Other Pickup Locations</h5>
						<div class="scroll">
							<c:forEach var="info" items="${infos}" varStatus="index">
								<c:if test="${defaultBranchId ne info.branchId}">
									<section>
										<dsp:include src="gadgets/locations-avail-info.jsp">
											<dsp:param name="info" value="${info}"/>
											<dsp:param name="index" value="${index.index}"/>
											<dsp:param name="distances" value="${distances}"/>
											<dsp:param name="qty" value="${qty}"/>
											<dsp:param name="productId" value="${prodId}"/>
										</dsp:include>
									</section>
								</c:if>
							</c:forEach>
						</div>
					</div>
				</form>
			</div>
		</c:otherwise>
	</c:choose>
</dsp:page>