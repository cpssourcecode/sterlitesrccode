<dsp:page>
	<div class="modal fade" id="permissionDenied" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-notallowed-title">Permission Denied</h4>
	
					<p>Sorry, you do not currently have privileges for this action. Contact the Administrator for more information.</p>
	
					<button class="btn btn-info" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</dsp:page>