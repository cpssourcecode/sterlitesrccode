<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	<%--<dsp:importbean bean="/atg/userprofiling/Profile"/>--%>

	<dsp:getvalueof var="orderId" param="orderId"/>

	<a href="#" data-toggle="modal" data-target="#modalResubmitOrder${orderId}" style="display: none;"
	   id="link-modalResubmitOrder${orderId}">
	</a>
	
	<!-- Resubmition request order -->
	<div class="modal fade in" id="modalResubmitOrder${orderId}" tabindex="-1" role="dialog" aria-labelledby="modalResubmitOrder${orderId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
				</div>
				<div class="modal-body">
					<h2 class="modal-title" style="font-size: 25px;"><fmt:message key="account.approval.resubmit_order"/></h2>
					<p>Are you sure you want to resubmit this Order Request?</p>
	
					<a href="#" class="btn btn-primary" data-event-click-id="global225" data-event-click-param0="${vsg_utils:escapeJS(orderId)}" >
						<fmt:message key="account.approval.confirm"/>
					</a>
					<a href="#" class="btn btn-info" data-dismiss="modal">
						<fmt:message key="account.approval.cancel"/>
					</a>
				</div>
			</div>
		</div>
	</div>		


</dsp:page>
