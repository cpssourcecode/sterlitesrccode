<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	<%--<dsp:importbean bean="/atg/userprofiling/Profile"/>--%>

	<dsp:getvalueof var="orderId" param="orderId"/>

	<a href="#" data-toggle="modal" data-target="#modalApproveSelectedPendingOrder${orderId}" style="display: none;"
	   id="link-modalApproveSelectedPendingOrder${orderId}">
	</a>

	<!-- Approve current order -->
	<div class="modal fade in" id="modalApproveSelectedPendingOrder${orderId}" tabindex="-1" role="dialog" 
			aria-labelledby="modalApproveSelectedPendingOrder${orderId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-confirm-title"><fmt:message key="account.approval.approve_order"/></h4>
	
					<p>Are you sure you want to approve this order request? </p>
	
					<a href="#" class="btn btn-primary" id="modalBtnApprove" data-event-click-param0="${vsg_utils:escapeJS(orderId)}" >
						<%-- <fmt:message key="account.approval.confirm"/> --%>
						Proceed						
					</a>
					<a href="#" class="btn btn-info" data-dismiss="modal">
						<fmt:message key="account.approval.cancel"/>
					</a>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
