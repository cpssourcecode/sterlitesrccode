<dsp:page>
	<a href="#" data-toggle="modal" data-target="#resendInviteSuccess" style="display: none;" id="link-resendInviteSuccess"></a>
    <div class="modal fade in" id="resendInviteSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none; padding-right: 17px;">
        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="fa fa-info"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-userreinvite-title">Reinvite User</h4>

                    <p>An email with temporary User ID and Password information has been sent to this User.</p>

                    <button class="btn btn-info" data-dismiss="modal">Okay</button>
                </div>
            </div>
        </div>
    </div>
</dsp:page>