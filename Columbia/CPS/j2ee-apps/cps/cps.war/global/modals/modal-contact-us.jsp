<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<a href="#" data-toggle="modal" data-target="#contactUsModal" style="display: none;" id="link-contactUsModal"></a>
    <div class="modal fade in" id="contactUsModal" tabindex="-1" role="dialog" aria-labelledby="modal-text-title" style="display: none; padding-right: 17px;">
        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="fa fa-info"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-text-title">Your request is accepted.</h4>

                    <p>Thank you for Contacting Us, We will get back to you shortly.</p>

                    <button class="btn btn-info" data-dismiss="modal" data-event-click-id="global190" >Okay</button>
                </div>
            </div>
        </div>
    </div>
</dsp:page>
