<dsp:page>
    <!-- Order Approval -->
    <dsp:importbean bean="/cps/droplet/RoleLookupDroplet" />
    <dsp:importbean bean="/cps/droplet/SavedCartsDroplet" />
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="firstLogin" bean="Profile.firstLogin"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:importbean var="msgutils" bean="/vsg/util/MessageUtils"/>
    <c:if test="${(not isTransient) and firstLogin}">
        <dsp:droplet name="/atg/targeting/RepositoryLookup">
            <dsp:param name="repository" bean="/atg/userprofiling/ProfileAdapterRepository"/>
            <dsp:param name="id" bean="Profile.id" />
            <dsp:oparam name="output">
                <dsp:droplet name="/cps/droplet/RoleLookupDroplet">
                    <dsp:param name="userId" param="id"/>
                    <dsp:param name="role" value="buyer"/>
                    <dsp:oparam name="true">
                        <dsp:getvalueof var="pendingRequests" value="true"/>
                    </dsp:oparam>
                </dsp:droplet>
                <dsp:droplet name="/cps/droplet/RoleLookupDroplet">
                    <dsp:param name="userId" param="id"/>
                    <dsp:param name="role" value="approver"/>
                    <dsp:oparam name="true">
                        <dsp:getvalueof var="pendingApprovals" value="true"/>
                    </dsp:oparam>
                </dsp:droplet>
            </dsp:oparam>
        </dsp:droplet>
        <c:if test="${pendingApprovals || pendingRequests}">
            <dsp:getvalueof var="ordersSize" value="${0}"/>
            <dsp:droplet name="/cps/droplet/OrderApprovalsDroplet">
                <dsp:param name="profile" bean="Profile" />
                <dsp:oparam name="output">
                    <dsp:getvalueof var="ordersSize" param="ordersSize"/>
                </dsp:oparam>
            </dsp:droplet>
            <dsp:getvalueof var="savedCartsCount" value="${0}"/>
            <dsp:droplet name="/cps/droplet/SavedCartsDroplet">
                <dsp:param name="profile" bean="Profile" />
                <dsp:oparam name="output">
                    <dsp:getvalueof var="savedCartsCount" param="ordersSize"/>
                </dsp:oparam>
            </dsp:droplet>
            <c:if test="${ordersSize > 0 || savedCartsCount > 0}">
                <dsp:setvalue bean="Profile.firstLogin" value="false"/>
                <div class="modal fade" id="orderApproval" tabindex="-1" role="dialog"
                     aria-labelledby="sharePageModalTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <span class="modal-icon"><i class="fa fa-info"></i></span>
                            </div>
                            <c:if test="${ordersSize > 0 && pendingApprovals}">
                                <div class="modal-body">
                                    <h4 class="modal-title">${vsg_utils:prepareMessage(msgutils, 'Pending order approvals', null)}
                                        <a href="/account/pending-approvals.jsp">
                                                <%--<i class="icon icon-folder-motion"></i>--%>
                                            Pending Orders (<dsp:valueof value="${ordersSize}"/>)
                                        </a>
                                    </h4>
                                </div>
                            </c:if>
                            <c:if test="${savedCartsCount > 0 && pendingRequests}">
                                <div class="modal-body">
                                    <h4 class="modal-title">${vsg_utils:prepareMessage(msgutils, 'Saved carts', null)}
<%--                                          <a href="/account/order-carts.jsp">
                                             <i class="icon icon-folder-motion"></i>
                                            Saved Carts (<dsp:valueof value="${savedCartsCount}"/>) 
                                        </a> --%>
                                    </h4>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <script nonce="${requestScope.nonce}">
	                <c:if test="${(ordersSize > 0 && pendingApprovals) || (savedCartsCount > 0 && pendingRequests)}">
	                    $('#orderApproval').modal('show');
	                </c:if>    
                </script>
            </c:if>
        </c:if>
    </c:if>
</dsp:page>