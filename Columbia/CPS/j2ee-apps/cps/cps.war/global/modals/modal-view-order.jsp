<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/cps/commerce/order/ApproverOrderLookup"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>

	<dsp:getvalueof var="orderId" param="orderId"/>
	<dsp:getvalueof var="page" param="page" />
	<input type="hidden" value="${orderId}" id="hiddenOrderId"/>
	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-approve-order.jsp?orderId=${orderId}"/>
	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-reject-order.jsp?orderId=${orderId}"/>		
	<a href="#" data-toggle="modal" data-target="#modalViewOrder${orderId}" style="display: none;"
	   id="link-modalViewOrder${orderId}">
	</a>
	
 	<div class="modal fade in modalViewOrder" id="modalViewOrder${orderId}" tabindex="-1" role="dialog" 
 			aria-labelledby="modalViewOrder${orderId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog" role="document" style="display: flex;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
				</div>
				<div class="modal-body" id="modal_body">
					<h4 class="modal-title" id="modal-order-title">Order Details</h4>
					<%-- <dsp:getvalueof var="order" bean="/atg/commerce/ShoppingCart.last" /> --%>
					<dsp:form action="${originatingRequest.requestURI}" method="post" formid="updateOrderDetailsForm" id="updateOrderDetailsForm">
					<dsp:droplet name="ApproverOrderLookup">
						<dsp:param name="orderId" param="orderId"/>
						<dsp:oparam name="output">
							<dsp:setvalue param="order" paramvalue="result"/>
							<dsp:getvalueof var="jobNumberOrName" param="order.jobName"/>
	
							<div class="row row-narrow">
								<div class="col-sm-8">
									<!-- Addresses -->
									<div class="well">
										<div class="row row-narrow">
											<!-- Shipping address -->											
											<div class="col-sm-6">
												<dsp:getvalueof var="shippingGroup" param="order.shippingGroups[0]"/>
												<c:choose>
													<c:when test="${shippingGroup.shippingGroupClassType == 'inStorePickupShippingGroup'}">
														<h4 class="text-nocase">Pick up in store</h4>
														<p>													
															<dsp:tomap var="store" value="${shippingGroup.store}"/>
															<address>
																<c:if test="${!empty store}">
																	${store.address1}<br/>
																	<c:if test="${!empty store.address2}">
																		${store.address2}<br/>
																	</c:if>	
																	${store.city},&nbsp;${store.stateAddress}&nbsp;${store.postalCode}<br/>
																</c:if>
															</address>
														</p>
													</c:when>
													<c:otherwise>
														<h4 class="text-nocase">Ship to Address</h4>
														<p>
															<dsp:getvalueof var="shippingAddress" value="${shippingGroup.shippingAddress}"/>
															<address>
																<c:if test="${!empty shippingAddress}">
																	${shippingAddress.address1}<br/>
																	<c:if test="${!empty shippingAddress.address2}">
																		${shippingAddress.address2}<br/>
																	</c:if>
																	${shippingAddress.city},&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}<br/>
																</c:if>
															</address>
														</p>
													</c:otherwise>
												</c:choose>
											
											</div>
											<!-- Billing address -->
											<div class="col-sm-6">
												<h4 class="text-nocase">Billing Address</h4>
												<dsp:getvalueof var="paymentGroup" param="order.paymentGroups[0]"/>
												<dsp:getvalueof var="billingAddress" value="${paymentGroup.billingAddress}"/>
												<p>
													<address>	
														<c:if test="${not empty billingAddress}">
															<p>
																<c:if test="${!empty billingAddress.companyName}">${billingAddress.companyName}<br/></c:if>
																${billingAddress.address1}<br/>
																<c:if test="${!empty billingAddress.address2}">
																	${billingAddress.address2}<br/>
																</c:if>
																${billingAddress.city},&nbsp;${billingAddress.state}&nbsp;${billingAddress.postalCode}<br/>
															</p>
														</c:if>
													</address>
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4">
									<div class="well well-grey">
										<h4 class="text-nocase">Summary</h4>
										<dl class="row row-narrow">
											<dt class="col-sm-12 col-xs-6">PO #</dt>
											<dd id="textWrap" class="col-sm-12 col-xs-12">
												<c:if test="${paymentGroup.paymentGroupClassType eq 'invoiceRequest'}">
													<dsp:getvalueof var="PONumber" value="${paymentGroup.PONumber}" />
													<c:choose>
														<c:when test="${page eq 'account' }">
															<label class="viewOrderDetails" style="display: block;"><dsp:valueof value="${paymentGroup.PONumber}"/></label>
															<div class="form-group editOrderDetails" style="display: none;">
																<dsp:input iclass="form-control" id="approvedPONumber" bean="ApprovalFormHandler.poNumber" type="text" maxlength="30" value="${paymentGroup.PONumber}">
																	<dsp:tagAttribute name="placeholder" value="Enter PO #" />
																</dsp:input>
																<small class="text-danger" style="display: none;" id="err-message-approvedPONumber"></small>
															</div>															
														</c:when>
														<c:otherwise>
															<dsp:valueof value="${paymentGroup.PONumber}"/>
														</c:otherwise>
													</c:choose>
												</c:if>												
											</dd>
											
											<c:choose>
												<c:when test="${page eq 'account' }">
													<dt class="col-sm-12 col-xs-9">Job Name/Number</dt>
													<dd id="textWrap" class="col-sm-12 col-xs-12">
														<label class="viewOrderDetails" style="display: block;">${jobNumberOrName}</label>
														<div class="form-group editOrderDetails" style="display: none;">
															<dsp:input iclass="form-control" id="approvedJobName" bean="ApprovalFormHandler.jobName" type="text" maxlength="30" value="${jobNumberOrName}">
																<dsp:tagAttribute name="placeholder" value="Job Name/Number" />
															</dsp:input>
															<small class="text-danger" style="display: none;" id="err-message-approvedJobName"></small>
														</div>
													</dd>												
												</c:when>
												<c:otherwise>
													<c:if test="${not empty jobNumberOrName}">
														<dt class="col-sm-12 col-xs-9">Job Name/Number</dt>
														<dd id="textWrap" class="col-sm-12 col-xs-9">${jobNumberOrName}</dd>
													</c:if>
												</c:otherwise>
											</c:choose>
											
											<dsp:getvalueof var="orderItems" param="order.commerceItems"/>
											<dt class="col-sm-12 col-xs-3"># Items</dt>
											<dd class="col-sm-12 col-xs-9">${fn:length(orderItems)}</dd>
											
											<dt class="col-sm-12 col-xs-3">Subtotal</dt>
											<dd class="col-sm-12 col-xs-9"><dsp:valueof param="order.priceInfo.rawSubtotal" converter="currencyConversion"/></dd>
										</dl>
									</div>
								</div>
							</div>
							
							<table class="table table-striped table-mobile" id="order-tbl">
								<thead>
									<tr>
										<th style="width: 14.1%">&nbsp;</th>
										<th style="width: 18.5%">Item Name</th>
										<th style="word-break: inherit; width: 15.4%" id="sort-item">
											<a href="#">Item # <i id="sort-item-i" class="fa fa-sort"></i></a>
										</th>
										<th class="text-center" style="width: 8%">Qty</th>
										<th style="width: 13.1%" id="sort-price">
											<a href="#">Price <i id="sort-price-i" class="fa fa-sort"></i></a>
										</th>
										<th style="width: 12.1%">Total</th>
										<th class="text-center" style="width: 17.8%">Availability</th>
									</tr>
								</thead>
							 	<tbody>
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="order.commerceItems"/>
										<dsp:param name="elementName" value="item"/>
										<dsp:oparam name="output">
											<tr>
												<dsp:tomap var="productMap" param="item.auxiliaryData.productRef"/>
												<dsp:tomap var="commerceItem" param="item"/>
												<dsp:getvalueof var="skuId" value="${commerceItem.catalogRefId}"/>
												<dsp:getvalueof var="eCommerceDisplay" param="item.auxiliaryData.productRef.parentCategory.eCommerceDisplay"/>
												<dsp:getvalueof var="imageUrl" vartype="java.lang.String"
																value="${empty productMap.thumbnail_url ? '/assets/images/plp-placeholder.png' : productMap.thumbnail_url}"/>
												<%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String"
																value="${originatingRequest.contextPath}/catalog/pdp.jsp?prodId=${skuId}"/> --%>
												<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
													<dsp:param name="prodId" value="${productMap.id}"/>
													<dsp:oparam name="output">
														<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
													</dsp:oparam>
												</dsp:droplet>
		
												<td class="text-center">
													<cp:readerimg src="${imageUrl}" alt="" style="max-height: 60px; max-width: 60px"/>
												</td>
												<td><span class="caption">Item Name</span>
													<dsp:valueof value="${productMap.description}" converter="valueishtml"/>
												</td>
												<td style="word-break: break-all;"><span class="caption">Item #</span>
													<dsp:valueof value="${productMap.displayName}"/>
												</td>
												<td class="text-center text-left-xs"><span class="caption">Qty</span>
													<dsp:valueof value="${commerceItem.quantity}"/>
												</td>
												<td>
													<span class="caption">Price</span>
													<dsp:valueof value="${commerceItem.priceInfo.listPrice}" converter="currencyConversion"/>
												</td>
												<td>
													<span class="caption">Total</span>
													<dsp:valueof value="${commerceItem.priceInfo.amount}" converter="currencyConversion"/>
												</td>												
												<td class="text-center text-left-xs" data-event-click-id="global237" data-event-click-param0="${eCommerceDisplay}" data-event-click-param1="${skuId}" >
													<span class="caption">Availability</span>
                                                </td>
											 </tr>
										</dsp:oparam>
									</dsp:droplet>
								</tbody>
							</table>
						</dsp:oparam>
					</dsp:droplet>
										
					<p>
						<c:if test="${page eq 'account'}">
							<button type="button" id="btnApproveOrderDetails" class="btn btn-primary" data-event-click-id="btnApproveOrderDetails" data-order-id="${orderId}">Approve</button>
							<button type="button" id="btnEditOrderDetails" class="btn btn-primary">Edit</button>
							<button type="button" id="btnUpdateOrderDetails" data-event-click-id="btnUpdateOrderDetails" data-orderId="${orderId}" class="btn btn-primary" style="display: none;">Save for later</button>
							<button type="button" id="btnRejectOrderDetails" class="btn btn-primary" data-event-click-id="btnRejectOrderDetails" data-order-id="${orderId}">Reject</button>
						</c:if>
						<a href="#" class="btn btn-info" data-dismiss="modal">Close</a>
					</p>
						<dsp:input type="hidden" bean="ApprovalFormHandler.orderId" value="${orderId}" id="approval-orderId" />						
						<dsp:input type="hidden" bean="ApprovalFormHandler.updateOrderDetails" value="true" priority="-10"/>
					</dsp:form>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
