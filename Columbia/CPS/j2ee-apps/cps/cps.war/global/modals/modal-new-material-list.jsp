<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/userprofiling/MaterialListHandler"/>
    <dsp:importbean bean="/cps/util/FileUploadHandler"/>


    <a href="#" data-toggle="modal" data-target="#newMaterialListModal" style="display: none;" id="link-newMaterialListModal"></a>

    <!-- New material list -->
    <div class="modal fade in" id="newMaterialListModal" tabindex="-1" role="dialog" aria-labelledby="newMaterialListModal" style="display: none; padding-right: 17px;">
        <div class="modal-dialog modal-narrow" role="document" id="newMaterialListDocument">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="closeNewMLModal" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
                    <span class="modal-icon"><i class="icon icon-user-material"></i></span>
                </div>
                <div class="modal-body">
                <div class="row">
	                <div class=" col-sm-12 col-xs-12 col-container" id="newMaterialListLeft">
	                	<div class="container preload-cover">
							<div class="loading preload-spinner custom-loading">
								<i class="fa fa-spinner fa-spin"></i>
							</div>
				   		</div>
	                    <h4 class="modal-title" id="modal-matlist-title"><fmt:message key="account.material.create_new_material_list"/></h4>
						<dsp:getvalueof var="emptyFileErrorMessage" bean="FileUploadHandler.emptyFileErrorMessage" />
						<input type="hidden" id="emptyFileErrorMessage" value="${emptyFileErrorMessage}">
	                    <div id="newMLFileError" style="display:none;" class="alert alert-danger errors">
	                        <dsp:include page="/includes/gadgets/info-message.jsp">
	                            <dsp:param name="key" value="err_incorrect_file"/>
	                            <dsp:param name="notWrap" value="true"/>
	                        </dsp:include>
	                    </div>
						<%-- <div id="newMLEmptyFileError" style="display:none;" class="alert alert-danger errors">
	                        <dsp:include page="/includes/gadgets/info-message.jsp">
	                            <dsp:param name="key" value="err_empty_file"/>
	                            <dsp:param name="notWrap" value="true"/>
	                        </dsp:include>
	                    </div> --%>
	                    <div style="display: none;" id="modal-message-error-div">
	                        <div class="alert alert-danger">
	                            <div id="modal-message-error"></div>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <input type="text" class="form-control" id="control-ml-name"
	                               maxlength="50" placeholder="Name"/>
	                    </div>
	                    <div class="form-group">
							<textarea id="control-ml-description" class="form-control" cols="4"
	                                  rows="5" maxlength="125" placeholder="Description"></textarea>
	                    </div>
	
	                    <dsp:getvalueof var="formExceptions" vartype="java.lang.Object"
	                                    bean="FileUploadHandler.formExceptions"/>
	
	                    <c:if test="${importModalError && not empty formExceptions}">
	                        <div class="alert alert-danger errors">
	                            <p>
	                                <c:forEach var="formException" items="${formExceptions}">
	                                    <dsp:param name="formException" value="${formException}"/>
	                                    <c:if test="${formException.message != ''}">
	                                        <dsp:valueof param="formException.message" valueishtml="true"/><br/>
	                                    </c:if>
	                                </c:forEach>
	                            </p>
	                        </div>
	                    </c:if>
	
	                    <dsp:form enctype="multipart/form-data" name="createListFromFile" id="createListFromFile" method="post">
	                        <div class="form-group">
	                            <div class="fileinput-custom box">
	                                <dsp:input iclass="inputfile inputfile-6" name="importFile" id="importFile" type="file"
	                                           bean="FileUploadHandler.importFile"/>
	                                <label for="importFile">
	                                    <span id="material-list-file"></span>
	                                    <strong>
	                                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17"
	                                             viewBox="0 0 20 17">
	                                            <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
	                                        </svg>
	                                    </strong>
	                                </label>
	
	                                <%-- <a class="download-template-link" href="/document/MaterialListTemplate.xlsx">
	                                    <br>
	                                    <fmt:message key="global.modal.download_template"/>
	                                </a> --%>
	                                <br>
	                                <a class="importHelpNML green-link" >
										<dsp:include page="/includes/gadgets/info-message.jsp">
											<dsp:param name="key" value="infoHowCanIImportFile"/>
											<dsp:param name="notWrap" value="true"/>
										</dsp:include>
									</a><br>
	
	                                <dsp:input type="hidden" bean="FileUploadHandler.listName" id="listNameFromFile"/>
	                                <dsp:input type="hidden" bean="FileUploadHandler.listDescription" id="listDescriptionFromFile"/>
	                                <dsp:input type="hidden" bean="FileUploadHandler.readFileSuccessURL" value="/account/material-lists.jsp"/>
	                                <dsp:input type="hidden" bean="FileUploadHandler.readFileErrorURL" value="/account/material-lists.jsp?importModalError=true"/>
	                                <dsp:input type="hidden" bean="FileUploadHandler.giftlistId" value="${giftlistId}"/>
	                                <dsp:getvalueof var="ca" param="ca"/>
	                                <c:if test="${ca}">
	                                    <dsp:input type="hidden" bean="FileUploadHandler.giftlistCheckAvailability" value="true"/>
	                                </c:if>
	                                <dsp:input type="hidden" bean="FileUploadHandler.createMaterialListFromFile" value="true" priority="-10"/>
	                            </div>
	                        </div>
	                    </dsp:form>
	
	                    <dsp:form action="${originatingRequest.contextPath}/global/modals/modal-new-material-list.jsp.jsp?modal=true" method="post" id="createList">
	                        <dsp:input type="hidden" bean="MaterialListHandler.eventName" id="eventName"/>
	                        <dsp:input type="hidden" bean="MaterialListHandler.description" id="description"/>
	                        <dsp:input type="hidden" bean="MaterialListHandler.saveGiftlist" value="true" priority="-10"/>
	                    </dsp:form>
	
	                    <br>
	                    <a href="#" id="submitNewMaterialListForm" class="btn btn-primary" data-event-click-id="global214">
	                        Save
	                    </a>
	                    <a href="#" class="btn btn-info" data-dismiss="modal">
	                        <fmt:message key="global.modal.cancel"/>
	                    </a>
	                </div>  
	                
	                
	                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-container p15 well collapse green-text" id="importHelpNML">
	                	<span>
	                		<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="infoImportFileInstruction"/>
								<dsp:param name="notWrap" value="true"/>
							</dsp:include>
							<p>
								<span class="pull-right importHelpNML green-link">CLOSE</span>&nbsp;
							</p>
	                	</span>
	                </div>
                </div>
                
                </div>
            </div>
        </div>
    </div>

</dsp:page>