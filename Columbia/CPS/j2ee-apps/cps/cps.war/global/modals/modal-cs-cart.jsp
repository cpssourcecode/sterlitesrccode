<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>

	<a href="#" data-toggle="modal" data-target="#selectShipAddressCart" style="display: none;" id="link-selectShipAddressCart"></a>


	<div class="modal fade" id="selectShipAddressCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-page-name='modal-cs-cart.jsp'>
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-truck"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title">Change Ship To Address</h4>

					<form class="form">
						<p><strong>For this session</strong>, confirm your default Ship To Address or choose an
							alternate Ship To Address from the list below. </p>

						<dsp:droplet name="CartAddressDroplet">
							<dsp:param bean="ShoppingCart.current" name="order"/>
							<dsp:param bean="Profile" name="profile"/>
							<dsp:param value="cart" name="page"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="address" param="address"/>
								<dsp:getvalueof var="csId" param="addressId"/>
							</dsp:oparam>
						</dsp:droplet>


						<c:if test="${not empty address}">
							<h5>Your Default Ship To Address</h5>

							<div class="radio checked">
								<label class="active" data-initialize="radio" name="addressSelectRadio" id="myCustomRadioLabel"
									   value="${csId}" data-event-click-id="global192" >
									<input type="radio" name="shipto" checked >
									<dsp:include
											page="/checkout/gadgets/cart-ship-to-address.jsp">
										<dsp:param name="addressId" value="${csId}"/>
										<dsp:param name="withoutForm" value="${true}"/>
										<%--<dsp:param name="address" value="${address}"/>--%>
									</dsp:include>
								</label>
							</div>
						</c:if>

						<hr>

						<h5>Select an Alternate Ship To Address for this Session</h5>

						<div class="alert alert-icon">
							<i class="fa fa-exclamation-triangle red"></i>
							<strong>Changing your Ship To Address may affect prices and availability of items in your cart.</strong>
						</div>

						<div class="row row-narrow mb20">
							<div class="col-sm-9 mb5">

								<div class="input-group input-group-lg">
									<input type="text" class="form-control input-lg" id="modal-cs-cart-input-search" placeholder="Search for Ship To Address">
									<div class="input-group-btn">
										<a href="#" type="button" class="btn" title="Search" data-event-click-id="global193" ><i class="fa fa-search"></i></a>
									</div>
								</div>
							</div>
							<div class="col-sm-3"><a href="#" class="btn btn-info btn-block btn-lg" data-event-click-id="global194" >Reset</a>
							</div>
						</div>

						<div id="modal-cs-cart-list-content" class="form-group">
							<dsp:include page="gadgets/modal-cs-cart-list.jsp?cs=${csId}"/>
						</div>

						<a href="#" class="btn btn-lg btn-primary cs-btn-confirm">Confirm</a>

						<a href="#" class="btn btn-lg btn-info" data-dismiss="modal">Cancel</a>
					</form>
					<dsp:form method="post" id="modal-cs-cart-form">
						<dsp:input type="hidden" value="" id="selected_shipping_cart_id"
								   bean="ProfileDefaultShippingAddressFormHandler.selectedId"/>
						<dsp:input type="hidden" value="true"
								   bean="ProfileDefaultShippingAddressFormHandler.setShippingAddressOnCart"/>
					</dsp:form>
				</div>
			</div>
		</div>
	</div>
</dsp:page>