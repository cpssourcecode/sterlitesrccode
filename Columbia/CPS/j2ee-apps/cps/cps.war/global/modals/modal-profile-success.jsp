<dsp:page>
	<a href="#" data-toggle="modal" data-target="#profileSuccess" style="display: none;" id="link-profileSuccess"></a>
	<div class="modal fade" id="profileSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<div class="header-symbol">
						<svg class="icon icon-missing">
							<use xmlns:xlink="http://www.w3.org/1999/xlink"
								xlink:href="#icon-missing"></use>
						</svg>
					</div>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true"><span
							class="glyphicon glyphicon-remove"></span></span>
					</button>
				</div>
				<div class="modal-body">
					<h2 class="modal-title">Success</h2>
					<div class="row">
						<div class="col-sm-12">
							<p>Changes have been saved.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>