<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>

	<%--<dsp:importbean bean="/atg/userprofiling/Profile"/>--%>

	<dsp:getvalueof var="commerceItemCount" bean="ShoppingCart.current.commerceItemCount"/>
	<dsp:getvalueof var="isEmptyCart" value="${commerceItemCount == 0}"/>

	<dsp:getvalueof var="orderId" param="orderId"/>

	<a href="#" data-toggle="modal" data-target="#modalMoveToCart${orderId}" style="display: none;"
	   id="link-modalMoveToCart${orderId}">
	</a>
	<div class="modal fade in" id="modalMoveToCart${orderId}" tabindex="-1" role="dialog" aria-labelledby="modalMoveToCart${orderId}" aria-hidden="true">

		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
				</div>
				<div class="modal-body">
					<c:choose>
						<c:when test="${isEmptyCart}">
							<h4 class="modal-title" id="modal-movetoactivecart-title"><fmt:message key="account.approval.move_to_cart"/></h4>
							<p>Moving a Saved Cart to the Active Cart will update pricing. Do you want to continue?</p>
						</c:when>
						<c:otherwise>
							<h4 class="modal-title" id="modal-movetoactivecart-title"><fmt:message key="account.approval.move_to_cart_items_in_cart"/></h4>
							<p>
								There are items in the active Cart. To move a Saved Cart to an NEW active Cart, you must first SAVE the active Cart.
								<strong>When you SAVE the active Cart, it is moved to the Saved Carts page where you can later turn it back into an active Cart.</strong>
							</p>
						</c:otherwise>
					</c:choose>
					<a href="#" class="btn btn-primary" data-event-click-id="global212" data-event-click-param0="${vsg_utils:escapeJS(orderId)}" >
						<fmt:message key="account.approval.confirm_and_cart"/>
					</a>
					<a href="#" class="btn btn-info" data-dismiss="modal">
						<fmt:message key="account.approval.cancel"/>
					</a>
				</div>
			</div>
		</div>

	</div>


</dsp:page>
