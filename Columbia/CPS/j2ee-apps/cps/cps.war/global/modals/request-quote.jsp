<dsp:page>
	<dsp:importbean bean="/cps/util/FileUploadHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:getvalueof var="scess" param="scess"/>
	<div class="modal fade" id="requestQuote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<c:choose>
			<c:when test="${scess}">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<span class="modal-icon"><i class="fa fa-info"></i></span>
					</div>
					<div class="modal-body">
						<h4 class="modal-title" id="modal-text-title"></h4>

						<p>Your request has been successfully submitted. Your Sales Team will contact you shortly. Thank you for using columbiapipe.com.</p>

						<button class="btn btn-info" data-dismiss="modal">Okay</button>
					</div>
				</div>
			</c:when>
			<c:otherwise>
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
						<span class="modal-icon"><i class="fa fa-upload"></i></span>
					</div>
					<div class="modal-body">
						<dsp:form enctype="multipart/form-data" id="reqQuote" method="post" action="">
							<h4 class="modal-title" id="modal-quote-title">Request a Quote</h4>
							<dsp:include page="/account/gadgets/account-error-message.jsp">
								<dsp:param name="formHandler" bean="FileUploadHandler"/>
							</dsp:include>
							<p>To request a quote, upload a document in one of the folloiwng formats: .doc, .pdf, .exl.</p>
							<div class="form-group custom-file-upload">
								<dsp:input bean="FileUploadHandler.requestQuoteFile" iclass="form-control" name="requestquotefile" type="file" />
							</div>

							<div class="form-group">
								<textarea class="form-control" id="request-quote-textarea" placeholder="Message"></textarea>
							</div>


							<button type="button" data-event-click-id="global245"  class="btn btn-primary" >Upload</button>
							<button class="btn btn-info" data-dismiss="modal">Cancel</button>
							<span hidden><dsp:input type="submit" bean="FileUploadHandler.requestAQuote" iclass="btn btn-primary" value="Upload" id="reqSubUpload"/></span>
							<dsp:input type="hidden" bean="FileUploadHandler.requestQuoteMessage" id="request" value=""/>
							<dsp:getvalueof var="requestURI" bean="OriginatingRequest.requestURI"/>
							<dsp:getvalueof var="queryString" bean="OriginatingRequest.queryString"/>
							<dsp:input type="hidden" id="rqsuccess" bean="FileUploadHandler.requestAQuoteSuccessURL" value=""/>
							<dsp:input type="hidden" id="rqfail" bean="FileUploadHandler.requestAQuoteFailURL" value=""/>
						</dsp:form>
					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	</div>
</dsp:page>