<dsp:page>
<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
<dsp:importbean bean="/cps/util/CPSSessionBean"/>
<!-- All or Some Items Not Added to Cart -->
<div class="modal fade in" id="itemsAddtoMl" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<dsp:getvalueof var="giftlistId" param="giftlistId"/>
	<dsp:getvalueof var="ca" param="ca"/>
	<%--<dsp:getvalueof var="invalidMaterialItemsList" bean="CPSSessionBean.invalidMaterialItemsList"/>--%>
	<dsp:param name="invalidMaterialItemsList" bean="CPSSessionBean.invalidMaterialItemsList"/>
	<dsp:param name="materialDuplicates" bean="CPSSessionBean.materialDuplicates"/>
	<dsp:param name="hasMaterialDuplicates" bean="CPSSessionBean.hasMaterialDuplicates"/>
	<dsp:getvalueof var="showErrorDivs" bean="CPSSessionBean.showErrorDivs"/>
	<dsp:getvalueof var="hasMaterialDuplicates" param="hasMaterialDuplicates"/>
	<dsp:getvalueof var="hasInvalidItems" value="false"/>

<dsp:form name="dupeAddToMaterial" formid="dupeAddToMaterial" id="dupeAddToMaterial" action="" method="post">
	<%-- <dsp:input bean="GiftlistFormHandler.addItemToOrderSuccessURL" name="nextpageval" type="hidden" value="/checkout/cart.jsp?qo=t"/>
	<dsp:input bean="GiftlistFormHandler.addItemToOrderErrorURL" type="hidden" value=""/> --%>
	<dsp:input type="hidden" bean="GiftlistFormHandler.skuIdsList" value="" id="dupeMaterialSkuIdsList"/>
	<%-- <dsp:input type="hidden" id="notquickcerror" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListErrorURL" value=""/> --%>
	<%-- <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListSuccessURL" value="/checkout/cart.jsp?qo=t"/> --%>
	<%-- <dsp:input type="hidden" id="notinvalidModallink" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListInvalidURL" value=""/> --%>
	<dsp:input type="hidden" bean="GiftlistFormHandler.qtyList" value="" id="dupeMaterialQtyList"/>
	<dsp:input type="hidden" bean="GiftlistFormHandler.addFromErrorModal" value="yes"/>
	<dsp:input type="hidden" bean="GiftlistFormHandler.giftlistId" value="${giftlistId}"/>
	<dsp:input type="hidden" bean="GiftlistFormHandler.giftlistCheckAvailability" value="${ca}"/>
	<dsp:input type="hidden" bean="GiftlistFormHandler.addMaterialListItemsFromFile" value="true"/>
</dsp:form>

	<div class="modal-dialog modal-narrow" role="document" style="width: 740px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
				<span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
			</div>
			<div class="modal-body">
				<h4 class="modal-title">All or Some Items Not Added to Material List</h4>

				<c:if test="${showErrorDivs}">
					<dsp:include page="/account/gadgets/account-error-message.jsp">
						<dsp:param name="formHandler" bean="GiftlistFormHandler"/>
					</dsp:include>
					<dsp:include page="/account/gadgets/account-error-message.jsp">
						<dsp:param name="formHandler" bean="/cps/util/FileUploadHandler"/>
					</dsp:include>
				</c:if>

				<dsp:droplet name="cps/droplet/QuickOrderErrorDroplet">
					<dsp:param name="invalidSkuList" param="invalidMaterialItemsList"/>
					<dsp:param name="duplicateProducts" param="materialDuplicates"/>
					<dsp:param name="hasDuplicates" param="hasMaterialDuplicates"/>

					<!-- text after header -->
					<dsp:oparam name="outputStartInvalidSearchInput">
						<div class="row">
							<div class="col-sm-12">
								<p>The following items were NOT added to your Material List because you entered an invalid item #,
								 quantity, alias #, or these items are no longer available.</p>
								<p>
					</dsp:oparam>
						<dsp:oparam name="outputInvalidSearchInput">
							<dsp:valueof param="invalidInput"/><br/>
						</dsp:oparam>
						<dsp:oparam name="outputEndInvalidSearchInput">
								</p>
							</div>
						</div>
						</dsp:oparam>
					<!-- /text after header -->
					
					<!-- duplicates --> 
					<dsp:oparam name="outputStartDuplicatesDisplay">
						<hr/>
						<div class="row">
							<div class="col-xs-12">
								<h3>Duplicate Matches Found</h3>
								<p>The following items in each row share the same Item # and Alias #. Select one item in each row to add to your Material List. </p>
								<br/>
							</div>
						</div>
						<div class="dublicate-list form-group">
							<div class="scrollable">
								<div class="scroll-wrapper content scrollbar-inner" style="position: relative;">
									<div class="content scrollbar-inner scroll-content scroll-scrolly_visible" style="margin-bottom: 0px; margin-right: 0px; height: 440px;">
										<div class="col-lg-12 grid">
					</dsp:oparam>
					<dsp:oparam name="outputStartDuplicateRow">
											<div class="row item">
					</dsp:oparam>
					<dsp:oparam name="outputDuplicateCol">
						<dsp:getvalueof var="rowQuantity" param="rowQuantity"/>
						<param name="dupematerialqty" value="${rowQuantity}"/>
						<dsp:droplet name="ProductLookup">
							<dsp:param name="id" param="duplicateId"/>
							<dsp:param name="filterBySite" value="false"/>
							<dsp:param name="filterByCatalog" value="false"/>
							<dsp:oparam name="output">
								<dsp:setvalue param="product" paramvalue="element"/>
								<dsp:getvalueof var="detailImg" param="product.web_url"/>
					 			<dsp:getvalueof var="defaultImg" value="${staticContentPrefix}/assets/images/pdp-placeholder.png"/>
					 			<dsp:getvalueof var="dupeProdId" param="product.id"/>
								<div class="col-md-6">
									<dsp:getvalueof var="rowNumber" param="rowNumber"/>
									<div class="radio set-default">
										<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
											<input class="dupematerialproduct" name="dupematerialproduct${rowNumber}" type="radio" value="${dupeProdId}"> Select
												<span class="img-label">
													<dsp:getvalueof var="lowerImageURL"	value="${fn:toLowerCase(detailImg) }" />
									<c:choose>
										<c:when test="${fn:startsWith(lowerImageURL,'http')}">
													<img data-src="<c:out value="${detailImg}" default="${defaultImg}"/>" data-fallback-image="${defaultImg}" id="imagePlus"/>
										</c:when>
										<c:otherwise>
													<img data-src="${staticContentPrefix}<c:out value="${detailImg}" default="${defaultImg}"/>" data-fallback-image="${defaultImg}" id="imagePlus"/>
										</c:otherwise>
									</c:choose>
										</span>
									   </label>
									</div>
									<h5><dsp:valueof param="product.displayName" converter="valueishtml" /></h5>
										<div class="price"><span>
										<dsp:droplet name="/cps/droplet/CPSPriceDroplet">
														<dsp:param name="productId" param="product.id"/>
														<dsp:param name="profile" bean="Profile"/>
														<dsp:oparam name="outputPrice">
															<dsp:valueof param="currentPrice" converter="currencyConversion"/>
														</dsp:oparam>
													</dsp:droplet>
										</span>/unit/pack info</div>
											<p>
											Mfr. # <dsp:valueof param="product.vendorItem"/>
											<br> Item # <dsp:valueof param="duplicateId"/>
											<br> UPC <dsp:valueof param="product.upc"/>
											<br> Alias <dsp:droplet name="/cps/droplet/CustomerAliasDroplet">
												<dsp:param name="product" param="product"/>
												<dsp:oparam name="outputAlias">
													<dsp:getvalueof var="prodAlias" param="customerAlias"/>
                    								<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
													<dsp:valueof value="${alias}"/>
												</dsp:oparam>
											</dsp:droplet>
											<br>
											</p>
											<p><dsp:valueof param="product.description"/> </p>
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
					<dsp:oparam name="outputEndDuplicateRow">
											</div>
					</dsp:oparam>
					<dsp:oparam name="outputEndDuplicatesDisplay">
										</div>
									</div>
								</div>
							</div>
						</div>
					</dsp:oparam>
					<!-- /duplicates -->
					
					<dsp:oparam name="outputDuplicatesFooter">
						<a href="#" data-event-click-id="global209"  class="btn btn-primary" data-dismiss="modal">Add Selected to Cart</a>
						<a href="#" class="btn btn-info" data-dismiss="modal">Close</a>
					</dsp:oparam>	
					<dsp:oparam name="outputInvalidOnlyFooter">
						<button class="btn btn-primary" data-dismiss="modal">Okay</button>
					</dsp:oparam>	
				</dsp:droplet>
			</div><!-- <div class="modal-body"> -->
		</div><!-- <div class="modal-content"> -->
	</div><!-- <div class="modal-dialog modal-narrow" -->
</div><!-- <div class="modal fade in" id="itemsAddtoMl" -->

</dsp:page>