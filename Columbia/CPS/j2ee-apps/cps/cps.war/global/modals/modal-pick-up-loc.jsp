<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>

	<a href="#" data-toggle="modal" data-target="#changePickupLocation" style="display: none;" id="link-changePickupLocation"></a>

	<div class="modal fade" id="changePickupLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-truck"></i></span>
				</div>

				<div class="modal-body">
					<h4 class="modal-title">Change Pickup Location</h4>


					<dsp:droplet name="CartAddressDroplet">
						<dsp:param bean="ShoppingCart.current" name="order"/>
						<dsp:param bean="Profile" name="profile"/>
						<dsp:param value="pickup" name="page"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="store" param="store"/>
							<dsp:tomap var="pickUpStore" param="store"/>
							<dsp:getvalueof var="storeId" param="storeId"/>
						</dsp:oparam>
					</dsp:droplet>

					<hr>

					<form class="form">
						<c:if test="${not empty store}">
							<h5>Your Default or Current Pickup Location</h5>
						</c:if>

						<c:if test="${not empty store}">
							<div class="radio checked">
								<label class="active">
									<input type="radio" checked class="active" data-initialize="radio" name="addressSelectRadio" id="myCustomRadioLabel"
										   value="${storeId}" data-event-click-id="global220" >

											<strong>${pickUpStore.region}<br />
											${pickUpStore.address1}<br />
											<c:if test="${not empty pickUpStore.address2}">${pickUpStore.address2}<br /></c:if>
											<c:if test="${not empty pickUpStore.address3}">${pickUpStore.address3}<br /></c:if>
											${pickUpStore.city},&nbsp;${pickUpStore.stateAddress}&nbsp;${pickUpStore.postalCode}</strong>
									</input>
								</label>
							</div>
							<hr>
						</c:if>


						<h5>Select an alternate Pickup Location for this session.</h5>

						<div class="alert alert-icon">
							<i class="fa fa-exclamation-triangle red"></i>
							<strong>Changing your Pickup Location may affect prices and availability of items in your cart.</strong>
						</div>

						<div class="row row-narrow mb20">
							<div class="col-sm-9 mb5">

								<div class="input-group input-group-lg">
									<input type="text" id="modal-pickup-input-search" class="form-control input-lg" placeholder="Search for Pickup Location">
									<div class="input-group-btn">
										<button type="button" class="btn" title="Search" data-event-click-id="global221" ><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
							<div class="col-sm-3">
								<button class="btn btn-info btn-block btn-lg" data-event-click-id="global222" >Reset</button>
							</div>
						</div>

						<div id="modal-pickup-list-content" class="modal-shipping-list">
							<div class="list-header row row-flush">
								<div class="col-xs-2 text-center">Select</div>
								<div class="col-xs-10">Pickup Location</div>
							</div>
							<dsp:include page="gadgets/modal-pickup-list.jsp?storeid=${storeId}"/>
						</div>

						<a href="#" class="btn btn-lg btn-primary pickup-btn-confirm">Confirm</a>
						<button class="btn btn-lg btn-info" data-dismiss="modal">Cancel</button>
					</form>
					<dsp:form method="post" id="modal-pickup-form">
						<dsp:input type="hidden" value="${storeId}" id="selected_pickup_id"
								   bean="ProfileDefaultShippingAddressFormHandler.selectedId"/>
						<dsp:input type="hidden" value="true"
								   bean="ProfileDefaultShippingAddressFormHandler.setPickupLocationOnCart"/>
					</dsp:form>
				</div>
			</div>
		</div>
	</div>
</dsp:page>