<dsp:page>
	<dsp:importbean bean="/cps/util/FileDownloadHandler" />
	<div class="modal fade" id="exportUsers" tabindex="-1" role="dialog" aria-labelledby="exportUsers" aria-hidden="true">
		<div class="modal-dialog modal-narrow">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<span class="modal-icon">
						<i class="fa fa-exclamation-triangle"></i>
					</span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-usersout-title">Export Users</h4>

					<p>To export Users, click the Export button. Save the CSV file to a location of your choice.</p>

					<dsp:form id="export-users" formid="export-users" action="/" method="post">
						<button class="btn btn-primary">Export</button>
						<dsp:input type="hidden" bean="FileDownloadHandler.createUserSpreadsheetSuccessURL" value="/account/manage-users.jsp" />
						<dsp:input type="hidden" bean="FileDownloadHandler.createUserSpreadsheetErrorURL" value="/account/manage-users.jsp" />
						<dsp:input type="hidden" bean="FileDownloadHandler.createUserSpreadsheet" value="true" />
						<button class="btn btn-info" data-dismiss="modal">Cancel</button>
					</dsp:form>
					<script type="text/javascript" nonce="${requestScope.nonce}">
						function exportUsers() {
							$('#export-users').submit();
							$('#cancel').click();
						}
					</script>
				</div>
			</div>
		</div>
	</div>
</dsp:page>