<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<dsp:getvalueof var="isReset" param="isReset"/>

	<a href="#" data-toggle="modal" data-target="#addressAdminConfirm" style="display: none;" id="link-addressAdminConfirm"></a>

	<!-- confirm Modal on View Address Page -->
	<div class="modal fade" id="addressAdminConfirm" tabindex="-1" role="dialog" aria-labelledby="addressConfirmChange" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-ship-change-title">Assign a default Ship to Address</h4>

					<p><cp:repositoryMessage key="changeShippingAddress"></cp:repositoryMessage></p>
					<button class="btn btn-primary" id="shipToAddressSubmit" data-event-click-id="eventDefaultShipping" data-cs-id="${csId}"><fmt:message key="account.address.label.confirm"/></button>
					<button class="btn btn-info" id="shipToAddressCancel" data-dismiss="modal" data-event-click-id="global229" data-event-click-param0="${vsg_utils:escapeJS(isReset)}" >Cancel</button>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
