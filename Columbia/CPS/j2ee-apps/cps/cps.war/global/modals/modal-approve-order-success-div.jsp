<dsp:page>
<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
<dsp:form action="" method="post" id="approveOrders" formid="approveOrders">
	<dsp:input type="hidden" bean="ApprovalFormHandler.orderIds" value="" id="approveList"/>

	<div class="modal-body">
		<h4 class="modal-title" id="modal-confirm-title"><fmt:message key="account.approval.approve_orders"/></h4>

		<p>Are you sure you want to approve selected order requests?</p>

		<a href="#" class="btn btn-primary" data-event-click-id="global183" >
			<%-- <fmt:message key="account.approval.confirm"/> --%>
			Proceed						
		</a>
		<a href="#" class="btn btn-info" data-dismiss="modal">
			<fmt:message key="account.approval.cancel"/>
		</a>
	</div>

	<dsp:input type="hidden" bean="ApprovalFormHandler.approveOrders" value="true" priority="-10"/>
</dsp:form>
</dsp:page>