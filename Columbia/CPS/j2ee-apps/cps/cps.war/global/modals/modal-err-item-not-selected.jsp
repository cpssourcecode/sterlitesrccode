<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
<dsp:page>

	<a href="#" data-toggle="modal" data-target="#err_search_order" style="display: none;" id="link-err_search_order"></a>

	<div class="modal fade" id="alertItemNotSelected" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-text-title"></h4>
					<p>
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="notWrap" value="true"/>
							<dsp:param name="key" value="errItemNotSelected"/>
						</dsp:include>
					</p>

					<button class="btn btn-info" data-dismiss="modal" id="dismiss-err_search_order">Okay</button>
				</div>
			</div>
		</div>
	</div>
</dsp:page>












<%-- <dsp:page>

	<div class="modal fade" id="alertItemNotSelected" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<div class="header-symbol">
						<span class="glyphicon glyphicon-alert"></span>
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span
							class="glyphicon glyphicon-remove"></span></span>
					</button>
				</div>
				<div class="modal-body">

					<h2 class="modal-title">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="errItemNotSelected"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include>
					</h2>

				</div>
				<div class="modal-footer">
					<ul class="list-inline pull-left">
						<li>
							<a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal"><fmt:message key="modal.cart.button.okay"/></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

</dsp:page> --%>
