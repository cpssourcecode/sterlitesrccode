<dsp:page>

	<a href="#" data-toggle="modal" data-target="#modalNoHeatEntered" style="display: none;" id="link-modalNoHeatEntered"></a>

	<!-- Please enter Heat # -->
	<div class="modal fade in" id="modalNoHeatEntered" tabindex="-1" role="dialog" aria-labelledby="noHeatEntered" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-text-title">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="err_no_search_heat_entered"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include></h4>
	
					<p>	</p>
	
					<a href="#" class="btn btn-info" data-dismiss="modal">Okay</a>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
