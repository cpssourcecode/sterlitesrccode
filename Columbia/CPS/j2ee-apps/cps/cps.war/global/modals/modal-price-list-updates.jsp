<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<a href="#" data-toggle="modal" data-target="#priceListUpdate" style="display: none;" id="link-priceListUpdate"></a>
	<div class="modal fade" id="priceListUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<!-- <div class="modal-header">
					<div class="header-symbol">
						<svg class="icon icon-missing">
							<use xlink:href="#icon-missing" xmlns:xlink="http://www.w3.org/1999/xlink" />
						</svg>
					</div>
					<button aria-label="Close" data-dismiss="modal" class="close" type="button">
						<span aria-hidden="true">
							<span class="glyphicon glyphicon-remove"></span>
						</span>
					</button>
				</div> -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<span class="modal-icon">
						<i class="fa fa-envelope"></i>
					</span>
				</div>
				<%-- 	<div class="modal-body">

					<h2 class="modal-title">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="notWrap" value="true" />
							<dsp:param name="key" value="msgPliceListUpdatesInfoSubmit" />
						</dsp:include>
					</h2>
					<h4>
					<dsp:include page="/includes/gadgets/info-message.jsp">
					<dsp:param name="notWrap" value="true"/>
					<dsp:param name="key" value="msgPliceListUpdatesInfoSubmit"/>
					</dsp:include>
					</h4>

					<br />

					<div class="form-group">
						<button type="button" class="btn btn-warning btn-huge btn-block" data-dismiss="modal">
							<fmt:message key="global.modal.ok" />
						</button>
					</div>

				</div> --%>
				<div class="modal-body">
					<br/><p>You have been signed up successfully</p>
					<button class="btn btn-info" data-dismiss="modal">
						<fmt:message key="global.modal.ok" />
					</button>
				</div>
			</div>
		</div>
	</div>
</dsp:page>