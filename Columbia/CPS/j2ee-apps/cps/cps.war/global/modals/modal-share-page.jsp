<dsp:importbean bean="/cps/util/SharePageFormHandler" />
<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
<dsp:page>
	<div class="modal fade" id="sharePageModal" tabindex="-1" role="dialog" aria-labelledby="sharePageModalTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<dsp:form action="${originatingRequest.requestURI}" method="POST" id="share-page-form" formid="share-page-form">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">x</span>
						</button>
						<span class="modal-icon">
							<i class="fa fa-envelope"></i>
						</span>
					</div>
					<div class="modal-body">
						<h4 class="modal-title" id="modal-emailpage-title">Email Page</h4>

						<div class="form-group">
							<fmt:message var="recipientsPlaceholder" key="share.field.recipients" />
							<dsp:input iclass="form-control" id="shareModalRecipients" bean="SharePageFormHandler.recipients" type="text" maxlength="1000">
								<dsp:tagAttribute name="placeholder" value="${recipientsPlaceholder}" />
							</dsp:input>
							<small class="text-danger" style="display: none;" id="share-error-message-shareModalRecipients"></small>
						</div>
						<div class="row row-narrow">
							<div class="form-group col-md-4">
								<fmt:message var="firstNamePlaceholder" key="share.field.firstName" />
								<dsp:input iclass="form-control" id="shareModalFirstName" bean="SharePageFormHandler.firstName" type="text" maxlength="30">
									<dsp:tagAttribute name="placeholder" value="${firstNamePlaceholder}" />
								</dsp:input>
								<small class="text-danger" style="display: none;" id="share-error-message-shareModalFirstName"></small>
							</div>
							<div class="form-group col-md-4">
								<fmt:message var="lastNamePlaceholder" key="share.field.lastName" />
								<dsp:input iclass="form-control" id="shareModalLastName" bean="SharePageFormHandler.lastName" type="text" maxlength="30">
									<dsp:tagAttribute name="placeholder" value="${lastNamePlaceholder}" />
								</dsp:input>
								<small class="text-danger" style="display: none;" id="share-error-message-shareModalLastName"></small>
							</div>
							<div class="form-group col-md-4">
								<fmt:message var="emailAddressPlaceholder" key="share.field.emailAddress" />
								<dsp:input iclass="form-control" id="shareModalEmailAddress" bean="SharePageFormHandler.emailAddress" type="text" maxlength="30">
									<dsp:tagAttribute name="placeholder" value="${emailAddressPlaceholder}" />
								</dsp:input>
								<small class="text-danger" style="display: none;" id="share-error-message-shareModalEmailAddress"></small>
							</div>
						</div>
						<div class="form-group">
							<fmt:message var="messagePlaceholder" key="share.field.message" />
							<textarea name="/cps/util/SharePageFormHandler.message" class="form-control" id="shareModalMessage" placeholder="${messagePlaceholder}"></textarea>
						</div>

						<div class="checkbox" id="sendCopyCheckbox">
							<label data-initialize="checkbox">
								<dsp:input id="shareModalCopyToSender" bean="SharePageFormHandler.sendCopyToSender" type="checkbox" />
								<span class="checkbox-label">
									<fmt:message key="share.field.sendCopyToSender" />
								</span>
							</label>
						</div>

						<fmt:message var="submitPlaceholder" key="share.button.submit" />
						<dsp:input type="hidden" id="share-page-url" bean="SharePageFormHandler.sharedUrl" value="" />
						<dsp:input type="hidden" bean="SharePageFormHandler.sharePage" value="${submitPlaceholder}" />
						<c:choose>
							<c:when test="${isTransient}">
							<div id="share-form-captcha">
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<div id="recaptchaShare"></div>
										</div>
									</div>
								</div>
							</div>
								<button type="button" id="shareModalSubmit" data-event-click-id="global228"  class="btn btn-primary">${submitPlaceholder}</button>
			        			<a href="#" class="btn btn-info" data-dismiss="modal">
								<fmt:message key="share.button.cancel" />
								</a>
							</c:when>
							<c:otherwise>
								<button type="button" id="shareModalSubmit" data-event-click-id="global228"  class="btn btn-primary">${submitPlaceholder}</button>
				        		<a href="#" class="btn btn-info" data-dismiss="modal">
								<fmt:message key="share.button.cancel" />
								</a>
							</c:otherwise>
						</c:choose>
					</div>

				</dsp:form>
			</div>
		</div>
	</div>
<script type="text/javascript" nonce="${requestScope.nonce}">
$(document).ready(function() {
if($("#isTransient").val() == 'true'){
    checkShareSubmitButton();
}
});
</script>

</dsp:page>