<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>

	<dsp:getvalueof var="error" param="error"/>
	<dsp:getvalueof var="errorCode" param="errorCode"/>

	<dsp:getvalueof var="profileId" param="user.repositoryId"/>
	<dsp:getvalueof var="firstName" param="user.firstName"/>
	<dsp:getvalueof var="lastName" param="user.lastName"/>
	<dsp:getvalueof var="email" param="user.email"/>



	<div class="modal fade in" id="modal-passwordreset" tabindex="-1" role="dialog" aria-labelledby="modal-passwordreset-title" style="display: block; padding-right: 16px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="icon icon-user"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-passwordreset-title">Reset Your Password</h4>

					<div class="alert alert-danger" style="display:none;" id="error-messages-reset-password"></div>
					<c:choose>
						<c:when test="${error}">
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="${errorCode}"/>
							</dsp:include>
						</c:when>
						<c:otherwise>
							<dsp:form action="${originatingRequest.contextPath}/global/modal/modal-password-reset.jsp" method="post"
									  id="change-password-form" formid="change-password-form">
								<dsp:input type="hidden" bean="ProfileFormHandler.value.email" value="${email}"/>
								<dsp:input type="hidden" bean="ProfileFormHandler.value.firstName" value="${firstName}"/>
								<div class="form-group">
									<label class="control-label sr-only">New Password</label>
									<dsp:input iClass="form-control input-lg" id="input-password"
											   bean="ProfileFormHandler.value.password" type="password" autocomplete="new-password" maxlength="15">
										<dsp:tagAttribute name="placeholder" value="New Password"/>
									</dsp:input>
								</div>
								<div class="form-group">
									<label class="control-label sr-only">Confirm New Password</label>
									<dsp:input iclass="form-control input-lg" id="input-password"
											   bean="ProfileFormHandler.value.confirmPassword" type="password" autocomplete="new-password"
											   maxlength="15">
										<dsp:tagAttribute name="placeholder" value="Confirm New Password"/>
									</dsp:input>
								</div>
								<div class="form-group collapse profile-password">
									<div class="well well-grey" style="padding: 14px; margin: 0px">
										<h5>Password rules</h5>
										<ul>
											<li>Minimum of six and maximum of 15 characters;</li>
											<li>Contain at least one uppercase alphabetic character;</li>
											<li>Contain at least one numeric character;</li>
											<li>Contain at least one special character (examples: !, @, #, $, %, &amp;, *, _).</li>
										</ul>
									</div>
								</div>
								<div class="checkbox mb15">
									<label>
										<dsp:input type="checkbox" value="false" id="change-password-rememberMe"
												   bean="ProfileFormHandler.value.rememberMe">
											<dsp:tagAttribute name="data-event-click-id" value="global218"/>
										</dsp:input>
										Remember Me
									</label>
								</div>


								<button type="button" data-event-click-id="global219" 
										class="btn btn-primary btn-lg btn-block">Submit
								</button>

								<dsp:input type="hidden" bean="ProfileFormHandler.changePassword" value="true" priority="-10" />
							</dsp:form>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
