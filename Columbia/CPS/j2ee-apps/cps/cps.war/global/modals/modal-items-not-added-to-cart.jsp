<dsp:page>
    <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <!-- All or Some Items Not Added to Cart -->
    <div class="modal fade in" id="modal-carterror" tabindex="-1" role="dialog" aria-labelledby="modal-carterror-title"
         aria-hidden="true">
        <div class="modal-dialog" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <span class="modal-icon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-carterror-title">Some Items Not Added to Cart</h4>
                    <dsp:droplet name="cps/droplet/QuickOrderErrorDroplet">
                        <dsp:param name="invalidSkuList" param="invalidItemList"/>
                        <dsp:param name="duplicateProducts" param="duplicateProducts"/>
                        <dsp:param name="hasDuplicates" param="hasQuickOrderDuplicates"/>
                        <dsp:oparam name="outputStartInvalidSearchInput">
                            <p>The following items were NOT added to your cart because you entered an invalid Item # or
                                Alias # or these items are no longer available:</p>
                            <ul class="list-unstyled">
                        </dsp:oparam>
                        <dsp:oparam name="outputInvalidSearchInput">
                            <li><dsp:valueof param="invalidInput"/></li>
                        </dsp:oparam>
                        <dsp:oparam name="outputEndInvalidSearchInput">
                            </ul>
                            <button class="btn btn-info" data-dismiss="modal">Close</button>
                        </dsp:oparam>
                    </dsp:droplet>
                </div>
            </div>
        </div>
    </div>

</dsp:page>