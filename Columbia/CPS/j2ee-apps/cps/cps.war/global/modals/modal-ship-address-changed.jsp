<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<dsp:getvalueof var="isReset" param="isReset"/>

	<a href="#" data-toggle="modal" data-target="#addressConfirm" style="display: none;" id="link-addressConfirm"></a>

	<!-- confirm Modal on View Address Page -->
	<div class="modal fade" id="addressConfirm" tabindex="-1" role="dialog" aria-labelledby="addressConfirm" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-shipsuccess-title">Shipping Address Changed</h4>

					<p>Your default Ship To address has been changed.</p>

					<button class="btn btn-info" data-dismiss="modal" data-event-click-id="global229" data-event-click-param0="${vsg_utils:escapeJS(isReset)}" >Close</button>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
