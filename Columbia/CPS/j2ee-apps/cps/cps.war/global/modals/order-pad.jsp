<dsp:page>
<!--  Order Pad Modal-->
<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" scope="request"/>
<dsp:importbean bean="/cps/util/FileUploadHandler"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
<div class="modal fade" id="orderPadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="header-symbol">
                    <svg class="icon icon-power">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-power"></use>
                    </svg>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span>
                </button>
                <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
            </div>
			<dsp:form name="quickAddToCart" formid="quickAddToCart" id="quickAddToCart" action="${originatingRequest.contextPath}/" method="post">
                <dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" name="nextpageval" type="hidden" value="/checkout/cart.jsp?qo=t"/>
                <dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden" value=""/>
                <dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsList"/>
                <dsp:input type="hidden" id="quickcerror" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListErrorURL"
                           value=""/>
                <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListSuccessURL"
                           value="/checkout/cart.jsp?qo=t"/>
                <dsp:input type="hidden" id="invalidModallink" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListInvalidURL" value=""/>

                <dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyList"/>
                <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrder" value="true"/>
            </dsp:form>
            <div class="modal-body modal-import-tab">
                <h2 class="modal-title">Order Pad</h2>
                <dsp:include page="/account/gadgets/account-error-message.jsp">
						<dsp:param name="formHandler" bean="CartModifierFormHandler"/>
					</dsp:include>
				<dsp:include page="/account/gadgets/account-error-message.jsp">
						<dsp:param name="formHandler" bean="FileUploadHandler"/>
					</dsp:include>
                <div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="importTabs">
                        <li role="presentation" class="active"><a href="#manualEntry" aria-controls="manualEntry" role="tab" data-toggle="tab">Manual Entry</a></li>
                        <li role="presentation" class="hidden-xs"><a href="#importUpload" aria-controls="importUpload" role="tab" data-toggle="tab">Import/Upload List</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="manualEntry">
                            <table class="table table-striped table-order-pad">
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" name="quickproductPage" class="form-control  quick-order-item" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" data-event-keypress-id="global239"  name="quickqtyPage" maxlength="4" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" name="quickproductPage" class="form-control  quick-order-item" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" data-event-keypress-id="global240"  name="quickqtyPage" maxlength="4" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" name="quickproductPage" class="form-control  quick-order-item" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" data-event-keypress-id="global241"  name="quickqtyPage" maxlength="4" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" name="quickproductPage" class="form-control  quick-order-item" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" data-event-keypress-id="global242"  name="quickqtyPage" maxlength="4" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" name="quickproductPage" class="form-control  quick-order-item" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" data-event-keypress-id="global243"  name="quickqtyPage" maxlength="4" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 col-xxs-12">
                                                    <div class="row smaller">
                                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                            <input type="text" name="quickproductPage" class="form-control  quick-order-item" placeholder="Item# or Alias#">
                                                        </div>
                                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                            <input type="text" data-event-keypress-id="global244"  name="quickqtyPage" maxlength="4" class="form-control" placeholder="Qty">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-sm-7 col-xs-7 col-xxs-12">
                                        <ul class="list-inline two-mobile">
                                            <li id="qoManualSubmit">
                                                <a data-event-click-id="global238"  class="btn btn-lg btn-warning btn-lg">Add to Cart</a>
                                            </li>
                                            <li>
                                                <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-5 col-xs-5 col-xxs-12 text-right">
                                        <ul class="list-inline">
											<li class="more-order-pad">
												<a href="/account/quick-order.jsp" class="btn btn-link gray-noreload btn-nopadding"><i class="fa fa-plus"></i> <span>Add More Items</span></a>
											</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="importUpload">
                        <dsp:form enctype="multipart/form-data" id="qorderUpload" method="post" action="">
                            <br>
                            <div class="kv-main">

                                    <!-- <div class="form-group">
                                        <input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1">
                                    </div> -->
                                    <div class="form-group">
                                        <!--input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1"-->
                                        <label class="custom-file-upload">
                                            <span class="btn btn-lg btn-default">Choose File</span>
                                            <mark>No file chosen</mark>
                                            <dsp:input type="file" bean="FileUploadHandler.quickOrderFile" name="quickorderfile"/>
                                        </label>

                                    </div>
                            </div>
                            <div class="modal-footer">
                                <div class="row">
                                    <div class="col-sm-7 col-xs-7">
                                        <ul class="list-inline two-mobile">
                                            <li id="qoUploadSubmit">
												<dsp:input type="submit" bean="FileUploadHandler.readQuickOrderFile" iclass="btn btn-lg btn-warning btn-lg" value="Upload" id="subUpload"/>
                                            </li>
                                            <li>
                                                <a href="#" class="btn btn-lg btn-default btn-lg" data-dismiss="modal">Cancel</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-5 col-xs-5 text-right">
                                        <ul class="list-inline">
                                            <!-- <li class="download-order-pad">
                                                <a href="/document/QuickOrderTemplate.xlsx" class="btn btn-link gray-noreload btn-nopadding">
                                                	<i class="fa fa-download"></i> <span>Download Spreadsheet Template</span>
                                                </a>
                                            </li> -->
                                            <li>
                                           		<a class="" href="/faq?oq=${instructionImportQO}" target="_blank">
													<dsp:include page="/includes/gadgets/info-message.jsp">
														<dsp:param name="key" value="infoHowCanIImportFile"/>
														<dsp:param name="notWrap" value="true"/>
													</dsp:include>
												</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


					<dsp:getvalueof var="requestURI" bean="OriginatingRequest.requestURI"/>
					<dsp:getvalueof var="queryString" bean="OriginatingRequest.queryString"/>
					<dsp:input type="hidden" id="quickosuccess" bean="FileUploadHandler.readQuickOrderFileSuccessURL" value="/checkout/cart.jsp?qo=t"/>
					<dsp:input type="hidden" id="quickoinvalid" bean="FileUploadHandler.readQuickOrderInvalidURL" value=""/>
					<dsp:input type="hidden" id="quickofail" bean="FileUploadHandler.readQuickOrderFileFailURL" value="/global/quickorder.jsp"/>
						</dsp:form>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" nonce="${requestScope.nonce}">

    $(document).ready(function () {
    	var baseParams=removeURLParameterOP(window.location.search,"quickomodal");
    	baseParams=removeURLParameterOP(baseParams,"_requestid");
        baseParams=removeURLParameterOP(baseParams, "productId");
        baseParams=removeURLParameterOP(baseParams, 'sreqmodal');
        baseParams=removeURLParameterRQ(baseParams, 'showCS');
        baseParams=removeURLParameterRQ(baseParams, 'qo');
        baseParams=removeURLParameterRQ(baseParams, 'invalidskumodal');
        baseParams=removeURLParameterRQ(baseParams, 'quickomodal');
    	if(baseParams==""){
    		//?
    		//$('#quickosuccess').val("${requestURI}"+"?quickomodal=false");

    		$('#quickcerror').val(window.location.pathname+"?quickomodal=true");
        	$('#quickofail').val(window.location.pathname+"?quickomodal=true");
        	$('#quickoinvalid').val(window.location.pathname+"?invalidskumodal=true");
        	$('#invalidModallink').val(window.location.pathname+"?invalidskumodal=true");
    	}
    	else{
    		//&
    		//$('#quickosuccess').val("${requestURI}"+"?"+baseParams+"&quickomodal=false");
    		$('#quickcerror').val(window.location.pathname+"?"+baseParams+"&quickomodal=true");
        	$('#quickofail').val(window.location.pathname+"?"+baseParams+"&quickomodal=true");
        	$('#quickoinvalid').val(window.location.pathname+"?"+baseParams+"&invalidskumodal=true");
        	$('#invalidModallink').val(window.location.pathname+"?"+baseParams+"&invalidskumodal=true");
    	}
    });
    function removeURLParameterOP(params, parameter) {
            var pars= params.split(/[&;]/g);
            var prefix= encodeURIComponent(parameter)+'=';
            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            params=pars.join('&');
            return params;
    }
</script>
</dsp:page>