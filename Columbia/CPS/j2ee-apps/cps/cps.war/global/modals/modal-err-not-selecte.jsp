<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<%--<a href="#" data-toggle="modal" data-target="#sessionIdleModal" style="display: none;" id="link-sessionIdleModal"></a>--%>

	<div class="modal fade" id="alertNotSelected" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">

					<h4 class="modal-title">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="err_not_selecte"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include>
					</h4>

					<button class="btn btn-info" data-dismiss="modal"><fmt:message key="modal.cart.button.okay"/></button>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
