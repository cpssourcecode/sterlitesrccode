<dsp:page>
	<dsp:importbean bean="/cps/droplet/RefinementsSorterDroplet"/>
	<dsp:getvalueof var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}"/>
	<dsp:getvalueof var="currentUrlParams" value="${requestScope['javax.servlet.forward.query_string']}"/>
	<c:if test="${not empty currentUrlParams}">
		<dsp:getvalueof var="currentUrl" value="${currentUrl}?${currentUrlParams}"/>
	</c:if>
	<dsp:getvalueof var="multiSelect" param="multiSelect"/>
	<dsp:droplet name="RefinementsSorterDroplet">
		<dsp:param name="refinements" param="refinements"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="sorted" param="sortedRefinements"/>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:getvalueof var="name" param="name"/>
	<dsp:getvalueof var="modal_key" value="${fn:replace(name, ' ', '_')}"/>

	<div class="modal fade viewAllOptionsModal" id="viewAllOptions${modal_key}" tabindex="-1" role="dialog"
		aria-labelledby="viewAllManufacturerOption" aria-hidden="true"
		style="display: none;">
		<div class="modal-dialog modal-wide" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<span class="modal-icon"><i class="fa fa-cube"></i></span>
				</div>
				
				<c:forEach var="entry" items="${sorted}" end="0">
					<input type="hidden" id="active_tab_${modal_key}" value="${entry.key}">
				</c:forEach>
				
				<div class="modal-body">
					<h4 class="modal-title">${name}</h4>
					<dsp:include page="${originatingRequest.contextPath}/global/gadgets/error-success-msg-divs.jsp"/>
					<nav class="text-center">
						<ul class="pagination view-all-abc-pagination">
							
							<c:forEach items="${sorted}" var="entry" varStatus="status">
								<li class="${status.index eq 0 ? 'active' : ''}" id="link_${modal_key}_${entry.key}">
									<a href="javascript:void(0);" data-event-click-id="global231" data-event-click-param0="${modal_key}" data-event-click-param1="${entry.key}" >${entry.key}</a>
								</li>
							</c:forEach>
						</ul>
					</nav>
					<div class="row">
					<c:forEach items="${sorted}" var="entry" varStatus="status">
						<div style="${status.index eq 0 ? '' : 'display: none'}" id="tab_${modal_key}_${entry.key}">
							<c:forEach items="${entry.value}" var="item" varStatus="itemStatus">
								<c:if test="${itemStatus.index % 4 eq 0}">
									<div class="col-sm-4">
								</c:if>
								
								<c:choose>
									<c:when test="${item.multiSelect || item.properties.selected}">
										<div id="checkbox_${modal_key}_${status.index}_${itemStatus.index}">
											<label class="${item.properties.selected ? 'checked':''}">
												<c:choose>
													<c:when test="${item.properties.selected}">
														<input type="checkbox" value="" checked
															data-event-change-id="global234" data-event-change-param0="${item.properties.dimValId}" >
													</c:when>
													<c:otherwise>
														<input type="checkbox" value=""
															data-event-change-id="global235" data-event-change-param0="${item.properties.dimValId}" >
													</c:otherwise>
												</c:choose>
												<span class="checkbox-label">${item.label}&nbsp;${item.properties['category.description']}</span>
											</label>
										</div>
									</c:when>
									<c:otherwise>
										<c:set var="uri" value="${vsg_utils:getUrlForAction(item)}"/>
										<div class="radio">
											<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel4">
												<input class="sr-only" name="radio1_${modal_key}" 
													data-event-change-id="global236" data-event-change-param0="${uri}" data-event-change-param1="${modal_key}" type="radio" value="delivery-method-pick-up">
													${item.label}&nbsp;${item.properties['category.description']}
											</label>
										</div>
									</c:otherwise>
								</c:choose>
								
								
								<c:if test="${(itemStatus.index + 1) % 4 eq 0 || itemStatus.index + 1 eq fn:length(entry.value)}">
									</div>
								</c:if>
							</c:forEach>
						</div>
					</c:forEach>
					</div>
					<c:choose>
						<c:when test="${multiSelect}">
							<button class="btn btn-primary" data-dismiss="modal" data-event-click-id="global232" >Select</button>
						</c:when>
						<c:otherwise>
							<button class="btn btn-primary" data-dismiss="modal" id="select-single-button-${modal_key}">Select</button>
						</c:otherwise>
					</c:choose>
						
					<a href="javascript:viod(0);" class="btn btn-info" id="view-all-options-close" data-dismiss="modal" data-event-click-id="global233" data-event-click-param0="${currentUrl}" >Close</a>
				</div>
			</div>
		</div>
	</div>
	
</dsp:page>