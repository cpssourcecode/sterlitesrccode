<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/cps/commerce/order/ApproverOrderLookup"/>

	<dsp:getvalueof var="orderId" param="orderId"/>
	
	<input type="hidden" value="${orderId}" id="hiddenOrderId"/>
	
	<a href="#" data-toggle="modal" data-target="#modalSavedCart${orderId}" style="display: none;"
	   id="link-modalSavedCart${orderId}">
	</a>
	
 	<div class="modal fade in" id="modalSavedCart${orderId}" tabindex="-1" role="dialog"
 			aria-labelledby="modalViewOrder${orderId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-wide" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
				</div>
				<div class="modal-body" id="modal_body">
					<h4 class="modal-title" id="modal-order-title">Saved Cart</h4>
					
					<dsp:droplet name="ApproverOrderLookup">
						<dsp:param name="orderId" param="orderId"/>
						<dsp:oparam name="output">
							<dsp:setvalue param="order" paramvalue="result"/>
	
							<div class="row row-narrow">
								<div class="col-sm-8">
									<!-- Addresses -->
									<div class="well">
										<div class="row row-narrow">
											<!-- Shipping address -->											
											<div class="col-sm-6">
												<dsp:getvalueof var="shippingGroup" param="order.shippingGroups[0]"/>
												<c:choose>
													<c:when test="${shippingGroup.shippingGroupClassType == 'inStorePickupShippingGroup'}">
														<h4 class="text-nocase">Pick up in store</h4>
														<p>													
															<dsp:tomap var="store" value="${shippingGroup.store}"/>
															<address>
																<c:if test="${!empty store}">
																	${store.address1}<br/>
																	<c:if test="${!empty store.address2}">
																		${store.address2}<br/>
																	</c:if>	

																	<c:if test="${not empty store.city || not empty store.stateAddres || not empty store.postalCode}">
																		${store.city},&nbsp;${store.stateAddres}&nbsp;${store.postalCode}<br/>
																	</c:if>
																</c:if>
															</address>
														</p>
													</c:when>
													<c:otherwise>
														<h4 class="text-nocase">Ship to Address</h4>
														<p>
															<dsp:getvalueof var="shippingAddress" value="${shippingGroup.shippingAddress}"/>
															<address>
																<c:if test="${!empty shippingAddress}">
																	${shippingAddress.address1}<br/>
																	<c:if test="${!empty shippingAddress.address2}">
																		${shippingAddress.address2}<br/>
																	</c:if>
																	<c:if test="${not empty shippingAddress.city || not empty shippingAddress.state || not empty shippingAddress.postalCode}">
																		${shippingAddress.city},&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}<br/>
																	</c:if>
																</c:if>
															</address>
														</p>
													</c:otherwise>
												</c:choose>
											
											</div>
											<!-- Billing address -->
											<div class="col-sm-6">
												<h4 class="text-nocase">Billing Address</h4>
												<dsp:getvalueof var="paymentGroup" param="order.paymentGroups[0]"/>
												<dsp:getvalueof var="billingAddress" value="${paymentGroup.billingAddress}"/>
												<p>
													<address>	
														<c:if test="${not empty billingAddress}">
															<p>
																<c:if test="${!empty billingAddress.companyName}">${billingAddress.companyName}<br/></c:if>
																${billingAddress.address1}<br/>
																<c:if test="${!empty billingAddress.address2}">
																	${billingAddress.address2}<br/>
																</c:if>

																<c:if test="${not empty billingAddress.city || not empty billingAddress.state || not empty billingAddress.postalCode}">
																	${billingAddress.city},&nbsp;${billingAddress.state}&nbsp;${billingAddress.postalCode}<br/>
																</c:if>
															</p>
														</c:if>
													</address>
												</p>
											</div>
										</div>
									</div>
								</div>


								<div class="col-sm-4">
									<div class="well well-grey">
										<h4 class="text-nocase">Summary</h4>
										<dl class="row row-narrow">
											<dt class="col-sm-12 col-xs-3">PO #</dt>
											<dd class="col-sm-12 col-xs-9">
												<c:choose>
													<c:when test="${paymentGroup.paymentGroupClassType eq 'invoiceRequest'}">
														<dsp:valueof value="${paymentGroup.PONumber}"/>
													</c:when>
													<c:otherwise>
														&nbsp;
													</c:otherwise>
												</c:choose>
												<dsp:getvalueof var="orderItems" param="order.commerceItems"/>
											</dd>
											
											<dt class="col-sm-12 col-xs-3"># Items</dt>
											<dd class="col-sm-12 col-xs-9"><dsp:valueof param="order.totalCommerceItemCount"/></dd>
											
											<dt class="col-sm-12 col-xs-3">Subtotal</dt>
											<dd class="col-sm-12 col-xs-9"><dsp:valueof param="order.priceInfo.rawSubtotal" converter="currencyConversion"/></dd>
										</dl>
									</div>
								</div>
							</div>
							
							<table class="table table-striped table-mobile" id="saved-cart-tbl">
								<thead>
								<tr>
									<th style="width: 14.1%">&nbsp;</th>
									<th style="width: 18.5%">Item Name</th>
									<th style="word-break: inherit; width: 15.4%" id="sort-item">
										<a href="#">Item # <i id="sort-item-i" class="fa fa-sort"></i></a>
									</th>
									<th class="text-center" style="width: 8%">Qty</th>
									<th style="width: 13.1%" id="sort-price">
										<a href="#">Price <i id="sort-price-i" class="fa fa-sort"></i></a>
									</th>
									<th style="width: 12.1%">Total</th>
									<th class="text-center" style="width: 17.8%">Availability</th>
								</tr>
								</thead>
							 	<tbody>
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="order.commerceItems"/>
										<dsp:param name="elementName" value="item"/>
										<dsp:oparam name="output">
											<tr>
												<dsp:tomap var="productMap" param="item.auxiliaryData.productRef"/>
												<dsp:tomap var="commerceItem" param="item"/>
												<dsp:getvalueof var="skuId" value="${commerceItem.catalogRefId}"/>
												<dsp:getvalueof var="eCommerceDisplay" param="item.auxiliaryData.productRef.parentCategory.eCommerceDisplay"/>
												<dsp:getvalueof var="imageUrl" vartype="java.lang.String"
																value="${empty productMap.thumbnail_url ? '/assets/images/plp-placeholder.png' : productMap.thumbnail_url}"/>
												<%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String"
																value="${originatingRequest.contextPath}/catalog/pdp.jsp?prodId=${skuId}"/> --%>
												<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
													<dsp:param name="prodId" value="${productMap.id}"/>
													<dsp:oparam name="output">
														<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
													</dsp:oparam>
												</dsp:droplet>
		
												<td class="text-center">
													<cp:readerimg src="${imageUrl}" alt="" style="max-height: 60px; max-width: 60px" />
												</td>
												<td><span class="caption">Item Name</span>
													<dsp:valueof value="${productMap.description}" converter="valueishtml"/>
												</td>
												<td><span class="caption">Item #</span>
													<dsp:valueof value="${productMap.displayName}"/>
												</td>
												<td class="text-center text-left-xs"><span class="caption">Qty</span>
													<dsp:valueof value="${commerceItem.quantity}"/>
												</td>
												<td><span class="caption">Price</span>
													<dsp:valueof value="${commerceItem.priceInfo.listPrice}" converter="currencyConversion"/>
												</td>
												<td>
													<span class="caption">Total</span>
													<dsp:valueof value="${commerceItem.priceInfo.amount}" converter="currencyConversion"/>
												</td>												
												<td class="text-center"><span class="caption">Availability</span>
													<%-- <dsp:valueof value="${skuId  % 5 }" /> --%>
													<dsp:droplet name="/cps/droplet/CPSInventoryDroplet">
														<dsp:param name="productId" value="${skuId}"/>
														<dsp:oparam name="output">
															<dsp:getvalueof var="quantityAvailable" param="quantityAvailable"/>
															<dsp:getvalueof var="productDisabled" value="${false}"/>
															<dsp:getvalueof var="stockingType" param="productMap.stockingType"/>
															<c:if test="${stockingType == 'O' || stockingType == 'U' || stockingType == 'K' || stockingType == 'X'}">
																<dsp:getvalueof var="productDisabled" value="${true}"/>
															</c:if>															
															<c:choose>
																<c:when test="${not eCommerceDisplay}">
																	<span class="text-danger">No Longer Offered</span>
																</c:when>
																<c:when test="${productDisabled}">
																	<span class="text-danger">
																		Unavailable
																	</span>
																</c:when>
																<c:otherwise>
																	<span>${quantityAvailable < 0 ? 0 : quantityAvailable}</span>
																</c:otherwise>
															</c:choose>
														</dsp:oparam>
													</dsp:droplet>
												</td>
											 </tr>
											
										</dsp:oparam>
									</dsp:droplet>
								</tbody>
							</table>
					
						</dsp:oparam>
					</dsp:droplet>
					<p>
						<a href="#" class="btn btn-lg btn-info" data-dismiss="modal">Close</a>
					</p>
				</div>
			</div>
		</div>
	</div>



</dsp:page>
