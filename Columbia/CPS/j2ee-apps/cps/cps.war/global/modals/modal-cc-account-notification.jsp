<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isCCAccountAgreed" param="isAgreed"/>
	<dsp:getvalueof var="isCCAccountModalShown" param="isCCAccountModalShown"/>
	<c:if test="${not empty isCCAccountAgreed}">
		<dsp:setvalue value="${isCCAccountAgreed}" bean="Profile.isCCAccountAccepted"/>
		<dsp:setvalue value="${isCCAccountModalShown}" bean="Profile.showCCNotification"/>
	</c:if>
	<dsp:getvalueof var="isAcceptedCCPayment" bean="Profile.isCCAccountAccepted" />
	<dsp:getvalueof var="isCCNotificationShown" bean="Profile.showCCNotification" />
	<input type="hidden" value="${isAcceptedCCPayment}" id="isAcceptedCCPayment">
	<input type="hidden" value="${isCCNotificationShown}" id="isCCNotificationShown">
	
	<div class="modal fade" id="ccAccountNotificationModal" tabindex="-1" role="dialog" aria-labelledby="ccAccountNotificationTitle" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<span class="modal-icon">
						<i class="fa fa-info"></i>
					</span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-cc-notification-title"><fmt:message key="cc.account.notification.header" /></h4>
					<p><cp:repositoryMessage key="ccAccountNotificationMsg" /></p>
					<hr>
					<button type="button" id="ccNotificationSubmit" data-event-click-id="ccNotificationSubmit"  class="btn btn-primary" data-dismiss="modal">I Agree</button>
					<a href="#" class="btn btn-info" data-dismiss="modal"><fmt:message key="global.modal.cancel"/></a>
				</div>
			</div>
		</div>
	</div>
</dsp:page>