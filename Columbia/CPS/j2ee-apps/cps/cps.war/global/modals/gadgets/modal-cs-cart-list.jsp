<dsp:page>


	<dsp:getvalueof var="defaultCsId" param="cs"/>

	<dsp:droplet name="/cps/droplet/CSSelectionDroplet">
		<dsp:param name="filter" param="filter"/>
		<dsp:oparam name="output">
			<div class="modal-shipping-list">
				<div class="list-header row row-flush">
					<div class="col-xs-2 text-center">Select</div>
					<div class="col-xs-10">Ship To Address</div>
				</div>
				<ul class="list">
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" param="associatedCS"/>
						<dsp:param name="elementName" value="cs"/>
						<dsp:oparam name="output">
						<dsp:getvalueof var="address1" param="cs.address1"/>
						<dsp:getvalueof var="address2" param="cs.address2"/>
						<dsp:getvalueof var="address3" param="cs.address3"/>
						<dsp:getvalueof var="city" param="cs.city"/>
						<dsp:getvalueof var="state" param="cs.state"/>
						<dsp:getvalueof var="zip" param="cs.postalCode"/>
						<dsp:getvalueof var="csId" param="cs.id"/>

					<c:if test="${defaultCsId ne csId}">
						<li class="list-item row row-flush">
							<label name="addressSelectRadio" value="${csId}" data-event-click-id="global253" >

								<div class="col-xs-2 text-center"><input type="radio" name="shipto"></div>
								<div class="col-xs-10">${address1}</br>
								<c:if test="${not empty address2}">
									${address2}</br>
								</c:if>
								${city},&nbsp;${state}&nbsp;${zip}
								</div>
							</label>
						</li>
					</c:if>
					</dsp:oparam>
					</dsp:droplet>
				</ul>
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>