<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>
	<dsp:getvalueof var="leadTimeThreshold" bean="/cps/droplet/CPSInventoryDroplet.leadTimeThreshold"/>
	<dsp:getvalueof var="info" param="info"/>
	<dsp:getvalueof var="index" param="index"/>
	<dsp:getvalueof var="isHead" param="isHead"/>
	<dsp:getvalueof var="qty" param="qty"/>
	<dsp:getvalueof var="productId" param="productId"/>
	
	<dsp:droplet name="ProductLookup">
                <dsp:param name="id" value="${productId}"/>
                <dsp:setvalue param="product" paramvalue="element"/>
                <dsp:oparam name="output">
                    <dsp:getvalueof var="productPurchasable" param="product.productPurchasable"/>
                    <dsp:getvalueof var="stockingType" param="product.stockingType"/>
                  </dsp:oparam>
    </dsp:droplet>
					<dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType)}"/>            
	<dsp:droplet name="/cps/droplet/LocationByBranchLookupDroplet">
		<dsp:param name="branchId" value="${info.branchId}" />
		<dsp:oparam name="output">
			<dsp:tomap var="location" param="store" />
			<c:if test="${!empty location}">
				<div class="row row-flush">
					<div class="col-xs-1 text-center">
						<label><input name="pickupLoc" type="radio"></label>
					</div>
					<div class="col-xs-3">
						<c:choose>
							<c:when test="${info.quantity > 0}">
								Available: ${vsg_utils:escapeHtml(info.quantity)}
							</c:when>
							<c:otherwise>
								<dsp:valueof value="${info.leadTime}"/> business days
							</c:otherwise>
						</c:choose>
					</div>
					<div class="col-xs-6">
			
									<address>
										${location.address1}&nbsp;${location.address2}<br />
										${location.city},&nbsp;${location.stateAddress}&nbsp;${location.postalCode}
										<br>
									</address>
			
					</div>
					<div class="col-xs-2">
						<c:if test="${!empty location}">
							<dsp:getvalueof var="distance" param="distances.${location.locationId}"/>
							<c:if test="${!empty distance}">
								${distance} Miles
							</c:if>
						</c:if>
					</div>
				</div>
				<c:if test="${productPurchasable && !isProductPurchasableStockingType}">
					<div class="row row-narrow pt10">
						<div class="col-sm-2 col-xs-3">
							<input type="text" class="form-control input-lg text-center" id="addToCartFromModalQty-${index}" placeholder="Qty" value="${qty}" maxlength="5"
							data-event-keydown-id="global251"  data-event-change-id="global252" >
						</div>
						<div class="col-sm-10 col-xs-9">
								<a href="#" class="btn btn-lg xs-block btn-primary" data-event-click-id="global250" data-event-click-param0="${vsg_utils:escapeHtml(info.productId)}" data-event-click-param1="${index}" data-event-click-param2="${location.repositoryId}" >Add to Cart</a>
						</div>
					</div>
				</c:if>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>