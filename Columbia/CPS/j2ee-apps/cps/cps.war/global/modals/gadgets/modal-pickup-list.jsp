<dsp:page>
    <dsp:importbean bean="/cps/droplet/LocationLookupDroplet"/>
    <dsp:getvalueof var="defaultStoreId" param="storeid"/>


    <%--<div class="form-group">--%>
    <dsp:droplet name="LocationLookupDroplet">
        <dsp:param name="filter" param="filter"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="locationMap" param="locationMap"/>
            <ul class="list">
                <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                    <dsp:param name="array" value="${locationMap}"/>
                    <dsp:param name="sortProperties" value="_key"/>
                    <dsp:setvalue param="storeList" paramvalue="element"/>
                    <dsp:oparam name="output">
                        <dsp:getvalueof var="index" param="index"/>
                        <dsp:getvalueof var="region" param="key"/>
                            <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                                <dsp:param name="array" param="storeList"/>
                                <dsp:oparam name="output">

                                    <dsp:getvalueof var="address1" param="element.address1"/>
                                    <dsp:getvalueof var="address2" param="element.address2"/>
                                    <dsp:getvalueof var="address3" param="element.address3"/>
                                    <dsp:getvalueof var="city" param="element.city"/>
                                    <dsp:getvalueof var="stateAddress" param="element.stateAddress"/>
                                    <dsp:getvalueof var="postalCode" param="element.postalCode"/>
                                    <dsp:getvalueof var="storeItem" param="element"/>


                                    <c:if test="${defaultStoreId ne storeItem.repositoryId}">

                                        <li class="list-item row row-flush">

                                            <label>
                                                <div class="col-xs-2 text-center">
                                                    <input type="radio" name="addressSelectRadio"
                                                           value="${storeItem.repositoryId}"
                                                           data-event-click-id="global255" >
                                                </div>
                                                <div class="col-xs-10">${region}<br/>
                                                        ${address1}<br/>
                                                    <c:if test="${not empty address2}">${address2}<br/></c:if>
                                                    <c:if test="${not empty address3}">${address3}<br/></c:if>
                                                        ${city},&nbsp;${stateAddress}&nbsp;${postalCode}</div>
                                            </label>
                                        </li>
                                    </c:if>
                                </dsp:oparam>
                            </dsp:droplet>

                    </dsp:oparam>
                </dsp:droplet>
            </ul>
        </dsp:oparam>
    </dsp:droplet>

    <%--</div>--%>


</dsp:page>