<dsp:page>
    <dsp:getvalueof var="companyId" param="companyId"/>
    <dsp:getvalueof var="userId" param="userId"/>
    <dsp:getvalueof var="searchTerm" param="searchTerm"/>

    <div class="list-header row row-flush">
        <div class="col-xs-2 text-center"><label class="mb0">
            <input type="checkbox" data-event-change-id="global246" > All</label>
        </div>
        <div class="col-xs-10">Ship To Address</div>
    </div>
    <dsp:droplet name="/cps/droplet/ManageUsersShipping">
        <dsp:param name="companyId" value="${companyId}"/>
        <dsp:param name="userId" value="${userId}"/>
        <dsp:param name="searchTerm" value="${searchTerm}"/>
        <dsp:param name="isModal" value="true"/>
        <dsp:oparam name="output">
            <ul class="list">
                <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                    <dsp:param name="array" param="currentList"/>
                    <dsp:param name="elementName" value="modalCS"/>
                    <dsp:oparam name="output">
                        <dsp:getvalueof var="modalCsId" param="modalCS.id"/>
                        <dsp:getvalueof var="modalAddress1" param="modalCS.addressItem.address1"/>
                        <dsp:getvalueof var="modalAddress2" param="modalCS.addressItem.address2"/>
                        <dsp:getvalueof var="modalAddress3" param="modalCS.addressItem.address3"/>
                        <li class="list-item row row-flush">
                            <label>
                                <div class="col-xs-2 text-center">
                                    <input type="checkbox" name="modal-check" value="${modalCsId}">
                                </div>
                                <div class="col-xs-10">
                                        ${modalAddress1} <br> ${modalAddress2} <br>
                                        ${modalAddress3}
                                </div>
                            </label>
                        </li>
                    </dsp:oparam>
                </dsp:droplet>
            </ul>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>