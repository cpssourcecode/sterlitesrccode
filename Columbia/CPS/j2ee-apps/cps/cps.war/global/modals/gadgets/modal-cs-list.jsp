<dsp:page>
    <dsp:getvalueof var="defaultCsId" param="cs" />
    <dsp:droplet name="/cps/droplet/CSSelectionDroplet">
        <dsp:param name="filter" param="filter" />
        <dsp:param name="orgId" param="orgId" />
        <dsp:oparam name="output">
            <dsp:getvalueof var="showBillingAccount" param="showBillingAccount"/>
            <div class="modal-shipping-list">
                <div class="list-header row row-flush">
                    <div class="col-xs-2 text-center">Select</div>
                    <div class="col-xs-4">Ship To Address</div>
                    <div class="col-xs-3">Account Number</div>
                    <c:if test="${showBillingAccount}">
                        <div class="col-xs-3">Billing Account</div>
                    </c:if>
                </div>
                <ul class="list">
                    <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                        <dsp:param name="array" param="associatedCSData" />
                        <dsp:param name="elementName" value="cs" />
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="address1" param="cs.addressItem.address1" />
                            <dsp:getvalueof var="address2" param="cs.addressItem.address2" />
                            <dsp:getvalueof var="address3" param="cs.addressItem.address3" />
                            <dsp:getvalueof var="city" param="cs.addressItem.city" />
                            <dsp:getvalueof var="state" param="cs.addressItem.state" />
                            <dsp:getvalueof var="zip" param="cs.addressItem.postalCode" />
                            <dsp:getvalueof var="csId" param="cs.addressItem.id" />
                            <dsp:getvalueof var="orgId" param="cs.organization.id"/>
                            <dsp:getvalueof var="orgName" param="cs.organization.name"/>
                            <dsp:getvalueof var="billingAddressId" param="cs.organization.billingAddress.id"/>
							<dsp:getvalueof var="derivedBillingAddressId" param="cs.organization.derivedBillingAddress.id"/>
                            <c:if test="${defaultCsId ne csId}">
                                <li class="list-item row row-flush">
                                    <label id="addressSelectRadio${csId}" value="${csId}" data-event-click-id="global254" >
                                        <div class="col-xs-2 text-center">
                                            <input type="radio" name="shipto">
                                        </div>
                                        <div class="col-xs-4">${address1}</br>
                                            <c:if test="${not empty address2}">
                                                ${address2}</br>
                                            </c:if>
                                            ${city},&nbsp;${state}&nbsp;${zip}
                                        </div>
                                        <%-- SM-188 Display account number on ship to address table --%>
                                        <c:choose>
                                        	<c:when test="${showBillingAccount}">
                                        		<%-- <div class="col-xs-3">${billingAddressId}-${csId}</div> --%>                                        		
                                            	<c:choose>
													<c:when test="${not empty billingAddressId}">
														<div class="col-xs-3">${billingAddressId}-${csId}</div>
													</c:when>
													<c:otherwise>
														<div class="col-xs-3">${derivedBillingAddressId}-${csId}</div>
													</c:otherwise>
												</c:choose>
												<div class="col-xs-3">${orgName}</div>
                                        	</c:when>
                                        	<c:otherwise>
                                        		<div class="col-xs-3">${csId}</div>
                                        	</c:otherwise>
                                        </c:choose>                                        
                                    </label>
                                </li>
                            </c:if>
                        </dsp:oparam>
                    </dsp:droplet>
                </ul>
            </div>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>
