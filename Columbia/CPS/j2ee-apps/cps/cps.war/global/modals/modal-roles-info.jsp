<dsp:page>
    <div class="modal fade in" id="rolesInfoModal" tabindex="-1" role="dialog" aria-labelledby="modal-roles-info-title" style="display: none; padding-right: 17px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                    <span class="modal-icon"><i class="fa fa-info-circle"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-roles-info-title">Role Descriptions</h4>
                    <div class="row">
						<div class="col-md-3 col-xs-12"><strong>Administrator</strong></div>
						<div class="col-md-9 col-xs-12">
							<span>
							<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
								<dsp:param name="id" value="infoRoleAdministrator" />
								<dsp:param name="elementName" value="message"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="message" param="message.message"/>
								</dsp:oparam>
								<dsp:oparam name="empty">
									<dsp:getvalueof var="message" value="Users with the administrator role will have full access to our site's features. Administrators
	can make purchases, setup and manage users, view Invoices, statements, packing slips, order
	statuses and more."/>
								</dsp:oparam>
							</dsp:droplet>
							${message}
							</span>
						</div>
                    </div>
                    <br/>
                    <div class="row">
                    	<div class="col-md-3 col-xs-12"><strong>Buyer</strong></div>
						<div class="col-md-9 col-xs-12">
							<span>
							<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
								<dsp:param name="id" value="infoRoleBuyer" />
								<dsp:param name="elementName" value="message"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="message" param="message.message"/>
								</dsp:oparam>
								<dsp:oparam name="empty">
									<dsp:getvalueof var="message" value="Users with the administrator role will have full access to our site's features. Administrators
	can make purchases, setup and manage users, view Invoices, statements, packing slips, order
	statuses and more."/>
								</dsp:oparam>
							</dsp:droplet>
							${message}
							</span>
						</div>
                    </div>
                    <br/>
                    <div class="row">
                    	<div class="col-md-3 col-xs-12"><strong>Approver</strong></div>
						<div class="col-md-9 col-xs-12">
							<span>
							<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
								<dsp:param name="id" value="infoRoleApprover" />
								<dsp:param name="elementName" value="message"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="message" param="message.message"/>
								</dsp:oparam>
								<dsp:oparam name="empty">
									<dsp:getvalueof var="message" value="Users with the administrator role will have full access to our site's features. Administrators
	can make purchases, setup and manage users, view Invoices, statements, packing slips, order
	statuses and more."/>
								</dsp:oparam>
							</dsp:droplet>
							${message}
							</span>
						</div>
                    </div>
                    <br/>
                    <div class="row">
                    	<div class="col-md-3 col-xs-12"><strong>Finance</strong></div>
						<div class="col-md-9 col-xs-12">
							<span>
							<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
								<dsp:param name="id" value="infoRoleFinance" />
								<dsp:param name="elementName" value="message"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="message" param="message.message"/>
								</dsp:oparam>
								<dsp:oparam name="empty">
									<dsp:getvalueof var="message" value="Users with the administrator role will have full access to our site's features. Administrators
	can make purchases, setup and manage users, view Invoices, statements, packing slips, order
	statuses and more."/>
								</dsp:oparam>
							</dsp:droplet>
							${message}
							</span>
						</div>
                    </div>
                    <br/>

                    <div>
                        <button class="btn btn-lg btn-info center-block" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</dsp:page>