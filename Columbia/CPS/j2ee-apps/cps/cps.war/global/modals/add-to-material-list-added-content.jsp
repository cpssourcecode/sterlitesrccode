<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<dsp:getvalueof var="prodId" param="prodId"/>
	<dsp:getvalueof var="giftListId" param="giftListId"/>
	<dsp:getvalueof var="giftItemId" param="giftItemId"/>
	<dsp:getvalueof var="giftItemQty" param="qty"/>

	<dsp:droplet name="/atg/targeting/RepositoryLookup">
		<dsp:param name="id" value="${giftListId}"/>
		<dsp:param name="repository" bean="/atg/commerce/gifts/Giftlists"/>
		<dsp:param name="itemDescriptor" value="gift-list"/>
		<dsp:param name="elementName" value="giftList"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="giftListName" param="giftList.eventName"/>
		</dsp:oparam>
	</dsp:droplet>

	<h2 class="modal-title">
		1 Item Added to Material List
	</h2>

	<dsp:droplet name="/atg/targeting/RepositoryLookup">
		<dsp:param name="id" value="${prodId}"/>
		<dsp:param name="repository" bean="/atg/commerce/catalog/ProductCatalog"/>
		<dsp:param name="itemDescriptor" value="product"/>
		<dsp:param name="elementName" value="product"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="product" param="product"/>

			<div class="media media-product">
				<div class="media-left">
					 <div class="image">
						 <dsp:getvalueof var="imageUrl" vartype="java.lang.String" param="product.thumbnail_url"/>
						 <c:set var="imageUrl" value="${empty imageUrl ? '/assets/images/plp-placeholder.png' : imageUrl}"/>
						 <a href="#">
							 <cp:readerimg iclass="media-object" src="${imageUrl}" alt="" style="max-width: 85px;"/>
						 </a>
					 </div>
				</div>
				<div class="media-body">
					<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
						<dsp:param name="prodId" value="${prodId}"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
						</dsp:oparam>
					</dsp:droplet>
					<p>Added to Material List: <strong><dsp:valueof value="${giftListName}"/></strong></p>
					<h4 class="productTitleName">
						<dsp:a href="${productUrl}">
							<dsp:valueof param="product.description" converter="valueishtml"/>
						</dsp:a>
					</h4>
					<p>
						<dsp:getvalueof var="vendorItem" param="product.vendorItem"/>
						Mfr. # ${vsg_utils:escapeHtml(vendorItem)}<br>
						Item # <dsp:valueof param="product.displayName" valueishtml="true"/><br>
						<dsp:getvalueof var="pricesvc_upc" param="product.pricesvc_upc"/>
						<c:if test="${not empty pricesvc_upc}">
							UPC # ${vsg_utils:escapeHtml(pricesvc_upc)}<br>
						</c:if>
						<dsp:droplet name="/cps/droplet/AliasNumberLookup">
							<dsp:param name="skuId" param="product.id"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="prodAlias" param="alias"/>
                    			<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
								Customer Alias: <dsp:valueof value="${alias}"/>
							</dsp:oparam>
						</dsp:droplet>
					</p>
					<p>Qty: ${giftItemQty}</p>
					<dsp:a href="/account/material-detail.jsp?giftlistId=${vsg_utils:escapeHtml(giftListId)}" iclass="btn btn-link btn-nopadding btn-underline">
						View Material List
					</dsp:a>
				</div>
			</div>

		</dsp:oparam>
	</dsp:droplet>

</dsp:page>