<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="modal-title-error" aria-hidden="true">
    <div class="modal-dialog modal-narrow" role="dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                <span class="modal-icon"><i class="fa fa-info"></i></span>
            </div>
            <div class="modal-body">
                <h4 id="modal-title-error" class="modal-title"></h4>
                <button class="btn btn-info" data-dismiss="modal"><fmt:message key="modal.cart.button.okay"/></button>
            </div>
        </div>
    </div>
</div>
