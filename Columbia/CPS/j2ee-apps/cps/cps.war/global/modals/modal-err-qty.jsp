<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>

	<div class="modal fade" id="alertQuickAddQty" tabindex="-1" role="dialog" aria-labelledby="modal-error-title">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<span class="modal-icon"><i class="fa fa-exclamation-triangle" style="padding-top: 3px;"></i></span>
				</div>
				<div class="modal-body">
					
					<br>
					<p>
						<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="err_qty_erree"/>
								<dsp:param name="notWrap" value="true"/>
							</dsp:include>
					</p>
					
					<div class="text-center"><button class="btn btn-primary" data-dismiss="modal"><fmt:message key="modal.cart.button.okay"/></button></div>
				</div>
			</div>
		</div>
	</div>
	
</dsp:page>
