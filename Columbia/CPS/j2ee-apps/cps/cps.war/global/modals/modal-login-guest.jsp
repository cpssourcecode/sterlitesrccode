<dsp:page>

    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
    <dsp:importbean bean="/cps/userprofiling/util/LoginSessionInfo"/>
    <dsp:importbean bean="/cps/userprofiling/ForgotPasswordFormHandler"/>
    <dsp:importbean bean="/atg/userprofiling/ProfileTools"/>
    <dsp:getvalueof var="allowNewUserSelfRegistration" bean="ProfileTools.allowNewUserSelfRegistration" />

    <div class="modal fade" id="logIn-guest" tabindex="-1" role="dialog" aria-labelledby="modal-login-title">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <span class="modal-icon"><i class="icon icon-user"></i></span>
                </div>
                <div class="modal-body modal-body-guest">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 bg-gray col-container">
                            <h4 class="modal-title">Registered User</h4>
                            <p>Login to checkout with your account information.</p>
                            <div class="alert alert-danger" style="display:none;" id="error-messages-guest"></div>
                            <div class="alert bg-success" style="display:none;" id="modal-success-messages-guest"></div>
                            <dsp:form action="${originatingRequest.contextPath}/" method="post" id="login-form-guest" formid="login-form-guest" iclass="form login-form">
                                <dsp:droplet name="/cps/droplet/RememberMeDroplet">
                                    <dsp:oparam name="output">
                                        <dsp:getvalueof var="email" param="email"/>
                                        <dsp:getvalueof var="remember" param="isRemember"/>
                                    </dsp:oparam>
                                </dsp:droplet>
                                <fmt:message key="account.fields.email" var="emailText"/>
                                <fmt:message key="account.fields.password" var="passwordText"/>

                                <div class="form-group">
                                    <label class="control-label sr-only"><fmt:message key="account.fields.emailAddress"/></label>
                                    <dsp:input iclass="form-control input-lg" id="input-email-guest"
                                               bean="ProfileFormHandler.value.login" type="email" maxlength="100" value="${email}">
                                        <dsp:tagAttribute name="placeholder" value="${emailText}"/>
                                    </dsp:input>
                                </div>
                                <div class="form-group">
                                    <label class="control-label sr-only"><fmt:message key="account.fields.password"/></label>
                                    <dsp:input iclass="form-control input-lg" id="input-password-guest"
                                               bean="ProfileFormHandler.value.password" type="password" autocomplete="new-password" maxlength="15">
                                        <dsp:tagAttribute name="placeholder" value="${passwordText}"/>
                                    </dsp:input>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pl0">
                                        <div class="checkbox mb15">
                                            <label>
                                                <dsp:input id="input-rememberMe-guest" bean="ProfileFormHandler.value.rememberMe" value="true" type="checkbox" checked="${remember}"/>
                                                Remember My Log In
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pr0 text-right">
                                        <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#forgotPasswordModal">Forgot your password?</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="login-form-captcha-guest" style="display: none">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div id="recaptchaDefault-guest"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary btn-lg mb15" id="submitLoginForm-guest" type="button" data-suffix-id="-guest" data-event-click-id="global211">Log In</button>
                                <dsp:input type="hidden" bean="ProfileFormHandler.login" value="true" priority="-10"/>
                            </dsp:form>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-container">
                            <h4 class="modal-title">Guest</h4>
                            <c:if test="${allowNewUserSelfRegistration}">
	                            <p class="mt30 mb10">Register now for faster checkout and get access to order history, account information, product list and more.</p>
	                            <a href data-event-click-id="eventGoToRegistrationPage" class="btn btn-primary btn-lg mb30" type="button">Register Now</a>
                            </c:if>
                            <p class="mt20">Continue checkout as a guest and register next time.</p>
                            <button id="continueAsGuestButton" class="btn btn-primary btn-lg mb15" type="button">Continue as Guest</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</dsp:page>

<script type="text/javascript" nonce="${requestScope.nonce}">
    $('#logIn-guest input').on('input', function(e) {
        checkEnableLoginButton('guest');
    });

    // catch enter click and trigger correct button click instead of form submit
    $('#logIn-guest input').keypress(function (e) {
        if (e.which == 13) {
            $('#submitLoginForm-guest:enabled').click();
            return false;
        }
    });

    window.continueAsGuestCallback = function() { };
    $("#continueAsGuestButton").click(function(){
        $("#logIn-guest").modal('hide');
        window.continueAsGuestCallback();
        window.continueAsGuestCallback = function() { };
    });


    function showLoginGuestModal(guestCallback){
        if (typeof guestCallback === 'undefined'){
            guestCallback = function () { };
        }
        window.continueAsGuestCallback = guestCallback;

        var loginGuestModal = $("#logIn-guest");
        loginGuestModal.modal('show');
    }

</script>