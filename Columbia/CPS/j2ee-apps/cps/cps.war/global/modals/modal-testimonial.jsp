<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
	<a href="#" data-toggle="modal" data-target="#testimonialModal" style="display: none;" id="link-testimonialModal"></a>
	<div class="modal fade" id="testimonialModal" tabindex="-1" role="dialog" aria-labelledby="testimonialModal"
		 aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<span class="modal-icon"><i class="icon icon-user"></i></span>
				</div>
				<div class="modal-body">
					<h2 class="modal-title">Message Submitted</h2>

					<div id="password-forgot-form-step-1-div">
						<div class="form-group">
							<p>Thank you for submitting your testimonial</p>
						</div>
						<div class="form-group">
							<button class="btn btn-info" data-dismiss="modal" data-event-click-id="global230" >Okay</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
