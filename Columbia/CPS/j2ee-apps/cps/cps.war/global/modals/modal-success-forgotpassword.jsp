<dsp:page>		
<div class="modal fade in" id="forgotPasswordSuccess" tabindex="-1" role="dialog" aria-labelledby="forgotPasswordSuccessModal" style="display: none; padding-right: 10px;">
		<div class="modal-dialog modal-narrow" >
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-info"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-matnone-title">
						<div class="modal-body">
						<p>Instructions were successfully 
						sent to your email address.</p>						
						<button class="btn btn-info" data-dismiss="modal"><fmt:message key="modal.cart.button.okay"/></button>
					</div>											
					</h4>																	
				</div>
			</div>
		</div>
	</div>
</dsp:page>
