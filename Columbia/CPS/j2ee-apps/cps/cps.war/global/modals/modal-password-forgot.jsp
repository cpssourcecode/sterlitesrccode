<dsp:importbean bean="/cps/userprofiling/ForgotPasswordFormHandler"/>
<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>
    <div class="modal fade in" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="modal-password-title" style="display: none;">
        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <span class="modal-icon"><i class="icon icon-user"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-password-title">Reset Your Password</h4>

                    <div class="alert alert-danger" style="display:none;" id="modal-error-messages"></div>
                    <div class="alert bg-success" style="display:none;" id="modal-success-messages"></div>

                    <div id="password-forgot-form-step-1-div">
                        <dsp:form action="${originatingRequest.contextPath}/global/modal/modal-password-forgot.jsp" method="post" iclass="form login-form"
                                  id="password-forgot-form-step-1" formid="password-forgot-form-step-1">
                            <div class="form-group">
                                <label class="control-label sr-only">Email Address</label>
                                <dsp:input type="email" bean="ForgotPasswordFormHandler.value.email" iclass="form-control input-lg" id="password-forgot-input-email">
                                    <dsp:tagAttribute name="placeholder" value="Email Address"/>
                                </dsp:input>
                            </div>
                            <button type="button" data-event-click-id="global217"  class="btn btn-primary btn-lg btn-block mb15 send_instruction" type="button">Send Instructions</button>
                            <p><a data-dismiss="modal" data-toggle="modal" data-target="#logIn">Log In</a></p>
                            <dsp:input type="hidden" bean="ForgotPasswordFormHandler.value.step" value="1"/>
                            <dsp:input type="hidden" bean="ForgotPasswordFormHandler.forgotPassword" value="true"/>
                        </dsp:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</dsp:page>