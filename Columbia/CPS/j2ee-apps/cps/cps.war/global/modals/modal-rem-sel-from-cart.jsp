<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

<dsp:page>

	<a href="#" data-toggle="modal" data-target="removeFromCart" style="display: none;" id="link-removeFromCart"></a>

	<!-- Remove Selected From Cart -->
	<div class="modal fade in" id="removeFromCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-narrow">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-cartremove-title">Remove Selected From Cart</h4>

					<p>Are you sure you want to remove the selected items from your cart?
						This cannot be undone.</p>

					<button class="btn btn-primary">Confirm</button>
					<button class="btn btn-info" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
