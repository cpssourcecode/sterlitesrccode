<dsp:page>

    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
    <%--<dsp:importbean bean="/atg/userprofiling/Profile"/>--%>

    <dsp:getvalueof var="orderId" param="orderId"/>

    <a href="#" data-toggle="modal" data-target="#modalRejectOrder${orderId}" style="display: none;"
       id="link-modalRejectOrder${orderId}">
    </a>

    <!-- Reject current order -->
    <div class="modal fade in" id="modalRejectOrder${orderId}" tabindex="-1" role="dialog"
         aria-labelledby="modalRejectOrder${orderId}" style="display: none; padding-right: 17px;">
        <div class="modal-dialog modal-narrow" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">x</span></button>
                    <span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
                </div>
                <div class="modal-body">
                    <h4 class="modal-title" id="modal-accountrejectorder-title">Reject Order Request</h4>

                    <p>Are you sure you want to reject this Order Request?</p>

                    <div class="form-group">
                        <textarea id="reject-approver-message" class="form-control"
                                  placeholder="Optional Message (125 Characters Max)"></textarea>
                    </div>

                    <a href="#" class="btn btn-primary" id="modalBtnReject" data-event-click-id="modalBtnReject" data-event-click-param0="${vsg_utils:escapeJS(orderId)}" >
                        <fmt:message key="account.approval.confirm"/>
                    </a>
                    <a href="#" class="btn btn-info" data-dismiss="modal">
                        <fmt:message key="account.approval.cancel"/>
                    </a>
                </div>
            </div>
        </div>
    </div>

</dsp:page>
