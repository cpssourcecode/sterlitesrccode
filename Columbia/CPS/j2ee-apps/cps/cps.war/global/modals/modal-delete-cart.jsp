<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<dsp:getvalueof var="orderId" param="orderId"/>

	<a href="#" data-toggle="modal" data-target="#modalDeleteCart${orderId}" style="display: none;"
	   id="link-modalDeleteCart${orderId}">
	</a>

	<div class="modal fade in" id="modalDeleteCart${orderId}" tabindex="-1" role="dialog" aria-labelledby="modalDeleteCart${orderId}" aria-hidden="true">
		<div class="modal-dialog modal-narrow" role="dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<span class="modal-icon"><i class="glyphicon glyphicon-shopping-cart"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-deletesavedcart-title"><fmt:message key="account.approval.delete_cart"/></h4>
					<p>Are you sure you want to delete this saved cart?</p>
					<a href="#" class="btn btn-primary" data-event-click-id="global199" data-event-click-param0="${vsg_utils:escapeJS(orderId)}" >
						Delete
					</a>
					<a href="#" class="btn btn-info" data-dismiss="modal">
						<fmt:message key="account.approval.cancel"/>
					</a>
				</div>
			</div>
		</div>
	</div>
</dsp:page>
