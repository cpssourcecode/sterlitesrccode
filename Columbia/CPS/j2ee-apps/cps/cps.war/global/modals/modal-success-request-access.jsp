<dsp:page>
	<fmt:bundle basename="web.resources.WebAppResources">
		<div class="modal fade" id="infoRequestAccessSent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<span class="modal-icon"><i class="fa fa-info"></i></span>
					</div>
					<div class="modal-body">
						<h4 class="modal-title" id="modal-text-title"><fmt:message key="modal.request.access"/></h4>
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="infoRequestAccessSent"/>
								<dsp:param name="notWrap" value="true"/>
							</dsp:include>
						</p>
						<button class="btn btn-info" data-dismiss="modal"><fmt:message key="modal.cart.button.okay"/></button>
					</div>
				</div>
			</div>
		</div>
	</fmt:bundle>
</dsp:page>