<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/droplet/UsersAccountCBLookupDroplet"/>

	<dsp:getvalueof var="giftlistId" param="giftlistId"/>

	<a href="#" data-toggle="modal" data-target="#modalShareMaterial${giftlistId}" style="display: none;"
	   id="link-modalShareMaterial${giftlistId}"></a>

	<!-- Share list -->
	<div class="modal fade in" id="modalShareMaterial${giftlistId}" tabindex="-1" role="dialog" aria-labelledby="modalShareMaterial${giftlistId}" style="display: none; padding-right: 17px;">
		<div class="modal-dialog modal-narrow" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="icon icon-user-material"></i></span>
				</div>
				<div class="modal-body">
					<h4 class="modal-title" id="modal-matshare-title"><fmt:message key="account.material.share_material_list"/></h4>
					<div class="form-group">
						<dsp:form action="" method="post" id="shareList${giftlistId}" formid="shareList${giftlistId}">
							<dsp:input type="hidden" bean="GiftlistFormHandler.giftlistId" value="${giftlistId}"/>
							<dsp:input type="hidden" bean="GiftlistFormHandler.emailNotes" value=""/>
		
							<div class="alert alert-danger" style="display:none;" id="error-messages${giftlistId}"></div>
							<!-- <span class="select-wrap"> -->
								<dsp:select id="email" iclass="form-control" bean="GiftlistFormHandler.userId">
									<option value="" disabled selected><fmt:message key="account.material.select"/></option>
									<dsp:droplet name="UsersAccountCBLookupDroplet">
										<dsp:param name="userId" bean="Profile.id"/>
										<dsp:oparam name="empty"></dsp:oparam>
										<dsp:oparam name="output">
											<dsp:getvalueof var="resultMap" vartype="java.util.Map" param="resultMap"/>
											<c:forEach var="user" items="${resultMap}">
												<dsp:option value="${user.key}">${user.value}</dsp:option>
											</c:forEach>
										</dsp:oparam>
									</dsp:droplet>
								</dsp:select>
						<!-- 	</span> -->
		
							<dsp:input type="hidden" bean="GiftlistFormHandler.shareGiftlist" value="true" priority="-10"/>
							
						</dsp:form>
					</div>
					<a href="#" class="btn btn-primary" data-event-click-id="global226" data-event-click-param0="${vsg_utils:escapeJS(giftlistId)}" >
						<fmt:message key="account.material.share"/>
					</a>
					<a href="#" class="btn btn-info" data-dismiss="modal" data-event-click-id="global227" >
					   <fmt:message key="global.modal.cancel"/>
					</a>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
