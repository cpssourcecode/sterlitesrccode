<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/targeting/RepositoryLookup" />
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistLookupDroplet" />
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>

	<dsp:getvalueof var="giftListId" param="giftListId" />
	<dsp:getvalueof var="giftItemIds" param="giftItemIds" />
	<dsp:getvalueof var="itemsCount" param="itemsCount" />
	<dsp:getvalueof var="addedSkuCount" param="addedSkuCount" />

	<dsp:getvalueof var="pdpUrl" vartype="java.lang.String" value="${originatingRequest.contextPath}/catalog/pdp.jsp" />

	<dsp:droplet name="GiftlistLookupDroplet">
		<dsp:param name="id" param="giftListId" />
		<dsp:oparam name="output">
			<dsp:droplet name="IsEmpty">
				<dsp:param name="value" param="element" />
				<dsp:oparam name="false">
					<dsp:setvalue paramvalue="element" param="giftlist" />
					<dsp:getvalueof var="giftListName" param="element.eventName" />

					<h4 class="modal-title" id="modal-cartmatlisted-title">
						${addedSkuCount} Items Added to
						<dsp:a href="/account/material-detail.jsp?giftlistId=${giftListId}">${giftListName}</dsp:a>
					</h4>

					<ul class="item item-matlist">
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="giftItemIds" converter="map" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="giftItemQty" param="element" />
								<dsp:getvalueof var="giftItemId" param="key" />

								<dsp:droplet name="RepositoryLookup">
									<dsp:param name="id" value="${giftItemId}" />
									<dsp:param name="repository" bean="/atg/commerce/gifts/Giftlists" />
									<dsp:param name="itemDescriptor" value="gift-item" />
									<dsp:param name="elementName" value="giftItem" />
									<dsp:oparam name="output">

										<dsp:droplet name="ProductLookup">
											<dsp:param name="id" param="giftItem.productId" />
											<dsp:param name="repository" bean="/atg/commerce/catalog/ProductCatalog" />
											<dsp:param name="itemDescriptor" value="product" />
											<dsp:param name="elementName" value="product" />
											<dsp:oparam name="output">
												<dsp:tomap var="product" param="product" />
												<dsp:getvalueof var="imageUrl" vartype="java.lang.String" value="${empty product.web_url ? '/assets/images/pdp-placeholder.png' : product.web_url}" />


												<li class="item-item row row-narrow">
													<div class="col-xs-4">
														<span class="item-thumb">
															<cp:readerimg src="${imageUrl}" />
														</span>
													</div>
													<div class="col-xs-8">
														<span class="item-meta">
															<span class="item-title">
																<a href="${pdpUrl}?prodId=${product.id}">${product.description}
																	<%-- <c:if test="${!empty product.descriptionLine2 && product.descriptionLine2 != ''}">
																		, ${product.descriptionLine2}
																	</c:if> --%>
																</a>															
															</span>
															<span class="item-price">
																<dsp:droplet name="/cps/droplet/CPSPriceDroplet">
																	<dsp:param name="productId" param="product.id"/>
																	<dsp:param name="profile" bean="Profile"/>
																	<dsp:oparam name="outputPrice">
																		<dsp:valueof param="currentPrice" converter="currencyConversion"/>
																		<dsp:getvalueof var="uomPrimary" param="product.uomPrimary"/>
																		<c:if test="${not empty uomPrimary}">/${uomPrimary}</c:if>
																	</dsp:oparam>
																</dsp:droplet>
															</span>
															<span class="item-sku">Mfr. # ${product.vendorItem}</span>
															<span class="item-sku">Item # ${product.displayName}</span>
															<c:if test="${not empty product.pricesvc_upc}">
																<span class="item-sku">UPC # ${product.pricesvc_upc}</span>
															</c:if>
															<span class="item-sku">Qty: ${giftItemQty}</span>
															</span>
													</div>
												</li>
											</dsp:oparam>
										</dsp:droplet>

									</dsp:oparam>
								</dsp:droplet>

							</dsp:oparam>
						</dsp:droplet>
					</ul>
					<button data-event-click-id="global213" data-event-click-param0="${vsg_utils:escapeHtml(giftListId)}"  class="btn btn-primary">View List</button>
					<button class="btn btn-info" data-dismiss="modal">Close</button>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>