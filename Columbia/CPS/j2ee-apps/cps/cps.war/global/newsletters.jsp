<dsp:page>
    <dsp:importbean bean="/cps/userprofiling/NewsletterSignUpFormHandler"/>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="userId" bean="Profile.id"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:getvalueof var="pEmail" param="email"/>
    <cp:pageContainer page="account">
        <div class="container main-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-9">
                        <div class="page-title">
                            <h1>Newsletter Subscription</h1>
                        </div>
                        <dsp:form action="${originatingRequest.contextPath}" method="post" id="optionsForm">
                        <dsp:droplet name="/cps/servlet/FindSubscriber">
                            <dsp:param name="userId" value="${userId}"/>
                            <dsp:param name="email" value="${pEmail}"/>
                            <dsp:param name="isTransient" value="${isTransient}"/>
                            <dsp:oparam name="error">
                                <dsp:getvalueof var="message" param="message"/>
                                <h2 class="h5">${message}</h2>
                            </dsp:oparam>

                            <dsp:oparam name="output">
                                <dsp:getvalueof var="isNew" param="isNew"/>
                                <dsp:getvalueof var="options" param="options"/>
                                    <h2 class="h5">Receive the latest news and product announcements.</h2>
                                    <ul id="options_list">
                                        <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                                            <dsp:param name="array" value="${options}"/>
                                            <dsp:oparam name="output">
                                                <dsp:getvalueof var="option" param="element"/>
                                                <li>
                                                    <div>
                                                        <dsp:input bean="NewsletterSignUpFormHandler.value.options" type="checkbox" value="${option.id}" checked="${option.checked}" priority="-1">
                                                            <dsp:tagAttribute name="data-event-click-id" value="global16"/>
                                                        </dsp:input>
                                                        <label>${option.description}</label>
                                                    </div>
                                                </li>
                                            </dsp:oparam>
                                        </dsp:droplet>
                                        <c:if test="${isNew != 'true'}">
                                            <h2 class="h5">Unsubscribe</h2>
                                            <li>
                                                <div>
                                                    <input id="optOut" name="optOut" class="checkbox" type="checkbox" value="true" data-event-click-id="global17" >
                                                    <label >Please unsubscribe me from all Applied emails. <br> You will continue to receive emails regarding any orders you place, such as order confirmations and shipping notifications. View our <a class="link inline-text" href="#">&nbsp;&nbsp;privacy policy.</a></label>
                                                </div>
                                            </li>
                                        </c:if>
                                    </ul>
                                    <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.value.user_id" value="${userId}" priority="-2"/>
                                    <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.value.login" value="${pEmail}" priority="-3"/>
                                <c:choose>
                                    <c:when test="${isNew == 'true'}">
                                        <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.createSuccessURL" value="/global/newsletters.jsp?email=${pEmail}" priority="1"/>
                                        <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.create" value="true" priority="-4"/>
                                    </c:when>
                                    <c:otherwise>
                                        <dsp:getvalueof var="id" param="subscriber_id"/>
                                        <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.repositoryId" value="${id}" priority="2"/>
                                        <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.updateSuccessURL" value="/global/newsletters.jsp?email=${pEmail}" priority="0"/>
                                        <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.update" value="true" priority="-5"/>
                                    </c:otherwise>
                                </c:choose>
                            </dsp:oparam>
                        </dsp:droplet>
                </dsp:form>
                        <c:if test="${isNew != 'true'}">
                            <dsp:form id="unsubcribeForm">
                                <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.value.user_id" value="${userId}"/>
                                <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.repositoryId" value="${id}" priority="2"/>
                                <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.value.login" value="${pEmail}" priority="-3"/>
                                <dsp:input type="hidden" bean="NewsletterSignUpFormHandler.delete" value="true" priority="-10"/>
                            </dsp:form>
                        </c:if>
                        <div>
                            <button id="submitBtn" type="button" class="button" data-event-click-id="global18" >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </cp:pageContainer>
    <script type="application/javascript" nonce="${requestScope.nonce}">
        function switchSubmit(checkbox) {
            if (checkbox.checked){
                $("#options_list :checkbox[name!='optOut']").prop("checked", false);
                $('#submitBtn').click(doUnsubscribe);
            }else{
                checkbox.checked = false;
                $('#submitBtn').click(doSignUp);
            }
        }

        function checkUnsubs() {
            if ($("#options_list :checkbox[name!='optOut']:checked").length == 0){
                if ($("#optOut").length > 0){
                    $('#submitBtn').prop("disabled", false);
                    $("#optOut").prop('checked', true);
                    $('#submitBtn').click(doUnsubscribe);
                } else {
                    $('#submitBtn').prop( "disabled", true);
                }

            } else{
                $("#optOut").prop('checked', false);
                $('#submitBtn').prop( "disabled", false);
                $('#submitBtn').click(doSignUp);
            }

        }

        function doSignUp() {
            $('#optionsForm').submit();
        }
        function doUnsubscribe() {
            $('#unsubcribeForm').submit();
        }
    </script>
</dsp:page>
