<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/cps/droplet/LocationByBranchLookupDroplet"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/handler/CreditApplicationFormHandler"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/pojo/UserCreditApplication"/>
    <dsp:getvalueof var="taxExemptionCertificate" bean="UserCreditApplication.taxExemptionCertificate"/>
    <dsp:getvalueof var="transient" bean="Profile.transient"/>
    <dsp:getvalueof var="securityStatus" bean="Profile.securityStatus"/>
    <dsp:getvalueof var="transient" value="${transient or securityStatus eq 1}"/>
    <dsp:getvalueof var="isProfileTransient" value="${transient}"/>
    <h1 class="mb20"><fmt:message key="credit.fields.tax.title"/></h1>
    <dsp:form action="${originatingRequest.contextPath}/account/credit-application.jsp"
              method="post" id="credit-application-form" formid="credit-application-form">
		
		<dsp:getvalueof var="taxExemptStatus" bean="UserCreditApplication.taxExemptionCertificate.taxExemptStatus"/>
        <dsp:getvalueof var="firmName" bean="UserCreditApplication.taxExemptionCertificate.firmName"/>
        <dsp:getvalueof var="firmStreet" bean="UserCreditApplication.taxExemptionCertificate.address.street"/>
        <dsp:getvalueof var="firmCity" bean="UserCreditApplication.taxExemptionCertificate.address.city"/>
        <dsp:getvalueof var="firmState" bean="UserCreditApplication.taxExemptionCertificate.address.state"/>
        <dsp:getvalueof var="firmCode" bean="UserCreditApplication.taxExemptionCertificate.address.zipCode"/>

        <dsp:getvalueof var="registeredType"
                        bean="UserCreditApplication.taxExemptionCertificate.registeredType"/>
        <dsp:getvalueof var="specifyRegisteredType"
                        bean="UserCreditApplication.taxExemptionCertificate.specifyRegisteredType"/>

        <dsp:getvalueof var="buyerActivityType" bean="UserCreditApplication.taxExemptionCertificate.buyerActivityType"/>
        <dsp:getvalueof var="specifyBuyerActivityType"
                        bean="UserCreditApplication.taxExemptionCertificate.specifyBuyerActivityType"/>

        <dsp:getvalueof var="businessFollowing" bean="UserCreditApplication.taxExemptionCertificate.businessFollowing"/>
        <dsp:getvalueof var="generalDescriptionProducts"
                        bean="UserCreditApplication.taxExemptionCertificate.generalDescriptionProducts"/>
        <div class="row">     
        <dsp:include page="${originatingRequest.contextPath}/global/gadgets/error-success-msg-divs.jsp">
					<dsp:param name="addColClass" value="false" />
		</dsp:include>         
		<div class="credit-card-align">
		Do you require Tax Exempt status for multiple states?  &nbsp;&nbsp;
			<fmt:message key="credit.fields.tax.header.check" />
			*
			</div>
			<div class="col-sm-6">
			<div class="form-group select-wrap">
				<label class="select-wrap radio-custom radio-inline active"
					data-initialize="radio"> <input class="checked"
					id="ca_input_tax_exempt_status" name="ca_input_tax_exempt_status"
					value="Yes" type="radio" ${taxExemptStatus == 'Yes' ? 'checked' : ''}
					data-event-click-id="global144"> <fmt:message
						key="credit.fields.tax.header.yes" />
				</label>
			</div>
			<div class="form-group select-wrap">
				<label class="select-wrap radio-custom radio-inline active"
					data-initialize="radio"> <input class="checked"
					id="ca_input_tax_exempt_status" name="ca_input_tax_exempt_status"
					value="No" type="radio" ${taxExemptStatus == 'No' ? 'checked' : ''}
					data-event-click-id="global145"> <fmt:message
						key="credit.fields.tax.header.no" />
				</label>
				</div>
			</div>
		</div>
		<div id="showStep3">
       	<div class="row">
                <div class="col-sm-2">
                    <p><fmt:message key="credit.fields.tax.issued"/></p>
                    <p><fmt:message key="credit.fields.tax.header_1"/>
                        <fmt:message key="credit.fields.tax.header_2"/>
                        <fmt:message key="credit.fields.tax.header_3"/></p>
                </div>
                </div>
        <section>
            <div class="row">
                <div class="col-sm-9">
                    <h2 class="h3"><fmt:message key="credit.fields.tax.certify"/></h2>
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message key="credit.fields.tax.firm_name"/>*</label>
                    <input id="ca_input_tax_firm_name" name="ca_input_tax_firm_name" type="text"
                           class="form-control" maxlength="100"
                           value="${firmName}"
                           data-event-keydown-id="global150" 
                           data-event-keyup-id="global156" >
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message key="credit.fields.tax.address"/>*</label>
                    <input id="ca_input_tax_address" name="ca_input_tax_address" type="text"
                           class="form-control"
                           maxlength="100"
                           value="${firmStreet}"
                           data-event-keydown-id="global151" 
                           data-event-keyup-id="global157" >
                </div>
                <div class="form-group col-sm-4">
                    <label class="control-label"><fmt:message key="credit.fields.tax.state"/>*</label>
                    <span class="select-wrap">
                        <select id="ca_input_tax_state" name="ca_input_tax_state" class="form-control">
                            <option disabled selected>State</option>
                            <%@include file="/includes/states-html.jspf" %>
                        </select>
                        <span class="caret caret-select"></span>
                    </span>
                </div>
                <div class="form-group col-sm-4">
                    <label class="control-label"><fmt:message key="credit.fields.tax.city"/>*</label>
                    <input id="ca_input_tax_city" name="ca_input_tax_city" type="text"
                           class="form-control" maxlength="100"
                           value="${firmCity}"
                           data-event-keydown-id="global152" 
                           data-event-keyup-id="global158" >
                </div>
                <div class="form-group col-sm-4">
                    <label class="control-label"><fmt:message key="credit.fields.tax.zip"/>*</label>
                    <input id="ca_input_tax_zip" name="ca_input_tax_zip" value="${firmCode}"
                           class="form-control" type="text" maxlength="5" data-event-input-id="global164" >
                </div>
            </div>
        </section>
        <section>
            <div class="row" id="engaged_registered">
                <div class="form-group">
                    <fmt:message key="credit.fields.tax.engaged"/>*
                </div>

                <div class="form-group col-sm-3">
                    <span class="select-wrap">
                        <select id="ca_input_tax_engaged" name="ca_input_tax_engaged" class="form-control"
                                data-event-change-id="global162" >
                            <option disabled selected>Select...</option>
                            <option value="Wholesaler"><fmt:message
                                    key="credit.fields.tax.wholesaler"/></option>
                            <option value="Retailer"><fmt:message key="credit.fields.tax.retailer"/></option>
                            <option value="Manufacturer"><fmt:message
                                    key="credit.fields.tax.manufacturer"/></option>
                            <option value="Lessor"><fmt:message key="credit.fields.tax.lessor"/></option>
                            <option value="Government Entity, Nonprofit School, Nonprofit Hospital, Church">
                            <fmt:message key="credit.fields.tax.government"/></option>
                            <option value="Other (specify)"><fmt:message
                                    key="credit.fields.tax.other"/></option>
                        </select>
                        <span class="caret caret-select"></span>
                    </span>
                </div>
            </div>
        </section>
        <section id="tax_registered">
            <div class="row">
                <div class="col-sm-9" align="left">
                    <fmt:message key="credit.fields.tax.state.title"/>*
                </div>
                <div class="col-sm-3">
                    <input type="button" align="right" id="addState" name="addState" class="add-state"
                           value="Add State" data-event-click-id="global146" >
                    <input type="button" id="removeState" class="add-state"
                           value="Delete State" data-event-click-id="global147" />
                </div>
            </div>
            <div id="tax_registered_1" class="row">
                <div class="col-sm-2">
                    <h5 align="right"><fmt:message key="credit.fields.tax.state.state"/>*</h5>
                </div>
                <div class="col-sm-3">
                    <span class="select-wrap">
                        <select id="ca_input_tax_registered_state_1" name="ca_input_tax_registered_state_1"
                                class="form-control">
                            <option disabled selected>State</option>
                            <%@include file="/includes/states-html.jspf" %>
                        </select>
                        <span class="caret caret-select"></span>
                    </span>
                </div>
                <div class="col-sm-4">
                    <h5 align="right"><fmt:message key="credit.fields.tax.state.id"/>*</h5>
                </div>
                <div class="col-sm-3">
                    <input id="ca_input_tax_registered_id_1" name="ca_input_tax_registered_id_1" type="text"
                           class="form-control"
                           maxlength="100"
                           sid="stateNumber"
                           data-event-keydown-id="global153" 
                           data-event-keyup-id="global159" >
                </div>
            </div>
        </section>
        <section>
            <div class="row" id="tax-purchase">
                <div class="form-group">
                    <fmt:message key="credit.fields.tax.purchase"/>
                </div>
                <div class="form-group col-sm-6">
                    <span class="select-wrap">
                        <select id="ca_input_tax_buyer_activity" name="ca_input_tax_buyer_activity" class="form-control"
                                data-event-change-id="global163" >
                            <option disabled selected>Select...</option>
                            <option value="For Resale"><fmt:message
                                    key="credit.fields.tax.purchase.resale"/></option>
                            <option value="Ingredients or component parts of a new product to be sold in the normal course of business">
                            <fmt:message key="credit.fields.tax.purchase.business"/></option>
                            <option value="Exempt under direct payment permit #"><fmt:message
                                    key="credit.fields.tax.purchase.exempt"/></option>
                            <option value="Industrial processing equipment or supplies"><fmt:message
                                    key="credit.fields.tax.purchase.equipment"/></option>
                            <option value="Other (explain)">
                            <fmt:message key="credit.fields.tax.purchase.other"/></option>
                        </select>
                        <span class="caret caret-select"></span>
                    </span>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="form-group">
                    <fmt:message key="credit.fields.tax.action"/>
                </div>
                <div class="col-sm-12">
                    <input id="ca_input_tax_action" name="ca_input_tax_action" type="text"
                           class="form-control" value="${businessFollowing}"
                           maxlength="100"
                           data-event-keydown-id="global154" 
                           data-event-keyup-id="global160" >
                </div>
            </div>
        </section>

        <section>
            <div class="row">
                <div class="form-group">
                    <fmt:message key="credit.fields.tax.product_description"/>
                </div>
                <div class="col-sm-12">
                    <input id="ca_input_tax_product_description" name="ca_input_tax_product_description"
                           type="text" value="${generalDescriptionProducts}"
                           class="form-control"
                           maxlength="100"
                           data-event-keydown-id="global155" 
                           data-event-keyup-id="global161" >
                </div>
            </div>
        </section>
        </div>

        <section>
            <div class="row">
                <div class="col-sm-5">
                    <a href="#" class="btn btn-info xs-block"
                       data-event-click-id="global148" ><fmt:message
                            key="credit.button.back"/></a>
                    <a class="btn btn-primary xs-block"
                       data-event-click-id="global149" id="step3NextBtn" ><fmt:message
                            key="credit.button.next"/></a>
                </div>
            </div>
        </section>

        <dsp:input type="hidden" bean="CreditApplicationFormHandler.taxExemptionCertificate" value="true"
                   priority="-10"/>
    </dsp:form>
	<input type="hidden" name="taxExemptStatus" id="taxExemptStatus" value="${taxExemptStatus}"/>
    <input type="hidden" name="firmState" id="firmState" value="${firmState}"/>
    <input type="hidden" name="registeredType" id="registeredType" value="${registeredType}"/>
    <input type="hidden" name="specifyRegisteredType" id="specifyRegisteredType" value="${specifyRegisteredType}"/>
    <input type="hidden" name="buyerActivityType" id="buyerActivityType" value="${buyerActivityType}"/>
    <input type="hidden" name="specifyBuyerActivityType" id="specifyBuyerActivityType" value="${specifyBuyerActivityType}"/>

    <dsp:getvalueof var="registeredStateList" bean="UserCreditApplication.taxExemptionCertificate.registeredStateList" />
    <input type="hidden" name="registeredStateListLength" id="registeredStateListLength" value="${fn:length(registeredStateList)}"/>

    <dsp:droplet name="ForEach">
        <dsp:param bean="UserCreditApplication.taxExemptionCertificate.registeredStateList" name="array"/>
        <dsp:param name="elementName" value="item"/>
        <dsp:getvalueof var="item" param="item"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="registeredState" param="item.state"/>
            <dsp:getvalueof var="registeredStateId" param="item.stateRegistration"/>
            <dsp:getvalueof var="index" param="index"/>

            <input type="hidden" name="registeredState${index}" id="registeredState${index}" value="${registeredState}"/>
            <input type="hidden" name="registeredStateId${index}" id="registeredStateId${index}" value="${registeredStateId}"/>
        </dsp:oparam>
    </dsp:droplet>

</dsp:page>