<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/cps/droplet/LocationByBranchLookupDroplet"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/handler/CreditApplicationFormHandler"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/pojo/UserCreditApplication"/>
    <dsp:getvalueof var="transient" bean="Profile.transient"/>
    <dsp:getvalueof var="securityStatus" bean="Profile.securityStatus"/>
    <dsp:getvalueof var="transient" value="${transient or securityStatus eq 1}"/>
    <dsp:getvalueof var="isProfileTransient" value="${transient}"/>
    <h1 class="mb20"><fmt:message key="credit.fields.profile.title"/></h1>
    <dsp:form action="${originatingRequest.contextPath}/account/credit-application.jsp"
              method="post" id="credit-application-form" formid="credit-application-form">

        <dsp:getvalueof var="expectedAnnualPurchases" bean="UserCreditApplication.expectedAnnualPurchases"/>
        <dsp:getvalueof var="originatedCreditRequest" bean="UserCreditApplication.originatedCreditRequest"/>
        <dsp:getvalueof var="personalGuaranty" bean="UserCreditApplication.personalGuaranty"/>
        <dsp:getvalueof var="personalGuarantyDebtor" bean="UserCreditApplication.personalGuarantyDebtor"/>

        <section>
            <div class="row">
                <dsp:include page="${originatingRequest.contextPath}/global/gadgets/error-success-msg-divs.jsp">
                    <dsp:param name="addColClass" value="false"/>
                </dsp:include>
                <div class="form-group">
                    <fmt:message key="credit.fields.profile.phrase.following_questionnaire"/>
                </div>
                <div class="form-group">
                    <fmt:message key="credit.fields.profile.phrase.following_products"/>
                </div>
                <div class="form-group">
                    <label class="radio-custom radio-inline active" data-initialize="radio">
                        <input class="checked" name="ca_input_product_pipe"
                               value="Pipe, Valves, & Fittings (PVC)" type="checkbox">
                        <fmt:message key="credit.fields.profile.pipe_valve_fitting"/>
                    </label>
                </div>
                <div class="form-group">
                    <label class="radio-custom radio-inline active" data-initialize="radio">
                        <input class="checked" name="ca_input_product_plumbing"
                               value="Plumbing" type="checkbox">
                        <fmt:message key="credit.fields.profile.plumbing"/>
                    </label>
                </div>
                <div class="form-group">
                    <label class="radio-custom radio-inline active" data-initialize="radio">
                        <input class="checked" name="ca_input_product_support"
                               value="Pipe, Hangers & Supports" type="checkbox">
                        <fmt:message key="credit.fields.profile.pipe_hangers_support"/>
                    </label>
                </div>
                <div class="form-group">
                    <label class="radio-custom radio-inline active" data-initialize="radio">
                        <input class="checked" name="ca_input_product_automated"
                               value="Automated Products Specialties" type="checkbox">
                        <fmt:message key="credit.fields.profile.automated_product"/>
                    </label>
                </div>
                <div class="form-group">
                    <label class="radio-custom radio-inline active" data-initialize="radio">
                        <input class="checked" name="ca_input_product_ventilation"
                               value="Heating, Ventilation & Air Conditioning Products (HVAC)"
                               type="checkbox">
                        <fmt:message key="credit.fields.profile.ventilation"/>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label"><fmt:message
                        key="credit.fields.profile.phrase.annual_purchases"/></label>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <div class="input-group-addon">$</div>
                            <input id="ca_input_annual_purchases" name="ca_input_annual_purchases"
                                   type="number"
                                   maxlength="10"
                                   value="${expectedAnnualPurchases}"
                                   data-event-keydown-id="global110" 
                                   data-event-keyup-id="global116" >
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <hr>
        <section>
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="h3"><fmt:message key="credit.fields.profile.phrase.order_customer"/></h2>
                </div>
                <div class="col-sm-3">
                    <input type="button" align="right" id="addCustomer" name="addCustomer"
                           class="add-customer"
                           value="Add Customer" data-event-click-id="global106" >
                    <input type="button" id="removeCustomer" class="add-customer"
                           value="Delete Customer" data-event-click-id="global107" />
                </div>
            </div>
            <div id="profile_customer" class="row">
                <div class="col-sm-3 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.profile.customer.name"/></label>
                    <input id="ca_input_profile_customer_name_1" name="ca_input_profile_customer_name_1"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           cid="customerNumber"
                           data-event-keydown-id="global111" 
                           data-event-keyup-id="global117" >
                </div>
                <div class="col-sm-3 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.profile.customer.title"/></label>
                    <input id="ca_input_profile_customer_title_1" name="ca_input_profile_customer_title_1"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           cid="customerNumber"
                           data-event-keydown-id="global112" 
                           data-event-keyup-id="global118" >
                </div>
                <div class="col-sm-3 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.profile.customer.phone"/></label>
                    <input id="ca_input_profile_customer_phone_1" name="ca_input_profile_customer_phone_1"
                           class="form-control"
                           maxlength="15"
                           cid="customerNumber"
                           data-event-keydown-id="global113" 
                           data-event-keyup-id="global119" >
                </div>
                <div class="col-sm-3 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.profile.customer.email"/></label>
                    <input id="ca_input_profile_customer_email_1" name="ca_input_profile_customer_email_1"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           cid="customerNumber"
                           data-event-keydown-id="global114" 
                           data-event-keyup-id="global120" >
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col-sm-5">
                    <label class="control-label"><fmt:message
                            key="credit.fields.profile.phrase.credit_request"/></label>
                    <input id="ca_input_credit_request" name="ca_input_credit_request" type="text"
                           maxlength="100"
                           value="${originatedCreditRequest}"
                           class="form-control"
                           data-event-keydown-id="global115" 
                           data-event-keyup-id="global121" >
                </div>
            </div>
            <hr>
            <div class="expand mb20">
                <div class="expand-item"><span class="expand-label">Personal Guaranty</span>
                    <div class="expand-content form-inline">
                        <p>I, <input type="text" class="form-control input-sm" id="ca_input_personal_guaranty" value="${personalGuaranty}"
                                     name="ca_input_personal_guaranty"> , for and in consideration of the extension
                            of credit to <input type="text" class="form-control input-sm" value="=${personalGuarantyDebtor}"
                                                id="ca_input_personal_guaranty_debtor" name="ca_input_personal_guaranty_debtor">
                            ("Debtor") for the purchase of goods, wares and merchandise from COLUMBIA PIPE
                            &amp; SUPPLY CO., an Illinois corporation ("Columbia Pipe"), hereby personally
                            guarantee to Columbia Pipe, its successors and assigns, unconditionally the
                            payment of the purchase price of all goods, wares and merchandise sold by
                            Columbia Pipe to Debtor, plus accrued finance charges of 1.5% per month on all
                            balances that are past due for more than 30 days and all expenses including
                            reasonable attorneys' fees incurred by Columbia Pipe in collecting or attempting
                            to collect any of the Debtor's obligations to Columbia Pipe or enforcing or
                            attempting to enforce this Guaranty.</p>
                        <p>I agree to abide by the terms of payment set forth by Columbia Pipe, including
                            the requirement that all invoices to be discounted must be paid within the terms
                            specified on the invoice. All invoices paid after discount terms specified on
                            the invoice must be paid net.</p>
                        <p>This Guaranty shall in all respects be continuing, absolute and unconditional an
                            shall be binding upon my heirs, executors, administrators and assigns.</p>
                        <p>Suits for the enforcement of this Guaranty may be brought against me and I hereby
                            consent to the in personam jurisdiction and venue of any state or federal court
                            within the state of Illinois. If there is a litigation based on this Guaranty, I
                            waive all claims for set-off, counterclaims and defenses based on any statute of
                            limitations and laches and waive trial by jury.</p>
                        <p>I have read the above and consent to all the terms thereof.</p>
                    </div>
                </div>
            </div>
            <div class="row mb20">
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                    </div>
                </div>
                <div class="col-md-8 col-sm-6 pt20">
                    <p>You agree to be bound by the following <a href="/termsOfSale">terms of sale</a> and <a href="/termsOfUse">terms of use</a> and the registration process will be completed. If you do not agree
                        to these terms and conditions, your registration will not be processed.</p>
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col-sm-5">

                    <a href="#" class="btn btn-info xs-block"
                       data-event-click-id="global108" ><fmt:message
                            key="credit.button.back"/></a>
                    <a href="#" class="btn btn-primary xs-block"
                       data-event-click-id="global109" ><fmt:message
                            key="credit.button.next"/></a>
                </div>
            </div>
        </section>

        <dsp:input type="hidden" bean="CreditApplicationFormHandler.customerProfile" value="true"
                   priority="-10"/>
    </dsp:form>

    <dsp:getvalueof var="productTypeList" bean="UserCreditApplication.productTypeList" />
    <input type="hidden" name="productTypeList" id="productTypeList" value="${vsg_utils:join(productTypeList, ',')}"/>

    <dsp:getvalueof var="supposedCustomerList" bean="UserCreditApplication.supposedCustomerList"/>
    <input type="hidden" name="supposedCustomerListLength" id="supposedCustomerListLength" value="${fn:length(supposedCustomerList)}" />

    <dsp:droplet name="ForEach">
        <dsp:param bean="UserCreditApplication.supposedCustomerList" name="array"/>
        <dsp:param name="elementName" value="item"/>
        <dsp:getvalueof var="item" param="item"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="supposedCustomerName" param="item.name"/>
            <dsp:getvalueof var="supposedCustomerTitle" param="item.title"/>
            <dsp:getvalueof var="supposedCustomerPhoneNumber" param="item.phoneNumber"/>
            <dsp:getvalueof var="supposedCustomerEmail" param="item.email"/>

            <dsp:getvalueof var="index" param="index"/>

            <input type="hidden" name="supposedCustomerName${index}" id="supposedCustomerName${index}" value="${supposedCustomerName}"/>
            <input type="hidden" name="supposedCustomerTitle${index}" id="supposedCustomerTitle${index}" value="${supposedCustomerTitle}"/>
            <input type="hidden" name="supposedCustomerPhoneNumber${index}" id="supposedCustomerPhoneNumber${index}" value="${supposedCustomerPhoneNumber}"/>
            <input type="hidden" name="supposedCustomerEmail${index}" id="supposedCustomerEmail${index}" value="${supposedCustomerEmail}"/>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>