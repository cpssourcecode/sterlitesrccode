<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/handler/CreditApplicationFormHandler"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/pojo/UserCreditApplication"/>

    <dsp:getvalueof var="transient" bean="Profile.transient"/>
    <dsp:getvalueof var="securityStatus" bean="Profile.securityStatus"/>
    <dsp:getvalueof var="transient" value="${transient or securityStatus eq 1}"/>
    <dsp:getvalueof var="isProfileTransient" value="${transient}"/>
    <dsp:getvalueof var="userCreditApplication" bean="UserCreditApplication"/>
    <h1 class="mb20"><fmt:message key="credit.title.application"/></h1>
    <dsp:include page="${originatingRequest.contextPath}/global/gadgets/error-success-msg-divs.jsp">
        <dsp:param name="addColClass" value="false"/>
    </dsp:include>
    <dsp:form method="POST" id="credit-application-form" formid="credit-application-form">

        <dsp:getvalueof var="legalBusinessName" bean="UserCreditApplication.legalBusinessName"/>
        <dsp:getvalueof var="tradeName" bean="UserCreditApplication.tradeName"/>
        <dsp:getvalueof var="organizationType" bean="UserCreditApplication.organizationType"/>
        <dsp:getvalueof var="customerType" bean="UserCreditApplication.customerType"/>
        <dsp:getvalueof var="phoneNumber" bean="UserCreditApplication.phoneNumber"/>
        <dsp:getvalueof var="businessType" bean="UserCreditApplication.businessType"/>
        <dsp:getvalueof var="businessYearEstablished" bean="UserCreditApplication.businessYearEstablished"/>
        <dsp:getvalueof var="businessLicenseNumber" bean="UserCreditApplication.businessLicenseNumber"/>
        <dsp:getvalueof var="requiredPO" bean="UserCreditApplication.requiredPO"/>
        <dsp:getvalueof var="taxExempt" bean="UserCreditApplication.taxExempt"/>
        <dsp:getvalueof var="taxNumber" bean="UserCreditApplication.taxNumber"/>

        <dsp:getvalueof var="businessStreet" bean="UserCreditApplication.addressBusiness.street"/>
        <dsp:getvalueof var="businessCity" bean="UserCreditApplication.addressBusiness.city"/>
        <dsp:getvalueof var="businessState" bean="UserCreditApplication.addressBusiness.state"/>
        <dsp:getvalueof var="businessCode" bean="UserCreditApplication.addressBusiness.zipCode"/>

        <dsp:getvalueof var="billingStreet" bean="UserCreditApplication.addressBilling.street"/>
        <dsp:getvalueof var="billingCity" bean="UserCreditApplication.addressBilling.city"/>
        <dsp:getvalueof var="billingState" bean="UserCreditApplication.addressBilling.state"/>
        <dsp:getvalueof var="billingCode" bean="UserCreditApplication.addressBilling.zipCode"/>

        <dsp:getvalueof var="bankReferenceName" bean="UserCreditApplication.BankReference.name"/>
        <dsp:getvalueof var="bankReferenceAccount" bean="UserCreditApplication.BankReference.accountNumber"/>
        <dsp:getvalueof var="bankReferenceOfficer" bean="UserCreditApplication.BankReference.accountOfficer"/>
        <dsp:getvalueof var="bankReferencePhone" bean="UserCreditApplication.BankReference.phoneNumber"/>
        <dsp:getvalueof var="bankReferenceEmail" bean="UserCreditApplication.BankReference.email"/>
        <dsp:getvalueof var="bankReferenceAddress" bean="UserCreditApplication.BankReference.address.street"/>
        <dsp:getvalueof var="bankReferenceCity" bean="UserCreditApplication.BankReference.address.city"/>
        <dsp:getvalueof var="bankReferenceState" bean="UserCreditApplication.BankReference.address.state"/>
        <dsp:getvalueof var="bankReferenceCode" bean="UserCreditApplication.BankReference.address.zipCode"/>

        <section>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message key="credit.fields.business_name"/>*</label>
                    <input id="ca_input_business_name" name="ca_input_business_name" type="text"
                           class="form-control" maxlength="100"
                           value="${legalBusinessName}"
                           data-event-keydown-id="global27" 
                           data-event-keyup-id="global63" >
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message key="credit.fields.trade_name"/>*</label>
                    <input id="ca_input_trade_name" name="ca_input_trade_name" type="text"
                           class="form-control"
                           maxlength="100"
                           value="${tradeName}"
                           data-event-keydown-id="global28" 
                           data-event-keyup-id="global64" >
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.organization_type"/>*</label>
                    <select id="ca_input_organization" name="ca_input_organization" class="form-control">
                        <option selected="" disabled="">Select...</option>
                        <option value="Proprietorship"><fmt:message
                                key="credit.fields.proprietorship"/></option>
                        <option value="Partnership"><fmt:message key="credit.fields.partnership"/></option>
                        <option value="Corporation"><fmt:message key="credit.fields.corporation"/></option>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message key="credit.fields.customer_type"/>*</label>
                    <select id="ca_input_customer" name="ca_input_customer" class="form-control">
                        <option selected="" disabled="">Select...</option>
                        <option value="Contractor"><fmt:message
                                key="credit.fields.customer_type.contractor"/></option>
                        <option value="Mechanical"><fmt:message
                                key="credit.fields.customer_type.mechanical"/></option>
                        <option value="Plumbing"><fmt:message
                                key="credit.fields.customer_type.plumbing"/></option>
                        <option value="Industrial"><fmt:message
                                key="credit.fields.customer_type.industrial"/></option>
                        <option value="Institutional"><fmt:message
                                key="credit.fields.customer_type.institutional"/></option>
                        <option value="Wholesaler"><fmt:message
                                key="credit.fields.customer_type.wholesaler"/></option>
                        <option value="Other"><fmt:message
                                key="credit.fields.customer_type.other"/></option>
                    </select>
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message key="credit.fields.phone"/>*</label>
                    <input id="ca_input_phone" name="ca_input_phone" type="text"
                           class="form-control" maxlength="15"
                           value="${phoneNumber}"
                           data-event-keydown-id="global29" 
                           data-event-keyup-id="global65" >
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message key="credit.fields.business_type"/>*</label>
                    <input id="ca_input_business_type" name="ca_input_business_type" type="text"
                           class="form-control" maxlength="100"
                           value="${businessType}"
                           data-event-keydown-id="global31" 
                           data-event-keyup-id="global67" >
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_year_established"/>*</label>
                    <div class="input-group form-date">
                        <input type="text" class="form-control" id="ca_input_business_year_established" value="${businessYearEstablished}"
                               name="ca_input_business_year_established"/>
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                    </div>
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message key="credit.fields.p_o_required"/>*</label>
                    <div class="radio select-wrap">
                        <label class="select-wrap radio-custom radio-inline active" data-initialize="radio">
                            <input class="checked" id="ca_input_po" name="ca_input_po" value="Yes" type="radio"
                                   data-event-click-id="global19" ><fmt:message
                                key="credit.fields.p_o_required.yes"/>
                        </label>
                    </div>
                    <div class="radio select-wrap">
                        <label class="select-wrap radio-custom radio-inline active" data-initialize="radio">
                            <input class="checked" id="ca_input_po" name="ca_input_po" value="No" type="radio"
                                   data-event-click-id="global20" ><fmt:message
                                key="credit.fields.p_o_required.no"/>
                        </label>
                    </div>
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message key="credit.fields.tax_exempt"/>*</label>
                    <div class="radio select-wrap">
                        <label class="select-wrap radio-custom radio-inline active" data-initialize="radio">
                            <input class="checked" id="ca_input_tax_exempt" name="ca_input_tax_exempt" value="Yes"
                                   type="radio"
                                   data-event-click-id="global21" ><fmt:message
                                key="credit.fields.tax_exempt.yes"/>
                        </label>
                    </div>
                    <div class="radio select-wrap">
                        <label class="select-wrap radio-custom radio-inline active" data-initialize="radio">
                            <input class="checked" id="ca_input_tax_exempt" name="ca_input_tax_exempt" value="No"
                                   type="radio"
                                   data-event-click-id="global22" ><fmt:message
                                key="credit.fields.tax_exempt.no"/>
                        </label>
                    </div>
                </div>
                <div class="form-group col-sm-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_license"/></label>
                    <input id="ca_input_business_license" name="ca_input_business_license" type="text"
                           class="form-control"
                           maxlength="100"
                           value="${businessLicenseNumber}"
                           data-event-keydown-id="global32" 
                           data-event-keyup-id="global68" >
                </div>
                <div class="form-group col-sm-6" id="required_tax">
                </div>
            </div>
            <hr class="row">
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="h4"><fmt:message key="credit.fields.address.business.title"/></h3>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.business_street"/>*</label>
                        <input id="ca_input_business_street" name="ca_input_business_street" type="text"
                               class="form-control" maxlength="100"
                               value="${businessStreet}"
                               data-event-keydown-id="global33" 
                               data-event-keyup-id="global69" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.business_city"/>*</label>
                        <input id="ca_input_business_city" name="ca_input_business_city" type="text"
                               class="form-control" maxlength="100"
                               value="${businessCity}"
                               data-event-keydown-id="global34" 
                               data-event-keyup-id="global70" >
                    </div>
                    <div class="row row-narrow">
                        <div class="form-group col-sm-7">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.business_state"/>*</label>
                            <select id="ca_input_business_state" name="ca_input_business_state"
                                    class="form-control">
                                <option disabled selected>State</option>
                                <%@include file="/includes/states-html.jspf" %>
                            </select>
                        </div>
                        <div class="form-group col-sm-5">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.business_code"/>*</label>
                            <input id="ca_input_business_code" name="ca_input_business_code" type="text"
                                   class="form-control" maxlength="5"
                                   value="${businessCode}"
                                   data-event-input-id="global99" >
                        </div>
                    </div>
                </div>
                <hr class="visible-xs ml15 mr15">
                <div class="col-sm-6">
                    <h3 class="h4"><fmt:message key="credit.fields.address.billing.title"/></h3>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.billing_street"/>*</label>
                        <input id="ca_input_billing_street" name="ca_input_billing_street" type="text"
                               class="form-control" maxlength="100"
                               value="${billingStreet}"
                               data-event-keydown-id="global35" 
                               data-event-keyup-id="global71" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.billing_city"/>*</label>
                        <input id="ca_input_billing_city" name="ca_input_billing_city" type="text"
                               class="form-control" maxlength="100"
                               value="${billingCity}"
                               data-event-keydown-id="global36" 
                               data-event-keyup-id="global72" >
                    </div>
                    <div class="row row-narrow">
                        <div class="form-group col-sm-7">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.billing_state"/>*</label>
                            <select id="ca_input_billing_state" name="ca_input_billing_state"
                                    class="form-control">
                                <option disabled selected>State</option>
                                <%@include file="/includes/states-html.jspf" %>
                            </select>
                        </div>
                        <div class="form-group col-sm-5">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.billing_code"/>*</label>
                            <input id="ca_input_billing_code" name="ca_input_billing_code" type="text"
                                   class="form-control" maxlength="5"
                                   value="${billingCode}"
                                   data-event-input-id="global100" >
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            
            <div class="row">
                <div class="col-sm-6">
                    <h3 class="h4"><fmt:message key="credit.fields.principal_name.title1"/></h3>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_name"/>*</label>
                        <input id="ca_input_principal_name_1" name="ca_input_principal_name_1" type="text"
                               class="form-control" maxlength="100"
                               value=""
                               data-event-keydown-id="global37" 
                               data-event-keyup-id="global73" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_title"/>*</label>
                        <input id="ca_input_principal_title_1" name="ca_input_principal_title_1" type="text"
                               class="form-control"
                               maxlength="100"
                               value=""
                               data-event-keydown-id="global38" 
                               data-event-keyup-id="global74" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_email"/>*</label>
                        <input id="ca_input_principal_email_1" name="ca_input_principal_email_1"
                               class="form-control" maxlength="50"
                               value=""
                               data-event-keydown-id="global40" 
                               data-event-keyup-id="global76" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_phone"/>*</label>
                        <input id="ca_input_principal_phone_1" name="ca_input_principal_phone_1"
                               class="form-control" maxlength="15"
                               value=""
                               data-event-keydown-id="global41" 
                               data-event-keyup-id="global77" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_address"/>*</label>
                        <input id="ca_input_principal_address_1" name="ca_input_principal_address_1"
                               type="text"
                               class="form-control" maxlength="100"
                               value=""
                               data-event-keydown-id="global42" 
                               data-event-keyup-id="global78" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_city"/>*</label>
                        <input id="ca_input_principal_city_1" name="ca_input_principal_city_1" type="text"
                               class="form-control" maxlength="100"
                               value=""
                               data-event-keydown-id="global43" 
                               data-event-keyup-id="global79" >
                    </div>
                    <div class="row row-narrow">
                        <div class="form-group col-sm-7">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.principal_state"/>*</label>
                            <select id="ca_input_principal_state_1" name="ca_input_principal_state_1"
                                    class="form-control">
                                <option disabled selected>State</option>
                                <%@include file="/includes/states-html.jspf" %>
                            </select>
                        </div>
                        <div class="form-group col-sm-5">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.principal_code"/>*</label>
                            <input id="ca_input_principal_code_1" name="ca_input_principal_code_1"
                                   type="text"
                                   class="form-control" maxlength="5"
                                   value=""
                                   data-event-input-id="global101" >
                        </div>
                    </div>
                </div>
                <hr class="visible-xs ml15 mr15">
                <div class="col-sm-6">
                    <h3 class="h4"><fmt:message key="credit.fields.principal_name.title2"/></h3>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_name"/>*</label>
                        <input id="ca_input_principal_name_2" name="ca_input_principal_name_2" type="text"
                               class="form-control" maxlength="100"
                               value=""
                               data-event-keydown-id="global44" 
                               data-event-keyup-id="global80" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_title"/>*</label>
                        <input id="ca_input_principal_title_2" name="ca_input_principal_title_2" type="text"
                               class="form-control"
                               maxlength="100"
                               value=""
                               data-event-keydown-id="global45" 
                               data-event-keyup-id="global81" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_email"/>*</label>
                        <input id="ca_input_principal_email_2" name="ca_input_principal_email_2"
                               class="form-control" maxlength="50"
                               value=""
                               data-event-keydown-id="global47" 
                               data-event-keyup-id="global83" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_phone"/>*</label>
                        <input id="ca_input_principal_phone_2" name="ca_input_principal_phone_2"
                               class="form-control" maxlength="15"
                               value=""
                               data-event-keydown-id="global48" 
                               data-event-keyup-id="global84" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_address"/>*</label>
                        <input id="ca_input_principal_address_2" name="ca_input_principal_address_2"
                               type="text"
                               class="form-control" maxlength="100"
                               value=""
                               data-event-keydown-id="global49" 
                               data-event-keyup-id="global85" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.principal_city"/>*</label>
                        <input id="ca_input_principal_city_2" name="ca_input_principal_city_2" type="text"
                               class="form-control" maxlength="100"
                               value=""
                               data-event-keydown-id="global50" 
                               data-event-keyup-id="global86" >
                    </div>
                    <div class="row row-narrow">
                        <div class="form-group col-sm-7">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.principal_state"/>*</label>
                            <select id="ca_input_principal_state_2" name="ca_input_principal_state_2"
                                    class="form-control">
                                <option disabled selected>State</option>
                                <%@include file="/includes/states-html.jspf" %>
                            </select>
                        </div>
                        <div class="form-group col-sm-5">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.principal_code"/>*</label>
                            <input id="ca_input_principal_code_2" name="ca_input_principal_code_2"
                                   type="text"
                                   class="form-control" maxlength="5"
                                   value=""
                                   data-event-input-id="global102" >
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <hr>
        <section>
            <div class="row">
                <div class="col-sm-4">
                    <h2 class="h3"><fmt:message key="credit.fields.business_references"/></h2>
                </div>
                <div class="col-sm-4">
                    <input type="button" align="right" id="add-reference" name="add-reference"
                           class="add-reference"
                           value="Add Reference" data-event-click-id="global23" >
                    <input type="button" id="remove-reference" class="add-reference"
                           value="Delete Reference" data-event-click-id="global24" />
                </div>
            </div>
            <div id="references" class="row row-narrow">
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_trade_name"/>*</label>
                    <input id="ca_input_business_trade_name_1" name="ca_input_business_trade_name_1"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           uid="referenceNumber"
                           data-event-keydown-id="global51" 
                           data-event-keyup-id="global87" >
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_trade_address"/>*</label>
                    <input id="ca_input_business_trade_address_1" name="ca_input_business_trade_address_1"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           uid="referenceNumber"
                           data-event-keydown-id="global52" 
                           data-event-keyup-id="global88" >
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_trade_city"/>*</label>
                    <input id="ca_input_business_trade_city_1" name="ca_input_business_trade_city_1"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           uid="referenceNumber"
                           data-event-keydown-id="global53" 
                           data-event-keyup-id="global89" >
                </div>
                <div class="form-group col-sm-1">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_trade_state"/>*</label>
                    <select id="ca_input_business_trade_state_1" name="ca_input_business_trade_state_1"
                            class="form-control">
                        <option disabled selected>State</option>
                        <%@include file="/includes/states-html.jspf" %>
                    </select>
                </div>
                <div class="form-group col-sm-1">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_trade_zip"/>*</label>
                    <input id="ca_input_business_trade_zip_1" name="ca_input_business_trade_zip_1"
                           type="text"
                           class="form-control"
                           maxlength="5"
                           uid="referenceNumber"
                           data-event-input-id="global103" >
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_trade_phone"/>*</label>
                    <input id="ca_input_business_trade_phone_1" name="ca_input_business_trade_phone_1"
                           class="form-control"
                           maxlength="15"
                           uid="referenceNumber"
                           data-event-keydown-id="global54" 
                           data-event-keyup-id="global90" >
                </div>
                <div class="form-group col-sm-2">
                    <label class="control-label"><fmt:message
                            key="credit.fields.business_trade_email"/>*</label>
                    <input id="ca_input_business_trade_email_1" name="ca_input_business_trade_email_1"
                           class="form-control"
                           maxlength="50"
                           uid="referenceNumber"
                           data-event-keydown-id="global55" 
                           data-event-keyup-id="global91" >
                </div>
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col-sm-3">
                    <h2 class="h3"><fmt:message key="credit.fields.bank_reference"/></h2>
                </div>
            </div>
            <div class="row row-narrow">
                <div class="col-sm-4 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.name"/></label>
                    <input id="ca_input_bank_reference_name" name="ca_input_bank_reference_name"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           value="${bankReferenceName}"
                           data-event-keydown-id="global56" 
                           data-event-keyup-id="global92" >
                </div>
                <div class="col-sm-4 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.acct"/></label>
                    <input id="ca_input_bank_reference_acct" name="ca_input_bank_reference_acct"
                           type="number"
                           class="form-control"
                           maxlength="100"
                           value="${bankReferenceAccount}"
                           data-event-keydown-id="global57" 
                           data-event-keyup-id="global93" >
                </div>
                <div class="col-sm-4 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.officer"/></label>
                    <input id="ca_input_bank_reference_officer" name="ca_input_bank_reference_officer"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           value="${bankReferenceOfficer}"
                           data-event-keydown-id="global58" 
                           data-event-keyup-id="global94" >
                </div>
                <div class="col-sm-3 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.address"/></label>
                    <input id="ca_input_bank_reference_address" name="ca_input_bank_reference_address"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           value="${bankReferenceAddress}"
                           data-event-keydown-id="global59" 
                           data-event-keyup-id="global95" >
                </div>
                <div class="col-sm-3 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.city"/></label>
                    <input id="ca_input_bank_reference_city" name="ca_input_bank_reference_city"
                           type="text"
                           class="form-control"
                           maxlength="100"
                           value="${bankReferenceCity}"
                           data-event-keydown-id="global60" 
                           data-event-keyup-id="global96" >
                </div>
                <div class="col-sm-1 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.state"/></label>
                    <select id="ca_input_bank_reference_state" name="ca_input_bank_reference_state"
                            class="form-control">
                        <option disabled selected>State</option>
                        <%@include file="/includes/states-html.jspf" %>
                    </select>
                </div>
                <div class="col-sm-1 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.zip"/></label>
                    <input id="ca_input_bank_reference_zip" name="ca_input_bank_reference_zip"
                           type="text"
                           class="form-control"
                           maxlength="5"
                           value="${bankReferenceCode}"
                           data-event-input-id="global104" >
                </div>
                <div class="col-sm-2 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.phone"/></label>
                    <input id="ca_input_bank_reference_phone" name="ca_input_bank_reference_phone"
                           class="form-control"
                           maxlength="15"
                           value="${bankReferencePhone}"
                           data-event-keydown-id="global61" 
                           data-event-keyup-id="global97" >
                </div>
                <div class="col-sm-2 col-xs-6">
                    <label class="control-label"><fmt:message
                            key="credit.fields.bank_reference.email"/></label>
                    <input id="ca_input_bank_reference_email" name="ca_input_bank_reference_email"
                           class="form-control"
                           maxlength="50"
                           value="${bankReferenceEmail}"
                           data-event-keydown-id="global62" 
                           data-event-keyup-id="global98" >
                </div>
            </div>
        </section>

        <div class="row">
            <div class="col-sm-5">
                <a href="#" class="btn btn-info xs-block" data-event-click-id="global25" ><fmt:message
                        key="credit.button.cancel"/></a>
                <a href="#" class="btn btn-primary xs-block" data-event-click-id="global26" ><fmt:message
                        key="credit.button.next"/></a>
            </div>
        </div>

        <%--<dsp:input id="ca_business_name" type="hidden" bean="CreditApplicationFormHandler.value.businessName" value=""/>--%>
        <%--<dsp:input id="ca_trade_name" type="hidden" bean="CreditApplicationFormHandler.value.tradeName" value=""/>--%>
        <%--<dsp:input id="ca_phone_fax" type="hidden" bean="CreditApplicationFormHandler.value.phoneFax" value=""/>--%>
        <%--<dsp:input id="ca_business_street" type="hidden" bean="CreditApplicationFormHandler.value.businessStreet" value=""/>--%>
        <%--<dsp:input id="ca_business_city" type="hidden" bean="CreditApplicationFormHandler.value.businessCity" value=""/>--%>
        <%--<dsp:input id="ca_business_country" type="hidden" bean="CreditApplicationFormHandler.value.businessCountry" value=""/>--%>
        <%--<dsp:input id="ca_business_state" type="hidden" bean="CreditApplicationFormHandler.value.businessState" value=""/>--%>
        <%--<dsp:input id="ca_business_code" type="hidden" bean="CreditApplicationFormHandler.value.businessCode" value=""/>--%>
		<dsp:getvalueof var="login" bean="Profile.login"/>
		<dsp:getvalueof var="name" bean="Profile.firstName"/>
        <dsp:input type="hidden" bean="CreditApplicationFormHandler.sendMessage" value="true"
                   priority="-10"/>
        <dsp:input type="hidden" bean="CreditApplicationFormHandler.value.email" value="${login}"/>
    	<dsp:input type="hidden" bean="CreditApplicationFormHandler.value.firstName" value="${name}"/>
    
    </dsp:form>

    <input type="hidden" name="organizationType" id="organizationType" value="${organizationType}"/>
    <input type="hidden" name="customerType" id="customerType" value="${customerType}"/>
    <input type="hidden" name="requiredPO" id="requiredPO" value="${requiredPO}"/>
    <input type="hidden" name="taxExempt" id="taxExempt" value="${taxExempt}"/>
    <input type="hidden" name="taxNumber" id="taxNumber" value="${taxNumber}"/>
    <input type="hidden" name="businessState" id="businessState" value="${businessState}"/>
    <input type="hidden" name="billingState" id="billingState" value="${billingState}"/>
    <input type="hidden" name="bankReferenceState" id="bankReferenceState" value="${bankReferenceState}"/>

    <dsp:getvalueof bean="UserCreditApplication.principalOrganizationList" var="principalOrganizationList"/>
    <input type="hidden" name="principalOrganizationListLength" id="principalOrganizationListLength" value="${fn:length(principalOrganizationList)}"/>
    <dsp:droplet name="ForEach">
        <dsp:param bean="UserCreditApplication.principalOrganizationList" name="array"/>
        <dsp:param name="elementName" value="item"/>
        <dsp:getvalueof var="item" param="item"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="principalName" param="item.name"/>
            <dsp:getvalueof var="principalTitle" param="item.title"/>
            <dsp:getvalueof var="socialSecurityNumber" param="item.socialSecurityNumber"/>
            <dsp:getvalueof var="principalEmail" param="item.email"/>
            <dsp:getvalueof var="principalPhoneNumber" param="item.phoneNumber"/>
            <dsp:getvalueof var="principalStreet" param="item.address.street"/>
            <dsp:getvalueof var="principalCity" param="item.address.city"/>
            <dsp:getvalueof var="principalState" param="item.address.state"/>
            <dsp:getvalueof var="principalCode" param="item.address.zipCode"/>
            <dsp:getvalueof var="index" param="index"/>

            <input type="hidden" name="principalName${index}" id="principalName${index}" value="${principalName}"/>
            <input type="hidden" name="principalTitle${index}" id="principalTitle${index}" value="${principalTitle}"/>
            <input type="hidden" name="socialSecurityNumber${index}" id="socialSecurityNumber${index}" value="${socialSecurityNumber}"/>
            <input type="hidden" name="principalEmail${index}" id="principalEmail${index}" value="${principalEmail}"/>
            <input type="hidden" name="principalPhoneNumber${index}" id="principalPhoneNumber${index}" value="${principalPhoneNumber}"/>
            <input type="hidden" name="principalStreet${index}" id="principalStreet${index}" value="${principalStreet}"/>
            <input type="hidden" name="principalCity${index}" id="principalCity${index}" value="${principalCity}"/>
            <input type="hidden" name="principalState${index}" id="principalState${index}" value="${principalState}"/>
            <input type="hidden" name="principalCode${index}" id="principalCode${index}" value="${principalCode}"/>
        </dsp:oparam>
    </dsp:droplet>

    <dsp:getvalueof bean="UserCreditApplication.businessTradeReferenceList" var="businessTradeReferenceList"/>
    <input type="hidden" name="businessTradeReferenceListLength" id="businessTradeReferenceListLength" value="${fn:length(businessTradeReferenceList)}"/>
    <dsp:droplet name="ForEach">
        <dsp:param bean="UserCreditApplication.businessTradeReferenceList" name="array"/>
        <dsp:param name="elementName" value="item"/>
        <dsp:getvalueof var="item" param="item"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="referenceName" param="item.name"/>
            <dsp:getvalueof var="referenceStreet" param="item.address.street"/>
            <dsp:getvalueof var="referenceCity" param="item.address.city"/>
            <dsp:getvalueof var="referenceState" param="item.address.state"/>
            <dsp:getvalueof var="referenceCode" param="item.address.zipCode"/>
            <dsp:getvalueof var="referencePhone" param="item.phoneNumber"/>
            <dsp:getvalueof var="referenceEmail" param="item.email"/>
            <dsp:getvalueof var="index" param="index"/>

            <input type="hidden" name="referenceName${index}" id="referenceName${index}" value="${referenceName}"/>
            <input type="hidden" name="referenceStreet${index}" id="referenceStreet${index}" value="${referenceStreet}"/>
            <input type="hidden" name="referenceCity${index}" id="referenceCity${index}" value="${referenceCity}"/>
            <input type="hidden" name="referenceState${index}" id="referenceState${index}" value="${referenceState}"/>
            <input type="hidden" name="referenceCode${index}" id="referenceCode${index}" value="${referenceCode}"/>
            <input type="hidden" name="referencePhone${index}" id="referencePhone${index}" value="${referencePhone}"/>
            <input type="hidden" name="referenceEmail${index}" id="referenceEmail${index}" value="${referenceEmail}"/>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>