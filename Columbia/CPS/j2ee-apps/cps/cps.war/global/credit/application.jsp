<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/pojo/UserCreditApplication"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/handler/CreditApplicationFormHandler"/>
    <dsp:getvalueof var="transient" bean="Profile.transient"/>
    <dsp:getvalueof var="securityStatus" bean="Profile.securityStatus"/>
    <dsp:getvalueof var="isTransient" value="${transient or securityStatus eq 1}"/>
    <dsp:getvalueof var="isProfileTransient" value="${isTransient}"/>
    <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
    <c:if test="${not empty selectedOrg}">
        <dsp:getvalueof var="csrClass" value="admin-access"/>
    </c:if>
    <cp:pageContainer page="account">
        <c:if test="${!isTransient}">
            <dsp:droplet name="/cps/userprofiling/credit/application/service/CreditApplicationDroplet">
                <dsp:param name="profile" bean="Profile"/>
                <dsp:param name="userCreditApplication" bean="UserCreditApplication"/>
                <dsp:oparam name="output">
                </dsp:oparam>
            </dsp:droplet>
        </c:if>
        <%--<dsp:getvalueof var="emptyResult" param="empty"/>--%>
        <main id="body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ol class="breadcrumb" id="breadcrumb-credit">
                            <li><fmt:message key="credit.navigation.application.title"/></li>
                            <li><a style="cursor: pointer;" data-event-click-id="global105" ><fmt:message
                                    key="credit.navigation.application.step_1"/></a></li>
                        </ol>
                    </div>
                    <dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
                        <dsp:param name="email" value="true"/>
                        <dsp:param name="print" value="false"/>
                    </dsp:include>
                </div>
                <div id="credit-application">
                    <dsp:include page="${originatingRequest.contextPath}/global/credit/application-content.jsp"/>
                    <script nonce="${requestScope.nonce}">
                        onApplicationContentLoaded();
                    </script>
                </div>
            </div>
        </main>
        <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-credit-application.jsp"/>
    </cp:pageContainer>
</dsp:page>
