<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/cps/droplet/LocationByBranchLookupDroplet"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/handler/CreditApplicationFormHandler"/>
    <dsp:importbean bean="/cps/userprofiling/credit/application/pojo/UserCreditApplication"/>
    <dsp:getvalueof var="resaleCertificate" bean="UserCreditApplication.resaleCertificate"/>
    <dsp:getvalueof var="transient" bean="Profile.transient"/>
    <dsp:getvalueof var="securityStatus" bean="Profile.securityStatus"/>
    <dsp:getvalueof var="transient" value="${transient or securityStatus eq 1}"/>
    <dsp:getvalueof var="isProfileTransient" value="${transient}"/>
    <h1 class="mb20"><fmt:message key="credit.fields.certificate.title"/></h1>

    <dsp:form action="${originatingRequest.contextPath}/account/credit-application.jsp"
              method="post" id="credit-application-form" formid="credit-application-form">

        <dsp:getvalueof var="sellerName" bean="UserCreditApplication.resaleCertificate.sellerName"/>
        <dsp:getvalueof var="sellerStreet" bean="UserCreditApplication.resaleCertificate.sellerAddress.street"/>
        <dsp:getvalueof var="sellerCity" bean="UserCreditApplication.resaleCertificate.sellerAddress.city"/>
        <dsp:getvalueof var="sellerState" bean="UserCreditApplication.resaleCertificate.sellerAddress.state"/>
        <dsp:getvalueof var="sellerCode" bean="UserCreditApplication.resaleCertificate.sellerAddress.zipCode"/>
        <dsp:getvalueof var="purchaserName" bean="UserCreditApplication.resaleCertificate.purchaserName"/>
        <dsp:getvalueof var="purchaserStreet" bean="UserCreditApplication.resaleCertificate.purchaserAddress.street"/>
        <dsp:getvalueof var="purchaserCity" bean="UserCreditApplication.resaleCertificate.purchaserAddress.city"/>
        <dsp:getvalueof var="purchaserState" bean="UserCreditApplication.resaleCertificate.purchaserAddress.state"/>
        <dsp:getvalueof var="purchaserCode" bean="UserCreditApplication.resaleCertificate.purchaserAddress.zipCode"/>
        <dsp:getvalueof var="purchaserRegisteredType" bean="UserCreditApplication.resaleCertificate.purchaserRegisteredType"/>
        <dsp:getvalueof var="registrationNumber" bean="UserCreditApplication.resaleCertificate.registrationNumber"/>
        <dsp:getvalueof var="describePropertyResale" bean="UserCreditApplication.resaleCertificate.describePropertyResale"/>
        <dsp:getvalueof var="purchasesResaleType" bean="UserCreditApplication.resaleCertificate.purchasesResaleType"/>
        <dsp:getvalueof var="percentagePurchasesResale" bean="UserCreditApplication.resaleCertificate.percentagePurchasesResale"/>

        <section>
            <div class="row">
                <dsp:include page="${originatingRequest.contextPath}/global/gadgets/error-success-msg-divs.jsp">
                    <dsp:param name="addColClass" value="false"/>
                </dsp:include>
                <div class="col-sm-6">
                    <h2 class="h3"><fmt:message key="credit.fields.certificate.step_1.title"/></h2>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.certificate.step_1.name"/>*</label>
                        <input id="ca_input_certificate_name_step_1" name="ca_input_certificate_name_step_1"
                               type="text"
                               class="form-control" maxlength="100"
                               value="${sellerName}"
                               data-event-keydown-id="global130" 
                               data-event-keyup-id="global136" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.certificate.step_1.address"/>*</label>
                        <input id="ca_input_certificate_address_step_1"
                               name="ca_input_certificate_address_step_1" type="text"
                               class="form-control" maxlength="100"
                               value="${sellerStreet}"
                               data-event-keydown-id="global131" 
                               data-event-keyup-id="global137" >
                    </div>
                    <div class="row row-narrow">
                        <div class="form-group col-sm-4">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.certificate.step_1.city"/>*</label>
                            <input id="ca_input_certificate_city_step_1" name="ca_input_certificate_city_step_1"
                                   type="text"
                                   class="form-control" maxlength="100"
                                   value="${sellerCity}"
                                   data-event-keydown-id="global132" 
                                   data-event-keyup-id="global138" >
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.certificate.step_1.state"/>*</label>
                            <span class="select-wrap">
                                <select id="ca_input_certificate_state_step_1"
                                        name="ca_input_certificate_state_step_1" class="form-control">
                                    <option disabled selected>State</option>
                                    <%@include file="/includes/states-html.jspf" %>
                                </select>
                                <span class="caret caret-select"></span>
                            </span>
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.certificate.step_1.zip"/>*</label>
                            <input id="ca_input_certificate_zip_step_1" name="ca_input_certificate_zip_step_1"
                                   type="text"
                                   class="form-control" maxlength="5"
                                   value="${sellerCode}"
                                   data-event-input-id="global142" >
                        </div>
                    </div>
                    <h2 class="h3"><fmt:message key="credit.fields.certificate.step_2.title"/></h2>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.certificate.step_2.name"/>*</label>
                        <input id="ca_input_certificate_name_step_2" name="ca_input_certificate_name_step_2"
                               type="text"
                               class="form-control" maxlength="100"
                               value="${purchaserName}"
                               data-event-keydown-id="global133" 
                               data-event-keyup-id="global139" >
                    </div>
                    <div class="form-group">
                        <label class="control-label"><fmt:message
                                key="credit.fields.certificate.step_2.address"/>*</label>
                        <input id="ca_input_certificate_address_step_2"
                               name="ca_input_certificate_address_step_2" type="text"
                               class="form-control" maxlength="100"
                               value="${purchaserStreet}"
                               data-event-keydown-id="global134" 
                               data-event-keyup-id="global140" >
                    </div>
                    <div class="row row-narrow">
                        <div class="form-group col-sm-4">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.certificate.step_2.city"/>*</label>
                            <input id="ca_input_certificate_city_step_2" name="ca_input_certificate_city_step_2"
                                   type="text"
                                   class="form-control" maxlength="100"
                                   value="${purchaserCity}"
                                   data-event-keydown-id="global135" 
                                   data-event-keyup-id="global141" >
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.certificate.step_2.state"/>*</label>
                            <span class="select-wrap">
                                <select id="ca_input_certificate_state_step_2"
                                        name="ca_input_certificate_state_step_2" class="form-control">
                                    <option disabled selected>State</option>
                                    <%@include file="/includes/states-html.jspf" %>
                                </select>
                                <span class="caret caret-select"></span>
                            </span>
                        </div>
                        <div class="form-group col-sm-4">
                            <label class="control-label"><fmt:message
                                    key="credit.fields.certificate.step_2.zip"/>*</label>
                            <input id="ca_input_certificate_zip_step_2" name="ca_input_certificate_zip_step_2"
                                   type="text"
                                   class="form-control" maxlength="5"
                                   value="${purchaserCode}"
                                   data-event-input-id="global143" >
                        </div>
                    </div>
                    <div>
                    <div class="form-group">
                        <fmt:message key="credit.fields.certificate.step_2.purchaser_registered"/>
                    </div>
                    <div class="form-group" id="retailer">
                        <div class="col-sm-7">
                            <label class="select-wrap find-parent radio-custom radio-inline active" data-initialize="radio">
                                <input class="checked" id="ca_input_certificate_registered"
                                       name="ca_input_certificate_registered"
                                       type="radio" value="retailer"
                                       data-event-click-id="global122" >
                                <fmt:message key="credit.fields.certificate.step_2.purchaser.retailer"/>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="reseller">
                        <div class="col-sm-7">
                            <label class="select-wrap find-parent radio-custom radio-inline active" data-initialize="radio">
                                <input class="checked" id="ca_input_certificate_registered"
                                       name="ca_input_certificate_registered"
                                       type="radio" value="reseller"
                                       data-event-click-id="global123" >
                                <fmt:message key="credit.fields.certificate.step_2.purchaser.reseller"/>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-sm-12">
                        <label class="select-wrap find-parent radio-custom radio-inline active" data-initialize="radio">
                            <input class="checked" id="ca_input_certificate_registered"
                                   name="ca_input_certificate_registered"
                                   type="radio" value="other"
                                   data-event-click-id="global124" >
                            <fmt:message key="credit.fields.certificate.step_2.purchaser.other"/>
                        </label>
                    </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h2 class="h3"><fmt:message key="credit.fields.certificate.step_3.title"/></h2>
                    <div class="form-group">
                        <fmt:message key="credit.fields.certificate.step_3.describe_property"/>
                        <textarea id="ca_input_certificate_describe_property"
                                  name="ca_input_certificate_describe_property" class="form-control" cols="5" rows="3"
                                  maxlength="500"></textarea>
                    </div>
                    <h2 class="h3"><fmt:message key="credit.fields.certificate.step_4.title"/></h2>
                    <div class="form-group">
                        <fmt:message key="credit.fields.certificate.step_4.purchaser_identified"/>
                    </div>
                    <div>
                    <div class="form-group">
                        <div class="col-sm-10">
                            <label class="select-wrap find-parent radio-custom radio-inline active" data-initialize="radio">
                                <input class="checked" id="ca_input_certificate_purchases_resale_type"
                                       name="ca_input_certificate_purchases_resale_type"
                                       type="radio" value="fully"
                                       data-event-click-id="global125" >
                                <fmt:message key="credit.fields.certificate.step_4.resale"/>
                            </label>
                        </div>
                    </div>
                    <div class="form-group" id="reseller_partly">
                        <div class="col-sm-10">
                            <label class="select-wrap find-parent radio-custom radio-inline active" data-initialize="radio">
                                <input class="checked" id="ca_input_certificate_purchases_resale_type"
                                       name="ca_input_certificate_purchases_resale_type"
                                       type="radio" value="partly"
                                       data-event-click-id="global126" >
                                <fmt:message key="credit.fields.certificate.step_4.partly_resale"/>
                            </label>
                        </div>
                    </div>
                    </div>
                    <h2 class="h3"><fmt:message key="credit.fields.certificate.step_5.title"/></h2>
                    <div class="form-group">
                        <fmt:message key="credit.fields.certificate.step_5.certify_resale"/>
                    </div>
                </div>
            </div>
        </section>

<section>
            <div class="row">
                <div class="col-sm-5">
                    <a href="#" class="btn btn-info xs-block"
                       data-event-click-id="global127" ><fmt:message
                            key="credit.button.back"/></a>
                   <a href="#" class="btn btn-primary xs-block"  data-event-click-id="global128" ><fmt:message  key="credit.button.save"/></a>          
                   <c:choose>
                    <c:when test="${isProfileTransient}">
                    <div id="credite-application-form-captcha">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<div id="recaptchaCrediteApplication"></div>
									</div>
								</div>
							</div>
						</div>
                     <a href="#" class="btn btn-primary xs-block" id="creditApplicationSubmit" data-event-click-id="global129" ><fmt:message key="credit.button.submit"/></a>
                    </c:when>
                    <c:otherwise>
                    <a href="#" class="btn btn-primary xs-block"  data-event-click-id="global129" ><fmt:message  key="credit.button.submit"/></a>
                    </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </section>
        <dsp:input type="hidden" bean="CreditApplicationFormHandler.selectedOption" value="" id="addSelectedOption" />

        <dsp:input type="hidden" bean="CreditApplicationFormHandler.resaleCertificate" value="true"
                   priority="-10"/>
    </dsp:form>

    <input type="hidden" name="sellerState" id="sellerState" value="${sellerState}"/>
    <input type="hidden" name="purchaserState" id="purchaserState" value="${purchaserState}"/>
    <input type="hidden" name="purchaserRegisteredType" id="purchaserRegisteredType" value="${purchaserRegisteredType}"/>
    <input type="hidden" name="registrationNumber" id="registrationNumber" value="${registrationNumber}"/>
    <input type="hidden" name="describePropertyResale" id="describePropertyResale" value="${describePropertyResale}"/>
    <input type="hidden" name="purchasesResaleType" id="purchasesResaleType" value="${purchasesResaleType}"/>
    <input type="hidden" name="percentagePurchasesResale" id="percentagePurchasesResale" value="${percentagePurchasesResale}"/>
</dsp:page>