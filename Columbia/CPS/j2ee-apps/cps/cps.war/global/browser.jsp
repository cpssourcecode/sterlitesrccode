<dsp:page>
   <dsp:importbean bean="/atg/userprofiling/Profile" />
   <dsp:getvalueof var="compatibilityModeOn" bean="Profile.compatibilityModeOn"/>

   <!doctype html>
   <html>

    <head>

        <meta charset="UTF-8">
        <title>Columbia Pipe</title>

        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">

        <style type="text/css">
            * { -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; }
            body { text-align: center; background: #fff; }
            .main { max-width: 600px; margin: 20% auto 0; }
            h1 { font-family: sans-serif; color: #666; }
            p { font-family: sans-serif; color: #666; font-size: 20px; line-height: 1.3; }

			.instructions { color: #666; text-align: left; padding-left: 25%; padding-bottom: 50px; }
			.instructions > p { font-size: 15px !important; }
			
            .text-center { text-align: center; }
            .col-xs-3 { width: 25%; float: left; }
            .row::after { clear: both; display: block; height: 0; overflow: hidden; }

            a {
                color: #333333;
                font-family: sans-serif;
            }
            a:hover, a:active, a:focus {
                color: #333333;
                font-family: sans-serif;
            }
        </style>

    </head>
    <body>

	    <c:if test="${!compatibilityModeOn}">
	    	<div class="main">
		        <h1>It looks like you're using an outdated browser</h1>
		        <p>We recommend using the most recent version of your browser for an optimal user experience on our site. Please download the most recent version of one of these browsers:</p>
		
		        <div class="row">
		            <div class="col-xs-3 text-center">
		                <a href="https://www.mozilla.org/en-US/firefox/new/">
		                    <img src="${staticContentPrefix}/assets/images/br-ff.png">
		                    <strong>
		                        Firefox
		                    </strong>
		                </a>
		            </div>
		            <div class="col-xs-3 text-center">
		                <a href="https://www.google.com/chrome/">
		                    <img src="${staticContentPrefix}/assets/images/br-chrome.png">
		                    <strong>
		                        Chrome
		                    </strong>
		                </a>
		            </div>
		            <div class="col-xs-3 text-center">
		                <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">
		                    <img src="${staticContentPrefix}/assets/images/br-ie.png">
		                    <strong>
		                        Internet Explorer
		                    </strong>
		                </a>
		            </div>
		            <div class="col-xs-3 text-center">
		                <a href="https://support.apple.com/downloads/safari">
		                    <img src="${staticContentPrefix}/assets/images/br-safari.png">
		                    <strong>
		                        Safari
		                    </strong>
		                </a>
		            </div>
		        </div>
	        </div>
        </c:if>
	    
	    <c:if test="${compatibilityModeOn}">
	    	<div class="main" style="border: 2px solid green; border-radius: 10px;">
	    		<img src="${staticContentPrefix}/assets/images/logo-turned-comp-mode.jpg" style="margin-top: -50px;">
	    		<br>
	    		<span style="float: left;padding-left: 35px;"><img src="${staticContentPrefix}/assets/images/br-ie.png" style="width: 75px;"></span>
	    		<span><p>Turn off compatibility mode in IE to continue to site.</p></span>
				<br>
	    		<div class="instructions">
					<p>1. Click on the Tools menu tab.</p>
					<p>2. Select the Compatibility View settings option.</p>
					<p>3. Uncheck all compatibility mode options.</p>
				</div>
	    	</div>
	    </c:if>

    </body>
   </html>
</dsp:page>