<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isShowEmail" param="isShowEmail"/>
	<dsp:getvalueof var="isShowPrint" param="isShowPrint"/>
	<dsp:getvalueof var="isShowPDPErrorForm" param="isShowPDPErrorForm" />
	<dsp:getvalueof var="isTransient" bean="Profile.transient" />
	<div class="col-md-4 col-xs-12 hidden-sm hidden-xs text-right page-tools-cover">
		<ul class="page-tools">
			<c:if test="${isShowPDPErrorForm eq 'true'}">
				<li>
					<a href="#" class="action" data-toggle="modal" data-target="#errorFoundOnPDPModal">
						<i class="fa fa-exclamation-triangle gold"></i><span><fmt:message key="header.error_found_pdp_page"/></span>
					</a>
				</li>
			</c:if>

			<c:if test="${!(isShowEmail eq 'false')}">
				<li>
					<a href="#" class="action" data-toggle="modal" data-target="#sharePageModal">
						<i class="fa fa-envelope"></i><span><fmt:message key="account.material.share"/></span>
					</a>
				</li>
			</c:if>
			<input type="hidden"  value="${isTransient}" id="transient" />
			<c:if test="${!(isShowPrint eq 'false')}">
				<li>
					<a href="#" class="action" data-event-click-id="global172" >
						<i class="fa fa-print"></i><span><fmt:message key="header.print_page"/></span>
					</a>
				</li>
			</c:if>
		</ul>
	</div>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		$(document).ready(function() {				
			$('#errorFoundOnPDPModal').on('hidden.bs.modal', function () {
				expireRecaptchaDefault();
			});
			
			$('#errorFoundOnPDPModal').on('shown.bs.modal', function () {
				$('#pdp-err-description').prop('checked',true);
				$('#pdp-err-features').prop('checked',false);
				$('#pdp-err-image').prop('checked',false);
				$('#pdp-err-brochure').prop('checked',false);
				$('#pdp-err-search-nav').prop('checked',false);
				$('#pdp-err-pricing').prop('checked',false);
				$('#pdp-err-others').prop('checked',false);
			 	$("#pdp-err-message-pdpErrorModalEmailAddress").hide();
			 	$("#pdp-err-message-pdpErrorModalFirstName").hide();
			 	$("#pdpErrFoundModalAdditionalInfo").val("");
			 	$("#pdp-err-message-pdpErrorModalEmailAddress").parent().removeClass("has-error");
			 	$("#pdp-err-message-pdpErrorModalFirstName").parent().removeClass("has-error");						
				if ($('#transient').val()=='true') {						
					$('#pdpErrorModalFirstName').val("");
					$('#pdpErrorModalEmailAddress').val("");									
				}
				checkSubmitButton();
			});
		});
	</script>

</dsp:page>
