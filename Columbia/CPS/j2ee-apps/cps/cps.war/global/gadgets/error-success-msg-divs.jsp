<dsp:page>
	<dsp:getvalueof var="addColClass" param="addColClass"/>
	<!-- errors -->
	<div class="${addColClass == 'false' ? '' : 'col-xs-12'}" style="display: none;" id="message-error-div">
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">
				<span class="glyphicon glyphicon-remove-circle"></span>
			</span>
			</button>
			<div id="message-error">
					<%--
								Please, fill in <a class="error-anchor"
												   href="#name-anchor"><span>Name</span></a> and <a
									class="error-anchor"
									href="#company-anchor"><span>Your Compay Name</span></a> field
					--%>
			</div>
		</div>
	</div>

	<!-- success -->
	<div class="${addColClass == 'false' ? '' : 'col-xs-12'}" style="display: none;" id="message-info-div">
		<div class="alert alert-info">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">
				<span class="glyphicon glyphicon-remove-circle"></span>
			</span>
			</button>
			<div id="message-info">
				MSG
			</div>
		</div>
	</div>

</dsp:page>