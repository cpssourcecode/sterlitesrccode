<dsp:page>
<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
    <dsp:getvalueof var="orgId" param="orgId" />
    <dsp:droplet name="/cps/droplet/CSSelectionDroplet">
        <dsp:oparam name="output">
                <option value="" disabled selected>
                    <fmt:message key="account.invoices.selectOptions.shipTo" />
                </option>
                <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                    <dsp:param name="array" param="associatedCSAddr" />
                    <dsp:param name="elementName" value="cs" />
                    <dsp:oparam name="output">
                        <dsp:getvalueof var="csAddress1" param="cs.address1"/>
                        <dsp:getvalueof var="csAddress2" param="cs.address2"/>
                        <dsp:getvalueof var="csAddress3" param="cs.address3"/>
                        <dsp:getvalueof var="city" param="cs.city"/>
                        <dsp:getvalueof var="state" param="cs.state"/>
                        <dsp:getvalueof var="postalCode" param="cs.postalCode"/>
                        <dsp:getvalueof var="csId" param="cs.addressId"/>
                        <dsp:getvalueof var="content" value="${csAddress1}"/>
                        <c:if test="${!empty csAddress2}">
                            <dsp:getvalueof var="content" value="${content}, ${csAddress2}"/>
                        </c:if>
                        <c:if test="${!empty csAddress3}">
                            <dsp:getvalueof var="content" value="${content}, ${csAddress3}" />
                        </c:if>
                        <dsp:getvalueof var="content" value="${content}, ${city},&nbsp;${state}&nbsp;${postalCode}"/>
                        <option value="${csId}" title="${csAddress1}" data-content="${content}">${content}</option>
                    </dsp:oparam>
                </dsp:droplet>

        </dsp:oparam>
    </dsp:droplet>
</dsp:page>
