<dsp:page>
	<input type="hidden" id="redirect-link" value='<dsp:valueof param="url"/>'/>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		window.location.href = '<dsp:valueof param="url"/>';
	</script>
</dsp:page>