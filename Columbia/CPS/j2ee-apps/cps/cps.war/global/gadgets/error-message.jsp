<dsp:page>
	<dsp:getvalueof var="formHandler" param="formHandler"/>
	<dsp:getvalueof var="formExceptions" vartype="java.lang.Object" param="formHandler.formExceptions"/>
	<c:if test="${not empty formExceptions}">
		<c:forEach var="formException" items="${formExceptions}">
			<dsp:param name="formException" value="${formException}"/>
			<%--<dsp:getvalueof var="errorCode" param="formException.property"/>--%>
			<c:if test="${formException.message != ''}">
				
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true"> 
								<span class="glyphicon glyphicon-remove-circle"></span>
							</span>
						</button>
						<dsp:valueof param="formException.message" valueishtml="true"/>
					</div>
				
			</c:if>
		</c:forEach>
	</c:if>
</dsp:page>