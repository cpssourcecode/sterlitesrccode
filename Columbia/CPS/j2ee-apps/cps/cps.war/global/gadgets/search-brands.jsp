<dsp:page>
	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:droplet name="InvokeAssembler">
		<dsp:param name="contentCollection"
			value="/content/Shared/SearchBrandList" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="content"
							vartype="com.endeca.infront.assembler.ContentItem"
							param="contentItem" />
		</dsp:oparam>
	</dsp:droplet>
	
	<c:if test="${not empty content.contents}">
		<dsp:getvalueof var="item" value="${content.contents[0]}"/>
		<dsp:getvalueof var="ntt" param="Ntt"/>
	</c:if>
	
	<c:choose>
		<c:when test="${not empty item && not empty item.dimensionSearchGroups}">
            <ul class="list-unstyled">
                <c:forEach var="dimensionGroups" items="${item.dimensionSearchGroups}">
                    <c:forEach var="dim" items="${dimensionGroups.dimensionSearchValues}">
                        <li style="display: inline-block; width: 33%">
                        	<dsp:droplet name="/cps/droplet/DimensionSeoItemDroplet">
								<dsp:param name="dimValName" value="${dim.label}"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="dimValItem" param="dimValItem" />
									<c:if test="${not empty dimValItem && not empty dimValItem.repositoryId}">
										<a href="${dimValItem.repositoryId}">${vsg_utils:boldText(dim.label,ntt)}</a>
									</c:if>
									<c:if test="${empty dimValItem || empty dimValItem.repositoryId}">
										<a href="${vsg_utils:formatLink(dim.navigationState)}">${vsg_utils:boldText(dim.label,ntt)}</a>
									</c:if>
								</dsp:oparam>
							</dsp:droplet>
                        </li>
                    </c:forEach>
                </c:forEach>
            </ul>
        </c:when>	        
        <c:otherwise>
        	<div class="alert alert-danger"><cp:repositoryMessage key="errNoResults" /></div>
       	</c:otherwise>
  		</c:choose>	
	
</dsp:page>