<dsp:page>
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:getvalueof var="gtmId" bean="Site.googleTagManagerId"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
<c:choose>
	<c:when test="${ not isTransient}">
		<dsp:getvalueof var="userId" bean="Profile.Id"/>
	</c:when>
	<c:otherwise>
		<dsp:getvalueof var="userId" value="" />
	</c:otherwise>
</c:choose>
	
	
	<script nonce="${requestScope.nonce}">
		(function(d) {
			var o = d.createElement;
			d.createElement = function() {
				var e = o.apply(d, arguments);
				if (e.tagName === 'SCRIPT') {
					e.setAttribute('nonce', '${requestScope.nonce}');
				}
				return e;
			}
		})(document);
	</script>

	<c:if test="${not empty gtmId}">
	 	<script nonce="${requestScope.nonce}">dataLayer = [{'userID': '${userId}'}];</script>
		<!-- Google Tag Manager -->
		<script nonce="${requestScope.nonce}">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','${gtmId}');</script>
		<!-- End Google Tag Manager -->
	   
	</c:if>
</dsp:page>