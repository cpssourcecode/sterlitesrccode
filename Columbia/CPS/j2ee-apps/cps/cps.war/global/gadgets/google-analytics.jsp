<dsp:page>
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:getvalueof var="gaId" bean="Site.googleAnalylicId"/>
	<dsp:getvalueof var="gaSite" bean="Site.googleAnalylicSite"/>
	<c:if test="${not prorogueJSIntegrationLoad and not empty gaId}">
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=${gaId}"></script>
		<script nonce="${requestScope.nonce}">
			window.dataLayer = window.dataLayer || [];
			function gtag() {
				dataLayer.push(arguments);
			}

			gtag('js', new Date());

			gtag('config', '${gaId}');
		</script>
	</c:if>
</dsp:page>