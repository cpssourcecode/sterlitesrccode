<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest" scope="request"/>
    <dsp:importbean bean="/cps/util/FileUploadHandler"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:getvalueof var="build_version" bean="/cps/version/Build.version"/>
    <dsp:importbean bean="/atg/userprofiling/Profile" />
    
    <dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
		<dsp:param name="value" bean="CartModifierFormHandler.formExceptions"/>
		<dsp:oparam name="true">
			<dsp:getvalueof var="cmErrorExist" value="${false}"/>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="cmErrorExist" value="${true}"/>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:droplet name="/atg/dynamo/droplet/IsEmpty">
		<dsp:param name="value" bean="FileUploadHandler.formExceptions"/>
		<dsp:oparam name="true">
			<dsp:getvalueof var="fileErrorExist" value="${false}"/>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="fileErrorExist" value="${true}"/>
		</dsp:oparam>
	</dsp:droplet>
    
    <div class="dropdown-menu quick-order-popup" id="quickOrderPanel">
        <dsp:form name="quickAddToCart" formid="quickAddToCartDropdown" id="quickAddToCartDropdown"
                  action="${originatingRequest.contextPath}/" method="post">
            <dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden" value=""/>
            <dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsList"/>
            <dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyList"/>
            <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderPadAddItems" value="true" priority="-10"/>
        </dsp:form>
        <h4>Quick Order Pad</h4>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="${cmErrorExist || (!cmErrorExist && !fileErrorExist) ? 'active' : ''}">
            	<a href="#list-manual" aria-controls="list-manual" role="tab" data-toggle="tab">Manual Entry</a>
            </li>
            <li role="presentation" class="${fileErrorExist && !cmErrorExist ? 'active' : ''}">
            	<a href="#list-import" aria-controls="list-import" role="tab" data-toggle="tab">Import / Upload List</a>
            </li>
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane ${cmErrorExist || (!cmErrorExist && !fileErrorExist) ? 'active' : ''}" id="list-manual">
	            <%-- <c:if test="${param['quickodropdown']}"> --%>
		            <%--<dsp:include page="/account/gadgets/account-error-message.jsp">--%>
		                <%--<dsp:param name="formHandler" bean="CartModifierFormHandler"/>--%>
		            <%--</dsp:include>--%>
	            <%-- </c:if> --%>
	            <div class="quick-order-pad-auto-scroll">
                <div style="display: none;" id="qo-pad-modal-message-error-div">
                    <div class="alert alert-danger">
                        <div id="qo-pad-modal-message-error"></div>
                        <div id="qo-pad-modal-errors"></div>
                    </div>
                </div>

                <div class="well">
                    
                    <c:forEach var="i" begin="1" end="3">
                        <div class="row row-flush">
                        <c:forEach var="j" begin="1" end="2">
                            <div class="col-xs-6 list-item">
                                <input type="text" class="list-num form-control quick-order-item" name="quickproductPad"
                                       placeholder="ITEM # or ALIAS #">
                                <input type="text" class="list-qty form-control text-center" name="quickqtyPad" maxlength="5"
                                       data-event-keypress-id="global166"
                                       placeholder="Qty">
                            </div>
                            </c:forEach>
                        </div>
                        </c:forEach>
                    </div>
                    
                </div>
				<div class="add-more-items"><button class="btn btn-default btn-sm list-add">+ Add More Items</button></div>
                <div class="list-controls">
                    <dsp:droplet name="/cps/droplet/AccessRightDroplet">
						<dsp:param name="profile" bean="Profile" />
						<dsp:param name="accessRightKey" value="plp-pdp-cart" />
						<dsp:oparam name="false">
							<dsp:getvalueof var="permissionDenied" value="true" />
						</dsp:oparam>
					</dsp:droplet>
                 	<c:choose>
	                	<c:when test="${permissionDenied}">
	                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#permissionDenied">
	                            Add to Cart
	                        </a>
	                    </c:when>
	                    <c:otherwise>
	                    	<!-- fix SM-398 Quick order pad - Issue in Add to Cart functionality -->
	                    	<button id="quickOrderAddToPad" data-event-click-id="global165"  class="btn btn-primary addToCartButton">
	                    		Add to Cart
                    		</button>
	                     </c:otherwise>
	                </c:choose>
                
                    <button class="btn btn-info quick-order-close">Cancel</button>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane ${fileErrorExist && !cmErrorExist ? 'active' : ''}" id="list-import">
	            <%-- <c:if test="${param['quickodropdown']}"> --%>
	            	<%--<dsp:include page="/account/gadgets/account-error-message.jsp">--%>
		                <%--<dsp:param name="formHandler" bean="FileUploadHandler"/>--%>
		            <%--</dsp:include>--%>
	            <%-- </c:if> --%>

                <div style="display: none;" id="qo-pad-file-error-div">
                    <div class="alert alert-danger">
                        <div id="qo-pad-file-error"></div>
                    </div>
                </div>

                <dsp:form enctype="multipart/form-data" id="qorderUploadDropdown" method="post" action="">
                    <div class="well p15">

                        <dsp:input type="file" iclass="form-control" bean="FileUploadHandler.quickOrderFile"
                                   name="quickorderfile"/>

                        <!-- <a href="/document/QuickOrderTemplate.xlsx" class="btn btn-default btn-sm list-download"><i
                                class="fa fa-download mr5"></i> Download Template
                        </a><br> -->
                        
                    </div>

                    <div class="list-controls" >
                    	<button id="subUploadButton" type="button" class="btn btn-primary addToCartButton" data-event-click-id="catalog72">Upload</button>
                        <button type="button" class="btn btn-info quick-order-close">Cancel</button>
                        
                        <a class="pull-right importHelpQOP green-link" >
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="infoHowCanIImportFile"/>
								<dsp:param name="notWrap" value="true"/>
							</dsp:include>
						</a>
                    </div>

                    <dsp:getvalueof var="requestURI" bean="OriginatingRequest.requestURI"/>
                    <dsp:getvalueof var="queryString" bean="OriginatingRequest.queryString"/>
                    <dsp:input type="hidden" id="quickosuccess-dropdown"
                               bean="FileUploadHandler.readQuickOrderFileSuccessURL"
                               value="/checkout/cart.jsp?qo=t"/>
                    <dsp:input type="hidden" id="quickoinvalid-dropdown"
                               bean="FileUploadHandler.readQuickOrderInvalidURL" value=""/>
                    <dsp:input type="hidden" id="quickofail-dropdown" bean="FileUploadHandler.readQuickOrderFileFailURL"
                               value=""/>
                    <dsp:input type="hidden" bean="FileUploadHandler.quickOrderPadReadQFile" value="Upload" priority="-10"/>
                </dsp:form>
                
                <div class="p15 well collapse green-text" id="importHelpDropDown">
                	<span>
                		<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="infoImportFileInstruction"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include>
						<p>
							<span class="pull-right importHelpQOP green-link">CLOSE</span>&nbsp;
						</p>
                	</span>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" nonce="${requestScope.nonce}">
        $(document).ready(function () {
            function removeURLParameterOP(params, parameter) {
                var pars = params.split(/[&;]/g);
                var prefix = encodeURIComponent(parameter) + '=';
                //reverse iteration as may be destructive
                for (var i = pars.length; i-- > 0;) {
                    //idiom for string.startsWith
                    if (pars[i].lastIndexOf(prefix, pars[i].length - 1 ) !== -1) {
                        pars.splice(i, 1);
                    }
                }

                params = pars.join('&');
                return params;
            }
            var baseParams = removeURLParameterOP(window.location.search, "quickomodal");
            baseParams = removeURLParameterOP(baseParams, "_requestid");
            baseParams = removeURLParameterOP(baseParams, "productId");
            baseParams = removeURLParameterOP(baseParams, 'sreqmodal');
            baseParams = removeURLParameterRQ(baseParams, 'showCS');
            baseParams = removeURLParameterRQ(baseParams, 'qo');
            baseParams = removeURLParameterRQ(baseParams, 'invalidskumodal');
            baseParams = removeURLParameterRQ(baseParams, 'quickomodal');
            if (baseParams == "") {
                //?
                //$('#quickosuccess').val("${requestURI}"+"?quickomodal=false");

                $('#quickcerror-dropdown').val(window.location.pathname + "?quickodropdown=true");
                $('#quickofail-dropdown').val(window.location.pathname + "?quickodropdown=true");
                $('#quickoinvalid-dropdown').val(window.location.pathname + "?invalidskumodal=true");
                $('#invalidModallink-dropdown').val(window.location.pathname + "?invalidskumodal=true");
            }
            else {
                //&
                //$('#quickosuccess').val("${requestURI}"+"?"+baseParams+"&quickomodal=false");
                $('#quickcerror-dropdown').val(window.location.pathname + "?" + baseParams + "&quickodropdown=true");
                $('#quickofail-dropdown').val(window.location.pathname + "?" + baseParams + "&quickodropdown=true");
                $('#quickoinvalid-dropdown').val(window.location.pathname + "?" + baseParams + "&invalidskumodal=true");
                $('#invalidModallink-dropdown').val(window.location.pathname + "?" + baseParams + "&invalidskumodal=true");
            }

            $(".quick-order-popup").click(function(event) {
                event.stopPropagation();
            });
            $(".quick-order-close").click(function() {
                $("#orderPadDropdownLink").click();
           		//$(this).parents('.dropdown').find('.dropdown-toggle').dropdown('toggle');
            });
            
            $('.importHelpQOP').click(function() {
            	$("#importHelpDropDown").toggle();
            });
        });

    </script>
</dsp:page>