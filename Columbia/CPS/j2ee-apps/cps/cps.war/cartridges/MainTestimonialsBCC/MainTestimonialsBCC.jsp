<dsp:droplet name="/atg/targeting/TargetingRandom">
	<dsp:param bean="/atg/registry/RepositoryTargeters${contentItem.componentPath}" name="targeter" />
	<dsp:param name="howMany" value="1" />
	<dsp:oparam name="output">
		<dsp:getvalueof var="element" param="element" />
		<dsp:getvalueof var="organization" param="element.organization" />
		<dsp:getvalueof var="person" param="element.person" />
		<dsp:getvalueof var="summary" param="element.summary" />
		
		<div class="row">
			<div class="col-xs-12">
				<div class="well visible-xs centered-mobile">
					<h3>Our Customers Love Us</h3>
					<blockquote>
						<p>${summary} &#8221;</p>
						<footer>
							${person} in - <cite title="Source Title"> ${organization} </cite>
						</footer>
					</blockquote>
					<a class="testimonial-detail" href="${contentItem.url}"><span>Read More
							testimonials</span> <span class="glyphicon glyphicon-menu-right"></span></a>
				</div>
			</div>
		</div>
	</dsp:oparam>

</dsp:droplet>

