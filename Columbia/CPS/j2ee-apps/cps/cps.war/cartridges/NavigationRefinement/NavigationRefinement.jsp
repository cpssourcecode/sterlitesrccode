<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof param="dimIndex" var="dimIndex"/>
	<dsp:getvalueof var="collapsed" param="collapsed"/>
	<fmt:bundle basename="web.resources.WebAppResources">
	<c:choose>
		<c:when test="${content.sort eq STATIC}">
			<c:set var="refinements" value="${content.multiSelect ? vsg_utils:multiRefinements(breadcrumbs.refinementCrumbs,content.refinements,content.dimensionName) : content.refinements}"/>
		</c:when>
		<c:otherwise>
			<c:set var="refinements" value="${content.refinements}"/>
		</c:otherwise>
	</c:choose>
	
		<c:if test="${not empty refinements && !(breadcrumbs.refinementCrumbs[0].dimensionName eq 'product.mfr_fullname' && content.dimensionName eq 'product.mfr_fullname' && empty breadcrumbs.searchCrumbs[0])}">
		<!-- navigation refinement -->
		<li class="expand-item facet-item">
			<span class="expand-label active">${content.name}
				<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span>
			</span>
			<ul class="facet-refine expand-content active">
			<c:forEach items="${refinements}" var="refinement" varStatus="status" >
				
				<c:set var="uri" value="${vsg_utils:changeUrlToLower(refinement.navigationState)}"/>
				 
				  <c:set var='uri1' value='${vsg_utils:escapeHtml4(uri)}'/> 
				 
				<c:choose>
					<c:when test="${content.multiSelect}">
					<li class="checkbox">
						<label><input type="checkbox" data-event-change-id="cartridges9" data-event-change-param0="${uri1}"  ${refinement.properties.selected ? 'checked':''}>
							${refinement.label}&nbsp;${refinement.properties['category.description']}
							<small>(${refinement.count})</small>
						</label>
					</li>
					</c:when>
					<c:otherwise>
					<li>
						<a href="${uri1}" data-event-click-id="cartridges8" data-event-click-param0="${uri1}">${refinement.label} <small>(${refinement.count})</small></a>
					</li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			</ul>
		</li>
		</c:if>
	</fmt:bundle>
</dsp:page>