<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<div class="container main-content">
		<div class="row">
			<div class="col-sm-12">
				<h1>We'll be back soon.</h1>
				<h2>
					Our apologies. The Columbia Pipe website is <strong>temporary</strong> unavailable due to <strong>scheduled maintenance</strong>.
				</h2>
				<p>
					We will be performing maintenance on columbiapipe.com on <dsp:valueof value="${content.day}" valueishtml="true" />,
					<dsp:valueof value="${content.date}" valueishtml="true" /> between
					<dsp:valueof value="${content.timeFrame}" valueishtml="true" />
					Central time. During the time, columbiapipe.com will be unavailable. We hope this won't be too
					much of an inconvenience as we work to perform necessary upgrades as quickly as possible.
				</p>
				<p>
					<strong>Be sure to check back soon.</strong> In the meantime, here are some resources you may
					be interested in.
				</p>
				<dsp:valueof value="${content.resourcesList}" valueishtml="true" />
				<p>
					Sincerely,<br /> 
					Your Columbia Pipe Online Team
				</p>
			</div>
		</div>
	</div>
</dsp:page>