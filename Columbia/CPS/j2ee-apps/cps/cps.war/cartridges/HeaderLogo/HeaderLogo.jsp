<dsp:page>
    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
    <dsp:getvalueof var="showHeaderLinks" param="showHeaderLinks"/>
    <c:choose>
        <c:when test="${showHeaderLinks}">
            <dsp:getvalueof var="logoLink" value="/"/>
        </c:when>
        <c:otherwise>
            <dsp:getvalueof var="logoLink" value="javascript:void(0);"/>
        </c:otherwise>
    </c:choose>

    <div class="col-md-5 header-logo">
        <a href="${logoLink}" class="logo hidden-print"><cp:readerimg src="${content.url}" alt="" /></a>
        <%--<img src="${staticContentPrefix}/assets/images/header-logo.png">--%>
    </div>
</dsp:page>

