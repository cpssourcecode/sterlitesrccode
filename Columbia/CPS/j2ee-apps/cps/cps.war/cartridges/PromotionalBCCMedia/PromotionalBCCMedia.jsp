<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>

<%--
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" param="contentItem.items"/>
		<dsp:oparam name="output">
			&lt;%&ndash;<dsp:getvalueof var="mediaURL" param="element.media_url"/>&ndash;%&gt;
			<dsp:getvalueof var="mediaURL" param="element.html"/>

			&lt;%&ndash;<div data-img="content_images/banner_industrial.jpg">&ndash;%&gt;
			<div data-img="${mediaURL}">
				<div class="fotorama__select">
					<h2>
						<dsp:getvalueof var="mediaName" param="element.displayName"/>
						&lt;%&ndash;<a href="#">Steel Pipe</a>&ndash;%&gt;
						<a href="#">${mediaName}</a>
					</h2>
					<dsp:getvalueof var="mediaDescription" param="promo.description"/>
					&lt;%&ndash;<p>Carbon Steel 1/8" through 24" O.D.</p>&ndash;%&gt;
					<p>${mediaDescription}</p>
				</div>
			</div>

		</dsp:oparam>
	</dsp:droplet>
--%>

	<dsp:droplet name="/atg/targeting/TargetingFirst">
		<%--<dsp:param bean="/atg/registry/RepositoryTargeters/HomePageTargeters/homePageBannerLeft" name="targeter"/>--%>
		<dsp:param bean="/atg/registry/RepositoryTargeters${contentItem.componentPath}" name="targeter"/>
		<dsp:param name="howMany" value="3"/>
		<dsp:oparam name="output">
			 			<%--<dsp:getvalueof var="mediaURL" param="element.media_url"/>--%>
			<dsp:getvalueof var="mediaURL" param="element.html"/>

			<%--<div data-img="content_images/banner_industrial.jpg">--%>
			<div data-img="${mediaURL}">
				<div class="fotorama__select">
					<h2>
						<dsp:getvalueof var="mediaName" param="element.displayName"/>
							<%--<a href="#">Steel Pipe</a>--%>
						<a href="#">${mediaName}</a>
					</h2>
					<dsp:getvalueof var="mediaDescription" param="promo.description"/>
						<%--<p>Carbon Steel 1/8" through 24" O.D.</p>--%>
					<p>${mediaDescription}</p>
				</div>
			</div>

		</dsp:oparam>

	</dsp:droplet>

</dsp:page>