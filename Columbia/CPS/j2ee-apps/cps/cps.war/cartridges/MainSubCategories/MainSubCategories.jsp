<dsp:page>
	<dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>

	<dsp:getvalueof var="showCS" param="showCS"/>
	<dsp:getvalueof var="onHold" param="onHold"/>
	<dsp:getvalueof var="categoryRefinements" param="contentItem.refinements"/>
	<dsp:getvalueof var="noImg" param="contentItem.noImgUrl"/>
	<dsp:getvalueof var="lowerNoImgUrl" value="${fn:toLowerCase(noImg)}"/>
	<c:choose>
		<c:when test="${fn:startsWith(lowerNoImgUrl,'http')}">
			<dsp:getvalueof var="noImgUrl" value="${lowerNoImgUrl}"/>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="noImgUrl" value="${staticContentPrefix}${lowerNoImgUrl}"/>
		</c:otherwise>
	</c:choose>	
	<c:choose>
		<c:when test="${not empty categoryRefinements}">
			<dsp:droplet name="/cps/droplet/GetCategoryDescriptionDroplet">
				<dsp:param name="childCategoryId" value="${categoryRefinements[0].properties['category.repositoryId']}"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="longDescription" param="longDescription"/>
						<div class="row">
							<div class="col-xs-12">
								<p><dsp:valueof param="longDescription" valueishtml="true"/></p>
							</div>
						</div>
					</dsp:oparam>
			</dsp:droplet>
			
			<section class="hidden-xs">
				<ul class="row cat-list">
					<c:forEach items="${categoryRefinements}" var="refinement">
						<dsp:getvalueof var="catId" value="${refinement.properties['category.repositoryId']}"/>
						<dsp:droplet name="CategoryLookup">
							<dsp:param name="id" value="${catId}"/>
							<dsp:setvalue param="category" paramvalue="element"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="imageURL" param="category.thumbnail_url"/>
								<dsp:getvalueof var="metaKeywords" param="category.metaKeywords"/>
								<dsp:getvalueof var="metaDescription" param="category.metaDescription"/>
							</dsp:oparam>
							<dsp:oparam name="empty">
							</dsp:oparam>
						</dsp:droplet>
						<dsp:getvalueof var="seoTags" bean="/cps/seo/SEOTagHolder.seoTags" scope="request"></dsp:getvalueof>
						<dsp:droplet name="/atg/dynamo/droplet/RQLQueryRange">
							<dsp:param name="repository" value="/atg/seo/SEORepository" />
							<dsp:param name="itemDescriptor" value="SEOTags" />
							<dsp:param name="howMany" value="1" />
							<dsp:param name="pageUrl" value="${fn:toLowerCase(refinement.navigationState)}" />
							<dsp:param name="queryRQL" value="key = :pageUrl" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="seoTitle" param="element.title" />
							</dsp:oparam>
							<dsp:oparam name="empty">
								<dsp:getvalueof var="seoTitle" value="" />
							</dsp:oparam>
						</dsp:droplet>

						<c:choose>
							<c:when test="${catId == 'ZZZ' or catId == 'zzz'}">
							</c:when>
							<c:otherwise>
								<c:if test="${empty imageURL}">
									<dsp:getvalueof var="imageURL" value="${noImgUrl}"/>
								</c:if>
								<li class="col-xs-3 col-xxs-4 text-center">
									<a href="${fn:toLowerCase(refinement.navigationState)}" data-event-click-id="cartridges7" data-event-click-param0="${fn:toLowerCase(refinement.navigationState)}" 
										data-event-click-param1="${metaDescription}" data-event-click-param2="${metaKeywords}" >
								<span class="cat-thumb">
								<dsp:getvalueof var="lowerImageURL"	value="${fn:toLowerCase(imageURL) }" />
									<c:choose>
										<c:when test="${fn:startsWith(lowerImageURL,'http')}">
											<img data-src="${imageURL}" alt="${refinement.label}" data-fallback-image="${noImgUrl}">
										</c:when>
										<c:otherwise>
											<img data-src="${staticContentPrefix}${imageURL}" alt="${refinement.label}" data-fallback-image="${noImgUrl}">
										</c:otherwise>
									</c:choose>
								</span>
								<span style="display:none" class="seoRepositoryTitle">${seoTitle}</span>
										<c:choose>
											<c:when test="${breadcrumbs.refinementCrumbs[0].dimensionName eq 'product.mfr_fullname'}">
												<h5 class="catalogTitleName">${breadcrumbs.refinementCrumbs[0].label}<br>${refinement.label}&nbsp;${refinement.properties['category.description']}</h5>
											</c:when>
											<c:otherwise>
												<h5 class="catalogTitleName">${refinement.label}&nbsp;${refinement.properties['category.description']}</h5>
											</c:otherwise>
										</c:choose>
									</a>
								</li>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</ul>
			</section>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>

	<div id="modal-on-hold-show-div"></div>

	<div id="modal-cs-show-div"></div>

	<dsp:getvalueof param="loadSection" var="loadSection"/>

	<c:if test="${loadSection != 'inner'}">
		<script type="text/javascript" nonce="${requestScope.nonce}">

			$(document).ready(function () {
				checkShowOnHold();
				$(".cat-image").one("load", function() {
					equalizeCategory();
				}).each(function() {
					if(this.complete){
						$(this).load();
					}
				});
			});

			function checkShowCS() {confirmBtn2
				if ('${vsg_utils:escapeJS(showCS)}' == 'true') {
					showSelectShippingAddress()
				}
			}

			function checkShowOnHold() {
				if ('${vsg_utils:escapeJS(onHold)}' == 'true') {
					showOnHold('${vsg_utils:escapeJS(showCS)}');
				} else {
					checkShowCS();
				}
			}
		</script>
	</c:if>
</dsp:page>

