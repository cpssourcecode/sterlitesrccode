<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:importbean bean="/atg/commerce/endeca/cache/DimensionValueCacheDroplet"/>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>


	<div class="row home-slider">

		<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
			<div class="fotorama" data-autoplay="6000" data-transition="fade" data-clicktransition="fade" data-swipetransition="slide" data-fit="cover" data-loop="true" data-width="100%"
				 data-height="400px" data-minheight="250px">

				<c:forEach var="mainItem" items="${contentItem.promoCarousel}">
					<dsp:renderContentItem contentItem="${mainItem}"/>
				</c:forEach>

<%--
				<div data-img="content_images/banner_industrial.jpg">
					<div class="fotorama__select">
						<h2><a href="#">Steel Pipe</a>
						</h2>

						<p>Carbon Steel 1/8" through 24" O.D.</p>
					</div>
				</div>

				<div data-img="content_images/banner_for_customers_truck.jpg">
					<div class="fotorama__select">
						<h2><a href="#">Seamless Pipe and Pressure Tubing</a>
						</h2>

						<p>Carbon Steel 1/8" through 24" O.D.</p>
					</div>
				</div>

				<div data-img="content_images/locations_banner1.jpg">
					<div class="fotorama__select">
						<h2><a href="#">Epoxy Coated for Underground</a>
						</h2>

						<p>Carbon Steel 1/8" through 24" O.D.</p>
					</div>
				</div>
--%>

			</div>
		</div>


		<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 hidden-xs">
			<div class="row">

				<c:forEach var="mainItem" items="${contentItem.promoBanners}">
					<dsp:renderContentItem contentItem="${mainItem}"/>
				</c:forEach>


<%--
				<div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 well-advert">
					<a href="#" class="well well-sm" style="background: url(content_images/news_events_banner.jpg);">
						<div class="description">
							<h4>Brand New Technologies</h4>

							<p>Lorem ipsum dolor.</p>
						</div>
					</a>
				</div>

				<div class="col-lg-12 col-md-12 col-sm-6 col-xs-6 well-advert">
					<a href="#" class="well well-sm" style="background: url(content_images/steam_banner2.jpg);">
						<div class="description">
							<h4>Advertisment Banner</h4>

							<p>Lorem ipsum dolor.</p>
						</div>
					</a>
				</div>
--%>

			</div>
		</div>
	</div>


</dsp:page>