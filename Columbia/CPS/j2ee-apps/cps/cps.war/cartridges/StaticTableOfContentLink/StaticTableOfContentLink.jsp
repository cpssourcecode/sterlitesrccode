<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<li><a href="${content.tableOfContentLinkUrl}">${content.tableOfContentLinkTitle}</a></li>
</dsp:page>