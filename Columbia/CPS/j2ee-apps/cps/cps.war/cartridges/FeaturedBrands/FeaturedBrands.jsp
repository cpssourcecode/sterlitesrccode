<dsp:page>
    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>

    <c:if test="${!empty content.content && fn:length(content.content) > 0}">
        <section>
            <h4 class="bar mb20">Featured Brands</h4>
            <div class="brands-featured row row-narrow">
                <c:forEach items="${content.content}" var="i">
                    <dsp:renderContentItem contentItem="${i}"/>
                </c:forEach>
            </div>
        </section>
    </c:if>

</dsp:page>
