<dsp:page>
	<c:if test="${(not empty contentItem.parentPath) && (not empty contentItem.parentDisplayTitle)}">
	<li><a href="${contentItem.parentPath}"><dsp:valueof value="${contentItem.parentDisplayTitle}" valueishtml="true" /></a></li>
	</c:if>
	<li class="active"><dsp:valueof param="contentTitle" valueishtml="true" /></li>
</dsp:page>