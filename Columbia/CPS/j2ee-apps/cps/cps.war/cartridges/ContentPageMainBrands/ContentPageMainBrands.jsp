<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="totalNumRecs" param="totalNumRecs"/>

	<main id="body">
	   <div class="container">
	  		<ol class="breadcrumb">
				<li><a href="" data-event-click-id="cartridges2" >All Products</a></li>
				<li class="active">Brands</li>
			</ol>
	   		
	   		<h1 class="mb20">Brands</h1>
	
			<dsp:renderContentItem contentItem="${content.featuredBrands}" />
			
			<dsp:renderContentItem contentItem="${content.allBrands}" />
			<%-- <c:forEach var="navigation" items="${content.navigation}">
				<dsp:renderContentItem contentItem="${navigation}" />
			</c:forEach> --%>
			
			<%-- <dsp:include src="${content.pagePath}"/> --%>
			
		</div>
	</main>
</dsp:page>