<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<c:if test="${not empty content.href}">
		<a href="${content.href}">
	</c:if>
	<cp:readerimg src="${content.url}" alt="${content.alt}"/>
	<c:if test="${not empty content.href}">
		</a>
	</c:if>
</dsp:page>