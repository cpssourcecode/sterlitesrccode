<dsp:page>
    
    <dsp:importbean bean="/atg/userprofiling/Profile"/>

    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>

    <dsp:getvalueof var="transient" bean="Profile.transient"/>

	<section class="hidden-xs">
   		<div class="well well-grey">
   			<div class="row">
   				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
   					<div class="input-group input-group-search">
   						<input type="text" class="form-control text-center" placeholder="Search Brands" id="brandsSearch">
   						<div class="input-group-btn">
							<button class="btn btn-primary" id="brandsSearchBtn" data-event-click-id="cartridges0" >Go</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section class="sitemap-list hidden-sm hidden-xs allBrandsWide">
	   	<div class="row">
	   		<dsp:getvalueof var="modVal" value="${fn:length(content.refinements) % 4}"/>
			<dsp:getvalueof var="divVal" value="${fn:length(content.refinements) / 4}"/>
			<dsp:getvalueof var="refCounter" value="${0}"/>
			<dsp:getvalueof var="divValCounter" value="${0}"/>
			
	   		<c:forEach var="splittedRefinementItem" items="${content.splittedRefinements}">
	   			<c:if test="${refCounter eq 0 || refCounter ge divValCounter}">
	   				<dsp:getvalueof var="divValCounter" value="${divValCounter + divVal}"/>
	   				<div class="col-sm-3">
	   			</c:if>
	   			<h4>${splittedRefinementItem.key}</h4>
   				<ul class="list-unstyled">
   					<c:forEach var="refinement" items="${splittedRefinementItem.value}">
						<li><a href="${fn:toLowerCase(refinement.navigationState)}">${refinement.label}</a></li>
					</c:forEach>
				</ul>
				<dsp:getvalueof var="refCounter" value="${refCounter + fn:length(splittedRefinementItem.value)}"/>
				<c:if test="${refCounter ge divValCounter}">
	   				</div>
	   			</c:if>
			</c:forEach>			
	   	</div>
	</section>
	<section>
		<div class="row" style="margin-left: 0">
			<div id="brands_list_div" style="display: none" class="hidden-xs">
			</div>
		</div>
	</section>
	
	<c:set var="sharp" value="#" scope="request" />
	<c:set var="ae" value="${fn:split('A,B,C,D,E', ',')}" scope="request" />
	<c:set var="fj" value="${fn:split('F,G,H,I,J', ',')}" scope="request" />
	<c:set var="ko" value="${fn:split('K,L,M,N,O', ',')}" scope="request" />
	<c:set var="pt" value="${fn:split('P,Q,R,S,T', ',')}" scope="request" />
	<c:set var="uz" value="${fn:split('U,V,W,X,Y,Z', ',')}" scope="request" />
	
	<section class="visible-sm visible-xs" id="tablet_brands">
	   	<ul class="expand">
	   		<c:if test="${not empty content.splittedRefinements['#']}">
	   			<li class="expand-item"><span class="expand-label">#<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span></span>
					<ul class="expand-content" style="display: none;">
						<c:forEach var="refinement" items="${content.splittedRefinements['#']}">
							<li><a href="${fn:toLowerCase(refinement.navigationState)}">${refinement.label}</a></li>
						</c:forEach>
					</ul>
				</li>
	   		</c:if>
	   		
	   		<li class="expand-item"><span class="expand-label">A - E<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span></span>
	   			<ul class="expand-content" style="display: none;">
			   		<c:forEach var="letter" items="${ae}">
						<c:if test="${not empty content.splittedRefinements[letter]}">
							<li class="expand-subhead">${letter}</li>
							<c:forEach var="refinement" items="${content.splittedRefinements[letter]}">
								<li><a href="${fn:toLowerCase(refinement.navigationState)}">${refinement.label}</a></li>
							</c:forEach>
						</c:if>
			   		</c:forEach>
		   		</ul>
	   		</li>
	   		<li class="expand-item"><span class="expand-label">F - J<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span></span>
	   			<ul class="expand-content" style="display: none;">
			   		<c:forEach var="letter" items="${fj}">
			   			<c:if test="${not empty content.splittedRefinements[letter]}">
			   				<li class="expand-subhead">${letter}</li>
							<c:forEach var="refinement" items="${content.splittedRefinements[letter]}">
								<li><a href="${fn:toLowerCase(refinement.navigationState)}">${refinement.label}</a></li>
							</c:forEach>
				   		</c:if>
			   		</c:forEach>
		   		</ul>
	   		</li>
	   		<li class="expand-item"><span class="expand-label">K - O<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span></span>
	   			<ul class="expand-content" style="display: none;">
			   		<c:forEach var="letter" items="${ko}">
						<c:if test="${not empty content.splittedRefinements[letter]}">
							<li class="expand-subhead">${letter}</li>
							<c:forEach var="refinement" items="${content.splittedRefinements[letter]}">
								<li><a href="${fn:toLowerCase(refinement.navigationState)}">${refinement.label}</a></li>
							</c:forEach>
						</c:if>
			   		</c:forEach>
		   		</ul>
	   		</li>
	   		<li class="expand-item"><span class="expand-label">P - T<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span></span>
	   			<ul class="expand-content" style="display: none;">
			   		<c:forEach var="letter" items="${pt}">
						<c:if test="${not empty content.splittedRefinements[letter]}">
							<li class="expand-subhead">${letter}</li>
							<c:forEach var="refinement" items="${content.splittedRefinements[letter]}">
								<li><a href="${fn:toLowerCase(refinement.navigationState)}">${refinement.label}</a></li>
							</c:forEach>
						</c:if>
			   		</c:forEach>
				</ul>
	   		</li>
	   		<li class="expand-item"><span class="expand-label">U - Z<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span></span>
	   			<ul class="expand-content" style="display: none;">
			   		<c:forEach var="letter" items="${uz}">
						<c:if test="${not empty content.splittedRefinements[letter]}">
							<li class="expand-subhead">${letter}</li>
							<c:forEach var="refinement" items="${content.splittedRefinements[letter]}">
								<li><a href="${fn:toLowerCase(refinement.navigationState)}">${refinement.label}</a></li>
							</c:forEach>
						</c:if>
			   		</c:forEach>
		   		</ul>
	   		</li>
		</ul>
	</section>
	
	<script nonce="${requestScope.nonce}">
		activateSearchBrand();
	</script>

</dsp:page>


