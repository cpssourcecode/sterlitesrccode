<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<c:if test="${not empty content.targeter_full_path}">
		<dsp:droplet name="/vsg/droplet/VSGTargetingForEach">
			<dsp:param name="targeterName" value="${content.targeter_full_path}" />
			<dsp:param name="profile" bean="/atg/userprofiling/Profile" />
			<dsp:param name="maxNumber" value="${content.maxNumber}" />
			<dsp:param name="random" value="${content.showRandom}" />
			<dsp:param name="tagFilter" value="${navRefinement.label}" />
			<dsp:param name="elementName" value="categoryPromo" />
			<dsp:param name="sortProperties" value="order" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="promoHTML" param="categoryPromo.html" />
				<c:choose>
					<c:when test="${empty promoHTML}">
						<dsp:getvalueof var="promoImage" param="categoryPromo.url" />
						<div class="row">
							<div class="col-md-12" style="margin-top:9px;">
								<div class="content-box box-default promo-banner-a" style="padding:0">
									<cp:readerimg src="${promoImage}" style="width:100%;" alt="Image"/>
								</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="row">
							<div class="col-md-12" style="margin-top:9px;">
								<div class="content-box box-default promo-banner-a">
									<dsp:valueof value="${promoHTML}" valueishtml="true" />
								</div>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
</dsp:page>

