<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="myAccount" vartype="com.endeca.infront.assembler.ContentItem" param="myAccount" />
	
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:droplet name="/cps/droplet/CategoryNavigationTreeDroplet">
		<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
		<dsp:param name="catCount" value="${content.catCount}" />
		<dsp:param name="subCatCount" value="0" />
		<dsp:param name="sortProperties" value="displayName" />
		<dsp:oparam name="output">
			<div class="col-md-4 col-sm-4 col-xs-6 col-xxs-12">
				<strong>Products</strong>
				<ul class="list-unstyled mobile-col-2">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="rootCategoriesMap" />
						<dsp:param name="elementName" value="rootCat"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="rootCatName" param="rootCat"/>
							<dsp:getvalueof var="rootCatURL" param="key"/>
							<li><a href="${rootCatURL}">${rootCatName}</a></li>
						</dsp:oparam>
					</dsp:droplet>
				</ul>

				<div class="visible-xs">
					<dsp:renderContentItem contentItem="${myAccount}"/>
				</div>

			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>