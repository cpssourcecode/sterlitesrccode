<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<c:set var="url" value="${content.url}"/>
	<c:set var="imageURL" value="${content.imageURL}"/>
	<c:set var="alt" value="${content.alt}"/>
	<div class="col-sm-20 col-xs-6">
		<a href="${not empty url ? url : '#'}">
			<cp:readerimg src="${not empty imageURL ? imageURL : ''}"
				 alt="${not empty alt ? alt : ''}" />
		</a>
	</div>
</dsp:page>

