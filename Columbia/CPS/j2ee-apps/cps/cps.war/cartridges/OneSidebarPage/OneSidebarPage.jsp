<dsp:page>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
   <dsp:importbean bean="/atg/commerce/endeca/cache/DimensionValueCacheDroplet" />
  <%-- <dsp:importbean bean="/cps/droplet/ReadOriginatingRequest"/> --%>
  <dsp:importbean bean="/atg/commerce/catalog/CatalogNavigation" />


        <%-- <dsp:droplet name="ReadOriginatingRequest">
                 <dsp:oparam name="output">
                <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

                </dsp:oparam>
        </dsp:droplet>

		<c:if test="${empty contentItem}" >
         	 	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
		 </c:if> --%>

	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

      <cp:pageContainer page="catalog" metaKeywords="${contentItem.metaKeywords}" metaDescription="${contentItem.metaDescription}">

 <%-- <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/> --%>

 			<dsp:droplet name="/atg/commerce/catalog/custom/CatalogLookup">
 				 <dsp:param name="id" value="homeStoreCatalog" />
 				 <dsp:oparam name="output">
 				  <dsp:getvalueof var="topLevelCatalog" param="element" />
 				 </dsp:oparam>
 			</dsp:droplet>

 			<dsp:getvalueof var="topLevelCategoryId" bean="CatalogNavigation.currentCategory" />





         		    			<dsp:droplet name="/atg/commerce/catalog/CategoryLookup">
										<dsp:param name="id" value="${topLevelCategoryId}"/>
										<dsp:param name="catalog" value="${topLevelCatalog}"/>
										<dsp:oparam name="empty" >
										<c:out value="NOT FOUND" />
										</dsp:oparam>
										<dsp:oparam name="wrongCatalog" >
										<c:out value="wrong catalog" />
										</dsp:oparam>
										<dsp:oparam name="noCatalog" >
										<c:out value="no catalog" />
										</dsp:oparam>
										<dsp:oparam name="output">
										<dsp:getvalueof var="topLevelDisplayName" param="element.displayName" />

											<dsp:droplet name="DimensionValueCacheDroplet">
												<dsp:param name="repositoryId" value="${topLevelCategoryId}"/>
												<dsp:oparam name="output">
													 <dsp:getvalueof var="topLevelCategoryCacheEntry" param="dimensionValueCacheEntry" />
												</dsp:oparam>
											</dsp:droplet>

										<div id="breadcrumb">
      										<div class="center">
     	 										<p><a href="/index.jsp">Home</a> &raquo; <dsp:a href="${topLevelCategoryCacheEntry.url}"> <dsp:property bean="/atg/commerce/catalog/CatalogNavigation.currentState" value="sectionpage" /><dsp:valueof value="${topLevelDisplayName}" /></dsp:a></p>

     										 </div>
    									</div>
   										 <div id="body">
  										 <div class="center">



									      <%-- Render the main content --%>

									            <div id="sidebar">

											 <h3><dsp:valueof value="${topLevelDisplayName}" /></h3>

										    <dsp:getvalueof var="subcategories" param="element.childCategories" />

												 <ul class="subnav accordion">


									 				 <c:forEach var="subcategory" items="${subcategories}" varStatus="status">
									 					<dsp:getvalueof var="secondLevelCategoryId" value="${subcategory.repositoryId}" />
									         		    	 	<dsp:droplet name="/atg/commerce/catalog/CategoryLookup">
																	<dsp:param name="id" value="${secondLevelCategoryId}"/>
																	<dsp:param name="catalog" value="${topLevelCatalog}"/>
																	<dsp:oparam name="empty" >
																		<c:out value="NOT FOUND" />
																	</dsp:oparam>
																	<dsp:oparam name="wrongCatalog" >
																		<c:out value="wrong catalog" />
																	</dsp:oparam>
																	<dsp:oparam name="noCatalog" >
																		<c:out value="no catalog" />
																	</dsp:oparam>
																	<dsp:oparam name="output">

												  					<dsp:getvalueof var="secondLevelDisplayName" param="element.displayName" />
																    <dsp:getvalueof var="subcategories" param="element.childCategories" />

																	<dsp:droplet name="DimensionValueCacheDroplet">
												      				<dsp:param name="repositoryId" value="${secondLevelCategoryId}"/>
												     				 <dsp:oparam name="output">
												       					 <dsp:getvalueof var="secondLevelCategoryCacheEntry" param="dimensionValueCacheEntry" />
												      				</dsp:oparam>
												    				</dsp:droplet>

									              					<dsp:getvalueof var="subcategoryDisplayName" value="${secondLevelDisplayName}" />

												         		  	<li><span class="subnav-header"><a href="${secondLevelCategoryCacheEntry.url}"><dsp:valueof value="${subcategoryDisplayName}" /></a></span>


		      												        <ul>
														                <li><a href="#">Category</a></li>
														                <li><a href="#">Category</a></li>
														                <li><a href="#">Category</a></li>
														                <li><a href="#">Category</a></li>
														                <li><a href="#">Category</a></li>
														              </ul>
														            </li>

																  </dsp:oparam>
																</dsp:droplet>
															    </c:forEach>


													</ul>


           			 </div>






           <div id="content">
	          <div class="slideshow">
	            <ul>
	              <li><a href="#"><img src="${staticContentPrefix}/fpo/callout-700x290.gif" /></a></li>
	              <li><a href="#"><img src="${staticContentPrefix}/fpo/callout-700x290.gif" /></a></li>
	              <li><a href="#"><img src="${staticContentPrefix}/fpo/callout-700x290.gif" /></a></li>
	              <li><a href="#"><img src="${staticContentPrefix}/fpo/callout-700x290.gif" /></a></li>
	              <li><a href="#"><img src="${staticContentPrefix}/fpo/callout-700x290.gif" /></a></li>
	            </ul>
	          </div>
	          <h1>Section Title</h1>
	          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum erat eu nisi sodales suscipit. Curabitur sit amet justo a magna molestie tempus id a sapien. Pellentesque lacinia nisi et nibh vehicula lacinia.</p>
	          <br />
	          <div class="tab"> <a class="button button-manu" href="#">Shop by Manufacturer</a>
	            <ul class="tab-nav">
	              <li><a href="#browse">Browse Products</a></li>
	              <li><a href="#fpo1">Tab FPO</a></li>
	              <li><a href="#fpo2">Tab FPO</a></li>
	            </ul>
	            <div class="tab-content" id="browse">
	              <h2 class="print-only">Browse Products</h2>
	              <ul class="cats">
	                <li><a href="#"> <img src="${staticContentPrefix}/fpo/product-140x140.gif" class="cats-thumb" />
	                  <h3>Group Name</h3>
	                  </a></li>
	                <li><a href="#"> <img src="${staticContentPrefix}/fpo/product-140x140.gif" class="cats-thumb" />
	                  <h3>Group Name</h3>
	                  </a></li>
	                <li><a href="#"> <img src="${staticContentPrefix}/fpo/product-140x140.gif" class="cats-thumb" />
	                  <h3>Group Name</h3>
	                  </a></li>
	                <li><a href="#"> <img src="${staticContentPrefix}/fpo/product-140x140.gif" class="cats-thumb" />
	                  <h3>Group Name Lorem Ipsum Dolor Sit Amet Consectitur</h3>
	                  </a></li>
	                <li><a href="#"> <img src="${staticContentPrefix}/fpo/product-140x140.gif" class="cats-thumb" />
	                  <h3>Group Name</h3>
	                  </a></li>
	                <li><a href="#"> <img src="${staticContentPrefix}/fpo/product-140x140.gif" class="cats-thumb" />
	                  <h3>Group Name</h3>
	                  </a></li>
	                <li><a href="#"> <img src="${staticContentPrefix}/fpo/product-140x140.gif" class="cats-thumb" />
	                  <h3>Group Name</h3>
	                  </a></li>
	                <li class="clear">&nbsp;</li>
	              </ul>
	            </div>
	            <div class="tab-content" id="fpo1">
	              <h2 class="print-only">FPO</h2>
	              <p>Tab Content</p>
	            </div>
	            <div class="tab-content" id="fpo2">
	              <h2 class="print-only">FPO</h2>
	              <p>Tab Content</p>
	            </div>
          </div>
       </div>
        <div class="clear">&nbsp;</div>
     </div>
    </div>



										  </dsp:oparam>
										</dsp:droplet>

</cp:pageContainer>
</dsp:page>

