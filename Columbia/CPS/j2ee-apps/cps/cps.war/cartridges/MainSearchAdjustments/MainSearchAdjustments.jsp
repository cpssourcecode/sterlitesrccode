<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="originalTerms" value="${content.originalTerms}"/>
	<dsp:param name="adjustedSearches" value="${content.adjustedSearches}"/>
	<dsp:param name="suggestedSearches" value="${content.suggestedSearches}"/>
	<%-- <dsp:param name="intepretedTerms" value="${content.intepretedTerms}"/> --%>
	
	<fmt:bundle basename="web.resources.WebAppResources">
		<c:forEach items="${content.originalTerms}" var="searchTerm">
			<dsp:getvalueof var="adjustedSearches" param="adjustedSearches.${searchTerm}"/>
			<c:set var="correctionString" value=""/>
			<c:forEach var="adjustedSearch" items="${adjustedSearches}" varStatus="status">
				<c:if test="${not status.first}">
					<c:set var="correctionString" value=",&nbsp;${correctionString}"/>
				</c:if>
				<c:set var="correctionString" value="${adjustedSearch.adjustedTerms}${correctionString}"/>
			</c:forEach>
			<c:set var="correctedTerm" value="${correctionString}"/>
		
			<c:if test="${not empty correctedTerm}">
				<h4><fmt:message key="listing.title.corrected">
					<fmt:param value="${searchTerm}"/>
					<fmt:param value="${correctedTerm}"/>
				</fmt:message></h4>
			</c:if>
		</c:forEach>
		
		<c:forEach items="${content.originalTerms}" var="searchTerm">
			<dsp:getvalueof var="suggestedSearches" param="suggestedSearches.${searchTerm}"/>
			<c:if test="${not empty suggestedSearches}">
				<fmt:message key="listing.title.didyoumean"/>
				<c:forEach var="suggestedSearch" items="${suggestedSearches}" varStatus="status" end="2">
					<c:set var="suggestedCount" value="${suggestedSearch.count}"/>
					<c:if test="${not status.first}">,&nbsp;</c:if>
					<a href="${vsg_utils:getUrlForAction(suggestedSearch)}">${suggestedSearch.label} (${suggestedCount})</a>
				</c:forEach>
			</c:if>
		</c:forEach>
	</fmt:bundle>
</dsp:page>