<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	
	<h4>My Account</h4>
	<ul class="list-unstyled">
		<c:choose>
			<c:when test="${not isTransient}">
				<li><a href="/account/account-landing.jsp">My Account Overview</a></li>
			</c:when>
			<c:otherwise>
				<li><a href="#" data-toggle="modal" data-target="#logIn">My Account Overview</a></li>
			</c:otherwise>
		</c:choose>
	</ul>

</dsp:page>