<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<h2>${content.name}</h2>
	<div class="row">
		<div class="col-sm-4">
			<a href="${content.pdfLink}" class="thumbnail" target="_blank">
				<cp:readerimg src="${content.previewImage}"> </cp:readerimg><span>${content.description}</span>
			</a>
		</div>
	</div>
	<hr>
</dsp:page>