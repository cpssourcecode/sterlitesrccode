<dsp:page>
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
  <c:if test="${not empty(contentItem)}">
  <ul>
    <c:choose>
      <c:when test="${contentItem.dimensionName eq 'product.category'}">
        <c:forEach var="refinement" items="${contentItem.refinements}">
          <c:set var="categoryId" value="${refinement.properties['category.repositoryId'] }"/>
            <preview:repositoryItem itemId="${categoryId}" itemType="category" repositoryName="ProductCatalog">
              <li>
               <dsp:a href="${contextPath}${refinement.contentPath}${refinement.navigationState}" title="${refinement.label}">
                 <c:out value="${refinement.label}"/> <span class="productCount">(<c:out value="${refinement.count}"/>)</span>
               </dsp:a>
             </li>
           </preview:repositoryItem>
         </c:forEach>
       </c:when>
       <c:otherwise>
         <c:forEach var="refinement" items="${contentItem.refinements}">
           <li>
             <dsp:a href="${contextPath}${refinement.contentPath}${refinement.navigationState}" title="${refinement.label}">
               <c:out value="${refinement.label}"/> <span class="productCount">(<c:out value="${refinement.count}"/>)</span>
             </dsp:a>
           </li>
         </c:forEach>
       </c:otherwise>
     </c:choose>
    <%-- Display the more link --%>
    <c:if test="${!empty(contentItem.moreLink)}">
      <fmt:message var="moreText" key="facet.panel.more" />
      <li>
        <dsp:include page="/global/renderNavLink.jsp">
          <dsp:param name="navAction" value="${contentItem.moreLink}"/>
          <dsp:param name="text" value="${moreText}"/>
          <dsp:param name="titleText" value="${moreText}"/>
        </dsp:include>
      </li>
    </c:if>
    <%-- Display the less link --%>
    <c:if test="${!empty(contentItem.lessLink)}">
      <fmt:message var="lessText" key="facet.panel.less" />
      <li>
        <dsp:include page="/global/renderNavLink.jsp">
          <dsp:param name="navAction" value="${contentItem.lessLink}"/>
          <dsp:param name="text" value="${lessText}"/>
          <dsp:param name="titleText" value="${lessText}"/>
        </dsp:include>
      </li>
    </c:if>
  </ul>
  </c:if>
</dsp:page>