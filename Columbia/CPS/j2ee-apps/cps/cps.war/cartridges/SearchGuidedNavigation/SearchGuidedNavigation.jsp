<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof param="dimIndex" var="dimIndex"/>
	<dsp:getvalueof var="collapsed" param="collapsed"/>
	<fmt:bundle basename="web.resources.WebAppResources">
	<c:forEach var="refinementMenu" items="${content.refinementMenus}">
<%-- 		<c:set var="refinements" value="${refinementMenu.multiSelect ?  --%>
<%-- 			vsg_utils:multiRefinements(breadcrumbs.refinementCrumbs,refinementMenu.refinements,refinementMenu.dimensionName) : refinementMenu.refinements}"/> --%>
		<c:set var="refinements" value="${refinementMenu.refinements}"/>
		<c:if test="${not empty refinements}">
			<!-- search guided -->
			<li class="expand-item facet-item">
				<span class="expand-label active">
					<c:choose>
						<c:when test="${not empty refinementMenu.displayName}">		
							${refinementMenu.displayName}
						</c:when>
						<c:otherwise>${refinementMenu.dimensionName}</c:otherwise>
					</c:choose>
					<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span>
				</span>
				<ul class="facet-refine expand-content active">
					<c:forEach items="${refinements}" var="refinement" varStatus="status" >
					 <c:set var='uri' value='${vsg_utils:escapeHtml4(refinement.navigationState)}'/>
					 <!-- value after ${uri} and before is ${refinement.navigationState} -->
					<li class="checkbox">
						<label>
							<input type="checkbox" data-event-change-id="cartridges1" data-event-change-param0="${uri}"  ${refinement.properties.selected ? 'checked':''}>
							${refinement.label}&nbsp;${refinement.properties['category.description']}
							<small>(${refinement.count})</small>
						</label>
					</li>
					</c:forEach>
				</ul>
			</li>
		</c:if>
	</c:forEach>
	</fmt:bundle>
</dsp:page>