<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	
	<h2>${content.title}</h2>
		
	<div class="media">
		<div class="media-left">
			<cp:readerimg iclass="media-object" style="width: 180px;"
				src="${content.imageUrl}" alt="..." />
			
		</div>
		<dsp:valueof value="${content.textContent}" valueishtml="true" />
	</div>
	
</dsp:page>