<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof var="currentSearchTerm" param="Ntt"/>	 
				  
				  <c:set var='currentSearchTerm1' value='${vsg_utils:escapeHtml4(currentSearchTerm)}'/> 
				 
	<c:set var="queryStringCount" value="${fn:split(currentSearchTerm, '|')}"></c:set>
	<c:set var="number" value="${fn:length(queryStringCount)}"/>	
	
	<li class="expand-item facet-search"><span class="expand-label">Filter Results By</span>
		<div class="form-group">
			<label>Search within these results</label>
			<div class="input-group input-group-search">
				<input id="secondarySearchQuestion" data-event-keypress-id="cartridges15" class="form-control" type="text" placeholder=""
							  maxlength="40">
				<input id="secondarySearchHidden" type="hidden" value="${currentSearchTerm1}"/>
				<c:set var="questionlast" value="${fn:replace(currentSearchTerm, '|', ',')}"/>
				<c:set var="questionLastValue" value="${fn:substring(questionlast,fn:length(questionlast)-1,fn:length(questionlast))}"></c:set>
				<c:choose>
					<c:when test="${questionLastValue eq  ',' }">
							<c:set var="question" value="${fn:substring(questionlast,0,fn:length(questionlast)-1)}"></c:set>
					</c:when>
					<c:otherwise>
							<c:set var="question" value="${questionlast}"></c:set>
					</c:otherwise>
				</c:choose>										
				<input id="question" type="hidden" value='${question}'/>
				<c:forEach var="refCrumb" items="${breadcrumbs.refinementCrumbs}">				
					<span id="refinementQuestion" style="display: none;" >${refCrumb.label}</span> 			 
				</c:forEach>
	            <c:set var="count" value="0" />
                <c:forEach var="searchCrumb" items="${breadcrumbs.searchCrumbs}">				
					<span class="searchQuestion" style="display: none;" >${searchCrumb.terms}</span> 	
                    <c:set var="count" value="${count + 1}" />		 
				</c:forEach>
                     <c:set var="searchPage" value="true"/>
                    <dsp:getvalueof var="endecaRequestURI" value="${requestScope['javax.servlet.forward.request_uri']}" />							
                    <c:if test="${fn:startsWith(endecaRequestURI, '/search') and count == 1}">
                           <c:set var="searchPage" value="false"/>
                    </c:if>
                				<input id="isFirstSearch" type="hidden" value="${searchPage}"/>
                    						
				<div class="input-group-btn">
					<button class="btn btn-primary secondarySearchSubmit" data-event-click-id="cartridges14">Go</button>
				</div>
			</div>
		</div>
	</li>
</dsp:page>