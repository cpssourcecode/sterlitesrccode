<dsp:page>
    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
    <li><a target="<dsp:valueof value="${content.target}"/>" href="<dsp:valueof value="${content.socialLink}"/>"> <i
            class="<dsp:valueof value="${content.socialStyle}"/>"></i></a></li>
</dsp:page>