<dsp:page>
	<dsp:importbean bean="/cps/droplet/CategoryPropertiesLookupDroplet" />
	<dsp:importbean bean="/atg/userprofiling/ProfileTools" />
	<dsp:getvalueof var="isTransient" bean="/atg/userprofiling/Profile.transient"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="allowNewUserSelfRegistration" bean="ProfileTools.allowNewUserSelfRegistration" />
	
	<div class="row row-flush">
		<div class="col-md-push-0 col-md-12 col-xs-8 col-xs-push-4 col-xxs-12 col-xxs-push-0">
			<section class="home-cats">
				<ul class="row cat-list">
					<c:forEach items="${content.boostRefinements}" var="item">
						<dsp:droplet name="CategoryPropertiesLookupDroplet">
							<dsp:param name="dimValId" value="${item.id}" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="categoryName" param="categoryName" />
								<dsp:getvalueof var="categoryImage" param="categoryImage" />
								<dsp:getvalueof var="categoryUrl" param="categoryUrl" />
							</dsp:oparam>
						</dsp:droplet>
						<li class="col-xs-2 col-xxs-4 text-center"><a href="${categoryUrl}">
								<span class="cat-thumb">
								<cp:readerimg src="${categoryImage}"/>
								</span>
								<h5 class="catalogTitleName">${categoryName}</h5>
						</a></li>
					</c:forEach>
				</ul>
			</section>
		</div>

		<div class="col-md-pull-0 col-xs-4 col-xs-pull-8 visible-xs visible-sm col-xxs-12 col-xxs-pull-0">
			<ul class="mobile">
				<li><a href="#mobile-products" data-toggle="collapse" aria-expanded="false" aria-controls="mobile-products"><i class="fa fa-cube"></i> View All Products</a>
					<ul id="mobile-products" class="mobile-subnav collapse">
						<dsp:droplet name="/cps/droplet/CategoryNavigationTreeDroplet">
							<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
							<dsp:param name="catCount" value="-1" />
							<dsp:param name="subCatCount" value="0" />
							<dsp:param name="sortProperties" value="displayName" />
							<dsp:oparam name="output">
								<dsp:droplet name="/atg/dynamo/droplet/ForEach">
									<dsp:param name="array" param="menuList" />
									<dsp:param name="elementName" value="menu"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="count" param="count"/>
										<dsp:getvalueof var="size" param="size"/>
										<c:if test="${count le 5}">
											<dsp:getvalueof var="menuName" param="menu.name"/>
											<dsp:getvalueof var="menuURL" param="menu.URL"/>
											<li><a href="${menuURL}">${vsg_utils:formatName(menuName)}</a></li>
										</c:if>
									</dsp:oparam>
								</dsp:droplet>
								<c:if test="${not empty size && size > 5}">
									<li><a href="/all-products">See All Products</a></li>
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
					</ul>
				</li>
				<li><a href="/all-brands"  aria-expanded="false" aria-controls="mobile-products"><i class="fa fa-tag"></i> View All Brands</a>
					<ul id="mobile-brands" class="mobile-subnav collapse">
						<dsp:droplet name="/cps/droplet/BrandsNavigationDroplet">
							<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="brandCount" value="0"/>
								<dsp:getvalueof var="brandSize" value="0"/>
								
								<dsp:droplet name="/atg/dynamo/droplet/ForEach">
									<dsp:param name="array" param="menuList" />
									<dsp:param name="elementName" value="menu"/>/
									<dsp:oparam name="output">
										<dsp:droplet name="/atg/dynamo/droplet/ForEach">
											<dsp:param name="array" param="menu" />
											<dsp:param name="elementName" value="navigationMenuItem" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="navigationMenuItem" param="navigationMenuItem" />
												
												<c:if test="${brandCount le 5}">
													<li>
														<a href="${navigationMenuItem.URL}">${navigationMenuItem.name}</a>
													</li>
												</c:if>
												<dsp:getvalueof var="brandCount" value="${brandCount + 1}"/>
												<dsp:getvalueof var="brandSize" value="${brandSize + 1}"/>
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
								<c:if test="${not empty brandSize && brandSize > 5}">
									<li><a href="/all-brands">See All Brands</a></li>
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
					</ul>
				</li>
				<c:if test="${!isTransient}">
					<li><a href="/account/account-landing.jsp"><i class="fa fa-user"></i> My Account</a></li>
				</c:if>
				<li><a href="/locations/locations.jsp"><i class="fa fa-map-marker"></i> Locations</a></li>
			</ul>
			<ul class="mobile mobile-secondary">
				<c:if test="${isTransient  && allowNewUserSelfRegistration}">
					<li><a href data-event-click-id="eventGoToRegistrationPage">Register</a></li>
				</c:if>
				<li><a href="/our-customers">Our Customers</a></li>
				<li><a href="/rolling-warehouse">Rolling Warehouse</a></li>
			</ul>
		</div>

	</div>
</dsp:page>
