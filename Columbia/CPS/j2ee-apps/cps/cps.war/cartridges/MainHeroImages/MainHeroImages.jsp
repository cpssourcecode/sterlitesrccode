<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />


	<div class="hidden-xs hidden-sm" id="masthead" style="background-image: url(${content.Image != null ? content.Image.url : ''});">
		<dsp:valueof param="contentItem.content" valueishtml="true"/>
		<dsp:renderContentItem contentItem="${content.Button}">
			<dsp:param name="defaultClass" value="btn btn-lg btn-primary"/>
		</dsp:renderContentItem>
	</div>

</dsp:page>