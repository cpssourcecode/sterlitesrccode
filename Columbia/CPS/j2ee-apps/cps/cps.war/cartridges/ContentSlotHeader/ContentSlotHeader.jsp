<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

	<c:choose>
		<c:when test="${endecaPreview}">
			<endeca:previewAnchor contentItem="${content}">
				<c:forEach var="element" items="${content.contents}">
					<dsp:renderContentItem contentItem="${element}" />
				</c:forEach>
			</endeca:previewAnchor>
		</c:when>
		<c:otherwise>
			<c:forEach var="element" items="${content.contents}">
				<dsp:renderContentItem contentItem="${element}" />
			</c:forEach>
		</c:otherwise>
	</c:choose>
</dsp:page>

