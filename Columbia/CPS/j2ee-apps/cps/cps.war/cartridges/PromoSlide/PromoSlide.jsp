<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="title" value="${content.title}" />
	<dsp:getvalueof var="imageUrl" value="${content.imageURL}" />
	<dsp:getvalueof var="description" value="${content.description}" />
	<dsp:getvalueof var="learnMoreLink" value="${content.learnMoreURL}" />
	<dsp:getvalueof var="status" param="status" />
	
	<dsp:getvalueof var="itemClass" value="item"/>
	<c:if test="${status == '0'}">
		<dsp:getvalueof var="itemClass" value="item active"/>
	</c:if>
	
	<div class="${itemClass}">
		<div class="carousel-caption">
			<a href="#">
				<h3>
					<span>${title}</span>
				</h3>
				<div class="image">
					<cp:readerimg src="${imageUrl}" alt="" />
				</div>
			</a>
			<p>${description}</p>
			<a href="${learnMoreLink}" class="read-more"><span>Learn More</span><i class="fa fa-angle-right"></i></a>
		</div>
	</div>
</dsp:page>