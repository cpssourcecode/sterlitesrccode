<dsp:page>

 <%--
 <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>

  <div id="atg_store_facets" class="atg_store_facetsGroup">
    <c:if test="${not empty contentItem.navigation}">
      <c:forEach var="element" items="${contentItem.navigation}">
        <dsp:renderContentItem contentItem="${element}"/>
      </c:forEach>
    </c:if>
  </div>--%>



  <%-- <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:droplet name="/cps/droplet/ReadOriginatingRequest">
		<dsp:oparam name="output">
			<dsp:getvalueof var="contentItem"
				vartype="com.endeca.infront.assembler.ContentItem"
				param="contentItem" />
		</dsp:oparam>
	</dsp:droplet> --%>

<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

	<!-- FACETS TEST: ${contentItem.navigation} -->
  <div id="sidebar">
  	<h3>Refine Results</h3>
    <c:if test="${not empty contentItem.navigation}">
    <!-- Content Item Name: ${contentItem.name} -->
    <ul class="subnav expand">
      <c:forEach var="nav" items="${contentItem.navigation}">
     	 <li>
      		<span class="subnav-header">

	      		<dsp:valueof value="${nav.displayName}" valueishtml="true"/>
	      		<span class="ui-icon">&nbsp;</span>
      		</span>
      		<!-- Output: ${element.name} -->



        		<dsp:renderContentItem contentItem="${nav}"/>



	   </li>
      </c:forEach>
    </ul>
    </c:if>
  </div>

</dsp:page>
