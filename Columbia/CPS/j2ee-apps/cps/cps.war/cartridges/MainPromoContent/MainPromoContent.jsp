<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:include src="/content/promo.jsp">
		<dsp:param name="targeterComponent" value="${content.targeter_full_path}" />
		<dsp:param name="showRandom" value="${content.showRandom}" />
		<dsp:param name="maxNumber" value="${content.maxNumber}" />
		<dsp:param name="tag" value="${navRefinement.label}" />
	</dsp:include>
</dsp:page>