<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	
	<div class="row">
		<div class="col-sm-4 col-xs-6">
			<h3>${content.tableOfContentTitle}</h3>
			<ul class="nav nav-pills nav-stacked">
				<c:forEach var="links" items="${contentItem.tableOfContentLinks}">
				<dsp:renderContentItem contentItem="${links}"/>
			</c:forEach>
			</ul>
		</div>
	</div>
	
	<hr>
	
</dsp:page>