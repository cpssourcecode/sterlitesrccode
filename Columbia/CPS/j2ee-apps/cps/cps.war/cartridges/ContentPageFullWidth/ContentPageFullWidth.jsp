<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<cp:staticPageContainer page="contentPage" title="${contentItem.title}" metaKeywords="${contentItem.metaKeywords}" metaDescription="${contentItem.metaDescription}" >
		
		<c:forEach var="headerContent" items="${contentItem.headerContent}">
			<dsp:renderContentItem contentItem="${headerContent}"/>
		</c:forEach>
		
		<div class="container main-content">
			<div class="row">
				<c:if test="${not empty contentItem.breadcrumbs['@type']}">
					<div class="col-sm-8">

						<ol class="breadcrumb">
							<dsp:renderContentItem contentItem="${contentItem.breadcrumbs}">
								<dsp:param name="contentTitle" value="${contentItem.title}" />
							</dsp:renderContentItem>
						</ol>
					</div>
				</c:if>

				<c:if test="${(contentItem.showSharePageButton == true) || (contentItem.showPrintPageButton == true)}">
					<dsp:include page="${originatingRequest.contextPath}/global/gadgets/page-actions.jsp">
						<dsp:param name="isShowEmail" value="${contentItem.showSharePageButton}" />
						<dsp:param name="isShowPrint" value="${contentItem.showPrintPageButton}" />
					</dsp:include>
				</c:if>

				<c:if test="${not empty contentItem.title}">
					<div class="col-xs-12">
						<h1>${contentItem.title}</h1>
					</div>
				</c:if>

				<dsp:include page="${originatingRequest.contextPath}/global/gadgets/error-success-msg-divs.jsp"/>

				<%--<div class="col-xs-12">--%>
					<c:forEach var="mainContent" items="${contentItem.mainContent}">
						<dsp:renderContentItem contentItem="${mainContent}">
							<dsp:param name="contentTitle" value="${contentItem.title}" />
						</dsp:renderContentItem>
					</c:forEach>
				<%--</div>--%>
			</div>
		</div>
		
		<dsp:importbean bean="/atg/userprofiling/Profile"/>
		<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
			<dsp:param name="userId" bean="Profile.id"/>
			<dsp:param name="role" value="superAdmin,regularAdmin"/>
			<dsp:oparam name="false">
				<footer id="footer">
					<c:forEach var="footerContent" items="${contentItem.footerContent}">
						<dsp:renderContentItem contentItem="${footerContent}"/>
					</c:forEach>
				</footer>
			</dsp:oparam>
		</dsp:droplet>
	</cp:staticPageContainer>
</dsp:page>