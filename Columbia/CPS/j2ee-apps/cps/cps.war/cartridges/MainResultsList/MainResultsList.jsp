<dsp:page>
	<dsp:getvalueof var="showCS" param="showCS"/>
	<dsp:getvalueof var="onHold" param="onHold"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	
	<dsp:include src="/catalog/gadgets/listing-results-list-full.jsp">
		<dsp:param name="content" param="contentItem"/>
	</dsp:include>

	<div id="modal-on-hold-show-div"></div>

	<div id="modal-cs-show-div"></div>

	<div id="modal-check-availability-div"></div>

	<div id="modal-add-to-material-list-div"></div>
</dsp:page>
