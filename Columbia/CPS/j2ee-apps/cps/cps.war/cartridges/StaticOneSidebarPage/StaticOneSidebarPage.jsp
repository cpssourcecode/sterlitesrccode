<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<cp:staticPageContainer page="staticPage" title="${contentItem.title}" metaKeywords="${contentItem.metaKeywords}" metaDescription="${contentItem.metaDescription}" >
		
		<c:forEach var="headerContent" items="${contentItem.headerContent}">
			<dsp:renderContentItem contentItem="${headerContent}"/>
		</c:forEach>
		
		<main id="body">
			<div class="container">
				<c:if test="${not empty contentItem.breadcrumbs['@type']}">
					<ol class="breadcrumb">
						<dsp:renderContentItem contentItem="${contentItem.breadcrumbs}">
							<dsp:param name="contentTitle" value="${contentItem.title}" />
						</dsp:renderContentItem>
					</ol>
				</c:if>
				
				
				<%-- <dsp:include page="${originatingRequest.contextPath}/global/gadgets/page-actions.jsp">
					<dsp:param name="isShowEmail" value="true"/>
				</dsp:include> --%>

				<h1 class="mb20">
					${contentItem.title}
				</h1>
				
				<div class="row">
					<div id="sidebar" class="col-sm-3">
						<c:forEach var="secondaryContent" items="${contentItem.secondaryContent}">
							<dsp:renderContentItem contentItem="${secondaryContent}"/>
						</c:forEach>
					</div>
					<div id="content" class="col-sm-9">
						<c:forEach var="mainContent" items="${contentItem.mainContent}">
							<dsp:renderContentItem contentItem="${mainContent}"/>
						</c:forEach>
					</div>
				</div>

			</div>
		</main>
		
		<dsp:importbean bean="/atg/userprofiling/Profile"/>
		<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
			<dsp:param name="userId" bean="Profile.id"/>
			<dsp:param name="role" value="superAdmin,regularAdmin"/>
			<dsp:oparam name="false">
				<footer id="footer">
					<c:forEach var="footerContent" items="${contentItem.footerContent}">
						<dsp:renderContentItem contentItem="${footerContent}"/>
					</c:forEach>
				</footer>
			</dsp:oparam>
		</dsp:droplet>
	</cp:staticPageContainer>
</dsp:page>