<%--
  SearchBox

  Renders a search box which allows the user to query for search results.
--%>
<dsp:page>
  <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
  <dsp:importbean bean="/atg/dynamo/droplet/multisite/CartSharingSitesDroplet" />
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
  <dsp:importbean bean="/atg/multisite/Site" var="currentSite"/>
  <dsp:importbean bean="/atg/search/routing/command/search/DynamicTargetSpecifier"/>
  <dsp:importbean bean="/atg/endeca/assembler/SearchFormHandler"/>
  <dsp:importbean bean="/atg/endeca/assembler/cartridge/manager/DefaultActionPathProvider"/>
<%--    <dsp:importbean bean="/cps/droplet/ReadOriginatingRequest"/>
        <dsp:droplet name="ReadOriginatingRequest">
                 <dsp:oparam name="output">
                <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

                </dsp:oparam>
                </dsp:droplet>

                <c:if test="${empty contentItem}" >
		       <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" value="${originatingRequest.contentItem}"/>
		 </c:if> --%>

	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

  <dsp:getvalueof var="contextPath" vartype="java.lang.String" value="${originatingRequest.contextPath}"/>
  <dsp:getvalueof var="actionPath" bean="DefaultActionPathProvider.defaultExperienceManagerNavigationActionPath"/>

  <fmt:message var="hintText" key="common.search.input"/>
  <fmt:message var="submitText" key="search_simpleSearch.submit"/>

  <div id="atg_store_search">

    <%-- The search form --%>
    <dsp:form action="${contextPath}${actionPath}" id="searchForm" >
      <input type="hidden" name="Dy" value="1"/>
      <input type="hidden" name="Nty" value="1"/>
      <dsp:input bean="SearchFormHandler.siteScope" type="hidden" value="ok" name="siteScope"/>

       <label for="global-search">Search by Keyword or item #</label>
        <input type="text" name="global-search" id="global-search" />
        <input type="submit" value="Search" />



    </dsp:form>
  </div>


</dsp:page>