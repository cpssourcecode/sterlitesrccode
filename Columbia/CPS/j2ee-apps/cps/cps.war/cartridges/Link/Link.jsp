<dsp:page>
    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

    <dsp:getvalueof var="defaultClass" param="defaultClass"/>

    <c:set var="class" value="${empty content.class ? defaultClass : content.class}"/>

    <a href="${content.url}" target="${content.target}" class="${class}">${content.text}</a>

</dsp:page>