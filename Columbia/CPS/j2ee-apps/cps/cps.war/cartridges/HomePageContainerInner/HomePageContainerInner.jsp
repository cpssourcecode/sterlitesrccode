<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
		<c:forEach var="element" items="${content.mainContent}">
			<dsp:renderContentItem contentItem="${element}"/>
		</c:forEach>
</dsp:page>
