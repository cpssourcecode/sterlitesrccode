<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="noImgUrl" param="contentItem.noImgUrl"/>
	<dsp:valueof value="${content.content}" valueishtml="true" />
	
	
	<dsp:droplet name="/cps/droplet/CategoryNavigationTreeDroplet">
			<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
			<dsp:param name="zeroResults" value="true" />
			<dsp:param name="catCount" value="-1" />
			<dsp:param name="sortProperties" value="displayName" />
			<dsp:oparam name="output">

				<section class="home-cats">
					<ul class="row cat-list">
						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" param="menuList" />
							<dsp:param name="elementName" value="catMenu"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="catMenu" param="catMenu"/>
								<dsp:getvalueof var="imageURL" value="${catMenu.img}"/>
								<c:if test="${empty imageURL}">
									<dsp:getvalueof var="imageURL" value="${noImgUrl}"/>
								</c:if>
								<li class="col-sm-20 col-xs-3 col-xxs-4 text-center">
									<a href="${catMenu.URL}">
										<span class="cat-thumb"><cp:readerimg src="${imageURL}" /></span>
										<h5 class="catalogTitleName">${vsg_utils:formatName(catMenu.name)}</h5>
									</a>
								</li>
							</dsp:oparam>
						</dsp:droplet>
					</ul>
				</section>
				
			</dsp:oparam>
		</dsp:droplet>
	
</dsp:page>