<dsp:page>
    
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/droplet/GetPriceFromMapDroplet"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/cps/userprofiling/service/ProfileRecentlyViewedService"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>

    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
    <dsp:getvalueof var="transient" bean="Profile.transient"/>

    <c:set var="useFeatured" value="${false}"/>
    <dsp:droplet name="/cps/droplet/CPSRecentlyViewedDroplet">
    	<dsp:param name="profileRecentlyViewedService" bean="ProfileRecentlyViewedService" />
        <dsp:param name="shoppingCart" bean="ShoppingCart" />
        <dsp:param name="prices" value="${false}" />
        <dsp:oparam name="output">
            <dsp:getvalueof var="recentProducts" param="elements"/>
            <dsp:getvalueof var="len" param="length"/>
            <c:if test="${len < 5}">
                <c:set var="useFeatured" value="${true}"/>
            </c:if>
        </dsp:oparam>
        <dsp:oparam name="empty">
            <c:set var="useFeatured" value="${true}"/>
        </dsp:oparam>
    </dsp:droplet>
	
	<dsp:getvalueof var="pageClass" value="home-recent hidden-xs hidden-sm"/>
	<dsp:getvalueof var="ulClass" value="item item-ymal row row-5 row-eq-height row-narrow"/>
	<dsp:getvalueof var="liClass" value="item-item col-sm-4 col-xs-12"/>
	<c:choose>
		<c:when test="${page eq 'pdp'}">
			<dsp:getvalueof var="pageClass" value="pdp-recent"/>
			<dsp:getvalueof var="ulClass" value="item item-ymal row row-eq-height row-narrow"/>
			<dsp:getvalueof var="liClass" value="item-item col-md-20 col-sm-4 col-xs-12"/>
		</c:when>
        <c:when test="${page eq 'checkout'}">
            <dsp:getvalueof var="pageClass" value="cart-recent"/>
            <dsp:getvalueof var="ulClass" value="item item-ymal row row-eq-height row-narrow"/>
            <dsp:getvalueof var="liClass" value="item-item col-md-20 col-sm-4 col-xs-12"/>
        </c:when>
	</c:choose>

    <c:choose>

        <c:when test="${useFeatured}">
            <section class="${pageClass}">
                <h4 class="bar mb20">${content.title}</h4>

                <ul class="${ulClass}">
                    <c:forEach var="record" items="${content.records}">

                        <dsp:getvalueof var="prodId" value="${record.attributes['product.repositoryId']}"/>
                        <dsp:getvalueof var="skuId" value="${record.attributes['sku.repositoryId']}"/>
                        <dsp:getvalueof var="displayName" value="${record.attributes['product.displayName']}"/>
                        <dsp:getvalueof var="description" value="${record.attributes['product.description']}"/>
                        <dsp:getvalueof var="web_url" value="${record.attributes['product.web_url']}"/>
                        <dsp:getvalueof var="productUrl" value="${fn:toLowerCase(record.attributes['product.seoUrl'])}"/>
                        <c:set var="web_url" value="${empty web_url ? '/assets/images/plp-placeholder.png' : web_url}"/>
						<dsp:getvalueof var="altText" value=""/>
						<c:if test="${record.attributes['product.altText'] != null}">
							<dsp:getvalueof var="altText" value="${fn:toLowerCase(record.attributes['product.altText'])}"/>
						</c:if>

                        <li class="${liClass}">
                            <span class="item-frame">
                                <div class="row row-flush">
                                    <div class="col-sm-12 col-sm-push-0 col-xs-4 col-xs-push-8 col-eq-height"><a
                                            href="${productUrl}" class="item-thumb"><cp:readerimg alt="${vsg_utils:escapeHtml(altText)}"
                                            src="${fn:escapeXml(web_url)}" /></a></div>
                                    <div class="col-sm-12 col-sm-pull-0 col-xs-8 col-xs-pull-4">

                                       <div style="display: none">
                                            <dsp:form id="addToCart${prodId}" formid="addToCart${prodId}" action="" method="post">
                                                <dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10"/>
                                                <dsp:input bean="CartModifierFormHandler.items[0].quantity" type="hidden" value="1"/>
                                                <dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden" value="${prodId}"/>
                                                <dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" value="${skuId}"/>
                                                <dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
                                            </dsp:form>
                                       </div>

                                       <input id="quantity${prodId}" type="hidden" value="1">

                                         <dsp:droplet name="/cps/droplet/AccessRightDroplet">
                                             <dsp:param name="profile" bean="Profile"/>
                                             <dsp:param name="accessRightKey" value="plp-pdp-cart"/>
                                             <dsp:oparam name="false">
                                                 <dsp:getvalueof var="permissionDenied" value="true"/>
                                             </dsp:oparam>
                                         </dsp:droplet>

                                        <span class="item-meta">
                                            <span class="item-title title-eq-height"><a href="${productUrl}">${description}</a></span>
                                            <span class="item-sku">${displayName}</span>
                                            <%--<span class="item-sku">${skuId}</span>--%>
                                            <%--<span class="item-price">Your Price: <dsp:valueof value="${priceValue}" converter="currencyConversion"/></span>--%>
                                        </span>
                                        <span class="item-order"><a
                                                href="${productUrl}"
                                                <%--data-event-click-id="cartridges10" data-event-click-param0="${prodId}" --%>
                                                class="btn btn-default btn-block"><fmt:message key="listing.product.tile.learnMore"/></a>
                                        </span>
                                    </div>
                                </div>
                            </span>
                        </li>

                    </c:forEach>
                </ul>
            </section>
        </c:when>
        <c:otherwise>
        	<dsp:droplet name="/cps/droplet/CPSRecentlyViewedDroplet">
                <dsp:param name="profileRecentlyViewedService" bean="ProfileRecentlyViewedService" />
                <dsp:param name="shoppingCart" bean="ShoppingCart" />
		    	<dsp:param name="prices" value="${true}" />
		        <dsp:oparam name="output">
		        	<dsp:getvalueof var="itemPrices" vartype="Map" param="itemPrices"/>
		            <dsp:getvalueof var="recentProducts" param="elements"/>
		        </dsp:oparam>
		    </dsp:droplet>
		    
            <section class="${pageClass}">
                <h4 class="bar mb20">RECENTLY VIEWED PRODUCTS</h4>

                <ul class="${ulClass}">
                    <c:forEach var="product" items="${recentProducts}">
						<dsp:getvalueof var="productAltText" value="${product.altText}"/>
                        <dsp:getvalueof var="prodId" value="${product.repositoryId}"/>
                        <dsp:getvalueof var="displayName" value="${product.displayName}"/>
                        <dsp:getvalueof var="web_url" value="${product.web_url}"/>
                        <dsp:getvalueof var="description" value="${product.description}"/>
                        <dsp:getvalueof var="skuId" value="${product.childSKUs[0].repositoryId}"/>
                        <dsp:getvalueof var="minimum_order_qty" value="${product.minimum_order_qty}"/>
                        <dsp:getvalueof var="productPurchasable" value="${product.productPurchasable}"/>
                       	<dsp:getvalueof var="stockingType" value="${product.stockingType}"/>
                        <dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType)}"/>
                        
                        <c:set var="web_url" value="${empty web_url ? '/assets/images/plp-placeholder.png' : web_url}"/>

						<dsp:droplet name="GetPriceFromMapDroplet">
                            <dsp:param name="productId" value="${prodId}"/>
                            <dsp:param name="itemPrices" value="${itemPrices}"/>
                            <dsp:oparam name="outputPrice">
                                <dsp:getvalueof var="priceValue" param="itemPrice"/>
                            </dsp:oparam>
                        </dsp:droplet>
                        
                        <div style="display: none">
                            <dsp:form id="addToCart${prodId}" formid="addToCart${prodId}" action="" method="post">
                                <dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10"/>
                                <dsp:input bean="CartModifierFormHandler.items[0].quantity" type="hidden" value="${minimum_order_qty}"/>
                                <dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden" value="${prodId}"/>
                                <dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" value="${skuId}"/>
                                <dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
                            </dsp:form>
                        </div>
                        
						<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
							<dsp:param name="prodId" value="${prodId}"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
							</dsp:oparam>
						</dsp:droplet>
						
                        <dsp:droplet name="/cps/droplet/AccessRightDroplet">
                            <dsp:param name="profile" bean="Profile"/>
                            <dsp:param name="accessRightKey" value="plp-pdp-cart"/>
                            <dsp:oparam name="false">
                                <dsp:getvalueof var="permissionDenied" value="true"/>
                            </dsp:oparam>
                        </dsp:droplet>

                        <li class="${liClass}">
                            <span class="item-frame">
                                <div class="row row-flush">
                                    <div class="col-sm-12 col-sm-push-0 col-xs-4 col-xs-push-8 col-eq-height"><a
                                            href="${productUrl}" class="item-thumb"><cp:readerimg alt="${vsg_utils:escapeHtml(productAltText)}"
                                            src="${fn:escapeXml(web_url)}" /></a></div>
                                    <div class="col-sm-12 col-sm-pull-0 col-xs-8 col-xs-pull-4">
                                        <span class="item-meta">
                                            <span class="item-title title-eq-height"><a
                                                    href="${productUrl}">${description}</a></span>
                                            <span class="item-sku">${displayName}</span>
                                            <span class="item-price">Your Price: <dsp:valueof value="${priceValue}" converter="currencyConversion"/></span>
                                        </span>
                                        <div id="minQtyError${prodId}" class="red" style="display:none;"></div>
                                        <c:choose>
		                                    <c:when test="${productPurchasable && !isProductPurchasableStockingType}">
		                                         <c:choose>
		                                             <c:when test="${transient}">
		                                                 <dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
															<dsp:oparam name="true">
		                                                         <span class="item-order"><a
		                                                                 href="#"
		                                                                 data-event-click-id="cartridges12" data-event-click-param0="${prodId}"
		                                                                 class="btn btn-default btn-block addToCartButton"><fmt:message key="listing.product.tile.addToCart"/></a>
		                                                         </span>
		                                                    </dsp:oparam>
		                                                     <dsp:oparam name="false">
		                                                          <span class="item-order"><a
		                                                                  href="#"
		                                                                  data-toggle="modal"
		                                                                  data-target="#logIn"
		                                                                  data-productId="${prodId}"
		                                                                  data-skuId="${skuId}"
		                                                                  class="btn btn-default btn-block addToCartButton"><fmt:message key="listing.product.tile.addToCart"/></a>
		                                                                </span>
		                                                     </dsp:oparam>
		                                                 </dsp:droplet>
		                                             </c:when>
		                                             <c:when test="${permissionDenied}">
		                                                   <span class="item-order"><a
		                                                           href="#"
		                                                           data-toggle="modal"
		                                                           data-target="#permissionDenied"
		                                                           class="btn btn-default btn-block addToCartButton"><fmt:message key="listing.product.tile.addToCart"/></a>
		                                                    </span>
		                                             </c:when>
		                                             <c:otherwise>
		                                                    <span class="item-order"><button
		                                                            href="#"
		                                                            data-event-click-id="cartridges12" data-event-click-param0="${prodId}" 
		                                                            class="btn btn-default btn-block addToCartButton"><fmt:message key="listing.product.tile.addToCart"/></button>
		                                                    </span>
		                                             </c:otherwise>
		                                         </c:choose>
	                                        </c:when>
	                                        <c:otherwise>
												<fmt:message key="listing.product.notPurchasableOnline"/>
	                                        </c:otherwise>
                                       </c:choose>
                                    </div>
                                </div>
                            </span>
                        </li>

                    </c:forEach>
                </ul>
            </section>
        </c:otherwise>

    </c:choose>
    <dsp:importbean bean="/cps/util/CPSSessionBean" />
    <dsp:getvalueof var="isAvailablePricesWebService" bean="CPSSessionBean.availablePricesWebService" />
    <script type="text/javascript" nonce="${requestScope.nonce}">
        $(document).ready(function () {
        	priceAvailableMsg('${isAvailablePricesWebService}');
        });
    </script>

</dsp:page>


