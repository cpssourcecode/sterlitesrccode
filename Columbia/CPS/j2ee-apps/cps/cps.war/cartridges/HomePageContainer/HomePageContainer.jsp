<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	
	<dsp:getvalueof var="selectedOrg" bean="/atg/userprofiling/Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<main id="body">
		<div class="container ${csrClass}">
			<dsp:renderContentItem contentItem="${content.mainContent}"/>
		</div>
	</main>
    <script type="application/javascript" nonce="${requestScope.nonce}">
        $(document).ready(function () {
            $(".carousel").not('.slick-initialized').slick({
                infinite: true,
                dots: false,
                arrows: true,
                slidesToShow: 5,
                slidesToScroll: 1,
                prevArrow: '<button type="button" class="btn slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
                nextArrow: '<button type="button" class="btn slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
                responsive: [
                    {
                        breakpoint: 990,
                        settings: {
                            slidesToShow: 4,
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 2,
                        }
                    }
                ]
            });
        });
    </script>
</dsp:page>
