<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="type" value="${content.navLinkType}" />
	<dsp:getvalueof var="navLinkText" value="${content.navLinkText}" />
	<dsp:getvalueof var="navLinkURL" value="${content.navLinkURL}" />
	<li><a href="${navLinkURL}">${navLinkText}</a></li>
</dsp:page>