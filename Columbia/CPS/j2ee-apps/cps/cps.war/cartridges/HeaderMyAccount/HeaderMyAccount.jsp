<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<div class="dropdown account">
		<button class="btn btn-default btn-lg btn-services" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			My Account
			<span class="caret"></span>
		</button>
		<ul class="dropdown-menu dropdown-services pull-right" role="menu" aria-labelledby="dLabel">
			<li><a href="/account/account-landing.jsp"><strong>My Account Overview</strong> <span class="glyphicon glyphicon-chevron-right text-warning"></span></a></li>
			<div class="row dropdown-grid dropdown-account">
				<div class="col-sm-6 col-xs-12">
					<ul>
						<dsp:include page="/includes/gadgets/my-account-links-col1.jsp" />
					</ul>
				</div>
				<div class="col-sm-6 col-xs-12">
					<ul>
						<dsp:include page="/includes/gadgets/my-account-links-col2.jsp" />
					</ul>
				</div>
			</div>
		</ul>
	</div>
</dsp:page>