<dsp:page>
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/endeca/cache/DimensionValueCacheDroplet"/>
	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof var="showCS" param="showCS"/>
	<dsp:getvalueof var="onHold" param="onHold"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	
	<c:choose>
		<c:when test="${contentItem.isHomePage}">
			<dsp:getvalueof var="page" value="home"/>
		</c:when>
	</c:choose>
	<cp:pageContainer page="${page}" metaKeywords="${contentItem.metaKeywords}" metaDescription="${contentItem.metaDescription}">

		<c:forEach var="mainItem" items="${contentItem.mainContent}">
			<dsp:renderContentItem contentItem="${mainItem}"/>
		</c:forEach>

		<%--removed section below per CLMB0207-149. Looks like recent design doesn't support mobile promos carousel--%>
		<%--<c:if test="${not empty contentItem.homepageMobilePromo}">--%>
			<%--<div class="col-sm-9 col-sm-offset-3">--%>
				<%--<div class="row">--%>
					<%--<div class="col-xs-12">--%>
						<%--<div class="thumbnail thumbnail-testimonial visible-xs">--%>
							<%--<div id="carousel-example-generic2" class="carousel slide testimonial" data-ride="carousel">--%>
								<%--<!-- Indicators -->--%>
								<%--<ol class="carousel-indicators">--%>
									<%--<dsp:getvalueof var="slideClass" value="active"/>--%>
									<%--<c:forEach var="slide" items="${contentItem.homepageMobilePromo}" varStatus="s">--%>
										<%--<li data-target="#carousel-example-generic2" data-slide-to="${s.index}" class="${slideClass}"></li>--%>
										<%--<dsp:getvalueof var="slideClass" value=""/>--%>
									<%--</c:forEach>--%>
								<%--</ol>--%>
								<%--<!-- Wrapper for slides -->--%>
								<%--<div class="carousel-inner" role="listbox">--%>
									<%--<c:forEach var="slide" items="${contentItem.homepageMobilePromo}" varStatus="s">--%>
										<%--<dsp:renderContentItem contentItem="${slide}">--%>
											<%--<dsp:param name="status" value="${s.index}" />--%>
										<%--</dsp:renderContentItem>--%>
									<%--</c:forEach>--%>
								<%--</div>--%>
							<%--</div>--%>
						<%--</div>--%>
					<%--</div>--%>
				<%--</div>--%>
			<%--</div>--%>
		<%--</c:if>--%>

		<div id="modal-on-hold-show-div"></div>
		<div id="modal-cs-show-div"></div>

	</cp:pageContainer>

	<c:if test="${not isTransient}">
		<script type="text/javascript" nonce="${requestScope.nonce}">

			$(document).ready(function () {
				checkShowOnHold();
			});

			function checkShowCS() {
				if ('${vsg_utils:escapeJS(showCS)}' == 'true') {
					showSelectShippingAddress()
				}
			}

			function checkShowOnHold() {
				if ('${vsg_utils:escapeJS(onHold)}' == 'true') {
					showOnHold('${vsg_utils:escapeJS(showCS)}');
				} else {
					checkShowCS();
				}
			}

		</script>
	</c:if>
</dsp:page>