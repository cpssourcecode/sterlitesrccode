<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	
	<div class="col-md-12 col-xs-6 col-xxs-12">
		<c:forEach var="secondaryContent" items="${content.secondaryContent}">
			<dsp:renderContentItem contentItem="${secondaryContent}"/>
		</c:forEach>
	</div>
</dsp:page>