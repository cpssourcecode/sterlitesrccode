<dsp:page>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
    <cp:pageContainer page="contentPage" title="CPS Sitemap" metaKeywords="${contentItem.metaKeywords}" metaDescription="${contentItem.metaDescription}">
        <main id="body">
            <div class="container">
                <ol class="breadcrumb">
                    <li class="active">Site Map</li>
                </ol>
                <h1 class="mb20">Site Map</h1>
                <section class="sitemap-list">
                    <div class="row">
                        <div class="col-sm-4">
                            <dsp:renderContentItem contentItem="${contentItem.productNavigation}"/>
                        </div>
                        <div class="col-sm-4">
                            <dsp:renderContentItem contentItem="${contentItem.myAccount}"/>
                            <dsp:renderContentItem contentItem="${contentItem.services}"/>
                        </div>
                        <div class="col-sm-4">
                            <dsp:renderContentItem contentItem="${contentItem.ourBusiness}"/>
                            <dsp:renderContentItem contentItem="${contentItem.support}"/>
                        </div>
                    </div>
                </section>
            </div>
        </main>
    </cp:pageContainer>
</dsp:page>
