<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof param="loadSection" var="loadSection"/>
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<c:choose>
		<c:when test="${content.isSearchPage}">
			<dsp:getvalueof var="searchpage" value="search" scope="request"/>
		</c:when>
	</c:choose>

	<c:choose>
		<c:when test="${loadSection eq 'inner'}">
		
			<dsp:getvalueof var="question" param="Ntt"/>
				
			<c:set var="splittedNtt" value="${fn:split(question, '|')}" />
			<c:if test="${fn:length(splittedNtt) > 1}">
				<dsp:getvalueof var="question" value="${splittedNtt[fn:length(splittedNtt) - 1]}"/>
			</c:if>
			
			<dsp:getvalueof var="question" value="${question}" scope="request"/>
			
			<c:forEach var="headerContent" items="${content.headerContent}">
				<dsp:renderContentItem contentItem="${headerContent}"/>
			</c:forEach>

			<dsp:getvalueof var="mainResultsList" value="${vsg_utils:getMainContentSubItem(content, 'MainResultsList')}"/>

			<dsp:getvalueof var="totalNumRecs" value="${mainResultsList.totalNumRecs}" scope="request"/>
			
			<dsp:include page="pageHeader.jsp">
				<dsp:param name="question" value="${question}"/>
			</dsp:include>
			
			<div class="row">
				<div class="container preload-cover" id="spinner-on-start">
<!-- 					<div class="loading"> -->
<!-- 						<i class="fa fa-spinner fa-spin"></i> -->
<!-- 					</div> -->
				</div>
				<div id="sidebar" class="col-sm-3">
					<ul class="list-group facet expand">
						<c:forEach var="secondaryContent" items="${content.secondaryContent}">
							<dsp:renderContentItem contentItem="${secondaryContent}"/>
						</c:forEach>
					</ul>
				</div>
				
				<div id="mainContent" class="col-sm-9">
					
					<div id="error" class="col-md-9 alert alert-red message message-red hidden"></div>
					<div id="success" class="col-md-9 alert alert-green message message-green hidden"></div>
					
					<dsp:include src="/content/mainContent.jsp">
						<dsp:param name="contentItem" param="contentItem"/>
						<dsp:param name="totalNumRecs" value="${totalNumRecs}"/>
					</dsp:include>
				</div>
			</div>
		</c:when>
		
		<c:otherwise>
			<c:set var="canonicalLink" value=""/>
			<c:forEach var="hcnt" items="${content.headerContent}">
				<c:if test="${'Breadcrumbs'==hcnt.name}">
					<c:if test="${not fn:startsWith(hcnt['endeca:auditInfo']['ecr:resourcePath'],'content/Web/Search')}">
						<c:forEach var="refc" items="${hcnt.refinementCrumbs}">
							<c:if test="${refc.dimensionName eq 'product.category'}">
								<c:set var="canonicalLink" value=""/>
							</c:if>
						</c:forEach>
					</c:if>
				</c:if>
			</c:forEach>
			<dsp:getvalueof var="mainResultsList" value="${vsg_utils:getMainContentSubItem(content, 'MainResultsList')}"/>
			<cp:endecaPageContainer title="${content.headerContent[0].refinementCrumbs[0].label}" metaKeywords="${content.metaKeywords}" 
					metaDescription="${content.headerContent[0].refinementCrumbs[0].properties['category.description']}"
					page="twoColumn" type="endecaCartridge" endecaDrivenPage="true" canonicalLink="${canonicalLink}">

				<dsp:renderContentItem contentItem="${content.siteHeader}"/>
				
				<main id="body">
					
					<div class="container" id="pageContent" ${csrClass}>
						<dsp:getvalueof var="question" value="${param.Ntt}" />
							
						<c:set var="splittedNtt" value="${fn:split(question, '|')}" />
						<c:if test="${fn:length(splittedNtt) > 1}">
							<dsp:getvalueof var="question" value="${splittedNtt[fn:length(splittedNtt) - 1]}"/>
						</c:if>
						
						<dsp:getvalueof var="question" value="${question}" scope="request"/>
						
						<c:forEach var="headerContent" items="${content.headerContent}">
							<dsp:renderContentItem contentItem="${headerContent}"/>
						</c:forEach>
						<dsp:getvalueof var="totalNumRecs" value="${mainResultsList.totalNumRecs}" scope="request"/>
						
						<dsp:include page="pageHeader.jsp">
							<dsp:param name="question" value="${question}"/>
						</dsp:include>
						
						<div class="row">
							<div class="container preload-cover" id="spinner-on-start">
<!-- 								<div class="loading"> -->
<!-- 									<i class="fa fa-spinner fa-spin"></i> -->
<!-- 								</div> -->
							</div>
						
							<div id="sidebar" class="col-sm-3">
								<ul class="list-group facet expand">
									<c:forEach var="secondaryContent" items="${content.secondaryContent}">
										<dsp:renderContentItem contentItem="${secondaryContent}"/>
									</c:forEach>
								</ul>
							</div>
							
							<div id="mainContent" class="col-sm-9">
								<div id="error" class="col-md-9 alert alert-red message message-red hidden"></div>
								<div id="success" class="col-md-9 alert alert-green message message-green hidden"></div>
								<dsp:include src="/content/mainContent.jsp">
									<dsp:param name="contentItem" param="contentItem"/>
									<dsp:param name="totalNumRecs" value="${totalNumRecs}"/>
								</dsp:include>
							</div>
						</div>
					</div>
				</main>
				<input type="hidden" id="opened_facet" value=""/>
				<dsp:importbean bean="/atg/userprofiling/Profile"/>
				<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
					<dsp:param name="userId" bean="Profile.id"/>
					<dsp:param name="role" value="superAdmin,regularAdmin"/>
					<dsp:oparam name="false">
						<footer id="footer">
							<dsp:renderContentItem contentItem="${content.siteFooter}"/>
						</footer>
					</dsp:oparam>
				</dsp:droplet>
			</cp:endecaPageContainer>
		</c:otherwise>
	</c:choose>
</dsp:page>

