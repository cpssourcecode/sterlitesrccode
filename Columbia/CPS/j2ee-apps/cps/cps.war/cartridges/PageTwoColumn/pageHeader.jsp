<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:getvalueof var="totalNumRecs" param="totalNumRecs"/>
	<dsp:getvalueof var="question" param="question"/>
	<dsp:getvalueof var="queryString" param="Ntt"/>
	<dsp:getvalueof var="endecaRequestURI" value="${requestScope['javax.servlet.forward.request_uri']}" />
	
	<c:set var="queryStringCount" value="${fn:split(queryString, '|')}"></c:set>
	<c:set var="number" value="${fn:length(queryStringCount)}"/>

	<c:choose>
		<c:when test="${fn:startsWith(endecaRequestURI, '/search')}">
			<c:choose>
				<c:when test="${totalNumRecs eq 0 && number eq 1 }">					
					<h1 class="mb20">No results found for "${fn:replace(queryString, '|', ',')}
					<c:forEach var="refCrumb" items="${breadcrumbs.refinementCrumbs}">	
					,${refCrumb.label}								
				</c:forEach>".</h1>
				</c:when>
				<c:when test="${number > 1 &&  totalNumRecs eq 0 }">
				<h1 class="mb20">No results found for this combination "${fn:replace(queryString, '|', ',')}
					<c:forEach var="refCrumb" items="${breadcrumbs.refinementCrumbs}">	
					,${refCrumb.label}								
				</c:forEach>".</h1>
				</c:when>
				<c:otherwise>
					<div class="row">
						<div class="col-md-offset-3 col-md-9">
							<h1 class="mb20 searchAppend">
								<fmt:message key="listing.title.normal">
									<fmt:param value="${question}"/>
								</fmt:message>
							</h1>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			
		</c:when>
		<c:otherwise>
			<div class="row">
				<div class="col-md-offset-3 col-md-9"><h1 class="mb20">${breadcrumbs.refinementCrumbs[0].label}&nbsp;${breadcrumbs.refinementCrumbs[0].properties['category.description']}</h1></div>
			</div>
		</c:otherwise>
	</c:choose>
</dsp:page>