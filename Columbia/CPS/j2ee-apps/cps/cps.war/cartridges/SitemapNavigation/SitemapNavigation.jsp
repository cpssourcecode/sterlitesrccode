<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:droplet name="/cps/droplet/CategoryNavigationTreeDroplet">
		<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
		<dsp:param name="catCount" value="${content.catCount}" />
		<dsp:param name="subCatCount" value="0" />
		<dsp:param name="sortProperties" value="displayName" />
		<dsp:oparam name="output">
			<h4>Products</h4>
			<ul class="list-unstyled">
				<dsp:droplet name="/cps/droplet/CategoryNavigationTreeDroplet">
					<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
					<dsp:param name="catCount" value="-1" />
					<dsp:param name="subCatCount" value="0" />
					<dsp:param name="sortProperties" value="displayName" />
					<dsp:oparam name="output">
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="menuList" />
							<dsp:param name="elementName" value="rootCat" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="rootCatName" param="rootCat.name" />
								<dsp:getvalueof var="rootCatURL" param="rootCat.url" />
								<li>
									<a href="${rootCatURL}">${rootCatName}</a>
								</li>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</ul>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>