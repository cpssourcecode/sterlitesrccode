<dsp:importbean bean="/cps/util/ServicesContactFormHandler" />
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>

<dsp:page>

	<div class="row request-account-container">
		<div class="col-xs-12">
			<h3><dsp:valueof value="${contentItem.headingText}" /></h3>
		</div>
		<div class="col-xs-12 request-account-inner">
			<div class="row">
				<div class="col-xs-12 col-sm-4 text-center request-account-step">
					<h3>STEP 1:</h3>
					<dsp:valueof value="${contentItem.step1Content}" valueishtml="true" />
					<%--<p><a href="${contentItem.pdfUrl}" target="_blank" class="btn btn-lg btn-warning"><dsp:valueof value="${contentItem.downloadButtonLabel}" /></a></p>--%>
					<p><a href="#" target="_blank" class="btn btn-lg btn-warning"><dsp:valueof value="${contentItem.downloadButtonLabel}" /></a></p>
				</div>
				<div class="col-xs-12 col-sm-4 text-center request-account-step">
					<h3>STEP 2:</h3>
					<dsp:valueof value="${contentItem.step2Content}" valueishtml="true" />
				</div>
				<div class="col-xs-12 col-sm-4 text-center request-account-step">
					<h3>STEP 3:</h3>
					<dsp:valueof value="${contentItem.step3Content}" valueishtml="true" />
				</div>
				<div class="col-xs-12 request-account-next-steps">
					<h3>What happens next?</h3>
					<dsp:valueof value="${contentItem.nextStepContent}" valueishtml="true" />
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" nonce="${requestScope.nonce}">
		function rasResize(){
			var mh=0,ras=$('.request-account-step');
			ras.css('height','');
			if ($(window).width() > 767) {
				ras.each(function(){var t=$(this),h=t.height();mh=(h>mh?h:mh);});
				ras.css('height',mh);
			}
		}

		$(document).ready(rasResize);
		$(window).resize(rasResize);
	</script>

</dsp:page>