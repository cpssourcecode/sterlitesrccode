<dsp:page>

<%--   <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

         <dsp:importbean bean="/cps/droplet/ReadOriginatingRequest"/>
        <dsp:droplet name="ReadOriginatingRequest">
                 <dsp:oparam name="output">
                <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

                </dsp:oparam>
                </dsp:droplet> --%>

	<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

<c:if test="${not empty contentItem.originalTerms}">
    <div class="SearchAdjustments">
        <div>
             <c:forEach var="originalTerm" items="${contentItem.originalTerms}" varStatus="status">
                 <c:if test="${not empty contentItem.adjustedSearches[originalTerm]}">
                     Your search for <span style="font-weight: bold;">${originalTerm}</span> was adjusted to
                     <c:forEach var="adjustment" items="${contentItem.adjustedSearches[originalTerm]}" varStatus="status">
                         <span class="autoCorrect">${adjustment.adjustedTerms}</span>
                         <c:if test="${!status.last}">, </c:if>
                     </c:forEach>
                     <br>
                 </c:if>
                 <c:if test="${not empty contentItem.suggestedSearches[originalTerm]}">
                     Did you mean
                     <c:forEach var="suggestion" items="${contentItem.suggestedSearches[originalTerm]}" varStatus="status">
                         <c:set var="label">
                             ${suggestion.label}
                             <c:if test="${not empty suggestion.count}">
                                 (${suggestion.count} results)
                             </c:if>
                         </c:set>
                         <c:if test="${!status.last}">, </c:if>
                     </c:forEach>?
                     <br>
                 </c:if>
             </c:forEach>
        </div>
    </div>
</c:if>

</dsp:page>