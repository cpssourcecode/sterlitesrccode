<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/droplet/CPSMultiPriceDroplet"/>
	<dsp:importbean bean="/cps/droplet/GetPriceFromMapDroplet"/>

	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>

	<dsp:getvalueof var="transient" bean="Profile.transient"/>

	<dsp:droplet name="CPSMultiPriceDroplet">
		<dsp:param bean="Profile" name="profile"/>
		<dsp:param bean="ShoppingCart" name="shoppingCart"/>
		<dsp:param name="records" value="${content.records}"/>
		<dsp:oparam name="outputPrice">
			<dsp:getvalueof var="itemPrices" vartype="Map" param="itemPrices"/>
		</dsp:oparam>
	</dsp:droplet>

	<h3 class="centered-mobile">You May Be Interested In</h3>

	<div class="row">
		<div class="col-sm-12">
			<div class="owl-thumbnails">


				<c:forEach var="record" items="${content.records}">

					<dsp:getvalueof var="prodId" value="${record.attributes['product.repositoryId']}"/>
					<dsp:getvalueof var="skuId" value="${record.attributes['sku.repositoryId']}"/>
					<dsp:getvalueof var="displayName" value="${record.attributes['product.displayName']}"/>
					<dsp:getvalueof var="description" value="${record.attributes['product.description']}"/>
					<dsp:getvalueof var="web_url" value="${record.attributes['product.web_url']}"/>
					<dsp:getvalueof var="uomPrimary" value="${record.attributes['product.uomPrimary']}"/>
					<dsp:getvalueof var="productUrl" value="${fn:toLowerCase(record.attributes['product.seoUrl'])}"/>
					<c:set var="web_url" value="${empty web_url ? '/assets/images/plp-placeholder.png' : web_url}"/>


					<dsp:droplet name="GetPriceFromMapDroplet">
						<dsp:param name="productId" value="${prodId}"/>
						<dsp:param name="itemPrices" value="${itemPrices}"/>
						<dsp:oparam name="outputPrice">
							<dsp:getvalueof var="priceValue" param="itemPrice"/>
						</dsp:oparam>
					</dsp:droplet>

					<div class="item">
						<div class="thumbnail">
							<div class="row">
								<div class="col-sm-12 col-xs-12">
									<a href="${productUrl}">
										<div class="image">
											<cp:readerimg src="${web_url}" alt="" />
										</div>
											<%--<h4 class="hidden-xs">After Sales Service</h4>--%>
										<h4 class="hidden-xs">${displayName}</h4>
									</a>
								</div>
								<div class="col-sm-12 col-xs-12">
									<div class="thumbnail-content">
										<a href="${productUrl}">
											<h4 class="visible-xs">${displayName}</h4>
										</a>
										<small>${description}</small>
									</div>
								</div>
							</div>
							<div class="row counter floated-bottom">
								<div class="col-sm-12 col-xs-12">
									<div class="row">
										<div class="col-sm-12 col-xs-12">
											<big>
												<span class="text-success"><dsp:valueof value="${priceValue}" converter="currencyConversion"/></span>
												<span><c:if test="${not empty uomPrimary}">/${uomPrimary}</c:if></span>
											</big>
										</div>


										<dsp:form action="#" method="post" formid="addToCartSpotlightForm${prodId}"
												  id="addToCartSpotlightForm${prodId}" iclass="hidden">
											<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10"/>
											<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" value="${skuId}"
													   type="hidden"/>
											<dsp:input bean="CartModifierFormHandler.items[0].productId" value="${prodId}"
													   type="hidden"/>
											<dsp:input bean="CartModifierFormHandler.items[0].quantity" value="1"
													   type="hidden" id="quantityBean${prodId}"/>
											<dsp:input bean="CartModifierFormHandler.addMultipleItemsToOrder" type="hidden" value="true" priority="-10"/>
										</dsp:form>

										<div class="col-sm-5 col-xs-4 qty-field">
											<input id="quantity${prodId}" type="text" class="form-control text-center" placeholder="Qty" value="1" data-event-keypress-id="cartridges6"  maxlength="4">
										</div>

										<dsp:droplet name="/cps/droplet/AccessRightDroplet">
											<dsp:param name="profile" bean="Profile"/>
											<dsp:param name="accessRightKey" value="plp-pdp-cart"/>
											<dsp:oparam name="false">
												<dsp:getvalueof var="permissionDenied" value="true"/>
											</dsp:oparam>
										</dsp:droplet>

										<c:choose>
											<c:when test="${transient}">
												<dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
													<dsp:oparam name="true">
														<div class="col-sm-7 col-xs-8">
															<a href="#" class="btn btn-warning btn-block"
															   data-event-click-id="cartridges5" data-event-click-param0="${prodId}" data-event-click-param1="${prodId}" data-event-click-param2="${transient}" >
																<fmt:message key="listing.product.tile.addToCart"/>
															</a>
														</div>
													</dsp:oparam>
													<dsp:oparam name="false">
														<div class="col-sm-7 col-xs-8">
															<a href="#" class="btn btn-warning btn-block"
															   data-productId="${prodId}"
															   data-skuId="${skuId}"
															   data-toggle="modal" data-target="#logIn">
																<fmt:message key="listing.product.tile.addToCart"/>
															</a>
														</div>
													</dsp:oparam>
												</dsp:droplet>
											</c:when>
											<c:when test="${permissionDenied}">
												<div class="col-sm-7 col-xs-8">
													<a href="#" class="btn btn-warning btn-block" data-toggle="modal" data-target="#permissionDenied">
														<fmt:message key="listing.product.tile.addToCart"/>
													</a>
												</div>
											</c:when>
											<c:otherwise>
												<div class="col-sm-7 col-xs-8">
													<a href="#" class="btn btn-warning btn-block"
													   data-event-click-id="cartridges5" data-event-click-param0="${prodId}" data-event-click-param1="${prodId}" data-event-click-param2="${transient}" >
														<fmt:message key="listing.product.tile.addToCart"/>
													</a>
												</div>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
							</div>
						</div>
					</div>


				</c:forEach>


			</div>
		</div>
	</div>

</dsp:page>


