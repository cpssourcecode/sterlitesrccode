<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

	<section class="home-callouts hidden-xs hidden-sm">
		<div class="row">
			<c:forEach items="${content.Items}" var="item">
				<dsp:renderContentItem contentItem="${item}"/>
			</c:forEach>
		</div>
	</section>

</dsp:page>