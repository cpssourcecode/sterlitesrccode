<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />

	<div class="col-sm-3">
		<div class="callout">
			<c:choose>
				<c:when test="${not empty content.content && content.image['@type'] == 'Image'}">
					<c:choose>
						<c:when test="${not empty content.image.href}">
							<a href="${content.image.href}" style="min-height: 220px; background-image: url('${content.image.url}');">
								${content.content}
							</a>
						</c:when>
						<c:otherwise>
							<div style="min-height: 225px; background-image: url('${content.image.url}');">
								${content.content}
							</div>
						</c:otherwise>
					</c:choose>
				</c:when>
				<c:otherwise>
					<dsp:renderContentItem contentItem="${content.image}"/>
				</c:otherwise>
			</c:choose>
		</div>
	</div>

</dsp:page>