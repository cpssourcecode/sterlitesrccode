<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<c:set var="url" value="${content.url}"/>
	<c:set var="imageURL" value="${content.imageURL}"/>
	<c:set var="alt" value="${content.alt}"/>
	<div>
		<a href="${not empty url ? url : '#'}">
			<cp:readerimg src="${staticContentPrefix}${not empty imageURL ? imageURL : ''}"
				 alt="${not empty alt ? alt : ''}" />
		</a>
	</div>
</dsp:page>