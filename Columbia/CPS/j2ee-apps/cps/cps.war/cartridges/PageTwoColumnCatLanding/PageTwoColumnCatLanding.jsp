<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof param="loadSection" var="loadSection"/>
	<dsp:importbean bean="/cps/droplet/GetCategoryByNavigationState"/>
	<dsp:getvalueof var="isCategoryLanding" value="true" scope="request"/>

	<c:choose>
		<c:when test="${loadSection eq 'inner'}">
			
			<c:forEach var="headerContent" items="${content.headerContent}">
				<dsp:renderContentItem contentItem="${headerContent}"/>
			</c:forEach>
			
			<dsp:getvalueof var="mainResultsList" value="${vsg_utils:getMainContentSubItem(content, 'MainResultsList')}"/>
			
			<dsp:getvalueof var="totalNumRecs" value="${mainResultsList.totalNumRecs}"/>
			<dsp:getvalueof var="question" param="Ntt"/>

			<dsp:include page="pageHeader.jsp">
				<dsp:param name="question" value="${question}"/>
				<dsp:param name="totalNumRecs" value="${totalNumRecs}"/>
			</dsp:include>


			<div class="row">
				<div id="sidebar" class="col-sm-3">
					<ul class="list-group facet expand">
						<c:forEach var="secondaryContent" items="${content.secondaryContent}">
							<dsp:renderContentItem contentItem="${secondaryContent}"/>
						</c:forEach>
					</ul>
				</div>	
						
				<div id="mainContent" class="col-sm-9">
					<div id="error" class="col-md-9 alert alert-red message message-red hidden"></div>
					<div id="success" class="col-md-9 alert alert-green message message-green hidden"></div>
					
					<dsp:include src="/content/mainContent.jsp">
						<dsp:param name="contentItem" param="contentItem"/>
						<dsp:param name="totalNumRecs" value="${totalNumRecs}"/>
					</dsp:include>
					
				</div>
				
			</div>

		</c:when>
		
		<c:otherwise>
			<c:set var="canonicalLink" value=""/>
			<c:forEach var="hcnt" items="${content.headerContent}">
				<c:if test="${'Breadcrumbs'==hcnt.name}">
					<c:if test="${not fn:startsWith(hcnt['endeca:auditInfo']['ecr:resourcePath'],'content/Web/Search')}">
						<c:forEach var="refc" items="${hcnt.refinementCrumbs}">
							<c:if test="${refc.dimensionName eq 'product.category'}">
								<c:set var="canonicalLink" value=""/>
							</c:if>
						</c:forEach>
					</c:if>
				</c:if>
			</c:forEach>

			<dsp:getvalueof var="mainResultsList" value="${vsg_utils:getMainContentSubItem(content, 'MainResultsList')}"/>
			<dsp:getvalueof var="mainContent" value="${content.mainContent}"/>
			<dsp:getvalueof var="MainSubCategories" value="${vsg_utils:getMainContentSubItem(content, 'MainSubCategories')}"/>
			<dsp:getvalueof var="ancestors" value="${MainSubCategories.ancestors}"/>
			<dsp:getvalueof var="firstAncestor" value="${ancestors[0]}"/>
			<dsp:getvalueof var="categoryId" value="${firstAncestor.properties['category.repositoryId'] }"/>
			<dsp:getvalueof var="topLevelCategoryId" bean="/atg/commerce/catalog/CatalogNavigation.currentCategory" />
			<dsp:droplet name="/atg/commerce/catalog/custom/CatalogLookup">
				<dsp:param name="id" value="homeStoreCatalog" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="topLevelCatalog" param="element" />
				</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="/atg/commerce/catalog/CategoryLookup">
				<dsp:param name="id" value="${categoryId}" />
				<dsp:param name="catalog" value="${topLevelCatalog}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="metaKeywords" param="element.metaKeywords"/>
					<dsp:getvalueof var="metaDescription" param="element.metaDescription"/>
				</dsp:oparam>
			</dsp:droplet>
			<cp:endecaPageContainer title="${content.headerContent[0].refinementCrumbs[0].label}" metaKeywords="${metaKeywords}"
					metaDescription="${metaDescription}"
					page="twoColumn" type="endecaCartridge" endecaDrivenPage="true" canonicalLink="${canonicalLink}">
				<dsp:renderContentItem contentItem="${content.siteHeader}"/>

				<dsp:getvalueof var="selectedOrg" bean="/atg/userprofiling/Profile.CSRSelectedOrg"/>
				<c:if test="${not empty selectedOrg}">
					<dsp:getvalueof var="csrClass" value="admin-access"/>
				</c:if>

				<main id="body">
					<div class="container" id="pageContent" ${csrClass}>

						<c:forEach var="headerContent" items="${content.headerContent}">
							<dsp:renderContentItem contentItem="${headerContent}"/>
						</c:forEach>


						<dsp:getvalueof var="totalNumRecs" value="${mainResultsList.totalNumRecs}"/>
						<dsp:getvalueof var="question" param="Ntt"/>
						<dsp:include page="pageHeader.jsp">
							<dsp:param name="question" value="${question}"/>
							<dsp:param name="totalNumRecs" value="${totalNumRecs}"/>
						</dsp:include>

						<div class="row">
							<div id="sidebar" class="col-sm-3">
								<ul class="list-group facet expand">
									<c:forEach var="secondaryContent" items="${content.secondaryContent}">
										<dsp:renderContentItem contentItem="${secondaryContent}"/>
									</c:forEach>
								</ul>
							</div>
							
							<div id="mainContent" class="col-sm-9">
								<div id="error" class="col-md-9 alert alert-red message message-red hidden"></div>
								<div id="success" class="col-md-9 alert alert-green message message-green hidden"></div>
								
								<dsp:include src="/content/mainContent.jsp">
									<dsp:param name="contentItem" param="contentItem"/>
									<dsp:param name="totalNumRecs" value="${totalNumRecs}"/>
								</dsp:include>
							</div>
						</div>
					</div>
				</main>
				<dsp:importbean bean="/atg/userprofiling/Profile"/>
				<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
					<dsp:param name="userId" bean="Profile.id"/>
					<dsp:param name="role" value="superAdmin,regularAdmin"/>
					<dsp:oparam name="false">
						<footer id="footer">
							<dsp:renderContentItem contentItem="${content.siteFooter}"/>
						</footer>
					</dsp:oparam>
				</dsp:droplet>
			</cp:endecaPageContainer>
		</c:otherwise>
	</c:choose>
</dsp:page>