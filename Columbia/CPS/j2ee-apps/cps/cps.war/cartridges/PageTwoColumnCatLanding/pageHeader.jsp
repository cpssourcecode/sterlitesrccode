<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:getvalueof var="totalNumRecs" param="totalNumRecs"/>
	<dsp:getvalueof var="question" param="question"/>
	<dsp:getvalueof var="endecaRequestURI" value="${requestScope['javax.servlet.forward.request_uri']}" />

	<div class="row">
		<div class="col-md-offset-3 col-md-9"><h1 class="mb20">${breadcrumbs.refinementCrumbs[0].label}&nbsp;${breadcrumbs.refinementCrumbs[0].properties['category.displayName']}</h1></div>
	</div>
</dsp:page>