<dsp:page>
	<dsp:importbean bean="/atg/commerce/catalog/CategoryLookup"/>

	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof var="endecaRequestURI" value="${requestScope['javax.servlet.forward.request_uri']}"/>
	<c:set var="breadcrumbs" value="${content}" scope="request"/>
	<c:set var="search" value="${fn:startsWith(content['endeca:auditInfo']['ecr:resourcePath'],'content/Web/Search')}"/>

	<fmt:bundle basename="web.resources.WebAppResources">
		<c:choose>
			<c:when test="${fn:startsWith(endecaRequestURI, '/search')}">
				<ol class="breadcrumb"> 
					<c:if test="${not empty breadcrumbs.searchCrumbs }">
					<%--	<li class="active">"${question}"</li> --%> <!--no need to print this search term here in the BC -->
					</c:if>
				</ol>
			</c:when>
			<c:otherwise>
				<ol class="breadcrumb">
					<c:forEach var="refinement" items="${content.refinementCrumbs}">
						<c:if test="${refinement.dimensionName eq 'product.category'}">
							<c:set var="navRefinement" value="${refinement}" scope="request"/>
							<c:forEach items="${refinement.ancestors}" var="ancestor">
								<dsp:getvalueof var="catId" value="${ancestor.properties['category.repositoryId'] }"/>
								<dsp:droplet name="CategoryLookup">
									<dsp:param name="id" value="${catId}"/>
									<dsp:setvalue param="category" paramvalue="element"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="metaKeywords" param="category.metaKeywords"/>
										<dsp:getvalueof var="metaDescription" param="category.metaDescription"/>
									</dsp:oparam>
									<dsp:oparam name="empty">
									</dsp:oparam>
								</dsp:droplet>

								<li><a href="#"
									   data-event-click-id="cartridges3" data-event-click-param0="${fn:toLowerCase(vsg_utils:getUrlForAction(ancestor))}" data-event-click-param1="${metaDescription}" data-event-click-param2="${metaKeywords}" >${ancestor.label}</a>
								</li>
							</c:forEach>
							<li class="active">
									${refinement.label}&nbsp;${refinement.properties['category.displayName']}
							</li>
						</c:if>
					</c:forEach>
				</ol>
			</c:otherwise>
		</c:choose>
	</fmt:bundle>
</dsp:page>

