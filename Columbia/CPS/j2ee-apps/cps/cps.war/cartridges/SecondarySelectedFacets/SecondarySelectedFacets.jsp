<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<fmt:bundle basename="web.resources.WebAppResources">
		<c:choose>
			<c:when test="${searchpage eq 'search' && (not (fn:length(breadcrumbs.searchCrumbs) eq 1 && fn:length(breadcrumbs.refinementCrumbs) eq 0))}">
				<li class="expand-item facet-selected active">
					<span class="expand-label"><fmt:message key="selected.title"/></span>
					<ul class="facet-selected expand-static">
						<c:forEach var="searchCrumb" items="${breadcrumbs.searchCrumbs}" varStatus="loop">
							<c:if test="${loop.index ne 0}">
							<c:set var='searchCrumbTerms' value='${vsg_utils:escapeHtml4(searchCrumb.terms)}'/> 
								<li>
									<a href="#" data-event-click-id="cartridges16" data-event-click-param0="${searchCrumbTerms}" >
									<i class="fa fa-close"></i>
									<fmt:message key="selected.searchTerm"/>:
									<strong>${empty searchCrumb.correctedTerms ? searchCrumb.terms : searchCrumb.correctedTerms}</strong>
									</a>
								</li>
							</c:if>
						</c:forEach>
						<c:forEach var="refCrumb" items="${breadcrumbs.refinementCrumbs}">
						 <c:set var='removeAction' value='${vsg_utils:getUrlForAction(refCrumb.removeAction)}'/> 
						 <c:set var='removeAction1' value=' ${vsg_utils:escapeHtml4(removeAction)}'/> 
						
							<li>
								<a href="#" data-event-click-id="cartridges17" data-event-click-param0="${removeAction1}" >
								<i class="fa fa-close"></i>
								<%-- <fmt:message key="refinement.${refCrumb.displayName}"/> --%>
								<fmt:message key="refinement.${refCrumb.displayName}" var="refName"/>
								<c:if test="${fn:startsWith(refName,'???')}">
									<c:set var="refName" value="${refCrumb.displayName}"/>
								</c:if>
								${refName}:
								<strong>${refCrumb.label}&nbsp;${refCrumb.properties['category.description']}</strong>
								</a>
							</li>
						</c:forEach>						 
				 
				  <c:set var='question1' value='${vsg_utils:escapeHtml4(question)}'/> 
				 
						<li class="facet-toggle">
							<a href="#" data-toggle="modal" data-event-click-id="cartridges18" data-event-click-param0="${question1}" >
								<fmt:message key="selected.clearall"/>
							</a>
						</li>
					</ul>
				</li>
			</c:when>
			<c:when test="${searchpage ne 'search' && (fn:length(breadcrumbs.refinementCrumbs) > 1 || fn:length(breadcrumbs.searchCrumbs) > 0) ||
				(fn:length(breadcrumbs.refinementCrumbs) >= 1 && breadcrumbs.refinementCrumbs[0].dimensionName ne 'product.category')}">
			
				<li class="expand-item facet-selected active">
					<span class="expand-label"><fmt:message key="selected.title"/></span>
					<ul class="facet-selected expand-static">
						<c:forEach var="searchCrumb" items="${breadcrumbs.searchCrumbs}" varStatus="loop">
							<li>
								<a href="#" data-event-click-id="cartridges19" data-event-click-param0="${vsg_utils:escapeHtml4(searchCrumb.terms)}" >
								<i class="fa fa-close"></i>
								<fmt:message key="selected.searchTerm"/>:
								<strong>${empty searchCrumb.correctedTerms ? searchCrumb.terms : searchCrumb.correctedTerms}</strong>
								</a>
							</li>
						</c:forEach>
						<c:forEach var="refCrumb" items="${breadcrumbs.refinementCrumbs}">
							<c:if test="${refCrumb.dimensionName ne 'product.category'}">
								<c:set var="no_brand_link" value="${vsg_utils:getUrlForAction(refCrumb.removeAction)}"/>
								<li>
									<a role="button" data-event-click-id="cartridges20" data-event-click-param0="${vsg_utils:escapeHtml4(no_brand_link)}" >
									<i class="fa fa-close"></i>
									<fmt:message key="refinement.${refCrumb.displayName}" var="refName"/>
									<c:if test="${fn:startsWith(refName,'???')}">
										<c:set var="refName" value="${refCrumb.displayName}"/>
									</c:if>
									${refName}:<strong>${refCrumb.label}&nbsp;${refCrumb.properties['category.description']}</strong>
									</a>
								</li>
							</c:if>
						</c:forEach>
						<li class="facet-toggle">
							<c:set var="clearAll" value="${breadcrumbs.refinementCrumbs[0].properties['categoryNavState']}"/>
							<c:choose>
								<c:when test="${not empty clearAll}">
									<a href="${clearAll}">
										<fmt:message key="selected.clearall"/>
									</a>
								</c:when>
								<c:otherwise>
									<a href="#" data-event-click-id="cartridges21" data-event-click-param0="/all-brands" >
										<fmt:message key="selected.clearall"/>
									</a>
								</c:otherwise>
							</c:choose>
						</li>
					</ul>
				</li>
			</c:when>
		</c:choose>
	</fmt:bundle>
</dsp:page>