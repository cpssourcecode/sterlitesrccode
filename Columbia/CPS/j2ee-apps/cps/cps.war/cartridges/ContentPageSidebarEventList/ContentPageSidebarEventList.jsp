<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<c:if test="${not empty content.sidebarEvents}">
		<div class="well well-grey hidden-xs" id="sidebar-events">
			<h4>${content.listTitle}</h4>
			<ul class="events">
				<c:forEach var="events" items="${content.sidebarEvents}">
					<dsp:renderContentItem contentItem="${events}" />
				</c:forEach>
			</ul>
		</div>
	</c:if>
</dsp:page>