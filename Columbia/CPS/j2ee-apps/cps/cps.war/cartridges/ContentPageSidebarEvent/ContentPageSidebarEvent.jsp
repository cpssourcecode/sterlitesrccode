<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<li>
		<h5 class="mt0">${content.eventTitle}</h5>
		<strong>${content.eventDate}</strong>
		<p>${content.eventDescription}</p>
		<a class="more-link" href="${content.eventLink}">Learn more</a>
	</li>	
</dsp:page>