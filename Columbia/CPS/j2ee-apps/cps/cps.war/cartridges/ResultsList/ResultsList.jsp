<dsp:page>

 <%-- <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/> --%>
  <%-- <dsp:importbean bean="/cps/droplet/ReadOriginatingRequest" /> --%>
  <dsp:importbean bean="/atg/commerce/catalog/CatalogNavigation" />

  <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />


 <%--  <dsp:droplet name="ReadOriginatingRequest">
  	<dsp:oparam name="output">
  		<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
  	</dsp:oparam>
  </dsp:droplet> --%>



   	  <dsp:getvalueof var="size" value="${contentItem.totalNumRecs}"/>
      <dsp:getvalueof var="page" value="${contentItem.recsPerPage}"/>
      <c:set var="question"><dsp:valueof param="Ntt" valueishtml="true"/></c:set>
      <%-- Display the number of search results if this is a query search --%>

       <c:choose>
            <c:when test="${size == 1}">

              <c:choose>
                <c:when test="${not empty question}">
                   <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.oneResultFor"/>
                </c:when>
                <c:otherwise>
                  <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.oneResult"/>
                </c:otherwise>
              </c:choose>

            </c:when>
            <c:otherwise>
              <c:choose>
                <c:when test="${not empty question}">
                  <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.resultFor"/>
                </c:when>
                <c:otherwise>
                  <fmt:message var="resultsMessage" key="facet.facetGlossaryContainer.results"/>
                </c:otherwise>
              </c:choose>

            </c:otherwise>
          </c:choose>

        <%-- Use for search results
          <span id="resultsCount"><c:out value="${size}"/></span>&nbsp;<c:out value="${resultsMessage}"/>&nbsp;<span class="searchTerm">${fn:escapeXml(question)}</span>
      --%>


  <dsp:getvalueof var="p" param="p"/>
  <c:if test="${empty p}">
    <c:set var="p" value="1" />
  </c:if>
  <dsp:getvalueof var="viewAll" param="viewAll"/>
  <c:if test="${empty viewAll}">
    <c:set var="viewAll" value="false" />
  </c:if>

	  	 <dsp:getvalueof var="productList" value="${contentItem.records}"/>
	  	 <dsp:include page="/catalog/gadgets/category-listing.jsp">
		    <dsp:param name="contentItem" value="${contentItem}"/>
		    <dsp:param name="categoryId" value="${categoryId}"/>
		    <dsp:param name="categoryName" param="element.displayName"/>
		    <dsp:param name="productList" value="${productList}"/>
		    <dsp:param name="productsRenderer" value="/catalog/gadgets/category-products-list.jsp"/>
		    <dsp:param name="shareableTypeId" param="shareableTypeId"/>
		    <dsp:param name="sort" param="sort"/>
		    <dsp:param name="p" param="p"/>
		    <dsp:param name="viewAll" param="viewAll"/>
		  </dsp:include>


</dsp:page>