<dsp:page>
    <dsp:importbean bean="/cps/userprofiling/NewsletterSignUpFormHandler"/>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="userId" bean="Profile.id"/>
    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>

    <jsp:useBean id="date" class="java.util.Date"/>
    <fmt:formatDate value="${date}" pattern="yyyy" var="currentYear"/>

    <dsp:droplet name="/cps/droplet/RoleLookupDroplet">
        <dsp:param name="userId" bean="Profile.id"/>
        <dsp:param name="role" value="superAdmin,regularAdmin"/>
        <dsp:oparam name="false">
            <div class="row">
                <div class="container">
                    <div class="col-sm-4 mb20">
                        <ul class="footer-social clearfix">
                            <c:forEach items="${content.socialsContainer.footerSocial}" var="item">
                                <dsp:renderContentItem contentItem="${item}"/>
                            </c:forEach>
                        </ul>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-4 col-xxs-12 mb20">
                                <dsp:renderContentItem contentItem="${contentItem.support}"/>
                            </div>
                            <div class="col-xs-4 col-xxs-12 mb20">
                                <dsp:renderContentItem contentItem="${contentItem.ourCompany}"/>
                            </div>
                            <div class="col-xs-4 col-xxs-12 mb20">
                                <dsp:renderContentItem contentItem="${contentItem.pipesAndSupplies}"/>
                            </div>
                        </div>
                    </div>
                    <div class="copyright text-center">
                        © Copyright <c:out value="${currentYear}"/>,&nbsp;<dsp:renderContentItem
                            contentItem="${contentItem.copyright}"/>
                        <dsp:renderContentItem contentItem="${contentItem.staticText}"/>
                    </div>
                </div>
            </div>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>
