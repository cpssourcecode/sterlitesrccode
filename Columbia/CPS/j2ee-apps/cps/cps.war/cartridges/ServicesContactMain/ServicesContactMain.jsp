<dsp:importbean bean="/cps/util/ServicesContactFormHandler" />
<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>


<dsp:page>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	
	<div class="row services-contact-container">

		<%-- CLICKABLE TRIGGER TO DISPLAY FORM --%>
		<div class="col-xs-12 services-contact-trigger">
			<p><a href="javascript:void(0);" class="services-contact-trigger-link"><dsp:valueof value="${contentItem.triggerLinkText}" /></a></p>
		</div>

		<%-- CONTACT FORM --%>
		<div class="well well-gray col-xs-12 hidden-print services-contact-form" style="display: none;">
			<dsp:form method="POST" action="/">
				<div class="alert alert-danger services-contact-form-errors" style="display:none;"></div>
				<div class="row">
					<div class="form-group col-xs-12 col-sm-4">
						<fmt:message key="content.service.contact.firstName" var="firstNamePlaceholder"/>
						<dsp:input bean="ServicesContactFormHandler.firstName" type="text" iclass="form-control services-contact-firstName" maxlength="30">
							<dsp:tagAttribute name="placeholder" value="*${firstNamePlaceholder}" />
						</dsp:input>
					</div>
					<div class="form-group col-xs-12 col-sm-4">
						<fmt:message key="content.service.contact.lastName" var="lastNamePlaceholder"/>
						<dsp:input bean="ServicesContactFormHandler.lastName" type="text" iclass="form-control services-contact-lastName" maxlength="30">
							<dsp:tagAttribute name="placeholder" value="*${lastNamePlaceholder}" />
						</dsp:input>
					</div>
					<div class="form-group col-xs-12 col-sm-4">
						<fmt:message key="content.service.contact.emailAddress" var="emailAddressPlaceholder"/>
						<dsp:input bean="ServicesContactFormHandler.emailAddress" type="text" iclass="form-control services-contact-emailAddress" maxlength="100">
							<dsp:tagAttribute name="placeholder" value="*${emailAddressPlaceholder}" />
						</dsp:input>
					</div>
					<div class="form-group col-xs-12 col-sm-12">
						<fmt:message key="content.service.contact.message" var="messagePlaceholder"/>
						<textarea name="/cps/util/ServicesContactFormHandler.message" cols="5" rows="7" maxlength="500" placeholder="*${messagePlaceholder}" class="form-control services-contact-message"></textarea>
					</div>
					<div class="col-xs-12 col-sm-12">
					<c:choose>
						<c:when test="${isTransient}">
						<div id="service-form-captcha">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<div id="recaptchaService"></div>
									</div>
								</div>
							</div>
						</div>
						
						<ul class="list-inline">
							<li>
								<button type="button" id="serviceButton" class="btn btn-primary services-contact-submit"><fmt:message key="content.service.contact.submit" /></button>
							</li>
							<li>
								<button type="button" class="btn btn-info services-contact-cancel"><fmt:message key="content.service.contact.cancel" /></button>
							</li>
						</ul>
						</c:when>
						<c:otherwise>
						<ul class="list-inline">
							<li>
								<button type="button" class="btn btn-primary services-contact-submit"><fmt:message key="content.service.contact.submit" /></button>
							</li>
							<li>
								<button type="button" class="btn btn-info services-contact-cancel"><fmt:message key="content.service.contact.cancel" /></button>
							</li>
						</ul>
						</c:otherwise>
					</c:choose>				
					</div>
				</div>
				<dsp:getvalueof var="serviceName" param="contentTitle" />
				<dsp:input type="hidden" bean="ServicesContactFormHandler.serviceName" value="${serviceName}" />
				<dsp:input type="hidden" bean="ServicesContactFormHandler.servicesContactUs" value="true" />
			</dsp:form>
		</div>

		<%-- CONFIRMATION --%>
		<div class="well well-gray col-xs-12 hidden-print services-contact-confirmation" style="display: none;">
			<p><dsp:valueof value="${contentItem.confirmationText}" /></p>
			<br />
			<div class="form-group">
				<button type="button" class="btn btn-default btn-lg services-contact-confirm"><dsp:valueof value="${contentItem.closeText}" /></button>
			</div>
		</div>

	</div>
<script type="text/javascript" nonce="${requestScope.nonce}">
$(document).ready(function() {
    checkServiceSubmitButton();
    renderCaptchaCallbackService();
});
    
var service_captcha_success_default = false;
var service_captcha_default = null;

function checkServiceSubmitButton() {
    var enableLoginButton = true;
    if ($('#service-form-captcha').length > 0) {
        enableLoginButton = service_captcha_success_default;
    }

    if (enableLoginButton) {
        $('#serviceButton').removeAttr('disabled');
    } else {
        $('#serviceButton').attr('disabled', 'disabled');
    }
}

function renderCaptchaCallbackService() {
    var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
    service_captcha_default = grecaptcha.render(
        'recaptchaService', {
            'sitekey' : sitekey,
            'callback' : validateRecaptchaDefaultService,
            'expired-callback' : expireRecaptchaDefaultService
    });
}

function validateRecaptchaDefaultService(captchaResponse) {
    service_captcha_success_default = true;
    checkServiceSubmitButton();
}

function expireRecaptchaDefaultService() {
    //console.log('inside expireRecaptchaDefault');
    //console.log('credit_application_captcha_success_default b4 :: ' + credit_application_captcha_success_default);
    grecaptcha.reset(service_captcha_default);
    service_captcha_success_default = false;
    checkServiceSubmitButton();
    //console.log('credit_application_captcha_success_default after :: ' + credit_application_captcha_success_default);
}

</script>
</dsp:page>