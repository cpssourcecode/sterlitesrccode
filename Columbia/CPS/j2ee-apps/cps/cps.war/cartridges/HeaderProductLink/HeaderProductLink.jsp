<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>

	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>

	<dsp:getvalueof var="testimonials" param="testimonials" />
	<dsp:getvalueof var="homepageSidePromo" param="homepageSidePromo" />
	
	<li class="visible-xs visible-sm">
		<a href="#product-menu-mobile" data-toggle="collapse" aria-expanded="false" 
			aria-controls="product-menu-mobile" class="collapsed visible-xs visible-sm">All Products</a>	
			
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
		<dsp:droplet name="/cps/droplet/CategoryNavigationTreeDroplet">
			<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
			<dsp:param name="catCount" value="${content.catCount}" />
			<dsp:param name="subCatCount" value="${content.subCatCount}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="navLinkText" value="${content.navLinkText}" />
				<ul id="product-menu-mobile" class="collapse mobile-subnav" >
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="menuList" />
						<dsp:param name="elementName" value="catMenu"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="catMenu" param="catMenu"/>

							<li data-trigger="<dsp:valueof  param="index"/>">
								<a href="${catMenu.URL}">${vsg_utils:formatName(catMenu.name)}</a>
							</li>
						</dsp:oparam>
					</dsp:droplet>
					<li class="visible-xs visible-sm">
						<a href="/all-products">See All Products</a>
					</li>
				</ul>
			</dsp:oparam>
		</dsp:droplet>
	</li>

	<li class="dropdown nav-products hidden-xs hidden-sm">
		<a href="#" data-event-click-id="cartridges4" class="dropdown-toggle hidden-xs hidden-sm" 
			data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">All Products <i class="fa fa-angle-down"></i></a>
			
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
		<dsp:droplet name="/cps/droplet/CategoryNavigationTreeDroplet">
			<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
			<dsp:param name="catCount" value="${content.catCount}" />
			<dsp:param name="subCatCount" value="${content.subCatCount}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="navLinkText" value="${content.navLinkText}" />
				<ul class="dropdown-menu dropdown-menu-first-level nav-subnav dropdown-menu-autoheight" >
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="menuList" />
						<dsp:param name="elementName" value="catMenu"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="catMenu" param="catMenu"/>

							<li data-trigger="<dsp:valueof  param="index"/>">
								<a href="${catMenu.URL}">${vsg_utils:formatName(catMenu.name)}</a>
							</li>
						</dsp:oparam>
					</dsp:droplet>
				</ul>
				<ul class="subdropdown-menu-second-level list-unstyled dropdown-menu-autoheight">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="menuList" />
						<dsp:param name="elementName" value="catMenu"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="catMenu" param="catMenu"/>
							<li data-toggler="<dsp:valueof  param="index"/>">
							<dsp:droplet name="ForEach">
								<dsp:param name="array" param="catMenu.subMenu" />
								<dsp:param name="elementName" value="subCatMenu"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="subCatMenu" param="subCatMenu"/>
									<a href="${vsg_utils:escapeHtml(subCatMenu.URL)}">${vsg_utils:formatName(vsg_utils:escapeHtml(subCatMenu.name))}</a>
								</dsp:oparam>

							</dsp:droplet>
							</li>
						</dsp:oparam>
					</dsp:droplet>
				</ul>
			</dsp:oparam>
		</dsp:droplet>
	</li>

</dsp:page>