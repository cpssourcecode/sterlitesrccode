<%--
  PageSlot
  
  Passes the contents of the PageSlot to a renderer JSP that knows how to
  handle the contents particular type.
--%>
<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:getvalueof var="endecaPreview" bean="/atg/endeca/assembler/cartridge/manager/AssemblerSettings.previewEnabled" scope="request" />

	<c:if test="${endecaPreview && empty rootComponent}">
		<dsp:getvalueof var="rootComponent" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" scope="request" />
	</c:if>

	<c:forEach var="element" items="${content.contents}">
		<dsp:renderContentItem contentItem="${element}" />
	</c:forEach>
</dsp:page>