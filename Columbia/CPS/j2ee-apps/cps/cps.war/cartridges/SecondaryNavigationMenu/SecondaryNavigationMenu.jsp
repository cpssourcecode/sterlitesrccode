<dsp:page>
	<dsp:importbean bean="/cps/commerce/catalog/NavStateFromMultipleRefinementsHandler"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<c:set var="numExpanded" value="${empty content.numExpanded ? 1 : content.numExpanded}"/>
	<c:set var="dimIndex" value="0"/>

	<c:if test="${not empty content.navigation}">
		<c:forEach items="${content.navigation}" var="refinementNavigation">
			<c:if test="${not empty refinementNavigation.refinements}">
				<c:set var="collapsed" value="${dimIndex + 1 >= numExpanded}"/>
				<c:if test="${refinementNavigation.dimensionName eq 'product.category'}">
					<c:set var="categoryRefinements" value="${refinementNavigation.refinements}" scope="request"/>
				</c:if>
				<c:set var="refinements" value="${refinementNavigation.refinements}" scope="request"/>
				<c:choose>
					<c:when test="${catLandingPage}">
						<li>
							<c:forEach items="${refinements}" var="refinement">
								<a href="#" class="collapsed" style="font-size:14px;font-weight:600;"
								   data-event-click-id="cartridges13" data-event-click-param0="${fn:toLowerCase(vsg_utils:getUrlForAction(refinement))}" >
										${refinement.label}
								</a>
							</c:forEach>
						</li>
					</c:when>
					<c:otherwise>
						<dsp:renderContentItem contentItem="${refinementNavigation}">
							<dsp:param name="dimIndex" value="${dimIndex}"/>
							<dsp:param name="collapsed" value="${collapsed}"/>
						</dsp:renderContentItem>
					</c:otherwise>
				</c:choose>
				<c:set var="dimIndex" value="${dimIndex + 1}"/>
			</c:if>
			<c:if test="${refinementNavigation['@type'] eq 'CategoryGuidedNavigation'}">
				<dsp:renderContentItem contentItem="${refinementNavigation}">
				</dsp:renderContentItem>
			</c:if>
			<c:if test="${refinementNavigation['@type'] eq 'SearchGuidedNavigation'}">
				<dsp:renderContentItem contentItem="${refinementNavigation}">
				</dsp:renderContentItem>
			</c:if>
		</c:forEach>

		<dsp:getvalueof var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}"/>
		<dsp:getvalueof var="currentUrlParams" value="${requestScope['javax.servlet.forward.query_string']}"/>
		<c:if test="${not empty currentUrlParams}">
			<dsp:getvalueof var="currentUrl" value="${currentUrl}?${currentUrlParams}"/>
		</c:if>
		
		<input type="hidden" id="add-dimval-ids-single" value="${vsg_utils:crumbsDimValIds(breadcrumbs.refinementCrumbs,false)}"/>
		<input type="hidden" id="add-dimval-ids" value="${vsg_utils:crumbsDimValIds(breadcrumbs.refinementCrumbs,true)}"/>
		<input type="hidden" id="remove-dimval-ids" value=""/>
		<input type="hidden" id="current-url" value="${currentUrl}"/>
		<input type="hidden" id="current-modal" value=""/>
		
		<dsp:form action="#" method="post" id="nav-state-form" formid="nav-state-form">
			<dsp:input value="${currentUrl}" type="hidden" id="currentUrl_hidden" bean="NavStateFromMultipleRefinementsHandler.value.currentUrl"/>
			<dsp:input value="" type="hidden" id="add_dimvals_hidden" bean="NavStateFromMultipleRefinementsHandler.value.addDimValIds"/>
			<dsp:input value="" type="hidden" id="add_dimvals_single_hidden" bean="NavStateFromMultipleRefinementsHandler.value.addDimValIdsSingle"/>
			<dsp:input value="" type="hidden" id="remove_dimvals_hidden" bean="NavStateFromMultipleRefinementsHandler.value.removeDimValIds"/>
			<dsp:input type="hidden" bean="NavStateFromMultipleRefinementsHandler.createNavigationState" value="true" priority="-10"/>
		</dsp:form>
	</c:if>
</dsp:page>
