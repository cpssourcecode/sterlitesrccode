<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	

		<strong>My Account</strong>
		<ul class="list-unstyled mobile-col-2">
			<c:choose>
				<c:when test="${not isTransient}">
					<li><a href="/account/account-landing.jsp">My Account Overview</a></li>
				</c:when>
				<c:otherwise>
					<li><a href="#" data-toggle="modal" data-target="#logIn">My Account Overview</a></li>
				</c:otherwise>
			</c:choose>
		</ul>

</dsp:page>