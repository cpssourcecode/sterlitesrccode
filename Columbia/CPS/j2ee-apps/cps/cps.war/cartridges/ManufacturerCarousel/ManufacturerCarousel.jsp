<dsp:page>
    <dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
    <section class="home-brands hidden-xs hidden-sm">
    <h4 class="bar mb20">Our Manufacturers</h4>
    <div class="carousel">
    	<c:forEach items="${content.content}" var="i">
            <dsp:renderContentItem contentItem="${i}"/>&nbsp; &nbsp;
        </c:forEach>
    </div>
    </section>
</dsp:page>