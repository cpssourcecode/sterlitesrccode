<%--
  This page included by page container tag
--%>
<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" scope="request"/>
	<dsp:getvalueof var="seoTags" bean="/cps/seo/SEOTagHolder.seoTags" scope="request"></dsp:getvalueof>
	<dsp:getvalueof var="productURL" bean="/cps/seo/SEOTagHolder.productURL"></dsp:getvalueof>
	<c:if test="${originatingRequest.contextPath eq '/'}">
		<dsp:setvalue bean="/OriginatingRequest.contextPath" value=""/>
	</c:if>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties" />
	<dsp:importbean bean="/atg/dynamo/droplet/Compare"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/userprofiling/PropertyManager"/>
	<dsp:importbean bean="/atg/multisite/Site"/>
	<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
	<dsp:getvalueof var="title" param="title"/>
	<dsp:getvalueof var="page" param="page" scope="request"/>
	<dsp:getvalueof var="metaKeywords" param="metaKeywords"/>
	<dsp:getvalueof var="metaDescription" param="metaDescription"/>
	<dsp:getvalueof var="favicon" bean="/atg/multisite/Site.favicon" />
	<dsp:getvalueof bean="Profile.transient" var="isTransient"/>
	<dsp:getvalueof var="siteProductionUrl" bean="/atg/multisite/Site.productionURL" />
	<%--<dsp:valueof bean="/atg/multisite/Site.id">No SiteId</dsp:valueof>--%>
	<dsp:getvalueof var="relativeDefaultUrl" value="/home"/>
	<dsp:getvalueof var="relativeActivePageUrl" value="${requestScope['javax.servlet.forward.request_uri']}"/>
	<dsp:getvalueof var="activePageUrl" value="${requestScope['javax.servlet.forward.request_uri']}"/>
	<c:if test="${not fn:containsIgnoreCase(activePageUrl, siteProductionUrl)}">
  	  <dsp:getvalueof var="activePageUrl" value="${siteProductionUrl}${activePageUrl}"></dsp:getvalueof>
	</c:if>
	<dsp:getvalueof var="sessionTimeout" bean="CPSGlobalProperties.sessionTimeoutInMinutes" />
	<c:if test="${empty relativeActivePageUrl }">
		<dsp:getvalueof var="relativeActivePageUrl" value="${originatingRequest.requestURI}"/>
	</c:if>
	
	<!DOCTYPE html>
	<html >
	<head>
		<meta charset="utf-8">
		<dsp:droplet name="/atg/dynamo/droplet/RQLQueryRange">
		<dsp:param name="repository" value="/atg/seo/SEORepository"/>
		<dsp:param name="itemDescriptor" value="SEOTags"/>
	 	<dsp:param name="howMany" value="1"/>
	 	<dsp:param name="pageUrl" value="${not empty relativeActivePageUrl ? relativeActivePageUrl : relativeDefaultUrl }"/>
	 	<dsp:param name="queryRQL" value="key = :pageUrl"/>
		 <dsp:oparam name="output">
	 		<dsp:getvalueof var="seoTitle" param="element.title"/>
	 	 </dsp:oparam>
	 	<dsp:oparam name="empty">
	    	  <dsp:getvalueof var="seoTitle" value=""/>
	   	 </dsp:oparam>
    </dsp:droplet>
	<dsp:droplet name="/atg/dynamo/droplet/RQLQueryRange">
	 <dsp:param name="repository" value="/atg/seo/SEORepository"/>
	 <dsp:param name="itemDescriptor" value="SEOTags"/>
	 <dsp:param name="howMany" value="1"/>
	 <dsp:param name="defaultKey" value="defaultSeoTagValues"/>
	 <dsp:param name="queryRQL" value="key = :defaultKey"/>
	 <dsp:oparam name="output">
	 	<dsp:getvalueof var="default" param="element.title"/>
	 </dsp:oparam>	
	 <dsp:oparam name="empty">
	    	  <dsp:getvalueof var="default" value=""/>
	   </dsp:oparam>
</dsp:droplet>
	<c:choose>
		<c:when test="${not empty seoTitle}">
			<title><dsp:valueof value="${seoTitle}" converter="valueishtml"/> - ${default}</title>
		</c:when>
		<c:when test="${not empty title}">
			<title><dsp:valueof value="${title}" converter="valueishtml"/> -  ${default}</title>
		</c:when>
		<c:otherwise>
			<title> ${default} </title>
		</c:otherwise>
	</c:choose>

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
		<dsp:include page="/global/gadgets/google-analytics.jsp"/>
		<dsp:include page="/global/gadgets/google-tagmanager.jsp"/>
		<dsp:droplet name="/cps/droplet/CPSMetaDataDroplet">
			<dsp:param name="catalogURL" value="${requestScope['javax.servlet.forward.request_uri']}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="newMetaKeywords" param="metaKeywords"/>
				<dsp:getvalueof var="newMetaDescription" param="metaDescription"/>
			</dsp:oparam>
		</dsp:droplet>

		<c:choose>
			<c:when test="${!(metaDescription eq newMetaDescription)}">
		<meta name="description" content="${newMetaDescription}">
			</c:when>
			<c:otherwise>
		<meta name="description" content="${metaDescription}">
			</c:otherwise>
		</c:choose>
		<c:choose>
			<c:when test="${!(metaKeywords eq newMetaKeywords)}">
		<meta name="keywords" content="${newMetaKeywords}">
			</c:when>
			<c:otherwise>
		<meta name="keywords" content="${metaKeywords}">
			</c:otherwise>
		</c:choose>

		
		<%-- CPS-1123 - Always use HTTPS when requesting external font to avoid mixed content errors. --%>

	<%--<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>--%>
		<%--<link href="${originatingRequest.contextPath}/assets/stylesheets/styles${build_version}.css" rel="stylesheet" type="text/css"/>--%>
		<%--<link href="${originatingRequest.contextPath}/assets/stylesheets/common${build_version}.css" rel="stylesheet" type="text/css"/>--%>
	
		<link rel="icon" type="image/ico" href="${originatingRequest.contextPath}${favicon}"/>
		
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link href="${staticContentPrefix}/assets/stylesheets/font-awesome.min${build_version}.css" rel="stylesheet" type="text/css"/>
		<link href="${staticContentPrefix}/assets/stylesheets/datepicker${build_version}.css" rel="stylesheet" type="text/css"/>
		<link href="${staticContentPrefix}/assets/stylesheets/icons${build_version}.css" rel="stylesheet" type="text/css"/>
		<link href="${staticContentPrefix}/assets/stylesheets/base${build_version}.css" rel="stylesheet" type="text/css"/>
		<link href="${staticContentPrefix}/assets/stylesheets/custom${build_version}.css" rel="stylesheet" type="text/css"/>
        <link href="${staticContentPrefix}/assets/stylesheets/bootstrap-datetimepicker${build_version}.css" rel="stylesheet" type="text/css"/>
        <link href="${staticContentPrefix}/assets/javascripts/lightbox-master/dist/ekko-lightbox.min${build_version}.css" rel="stylesheet" type="text/css"/>
        <link href="https://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/jquery.min${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/bootstrap.min${build_version}.js"></script>
        <script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/fuelux/moment${build_version}.js"></script>
        <script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/bootstrap-datetimepicker${build_version}.js"></script>
        <script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/lightbox-master/dist/ekko-lightbox.min${build_version}.js"></script>

		<!--[if lt IE 9]>
		<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<!--[if IE 9]>
		<link href="/assets/stylesheets/ie9${build_version}.css" rel="stylesheet" type="text/css"/>
		<![endif]-->

		<%-- <script type="text/javascript" src="${originatingRequest.contextPath}/js/jquery${build_version}.js"></script> --%>
		<%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--%>

		<%--<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->--%>
		<%--<script src="${staticContentPrefix}/assets/javascripts/jquery-1.11.3.min${build_version}.js"></script>--%>
		<%--<script src="${staticContentPrefix}/assets/javascripts/jquery-migrate-1.2.1.min${build_version}.js"></script>--%>
		<%--<!-- Include all compiled plugins (below), or include individual files as needed -->--%>
		<%--<script src="${staticContentPrefix}/assets/javascripts/bootstrap${build_version}.js"></script>--%>
		<%--<script src="${staticContentPrefix}/assets/javascripts/bootstrap/tooltip${build_version}.js"></script>--%>
		<script src="${staticContentPrefix}/assets/javascripts/bootstrap-select/bootstrap-select${build_version}.js"></script>
		<%--<script src="${staticContentPrefix}/assets/javascripts/bootstrap/popover${build_version}.js"></script>--%>
		<%--<script src="${staticContentPrefix}/assets/javascripts/bootstrap/alert${build_version}.js"></script>--%>

		<%--<script src="${staticContentPrefix}/assets/javascripts/bootstrap/tab${build_version}.js"></script>--%>
		<script src="${staticContentPrefix}/assets/javascripts/bootstrap-notify.min${build_version}.js"></script>


		<!-- // <script src="${staticContentPrefix}/assets/javascripts/bootstrap/modal${build_version}.js"></script> -->
		<!-- // <script src='/assets/javascripts/bootstrap/dropdown${build_version}.js'></script> -->
		<script src="${staticContentPrefix}/assets/javascripts/bootstrap-hover-dropdown.min${build_version}.js"></script>
		<%--<!-- fuel ux -->--%>
		<script src="${staticContentPrefix}/assets/javascripts/fuelux/checkbox${build_version}.js"></script>
		<script src="${staticContentPrefix}/assets/javascripts/fuelux/radio${build_version}.js"></script>
		<%--<!-- owl -->--%>
		<script src='${originatingRequest.contextPath}/assets/javascripts/owl/owl.carousel.min${build_version}.js'></script>
		<%--<!-- custom scroll -->--%>
		<%--<!-- dropdown-enchancements -->--%>
		<script src='${originatingRequest.contextPath}/assets/javascripts/jquery.scrollbar.min${build_version}.js'></script>

		<%--<!-- Placeholders for old browsers -->--%>
		<script src='${originatingRequest.contextPath}/assets/javascripts/jquery-placeholder-gh-pages/jquery.placeholder.min${build_version}.js'></script>

		<%--<script src="${staticContentPrefix}/assets/javascripts/lightbox-master/dist/ekko-lightbox.min${build_version}.js"></script>--%>
		<%--<!-- // <script src='/assets/javascripts/owl/owl.support.modernizr${build_version}.js'></script> -->--%>
		<%--<!-- fileinput -->--%>
		<script src='${staticContentPrefix}/assets/javascripts/bootstrap-fileinput/fileinput.min${build_version}.js'></script>
		<script src='${staticContentPrefix}/assets/javascripts/jasny-bootstrap/fileinput${build_version}.js'></script>
		<script src='${staticContentPrefix}/assets/javascripts/jasny-bootstrap/inputmask${build_version}.js'></script>

		<script src='${staticContentPrefix}/assets/javascripts/jquery.mobile.custom/jquery.mobile.custom.min${build_version}.js'></script>
		<%--<!-- custom -->--%>
		<script src="${staticContentPrefix}/assets/javascripts/prototype${build_version}.js"></script>


		<script type="text/javascript" src="${staticContentPrefix}/js/jquery.form${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/jquery.ui${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/jquery.hoverintent${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/jquery.labels${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/jquery.maskedinput${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/jquery.cycle${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/jquery.quicksearch${build_version}.js"></script>

		<script type="text/javascript" src="${staticContentPrefix}/js/base${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/csr${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/modal${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/common${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/catalog${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/account${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/credit-application${build_version}.js"></script>

		<dsp:droplet name="Compare">
			<dsp:param name="obj1" bean="Profile.securityStatus" converter="number"/>
			<dsp:param name="obj2" bean="PropertyManager.securityStatusCookie" converter="number"/>
			<%--Explicitly logged in or user with a cookie. Redirecting after 31 min of inactivity --%>
			<dsp:oparam name="greaterthan">

				<script type="text/javascript" nonce="${requestScope.nonce}">
					//var sessionTimeout = ${originatingRequest.session.maxInactiveInterval} * 1000 + 60000;
			<%-- 		var sessionTimeout = ${sessionTimeout}*60000+60000;
					var pageLoadedTime = (new Date()).getTime();
					jQuery(document).ready(function() {
//						setTimeout(function() {
// 							timeoutRedirect();
//							handleLogout();
//							window.location.reload();
//						}, sessionTimeout);

						Handle case where idle session timeout happens while browser is out of focus (e.g. mobile)
						Based on http://stackoverflow.com/a/1060034
						if ('hidden' in document) {
							document.addEventListener('visibilitychange', checkSessionTimeoutFocus);
						}
						else if ('mozHidden' in document) {
							document.addEventListener('mozvisibilitychange', checkSessionTimeoutFocus);
						}
						else if ('webkitHidden' in document) {
							document.addEventListener('webkitvisibilitychange', checkSessionTimeoutFocus);
						}
						else if ('msHidden' in document) {
							document.addEventListener('msvisibilitychange', checkSessionTimeoutFocus);
						}
						else if ('onfocusin' in document) {
							document.onfocusin = document.onfocusout = checkSessionTimeoutFocus;
						}
					});

					function checkSessionTimeoutFocus(event) {
						var events = ['focus', 'focusin', 'pageshow', 'visibilitychange'];
						if ($.inArray(event.type, events) > -1) {
							var currentTime = (new Date()).getTime();
							if (currentTime > (pageLoadedTime + sessionTimeout)) {
								if(${isTransient} == false){
									handleSessionTimeout();
								}
							}
						}
					} --%>
/*
					function timeoutRedirect() {
						parent.location.href = "${originatingRequest.contextPath}/account/login.jsp?sesExp=true";
					}
*/
			// fix SM-530 Session expired even if the user is active on site
			var sessionTimeout = ${sessionTimeout}*60000; 
			
			function sessionExpireOrNor(){
			    var lastAccessTime = Number(getCookie('pageLoadedTime'))+1000;
			    var currentTime = Number((new Date()).getTime());
			 
			    if(currentTime > (lastAccessTime+sessionTimeout)){
			        if(${isTransient} == false){
			        	handleSessionTimeout();
					}
			    }
			}
			
			$(document).ready(function(){
			     
			    var pageLoadedTime = (new Date()).getTime();
			    setCookie('pageLoadedTime', pageLoadedTime, 365);
			 
			    setInterval(function () {
			        sessionExpireOrNor()       
			    }, 60000);
			
			
			});


			</script>
			</dsp:oparam>
			<%-- New user or unrecognized member --%>
			<dsp:oparam name="default">
			</dsp:oparam>
		</dsp:droplet>

		<c:if test="${page eq '404'}">
			<%-- <link href="${originatingRequest.contextPath}/css/pdp${build_version}.css" rel="stylesheet" type="text/css" /> --%>
		</c:if>
		<c:if test="${page eq 'home'}">
			<c:set var="bodyClass" value="${page eq 'home' ? 'homepage' : ''}"/>

			<script type="text/javascript" src="${staticContentPrefix}/js/account${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/home${build_version}.js"></script>
			
			<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
			<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
				<dsp:param name="userId" bean="Profile.id"/>
				<dsp:param name="role" value="superAdmin,regularAdmin"/>
				<dsp:oparam name="true">
					<c:if test="${empty selectedOrg}">
						<script type="text/javascript" nonce="${requestScope.nonce}">
							parent.location.href = "${originatingRequest.contextPath}/account/manage-companies.jsp"
						</script>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		
		</c:if>
		<c:if test="${page eq 'catalog'}">
			<script type="text/javascript" nonce="${requestScope.nonce}">
				var pageSeoUrl = window.location.href;
//				$.post("/includes/gadgets/addLastBrowsedURL.jsp", {seoURL: pageSeoUrl});
			</script>
		</c:if>
		<c:if test="${page eq 'csr'}">
			<%-- <link href="${originatingRequest.contextPath}/css/account${build_version}.css" rel="stylesheet" type="text/css" /> --%>

		</c:if>
		<c:if test="${page eq 'account'}">
			<%-- <link href="${originatingRequest.contextPath}/css/account${build_version}.css" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="${staticContentPrefix}/js/account${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/invoice${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'invoice'}">
			<%-- <link href="${originatingRequest.contextPath}/css/account${build_version}.css" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="${staticContentPrefix}/js/invoice${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'mtr'}">
			<%-- <link href="${originatingRequest.contextPath}/css/account${build_version}.css" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="${staticContentPrefix}/js/mtr${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'packing'}">
			<script type="text/javascript" src="${staticContentPrefix}/js/packing${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'account-order'}">
			<script type="text/javascript" src="${staticContentPrefix}/js/order${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'account-approvals'}">
			<script type="text/javascript" src="${staticContentPrefix}/js/approvals${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'reorder'}">
			<%-- <link href="${originatingRequest.contextPath}/css/account${build_version}.css" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="${staticContentPrefix}/js/account${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/reorder${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/checkout${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'content'}">
			<script type="text/javascript" src="${staticContentPrefix}/js/map${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'pa'}">
			<%-- <link href="${originatingRequest.contextPath}/css/checkout${build_version}.css" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="${staticContentPrefix}/js/pa${build_version}.js"></script>
		</c:if>
		<c:if test="${page eq 'pdp'}">
			<c:set var="bodyClass" value="pdp ontop"/>
		</c:if>
			<script src="${staticContentPrefix}/assets/javascripts/cycle${build_version}.js"></script>
		<script type="text/javascript" src="${staticContentPrefix}/js/pdp${build_version}.js"></script>

		<c:if test="${page eq 'quickorder'}">
			<%-- <link href="${originatingRequest.contextPath}/css/checkout${build_version}.css" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="${staticContentPrefix}/js/quickorder${build_version}.js"></script>
		</c:if>

		<c:if test="${page eq 'checkout'}">
			<script type="text/javascript" src="${staticContentPrefix}/js/jquery.ui${build_version}.js"></script>

			<%-- <link href="${originatingRequest.contextPath}/css/checkout${build_version}.css" rel="stylesheet" type="text/css" /> --%>
			<script type="text/javascript" src="${staticContentPrefix}/js/checkout${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/date${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/jquery.datePicker${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/jquery.datePickerMultiMonth${build_version}.js"></script>
			<script type="text/javascript" src="${staticContentPrefix}/js/jquery.dateFormat${build_version}.js"></script>
		</c:if>

		<script src="https://www.google.com/recaptcha/api.js?onload=renderCaptchaCallback&render=explicit" async defer></script>

		<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
			<dsp:param name="userId" bean="Profile.id"/>
			<dsp:param name="role" value="superAdmin,regularAdmin"/>
			<dsp:oparam name="true">
				<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
				<c:if test="${not empty selectedOrg}">
					<c:set var="bodyClass" value="${bodyClass} admin"/>
				</c:if>
			</dsp:oparam>
		</dsp:droplet>
	</head>
	<body class="${bodyClass}">
	<dsp:getvalueof var="gtmId" bean="Site.googleTagManagerId"/>
		<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=${gtmId}"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager -->

		<dsp:getvalueof var="accessCountBeforeModalShowed" bean="Site.accessCountBeforeModalShowed" />
		<dsp:getvalueof var="accessCountInterval" bean="Site.accessCountInterval" />
		<script nonce="${requestScope.nonce}">
			window.siteProperties = {
				accessCountBeforeModalShowed : ${empty accessCountBeforeModalShowed ? 10 : accessCountBeforeModalShowed},
				accessCountInterval : ${empty accessCountInterval ? 15 : accessCountInterval}
			};
		</script>
		<dsp:getvalueof var="welcomeModal" bean="/atg/multisite/Site.welcomeModal" />
		<dsp:getvalueof var="welcomeRegisterModal" bean="/atg/multisite/Site.welcomeRegisterModal" />
		<input type="hidden" id="welcomeModalValue"  value="${welcomeModal}"/>
		<input type="hidden" id="welcomeRegisterModalValue"  value="${welcomeRegisterModal}"/>
	<c:choose>
		<c:when test="${isTransient}">
			<script nonce="${requestScope.nonce}">
			/* fix SM-555 Allow popups to be disabled in BCC  */
			var welcomeRegisterModalValue = $('#welcomeRegisterModalValue').val();
			$(document).ready(function(){
				if(welcomeRegisterModalValue == 'true'){
				updateGuestLastAccessCookie();
				}
				// session timeout for anonymous user
				var sessionTimeout = '${sessionTimeout}'*60000;
				setTimeout(function () {
					window.location.reload();
				}, sessionTimeout); // 30 minutes - 1800000
			});
				
			</script>
		</c:when>
		<c:otherwise>
			<script nonce="${requestScope.nonce}">
				/* $(document).ready(function(){
					//session timeout for login user.
					setTimeout(function () {
						showIdleSessionPopup()
					}, '${sessionTimeout}'*60000); // 30 minutes - 1800000
					// closing notification modal and reloading the page.
// 					setTimeout(function () {
// 						window.location.reload();
// 					}, '${sessionTimeout}'*60000+60000); // 31 minutes - 1860000
				}); */
			</script>
		</c:otherwise>
	</c:choose>
		<%-- SVG Include Removed - Now added via AJAX --%>
		<%-- <%@ include file="/assets/images/subway-icons/icons-svgdefs.svg" %> --%>
		<%-- Temporary password check --%>
		<dsp:getvalueof var="ln" param="ln"/>
		<dsp:getvalueof var="pr" param="pr"/>
		<dsp:getvalueof var="dt" param="dt"/>
		<dsp:getvalueof var="tp" param="tp"/>
		<c:if test="${not empty ln && not empty pr && not empty dt}">
			<dsp:include page="/includes/gadgets/new-user-login.jsp?ln=${ln}&pr=${pr}&dt=${dt}&tp=${tp}"/>
		</c:if>
		<script nonce="${requestScope.nonce}">
            $(document).ready(function() {
                if (!Object.prototype.watch) {
                    Object.defineProperty(Object.prototype, "watch", {
                        enumerable: false
                        , configurable: true
                        , writable: false
                        , value: function (prop, handler) {
                            var
                                oldval = this[prop]
                                , newval = oldval
                                , getter = function () {
                                    return newval;
                                }
                                , setter = function (val) {
                                    oldval = newval;
                                    return newval = handler.call(this, prop, oldval, val);
                                }
                            ;

                            if (delete this[prop]) { // can't watch constants
                                Object.defineProperty(this, prop, {
                                    get: getter
                                    , set: setter
                                    , enumerable: true
                                    , configurable: true
                                });
                            }
                        }
                    });
                }

// object.unwatch
                if (!Object.prototype.unwatch) {
                    Object.defineProperty(Object.prototype, "unwatch", {
                        enumerable: false
                        , configurable: true
                        , writable: false
                        , value: function (prop) {
                            var val = this[prop];
                            delete this[prop]; // remove accessors
                            this[prop] = val;
                        }
                    });
                }
                function J(a) {
                        try {
                            return typeof a === "string" && decodeURI(a) !== a
                        } catch (b) {
                            return false
                        }
                    }

                    function a(a) {
                        return typeof a === "undefined" || null === a || a === ""
                    }
                window.watch('_elq', function (id, oldval, newval) {
                    setTimeout(function(){
                      	try{
                        	window._elq.trackOutboundLink = function (b, f, g) {
                                window._elq.trackEvent(b.href, f, g);
                        		var d = J(b.href) ? b.href : encodeURI(b.href), c, e = b.getAttribute("target");
                        		if (a(e)) {
                                    setTimeout(function () {
                                        document.location = d;
                                    }, 1e3);
                                }else{
                            		setTimeout(function () {
                                		window.open(d, encodeURIComponent(e));
                            		}, 1e3);
                        		}
                    		};
                       }catch(e){
                            console.error(e);
                      	}
					},1e3);
                    return newval;
                });
            });
        	function showIdleSessionPopup() {
        			handleSessionTimeout();
        	  	}
		</script>
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-session-idle.jsp"/>
		<dsp:getvalueof var="expiryDays" bean="CPSGlobalProperties.welcomeModalExpiry"/>
		<input type="hidden" id="expireModal"  value="${expiryDays}"/>
		<input type="hidden" id="isTransient"  value="${isTransient}"/>
		<dsp:include page="/global/modals/welcome-modal.jsp"/>
		
</dsp:page>	