<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/csr/util/CPSCSRSessionBean"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/endeca/CPSSearchFormHandler"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties" />
	<%-- <dsp:droplet name="/cps/droplet/RoleLookupDroplet">
		<dsp:param name="userId" bean="Profile.id"/>
		<dsp:param name="role" value="superAdmin"/>
		<dsp:oparam name="true">
			<dsp:include page="/csr/header.jsp"/>
		</dsp:oparam>
	</dsp:droplet> --%>

	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:getvalueof var="securityStatus" bean="Profile.securityStatus"/>
	<dsp:getvalueof var="transient" value="${isTransient or securityStatus eq 1}"/>
	<dsp:getvalueof var="sessionTimeout" bean="CPSGlobalProperties.sessionTimeoutInMinutes" />
	
			
	
		<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
		<c:set var="contentCollection" value="/content/Shared/Headers" />
		<dsp:droplet name="InvokeAssembler">
			<dsp:param name="contentCollection" value="${contentCollection}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
				<dsp:renderContentItem contentItem="${content}" />
			</dsp:oparam>
		</dsp:droplet>
	
</dsp:page>


<script type="text/javascript" nonce="${requestScope.nonce}">

	$(document).ready(function () {
	   
		$("#itemNumPH0").blur(function () {
			if ($(this).val() == "") {
				$(this).val('Item #');
			}
		}).focus(function () {
			if ($(this).val() == "Item #") {
				$(this).val("");
			}
		})

		$("#qtyPH0").blur(function () {
			if ($(this).val() == "") {
				$(this).unmask();
				$(this).val('Qty');
			}
		}).focus(function () {
			if ($(this).val() == "Qty") {

				$(this).mask("9?99");
				$(this).val("");
			}
		})

		for (i = 1; i < 5; i++) {

			$("#itemNumPH" + i).blur(function () {
				$(this).val($(this).val());
			})

		}

		
	});



	function validateQtys() {
		for (i = 0; i < 5; i++) {
			if ($("#itemNumPH" + i).val() != "Item #") {
				$('#qtyPH' + i).val($.trim($('#qtyPH' + i).val()));
				$('#itemNumPH' + i).val($.trim($('#itemNumPH' + i).val()));
				if (($("#qtyPH" + i).val() == "" || $("#qtyPH" + i).val() == "Qty")
						&& ($("#itemNumPH" + i).val() != "")) {
					$("#qtyPH" + i).val('1')
				} else if ($("#itemNumPH" + i).val() == ""
						|| $("#itemNumPH" + i).val() == "Item #") {
					$("#qtyPH" + i).val('0')
				}
			} else {
				$("#qtyPH" + i).val('0')
			}
		}
	}

	function clearForm() {
		for (i = 0; i < 5; i++) {
			$("#qtyPH" + i).val('')
			$("#itemNumPH" + i).val('')
		}
	}

	function gatherSkusAndQtys() {
		itemIds = [];
		qtys = [];
		count = 0;
		for (i = 0; i < 5; i++) {
			if ($("#itemNumPH" + i).val() != ""
					&& $("#itemNumPH" + i).val() != "Item #") {
				itemIds[count] = $("#itemNumPH" + i).val();
				qtys[count++] = $("#qtyPH" + i).val();
			}
		}
		return [itemIds, qtys];
	}

	function submitWithAddAction(skusAndQtys) {
		$("#addSkusHead").val(skusAndQtys[0]);
		$("#addQtysHead").val(skusAndQtys[1]);
		$("#addToCartQuickOrderHeader").submit();
	}

</script>
