<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>

    <a href="${originatingRequest.contextPath}/checkout/cart.jsp">
        <i class="gold glyphicon glyphicon-shopping-cart"></i>
        <dsp:getvalueof var="commerceItems" bean="ShoppingCart.current.commerceItems"/>
            ${fn:length(commerceItems)} items
    </a>


</dsp:page>