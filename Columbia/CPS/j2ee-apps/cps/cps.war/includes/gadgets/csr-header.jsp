<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
	<dsp:tomap var="org" bean="Profile.CSRSelectedOrg"/>

	<div id="adminbar">
		<div class="row">
			<div class="col-sm-10"><i class="fa fa-exclamation-triangle"></i> You are accessing Customer Account:
				<span class="ml5"><dsp:valueof value="${org.name}"/></span>
				<span class="ml5">Account #
					<c:choose>
						<c:when test="${not empty org.billingAddress}">
							<dsp:tomap var="billingAddress" bean="Profile.CSRSelectedOrg.billingAddress"/>
							<dsp:valueof value="${billingAddress.jdeAddressNumber}"/>
							<c:if test="${empty billingAddress.jdeAddressNumber}">
								<dsp:valueof value="${org.id}"/>
							</c:if>
						</c:when>
						<c:otherwise>
							<dsp:valueof value="${org.id}"/>
						</c:otherwise>
					</c:choose> 
				</span>
			</div>
			<div class="col-sm-2 text-right text-center-xs"><button data-event-click-id="includes0"  class="btn btn-info btn-sm xs-block">Stop Access</button></div>
			<dsp:form id="stopAccess" method="post" action="/account/manage-companies.jsp">
				<dsp:input type="hidden" bean="MultiUserAddFormHandler.stopAccess" value="true"/>
			</dsp:form>
		</div>
	</div>

	<script type="text/javascript" nonce="${requestScope.nonce}">
		function stopAccess(){
			$("#stopAccess").submit();
		}
	</script>
</dsp:page>