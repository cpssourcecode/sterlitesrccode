<dsp:page>
<nav class="navbar navbar-default yamm" role="navigation">
			<div class="container">
				<ul class="nav navbar-nav navbar-mainmenu">
					<li class="mainmenu-item dropdown yamm-fw"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <span>Products</span>
							<span class="caret"></span>
					</a>
						<ul class="dropdown-menu dropdown-large" role="menu">
							<li>
								<div class="row">
									<div class="col-lg-12">
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
												<ul class="nav nav-pills nav-stacked" id="mainTabs"
													role="tablist">
													<li role="presentation" class="active"><a href="#home"
														role="tab" data-toggle="tab">Steel Pipe, Fittings and
															Flanges</a></li>
													<li role="presentation"><a href="#profile" role="tab"
														data-toggle="tab">Stainless Steel Pipe</a></li>
													<li role="presentation"><a href="#messages" role="tab"
														data-toggle="tab">Tube, Fittings, and Flanges</a></li>
													<li role="presentation"><a href="#settings" role="tab"
														data-toggle="tab">Copper Tube and Fittings</a></li>
												</ul>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-9 col-xs-8">
												<div class="tab-content">
													<div role="tabpanel" class="tab-pane fade in active"
														id="home">
														<h3>Steel Pipe, Fittings and Flanges</h3>
														<div class="row maincontent-row">
															<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
																<a href="#" class="cart-tile"> <img
																	class="img-responsive"
																	src="content_images/img_placeholder.jpg" alt="">
																	<span>Lorem ipsum dolor sit amet, consectetur
																		adipisicing elit.</span>
																</a>
															</div>
															<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
																<a href="#" class="cart-tile"> <img
																	class="img-responsive"
																	src="content_images/img_placeholder.jpg" alt="">
																	<span>Lorem ipsum dolor sit amet, consectetur
																		adipisicing elit.</span>
																</a>
															</div>
															<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
																<a href="#" class="cart-tile"> <img
																	class="img-responsive"
																	src="content_images/img_placeholder.jpg" alt="">
																	<span>Lorem ipsum dolor sit amet, consectetur
																		adipisicing elit.</span>
																</a>
															</div>
															<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
																<a href="#" class="cart-tile"> <img
																	class="img-responsive"
																	src="content_images/img_placeholder.jpg" alt="">
																	<span>Lorem ipsum dolor sit amet, consectetur
																		adipisicing elit.</span>
																</a>
															</div>
															<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
																<a href="#" class="cart-tile"> <img
																	class="img-responsive"
																	src="content_images/img_placeholder.jpg" alt="">
																	<span>Lorem ipsum dolor sit amet, consectetur
																		adipisicing elit.</span>
																</a>
															</div>
															<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
																<a href="#" class="cart-tile"> <img
																	class="img-responsive"
																	src="content_images/img_placeholder.jpg" alt="">
																	<span>Lorem ipsum dolor sit amet, consectetur
																		adipisicing elit.</span>
																</a>
															</div>
														</div>
													</div>
													<div role="tabpanel" class="tab-pane fade" id="profile">
														<div class="row maincontent-row">
															<div class="col-lg-12">
																<h3>Stainless Steel Pipe</h3>
																<div class="row">
																	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
																		<ul class="list-unstyled">
																			<li><a href="">Lorem ipsum dolor.</a></li>
																			<li><a href="">Pariatur, nihil, illum.</a></li>
																			<li><a href="">Consequuntur, commodi,
																					excepturi.</a></li>
																			<li><a href="">Porro, ab, cum.</a></li>
																			<li><a href="">Repudiandae, dignissimos,
																					amet.</a></li>
																			<li><a href="">Officiis, vel, aspernatur.</a></li>
																			<li><a href="">Dolorum, libero, consequatur?</a>
																			</li>
																			<li><a href="">Consectetur, fuga placeat.</a></li>
																			<li><a href="">Laudantium, aut, et!</a></li>
																			<li><a href="">Omnis, eveniet, animi.</a></li>
																		</ul>
																	</div>
																	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
																		<ul class="list-unstyled">
																			<li><a href="">Lorem ipsum dolor sit.</a></li>
																			<li><a href="">Eveniet tempore, dolorum
																					ratione.</a></li>
																			<li><a href="">Possimus iure, labore iste.</a></li>
																			<li><a href="">Provident sed et at.</a></li>
																			<li><a href="">Placeat nulla eum amet.</a></li>
																			<li><a href="">Rem, deleniti accusamus
																					voluptatum?</a></li>
																			<li><a href="">Quasi ullam suscipit, sint?</a></li>
																			<li><a href="">Vitae blanditiis ad rem!</a></li>
																			<li><a href="">Impedit sit nisi dolores.</a></li>
																			<li><a href="">Quia iure quam maxime.</a></li>
																		</ul>
																	</div>
																	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
																		<ul class="list-unstyled">
																			<li><a href="">Lorem ipsum dolor sit.</a></li>
																			<li><a href="">Eveniet tempore, dolorum
																					ratione.</a></li>
																			<li><a href="">Possimus iure, labore iste.</a></li>
																			<li><a href="">Provident sed et at.</a></li>
																			<li><a href="">Placeat nulla eum amet.</a></li>
																			<li><a href="">Rem, deleniti accusamus
																					voluptatum?</a></li>
																			<li><a href="">Quasi ullam suscipit, sint?</a></li>
																			<li><a href="">Vitae blanditiis ad rem!</a></li>
																			<li><a href="">Impedit sit nisi dolores.</a></li>
																			<li><a href="">Quia iure quam maxime.</a></li>
																		</ul>
																	</div>
																</div>

															</div>
														</div>
													</div>
													<div role="tabpanel" class="tab-pane fade" id="messages">
														<h3>Tube, Fittings, and Flanges</h3>
													</div>
													<div role="tabpanel" class="tab-pane fade" id="settings">
														<h3>Copper Tube and Fittings</h3>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul></li>
					<li class="mainmenu-item dropdown yamm-fw"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <span>Services</span>
							<span class="caret"></span>
					</a>
						<ul class="dropdown-menu dropdown-large" role="menu">
							<li>
								<div class="row">
									<div class="col-lg-12">
										<a href="#"
											class="col-lg-3 col-md-3 col-sm-4 col-xs-6 well well-tile-link">
											<h4>After Sale Service</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing
												elit.</p>
										</a> <a href="#"
											class="col-lg-3 col-md-3 col-sm-4 col-xs-6 well well-tile-link">
											<h4>After Sale Service</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing
												elit. Dolor qui maxime, facere id quidem. Vero quisquam est
												odio quis, omnis ullam eveniet suscipit commodi, porro ea
												corporis tenetur numquam laudantium.</p>
										</a> <a href="#"
											class="col-lg-3 col-md-3 col-sm-4 col-xs-6 well well-tile-link">
											<h4>After Sale Service</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing
												elit. Dolor qui maxime, facere id quidem.</p>
										</a> <a href="#"
											class="col-lg-3 col-md-3 col-sm-4 col-xs-6 well well-tile-link">
											<h4>After Sale Service</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing
												elit. Dolor qui maxime, facere id quidem. Vero quisquam est
												odio quis, omnis ullam eveniet suscipit commodi, porro ea
												corporis tenetur numquam laudantium.</p>
										</a> <a href="#"
											class="col-lg-3 col-md-3 col-sm-4 col-xs-6 well well-tile-link">
											<h4>After Sale Service</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing
												elit. Dolor qui maxime, facere id quidem. Vero quisquam est
												odio quis, omnis ullam eveniet suscipit commodi, porro ea
												corporis tenetur numquam laudantium.</p>
										</a> <a href="#"
											class="col-lg-3 col-md-3 col-sm-4 col-xs-6 well well-tile-link">
											<h4>After Sale Service</h4>
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing
												elit. Dolor qui maxime, facere id quidem. Vero quisquam est
												odio quis, omnis ullam eveniet suscipit commodi, porro ea
												corporis tenetur numquam laudantium.</p>
										</a>
									</div>
								</div>
							</li>
						</ul></li>
					<li class="mainmenu-item dropdown yamm"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <span>Tools</span>
							<span class="caret"></span>
					</a>
						<ul class="dropdown-menu dropdown-large dropdown-simple"
							role="menu">
							<li><a href="#">Price & Availability Check (4)</a></li>
							<li><a href="#">Availability</a></li>
							<li><a href="#">List item 1</a></li>
							<li><a href="#">List item 2</a></li>
						</ul></li>
					<li class="mainmenu-item"><a href="#"> <span>Locations</span>
					</a></li>
				</ul>
				<form class="navbar-form navbar-right" role="search">
					<div class="input-group">
						<input type="text" class="form-control"
							placeholder="Search by Keyword or item #"> <span
							class="input-group-btn">
							<button class="btn btn-default" type="button">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
					<!-- /input-group -->
				</form>
			</div>
		</nav>

</dsp:page>
