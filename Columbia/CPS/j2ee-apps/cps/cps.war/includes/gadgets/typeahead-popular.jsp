<dsp:page>
	<dsp:getvalueof var="item" param="item" />
	<dsp:getvalueof var="ntt" param="ntt" />
	<dsp:getvalueof bean='/cps/util/CPSGlobalProperties.maxEndecaSuggestions' var='maxEndecaSuggestions' />

	<c:if test="${not empty item && not empty item.dimensionSearchGroups}">
        <span class="ta-heading">
			<h4 class="text-bwtn-line">
				<span>
				Suggestions
				</span>
			</h4>
		</span>
		<span id='ta-suggession'  class='sugg1'>
		<ul class='sugg2'>
		 
		 <dsp:droplet name='/cps/droplet/FetchUniqueEndecaSuggestionsDroplet'>
		 <dsp:param name='contentItem' value='${item}'/>
		 <dsp:oparam name='output'>
		 <dsp:getvalueof var='filteredEndecaDimensions' param='filteredEndecaDimensions'/>
		 </dsp:oparam>
		 </dsp:droplet>			
			<c:forEach var="filteredDimension" items="${filteredEndecaDimensions}" varStatus="recordCount"  end="${maxEndecaSuggestions-1}">
			<c:set var="label" value="${vsg_utils:boldText(filteredDimension,ntt)}"/>
					  <li class="suggession-${recordCount.count} focusable"   data-value='${filteredDimension}'><a  class='sugg3' href="/search?Ntt=<c:out value='${filteredDimension}'/>"> ${label}</a></li>
					  <div class="div-suggession-${recordCount.count} hidden"  data-status='unpopulated'></div>
			</c:forEach>
			
		</ul>
</span>
	</c:if>
</dsp:page>