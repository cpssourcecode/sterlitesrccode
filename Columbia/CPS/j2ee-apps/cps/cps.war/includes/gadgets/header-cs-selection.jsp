<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>
	<span class="hidden-xs text">| </span> <span class="text">Ship To:</span>
	<div class="btn-group login-box-select">
		<a data-toggle="dropdown" class="btn btn-default dropdown-toggle">
			<span class="text"><dsp:valueof bean="Profile.selectedCS.address1"/></span> 
			<span class="caret"></span>
		</a>
		<ul class="dropdown-menu header-ship">
			<button class="close"><i class="fa fa-close"></i></button>
			<li class="current">
				<dsp:getvalueof var="city" bean="Profile.selectedCS.city"/>
				<dsp:getvalueof var="state" bean="Profile.selectedCS.state"/>
				<dsp:getvalueof var="zip" bean="Profile.selectedCS.postalCode"/>
				<span>Current Ship To Address</span>
				<p>
					<span class="addr-title">
						<dsp:valueof bean="Profile.selectedCS.companyName"/>
					</span>
					<span class="addr1">
						<dsp:valueof bean="Profile.selectedCS.address1"/>
					</span>
					<span class="addr2">
						${city},&nbsp;${state}&nbsp;${zip}
					</span>
				</p>
			</li>
			<li class="divider"></li>
			<li class="current">
				<span>Select New Ship To Address for this Session</span>
			</li>
			<dsp:droplet name="/cps/droplet/CSSelectionDroplet">
				<dsp:oparam name="output">
					<dsp:getvalueof var="associatedCS" param="associatedCS"/>
					<c:choose>
						<c:when test="${not empty associatedCS}">
							<dsp:getvalueof var="currentCS" bean="Profile.selectedCS.id"/>
							<dsp:droplet name="/atg/dynamo/droplet/ForEach">
								<dsp:param name="array" param="associatedCS"/>
								<dsp:param name="elementName" value="cs"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="address1" param="cs.address1"/>
									<dsp:getvalueof var="csId" param="cs.id"/>
									<dsp:getvalueof var="count" param="count"/>
									<dsp:getvalueof var="address2" param="cs.address2"/>
									<c:if test="${count <=5}">
										<li value="${csId}" data-event-click-id="includes1" >
											<label>
												<span class="addr-title"><dsp:valueof param="cs.companyName"/></span>
												<span class="addr1"><dsp:valueof param="cs.address1"/></span>
												<c:if test="${not empty address2}">
													<span class="addr2"><dsp:valueof param="cs.address2"/></span>
												</c:if>
		 										<span class="addr3">
		 											<dsp:valueof param="cs.city"/>,&nbsp;<dsp:valueof param="cs.state"/>&nbsp;<dsp:valueof param="cs.postalCode"/>
		 										</span>
											</label>
										</li>
									</c:if>
								</dsp:oparam>
								<dsp:oparam name="outputEnd">
									<dsp:getvalueof var="size" param="size"/>
									<c:if test="${size >= 6}">
										<li value="${csId}" data-event-click-id="includes2" >
											<label>
												<span class="addr-title">View All</span>
											</label>
										</li>
									</c:if>
								</dsp:oparam>
							</dsp:droplet>
						</c:when>
					</c:choose>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:form method="post" id="header-cs-form">
				<dsp:input type="hidden" value="" id="selected_shipping_id_header"
							bean="ProfileDefaultShippingAddressFormHandler.selectedId"/>
				<dsp:input type="hidden" value="true"
							bean="ProfileDefaultShippingAddressFormHandler.saveProfileShippingAddress"/>
			</dsp:form>
		</ul>
	</div>
</dsp:page>