<dsp:page>
	<dsp:getvalueof var="category" param="category"/>
	<dsp:getvalueof var="displayName" param="category.displayName" />
	<dsp:getvalueof var="categoryURL" param="categoryURL" />
	
	<c:if test="${!empty category}">
		<li><a href="${vsg_utils:escapeHtml(categoryURL)}">${vsg_utils:formatName(vsg_utils:escapeHtml(displayName))}</a></li>
	</c:if>
</dsp:page>