<%@page import="atg.servlet.ServletUtil"%>
<dsp:page>
<dsp:importbean bean='/cps/endeca/droplet/FetchDimensionIdsDroplet' />
	<dsp:getvalueof var="item" param="item" />
	<dsp:getvalueof var="ntt" param="ntt" />

	<c:if test="${not empty item && not empty item.dimensionSearchGroups}" >
        <span class="ta-heading">
		    <h4 class="text-bwtn-line">
			    <span>Brands</span>
		    </h4>
        </span>
		<span id='ta-brands' >
		<ul>
			<c:forEach var="dimensionGroups"  
				items="${item.dimensionSearchGroups}">
				<c:forEach var="dim" varStatus="recordCount" 
					items="${dimensionGroups.dimensionSearchValues}">
					<c:set var='dgraphSpec' value='${dim.label}'/>
 					
 					<dsp:droplet name='FetchDimensionIdsDroplet'>
 					<dsp:param name='dgraphSpec' value='${dgraphSpec}'/>
 					</dsp:droplet>
		 
					<li class="brands-${recordCount.count} focusable"><a href="${vsg_utils:formatLink(dim.navigationState)}">${vsg_utils:boldText(dim.label,ntt)}</a></li>
				</c:forEach>
			</c:forEach>
		</ul>
</span>
	</c:if>
</dsp:page>