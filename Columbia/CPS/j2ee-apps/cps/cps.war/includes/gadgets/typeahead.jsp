<dsp:page>

	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:droplet name="InvokeAssembler">
		<dsp:param name="contentCollection"
			value="/content/Shared/Auto-Suggest Panels" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="content"
							vartype="com.endeca.infront.assembler.ContentItem"
							param="contentItem" />
		</dsp:oparam>
	</dsp:droplet>
	
	<c:if test="${not empty content.contents}">
		<c:set var="autoSuggestPanel" value="${content.contents[0]}" />	 
		<c:forEach var="item" items="${autoSuggestPanel.autoSuggest}">
			<c:set var="recordsType" value="${item.recordsType}" />
			<c:choose>
				<c:when test="${'product' eq recordsType}">
					<c:set var="products" value="${item}"/>
				</c:when>				
				<c:when test="${item['@type'] eq 'DimensionSearchAutoSuggestItem'}">				
					<c:if test="${not empty item.dimensionSearchGroups}">					
					<c:choose>
						<c:when test="${item.dimensionSearchGroups[0].dimensionName eq 'product.category' }">
							<c:set var="categories" value="${item}"/>							
						</c:when>
						<c:when test="${item.dimensionSearchGroups[0].dimensionName eq 'product.mfr_fullname' }">						
							<c:set var="brands" value="${item}"/>							
						</c:when>
						<c:otherwise>						
							<c:set var="populars" value="${item}"/>									 
				</c:otherwise>
						</c:choose>
					</c:if>
				</c:when>
			</c:choose>
		</c:forEach>
	</c:if>

<c:choose>
<c:when test="${ not empty populars and not empty populars.dimensionSearchGroups 
		or not empty categories and not empty categories.dimensionSearchGroups 
		or not empty brands and not empty brands.dimensionSearchGroups}">
		<div class="row row-flush">
			<dsp:getvalueof var="ntt" param="Ntt"/>
			<c:if test="${not empty content.contents}">
	
				<div class="col-sm-5 ta-results">
					<dsp:include src="/includes/gadgets/typeahead-popular.jsp" >
						<dsp:param name="item" value="${populars}" />
						<dsp:param name="ntt" value="${ntt}" />
					</dsp:include>
					<dsp:include src="/includes/gadgets/typeahead-category.jsp" >
						<dsp:param name="item" value="${categories}" />
						<dsp:param name="ntt" value="${ntt}" />
					</dsp:include>
					<dsp:include src="/includes/gadgets/typeahead-brands.jsp" >
						<dsp:param name="item" value="${brands}" />
						<dsp:param name="ntt" value="${ntt}" />
					</dsp:include>
				</div>
				<div class="col-sm-7" id="search-result-cont">
				    
				</div>
			<%-- 
				<div class="col-sm-9 ta-products hidden-xs">
					<c:if test="${not empty mainCategory}">
						<div class="row row-flush ta-products-header">
							<div class="col-xs-7">
								<h3 class="text-upper">${mainCategory.label}</h3>
							</div>
							<div class="col-xs-5 text-right pt15"><a href="${vsg_utils:formatLink(mainCategory.navigationState)}" class="btn btn-primary">Shop All</a>
							</div>
						</div>
					</c:if>
					<dsp:include src="/includes/gadgets/typeahead-products.jsp">
						<dsp:param name="item" value="${products}" />
						<dsp:param name="ntt" value="${ntt}" />
					</dsp:include>
				</div>
			--%>
	
			</c:if>
		</div>
		
		</c:when>
		<c:otherwise>		
		</c:otherwise>
</c:choose>
	
</dsp:page>