
<dsp:page>
    <dsp:getvalueof var="item" param="item"/>
    <dsp:getvalueof var="ntt" param="ntt"/>

    <c:choose>
        <c:when test="${not empty item && not empty item.dimensionSearchGroups}">
        <span class="ta-heading">
			<h4 class="text-bwtn-line">
				<span>Related Categories</span>
			</h4>
        </span>
            <span id='ta-categories' >
            <ul>
                <c:forEach var="dimensionGroups" items="${item.dimensionSearchGroups}" >
                    <c:if test="${not empty dimensionGroups.dimensionSearchValues}">
                        <c:set var="mainCategory" value="${dimensionGroups.dimensionSearchValues[0]}" scope="request"/>
                    </c:if>
                    <c:forEach var="dim" items="${dimensionGroups.dimensionSearchValues}" varStatus="recordCount">
                        <li class="categories-${recordCount.count} focusable"><a href="${vsg_utils:formatLink(dim.navigationState)}">${vsg_utils:boldText(dim.label,ntt)}</a> 			
			<c:if test="${fn:length(dim.ancestors) >=1}">
			<span class='gray'>- in ${dim.ancestors[0].label}</span>
			</c:if>

			</li>
                    </c:forEach>
                </c:forEach>
            </ul>
            </span>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>
</dsp:page>