<dsp:page> 
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" var='shoppingCart' />
	<dsp:importbean bean="/cps/util/CPSGlobalProperties" />
	
	<dsp:getvalueof var="shippingAddress" bean="Profile.derivedShippingAddress.address1" />
	<dsp:getvalueof var="showOrderShippingAddressForSession" bean="CPSGlobalProperties.showOrderShippingAddressForSession" />
							<c:if test="${shoppingCart.current.inStorePickupOrder=='false'}">
								<dsp:getvalueof var="orderShippingAddress"
									bean="ShoppingCart.current.shippingGroups[0].shippingAddress" />
							</c:if>
							
							<c:choose>
								<c:when test='${showOrderShippingAddressForSession}'>
								<dsp:getvalueof var="enableDefaultShipTo" bean="CPSGlobalProperties.enableDefaultShipTo" />
								<!-- showing order shippingAddress for Session -->									
									<a href="#" data-dismiss="modal"   data-val1='${orderShippingAddress}'
										data-toggle="modal" data-target="#selectShipAddress"> <span
											class="top-label">Session Ship To:</span> <c:choose>
												<c:when
													test="${shoppingCart.current.inStorePickupOrder=='false' && not empty orderShippingAddress.address1 && !enableDefaultShipTo}">
													<dsp:valueof
														bean="ShoppingCart.current.shippingGroups[0].shippingAddress.address1" />
													<i class="fa fa-angle-down"></i>
												</c:when>
												<c:when test="${!empty shippingAddress}">
													<dsp:getvalueof var="profileSelectedCS" bean="Profile.selectedCS.address1" />${profileSelectedCS}
													<c:if test="${empty profileSelectedCS}">
														<dsp:valueof bean="Profile.shippingAddress.address1" />
													</c:if>
													<i class="fa fa-angle-down"></i>
												</c:when>
											</c:choose>
									</a>									 
								</c:when>
								<c:otherwise>		
																<!-- showing  profile selectedCS for Session -->							 
									<a href="#" data-dismiss="modal"
										data-toggle="modal" data-target="#selectShipAddress"> 
										<span
											class="top-label">Session Ship To:</span> 
											<c:if
												test="${! empty shippingAddress}">
												<dsp:valueof bean="Profile.selectedCS.address1" /> 
												<i class="fa fa-angle-down"></i>
											</c:if>
									</a>
									</li>
								</c:otherwise>
							</c:choose>

	



</dsp:page>