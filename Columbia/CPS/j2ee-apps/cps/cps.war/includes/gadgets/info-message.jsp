<dsp:page>
	<dsp:getvalueof var="notWrap" param="notWrap"/>
	<dsp:getvalueof var="key" param="key"/>
	<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
		<dsp:param name="id" value="${key}" />
		<dsp:param name="elementName" value="message"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="messageType" param="message.messageType" />

			<%--
				<option value="ERROR" code="0"/>
				<option value="SUCCESS" code="1"/>
				<option value="INFO" code="2"/>
			--%>

			<c:choose>
				<c:when test="${notWrap=='true'}">
					<dsp:valueof param="message.message" valueishtml="true"/>
				</c:when>
				<c:otherwise>
					<c:set var="messageTypeClass" value="" />
					<c:choose>
						<c:when test="${messageType eq '0'}">
							<c:set var="messageTypeClass" value="alert-danger" />
						</c:when>
						<c:when test="${messageType eq '1'}">
							<c:set var="messageTypeClass" value="alert-success" />
						</c:when>
						<c:otherwise>
							<c:set var="messageTypeClass" value="alert-info" />
						</c:otherwise>
					</c:choose>
					<div class="alert ${messageTypeClass}">
						<button class="close" aria-label="Close" data-dismiss="alert" type="button">
							<span aria-hidden="true">
								<span class="glyphicon glyphicon-remove-circle"></span>
							</span>
						</button>
						<dsp:valueof param="message.message" valueishtml="true"/>
					</div>

				</c:otherwise>
			</c:choose>

		</dsp:oparam>
	</dsp:droplet>
</dsp:page>