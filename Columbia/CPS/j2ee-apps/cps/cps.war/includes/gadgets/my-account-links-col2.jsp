<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileTools"/>
	<dsp:getvalueof var="showPaymentMethod" bean="ProfileTools.showPaymentMethod" />
	<dsp:droplet name="/cps/droplet/AccessRightDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="accessRightKey" value="acc-billing-payment" />
		<dsp:oparam name="true">
			<li><p><strong>Billing and Payment</strong></p></li>
			<li><a href="/account/invoices.jsp">Invoices</a></li>
			<dsp:droplet name="/cps/droplet/AccessRightDroplet">
				<dsp:param name="profile" bean="Profile" />
				<dsp:param name="accessRightKey" value="acc-billing-payment-statements" />
				<dsp:oparam name="true">
					<li><a href="/account/statements.jsp">Statements</a></li>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/cps/droplet/AccessRightDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="accessRightKey" value="acc-lists" />
		<dsp:oparam name="true">
			<li><p><strong>Lists</strong></p></li>
			<li><a href="/account/material-lists.jsp">My Material Lists</a></li>
		<%-- <li><a href="/account/order-carts.jsp">Saved Carts</a></li>--%>
		</dsp:oparam> 
	</dsp:droplet>
	<dsp:droplet name="/cps/droplet/AccessRightDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="accessRightKey" value="acc-my-profile" />
		<dsp:oparam name="true">
			<li><p><strong>My Profile</strong></p></li>
			<li><a href="/account/addresses.jsp">View Addresses</a></li>
			<li><a href="/account/profile-settings.jsp">Edit Profile</a></li>
			<c:if test="${showPaymentMethod}">
				<li><a href="/account/profile-payment-method.jsp">Payment Method</a></li>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
	
</dsp:page>