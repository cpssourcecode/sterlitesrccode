<dsp:page>
	<span class="product-not-available">
		<div>
			<i class="fa fa-exclamation-triangle red"></i>
			<strong style="color: #333333; font-size: 13;">
				<dsp:include page="/includes/gadgets/info-message.jsp">
					<dsp:param name="key" value="errProductNotPurchasable"/>
					<dsp:param name="notWrap" value="true"/>
				</dsp:include>
				<!-- This product is not purchasable online.Please call us to purchase. -->
			</strong>
		</div>
	</span>
</dsp:page>