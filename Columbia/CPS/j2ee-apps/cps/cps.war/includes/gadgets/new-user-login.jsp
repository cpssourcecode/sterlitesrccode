<dsp:page>
	<dsp:getvalueof var="ln" param="ln"/>
	<dsp:getvalueof var="pr" param="pr"/>
	<dsp:getvalueof var="dt" param="dt"/>
	
	<dsp:droplet name="/cps/droplet/NewUserLogin">
		<dsp:param name="ln" value="${ln}"/>
		<dsp:param name="pr" value="${pr}"/>
		<dsp:param name="dt" value="${dt}"/>
		<dsp:param name="tp" value="${tp}"/>
		<dsp:oparam name="true">
			<dsp:getvalueof var="email" param="email"/>
			<dsp:getvalueof var='tempP' param='temporaryPassword'/>
			<dsp:include page="/global/modals/modal-new-user-login.jsp?email=${email}&tempP=${tempP}"/>
		</dsp:oparam>
		<dsp:oparam name="error">
			<dsp:getvalueof var="errorMsg" param="errorMessage"/>
			<dsp:include page="/global/modals/modal-new-user-login.jsp?e=${errorMsg}"/>
		</dsp:oparam>
	</dsp:droplet>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		jQuery(document).ready(function(){
			console.log("Error: ${errorMsg}");
			$("#link-new-user-login").click();
		});
	</script>
</dsp:page>