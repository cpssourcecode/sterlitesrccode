<dsp:page>
    <dsp:getvalueof var="item" param="item"/>
    <dsp:getvalueof var="ntt" param="ntt"/>
    <dsp:getvalueof bean='/cps/util/CPSGlobalProperties.displayableSearchTermsMaxLength' var='searchTextMaxLength'/>
	<c:set var='searchTextMaxLength' value='32'/>
    <dsp:importbean bean="/atg/userprofiling/Profile" />

    <dsp:getvalueof var="isTransient" bean="Profile.transient" />
    <dsp:droplet name="/cps/droplet/AccessRightDroplet">
        <dsp:param name="profile" bean="Profile"/>
        <dsp:param name="accessRightKey" value="plp-pdp-cart"/>
        <dsp:oparam name="false">
            <dsp:getvalueof  var="permissionDenied"  value="true"/>
        </dsp:oparam>
    </dsp:droplet>
    <c:choose>
        <c:when test="${not empty item && not empty item.records}">
        
       			 <c:choose>
                    	<c:when test='${fn:length(ntt) < searchTextMaxLength }'>
                    	<c:set var='nextNtt' value="${fn:substring(ntt,0,fn:length(ntt)-1)}"/>
                    	</c:when>
                    	<c:otherwise>
                    	<c:set var='nextNtt' value="${fn:substring(ntt,0,searchTextMaxLength)}..."/>
                    	</c:otherwise>
                 </c:choose>
        
        
            <ul class="item item-ta">            
              <li class="item-item-short"> 
                    <div class="col-xs-9">
                        <div class='ta-header-div'>Recommended Products for: </div>
                        	${nextNtt}
                    </div>
                         <div class="col-xs-3">
                        <a  href='/search?Ntt=${ntt}' class="btn btn-primary-no-border btn-primary">Shop All</a>
                    </div>                        
               </li>
                <c:forEach var="record" items="${item.records}" end='2'  varStatus="loop">
                    <%-- <c:set var="description"
                           value="${record.attributes['product.description'][0]} ${record.attributes['product.descriptionLine2'][0]}"/> --%>
                    <c:set var="description"
                           value="${record.attributes['product.product_name']}"/>
                    <c:set var="prodId" value="${record.attributes['product.id'][0]}"/>
                    
                    <c:set var="label" value="${vsg_utils:boldText(description,ntt)}"/>
                    <c:set var="link" value="${fn:toLowerCase(record.attributes['product.seoUrl'][0])}"/>
                    <c:set var="imgUrl"
                           value="${empty record.attributes['product.thumbnail_url'] ? '/assets/images/typeahead-placeholder.png' : record.attributes['product.thumbnail_url']}"/>                    
                           
                    <li class="item-item-${loop.count}  item-item">   
                    <c:if test='${loop.count==1}'>
                    	<div  class='ta-top-result'>Top Results for  &nbsp;&nbsp; <b>"${nextNtt}"</b> </div>
                    	</c:if>
                    	                 	
                        <div class="col-xs-3">
						    <a href="${link}" class="item-thumb">
                            	<cp:readerimg  src="${imgUrl}"/>
                        	</a>
   						</div> 
                    	<div class="col-xs-6">
                    		<span class="item-title"><a href="${link}">${label}</a></span>
                    	</div>                    
                        <div class="col-xs-3">
	                        <span>
	                        	<dsp:getvalueof var="minimum_order_qty" value="${record.attributes['product.minimum_order_qty']}"/>
	                            <input type="text" class="form-control text-center item-qty" role="button"
										id="qty${record.attributes['product.repositoryId']}" data-class="warn" data-placement="top"
										data-trigger="hover" data-content="Please enter qty" placeholder="Qty" value="${minimum_order_qty}"> 
								<c:choose>
	                                <c:when test="${isTransient}">
	                                    <dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
	                                        <dsp:oparam name="true">
	                                             <button type="button" data-event-click-id="includes3" 
	                                               data-product-id="${record.attributes['product.repositoryId']}"
	                                               data-sku-id="${record.attributes['sku.repositoryId']}"
	
	                                               id="${record.attributes['product.repositoryId']}"
	                                               class="btn  btn-primary-no-border btn-primary  addToCartButton">
	                                               <fmt:message key="listing.product.tile.addToCart"/>
	                                            </button>
	                                        </dsp:oparam>
	                                        <dsp:oparam name="false">
	                                             <button type="button" class="btn btn-primary-no-border btn-primary addToCartButton"
	                                                    data-productId="${prodId}"
	                                                    data-skuId="${record.attributes['sku.repositoryId']}"
	                                                    data-toggle="modal" data-target="#logIn">
	                                                <fmt:message key="listing.product.tile.addToCart"/>
	                                            </button>
	                                        </dsp:oparam>
	                                    </dsp:droplet>
	                                </c:when>
	                                <c:when test="${permissionDenied}">
	                                    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#permissionDenied">
	                                        <fmt:message key="listing.product.tile.addToCart"/>
	                                    </a>
	                                </c:when>
	                                <c:otherwise>
	                                    <button type="button" data-event-click-id="includes3"
	                                            data-product-id="${record.attributes['product.repositoryId']}"
	                                            data-sku-id="${record.attributes['sku.repositoryId']}"
	                                            
	                                        	id="${record.attributes['product.repositoryId']}"
	                                            class="btn btn-default-no-border  btn-default addToCartButton">
	                                       <fmt:message key="listing.product.tile.addToCart"/>
										</button>
	                                </c:otherwise>
	                            </c:choose>
	                        </span>
                        </div>
                        <div id="minQtyError${prodId}" class="red" style="display:none;"></div>
                    </li>
                </c:forEach>
            </ul>
        </c:when>
        <c:otherwise>
        </c:otherwise>
    </c:choose>
</dsp:page>