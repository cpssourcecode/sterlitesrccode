<dsp:page>
	<div class="col-md-5 col-xs-12 header-full-logo">
		<a href="/" class="hidden-print"> 
			<img class="hidden-sm hidden-xs" src="${staticContentPrefix}/assets/images/logo.png" alt="Image">
			<img class="visible-sm visible-xs" src="${staticContentPrefix}/assets/images/logo.png" class="img-responsive" alt="Image">
		</a>
		<img class="visible-print" src="${staticContentPrefix}assets/images/logo_print.jpg" alt="">
	</div>
</dsp:page>