<dsp:page>
	<span class="product-not-available">
		<div>
			<i class="fa fa-exclamation-triangle red"></i>
			<strong style="color: #333333; font-size: 13;">
				<dsp:include page="/includes/gadgets/info-message.jsp">
					<dsp:param name="key" value="errProductNoLongerAvailable"/>
					<dsp:param name="notWrap" value="true"/>
				</dsp:include>
				<!-- This product is no longer available -->
			</strong>
		</div>
	</span>
</dsp:page>