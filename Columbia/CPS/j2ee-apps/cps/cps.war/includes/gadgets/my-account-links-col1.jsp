<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/droplet/OrderApprovalsDroplet" />
	<dsp:importbean bean="/cps/droplet/OrderRequestsDroplet"/>
	<dsp:droplet name="/cps/droplet/AccessRightDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="accessRightKey" value="acc-manage-orders" />
		<dsp:oparam name="true">
			<li><p><strong>Manage Orders</strong></p></li>
			<li><a href="/account/order-list.jsp">Order History</a></li>
			<dsp:droplet name="/cps/droplet/AccessRightDroplet">
				<dsp:param name="profile" bean="Profile" />
				<dsp:param name="accessRightKey" value="acc-manage-orders-approvals" />
				<dsp:oparam name="true">
					<dsp:getvalueof var="ordersSize" value="${0}" />
					<dsp:droplet name="OrderApprovalsDroplet">
						<dsp:param name="profile" bean="Profile" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="ordersSize" param="ordersSize" />
						</dsp:oparam>
					</dsp:droplet>
					<li><a href="${originatingRequest.contextPath}/account/order-approvals.jsp">
						Order Approvals <span class="text-danger">(<dsp:valueof	value="${ordersSize}" />)</span></a></li>
				</dsp:oparam>
			</dsp:droplet>
			<dsp:droplet name="/cps/droplet/AccessRightDroplet">
				<dsp:param name="profile" bean="Profile" />
				<dsp:param name="accessRightKey" value="acc-manage-orders-requests" />
				<dsp:oparam name="true">
					<dsp:getvalueof var="ordersSize" value="${0}" />
					<dsp:droplet name="OrderRequestsDroplet">
						<dsp:param name="profile" bean="Profile"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="ordersSize" param="ordersSize" />
						</dsp:oparam>
					</dsp:droplet>
					<li><a href="${originatingRequest.contextPath}/account/order-requests.jsp">
						Order Requests <span class="text-danger">(<dsp:valueof	value="${ordersSize}" />)</span></a></li>
				</dsp:oparam>
			</dsp:droplet>
			<li><a href="/account/packing-list.jsp">Packing Slips</a></li>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/cps/droplet/AccessRightDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="accessRightKey" value="acc-admin-tools" />
		<dsp:oparam name="true">
			<li><p><strong>Admin Tools</strong></p></li>
			<li><a href="/account/manage-users.jsp">Manage Users</a></li>
			<dsp:droplet name="/cps/droplet/AccessRightDroplet">
				<dsp:param name="profile" bean="Profile" />
				<dsp:param name="accessRightKey" value="acc-admin-tools-manage-companies" />
				<dsp:oparam name="true">
					<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
						<c:if test="${empty selectedOrg}">
							<li><a href="/account/manage-companies.jsp">Manage Companies</a></li>
						</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>