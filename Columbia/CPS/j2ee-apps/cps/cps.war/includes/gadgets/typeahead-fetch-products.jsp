<dsp:page>

	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:droplet name="InvokeAssembler">
		<dsp:param name="contentCollection"
			value="/content/Shared/Auto-Suggest Panels" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="content"
							vartype="com.endeca.infront.assembler.ContentItem"
							param="contentItem" />
		</dsp:oparam>
	</dsp:droplet>

	<c:if test="${not empty content.contents}">
		<c:set var="autoSuggestPanel" value="${content.contents[0]}" />
		<c:forEach var="item" items="${autoSuggestPanel.autoSuggest}">
			<c:set var="recordsType" value="${item.recordsType}" />
			<c:choose>
				<c:when test="${'product' eq recordsType}">
					<c:set var="products" value="${item}"/>
				</c:when>				  
				</c:choose>
					
			
		</c:forEach>
	</c:if>

	<c:if test="${not empty products and not empty products.records}">
		<div class="row row-flush">
			<dsp:getvalueof var="ntt" param="Ntt"/>
			<c:if test="${not empty content.contents}">
					<div class="col-sm-12 ta-products hidden-xs">					
					<dsp:include src="/includes/gadgets/typeahead-products.jsp">
						<dsp:param name="item" value="${products}" />
						<dsp:param name="ntt" value="${ntt}" />
					</dsp:include>
				</div>
		 
	
			</c:if>
		</div>
	</c:if>
 
</dsp:page>
