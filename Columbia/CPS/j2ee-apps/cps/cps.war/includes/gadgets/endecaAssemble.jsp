<dsp:page>
	<dsp:getvalueof var="contentCollection" param="contentCollection" />
	<dsp:getvalueof var="ruleLimit" param="ruleLimit" />
	<dsp:getvalueof var="defaultPage" param="defaultPage" />

	<c:if test="${empty ruleLimit}">
		<c:set var="ruleLimit" value="1" />
	</c:if>
	<dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
		<dsp:param name="contentCollection" value="${contentCollection}"/>
		<dsp:param name="ruleLimit" value="${ruleLimit}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
			<c:choose>
				<c:when test="${empty content.contents}">
					<c:if test="${not empty defaultPage}">
						<dsp:include page="${defaultPage}"/>
					</c:if>
				</c:when>
				<c:otherwise>
					<c:forEach var="element" items="${content.contents}">
						<dsp:renderContentItem contentItem="${element}"/>
					</c:forEach>
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
		<dsp:oparam name="error">
			<c:if test="${not empty defaultPage}">
				<dsp:include page="${defaultPage}"/>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>