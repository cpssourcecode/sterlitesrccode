<dsp:page>
	<dsp:getvalueof var="src" value="${src}" />
	<dsp:getvalueof var="prefix" value="${prefix}" />
	<c:choose>
		<c:when test="${fn:startsWith(fn:toLowerCase(src),'http')}">
			<c:out value="${src}"></c:out>
		</c:when>
		<c:otherwise>
			<c:out value="${prefix}${src}"></c:out>
		</c:otherwise>
	</c:choose>
</dsp:page>