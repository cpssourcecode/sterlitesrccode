<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<footer id="footer">
		<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
		<c:set var="contentCollection" value="/content/Shared/Footers" />
		<dsp:droplet name="InvokeAssembler">
			<dsp:param name="contentCollection" value="${contentCollection}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
				<dsp:renderContentItem contentItem="${content}" />
			</dsp:oparam>
		</dsp:droplet>
	</footer>
	<dsp:include page="gadgets/footer-modals.jsp"/>
</dsp:page>
