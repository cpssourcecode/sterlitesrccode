<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/cps/userprofiling/UserCheckForActionFormHandler"/>
    <dsp:getvalueof var="page" param="page"/>
    <dsp:getvalueof var="title" param="title"/>
    <dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
<%--
     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="${staticContentPrefix}/assets/javascripts/jquery-1.11.3.min${build_version}.js"></script>
    <script src="${staticContentPrefix}/assets/javascripts/jquery-migrate-1.2.1.min${build_version}.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${staticContentPrefix}/assets/javascripts/bootstrap${build_version}.js"></script>
    <script src="${staticContentPrefix}/assets/javascripts/bootstrap/collapse${build_version}.js"></script>
    <script src="${staticContentPrefix}/assets/javascripts/bootstrap/tooltip${build_version}.js"></script>
    <script src="${staticContentPrefix}/assets/javascripts/bootstrap/popover${build_version}.js"></script>
    <script src="${staticContentPrefix}/assets/javascripts/bootstrap/alert${build_version}.js"></script>
    <!-- // <script src="${staticContentPrefix}/assets/javascripts/bootstrap/modal${build_version}.js"></script> -->
    <!-- // <script src='/assets/javascripts/bootstrap/dropdown${build_version}.js'></script> -->
    <script src="${staticContentPrefix}/assets/javascripts/bootstrap-hover-dropdown.min${build_version}.js"></script>
    <!-- fuel ux -->
    <script src="${staticContentPrefix}/assets/javascripts/fuelux/checkbox${build_version}.js"></script>
    <script src="${staticContentPrefix}/assets/javascripts/fuelux/radio${build_version}.js"></script>
    <!-- owl -->
    <script src='/assets/javascripts/owl/owl.carousel.min${build_version}.js'></script>
    <!-- custom scroll -->
    <script src='/assets/javascripts/owl/owl.carousel.min${build_version}.js'></script>
    <!-- dropdown-enchancements -->
    <script src='/assets/javascripts/jquery.scrollbar.min${build_version}.js'></script>
    <!-- // <script src='/assets/javascripts/owl/owl.support.modernizr${build_version}.js'></script> -->
    <!-- fileinput -->
    <script src='/assets/javascripts/bootstrap-fileinput/fileinput.min${build_version}.js'></script>
    <script src='/assets/javascripts/fileupload-ui${build_version}.js'></script>
    <!-- custom -->
    <script src="${staticContentPrefix}/assets/javascripts/prototype${build_version}.js"></script>

--%>

	<script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/floatlabels.min${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/bowser.min${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/placeholder${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/slick.min${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/assets/javascripts/base${build_version}.js"></script>

	<dsp:form action="${originatingRequest.contextPath}/includes/pageEnd.jsp"
   			method="post" id="check-user-action-form" formid="check-user-action-form">
   		<dsp:input id="check-user-action" bean="UserCheckForActionFormHandler.action" type="hidden" value=""/>
   		<dsp:input type="hidden" bean="UserCheckForActionFormHandler.checkForAction" value="true" priority="-10"/>
   	</dsp:form>
	<c:if test="${page eq 'location'}">
		<c:if test="${title eq 'landing'}">
			<script type="text/javascript" src="${staticContentPrefix}/js/gmap${build_version}.js"></script>
		</c:if>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhvlSr_k-kf386dGCg3Qhp4pHMduIUUgg&callback=initMap" async defer></script>
	</c:if>

	<div class="legdis-fixed legdis-fixed-bottom legal-disclaimer js-legal-disclaimer">
		<div class="legdis-media">
			<div class="legdis-media-body legdis-media-center">
				<p>By using our site, you are agreeing to our Terms of Site Use. For more information, see our
					<a class="legdis-href"
					   href="/termsOfUse">Terms of Use</a> and <a
							class="legdis-href"
							href="/privacyPolicy">Privacy Policy</a>.
					<button class="legdis-button"><strong>I agree</strong></button>
				</p>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="${staticContentPrefix}/js/events/events-account${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/events/events-cartridges${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/events/events-catalog${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/events/events-checkout${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/events/events-content${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/events/events-global${build_version}.js"></script>

	<%-- Clean up params from url: --%>
	<script type="application/javascript" nonce="${requestScope.nonce}">
		$(document).ready(function () {
            $(window).load(function() {
                function removeParam(key, sourceURL) {
                    var rtn = sourceURL.split("?")[0],
                        param,
                        params_arr = [],
                        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
                    if (queryString !== "") {
                        params_arr = queryString.split("&");
                        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                            param = params_arr[i].split("=")[0];
                            if (param === key) {
                                params_arr.splice(i, 1);
                            }
                        }
                        rtn = rtn + "?" + params_arr.join("&");
                    }
                    return rtn;
                }

                var paramsToRemove = ['quickomodal', 'quickodropdown', '_requestid', 'invalidskumodal', 'showCS', 'productId', 'qo'];

                var newURl = window.location.href;
                for(var i = 0; i < paramsToRemove.length; i++){
                    newURl = removeParam(paramsToRemove[i], newURl);
				}

                if (newURl != window.location.href && window.history != undefined && window.history.pushState != undefined) {
                    window.history.pushState({}, document.title, newURl);
                }
            });
        });
	</script>

</body>
</html>
<!-- Displayed ServerName, Profile Id, and Current Order Id.
Server Name       : <dsp:valueof bean="/atg/dynamo/service/ServerName.serverName" />
Profile           : <dsp:valueof bean="/atg/userprofiling/Profile.id" />
Profile Status    : <dsp:valueof bean="/atg/userprofiling/Profile.securityStatus" />
Order             : <dsp:valueof bean="/atg/commerce/ShoppingCart.current.id" />
current date time : <dsp:valueof bean="/atg/dynamo/service/CurrentDate.timeAsDate" />
 -->
</dsp:page>