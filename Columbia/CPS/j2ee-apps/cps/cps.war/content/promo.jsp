<dsp:page>
	<dsp:getvalueof var="targeterComponent" param="targeterComponent" />
	<c:if test="${not empty targeterComponent}">
		<dsp:getvalueof var="showRandom" param="showRandom" />
		<dsp:getvalueof var="maxNumber" param="maxNumber" />
		<dsp:getvalueof var="tag" param="tag" />
		<c:choose>
			<c:when test="${showRandom}">
				<dsp:droplet name="/atg/targeting/TargetingRandom">
					<dsp:param name="targeter" bean="${targeterComponent}" />
					<dsp:param name="elementName" value="promoMedia" />
					<dsp:oparam name="output">
						<dsp:include src="/content/promoMedia.jsp">
							<dsp:param name="promoMedia" param="promoMedia" />
						</dsp:include>
					</dsp:oparam>
				</dsp:droplet>
			</c:when>
			<c:otherwise>
				<dsp:droplet name="/vsg/droplet/VSGTargetingForEach">
					<dsp:param name="targeterName" value="${targeterComponent}" />
					<dsp:param name="profile" bean="/atg/userprofiling/Profile" />
					<dsp:param name="maxNumber" value="100" />
					<dsp:param name="tagFilter" value="${tag}" />
					<dsp:param name="elementName" value="promoMedia" />
					<dsp:oparam name="output">
						<dsp:include src="/content/promoMedia.jsp">
							<dsp:param name="promoMedia" param="promoMedia" />
						</dsp:include>
					</dsp:oparam>
				</dsp:droplet>
			</c:otherwise>
		</c:choose>
	</c:if>
</dsp:page>