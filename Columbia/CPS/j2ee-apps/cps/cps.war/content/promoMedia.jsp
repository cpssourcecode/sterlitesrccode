<dsp:page>
	<dsp:getvalueof var="promoHTML" param="promoMedia.html" />
	<c:choose>
		<c:when test="${empty promoHTML}">
			<dsp:getvalueof var="promoImage" param="promoMedia.url" />
			<div class="row">
				<div class="col-md-12" style="margin-top:9px;margin-bottom:9px;">
					<cp:readerimg src="${promoImage}" style="width:100%;" alt="Image"/>
					<div class="rwd-space_20"></div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="row">
				<div class="col-md-12" style="margin-top:9px;">
					<div class="content-box box-default promo-banner-a">
						<dsp:valueof value="${promoHTML}" valueishtml="true" />
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</dsp:page>