<dsp:page>
	<dsp:droplet name="/cps/droplet/BrandsNavigationDroplet">
		<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
		<dsp:oparam name="output">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="menuList" />
				<dsp:param name="elementName" value="menu"/>
				<dsp:oparam name="output">
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" param="menu" />
						<dsp:param name="elementName" value="navigationMenuItem" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="navigationMenuItem" param="navigationMenuItem" />
							<li>
								<a href="${navigationMenuItem.URL}">${navigationMenuItem.name}</a>
							</li>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>