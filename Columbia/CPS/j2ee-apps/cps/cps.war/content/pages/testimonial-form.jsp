<dsp:page>
	<dsp:importbean bean="/cps/content/handler/TestimonialsFormHandler"/>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-testimonial.jsp"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<div class="well well-grey hidden-xs">
		<h4>Submit a Testimonial</h4>
			<div class="form-group">
				<input id="testimonialFirstName_secondary" value="" type="text" class="form-control" placeholder="First Name*" maxlength="20" required=""/>
			</div>
			<div class="form-group">
				<input id="testimonialLastName_secondary" value="" type="text" class="form-control" placeholder="Last Name*" maxlength="20" required=""/>
			</div>
			<div class="form-group">
				<input id="testimonialEmail_secondary" value="" type="email" class="form-control" placeholder="Email (Optional)" maxlength="50" required=""/>
			</div>
			<div class="form-group">
				<input id="testimonialCompany_secondary" type="text" class="form-control" placeholder="Company*" maxlength="30" required="" />
			</div>
			<div class="form-group">
				<textarea class="form-control" name="message"
					id="testimonialMessage_secondary" cols="6" rows="5" maxlength="250"
					placeholder="Testimonial Message* (Max 250 Characters)"
					required=""></textarea>
			
			</div>
			<c:choose>
						<c:when test="${isTransient}">
						<div id="testimonial-form-captcha">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<div id="recaptchaTestimonial"></div>
									</div>
								</div>
							</div>
						</div>
							<button type="submit" id="testimonialSubmit" class="btn btn-primary btn-block" data-event-click-id="content12" >
				Submit</button>
						</c:when>
						<c:otherwise>
							<button type="submit" class="btn btn-primary btn-block" data-event-click-id="content12" >
				Submit</button>
						</c:otherwise>
					</c:choose>				
			
	</div>
	<dsp:form action="#" method="post" id="testimonial-form_secondary" formid="testimonial-form_secondary">
		<dsp:input value="" type="hidden" id="firstName_hidden_secondary" bean="TestimonialsFormHandler.value.testimonialFirstName"/>
		<dsp:input value="" type="hidden" id="lastName_hidden_secondary" bean="TestimonialsFormHandler.value.testimonialLastName"/>
		<dsp:input value="" type="hidden" id="email_hidden_secondary" bean="TestimonialsFormHandler.value.testimonialEmail"/>
		<dsp:input value="" type="hidden" id="company_hidden_secondary" bean="TestimonialsFormHandler.value.testimonialCompany"/>
		<dsp:input value="" type="hidden" id="message_hidden_secondary" bean="TestimonialsFormHandler.value.testimonialMessage"/>
		<dsp:input type="hidden" bean="TestimonialsFormHandler.submitTestimonial" value="true" priority="-10"/>
	</dsp:form>
	<script type="text/javascript" nonce="${requestScope.nonce}">
$(document).ready(function() {
    checkTestimonialSubmitButton();
});
    
var testimonial_captcha_success_default = false;
var testimonial_captcha_default = null;

function checkTestimonialSubmitButton() {
    var enableLoginButton = true;
    if ($('#testimonial-form-captcha').length > 0) {
        enableLoginButton = testimonial_captcha_success_default;
    }

    if (enableLoginButton) {
        $('#testimonialSubmit').removeAttr('disabled');
    } else {
        $('#testimonialSubmit').attr('disabled', 'disabled');
    }
}

function renderCaptchaCallback() {
    var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
    testimonial_captcha_default = grecaptcha.render(
        'recaptchaTestimonial', {
            'sitekey' : sitekey,
            'callback' : validateRecaptchaDefaultTestimonial,
            'expired-callback' : expireRecaptchaDefaultTestimonial
    });
}

function validateRecaptchaDefaultTestimonial(captchaResponse) {
    testimonial_captcha_success_default = true;
    checkTestimonialSubmitButton();
}

function expireRecaptchaDefaultTestimonial() {
    grecaptcha.reset(testimonial_captcha_default);
    testimonial_captcha_success_default = false;
    checkTestimonialSubmitButton();
}

</script>
</dsp:page>