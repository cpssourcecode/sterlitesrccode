<dsp:page>

	<div id="caseStudies">
		<dsp:include src="gadgets/case-studies-list.jsp">
			<dsp:param name="howMany" value="9" />
			<dsp:param name="start" value="1" />
		</dsp:include>
	</div>
	
	<script type="text/javascript" nonce="${requestScope.nonce}">
		var currentPage = 1;
		function changePage(page) {
			$("#caseStudies").load("/content/pages/gadgets/case-studies-list.jsp?howMany=9&start=" + page, function() {
                eventsCaseStudiesPagination();
				$(document).resize();
			});
		}
	</script>
	
</dsp:page>