<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/cps/droplet/CPSPriceListDateDroplet" />
	<dsp:importbean bean="/cps/content/droplet/PriceListUpdatesLookupDroplet" />
	<dsp:importbean bean="/cps/content/handler/PriceListUpdatesFormHandler" />

	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:getvalueof var="itemId" bean="/cps/content/service/ContentPagesService.priceListUpdatesItemId" />

	<dsp:droplet name="PriceListUpdatesLookupDroplet">
		<dsp:param name="id" value="${itemId}" />
		<dsp:oparam name="output">
			<dsp:setvalue param="item" paramvalue="element" />
			<dsp:getvalueof var="size" param="item.priceSections"/>
            <fmt:formatNumber var="sizeCenter" value="${fn:length(size)/2}" maxFractionDigits="0"/>
				<div class="row row-narrow">
					<div class="col-sm-12 col-xs-12">
                         <table class="table table-striped" style="margin-bottom: 0px;">
                                <col width="20%">
                                <col width="80%">
                                <thead class="header">
                                <tr><th class="text-center">Select All<br><input id="priceBookAll" type="checkbox" /></th><th></th></tr></thead></table>		
					    		<dsp:droplet name="ForEach">
								<dsp:param name="array" param="item.priceSections"/>
								<dsp:param name="sortProperties" value="_key"/>
								<dsp:param name="elementName" value="priceSection"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="name" param="key"/>
									<dsp:getvalueof var="url" param="priceSection"/>
									<dsp:getvalueof var="index" param="count"/>
                                    
									 <c:choose>
                                            <c:when test="${index <= sizeCenter}">
                                                <c:if test="${index == 1 }">
                                                <div class="oddClass col-sm-6 col-xs-6">
                                                  <table class="table table-striped table-mobile">
                                                </c:if>
                                                    <tr>
                                                    <td>
                                                    <label for="ch-pr-2-${index}">
                                                    	<input type="checkbox" name="priceBook" data-value="${url}" id="ch-pr-2-${index}" data-name="${name}"/>
                                                    	<span class="priceBookLable">
                                                    	 ${name}
                                                    	 </span>
                                                    </label>
                                                    </td>
	        									    </tr>
	        									<c:if test="${index == sizeCenter}">   
                                                	</table>	
                                                    </div>
                                                </c:if>
                                           </c:when>
                                           <c:otherwise>
                                           <c:if test="${index == (sizeCenter + 1) }">
                                              <div class="evenClass col-sm-6 col-xs-6">
                                                 <table class="table table-striped table-mobile">
                                              </c:if>
                                                    <tr>
                                                    	<td>
                                                    <label for="ch-pr-2-${index}">
                                                    	<input type="checkbox" name="priceBook" data-value="${url}" id="ch-pr-2-${index}" data-name="${name}"/>
                                                    	 <span class="priceBookLable">${name}</span>
                                                    </label>
                                                    </td>
                                                    </tr>
	        								<c:if test="${index == (fn:length(size))}"> 
                                                      </table>
                                                	</div>
                                             </c:if>
                                           </c:otherwise>
                                        </c:choose>
								</dsp:oparam>
							</dsp:droplet>
							</div>
							<div class="col-sm-6 col-xs-12" style="margin-top: 20px">
						<button class="btn btn-default btn-priceBook" id="price-book-section" disabled="true" >Download </button>
					</div>
						</div>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
	<script src="${staticContentPrefix}/js/price-lists${build_version}.js"></script>
	<script src="${staticContentPrefix}/assets/javascripts/zip/jszip.min${build_version}.js"></script>
	<script src="${staticContentPrefix}/assets/javascripts/zip/FileSaver.min${build_version}.js"></script>
</dsp:page>