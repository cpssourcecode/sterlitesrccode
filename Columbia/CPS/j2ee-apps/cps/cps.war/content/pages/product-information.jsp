<dsp:page>

	<div class="accordion panel-group panel-light" id="pi-accordion" role="tablist" aria-multiselectable="true">
		<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
			<dsp:param name="queryRQL" value="ALL" />
			<dsp:param name="repository" value="/cps/registry/repository/ContentPagesRepository" />
			<dsp:param name="itemDescriptor" value="prodinfo_category" />
			<dsp:param name="sortProperties" value="+displayOrder" />
			<dsp:oparam name="output">
				<dsp:include page="/content/pages/gadgets/product-information-display.jsp">
					<dsp:param name="category" param="element" />
				</dsp:include>
			</dsp:oparam>
		</dsp:droplet>
	</div>

</dsp:page>