<dsp:page>
	<%--VIDEOS SORTING--%>
	<%--<div class="well">
		<div class="row">
			<div class="form-group col-md-4 col-sm-6">
				<label class="control-label">Sort Videos</label>
				<span class="select-wrap">
					<select name="filter-role" id="filter-role" class="form-control">

						<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
							<dsp:param name="queryRQL" value="ALL" />
							<dsp:param name="repository" value="/cps/registry/repository/ContentPagesRepository" />
							<dsp:param name="itemDescriptor" value="instructional_video_role" />
							<dsp:param name="sortProperties" value="+displayOrder" />
							<dsp:oparam name="output">
								<option value="<dsp:valueof param="element.id" />"><dsp:valueof param="element.roleName" /></option>

								&lt;%&ndash; Store initial item for page init &ndash;%&gt;
								<dsp:getvalueof var="index" param="index" />
								<c:if test="${index == 0}">
									<dsp:getvalueof var="initialRole" param="element.id" />
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
					</select>
				</span>
			</div>
		</div>
	</div>
--%>
	<div id="instructional-videos">
		<dsp:include page="gadgets/instructional-video-list.jsp">
			<dsp:param name="role" value="${initialRole}" />
			<dsp:param name="page" value="1" />
		</dsp:include>
	</div>

	<script type="text/javascript" nonce="${requestScope.nonce}">
		function changePage(page) {
			var role = $('#filter-role :selected').val();

			$('#instructional-videos').load(
					'/content/pages/gadgets/instructional-video-list.jsp?page='
							+ page + '&role=' + role, function() {
                    eventsContentPagesPagination();
						resizeVideoCells();
					});
		}

		function resizeVideoCells() {
			var maxHeight = 0;
			$('#instructional-videos .row .col-lg-4').each(function() {
				var height = $(this).height();
				maxHeight = (maxHeight < height ? height : maxHeight)
			});
			$('#instructional-videos .row .col-lg-4 .thumbnail').css('height',
					maxHeight);
		}

		$('#filter-role').on('change', function() {
			changePage(1);
		});
		$(document).delegate('*[data-toggle="lightbox"]', 'click',
				function(event) {
					event.preventDefault();
					$(this).ekkoLightbox();
				});
		$(document).ready(function() {
			resizeVideoCells();
		});
	</script>

</dsp:page>