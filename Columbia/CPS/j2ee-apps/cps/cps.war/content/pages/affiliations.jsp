<dsp:page>
	<dsp:importbean bean="/cps/content/droplet/QueryAffiliationsDroplet" />
	<div class="affiliation-list">
		<dsp:droplet name="QueryAffiliationsDroplet">
			<dsp:oparam name="output">
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="repositoryItems"/>
					<dsp:param name="sortProperties" value="+name,+description" />
					<dsp:param name="elementName" value="currentItem"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="link" param="currentItem.link" />
						<dsp:getvalueof var="img" param="currentItem.imageUri" />
						<dsp:getvalueof var="name" param="currentItem.name" />
						<dsp:getvalueof var="description" param="currentItem.description" />

						<div class="row affiliate">
							<div class="col-sm-4 mb10 text-center">
								<c:if test="${not empty link}">
									<a href="${link}"> <cp:readerimg src="${img}" alt="${name}"	style="max-width: 300px; max-height: 200px; align-items: center" /></a>
								</c:if>
								<c:if test="${empty link}">
									<cp:readerimg src="${img}" alt="${name}" style="max-width: 300px; max-height: 200px; align-items: center" />
								</c:if>
							</div>
							
							<div class="col-sm-8">
								<h4 class="media-heading"><dsp:valueof value="${name}" /></h4>
								
								<c:if test="${not empty description}">
									<p><dsp:valueof value="${description}" valueishtml="true" /></p>
								</c:if>
								
								<!-- <a href="#" class="btn btn-info">Visit Site</a> -->
								<c:if test="${not empty link}">
									<a href="${link}" target="_blank" class="btn btn-info">Visit Site</a>
								</c:if>
								
							</div>
						</div>

					</dsp:oparam>
					<dsp:oparam name="empty">
						No items.
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
	</div>
</dsp:page>