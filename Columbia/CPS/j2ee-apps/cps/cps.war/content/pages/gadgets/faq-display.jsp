<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	
	<dsp:getvalueof var="faqCategory" param="cat" />
	<dsp:getvalueof var="openQuestion" param="oq" />
	
	<%--
		Get default category in case of invalid/unspecified category and use that to display.
		I run the query this way so we use the same sorting as on the category display page
		(so the "first" item displayed is default).
	--%>
	<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
		<dsp:param name="queryRQL" value="ALL" />
		<dsp:param name="repository" value="/cps/registry/repository/ContentPagesRepository" />
		<dsp:param name="itemDescriptor" value="faq_category" />
		<dsp:param name="sortProperties" value="+displayOrder" />
		<dsp:oparam name="output">
			<dsp:getvalueof	var="idx" param="index"	/>
			<c:if test="${idx == 0}">
				<dsp:getvalueof var="defaultFaqCategory" param="element" />
			</c:if>
		</dsp:oparam>
	</dsp:droplet>

	<%-- Validate parameter (if available) or fall back to default category --%>
	<dsp:getvalueof var="selectedFaqCategory" value="" />
	<c:choose>
		<c:when test="${not empty faqCategory}">
			<dsp:droplet name="/atg/targeting/RepositoryLookup">
				<dsp:param name="id" value="${faqCategory}" />
				<dsp:param name="repository" bean="/cps/registry/repository/ContentPagesRepository" />
				<dsp:param name="itemDescriptor" value="faq_category" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="selectedFaqCategory" param="element" />
				</dsp:oparam>
				<dsp:oparam name="empty">
					<dsp:getvalueof var="selectedFaqCategory" value="${defaultFaqCategory}" /> 
				</dsp:oparam>
			</dsp:droplet>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="selectedFaqCategory" value="${defaultFaqCategory}" /> 
		</c:otherwise>
	</c:choose>

	
	<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
		<dsp:param name="queryRQL" value="category = :faqCategory" />
		<dsp:param name="faqCategory" value="${selectedFaqCategory}" />
		<dsp:param name="repository" value="/cps/registry/repository/ContentPagesRepository" />
		<dsp:param name="itemDescriptor" value="faq_question" />
		<dsp:param name="sortProperties" value="+displayOrder" />
		
		<dsp:oparam name="outputStart">
            <h4 class="text-nocase"><dsp:valueof param="faqCategory.categoryName" /></h4>
            <div class="accordion panel-group panel-light" role="tablist" aria-multiselectable="true" id="faq-accordion">
		</dsp:oparam>
		<dsp:oparam name="output">

            <div class="panel">
            	<dsp:getvalueof var="questionId" param="element.id" />
            	
            <c:choose>
				<c:when test="${openQuestion eq questionId}">	
				<div class="panel-heading" role="tab" id="heading${questionId}">
					<a href="#collapse${questionId}" class="" role="button" data-toggle="collapse" data-parent="#faq-accordion" id="link${questionId}"
					aria-expanded="true" aria-controls="collapse${questionId}">
                        <dsp:valueof param="element.question" />
                    </a>
                </div>
				<div id="collapse${questionId}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading${questionId}" aria-expanded="true">
					<div class="panel-body">
						<dsp:valueof param="element.answer" valueishtml="true"/>
					</div>
				</div>
				</c:when>
				
				<c:otherwise>
				<div class="panel-heading" role="tab" id="heading${questionId}">
					<a href="#collapse${questionId}" class="collapsed" role="button" data-toggle="collapse" data-parent="#faq-accordion" 
					aria-expanded="false" aria-controls="collapse${questionId}">
                        <dsp:valueof param="element.question" />
                    </a>
				</div>
				<div id="collapse${questionId}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading${questionId}" aria-expanded="false">
					<div class="panel-body">
						<dsp:valueof param="element.answer" valueishtml="true"/>
					</div>
				</div>
				</c:otherwise>
			</c:choose>
			</div>

		</dsp:oparam>
		<dsp:oparam name="outputEnd">
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>