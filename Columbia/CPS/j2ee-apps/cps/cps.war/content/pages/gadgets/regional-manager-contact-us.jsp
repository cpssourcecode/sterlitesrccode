<dsp:importbean bean="/cps/util/RegionalManagerContactFormHandler" />

<dsp:page>
	<dsp:getvalueof var="regionalManager" param="regionalManager" />
	<dsp:getvalueof var="email" param="regionalManager.email" />
	<dsp:getvalueof var="rmId" param="regionalManager.id" />

	<c:if test="${not empty email}">
		<div class="rm-contact-container">

			<%-- REGIONAL MANAGER CONTACT FORM --%>
			<div class="well well-gray col-xs-12 hidden-print rm-contact-form" style="display: none;">
				<dsp:form method="POST" action="/">
					<div class="alert alert-danger rm-contact-form-errors" style="display:none;"></div>
					<div class="row">
						<div class="form-group col-xs-12 col-sm-4">
							<fmt:message key="content.regional_managers.contact.firstName" var="firstNamePlaceholder"/>
							<dsp:input bean="RegionalManagerContactFormHandler.firstName" type="text" iclass="form-control rm-contact-firstName" maxlength="30">
								<dsp:tagAttribute name="placeholder" value="*${firstNamePlaceholder}" />
							</dsp:input>
						</div>
						<div class="form-group col-xs-12 col-sm-4">
							<fmt:message key="content.regional_managers.contact.lastName" var="lastNamePlaceholder"/>
							<dsp:input bean="RegionalManagerContactFormHandler.lastName" type="text" iclass="form-control rm-contact-lastName" maxlength="30">
								<dsp:tagAttribute name="placeholder" value="*${lastNamePlaceholder}" />
							</dsp:input>
						</div>
						<div class="form-group col-xs-12 col-sm-4">
							<fmt:message key="content.regional_managers.contact.emailAddress" var="emailAddressPlaceholder"/>
							<dsp:input bean="RegionalManagerContactFormHandler.emailAddress" type="text" iclass="form-control  rm-contact-emailAddress" maxlength="100">
								<dsp:tagAttribute name="placeholder" value="*${emailAddressPlaceholder}" />
							</dsp:input>
						</div>
						<div class="form-group col-xs-12 col-sm-12">
							<fmt:message key="content.regional_managers.contact.message" var="messagePlaceholder"/>
							<textarea name="/cps/util/RegionalManagerContactFormHandler.message" cols="5" rows="7" maxlength="500" placeholder="*${messagePlaceholder}" class="form-control rm-contact-message"></textarea>
						</div>
						<div class="col-xs-12 col-sm-12">
							<ul class="list-inline">
								<li>
									<button type="button" class="btn btn-primary rm-contact-submit"><fmt:message key="content.regional_managers.contact.submit" /></button>
								</li>
								<li>
									<button type="button" class="btn btn-info rm-contact-cancel"><fmt:message key="content.regional_managers.contact.cancel" /></button>
								</li>
							</ul>
						</div>
					</div>
					<dsp:input type="hidden" bean="RegionalManagerContactFormHandler.regionalManager" value="${rmId}" />
					<dsp:input type="hidden" bean="RegionalManagerContactFormHandler.regionalManagerContactUs" value="true" />
				</dsp:form>
			</div>

			<%-- CONFIRMATION --%>
			<div class="well well-gray col-xs-12 hidden-print rm-contact-confirmation" style="display: none;">
				<p><fmt:message key="content.regional_managers.contact.confirmation" /></p>
				<br />
				<div class="form-group">
					<button type="button" class="btn btn-primary rm-contact-confirm">
						<fmt:message key="content.regional_managers.contact.close" />
					</button>
				</div>
			</div>

		</div>
	</c:if>

</dsp:page>