<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<div class="col-sm-8">
		<ol class="breadcrumb">
			<li><a href="${originatingRequest.contextPath}">Home</a></li>
			<li class="active"><dsp:valueof param="currentPage" /></li>
		</ol>
	</div>

</dsp:page>