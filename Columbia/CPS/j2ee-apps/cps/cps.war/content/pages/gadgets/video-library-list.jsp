<dsp:page>
	<dsp:importbean bean="/cps/content/droplet/VideoLibraryDroplet" />
	<dsp:importbean bean="/cps/content/droplet/YoutubePreviewDroplet" />
	
	<dsp:getvalueof var="page" param="page"/>

	<dsp:droplet name="VideoLibraryDroplet">
		<dsp:oparam name="output">
			<dsp:getvalueof var="videos" param="videos"/>
			<div class="row row-narrow cs">
				<c:forEach var="video" items="${videos}" begin="${6 * (page-1)}" end="${(6 * (page-1)) + 5}" >
					<dsp:param name="video" value="${video}"/>
					<div class="col-xs-4 col-xxs-12">
						<div class="thumbnail">
							<dsp:getvalueof var="videoUrl" param="video.videoUrl"/>
							<a href="${fn:replace(videoUrl,'watch?v=', 'v/')}" class="text-nodec resize-thumb" data-toggle="lightbox">

								<dsp:droplet name="YoutubePreviewDroplet">
									<dsp:param name="videoUrl" value="${videoUrl}"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="videoPreview" param="preview"/>
										<c:if test="${empty videoPreview}">
											<dsp:getvalueof var="videoPreview" value="https://img.youtube.com/vi/6wL1RD_bS3M/mqdefault.jpg"/>
										</c:if>
									</dsp:oparam>
								</dsp:droplet>


								<img src="${videoPreview}" alt="">
							</a>

							<div class="cs-desc">
								<a href="${fn:replace(videoUrl,'watch?v=', 'v/')}" data-toggle="lightbox">
									<h4 class="hidden-xs"><dsp:valueof param="video.title"/></h4>
								</a>
								<h6><dsp:valueof param="video.time"/></h6>
								<p><dsp:valueof param="video.description"/></p>
							</div>

						</div>
					</div>

				</c:forEach>
			</div>

			<div class="col-xs-12 text-center">
				<nav id="pagination">
					<dsp:include src="/content/pages/gadgets/content-pages-pagination.jsp">
						<dsp:param name="numPerPage" value="6" />
						<dsp:param name="page" value="${page}" />
						<dsp:param name="total" value="${fn:length(videos)}" />
					</dsp:include>
				</nav>
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>