<dsp:page>
	
	<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
		<dsp:param name="queryRQL" value="categories INCLUDES :category" />
		<dsp:param name="repository" value="/cps/registry/repository/ContentPagesRepository" />
		<dsp:param name="itemDescriptor" value="prodinfo_item" />
		<dsp:param name="sortProperties" value="+itemName" />
		<dsp:param name="category" param="category" />
		<dsp:oparam name="outputStart">
				<div class="panel">
					<div class="panel-heading" role="tab" id="heading<dsp:valueof param="category.id" />">
						<a href="#collapse<dsp:valueof param="category.id" />" class="collapsed" role="button" data-toggle="collapse" data-parent="#pi-accordion" aria-expanded="false" aria-controls="collapse<dsp:valueof param="category.id" />">
							<dsp:valueof param="category.categoryName" />
						</a>
					</div>
					<div id="collapse<dsp:valueof param="category.id" />" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<dsp:valueof param="category.id" />" aria-expanded="false" style="height: 0px;">
						<div class="panel-body">
							<ul class="pdp-tools">
		</dsp:oparam>
		<dsp:oparam name="output">
								<li>
									<a href="<dsp:valueof param="element.itemUrl" />" target="_blank"><i class="icon icon-paper-list"></i><dsp:valueof param="element.itemName" /></a>
								</li>
		</dsp:oparam>
		<dsp:oparam name="outputEnd">
							</ul>
						</div>
					</div>
				</div>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>