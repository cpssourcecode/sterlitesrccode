<dsp:page>
	<dsp:getvalueof var="page" param="page"/>
	<dsp:getvalueof var="numPerPage" param="numPerPage"/>
	<dsp:getvalueof var="total" param="total"/>
	<dsp:getvalueof var="numPages" vartype="java.lang.Integer" value="${total/numPerPage}"/>

	<c:if test="${total%numPerPage != 0}">
		<dsp:getvalueof var="numPages" value="${numPages+1}"/>
	</c:if>

	<dsp:getvalueof var="set" value="false"/>
	<dsp:droplet name="/atg/dynamo/droplet/For">
		<dsp:param name="howMany" value="${total}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="count" param="count"/>
			<c:if test="${(count > numPages) && set == false}">
				<dsp:getvalueof var="numPages" value="${count-1}"/>
				<dsp:getvalueof var="set" value="true"/>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
	


	<c:if test="${numPages >= 2}">

		<ul class="pagination pagination-lg">

			<c:if test="${page > 1}">
				<li class="prev">
					<a href="#" aria-label="Previous" data-event-click-id="content44" data-event-click-param0="${(page-1)}" >
						<span class="glyphicon glyphicon-chevron-left"></span>
					</a>
				</li>
			</c:if>

			<c:choose>
				<c:when test="${numPages <=5}">
					<dsp:droplet name="/atg/dynamo/droplet/For">
						<dsp:param name="howMany" value="${total}"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<c:if test="${count <= numPages}">
								<c:choose>
									<c:when test="${page == count}">
										<li class="active"><a href="#" data-event-click-id="content45" data-event-click-param0="${count}" ><dsp:valueof param="count"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="#" data-event-click-id="content46" data-event-click-param0="${count}" ><dsp:valueof param="count"/></a></li>
									</c:otherwise>
								</c:choose>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</c:when>
				<c:otherwise>
					<c:if test="${(page > 4)}">
						<li><a href="#" data-event-click-id="content47" >1</a></li>
						<li><span>...</span></li>
					</c:if>
					<c:if test = "${(page <= 3)}">
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<c:choose>
									<c:when test="${page == count}">
										<li class="active">
											<a href="#" data-event-click-id="content48" data-event-click-param0="${count}" ><dsp:valueof value="${count}"/></a>
										</li>
									</c:when>
									<c:otherwise>
										<li>
											<a href="#" data-event-click-id="content49" data-event-click-param0="${count}" ><dsp:valueof value="${count}"/></a>
										</li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page > 3) && (page < (numPages - 2))}">
						<li><a href="#" data-event-click-id="content50" data-event-click-param0="${page-2}" ><dsp:valueof value="${page-2}"/></a></li>
						<li><a href="#" data-event-click-id="content51" data-event-click-param0="${page-1}" ><dsp:valueof value="${page-1}"/></a></li>
						<li class="active"><a href="#" data-event-click-id="content52" data-event-click-param0="${page}" ><dsp:valueof value="${page}"/></a></li>
						<li><a href="#" data-event-click-id="content53" data-event-click-param0="${page+1}" ><dsp:valueof value="${page+1}"/></a></li>
						<li><a href="#" data-event-click-id="content54" data-event-click-param0="${page+2}" ><dsp:valueof value="${page+2}"/></a></li>
					</c:if>
					<c:if test = "${(page >= (numPages - 2))}">
						<dsp:getvalueof var="start" value="${numPages-5}"/>
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<dsp:getvalueof var="start" value="${start+1}"/>
								<c:choose>
									<c:when test="${page == start}">
										<li class="active"><a href="#" data-event-click-id="content55" data-event-click-param0="${start}" ><dsp:valueof value="${start}"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="#" data-event-click-id="content56" data-event-click-param0="${start}" ><dsp:valueof value="${start}"/></a></li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page < (numPages - 2))}">
						<li><span>...</span></li>
						<li><a href="#" data-event-click-id="content57" data-event-click-param0="${numPages}" ><dsp:valueof value="${numPages}"/></a></li>
					</c:if>
				</c:otherwise>
			</c:choose>

			<c:if test="${page < numPages}">
				<li class="next">
					<a href="#" aria-label="Next" data-event-click-id="content58" data-event-click-param0="${(page+1)}" >
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				</li>
			</c:if>

		</ul>

	</c:if>

</dsp:page>