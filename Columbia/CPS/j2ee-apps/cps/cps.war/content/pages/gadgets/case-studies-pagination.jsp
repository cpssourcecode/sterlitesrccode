<dsp:page>
	<dsp:getvalueof var="page" param="page"/>
	<dsp:getvalueof var="numPerPage" param="numPerPage"/>
	<dsp:getvalueof var="total" param="total"/>
	<dsp:getvalueof var="numPages" vartype="java.lang.Integer" value="${total/numPerPage}"/>

	<c:if test="${total%numPerPage != 0}">
		<dsp:getvalueof var="numPages" value="${numPages+1}"/>
	</c:if>

	<dsp:getvalueof var="set" value="false"/>
	<dsp:droplet name="/atg/dynamo/droplet/For">
		<dsp:param name="howMany" value="${total}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="count" param="count"/>
			<c:if test="${(count > numPages) && set == false}">
				<dsp:getvalueof var="numPages" value="${count-1}"/>
				<dsp:getvalueof var="set" value="true"/>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
	
	<c:if test="${numPages >= 2}">

		<ul class="pagination">

			<c:if test="${page > 1}">
				<li class="prev">
					<a href="#" aria-label="Previous" data-event-click-id="content14" data-event-click-param0="${vsg_utils:escapeJS(page-1)}" >
						<span class="fa fa-angle-left"></span>
					</a>
				</li>
			</c:if>
			<c:if test="${page == 1}">
				<li class="prev">
					<a href="#" aria-label="Previous">
						<span class="fa fa-angle-left" style="display: none"></span>
					</a>
				</li>
			</c:if>

			<c:choose>
				<c:when test="${numPages <=5}">
					<dsp:droplet name="/atg/dynamo/droplet/For">
						<dsp:param name="howMany" value="${total}"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<c:if test="${count <= numPages}">
								<c:choose>
									<c:when test="${page == count}">
										<li class="active"><a href="#" data-event-click-id="content15" data-event-click-param0="${count}" ><dsp:valueof param="count"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="#" data-event-click-id="content16" data-event-click-param0="${count}" ><dsp:valueof param="count"/></a></li>
									</c:otherwise>
								</c:choose>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</c:when>
				<c:otherwise>
					<c:if test="${(page > 4)}">
						<li><a href="#" data-event-click-id="content17" >1</a></li>
						<li><span>...</span></li>
					</c:if>
					<c:if test = "${(page <= 3)}">
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<c:choose>
									<c:when test="${page == count}">
										<li class="active">
											<a href="#" data-event-click-id="content18" data-event-click-param0="${count}" ><dsp:valueof value="${count}"/></a>
										</li>
									</c:when>
									<c:otherwise>
										<li>
											<a href="#" data-event-click-id="content19" data-event-click-param0="${count}" ><dsp:valueof value="${count}"/></a>
										</li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page > 3) && (page < (numPages - 2))}">
						<li><a href="#" data-event-click-id="content20" data-event-click-param0="${vsg_utils:escapeJS(page-2)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page-2)}"/></a></li>
						<li><a href="#" data-event-click-id="content21" data-event-click-param0="${vsg_utils:escapeJS(page-1)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page-1)}"/></a></li>
						<li class="active"><a href="#" data-event-click-id="content22" data-event-click-param0="${vsg_utils:escapeJS(page)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page)}"/></a></li>
						<li><a href="#" data-event-click-id="content23" data-event-click-param0="${vsg_utils:escapeJS(page+1)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page+1)}"/></a></li>
						<li><a href="#" data-event-click-id="content24" data-event-click-param0="${vsg_utils:escapeJS(page+2)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page+2)}"/></a></li>
					</c:if>
					<c:if test = "${(page >= (numPages - 2))}">
						<dsp:getvalueof var="start" value="${numPages-5}"/>
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<dsp:getvalueof var="start" value="${start+1}"/>
								<c:choose>
									<c:when test="${page == start}">
										<li class="active"><a href="#" data-event-click-id="content25" data-event-click-param0="${start}" ><dsp:valueof value="${start}"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="#" data-event-click-id="content26" data-event-click-param0="${start}" ><dsp:valueof value="${start}"/></a></li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page < (numPages - 2))}">
						<li><span>...</span></li>
						<li><a href="#" data-event-click-id="content27" data-event-click-param0="${numPages}" ><dsp:valueof value="${numPages}"/></a></li>
					</c:if>
				</c:otherwise>
			</c:choose>

			<c:if test="${page < numPages}">
				<li class="next">
					<a href="#" aria-label="Next" data-event-click-id="content28" data-event-click-param0="${vsg_utils:escapeJS(page+1)}" >
						<span class="fa fa-angle-right"></span>
					</a>
				</li>
			</c:if>
			
			<c:if test="${page == numPages}">
				<li class="next">
					<a href="#" aria-label="Next">
						<span class="fa fa-angle-right" style="display: none"></span>
					</a>
				</li>
			</c:if>

		</ul>

	</c:if>

</dsp:page>