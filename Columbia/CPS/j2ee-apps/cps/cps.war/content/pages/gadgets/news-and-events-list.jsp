<dsp:page>
	<dsp:getvalueof var="start" param="start" />
	<dsp:getvalueof var="howMany" param="howMany" />
	<dsp:getvalueof var="page" param="${start}" />
	<c:if test="${start > 1}">
		<dsp:getvalueof var="rangeStart" value="${(start - 1) * howMany + 1}"/>
	</c:if>
		<c:if test="${start == 1}">
		<dsp:getvalueof var="rangeStart" value="1"/>
	</c:if>
	<dsp:droplet name="/cps/content/droplet/NewsAndEventsRangeDroplet">
		<dsp:param name="howMany" value="${howMany}"/>
		<dsp:param name="start" value="${rangeStart-1}"/>
		<dsp:oparam name="outputStart">

		</dsp:oparam>
		<dsp:oparam name="output">
			<dsp:getvalueof var="size" param="size" />


			<article class="news">
				<header>
					<h3 class="news-color">
						<dsp:valueof param="element.title"/>
					</h3>
					<div class="news-date">
						<dsp:getvalueof var="date" param="element.creationDate"/>
						<fmt:formatDate pattern="MM/dd/yyyy" value="${date}" />
					</div>
				</header>
				<div class="news-data row">					
					<div class="news-thumb col-sm-2 col-xs-3" style="float: left;" id="view_button">
						<dsp:getvalueof var="imgUri" param="element.imageUri"/>
						<dsp:getvalueof var="contentUri" param="element.additionalContentUri"/>
						<c:if test="${not empty imgUri}">
							<cp:readerimg src="${imgUri}" alt="" />
						</c:if>
						<c:choose>
							<c:when test="${not empty contentUri}">
								<a class="btn btn-info btn-block" href="<dsp:valueof value="${contentUri}" />" title="" target="_blank">View</a>
							</c:when>
							<c:when test="${empty contentUri and not empty imgUri}">
								<a class="btn btn-info btn-block" data-toggle="lightbox" href="<dsp:valueof value="${imgUri}" />" title="">View</a>
							</c:when>
						</c:choose>
					</div>
					<div id="news_content">
						<dsp:valueof param="element.textContent" valueishtml="true"/>
					</div>
				</div>
			</article>

		</dsp:oparam>
		<dsp:oparam name="outputEnd">
			<div class="text-center">
				<div class="pagination">
					<nav id="pagination">
						<dsp:include src="/content/pages/gadgets/content-pages-pagination.jsp">
							<dsp:param name="numPerPage" value="${howMany}" />
							<dsp:param name="page" value="${start}" />
							<dsp:param name="total" value="${size}" />
						</dsp:include>
					</nav>
				</div>
			</div>
			
			<div class="sidebar-block visible-xs visible-xxs" id="sidebar-events-copy">
			
			</div>
			
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>


