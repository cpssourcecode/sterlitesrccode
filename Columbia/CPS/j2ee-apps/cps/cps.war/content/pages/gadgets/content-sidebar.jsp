<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:getvalueof var="pageName" param="pageName" />

	<div class="col-md-3 col-xs-12">
		<div class="sidebar-menu">
			<ul class="sidebar-menu-container">
				<li <c:if test="${pageName == 'productInformation'}">class="active"</c:if>><a href="#">Product Information</a></li>
				<li <c:if test="${pageName == 'faq'}">class="active"</c:if>><a href="/content/pages/faq.jsp">FAQs</a></li>
				<li <c:if test="${pageName == 'videoLibrary'}">class="active"</c:if>><a href="#">Video Library</a></li>
			</ul>
		</div>
	</div>

</dsp:page>