<dsp:page>
	<dsp:importbean bean="/cps/content/droplet/YoutubePreviewDroplet" />

	<dsp:droplet name="/cps/content/droplet/InstructionalVideoDroplet">
		<dsp:param name="role" param="role" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="pages" param="pages" />

			<dsp:getvalueof var="pageNum" param="page" />
			<dsp:getvalueof var="activePage" value="${pages[pageNum]}" />

			<div class="row row-narrow cs">
				<c:forEach var="item" varStatus="videoItemCount" items="${activePage.videos}">
					<dsp:param name="videoItem" value="${item}" />

					<dsp:getvalueof var="videoRoles" value="" />
					<dsp:getvalueof var="roleLabel" value="Role" />
					<dsp:getvalueof var="itemRoles" param="videoItem.roles" />

					<c:forEach var="role" varStatus="roleStatus" items="${itemRoles}">
						<dsp:param name="role" value="${role}" />
						<dsp:getvalueof var="roleName" param="role.roleName" />
						<dsp:getvalueof var="videoRoles" value="${videoRoles}${roleName}" />
						<c:if test="${not roleStatus.last}">
							<dsp:getvalueof var="roleLabel" value="Roles" />
							<dsp:getvalueof var="videoRoles" value="${videoRoles}, " />
						</c:if>
					</c:forEach>

					<dsp:getvalueof var="videoUrl" param="videoItem.videoUrl" />
					<dsp:droplet name="YoutubePreviewDroplet">
						<dsp:param name="videoUrl" value="${videoUrl}" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="videoPreviewUrl" param="preview" />
							<c:if test="${empty videoPreviewUrl}">
								<dsp:getvalueof var="videoPreviewUrl" value="https://img.youtube.com/vi/6wL1RD_bS3M/mqdefault.jpg" />
							</c:if>
						</dsp:oparam>
					</dsp:droplet>

					<div class="col-xs-4 col-xxs-12">
						<div class="thumbnail">
							<a class="text-nodec resize-thumb" href="${fn:replace(videoUrl, 'watch?v=', 'v/')}" data-norelated data-toggle="lightbox">
								<img src="${videoPreviewUrl}" alt="" />
							</a>
							<div class="cs-desc youtubeVideo">
								<h4>
									<dsp:valueof param="videoItem.videoTitle" />
								</h4>
								<h6>
									<dsp:valueof param="videoItem.videoLength" />
								</h6>
								<%--<p>
									<dsp:valueof value="${roleLabel}" />
									:
									<dsp:valueof value="${videoRoles}" />
								</p>--%>
								<c:if test="${not empty topic1 or not empty topic2 or not empty topic3 or not empty topic4 or not empty topic5 or not empty topic6}">
									<p>
										Topics Covered:
										<c:if test="${not empty topic1}">
											<br />&bull;<dsp:valueof value="${topic1}" />
										</c:if>
										<c:if test="${not empty topic2}">
											<br />&bull;<dsp:valueof value="${topic2}" />
										</c:if>
										<c:if test="${not empty topic3}">
											<br />&bull;<dsp:valueof value="${topic3}" />
										</c:if>
										<c:if test="${not empty topic4}">
											<br />&bull;<dsp:valueof value="${topic4}" />
										</c:if>
										<c:if test="${not empty topic5}">
											<br />&bull;<dsp:valueof value="${topic5}" />
										</c:if>
										<c:if test="${not empty topic6}">
											<br />&bull;<dsp:valueof value="${topic6}" />
										</c:if>
									</p>
								</c:if>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
			<div class="col-xs-12 text-center">
				<nav id="pagination">
					<dsp:include src="/content/pages/gadgets/content-pages-pagination.jsp">
						<dsp:param name="numPerPage" value="1" />
						<dsp:param name="page" value="${pageNum}" />
						<dsp:param name="total" value="${pages.size()}" />
					</dsp:include>
				</nav>
			</div>
		</dsp:oparam>
		<dsp:oparam name="empty">
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>