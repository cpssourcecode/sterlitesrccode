<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:getvalueof var="faqCategory" param="faqCategory" />
    <dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
        <dsp:param name="queryRQL" value="ALL" />
        <dsp:param name="repository" value="/cps/registry/repository/ContentPagesRepository" />
        <dsp:param name="itemDescriptor" value="faq_category" />
        <dsp:param name="sortProperties" value="+displayOrder" />

        <dsp:oparam name="outputStart">
            <nav>
            <ul class="sidebar-nav">
        </dsp:oparam>

        <dsp:oparam name="output">
            <dsp:getvalueof var="categoryCount" param="size" />
            <dsp:getvalueof var="index" param="index" />
            <li>
                <a href="#" class="faq-content-link" data-faq-id="<dsp:valueof param="element.id" />">
                    <dsp:valueof param="element.categoryName" />
                </a>
            </li>
        </dsp:oparam>

        <dsp:oparam name="outputEnd">
            </ul>
            </nav>
        </dsp:oparam>

    </dsp:droplet>
    <script type="text/javascript" nonce="${requestScope.nonce}">
        $('.faq-content-link').click(function(){
            var faqCategoryId = $(this).data('faqId');
            $('#content').load('/content/pages/gadgets/faq-display.jsp?cat=' + faqCategoryId);
        });
    </script>
</dsp:page>