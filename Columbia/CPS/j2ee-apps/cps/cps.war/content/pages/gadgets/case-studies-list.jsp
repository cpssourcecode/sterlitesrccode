<dsp:page>
	<dsp:getvalueof var="start" param="start" />
	<dsp:getvalueof var="howMany" param="howMany" />
	<dsp:getvalueof var="page" param="${start}" />
	<c:if test="${start > 1}">
		<dsp:getvalueof var="rangeStart" value="${(start - 1) * howMany + 1}"/>
	</c:if>
		<c:if test="${start == 1}">
		<dsp:getvalueof var="rangeStart" value="1"/>
	</c:if>

	<div class="row row-narrow cs">
		<dsp:droplet name="/cps/content/droplet/CaseStudiesRangeDroplet">
			<dsp:param name="howMany" value="${howMany}" />
			<dsp:param name="start" value="${rangeStart-1}" />
			<dsp:oparam name="outputStart">
			</dsp:oparam>
			<dsp:oparam name="output">
				<dsp:getvalueof var="size" param="size" />
				<dsp:getvalueof var="title" param="element.title" />
				<dsp:getvalueof var="description" param="element.description" />
				<dsp:getvalueof var="imageUrl" param="element.imageUrl" />
				<dsp:getvalueof var="link" param="element.link" />

				<!-- CUBIC -->
				<div class="col-xs-4 col-xxs-12">
					<div class="thumbnail">
						<a href="${link}" class="text-nodec resize-thumb hidden-xs"> <cp:readerimg src="${imageUrl}"
							style="max-height: 300px; max-width: 400px" />
						</a>
						<div class="cs-desc">
							<h4>
								<dsp:valueof value="${title}" valueishtml="true" />
							</h4>
							<p>
								<dsp:valueof value="${description}" valueishtml="true" />
							</p>
						</div>

						<a class="btn btn-link btn-block" href="${link}">View Case Study</a>
					</div>
				</div>

			</dsp:oparam>
			<dsp:oparam name="outputEnd">
				<div class="col-xs-12 text-center">
					<nav id="pagination">
						<dsp:include src="/content/pages/gadgets/case-studies-pagination.jsp">
							<dsp:param name="numPerPage" value="${howMany}" />
							<dsp:param name="page" value="${start}" />
							<dsp:param name="total" value="${size}" />
						</dsp:include>
					</nav>
				</div>

			</dsp:oparam>
		</dsp:droplet>
	</div>

</dsp:page>