<dsp:page>
	<dsp:getvalueof var="start" param="start" />
	<dsp:getvalueof var="howMany" param="howMany" />
	<dsp:getvalueof var="page" param="${start}" />
	<c:if test="${start > 1}">
		<dsp:getvalueof var="rangeStart" value="${(start - 1) * howMany + 1}"/>
	</c:if>
		<c:if test="${start == 1}">
		<dsp:getvalueof var="rangeStart" value="1"/>
	</c:if>						
	<dsp:droplet name="/cps/content/droplet/TestimonialsRangeDroplet">
		<dsp:param name="howMany" value="${howMany}"/>
		<dsp:param name="start" value="${rangeStart-1}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="size" param="size" />
			
			<dsp:getvalueof var="description" param="element.quote" />
			<c:if test="${fn:length(description) > 500}">
				<dsp:getvalueof var="class" value="collapse-description"/>
			</c:if>
			<blockquote>
				<p>
					<dsp:valueof value="${description}" valueishtml="true"/>
				</p>
				<dsp:getvalueof var="person" param="element.person"/>
				<dsp:getvalueof var="organization" param="element.parentFolder.organization.name" />
				<c:choose>
					<c:when test="${not empty person && not empty organization}">
						<footer>
							<dsp:valueof param="element.person" />,&nbsp;<dsp:valueof param="element.parentFolder.organization.name" />
						</footer>
					</c:when>
					<c:when test="${not empty person}">
						<footer> 
							<dsp:valueof param="element.person" />
						</footer>
					</c:when>
					<c:when test="${not empty organization}">
						<footer>
							<dsp:valueof param="element.parentFolder.organization.name" />
						</footer>
					</c:when>
					<c:otherwise>
						&nbsp;
					</c:otherwise>
				</c:choose>
				<br>
				<dsp:getvalueof var="link" param="element.caseStudylink"/>
				<c:if test="${not empty link}">
					<a class="btn btn-info" href="${link}">View Case Study</a>
				</c:if>
			</blockquote>
		</dsp:oparam>
		<dsp:oparam name="outputEnd">
			<div class="text-center">
				<nav id="pagination">
					<dsp:include src="/content/pages/gadgets/content-pages-pagination.jsp">
						<dsp:param name="numPerPage" value="${howMany}" />
						<dsp:param name="page" value="${start}" />
						<dsp:param name="total" value="${size}" />
					</dsp:include>
				</nav>
			</div>
			
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>