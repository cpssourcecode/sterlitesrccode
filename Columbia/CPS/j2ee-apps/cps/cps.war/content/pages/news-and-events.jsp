<dsp:page>

	<div id="news-and-events">
		<dsp:include src="gadgets/news-and-events-list.jsp">
			<dsp:param name="howMany" value="8" />
			<dsp:param name="start" value="1" />
		</dsp:include>

		<script nonce="${requestScope.nonce}">
			function onNewsAndEventsLoaded(){
                $('.collapse-description')
                    .on(
                        'click',
                        '.more',
                        function(event) {
                            event.preventDefault();
                            var parentCover = $(this).parent(
                                '.collapse-description'), newHeight = parentCover
                                .children('p').height();

                            parentCover.toggleClass('full');

                            if (parentCover.hasClass('full')) {
                                parentCover.css('height', newHeight);
                                $(this).children('span').text('Less');
                            } else {
                                parentCover.css('height', '');
                                $(this).children('span').text('More');
                            }
                            $(this).blur();
                        });

                $("#sidebar-events-copy").html($("#sidebar-events").html());
                $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
                    event.preventDefault();
                    $(this).ekkoLightbox();
                });

                $(document).ready(function() {
                    var a = $('.news-data.row').toArray();

                    for (i = 0; i < a.length; i++) {
                        var viewBtnDiv = $(a[i]).children('#view_button');
                        var newsCntDiv = $(a[i]).children('#news_content');

                        var children = $(newsCntDiv).children();

                        if (children.length >= 3) {
                            $(viewBtnDiv).detach();
                            $(newsCntDiv).detach();
                            //top content
                            $(a[i]).append('<div class="col-xs-12"></div>');
                            //view button
                            $(a[i]).append(viewBtnDiv);
                            //bottom right content
                            $(a[i]).append('<div class="col-sm-10 col-xs-9"></div>');

                            for (j = 0; j < children.length - 2; j++) {
                                var workElem = children[j];
                                $(workElem).detach();
                                $('.col-xs-12').append(workElem);
                            }
                            for (j = 2; j < children.length; j++) {
                                var workElem = children[j];
                                $(workElem).detach();
                                $('.col-sm-10.col-xs-9').append(workElem);
                            }
                        }
                    }
                });
			}
            onNewsAndEventsLoaded();
		</script>
	</div>
	
	<script type="text/javascript" nonce="${requestScope.nonce}">
		var currentPage = 1;
		function changePage(page) {
			$("#news-and-events").load("/content/pages/gadgets/news-and-events-list.jsp?howMany=8&start=" + page, function () {
                onNewsAndEventsLoaded();
				eventsContentPagesPagination();
            });
		}
	</script>
	
</dsp:page>