<dsp:page>
	<div id="video-library">
		<dsp:include src="gadgets/video-library-list.jsp">
			<dsp:param name="page" value="1" />
		</dsp:include>
	</div>
	
	<script type="text/javascript" nonce="${requestScope.nonce}">
		var currentPage = 1;
		function changePage(page) {
			$("#video-library").load("/content/pages/gadgets/video-library-list.jsp?page=" + page, function () {
				eventsContentPagesPagination();
            });
		}
		$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox();
		});
	</script>
</dsp:page>