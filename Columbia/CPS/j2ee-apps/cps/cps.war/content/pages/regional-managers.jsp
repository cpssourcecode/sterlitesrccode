<dsp:page>
	<dsp:importbean bean="/cps/content/droplet/QueryRegionalManagersDroplet" />
	<div class="affiliation-list">
		<dsp:droplet name="QueryRegionalManagersDroplet">
			<dsp:oparam name="output">
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" param="repositoryItems"/>
					<dsp:param name="elementName" value="currentItem"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="email" param="currentItem.email"/>
						<dsp:getvalueof var="imageUri" param="currentItem.imageUri"/>
						<dsp:getvalueof var="localtion" param="currentItem.localtion"/>
						<dsp:getvalueof var="managerDescription" param="currentItem.managerDescription"/>
						<dsp:getvalueof var="name" param="currentItem.name"/>
						<dsp:getvalueof var="phone" param="currentItem.phone"/>
						<dsp:getvalueof var="title" param="currentItem.title"/>

						<div class="row manager">
							<div class="col-sm-3 col-xs-4 col-xxs-12">
								<!-- <img src="${staticContentPrefix}http://fpoimg.com/400x300" alt="" class="mb10"> -->
								<cp:readerimg src="${imageUri}" alt="" iclass="img-responsive mb10" />
							</div>
							<div class="col-sm-9 col-xs-8 col-xxs-12">
								<h3>${name}</h3>
								<h4>${title}</h4>
								<address>
									<c:if test="${not empty localtion}">
										<strong>Location</strong>
										<p>${localtion}</p>
									</c:if>
									
									<c:if test="${not empty email}">
										<strong>Email</strong>
										<p><a href="javascript:void(0);" class="rm-contact-trigger-link">${email}</a></p>
									</c:if>
									<c:if test="${not empty phone}">
										<strong>Phone</strong>
										<p>${phone}</p>
									</c:if>
								</address>
								
								<div class="item">
									<c:set var="addMore" value="${false}"/>
									<c:if test="${fn:length(managerDescription) > 200}">
										<c:set var="addMore" value="${true}"/>
										<dsp:getvalueof var="class" value="collapse-description"/>
									</c:if>
									<article class="${class}">
										${managerDescription}
										<%-- <c:if test="${not empty class}">
											<a class="more" href="#"><span>More</span></a>
										</c:if> --%>
										<c:if test="${addMore}">
											<a class="more" href="#"><span>More</span></a>
										</c:if>
									</article>
									<dsp:getvalueof var="class" value=""/>
								</div>
							</div>
							<dsp:include src="/content/pages/gadgets/regional-manager-contact-us.jsp">
								<dsp:param name="regionalManager" param="currentItem" />
							</dsp:include>
						</div> 
						
					</dsp:oparam>
					<dsp:oparam name="empty">
						No items.
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
	</div>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		/* $('.collapse-description').on('click', '.more', function(event) {
		  event.preventDefault();
		  var parentCover = $(this).parent('.collapse-description'), newHeight = 0;
		  
		  parentCover.children().each(function(){newHeight += $(this).height();});

		  parentCover.toggleClass('full');

		  if ( parentCover.hasClass('full') ) {
		      parentCover.css('height', newHeight);
		      $(this).children('span').text('Less');
		  } else {
		      parentCover.css('height', '');
		      $(this).children('span').text('More');
		  }
		  $(this).blur();
		 
		}); */
		$(function(){ /* to make sure the script runs after page load */

			$('.itemDel').each(function(event){ /* select all divs with the item class */
				var max_length = 200; /* set the max content length before a read more link will be added */
				if($(this).html().length > max_length){ /* check for content length */
					var short_content 	= $(this).html().substr(0, max_length); /* split the content in two parts */
					var long_content	= $(this).html().substr(max_length);
					$(this).html(short_content+
								 '<a href="#" class="read_more"><br/>More</a>'+
								 '<span class="more_text" style="display:none;">'+long_content+'</span>'); /* Alter the html to allow the read more functionality */
								 
					$(this).find('a.read_more').click(function(event){ /* find the a.read_more element within the new html and bind the following code to it */
						event.preventDefault(); /* prevent the a from changing the url */
						$(this).hide(); /* hide the read more button */
						$(this).parents('.item').find('.more_text').show(); /* show the .more_text span */
					});
				}
			});
		});
	</script>
</dsp:page>