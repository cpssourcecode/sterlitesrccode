<dsp:page>
	<dsp:importbean bean="/cps/content/handler/TestimonialsFormHandler"/>
	<div id="testimonials">
		<dsp:include src="gadgets/testimonials-list.jsp">
			<dsp:param name="howMany" value="10" />
			<dsp:param name="start" value="1" />
		</dsp:include>
	</div>
	
	
		<div class="well well-grey visible-xs">
		<h4>Submit a Testimonial</h4>
		<div class="sidebar-form">
			<div class="row">
				<div class="col-xs-4 col-xxs-12">
					<div class="form-group">
						<input id="testimonialFirstName_main" value="" type="text" class="form-control" placeholder="First Name*" maxlength="20" required=""/>
					</div>
				</div>
				<div class="col-xs-4 col-xxs-12">
					<div class="form-group">
						<input id="testimonialLastName_main" value="" type="text" class="form-control" placeholder="Last Name*" maxlength="20" required=""/>
					</div>
				</div>
				<div class="col-xs-4 col-xxs-12">
					<div class="form-group">
						<input id="testimonialCompany_main" type="text" class="form-control" placeholder="Company*" maxlength="30" required="" />
					</div>
				</div>
				<div class="col-xs-4 col-xxs-12">
					<div class="form-group">
						<input id="testimonialEmail_main" value="" type="email" class="form-control" placeholder="Email (Optional)" maxlength="50" required="" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<textarea class="form-control" name="message"
					id="testimonialMessage_main" cols="6" rows="5" maxlength="250"
					placeholder="Testimonial Message* (Max 250 Characters)"
					required=""></textarea>
			
			</div>
			<div class="row">
				<div class="col-xs-4 col-xxs-12">
					<button type="submit" class="btn btn-primary btn-block" data-event-click-id="content13" >
						Submit</button>
					</div>
				</div>
			</div>	
		</div>
		<dsp:form action="#" method="post" id="testimonial-form_main" formid="testimonial-form_main">
			<dsp:input value="" type="hidden" id="firstName_hidden_main" bean="TestimonialsFormHandler.value.testimonialFirstName"/>
			<dsp:input value="" type="hidden" id="lastName_hidden_main" bean="TestimonialsFormHandler.value.testimonialLastName"/>
			<dsp:input value="" type="hidden" id="email_hidden_main" bean="TestimonialsFormHandler.value.testimonialEmail"/>
			<dsp:input value="" type="hidden" id="company_hidden_main" bean="TestimonialsFormHandler.value.testimonialCompany"/>
			<dsp:input value="" type="hidden" id="message_hidden_main" bean="TestimonialsFormHandler.value.testimonialMessage"/>
			<dsp:input type="hidden" bean="TestimonialsFormHandler.submitTestimonial" value="true" priority="-10"/>
		</dsp:form>
	
	
	<script type="text/javascript" nonce="${requestScope.nonce}">
		var currentPage = 1;
		function changePage(page) {
			$("#testimonials").load("/content/pages/gadgets/testimonials-list.jsp?howMany=10&start=" + page, function () {
				eventsContentPagesPagination();
            });
		}
	</script>
	
</dsp:page>