<dsp:page>
	<dsp:getvalueof var="content"
		vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" var='shoppingCart' />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/droplet/LocationByBranchLookupDroplet" />
	<dsp:importbean bean="/cps/endeca/CPSSearchFormHandler" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean bean="/cps/util/CPSSessionBean" />
	<dsp:importbean bean="/cps/droplet/OrderApprovalsDroplet" />
	<dsp:importbean bean="/cps/droplet/OrderRequestsDroplet" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/cps/droplet/SavedCartsDroplet" />
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet" />
	<dsp:importbean bean="/atg/userprofiling/ProfileTools" />
	<dsp:importbean bean="/cps/util/CPSGlobalProperties" />
	<dsp:importbean bean="/atg/commerce/endeca/cache/DimensionValueCacheDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/cps/droplet/OrganizationAliasDroplet"/>
	
	
	<dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>
	<dsp:getvalueof var="searchDropdownShade" bean="CPSGlobalProperties.searchDropdownShade" />
	<dsp:getvalueof var="allowNewUserSelfRegistration"
		bean="ProfileTools.allowNewUserSelfRegistration" />
	<dsp:getvalueof var="showPaymentMethod"
		bean="ProfileTools.showPaymentMethod" />

	<dsp:getvalueof var="invalidItemList"
		bean="CPSSessionBean.invalidItemList" />
	<dsp:param name="duplicateProducts"
		bean="CPSSessionBean.quickOrderDuplicates" />
	<dsp:param name="hasQuickOrderDuplicates"
		bean="CPSSessionBean.hasQuickOrderDuplicates" />
	<dsp:getvalueof var="isHasAddedNewProductToShoppingCart"
		bean="CPSSessionBean.hasAddedNewProductToShoppingCart" />

	<dsp:getvalueof var="isTransient" bean="Profile.transient" />
	<dsp:getvalueof var="securityStatus" bean="Profile.securityStatus" />
	<dsp:getvalueof var="transient"
		value="${isTransient or securityStatus eq 1}" />
	<dsp:getvalueof var="showHeaderLinks" value="true" />

	<dsp:droplet name="RepriceOrderDroplet">
		<dsp:param name="pricingOp" value="ORDER_TOTAL" />
	</dsp:droplet>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>

	<%--Parameters from the  user reset password url --%>
	<dsp:getvalueof var="ln" param="ln" />
	<dsp:getvalueof var="pr" param="pr" />
	<dsp:getvalueof var="rd" param="rd" />
	<c:if test="${not empty ln && not empty pr && not empty rd}">
		<dsp:droplet name="/cps/droplet/ChangePasswordDroplet">
			<dsp:param name="ln" value="${ln}" />
			<dsp:param name="pr" value="${pr}" />
			<dsp:param name="rd" value="${rd}" />
			<dsp:oparam name="empty">
				<dsp:include
					page="${originatingRequest.contextPath}/global/modals/modal-password-reset.jsp" />
			</dsp:oparam>
			<dsp:oparam name="success">
				<dsp:include
					page="${originatingRequest.contextPath}/global/modals/modal-password-reset.jsp">
					<dsp:param name="user" param="user" />
				</dsp:include>
			</dsp:oparam>
			<dsp:oparam name="error">
				<dsp:include
					page="${originatingRequest.contextPath}/global/modals/modal-password-reset.jsp">
					<dsp:param name="user" param="user" />
					<dsp:param name="errorCode" param="errorCode" />
					<dsp:param name="error" value="${true}" />
				</dsp:include>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
	
	
	<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
		<dsp:param name="userId" bean="Profile.id" />
		<dsp:param name="role" value="superAdmin" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="superAdmin" value="true" />
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
		<dsp:param name="userId" bean="Profile.id" />
		<dsp:param name="role" value="regularAdmin" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="regularAdmin" value="true" />
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
		<dsp:param name="userId" bean="Profile.id" />
		<dsp:param name="role" value="superAdmin,regularAdmin" />
		<dsp:oparam name="true">
			<c:choose>
				<c:when test="${not empty selectedOrg}">
					<dsp:include page="/includes/gadgets/csr-header.jsp" />
				</c:when>
				<c:otherwise>
					<dsp:getvalueof var="showHeaderLinks" value="false" />
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:getvalueof var="showOrderShippingAddressForSession"
		bean="CPSGlobalProperties.showOrderShippingAddressForSession" />
	<dsp:getvalueof var="requestURI" bean="OriginatingRequest.requestURI" />
	<dsp:droplet name="/cps/droplet/CPSPageHeaderDroplet">
		<dsp:param name="currentPage" value="${requestURI}" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="isCheckoutPage" param="isCheckoutPage" />
			<dsp:getvalueof var="isHomePage" param="isHomePage" />
			<dsp:getvalueof var="isCartPage" param="isCartPage" />
		</dsp:oparam>
	</dsp:droplet>
	<input id="isHomePage" type="hidden" value="${isHomePage}"/>
	<input id="isCartPage" type="hidden" value="${isCartPage}"/>
	<input id="transient" type="hidden" value="${transient}"/>	
	<dsp:getvalueof var="queryString" value="${originatingRequest.queryString}" />
	<c:if test="${not empty queryString}">
		<dsp:getvalueof param="originalURL" var="url" />
		<c:if test="${not empty url}">
			<input type="hidden" value="${url}" id="originalURL"/>
		</c:if>
	</c:if>
	<%-- SM-356 Credit card only account notification --%>
	<dsp:droplet name="/cps/droplet/CreditCardAccountNotificationDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="isCCNotificationAccepted" param="isCCNotificationAccepted"/>
			<dsp:getvalueof var="isCCNotificationShown" param="isCCNotificationShown"/>
		</dsp:oparam>
	</dsp:droplet>

	<div id="stick">
		<nav id="top" class="navbar navbar-static-top">
			<div class="container">
				<c:if test="${!isTransient}">
					<dsp:getvalueof var="userParentOrg" bean="Profile.parentOrganization"/>
					<dsp:getvalueof var="userOrgId" value="${userParentOrg.repositoryId}"/>
					<dsp:getvalueof var="lookup" value="Lookup" />
					<dsp:droplet name="OrganizationAliasDroplet">
						<dsp:param name="profile" bean="Profile"/>
						<dsp:oparam name="true">
							<dsp:getvalueof var="lookup" value="Lookup_${userOrgId}" />
						</dsp:oparam>
					</dsp:droplet>
					<input type="hidden" id="lookup" value="${lookup}"/>
				</c:if>
				<c:choose>
					<c:when
						test="${!isTransient && !superAdmin && isCheckoutPage=='false'}">
						<ul class="nav navbar-nav">
							<li id="header-ship-to">
							<dsp:include page="/includes/gadgets/header-ship-to.jsp"/>
							<li>
							<dsp:droplet
									name="/cps/droplet/CPSBranchPhoneNumberDroplet">
									<dsp:param bean="Profile" name="profile" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="phone" param="phoneNumber" />
									</dsp:oparam>
								</dsp:droplet> <span class="nav-text"> <span class="top-label">Sales
										Team:</span> ${phone}
							</span></li>
						</ul>
					</c:when>
				</c:choose>
				<ul class="nav navbar-nav navbar-right">
					<c:choose>
						<c:when test="${isTransient}">
							<c:if test="${allowNewUserSelfRegistration}">
								<li><a href data-event-click-id="eventGoToRegistrationPage">
										<span class="top-label">Register</span>
								</a></li>
							</c:if>
							<li><a href="#" data-dismiss="modal" data-toggle="modal" id="loggedIn"
								data-target="#logIn"> <span class="top-label">Log In</span>
							</a></li>
						</c:when>
						<c:when test="${superAdmin && !showHeaderLinks}">
							<li><span class="nav-text"> Welcome, <strong>
										<dsp:valueof bean="Profile.firstName" />
								</strong>
							</span></li>
							<li><a href="#" data-event-click-id="content0"> <span
									class="top-label">Log Out</span>
							</a></li>
						</c:when>
						<c:otherwise>
							<li><span class="nav-text"> Welcome, <strong>
										<dsp:valueof bean="Profile.firstName" />
								</strong> <dsp:valueof bean="Profile.currentOrganization.name" />
							</span></li>

							<c:if test="${empty selectedOrg}">
								<li class="top-account-logout" data-event-click-id="content1">
									<span class="header-link">LOG OUT</span>
								</li>
							</c:if>

							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-haspopup="true"
								aria-expanded="false"> <span class="top-label">My
										Account</span> <i class="fa fa-angle-down"></i>
							</a> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey" value="acc-manage-orders" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="ordersHistory" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey"
										value="acc-manage-orders-approvals" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="pendingApprovals" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey"
										value="acc-manage-orders-requests" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="pendingRequests" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey"
										value="quick-tools-approver-buyer-super" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="quickTools" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey" value="acc-lists" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="lists" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey" value="acc-billing-payment" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="billingInvoices" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey"
										value="acc-billing-payment-statements" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="billingStatements" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey" value="acc-my-profile" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="myProfile" value="true" />
									</dsp:oparam>
								</dsp:droplet> <dsp:droplet name="/cps/droplet/AccessRightDroplet">
									<dsp:param name="profile" bean="Profile" />
									<dsp:param name="accessRightKey" value="acc-admin-tools" />
									<dsp:oparam name="true">
										<dsp:getvalueof var="adminTools" value="true" />
									</dsp:oparam>
								</dsp:droplet>
								<!-- fix SM-520 New User Role - open -->
                                <dsp:droplet name="/cps/droplet/AccessRightDroplet">
                                  <dsp:param name="profile" bean="Profile" />
                                  <dsp:param name="accessRightKey" value="acc-manage-orders-auto-reorder" />
                                  <dsp:oparam name="true">
                                    <dsp:getvalueof var="manageOrdersAutoReorder" value="true" />
                                  </dsp:oparam>
                                </dsp:droplet>
                                <dsp:droplet name="/cps/droplet/AccessRightDroplet">
                                  <dsp:param name="profile" bean="Profile" />
                                  <dsp:param name="accessRightKey" value="quick-tools-approver-buyer-super-order-pad" />
                                  <dsp:oparam name="true">
                                    <dsp:getvalueof var="quickToolsOrderPad" value="true" />
                                  </dsp:oparam>
                                </dsp:droplet>
								<!-- fix SM-520 New User Role - close -->
								<div class="dropdown-menu top-account">
									<div class="row row-flush">
										<div class="col-sm-6">
											<c:if test="${ordersHistory}">
												<h6>Manage Orders</h6>
												<ul>
													<li><a href="/account/order-list.jsp"> <i
															class="icon icon-folder-pair"></i> Order History
													</a></li>
													<c:if test="${pendingApprovals}">
														<dsp:getvalueof var="ordersSize" value="${0}" />
														<dsp:droplet name="OrderApprovalsDroplet">
															<dsp:param name="profile" bean="Profile" />
															<dsp:oparam name="output">
																<dsp:getvalueof var="ordersSize" param="ordersSize" />
															</dsp:oparam>
														</dsp:droplet>
														<li><a href="/account/pending-approvals.jsp"> <i
																class="icon icon-folder-single"></i> Pending Order
																Approvals (<dsp:valueof value="${ordersSize}" />)
														</a></li>
													</c:if>
													<c:if test="${pendingRequests}">
														<dsp:getvalueof var="ordersSize" value="${0}" />
														<dsp:droplet name="OrderRequestsDroplet">
															<dsp:param name="profile" bean="Profile" />
															<dsp:oparam name="output">
																<dsp:getvalueof var="ordersSize" param="ordersSize" />
															</dsp:oparam>
														</dsp:droplet>
														<li><a href="/account/order-requests.jsp"> <i
																class="icon icon-folder-motion"></i> Pending Order
																Requests (<dsp:valueof value="${ordersSize}" />)
														</a></li>
													</c:if>
													<!-- fix SM-520 New User Role -->
													<c:if test="${manageOrdersAutoReorder}">
													<li><dsp:droplet
															name="/cps/droplet/CPSActiveAutoOrdersDroplet">
															<dsp:param name="profile" bean="Profile" />
															<dsp:param name="loadList" value="false" />
															<dsp:oparam name="output">
																<dsp:getvalueof var="activeAutoOrders"
																	param="activeAutoOrders" />
															</dsp:oparam>
														</dsp:droplet> <a href="/account/account-landing.jsp?tab=autoOrders">
															<i class="fa fa-list"></i> Auto-Reorder
															(${activeAutoOrders})
													</a></li>
													</c:if>
													<li><a href="/account/packing-list.jsp"> <i
															class="icon icon-paper-list"></i> Packing Slips
													</a></li>
												</ul>
											</c:if>
											<c:if test="${quickTools}">
												<h6>Quick Tools</h6>
												<ul>
												<!-- fix SM-520 New User Role -->
													 <c:if test="${quickToolsOrderPad}">
													<li class="dropdown"><a
														href="/account/quick-order.jsp"> <i
															class="icon icon-clipboard"></i> Order Pad
													</a></li>
                                                    </c:if>
													<li class="dropdown"><a
														href="/account/request-quote.jsp"> <i
															class="icon icon-quote"></i> Request a Quote
													</a></li>
													<li><a href="/account/mtr-list.jsp"> <i
															class="icon icon-paper-check"></i> Material Test Reports
													</a></li>
												</ul>
											</c:if>
											<c:if test="${lists}">
												<h6>Lists</h6>
												<ul>
													<li><a href="/account/material-lists.jsp"> <i
															class="icon icon-user-material"></i> My Material Lists
													</a></li>
<%-- 													<li><a href="/account/order-carts.jsp"> <i
															class="glyphicon glyphicon-shopping-cart"></i> <dsp:droplet
																name="SavedCartsDroplet">
																<dsp:param name="profile" bean="Profile" />
																<dsp:oparam name="output">
																	<dsp:getvalueof var="savedCartsCount"
																		param="ordersSize" />
																		Saved Carts (${savedCartsCount})
																	</dsp:oparam>
																<dsp:oparam name="empty">
																		Saved Carts (0)
																	</dsp:oparam>
															</dsp:droplet> 
													</a></li>--%>
												</ul>
											</c:if>
										</div>
										<div class="col-sm-6">
											<c:if test="${billingInvoices}">
												<h6>Billing / Payments</h6>
												<ul>
													<li><a href="/account/invoices.jsp"> <i
															class="icon icon-paper-header"></i> Invoices
													</a></li>
													<c:if test="${billingStatements}">
														<li><a href="/account/statements.jsp"> <i
																class="icon icon-paper-list"></i> Statements
														</a></li>
													</c:if>
													<%-- <li><a href="/global/credit/application.jsp"> <i
															class="icon icon-paper-list"></i> Credit Application
													</a></li> --%>
												</ul>
											</c:if>
											<h6>My Profile</h6>
											<ul>
												<li><a href="/account/account-landing.jsp"> <i
														class="fa fa-list"></i> My Account Overview
												</a></li>
												<c:if test="${myProfile}">
													<li><a href="/account/addresses.jsp"> <i
															class="fa fa-map-marker ml5"></i> Default Shipping Locations
													</a></li>
													<li><a href="/account/profile-settings.jsp"> <i
															class="icon icon-user"></i> Edit Profile
													</a></li>
													<li><c:if test="${showPaymentMethod}">
															<a href="/account/profile-payment-method.jsp"> <i
																class="fa fa-credit-card"></i> Payment Method
															</a>
														</c:if></li>
												</c:if>
											</ul>
											<c:if test="${adminTools}">
												<h6>Admin Tools</h6>
												<ul>
													<li><a href="/account/manage-users.jsp"> <i
															class="icon icon-users"></i> Manage Users
													</a></li>
												</ul>
											</c:if>
											<!-- <h6 class="top-account-logout" data-event-click-id="content2" >
												<a href="#">Log Out</a>
											</h6> -->

										</div>
									</div>
								</div></li>
						</c:otherwise>
					</c:choose>
				</ul>
			</div>
		</nav>
		<header id="header">
			<div class="container">
				<div class="row">
					<dsp:renderContentItem contentItem="${contentItem.logo}">
						<dsp:param name="showHeaderLinks" value="${showHeaderLinks}" />
					</dsp:renderContentItem>
					<div class="col-md-7 header-nav text-right hidden-sm hidden-xs">
						<c:if test="${showHeaderLinks}">
							<dsp:renderContentItem contentItem="${contentItem.links}" />
						</c:if>
					</div>
				</div>
			</div>
		</header>

		<c:if test="${isCheckoutPage=='false'}">
			<nav id="nav" class="navbar navbar-inverse">
				<div class="container">
					<c:if
						test="${(!superAdmin && !regularAdmin) || (csrClass == 'admin-access')}">
						<div class="navbar-header visible-xs visible-sm">
							<button data-toggle="collapse" data-target="#navigation"
								aria-expanded="false">
								<i class="fa fa-bars"></i>
							</button>
						</div>
						<%-- Webservice will be called after page load, if ajax is enabled --%>
						<dsp:getvalueof var="ajaxEnabled"
							bean="CPSGlobalProperties.ajaxEnabledForPricingServiceAvailabilityCheck" />
						<input id="ajaxEnabled" name="ajaxEnabled" type="hidden" value="${ajaxEnabled}">
						<c:if test="${!ajaxEnabled}">
							<dsp:include page="/xhr/productPricingAvailabilityInfo.jsp" />
						</c:if>
						<dsp:droplet name="/cps/droplet/AllProductsDroplet">
							<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
							<dsp:param name="catCount" value="-1" />
							<dsp:param name="subCatCount" value="-1" />
							<dsp:param name="sortProperties" value="displayName" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="rootCategoriesMap" param="rootCategoriesMap"/>
							</dsp:oparam>
						</dsp:droplet>
						<dsp:getvalueof var="headerSearchCategories" bean="/atg/multisite/Site.searchCategories"/> 
						<dsp:getvalueof var="rootCategories" bean="/atg/multisite/Site.defaultCatalog.allRootCategoryIds"/>
						<c:set var="searchCategories" value=""/>
						<c:choose>
							<c:when test="${not empty headerSearchCategories}">
								<c:set var="searchCategories" value="${headerSearchCategories}"/>
							</c:when>
							<c:otherwise>
								<c:set var="searchCategories" value="${rootCategories}"/>
							</c:otherwise>
						</c:choose>
						<dsp:getvalueof var="endecaRequestURI" value="${requestScope['javax.servlet.forward.request_uri']}"/>
				        <dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog"/>
						<dsp:form id="searchform" formid="globalSearchForm" method="post" action="/">
							<div class="input-group category_search_group">
                                <div class="category_warp">
                                    <select class="resizeselect form-control ${searchDropdownShade} archive" id="headerSearchCategory"> 
                                    <option value="" disabled selected>Search In</option> 
                                    <option value="all">All</option>
                                        <dsp:droplet name="ForEach">
                                        <dsp:param name="array" value="${searchCategories}"/> 
                                        <dsp:param name="elementName" value="categoryId"/>
                                        <dsp:oparam name="output">
                                            <dsp:droplet name="/atg/commerce/catalog/CategoryLookup">
                                            <dsp:param name="id" param="categoryId"/>
                                            <dsp:param name="elementName" value="category"/>
                                            <dsp:oparam name="output">
                                                <dsp:getvalueof var="displayName" param="category.displayName" />
                                            </dsp:oparam>
                                            </dsp:droplet>
                                            <dsp:getvalueof var="currentCategoryId" param="categoryId"/>
                                            <c:if test="${currentCategoryId ne 'ZZZ'}">
	                                            <dsp:droplet name="DimensionValueCacheDroplet">
	                                            <dsp:param name="repositoryId" param="categoryId"/>
	                                                <dsp:oparam name="output">
	                                                <dsp:getvalueof var="topLevelCategoryCacheEntry" param="dimensionValueCacheEntry" />
	                                                <c:choose>
	                                                <c:when test="${fn:containsIgnoreCase(endecaRequestURI, topLevelCategoryCacheEntry.url)}">
	                                                    <option value="${topLevelCategoryCacheEntry.url}" data-dimvalid="${topLevelCategoryCacheEntry.dimvalId}" selected="true">${displayName}</option>
	                                                </c:when>
	                                                <c:otherwise>
	                                                <option value="${topLevelCategoryCacheEntry.url}" data-dimvalid="${topLevelCategoryCacheEntry.dimvalId}" >${displayName}</option> 
	                                                </c:otherwise>
	                                                </c:choose>
	                                                </dsp:oparam>
	                                            </dsp:droplet>
                                            </c:if>
                                        </dsp:oparam>
                                        </dsp:droplet>
                                    </select> 
                                    <select id="compute_select">
                                      <option id="compute_option"></option>
                                    </select>
                                </div> 
								<dsp:getvalueof var="enableAutoSuggestion"
									bean="CPSGlobalProperties.enableAutoSuggestion" />
								<input id="enableAutoSuggestion" name="enableAutoSuggestion"
									type="hidden" value="${enableAutoSuggestion}"/>
                                <div class="searchbox_warp">
                                    <dsp:input id="headerSearchQuestion" name="headerSearchQuestion"
                                        bean="CPSSearchFormHandler.question" type="text" 
                                        iclass="form-control placeholder_change" maxlength="40">
                                        <dsp:tagAttribute name="data-event-keypress-id"
                                            value="content8" />
                                        <dsp:tagAttribute name="placeholder"
                                            value="Keyword, Item #, or Mfr #" />
                                        <dsp:tagAttribute name="data-placeholder-short" value="Search" />
                                        <dsp:tagAttribute name="data-placeholder-long"
                                            value="Keyword, Item #, or Mfr #" />
                                    </dsp:input>
                                </div>
                                <dsp:input type="hidden" bean="CPSSearchFormHandler.headerCategory" id="headerCategory"/>
                                <input type="hidden" id="dimVal" value="0"/>
								<dsp:input type="hidden" bean="CPSSearchFormHandler.search"
									value="" priority="-10" />

								<div class="input-group-btn">
									<button type="button" class="btn" id="search-btn"  
										data-event-click-id="content3">
										<i class="fa fa-search header_search_icon"></i>
									</button>
								</div>
							</div>
							<div id="ta" class="col-sm-8" style="display: none"></div>
						</dsp:form>

						<div style="display: none">
							<dsp:form id="addToCartFromTypeahead"
								formid="addToCartFromTypeahead" action="" method="post">
								<dsp:input bean="CartModifierFormHandler.addItemCount"
									type="hidden" value="1" priority="10" />
								<dsp:input id="qtyFromTypeahead"
									bean="CartModifierFormHandler.items[0].quantity" type="hidden"
									value="0" />

								<dsp:input id="productIdFromTypeahead"
									bean="CartModifierFormHandler.items[0].productId" type="hidden"
									value="" />
								<dsp:input id="skuIdFromTypeahead"
									bean="CartModifierFormHandler.items[0].catalogRefId"
									type="hidden" value="" />

								<dsp:input type="hidden" value="true" priority="-10"
									bean="CartModifierFormHandler.addMultipleItemsToOrder" />
							</dsp:form>
						</div>

						<div class="collapse navbar-collapse" id="navigation">
							<c:if test="${showHeaderLinks}">
								<ul class="nav navbar-nav nav-hover mobile">
									<!-- ALL PRODUCT -->
									<c:forEach var="navigation" items="${contentItem.navigation}">
										<dsp:renderContentItem contentItem="${navigation}">
											<dsp:param name="testimonials"
												value="${contentItem.testimonials}" />
											<dsp:param name="homepageSidePromo"
												value="${contentItem.homepageSidePromo}" />
										</dsp:renderContentItem>
									</c:forEach>

									<!-- ALL BRANDS -->
									<li class="nav-brands hidden-xs hidden-sm"><a
										id="menuSeeAllBrands" href="/all-brands" aria-expanded="false"> All Brands</i>
									</a>
		
										<ul class="dropdown-menu nav-subnav sub-brands" id="brandNavi">
											<dsp:droplet name="/cps/droplet/BrandsNavigationDroplet">
												<dsp:param name="catalog"
													bean="/atg/multisite/Site.defaultCatalog" />
												<dsp:oparam name="output">
													<div class="row" style="margin-left: 0; margin-right: 0">
														<c:set var="col" value="${1}" />
														<dsp:droplet name="/atg/dynamo/droplet/ForEach">
															<dsp:param name="array" param="menuList" />
															<dsp:param name="elementName" value="columnMenu" />
															<dsp:oparam name="output">
																<dsp:getvalueof var="columnMenu" param="columnMenu" />
																<div class="col-xs-12 col-lg-6"
																	style="padding-left: 0; padding-right: 0">
																	<ul class="list-unstyled" id="column_${col}">
																		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
																			<dsp:param name="array" param="columnMenu" />
																			<dsp:param name="elementName"
																				value="navigationMenuItem" />
																			<dsp:oparam name="output">
																				<dsp:getvalueof var="navigationMenuItem"
																					param="navigationMenuItem" />
																				<li><a href="${navigationMenuItem.URL}">${navigationMenuItem.name}</a>
																				</li>
																			</dsp:oparam>
																		</dsp:droplet>
																	</ul>
																</div>
																<c:set var="col" value="${col + 1}" />
															</dsp:oparam>
														</dsp:droplet>
													</div>
													<div class="row">
														<div class="col-xs-12">
															<ul class="list-unstyled">
																<li style="border-top: 1px solid #e0dfdc;"><a
																	id="seeAllBrandsLnk" href="#"
																	data-event-click-id="content5">SEE ALL BRANDS</a></li>
															</ul>
														</div>
													</div>
												</dsp:oparam>
											</dsp:droplet>
										</ul></li>
									<li class="visible-xs visible-sm"><a href="/all-brands"
										aria-controls="brandNaviMenu" class="collapsed content_none"
										role="button" aria-expanded="false">
											All Brands </a>
										<ul class="mobile-subnav collapse" id="brandNaviMenu">
											<dsp:include
												page="${originatingRequest.contextPath}/content/gadgets/brandHeaderMenu.jsp" />
										</ul>
									</li>
								</ul>
								<ul class="nav navbar-nav visible-xs visible-sm">
									<li><a href="/services/default">Services</a></li>
									<li><a href="/locations/locations.jsp">Locations</a></li>
									<li><a href="/did-you-know">Did You Know</a></li>
									<li class="dropdown nav-products"><a href="#">Support</a>
										<ul class="dropdown-menu nav-subnav">
											<li>Instructional Videos</li>
											<li>FAQs</li>
											<li>Video Library</li>
											<li>Product Information</li>
											<li><a href="/global/contact-us.jsp">Contact US</a></li>
										</ul></li>
								</ul>
							</c:if>
						</div>

						<ul id="navbar-cart" class="nav navbar-nav navbar-right">
							<dsp:getvalueof var="commerceItems"
								bean="ShoppingCart.current.commerceItems" />

							<dsp:droplet name="/cps/droplet/AccessRightDroplet">
								<dsp:param name="profile" bean="Profile" />
								<dsp:param name="accessRightKey" value="plp-pdp-cart" />
								<dsp:oparam name="false">
									<dsp:getvalueof var="permissionDenied" value="true" />
								</dsp:oparam>
							</dsp:droplet>

							<c:choose>
								<c:when test="${isTransient}">
									<!-- <li class="dropdown nav-list" id="orderPadDropdown"><a
	                                    href="#" class="dropdown-toggle" data-toggle="modal"
	                                    data-target="#logIn" role="button" aria-haspopup="true"
	                                    aria-expanded="true"><i class="icon icon-clipboard"></i></a></li> -->

									<li class="nav-cart"><dsp:droplet
											name="/cps/droplet/GuestCheckoutAllowedDroplet">
											<dsp:oparam name="true">
												<a
													href="${originatingRequest.contextPath}/checkout/cart.jsp">
													<i class="gold glyphicon glyphicon-shopping-cart"></i> <span
													class="summary-items-count">${fn:length(commerceItems)}</span>
													items
												</a>
											</dsp:oparam>
											<dsp:oparam name="false">
												<a href="#" data-toggle="modal" data-target="#logIn"> <i
													class="gold glyphicon glyphicon-shopping-cart"></i> <span
													class="summary-items-count">${fn:length(commerceItems)}</span>
													items
												</a>
											</dsp:oparam>
										</dsp:droplet></li>
								</c:when>
								<c:when test="${permissionDenied}">
									<!-- <li class="dropdown nav-list" id="orderPadDropdown"><a
	                                    href="#" class="dropdown-toggle" data-toggle="modal"
	                                    data-target="#permissionDenied" role="button"
	                                    aria-haspopup="true" aria-expanded="true"><i
	                                        class="icon icon-clipboard"></i></a></li> -->
									<%-- <li class="dropdown nav-list" id="orderPadDropdown"><a
										href="#" class="dropdown-toggle" data-toggle="dropdown"
										role="button" aria-haspopup="true" aria-expanded="true"
										id="orderPadDropdownLink"> <i class="icon icon-clipboard"></i>
											<span>Quick Order Pad</span>
									</a> <dsp:include page="/global/gadgets/order-pad-dropdown.jsp" />
									</li> --%>
									<%-- SM-532 Finance account - Issues --%> 
									<li class="nav-list"><a
	                                    href="#" data-toggle="modal"
	                                    data-target="#permissionDenied" role="button"
	                                    aria-haspopup="true" aria-expanded="true"><i
	                                        class="icon icon-clipboard"></i><span>Quick Order Pad</span></a>
                                    </li> 
									<li class="nav-cart"><a href="#" data-toggle="modal"
										data-target="#permissionDenied"> <i
											class="gold glyphicon glyphicon-shopping-cart"></i> <span
											class="summary-items-count">${fn:length(commerceItems)}</span>
											items
									</a></li>
								</c:when>
								<c:otherwise>
									<!-- ORDER PAD -->
									<li class="dropdown nav-list" id="orderPadDropdown"><a
										href="#" class="dropdown-toggle" data-toggle="dropdown"
										role="button" aria-haspopup="true" aria-expanded="true"
										id="orderPadDropdownLink"> <i class="icon icon-clipboard"></i>
											<span>Quick Order Pad</span>
									</a> <dsp:include page="/global/gadgets/order-pad-dropdown.jsp" />
									</li>
									<li class="nav-cart"><a
										href="${originatingRequest.contextPath}/checkout/cart.jsp">
											<i class="gold glyphicon glyphicon-shopping-cart"></i> <span
											class="summary-items-count">${fn:length(commerceItems)}</span>
											items
									</a></li>
								</c:otherwise>
							</c:choose>
						</ul>
					</c:if>
				</div>
			</nav>
		</c:if>
		<div id="jdeContent"></div>
		<nav id="error-message-connection-JDE"
			class="navbar-static-top alert-danger jde-error"
			style="display: none;">
			<div class="container">
				<div class="alert-prices alert-icon">
					<i class="fa fa-exclamation-triangle red"></i> <strong
						style="color: #333333;"> <dsp:include
							page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="errPriceUnavailable" />
							<dsp:param name="notWrap" value="true" />
						</dsp:include> <%--<fmt:message key="catalog.pdp.price.unavailable"/>--%>
					</strong>
				</div>
			</div>
		</nav>

		<c:if test="${page eq 'pdp'}">
			<dsp:getvalueof param="prodId" var="prodId" />

			<dsp:droplet name="/cps/droplet/AccessRightDroplet">
				<dsp:param name="profile" bean="Profile" />
				<dsp:param name="accessRightKey" value="plp-pdp-cart" />
				<dsp:oparam name="false">
					<dsp:getvalueof var="permissionDenied" value="true" />
				</dsp:oparam>
			</dsp:droplet>

			<dsp:droplet name="/atg/commerce/catalog/ProductLookup">
				<dsp:param name="id" value="${prodId}" />
				<dsp:param name="filterBySite" value="false" />
				<dsp:param name="filterByCatalog" value="false" />
				<dsp:oparam name="output">
					<dsp:setvalue param="product" paramvalue="element" />
					<dsp:getvalueof var="skuId"
						param="product.childSKUs[0].repositoryId" />
					<dsp:getvalueof var="parentCategory" param="product.parentCategory" />
					<c:if test="${parentCategory == null}">
						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" param="product.parentCategories" />
							<dsp:param name="elementName" value="category" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="index" param="index" />
								<c:if test="${index eq 0}">
									<dsp:getvalueof var="eCommerceDisplay"
										param="category.eCommerceDisplay" />
								</c:if>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${parentCategory != null}">
						<dsp:getvalueof var="eCommerceDisplay"
							param="product.parentCategory.eCommerceDisplay" />
					</c:if>
					<dsp:getvalueof var="stockingType" param="product.stockingType" />
					<dsp:getvalueof var="productDisabled"
						value="${stockingType eq 'O' || stockingType eq 'U' || stockingType eq 'K' || stockingType eq 'X' || not eCommerceDisplay}" />
					<dsp:getvalueof var="productPurchasable" param="product.productPurchasable"/>
					<dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType)}"/>
					<c:if test="${!productDisabled && productPurchasable && !isProductPurchasableStockingType}">
						<div class="pdp-bar hidden-sm hidden-xs">
							<div class="container">
								<div class="row">
									<dsp:getvalueof var="altText" param="product.altText"/>
									<div class="col-xs-1 text-right">
										<dsp:getvalueof var="detailImg" param="product.web_url" />
										<dsp:getvalueof var="lowerDetailImg" value="${fn:toLowerCase(detailImg)}"/>
										<c:choose>
											<c:when test="${fn:startsWith(lowerDetailImg,'http')}">
												<dsp:getvalueof var="detailImg" value="${detailImg}"/>
											</c:when>
											<c:otherwise>
													<dsp:getvalueof var="detailImg" value="${staticContentPrefix}${detailImg}"/>
											</c:otherwise>
										</c:choose>	
										<dsp:getvalueof var="defaultImg" value="/assets/images/pdp-placeholder.png" />
										<img alt="${vsg_utils:escapeHtml(altText)}" style="max-height: 60px; max-width: 60px"
											data-src="<c:out value="${vsg_utils:escapeHtml(detailImg)}" default="${defaultImg}"/>"
											data-fallback-image="${defaultImg}" />
									</div>
									<div class="col-lg-8 col-xs-6">
										<div class="pdp-bar-title-cover">
											<h2 class="pdp-bar-title">
												<dsp:valueof param="product.description"
													converter="valueishtml" />
												<%-- <dsp:getvalueof var="descriptionLine2" param="product.descriptionLine2" />
                                                    <c:if test="${not empty descriptionLine2}">
                                                        , <dsp:valueof value="${descriptionLine2}" converter="valueishtml" />
                                                    </c:if> --%>
											</h2>
										</div>
									</div>
									<div class="col-lg-3 col-xs-5 text-right form-inline mt5">
										<dsp:form id="addToCartHeader${vsg_utils:escapeHtml(prodId)}"
											formid="addToCartHeader${vsg_utils:escapeHtml(prodId)}"
											action="/checkout/cart.jsp" method="post"
											style="display: inline-block;">
											<dsp:input bean="CartModifierFormHandler.addItemCount"
												type="hidden" value="1" priority="10" />
											<dsp:getvalueof var="minimum_order_qty"
												param="product.minimum_order_qty" />
											<dsp:input id="qty${vsg_utils:escapeHtml(prodId)}"
												bean="CartModifierFormHandler.items[0].quantity"
												iclass="form-control text-center input-lg" maxlength="5"
												type="text" value="${minimum_order_qty}">
												<dsp:tagAttribute name="data-event-keydown-id"
													value="content9" />
												<dsp:tagAttribute name="placeholder" value="Qty" />
											</dsp:input>
											<c:choose>
												<c:when test="${isTransient}">
													<dsp:droplet
														name="/cps/droplet/GuestCheckoutAllowedDroplet">
														<dsp:oparam name="true">
															<button type="button"
																class="btn btn-primary btn-lg addToCartButton"
																data-event-click-id="content6"
																data-event-click-param0="${vsg_utils:escapeJS(prodId)}">Add
																to Cart</button>
														</dsp:oparam>
														<dsp:oparam name="false">
															<button type="button"
																class="btn btn-primary btn-lg addToCartButton"
																data-productId="${prodId}" data-skuId="${skuId}"
																data-toggle="modal" data-target="#logIn">Add to
																Cart</button>
														</dsp:oparam>
													</dsp:droplet>
												</c:when>
												<c:when test="${permissionDenied}">
													<button type="button"
														class="btn btn-primary btn-lg addToCartButton"
														data-toggle="modal" data-target="#permissionDenied">Add
														to Cart</button>
												</c:when>
												<c:otherwise>
													<button type="button"
														class="btn btn-primary btn-lg addToCartButton"
														data-event-click-id="content6"
														data-event-click-param0="${vsg_utils:escapeJS(prodId)}">Add
														to Cart</button>
												</c:otherwise>
											</c:choose>
											<dsp:input bean="CartModifierFormHandler.items[0].productId"
												type="hidden" value="${prodId}" />
											<dsp:input
												bean="CartModifierFormHandler.items[0].catalogRefId"
												type="hidden" value="${skuId}" />
											<dsp:input type="hidden" value="true" priority="-10"
												bean="CartModifierFormHandler.addMultipleItemsToOrder" />
										</dsp:form>
										<c:choose>
											<c:when test="${not isTransient}">
												<c:choose>
													<c:when test="${permissionDenied}">
														<button class="btn btn-link btn-lg ml10"
															data-toggle="modal" data-target="#permissionDenied">
															<i class="icon icon-paper-list"></i>
														</button>
													</c:when>
													<c:otherwise>
														<button class="btn btn-link btn-lg ml10"
															data-event-click-id="content7"
															data-event-click-param0="${vsg_utils:escapeJS(prodId)}"
															data-event-click-param1="${vsg_utils:escapeJS(skuId)}"
															data-event-click-param2="true"
															title="Add to Material List" >
															<i class="icon icon-paper-list"></i>
														</button>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<button class="btn btn-link btn-lg ml10" data-toggle="modal"
													data-target="#logIn" title="Add to Material List">
													<i class="icon icon-paper-list"></i>
												</button>
											</c:otherwise>
										</c:choose>
									</div>
								</div>
							</div>
						</div>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
		</c:if>
	</div>

	<dsp:getvalueof var="loc" param="loc" />
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-login-new.jsp?loc=${loc}" />
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-login-guest.jsp" />

	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
		<dsp:param name="messageId" value="no-search-term-typed" />
		<dsp:param name="divId" value="_noSearchTerm" />
		<dsp:param name="isReset" value="false" />
	</dsp:include>
	<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
		<dsp:param name="messageKey" value="no-results-found" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="singleMessage" param="message" />
		</dsp:oparam>
		<dsp:oparam name="empty">
			<dsp:getvalueof var="singleMessage" param="message" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
		<dsp:param name="messageKey" value="no-results-found-combination" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="multipleMessage" param="message" />
		</dsp:oparam>
		<dsp:oparam name="empty">
			<dsp:getvalueof var="multipleMessage" param="message" />
		</dsp:oparam>
	</dsp:droplet>

	<input type="hidden" id="singleMessage" value="${singleMessage}" />
	<input type="hidden" id="multipleMessage" value="${multipleMessage}" />
	
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
		<dsp:param name="messageId" value="no-results-found" />
		<dsp:param name="divId" value="_noResultsFound" />
		<dsp:param name="isReset" value="false" />
	</dsp:include>

	<div id="materialListFileErrorModal">
		<dsp:include
			page="${originatingRequest.contextPath}/global/modals/modal-items-not-added-to-ml.jsp">
			<dsp:param name="invalidMaterialItemsList"
				value="${invalidMaterialItemsList}" />
			<dsp:param name="materialDuplicates" param="materialDuplicates" />
			<dsp:param name="hasMaterialDuplicates" param="hasMaterialDuplicates" />
			<dsp:param name="giftlistId" param="giftlistId" />
			<dsp:param name="ca" param="ca" />
		</dsp:include>
	</div>
	<div id="modalCCNotification">
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-cc-account-notification.jsp" />
	</div>
<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-err-found-pdp.jsp" />
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-share-page.jsp" />
	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-success-forgotpassword.jsp"/>	
	<div id="modal-cs">	
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-cs.jsp" />
	</div>
	<div id="differentSessionAddress">
		<dsp:include page="${originatingRequest.contextPath}/global/modals/different-shipto.jsp" />
	</div>
	<script nonce="${requestScope.nonce}">
		onModalCsLoaded();
	</script>
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/order-pad.jsp" />

	<!-- Order approval modal start-->
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-order-saved-carts.jsp" />
	<!-- Order approval modal end-->

	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-product-added-to-cart.jsp" />
	<!-- add to cart-error modal start-->
	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-add-to-cart-error.jsp" />
	<!-- add to cart-error modal end-->

	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-error.jsp" />

	<dsp:include page="/global/modals/modal-items-not-added-to-cart.jsp">
		<dsp:param name="invalidItemList" value="${invalidItemList}" />
		<dsp:param name="duplicateProducts" param="duplicateProducts" />
		<dsp:param name="hasQuickOrderDuplicates"
			param="hasQuickOrderDuplicates" />
	</dsp:include>

	<dsp:include
		page="${originatingRequest.contextPath}/global/modals/modal-permission-denied.jsp" />
	<dsp:form action="${originatingRequest.contextPath}" method="post"
		id="form-logout" formid="form-logout" style="display:none;">
		<dsp:input type="hidden" bean="ProfileFormHandler.logout" value="true"
			priority="-10" />
	</dsp:form>
	<div id="modal-cs-show-div"></div>

	<div id="request-quote-modal">
		<dsp:include page="/global/modals/request-quote.jsp" />
		<script nonce="${requestScope.nonce}">
			onRequestQuoteModalLoaded();
		</script>
	</div>

	<dsp:form id="compareProductHandler" formid="compareProductHandler"
		method="POST">
		<dsp:input type="hidden"
			bean="CartModifierFormHandler.comparisonReferer"
			id="comparisonReferer" value="" />
		<dsp:input type="hidden"
			bean="CartModifierFormHandler.compareProducts" value="true"
			priority="-10" />
	</dsp:form>

	<dsp:form id="preRegisterPageHandler" formid="preRegisterPageHandler"
		method="POST">
		<dsp:input type="hidden" bean="ProfileFormHandler.preRegisterPage"
			id="preRegisterPage" value="" />
		<dsp:input type="hidden"
			bean="ProfileFormHandler.goToRegistrationPage" value="true"
			priority="-10" />
	</dsp:form>

	<c:if test="${allowNewUserSelfRegistration}">
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-register.jsp" />
	</c:if>
	
	<c:set var="differentAddress" value="false" />
    <dsp:getvalueof var="defaultAddressID" bean="Profile.selectedCS.id" />
        <dsp:getvalueof var="shippingAddressID" bean="Profile.shippingAddress.repositoryId" />
		<c:if test="${empty shippingAddressID}">
                <dsp:getvalueof var="shippingAddressID" bean="Profile.derivedShippingAddress.repositoryId" />
		</c:if>
        <c:if test="${(defaultAddressID != shippingAddressID ) && not empty defaultAddressID && shoppingCart.current.commerceItemCount !=0}">
                <c:set var="differentAddress" value="true" />
		</c:if>
		<input type="hidden" value="${differentAddress}" id="differentAddress"/>
	<dsp:getvalueof var="currentPage" value="${originatingRequest.requestURI}" />
	<dsp:getvalueof var="defaultShipToAddress" bean="Profile.selectedCS.address1" />
	<dsp:getvalueof var="defaultShipToAddressFromProfile" bean="Profile.shippingAddress.address1" />
	<c:if test="${empty defaultShipToAddress && !isTransient && !superAdmin && isHomePage=='true' && empty defaultShipToAddressFromProfile}">
		<input type="hidden" value="true" id="defaultShipToAddress" name="defaultShipToAddress">
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-default-ship-to-address.jsp" />
	</c:if>
	<c:if test="${empty defaultShipToAddress && !isTransient && !superAdmin && currentPage=='/checkout/cart.jsp' && empty defaultShipToAddressFromProfile}">	
		<input type="hidden" value="true" id="cartPage" name="cartPage"/>
		<input type="hidden" value="true" id="defaultShipToAddress" name="defaultShipToAddress">
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-default-ship-to-address.jsp" />
	</c:if>
</dsp:page>
<script type="text/javascript" nonce="${requestScope.nonce}">
	$(document).ready(function() {
				var link = $("#orderPadDropdownLink");
				var panel = $("#quickOrderPanel");
				var errorCodesSize = parseInt($("#errorCodesSize").val());
				if (window.location.pathname !== "/account/quick-order.jsp"
						&& errorCodesSize > 0 && panel.is(":hidden")) {
					link.click();
				}
				link.on("click", function(e) {
					if (panel.is(":visible")) {
						$("#qo-pad-modal-message-error-div").attr("style",
								"display: none");
						$("#qo-pad-modal-message-error").html("");
						$("#qo-pad-file-error-div").attr("style",
								"display: none");
						$("#qo-pad-file-error").html("");
					}
				});
  $("#compute_option").html($('#headerSearchCategory option:selected').text()); 
  $("#headerSearchCategory").width($("#compute_select").width()+15); 
  
  $('#headerSearchCategory').change(function(){
	$('#headerCategory').val($('#headerSearchCategory').val());
	  if($('#headerSearchCategory').val() != "all") {
      	$('#dimVal').val(($('#headerSearchCategory option:selected').data('dimvalid')));
      	console.log("selected dim val :: " + $('#dimVal').val());
  	  }else{
	  	$('#dimVal').val("0");
  	  }
    $("#compute_option").html($('#headerSearchCategory option:selected').text()); 
    $(this).width($("#compute_select").width());  
    //$('#headerSearchQuestion').autocomplete("search");
  });
  let searchAppend = $('.searchAppend').text();
  if(searchAppend){
      let searchedValue = searchAppend.trim().slice(18);
      let results = searchedValue.substring(0, searchedValue.length - 1);
      $('#headerSearchQuestion').val(results);
  }
});
</script>

<script type="text/javascript" nonce="${requestScope.nonce}">
	//made logo unclickable for CSR
	if ("${superAdmin}") {
		$('.header-logo a').replaceWith(function() {
			return $(this).contents();
		});
	}

	function checkSearchTermTyped() {

		if ($("#headerSearchQuestion").val().length == 0 && ($("#headerSearchCategory").val() == "all")) {
			$("#link-noResultsModal_noSearchTerm").click();
		} else {
			searchRedirect();
		}
	}
	function gup(name, url) {
		if (!url)
			url = location.href;
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(url);
		return results == null ? null : results[1];
	}
	var isCartPage=$('#isCartPage').val();
	var isModalAddToCartShow = false;
	$(window).load(
					function() {
						if (gup("showLogin", location.href) == "true") {
							$('#logIn').modal('show');
						}
						if (gup("ship", location.href) == "true") {
							$('#selectShipAddress').modal('show');
						}
						if (gup("sreqmodal", location.href) == "true") {
							$('#requestQuote').modal('show');
						}
						if (gup("quickodropdown", location.href) == "true") {
							$("#orderPadDropdown").addClass("open");
						}
						if (gup("invalidskumodal", location.href) == "true") {
							$('#modal-carterror').modal('show');
						}
						if ("${isHasAddedNewProductToShoppingCart}" === "true") {
							if (!isModalAddToCartShow) {
								isModalAddToCartShow = true;
								$.get('/catalog/gadgets/pdp-added-to-cart.jsp',function(jsp) {
										$("#productAddedToCartModalContent").html(jsp);
										    if (isCartPage == "false") {
												$('#productAddedToCartModal').modal('show');
										    }
									});
							}
							<dsp:setvalue bean="CPSSessionBean.hasAddedNewProductToShoppingCart" value="false"/>
						}
						var isEmptyDefaultShipToAddress = $('#defaultShipToAddress').val();
						if(isEmptyDefaultShipToAddress == "true"){
							console.log('isEmptyDefaultShipToAddress : '+isEmptyDefaultShipToAddress);
							 $('#defaultShipToAddressModal').modal('show');
						}
						
					});

	var maxSize = 0;
	$('#menuSeeAllBrands')
			.hover(
					function() {
						if (maxSize == 0) {
							console.log('change allBrands menu size');
							var col1Size = $('#column_1').children().length;
							var col2Size = $('#column_2').children().length;
							var minCol = col1Size < col2Size ? $('#column_1')
									.children() : $('#column_2').children();
							var maxCol = col1Size < col2Size ? $('#column_2')
									.children() : $('#column_1').children();
							var seeAllBrandsLnkWidth = $('#seeAllBrandsLnk')
									.width() / 2;

							minCol.each(function(index) {
								var elem1 = $(this).children("a");
								var elem2 = maxCol.eq(index).children("a");

								if (elem1.height() > elem2.height()) {
									elem2.height(elem1.height());
									elem2.css({
										"display" : "table-cell",
										"vertical-align" : "middle"
									});
									elem2.width(seeAllBrandsLnkWidth);
								} else {
									elem1.height(elem2.height());
									elem1.css({
										"display" : "table-cell",
										"vertical-align" : "middle"
									});
									elem1.width(seeAllBrandsLnkWidth);
								}
								maxSize = elem1.height();
							});
						}
					});

	$('#modal-passwordreset').modal('show');
</script>