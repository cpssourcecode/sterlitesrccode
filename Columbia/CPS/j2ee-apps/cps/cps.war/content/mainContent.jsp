<dsp:page>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
	<c:set var="resultTabs" value="${vsg_utils:resultsTabs(content)}" />
	<dsp:getvalueof var="totalNumRecs" param="totalNumRecs"/>
	
	<dsp:getvalueof var="endecaRequestURI" value="${requestScope['javax.servlet.forward.request_uri']}" />
	<c:choose>
		<c:when test="${fn:startsWith(endecaRequestURI, '/search')}">
			<p id="showing-total-num-results">
				<%-- Showing <strong>${totalNumRecs}</strong> items matching you search criteria. Narrow your results using the filters below. --%>
				<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
					<dsp:param name="messageKey" value="searchResultMsg" />
					<dsp:oparam name="output">						
						<dsp:getvalueof param="message" var="searchResultMsg" />
						${fn:replace(searchResultMsg,"{0}",totalNumRecs)}
					</dsp:oparam>
					<dsp:oparam name="empty">
						<dsp:valueof param="message" valueishtml="true" />
					</dsp:oparam>
				</dsp:droplet>				
			</p>
		</c:when>
	</c:choose>
	<div id="seconday_search_info"></div>
	<dsp:getvalueof param="loadSection" var="loadSection"/>

	<c:forEach var="mainContent" items="${resultTabs.mainContent}">
		<dsp:renderContentItem contentItem="${mainContent}" />
		<c:if test="${mainContent['@type'] == 'MainResultsList' && loadSection != 'inner'}">
			<script nonce="${requestScope.nonce}">
				onResultListLoaded();
			</script>
		</c:if>
	</c:forEach>
		
	<script nonce="${requestScope.nonce}">
		document.addEventListener("DOMContentLoaded", function(){
			loadMessage();
		});
	</script>
</dsp:page>