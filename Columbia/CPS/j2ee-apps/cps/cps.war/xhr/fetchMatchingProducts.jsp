<%@ page contentType="application/json; charset=UTF-8" %>
<dsp:page>

	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:droplet name="InvokeAssembler">
		<dsp:param name="contentCollection"
			value="/content/Shared/Results List" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="content"
							vartype="com.endeca.infront.assembler.ContentItem"
							param="contentItem" />
		</dsp:oparam>
	</dsp:droplet>
	
	<c:if test="${not empty content.contents}">
		<c:set var="resultsList" value="${content.contents[0]}" />	 
	 	<json:array>
		<c:forEach var="record" items="${resultsList.records}">
	 		<json:object>
 				<json:property name="value" value="${record.attributes['product.repositoryId']}"/>
 				<json:property name="label" value="${record.attributes['product.description']}"/>
 				<json:property name="desc" value="${record.attributes['product.displayName']}"/>
 			</json:object>
		</c:forEach>
	 	</json:array>
	</c:if>
</dsp:page>
