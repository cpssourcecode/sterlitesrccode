<%@ page contentType="text/html" %>

<dsp:page>
	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />
	<dsp:droplet name="InvokeAssembler">
		<dsp:param name="contentCollection"
			value="/content/Shared/Results List" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="content"
							vartype="com.endeca.infront.assembler.ContentItem"
							param="contentItem" />
		</dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="ntt_value" param="Ntt" />
	<dsp:getvalueof var="noResult" param="noResult" />
	<c:choose>
	<c:when test="${noResult eq true}">
	<c:if test="${not empty content.contents}">
		<c:set var="resultsList" value="${content.contents[0]}" />	 
		<c:set var="record" value="${resultsList.totalNumRecs}" />
		<c:if test="${record gt 0}">
		<div><br/>
		<strong>There are ${record} results for "${ntt_value}" in other categories<br>
		<a style="color: #00a2d8 !important;"href="/search?Ntt=${ntt_value}">Click here to see Results</a></strong>
		<div>
		</c:if>
		</c:if>
	</c:when>
	<c:otherwise>
	<c:if test="${not empty content.contents}">
		<c:set var="resultsList" value="${content.contents[0]}" />	 
		<c:set var="record" value="${resultsList.totalNumRecs}" />
		There are ${record} <a  href="/search?Ntt=${ntt_value}">additional results</a> for "${ntt_value}".
	</c:if>
	</c:otherwise>
	
	</c:choose>
</dsp:page>
