<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/cps/droplet/RoleLookupDroplet"/>
    <dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>

    <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
    <c:if test="${not empty selectedOrg}">
        <dsp:getvalueof var="csrClass" value="admin-access"/>
    </c:if>

    <cp:pageContainer page="account">
        <dsp:getvalueof var="companyId" param="c"/>
        <dsp:getvalueof var="companyName" param="companyName"/>
        <main id="body">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/account/manage-companies.jsp">Manage Companies</a>
                    </li>
                    <li class="active">Add Customer Admin</li>
                </ol>
                <h1 class="mb20">Add Customer Admin</h1>
                <div class="manage-users">
                    <dsp:form method="post" action="/account/add-customer-admin.jsp" id="add-admin">
                    	<div class="row form-group">
                            <div class="col-md-12 col-xs-12">
                                <h5 class="text-nocase">Company Name: <dsp:valueof param="companyName" valueishtml="true"/></h5>
                            </div>
                        </div>
                        <h4 class="text-nocase">User Information</h4>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-horizontal">

                                <dsp:getvalueof var="formExceptions" vartype="java.lang.Object"
                                                bean="MultiUserAddFormHandler.formExceptions"/>
                                <c:if test="${not empty formExceptions}">
                                    <c:forEach var="formException" items="${formExceptions}">
                                        <dsp:param name="formException" value="${formException}"/>
                                        <dsp:getvalueof var="errorCode" param="formException.property"/>
                                        <script type="text/javascript" nonce="${requestScope.nonce}">
                                            jQuery(document).ready(function () {
                                                loadErrors('${errorCode}');
                                            });

                                            function loadErrors(errorCode) {
                                                $("#" + errorCode).parent().addClass("has-error");
                                            }
                                        </script>
                                        <c:if test="${formException.message != ''}">
                                            <div class="alert alert-danger">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
                                                        <span aria-hidden="true"> <span
                                                                class="glyphicon glyphicon-remove-circle"></span>
                                                        </span>
                                                </button>
                                                <dsp:valueof param="formException.message" valueishtml="true"/>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                
                                <div class="row row-narrow form-group">
                                    <div class="col-md-6 col-xs-6">
                                        <dsp:input type="text" id="firstName" bean="MultiUserAddFormHandler.firstName"
                                                   iclass="form-control form-error">
                                            <dsp:tagAttribute name="placeholder" value="First Name*"/>
                                        </dsp:input>
                                    </div>
                                    <div class="col-md-6 col-xs-6">
                                        <dsp:input type="text" id="lastName" bean="MultiUserAddFormHandler.lastName"
                                                   iclass="form-control">
                                            <dsp:tagAttribute name="placeholder" value="Last Name*"/>
                                        </dsp:input>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <dsp:input type="text" id="email" bean="MultiUserAddFormHandler.email"
                                                   iclass="form-control">
                                            <dsp:tagAttribute name="placeholder"
                                                              value="Email Address (will be User's login ID)*"/>
                                        </dsp:input>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <dsp:input type="text" id="phone" bean="MultiUserAddFormHandler.phone"
                                                   iclass="form-control phone">
                                            <dsp:tagAttribute name="placeholder" value="Phone*"/>
                                        </dsp:input>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>
                            <a href="javascript:void(0);" data-event-click-id="eventSubmitAddAdmin"
                               class="btn btn-primary">
                                Add Customer Admin
                            </a>
                            <a href="/account/manage-companies.jsp"
                               class="btn btn-info">
                                Cancel
                            </a>
                            <dsp:input type="hidden" bean="MultiUserAddFormHandler.company" paramvalue="c"/>
                            <dsp:input type="hidden" bean="MultiUserAddFormHandler.addAdminSuccessURL"
                                       value="/account/manage-companies.jsp"/>
                            <dsp:input type="hidden" bean="MultiUserAddFormHandler.addAdminErrorURL"
                                       value="/account/add-customer-admin.jsp?c=${companyId}"/>
                            <dsp:input type="hidden" bean="MultiUserAddFormHandler.addAdmin" value="true" priority="-10"/>
                        </p>
                        <script type="text/javascript" nonce="${requestScope.nonce}">
                            $(".phone").mask("(999) 999-9999");
                            function submitAddAdmin() {
                                $("#add-admin").submit();
                            }
                        </script>
                    </dsp:form>
                </div>
            </div>
        </main>
    </cp:pageContainer>
</dsp:page>