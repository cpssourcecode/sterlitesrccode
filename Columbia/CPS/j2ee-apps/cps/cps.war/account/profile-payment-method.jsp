<%@ taglib prefix="dsp" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>
<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/droplet/SnapPayFormDroplet"/>
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>
	<cp:pageContainer page="account">
	<main id="body">
	<div class="container">
 		<div class="row">
  			<div class="col-md-8">
			<ol class="breadcrumb">
			  <li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
			  <li class="active">Payment Method</li>
			</ol>
			</div>
			<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
				<dsp:param name="email" value="false"/>
				<dsp:param name="print" value="true"/>
			</dsp:include>
	    </div>
		<h1 class="mb20">Payment Method</h1>
		<div class="hidden" id="success-message"></div>

		<dsp:droplet name="SnapPayFormDroplet">
			<dsp:param name="order" bean="ShoppingCart.current"/>
			<dsp:param name="profile" bean="Profile"/>
			<dsp:param name="maintainCreditCards" value="${true}"/>
			<dsp:oparam name="output">
				<div class="">
					<dsp:getvalueof var="iframeUrl" bean="SnapPayFormDroplet.iframeUrl"/>
					<dsp:getvalueof var="requestNumber" param="requestNumber"/>
					<iframe src="${iframeUrl}${requestNumber}" style="height: 700px; width: 900px;"></iframe>
				</div>
			</dsp:oparam>
			<dsp:oparam name="error">
				Error
			</dsp:oparam>
		</dsp:droplet>

	</div>
	</main>
	</cp:pageContainer>
</dsp:page>