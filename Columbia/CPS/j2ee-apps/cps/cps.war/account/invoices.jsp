<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>
	<cp:pageContainer page="invoice">
		<main id="body">
		<div class="container ${csrClass}">
			<div class="row">
				<div class="col-md-8">
					<ol class="breadcrumb">
						<li>
							<a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a>
						</li>
						<li class="active">View Invoices</li>
					</ol>
				</div>
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="false" />
					<dsp:param name="print" value="true" />
				</dsp:include>
			</div>
	        <div class="container preload-cover spinner-on-start">
				<div class="loading">
					<i class="fa fa-spinner fa-spin"></i>
				</div>
			</div>
			<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-err-search-invoice.jsp" />
			<h1 class="mb20">View Invoices</h1>
			<section>
				<h3 class="h4 text-nocase">
					<fmt:message key="account.invoices.label.searchInvoices" />
				</h3>
				<ul>
					<li>Searching by Invoice #, Sales Order #, or PO # will return ALL Invoice History records for the last 2 years.</li>
					<li>Searching by Ship to Address will return Invoice History records up to 2 years old.</li>
					<li>Searching by Date Range will return records up to 30, 60, 90, or 120 days old.</li>
				</ul>
			</section>
			<section>
				<div class="row mt40 cols-padding-sm">
					<dsp:form action="#" method="post" id="invoice-search">
						<dsp:input id="invoiceSearchPageSize" bean="ProfileFormHandler.value.invoiceSearchPageSize" type="hidden" value="20" />
						<dsp:input id="invoiceSearchStartIndex" bean="ProfileFormHandler.value.invoiceSearchStartIndex" type="hidden" value="0" />
						<dsp:input id="invoiceSearchSortField" bean="ProfileFormHandler.value.invoiceSearchSortField" type="hidden" value="" />
						<dsp:input id="invoiceSearchSortOption" bean="ProfileFormHandler.value.invoiceSearchSortOption" type="hidden" value="" />
						<dsp:input id="invoiceSearchInitial" bean="ProfileFormHandler.value.invoiceSearchInitial" type="hidden" value="true" />
						<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
							<div class="radio">
								<label id="myCustomRadioLabel0" data-initialize="radio">
									<dsp:input bean="ProfileFormHandler.value.invoiceSearchValue" iclass="radio" type="radio"
											value="invoiceNumber" name="radioEx1" id="invoiceId">
										<dsp:tagAttribute name="data-event-click-id" value="eventInvoiceSearchType" />
									</dsp:input>
									<fmt:message key="account.invoices.label.invoice" />
								</label>
							</div>

							<div class="radio">
								<label id="myCustomRadioLabel1" data-initialize="radio">
									<dsp:input bean="ProfileFormHandler.value.invoiceSearchValue" iclass="radio" type="radio"
											value="orderNumber" name="radioEx1" id="salesOrderId">
										<dsp:tagAttribute name="data-event-click-id" value="eventInvoiceSearchType" />
									</dsp:input>
									<fmt:message key="account.invoices.label.orderNumber" />
								</label>
							</div>

							<div class="radio">
								<label id="myCustomRadioLabel2" data-initialize="radio">
									<dsp:input bean="ProfileFormHandler.value.invoiceSearchValue" iclass="radio" type="radio"
											value="poNumber" name="radioEx1" id="searchPoId">
										<dsp:tagAttribute name="data-event-click-id" value="eventInvoiceSearchType" />
									</dsp:input>
									<fmt:message key="account.invoices.label.po" />
								</label>
							</div>

						</div>

						<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
							<div class="row cols-padding-5">
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
									<dsp:input id="invoiceSearchOrderNumber" bean="ProfileFormHandler.value.invoiceSearchOrderNumber" iclass="form-control" type="text" maxlength="30">
										<dsp:tagAttribute name="placeholder" value="#" />
									</dsp:input>
								</div>

								<dsp:getvalueof var="pOrganization" bean="Profile.parentOrganization" />
								<c:if test="${not empty pOrganization}">
									<dsp:getvalueof var="orgList" bean="Profile.parentOrganization.childOrganizations" />
									<c:if test="${not empty orgList}">
										<dsp:getvalueof var="billingAccounts" bean="Profile.billingAccounts" />
										<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
											<span class="select-wrap">
												<dsp:select bean="ProfileFormHandler.value.selectedBillTo" id="selectedBillTo" iclass="form-control">
													<dsp:tagAttribute name="data-event-change-id" value="selectOrg"/>
													<option value="" selected>
														Billing Account
														<!-- <fmt:message key="account.invoices.selectOptions.billTo" /> -->
													</option>
													<dsp:droplet name="/atg/dynamo/droplet/ForEach">
														<dsp:param name="array" value="${billingAccounts}" />
														<dsp:param name="elementName" value="org" />
														<dsp:oparam name="output">
															<dsp:getvalueof var="name" param="org.name" />
															<dsp:getvalueof var="orgId" param="org.id" />
															<option value="${orgId}" title="${name}">
																${name}
															</option>
														</dsp:oparam>
													</dsp:droplet>
												</dsp:select>
											</span>
										</div>
									</c:if>
								</c:if>
                                <dsp:getvalueof var="selectedBilling" bean="ProfileFormHandler.value.selectedBillTo" />
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12" >
                                    <dsp:select bean="ProfileFormHandler.value.invoiceSearchCS" id="searchCS" >
                                        <%-- <dsp:tagAttribute name="id" value="address-results"/> --%>
									    <dsp:include page="/global/gadgets/cs-list.jsp?orgId=${selectedBilling}&showDeletedAddress=true"/>
									    <input id="autocompleteSearchCS" type="text" class="form-control" placeholder="<fmt:message key="account.invoices.selectOptions.shipTo"  />">
                                    </dsp:select>
								</div>

								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
									<!-- <span class="select-wrap"> -->
										<dsp:select bean="ProfileFormHandler.value.invoiceSearchTime" id="invoiceSearchTime" iclass="form-control">
											<option value="" disabled selected>Date Range</option>
											<option value="1">30 days</option>
											<option value="2">60 days</option>
											<option value="3">90 days</option>
											<option value="4">120 days</option>
										</dsp:select>
								<!-- 	</span>
									<span class="caret caret-select"></span> -->
								</div>

								<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
									<button data-event-click-id="eventSearchInvoiceButton"  class="btn btn-primary xs-block">Search</button>
									<button data-event-click-id="eventResetInvoiceSearch"  class="btn btn-info xs-block" id="reset-button" disabled="">Reset</button>
								</div>
							</div>
						</div>
						<dsp:input type="hidden" bean="ProfileFormHandler.value.isSearchButton" value="false" id="is-search-button" />
						<dsp:input type="hidden" bean="ProfileFormHandler.invoiceSearch" value="true" priority="-10" />
					</dsp:form>
				</div>
			</section>
			<div id="invoices-list"></div>
			<div class="text-center">
				<nav id="pagination"></nav>
			</div>
		</div>
		</main>
		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(function() {
				$("#invoiceSearchOrderNumber").on('keypress', function(event) {
					if (event.keyCode == 13) {
						event.preventDefault();
					}
				});

				loadInitialInvoices();
			});

			//			$(document).ready(function () {
			//				$("#invoices-list").load("/account/gadgets/invoices-list-display.jsp?page=1&sort=invDate&accend=false");
			//			});

			var currentPage = 1;

			//---------for invoices-list-display.jsp-----------
			//------------------------------
		</script>
	</cp:pageContainer>
</dsp:page>