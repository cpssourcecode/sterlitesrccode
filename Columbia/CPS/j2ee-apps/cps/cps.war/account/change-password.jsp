<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>

	<dsp:getvalueof var="error" param="error"/>
	<dsp:getvalueof var="errorCode" param="errorCode"/>

	<dsp:getvalueof var="profileId" param="user.repositoryId"/>
	<dsp:getvalueof var="firstName" param="user.firstName"/>
	<dsp:getvalueof var="lastName" param="user.lastName"/>
	<dsp:getvalueof var="email" param="user.email"/>


	<cp:pageContainer page="account">
		<div class="breadcrumbs-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li>
								<a href="${originatingRequest.contextPath}/account/account-landing.jsp">
								<span>Home</span>
								</a>
							</li>
							<li class="active">Reset Your Password</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div style="width: 500px; margin: auto;">
					<h2 class="modal-title">Reset Your Password</h2>

					<div class="alert alert-danger" style="display:none;" id="error-messages-reset-password"></div>
					
					<c:choose>
						<c:when test="${error}">
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="${errorCode}"/>
							</dsp:include>
						</c:when>
						<c:otherwise>
							<dsp:form action="${originatingRequest.contextPath}/account/change-password.jsp" method="post"
									  id="change-password-form" formid="change-password-form">
								<dsp:input type="hidden" bean="ProfileFormHandler.value.email" value="${email}"/>
								<dsp:input type="hidden" bean="ProfileFormHandler.value.firstName" value="${firstName}"/>
								<div class="form-group">
									<dsp:input iclass="form-control input-lg" id="input-password"
											   bean="ProfileFormHandler.value.password" type="password" autocomplete="new-password" maxlength="15">
										<dsp:tagAttribute name="placeholder" value="Enter New Password"/>
									</dsp:input>
								</div>
								<div class="form-group">
									<dsp:input iclass="form-control input-lg" id="input-password"
											   bean="ProfileFormHandler.value.confirmPassword" type="password" autocomplete="new-password"
											   maxlength="15">
										<dsp:tagAttribute name="placeholder" value="Re-enter New Password"/>
									</dsp:input>
								</div>
								<div class="form-group">
									<div class="checkbox checked" id="myCheckbox">
										<label class="checkbox-custom" data-initialize="checkbox">
											<dsp:input iclass="sr-only" type="checkbox"
													   value="false" id="change-password-rememberMe"
													   bean="ProfileFormHandler.value.rememberMe">
												<dsp:tagAttribute name="data-event-click-id" value="eventRememberMe"/>
											</dsp:input>
											<span class="checkbox-label">Remember Me</span>
										</label>
									</div>
								</div>
								<div class="form-group">
									<button type="button" data-event-click-id="eventHandleResetPassword"
											class="btn btn-warning btn-huge btn-block">Login
									</button>
								</div>
								<dsp:input type="hidden" bean="ProfileFormHandler.changePassword" value="true" priority="-10" />
							</dsp:form>
						</c:otherwise>
					</c:choose>
				</div>

			</div>
		</div>

	</cp:pageContainer>


</dsp:page>
