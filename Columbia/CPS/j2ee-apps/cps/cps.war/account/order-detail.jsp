<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/cps/droplet/CPSOrderDetailDroplet"/>
	<dsp:importbean bean="/cps/droplet/CPSProductFromIdentifierDroplet"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/commerce/order/OrderLookup"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>

	<dsp:getvalueof var="orderId" value="${vsg_utils:aesDecrypt(param.orderId)}"/>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<cp:pageContainer page="checkout">
		<div class="container ${csrClass}">

			<div class="row">
				<div class="col-sm-8">
					<ol class="breadcrumb">
						<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
						<li><a href="${originatingRequest.contextPath}/account/order-list.jsp">Order History</a></li>
						<li class="active">View Order Detail</li>
					</ol>
				</div>
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="false"/>
					<dsp:param name="print" value="true"/>
				</dsp:include>
				<div class="col-xs-12">
					<h1>View Order Detail</h1>
				</div>
				<div class="col-xs-12">
					<div class="material-list detail">
						<div class="row">
							<div class="col-sm-12 info-block">
								<dsp:droplet name="OrderLookup">
									<dsp:param name="userId" bean="Profile.repositoryId"/>
									<dsp:param name="orderId" value="${orderId}"/>
									<dsp:oparam name="output">
										<dsp:setvalue paramvalue="result" param="order"/>
										<h4 class="title">Order Number: <dsp:valueof param="order.webOrderId"/></h4>

										<div class="info">
											<div class="box">
												<strong>Ordered On</strong>
												<span><dsp:valueof param="order.submittedDate" converter="date" format="MM/dd/YYYY"/></span>
											</div>
											<div class="box">
												<strong># of Items</strong>
												<span><dsp:valueof param="order.totalCommerceItemCount"/></span>
											</div>
											<div class="box">
												<dsp:include page="/checkout/gadgets/confirmation-ship-to-address.jsp">
													<dsp:param name="order" param="order"/>
												</dsp:include>
											</div>
											<div class="box">
												<strong>Total</strong>
                                                <span>
                                                    Subtotal: <dsp:valueof param="order.priceInfo.rawSubtotal" converter="currencyConversion"/> <br>
                                                    Shipping: <dsp:valueof param="order.priceInfo.shipping" converter="currencyConversion"/> <br>
                                                    Tax: <dsp:valueof param="order.priceInfo.tax" converter="currencyConversion"/>
                                                </span>
												<br/><br/>
												<strong>Total Amount</strong>
                                                <span>
                                                    <dsp:valueof param="order.priceInfo.amount" converter="currencyConversion"/>
                                                </span>
											</div>
										</div>
									</dsp:oparam>
									<dsp:oparam name="error">
                                        <span class=profilebig>ERROR:
                                           <dsp:valueof param="errorMsg">no error message</dsp:valueof>
                                        </span>
									</dsp:oparam>
								</dsp:droplet>
							</div>


						</div>
					</div>
				</div>

				<dsp:droplet name="/cps/droplet/AccessRightDroplet">
					<dsp:param name="profile" bean="Profile"/>
					<dsp:param name="accessRightKey" value="order-detail-add-items"/>
					<dsp:oparam name="false">
						<dsp:getvalueof var="forbidAddItem" value="true"/>
					</dsp:oparam>
				</dsp:droplet>

				<div class="col-xs-12 hidden-print">
					<ul class="list-inline order-action-button-list">
						<c:choose>
							<c:when test="${forbidAddItem}">
								<li>
									<a class="btn btn-warning btn-lg" href="#" data-toggle="modal"
									   data-target="#permissionDenied">Add Selected to Cart</a>
								</li>
								<li>
									<a class="btn btn-warning btn-lg" href="#" data-toggle="modal"
									   data-target="#permissionDenied">Add Selected to Material List</a>
								</li>
							</c:when>
							<c:otherwise>
								<li>
								    <div class="form-group form-group-inline">
									    <input name="reorder-frequency" type="text" class="form-control input-qty text-center display-inline-xs" value="30"> days
									    <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-text" ><i class="fa fa-info-circle" aria-hidden="true"></i></a>
								    </div>
								    <button class="btn btn-info xs-block" data-event-click-id="eventAddSelectedToReorder" >Auto-Reorder</button>
								</li>
								<li>
									<a class="btn btn-warning btn-lg" data-event-click-id="eventAddSelectedToCart" >Add Selected to Cart</a>
								</li>
								<li>
									<a class="btn btn-warning btn-lg add-to-mat-btn">Add Selected to Material List</a>
								</li>
							</c:otherwise>
						</c:choose>
					</ul>
					<br>
				</div>
				<div id="com-items-pagination-block">
					<dsp:include page="/account/order/include/items-pagination-list.jsp">
						<dsp:param name="orderId" value="${orderId}"/>
					</dsp:include>
					<script nonce="${requestScope.nonce}">
						function onOrderItemsPaginationLoaded(){
                            $(document).ready(function () {
                                jQuery("#com-items-pagination-block .scrollbar-inner").scrollbar();
                                $("#com-items-pagination-block [data-toggle='popover']").popover();
                            });
						}
						onOrderItemsPaginationLoaded();
					</script>
				</div>

			</div>
		</div>


		<dsp:form method="post" id="order-details-add" formid="order-details-add">
			<dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsListGL"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyListGL"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.giftIds" value="false"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.orderItems" value="true"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.addItemToOrder" value="true" priority="-10"/>
		</dsp:form>


		<dsp:form method="post" id="auto-order-details-add" formid="auto-order-details-add">
			<dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsListGL"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyListGL"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.giftIds" value="false"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.orderItems" value="true"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.addItemToAutoOrder" value="true" priority="-10"/>
		</dsp:form>

		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-err-item-not-selected.jsp"/>

		<div id="modal-add-to-material-list-div"></div>

		<script nonce="${requestScope.nonce}">
			$(document).ready(function () {

				var selectedItemStorage = {};

				checkboxer({
					delegate: "#com-items-pagination-block",
					header: ".header .checkbox :checkbox",
					content: ".table-content .check :checkbox:enabled",
					checkListener: function () {
						if ($(this).checkbox("isChecked")) {
							selectedItemStorage[this.value] = $(this).parents(".content").find(".item-qty").text().trim();
						} else {
							delete selectedItemStorage[this.value];
						}
					}
				});

				function restoreTableCheckboxes() {
					for (var sku in selectedItemStorage) {
						$(".content-list .check :checkbox[value=" + sku + "]").checkbox("check");
					}
					var checkboxCount = $(".table-content .check :checkbox:enabled").length;
					var checkedCount = $(".table-content .check :checkbox:enabled:checked").length;
					if (checkedCount == checkboxCount) {
						$(".header .checkbox :checkbox").checkbox("check");
					} else {
						$(".header .checkbox :checkbox").checkbox("uncheck");
					}
				}

				$(".add-to-mat-btn").click(function () {
					onAddToMaterialLClickMultiPages();
				});

				function onAddToMaterialLClickMultiPages() {
					console.log(selectedItemStorage);
					if ($.isEmptyObject(selectedItemStorage)) {
						showAlertItemNotSelectedPopup();
						return;
					}
					var itemsInfo = Object.keys(selectedItemStorage).map(function (value) {
						return value + "=" + selectedItemStorage[value];
					}).join(",");
					resetTableCheckboxes();
					showAddToMaterialListModal(itemsInfo);
				}


				$("#com-items-pagination-block").on("click", "nav .pagination li a", function (e) {
					e.preventDefault();
				});
				$("#com-items-pagination-block").on("click", "nav .pagination li.page-number", function (e) {
					var num = $(this).text();
					$("#com-items-pagination-block").load("/account/order/include/items-pagination-list.jsp", {
						orderId: '${orderId}',
						pageNumber: num
					}, function () {
                        onOrderItemsPaginationLoaded();
						restoreTableCheckboxes();
						savePaginationState(num);
					});
				});
				$("#com-items-pagination-block").on("click", "nav .pagination li.prev", function (e) {
					var num = parseInt($("#com-items-pagination-block nav .pagination li.active").text()) - 1;
					$("#com-items-pagination-block").load("/account/order/include/items-pagination-list.jsp", {
						orderId: '${orderId}',
						pageNumber: num
					}, function () {
                        onOrderItemsPaginationLoaded();
                        restoreTableCheckboxes();
						savePaginationState(num);
					});
				});
				$("#com-items-pagination-block").on("click", "nav .pagination li.next", function (e) {
					var num = parseInt($("#com-items-pagination-block nav .pagination li.active").text()) + 1;
					$("#com-items-pagination-block").load("/account/order/include/items-pagination-list.jsp", {
						orderId: '${orderId}',
						pageNumber: num
					}, function () {
                        onOrderItemsPaginationLoaded();
                        restoreTableCheckboxes();
						savePaginationState(num);
					});
				});

			});

			function savePaginationState(pageNumber) {
				var href = "";
				if (window.location.search && window.location.search.length) {
					if (window.location.search.indexOf("pageNumber=") != -1) {
						href = window.location.href.replace(/[&]?pageNumber=\d*/, "&pageNumber=" + pageNumber);
					} else {
						href = window.location.href + "&pageNumber=" + pageNumber;
					}
				} else {
					href = window.location.href + "?pageNumber=" + pageNumber;
				}
				window.history.replaceState({}, "Title", href);
			}


		</script>
		<!-- main container end -->
	</cp:pageContainer>
</dsp:page>
