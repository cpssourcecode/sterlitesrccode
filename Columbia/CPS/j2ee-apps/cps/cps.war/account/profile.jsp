<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>

	<dsp:getvalueof var="ln" param="ln" />
	<dsp:getvalueof var="pr" param="pr" />
	<dsp:getvalueof var="rd" param="rd" />

	<dsp:droplet name="/cps/droplet/ChangePasswordDroplet">
		<dsp:param name="ln" value="${ln}"/>
		<dsp:param name="pr" value="${pr}"/>
		<dsp:param name="rd" value="${rd}"/>
		<dsp:oparam name="empty">

			<dsp:include page="${originatingRequest.contextPath}/account/profile-settings.jsp"/>
		</dsp:oparam>
		<dsp:oparam name="success">

			<dsp:include page="${originatingRequest.contextPath}/account/change-password.jsp">
				<dsp:param name="user" param="user"/>
			</dsp:include>

		</dsp:oparam>
		<dsp:oparam name="error">

			<dsp:include page="${originatingRequest.contextPath}/account/change-password.jsp">
				<dsp:param name="user" param="user"/>
				<dsp:param name="errorCode" param="errorCode"/>
				<dsp:param name="error" value="${true}"/>
			</dsp:include>

		</dsp:oparam>
	</dsp:droplet>

</dsp:page>
