<dsp:page>
  <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
  <cp:pageContainer page="account">
    <div id="breadcrumb">
      <div class="center">
        <p><a href="#">Home</a> &raquo; <a href="#">Lorem Ipsum</a></p>
      </div>
    </div>
    <div id="body" class="account">
      <div class="center">
        <dsp:include page="gadgets/sidebar.jsp" flush="true"/>
        <div id="content">
          <h1>Manage Orders</h1>
          <div class="message">
            <p>You have <strong>2 orders</strong> awaiting approval.</p>
          </div>
          <div class="divider">-</div>
          <div class="account-section">
            <dl class="form">
              <dt class="form-float">
                <label>Search Orders:</label>
              </dt>
              <dd class="form-float">
                <input type="text" class="form-l" value="CS, Item#, Keyword" />
              </dd>
              <dt class="form-float">
                <label>Show:</label>
              </dt>
              <dd class="form-float">
                <select class="form-l">
                  <option selected="selected">Last 30 Days</option>
                </select>
              </dd>
              <dd class="account-submit">
                <input type="submit" class="button button-grey" value="Reset" />
                <input type="submit" class="button" value="Search" />
              </dd>
              <dd class="clear">&nbsp;</dd>
            </dl>
            <div class="listings">
              <ul class="paging">
                <li class="paging-prev"><a href="#">&laquo; Previous</a></li>
                <li class="paging-next"><a href="#">Next &raquo;</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li class="active">3</li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>...</li>
                <li><a href="#">22</a></li>
              </ul>
            </div>
            <div class="header"> <span class="order-id">Order # <a href="#" class="sort">Sort</a></span> <span class="order-date">Date <a href="#" class="sort sort-down">Sort</a></span> <span class="order-job">CS <a href="#" class="sort">Sort</a></span> <span class="order-total">Total <a href="#" class="sort">Sort</a></span> <span class="order-status">Status <a href="#" class="sort">Sort</a></span> </div>
            <ul class="account-list order-list">
              <li class="account-list-highlight"> <span class="account-list-name order-id"><a href="#">543543543543</a></span> <span class="order-date">10/1/2012</span> <span class="order-job">CS Name</span> <span class="order-total">$100.67</span> <span class="order-status">Pending</span> <span class="account-list-actions order-actions"> <a class="button button-bug" href="#">Approve</a> <a class="button button-bug button-red" href="#">Decline</a> </span> </li>
              <li class="account-list-highlight"> <span class="account-list-name order-id"><a href="#">543543543543</a></span> <span class="order-date">10/1/2012</span> <span class="order-job">CS Name</span> <span class="order-total">$100.67</span> <span class="order-status">Pending</span> <span class="account-list-actions order-actions"> <a class="button button-bug" href="#">Approve</a> <a class="button button-bug button-red" href="#">Decline</a> </span> </li>
              <li> <span class="account-list-name order-id"><a href="#">543543543543</a></span> <span class="order-date">10/1/2012</span> <span class="order-user"><a href="#">John Smith</a></span> <span class="order-total">$100.67</span> <span class="order-status red">Declined</span> </li>
              <li> <span class="account-list-name order-id"><a href="#">543543543543</a></span> <span class="order-date">10/1/2012</span> <span class="order-user"><a href="#">John Smith</a></span> <span class="order-total">$100.67</span> <span class="order-status">Processing</span> </li>
              <li> <span class="account-list-name order-id"><a href="#">543543543543</a></span> <span class="order-date">10/1/2012</span> <span class="order-user"><a href="#">John Smith</a></span> <span class="order-total">$100.67</span> <span class="order-status">Processing</span> </li>
              <li> <span class="account-list-name order-id"><a href="#">543543543543</a></span> <span class="order-date">10/1/2012</span> <span class="order-user"><a href="#">John Smith</a></span> <span class="order-total">$100.67</span> <span class="order-status"><a href="#">Shipped on 10/3/2012</a></span> </li>
            </ul>
          </div>
        </div>
        <div class="clear">&nbsp;</div>
      </div>
    </div>
    <%--</div>--%>
  </cp:pageContainer>
</dsp:page>
