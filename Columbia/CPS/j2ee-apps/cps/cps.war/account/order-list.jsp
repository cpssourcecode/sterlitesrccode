<%@ taglib prefix="dspeltags" uri="http://www.atg.com/taglibs/daf/dspjspELTaglib1_0" %>
<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/cps/reorders/AutoOrderFormHandler"/>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<cp:pageContainer page="account-order">
		<main id="body">
			<div class="container ${csrClass}">
				<div class="row">
					<div class="col-md-8">
						<ol class="breadcrumb">
							<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
							<li class="active">Order History</li>
						</ol>
					</div>
					<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
						<dsp:param name="print" value="true"/>
					</dsp:include>
				</div>
		        <div class="container preload-cover spinner-on-start">
					<div class="loading">
						<i class="fa fa-spinner fa-spin"></i>
					</div>
				</div>
				<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-err-search-order.jsp"/>

				<input type="hidden" value="4" id="numPerPage"/>

				<h1 class="mb20"><fmt:message key="account.orders.header.order_history"/></h1>

				<dsp:droplet name="/atg/dynamo/droplet/IsNull">
					<dsp:param name="value" bean="Profile.parentOrganization"/>
					<dsp:oparam name="false">
						<section>
							<h3 class="h4 text-nocase">Search Orders</h3>
							<ul>
								<li>Searching by Web Confirmation #, Sales Order #, or PO # will return ALL Order History records for the last 2 years.</li>
								<li>Searching by Ship to Address will return Order History records up to 2 years old.</li>
								<li>Searching by Date Range will return records up to 30, 60, 90, or 120 days old.</li>
							</ul>
						</section>

						<section>
							<dsp:form action="#" method="post" id="search-orders">
								<dsp:input id="orderSearchPageSize" bean="ProfileFormHandler.value.orderSearchPageSize" type="hidden" value="20" />
								<dsp:input id="orderSearchStartIndex" bean="ProfileFormHandler.value.orderSearchStartIndex" type="hidden" value="0" />
								<dsp:input id="orderSearchSortField" bean="ProfileFormHandler.value.orderSearchSortField" type="hidden" value="" />
								<dsp:input id="orderSearchSortOption" bean="ProfileFormHandler.value.orderSearchSortOption" type="hidden" value="" />

								<div class="row mt40">
									<div class="col-md-2 col-sm-3 mt-20">
										<div class="radio">
											<label>
												<dsp:input bean="ProfileFormHandler.value.orderSearchFieldName" iclass="radio-custom" id="orderId" type="radio" value="orderId" name="radioEx1">
													<dsp:tagAttribute name="data-event-click-id" value="eventOrderSearchType" />
												</dsp:input> Web Confirmation #
											</label>
										</div>
										<div class="radio">
											<label>
												<dsp:input bean="ProfileFormHandler.value.orderSearchFieldName" iclass="radio-custom" id="salesOrderId" type="radio" value="salesOrderId" name="radioEx1">
													<dsp:tagAttribute name="data-event-click-id" value="eventOrderSearchType" />
												</dsp:input> Sales Order
											</label>
										</div>
										<div class="radio">
											<label>
												<dsp:input bean="ProfileFormHandler.value.orderSearchFieldName" iclass="radio-custom" id="myCustomRadioLabel3" type="radio" value="poNumber" name="radioEx1">
													<dsp:tagAttribute name="data-event-click-id" value="eventOrderSearchType" />
												</dsp:input> PO #
											</label>
										</div>
									</div>
									<div class="col-md-10 col-sm-9">
										<div class="form-group form-group-inline w15">
											<dsp:input type="text" bean="ProfileFormHandler.value.orderSearchTerm" iclass="form-control" value="" id="orderSearchTerm" maxlength="30">
												<dsp:tagAttribute name="placeholder" value="#"/>
											</dsp:input>
										</div>
										<dsp:getvalueof var="pOrganization" bean="Profile.parentOrganization" />
										<c:if test="${not empty pOrganization}">
											<dsp:getvalueof var="orgList" bean="Profile.parentOrganization.childOrganizations" />
											<c:if test="${not empty orgList}">
												<dsp:getvalueof var="billingAccounts" bean="Profile.billingAccounts" />
												<div class="form-group form-group-inline w15">
											<span class="select-wrap">
												<dsp:select bean="ProfileFormHandler.value.selectedBillTo" id="selectedBillTo" iclass="form-control">
													<dsp:tagAttribute name="data-event-change-id" value="selectOrg"/>
													<option value="" selected>
														Billing Account
														<!-- <fmt:message key="account.invoices.selectOptions.billTo" /> -->
													</option>
													<dsp:droplet name="/atg/dynamo/droplet/ForEach">
														<dsp:param name="array" value="${billingAccounts}" />
														<dsp:param name="elementName" value="org" />
														<dsp:oparam name="output">
															<dsp:getvalueof var="name" param="org.name" />
															<dsp:getvalueof var="orgId" param="org.id" />
															<option value="${orgId}" title="${name}">
																	${name}
															</option>
														</dsp:oparam>
													</dsp:droplet>
												</dsp:select>
											</span>
												</div>
											</c:if>
										</c:if>
                                        <dsp:getvalueof var="selectedBilling" bean="ProfileFormHandler.value.selectedBillTo" />
                                        <div class="form-group form-group-inline w20">
                                            <dsp:select bean="ProfileFormHandler.value.orderSearchCS" id="searchCS">
                                                <%-- <dsp:tagAttribute name="id" value="address-results"/> --%>
                                                <dsp:include page="/global/gadgets/cs-list.jsp?orgId=${selectedBilling}&showDeletedAddress=true"/>
                                            </dsp:select>
                                            <input id="autocompleteSearchCS" type="text" class="form-control" placeholder="<fmt:message key="account.invoices.selectOptions.shipTo"  />">
										</div>

										<div class="form-group form-group-inline w20">
										<!-- <span class="select-wrap"> -->
											<dsp:select bean="ProfileFormHandler.value.orderSearchTime" id="orderSearchTime" iclass="form-control">
												<option value="" disabled selected>Date Range</option>
												<option value="1">30 days</option>
												<option value="2">60 days</option>
												<option value="3">90 days</option>
												<option value="4">120 days</option>
												<!-- <span class="caret caret-select"></span> -->
											</dsp:select>
										<!-- </span> -->
										</div>
										<span class="sm-block mt5">
										<button class="btn btn-primary xs-block" href="#" data-event-click-id="eventSearchOrdersButton" >
											<fmt:message key="account.order.button.search"/>
										</button>
										<button id="reset-button" class="btn btn-info xs-block" disabled href="#" data-event-click-id="eventResetSearch" >
											<fmt:message key="account.packing.reset"/>
										</button>
									</span>
									</div>
								</div>
								<dsp:input type="hidden" bean="ProfileFormHandler.value.isSearchButton" value="false" id="is-search-button"/>
								<dsp:input type="hidden" bean="ProfileFormHandler.orderSearch" value="true" priority="-10"/>
							</dsp:form>
						</section>

						<div id="order-list"></div>

						<div class="text-center">

							<nav id="pagination"></nav>

						</div>

						<script type="text/javascript" nonce="${requestScope.nonce}">
                            $(document).ready(function() {
                                $("#orderSearchTerm").on('keypress', function(event) {
                                    if (event.keyCode == 13) {
                                        event.preventDefault();
                                    }
                                });
                                
                                loadInitialOrders();
                            });

                            function resetSearchButton(){
                                document.getElementById("reset-button").disabled = false;
                            }

                            function uncheckSearchParameters(){
                                $(".radio-custom").each(function() {
                                    this.checked = false;
                                });
                            }
						</script>

					</dsp:oparam>
					<dsp:oparam name="true">
						<p>
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="user-no-org"/>
							</dsp:include>
						</p>
					</dsp:oparam>
				</dsp:droplet>

			</div>
		</main>
		<script type="text/javascript" nonce="${requestScope.nonce}">
            //implements behavior of radio button's for checkboxes
            var selectedBox = null;
            $(document).ready(function() {
                $(".radio-custom").click(function() {
                    selectedBox = this.id;

                    $(".radio-custom").each(function() {
                        if ( this.id == selectedBox )
                        {
                            this.checked = true;
                        }
                        else
                        {
                            this.checked = false;
                        };
                    });
                });
            });
		</script>
	</cp:pageContainer>
</dsp:page>
