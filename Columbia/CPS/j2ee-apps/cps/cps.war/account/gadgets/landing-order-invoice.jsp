<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/cps/reorders/AutoOrderFormHandler"/>

	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-cancel-reorder.jsp"/>
	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-success-cancel-reorder.jsp"/>

	<dsp:droplet name="/cps/droplet/AccessRightDroplet">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="accessRightKey" value="recent-invoices" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="showInvoices" value="true" />
		</dsp:oparam>
	</dsp:droplet>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<c:if test="${showInvoices}">
                <c:choose>
                    <c:when test="${param.tab eq 'autoOrders'}">
                        <li role="presentation">
                            <a href="#rinvoices" aria-controls="rinvoices" role="tab" data-toggle="tab">Recent Invoices</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li role="presentation" class="active">
                            <a href="#rinvoices" aria-controls="rinvoices" role="tab" data-toggle="tab">Recent Invoices</a>
                        </li>
                    </c:otherwise>
                </c:choose>
			</c:if>
			<li role="presentation">
				<a href="#rorders" aria-controls="rorders" role="tab" data-toggle="tab">Recent Orders</a>
			</li>

            <c:choose>
                <c:when test="${param.tab eq 'autoOrders'}">
                    <li role="presentation" class="active">
                        <a href="#autoOrders" aria-controls="autoOrders" role="tab" data-toggle="tab">Auto-Reorders</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li role="presentation">
                        <a href="#autoOrders" aria-controls="autoOrders" role="tab" data-toggle="tab">Auto-Reorders</a>
                    </li>
                </c:otherwise>
            </c:choose>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<c:if test="${showInvoices}">

                <c:choose>
                <c:when test="${param.tab eq 'autoOrders'}">
                <div role="tabpanel" class="tab-pane fade " id="rinvoices">
                    </c:when>
                    <c:otherwise>
                    <div role="tabpanel" class="tab-pane fade in active" id="rinvoices">
                        </c:otherwise>
                        </c:choose>

					<dsp:droplet name="/cps/droplet/InvoicesDroplet">
						<dsp:param name="profile" bean="Profile" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="emptyResult" param="emptyResult" />
							<c:if test="${!emptyResult}">
								<table class="table table-striped table-mobile">
									<thead>
										<tr>
											<th class="text-center">Invoice #</th>
											<th class="text-center">Invoice Date</th>
											<th>Ship To</th>
											<th class="text-center">Amount</th>
											<th class="text-center">Status</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<dsp:droplet name="/atg/dynamo/droplet/ForEach">
											<dsp:param name="array" param="invoices" />
											<dsp:param name="elementName" value="invoice" />
											<dsp:oparam name="output">
												<dsp:getvalueof var="invId" param="invoice.id" />
												<dsp:getvalueof var="invNumber" param="invoice.invoiceNumber" />
												<dsp:getvalueof var="invDate" param="invoice.invoiceDate" />
												<dsp:getvalueof var="invAddress" param="invoice.shipToName" />
												<dsp:getvalueof var="invAmount" param="invoice.invoiceAmount" />
												<dsp:getvalueof var="invLink" param="invoice.url" />
												<tr>
													<td class="text-center text-left-xs">
														<span class="caption">Invoice #</span>${invNumber}</td>
													<td class="text-center text-left-xs">
														<span class="caption">Invoice Date</span>
														<fmt:formatDate pattern="MM/dd/yyyy" value="${invDate}" />
													</td>
													<td>
														<span class="caption">Ship To</span>${invAddress}</td>
													<td class="text-center text-left-xs">
														<span class="caption">Amount</span>
														<dsp:valueof value="${invAmount}" converter="currencyConversion" />
													</td>
													<td class="text-center">
														<span id="status${invNumber}">
															<a class="table-icon-link" href="#" data-event-click-id="eventRequestInvoiceStatus" data-invoice-id="${invNumber}" >
																<span>Get Status</span>
															</a>
														</span>
													</td>
													<td class="text-center">
														<a href="${invLink}" target="_blank">
															<i class="icon icon-paper-page" aria-hidden="true"></i>
															View Details
														</a>
													</td>
												</tr>
											</dsp:oparam>
										</dsp:droplet>
									</tbody>
								</table>
								<p>
								<a href="${originatingRequest.contextPath}/account/invoices.jsp" class="btn btn-primary">Search Invoices</a>
								</p>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</c:if>

			<c:choose>
				<c:when test="${showInvoices or param.tab eq 'autoOrders'}">
					<div role="tabpanel" class="tab-pane fade" id="rorders">
				</c:when>
				<c:otherwise>
					<div role="tabpanel" class="tab-pane fade in active" id="rorders">
				</c:otherwise>
			</c:choose>

			<dsp:droplet name="/cps/droplet/OrdersDroplet">
				<dsp:param name="profile" bean="Profile" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="emptyResult" param="emptyResult" />
					<c:if test="${!emptyResult}">
						<table class="table table-striped table-mobile">
							<thead>
								<tr>
									<th class="text-center">Order #</th>
									<th class="text-center">Order Date</th>
									<th class="text-center">Total</th>
									<th>Ship To</th>
									<th class="text-center">Status</th>
								</tr>
							</thead>
							<tbody>
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="orders" />
									<dsp:param name="elementName" value="order" />
									<dsp:oparam name="output">

										<dsp:getvalueof var="orderNumber" param="order.orderNumber" />
										<dsp:getvalueof var="date" param="order.date" />
										<dsp:getvalueof var="shipToName" param="order.shipToName" />
										<dsp:getvalueof var="amount" param="order.amount" />
										<dsp:getvalueof var="link" param="order.url" />
										<tr>
											<td class="text-center text-left-xs">
												<span class="caption">Order #</span>${orderNumber}</td>
											<td class="text-center text-left-xs">
												<span class="caption">Order Date</span>
												<fmt:formatDate pattern="MM/dd/yyyy" value="${date}" />
											</td>
											<td class="text-center text-left-xs">
												<span class="caption">Total</span>
												<dsp:valueof value="${amount}" converter="currencyConversion" />
											</td>
											<td>
												<span class="caption">Ship To</span>${shipToName}</td>
											<td class="text-center">
												<a href="${originatingRequest.contextPath}/account/order-detail-ws.jsp?orderId=${vsg_utils:aesEncrypt(orderNumber)}"><fmt:message key="account.orders.get_status"/></a>
											</td>
										</tr>
									</dsp:oparam>
								</dsp:droplet>
							</tbody>
						</table>
						<p>
						<a href="${originatingRequest.contextPath}/account/order-list.jsp" class="btn btn-primary">Search Orders</a>
						</p>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>
			</div>

			<c:choose>
				<c:when test="${param.tab eq 'autoOrders'}">
					<div role="tabpanel" class="tab-pane fade in active" id="autoOrders">
				</c:when>
				<c:otherwise>
					<div role="tabpanel" class="tab-pane fade" id="autoOrders">
				</c:otherwise>
			</c:choose>
			
			<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
				<dsp:param name="messageKey" value="reorderDisclaimerMessage" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="reorderDisclaimerMessage" param="message" />
				</dsp:oparam>
			</dsp:droplet>

				<dsp:droplet name="/cps/droplet/CPSActiveAutoOrdersDroplet">
					<dsp:param name="profile" bean="Profile"/>
					<dsp:param name="loadList" value="true"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="hasAutoOrders" param="hasAutoOrders"/>
						<dsp:getvalueof var="autoOrderList" param="autoOrderList"/>

						<c:if test="${hasAutoOrders}">
							<table class="table table-striped table-mobile">
								<thead>
								<tr>
									<th class="text-center">Order #</th>
									<th class="text-center">Next Auto-Reorder Date</th>
									<th class="text-center">Total</th>
									<th>Ship To</th>
									<th class="text-center"></th>
								</tr>
								</thead>
								<tbody>
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="autoOrderList"/>
									<dsp:param name="elementName" value="order"/>
									<dsp:oparam name="output">

										<dsp:getvalueof var="orderNumber" param="order.autoOrderId"/>
										<dsp:getvalueof var="date" param="order.nextSubmitDate"/>
										<dsp:getvalueof var="shipToName" param="order.shipToAddress"/>
										<dsp:getvalueof var="amount" param="order.currentTotalAmount"/>
										<dsp:getvalueof var="enabled" param="order.enabled"/>

										<tr>
											<td class="text-center text-left-xs">
												<span class="caption">Order #</span>
													${orderNumber}
											</td>
											<td class="text-center text-left-xs">
												<span class="caption">Next Auto-Reorder Date</span>
												<fmt:formatDate pattern="MM/dd/yyyy" value="${date}"/>
											</td>
											<td class="text-center text-left-xs">
												<span class="caption">Total</span>
												<dsp:valueof value="${amount}" converter="currencyConversion"/>
											</td>
											<td>
												<span class="caption">Ship To</span>${shipToName}</td>
											<td class="text-center">
												<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-text" class="btn btn-sm" data-event-click-id="eventCancelAutoOrder" data-auto-order-id="${orderNumber}" >
													<fmt:message key="account.orders.cancel_auto_order"/>
												</a>
											</td>
										</tr>
									</dsp:oparam>
								</dsp:droplet>
								</tbody>
							</table>
							<c:if test="${not empty orderNumber }">
								<p class="reorder-disclaimer">${reorderDisclaimerMessage}</p>
							</c:if>
							<p>
								<a href="${originatingRequest.contextPath}/account/order-list.jsp"
								   class="btn btn-primary">Search Orders</a>
							</p>
						</c:if>
					</dsp:oparam>
				</dsp:droplet>
			</div>
			<dsp:form id="cancel-auto-order" formid="cancel-auto-order" method="POST">
				<dsp:input type="hidden" bean="AutoOrderFormHandler.autoOrderId" value="" id="addAutoOrderID"/>
				<dsp:input type="hidden" bean="AutoOrderFormHandler.cancelAutoOrder" value="Submit" priority="-10"/>
			</dsp:form>
		</div>
</dsp:page>