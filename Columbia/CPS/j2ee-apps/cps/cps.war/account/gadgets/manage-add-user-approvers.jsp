<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
	<dsp:droplet name="/cps/droplet/ManageApproversDroplet">
		<dsp:oparam name="output">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="approvers"/>
				<dsp:param name="elementName" value="approver"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="appId" param="approver.id"/>
					<dsp:getvalueof var="appFirstName" param="approver.firstName"/>
					<dsp:getvalueof var="appLastName" param="approver.lastName"/>
                    <div class="checkbox" id="myCheckbox">
                    	<label>
                    		<dsp:input id="${vsg_utils:escapeHtml(appId)}" name="check" bean="MultiUserAddFormHandler.approversMap.${appId}" type="checkbox" />
                            ${vsg_utils:escapeHtml(appFirstName)}&nbsp;${vsg_utils:escapeHtml(appLastName)}
                    	</label>
                    </div>
                </dsp:oparam>
				<dsp:oparam name="empty">
					<p>Warning! Your organization currently has no approvers associated with it. Please
						create a new approver before creating a new buyer.</p>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>