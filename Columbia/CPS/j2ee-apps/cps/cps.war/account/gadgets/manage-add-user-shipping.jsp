<dsp:page>
    <dsp:getvalueof var="companyId" param="companyId"/>
    <dsp:getvalueof var="companyIds" param="companyIds"/>
    <dsp:getvalueof var="userId" param="userId"/>
    <dsp:getvalueof var="addressList" param="addressList"/>
    <dsp:getvalueof var="defaultId" param="defaultId"/>
    <dsp:getvalueof var="removeId" param="removeId"/>
    <dsp:getvalueof var="pageType" param="pageType"/>
    <dsp:getvalueof var="searchText" param="searchText"/>
    <h4 class="text-nocase">Ship To Addresses</h4>
    <div class="well">
    <dsp:include page="/global/modals/modal-admin-confirm.jsp"></dsp:include>
    <dsp:droplet name="/cps/droplet/ManageUsersShipping">
        <dsp:param name="companyId" value="${companyId}"/>
        <dsp:param name="companyIds" value="${companyIds}"/>
        <dsp:param name="userId" value="${userId}"/>
        <dsp:param name="addressList" value="${addressList}"/>
        <dsp:param name="defaultId" value="${defaultId}"/>
        <dsp:param name="removeId" value="${removeId}"/>
        <dsp:param name="searchTerm" value="${searchText}"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="defaultCS" param="defaultCS.addressItem"/>
            <dsp:getvalueof var="showBillingAccount" param="showBillingAccount"/>
            <h5>Default</h5>
            <address>
                <dsp:valueof value="${vsg_utils:contactInfoAddress(defaultCS)}" valueishtml="true" />
            </address>
            </div>
             <c:if test="${pageType == 'add'}">
            <p>
				<a href="javascript:void(0);" id="btnCreateUser"  class="btn btn-primary">Save User</a>
				<a href="/account/manage-users.jsp" class="btn btn-info">Cancel</a>
			</p></c:if>
            <c:if test="${pageType != 'add'}">
                <p><cp:repositoryMessage key="selectDefaultAddressAsAdmin" /> </p>
              <%--  <p>
                    <button type="button" data-toggle="modal" data-target="#addShipToAddress" class="btn btn-info ">
                        Add Ship To Address
                    </button>
                </p>--%>
                <p>
					<a href="javascript:void(0);" id="btnUpdateUser" data-event-click-id="btnUpdateUser"  class="btn btn-primary">Save User</a>
					<a href="/account/manage-users.jsp" class="btn btn-info">Cancel</a>
				</p>
            </c:if>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 100px;">Default</th>
                        <c:if test="${showBillingAccount}"><th style="width: 20%;">Billing Account</th></c:if>
                        <th>Ship To</th>
                        <th><fmt:message key="account.address.label.account_number"/></th>
                        <th class="text-center" style="width: 50px;">&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                        <dsp:param name="array" param="currentList"/>
                        <dsp:param name="elementName" value="cs"/>
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="contactInfo" param="cs.addressItem"/>
                            <dsp:getvalueof var="csId" param="cs.id"/>
                            <dsp:getvalueof var="isdefault" param="cs.isDefault"/>
                            <dsp:getvalueof var="isactive" param="cs.isActive"/>
                            <dsp:getvalueof var="address1" param="cs.addressItem.address1"/>
                            <dsp:getvalueof var="address2" param="cs.addressItem.address2"/>
                            <dsp:getvalueof var="city" param="cs.addressItem.city"/>
                            <dsp:getvalueof var="state" param="cs.addressItem.state"/>
                            <dsp:getvalueof var="postalCode" param="cs.addressItem.postalCode"/>
                            <dsp:getvalueof var="orgId" param="cs.organization.id"/>
                            <dsp:getvalueof var="orgName" param="cs.organization.name"/>
                            <dsp:getvalueof var="billingAddressId" param="cs.organization.billingAddress.id"/>
							<dsp:getvalueof var="derivedBillingAddressId" param="cs.organization.derivedBillingAddress.id"/>
                            <c:if test="${not isdefault}">
                            <tr>
                                <td class="text-center">
                                    <label>
                                        <input type="radio" id="adminEdit" name="radioEx11" <c:if test="${isdefault}">checked</c:if> data-event-click-id="eventDefaultShipping" data-cs-id="${csId}"
                                               <c:if test="${!isactive}">disabled="disabled"</c:if> />
                                        <c:if test="${showBillingAccount}">
                                            <span class="visible-xs-inline ml5"><dsp:valueof value="${orgName}"/></span>
                                        </c:if>
                                    </label>
                                </td>
                                <c:if test="${showBillingAccount}">
                                    <td class="hidden-xs"><dsp:valueof value="${orgName}"/></td>
                                </c:if>
                                <td id="address-${csId}">
                                    <dsp:valueof value="${vsg_utils:contactInfoAddress(contactInfo)}" valueishtml="true" />
                                </td>
                                <%-- SM-188 Display account number on ship to address table --%>
                                <c:choose>
									<c:when test="${showBillingAccount}">
										<c:choose>
											<c:when test="${not empty billingAddressId}">
												<td>${billingAddressId}-${csId}</td>
											</c:when>
											<c:otherwise>
												<td>${derivedBillingAddressId}-${csId}</td>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<td>${csId}</td>
									</c:otherwise>
								</c:choose>
								
                                <td class="text-right">
                                    <c:choose>
                                        <c:when test="${isactive}">
                                            <a href="javascript:void(0);" data-is-active="${isactive}" data-event-click-id="eventRemoveShipping" data-cs-id="${csId}"  class="btn btn-link remove-shipping-btn">Remove</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="javascript:void(0);" data-is-active="${isactive}" data-event-click-id="eventRemoveShipping" data-cs-id="${csId}"  class="btn btn-link remove-shipping-btn"><span>Restore</span></a>
                                        </c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            </c:if>
                        </dsp:oparam>
                    </dsp:droplet>
                    </tbody>
                </table>
            </div>
        </dsp:oparam>
    </dsp:droplet>

</dsp:page>