<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<h4 class="text-nocase">Login / Security Information</h4>
	<div class="profile-login collapse in" aria-expanded="true">
		<div class="row">
			<div class="col-sm-8">
				<ul class="list-unstyled">
					<li>
						<strong>User ID:</strong>
						<dsp:valueof bean="Profile.login" />
					</li>
					<li>
						<strong>Password:</strong>
						************
					</li>
				</ul>
			</div>
			<div class="col-sm-4 text-right ">
				<button class="btn btn-info" type="button" id="securityInfoButton" data-toggle="collapse" data-target=".profile-login" aria-expanded="false" aria-controls="profile-login">Edit</button>
			</div>
		</div>
	</div>
	<dsp:form method="post" action="/account/profile-settings.jsp" id="edit-security-info">
		<dsp:getvalueof var="securityFormExceptions" vartype="java.lang.Object" bean="ProfileFormHandler.securityFormExceptions" />
		<dsp:getvalueof var="updateSuccessful" bean="ProfileFormHandler.securityUpdateSuccessful" />
		<c:if test="${updateSuccessful}">
			<script type="text/javascript" nonce="${requestScope.nonce}">
				document.getElementById("success-message").className = "alert alert-success";
				document.getElementById("success-message").innerHTML = "Security information changed successfully";
			</script>
		</c:if>
		<c:if test="${not empty securityFormExceptions}">
			<script type="text/javascript" nonce="${requestScope.nonce}">
				jQuery(document).ready(function() {
					$("#securityInfoButton").click();
				});
			</script>
			<c:forEach var="securityFormException" items="${securityFormExceptions}">
				<dsp:param name="securityFormException" value="${securityFormException}" />
				<dsp:getvalueof var="errorCode" param="securityFormException.propertyPath" />
				<script type="text/javascript" nonce="${requestScope.nonce}">
					jQuery(document).ready(function() {
						loadErrors('${errorCode}');
					});
					function loadErrors(errorCode) {
						$("#" + errorCode).parent().addClass("has-error");
					}
				</script>
				<c:if test="${securityFormException.message != ''}">
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">
								<span class="glyphicon glyphicon-remove-circle"></span>
							</span>
						</button>
						<dsp:valueof param="securityFormException.message" valueishtml="true" />
					</div>
				</c:if>
			</c:forEach>
		</c:if>
		<div class="profile-login collapse" aria-expanded="false" style="height: 0px;">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<dsp:input type="password" autocomplete="new-password" iclass="form-control" id="oldPassword" value="" bean="ProfileFormHandler.value.oldPassword" name="oldPassword" maxlength="15">
							<dsp:tagAttribute name="placeholder" value="Old Password" />
						</dsp:input>
					</div>
					<div class="form-group">
						<dsp:input type="password" autocomplete="new-password" iclass="form-control" value="" id="newPassword" bean="ProfileFormHandler.value.newPassword" name="newPassword" maxlength="15">
							<dsp:tagAttribute name="placeholder" value="New Password" />
						</dsp:input>
						<button type="button" class="btn btn-link topright visible-xs mr10" data-toggle="collapse" data-target=".profile-password" aria-expanded="false" aria-controls="profile-password">
							<i class="fa fa-info-circle"></i>
						</button>
					</div>
					<div class="form-group">
						<dsp:input type="password" autocomplete="new-password" id="confirmPassword" iclass="form-control" value="" bean="ProfileFormHandler.value.confirmPassword" name="confirmPassword" maxlength="15">
							<dsp:tagAttribute name="placeholder" value="Confirm New Password" />
						</dsp:input>
					</div>
				</div>
				<div class="col-sm-6 collapse profile-password">
					<div class="well well-grey" style="padding: 14px;">
						<h5>Password rules</h5>
						<ul>
							<li>Minimum of six and maximum of 15 characters;</li>
							<li>Contain at least one uppercase alphabetic character;</li>
							<li>Contain at least one numeric character;</li>
							<li>Contain at least one special character (examples: !, @, #, $, %, &amp;, *, _).</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="text-right ">
				<button data-event-click-id="eventSubmitProfileSecurityUpdate"  class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample0" aria-expanded="false" aria-controls="collapseExample0">Save</button>
				<button class="btn btn-info" type="button" data-toggle="collapse" data-target=".profile-login" aria-expanded="false" aria-controls="profile-login">Cancel</button>
			</div>
		</div>
		<dsp:input type="hidden" value="true" bean="ProfileFormHandler.updateSecurityInfo" priority="-10" />
		<dsp:input type="hidden" bean="ProfileFormHandler.updateSuccessURL" value="/account/profile-settings.jsp" />
		<dsp:input type="hidden" bean="ProfileFormHandler.updateErrorURL" value="/account/profile-settings.jsp" />
	</dsp:form>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		function submitProfileSecurityUpdate() {
			$("#edit-security-info").submit();
		}
	</script>
</dsp:page>