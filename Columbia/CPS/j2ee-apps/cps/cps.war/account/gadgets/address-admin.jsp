<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<div class="account-section">
		<dsp:droplet name="/atg/dynamo/droplet/IsNull">
			<dsp:param name="value" bean="Profile.selectedOrg"/>
			<dsp:oparam name="true">
				<dsp:droplet name="/atg/dynamo/droplet/IsNull">
					<dsp:param name="value" bean="Profile.parentOrganization"/>
					<dsp:oparam name="true">
						<dsp:getvalueof var="companyName" bean="Profile.derivedBillingAddress.companyName"/>
						<dsp:getvalueof var="address1" bean="Profile.derivedBillingAddress.address1"/>
						<dsp:getvalueof var="address2" bean="Profile.derivedBillingAddress.address2"/>
						<dsp:getvalueof var="city" bean="Profile.derivedBillingAddress.city"/>
						<dsp:getvalueof var="state" bean="Profile.derivedBillingAddress.state"/>
						<dsp:getvalueof var="postalCode" bean="Profile.derivedBillingAddress.postalCode"/>
					</dsp:oparam>
					<dsp:oparam name="false">
						<dsp:getvalueof var="companyName" bean="Profile.parentOrganization.billingAddress.companyName"/>
						<dsp:getvalueof var="address1" bean="Profile.parentOrganization.billingAddress.address1"/>
						<dsp:getvalueof var="address2" bean="Profile.parentOrganization.billingAddress.address2"/>
						<dsp:getvalueof var="city" bean="Profile.parentOrganization.billingAddress.city"/>
						<dsp:getvalueof var="state" bean="Profile.parentOrganization.billingAddress.state"/>
						<dsp:getvalueof var="postalCode" bean="Profile.parentOrganization.billingAddress.postalCode"/>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
			<dsp:oparam name="false">
				<dsp:getvalueof var="companyName" bean="Profile.selectedOrg.billingAddress.companyName"/>
				<dsp:getvalueof var="address1" bean="Profile.selectedOrg.billingAddress.address1"/>
				<dsp:getvalueof var="address2" bean="Profile.selectedOrg.billingAddress.address2"/>
				<dsp:getvalueof var="city" bean="Profile.selectedOrg.billingAddress.city"/>
				<dsp:getvalueof var="state" bean="Profile.selectedOrg.billingAddress.state"/>
				<dsp:getvalueof var="postalCode" bean="Profile.selectedOrg.billingAddress.postalCode"/>
			</dsp:oparam>
		</dsp:droplet>
		<%@include file="/account/gadgets/addresses-cb-display.jspf" %>
	</div>
	<div class="divider">-</div>
	<div class="account-section">
		<h2>Shipping Addresses (CS)</h2>
		<ul class="account-list account-address">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.selectedOrg.secondaryAddresses"/>
				<dsp:param name="elementName" value="address"/>
				<dsp:oparam name="output">
					<%@include file="/account/gadgets/addresses-cs-display.jspf" %>
				</dsp:oparam>
				<dsp:oparam name="empty">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="Profile.parentOrganization.secondaryAddresses"/>
						<dsp:param name="elementName" value="address"/>
						<dsp:oparam name="output">
							<%@include file="/account/gadgets/addresses-cs-display.jspf" %>
						</dsp:oparam>
						<dsp:oparam name="empty">
							<p><dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="user-no-cs"/>
							</dsp:include></p>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</ul>
	</div>
</dsp:page>