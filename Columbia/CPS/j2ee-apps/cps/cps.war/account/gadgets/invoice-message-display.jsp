<dsp:page>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:getvalueof var="pending" bean="CPSSessionBean.invoicesPending"/>
	<c:if test="${not empty pending && pending != 0}">
		<div class="message">
			<p>You have <strong>${pending} invoices</strong> awaiting payment.</p>
		</div>
	</c:if>
</dsp:page>