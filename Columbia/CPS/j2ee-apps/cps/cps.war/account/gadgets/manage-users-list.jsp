<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:getvalueof var="searchTerm" param="searchTerm"/>
	<dsp:getvalueof var="sort" param="sort"/>
	<dsp:getvalueof var="page" param="page"/>
	<dsp:getvalueof var="role" param="role"/>
	<dsp:getvalueof var="order" param="order"/>
	<dsp:getvalueof var="company" param="company"/>
	
	<dsp:droplet name="/cps/droplet/SearchUsersDroplet">
		<dsp:param name="term" value="${searchTerm}"/>
		<dsp:param name="page" value="${page}"/>
		<dsp:param name="sort" value="${sort}"/>
		<dsp:param name="role" value="${role}"/>
		<dsp:param name="order" value="${order}"/>
		<dsp:param name="companyName" value="${company}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="emptyResult" param="empty"/>
			<dsp:getvalueof var="notifyUsersId" param="notifyUsersId"/>
			<dsp:getvalueof var="list" param="list"/>
			<dsp:getvalueof var="total" param="total"/>
			<dsp:getvalueof var="numPerPage" param="numPerPage"/>
			<dsp:getvalueof var="numPages" param="numPages"/>
			
			<input type="hidden" id="currentSort" value="${sort}"/>
			<input type="hidden" id="currentPage" value="${page}"/>
			<input type="hidden" id="currentOrder" value="${order}"/>
			
			
				<table class="table table-striped table-mobile">
					<dsp:include page="/account/gadgets/manage-users-sort.jsp">
						<dsp:param name="sort" value="${sort}"/>
						<dsp:param name="order" value="${order}"/>
					</dsp:include>
					<tbody>
						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" value="${list}"/>
							<dsp:param name="elementName" value="user"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="userId" param="user.id"/>
								<dsp:getvalueof var="spendingLimit" param="user.spendingLimit"/>
								<dsp:getvalueof var="billingAccount" param="user.billingAccount" />
								<tr>
									<td><span class="caption">First Name</span><dsp:valueof param="user.firstName"/></td>
									<td><span class="caption">Last Name</span><dsp:valueof param="user.lastName"/></td>
									<td><span class="caption">Email Address</span><span><dsp:valueof param="user.email"/></span></td>
									<td><span class="caption">Role</span><dsp:valueof param="user.role"/></td>
									<dsp:getvalueof var="role" param="user.role"/>
									<td><span class="caption">Frequency</span><dsp:valueof param="user.frequency"/></td>
									<td><span class="caption">Spending Limit</span>
										<c:choose>
											<c:when test="${not empty spendingLimit  && spendingLimit!='NA'}">
												$<fmt:formatNumber type="currency" pattern="###,###" value="${spendingLimit}"/>
											</c:when>
											<c:when test="${not empty spendingLimit  && spendingLimit=='NA'}">
												NA
											</c:when>
											<c:otherwise>
												None
											</c:otherwise>
										</c:choose>
									</td>
									<!-- This is removed in html -->
									<td><span class="caption">Status</span><dsp:valueof param="user.status"/></td>
									<td class="text-center"><a href="/account/gadgets/manage-edit-user.jsp?id=${userId}" class="table-icon-link"><span>Edit User</span></a></td>
									
									<%-- Order Notification SM-345 --%>
									
									
									<td class="text-center activeNotification">
									<%-- ${vsg_utils:containsTag(notifyUsersId,userId)} --%>									
								<c:choose>
								<c:when test="${vsg_utils:containsTag(notifyUsersId,userId) && billingAccount == 'true'}">
								    <input type="checkbox" id="${userId}" name="checkUserOrder" class="userNotification" value="${userId}" checked="checked">
								</c:when>
								<c:when test="${not empty spendingLimit  && spendingLimit=='NA'}">
									 NA
								</c:when>
								<c:when test="${billingAccount =='false'}">NA</c:when>
								<c:otherwise>
								<input type="checkbox" id="${userId}" name="checkUserOrder" class="userNotification" value="${userId}">
								</c:otherwise>
								</c:choose>
									
									<%-- Order Notification SM-345 --%>
									
								</tr>
							</dsp:oparam>
							<dsp:oparam name="empty">
								<div>
									<c:choose>
										<c:when test="${emptyResult == true}">
											<p>
												No results found in search.
											</p>
										</c:when>
										<c:otherwise>
											<p>
												No additional users. Click Add User to add a new user to this organization.
											</p>
										</c:otherwise>
									</c:choose>
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</tbody>
				</table>
			
			<div class="col-xs-12 text-center">
				<nav id="pagination">
					<dsp:include page="/account/gadgets/pagination.jsp">
						<dsp:param name="page" value="${page}"/>
						<dsp:param name="total" value="${total}"/>
						<dsp:param name="numPerPage" value="${numPerPage}"/>
						<dsp:param name="numPages" value="${numPages}"/>
					</dsp:include>
				</nav>
			</div>
			
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:form action="${originatingRequest.requestURI}" method="post" formid="notifyUserOrders" id="notifyUserOrders">
		<dsp:input type="hidden" bean="ProfileFormHandler.userId" value="${userIds}" id="notify-userId" />
		<dsp:input type="hidden" bean="ProfileFormHandler.userOrderNotification" value="true"/>
	</dsp:form>
</dsp:page>