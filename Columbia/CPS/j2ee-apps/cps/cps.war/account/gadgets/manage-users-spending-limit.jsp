<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:tomap var="parentOrg" bean="Profile.parentOrganization" />
	<section>
		<div class="well">
			<h4 class="text-nocase">Default Spending Limit and Frequency for
				All Users</h4>
			<p>
				You can set a default spending limit and frequency for <strong>ALL
					USERS</strong>. You can also choose to set an individual User's spending
				limit and frequency via the Edit User page.
			</p>
			<dsp:form iclass="splimit" method="post"
				action="/account/manage-users.jsp" id="spending-limit"
				formid="spending-limit">
				<div class="radio ">
					<label data-initialize="radio" id="myCustomRadioLabel"> <dsp:input
							bean="MultiUserAddFormHandler.orgLimit"
							checked="${parentOrg.orderPriceLimit == null}" id="noDefault"
							name="radioEx1" type="radio" value="nospending" /> No default
						spending limit for all Users
					</label>
				</div>
				<div class="radio">
					<label data-initialize="radio" id="myCustomRadioLabel2"> <dsp:input
							bean="MultiUserAddFormHandler.orgLimit"
							checked="${parentOrg.orderPriceLimit != null}" id="chooseDefault"
							name="radioEx1" type="radio" value="defaultspending" /> Default
						spending limit for all Users
					</label>
				</div>
				<div class="splimit-options row" style="display: block;">
					<div class="col-md-4 col-sm-6" id="input-section">
						<div class="input-group">
							<div class="input-group-addon" id="currency-sign">$</div>
							<dsp:input bean="MultiUserAddFormHandler.spendingLimit"
								id="spendInput" type="text" iclass="form-control" maxlength="5"
								value="${orgOrderPriceLimit}">
								<dsp:tagAttribute name="placeholder" value="" />
							</dsp:input>
						</div>
						<div class="radio">
							<label class="radio-custom" data-initialize="radio"
								id="myCustomRadioLabel20"> <dsp:input
									bean="MultiUserAddFormHandler.frequency"
									checked="${parentOrg.spFrequency == 'ORDER'}" name="radioEx2"
									type="radio" value="ORDER" /> Per Order
							</label>
						</div>
						<div class="radio">
							<label class="radio-custom" data-initialize="radio"
								id="myCustomRadioLabel21"> <dsp:input
									bean="MultiUserAddFormHandler.frequency"
									checked="${parentOrg.spFrequency == 'DAY'}" name="radioEx2"
									type="radio" value="DAY" /> Per Day
							</label>
						</div>
						<div class="radio">
							<label class="radio-custom" data-initialize="radio"
								id="myCustomRadioLabel22"> <dsp:input
									bean="MultiUserAddFormHandler.frequency"
									checked="${parentOrg.spFrequency == 'WEEK'}" name="radioEx2"
									type="radio" value="WEEK" /> Per Week
							</label>
						</div>
						<div class="radio">
							<label class="radio-custom" data-initialize="radio"
								id="myCustomRadioLabel23"> <dsp:input
									bean="MultiUserAddFormHandler.frequency"
									checked="${parentOrg.spFrequency == 'MONTH'}" name="radioEx2"
									type="radio" value="MONTH" /> Per Month
							</label>
						</div>
					</div>
				</div>
				<div class="text-right">
					<button data-event-click-id="eventUpdateSpendingLimit"  class="btn btn-default">Save</button>
					<dsp:input type="hidden"
						bean="MultiUserAddFormHandler.updateSpendingLimit" value="true" />
				</div>
			</dsp:form>
			<script type="text/javascript" nonce="${requestScope.nonce}">
				$(document)
						.ready(
								function() {
									var noDefault = document
											.getElementById("noDefault");
									if (noDefault.checked) {
										//hide input div
										document
												.getElementById("input-section").className = "hidden";
									}
									$("#noDefault")
											.click(
													function() {
														//when default clicked, hide input div
														document
																.getElementById("input-section").className = "hidden";

													});
									$("#chooseDefault")
											.click(
													function() {
														//when clicked, make input div visible
														document
																.getElementById("input-section").className = "col-md-4 col-sm-6";

													});
								});
				function updateSpendingLimit() {
					$('#spending-limit').submit();
				}
			</script>
		</div>
	</section>
</dsp:page>