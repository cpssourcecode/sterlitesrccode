<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<h4 class="text-nocase">Contact Information</h4>
	<div class="profile-contact collapse in" aria-expanded="true">
		<dsp:getvalueof var="email" bean="Profile.email" />
		<dsp:getvalueof var="phoneNumber" bean="Profile.homeAddress.phoneNumber" />
		<dsp:getvalueof var="alternatePhone" bean="Profile.homeAddress.alternatePhoneNumber" />
		<dsp:getvalueof var="city" bean="Profile.homeAddress.city" />
		<dsp:getvalueof var="address1" bean="Profile.homeAddress.address1" />
		<dsp:getvalueof var="address2" bean="Profile.homeAddress.address2" />
		<dsp:getvalueof var="city" bean="Profile.homeAddress.city" />
		<dsp:getvalueof var="state" bean="Profile.homeAddress.state" />
		<dsp:getvalueof var="zip" bean="Profile.homeAddress.postalCode" />
		<div class="row">
			<div class="col-sm-8">
				<ul class="list-unstyled">
					<li>
						<strong>Email:</strong>
						<dsp:valueof bean="Profile.email" />
					</li>
					<li>
						<strong>Mobile Phone:</strong>
						${phoneNumber}
					</li>
					<li>
						<strong>Direct Phone:</strong>
						${alternatePhone}
					</li>
					<li>
						<strong>Company Address:</strong>
						<c:if test="${not empty address1}">${address1},&nbsp;</c:if>
						<c:if test="${not empty address2}">${address2},&nbsp;</c:if>
						<c:if test="${not empty city}">${city},&nbsp;</c:if>
						<c:if test="${not empty state}">${state}&nbsp;</c:if>
						<c:if test="${not empty zip}">${zip}</c:if>
					</li>
				</ul>
			</div>
			<div class="col-sm-4 text-right ">
				<button id="contactInfoButton" class="btn btn-info" type="button" data-toggle="collapse" data-target=".profile-contact" aria-expanded="false" aria-controls="profile-contact">Edit</button>
			</div>
		</div>
	</div>
	<div class="profile-contact collapse" aria-expanded="false" style="height: 0px;">
		<dsp:form method="post" action="/account/profile-settings.jsp" id="edit-contact-info">
			<p>
				Editing your Contact Information
				<strong>DOES NOT</strong>
				change or affect Bill To or Ship To information or settings and
				<strong>DOES NOT</strong>
				change your login ID.
			</p>
			<dsp:getvalueof var="contactFormExceptions" vartype="java.lang.Object" bean="ProfileFormHandler.contactFormExceptions" />
			<dsp:getvalueof var="updateSuccessful" bean="ProfileFormHandler.contactUpdateSuccessful" />
			<c:if test="${updateSuccessful}">
				<script type="text/javascript" nonce="${requestScope.nonce}">
					document.getElementById("success-message").className = "alert alert-success";
					document.getElementById("success-message").innerHTML = "Contact information changed successfully";
				</script>
			</c:if>
			<c:if test="${not empty contactFormExceptions}">
				<script type="text/javascript" nonce="${requestScope.nonce}">
					jQuery(document).ready(function() {
						$("#contactInfoButton").click();
					});
				</script>
				<c:forEach var="contactFormException" items="${contactFormExceptions}">
					<dsp:param name="contactFormException" value="${contactFormException}" />
					<dsp:getvalueof var="errorCode" param="contactFormException.propertyPath" />
					<script type="text/javascript" nonce="${requestScope.nonce}">
						jQuery(document).ready(function() {
							loadErrors('${errorCode}');
						});
						function loadErrors(errorCode) {
							$("#" + errorCode).parent().addClass("has-error");
						}
					</script>
					<c:if test="${contactFormException.message != ''}">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">
									<span class="glyphicon glyphicon-remove-circle"></span>
								</span>
							</button>
							<dsp:valueof param="contactFormException.message" valueishtml="true" />
						</div>
					</c:if>
				</c:forEach>
			</c:if>
			<div class="row">
				<div class="col-sm-8">
					<div class="form-group">
						<dsp:input id="email" type="email" iclass="form-control" bean="ProfileFormHandler.value.email" value="${email}" name="email" maxlength="50">
							<dsp:tagAttribute name="placeholder" value="Email" />
						</dsp:input>
					</div>
					<div class="row row-narrow">
						<div class="form-group col-sm-6">
							<dsp:input id="primaryPhone" type="text" iclass="form-control" value="${phoneNumber}" bean="ProfileFormHandler.value.phoneNumber" name="phoneNumber" maxlength="20">
								<dsp:tagAttribute name="placeholder" value="Mobile Phone" />
							</dsp:input>
						</div>
						<div class="form-group col-sm-6">
							<dsp:input id="alternatePhone" type="text" iclass="form-control" bean="ProfileFormHandler.value.alternatePhoneNumber" value="${alternatePhone}" name="alternatePhoneNumber" maxlength="20">
								<dsp:tagAttribute name="placeholder" value="Direct Phone" />
							</dsp:input>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<dsp:input id="address1" type="text" iclass="form-control" value="${address1}" bean="ProfileFormHandler.value.address1" name="address1">
							<dsp:tagAttribute name="placeholder" value="Company Address 1" />
							<dsp:tagAttribute name="maxlength" value="100" />
						</dsp:input>
					</div>
					<div class="form-group">
						<dsp:input id="address2" type="text" iclass="form-control" value="${address2}" bean="ProfileFormHandler.value.address2" name="address2">
							<dsp:tagAttribute name="placeholder" value="Company Address 2" />
							<dsp:tagAttribute name="maxlength" value="100" />
						</dsp:input>
					</div>
					<div class="form-group">
						<dsp:input id="city" type="text" iclass="form-control" value="${city}" bean="ProfileFormHandler.value.city" name="city" maxlength="50">
							<dsp:tagAttribute name="placeholder" value="City" />
						</dsp:input>
					</div>
					<div class="row row-narrow">
						<div class="form-group col-sm-6">
							<!-- <span class="select-wrap"> -->
								<dsp:select bean="ProfileFormHandler.value.state" iclass="form-control" name="state" id="state">
									<dsp:option value="">State</dsp:option>
									<%@include file="/includes/states.jspf"%>
								</dsp:select>
							<!-- 	<span class="caret caret-select"></span>
							</span> -->
						</div>
						<div class="form-group col-sm-6">
							<dsp:input id="zip" type="number" iclass="form-control" value="${zip}" bean="ProfileFormHandler.value.postalCode"
								name="zip" maxlength="5" >
								<dsp:tagAttribute name="placeholder" value="ZIP" />
							</dsp:input>
						</div>
					</div>
				</div>
			</div>
			<div class="text-right ">
				<button data-event-click-id="eventSubmitProfileContactUpdate"  class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample0" aria-expanded="false" aria-controls="collapseExample0">Save</button>
				<button class="btn btn-info" type="button" data-toggle="collapse" data-target=".profile-contact" aria-expanded="false" aria-controls="profile-contact">Cancel</button>
			</div>
			<dsp:input type="hidden" value="true" bean="ProfileFormHandler.updateContactInfo" priority="-10" />
			<dsp:input type="hidden" bean="ProfileFormHandler.updateSuccessURL" value="/account/profile-settings.jsp" />
			<dsp:input type="hidden" bean="ProfileFormHandler.updateErrorURL" value="/account/profile-settings.jsp" />
		</dsp:form>
	</div>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		$("#primaryPhone").mask("(999) 999-9999");
		$("#alternatePhone").mask("(999) 999-9999");
		function submitProfileContactUpdate() {
			$("#edit-contact-info").submit();
		}
		
		var currentVal = 0;
		var maxLength = 0;
	    $(function() {
	    	currentVal = $("#zip").value; 
	    	maxLength = Number($("#zip").attr("maxlength"));
	      $("#zip").on("keyup", function (e) {
	        var $field = $(this),
	            val = this.value;
	        if (this.validity && this.validity.badInput || isNaN(val)) {
	            this.value = currentVal;
	            return;
	        } 
	        if (val.length > maxLength) {
				val = val.slice(0, maxLength);
				$field.val(val);
	        }
	        currentVal = val;
	      });
	    });
	</script>
</dsp:page>