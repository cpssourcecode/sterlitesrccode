<dsp:page>
	<dsp:getvalueof var="sort" param="sort" />
	<dsp:getvalueof var="order" param="order" />
	<thead>
		<tr>
			<th>
				<c:choose>
					<c:when test="${sort == 'firstName'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="firstName" data-sort-dir="-" id="sortFirst">First Name
									<i class="fa fa-sort-up"></i>
								</a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="firstName" data-sort-dir="+"  id="sortFirst">First Name
									<i class="fa fa-sort-down"></i>
								</a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="firstName" data-sort-dir="+" id="sortFirst">First Name
							<i class="fa fa-sort"></i>
						</a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>
				<c:choose>
					<c:when test="${sort == 'lastName'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="lastName" data-sort-dir="-" id="sortLast">Last Name
									<i class="fa fa-sort-up"></i>
								</a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="lastName" data-sort-dir="+" id="sortLast">Last Name
									<i class="fa fa-sort-down"></i>
								</a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="lastName" data-sort-dir="+"  id="sortLast">Last Name
							<i class="fa fa-sort"></i>
						</a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>
				Email Address
			</th>
			<th>
				<c:choose>
					<c:when test="${sort == 'role'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="role" data-sort-dir="-" id="sortRole">Role
									<i class="fa fa-sort-up"></i>
								</a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="role" data-sort-dir="+" id="sortRole">Role
									<i class="fa fa-sort-down"></i>
								</a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="role" data-sort-dir="+" id="sortRole">Role
							<i class="fa fa-sort"></i>
						</a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>
				<c:choose>
					<c:when test="${sort == 'frequency'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="frequency" data-sort-dir="-" id="sortFrequency">Frequency
									<i class="fa fa-sort-up"></i>
								</a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="frequency" data-sort-dir="+" id="sortFrequency">Frequency
									<i class="fa fa-sort-down"></i>
								</a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="frequency" data-sort-dir="+" id="sortFrequency">Frequency
							<i class="fa fa-sort"></i>
						</a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>
				<c:choose>
					<c:when test="${sort == 'limit'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="limit" data-sort-dir="-" id="sortLimit">Spend Limit
									<i class="fa fa-sort-up"></i>
								</a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="limit" data-sort-dir="-" id="sortLimit">Spend Limit
									<i class="fa fa-sort-down"></i>
								</a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="limit" data-sort-dir="-" id="sortLimit">Spend Limit
							<i class="fa fa-sort"></i>
						</a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>
				<c:choose>
					<c:when test="${sort == 'status'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="status" data-sort-dir="-" id="sortStatus">Status
									<i class="fa fa-sort-up"></i>
								</a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="status" data-sort-dir="+" id="sortStatus">Status
									<i class="fa fa-sort-down"></i>
								</a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="status" data-sort-dir="+" id="sortStatus">Status
							<i class="fa fa-sort"></i>
						</a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>&nbsp;</th>
			
			<%-- Order Notification SM-345 - header checkbox --%>
			<th class="activeNotification"><input type="checkbox" id="selectAllUsersNotification" name="checkUserOrder"> All <i class="glyphicon glyphicon-info-sign" title='<cp:repositoryMessage key="manageUserAllInfo" />'></i></th>
			<%-- Order Notification SM-345 --%>
			
		</tr>
	</thead>
</dsp:page>