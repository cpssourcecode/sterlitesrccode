<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

    <dsp:importbean bean="/cps/droplet/SavedCartsDroplet"/>
    <dsp:importbean bean="/cps/droplet/UserLookupDroplet"/>
    <dsp:importbean bean="/cps/util/CPSSessionBean"/>

    <dsp:getvalueof var="sort" param="sort"/>
    <dsp:getvalueof var="accend" param="accend"/>
    <dsp:getvalueof var="page" param="page"/>
    <c:if test="${empty page}">
        <dsp:getvalueof var="page" value="${1}"/>
    </c:if>
    <input type="hidden" value="${page}" id="page"/>

    <dsp:getvalueof var="order" value="-"/>
    <c:if test="${accend eq true}">
        <dsp:getvalueof var="order" value="+"/>
    </c:if>

    <table class="table table-striped table-mobile">
        <thead>
        <tr>
            <th data-event-click-id="eventSortOrderSavedDate"  id="sort-date"><a href="#">Saved Date <i class="fa fa-sort"></i></a>
            </th>
            <th data-event-click-id="eventSortAddress"  id="sort-address"><a href="#">Ship To Address <i class="fa fa-sort"></i></a>
            </th>
            <th>&nbsp;</th>
        </tr>
        </thead>

        <dsp:droplet name="SavedCartsDroplet">
            <dsp:param name="profile" bean="Profile" />
            <dsp:oparam name="output">
                <dsp:getvalueof var="orders" param="orders"/>

                <dsp:getvalueof var="max" value="${page*10}"/>
                <dsp:getvalueof var="min" value="${max-10}"/>

                <input type="hidden" id="savedCartsOrdersCount" value="${fn:length(orders)}"/>

                <tbody>
                    <dsp:droplet name="ForEach">
                        <dsp:param name="array" param="orders"/>
                        <dsp:param name="sortProperties" value="${order}${sort}"/>
                        <dsp:param name="elementName" value="order"/>
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="count" param="count"/>
                            <dsp:getvalueof var="orderId" param="order.id"/>
                            <dsp:getvalueof var="index" param="index"/>

                            <input type="hidden" id="savedCartsOrderId${index}" value="${orderId}"/>
                            <c:if test="${count > min && count <= max}">
                                <tr>
                                    <td>
                                        <span class="caption">Saved Date</span>
                                            <%--<dsp:valueof value="${orderId}"/> &nbsp;--%>
                                        <dsp:getvalueof var="savedDate" param="order.savedDate"/>
                                        <fmt:formatDate pattern="MM/dd/yyyy" value="${savedDate}"/>
                                    </td>
                                    <td>
                                        <dsp:getvalueof var="address" param="order.address"/>
                                        <span class="caption">Ship To Address</span>
                                        <address>
                                            <c:if test="${!empty address}">
                                                <c:if test="${!empty address.address1}">${address.address1}<br/></c:if>
                                                <c:if test="${!empty address.address2}">${address.address2}<br/></c:if>
                                                <c:if test="${!empty address.address3}">${address.address3}<br/></c:if>
                                                <c:if test="${not empty address.city || not empty address.state || not empty address.postalCode}">
                                                    ${address.city},&nbsp;${address.state}&nbsp;${address.postalCode}<br/>
                                                </c:if>
                                            </c:if>
                                        </address>
                                    </td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true"
                                               aria-expanded="false" href="#" class="btn btn-link xs-block">
                                                Actions<i class="fa fa-angle-down ml5"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">

                                                <li>
                                                    <a href="javascript:void(0);"
                                                       data-event-click-id="eventShowMoveToCartModalPopup" data-order-id="${orderId}" >
                                                        Move to Active Cart
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);"
                                                       data-event-click-id="eventShowViewCartModalPopup" data-order-id="${orderId}" >
                                                        View
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);"
                                                       data-event-click-id="eventShowDeleteCartModalPopup" data-order-id="${orderId}" >
                                                        Delete
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <dsp:include
                                                page="${originatingRequest.contextPath}/global/modals/modal-move-to-cart.jsp?orderId=${orderId}"/>
                                        <dsp:include
                                                page="${originatingRequest.contextPath}/global/modals/modal-delete-cart.jsp?orderId=${orderId}"/>

                                    </td>
                                </tr>
                            </c:if>
                        </dsp:oparam>
                        <dsp:oparam name="outputEnd">
                            <input type="hidden" value="${count}" id="total_count"/>
                        </dsp:oparam>
                    </dsp:droplet>
                </tbody>

            </dsp:oparam>
            <dsp:oparam name="empty">
                <input type="hidden" value="0" id="total_count"/>
            </dsp:oparam>
        </dsp:droplet>

    </table>

</dsp:page>
