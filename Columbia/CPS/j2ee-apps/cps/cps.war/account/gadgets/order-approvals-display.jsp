<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

	<dsp:importbean bean="/cps/droplet/OrderApprovalsDroplet"/>
	<dsp:importbean bean="/cps/droplet/UserLookupDroplet"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>

	<dsp:getvalueof var="sort" param="sort"/>
	<dsp:getvalueof var="accend" param="accend"/>
	<dsp:getvalueof var="page" param="page"/>
	<c:if test="${empty page}">
		<dsp:getvalueof var="page" value="${1}"/>
	</c:if>
	<input type="hidden" value="${page}" id="page"/>

	<dsp:getvalueof var="order" value="-"/>
	<c:if test="${accend eq true}">
		<dsp:getvalueof var="order" value="+"/>
	</c:if>
	
	<table class="table table-striped table-mobile table-check orders">
		<thead>
			<tr>
				<th class="check">
					<strong>
						<input id="selectAllBoxesCheck" type="checkbox" data-event-click-id="eventSelectAll" >
						<span class="caption visible-xs-inline ml5">All</span>
					</strong>
				</th>
				<th data-event-click-id="eventSortProfileName" data-approvals="${true}"  id="sort-name">
					<strong>Requester</strong> <i class="fa fa-sort"></i>
				</th>
				<th data-event-click-id="eventSortOrderDate" data-approvals="${true}"  id="sort-date">
					<strong>Request Date</strong> <i class="fa fa-sort"></i>
				</th>
				<th>
					<strong>Spending Limit</strong> 
				</th>
				<th>
					<strong>Spend Request</strong> 
				</th>
				<th></th>
			</tr>
		</thead>
		<tbody>

		<dsp:droplet name="OrderApprovalsDroplet">
			<dsp:param name="profile" bean="Profile"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="orders" param="orders"/>

				<dsp:getvalueof var="max" value="${page*5}"/>
				<dsp:getvalueof var="min" value="${max-5}"/>

				<tbody>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="orders"/>
						<dsp:param name="sortProperties" value="${order}${sort}"/>
						<dsp:param name="elementName" value="order"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<dsp:getvalueof var="orderId" param="order.id"/>
							<c:if test="${count > min && count <= max}">
								<tr>
									<td class="check">
										<input type="checkbox" value="" name="check" id="${orderId}">
										<span class="checkbox-label"></span>
										<span class="caption visible-xs-inline ml5"><dsp:valueof param="order.profileName"/></span>
									</td>
									<td class="hidden-xs">
										<span class="caption">Requestor</span>
										<span class="hidden-mobile"><dsp:valueof param="order.profileName"/></span>
									</td>
									<td>
										<span class="caption">Request Date</span>
										<dsp:getvalueof var="submittedDate" param="order.submittedDate"/>
										<fmt:formatDate pattern="MM/dd/yyyy" value="${submittedDate}"/>
									</td>
									<td>
										<span class="caption">Spending Limit</span>
										<dsp:valueof param="order.spendingLimit" converter="currencyConversion"/>
									</td>
									<td>
										<span class="caption">Spend Request</span>
										<dsp:valueof param="order.orderTotal" converter="currencyConversion"/>
									</td>
									<td class="text-center">
										<div class="dropdown">
											<button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-link xs-block">Actions<i class="fa fa-angle-down ml5"></i></button>
											<ul class="dropdown-menu dropdown-table-actions">
												<li>
													<a href="javascript:void(0);" data-event-click-id="eventShowViewOrderModalPopup" data-order-id="${orderId}" data-page="account" >
														Review
													</a>
												</li>
												<%-- <li>
													<a href="javascript:void(0);" data-event-click-id="eventShowApproveOrderModalPopup" data-order-id="${orderId}" >
														Approve
													</a>
												</li> --%>
												<li>
													<a href="javascript:void(0);" data-event-click-id="eventShowRejectOrderModalPopup" data-order-id="${orderId}" >
														Reject
													</a>
												</li>
											</ul>
										</div>										
									</td>
								</tr>
								<%-- <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-view-order.jsp?orderId=${orderId}"/> --%>
								<div id="view_order_div"></div>
								<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-approve-order.jsp?orderId=${orderId}"/>
								<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-reject-order.jsp?orderId=${orderId}"/>
							</c:if>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
							<input type="hidden" value="${count}" id="total_count"/>
						</dsp:oparam>
				    </dsp:droplet>
				</tbody>

			</dsp:oparam>
			<dsp:oparam name="empty">
				<input type="hidden" value="0" id="total_count"/>
			</dsp:oparam>
		</dsp:droplet>
	</table>
</dsp:page>
