<dsp:page>

	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/cps/droplet/MTRListDisplayNameDroplet"/>
	<div class="form-group resultsMTRSection">
		<div class="scrollable scrollable-alt">
			<div class="content scrollbar-inner">
				<div class="table-responsive">
					
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="check text-left" colspan="5">
									<label>
										<input id="selectAllBoxesCheck" class="mr5" type="checkbox" data-event-click-id="eventSelectAll" > All
									</label>
								</th>
							</tr>
						</thead>
						<tbody>
							<dsp:droplet name="/atg/dynamo/droplet/ForEach">
								<dsp:param name="array" bean="CPSSessionBean.materialReports" />
								<dsp:param name="elementName" value="mtr" />
								<dsp:oparam name="outputStart">
									<dsp:getvalueof var="heatFields"
										bean="CPSSessionBean.foundHeatNums" />
									<input type="hidden" name="heatNums" id="heatNums"
										value="${vsg_utils:join(heatFields, ',')}" />
								</dsp:oparam>
								<dsp:oparam name="output">
									<dsp:getvalueof var="count" param="count" />
									<dsp:getvalueof var="mtrId" param="mtr.id" />
									<dsp:getvalueof var="mtrUrl" param="mtr.url" />
									<dsp:getvalueof var="heatNum" param="mtr.heatNum" />
									<dsp:getvalueof var="fields" param="mtr.fields" />
									<tr>
										<td class="check"><input type="checkbox" value=""
											name="check" id="${mtrId}" /></td>
										<td><a href="${mtrUrl}" class="table-icon-link"
											target="_blank"> <i class="icon icon-paper-page"></i> <dsp:valueof
													value="${heatNum}" />
										</a></td>
										<c:set var="itemNumber" value="${fields.toArray()[3]}" />
										<dsp:droplet name="/cps/droplet/MTRListDisplayNameDroplet">
											<dsp:param name="itemNumber" value="${itemNumber}" />
											<dsp:oparam name="output">
												<td><dsp:valueof param="displayName"/></td>
												<td><dsp:valueof param="description" /></td>
											</dsp:oparam>
											<dsp:oparam name="empty">
											<td>NA</td>
											<td>NA</td>
											</dsp:oparam>
										</dsp:droplet>
										<td><a href="${mtrUrl}" class="table-icon-link mtr_url"
											target="_blank"> <i class="fa fa-eye mtr_url"
												aria-hidden="true">Preview</i>
										</a></td>
									</tr>

								</dsp:oparam>
							</dsp:droplet>



						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
