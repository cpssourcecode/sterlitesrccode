<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="parentOrgId" bean="Profile.parentOrganization.parentOrganization" />
    <dsp:getvalueof var="customerNumber" param="customerNumber"/>
    <dsp:droplet name="/cps/droplet/CPSStatementsDroplet">
        <dsp:param name="customerId" value="${customerNumber}"/>
        <%--<dsp:param name="customerId" value="100109" />--%>
        <dsp:oparam name="output">

            <table class="table table-striped table-mobile">
                <thead>
                <tr>
                    <th>Month</th>
                    <th>Year</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                    <dsp:param name="array" param="statements"/>
                    <dsp:param name="elementName" value="statement"/>
                    <dsp:oparam name="output">
                        <dsp:getvalueof var="statementId" param="statement.id"/>
                        <dsp:getvalueof var="statementDate" param="statement.modifiedDate"/>
                        <dsp:getvalueof var="idEncrypted" param="statement.idEncrypted"/>
                        <dsp:getvalueof var="url" param="statement.url"/>
                        <tr>
                            <td><span class="caption">Month</span><fmt:formatDate pattern="MMMM" value="${statementDate}"/></td>
                            <td><span class="caption">Year</span><fmt:formatDate pattern="yyyy" value="${statementDate}"/></td>
                            <td class="text-center"><a href="${url}" target="_blank"><i class="icon icon-paper-page" aria-hidden="true"></i>
                                View Statement</a></td>
                        </tr>
                    </dsp:oparam>
                </dsp:droplet>
                </tbody>
            </table>
        </dsp:oparam>
        <c:if test="${parentOrgId == null}">
			<dsp:oparam name="empty">
						<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
							<dsp:param name="messageKey" value="err_statement_not_found" />
							<dsp:oparam name="output" >
							<dsp:getvalueof param="message" var="errStatement"></dsp:getvalueof>
							<input type="hidden" id="errStatement" value="${errStatement}"/>
							</dsp:oparam>
						</dsp:droplet>
				
			</dsp:oparam>
		</c:if>
        <dsp:oparam name="error">
            <div class="alert alert-danger">
                <dsp:valueof param="errorMessage"/>
            </div>
        </dsp:oparam>
    </dsp:droplet>

</dsp:page>
