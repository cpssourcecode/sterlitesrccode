<dsp:page>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/util/CPSSessionBean"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

    <table class="table table-striped table-mobile">
        <thead>
        <tr>
            <th id="sort-id">Order #</th>
            <th id="sort-date">Order Date</th>
            <th id="sort-po">PO #</th>
            <th id="sort-shipment">Shipment #</th>
            <th colspan="2">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <dsp:droplet name="ForEach">
            <dsp:param name="array" bean="CPSSessionBean.packingSlipRecords"/>
            <dsp:param name="elementName" value="slipRecord"/>
            <dsp:oparam name="output">
                <dsp:getvalueof var="orderNumber" param="slipRecord.orderNumber"/>
                <dsp:getvalueof var="date" param="slipRecord.packingSlips[0].date"/>
                <dsp:getvalueof var="po" param="slipRecord.packingSlips[0].customerPONumber"/>
                <dsp:getvalueof var="packingSlipsSize" param="slipRecord.size"/>

                <tr>
                    <td>
                        <span class="caption">Order</span><a href="#"
                                                             class="table-icon-link"><span>${orderNumber}</span></a>
                    </td>
                    <td>
                        <span class="caption">Order Date</span>
                        <fmt:formatDate pattern="MM/dd/yyyy" value="${date}"/>
                    </td>
                    <td>
                        <span class="caption">PO#</span>
                            ${po}
                    </td>
                    <c:choose>
                        <c:when test="${packingSlipsSize > 1}">
                            <td colspan="2">
                                <div class="caption">Shipment #</div>
                                    <%--<a href="#collapseMultiShipments${orderNumber}" role="button" data-toggle="collapse" data-target=".shipping-details" aria-expanded="false" aria-controls="shipping-details">--%>
                                <a href="#collapseMultiShipments${orderNumber}" role="button" data-toggle="collapse"
                                   aria-expanded="false" aria-controls="collapseMultiShipments${orderNumber}">
                                    Multiple <i class="fa fa-angle-down"></i>
                                </a>
                                <div id="collapseMultiShipments${orderNumber}" class="collapse shipping-details">
                                    <table class="table">
                                        <tbody>
                                        <dsp:droplet name="ForEach">
                                            <dsp:param name="array" param="slipRecord.packingSlips"/>
                                            <dsp:param name="elementName" value="slip"/>
                                            <dsp:oparam name="output">
                                                <dsp:getvalueof var="shipmentNumber" param="slip.shipmentNumber"/>
                                                <dsp:getvalueof var="link" param="slip.url"/>
                                                <tr>
                                                    <td class="shipment-value">
                                                        <c:choose>
                                                            <c:when test="${shipmentNumber == 0}">
                                                                Consolidated
                                                            </c:when>
                                                            <c:otherwise>
                                                                ${shipmentNumber}
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </td>
                                                    <td>
                                                        <a href="${link}" target="_blank" class="text-nowrap"><i
                                                                class="icon icon-paper-page"></i> Details</a>
                                                    </td>
                                                </tr>
                                            </dsp:oparam>
                                        </dsp:droplet>
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </c:when>
                        <c:otherwise>
                            <dsp:getvalueof var="shipmentNumber" param="slipRecord.packingSlips[0].shipmentNumber"/>
                            <dsp:getvalueof var="link" param="slipRecord.packingSlips[0].url"/>
                            <td class="shipment-value"><span class="caption">
									Shipment #</span>${shipmentNumber}
                            </td>
                            <td>
                                <a href="${link}" target="_blank" class="text-nowrap"><i
                                        class="icon icon-paper-page"></i> Details</a>
                            </td>
                        </c:otherwise>
                    </c:choose>
                </tr>
            </dsp:oparam>
            <dsp:oparam name="outputEnd">
                <dsp:getvalueof var="packingTotal" bean="CPSSessionBean.packingSlipsTotal"/>
                <input type="hidden" value="${packingTotal}" id="total_count"/>
                <input type="hidden" value="" id="page"/>
            </dsp:oparam>
            <dsp:oparam name="empty">
                <input type="hidden" value="0" id="total_count"/>
                <input type="hidden" value="1" id="page"/>
                <dsp:include page="/includes/gadgets/info-message.jsp">
                    <dsp:param name="key" value="err_packingslip_not_found"/>
                </dsp:include>
            </dsp:oparam>
        </dsp:droplet>
        </tbody>

    </table>

</dsp:page>