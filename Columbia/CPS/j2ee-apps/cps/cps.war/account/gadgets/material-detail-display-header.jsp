<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/droplet/ReorderlistLookupDroplet"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/util/MaterialListDownloadHandler"/>
	<dsp:getvalueof var="importModalError" param="importModalError"/>
	<dsp:importbean bean="/cps/userprofiling/MaterialListHandler"/>
	<dsp:getvalueof var="ca" param="ca"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>

	<dsp:getvalueof var="invalidMaterialItemsList" bean="CPSSessionBean.invalidMaterialItemsList"/>
	<dsp:param name="materialDuplicates" bean="CPSSessionBean.materialDuplicates"/>
	<dsp:param name="hasMaterialDuplicates" bean="CPSSessionBean.hasMaterialDuplicates"/>
	<dsp:getvalueof var="hasMaterialDuplicates" param="hasMaterialDuplicates"/>
	<dsp:getvalueof var="hasInvalidItems" value="false"/>
	
	<dsp:getvalueof var="giftlistFormExceptions" vartype="java.lang.Object" bean="GiftlistFormHandler.formExceptions"/>
	<dsp:getvalueof var="fileUploadFormExceptions" vartype="java.lang.Object" bean="/cps/util/FileUploadHandler.formExceptions"/>
	
	<c:if test="${fn:length(invalidMaterialItemsList) > 0 || not empty giftlistFormExceptions || not empty fileUploadFormExceptions}">
		<dsp:getvalueof var="hasInvalidItems" value="true"/>
	</c:if>
	
	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-items-not-added-to-ml.jsp">
		<dsp:param name="invalidMaterialItemsList" value="${invalidMaterialItemsList}"/>
		<dsp:param name="materialDuplicates" param="materialDuplicates"/>
		<dsp:param name="hasMaterialDuplicates" param="hasMaterialDuplicates"/>
		<dsp:param name="giftlistId" param="giftlistId"/>
		<dsp:param name="ca" param="ca"/>
	</dsp:include>
		
	<div class="col-sm-8">
		<ol class="breadcrumb">
			<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
			<li><a href="${originatingRequest.contextPath}/account/material-lists.jsp">My Material Lists</a></li>
			<li class="active">Material List Detail</li>
		</ol>
	</div>
	
	<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
		<dsp:param name="email" value="false"/>
		<dsp:param name="print" value="true"/>
	</dsp:include>
	
	<div class="col-xs-12">
		<h1><fmt:message key="account.material.detail"/></h1>
	</div>
	
<%--
	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-material-list-qty-error.jsp"/>
--%>
</dsp:page>