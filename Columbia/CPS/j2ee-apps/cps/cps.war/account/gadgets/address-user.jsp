<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

	<dsp:droplet name="/atg/dynamo/droplet/IsNull">
		<dsp:param name="value" bean="Profile.parentOrganization"/>
		<dsp:oparam name="true">
			<dsp:getvalueof var="companyName" bean="Profile.derivedBillingAddress.companyName"/>
			<dsp:getvalueof var="address1" bean="Profile.derivedBillingAddress.address1"/>
			<dsp:getvalueof var="address2" bean="Profile.derivedBillingAddress.address2"/>
			<dsp:getvalueof var="address3" bean="Profile.derivedBillingAddress.address3"/>
			<dsp:getvalueof var="city" bean="Profile.derivedBillingAddress.city"/>
			<dsp:getvalueof var="state" bean="Profile.derivedBillingAddress.state"/>
			<dsp:getvalueof var="postalCode" bean="Profile.derivedBillingAddress.postalCode"/>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="companyName" bean="Profile.parentOrganization.billingAddress.companyName"/>
			<dsp:getvalueof var="address1" bean="Profile.parentOrganization.billingAddress.address1"/>
			<dsp:getvalueof var="address2" bean="Profile.parentOrganization.billingAddress.address2"/>
			<dsp:getvalueof var="address3" bean="Profile.parentOrganization.billingAddress.address3"/>
			<dsp:getvalueof var="city" bean="Profile.parentOrganization.billingAddress.city"/>
			<dsp:getvalueof var="state" bean="Profile.parentOrganization.billingAddress.state"/>
			<dsp:getvalueof var="postalCode" bean="Profile.parentOrganization.billingAddress.postalCode"/>
		</dsp:oparam>
	</dsp:droplet>
	<%@include file="/account/gadgets/addresses-cb-display.jspf" %>

	<div class="col-lg-12 block-bordered">
		<div class="row">
			<div class="col-lg-12">
				<h3>Shipping Addresses (CS)</h3>
			</div>
			<div class="col-lg-12">
				<table class="table table-hover">
					<%--<caption>Optional table caption.</caption>--%>
					<thead>
					<tr>
						<th>Address Line</th>
						<th>Description</th>
						<th>Address</th>
					</tr>
					</thead>
					<tbody>
						<dsp:droplet name="ForEach">
							<dsp:param name="array" bean="Profile.associatedCS"/>
							<dsp:param name="elementName" value="address"/>
							<dsp:oparam name="output">
								<%@include file="/account/gadgets/addresses-cs-display.jspf" %>
							</dsp:oparam>
							<dsp:oparam name="empty">
								<dsp:droplet name="ForEach">
									<dsp:param name="array" bean="Profile.parentOrganization.secondaryAddresses"/>
									<dsp:param name="elementName" value="address"/>
									<dsp:oparam name="output">
										<%@include file="/account/gadgets/addresses-cs-display.jspf" %>
									</dsp:oparam>
									<dsp:oparam name="empty">
										<p><dsp:include page="/includes/gadgets/info-message.jsp">
											<dsp:param name="key" value="user-no-cs"/>
										</dsp:include></p>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</dsp:page>