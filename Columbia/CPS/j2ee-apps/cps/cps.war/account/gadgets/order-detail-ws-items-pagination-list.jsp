<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/cps/droplet/CPSOrderWSItemsPaginationDroplet"/>
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>

	<dsp:getvalueof var="order" param="order"/>
	<dsp:getvalueof var="pageNumber" param="pageNumber"/>
	<dsp:getvalueof var="pageNumber" value="${empty pageNumber || pageNumber == 0 ? 1 : pageNumber}"/>


    <table class="table table-striped table-mobile table-check">
        <thead>
        <tr>
            <th class="text-center">Select All</th>
            <th id="check-all-items-checkbox" class="check">
                <input type="checkbox" title="" />
                <span class="visible-xs-inline ml5">All</span>
            </th>
            <th>Item Name</th>
            <th>Item #</th>
            <th class="text-center">Quantity</th>
            <th>Price</th>
            <th>Total</th>
            <th class="text-center">Status</th>
        </tr>
        </thead>
        <tbody>

        <dsp:droplet name="CPSOrderWSItemsPaginationDroplet">
            <dsp:param name="orderId" param="orderId"/>
            <dsp:param name="pageNumber" param="pageNumber"/>
            <dsp:oparam name="output">

                <dsp:getvalueof var="commerceItemsCount" param="commerceItemsCount"/>
                <dsp:getvalueof var="itemsPerPage" param="itemsPerPage"/>
                <dsp:getvalueof var="pageCount" param="pageCount"/>
                <dsp:droplet name="/atg/dynamo/droplet/ForEach">

                    <dsp:param name="array" param="items"/>
                    <dsp:param name="elementName" value="item"/>
                    <dsp:oparam name="output">

                        <dsp:getvalueof var="prodId" param="item.identifierShortTerm"/>

                        <dsp:droplet name="ProductLookup">
                            <dsp:param name="id" value="${prodId}"/>
                            <dsp:param name="filterBySite" value="false"/>
                            <dsp:param name="filterByCatalog" value="false"/>
                            <dsp:param name="elementName" value="product"/>
                            <dsp:oparam name="output">
                                <tr>
                                    <dsp:tomap var="productMap" param="product"/>
                                    <dsp:tomap var="commerceItem" param="item"/>

                                    <dsp:getvalueof var="skuId" value="${commerceItem.identifierShortTerm}"/>

                                    <dsp:getvalueof var="parentCategory" param="product.parentCategory"/>
                                    <c:if test="${parentCategory == null}">
                                        <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                                            <dsp:param name="array" param="product.parentCategories" />
                                            <dsp:param name="elementName" value="category"/>
                                            <dsp:oparam name="output">
                                                <dsp:getvalueof var="index" param="index"/>
                                                <c:if test="${index eq 0}">
                                                    <dsp:getvalueof var="eCommerceDisplay" param="category.eCommerceDisplay"/>
                                                </c:if>
                                            </dsp:oparam>
                                        </dsp:droplet>
                                    </c:if>
                                    <c:if test="${parentCategory != null}">
                                        <dsp:getvalueof var="eCommerceDisplay" param="product.parentCategory.eCommerceDisplay"/>
                                    </c:if>
                                    <dsp:getvalueof var="imageUrl" vartype="java.lang.String"
                                                    value="${empty productMap.thumbnail_url ? '/assets/images/plp-placeholder.png' : productMap.thumbnail_url}"/>
                                    <%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String"
                                                    value="${originatingRequest.contextPath}/catalog/pdp.jsp?prodId=${skuId}"/> --%>
                                    <dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
										<dsp:param name="prodId" value="${prodId}"/>
										<dsp:oparam name="output">
											<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
										</dsp:oparam>
									</dsp:droplet>

                                    <dsp:getvalueof var="productDisabled"
                                                    value="${productMap.stockingType eq 'O' || productMap.stockingType eq 'U' || productMap.stockingType eq 'X' || productMap.stockingType eq 'K'
                                                    || commerceItem.disabled || not eCommerceDisplay}"/>
                                        <%--<div class="content ${productDisabled ? ' disabled' : ''}">--%>
                                    <td class="text-center">
                                        <span class="item-thumb"><cp:readerimg src="${imageUrl}" /></span>
                                    </td>
                                    <td class="check">
                                            <%--<label class="checkbox-custom hidden-print" data-initialize="checkbox">--%>
                                        <label>
                                            <c:choose>
                                                <c:when test="${productDisabled}">
                                                    <input type="checkbox" disabled/>
                                                </c:when>
                                                <c:otherwise>
                                                    <input type="checkbox" value="${skuId}" name="itemsInCart" id="${skuId}"/>
                                                    <input type="hidden" value="${commerceItem.transactionQuantity}" name="${skuId}" id="${skuId}_qty">
                                                </c:otherwise>
                                            </c:choose>
                                            <span class="visible-xs-inline ml5">
                                                 <a href="${productUrl}">
                                                      <%--<dsp:valueof value="${productMap.displayName}" converter="valueishtml"/>--%>
                                                    <dsp:valueof value="${productMap.description}" converter="valueishtml"/>,
                                                    <%-- <dsp:valueof value="${productMap.descriptionLine2}" converter="valueishtml"/> --%>
                                                </a>
                                            </span>
                                        </label>

                                            <%--<span class="checkbox-label">
                                                <c:if test="${productDisabled}">
                                                    <button class="question-tooltip" role="button" data-trigger="hover" data-toggle="popover"
                                                            data-trigger="focus" data-placement="right"
                                                            data-content="This item is not available for online order.">
                                                        <span class="glyphicon glyphicon-question-sign"></span>
                                                    </button>
                                                </c:if>
                                            </span>--%>
                                            <%--</label>--%>
                                    </td>
                                    <td class="hidden-xs">
                                        <a href="${productUrl}">
                                                <%--<dsp:valueof value="${productMap.displayName}" converter="valueishtml"/>--%>
                                            <dsp:valueof value="${productMap.description}" converter="valueishtml"/>
                                                    <%-- <c:if test="${!empty productMap.descriptionLine2 && productMap.descriptionLine2 != ''}">, </c:if>
                                            <dsp:valueof value="${productMap.descriptionLine2}" converter="valueishtml"/> --%>
                                        </a>
                                    </td>
                                    <td><span class="caption">Item #</span><dsp:valueof value="${productMap.displayName}" converter="valueishtml"/></td>
                                    <td class="text-center text-left-xs"><span class="caption">Quantity</span><span class="item-qty">${commerceItem.transactionQuantity}</span></td>
                                    <td>
                                        <span class="caption">Price</span><dsp:valueof value="${commerceItem.amountPricePerUnit}" converter="currencyConversion"/>
                                    </td>
                                    <td>
                                        <span class="caption">Total</span><dsp:valueof value="${commerceItem.amountExtendedPrice}" converter="currencyConversion"/>
                                    </td>
                                    <td class="text-center">
                                        <div class="caption">Status</div>

                                        <c:choose>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Received')}">
                                                <dsp:getvalueof var="labelClass" value="label-default"/>
                                                <dsp:getvalueof var="status" value="Received"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Processing')}">
                                                <dsp:getvalueof var="labelClass" value="label-warning"/>
                                                <dsp:getvalueof var="status" value="Processing"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Shipped')}">
                                                <dsp:getvalueof var="labelClass" value="label-primary"/>
                                                <dsp:getvalueof var="status" value="Shipped"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Delivered')}">
                                                <dsp:getvalueof var="labelClass" value="label-success"/>
                                                <dsp:getvalueof var="status" value="Delivered"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Invoiced')}">
                                                <dsp:getvalueof var="labelClass" value="label-info"/>
                                                <dsp:getvalueof var="status" value="Invoiced"/>
                                            </c:when>
                                            <c:otherwise>
                                                <dsp:getvalueof var="labelClass" value="label-success"/>
                                                <dsp:getvalueof var="status" value="${commerceItem.lineStatusDescription}"/>
                                            </c:otherwise>
                                        </c:choose>

                                        <span class="label ${labelClass}">${status}</span>
                                    </td>
                                </tr>
                            </dsp:oparam>
                            <dsp:oparam name="empty">
                                <tr>
                                    <dsp:getvalueof var="skuId" value="${commerceItem.identifierShortTerm}"/>
                                    <dsp:getvalueof var="productDisabled" value="true"/>
                                    <dsp:tomap var="commerceItem" param="item"/>
                                    <dsp:getvalueof var="imageUrl" vartype="java.lang.String" value="/assets/images/plp-placeholder.png"/>
                                    <td class="text-center">
                                        <span class="item-thumb"><cp:readerimg src="${imageUrl}" /></span>
                                    </td>
                                    <td class="check">
                                            <%--<label class="checkbox-custom hidden-print" data-initialize="checkbox">--%>
                                        <label>
                                            <c:choose>
                                                <c:when test="${productDisabled}">
                                                    <input type="checkbox" disabled/>
                                                </c:when>
                                                <c:otherwise>
                                                    <input type="checkbox" value="${skuId}" name="itemsInCart" id="${skuId}"/>
                                                    <input type="hidden" value="${commerceItem.transactionQuantity}" name="${skuId}" id="${skuId}_qty">
                                                </c:otherwise>
                                            </c:choose>
                                            <span class="visible-xs-inline ml5">
                                                          <%--<dsp:valueof value="${productMap.displayName}" converter="valueishtml"/>--%>
                                                        <dsp:valueof value="${commerceItem.itemDescription}" converter="valueishtml"/>,
                                                        <%-- <dsp:valueof value="${productMap.descriptionLine2}" converter="valueishtml"/> --%>
                                                </span>
                                        </label>

                                            <%--<span class="checkbox-label">
                                                <c:if test="${productDisabled}">
                                                    <button class="question-tooltip" role="button" data-trigger="hover" data-toggle="popover"
                                                            data-trigger="focus" data-placement="right"
                                                            data-content="This item is not available for online order.">
                                                        <span class="glyphicon glyphicon-question-sign"></span>
                                                    </button>
                                                </c:if>
                                            </span>--%>
                                            <%--</label>--%>
                                    </td>
                                    <td class="hidden-xs">
                                                <%--<dsp:valueof value="${productMap.displayName}" converter="valueishtml"/>--%>
                                            <dsp:valueof value="${commerceItem.itemDescription}" converter="valueishtml"/>
                                                <%-- <c:if test="${!empty productMap.descriptionLine2 && productMap.descriptionLine2 != ''}">, </c:if>
                                        <dsp:valueof value="${productMap.descriptionLine2}" converter="valueishtml"/> --%>
                                    </td>
                                    <td><span class="caption">Item #</span><dsp:valueof value="${commerceItem.identifier2ndItem}" converter="valueishtml"/></td>
                                    <td class="text-center text-left-xs"><span class="caption">Quantity</span><span class="item-qty">${commerceItem.transactionQuantity}</span></td>
                                    <td>
                                        <span class="caption">Price</span><dsp:valueof value="${commerceItem.amountPricePerUnit}" converter="currencyConversion"/>
                                    </td>
                                    <td>
                                        <span class="caption">Total</span><dsp:valueof value="${commerceItem.amountExtendedPrice}" converter="currencyConversion"/>
                                    </td>
                                    <td class="text-center">
                                        <div class="caption">Status</div>

                                        <c:choose>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Received')}">
                                                <dsp:getvalueof var="labelClass" value="label-default"/>
                                                <dsp:getvalueof var="status" value="Received"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Processing')}">
                                                <dsp:getvalueof var="labelClass" value="label-warning"/>
                                                <dsp:getvalueof var="status" value="Processing"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Shipped')}">
                                                <dsp:getvalueof var="labelClass" value="label-primary"/>
                                                <dsp:getvalueof var="status" value="Shipped"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Delivered')}">
                                                <dsp:getvalueof var="labelClass" value="label-success"/>
                                                <dsp:getvalueof var="status" value="Delivered"/>
                                            </c:when>
                                            <c:when test="${fn:contains(commerceItem.lineStatusDescription, 'Invoiced')}">
                                                <dsp:getvalueof var="labelClass" value="label-info"/>
                                                <dsp:getvalueof var="status" value="Invoiced"/>
                                            </c:when>
                                            <c:otherwise>
                                                <dsp:getvalueof var="labelClass" value="label-success"/>
                                                <dsp:getvalueof var="status" value="${commerceItem.lineStatusDescription}"/>
                                            </c:otherwise>
                                        </c:choose>

                                        <span class="label ${labelClass}">${status}</span>
                                    </td>
                                </tr>
                            </dsp:oparam>
                        </dsp:droplet>

                    </dsp:oparam>

                </dsp:droplet>

            </dsp:oparam>
        </dsp:droplet>
        </tbody>
    </table>

    <div class="col-xs-12 text-center">
        <nav>
            <c:if test="${pageCount >= 2}">

                <ul class="pagination">

                <c:if test="${pageNumber > 1}">
                    <li class="prev">
                        <a href="#" aria-label="Previous">
                            <span class="fa fa-angle-left"></span>
                        </a>
                    </li>
                </c:if>

                    <c:choose>
                        <c:when test="${pageCount <= 5}">
                            <dsp:droplet name="/atg/dynamo/droplet/For">
                                <dsp:param name="howMany" value="${commerceItemsCount}"/>
                                <dsp:oparam name="output">
                                    <dsp:getvalueof var="count" param="count"/>
                                    <c:if test="${count <= pageCount}">
                                        <c:choose>
                                            <c:when test="${pageNumber == count}">
                                                <li class="active"><a href="#"><dsp:valueof param="count"/></a></li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="page-number"><a href="#"><dsp:valueof param="count"/></a></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </dsp:oparam>
                            </dsp:droplet>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${(pageNumber > 4)}">
                                <li class="page-number"><a href="#">1</a></li>
                                <li><span>...</span></li>
                            </c:if>
                            <c:if test="${(pageNumber <= 3)}">
                                <dsp:droplet name="/atg/dynamo/droplet/For">
                                    <dsp:param name="howMany" value="5"/>
                                    <dsp:oparam name="output">
                                        <dsp:getvalueof var="count" param="count"/>
                                        <c:choose>
                                            <c:when test="${pageNumber == count}">
                                                <li class="active">
                                                    <a href="#"><dsp:valueof value="${count}"/></a>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="page-number">
                                                    <a href="#"><dsp:valueof value="${count}"/></a>
                                                </li>
                                            </c:otherwise>
                                        </c:choose>
                                    </dsp:oparam>
                                </dsp:droplet>
                            </c:if>
                            <c:if test="${(pageNumber > 3) && (pageNumber < (pageCount - 2))}">
                                <li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber-2)}"/></a></li>
                                <li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber-1)}"/></a></li>
                                <li class="active"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber)}"/></a></li>
                                <li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber+1)}"/></a></li>
                                <li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber+2)}"/></a></li>
                            </c:if>
                            <c:if test="${(pageNumber >= (pageCount - 2))}">
                                <dsp:getvalueof var="start" value="${pageCount-5}"/>
                                <dsp:droplet name="/atg/dynamo/droplet/For">
                                    <dsp:param name="howMany" value="5"/>
                                    <dsp:oparam name="output">
                                        <dsp:getvalueof var="count" param="count"/>
                                        <dsp:getvalueof var="start" value="${start+1}"/>
                                        <c:choose>
                                            <c:when test="${pageNumber == start}">
                                                <li class="active"><a href="#"><dsp:valueof value="${start}"/></a></li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="page-number"><a href="#"><dsp:valueof value="${start}"/></a></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </dsp:oparam>
                                </dsp:droplet>
                            </c:if>
                            <c:if test="${(pageNumber < (pageCount - 2))}">
                                <li class="image"><span>...</span></li>
                                <li class="page-number"><a href="#"><dsp:valueof value="${pageCount}"/></a></li>
                            </c:if>
                        </c:otherwise>
                    </c:choose>

                    <c:if test="${pageNumber < pageCount}">
                        <li class="next">
                            <a href="#" aria-label="Next">
                                <span class="fa fa-angle-right"></span>
                            </a>
                        </li>
                    </c:if>

                </ul>

            </c:if>


        </nav>
    </div>

</dsp:page>
