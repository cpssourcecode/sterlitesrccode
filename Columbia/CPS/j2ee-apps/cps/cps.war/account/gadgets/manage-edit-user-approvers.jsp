<dsp:page>
	<dsp:getvalueof var="userId" param="userId"/>
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
	<dsp:droplet name="/cps/droplet/ManageApproversDroplet">
		<dsp:oparam name="output">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="approvers"/>
				<dsp:param name="elementName" value="approver"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="appId" param="approver.id"/>
					<dsp:getvalueof var="appFirstName" param="approver.firstName"/>
					<dsp:getvalueof var="appLastName" param="approver.lastName"/>
					<div class="checkbox" id="myCheckbox">
						<dsp:droplet name="/cps/droplet/UserContainsApproverDroplet">
							<dsp:param name="user" value="${userId}"/>
							<dsp:param name="approverId" value="${appId}"/>
							<dsp:oparam name="true">
								<label>
									<input name="check" id="${vsg_utils:escapeHtml(appId)}" type="checkbox" checked="checked"/>${vsg_utils:escapeHtml(appFirstName)}&nbsp;${vsg_utils:escapeHtml(appLastName)}
                                </label>
							</dsp:oparam>
							<dsp:oparam name="false">
								<label>
									<input name="check" id="${vsg_utils:escapeHtml(appId)}" type="checkbox"/>
                                        ${vsg_utils:escapeHtml(appFirstName)}&nbsp;${vsg_utils:escapeHtml(appLastName)}
								</label>
							</dsp:oparam>
						</dsp:droplet>
					</div>
				</dsp:oparam>
				<dsp:oparam name="empty">
					<p>Warning! Your organization currently has no approvers associated with it. Please
						create a new approver before creating a new buyer.</p>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>