<dsp:page>
	<dsp:getvalueof var="formHandler" param="formHandler"/>
	<dsp:getvalueof var="formExceptions" vartype="java.lang.Object" param="formHandler.formExceptions"/>
	<dsp:getvalueof var="alertClass" param="alertClass"/>
	<c:if test="${empty alertClass}">
		<dsp:getvalueof var="alertClass" value="alert alert-danger"/>
	</c:if>

	<c:set var="errorCodesSize" value="0"/>

	<c:if test="${not empty formExceptions}">
			<div class="${vsg_utils:escapeHtml(alertClass)}" id="errorMessages">
				<c:forEach var="formException" items="${formExceptions}">
					<dsp:param name="formException" value="${formException}"/>
					<dsp:getvalueof var="errorCode" param="formException.errorCode"/>

					<input type="hidden" name="exceptionErrorCode${errorCodesSize}" id="exceptionErrorCode${errorCodesSize}" value="${errorCode}"/>

					<c:set var="errorCodesSize" value="${errorCodesSize + 1}"/>

					<c:if test="${formException.message != ''}">
						<dsp:valueof param="formException.message" valueishtml="true"/><br>
					</c:if>
				</c:forEach>
			</div>
	</c:if>

	<input type="hidden" name="errorCodesSize" id="errorCodesSize" value="${errorCodesSize}"/>
</dsp:page>