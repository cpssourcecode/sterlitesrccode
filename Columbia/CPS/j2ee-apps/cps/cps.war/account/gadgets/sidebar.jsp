<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>


	<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 catalog-filter">
		<h4>My Account</h4>

		<div class="panel-group">
			<div class="panel panel-default panel-link">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a href="${originatingRequest.contextPath}/account/profile-settings.jsp">Profile & Setting</a>
					</h4>
				</div>
			</div>
			<div class="panel panel-default panel-link">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a href="${originatingRequest.contextPath}/account/addresses.jsp">Addresses</a>
					</h4>
				</div>
			</div>
			<div class="panel panel-default panel-link">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a href="${originatingRequest.contextPath}/account/order-list.jsp">Orders</a>
					</h4>
				</div>
			</div>
			<div class="panel panel-default panel-link">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a href="${originatingRequest.contextPath}/account/packing-list.jsp">Packing Slips</a>
					</h4>
				</div>
			</div>
			<div class="panel panel-default panel-link">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a href="${originatingRequest.contextPath}/account/reorder-list.jsp">Re-order List</a>
					</h4>
				</div>
			</div>
			<div class="panel panel-default panel-link">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a href="${originatingRequest.contextPath}/account/material-lists.jsp">My Material Lists</a>
					</h4>
				</div>
			</div>
		</div>

		<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
			<dsp:param name="userId" bean="Profile.id"/>
			<dsp:param name="role" value="superAdmin"/>
			<dsp:oparam name="true">
				<div class="panel-group">
					<div class="panel panel-default panel-link">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a href="account-landing.jsp">Manage Users</a>
							</h4>
						</div>
					</div>
					<div class="panel panel-default panel-link">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a href="account-landing.jsp">Manage Orders</a>
							</h4>
						</div>
					</div>
					<div class="panel panel-default panel-link">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a href="account-landing.jsp">Manage Invoices</a>
							</h4>
						</div>
					</div>
				</div>
			</dsp:oparam>
		</dsp:droplet>
	</div>

</dsp:page>
