<dsp:page>
    <dsp:include page="/global/modals/access-customer-account.jsp"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="csrDisplayAccessWarning" bean="Profile.csrDisplayAccessWarning"/>
	<dsp:getvalueof var="searchTerm" param="searchTerm"/>
	<dsp:getvalueof var="letter" param="letter"/>
	<dsp:getvalueof var="sort" param="sort"/>
	<dsp:getvalueof var="order" param="order"/>
	<dsp:getvalueof var="page" param="page"/>

	<dsp:droplet name="/cps/droplet/ManageCompaniesDroplet">
		<dsp:param name="term" value="${searchTerm}"/>
		<dsp:param name="letter" value="${letter}"/>
		<dsp:param name="sort" value="${sort}"/>
		<dsp:param name="order" value="${order}"/>
		<dsp:param name="page" value="${page}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="emptyResult" param="empty"/>
			<dsp:getvalueof var="list" param="list"/>
			<dsp:getvalueof var="letterMap" param="letterMap"/>
			<dsp:getvalueof var="total" param="total"/>
			<dsp:getvalueof var="numPerPage" param="numPerPage"/>
			<dsp:getvalueof var="numPages" param="numPages"/>

			<input type="hidden" id="currentSort" value="${sort}"/>
			<input type="hidden" id="currentPage" value="${page}"/>
			<input type="hidden" id="currentOrder" value="${order}"/>
			<input type="hidden" id="currentLetter" value="${letter}"/>
			
			<div class="text-center">
				<nav>
					<ul class="pagination">
						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" value="${letterMap}"/>
							<dsp:param name="elementName" value="contains"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="key" param="key"/>
								<dsp:getvalueof var="contains" param="contains"/>
								<c:choose>
									<c:when test="${contains == 'true'}">
										<c:choose>
											<c:when test="${letter == key}">
												<li class="active">
											</c:when>
											<c:otherwise>
												<li>
											</c:otherwise>
										</c:choose>
										<a data-event-click-id="eventFirstLetter" data-key="${key}" ><dsp:valueof param="key"/></a>
										</li>
									</c:when>
									<c:otherwise>
										<li><span><dsp:valueof param="key"/></span></li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</ul>
				</nav>
			</div>

			<table class="table table-striped table-mobile">
				<dsp:include page="/account/gadgets/manage-companies-sort.jsp">
					<dsp:param name="sort" value="${sort}"/>
					<dsp:param name="order" value="${order}"/>
				</dsp:include>
				<tbody>
				<div class="container preload-cover loader-active">
					<div class="loading" style="display:block">
						<i class="fa fa-spinner fa-spin"></i>
					</div>
				</div>
				<dsp:droplet name="/atg/dynamo/droplet/ForEach">
					<dsp:param name="array" value="${list}"/>
					<dsp:param name="elementName" value="company"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="companyId" param="company.id"/>
						<dsp:getvalueof var="companyCity" param="company.city"/>
						<dsp:getvalueof var="companyState" param="company.state"/>

						<tr>
							<td>
								<dsp:valueof param="company.companyName"/>
							</td>
							<td><dsp:valueof param="company.accountNumber"/></td>
							<td><dsp:valueof param="company.address1"/> <br>
								${companyCity}<c:if test="${not empty companyCity}">, </c:if>${companyState}<c:if test="${not empty companyState}">, </c:if><dsp:valueof param="company.postalCode"/>
							</td>
							<td class="text-center">
								<dsp:getvalueof var="needsSetup" param="company.needsSetup"/>
								<c:choose>
									<c:when test="${needsSetup}">
										<dsp:a href="/account/add-customer-admin.jsp"> Set Up
										     <dsp:param name="c" param="company.id"/>
										     <dsp:param name="companyName" param="company.companyName"/>
										</dsp:a>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${csrDisplayAccessWarning == false}">
												<a href="javascript:void(0);" data-event-click-id="eventAccessWithoutWarning" data-company-id="${companyId}"  > Access
												</a>
											</c:when>
											<c:otherwise>
												<a href="javascript:void(0);" data-event-click-id="eventSetAccount" data-company-id="${companyId}"  data-toggle="modal" data-target="#accessCustomerAccount"> Access
												</a>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</dsp:oparam>
					<dsp:oparam name="empty">
						<c:choose>
							<c:when test="${emptyResult == true}">
								<div class="alert alert-danger">
									No results found, try again.
								</div>
							</c:when>
							<c:when test="${searchTerm == ''}">
								<div class="alert alert-danger">
									Please enter Company or Account #
								</div>
							</c:when>
						</c:choose>
					</dsp:oparam>
				</dsp:droplet>
				</tbody>
			</table>

			<div class="text-center">
				<nav id="pagination">
					<dsp:include page="/account/gadgets/pagination.jsp">
						<dsp:param name="page" value="${page}"/>
						<dsp:param name="total" value="${total}"/>
						<dsp:param name="numPerPage" value="${numPerPage}"/>
						<dsp:param name="numPages" value="${numPages}"/>
					</dsp:include>
				</nav>
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>