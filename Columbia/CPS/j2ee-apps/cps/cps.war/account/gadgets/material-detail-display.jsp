<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/cps/droplet/ReorderlistLookupDroplet"/>
	<dsp:importbean bean="/cps/droplet/SearchReorderDetailsDroplet"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>

	<%-- <dsp:getvalueof var="sort" param="sort"/> --%>
	<dsp:getvalueof var="class" param="class"/>
	<%-- <dsp:getvalueof var="accend" param="accend"/> --%>
	<dsp:getvalueof var="searchTerm" param="searchTerm"/>
	<dsp:getvalueof var="showPA" param="showPA"/>

	<dsp:getvalueof var="page" param="page"/>

	<%-- <dsp:getvalueof var="order" value="-"/>
	<c:if test="${accend eq true}">
		<dsp:getvalueof var="order" value="+"/>
	</c:if> --%>
	<c:if test="${empty class}">
		<dsp:getvalueof var="class" value="fa-sort-asc"/>
	</c:if>
	<dsp:getvalueof var="giftlistId" param="giftlistId"/>
	<dsp:getvalueof var="maxMLItemsPerPage" bean="CPSGlobalProperties.maxMLItemsPerPage"/>

	<dsp:droplet name="SearchReorderDetailsDroplet">
		<dsp:param name="term" value="${searchTerm}"/>
		<dsp:param name="giftlistId" value="${giftlistId}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="giftItems" param="giftItems"/>
			<dsp:getvalueof var="unavailableItems" param="unavailableItems"/>
			<dsp:getvalueof var="emptyResult" param="emptyResult"/>
			<dsp:getvalueof var="totalGiftlistItems" param="totalGiftlistItems"/>
			<dsp:getvalueof var="allIds" param="allIds"/>
			<input type="hidden" value="${allIds}" id="allGiftListIds"/>
			<input type="hidden" value="${fn:length(giftItems)}" id="total_desired_qty"/>
			<c:choose>
				<c:when test="${emptyResult}">
					<input type="hidden" value="0" id="total_count"/>
				</c:when>
				<c:otherwise>
					<input type="hidden" value="${fn:length(giftItems)}" id="total_count"/>
					<dsp:getvalueof var="isAvailablePricesWebService" value="true"/>
					
					<div class="col-md-12 alert-danger alert-no-available" id="products_obsolete_msg" style="display: none;">
						<div class="alert-icon">
							<i class="fa fa-exclamation-triangle red" style="position: static"></i>
							<strong style="color: #333333;">
								<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="errProductsObsoleteNoLongerAvailable"/>
									<dsp:param name="notWrap" value="true"/>
								</dsp:include>
								<!-- One or more items in your cart is now obsolete and no longer available for purchase. Obsolete items will be removed from cart at checkout. -->
							</strong>
						</div>
					</div>
					
					<table class="table table-striped table-mobile table-check">
				   	<thead>
				   		<c:if test="${fn:length(giftItems) != 0}">
					   		<tr>
								<th class="text-center">Select All</th>
								
								<th class="check">
									<label class="checkbox-custom hidden-print" data-initialize="checkbox">
										<input id="selectAllBoxesCheck" type="checkbox" data-event-click-id="eventSelectAll" >
										<span class="visible-xs-inline ml5">All</span>
									</label>
								</th>
								
								<th><span data-event-click-id="eventSortMaterialDetails" data-sort-by="description"  style="cursor: pointer"><strong>
									Description <i id="sort-description" class="fa ${sort == 'description' ? class : 'fa-sort'}"></i>
								</strong></span></th>
										
								<th><span data-event-click-id="eventSortMaterialDetails" data-sort-by="displayName"   style="cursor: pointer"><strong>
									Item # <i id="sort-itemNumber" class="fa ${sort == 'displayName' ? class : 'fa-sort'}"></i>
								</strong></span></th>
								
								<th><span data-event-click-id="eventSortMaterialDetails" data-sort-by="aliasNumber"   style="cursor: pointer"><strong>
									My Part # <i id="sort-aliasNumber" class="fa ${sort == 'aliasNumber' ? class : 'fa-sort'}"></i>
								</strong></span></th>
								
								<th class="text-center"><strong>Quantity</strong></th>

							    <th class="text-center"><span><strong>
									Availability
								</strong></span></th>
							    <th class="text-center"><span><strong>Price</strong></span></th>
							</tr>
				   		</c:if>
					   </thead>
					   
					   <tbody>
					   		<dsp:getvalueof var="max" value="${page*maxMLItemsPerPage}"/>
							<dsp:getvalueof var="min" value="${max-maxMLItemsPerPage}"/>
							
					   		<dsp:droplet name="ForEach">
								<%-- <dsp:param name="sortProperties" value="${order}${sort}"/> --%>
								<dsp:param name="array" value="${giftItems}"/>
								<dsp:param name="elementName" value="giftItem"/>
	
								<dsp:oparam name="output">
									<dsp:getvalueof var="count" param="count"/>
									<dsp:getvalueof var="index" param="index"/>
									<c:if test="${count>min && count<=max}">
										<dsp:getvalueof var="giftItemId" param="giftItem.id"/>
										<dsp:getvalueof var="giftItemUnavailable" param="giftItem.eCommerceUnavailable"/>
										<dsp:getvalueof var="prodId" param="giftItem.catalogRefId"/>
										<dsp:getvalueof var="productDisabled" value="${false}"/>
										<dsp:getvalueof var="disabledClass" value=""/>
										<dsp:droplet name="/atg/targeting/RepositoryLookup">
											<dsp:param name="id" value="${prodId}"/>
											<dsp:param name="repository" bean="/atg/commerce/catalog/ProductCatalog"/>
											<dsp:param name="itemDescriptor" value="product"/>
											<dsp:param name="elementName" value="product"/>
											<dsp:oparam name="output">
												<dsp:getvalueof var="product" param="product"/>
												<dsp:getvalueof var="stockingType" param="product.stockingType"/>
												<dsp:getvalueof var="productPurchasable" param="product.productPurchasable"/>
												<c:if test="${stockingType == 'O' || stockingType == 'U' || stockingType == 'K' || stockingType == 'X' || stockingType == '4' || giftItemUnavailable}">
													<dsp:getvalueof var="productDisabled" value="${true}"/>
													<dsp:getvalueof var="disabledClass" value="disabled"/>
												</c:if>
												<c:if test="${!productPurchasable}">
													<dsp:getvalueof var="disabledClass" value="disabled"/>
												</c:if>
												<c:if test="${index == 0}">
													<dsp:droplet name="/cps/droplet/CPSPriceDroplet">
														<dsp:param name="productId" param="product.id"/>
														<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
														<c:if test="${not isTransient}">
															<dsp:param name="profile" bean="Profile"/>
														</c:if>
														<dsp:oparam name="emptyPrice">
															<dsp:getvalueof var="isAvailablePricesWebService" bean="CPSSessionBean.availablePricesWebService" />
															<input type="hidden" name="shouldCheckPrice" id="shouldCheckPrice" value="true"/>
															<input type="hidden" name="isAvailablePricesWebService" id="isAvailablePricesWebService" value="${isAvailablePricesWebService}"/>
														</dsp:oparam>
													</dsp:droplet>
												</c:if>
												
												<c:if test="${productDisabled || !productPurchasable}">
													<input type="hidden" name="productDisabled" id="productDisabled" value="${productDisabled || !productPurchasable}"/>
									            </c:if>
		
												<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
													<dsp:param name="prodId" value="${prodId}"/>
													<dsp:oparam name="output">
														<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
													</dsp:oparam>
												</dsp:droplet>
												<!-- Table row start -->
												<tr>
													<!-- IMAGE -->
													<td class="text-center">
														<dsp:tomap var="productMap" param="product"/>
														<dsp:getvalueof var="imageUrl" vartype="java.lang.String"
																		value="${productMap.thumbnail_url}"/>
														<c:set var="imageUrl" value="${empty imageUrl ? '/assets/images/plp-placeholder.png' : imageUrl}"/>
														<cp:readerimg src="${imageUrl}" alt="" width="60px" height="60px" />
													</td>
													
													<!-- CHECK BOX id="select-item-material-check" -->
													<td class="" >
														<label class="checkbox-custom hidden-print" data-initialize="checkbox">
															<c:choose>
																<c:when test="${productDisabled || !productPurchasable}">
																	<input type="checkbox" value="" name="check" id="${giftItemId}" disabled/>
																</c:when>
																<c:otherwise>
																	<input type="checkbox" value="" name="check" id="${giftItemId}"/>
																</c:otherwise>
															</c:choose>
															<span class="caption visible-xs-inline ml5">
																${productMap.description}
																<c:if test="${productDisabled}">
							                                    	<dsp:include page="/includes/gadgets/product-not-available.jsp"/>
																</c:if>	
																<c:if test="${!productPurchasable}">
							                                    	<dsp:include page="/includes/gadgets/product-not-purchasable.jsp"/>
																</c:if>
															</span>
														</label>
													</td>
	
													<!-- DESCRIPTION -->
													<td class="hidden-xs">
														<dsp:a iclass="product-title" href="${productUrl}">
															<dsp:valueof value="${productMap.description}" converter="valueishtml"/>,
															<%-- <dsp:valueof value="${productMap.descriptionLine2}" converter="valueishtml"/> --%>
														</dsp:a>
														<div id="minQtyError${prodId}" class="red" style="display:none;"></div>
														
														<c:if test="${productDisabled}">
					                                    	<dsp:include page="/includes/gadgets/product-not-available.jsp"/>
														</c:if>
														<c:if test="${!productPurchasable}">
							                                    	<dsp:include page="/includes/gadgets/product-not-purchasable.jsp"/>
																</c:if>
													</td>
													
													<td>
														<span class="caption">Item #</span>
														<dsp:valueof param="product.displayName" valueishtml="true"/>
													</td>
													
													<td>
														<span class="caption">My Part #</span>
														<dsp:droplet name="/cps/droplet/AliasNumberLookup">
															<dsp:param name="skuId" param="product.childSKUs[0].id"/>
															<dsp:oparam name="output">
																<dsp:getvalueof var="prodAlias" param="alias"/>
                    											<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',<br/>')}"/>
																${alias}
															</dsp:oparam>
															<dsp:oparam name="empty">
																&nbsp;
															</dsp:oparam>
														</dsp:droplet>
													</td>

													<!-- Quantity -->
													<td class="text-center text-left-xs">
														<span class="caption" style="display:none" id="${giftItemId}_qtycaption">&nbsp;</span>
														<span class="help-block text-danger nowrap" style="display:none" id="error-qty-${giftItemId}_qty"></span>
														<div id="${giftItemId}_qtyDiv">
															<span class="caption">Quantity</span>
															<dsp:getvalueof var="quantityDesired" param="giftItem.quantityDesired"/>
															<input type="text" value="${quantityDesired}" name="${giftItemId}" id="${giftItemId}_qty"
															       maxlength="5" class="form-control text-center input-qty qty-input" data-prod-id="${prodId}"
															       <c:if test="${productDisabled}">disabled style="color: darkgray;"</c:if> >
															<a <c:if test="${!productDisabled}">href="#" data-event-click-id="eventUpdateList" data-gift-item-id="${giftItemId}" </c:if>
															   <c:if test="${productDisabled}">style="color: darkgray;"</c:if> ><br/>Update Item</a>
														</div>
													</td>

													<!-- Availability -->
													<td class="text-center" id="avlbt_${prodId}" <c:if test="${productDisabled}">style="color: darkgray;"</c:if>>
														<i class="fa fa-spinner fa-spin"></i>
													</td>


													<!-- Price -->
													<td class="text-center" id="price_${prodId}" <c:if test="${productDisabled}">style="color: darkgray;"</c:if>>
														<i class="fa fa-spinner fa-spin"></i>
													</td>

													<input type="hidden" value="${prodId}" name="priceInfo_${giftItemId}" id="priceInfo_${giftItemId}"/>
												 </tr>
											</dsp:oparam>
										</dsp:droplet>
									
									</c:if>
								</dsp:oparam>
								<dsp:oparam name="outputEnd">
									<input type="hidden" value="${count}" id="total_count"/>
								</dsp:oparam>
								<dsp:oparam name="empty">
									<input type="hidden" value="0" id="total_count"/>
									<dsp:include page="/includes/gadgets/info-message.jsp">
										<dsp:param name="key" value="prof-list-noItems"/>
									</dsp:include>
								</dsp:oparam>
							</dsp:droplet>
					   </tbody>
				   </table>
				
					<div class="content-list scrollbar-inner">
						
					</div>
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>