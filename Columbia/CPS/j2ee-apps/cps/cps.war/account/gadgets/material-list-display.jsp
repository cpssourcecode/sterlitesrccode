<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/cps/droplet/UserMaterialListsDroplet"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/util/MaterialListDownloadHandler"/>

	<dsp:getvalueof var="sort" param="sort"/>
	<dsp:getvalueof var="accend" param="accend"/>
	<dsp:getvalueof var="searchTerm" param="searchTerm"/>
	<dsp:getvalueof var="searchCS" param="searchCS"/>
	<dsp:getvalueof var="page" param="page"/>

	<dsp:getvalueof var="order" value="-"/>
	<c:if test="${accend eq true}">
		<dsp:getvalueof var="order" value="+"/>
	</c:if>

	<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />

	<dsp:droplet name="UserMaterialListsDroplet">
		<dsp:param name="term" value="${searchTerm}"/>
		<dsp:param name="cs" value="${searchCS}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="giftlists" param="giftlists"/>
			<dsp:getvalueof var="emptyResult" param="emptyResult"/>
			<c:choose>
				<c:when test="${emptyResult && empty searchTerm}">
					<input type="hidden" value="0" id="total_count"/>

					<div class="well text-center">
						<h4>
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="no-material-lists-created"/>
								<dsp:param name="notWrap" value="true"/>
							</dsp:include>
						</h4>
					</div>
					
				</c:when>
				<c:when test="${emptyResult}">
					<input type="hidden" value="0" id="total_count"/>
				</c:when>
				<c:otherwise>
					<dsp:getvalueof var="max" value="${page*3}"/>
					<dsp:getvalueof var="min" value="${max-3}"/>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="CPSSessionBean.sortableReorderLists"/>
						<dsp:param name="elementName" value="giftlist"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<c:if test="${count>min && count<=max}">
								<dsp:getvalueof var="id" param="giftlist.id"/>
								<dsp:getvalueof var="date" param="giftlist.creationDate"/>
								<dsp:getvalueof var="giftlistItems" param="giftlist.giftlistItems"/>

								<div class="material-list">

									<dsp:form action="${originatingRequest.requestURI}" method="post" formid="addGiftlistToCartForm${id}"
											  id="addGiftlistToCartForm${id}" iclass="hidden">
										<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="${fn:length(giftlistItems)}" priority="10"/>
										<dsp:droplet name="ForEach">
											<dsp:param name="array" value="${giftlistItems}"/>
											<dsp:param name="elementName" value="giftlistItem"/>
											<dsp:oparam name="output">
												<dsp:getvalueof var="index" param="index"/>
												<dsp:getvalueof var="productId" param="giftlistItem.productId"/>
												<dsp:getvalueof var="skuId" param="giftlistItem.catalogRefId"/>
												<dsp:getvalueof var="quantity" param="giftlistItem.quantityDesired"/>
												<dsp:getvalueof var="giftlistId" param="giftlistItem.id"/>
												<dsp:input bean="CartModifierFormHandler.items[${index}].catalogRefId" value="${skuId}" type="hidden"/>
												<dsp:input bean="CartModifierFormHandler.items[${index}].productId" value="${productId}" type="hidden"/>
												<dsp:input bean="CartModifierFormHandler.items[${index}].quantity" value="${quantity}" type="hidden"/>
												
												<c:if test="${index == 0}">
													<dsp:droplet name="/cps/droplet/CPSPriceDroplet">
														<dsp:param name="productId" value="${productId}"/>
														<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
														<c:if test="${not isTransient}">
															<dsp:param name="profile" bean="Profile"/>
														</c:if>
														<dsp:oparam name="emptyPrice">
															<dsp:getvalueof var="isAvailablePricesWebService" bean="CPSSessionBean.availablePricesWebService" />
															<input type="hidden" name="isAvailablePricesWebService" id="isAvailablePricesWebService" value="${isAvailablePricesWebService}"/>
														</dsp:oparam>
													</dsp:droplet>
												</c:if>
											</dsp:oparam>
										</dsp:droplet>
										<dsp:input bean="CartModifierFormHandler.addMultipleItemsToOrder" type="hidden" value="true" priority="-10"/>
										<input name="itemsFromMaterialList" type="hidden" value="true"/>
									</dsp:form>

									<dsp:getvalueof var="giftlistId" param="giftlist.id"/>
									<dsp:include
											page="${originatingRequest.contextPath}/global/modals/modal-import-material-list.jsp?giftlistId=${giftlistId}">
										<dsp:param name="importModalError" param="importModalError"/>
									</dsp:include>

									<div class="well row row-flush has-tools">
										<div class="col-sm-8 p15">
											<h4 class="title">
												<dsp:valueof param="giftlist.eventName"/>
											</h4>

											<div class="row">
												<div class="box col-sm-3 col-xs-6">
													<h3 class="h6 mb0">Created</h3>
													<p><fmt:formatDate pattern="MM/dd/yyyy" value="${date}"/></p>
												</div>
												<div class="box col-sm-3 col-xs-6">
													<h3 class="h6 mb0"># of Items</h3>
													<p>${fn:length(giftlistItems)}</p>
												</div>
											</div>
											<div class="row">
												<div class="box col-sm-12">
													<p style="word-wrap: break-word"><dsp:valueof param="giftlist.description"/></p>
												</div>
											</div>

											<div class="text-right">
												<a href="#" class="btn btn-primary xs-block"
												   data-event-click-id="eventShowImportMaterialListModalPopup"
												   data-giftlist-id="${giftlistId}">
													<fmt:message key="account.material.upload_material_list"/>
												</a>

												<dsp:a href="/account/material-detail.jsp"
													   iclass="btn btn-primary xs-block">
													<dsp:param name="giftlistId" param="giftlist.id"/>
													<fmt:message key="account.material.view_edit_list"/>
												</dsp:a>

												<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
													<dsp:param name="id" value="err_0_item"/>
													<dsp:param name="elementName" value="message"/>
													<dsp:oparam name="output">
														<dsp:getvalueof var="message" param="message.message"/>
													</dsp:oparam>
													<dsp:oparam name="empty">
														<dsp:getvalueof var="message"
																		value="There are 0 items to add to cart"/>
													</dsp:oparam>
												</dsp:droplet>
												<button class="btn btn-info xs-block addToCartButton" role="button"
														data-class="warn" data-placement="top" data-trigger="focus"
														data-content="${message}"
														data-event-click-id="eventAddGiftlistToCart" data-id="${id}"
														data-count="${fn:length(giftlistItems)}">
													<fmt:message key="account.material.add_all_items_to_cart"/>
												</button>
											</div>
										</div>
										
										<div class="col-sm-4">
											<ul class="tool-list mb0">
											<li><dsp:a href="/account/material-detail.jsp" iclass="list-group-item">
													<dsp:param name="giftlistId" param="giftlist.id"/>
													<dsp:param name="ca" value="true"/>
													<fmt:message key="account.material.check_price_and_availability"/>
												</dsp:a></li>
												
											<li><a href="#" class="list-group-item" data-event-click-id="eventShowShareMaterialModalPopup" data-id="${id}" >
													<fmt:message key="account.material.share"/>
												</a></li>
												
											<li><a href="#" class="list-group-item hidden-xs" data-event-click-id="eventExportMaterialList" data-id="${id}" >
													<fmt:message key="account.material.download"/>
												</a></li>
												
											<li><a href="#" class="list-group-item" data-event-click-id="eventShowDeleteMaterialListModalPopup" data-id="${id}" >
													<fmt:message key="account.material.delete_material_list"/>
												</a></li>
										   </ul>
									   </div>										
									</div>
								</div>

								<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-delete-material-list.jsp?giftlistId=${id}"/>
								<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-share-material-list.jsp?giftlistId=${id}"/>

								<dsp:form action="/account/material-lists.jsp" method="post" id="exportList${id}" formid="exportList${id}">
									<dsp:input type="hidden" bean="MaterialListDownloadHandler.quoteAcknowldge" value="false" />
									<dsp:input type="hidden" bean="MaterialListDownloadHandler.giftlistId" value="${id}"/>
									<dsp:input type="hidden" bean="MaterialListDownloadHandler.createSpreadsheet" value="true" priority="-10"/>
								</dsp:form>

							</c:if>

						</dsp:oparam>
						<dsp:oparam name="outputEnd">
							<input type="hidden" value="${count}" id="total_count"/>
						</dsp:oparam>
						<dsp:oparam name="empty">
							<div class="well text-center">
								<h4>
									<dsp:include page="/includes/gadgets/info-message.jsp">
										<dsp:param name="key" value="no-material-lists-created"/>
										<dsp:param name="notWrap" value="true"/>
									</dsp:include>
								</h4>
							</div>
							<input type="hidden" value="0" id="total_count"/>
						</dsp:oparam>
					</dsp:droplet>
				</c:otherwise>
			</c:choose>
		</dsp:oparam>

	</dsp:droplet>

	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
		<dsp:param name="messageId" value="err_search_fail"/>
		<dsp:param name="isReset" value="true"/>
	</dsp:include>

</dsp:page>