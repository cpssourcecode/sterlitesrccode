<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/droplet/ReorderlistLookupDroplet"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/util/MaterialListDownloadHandler"/>
	<dsp:getvalueof var="importModalError" param="importModalError"/>
	<dsp:importbean bean="/cps/userprofiling/MaterialListHandler"/>
	<dsp:getvalueof var="ca" param="ca"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="showPA" param="showPA"/>
	<dsp:getvalueof var="materialListSuccessfullyUpdated" bean="CPSSessionBean.materialListSuccessfullyUpdated" />
	<dsp:getvalueof var="maxMLItemsPerPage" bean="CPSGlobalProperties.maxMLItemsPerPage"/>

	<dsp:getvalueof var="itemsToUpdate" param="itemsToUpdate" />
	<div class="container main-content ${csrClass} minheight">
		<input type="hidden" value="${maxMLItemsPerPage}" id="numPerPage" />
		<section>
			<div class="row">
				<dsp:getvalueof var="invalidMaterialItemsList" bean="CPSSessionBean.invalidMaterialItemsList"/>
				<dsp:param name="materialDuplicates" bean="CPSSessionBean.materialDuplicates"/>
				<dsp:param name="hasMaterialDuplicates" bean="CPSSessionBean.hasMaterialDuplicates"/>
				<dsp:getvalueof var="hasMaterialDuplicates" param="hasMaterialDuplicates"/>
				<dsp:getvalueof var="hasInvalidItems" value="false"/>
				
				<dsp:getvalueof var="giftlistFormExceptions" vartype="java.lang.Object" bean="GiftlistFormHandler.formExceptions"/>
				<dsp:getvalueof var="fileUploadFormExceptions" vartype="java.lang.Object" bean="/cps/util/FileUploadHandler.formExceptions"/>
				
				<c:if test="${fn:length(invalidMaterialItemsList) > 0 || not empty giftlistFormExceptions || not empty fileUploadFormExceptions}">
					<dsp:getvalueof var="hasInvalidItems" value="true"/>
				</c:if>
				
				<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-items-not-added-to-ml.jsp">
					<dsp:param name="invalidMaterialItemsList" value="${invalidMaterialItemsList}"/>
					<dsp:param name="materialDuplicates" param="materialDuplicates"/>
					<dsp:param name="hasMaterialDuplicates" param="hasMaterialDuplicates"/>
					<dsp:param name="giftlistId" param="giftlistId"/>
					<dsp:param name="ca" param="ca"/>
				</dsp:include>
					
				<div class="col-sm-8">
					<ol class="breadcrumb">
						<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
						<li><a href="${originatingRequest.contextPath}/account/material-lists.jsp">My Material Lists</a></li>
						<li class="active">Material List Detail</li>
					</ol>
				</div>
		
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="false"/>
					<dsp:param name="print" value="true"/>
				</dsp:include>
		
				<div class="col-xs-12">
					<h1><fmt:message key="account.material.detail"/></h1>
				</div>
				
				<%--<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-material-list-qty-error.jsp"/>--%>
				
				<dsp:droplet name="ReorderlistLookupDroplet">
					<dsp:param name="id" param="giftlistId"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="giftlist" param="giftlist"/>
						<dsp:getvalueof var="giftlistId" param="giftlist.id"/>
						<dsp:getvalueof var="eventName" param="giftlist.eventName"/>
						<dsp:getvalueof var="creationDate" param="giftlist.creationDate"/>
						<dsp:getvalueof var="cs" param="giftlist.cs"/>
						<dsp:getvalueof var="description" param="giftlist.description"/>
						<dsp:getvalueof var="giftlistItems" param="giftlist-items"/>
						<dsp:getvalueof var="totalGiftlistItems" param="totalGiftlistItems"/>
						<input type="hidden" value="${giftlistId}" id="giftlistIdValue"/>
		
						<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-import-material-list.jsp?giftlistId=${giftlistId}">
							<dsp:param name="importModalError" param="importModalError"/>
						</dsp:include>
		
						<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-edit-material-list.jsp">
							<dsp:param name="giftlist" param="giftlist"/>
						</dsp:include>
		
						<dsp:form action="/account/material-detail.jsp?giftlistId=${giftlistId}" method="post" id="exportList${giftlistId}"
								  formid="exportList${giftlistId}">
							<dsp:input type="hidden" bean="MaterialListDownloadHandler.giftlistId" value="${giftlistId}"/>
							<dsp:input type="hidden" bean="MaterialListDownloadHandler.createSpreadsheet" value="true" priority="-10"/>
						</dsp:form>
		
						<dsp:form action="" method="post" id="deleteListForm" formid="deleteListForm">
							<dsp:input type="hidden" id="listToDelete" bean="GiftlistFormHandler.removeListId" value=""/>
							<dsp:input type="hidden" bean="GiftlistFormHandler.deleteList" value="true" priority="-10"/>
						</dsp:form>
						<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-delete-material-list.jsp?giftlistId=${giftlistId}"/>
						<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-share-material-list.jsp?giftlistId=${giftlistId}"/>
					
						<div class="col-xs-12">
							<div class="material-list detail">
								<div class="well row row-flush has-tools">
									<div class="col-sm-8 p15">
										<h4 class="title"><dsp:valueof value="${eventName}"/></h4>
										
										<div class="row">
											<div class="box col-sm-3 col-xs-6">
												<h3 class="h6 mb0">Created</h3>
												<p><fmt:formatDate pattern="MM/dd/yyyy" value="${creationDate}"/></p>
											</div>
											<div class="box col-sm-3 col-xs-6">
												<h3 class="h6 mb0"># of Items</h3>
												<p id="total_items_count"><%-- ${fn:length(giftlistItems)} --%></p>
											</div>
										</div>
										<p style="word-wrap: break-word"><dsp:valueof param="giftlist.description"/></p>
										
									</div>
									<div class="col-sm-4">
									
										<ul class="tool-list mb0">
											<li><a href="#" class="list-group-item hidden-xs" style="border-left: 1px solid #f7f7f7;"
											   data-event-click-id="eventShowImportMaterialListModalPopup" data-giftlist-id="${giftlistId}" >
												<fmt:message key="account.material.import"/>
											</a></li>
												
											<li><a href="#" class="list-group-item" style="border-left: 1px solid #f7f7f7;"
											   data-event-click-id="eventShowEditMaterialListModalPopup" data-giftlist-id="${giftlistId}" >
												<fmt:message key="account.material.material_edit_name_description"/>
											</a></li>
												
											<li><a href="#" class="list-group-item" style="border-left: 1px solid #f7f7f7;"
											   data-event-click-id="eventShowShareMaterialModalPopup" data-giftlist-id="${giftlistId}" >
												<fmt:message key="account.material.share"/>
											</a></li>
												
											<li><a href="#" class="list-group-item hidden-xs" style="border-left: 1px solid #f7f7f7;"
											   data-event-click-id="eventExportMaterialList" data-giftlist-id="${giftlistId}" >
												<fmt:message key="account.material.download"/>
											</a></li>
											
											<li><a href="#" class="list-group-item" style="border-left: 1px solid #f7f7f7;"
											   data-event-click-id="eventShowDeleteMaterialListModalPopup" data-giftlist-id="${giftlistId}" >
												<fmt:message key="account.material.delete_material_list"/>
											</a></li>
										</ul>
									
									</div>
								</div>
							</div>
						</div>
					
						<div class="col-xs-12 text-center mt40">
								<div class="form-group form-group-inline w65">
									<!-- <h4 class="text-nocase">Search Material Lists</h4>
									<input type="text" class="form-control" placeholder="Item Name, # or Customer Part # or Mfr Name, # or UPC or Short Code"> -->
									<h4 class="text-nocase">Search Material Lists</h4>
									<input id="reorderDetailsSearchTerm" type="text" class="form-control quick-order-item"
														   placeholder="Item Name, # or Custom Alias # or Mfr Name, # or UPC or Short Code"
														   data-event-keypress-id="account156" >
								</div>
								<button class="btn btn-primary xs-block" data-event-click-id="eventSearchListDetails" >
									<fmt:message key="account.material.search"/>
								</button>
								<button class="btn btn-info xs-block" type="button" id="btn-reset" disabled="" data-event-click-id="eventResetDetailsSearch" >
									<fmt:message key="account.material.reset"/>
								</button>
						</div>			
		
						<div class="col-xs-12" id="availabilityImportantInfo" style="display:none;">
							<section>
								<p>
									<span class="glyphicon glyphicon-alert text-danger"></span>&nbsp;
									<fmt:message key="account.material.availability_important_info"/>
								</p>
							</section>
						</div>
						<div class="container preload-cover">
							<div class="loading preload-spinner custom-loading">
								<i class="fa fa-spinner fa-spin"></i>
							</div>
						</div>
					
						<div class="text-center col-xs-12 ml-buttons ">
							<section>
								<button id="checkPriceSelected" class="btn btn-primary check-avail-submit xs-block" data-class="warn"
								   data-placement="top" data-trigger="focus"
								   data-content="<dsp:include page="/includes/gadgets/info-message.jsp">
													<dsp:param name="key" value="err_no_select"/>
													<dsp:param name="notWrap" value="true"/>
												</dsp:include>"
								   data-event-click-id="eventCheckPriceSelected" >
									<fmt:message key="account.material.check_pa_selected"/>
								</button>
		
								<button id="addSelected" class="btn btn-primary xs-block addToCartButton" href="#" role="button" data-class="warn"
								   data-placement="top" data-trigger="focus"
								   data-content="<dsp:include page="/includes/gadgets/info-message.jsp">
													<dsp:param name="key" value="err_no_select"/>
													<dsp:param name="notWrap" value="true"/>
												</dsp:include>"
								   data-event-click-id="eventAddSelected" >
									<fmt:message key="account.material.add_selected"/>
								</button>
								
								<%-- SM-358 Update All button implemented --%>
								<button id="updateAllItemQty" class="btn btn-primary xs-block" role="button" data-class="warn"
								   data-placement="top" data-trigger="focus">
									<fmt:message key="account.material.update_all_item_qty"/>
								</button>
								<%-- SM-358 Update All button implemented --%>
								
								<a id="removeMaterial" class="btn btn-info xs-block" href="#" role="button" data-class="warn"
		
								   data-content="<dsp:include page="/includes/gadgets/info-message.jsp">
													<dsp:param name="key" value="err_no_select"/>
													<dsp:param name="notWrap" value="true"/>
												</dsp:include>"
								   data-event-click-id="eventDeleteSelected" >
									<fmt:message key="account.material.delete_selected"/>
								</a>
							</section>
						</div>
						<div class="col-xs-12">
							<div id="updateAllItemsSuccess">
								<p><cp:repositoryMessage key="updateAllItemsSuccessMsg" /></p>
							</div>
						</div>
						<div class="col-xs-12">
							<section>
								<div id="emptyResults" style="display:none;" class="alert alert-warning">
									<dsp:include page="/includes/gadgets/info-message.jsp">
										<dsp:param name="key" value="err_search_items_fail"/>
										<dsp:param name="notWrap" value="true"/>
									</dsp:include>
								</div>
		
								<div id="errorsSelected" style="display:none;" class="alert alert-danger errors">
									<dsp:include page="/includes/gadgets/info-message.jsp">
										<dsp:param name="key" value="err_no_select"/>
										<dsp:param name="notWrap" value="true"/>
									</dsp:include>
								</div>
		
								<div id="addToCartErrorOccured" style="display:none;" class="error-msg alert alert-danger">
									<cp:messageLookup key="mtrListAddToCartErr"/>
								</div>
								<div id="updateItemErrorOccured" style="display:none;" class="error-msg alert alert-danger">
									<cp:messageLookup key="mtrListUpdateItemErr"/>
								</div>
								
								<div class="container preload-cover" id="spinner-on-start">
									<div class="loading">
										<i class="fa fa-spinner fa-spin"></i>
									</div>
								</div>
		
									<%--<dsp:form action="${originatingRequest.contextPath}/account/material-detail.jsp?giftlistId=${giftlistId}" method="post" id="material-details-form" formid="material-details-form">--%>
								<dsp:form action="" method="post" id="material-details-form" formid="material-details-form">
		
									<div id="material-items-list" class="material-items-list preload-cover mobile"></div>
		
									<dsp:input type="hidden" bean="GiftlistFormHandler.removeGiftitemIds" value="" id="removeList"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.giftlistId" value="${giftlistId}"/>
									<%--<dsp:input type="hidden" bean="GiftlistFormHandler.updateGiftlistItemsSuccessURL" value="${originatingRequest.contextPath}/account/material-detail.jsp?giftlistId=${giftlistId}"/>--%>
									<%--<dsp:input type="hidden" bean="GiftlistFormHandler.updateGiftlistItemsErrorURL" value="${originatingRequest.contextPath}/account/material-detail.jsp?giftlistId=${giftlistId}"/>--%>
									<dsp:input type="hidden" bean="GiftlistFormHandler.updateGiftlistItems" value="true" priority="-10"/>
								</dsp:form>
								<div class="ml-buttons">
									<button id="updateAllItemQtyBottom" class="btn btn-primary xs-block"
										role="button" data-class="warn" data-placement="top"
										data-trigger="focus">
										<fmt:message key="account.material.update_all_item_qty" />
									</button>
								</div>
								<div class="col-xs-12 text-center">
									<nav id="pagination"></nav>
								</div>
									
								<dsp:form method="post" id="material-details-add" formid="material-details-add">
									<%-- <dsp:input type="hidden" bean="CartModifierFormHandler.giftlistId" value="giftlistId"/> --%>
									<dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsListGL"/>
									<dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyListGL"/>
									<dsp:input type="hidden" bean="CartModifierFormHandler.giftIds" value="true" id="giftIds"/>
									<dsp:input type="hidden" bean="CartModifierFormHandler.addItemToOrder" value="true" priority="-10"/>
								</dsp:form>
		
								<dsp:form action="${originatingRequest.contextPath}/account/material-detail.jsp?giftlistId=${giftlistId}" method="post"
										  id="material-details-check-price" formid="material-details-check-price">
									<dsp:input type="hidden" bean="GiftlistFormHandler.itemIdsList" value="" id="paSkuIdsList" name="itemIdsList"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.quantities" value="" id="paQtyList" name="quantities"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.addItemToPASuccessURL"
											   value="/account/material-detail.jsp?showPA=true&giftlistId=${giftlistId}" name="addItemToPASuccessURL"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.addItemToPAErrorURL"
											   value="/account/material-detail.jsp?giftlistId=${giftlistId}" name="addItemToPAErrorURL"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.addItemToPA" value="true" priority="-10"/>
								</dsp:form>
								
								<dsp:form action="#" method="post" id="update-ml-item-qty" formid="update-ml-item-qty">
									<dsp:input id="update-ml-id" bean="MaterialListHandler.giftlistId" type="hidden" value="${giftlistId}" />
									<dsp:input id="input-ml-item-qty" bean="MaterialListHandler.newItemQty" type="hidden" value="" />
									<dsp:input id="input-ml-item-id" bean="MaterialListHandler.itemToUpdate" type="hidden" value="" />
									<dsp:input type="hidden" bean="MaterialListHandler.updateGiftlist" value="true" priority="-10" />
								</dsp:form>
								<%-- SM-358 Update All button implemented --%>
								<dsp:form action="#" method="post" id="update-all-ml-items-qty" formid="update-all-ml-items-qty">
									<dsp:input id="update-all-ml-id" bean="MaterialListHandler.giftlistId" type="hidden" value="${giftlistId}" />
									<dsp:input id="update-all-ml-item-qty" bean="MaterialListHandler.itemsToUpdate" type="hidden" converter="map" value="${itemsToUpdate}" />
									<dsp:input type="hidden" bean="MaterialListHandler.updateGiftlistItems" value="true" priority="-10" />
								</dsp:form>
								<%-- SM-358 Update All button implemented --%>
							</section>
						</div>
		
					</dsp:oparam>
				</dsp:droplet>

				<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-list-found.jsp">
					<dsp:param name="messageId" value="ml-added" />
					<dsp:param name="divId" value="_materialListAdded" />
					<dsp:param name="isReset" value="false" />
				</dsp:include>
			</div>
		</section>
	</div>	
	
	<script type="text/javascript" nonce="${requestScope.nonce}">
		$(document).ready(function () {
			$('.preload-cover').addClass('loader-active');
            $('.preload-cover .loading').show();
			$('#itemsAddtoMl').on('hide.bs.modal', function(e) {
                $.ajax({
                    type: 'POST',
                	url: "/xhr/invalid-material-list-items-removed.jsp",
                	dataType: 'json',
                    success: function(result){
                        console.log("result : "+ result);
                    },
					error: function(result){
						console.log("error");
					}
                });

			});
			
			$("#material-items-list").load("/account/gadgets/material-detail-display.jsp?page=1&giftlistId=${giftlistId}&showPA=${showPA}", function () {
				
                $('#spinner-on-start').addClass('loader-active');
                $('#spinner-on-start .loading').show();
                onMaterialDetailDisplayLoaded();
				eventsMaterialDetailDisplay();
			    var totalCount = $("#total_desired_qty").val();
				$("#itemsNumber").html(totalCount);
				jQuery('.scrollbar-inner').scrollbar();
				initializeCheckboxes();
				var showImportModal = '${vsg_utils:escapeJS(importModalError)}';
				if (showImportModal == 'true') {
					showImportMaterialListModalPopup('${giftlistId}');
				}
				if("${ca}" == "true"){
					$("#material-items-list .checkbox").addClass("checked");
					$("#material-items-list .checkbox-custom").addClass("checked");
					$("#material-items-list input").attr("checked","checked");
					checkPriceSelected();
				}
                   $('#spinner-on-start').removeClass('loader-active');
                   $('#spinner-on-start .loading').hide();
                   $('.preload-cover').removeClass('loader-active');
                   $('.preload-cover .loading').hide();
                   enableUpdateAllButton();
			});

            if ("${materialListSuccessfullyUpdated}" === "true") {
            	setTimeout(function(){
            		$("#link-noResultsModal_materialListAdded").click();
                },3000);
            	/* $("#link-noResultsModal_materialListAdded").click(); */
                <dsp:setvalue bean="CPSSessionBean.materialListSuccessfullyUpdated" value="false"/>
            }
		});
		
		function gup(name, url) {
			if (!url) url = location.href;
			name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
			var regexS = "[\\?&]" + name + "=([^&#]*)";
			var regex = new RegExp(regexS);
			var results = regex.exec(url);
			return results == null ? null : results[1];
		}
		$(window).load(function () {
			if (gup("im", location.href) == "true" && ("${hasMaterialDuplicates}" == "true" || "${hasInvalidItems}" == "true")) {
				$('#itemsAddtoMl').modal('show');
			}
		});
		
		var currentPage = 1;

		function changePage(page) {
			numPerPage = $("#numPerPage").val();
			currentPage = page;
			$("#material-items-list").load(
			"/account/gadgets/material-detail-display.jsp?modal=true&giftlistId=${giftlistId}" +
					"&sort=" + sortDetails + 
					"&accend=" + accendGlobal +
					"&numPerPage=" + numPerPage + 
					"&page=" + currentPage +
					"&showPA=" + showPA + 
					"&searchTerm="+encodeURIComponent(detailsSearchTerm), function () {
			    onMaterialDetailDisplayLoaded();
				eventsMaterialDetailDisplay();
				enableUpdateAllButton();
			});
			loadPagination();
		}
	</script>
</dsp:page>