<dsp:page>
	<dsp:importbean bean="/cps/util/CPSSessionBean" />
	<dsp:getvalueof var="page" param="page" />
	<dsp:getvalueof var="accend" param="accend" />
	<dsp:getvalueof var="sort" param="sort" />
	<dsp:getvalueof var="order" value="-" />
	<c:if test="${accend eq true}">
		<dsp:getvalueof var="order" value="+" />
	</c:if>
	<table class="table table-striped table-mobile">
		<thead>
			<tr>
				<th class="text-center" data-event-click-id="eventSortInvoiceNumber"  id="sort-id">
					<a href="#">
						<strong>Invoice #</strong>
						<i class="fa fa-sort"></i>
					</a>
				</th>
				<th class="text-center" data-event-click-id="eventSortOrderNumber"  id="sort-order">
					<a href="#">
						<strong>Sales Order #</strong>
						<i class="fa fa-sort"></i>
					</a>
				</th>
				<th data-event-click-id="eventSortPONumber"  id="sort-po">
					<a href="#">
						<strong>PO #</strong>
						<i class="fa fa-sort"></i>
					</a>
				</th>
				<th class="text-center" data-event-click-id="eventSortDateInvoice"  id="sort-date">
					<a href="#">
						<strong>Invoice date</strong>
						<i class="fa fa-sort"></i>
					</a>
				</th>
				<th class="text-center">Amount</th>
				<th class="text-center">Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" bean="CPSSessionBean.invoices" />
				<dsp:param name="elementName" value="invoice" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="count" param="count" />
					<dsp:getvalueof var="invId" param="invoice.id" />
					<dsp:getvalueof var="invNumber" param="invoice.invoiceNumber" />
					<dsp:getvalueof var="invOrderNumber" param="invoice.orderNumber" />
					<dsp:getvalueof var="invPoNumber" param="invoice.PONumber" />
					<dsp:getvalueof var="invDate" param="invoice.invoiceDate" />
					<dsp:getvalueof var="invAmount" param="invoice.invoiceAmount" />
					<dsp:getvalueof var="invLink" param="invoice.url" />
					<tr>
						<td class="text-center text-left-xs">
							<span class="caption">Invoice #</span>
							${invNumber}
						</td>
						<td class="text-center text-left-xs">
							<span class="caption">Sales Order #</span>
							${invOrderNumber}
						</td>
						<td>
							<span class="caption">PO #</span>
							${invPoNumber}
						</td>
						<td class="text-center text-left-xs">
							<span class="caption">Invoice date</span>
							<fmt:formatDate pattern="MM/dd/yyyy" value="${invDate}" />
						</td>
						<td class="text-center text-left-xs">
							<span class="caption">Amount</span>
							<dsp:valueof value="${invAmount}" converter="currencyConversion" />
						</td>
						<td class="text-center">

							<span id="status${invNumber}">
								<a class="table-icon-link" href="#" data-event-click-id="eventRequestInvoiceStatus" data-invoice-id="${invNumber}" >
									<span>Get Status</span>
								</a>
							</span>
						</td>
						<td class="text-center">

							<a href="${invLink}" target="_blank">
								<i class="icon icon-paper-page"></i>
								<span>View Details</span>
							</a>
						</td>
					</tr>
				</dsp:oparam>
				<dsp:oparam name="outputEnd">
					<dsp:getvalueof var="invoicesTotal" bean="CPSSessionBean.invoicesTotal" />
					<input type="hidden" value="${invoicesTotal}" id="total_count" />
					<input type="hidden" value="" id="page" />
				</dsp:oparam>
				<dsp:oparam name="empty">
					<input type="hidden" value="0" id="total_count" />
					<input type="hidden" value="1" id="page" />
					<dsp:include page="/includes/gadgets/info-message.jsp">
						<dsp:param name="key" value="err_invoices_not_found" />
					</dsp:include>

				</dsp:oparam>
			</dsp:droplet>
		</tbody>
	</table>
</dsp:page>
