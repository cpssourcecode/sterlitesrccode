<dsp:page>
	<dsp:getvalueof var="userId" param="id" />
	<dsp:getvalueof var="userId" value="${vsg_utils:escapeHtml(userId)}" />
	<dsp:getvalueof var="rs" param="rs" />
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/cps/droplet/RoleLookupDroplet" />
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler" />
	<dsp:getvalueof var="parentOrg" bean="Profile.parentOrganization.id" />

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>

	<cp:pageContainer page="account">
		<input type="hidden" id="currentUserId" value="${userId}" />
		<input type="hidden" id="company" value="${parentOrg}" />
		<input type="hidden" id="billingAccounts" value="" />
		<input type="hidden" id="page-type" value="edit" />
		<dsp:droplet name="/atg/targeting/RepositoryLookup">
			<dsp:param name="repository" bean="/atg/userprofiling/ProfileAdapterRepository" />
			<dsp:param name="itemDescriptor" value="user" />
			<dsp:param name="id" value="${userId}" />
			<dsp:param name="elementName" value="p" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="userFirstName" param="p.firstName" />
				<dsp:getvalueof var="userLastName" param="p.lastName" />
				<dsp:getvalueof var="userEmail" param="p.email" />
				<dsp:getvalueof var="phone" param="p.homeAddress.phoneNumber" />
				<dsp:getvalueof var="status" param="p.isActive" />
				<dsp:getvalueof var="spendingLimit" param="p.orderPriceLimit" />
				<dsp:getvalueof var="frequency" param="p.spFrequency" />
				<main id="body">
				<div class="container ${csrClass}">
					<ol class="breadcrumb">
						<li>
							<a href="/account/account-landing.jsp">My Account</a>
						</li>
						<li>
							<a href="/account/manage-users.jsp">Manage Users</a>
						</li>
						<li class="active">Edit User</li>
					</ol>
					<h1 class="mb20">Edit User</h1>
					 <div class="editUserError" id="editUserError">
            		 </div>
					<dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="MultiUserAddFormHandler.formExceptions" />
					<c:if test="${not empty formExceptions}">
						<c:forEach var="formException" items="${formExceptions}">
							<dsp:param name="formException" value="${formException}" />
							<dsp:getvalueof var="errorCode" param="formException.property" />
							<script type="text/javascript" nonce="${requestScope.nonce}">
                                    jQuery(document).ready(function () {
                                        loadErrors('${errorCode}');
                                    });

                                    function loadErrors(errorCode) {
                                        $("#" + errorCode).parent().addClass("has-error");
                                    }
                                </script>
							<c:if test="${formException.message != ''}">
								<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">
											<span class="glyphicon glyphicon-remove-circle"></span>
										</span>
									</button>
									<dsp:valueof param="formException.message" valueishtml="true" />
								</div>
							</c:if>
						</c:forEach>
					</c:if>
					<div class="manage-users">
						<dsp:form method="post" action="/account/gadgets/manage-edit-user.jsp" id="edit-user" formid="edit-user">
							<div class="well">
								<h4 class="text-nocase">Status</h4>
								<div class="radio checked">
									<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
										<dsp:input bean="MultiUserAddFormHandler.status" checked="${status}" name="radioEx1" type="radio" value="active">
										</dsp:input>
										Active
									</label>
								</div>
								<div class="radio">
									<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
										<dsp:input bean="MultiUserAddFormHandler.status" name="radioEx1" checked="${!status}" type="radio" value="deactive">
										</dsp:input>
										Deactivated
									</label>
								</div>
								<p>
									To Deactivate a User, select "Deactivated", then click "Save" at the bottom of this form. A Deactivated User will be
									<strong>UNABLE</strong>
									to log in to his or her account.
								</p>
								<a href="javascript:void(0);" data-event-click-id="eventResendInvite" >Resend Invite to this User.</a>
							</div>
							<h4 class="text-nocase">User Information</h4>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-horizontal">
									<div class="row row-narrow form-group">
										<div class="col-md-6 col-xs-6">
											<dsp:input bean="MultiUserAddFormHandler.firstName" type="text" iclass="form-control form-error" maxlength="40" id="first" value="${userFirstName}">
												<dsp:tagAttribute name="placeholder" value="*First Name" />
											</dsp:input>
										</div>
										<div class="col-md-6 col-xs-6">
											<dsp:input bean="MultiUserAddFormHandler.lastName" type="text" iclass="form-control" maxlength="40" id="last" value="${userLastName}">
												<dsp:tagAttribute name="placeholder" value="*Last Name" />
											</dsp:input>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-12 col-xs-12">
											<dsp:input bean="MultiUserAddFormHandler.email" type="text" iclass="form-control" maxlength="50" id="email" value="${userEmail}">
												<dsp:tagAttribute name="placeholder" value="*Email Address (will be User's login ID)" />
											</dsp:input>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-12 col-xs-12">
											<dsp:input bean="MultiUserAddFormHandler.phone" type="text" iclass="form-control phone" maxlength="20" id="phone" value="${phone}">
												<dsp:tagAttribute name="placeholder" value="Phone" />
											</dsp:input>
										</div>
									</div>
								</div>
							</div>

							<hr>

							<h4 class="text-nocase">*Role(s)</h4>

							<div class="checkbox" id="myCheckbox">
								<label>
									<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
										<dsp:param name="userId" param="id" />
										<dsp:param name="role" value="custAccAdmin" />
										<dsp:oparam name="true">
											<dsp:input id="accAdminRole" bean="MultiUserAddFormHandler.role.custAccAdmin" type="checkbox"  checked="true" >
												<dsp:tagAttribute name="data-event-change-id" value="eventUpdateCheckbox"/>
											</dsp:input>
										</dsp:oparam>
										<dsp:oparam name="false">
											<dsp:input id="accAdminRole" bean="MultiUserAddFormHandler.role.custAccAdmin" type="checkbox" >
												<dsp:tagAttribute name="data-event-change-id" value="eventUpdateCheckbox"/>
											</dsp:input>
										</dsp:oparam>
									</dsp:droplet>
									<span class="checkbox-label">Admin</span>
								</label>
							</div>
							<div class="checkbox" id="myCheckbox">
								<label>
									<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
										<dsp:param name="userId" param="id" />
										<dsp:param name="role" value="approver" />
										<dsp:oparam name="true">
											<dsp:input bean="MultiUserAddFormHandler.role.approver" type="checkbox" checked="true" />
										</dsp:oparam>
										<dsp:oparam name="false">
											<dsp:input bean="MultiUserAddFormHandler.role.approver" type="checkbox" />
										</dsp:oparam>
									</dsp:droplet>
									<span class="checkbox-label">Approver</span>
								</label>
							</div>
							<div class="checkbox" id="myCheckbox">
								<label>
									<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
										<dsp:param name="userId" param="id" />
										<dsp:param name="role" value="finance" />
										<dsp:oparam name="true">
											<dsp:input bean="MultiUserAddFormHandler.role.finance" type="checkbox" checked="true" />
										</dsp:oparam>
										<dsp:oparam name="false">
											<dsp:input bean="MultiUserAddFormHandler.role.finance" type="checkbox" />
										</dsp:oparam>
									</dsp:droplet>
									<span class="checkbox-label">Finance</span>
								</label>
							</div>
							<div class="checkbox" id="buyerRoleDiv">
								<label>
									<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
										<dsp:param name="userId" param="id" />
										<dsp:param name="role" value="buyer" />
										<dsp:oparam name="true">
											<dsp:input bean="MultiUserAddFormHandler.role.buyer" type="checkbox" checked="true" id="buyer-role">
												<dsp:tagAttribute name="data-event-change-id" value="eventDisplayOptions"/>
											</dsp:input>
										</dsp:oparam>
										<dsp:oparam name="false">
											<dsp:input bean="MultiUserAddFormHandler.role.buyer" type="checkbox" id="buyer-role" >
												<dsp:tagAttribute name="data-event-change-id" value="eventDisplayOptions"/>
											</dsp:input>
										</dsp:oparam>
									</dsp:droplet>
									<span class="checkbox-label">Buyer</span>
								</label>
							</div>
							<!--  SM-520 New User Role -->
							<div class="checkbox" id="myCheckbox">
								<label>
									<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
										<dsp:param name="userId" param="id" />
										<dsp:param name="role" value="appraiser" />
										<dsp:oparam name="true">
											<dsp:input bean="MultiUserAddFormHandler.role.appraiser" type="checkbox" checked="true" />
										</dsp:oparam>
										<dsp:oparam name="false">
											<dsp:input bean="MultiUserAddFormHandler.role.appraiser" type="checkbox" />
										</dsp:oparam>
									</dsp:droplet>
									<span class="checkbox-label">Appraiser</span>
								</label>
							</div>
							<hr>
							<div id="buyer-options" style="display: none;">
								<h4 class="text-nocase">Spending Limit and Frequency</h4>
								<p>
									If a spending limit has been entered for this User, selected Approvers will receive notifications. However, an Approver needs to be set for <strong>ALL</strong> buyers
								</p>
								<div class="row">
									<div class="col-lg-6 col-md-6 form-horizontal">
										<div class="form-group">
											<div class="col-xs-6">
												<div class="input-group">
													<div class="input-group-addon">$</div>
													<fmt:formatNumber var="userOrderPriceLimit" pattern="#####" value="${spendingLimit}" />
													<dsp:input bean="MultiUserAddFormHandler.spendingLimit" type="text" id="spendingLimit" iclass="form-control" maxlength="5" value="${userOrderPriceLimit}">
														<dsp:tagAttribute name="placeholder" value="" />
													</dsp:input>
												</div>
											</div>
										</div>
										<div class="radio checked">
											<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel20">
												<dsp:input checked="${frequency == 'ORDER'}" bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="ORDER" />
												Per Order
											</label>
										</div>
										<div class="radio">
											<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel21">
												<dsp:input checked="${frequency == 'DAY'}" bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="DAY" />
												Per Day
											</label>
										</div>
										<div class="radio">
											<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel22">
												<dsp:input checked="${frequency == 'WEEK'}" bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="WEEK" />
												Per Week
											</label>
										</div>
										<div class="radio">
											<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel23">
												<dsp:input checked="${frequency == 'MONTH'}" bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="MONTH" />
												Per Month
											</label>
										</div>
									</div>
								</div>
								<div id="user-approvers" style="display: block;">
									<hr>
									<h4 class="text-nocase">Approvers for this Buyer</h4>
									<p>If a spending limit has been entered for this User, you must choose at least one Approver. Selected Approvers will receive notifications.</p>
									<dsp:include page="/account/gadgets/manage-edit-user-approvers.jsp">
										<dsp:param name="userId" param="id" />
									</dsp:include>
								</div>
								<hr>
							</div>

							<dsp:include page="org-list.jsp">
								<dsp:param name="defaultOrgId" value="${parentOrg}"/>
								<dsp:param name="pageType" value="edit"/>
							</dsp:include>

							<span id="manageUsersShipping"></span>

							<dsp:input type="hidden" bean="MultiUserAddFormHandler.approvers" id="approvers" name="approvers" value="" />
							<dsp:input type="hidden" bean="MultiUserAddFormHandler.billingAccountIds" id="billingAccountIds" name="billingAccountIds" value="" />
							<dsp:input type="hidden" bean="MultiUserAddFormHandler.editId" value="${userId}" id="editId" name="editId" />
							<dsp:input type="hidden" bean="MultiUserAddFormHandler.editSuccessURL" value="/account/manage-users.jsp?e=s" id="editSuccessURL" name="editSuccessURL" />
							<dsp:input type="hidden" bean="MultiUserAddFormHandler.editErrorURL" value="/account/gadgets/manage-edit-user.jsp?id=${userId}" id="editErrorURL" name="editErrorURL" />
							<dsp:input type="hidden" bean="MultiUserAddFormHandler.edit" value="true" priority="-10"/>
						</dsp:form>
						<p>
							<a href="javascript:void(0);" data-event-click-id="eventUpdateUser"  class="btn btn-primary">Save User</a>
							<a href="/account/manage-users.jsp" class="btn btn-info">Cancel</a>
						</p>

					</div>
				</div>
				</main>
				<dsp:form method="post" action="/account/gadgets/manage-edit-user.jsp" id="resend-invite" formid="resend-invite">
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.email" value="${userEmail}" />
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.resendInviteSuccessURL" value="/account/gadgets/manage-edit-user.jsp?id=${userId}&rs=s" />
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.resendInviteErrorURL" value="/account/gadgets/manage-edit-user.jsp?id=${userId}" />
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.resendInvite" value="true" />
				</dsp:form>
				<dsp:include page="/global/modals/modal-add-ship-to.jsp">
					<dsp:param name="userId" value="${userId}" />
					<dsp:param name="companyId" value="${parentOrg}" />
				</dsp:include>
				<dsp:include page="/global/modals/modal-resend-success.jsp" />
				<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
				<script src="${staticContentPrefix}/js/manage-users-shipping${build_version}.js"></script>
				<script type="text/javascript" nonce="${requestScope.nonce}">
                    $(".phone").mask("(999) 999-9999");

                    $(document).ready(function () {
                        $("#shippingList").load("/global/modals/gadgets/add-ship-to-list.jsp?userId=" +
                                encodeURIComponent(${userId}) + "&companyId=" + encodeURIComponent(${parentOrg}), function () {
                            eventsAddShipToList();
                        });
                        if (document.getElementById('buyer-role').checked) {
                            $("#buyer-options").show();
                        }
                        if ($("#accAdminRole").is(':checked')) {
							$("#buyer-role").prop('checked', true);
							$("#buyer-role").prop('disabled', true);
							$("#buyerRoleDiv").addClass('disabled');
							$("#buyerRoleDiv").find('.checkbox-label').addClass('text-muted');
							$("#buyer-options").show();
                        }
                        console.log("Resend - ${rs}");
                        if ('${rs}' == 's') {
                            $("#link-resendInviteSuccess").click();
                        }
                    });
                    function updateUser() {
                    	$('.alert-danger').remove();
                        var a = gatherListChecked();
                        $("#approvers").val(a);
						var billingAccountIds = gatherListCheckedByName('billingIds');
						$("#billingAccountIds").val(billingAccountIds);
                        $('#edit-user').submit();
                    }
                    function displayOptions(v) {
                        if (v) {
                            $("#buyer-options").show();
                        } else {
                            $("#buyer-options").hide();
                        }
                    }
                    function resendInvite() {
                        $('#resend-invite').submit();
                    }

					function updateCheckbox(v) {
						if ($("#accAdminRole").is(':checked')) {
//							console.log("#accAdminRole:checked");
							$("#buyer-role").prop('checked', true);
							$("#buyer-role").prop('disabled', true);
							$("#buyerRoleDiv").addClass('disabled');
							$("#buyerRoleDiv").find('.checkbox-label').addClass('text-muted');
							$("#buyer-options").show();
						} else {
							$("#buyer-role").prop('disabled', false);
                            $("#buyerRoleDiv").removeClass('disabled');
                            $("#buyerRoleDiv").find('.checkbox-label').removeClass('text-muted');
                        }
					}
                </script>
			</dsp:oparam>
		</dsp:droplet>
	</cp:pageContainer>
</dsp:page>