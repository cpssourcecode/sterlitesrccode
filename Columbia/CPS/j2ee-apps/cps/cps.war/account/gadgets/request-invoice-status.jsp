<dsp:page>
	<dsp:importbean bean="/cps/droplet/RequestInvoiceStatus"/>

	<dsp:getvalueof var="invoiceId" param="invoiceId"/>


	<dsp:droplet name="RequestInvoiceStatus">
		<dsp:param name="invoiceId" value="${invoiceId}"/>
		<dsp:oparam name="output">{"status":"<dsp:valueof param="status"/>"}</dsp:oparam>
	</dsp:droplet>

</dsp:page>
