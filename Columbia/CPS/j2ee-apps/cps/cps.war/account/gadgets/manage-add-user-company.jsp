<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:tomap var="organization" bean="Profile.parentOrganization"/>
	
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<hr>
		<h4>Assign User to a Company</h4>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-horizontal">
		<div class="row form-group">
			<div class="col-md-12 col-xs-12">
				<dsp:select bean="MultiUserAddFormHandler.company" name="" id="company" iclass="form-control">
					<dsp:option value="${organization.id}">${organization.name}</dsp:option>
				</dsp:select>
			</div>
		</div>
	</div>
</dsp:page>