<dsp:page>

	<dsp:getvalueof var="page" param="page"/>
	<dsp:getvalueof var="total" param="total"/>
	<dsp:getvalueof var="numPerPage" param="numPerPage"/>
	<dsp:getvalueof var="numPages" param="numPages"/>
	
	<!-- Only show pagination if more than 1 page -->
	<c:if test="${numPages >= 2}">
		<ul class="pagination">
			<c:if test="${page > 1}">
				<li class="prev">
					<a href="javascript:void(0);" id="prevPage" data-event-click-id="eventChangePage" data-page="${vsg_utils:escapeJS(page-1)}"  aria-label="Previous">
						<span class="fa fa-angle-left"></span>
					</a>
				</li>
			</c:if>
			
			<c:choose>
				<c:when test="${numPages <=5}">
					<dsp:droplet name="/atg/dynamo/droplet/For">
						<dsp:param name="howMany" value="${total}"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<c:if test="${count <= numPages}">
								<c:choose>
									<c:when test="${page == count}">
										<!-- This would be used to modify style for active page, no current style change -->
										<li class="active"><a href="javascript:void(0);" id="pageNum" data-event-click-id="eventChangePage" data-page="${count}" ><dsp:valueof param="count"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="javascript:void(0);" id="pageNum" data-event-click-id="eventChangePage" data-page="${count}" ><dsp:valueof param="count"/></a></li>
									</c:otherwise>
								</c:choose>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</c:when>
			
				<c:otherwise>
					<c:if test="${(page > 4)}">
						<li><a href="javascript:void(0);" id="pageNum" data-event-click-id="eventChangePage" data-page="1" >1</a></li>
						<li><span>...</span></li>
					</c:if>
					<c:if test = "${(page <= 4)}">
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<c:choose>
									<c:when test="${page == count}">
										<li class="active"><a href="javascript:void(0);" id="pageNum" data-event-click-id="eventChangePage" data-page="${count}" ><dsp:valueof value="${count}"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="javascript:void(0);" id="pageNum" data-event-click-id="eventChangePage" data-page="${count}" ><dsp:valueof value="${count}"/></a></li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page > 4) && (page < (numPages - 3))}">
						<li><a href="javascript:void(0);" data-event-click-id="eventChangePage" data-page="${vsg_utils:escapeJS(page-1)}"  id="pageNum"><dsp:valueof value="${vsg_utils:escapeHtml(page-1)}"/></a></li>
						<!-- Active element --><li class="active"><a href="javascript:void(0);" data-event-click-id="eventChangePage" data-page="${vsg_utils:escapeJS(page)}"  id="pageNum"><dsp:valueof value="${vsg_utils:escapeHtml(page)}"/></a></li>
						<li><a href="javascript:void(0);" data-event-click-id="eventChangePage" data-page="${vsg_utils:escapeJS(page+1)}"  id="pageNum"><dsp:valueof value="${vsg_utils:escapeHtml(page+1)}"/></a></li>
					</c:if>
					<c:if test = "${(page >= (numPages - 3))}">
						<dsp:getvalueof var="start" value="${numPages-5}"/>
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<dsp:getvalueof var="start" value="${start+1}"/>
								<c:choose>
									<c:when test="${page == start}">
										<!-- This would be used to modify style for active page, no current style change -->
										<li class="active"><a href="javascript:void(0);" data-event-click-id="eventChangePage" data-page="${start}"  id="pageNum"><dsp:valueof value="${start}"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="javascript:void(0);" data-event-click-id="eventChangePage" data-page="${start}"  id="pageNum"><dsp:valueof value="${start}"/></a></li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page < (numPages - 3))}">
						<li><span>...</span></li>
						<li><a href="javascript:void(0);" data-event-click-id="eventChangePage" data-page="${numPages}"  id="pageNum"><dsp:valueof value="${numPages}"/></a></li>
					</c:if>
				</c:otherwise>
			</c:choose>
			
			<c:if test="${page < numPages}">
				<li class="next">
					<a href="javascript:void(0);" data-event-click-id="eventChangePage" data-page="${vsg_utils:escapeJS(page+1)}"  id="pageNum" aria-label="Next">
						<span class="fa fa-angle-right"></span>
					</a>
				</li>
			</c:if>
		</ul>
	</c:if>

</dsp:page>