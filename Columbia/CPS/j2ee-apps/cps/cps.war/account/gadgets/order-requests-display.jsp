<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

	<dsp:importbean bean="/cps/droplet/OrderRequestsDroplet"/>
	<dsp:importbean bean="/cps/droplet/UserLookupDroplet"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>

	<dsp:getvalueof var="sort" param="sort"/>
	<dsp:getvalueof var="accend" param="accend"/>
	<dsp:getvalueof var="page" param="page"/>
	<c:if test="${empty page}">
		<dsp:getvalueof var="page" value="${1}"/>
	</c:if>
	<input type="hidden" value="${page}" id="page"/>

	<dsp:getvalueof var="order" value="-"/>
	<c:if test="${accend eq true}">
		<dsp:getvalueof var="order" value="+"/>
	</c:if>

	<table class="table table-striped table-mobile">
		<thead>
			<tr>
				<th data-event-click-id="eventSortOrderIdOrderList"  id="sort-id"><strong>Web Order #</strong> <i class="fa fa-sort"></i></th>
				<th data-event-click-id="eventSortOrderRequestor"  id="sort-requestor"><strong>Requestor</strong> <i class="fa fa-sort"></i></th>
				<th data-event-click-id="eventSortOrderDateOrderList"  id="sort-date"><strong>Request Date</strong> <i class="fa fa-sort"></i></th>
				<th><strong>Status</strong></th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<dsp:droplet name="OrderRequestsDroplet">
			<dsp:param name="profile" bean="Profile"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="orders" param="orders"/>

				<dsp:getvalueof var="max" value="${page*5}"/>
				<dsp:getvalueof var="min" value="${max-5}"/>

				<tbody>
				    <dsp:droplet name="ForEach">
						<dsp:param name="array" param="orders"/>
						<dsp:param name="sortProperties" value="${order}${sort}"/>
						<dsp:param name="elementName" value="order"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<dsp:getvalueof var="orderId" param="order.id"/>
							<dsp:getvalueof var="orderState" param="order.stateAsString"/>
							<c:if test="${count > min && count <= max}">
								<tr>
									<td>
										<span class="caption">Web Order #</span>
										<dsp:valueof param="order.webOrderId"/>
									</td>
									<td>
										<dsp:droplet name="UserLookupDroplet">
											<dsp:param name="id" param="order.profileId"/>
											<dsp:param name="elementName" value="user"/>
											<dsp:oparam name="output">
												<span class="caption">Requester</span>
												<dsp:valueof param="user.firstName"/>&nbsp;<dsp:valueof param="user.lastName"/>
											</dsp:oparam>
										</dsp:droplet>
									</td>
									<td>
										<span class="caption">Request Date</span>
										<dsp:getvalueof var="submittedDate" param="order.submittedDate"/>
										<fmt:formatDate pattern="MM/dd/yyyy" value="${submittedDate}"/>
									</td>
									<td>
										<c:choose>
											<c:when test="${orderState eq 'PENDING_APPROVAL'}">
												<span class="caption">Status</span>
												<span class="label label-warning">Pending</span>
											</c:when>
											<c:when test="${orderState eq 'FAILED_APPROVAL'}">
												<span class="caption">Status</span>
												<span class="label label-danger">Rejected</span>
											</c:when>
											<c:otherwise>
											</c:otherwise>
										</c:choose>
									</td>
									<c:choose>
										<c:when test="${orderState eq 'FAILED_APPROVAL'}">
											<td class="text-center">
												<div class="dropdown">
													<a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" class="btn btn-link xs-block">
														<span>Actions<i class="fa fa-angle-down ml5"></i></span>
													</a>
													<ul class="dropdown-menu dropdown-table-actions" aria-labelledby="dLabel">
														<li>
															<a href="" data-toggle="modal" data-target="#modalResubmitOrder${orderId}">Resubmit</a>
														</li>
														<li>
															<a href="javascript:void(0);" data-event-click-id="eventShowViewOrderModalPopup" data-order-id="${orderId}" >
																View
															</a>
														</li>
														<li>
															<a href="" data-toggle="modal" data-target="#modalEditOrder${orderId}">Edit</a>
														</li>
														<li>
															<a href="" data-toggle="modal" data-target="#modalDeleteOrder${orderId}">Delete</a>
														</li>
													</ul>
												</div>
											</td>
										</c:when>
										<c:otherwise>
											<td></td>
										</c:otherwise>
									</c:choose>
									<c:if test="${orderState eq 'FAILED_APPROVAL'}">
										<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-resubmit-order.jsp?orderId=${orderId}"/>
										<%-- <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-view-order.jsp?orderId=${orderId}"/> --%>
										<div id="view_order_div"></div>
										<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-edit-order.jsp?orderId=${orderId}"/>
										<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-delete-order.jsp?orderId=${orderId}"/>
									</c:if>
								</tr>
								
							</c:if>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
							<input type="hidden" value="${count}" id="total_count"/>
						</dsp:oparam>
				    </dsp:droplet>
				</tbody>

			</dsp:oparam>
			<dsp:oparam name="empty">
				<input type="hidden" value="0" id="total_count"/>
			</dsp:oparam>
		</dsp:droplet>

	</table>

</dsp:page>
