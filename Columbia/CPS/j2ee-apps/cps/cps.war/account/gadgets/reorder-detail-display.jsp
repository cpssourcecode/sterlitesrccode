<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/cps/droplet/ReorderlistLookupDroplet"/>
	<dsp:importbean bean="/cps/droplet/SearchReorderDetailsDroplet"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	
	<dsp:getvalueof var="sort" param="sort"/>
	<dsp:getvalueof var="accend" param="accend"/>
	<dsp:getvalueof var="searchTerm" param="searchTerm"/>
	<dsp:getvalueof var="page" param="page"/>
	
	<dsp:getvalueof var="order" value="-"/>
	<c:if test="${accend eq true}">
		<dsp:getvalueof var="order" value="+"/>
	</c:if>
	<dsp:getvalueof var="giftlistId" param="giftlistId"/>
	<dsp:droplet name="SearchReorderDetailsDroplet">
		<dsp:param name="term" value="${searchTerm}"/>
		<dsp:param name="giftlistId" value="${giftlistId}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="giftItems" param="giftItems"/>
			<dsp:getvalueof var="emptyResult" param="emptyResult"/>
			<c:choose>
				<c:when test="${emptyResult}">
					<input type="hidden" value="0" id="total_count"/>
				</c:when>
				<c:otherwise>
					<dsp:getvalueof var="max" value="${page*20}"/>
					<dsp:getvalueof var="min" value="${max-20}"/>
					<dsp:droplet name="ForEach">
						<dsp:param name="sortProperties" value="${order}${sort}"/>
						<dsp:param name="array" value="${giftItems}"/>
						<dsp:param name="elementName" value="giftItem"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<dsp:getvalueof var="giftItemId" param="giftItem.id"/>
							<dsp:getvalueof var="prodId" param="giftItem.catalogRefId"/>
							<c:choose>
								<c:when test="${count>min && count<=max}">
									<dsp:droplet name="/atg/targeting/RepositoryLookup">
										<dsp:param name="id" value="${prodId}"/>
										<dsp:param name="repository" bean="/atg/commerce/catalog/ProductCatalog" />
										<dsp:param name="itemDescriptor" value="sku" />
										<dsp:param name="elementName" value="product"/>
										<dsp:oparam name="output">
											<dsp:getvalueof var="product" param="product"/>
											<dsp:getvalueof var="productPurchasable" value="${product.productPurchasable}"/>

											<div class="media">
												<div class="media-checkbox">
													<div class="checkbox">
														<label class="checkbox-custom checkbox-select-product" data-initialize="checkbox">
															<input class="sr-only" type="checkbox" value="" name="check" id="${giftItemId}"/>
															<span class="checkbox-label"><i class="text-muted"></i>
															</span>
														</label>
													</div>
												</div>

												<dsp:droplet name="/atg/repository/seo/CatalogItemLink">
													<dsp:param name="item" param="product"/>
													<dsp:oparam name="output">
														<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
													</dsp:oparam>
												</dsp:droplet>

												<dsp:a iclass="media-left" href="${productUrl}">
													<img src="${staticContentPrefix}/content_images/pdp3.jpg" alt="...">
												</dsp:a>

												<div class="media-body">
													<h4 class="media-heading">
														<dsp:a href="${productUrl}">
															<dsp:valueof param="product.displayName" valueishtml="true"/>
														</dsp:a>
													</h4>
													<ul class="list-inline">
														<li>
															<span class="text-muted">Item #: <dsp:valueof param="product.id"/></span>
														</li>
														<li>
															<span class="text-muted">Short Code: <dsp:valueof param="product.id"/></span>
														</li>
														<dsp:droplet name="/cps/droplet/AliasNumberLookup">
															<dsp:param name="skuId" param="product.id"/>
															<dsp:oparam name="output">
																<dsp:getvalueof var="prodAlias" param="alias"/>
                    											<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
																<li>
																	<span class="text-muted">Alias: <dsp:valueof value="${alias}"/></span>
																</li>
															</dsp:oparam>
														</dsp:droplet>
													</ul>
												</div>
												<div class="media-count">
													<ul class="vertical-alignment-fix list-inline">
														<li class="middle">
															<p class="text-muted">QTY</p>
														</li>
														<li>
															<div class="spinbox" data-initialize="spinbox" data-min="1" id="mySpinbox">
																<dsp:getvalueof var="quantityDesired" param="giftItem.quantityDesired"/>
																<input type="text" value="${quantityDesired}" name="${giftItemId}" id="${giftItemId}_qty" maxlength="3" class="form-control spinbox-input">
																<div class="spinbox-buttons btn-group btn-group-vertical">
																	<button type="button" class="btn btn-default spinbox-up">
																	<i class="fa fa-angle-up"></i>
																	<span class="sr-only">Increase</span>
																	</button>
																	<button type="button" class="btn btn-default spinbox-down">
																	<i class="fa fa-angle-down"></i>
																	<span class="sr-only">Decrease</span>
																	</button>
																</div>
															</div>
														</li>
													</ul>
													<c:choose>
														<c:when test="${productPurchasable}">
															<button class="btn btn-success btn-lg btn-block" data-event-click-id="eventAddItemToCart" data-gift-item-id="${giftItemId}" >Add to Cart</button>
														</c:when>
														<c:otherwise>
						                                    <span class="text-danger">
														        Unavailable for online purchase
							       							</span>
                                						</c:otherwise>
                                					</c:choose>
												</div>
											</div>
										</dsp:oparam>
									</dsp:droplet>
								</c:when>
								<c:otherwise>
									<dsp:getvalueof var="quantityDesired" param="giftItem.quantityDesired"/>
									<input type="hidden" value="${quantityDesired}" name="${giftItemId}" id="${giftItemId}_qty"/>
								</c:otherwise>
							</c:choose>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
							<input type="hidden" value="${count}" id="total_count"/>
						</dsp:oparam>
						<dsp:oparam name="empty">
							<input type="hidden" value="0" id="total_count"/>
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="prof-list-noItems"/>
							</dsp:include>
						</dsp:oparam>
					</dsp:droplet>
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>