<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler" />
	<dsp:getvalueof var="parentOrg" bean="Profile.parentOrganization.id" />

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>

	<cp:pageContainer page="account">
		<input type="hidden" id="currentUserId" value="newUser" />
		<input type="hidden" id="company" value="${parentOrg}" />
		<input type="hidden" id="billingAccounts" value="" />
		<input type="hidden" id="page-type" value="add" />
		<main id="body">
		<div class="container ${csrClass}">
			<ol class="breadcrumb">
				<li>
					<a href="/account/account-landing.jsp">My Account</a>
				</li>
				<li>
					<a href="/account/manage-users.jsp">Manage Users</a>
				</li>
				<li class="active">Add User</li>
			</ol>
			<h1 class="mb20">Add User</h1>
			<dsp:getvalueof var="formExceptions" vartype="java.lang.Object" bean="MultiUserAddFormHandler.formExceptions" />
			<c:if test="${not empty formExceptions}">
				<c:forEach var="formException" items="${formExceptions}">
					<dsp:param name="formException" value="${formException}" />
					<dsp:getvalueof var="errorCode" param="formException.property" />
					<script type="text/javascript" nonce="${requestScope.nonce}">
                        jQuery(document).ready(function () {
                            loadErrors('${errorCode}');
                            displayOptions($("#buyerRole").prop("checked"));
                        });

                        function loadErrors(errorCode) {
                            $("#" + errorCode).parent().addClass("has-error");
                        }
                    </script>
					<c:if test="${formException.message != ''}">
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">
									<span class="glyphicon glyphicon-remove-circle"></span>
								</span>
							</button>
							<dsp:valueof param="formException.message" valueishtml="true" />
						</div>
					</c:if>
				</c:forEach>
			</c:if>
			<div class="manage-users">
				<dsp:form method="post" action="/account/gadgets/manage-add-user.jsp" id="add-user" formid="add-user">
					<div class="well">
						<h4 class="text-nocase">Status</h4>
						<div class="radio checked">
							<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">							
								<dsp:input bean="MultiUserAddFormHandler.status" checked="${empty status || status eq 'active'}" name="radioEx1" type="radio" value="active">
								</dsp:input>
								Active
							</label>
						</div>
						<div class="radio">
							<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
								<dsp:input bean="MultiUserAddFormHandler.status" name="radioEx1" checked="${!(empty status || status eq 'active')}" type="radio" value="deactive">
								</dsp:input>
								Deactivated
							</label>
						</div>
					</div>
					<h4 class="text-nocase">User Information</h4>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 form-horizontal">
							<div class="row row-narrow form-group">
								<div class="col-md-6 col-xs-6">
									<dsp:input bean="MultiUserAddFormHandler.firstName" type="text" iclass="form-control form-error" maxlength="40" id="first">
										<dsp:tagAttribute name="placeholder" value="*First Name" />
									</dsp:input>
								</div>
								<div class="col-md-6 col-xs-6">
									<dsp:input bean="MultiUserAddFormHandler.lastName" type="text" iclass="form-control" maxlength="40" id="last">
										<dsp:tagAttribute name="placeholder" value="*Last Name" />
									</dsp:input>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-12 col-xs-12">
									<dsp:input bean="MultiUserAddFormHandler.email" type="text" iclass="form-control" maxlength="50" id="email">
										<dsp:tagAttribute name="placeholder" value="*Email Address (will be User's login ID)" />
									</dsp:input>
								</div>
							</div>
							<div class="row form-group">
								<div class="col-md-12 col-xs-12">
									<dsp:input bean="MultiUserAddFormHandler.phone" type="text" iclass="form-control phone" maxlength="20" id="phone">
										<dsp:tagAttribute name="placeholder" value="Phone" />
									</dsp:input>
								</div>
							</div>
						</div>
					</div>

					<hr>

					<h4 class="text-nocase">*Role(s) <i class="fa fa-info-circle" data-event-click-id="eventShowRoleTips"  title="Click for view role descriptions"></i></h4>
					<div class="checkbox">
						<label>
							<dsp:input id="accAdminRole" bean="MultiUserAddFormHandler.role.custAccAdmin" checked="false" type="checkbox">
								<dsp:tagAttribute name="data-event-change-id" value="eventUpdateCheckbox"/>
							</dsp:input>
							<span class="checkbox-label">Admin</span>
						</label>
					</div>

					<div class="checkbox">
						<label>
							<dsp:input bean="MultiUserAddFormHandler.role.approver" type="checkbox" checked="false"/>
							<span class="checkbox-label">Approver</span>
						</label>
					</div>

					<div class="checkbox">
						<label>
							<dsp:input bean="MultiUserAddFormHandler.role.finance" type="checkbox" checked="false"/>
							<span class="checkbox-label">Finance</span>
						</label>
					</div>
					<div class="checkbox" id="buyerRoleDiv">
						<label>
							<dsp:input id="buyerRole" bean="MultiUserAddFormHandler.role.buyer" checked="true" type="checkbox">
								<dsp:tagAttribute name="data-event-change-id" value="eventDisplayOptions"/>
							</dsp:input>
							<span class="checkbox-label">Buyer</span>
						</label>
					</div>
					<!--  SM-520 New User Role -->
					<div class="checkbox">
						<label>
							<dsp:input bean="MultiUserAddFormHandler.role.appraiser" type="checkbox" checked="false"/>
							<span class="checkbox-label">Appraiser</span>
						</label>
					</div>
					<hr>
					<div id="buyer-options" style="display: none;">
						<h4 class="text-nocase">Spending Limit and Frequency</h4>
						<p>
							If a spending limit has been entered for this User, selected Approvers will receive notifications. However, an Approver needs to be set for <strong>ALL</strong> buyers
						</p>
						<div class="row">
							<div class="col-lg-6 col-md-6 form-horizontal">
								<div class="form-group">
									<div class="col-xs-6">
										<div class="input-group">
											<div class="input-group-addon">$</div>
											<dsp:input bean="MultiUserAddFormHandler.spendingLimit" type="text" iclass="form-control" maxlength="5">
												<dsp:tagAttribute name="placeholder" value="" />
											</dsp:input>
										</div>
									</div>
								</div>
								<div class="radio checked">
									<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel3">
										<dsp:input bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="ORDER" />
										Per Order
									</label>
								</div>
								<div class="radio">
									<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel4">
										<dsp:input bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="DAY" />
										Per Day
									</label>
								</div>
								<div class="radio">
									<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel5">
										<dsp:input bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="WEEK" />
										Per Week
									</label>
								</div>
								<div class="radio">
									<label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel6">
										<dsp:input bean="MultiUserAddFormHandler.frequency" name="radioEx2" type="radio" value="MONTH" />
										Per Month
									</label>
								</div>
							</div>
						</div>
						<div id="user-approvers" style="display: block;">
							<hr>
							<h4 class="text-nocase">Approvers for this Buyer</h4>
							<p>If a spending limit has been entered for this User, you must choose at least one Approver. Selected Approvers will receive notifications.</p>
							<dsp:include page="/account/gadgets/manage-add-user-approvers.jsp" />
						</div>
						<hr>
					</div>

					<dsp:include page="/account/gadgets/org-list.jsp">
						<dsp:param name="defaultOrgId" value="${parentOrg}"/>
						<dsp:param name="pageType" value="add"/>
					</dsp:include>				

					<span id="manageUsersShipping"></span>
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.count" name="count" value="1" />

					<dsp:input type="hidden" bean="MultiUserAddFormHandler.approvers" id="approvers" name="approvers" value="" />
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.billingAccountIds" id="billingAccountIds" name="billingAccountIds" value="" />
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.users[0].value.email" value="" id="user0-email" />
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.users[0].value.login" value="" id="user0-login" />
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.create" value="true" priority="-10"/>
				</dsp:form>
				<p>
					<a href="javascript:void(0);" data-event-click-id="eventCreateUser"  class="btn btn-primary">Save User</a>
					<a href="/account/manage-users.jsp" class="btn btn-info">Cancel</a>
				</p>
			</div>
		</div>
		</main>
		<dsp:include page="/global/modals/modal-add-ship-to.jsp">
			<dsp:param name="userId" value="${userId}" />
			<dsp:param name="companyId" value="${parentOrg}" />
		</dsp:include>
		
		<dsp:include page="/global/modals/modal-roles-info.jsp">
		</dsp:include>
		
		<script type="text/javascript" nonce="${requestScope.nonce}">
            $(".phone").mask("(999) 999-9999");

            function createUser() {
                var a = gatherListChecked();
                $("#approvers").val(a);
				var billingAccountIds = gatherListCheckedByName('billingIds');
				$("#billingAccountIds").val(billingAccountIds);
                $("#user0-email").val($("#email").val());
                $("#user0-login").val($("#email").val());
                $('#add-user').submit();
            }
            function displayOptions(v) {
                if (v) {
                    $("#buyer-options").show();
                } else {
                    $("#buyer-options").hide();
                }
            }
            
            function showRoleTips() {
            	$("#rolesInfoModal").modal('show');	
            }

			function updateCheckbox(v) {
				if ($("#accAdminRole").is(':checked') || v) {
					$("#buyerRole").prop('checked', true);
					if (!v) {
						$("#buyerRole").prop('disabled', true);
						$("#buyerRoleDiv").addClass('disabled');
						$("#buyerRoleDiv").find('.checkbox-label').addClass('text-muted');
					}
					$("#buyer-options").show();
				} else {
					$("#buyerRole").prop('checked', false);
					$("#buyerRole").prop('disabled', false);
					$("#buyerRoleDiv").removeClass('disabled');
					$("#buyerRoleDiv").find('.checkbox-label').removeClass('text-muted');
					$("#buyer-options").hide();
				}
			}
			
			updateCheckbox(true);

        </script>
		<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
		<script src="${staticContentPrefix}/js/manage-users-shipping${build_version}.js"></script>
	</cp:pageContainer>
</dsp:page>
