<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	
	<dsp:getvalueof var="email" param="email"/>
	<dsp:getvalueof var="print" param="print"/>

	<div class="col-md-4 hidden-sm hidden-xs text-right">
		<ul class="page-tools">
			<c:if test="${email}">
				<li>
					<a href="#" data-toggle="modal" data-target="#sharePageModal">
						<i class="fa fa-envelope"></i> <fmt:message key="header.email_page"/>
					</a>
				</li>
			</c:if>
			<c:if test="${print}">
				<li>
					<a href="#" data-event-click-id="eventWindowPrint" >
						<i class="fa fa-print"></i> <fmt:message key="header.print_page"/>
					</a>
				</li>
			</c:if>
		</ul>
	</div>
</dsp:page>
