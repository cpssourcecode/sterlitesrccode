<dsp:page>
	<%--<dsp:importbean bean="/atg/userprofiling/Profile"/>--%>
	<%--<dsp:importbean bean="/atg/commerce/ShoppingCart"/>--%>

	<dsp:getvalueof var="order" param="order"/>
	<dt class="col-sm-12 col-xs-3">Ship to Address</dt>
	<%--<dsp:tomap var="shippingAddress" value="${address}"/>--%>
	<%--${not empty order ? '<span>':'<p>'}--%>
	<dd class="col-sm-12 col-xs-9">@${order.shipToAddressLine1}<br />
		<c:if test="${!empty order.shipToAddressLine2}">${order.shipToAddressLine2}<br/></c:if>
		<c:if test="${!empty order.shipToAddressLine3}">${order.shipToAddressLine3}<br/></c:if>
			${order.shipToCity},&nbsp;${order.shipToStateCode}&nbsp;${order.shipToPostalCode}<br/></dd>
	<%--<c:if test="${not empty shippingAddress}">--%>

	<%--</c:if>--%>
	<%--${not empty order ? '</span>':'</p>'}--%>

</dsp:page>
