<dsp:page>
	<dsp:getvalueof var="page" param="page"/>
	<dsp:getvalueof var="numPerPage" param="numPerPage"/>
	<dsp:getvalueof var="total" param="total"/>
	<dsp:getvalueof var="numPages" vartype="java.lang.Integer" value="${total/numPerPage}"/>

	<c:choose>
		<c:when test="${empty total || total=='undefined'}">
			<dsp:getvalueof var="total" value="${0}"/>
			<dsp:getvalueof var="numPages" value="${0}"/>
		</c:when>
		<c:otherwise>
			<c:if test="${total%numPerPage != 0}">
				<dsp:getvalueof var="numPages" value="${numPages+1}"/>
			</c:if>
		</c:otherwise>
	</c:choose>


	<dsp:getvalueof var="set" value="false"/>
	<dsp:droplet name="/atg/dynamo/droplet/For">
		<dsp:param name="howMany" value="${total}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="count" param="count"/>
			<c:if test="${(count > numPages) && set == false}">
				<dsp:getvalueof var="numPages" value="${count-1}"/>
				<dsp:getvalueof var="set" value="true"/>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>
	
<%--
	PAGE: <dsp:valueof value="${page}"/>
	NUMPERPAGE: <dsp:valueof value="${numPerPage}"/>
	TOTAL: <dsp:valueof value="${total}"/>
	NUMPAGES: <dsp:valueof value="${numPages}"/>
--%>

	<c:if test="${numPages >= 2}">

		<ul class="pagination">

			<c:if test="${page > 1}">
				<li class="prev">
					<a href="#" aria-label="Previous" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${vsg_utils:escapeJS(page-1)}" >
						<span class="fa fa-angle-left"></span>
					</a>
				</li>
			</c:if>

			<c:choose>
				<c:when test="${numPages <=5}">
					<dsp:droplet name="/atg/dynamo/droplet/For">
						<dsp:param name="howMany" value="${total}"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="count" param="count"/>
							<c:if test="${count <= numPages}">
								<c:choose>
									<c:when test="${page == count}">
										<li class="active"><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${count}" ><dsp:valueof param="count"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${count}" ><dsp:valueof param="count"/></a></li>
									</c:otherwise>
								</c:choose>
							</c:if>
						</dsp:oparam>
					</dsp:droplet>
				</c:when>
				<c:otherwise>
					<c:if test="${(page > 4)}">
						<li><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="1" >1</a></li>
						<li><span>...</span></li>
					</c:if>
					<c:if test = "${(page <= 3)}">
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<c:choose>
									<c:when test="${page == count}">
										<li class="active">
											<a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${count}" ><dsp:valueof value="${count}"/></a>
										</li>
									</c:when>
									<c:otherwise>
										<li>
											<a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${count}" ><dsp:valueof value="${count}"/></a>
										</li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page > 3) && (page < (numPages - 2))}">
						<%-- <li><a href="#" data-event-click-id="account81" data-event-click-param0="${vsg_utils:escapeJS(page-2)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page-2)}"/></a></li> --%>
						<li><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${vsg_utils:escapeJS(page-1)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page-1)}"/></a></li>
						<li class="active"><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${vsg_utils:escapeJS(page)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page)}"/></a></li>
						<li><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${vsg_utils:escapeJS(page+1)}" ><dsp:valueof value="${vsg_utils:escapeHtml(page+1)}"/></a></li>
					</c:if>
					<c:if test = "${(page >= (numPages - 2))}">
						<dsp:getvalueof var="start" value="${numPages-5}"/>
						<dsp:droplet name="/atg/dynamo/droplet/For">
							<dsp:param name="howMany" value="5"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="count" param="count"/>
								<dsp:getvalueof var="start" value="${start+1}"/>
								<c:choose>
									<c:when test="${page == start}">
										<li class="active"><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${start}" ><dsp:valueof value="${start}"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${start}" ><dsp:valueof value="${start}"/></a></li>
									</c:otherwise>
								</c:choose>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
					<c:if test="${(page < (numPages - 2))}">
						<c:if test="${(page < (numPages - 3))}">
							<li><span>...</span></li>
						</c:if>
						<li><a href="#" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${numPages}" ><dsp:valueof value="${numPages}"/></a></li>
					</c:if>
				</c:otherwise>
			</c:choose>

			<c:if test="${page < numPages}">
				<li class="next">
					<a href="#" aria-label="Next" data-event-click-id="eventChangeInvoicePage" data-event-click-param-page="${vsg_utils:escapeJS(page+1)}" >
						<span class="fa fa-angle-right"></span>
					</a>
				</li>
			</c:if>

		</ul>

	</c:if>

</dsp:page>