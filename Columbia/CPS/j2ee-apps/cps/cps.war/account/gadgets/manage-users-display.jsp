<dsp:page>
	<section>
		<div class="text-center mt40">
			<div class="form-group form-group-inline w35">
				<h4 class="text-nocase">Search Users</h4>
				<input type="text" class="form-control" id="searchValue" placeholder="Name or Email" maxlength="100">
			</div>
			<div class="form-group form-group-inline w20">
				<select name="" id="role" class="form-control">
					<option value="">Role</option>
					<option value="custAccAdmin">Admin</option>
					<option value="buyer">Buyer</option>
					<option value="approver">Approver</option>
					<option value="finance">Finance</option>
				</select>
			</div>
			<a href="javascript:void(0);" data-event-click-id="eventSearchUsers"  class="btn btn-primary xs-block">Search</a>
			<button id="clear-button" type="button" class="btn btn-info xs-block" disabled>Reset</button>

		</div>
	</section>
	
	<%-- Order Notification SM-345 --%>
	<div>
		<label for="adminNotification"><input type="checkbox" id="adminNotification"> Order Notifications : </label><cp:repositoryMessage key="orderNotificationInfo" />
		<!-- <button type="button" id="btnOrderNotification" class="btn btn-primary xs-block"> Notify Me</button> -->
	</div><p></p>
	<%-- Order Notification SM-345 --%>
	
	<span id="user-list">
		<dsp:include page="/account/gadgets/manage-users-list.jsp?page=1&sort=firstName&order=%2B" />
	</span>
	<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
	<script src="${staticContentPrefix}/js/manage-users-display${build_version}.js"></script>
</dsp:page>