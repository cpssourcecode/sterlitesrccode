<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/cps/reorders/AutoOrderFormHandler"/>

	<dsp:getvalueof bean="CPSSessionBean.orders" var="orders"/>

	<c:choose>
		<c:when test="${fn:length(orders) > 0}">
			<table class="table table-striped table-mobile">
				<thead>
					<tr>
						<th id="sort-date"><a href="javascript:void(0);" data-event-click-id="eventSortOrderDateOrderList" ><strong>Order Date</strong> <i class="fa fa-sort"></i></a></th>
						<th id="sort-id"><a href="javascript:void(0);" data-event-click-id="eventSortOrderIdOrderList" ><strong>Order #</strong> <i class="fa fa-sort"></i></a></th>
						<th id="sort-cs"><a href="javascript:void(0);" data-event-click-id="eventSortOrderPOOrderList" ><strong>PO #</strong> <i class="fa fa-sort"></i></a></th>
						<th id="sort-total"><a href="javascript:void(0);" data-event-click-id="eventSortOrderTotalOrderList" ><strong>Amount</strong> <i class="fa fa-sort"></i></a></th>
						<th colspan="3"></th>
					</tr>
				</thead>
				<tbody>
					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="CPSSessionBean.orders"/>
						<dsp:param name="elementName" value="order"/>
						<dsp:oparam name="output">

							<dsp:getvalueof var="orderNumber" param="order.orderNumber"/>
							<dsp:getvalueof var="date" param="order.date"/>
							<dsp:getvalueof var="po" param="order.customerPONumber"/>
							<dsp:getvalueof var="amount" param="order.amount"/>
							<dsp:getvalueof var="link" param="order.url"/>
							<dsp:getvalueof var="autoOrderId" param="order.autoOrderId"/>

							<tr>
								<td><span class="caption">Order Date</span><fmt:formatDate pattern="MM/dd/yyyy" timeZone="GMT" value="${date}"/></td>
								<td><span class="caption">Order #</span><span>${orderNumber}</span></td>
								<%--<td><a href="${originatingRequest.contextPath}/account/order-detail-ws.jsp?orderId=${vsg_utils:aesEncrypt(orderNumber)}" class="table-icon-link"><span class="caption">Order</span><span>${orderNumber}</span></a></td>--%>
								<td><span class="caption">PO #</span>${po}</td>
								<td><span class="caption">Amount</span><dsp:valueof value="${amount}" converter="currencyConversion"/></td>
								<td>
									<%--<a href="${link}" class="table-icon-link"><span>View Details</span></a>--%>
									<a href="${originatingRequest.contextPath}/account/order-detail-ws.jsp?orderId=${vsg_utils:aesEncrypt(orderNumber)}" class="table-icon-link"><span>View Details</span></a>
								</td>
								<td>
									<a href="${link}" class="table-icon-link" target="_blank">
										<i class="fa fa-file-text-o"></i>
										<span>Acknowledgement PDF</span>
									</a>
								</td>
								<td>
									<c:if test="${not empty autoOrderId}">
										<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-text" 
											class="btn btn-sm" data-event-click-id="eventCancelAutoOrder" data-auto-order-id="${autoOrderId}" >Cancel Auto-Reorder</a>
									</c:if>
								</td>
							</tr>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
							<dsp:getvalueof var="ordersTotal" bean="CPSSessionBean.ordersTotal"/>
							<input type="hidden" value="${ordersTotal}" id="total_count"/>
							<input type="hidden" value="" id="page"/>
						</dsp:oparam>
						<dsp:oparam name="empty">
							<input type="hidden" value="0" id="total_count"/>
							<input type="hidden" value="1" id="page"/>
							<dsp:include page="/includes/gadgets/info-message.jsp">
								<dsp:param name="key" value="err_order_not_found"/>
							</dsp:include>
						</dsp:oparam>
					</dsp:droplet>
				</tbody>

			</table>

			<dsp:form id="cancel-auto-order" formid="cancel-auto-order" method="POST">
				<dsp:input type="hidden" bean="AutoOrderFormHandler.autoOrderId" value="" id="addAutoOrderID"/>
				<dsp:input type="hidden" bean="AutoOrderFormHandler.cancelAutoOrder" value="Submit" priority="-10"/>
			</dsp:form>
		</c:when>
		<c:otherwise>
			<div class="well text-center">
				<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
					<dsp:param name="id" value="err-no-orders-were-found" />
					<dsp:param name="elementName" value="message"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="message" param="message.message" />
						${message}
					</dsp:oparam>
					<dsp:oparam name="empty">
						No orders were found. Click Reset to see your most recent orders or to search again. Please call us at 1-800-572-1904 if you're still unable to find your order. 
					</dsp:oparam>
				</dsp:droplet>
			</div>
		</c:otherwise>
	</c:choose>

</dsp:page>