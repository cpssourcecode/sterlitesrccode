<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

	<dsp:droplet name="/atg/dynamo/droplet/IsNull">
		<dsp:param name="value" bean="Profile.parentOrganization"/>
		<dsp:oparam name="true">
			<dsp:getvalueof var="companyName" bean="Profile.derivedBillingAddress.companyName"/>
			<dsp:getvalueof var="address1" bean="Profile.derivedBillingAddress.address1"/>
			<dsp:getvalueof var="address2" bean="Profile.derivedBillingAddress.address2"/>
			<dsp:getvalueof var="address3" bean="Profile.derivedBillingAddress.address3"/>
			<dsp:getvalueof var="city" bean="Profile.derivedBillingAddress.city"/>
			<dsp:getvalueof var="state" bean="Profile.derivedBillingAddress.state"/>
			<dsp:getvalueof var="postalCode" bean="Profile.derivedBillingAddress.postalCode"/>
			<dsp:getvalueof var="phoneNumber" bean="Profile.derivedBillingAddress.phoneNumber"/>
			<dsp:getvalueof var="emailAddress" bean="Profile.derivedBillingAddress.emailAddress"/>
			<dsp:getvalueof var="firstName" bean="Profile.derivedBillingAddress.firstName"/>
			<dsp:getvalueof var="lastName" bean="Profile.derivedBillingAddress.lastName"/>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="companyName" bean="Profile.parentOrganization.billingAddress.companyName"/>
			<dsp:getvalueof var="address1" bean="Profile.parentOrganization.billingAddress.address1"/>
			<dsp:getvalueof var="address2" bean="Profile.parentOrganization.billingAddress.address2"/>
			<dsp:getvalueof var="address3" bean="Profile.parentOrganization.billingAddress.address3"/>
			<dsp:getvalueof var="city" bean="Profile.parentOrganization.billingAddress.city"/>
			<dsp:getvalueof var="state" bean="Profile.parentOrganization.billingAddress.state"/>
			<dsp:getvalueof var="postalCode" bean="Profile.parentOrganization.billingAddress.postalCode"/>
			<dsp:getvalueof var="phoneNumber" bean="Profile.parentOrganization.billingAddress.phoneNumber"/>
			<dsp:getvalueof var="emailAddress" bean="Profile.parentOrganization.billingAddress.emailAddress"/>
			<dsp:getvalueof var="firstName" bean="Profile.parentOrganization.billingAddress.firstName"/>
			<dsp:getvalueof var="lastName" bean="Profile.parentOrganization.billingAddress.lastName"/>
		</dsp:oparam>
	</dsp:droplet>


	<dsp:getvalueof var="shippingAddress" bean="Profile.shippingAddress"/>
	<%--<dsp:tomap var="shippingAddress" bean="Profile.selectedCS"/>--%>

	<c:choose>
		<c:when test="${not empty shippingAddress}">
			<dsp:getvalueof var="shippingAddress1" bean="Profile.shippingAddress.address1"/>
			<dsp:getvalueof var="shippingAddress2" bean="Profile.shippingAddress.address2"/>
			<dsp:getvalueof var="shippingCity" bean="Profile.shippingAddress.city"/>
			<dsp:getvalueof var="shippingState" bean="Profile.shippingAddress.state"/>
			<dsp:getvalueof var="shippingPostalCode" bean="Profile.shippingAddress.postalCode"/>
		</c:when>
		<%-- If profile shipping address is empty, profile derived shipping address is displayed here.  
		  fix for "SM-361 - Do not display a default address until the customer selects one". 
		  Now Profile derived shipping address is removed. --%>
	</c:choose>
	<input type="hidden" value="${shippingAddress}" id="defaultSelectedAddress" />
	<div class="row">
		<div class="col-md-4 col-sm-6">
			<h3 class="h4 text-nocase"><fmt:message key="account.address.label.default_ship_to_address"/></h3> <%--Address Line 1, Address Line 2, City/State/Zip--%>
			<p>
				<strong>
					<%-- <dsp:getvalueof var="selectedCS" bean="Profile.selectedCS"/> --%>
					<dsp:valueof value="${vsg_utils:contactInfoAddress(shippingAddress)}" valueishtml="true" />
				</strong>
			</p>
			<%-- Displaying message to user if default ship to address is empty. fix for SM-361. --%>
			<c:if test="${empty shippingAddress && not emptyResult}">
				<p><cp:repositoryMessage key="emptyDefaultShipToAddressContent" /></p>
			</c:if>
			<p id="addressContent"><fmt:message key="account.address.label.need_to_add_address"/></p>
		</div>

		<%@include file="/account/gadgets/addresses-cb-display.jspf" %>
	</div>

</dsp:page>