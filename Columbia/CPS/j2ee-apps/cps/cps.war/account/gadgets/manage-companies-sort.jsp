<dsp:page>
	<dsp:getvalueof var="sort" param="sort"/>
	<dsp:getvalueof var="order" param="order"/>
	<thead>
		<tr>
			<th>
				<c:choose>
					<c:when test="${sort == 'companyName'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="company" data-sort-dir="-" id="sortCompany">Company <i class="fa fa-sort-up"></i></a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="company" data-sort-dir="+"  id="sortCompany">Company <i class="fa fa-sort-down"></i></a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="company" data-sort-dir="+"  id="sortCompany">Company <i class="fa fa-sort"></i></a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>
				<c:choose>
					<c:when test="${sort == 'accountNumber'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="accountNumber" data-sort-dir="-" id="sortAccount">Account # <i class="fa fa-sort-up"></i></a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="accountNumber" data-sort-dir="+" id="sortAccount">Account # <i class="fa fa-sort-down"></i></a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="accountNumber" data-sort-dir="+"   id="sortAccount">Account # <i class="fa fa-sort"></i></a>
					</c:otherwise>
				</c:choose>
			</th>
			<th>
				<c:choose>
					<c:when test="${sort == 'address'}">
						<c:choose>
							<c:when test="${order == '+'}">
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="address" data-sort-dir="-" id="sortAddress">Address <i class="fa fa-sort-up"></i></a>
							</c:when>
							<c:otherwise>
								<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="address" data-sort-dir="+" id="sortAddress">Address <i class="fa fa-sort-down"></i></a>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<a href="javascript:void(0);" data-event-click-id="eventSortPage" data-sort-key="address" data-sort-dir="+" id="sortAddress">Address <i class="fa fa-sort"></i></a>
					</c:otherwise>
				</c:choose>
			</th>
			<th></th>
		</tr>
	</thead>
</dsp:page>