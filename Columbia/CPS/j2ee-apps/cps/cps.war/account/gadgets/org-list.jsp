<dsp:page>
    <dsp:getvalueof var="orgId" param="defaultOrgId" />
    <dsp:getvalueof var="pType" param="pageType"/>

    <dsp:droplet name="/cps/droplet/CSSelectionDroplet">
        <dsp:param name="orgId" value="${orgId}" />
        <dsp:oparam name="output">
            <dsp:getvalueof var="orgList" param="orgList" />
            <c:choose>
                <c:when test="${not empty orgList}">
                    <h4 class="text-nocase">Select Billing Accounts</h4>
                    <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                        <dsp:param name="array" param="sortedOrgLists" />
                        <dsp:param name="elementName" value="userOrg" />
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="billToAddress1" param="userOrg.organization.billingAddress.address1" />
                            <dsp:getvalueof var="name" param="userOrg.organization.name" />
                            <dsp:getvalueof var="id" param="userOrg.organization.id" />
                            <dsp:getvalueof var="selected" param="userOrg.selected" />
                            <div class="checkbox">
                                <label>
                                    <input id="${id}" data-event-click-id="eventSelectBillingAddress" data-type="${pType}"  type="checkbox" value="${id}" name="billingIds" ${selected ? 'checked':''}> ${name} - ${id}
                                </label>
                            </div>
                        </dsp:oparam>
                        <dsp:oparam name="outputEnd">
                            <script type="text/javascript" nonce="${requestScope.nonce}">
                                $("#billingAccounts").val(gatherListCheckedByName('billingIds'));
                            </script>
                        </dsp:oparam>
                    </dsp:droplet>
                    <hr>
                </c:when>
            </c:choose>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>
