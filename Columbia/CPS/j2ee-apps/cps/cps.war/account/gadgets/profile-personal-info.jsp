<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<h4 class="text-nocase">Personal Information</h4>
	<div class="profile-personal collapse in" aria-expanded="true">
		<dsp:getvalueof var="firstName" bean="Profile.firstName" />
		<dsp:getvalueof var="lastName" bean="Profile.lastName" />
		<dsp:getvalueof var="jobTitle" bean="Profile.homeAddress.jobTitle" />
		<div class="row">
			<div class="col-sm-8">
				<ul class="list-unstyled">
					<li>
						<strong>Name:</strong>
						${firstName}&nbsp;${lastName}
					</li>
					<li>
						<strong>Title:</strong>
						${jobTitle}
					</li>
				</ul>
			</div>
			<div class="col-sm-4 text-right ">
				<button class="btn btn-info" type="button" id="personalInfoButton" data-toggle="collapse" data-target=".profile-personal" aria-expanded="false" aria-controls="profile-personal">Edit</button>
			</div>
		</div>
	</div>
	<div class="profile-personal collapse" id="profile-personal-edit" aria-expanded="false" style="height: 0px;">
		<dsp:form method="post" action="/account/profile-settings.jsp" id="edit-personal-info">
			<div class="row">
				<div class="col-md-8">
					<dsp:getvalueof var="personalFormExceptions" vartype="java.lang.Object" bean="ProfileFormHandler.personalFormExceptions" />
					<dsp:getvalueof var="updateSuccessful" bean="ProfileFormHandler.profileUpdateSuccessful" />
					<c:if test="${updateSuccessful}">
						<script type="text/javascript" nonce="${requestScope.nonce}">
							document.getElementById("success-message").className = "alert alert-success";
							document.getElementById("success-message").innerHTML = "Personal information changed successfully";
						</script>
					</c:if>
					<c:if test="${not empty personalFormExceptions}">
						<script type="text/javascript" nonce="${requestScope.nonce}">
							jQuery(document).ready(function() {
								$("#personalInfoButton").click();
							});
						</script>
						<c:forEach var="personalFormException" items="${personalFormExceptions}">
							<dsp:param name="personalFormException" value="${personalFormException}" />
							<dsp:getvalueof var="errorCode" param="personalFormException.propertyPath" />
							<script type="text/javascript" nonce="${requestScope.nonce}">
								jQuery(document).ready(function() {
									loadErrors('${errorCode}');
								});
								function loadErrors(errorCode) {
									$("#" + errorCode).parent().addClass(
											"has-error");
								}
							</script>
							<c:if test="${personalFormException.message != ''}">
								<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">
											<span class="glyphicon glyphicon-remove-circle"></span>
										</span>
									</button>
									<dsp:valueof param="personalFormException.message" valueishtml="true" />
								</div>
							</c:if>
						</c:forEach>
					</c:if>
					<div class="row row-narrow">
						<div class="form-group col-sm-6">
							<dsp:input type="text" iclass="form-control " bean="ProfileFormHandler.value.firstName" id="firstName" name="firstName" maxlength="50" />
							<label class="control=label">First Name</label>
						</div>
						<div class="form-group col-sm-6">
							<dsp:input type="text" iclass="form-control" bean="ProfileFormHandler.value.lastName" id="lastName" name="lastName" maxlength="50" />
							<label class="control=label">Last Name</label>
						</div>
					</div>
					<div class="form-group">
						<dsp:input type="text" id="title" iclass="form-control" value="${jobTitle}" bean="ProfileFormHandler.value.jobTitle" maxlength="50">
							<dsp:tagAttribute name="placeholder" value="Job Title" />
						</dsp:input>
						<label class="control=label">Job Title</label>
					</div>
				</div>
			</div>
			<div class="text-right ">
				<button data-event-click-id="eventSubmitProfilePersonalUpdate"  class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample0" aria-expanded="false" aria-controls="collapseExample0">Save</button>
				<button class="btn btn-info" type="button" data-toggle="collapse" data-target=".profile-personal" aria-expanded="false" aria-controls="profile-personal">Cancel</button>
			</div>
			<dsp:input type="hidden" value="true" bean="ProfileFormHandler.updatePersonalInfo" priority="-10" />
			<dsp:input type="hidden" bean="ProfileFormHandler.updateSuccessURL" value="/account/profile-settings.jsp" />
			<dsp:input type="hidden" bean="ProfileFormHandler.updateErrorURL" value="/account/profile-settings.jsp" />
		</dsp:form>
	</div>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		function submitProfilePersonalUpdate() {
			$("#edit-personal-info").submit();
		}
	</script>
</dsp:page>