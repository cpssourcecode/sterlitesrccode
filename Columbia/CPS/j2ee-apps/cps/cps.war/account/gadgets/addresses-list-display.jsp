<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/cps/droplet/CSSelectionDroplet"/>
	<dsp:getvalueof var="searchTerm" param="searchTerm"/>
	<dsp:getvalueof var="parentOrgId" bean="Profile.parentOrganization.id" />
	<dsp:getvalueof var="shippingAddress" bean="Profile.shippingAddress"/>	
	
	<c:if test="${not empty shippingAddress}">
		<dsp:getvalueof var="shippingAddressId" bean="Profile.shippingAddress.id"/>
	</c:if>
	
	<dsp:droplet name="/cps/droplet/CSSelectionDroplet">
		<dsp:param name="filter" value="${searchTerm}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="showBillingAccount" param="showBillingAccount"/>
			<input type="hidden" id="showBillingAccount" name="showBillingAccount" value="${showBillingAccount}">
			<dsp:getvalueof var="emptyResult" param="emptyResult"/>
			<c:choose>
				<c:when test="${emptyResult && empty searchTerm}">

					<h3 class="text-muted">
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="err_search"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include>
					</h3>

				</c:when>
				<c:when test="${emptyResult}">
					<input type="hidden" name="emptyResult" id="emptyResult" value="${emptyResult}"/>
				</c:when>
				<c:otherwise>

						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" param="associatedCSData"/>
							<dsp:param name="elementName" value="cs"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="csAddress1" param="cs.addressItem.address1" />
								<dsp:getvalueof var="csAddress2" param="cs.addressItem.address2" />
								<dsp:getvalueof var="csCity" param="cs.addressItem.city" />
								<dsp:getvalueof var="csState" param="cs.addressItem.state" />
								<dsp:getvalueof var="csZip" param="cs.addressItem.postalCode" />
								<dsp:getvalueof var="csId" param="cs.addressItem.id" />
								<dsp:getvalueof var="orgName" param="cs.organization.name"/>
								<dsp:getvalueof var="orgId" param="cs.organization.id"/>
                            	<dsp:getvalueof var="billingAddressId" param="cs.organization.billingAddress.id"/>
								<dsp:getvalueof var="derivedBillingAddressId" param="cs.organization.derivedBillingAddress.id"/>

								<c:set var="checked" value=""/>
								<c:choose>
									<c:when test="${csId eq shippingAddressId}">
										<c:set var="checked" value="checked"/>
									</c:when>
									<c:otherwise>
										<c:set var="checked" value=""/>
									</c:otherwise>
								</c:choose>

								<tr>
										<%--<div class="radio scrollbar-scrollable ${checked}">--%>
									<td class="text-center " data-initialize="radio" id="myCustomRadioLabel" >
										<input ${checked} type="radio" name="radioEx" value="${csId}" data-event-click-id="eventDoCS_CheckShippingAddress" >
									</td>
									<td  data-initialize="radio" >
										<c:if test="${!empty csAddress1}">${csAddress1},&nbsp; <br/></c:if>
										<c:if test="${!empty csAddress2}">${csAddress2},&nbsp; <br/></c:if>
										<c:if test="${!empty csCity}">${csCity},&nbsp;</c:if>
										<c:if test="${!empty csState}">${csState}&nbsp;</c:if>
										<c:if test="${!empty csZip}">${csZip}</c:if>
									</td>
									<%-- SM-188 Display account number on ship to address table --%>
									<c:choose>
										<c:when test="${showBillingAccount}">
											<c:choose>
												<c:when test="${not empty billingAddressId}">
													<td>${billingAddressId}-${csId}</td>
												</c:when>
												<c:otherwise>
													<td>${derivedBillingAddressId}-${csId}</td>
												</c:otherwise>
											</c:choose>
											<td>${orgName}</td>										
										</c:when>
										<c:otherwise>
											<td>${csId}</td>
										</c:otherwise>
									</c:choose>
								</tr>
								<%--</div>--%>
							</dsp:oparam>
						</dsp:droplet>
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>


	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
		<dsp:param name="messageId" value="err_search"/>
		<dsp:param name="isReset" value="true"/>
	</dsp:include>

</dsp:page>