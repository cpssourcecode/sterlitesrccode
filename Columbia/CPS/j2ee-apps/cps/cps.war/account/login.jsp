<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<dsp:droplet name="/atg/dynamo/droplet/Redirect">
		<dsp:param name="url" value="${originatingRequest.contextPath}/"/>
	</dsp:droplet>
</dsp:page>

