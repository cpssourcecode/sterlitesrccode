<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/cps/droplet/CPSOrderItemsPaginationDroplet"/>

	<dsp:getvalueof var="orderId" param="orderId"/>
	<dsp:getvalueof var="pageNumber" param="pageNumber"/>
	<dsp:getvalueof var="pageNumber" value="${empty pageNumber || pageNumber == 0 ? 1 : pageNumber}"/>

	<div class="col-xs-12">
		<div class="material-items-list order-detail-list items-order-mobile">
			<div class="header">
				<div class="check">
					<div class="checkbox" id="select-item-material">
						<label class="checkbox-custom" data-initialize="checkbox">
							<input class="sr-only" type="checkbox" value=""/>
							<span class="checkbox-label"><strong>Select All</strong></span>
						</label>
					</div>
				</div>
				<div class="name">
					<span class="title"><strong>Item Name</strong></span>
				</div>
				<div class="item-number">
					<span class="title"><strong>Item#</strong></span>
				</div>
				<div class="qty">
					<span class="title"><strong>Quantity</strong></span>
				</div>
				<div class="price">
					<span class="title"><strong>Price</strong> <i class="fa fa-sort"></i></span>
				</div>
				<div class="total">
					<span class="title"><strong>Total</strong></span>
				</div>
				<div class="status">
					<span class="title"><strong>Status</strong></span>
				</div>
			</div>
			<div class="content-list scrollbar-inner table-content">

				<dsp:droplet name="CPSOrderItemsPaginationDroplet">
					<dsp:param name="orderId" param="orderId"/>
					<dsp:param name="pageNumber" param="pageNumber"/>
					<dsp:oparam name="output">

						<dsp:getvalueof var="commerceItemsCount" param="commerceItemsCount"/>
						<dsp:getvalueof var="itemsPerPage" param="itemsPerPage"/>
						<dsp:getvalueof var="pageCount" param="pageCount"/>

						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" param="items"/>
							<dsp:param name="elementName" value="item"/>
							<dsp:oparam name="output">

								<dsp:tomap var="productMap" param="item.auxiliaryData.productRef"/>
								<dsp:tomap var="commerceItem" param="item"/>
								<dsp:getvalueof var="skuId" value="${commerceItem.catalogRefId}"/>

								<dsp:getvalueof var="parentCategory" param="item.auxiliaryData.productRef.parentCategory"/>
								<c:if test="${parentCategory == null}">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="item.auxiliaryData.productRef.parentCategories" />
										<dsp:param name="elementName" value="category"/>
										<dsp:oparam name="output">
											<dsp:getvalueof var="index" param="index"/>
											<c:if test="${index eq 0}">
												<dsp:getvalueof var="eCommerceDisplay" param="category.eCommerceDisplay"/>
											</c:if>
										</dsp:oparam>
									</dsp:droplet>
								</c:if>
								<c:if test="${parentCategory != null}">
									<dsp:getvalueof var="eCommerceDisplay" param="item.auxiliaryData.productRef.parentCategory.eCommerceDisplay"/>
								</c:if>



								<dsp:getvalueof var="imageUrl" vartype="java.lang.String"
												value="${empty productMap.thumbnail_url ? '/assets/images/plp-placeholder.jpg' : productMap.thumbnail_url}"/>
								<%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String"
												value="${originatingRequest.contextPath}/catalog/pdp.jsp?prodId=${skuId}"/> --%>
								<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
									<dsp:param name="prodId" value="${productMap.id}"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
									</dsp:oparam>
								</dsp:droplet>
								<dsp:getvalueof var="productDisabled"
												value="${productMap.stockingType eq 'O' || productMap.stockingType eq 'U' || productMap.stockingType eq 'K' || productMap.stockingType == 'X' || not eCommerceDisplay}"/>
								<div class="content ${productDisabled ? ' disabled' : ''}">
									<div class="check">
										<div class="checkbox image" id="select-item-material-check">
											<label class="checkbox-custom" data-initialize="checkbox">
												<input class="sr-only" type="checkbox" value="${skuId}" ${productDisabled ? 'disabled' : ''}
													   name="itemsInCart" id="${skuId}"/>
												<input type="hidden" value="${commerceItem.quantity}" name="${skuId}" id="${skuId}_qty">
                                                <span class="checkbox-label">
                                                    <c:if test="${productDisabled}">
														<button class="question-tooltip" role="button" data-trigger="hover" data-toggle="popover"
																data-trigger="focus" data-placement="right"
																data-content="This item is not available for online order.">
															<span class="glyphicon glyphicon-question-sign"></span>
														</button>
													</c:if>
                                                    <cp:readerimg src="${imageUrl}" alt="" />
                                                </span>
											</label>
										</div>
									</div>
									<div class="name">
										<a class="product-title" href="${productUrl}">
											<%--<dsp:valueof value="${productMap.displayName}" converter="valueishtml"/>--%>
												<dsp:valueof value="${productMap.description}" converter="valueishtml"/>,
												<%-- <dsp:valueof value="${productMap.descriptionLine2}" converter="valueishtml"/> --%>
										</a>
									</div>
									<div class="item-number"><div class="caption">Item #</div><dsp:valueof value="${productMap.displayName}" converter="valueishtml"/></div>
									<div class="qty"><div class="caption">Qty</div><span class="item-qty">${commerceItem.quantity}</span></div>
									<div class="price"><div class="caption">Price</div><dsp:valueof value="${commerceItem.priceInfo.listPrice}" converter="currencyConversion"/></div>
									<div class="total"><div class="caption">Total</div><dsp:valueof value="${commerceItem.priceInfo.amount}" converter="currencyConversion"/></div>
									<div class="status">
										<div class="caption">Status</div>
										<span class="label label-success">${commerceItem.state}</span>
									</div>
								</div>
							</dsp:oparam>
						</dsp:droplet>

					</dsp:oparam>
				</dsp:droplet>


			</div>
		</div>
	</div>


	<div class="col-xs-12 text-center">
		<nav>
			<c:if test="${pageCount >= 2}">

				<ul class="pagination pagination-lg">

					<c:if test="${pageNumber > 1}">
						<li class="prev">
							<a href="#" aria-label="Previous">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</a>
						</li>
					</c:if>

					<c:choose>
						<c:when test="${pageCount <= 5}">
							<dsp:droplet name="/atg/dynamo/droplet/For">
								<dsp:param name="howMany" value="${commerceItemsCount}"/>
								<dsp:oparam name="output">
									<dsp:getvalueof var="count" param="count"/>
									<c:if test="${count <= pageCount}">
										<c:choose>
											<c:when test="${pageNumber == count}">
												<li class="active"><a href="#"><dsp:valueof param="count"/></a></li>
											</c:when>
											<c:otherwise>
												<li class="page-number"><a href="#"><dsp:valueof param="count"/></a></li>
											</c:otherwise>
										</c:choose>
									</c:if>
								</dsp:oparam>
							</dsp:droplet>
						</c:when>
						<c:otherwise>
							<c:if test="${(pageNumber > 4)}">
								<li class="page-number"><a href="#">1</a></li>
								<li><span>...</span></li>
							</c:if>
							<c:if test="${(pageNumber <= 3)}">
								<dsp:droplet name="/atg/dynamo/droplet/For">
									<dsp:param name="howMany" value="5"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="count" param="count"/>
										<c:choose>
											<c:when test="${pageNumber == count}">
												<li class="active">
													<a href="#"><dsp:valueof value="${count}"/></a>
												</li>
											</c:when>
											<c:otherwise>
												<li class="page-number">
													<a href="#"><dsp:valueof value="${count}"/></a>
												</li>
											</c:otherwise>
										</c:choose>
									</dsp:oparam>
								</dsp:droplet>
							</c:if>
							<c:if test="${(pageNumber > 3) && (pageNumber < (pageCount - 2))}">
								<li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber-2)}"/></a></li>
								<li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber-1)}"/></a></li>
								<li class="active"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber)}"/></a></li>
								<li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber+1)}"/></a></li>
								<li class="page-number"><a href="#"><dsp:valueof value="${vsg_utils:escapeHtml(pageNumber+2)}"/></a></li>
							</c:if>
							<c:if test="${(pageNumber >= (pageCount - 2))}">
								<dsp:getvalueof var="start" value="${pageCount-5}"/>
								<dsp:droplet name="/atg/dynamo/droplet/For">
									<dsp:param name="howMany" value="5"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="count" param="count"/>
										<dsp:getvalueof var="start" value="${start+1}"/>
										<c:choose>
											<c:when test="${pageNumber == start}">
												<li class="active"><a href="#"><dsp:valueof value="${start}"/></a></li>
											</c:when>
											<c:otherwise>
												<li class="page-number"><a href="#"><dsp:valueof value="${start}"/></a></li>
											</c:otherwise>
										</c:choose>
									</dsp:oparam>
								</dsp:droplet>
							</c:if>
							<c:if test="${(pageNumber < (pageCount - 2))}">
								<li><span>...</span></li>
								<li class="page-number"><a href="#"><dsp:valueof value="${pageCount}"/></a></li>
							</c:if>
						</c:otherwise>
					</c:choose>

					<c:if test="${pageNumber < pageCount}">
						<li class="next">
							<a href="#" aria-label="Next">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</a>
						</li>
					</c:if>

				</ul>

			</c:if>


		</nav>
	</div>

</dsp:page>
