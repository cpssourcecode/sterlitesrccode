<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>

	<dsp:getvalueof var="close" param="close"/>
	<dsp:getvalueof var="parentOrgId" bean="Profile.parentOrganization.id" />
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>
	
	<cp:pageContainer page="account">
		<dsp:droplet name="/cps/droplet/CSSelectionDroplet">
			<dsp:param name="filter" value="${searchTerm}" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="showBillingAccount" param="showBillingAccount" />
				<input type="hidden" id="showBillingAccount"
					name="showBillingAccount" value="${showBillingAccount}">
				<dsp:getvalueof var="emptyResult" param="emptyResult" scope="request"/>
			</dsp:oparam>
		</dsp:droplet>
		<main id="body">
			<div class="container ${csrClass}">
				<div class="row">
					<div class="col-md-8">
						<ol class="breadcrumb">
							<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
							<li class="active">View Addresses</li>
						</ol>
					</div>
					<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
						<dsp:param name="print" value="true"/>
					</dsp:include>
				</div>
				<h1 class="mb20">Ship To Addresses</h1>

				<section id="user-address-section">
					<dsp:include page="${originatingRequest.contextPath}/account/gadgets/user-addresses.jsp"/>
				</section>
				<c:if test="${not emptyResult}">
				<section>
				
					<h3 class="h4 text-nocase"><fmt:message key="account.address.label.change_default_ship_to_address"/></h3>
					<p><fmt:message key="account.address.label.change_default_ship_to_address_todo"/></p>
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<form>
								<div class="well well-grey text-center">

									<div class="form-group form-group-inline w65">
										<input type="text" class="form-control" placeholder="Search for Ship To Address" id="csListSearchTerm">
									</div>

									<a href="#" class="btn btn-primary xs-block" data-event-click-id="eventSearchCS" ><fmt:message key="account.address.label.search"/></a>

									<button href="#" type="button" id="ship-to-search-reset-btn" disabled class="btn btn-info xs-block" data-event-click-id="eventLocationReload" ><fmt:message key="account.address.label.reset"/></button>

								</div>
							</form>
						</div>
					</div>
			
				</section>
				</c:if>
			<c:if test="${not emptyResult}">
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr id="table-header">
								<th class="text-center" style="width: 100px;"><fmt:message key="account.address.label.select"/></th>
								<th><fmt:message key="account.address.label.ship_to"/></th>
								<c:if test="${not empty parentOrgId}">
									<th><fmt:message key="account.address.label.account_number"/></th>
								</c:if>								
							</tr>
						</thead>
						<tbody id="addresses-list"></tbody>
					</table>
					<dsp:droplet name="/cps/droplet/CartAddressDroplet">
						<dsp:param bean="/atg/commerce/ShoppingCart.current" name="order" />
						<dsp:param bean="Profile" name="profile" />
						<dsp:param value="account" name="page" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="deliveryMethod" param="deliveryMethod" />
						</dsp:oparam>
					</dsp:droplet>
					<dsp:form method="post" id="modal-cs-form-default">
						<dsp:input type="hidden" value="" id="selected_default_shipping_id"	bean="ProfileDefaultShippingAddressFormHandler.selectedId"/>
						<dsp:input type="hidden" value="${deliveryMethod}" id="default_address_delivery_method" bean="ProfileDefaultShippingAddressFormHandler.deliveryMethod" />
						<dsp:input type="hidden" value="true" bean="ProfileDefaultShippingAddressFormHandler.saveDefaultShippingAddress"/>
					</dsp:form>

				</div>
				</c:if>
				<%-- <c:if test="${not emptyResult}">
				<button class="btn btn-primary" data-event-click-id="eventSelectedShippingAddress" ><fmt:message key="account.address.label.confirm"/></button>
				</c:if> --%>
			</div>
		</main>

		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-ship-address-changed.jsp">
			<dsp:param name="isReset" value="true"/>
		</dsp:include>
		
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-ship-address-confirm.jsp">
		</dsp:include>

		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
			<dsp:param name="messageId" value="err_shp_conf"/>
			<dsp:param name="divId" value="_showMessEmpty"/>
			<dsp:param name="isReset" value="true"/>
		</dsp:include>
		<!-- main container end -->

		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(function () {
			if($("#addresses-list").length != 0){
				$("#addresses-list").load("/account/gadgets/addresses-list-display.jsp", function (response, status, xhr) {
					eventsAddressListDisplay();
					var isShowBillingAccount = $('#showBillingAccount').val();
					if (isShowBillingAccount === 'true') {
						$('#table-header').append("<th>Billing Account</th>");
					}
               });
				}else{
                    $("#addressContent").css("color", "#d3191e");
				}
				
			});

			function enableShipToAddressResetButton(){
                document.getElementById("ship-to-search-reset-btn").disabled = false;
			}

            function doCS_SelectShippingAddress() {
                var selectedAddress = $("#selected_default_shipping_id").val();
                if(selectedAddress == null || selectedAddress==""){
                    closeModalDialog();
                    $("#link-noResultsModal_showMessEmpty").click();
                }
                else{
                    var dataString = $('#modal-cs-form-default').serialize();
                    $.ajax({
                        type: "POST",
                        data: dataString,
                        dataType: "json",
                        success: function(response) {
                            if ('success' == response.code) {
                                // prevent ie from caching
                                d = new Date();
                                t = d.getTime();
                                $("#header-cs-selection").load("/includes/gadgets/header-cs-selection.jsp?t="+t, function () {
									eventsHeaderCsSelection();
                                });
                                $("#header-ship-to").load("/includes/gadgets/header-ship-to.jsp");
                                $("#user-address-section").load("/account/gadgets/user-addresses.jsp?t="+t);
                           	    $("#addressConfirmChange").modal("hide");
                                $("#link-addressConfirm").click();
                                $("#modal-cs").load("/global/modals/modal-cs.jsp", function(){
                                	eventsModalCS();
                                    onModalCsLoaded();
                                });
                            } else {
                                showErrors(response);
                            }
                        },
                        error: ajaxErrorLogin

                    });
                }

                return false;
            }
		</script>

	</cp:pageContainer>

</dsp:page>