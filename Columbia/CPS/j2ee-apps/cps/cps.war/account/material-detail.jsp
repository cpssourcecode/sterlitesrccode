<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/droplet/ReorderlistLookupDroplet"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/util/MaterialListDownloadHandler"/>
	<dsp:getvalueof var="importModalError" param="importModalError"/>
	<dsp:importbean bean="/cps/userprofiling/MaterialListHandler"/>
	<dsp:getvalueof var="ca" param="ca"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>

	<dsp:droplet name="Switch">
		<dsp:param bean="Profile.transient" name="value"/>
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/?loc=reorder"/>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/IsNull">
				<dsp:param name="value" bean="Profile.parentOrganization"/>
				<dsp:oparam name="true">
					<dsp:droplet name="/atg/dynamo/droplet/Redirect">
						<dsp:param name="url" value="${originatingRequest.contextPath}/account/material-lists.jsp"/>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:droplet name="/cps/droplet/CPSCheckAccessToMaterialList">
		<dsp:param name="giftlistId" param="giftlistId"/>
		<dsp:param name="profile" bean="/atg/userprofiling/Profile"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="isHaveAccess" param="isHaveAccess" />
		</dsp:oparam>
	</dsp:droplet>
	
	<dsp:droplet name="/cps/droplet/CPSMaterialListPriceAvailabilityCheckDroplet">
		<dsp:param name="giftlistId" param="giftlistId"/>
		<dsp:param name="profile" bean="/atg/userprofiling/Profile"/>
		<dsp:param name="sessionBean" bean="/cps/util/CPSSessionBean"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="shotItemPA" param="showItemPA"/>
			<dsp:getvalueof var="showPA" value="${showPA || shotItemPA}"/>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<cp:pageContainer page="reorder">
		<c:choose>
			<c:when test="${isHaveAccess}">
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/material-detail-desplay-section.jsp">
					<dsp:param name="sort" param="sort"/>
					<dsp:param name="class" param="class"/>
					<dsp:param name="accend" param="accend"/>
					<dsp:param name="searchTerm" param="searchTerm"/>
					<dsp:param name="showPA" value="${showPA}"/>
					<dsp:param name="page" param="page"/>
					<dsp:param name="giftlistId" param="giftlistId"/>
				</dsp:include>
			</c:when>
			<c:otherwise>
				<div class="container main-content ${csrClass} minheight">
					<section>
						<div class="row">
							<dsp:include
								page="${originatingRequest.contextPath}/account/gadgets/material-detail-display-header.jsp" />

							<div class="text-center col-xs-12 ml-buttons ">
								<section>
									<div class="alert alert-danger errors">
										<button type="button" class="close" data-dismiss="alert"
											aria-label="Close">
											<span aria-hidden="true"> <span
												class="glyphicon glyphicon-remove-circle"></span>
											</span>
										</button>
										<dsp:include
											page="${originatingRequest.contextPath}/includes/gadgets/info-message.jsp">
											<dsp:param name="key" value="err_mat_no_access" />
											<dsp:param name="notWrap" value="true" />
										</dsp:include>
									</div>
								</section>
							</div>
						</div>
					</section>
				</div>
			</c:otherwise>
		</c:choose>
	</cp:pageContainer>

</dsp:page>
