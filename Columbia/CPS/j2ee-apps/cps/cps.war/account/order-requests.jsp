<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>

	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	<dsp:importbean bean="/cps/commerce/order/SavedCartFormHandler"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>
	
	<cp:pageContainer page="account-approvals">

	    <div class="container main-content ${csrClass} minheight">
			<section>
				<div class="row">
	
					<div class="col-sm-8">
						<ol class="breadcrumb">
							<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">Account Overview</a></li>
							<li class="active">Pending Order Requests</li>
						</ol>
					</div>
	
					<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
						<dsp:param name="email" value="false"/>
						<dsp:param name="print" value="true"/>
					</dsp:include>
	
					<input type="hidden" value="5" id="numPerPage"/>
	
					<div class="col-xs-12">
						<h1 class="mb20"><fmt:message key="account.orders.header.order_requests"/></h1>
					</div>
	
					<dsp:droplet name="/atg/dynamo/droplet/IsNull">
						<dsp:param name="value" bean="Profile.parentOrganization"/>
						<dsp:oparam name="false">
	
							<div class="col-xs-12" id="order-list"></div>
	
							<div class="col-xs-12 text-center">
	
								<nav id="pagination"></nav>
	
								<dsp:form action="" method="post" id="resubmitOrder" formid="resubmitOrder">
									<dsp:input id="resubmitOrderId" type="hidden" bean="ApprovalFormHandler.orderId" value=""/>
									<dsp:input type="hidden" bean="ApprovalFormHandler.resubmitOrder" value="true" priority="-10"/>
								</dsp:form>
	
								<dsp:form action="" method="post" id="deleteOrder" formid="deleteOrder">
									<dsp:input id="deleteOrderId" type="hidden" bean="ApprovalFormHandler.orderId" value=""/>
									<dsp:input type="hidden" bean="ApprovalFormHandler.deleteOrder" value="true" priority="-10"/>
								</dsp:form>
	
								<dsp:form action="" method="post" id="editOrder" formid="editOrder">
									<dsp:input id="editOrderId" type="hidden" bean="SavedCartFormHandler.orderId" value=""/>
									<dsp:input type="hidden" bean="SavedCartFormHandler.editOrder" value="true" priority="-10"/>
								</dsp:form>
	
							</div>
	
							<script type="text/javascript" nonce="${requestScope.nonce}">
								$(document).ready(function() {
									loadRequests();
								});
							</script>
	
						</dsp:oparam>
						<dsp:oparam name="true">
							<p>
								<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="user-no-org"/>
								</dsp:include>
							</p>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</section>
		</div>
	</cp:pageContainer>
</dsp:page>
