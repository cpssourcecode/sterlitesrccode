<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
    <dsp:importbean bean="/cps/util/CPSSessionBean"/>
    <dsp:importbean bean="/cps/util/FileDownloadHandler"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>

    <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
    <c:if test="${not empty selectedOrg}">
        <dsp:getvalueof var="csrClass" value="admin-access"/>
    </c:if>

    <cp:pageContainer page="packing">
        <main id="body">
        <div class="container">
        <div class="row">
            <div class="col-md-8">
                <ol class="breadcrumb">
                    <li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
                    <li class="active">Packing Slips</li>
                </ol>
            </div>

            <dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
                <dsp:param name="email" value="false"/>
                <dsp:param name="print" value="true"/>
            </dsp:include>
        </div>
        <div class="container preload-cover spinner-on-start">
			<div class="loading">
				<i class="fa fa-spinner fa-spin"></i>
			</div>
		</div>
        <dsp:droplet name="/atg/dynamo/droplet/IsNull">
            <dsp:param name="value" bean="Profile.parentOrganization"/>
            <dsp:oparam name="false">
                <h1 class="mb20"><fmt:message key="account.packing.header.packing_slips"/></h1>

                <section>
                    <h3 class="h4 text-nocase">Search Orders</h3>
						<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
							<dsp:param name="messageKey" value="packingSlipMessage" />
							<dsp:oparam name="output">
								<dsp:valueof param="message" valueishtml="true" />
							</dsp:oparam>
							<dsp:oparam name="empty">
								<dsp:valueof param="message" valueishtml="true" />
							</dsp:oparam>
						</dsp:droplet>							
                </section>
                <section>
                    <dsp:form action="#" method="post" id="packing-search">

                        <dsp:input id="packingSearchPageSize" bean="ProfileFormHandler.value.packingSearchPageSize"
                                   type="hidden" value="20"/>
                        <dsp:input id="packingSearchStartIndex" bean="ProfileFormHandler.value.packingSearchStartIndex"
                                   type="hidden" value="0"/>

                        <dsp:input id="packingSearchSortField" bean="ProfileFormHandler.value.packingSearchSortField"
                                   type="hidden" value=""/>
                        <dsp:input id="packingSearchSortOption" bean="ProfileFormHandler.value.packingSearchSortOption"
                                   type="hidden" value=""/>
                        <div class="row mt40">
                            <div class="col-md-2 col-sm-3 mt-20">
                                <div class="radio">
                                	<label>
	                                	<dsp:input bean="ProfileFormHandler.value.packingSearchOption" type="radio"
	                                        	value="orderNumber" name="radioEx1" id="salesOrderId">
	                                        <dsp:tagAttribute name="data-event-click-id" value="eventPackSlipSearchType" />
										</dsp:input> Sales Order #
									</label>
								</div>
                                <div class="radio">
                                	<label>
                                		<dsp:input bean="ProfileFormHandler.value.packingSearchOption" type="radio"
                                        		value="poNumber" name="radioEx1" id="searchPoId">
                                        	<dsp:tagAttribute name="data-event-click-id" value="eventPackSlipSearchType" />
										</dsp:input> PO #
									</label>
								</div>
                            </div>
                            <div class="col-md-10 col-sm-9">
                                <div class="form-group form-group-inline w15">
                                    <dsp:input type="text" bean="ProfileFormHandler.value.packingSearchTerm" maxlength="30"
                                               iclass="form-control" id="packingSearchTerm" >
                                        <dsp:tagAttribute name="placeholder" value="#"/>
                                        <dsp:tagAttribute name="data-event-keypress-id" value="account32"/>
                                    </dsp:input>
                                </div>
                                <dsp:getvalueof var="pOrganization" bean="Profile.parentOrganization" />
                                <c:if test="${not empty pOrganization}">
                                    <dsp:getvalueof var="orgList" bean="Profile.parentOrganization.childOrganizations" />
                                    <c:if test="${not empty orgList}">
                                        <dsp:getvalueof var="billingAccounts" bean="Profile.billingAccounts" />
                                        <div class="form-group form-group-inline w15">
											<span class="select-wrap">
												<dsp:select bean="ProfileFormHandler.value.selectedBillTo" id="selectedBillTo" iclass="form-control">
                                                    <dsp:tagAttribute name="data-event-change-id" value="selectOrg"/>
                                                    <option value="" selected>
														Billing Account
                                                        <!-- <fmt:message key="account.invoices.selectOptions.billTo" /> -->
													</option>
                                                    <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                                                        <dsp:param name="array" value="${billingAccounts}" />
                                                        <dsp:param name="elementName" value="org" />
                                                        <dsp:oparam name="output">
                                                            <dsp:getvalueof var="name" param="org.name" />
                                                            <dsp:getvalueof var="orgId" param="org.id" />
                                                            <option value="${orgId}" title="${name}">
                                                                    ${name}
                                                            </option>
                                                        </dsp:oparam>
                                                    </dsp:droplet>
                                                </dsp:select>
											</span>
                                        </div>
                                    </c:if>
                                </c:if>
                                <dsp:getvalueof var="selectedBilling" bean="ProfileFormHandler.value.selectedBillTo" />
                                <div class="form-group form-group-inline w20">
                                    <dsp:select bean="ProfileFormHandler.value.packingSearchCS" id="searchCS">
                                        <%-- <dsp:tagAttribute name="id" value="address-results"/> --%>
                                        <dsp:include page="/global/gadgets/cs-list.jsp?orgId=${selectedBilling}&showDeletedAddress=true"/>
                                    </dsp:select>
                                    <input id="autocompleteSearchCS" type="text" class="form-control" placeholder="<fmt:message key="account.invoices.selectOptions.shipTo"  />">
                                </div>

                            <!--     <span class="caret caret-select"></span>
                            </span> -->
                                <div class="form-group form-group-inline w20">
                        <!--     <span class="select-wrap"> -->
                                <dsp:select bean="ProfileFormHandler.value.packingSearchTime" id="packingSearchTime"
                                            iclass="form-control">
                                    <option value="" selected data-hidden="true">Date Range</option>
                                    <option value="1">30 days</option>
                                    <option value="2">60 days</option>
                                    <option value="3">90 days</option>
                                    <option value="4">120 days</option>
                                </dsp:select>
                          <!--       <span class="caret caret-select"></span>
                            </span> -->
                                </div>
                                <span class="sm-block mt5">
                            <button class="btn btn-primary xs-block"
                                    data-event-click-id="eventSearchPackingButton" ><fmt:message
                                    key="account.packing.search"/></button>
                            <button id="resetBtn" class="btn btn-info xs-block" data-event-click-id="eventResetSearch"  disabled><fmt:message
                                    key="account.packing.reset"/></button>
					    </span>
                            </div>
                        </div>
                        <dsp:input type="hidden" bean="ProfileFormHandler.value.isSearchButton" value="false"
                                   id="is-search-button"/>
                        <dsp:input type="hidden" bean="ProfileFormHandler.packingSearch" value="true" priority="-10"/>
                    </dsp:form>
                </section>
                <div id="packing-list"></div>
                <div class="text-center">
                    <nav id="pagination"></nav>
                </div>
                </div>
                </main>
                <script type="text/javascript" nonce="${requestScope.nonce}">
                    $(document).ready(function () {                			
                        $("#packingSearchTerm").on('keypress', function (event) {
                            if (event.keyCode == 13) {
                                event.preventDefault();
                            }
                        });
                        
                        searchPackingSlips();
                    });

                    //							var currentPage = 1;
                    //							function changePage(page){
                    //								numPerPage = $("#numPerPage").val();
                    //								currentPage = page;
                    //								$("#packing-list").load("/account/gadgets/packing-list-display.jsp?modal=true&sort="+sort+"&accend="+lastSortAccend+"&numPerPage="+numPerPage+"&page="+currentPage);
                    //								loadPagination();
                    //							}
                    //							function updateOptionDisplay(){
                    //								var csId = $("#csSelector").val();
                    //								var address1 = $("#cs"+csId).val();
                    //								$( "#csSelector").attr("title") = address1;
                    //							}
                </script>

            </dsp:oparam>
            <dsp:oparam name="true">
                <p>
                    <dsp:include page="/includes/gadgets/info-message.jsp">
                        <dsp:param name="key" value="user-no-org"/>
                    </dsp:include>
                </p>
            </dsp:oparam>
        </dsp:droplet>
    </cp:pageContainer>
    <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-err-search-packingslip.jsp" />
</dsp:page>
