<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>
	<cp:pageContainer page="account">
	<main id="body">
	<div class="container">
 		<div class="row">
  			<div class="col-md-8">
			<ol class="breadcrumb">
			  <li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
			  <li class="active">Edit Profile</li>
			</ol>
			</div>
  			
  				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="false"/>
					<dsp:param name="print" value="true"/>
				</dsp:include>
			
	   </div>
		<h1 class="mb20">Edit Profile</h1>
		<div class="hidden" id="success-message"></div>
		<div id="modal-contact-success-div"></div>
		<div class="well mb0">
			<dsp:include page="/account/gadgets/profile-personal-info.jsp"/>
		</div>
        <div class="well mb0">
        	<dsp:include page="/account/gadgets/profile-contact-info.jsp"/>
        </div>
		<div class="well">
			<dsp:include page="/account/gadgets/profile-security-info.jsp"/>
		</div>
	</div>
	</main>
	</cp:pageContainer>
</dsp:page>