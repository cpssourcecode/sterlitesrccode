<dsp:page>
	<dsp:importbean bean="/cps/util/FileUploadHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
	<dsp:importbean bean="/cps/util/RequestAQuoteHandler" />

	<dsp:getvalueof var="scess" param="scess" />
	<dsp:getvalueof var="transient" bean="Profile.transient" />
	<dsp:getvalueof var="securityStatus" bean="Profile.securityStatus" />
	<dsp:getvalueof var="transient" value="${transient or securityStatus eq 1}" />
	<dsp:getvalueof var="isProfileTransient" value="${transient}" />

	<cp:pageContainer page="request-quote">
		<main id="body">
		<div class="container">
			<ol class="breadcrumb">
				<li class="active">Request a Quote</li>
			</ol>
			<h1 class="mb20">Request a Quote</h1>
			<section class="form-horizontal">
				<div class="row">
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label class="control-label col-sm-4">Quote/Job Name</label>
							<div class="col-sm-8">
								<input type="text" id="cu_input_job_name" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Company</label>
							<div class="col-sm-8">
								<input id="cu_input_company" name="cu_input_company" type="text" class="form-control" value="" data-event-keydown-id="account42" 
									data-event-keyup-id="account50"  />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Account #</label>
							<div class="col-sm-8">
								<input id="cu_input_accountNumber" name="cu_input_accountNumber" type="text" class="form-control" value="" data-event-keydown-id="account43" 
									data-event-keyup-id="account51"  />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">First Name*</label>
							<div class="col-sm-8">
								<input id="cu_input_fname" name="cu_input_fname" type="text" class="form-control" value="" data-event-keydown-id="account44" 
									data-event-keyup-id="account52" >
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Last Name*</label>
							<div class="col-sm-8">
								<input id="cu_input_lname" name="cu_input_lname" text="text" class="form-control" value="" data-event-keydown-id="account45" 
									data-event-keyup-id="account53"  />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Phone *</label>
							<div class="col-sm-8">
								<input id="cu_input_phone" name="cu_input_phone" type="text" class="form-control" maxlength="15" value="" data-event-keydown-id="account46" 
									data-event-keyup-id="account54"  />
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<div class="form-group">
							<label class="control-label col-sm-4">Email*</label>
							<div class="col-sm-8">
								<input id="cu_input_email" name="cu_input_email" type="email" class="form-control" maxlength="100" value="" data-event-keydown-id="account47" 
									data-event-keyup-id="account55"  />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Zip Code</label>
							<div class="col-sm-8">
								<input id="cu_input_postal_code" name="cu_input_postal_code" type="text" class="form-control" value="" data-event-keydown-id="account48" 
									data-event-keyup-id="account56"  />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Ship To</label>
							<div class="col-sm-8">
								<input id="cu_input_shipping_address" name="cu_input_shipping_address" type="text" class="form-control" value=""
									data-event-keydown-id="account49"  data-event-keyup-id="account57"  />
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Date Required</label>
							<div class="col-sm-4">
								<div class="input-group form-date">
									<input type="text" class="form-control" id="cu_input_date_required" />
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-4">Req'd Ship Date</label>
							<div class="col-sm-4">
								<div class="input-group form-date">
									<input type="text" class="form-control" id="cu_input_ship_date" />
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
            <div class="quoteError" id="quoteError">
            </div>
			<dsp:form enctype="multipart/form-data" id="requestAQuoteFile" method="post" action="" name="requestAQuoteFile">
				<dsp:include page="/account/gadgets/account-error-message.jsp">
					<dsp:param name="formHandler" bean="RequestAQuoteHandler" />
				</dsp:include>
				<dsp:getvalueof var="requestURI" bean="OriginatingRequest.requestURI" />
				<dsp:getvalueof var="queryString" bean="OriginatingRequest.queryString" />
				<dsp:input type="hidden" id="rqsuccess" bean="RequestAQuoteHandler.requestAQuoteSuccessURL" value="/account/request-quote.jsp?sreqmodal=true&scess=true" />
				<dsp:input type="hidden" id="rqfail" bean="RequestAQuoteHandler.requestAQuoteFailURL" value="/account/request-quote.jsp" />
				<dsp:input type="hidden" id="personal-with-file" bean="RequestAQuoteHandler.personalInfoJSON" value="" />
				<%-- <dsp:input type="hidden" bean="RequestAQuoteHandler.requestAQuoteWithFile" value="true" id="requestWithFileSubmit" /> --%>
				
				<dsp:input type="hidden" id="quote-info" bean="RequestAQuoteHandler.quoteInfoJSON" value="" />
				<span hidden="true">
					<dsp:input type="submit" bean="RequestAQuoteHandler.requestAQuote" value="Upload" id="requestQuoteButton" />
				</span>

				<%-- <span hidden="true">
					<dsp:input type="submit" bean="RequestAQuoteHandler.requestAQuoteWithFile" iclass="btn btn-primary" value="Upload" id="quoteFileUpload" />
				</span> --%>
				<%-- <dsp:input type="submit" bean="RequestAQuoteHandler.requestAQuoteFile" iclass="btn btn-primary" value="Upload" id="subUpload" /> --%>
				<section>
					<div class="row">
						<div class="col-sm-6">
							<p>Request a quote for specific products by uploading a file or submitting the products via the form below. Please upload a document in one of the following formats: doc, pdf, xls, jpg,
								png, tif, or gif.</p>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<%-- <dsp:input bean="RequestAQuoteHandler.requestQuoteFile" iclass="form-control" name="requestquotefile" type="file" id="requestquotefile"/> --%>								
								<dsp:input type="file" bean="RequestAQuoteHandler.requestQuoteFile" iclass="file form-control" name="requestquotefile" >
                                    <dsp:tagAttribute name="multiple" value="true"/>
                                </dsp:input>
							</div>
						</div>
					</div>
					<!-- <p>
						<a class="btn btn-primary xs-block" data-event-click-id="eventSubmitRequestAQuotewithFile" >Submit Quote Request</a>
					</p> -->
				</section>
			</dsp:form>

			<section>
				<div id="list-of-items">
					<div class="row row-narrow hidden-xs">
						<label class="control-label col-sm-1 text-center">Qty</label>
						<label class="control-label col-sm-3">Description</label>
						<label class="control-label col-sm-2">Brand</label>
						<label class="control-label col-sm-2">CPS Part # / MFG #</label>
						<label class="control-label col-sm-2">Your Part #</label>
						<label class="control-label col-sm-2">Size</label>
					</div>
					<div id="first-row" class="row row-narrow count_1">
						<hr class="visible-xs">
						<div class="col-sm-1">
							<div class="form-group">
								<label class="control-label visible-xs">Qty</label>
								<input type="number" class="form-control qty-number" id="input_qty_count_1"  />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label visible-xs">Description</label>
								<input type="text" class="form-control" id="input_description_count_1" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Brand</label>
								<input type="text" class="form-control" id="input_brand_count_1" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">CPS Part # / MFG #</label>
								<input type="text" class="form-control" id="input_cps_part_count_1" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Your Part #</label>
								<input type="text" class="form-control" id="input_your_part_count_1" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Size</label>
								<input type="text" class="form-control" id="input_size_count_1" />
							</div>
						</div>
					</div>
					<div class="row row-narrow count_2">
						<hr class="visible-xs">
						<div class="col-sm-1">
							<div class="form-group">
								<label class="control-label visible-xs">Qty</label>
								<input type="number" max="4" class="form-control qty-number" id="input_qty_count_2" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label visible-xs">Description</label>
								<input type="text" class="form-control" id="input_description_count_2" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Brand</label>
								<input type="text" class="form-control" id="input_brand_count_2" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">CPS Part # / MFG #</label>
								<input type="text" class="form-control" id="input_cps_part_count_2" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Your Part #</label>
								<input type="text" class="form-control" id="input_your_part_count_2" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Size</label>
								<input type="text" class="form-control" id="input_size_count_2" />
							</div>
						</div>
					</div>
					<div class="row row-narrow count_3">
						<hr class="visible-xs">
						<div class="col-sm-1">
							<div class="form-group">
								<label class="control-label visible-xs">Qty</label>
								<input type="number" max="4" class="form-control qty-number" id="input_qty_count_3"/>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<label class="control-label visible-xs">Description</label>
								<input type="text" class="form-control" id="input_description_count_3" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Brand</label>
								<input type="text" class="form-control" id="input_brand_count_3" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">CPS Part # / MFG #</label>
								<input type="text" class="form-control" id="input_cps_part_count_3" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Your Part #</label>
								<input type="text" class="form-control" id="input_your_part_count_3" />
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label class="control-label visible-xs">Size</label>
								<input type="text" class="form-control" id="input_size_count_3" />
							</div>
						</div>
					</div>
				</div>
				<p>
					<button id="add-row-button" class="btn btn-info">Add Row</button>
				</p>
			</section>
			<section>
				<div class="row">
					<div class="col-sm-6">
						<h4 class="text-nocase">Comments</h4>
						<div class="form-group">
							<textarea class="form-control" maxlength="500" id="cu_input_coments"></textarea>
						</div>
					</div>
				</div>
				<p>
					<button class="btn btn-primary xs-block" data-event-click-id="eventSubmitRequestAQuote" >Submit Quote Request</button>
				</p>
			</section>
		</div>
		<%-- <dsp:form enctype="multipart/form-data" id="requestAQuote" method="post" action="" name="requestAQuote">

			<dsp:getvalueof var="requestURI" bean="OriginatingRequest.requestURI" />
			<dsp:getvalueof var="queryString" bean="OriginatingRequest.queryString" />
			<dsp:input type="hidden" id="rqsuccess" bean="RequestAQuoteHandler.requestAQuoteSuccessURL" value="/account/request-quote.jsp?sreqmodal=true&scess=true" />
			<dsp:input type="hidden" id="rqfail" bean="RequestAQuoteHandler.requestAQuoteFailURL" value="/account/request-quote.jsp" />
			<dsp:input type="hidden" id="personal-no-file" bean="RequestAQuoteHandler.personalInfoJSON" value="" />
			<dsp:input type="hidden" id="quote-info" bean="RequestAQuoteHandler.quoteInfoJSON" value="" />
			<span hidden="true">
				<dsp:input type="submit" bean="RequestAQuoteHandler.requestAQuote" value="Upload" id="requestQuoteButton" />
			</span>
		</dsp:form> --%></main>

		<script type="text/javascript" nonce="${requestScope.nonce}">

            var currentVal = 0;
            $(function() {
                currentVal = $(".qty-number").value;
                maxLength = 4;
                $(".qty-number").on("keyup", function (e) {
                    var $field = $(this),
                        val = this.value;
                    if (this.validity && this.validity.badInput || isNaN(val)) {
                        this.value = currentVal;
                        return;
                    }
                    if (val.length > maxLength) {
                        val = val.slice(0, maxLength);
                        $field.val(val);
                    }
                    currentVal = val;
                });
            });

			function submitRequestAQuote() {
				var jMapString = JSON.stringify(personalInfoFormColector());
				$('#personal-with-file').val(jMapString);
				quoteInfoFormColector();
				$('#requestQuoteButton').click();
			}

			function submitRequestAQuotewithFile() {
				var jMapString = JSON.stringify(personalInfoFormColector());
				$('#personal-with-file').val(jMapString);
				$('#quoteFileUpload').click();
			}

			//set values from personal info form to JSON
			function personalInfoFormColector() {
				var jMap = new Object();
				jMap.account = $('#cu_input_accountNumber').val();
				jMap.firstName = $('#cu_input_fname').val();
				jMap.lastName = $('#cu_input_lname').val();
				jMap.emailAddress = $('#cu_input_email').val();
				jMap.company = $('#cu_input_company').val();
				jMap.phone = $('#cu_input_phone').val();
				jMap.po = $('#cu_input_postal_code').val();
				jMap.jobName = $('#cu_input_job_name').val();
				jMap.dateRequired = $('#cu_input_date_required').val();
				jMap.shipDate = $('#cu_input_ship_date').val();
				jMap.shipTo = $('#cu_input_shipping_address').val();
				jMap.comments = $('#cu_input_coments').val();
				return jMap;
			}

			var JSONItemsArray = new Object();
			//set data from each row to JSON
			function quoteInfoFormColector() {
				var i;
				for (i = 1; i < count; i++) {
					var map = new Object();
					map.qty = $('#input_qty_count_' + i).val();
					map.description = $('#input_description_count_' + i).val();
					map.brand = $('#input_brand_count_' + i).val();
					map.cpsPart = $('#input_cps_part_count_' + i).val();
					map.yourPart = $('#input_your_part_count_' + i).val();
					map.size = $('#input_size_count_' + i).val();
					map.comments = $('#cu_input_coments').val();
					var rowItem = "row" + i;
					JSONItemsArray[rowItem] = map;
				}
				var JSONItemsArrayStringify = JSON.stringify(JSONItemsArray);
				$('#quote-info').val(JSONItemsArrayStringify);
			}
			// start count from fourth row
			count = 4;
			var $requestAQuoteItems = $('#first-row');
			$('#add-row-button').click(
					function(event) {
						event.stopPropagation();
						event.preventDefault();
						var newItems = $requestAQuoteItems.clone(true);
						newItems.find('input').val('');
						// adding new row
						$('#list-of-items').append(newItems);
						// remove old id and classes and set new accordinf to the count of
						// row
						newItems.removeClass("count_1");
						newItems.addClass("count_" + count);
						$(".count_" + count + " #input_qty_count_1")
								.removeAttr('id').attr('id',
										'input_qty_count_' + count);
						$(".count_" + count + " #input_description_count_1")
								.removeAttr('id').attr('id',
										'input_description_count_' + count);
						$(".count_" + count + " #input_brand_count_1")
								.removeAttr('id').attr('id',
										'input_brand_count_' + count);
						$(".count_" + count + " #input_cps_part_count_1")
								.removeAttr('id').attr('id',
										'input_cps_part_count_' + count);
						$(".count_" + count + " #input_your_part_count_1")
								.removeAttr('id').attr('id',
										'input_your_part_count_' + count);
						$(".count_" + count + " #input_size_count_1")
								.removeAttr('id').attr('id',
										'input_size_count_' + count);
						count++;
						return false;
					});

			$(function() {
				$('.input-group.form-date').datetimepicker({
					format : 'M/D/YYYY'
				});
			});

			$(document)
					.ready(
							function() {
								var baseParams = removeURLParameterRQ(
										window.location.search, "sreqmodal");
								baseParams = removeURLParameterRQ(baseParams, "_requestid");
                                baseParams = removeURLParameterRQ(baseParams, "productId");
                                baseParams = removeURLParameterRQ(baseParams, "qo");
								baseParams = removeURLParameterOP(baseParams, 'quickomodal');
								orbaseParams = removeURLParameterRQ(baseParams, 'showCS');
								if (baseParams == "") {
									//?
									$('#rqsuccess')
											.val(
													window.location.pathname
															);
									$('#rqfail').val(
											window.location.pathname
													);
								} else {
									//&
									$('#rqsuccess')
											.val(
													window.location.pathname
															);
									$('#rqfail').val(
											window.location.pathname );
								}

								var url = window.location.href;
								url = removeParameterFromUrl(url, "sreqmodal");
								url = removeParameterFromUrl(url, "scess");

								$('#requestQuote').on('hidden.bs.modal', function(e) {
									window.history.pushState('page', 'title', url);
									$("#request-quote-modal").load("/global/modals/request-quote.jsp?scess=false", function() {
										onRequestQuoteModalLoaded();
										eventsRequestQuote();
										initFileUpload();
									});
								})
							});

			function removeParameterFromUrl(url, parameter) {
				return url
						.replace(
								new RegExp('[?&]' + parameter
										+ '=[^&#]*(#.*)?$'), '$1').replace(
								new RegExp('([?&])' + parameter + '=[^&]*&'),
								'$1');
			}

			function removeURLParameterRQ(params, parameter) {
				params = (params.indexOf('?') === 0 ? params.substr(1) : params);
				var pars = params.split(/[&;]/g);
				var prefix = encodeURIComponent(parameter) + '=';
				//reverse iteration as may be destructive
				for (var i = pars.length; i-- > 0;) {
					//idiom for string.startsWith
					if (pars[i].lastIndexOf(prefix, 0) !== -1) {
						pars.splice(i, 1);
					}
				}

				params = pars.join('&');
				return params;
			}
			// Adds error messages to error-messages div and shows it
			function openErrorMessage(errorMessage) {
				$("#error-messages").html(errorMessage);
				$("#error-messages").show();
			}
			// Handles upload/confirmation button toggling
			function updateOrderPadButtons(btn) {
				console.log(btn);
				if (btn === 'importUpload') {
					$('#qoManualSubmit').hide();
					$('#qoUploadSubmit').show();
				} else {
					$('#qoUploadSubmit').hide();
					$('#qoManualSubmit').show();
				}
			}
		</script>

		<dsp:getvalueof var="transient" bean="Profile.transient" />
		<c:choose>
			<c:when test="${isProfileTransient}">
				<script nonce="${requestScope.nonce}">
					$(document)
							.ready(
									function() {
										var cu_input_accountNumber = $("#cu_input_accountNumber");
										cu_input_accountNumber.attr("disabled",
												"");
									});
				</script>
			</c:when>
			<c:otherwise>
				<dsp:getvalueof var="firstName" bean="Profile.firstName" />
				<dsp:getvalueof var="lastName" bean="Profile.lastName" />
				<dsp:getvalueof var="fullName" value="${firstName} ${lastName}" />
				<dsp:getvalueof var="email" bean="Profile.email" />
				<dsp:getvalueof var="company" bean="Profile.parentOrganization.name" />
				<dsp:getvalueof var="accountNumber" bean="Profile.selectedCS.id" />
				<dsp:getvalueof var="phone" bean="Profile.homeAddress.phoneNumber" />
				<dsp:getvalueof var="shipToAddress" bean="Profile.SelectedCs.address1" />
				<dsp:getvalueof var="postalCode" bean="Profile.SelectedCs.postalCode" />

				<script nonce="${requestScope.nonce}">
					$(document)
							.ready(
									function() {
										var disabled = "disabled";
										var cu_input_postal_code = $("#cu_input_postal_code");
										var cu_input_shipping_address = $("#cu_input_shipping_address");
										var cu_input_fname = $("#cu_input_fname");
										var cu_input_lname = $("#cu_input_lname");
										var cu_input_email = $("#cu_input_email");
										var cu_input_company = $("#cu_input_company");
										var cu_input_accountNumber = $("#cu_input_accountNumber");
										var cu_input_phone = $("#cu_input_phone");

										cu_input_postal_code
												.val("${postalCode}");
										cu_input_shipping_address
												.val("${shipToAddress}");
										cu_input_fname.val("${firstName}");
										cu_input_lname.val("${lastName}");
										cu_input_email.val("${email}");
										cu_input_company.val("${company}");
										cu_input_accountNumber
												.val("${accountNumber}");
										cu_input_phone.val("${phone}");
									});
					$("#cu_input_phone").mask("(999) 999-9999");
				</script>
			</c:otherwise>
		</c:choose>
	</cp:pageContainer>
</dsp:page>