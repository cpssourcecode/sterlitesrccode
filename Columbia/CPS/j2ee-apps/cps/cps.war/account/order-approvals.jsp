<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/atg/commerce/approval/ApprovalFormHandler"/>
	
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>
	
	<cp:pageContainer page="account-approvals">

	    <div class="container main-content ${csrClass} minheight" >
			<div class="row">
				<section>
					<div class="col-sm-8">
						<ol class="breadcrumb">
							<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">Account Overview</a></li>
							<li class="active">Pending Order Approvals</li>
						</ol>
					</div>
	
					<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
						<dsp:param name="email" value="false"/>
						<dsp:param name="print" value="true"/>
					</dsp:include>

					<input type="hidden" value="5" id="numPerPage"/>
	
					<div class="col-xs-12">
						<h1><fmt:message key="account.orders.header.order_approvals"/></h1>
					</div>
	
					<dsp:droplet name="/atg/dynamo/droplet/IsNull">
						<dsp:param name="value" bean="Profile.parentOrganization"/>
						<dsp:oparam name="false">
	
							<div class="col-xs-12">
								<section>
									<a href="#" class="btn btn-primary xs-block" data-event-click-id="eventOpenApproveOrdersModal" >Approve Selected</a>
									<a href="#" class="btn btn-info xs-block" data-event-click-id="eventOpenRejectOrdersModal" >Reject Selected</a>
								</section>	
							</div>
	
							<div class="col-xs-12" id="order-list"></div>
	
							<div class="col-xs-12 text-center">
	
								<%--<div class="col-xs-12 text-center">--%>
									<%--<nav>--%>
										<%--<ul class="pagination pagination-lg">--%>
											<%--<li class="prev">--%>
												<%--<a href="#" aria-label="Previous">--%>
													<%--<span class="glyphicon glyphicon-chevron-left"></span>--%>
												<%--</a>--%>
											<%--</li>--%>
											<%--<li class="active"><a href="#">1</a></li>--%>
											<%--<li><a href="#">2</a></li>--%>
											<%--<li><a href="#">3</a></li>--%>
											<%--<li><a href="#">4</a></li>--%>
											<%--<li><span>…</span></li>--%>
											<%--<li class="next">--%>
												<%--<a href="#" aria-label="Next">--%>
													<%--<span class="glyphicon glyphicon-chevron-right"></span>--%>
												<%--</a>--%>
											<%--</li>--%>
										<%--</ul>--%>
									<%--</nav>--%>
								<%--</div>--%>
	
								<nav id="pagination"></nav>
	
								<dsp:form action="" method="post" id="approveOrder" formid="approveOrder">
									<dsp:input id="approveOrderId" type="hidden" bean="ApprovalFormHandler.orderId" />
									<dsp:input type="hidden" bean="ApprovalFormHandler.approveOrder" value="true" priority="-10"/>
								</dsp:form>
	
								<dsp:form action="#" method="post" id="rejectOrder" formid="rejectOrder">
									<dsp:input id="rejectOrderId" value="" type="hidden" bean="ApprovalFormHandler.orderId" />
									<dsp:input id="rejectApproverMessage" value="" type="hidden" bean="ApprovalFormHandler.approverMessage" />
									<dsp:input type="hidden" bean="ApprovalFormHandler.rejectOrder" value="true" priority="-10"/>
								</dsp:form>
	
							</div>
							
							<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-reject-orders.jsp"/>
							<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-approve-orders.jsp"/>
	
							<script type="text/javascript" nonce="${requestScope.nonce}">
								$(document).ready(function() {
									loadApprovals();
								});
	
							</script>
	
						</dsp:oparam>
						<dsp:oparam name="true">
							<p>
								<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="user-no-org"/>
								</dsp:include>
							</p>
						</dsp:oparam>
					</dsp:droplet>
				</section>
			</div>
		</div>
	</cp:pageContainer>
</dsp:page>
