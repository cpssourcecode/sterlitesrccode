<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/droplet/ReorderlistLookupDroplet"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>

	<dsp:droplet name="Switch">
		<dsp:param bean="Profile.transient" name="value"/>
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/?loc=reorder"/>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/IsNull">
				<dsp:param name="value" bean="Profile.parentOrganization"/>
				<dsp:oparam name="true">
					<dsp:droplet name="/atg/dynamo/droplet/Redirect">
						<dsp:param name="url" value="${originatingRequest.contextPath}/account/reorder-list.jsp"/>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

	<cp:pageContainer page="reorder">

		<div class="breadcrumbs-section">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<ol class="breadcrumb">
							<li>
								<a href="${originatingRequest.contextPath}/index.jsp">
									<span>Home</span>
								</a>
							</li>
							<li>
								<a href="${originatingRequest.contextPath}/account/account-landing.jsp">
									<span>My Account</span>
								</a>
							</li>
							<li class="active">Re-order List Detail View</li>
						</ol>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/sidebar.jsp"/>

				<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 page-content">

					<input type="hidden" value="20" id="numPerPage"/>

					<dsp:droplet name="ReorderlistLookupDroplet">
						<dsp:param name="id" param="giftlistId"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="giftlist" param="giftlist"/>
							<dsp:getvalueof var="giftlistId" param="giftlist.id"/>
							<dsp:getvalueof var="eventName" param="giftlist.eventName"/>
							<dsp:getvalueof var="cs" param="giftlist.cs"/>
							<dsp:getvalueof var="description" param="giftlist.description"/>
							<input type="hidden" value="${giftlistId}" id="giftlistIdValue"/>

							<h2>${eventName}</h2>
							<div class="row">
								<div class="col-lg-12 block-bordered">
								</div>
								<dsp:getvalueof var="del" param="del"/>
								<dsp:getvalueof var="upd" param="upd"/>
								<c:if test="${del == 'success'}">
									<div class="alert alert-success">
										<p>Item(s) deleted successfully.</p>
									</div>
								</c:if>
								<c:if test="${upd == 'success'}">
									<div class="alert alert-success">
										<p>Reorder list updated successfully.</p>
									</div>
								</c:if>
								<dsp:include page="/account/gadgets/account-error-message.jsp">
									<dsp:param name="formHandler" bean="GiftlistFormHandler"/>
								</dsp:include>
								<dsp:include page="/account/gadgets/account-error-message.jsp">
									<dsp:param name="formHandler" bean="CartModifierFormHandler"/>
								</dsp:include>

								<dsp:form action="${originatingRequest.contextPath}/account/reorder-detail.jsp?giftlistId=${giftlistId}" method="post" id="reorder-details-form" formid="reorder-details-form">
									<div class="col-lg-12 block-bordered">
										<h3>List Details</h3>
										<div class="row">
											<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
												<div class="form-horizontal">
													<div class="form-group">
														<label for="eventName" class="col-sm-2 control-label">List Name</label>
														<div class="col-sm-10">
															<dsp:input type="text" bean="GiftlistFormHandler.eventName" value="${eventName}" iclass="form-control" id="eventName"/>
														</div>
													</div>

													<input type="hidden" value="${cs}" id="currentCS"/>
													<dsp:droplet name="/cps/droplet/CSSelectionDroplet">
														<dsp:oparam name="output">
															<div class="form-group">
																<label class="col-sm-2 control-label">CS</label>
																<div class="col-sm-10">
																	<dsp:select id="cs" bean="GiftlistFormHandler.cs" iclass="selectpicker form-control input-lg">
																		<dsp:tagAttribute name="data-style" value="btn-default"/>
																		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
																			<dsp:param name="array" param="associatedCS"/>
																			<dsp:param name="elementName" value="cs"/>
																			<dsp:oparam name="output">
																				<dsp:getvalueof var="address1" param="cs.address1"/>
																				<dsp:getvalueof var="csId" param="cs.id"/>
																				<dsp:option value="${csId}">${address1}</dsp:option>
																			</dsp:oparam>
																		</dsp:droplet>
																	</dsp:select>
																</div>
															</div>
														</dsp:oparam>
													</dsp:droplet>


													<div class="form-group">
														<label for="description" class="col-sm-2 control-label">Notes</label>
														<div class="col-sm-10">
															<dsp:textarea bean="GiftlistFormHandler.description" id="description" iclass="form-control" cols="30" rows="3">${description}</dsp:textarea>
														</div>
													</div>

													<hr>

													<div class="form-group">
														<div class="col-sm-12">
															<ul class="list-inline pull-right">
																<li>
																	<a class="btn btn-midnight" href="/global/modals/modal-import.jsp?giftlistId=${giftlistId}&modal=true" data-toggle="modal" data-target="#import-items-modal">Import Items</a>
																</li>
																<li>
																	<a class="btn btn-midnight" href="/global/modals/modal-list-share.jsp?giftlistId=${giftlistId}&modal=true" data-toggle="modal" data-target="#share-list-modal">Share List</a>
																	<%--<button type="submit" class="btn btn-midnight">Share List</button>--%>
																</li>
																<li>
																	<button type="submit" class="btn btn-success validate" value="Update List">Update List</button>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-lg-12 block-bordered">

										<div id="emptyResults" style="display:none;" class="alert alert-warning">
											<dsp:include page="/includes/gadgets/info-message.jsp">
												<dsp:param name="key" value="user-no-searchResults"/>
											</dsp:include>
										</div>

										<div class="category-filter">
											<nav class="text-center" id="pagination"></nav>

											<div class="row">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<p class="pull-left category-filter-attr"><i class="text-muted">Sort:</i>
														<select class="selectpicker" data-width="150px" name="select-value" data-event-change-id="eventSortReorderDetails"  id="sort-reorder-details">
															<option value="displayName">Title</option>
															<option value="productId">Item #</option>
															<option value="catalogRefId">Short Code</option>
															<option value="catalogRefId">Customer Alias</option>
														</select>
													</p>
													<div class="input-group">
														<input type="text" class="form-control" value="" id="reorderDetailsSearchTerm" placeholder="Search orders by CS, Item#, Keyword"/>
														<span class="input-group-btn">
															<button class="btn btn-success" type="button" data-event-click-id="eventSearchListDetails" >Search</button>
															<button class="btn btn-midnight" type="button" data-event-click-id="eventResetDetailsSearch" >Reset</button>
														</span>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
													<p class="category-filter-attr"><i class="text-muted">Selected items:</i>
														<a class="btn btn-link btn-dashed" href="#">
															<span>Add to Cart</span>
														</a>
														<a class="btn btn-link btn-dashed" href="#">
															<span>Check Price/Avail.</span>
														</a>
														<a class="btn btn-link btn-dashed" href="#">
															<span>Remove</span>
														</a>
													</p>
												</div>
												<div class="col-lg-12">
													<div class="checkbox">
													<label class="checkbox-custom checkbox-toggle-prodict" data-initialize="checkbox">
														<input class="sr-only" type="checkbox" value="">
														<span class="checkbox-label"><i class="text-muted">Toggle All</i>
														</span>
													</label>
												</div>
												</div>
											</div>
										</div>

										<div class="category-list" id="reorder-items"></div>

									</div>

									<dsp:input type="hidden" bean="GiftlistFormHandler.removeGiftitemIds" value="" id="removeList"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.giftlistId" value="${giftlistId}"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.updateGiftlistItemsSuccessURL" value="${originatingRequest.contextPath}/account/reorder-detail.jsp?giftlistId=${giftlistId}"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.updateGiftlistItemsErrorURL" value="${originatingRequest.contextPath}/account/reorder-detail.jsp?giftlistId=${giftlistId}"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.updateGiftlistItems" value="true" priority="-10"/>

								</dsp:form>

								<dsp:form action="${originatingRequest.contextPath}/account/reorder-detail.jsp?giftlistId=${giftlistId}" method="post" id="reorder-details-add" formid="reorder-details-add">
									<%-- <dsp:input type="hidden" bean="CartModifierFormHandler.giftlistId" value="giftlistId"/> --%>
									<dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsList"/>
									<dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyList"/>
									<dsp:input type="hidden" bean="CartModifierFormHandler.giftIds" value="" id="giftIds"/>
									<dsp:input type="hidden" bean="CartModifierFormHandler.addItemToOrder" value="true"/>
								</dsp:form>
								<dsp:form action="${originatingRequest.contextPath}/account/reorder-detail.jsp?giftlistId=${giftlistId}" method="post" id="reorder-details-check-price" formid="reorder-details-check-price">
									<dsp:input type="hidden" bean="GiftlistFormHandler.addItemToPA" value="true"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.itemIdsList" value="" id="paSkuIdsList" name="itemIdsList"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.quantities" value="" id="paQtyList" name="quantities"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.addItemToPASuccessURL" value="/checkout/pa.jsp" name="addItemToPASuccessURL"/>
									<dsp:input type="hidden" bean="GiftlistFormHandler.addItemToPAErrorURL" value="/account/reorder-detail.jsp?giftlistId=${giftlistId}" name="addItemToPAErrorURL"/>
								</dsp:form>

							</div>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</div>
		</div>

		<script type="text/javascript" nonce="${requestScope.nonce}">
			function onReorderDetailDisplayLoaded(){
			    if($("#total_count").val() == '0'){
                    $("#emptyResults").show();
                }
                loadPagination();
			}

			$(document).ready(function() {
				$("#reorder-items").load("/account/gadgets/reorder-detail-display.jsp?page=1&giftlistId=${giftlistId}&sort=displayName&accend=true", function () {
                    onReorderDetailDisplayLoaded();
                    eventsReorderDetailDisplay();
                });
				selectList();
//				if ($("#reorderDetailsSearchTerm").val() == ""){
//					$("#reorderDetailsSearchTerm").val("Item # or Keyword");
//				}
//				$('#reorderDetailsSearchTerm').focus(function(){
//					if($(this).val() == 'Item # or Keyword'){
//						$(this).val('');
//					}
//				}).blur(function(){
//					if ($(this).val() == ''){
//						$(this).val('Item # or Keyword');
//					}
//				});
			});
			var currentPage = 1;
			function changePage(page){
				numPerPage = $("#numPerPage").val();
				currentPage = page;
				$("#reorder-items").load("/account/gadgets/reorder-detail-display.jsp?modal=true&giftlistId=${giftlistId}&sort="+sortDetails+"&accend=true&numPerPage="+numPerPage+"&page="+currentPage+"&searchTerm="+encodeURIComponent(detailsSearchTerm), function () {
                    onReorderDetailDisplayLoaded();
                    eventsReorderDetailDisplay();
                });
				loadPagination();
			}
		</script>

	</cp:pageContainer>

</dsp:page>
