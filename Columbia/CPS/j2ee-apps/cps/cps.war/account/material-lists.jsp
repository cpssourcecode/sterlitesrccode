<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/commerce/gifts/GiftlistFormHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>

	<cp:pageContainer page="reorder">
		<div class="container main-content ${csrClass} minheight">
			<input type="hidden" value="3" id="numPerPage" />

			<dsp:form action="" method="post" id="deleteListForm" formid="deleteListForm">
				<dsp:input type="hidden" id="listToDelete" bean="GiftlistFormHandler.removeListId" value=""/>
				<dsp:input type="hidden" bean="GiftlistFormHandler.deleteList" value="true" priority="-10"/>
			</dsp:form>
		
			<section>
				<div class="row">
					<div class="col-sm-8">
						<ol class="breadcrumb">
							<li>
								<a href="${originatingRequest.contextPath}/account/account-landing.jsp">
									<fmt:message key="account.breadcrumb.my_account"/>
								</a>
							</li>
							<li class="active">
								<a href="#" data-event-click-id="eventResetMaterialListSearch" >
									<fmt:message key="account.material.header.material_lists"/>
								</a>
							</li>
						</ol>
					</div>
					<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
						<dsp:param name="email" value="false"/>
						<dsp:param name="print" value="true"/>
					</dsp:include>
	
					<div class="col-xs-12">
						<h1><fmt:message key="account.material.header.material_lists"/></h1>
					</div>
					<div class="col-xs-12">
						<cp:repositoryMessage key="materialListsDescription"/>
					</div>
	
					<div class="col-xs-12">
						<section>
						<form>
							<div class="text-center mt40">
								<button class="btn btn-primary xs-block" data-dismiss="modal"
									data-toggle="modal" data-target="#modal-matlist"
									data-event-click-id="eventShowNewMaterialListModalPopup" >
									<fmt:message key="account.material.create_new_material_list" />
								</button>
								<div class="form-group form-group-inline w45">
									<h4 class="text-nocase">
										<fmt:message key="account.material.search_material_lists" />
									</h4>
									<input type="text" class="form-control" placeholder="Material List Name"
										   id="reorderListSearchTerm"
										   data-event-keypress-id="account16" >
								</div>
								<button class="btn btn-primary xs-block" id="btn-search" data-event-click-id="eventSearchReorderLists" >
									<fmt:message key="account.material.search" />
								</button>
								<button class="btn btn-info xs-block" type="button" id="btn-reset" disabled="" data-event-click-id="eventResetMaterialListSearch" >
									<fmt:message key="account.material.reset" />
								</button>
							</div>
						</form>
						</section>
					</div>
					<div class="container preload-cover">
						<div class="loading preload-spinner custom-loading">
							<i class="fa fa-spinner fa-spin"></i>
						</div>
					</div>
	
					<div class="col-xs-12" id="material-lists"></div>
	
					<div class="col-xs-12 text-center">
						<nav id="pagination"></nav>
					</div>
				</div>
			</section>
		</div>

		<div id="add-new-list-modal">
			<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-new-material-list.jsp"/>
		</div>
		<div id="modal-giftlist-count"></div>
		<%-- <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
			<dsp:param name="messageId" value="ml-added" />
			<dsp:param name="divId" value="_materialListCreated" />
			<dsp:param name="isReset" value="false" />
		</dsp:include> --%>

		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(function () {
				var d = new Date();
				var t = d.getTime();
				$('.preload-cover').addClass('loader-active');
			    $('.preload-cover .loading').show();
				$("#material-lists").load("/account/gadgets/material-list-display.jsp?page=1&sort=eventName&accend=true&t=" + t, function () {
				    materialListDisplay();
					eventsMaterialListDisplay();
					importHelpInit();
                    eventMaterialListUpload();
                    $('.preload-cover').removeClass('loader-active');
    			    $('.preload-cover .loading').hide();
                });
				
				if(getURLParameter("matmodal") == 'true'){
					$("#link-newMaterialListModal").click();
				}
			});
			function getURLParameter(name) {
				  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
				}

			var currentPage = 1;

			function changePage(page) {
				$('.preload-cover').addClass('loader-active');
			    $('.preload-cover .loading').show();
				var d = new Date();
				var t = d.getTime();
				numPerPage = $("#numPerPage").val();
				currentPage = page;
				$("#material-lists").load("/account/gadgets/material-list-display.jsp?modal=true&sort=" + sort + 
											"&accend=" + !lastSortAccend + "&numPerPage=" + numPerPage + 
											"&page=" + currentPage + 
											"&searchTerm=" + encodeURIComponent(globalSearchTerm) + 
											"&searchCS=" + encodeURIComponent(globalSearchCS) + "&t=" + t, function () {
                    materialListDisplay();
					eventsMaterialListDisplay();
					importHelpInit();
                    eventMaterialListUpload();
                    $('.preload-cover').removeClass('loader-active');
    			    $('.preload-cover .loading').hide();
                });
				loadPagination();
			}


		</script>

	</cp:pageContainer>

</dsp:page>
