<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/cps/droplet/RoleLookupDroplet" />
	
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>
	
	<cp:pageContainer page="account">
		<main id="body">
			<dsp:getvalueof var="updateSuccessful" bean="MultiUserAddFormHandler.addAdminSuccessful"/>
			<c:if test="${updateSuccessful}">
				<script type="text/javascript" nonce="${requestScope.nonce}">
                    jQuery(document).ready(function(){
                        $.ajax({
                            type: "GET",
                            url: "/global/modals/modal-admin-success.jsp",
                            dataType: 'html',
                            data: ({action: 'loadModalDiv'}),
                            complete: function () {
                                console.log("open modal");
                                $("#link-adminSuccess").click();
                                //setTimeout(function(){closeModalDialog();}, 2000);
                            },
                            success: function (data) {
                                data = $.trim(data);
                                $("#modal-admin-success-div").html(data);
                            }
                        });
                    });
				</script>
			</c:if>
			<div id="modal-admin-success-div"></div>

			<div class="container ${csrClass}">
				<ol class="breadcrumb">
					<li class="active">Manage Companies</li>
				</ol>

				<h1 class="mb20">Manage Companies</h1>

				<section>

					<div class="text-center mt40">
						<div class="form-group form-group-inline w45">
							<h4 class="text-nocase">Search Companies</h4>
							<input type="text" id="searchValue" placeholder="Company Name or Account #" class="form-control"
								   data-event-keypress-id="account11" >
							<%--<input type="text" class="form-control" placeholder="Company Name or Account #">--%>
						</div>
							<button href="javascript:void(0);" data-event-click-id="eventSearchCompanies"
									class="btn btn-primary xs-block">Search</button>


							<button id="search-companies-reset-btn" disabled href="javascript:void(0);" data-event-click-id="eventResetSearch"
									class="btn btn-info xs-block">Reset</button>
					</div>

				</section>

				<div id="company-display">
					<script nonce="${requestScope.nonce}">
                        $('.preload-cover').addClass('loader-active');
                        $('.preload-cover .loading').show();
					</script>
					<div class="container preload-cover">
						<div class="loading">
							<i class="fa fa-spinner fa-spin"></i>
						</div>
					</div>
					<dsp:include page="/account/gadgets/manage-companies-display.jsp?page=1&sort=companyName&order=%2B"/>
					<script nonce="${requestScope.nonce}">
						onManageCompaniesLoaded();
					</script>
				</div>

				<dsp:form id="accessAccount" method="post" action="/account/manage-companies.jsp">
					<dsp:input type="hidden" id="access-account" name="company" bean="MultiUserAddFormHandler.company"/>
					<dsp:input type="hidden" id="hide-warning" name="warningMessage" bean="MultiUserAddFormHandler.warningMessage"/>
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.accessAccountSuccessURL" value="/account/account-landing.jsp"/>
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.accessAccountErrorURL" value="/account/manage-companies.jsp"/>
					<dsp:input type="hidden" bean="MultiUserAddFormHandler.accessAccount" value="true"/>
				</dsp:form>
				<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
				<script src="${staticContentPrefix}/js/manage-company${build_version}.js"></script>
				<script nonce="${requestScope.nonce}">
                    $('.preload-cover').removeClass('loader-active');
                    $('.preload-cover .loading').hide();
				</script>

			</div>
		</main>

	</cp:pageContainer>
</dsp:page>