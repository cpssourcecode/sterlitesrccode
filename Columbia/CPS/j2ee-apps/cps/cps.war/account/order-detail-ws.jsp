<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/cps/droplet/CPSOrderDetailDroplet"/>
	<dsp:importbean bean="/cps/droplet/CPSProductFromIdentifierDroplet"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/commerce/order/OrderLookup"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>
	<dsp:importbean bean="/cps/reorders/AutoOrderFormHandler"/>

	<dsp:getvalueof var="orderId" value="${vsg_utils:aesDecrypt(param.orderId)}"/>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<cp:pageContainer page="checkout">
		<main id="body">
			<div class="container ${csrClass}">
				<div class="row">
					<div class="col-md-8">
						<ol class="breadcrumb">
							<li><a href="${originatingRequest.contextPath}/account/account-landing.jsp">My Account</a></li>
							<li class="active">Order Detail</li>
						</ol>
					</div>
					<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
						<dsp:param name="print" value="true"/>
					</dsp:include>
				</div>

				<h1 class="mb20">Order Detail</h1>

				<section>
					<dsp:droplet name="CPSOrderDetailDroplet">
						<dsp:param name="orderId" value="${orderId}"/>
						<dsp:oparam name="output">

							<dsp:setvalue paramvalue="result" param="order"/>

							<dsp:getvalueof var="order" param="order"/>
							<dsp:getvalueof var="autoOrderId" param="autoOrderId"/>

							<h2 class="h4 text-nocase">${orderId}</h2>

							<div class="row">
								<div class="col-md-2 col-sm-3">
									<dl class="row row-narrow">
										<dt class="col-sm-12 col-xs-3">Ordered On</dt>
										<dd class="col-sm-12 col-xs-9"><dsp:valueof param="order.orderDate" converter="date" format="MM/dd/YYYY"/></dd>
									</dl>
								</div>
								<div class="col-md-2 col-sm-3">
									<dl class="row row-narrow">
										<dt class="col-sm-12 col-xs-3"># of Items</dt>
										<dd class="col-sm-12 col-xs-9"><dsp:valueof param="order.numberOfItems"/></dd>
									</dl>
								</div>
								<div class="col-md-2 col-sm-3">
									<dl class="row row-narrow">
										<dsp:include page="/account/gadgets/order-detail-ws-ship-to-address.jsp">
											<dsp:param name="order" param="order"/>
										</dsp:include>
									</dl>
								</div>
								<div class="col-md-2 col-sm-3">
									<dl class="row row-narrow">
										<dt class="col-sm-12 col-xs-3">Total</dt>
										<dd class="col-sm-12 col-xs-9">
											<ul class="list-unstyled">
												<li><strong>Subtotal:</strong> <dsp:valueof param="order.itemTotalAmount" converter="currencyConversion"/></li>
												<li><strong>Shipping:</strong> <dsp:valueof param="order.shippingHandlingTotalAmount" converter="currencyConversion"/></li>
												<li><strong>Tax:</strong> <dsp:valueof param="order.taxTotalAmount" converter="currencyConversion"/></li>
											</ul></dd>
									</dl>
								</div>
								<div class="col-md-2 col-sm-3">
									<dl class="row row-narrow">
										<dt class="col-sm-12 col-xs-3">Total Amount</dt>
										<dd class="col-sm-12 col-xs-9"><dsp:valueof param="order.orderTotalAmount" converter="currencyConversion"/></dd>
									</dl>
								</div>
							</div>
						</dsp:oparam>
						<dsp:oparam name="error">
							<span class=profilebig>ERROR:
								<dsp:valueof param="errorMsg">no error message</dsp:valueof>
							</span>
						</dsp:oparam>
					</dsp:droplet>
				</section>

				<dsp:droplet name="/cps/droplet/AccessRightDroplet">
					<dsp:param name="profile" bean="Profile"/>
					<dsp:param name="accessRightKey" value="order-detail-add-items"/>
					<dsp:oparam name="false">
						<dsp:getvalueof var="forbidAddItem" value="true"/>
					</dsp:oparam>
				</dsp:droplet>

				<section>
					<div class="text-center">
						<c:choose>
							<c:when test="${forbidAddItem}">
								<div class="form-group form-group-inline">
									<input type="text" class="form-control input-qty text-center display-inline-xs" value="30"> days
									<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-text" ><i class="fa fa-info-circle" aria-hidden="true"></i></a>
								</div>
								<a class="btn btn-info xs-block" href="#" data-target="#permissionDenied">Auto-Reorder</a>

								<c:if test="${not empty autoOrderId}">
									<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-text" class="btn btn-info xs-block" 
											data-event-click-id="eventCancelAutoOrder" data-auto-order-id="${autoOrderId}" >
										<fmt:message key="account.orders.cancel_auto_order"/>
									</a>
								</c:if>

								<a class="btn btn-primary xs-block" href="#" data-toggle="modal"
									   data-target="#permissionDenied">Add Selected to Cart</a>

								<a class="btn btn-info xs-block" href="#" data-toggle="modal"
									   data-target="#permissionDenied">Add Selected to Material List</a>
							</c:when>
							<c:otherwise>
								<div class="form-group form-group-inline">
									<input id="schedule-days" name="schedule-days" type="number" class="form-control input-qty text-center display-inline-xs" value="30"> days
									<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-text"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
								</div>
								<button class="btn btn-info xs-block" data-event-click-id="eventAddSelectedToReorder" >Auto-Reorder</button>

								<c:if test="${not empty autoOrderId}">
									<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-text" class="btn btn-info xs-block" data-event-click-id="eventCancelAutoOrder" data-auto-order-id="${autoOrderId}" >
										<fmt:message key="account.orders.cancel_auto_order"/>
									</a>
								</c:if>

								<a class="btn btn-primary xs-block" data-event-click-id="eventAddSelectedToCart" >Add Selected to Cart</a>

								<a class="btn btn-info xs-block add-to-mat-btn">Add Selected to Material List</a>
							</c:otherwise>
						</c:choose>
					</div>
				</section>

				<section id="com-items-pagination-block">
					<dsp:include page="/account/gadgets/order-detail-ws-items-pagination-list.jsp">
						<dsp:param name="orderId" value="${orderId}"/>
					</dsp:include>
					<script nonce="${requestScope.nonce}">
                        function orderDetailsWSItemsLoaded(){
                            $(document).ready(function () {
                                jQuery("#com-items-pagination-block").scrollbar();
                                $("#com-items-pagination-block [data-toggle='popover']").popover();
                            });
                        }
                        orderDetailsWSItemsLoaded();
					</script>
				</section>

			</div>
		</main>

		<dsp:form method="post" id="order-details-add" formid="order-details-add">
			<dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsListGL"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyListGL"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.giftIds" value="false"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.orderItems" value="true"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.addItemToOrder" value="true" priority="-10"/>
		</dsp:form>

	    <dsp:form method="post" id="auto-order-details-add" formid="auto-order-details-add">
		    <dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsListGLAuto"/>
		    <dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyListGLAuto"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.scheduleDays" value="" id="scheduleDays"/>
			<dsp:input type="hidden" bean="CartModifierFormHandler.giftIds" value="false"/>
		    <dsp:input type="hidden" bean="CartModifierFormHandler.orderItems" value="true"/>
		    <dsp:input type="hidden" bean="CartModifierFormHandler.addItemToAutoOrder" value="true" priority="-10"/>
	    </dsp:form>

		<dsp:form id="cancel-auto-order" formid="cancel-auto-order" method="POST">
			<dsp:input type="hidden" bean="AutoOrderFormHandler.autoOrderId" value="" id="addAutoOrderID"/>
			<dsp:input type="hidden" bean="AutoOrderFormHandler.cancelAutoOrder" value="Submit" priority="-10"/>
		</dsp:form>

		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-err-item-not-selected.jsp"/>
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-success-cancel-reorder.jsp"/>

		<div id="modal-add-to-material-list-div"></div>

		<script nonce="${requestScope.nonce}">
			$(document).ready(function () {

				var selectedItemStorage = {};

				checkboxer({
					delegate: "#com-items-pagination-block",
					header: "#check-all-items-checkbox :checkbox",
					content: "tbody .check :checkbox:enabled",
					checkListener: function () {
						if ($(this).checkbox("isChecked")) {
							selectedItemStorage[this.value] = $(this).closest("tr").find(".item-qty").text().trim();
							} else {
							delete selectedItemStorage[this.value];
						}
                        var checkboxCount = $(" .check label :checkbox:enabled").length;
                        var checkedCount = $(" .check label :checkbox:enabled:checked").length;
                        if (checkedCount == checkboxCount) {
                            $("#check-all-items-checkbox :checkbox").checkbox("check");
                        } else {
                            $("#check-all-items-checkbox :checkbox").checkbox("uncheck");
                        }
					}
				});

				function restoreTableCheckboxes() {
					for (var sku in selectedItemStorage) {
						$(":checkbox[value=" + sku + "]").checkbox("check");
					}
					var checkboxCount = $(" .check label :checkbox:enabled").length;
					var checkedCount = $(" .check label :checkbox:enabled:checked").length;
					if (checkedCount == checkboxCount) {
						$("#check-all-items-checkbox :checkbox").checkbox("check");
					} else {
						$("#check-all-items-checkbox :checkbox").checkbox("uncheck");
					}
				}

				$(".add-to-mat-btn").click(function () {
					onAddToMaterialLClickMultiPages();
				});

				function onAddToMaterialLClickMultiPages() {
					if ($.isEmptyObject(selectedItemStorage)) {
						showAlertItemNotSelectedPopup();
						return;
					}
					var itemsInfo = Object.keys(selectedItemStorage).map(function (value) {
						return value + "=" + selectedItemStorage[value];
					}).join(",");
					resetTableCheckboxes();
					showAddToMaterialListModal(itemsInfo);
				}


				$("#com-items-pagination-block").on("click", "nav .pagination li a", function (e) {
					e.preventDefault();
				});
				$("#com-items-pagination-block").on("click", "nav .pagination li.page-number", function (e) {
					var num = $(this).text();
					$("#com-items-pagination-block").load("/account/gadgets/order-detail-ws-items-pagination-list.jsp", {
						orderId: '${orderId}',
						pageNumber: num
					}, function () {
                        orderDetailsWSItemsLoaded();
						restoreTableCheckboxes();
						savePaginationState(num);
					});
				});
				$("#com-items-pagination-block").on("click", "nav .pagination li.prev", function (e) {
					var num = parseInt($("#com-items-pagination-block nav .pagination li.active").text()) - 1;
					$("#com-items-pagination-block").load("/account/gadgets/order-detail-ws-items-pagination-list.jsp", {
						orderId: '${orderId}',
						pageNumber: num
					}, function () {
                        orderDetailsWSItemsLoaded();
                        restoreTableCheckboxes();
						savePaginationState(num);
					});
				});
				$("#com-items-pagination-block").on("click", "nav .pagination li.next", function (e) {
				    var num = parseInt($("#com-items-pagination-block nav .pagination li.active").text()) + 1;
					$("#com-items-pagination-block").load("/account/gadgets/order-detail-ws-items-pagination-list.jsp", {
						orderId: '${orderId}',
						pageNumber: num
					}, function () {
                        orderDetailsWSItemsLoaded();
                        restoreTableCheckboxes();
						savePaginationState(num);
					});
				});

			});

			function savePaginationState(pageNumber) {
				var href = "";
				if (window.location.search && window.location.search.length) {
					if (window.location.search.indexOf("pageNumber=") != -1) {
						href = window.location.href.replace(/[&]?pageNumber=\d*/, "&pageNumber=" + pageNumber);
					} else {
						href = window.location.href + "&pageNumber=" + pageNumber;
					}
				} else {
					href = window.location.href + "?pageNumber=" + pageNumber;
				}
				window.history.replaceState({}, "Title", href);
			}


		</script>
		<!-- main container end -->
	</cp:pageContainer>
</dsp:page>
