<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/cps/util/CPSSessionBean"/>
    <dsp:importbean bean="/cps/commerce/order/SavedCartFormHandler"/>

    <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
    <c:if test="${not empty selectedOrg}">
        <dsp:getvalueof var="csrClass" value="admin-access"/>
    </c:if>

    <cp:pageContainer page="account-approvals">
        <main id="body">
            <div class="container">
                <ol class="breadcrumb">
                    <li><a href="/account/account-landing.jsp">My Account</a>
                    </li>
                    <li class="active">Saved Carts</li>
                </ol>

                <h1 class="mb20">Saved Carts</h1>

                <%--<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">--%>
                    <%--<dsp:param name="email" value="false"/>--%>
                    <%--<dsp:param name="print" value="true"/>--%>
                <%--</dsp:include>--%>

                <input type="hidden" value="10" id="numPerPage"/>

                <dsp:droplet name="/atg/dynamo/droplet/IsNull">
                    <dsp:param name="value" bean="Profile.parentOrganization"/>
                    <dsp:oparam name="false">
                        <div id="order-list"></div>

                        <div class="col-xs-12 text-center">

                            <nav id="pagination"></nav>

                            <dsp:form action="" method="post" id="editOrder" formid="editOrder">
                                <dsp:input id="editOrderId" type="hidden" bean="SavedCartFormHandler.orderId" value=""/>
                                <dsp:input type="hidden" bean="SavedCartFormHandler.editOrder" value="true"
                                           priority="-10"/>
                            </dsp:form>

                            <dsp:form action="" method="post" id="deleteOrder" formid="deleteOrder">
                                <dsp:input id="deleteOrderId" type="hidden" bean="SavedCartFormHandler.orderId"
                                           value=""/>
                                <dsp:input type="hidden" bean="SavedCartFormHandler.deleteOrder" value="true"
                                           priority="-10"/>
                            </dsp:form>

                        </div>

                        <script type="text/javascript" nonce="${requestScope.nonce}">
                            $(document).ready(function () {
                                loadSavedCarts();
                            });
                        </script>

                    </dsp:oparam>
                    <dsp:oparam name="true">
                        <p>
                            <dsp:include page="/includes/gadgets/info-message.jsp">
                                <dsp:param name="key" value="user-no-org"/>
                            </dsp:include>
                        </p>
                    </dsp:oparam>
                </dsp:droplet>
            </div>
        </main>
    </cp:pageContainer>
</dsp:page>
