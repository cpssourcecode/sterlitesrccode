<dsp:page>

    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/util/FileUploadHandler"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/cps/util/CPSSessionBean"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:getvalueof var="requestURI" bean="OriginatingRequest.requestURI"/>
    <dsp:getvalueof var="queryString" bean="OriginatingRequest.queryString"/>

    <%--     <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/> --%>
    <c:if test="${not empty selectedOrg}">
        <dsp:getvalueof var="csrClass" value="admin-access"/>
    </c:if>
    
    <cp:pageContainer page="pdp">
        <main id="body">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li class="active">Order Pad</li>
                        </ol>
                    </div>
                    <dsp:include page="/global/gadgets/page-actions.jsp">
                        <dsp:param name="isShowEmail" value="false"/>
                    </dsp:include>
                </div>

                <h1 class="mb20">Order Pad</h1>
                <div class="mb20">
					<cp:repositoryMessage key="orderPadDescription"/>
				</div>

                <c:if test="${not param['quickodropdown']}">
                    <dsp:include page="/account/gadgets/account-error-message.jsp">
                        <dsp:param name="formHandler" bean="CartModifierFormHandler"/>
                        <dsp:param name="alertClass" value="alert alert-danger alert-alt"/>
                    </dsp:include>
                    <dsp:include page="/account/gadgets/account-error-message.jsp">
                        <dsp:param name="formHandler" bean="FileUploadHandler"/>
                        <dsp:param name="alertClass" value="alert alert-danger alert-alt"/>
                    </dsp:include>
                    <div style="display: none;" id="qo-pad-message-error-div" class="col-xs-12">
	                    <div class="alert alert-danger">
	                        <div id="qo-pad-message-error"></div>
	                        <div id="qo-pad-errors"></div>
	                    </div>
                	</div>
                </c:if>

                <div class="col-xs-12">
                    <dsp:form iclass="well" enctype="multipart/form-data" id="qorderUpload" method="post" action="">
                        <h4 class="text-nocase">Import/Upload List</h4>
                        <div class="row">
                            <div class="col-sm-6 form-group">
                                <dsp:input type="file" bean="FileUploadHandler.quickOrderFile"
                                           iclass="file form-control"
                                           name="quickorderfile" >
                                    <dsp:tagAttribute name="data-overwrite-initial" value="false"/>
                                    <dsp:tagAttribute name="multiple" value="true"/>
                                    <dsp:tagAttribute name="data-min-file-count" value="1"/>
                                </dsp:input>
                            </div>
                            <div class="col-sm-6 text-right text-left-xs mb10">
                                <!-- <a href="/document/QuickOrderTemplate.xlsx" class="gray-noreload"><i
                                        class="fa fa-download"></i> Download Spreadsheet Template</a><br> -->
                                <a class="pull-right importHelpQO green-link" >
									<dsp:include page="/includes/gadgets/info-message.jsp">
										<dsp:param name="key" value="infoHowCanIImportFile"/>
										<dsp:param name="notWrap" value="true"/>
									</dsp:include>
								</a>
                            </div>
                        </div>

						<div class="p15 well collapse green-text" id="importHelpQuickOrder">
		                	<span>
		                		<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="infoImportFileInstruction"/>
									<dsp:param name="notWrap" value="true"/>
								</dsp:include>
								<p>
									<span class="pull-right importHelpQO green-link">CLOSE</span>&nbsp;
								</p>
		                	</span>
		                </div>

                        <dsp:input type="submit" bean="FileUploadHandler.readQuickOrderFile"
                                   iclass="btn btn-primary addToCartButton" value="Add to Cart" id="subUpload"/>
                        <dsp:input type="hidden" bean="FileUploadHandler.readQuickOrderFileSuccessURL"
                                   value="/checkout/cart.jsp?qo=t"/>
                        <dsp:input type="hidden" bean="FileUploadHandler.readQuickOrderFileFailURL"
                                   value="/account/quick-order.jsp"/>
                        <dsp:input type="hidden" id="quickPageFileFail"
                                   bean="FileUploadHandler.readQuickOrderInvalidURL" value=""/>
                    </dsp:form>

                    <dsp:form name="quickAddToCart" formid="quickAddToCart" id="quickAddToCart" action="${requestURI}" method="post">
                        <%-- <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListErrorURL" value="/account/quick-order.jsp"/> --%>
                        <%-- <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListSuccessURL" value="/checkout/cart.jsp?qo=t"/> --%>
                        <%-- <dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" name="nextpageval" type="hidden" value="/checkout/cart.jsp?qo=t"/> --%>
                        <dsp:input type="hidden" id="invalidQuickPagelink" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListInvalidURL" value=""/>
                        <dsp:input type="hidden" bean="CartModifierFormHandler.skuIdsList" value="" id="addSkuIdsList"/>
                        <dsp:input type="hidden" bean="CartModifierFormHandler.qtyList" value="" id="addQtyList"/>
                        <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrder" value="true"/>
                    </dsp:form>

                    <!-- <form class="order-pad-table well"> -->
                    <div class="order-pad-table well">
                        <div class="order-pad-items">
	                        <c:forEach var="i" begin="1" end="3">
	                        	<div class="row">
	                        		<c:forEach var="j" begin="1" end="2">
			                        	<div class="order-pad-item col-sm-6 col-xs-12">
			                                <div class="row row-narrow">
			                                    <div class="form-group col-sm-9 col-xs-8">
			                                        <div class="input-group">
			                                            <input type="text" name="quickproductCart" class="list-num form-control input-lg quick-order-item" maxlength="30"
			                                                   placeholder="Item # or My Part #">
			                                            <div class="input-group-btn">
			                                                <button class="input-clear btn">
			                                                    <i class="fa fa-times"></i>
			                                                </button>
			                                            </div>
			                                        </div>
			                                    </div>
			
			                                    <div class="form-group col-sm-3 col-xs-4">
			                                        <input type="text" data-event-keypress-id="account35" maxlength="5" name="quickqtyCart"
			                                               class="pad-list-qty form-control input-lg text-center" placeholder="Qty">
			                                    </div>
			                                </div>
			                            </div>                        	
			                        </c:forEach>		                        	
	                        	</div>
	                        </c:forEach>
                        </div>

                        <div class="text-right">
                            <button class="btn btn-link add-order-items">+ Add More Items</button>
                        </div>

                        <button data-event-click-id="eventQuickOrderAddToCart"  id="quickOrderAddToCart" class="btn btn-primary addToCartButton">Add to Cart</button>
                        <a data-event-click-id="eventClearQuickOrder"  class="btn btn-info">Clear All</a>
                    <!-- </form> -->
                    </div>
                </div>
            </div>
            
             <dsp:droplet name="/cps/droplet/CPSPriceDroplet">
				<dsp:param name="productId" value="1"/>
				<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
				<c:if test="${not isTransient}">
					<dsp:param name="profile" bean="Profile"/>
				</c:if>
				<dsp:oparam name="emptyPrice">
					<dsp:getvalueof var="isAvailablePricesWebService" bean="CPSSessionBean.availablePricesWebService" />
					<script nonce="${requestScope.nonce}">
						var isAvailable = '${isAvailablePricesWebService}';
						priceAvailableMsg(isAvailable);
						if (isAvailable === 'false') {
							$('.container.main-content').css('padding-top', '30px');
						}
					</script>
				</dsp:oparam>
			</dsp:droplet>
        </main>
    </cp:pageContainer>
   
    <script type="text/javascript" nonce="${requestScope.nonce}">

        $(document).ready(function () {
            var baseParams = removeURLParameterOP(window.location.search, "quickomodal");
            baseParams = removeURLParameterOP(baseParams, "_requestid");
            baseParams = removeURLParameterOP(baseParams, "productId");
            baseParams = removeURLParameterOP(baseParams, 'sreqmodal');
            baseParams = removeURLParameterRQ(baseParams, 'showCS');
            baseParams = removeURLParameterRQ(baseParams, 'qo');
            baseParams = removeURLParameterRQ(baseParams, 'invalidskumodal');
            if (baseParams == "") {
                $('#invalidQuickPagelink').val(window.location.pathname + "?invalidskumodal=true");
                $('#quickPageFileFail').val(window.location.pathname + "?invalidskumodal=true");
            }
            else {
                $('#invalidQuickPagelink').val(window.location.pathname + "?" + baseParams + "&invalidskumodal=true");
                $('#quickPageFileFail').val(window.location.pathname + "?" + baseParams + "&invalidskumodal=true");
            }
            
            $('.importHelpQO').click(function() {
            	$("#importHelpQuickOrder").toggle();
            });
        });
        function removeURLParameterOP(params, parameter) {
            var pars = params.split(/[&;]/g);
            var prefix = encodeURIComponent(parameter) + '=';
            //reverse iteration as may be destructive
            for (var i = pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, pars[i].length - 1) !== -1) {
                    pars.splice(i, 1);
                }
            }

            params = pars.join('&');
            return params;
        }
    </script>
</dsp:page>
