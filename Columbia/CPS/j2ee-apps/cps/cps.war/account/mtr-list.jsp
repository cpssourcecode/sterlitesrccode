<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/commerce/mtr/CPSMaterialTestReportFormHandler"/>
	
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	
	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:setvalue bean="CPSSessionBean.clearMtr" value="true"/>

	<cp:pageContainer page="mtr">
	
		<div class="container main-content ${csrClass} minheight">
			
		<section>	
			<div class="row">
				<div class="col-sm-8">
					<ol class="breadcrumb">
						<li class="active"><span><fmt:message key="account.mtr.header.request_mtr"/></span></li>
					</ol>
				</div>
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="false"/>
					<dsp:param name="print" value="true"/>
				</dsp:include>
				
				<div class="col-xs-12">
					<h1><fmt:message key="account.mtr.header.request_mtr"/></h1>
				</div>
		
				<div class="col-xs-12">
					<p>To retrieve a Material Test Report (MTR) on one or more items, enter up to six heat numbers below. <strong>NOTE</strong>: If you're having trouble finding a MTR, please <a href="${originatingRequest.contextPath}/global/contact-us.jsp">contact your Sales Team</a>.</p>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<dsp:form iclass="order-pad-table well well-gray nopadding" method="post" id="mtr-search">
						<div class="order-pad-items row row-narrow">
							<div class="order-pad-item form-group col-md-4 col-sm-6 col-xs-12" id="heatNumDiv1">
	
								<div class="input-group">
									<input id="heatNum1" type="text" class="form-control input-lg heatNum" placeholder="Heat #" maxlength="30">
									<div class="input-group-btn">
	
										<button class="input-clear btn" id="1">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="order-pad-item form-group col-md-4 col-sm-6 col-xs-12" id="heatNumDiv2">
								<div class="input-group ">
									<input id="heatNum2" type="text" class="form-control input-lg heatNum" placeholder="Heat #" maxlength="30">
									<div class="input-group-btn">
	
										<button class="input-clear btn" id="2">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="order-pad-item form-group col-md-4 col-sm-6 col-xs-12" id="heatNumDiv3">
								<div class="input-group ">
									<input id="heatNum3" type="text" class="form-control input-lg heatNum" placeholder="Heat #" maxlength="30">
									<div class="input-group-btn">
	
										<button class="input-clear btn" id="3">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="order-pad-item form-group col-md-4 col-sm-6 col-xs-12" id="heatNumDiv4">
								<div class="input-group ">
									<input id="heatNum4" type="text" class="form-control input-lg heatNum" placeholder="Heat #" maxlength="30">
									<div class="input-group-btn">
	
										<button class="input-clear btn" id="4">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="order-pad-item form-group col-md-4 col-sm-6 col-xs-12" id="heatNumDiv5">
								<div class="input-group ">
									<input id="heatNum5" type="text" class="form-control input-lg heatNum" placeholder="Heat #" maxlength="30">
									<div class="input-group-btn">
	
										<button class="input-clear btn" id="5">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="order-pad-item form-group col-md-4 col-sm-6 col-xs-12" id="heatNumDiv6">
								<div class="input-group ">
									<input id="heatNum6" type="text" class="form-control input-lg heatNum" placeholder="Heat #" maxlength="30">
									<div class="input-group-btn">
	
										<button class="input-clear btn" id="6">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
						
						<a class="btn btn-primary" id="mtrSubmit" data-event-click-id="eventRequestMTR" >
							Submit
						</a>
						<a href="#" class="btn btn-info" data-event-click-id="eventClearMTRForm" >
							Clear
						</a>
						
						<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.values.heatNum1" value="" id="f-heatNum1"/>
						<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.values.heatNum2" value="" id="f-heatNum2"/>
						<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.values.heatNum3" value="" id="f-heatNum3"/>
						<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.values.heatNum4" value="" id="f-heatNum4"/>
						<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.values.heatNum5" value="" id="f-heatNum5"/>
						<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.values.heatNum6" value="" id="f-heatNum6"/>
	
						<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.requestMTR" value="true" priority="-10" />
					</dsp:form>
				</div>
				
				<div class="col-md-6 col-xs-12" id="mtr-list"></div>
                <div class="col-md-6 col-xs-12" id="mtr-preview"></div>
				<dsp:form action="" method="post" id="remove-mtr-form" formid="remove-mtr-form">
					<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.values.removeMTRIds" value="" id="removeList"/>
					<dsp:input type="hidden" bean="CPSMaterialTestReportFormHandler.removeMTRs" value="true" priority="-10"/>
				</dsp:form>
			
				<div class="col-xs-12 resultsMTRSection" style="display:none;">
					<a href="#" class="btn btn-info" data-event-click-id="eventDeleteSelectedMTR" id="btnDeleteSelected">
						Delete Selected
					</a>
				</div>
				
				<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-delete-mtr-no-select.jsp"/>
				<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-heat-entered.jsp"/>
				<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-heat-selected.jsp"/>

			</div>
			</section>
		</div>

		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(function() {
/* 				$("button.input-clear.btn").on('click', function(){
					var c = $(this).closest('input.heatNum');
					var value = $.trim($('#heatNum' + c.context.id).val());
					if (value == '') {
						$("#link-modalNoHeatSelected").click();
					}
					$('#heatNum' + c.context.id).val('');
					$("#heatNumDiv" + c.context.id).removeClass("has-error");
 				}); */
				$(".heatNum").keypress(function (e) {
					 var key = e.which;
					 if(key == 13)  // the enter key code
					  {
	                    e.preventDefault();
					    $("#mtrSubmit").trigger( "click" );
					    }
					});   
			});
		</script>

	</cp:pageContainer>
</dsp:page>