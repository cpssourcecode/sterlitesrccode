<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/userprofiling/MultiUserAddFormHandler" />
	<dsp:importbean bean="/cps/util/FileUploadHandler" />

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>

	<cp:pageContainer page="account">
		<main id="body">
		<div class="container ${csrClass}">
			<ol class="breadcrumb">
				<li><a href="/account/account-landing.jsp">My Account</a></li>
				<li class="active">Manage Users</li>
			</ol>
			<h1 class="mb20">Manage Users</h1>
			<section>
				<dsp:include page="/global/gadgets/error-message.jsp">
					<dsp:param name="formHandler" bean="FileUploadHandler" />
				</dsp:include>
				<dsp:getvalueof var="editSuccess" param="e" />
				<dsp:getvalueof var="addSuccess" param="a" />
				<dsp:getvalueof var="importSuccess" param="import" />
				<dsp:getvalueof var="invite" param="invite" />
				<c:if test="${editSuccess == 's'}">
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert"
							aria-label="Close">
							<span aria-hidden="true"> <span
								class="glyphicon glyphicon-remove-circle"></span>
							</span>
						</button>
						<div id="message-info">User Profile successfully updated.</div>
					</div>
				</c:if>
				<c:if test="${addSuccess == 's'}">
					<div class="col-xs-12">
						<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true"> <span
									class="glyphicon glyphicon-remove-circle"></span>
								</span>
							</button>
							<c:if test="${invite}">
								<div id="message-info">User Added and Invite Sent.</div>
							</c:if>
							<c:if test="${!invite}">
								<div id="message-info">User Added.</div>
							</c:if>
						</div>
					</div>
				</c:if>
				<c:if test="${importSuccess == 's'}">
					<div class="col-xs-12">
						<div class="alert alert-info">
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true"> <span
									class="glyphicon glyphicon-remove-circle"></span>
								</span>
							</button>
							<div id="message-info">User Profile import successfully
								completed.</div>
						</div>
					</div>
				</c:if>
				<a href="/account/gadgets/manage-add-user.jsp"
					class="btn btn-primary xs-block">Add User</a> <a
					href="#modal-usersin" class="btn btn-primary xs-block"
					data-dismiss="modal" data-toggle="modal" data-target="#modal-usersin">Import/Upload</a>
				<a href="#exportUsers" class="btn btn-info xs-block"
					data-dismiss="modal" data-toggle="modal" data-target="#exportUsers">Export
					Users</a>
			</section>

			<section>
				<dsp:getvalueof var="formExceptions" vartype="java.lang.Object"
					bean="MultiUserAddFormHandler.formExceptions" />
				<c:if test="${not empty formExceptions}">
					<c:forEach var="formException" items="${formExceptions}">
						<dsp:param name="formException" value="${formException}" />
						<%--<dsp:getvalueof var="errorCode" param="formException.property" />--%>
						<c:if test="${formException.message != ''}">
						
								<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert"
										aria-label="Close">
										<span aria-hidden="true"> <span
											class="glyphicon glyphicon-remove-circle"></span>
										</span>
									</button>
									<dsp:valueof param="formException.message" valueishtml="true" />
								</div>
					
						</c:if>
					</c:forEach>
				</c:if>
				<dsp:include page="/account/gadgets/manage-users-spending-limit.jsp" />
			</section>

			
			<dsp:include page="/account/gadgets/manage-users-display.jsp" />
		</div>
		<dsp:include page="/global/modals/modal-import-users.jsp" /> 
		<dsp:include page="/global/modals/modal-export-users.jsp" /> 
		</main>
	</cp:pageContainer>
</dsp:page>