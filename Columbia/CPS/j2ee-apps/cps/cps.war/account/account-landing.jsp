<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
	<dsp:importbean bean="/cps/droplet/OrderApprovalsDroplet" />
	<dsp:importbean bean="/cps/droplet/OrderRequestsDroplet" />
	<dsp:importbean bean="/cps/droplet/SavedCartsDroplet" />
	<dsp:importbean bean="/atg/userprofiling/ProfileTools"/>
	<dsp:getvalueof var="showPaymentMethod" bean="ProfileTools.showPaymentMethod" />
	<dsp:getvalueof var="isFinanceAccess" bean="Profile.requestFinanceAccess"/>								
	<dsp:getvalueof var="isAdminAccess" bean="Profile.requestAdminAccess"/>
	
	<dsp:getvalueof var="showCS" param="showCS" />

	<dsp:droplet name="Switch">
		<dsp:param bean="Profile.transient" name="value" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/?loc=landing" />
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>
	
	<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
		<dsp:param name="userId" bean="Profile.id"/>
		<dsp:param name="role" value="superAdmin,regularAdmin"/>
		<dsp:oparam name="true">
			<c:if test="${empty selectedOrg}">
				<script type="text/javascript" nonce="${requestScope.nonce}">
					parent.location.href = "${originatingRequest.contextPath}/account/manage-companies.jsp"
				</script>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>

	<cp:pageContainer page="account">
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-manage-orders" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="ordersHistory" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-manage-orders-approvals" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="pendingApprovals" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-manage-orders-requests" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="pendingRequests" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="quick-tools-approver-buyer-super" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="quickTools" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-lists" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="lists" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-billing-payment" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="billingInvoices" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-billing-payment-statements" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="billingStatements" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-my-profile" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="myProfile" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="accessRightKey" value="acc-admin-tools" />
			<dsp:oparam name="true">
				<dsp:getvalueof var="adminTools" value="true" />
			</dsp:oparam>
		</dsp:droplet>
		<!-- fix SM-520 New User Role -->
		<dsp:droplet name="/cps/droplet/AccessRightDroplet">
		  <dsp:param name="profile" bean="Profile" />
		  <dsp:param name="accessRightKey" value="quick-tools-approver-buyer-super-order-pad" />
		  <dsp:oparam name="true">
		    <dsp:getvalueof var="quickToolsOrderPad" value="true" />
		  </dsp:oparam>
		</dsp:droplet>
		<main id="body">
		<div class="container ${csrClass}">
			<ol class="breadcrumb">
				<li class="active">My Account Overview</li>
			</ol>


			<h1 class="mb20">Account Overview</h1>

			<section class="account-overview-list">
				<div class="row">
					<div class="col-md-40 col-xs-6">
						<div class="row">
							<div class="col-md-6">
								<c:if test="${ordersHistory}">
									<h2 class="h5">Manage Orders</h2>
									<ul>
										<li>
											<a href="/account/order-list.jsp">
												<i class="icon icon-folder-pair"></i>
												Order History
											</a>
										</li>
										<c:if test="${pendingApprovals}">
											<dsp:getvalueof var="ordersSize" value="${0}" />
											<dsp:droplet name="OrderApprovalsDroplet">
												<dsp:param name="profile" bean="Profile" />
												<dsp:oparam name="output">
													<dsp:getvalueof var="ordersSize" param="ordersSize" />
												</dsp:oparam>
											</dsp:droplet>
											<li>
												<a href="/account/pending-approvals.jsp">
													<i class="icon icon-folder-single"></i>
													Pending Order Approvals (<dsp:valueof value="${ordersSize}" />)
												</a>
											</li>
										</c:if>
										<c:if test="${pendingRequests}">
											<dsp:getvalueof var="ordersSize" value="${0}" />
											<dsp:droplet name="OrderRequestsDroplet">
												<dsp:param name="profile" bean="Profile"/>
												<dsp:oparam name="output">
													<dsp:getvalueof var="ordersSize" param="ordersSize" />
												</dsp:oparam>
											</dsp:droplet>
											<li>
												<a href="/account/order-requests.jsp">
													<i class="icon icon-folder-motion"></i>
													Pending Order Requests (<dsp:valueof value="${ordersSize}" />)
												</a>
											</li>
										</c:if>
										<li>
											<a href="/account/packing-list.jsp">
												<i class="icon icon-paper-list"></i>
												Packing Slips
											</a>
										</li>
									</ul>
								</c:if>
							</div>
							<div class="col-md-6">
								<c:if test="${quickTools}">
									<h2 class="h5">Quick Tools</h2>
									<ul>
										<!-- fix SM-520 New User Role -->
										<c:if test="${quickToolsOrderPad}">
											<li>
												<a href="/account/quick-order.jsp">
													<i class="icon icon-clipboard"></i>
													Order Pad
												</a>
											</li>
										</c:if>
										<li>
											<a href="/account/request-quote.jsp">
												<i class="icon icon-quote"></i>
												Request a Quote
											</a>
										</li>
										<li>
											<a href="/account/mtr-list.jsp">
												<i class="icon icon-paper-check"></i>
												Material Test Reports
											</a>
										</li>
									</ul>
								</c:if>
							</div>
						</div>
					</div>
					<div class="col-md-60 col-xs-6">
						<div class="row">
							<c:if test="${lists}">
								<div class="col-md-4">
									<h2 class="h5">Lists</h2>
									<ul>
										<li>
											<a href="/account/material-lists.jsp">
												<i class="icon icon-user-material"></i>
												My Material Lists
											</a>
										</li>
<%--										<li>
											<a href="/account/order-carts.jsp">
															<i class="glyphicon glyphicon-shopping-cart"></i>
 															<dsp:droplet name="SavedCartsDroplet">
																<dsp:param name="profile" bean="Profile" />
																<dsp:oparam name="output">
																	<dsp:getvalueof var="savedCartsCount" param="ordersSize" />																	
																	Saved Carts (${savedCartsCount})															
																</dsp:oparam>
																<dsp:oparam name="empty">
																Saved Carts (0)
           														</dsp:oparam>
															</dsp:droplet>
														</a>
										</li> --%>
									</ul>
								</div>
							</c:if>
							
							<div class="col-md-4">
								<h2 class="h5">Billing / Payments</h2>								
								<ul>
									<li>
									<c:choose>
										<c:when test="${billingInvoices}">
										<a href="/account/invoices.jsp">
											<i class="icon icon-paper-header"></i>
											Invoices
										</a>
										</c:when>
										<c:when test="${isFinanceAccess}">
											<span class="disabled-acc-overview">
												<i class="icon icon-paper-header"></i>Invoices
												<span class="request-access-link">Access Pending</span>
											</span>
										</c:when>
										<c:otherwise>
										<span class="disabled-acc-overview">
											<i class="icon icon-paper-header"></i>
											Invoices
											<a href="/global/request-access.jsp" class="request-access-link" >Request Access</a>
										</span>
										</c:otherwise>
									</c:choose>
									</li>
									<li>	
									<c:choose>	
										<c:when test="${billingStatements}">
										<a href="/account/statements.jsp">
											<i class="icon icon-paper-list"></i>
											Statements
										</a>
										</c:when>
										<c:when test="${isFinanceAccess}">
											<span class="disabled-acc-overview">
											<i class="icon icon-paper-list"></i>Statements
											<span class="request-access-link">Access Pending</span>
										</span>
										</c:when>
										<c:otherwise>
										<span class="disabled-acc-overview">
											<i class="icon icon-paper-list"></i>
											Statements
											<a href="/global/request-access.jsp" class="request-access-link" >Request Access</a>
										</span>
										</c:otherwise>
									</c:choose>		
									</li>
								</ul>
							</div>
							
							<c:if test="${myProfile}">
								<div class="col-md-4">
									<h2 class="h5">My Profile</h2>
									<ul>
										<li>
											<a href="/account/addresses.jsp">
												<i class="fa fa-map-marker ml5"></i>
												Shipping Locations
											</a>
										</li>
										<li>
											<a href="/account/profile-settings.jsp">
												<i class="icon icon-user"></i>
												Edit Profile
											</a>
										</li>
										<li>
											<c:if test="${showPaymentMethod}">
												<a href="/account/profile-payment-method.jsp">
													<i class="fa fa-credit-card"></i>
													Payment Method
												</a>
											</c:if>
										</li>
									</ul>
									
									<h2 class="h5">Admin Tools</h2>
									<ul>
										<li>
										<c:choose>
											<c:when test="${adminTools}">
											<a href="/account/manage-users.jsp">
												<i class="icon icon-users"></i>
												Manage Users
											</a>
											</c:when>
											<c:when test="${isAdminAccess}">
												<i class="icon icon-users"></i>Manage Users<br>
												<span class="request-access-link" >Access Pending</span>
											</span>
											</c:when>
											<c:otherwise>
											<span class="disabled-acc-overview">
												<i class="icon icon-users"></i>
												Manage Users<br>
												<a href="/global/request-access.jsp" class="request-access-link" >Request Access</a>
											</span>
											
											</c:otherwise>
										</c:choose>
										</li>
									</ul>

									
								</div>
							</c:if>

						</div>
					</div>
				</div>



			</section>
			<dsp:droplet name="/cps/droplet/AccessRightDroplet">
				<dsp:param name="profile" bean="Profile" />
				<dsp:param name="accessRightKey" value="recent-orders-invoices" />
				<dsp:oparam name="true">
					<dsp:include page="/account/gadgets/landing-order-invoice.jsp" />
				</dsp:oparam>
			</dsp:droplet>
		</div>
		</main>

		<div id="modal-cs-show-div"></div>
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-cs.jsp" />
		<script nonce="${requestScope.nonce}">
            onModalCsLoaded();
		</script>
		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
			<dsp:param name="messageId" value="err_shp_conf" />
			<dsp:param name="divId" value="_showMessEmpty" />
			<dsp:param name="isReset" value="true" />
		</dsp:include>
		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(function() {
				checkShowCS();
			});

			function checkShowCS() {
				if ('${vsg_utils:escapeJS(showCS)}' == 'true') {
					openCSModal();
				}
			}
		</script>

	</cp:pageContainer>

</dsp:page>

