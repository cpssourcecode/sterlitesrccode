<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>

	<dsp:importbean bean="/cps/userprofiling/RequestStatementFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<dsp:getvalueof var="email" bean="Profile.email"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<cp:pageContainer page="account">
		<main id="body">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<ol class="breadcrumb">
						<li>
							<a href="${originatingRequest.contextPath}/account/account-landing.jsp">
								<fmt:message key="account.breadcrumb.my_account"/>
							</a>
						</li>
						<li class="active"><fmt:message key="account.statements.header.view_statements"/></li>
					</ol>
				</div>
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="false"/>
					<dsp:param name="print" value="true"/>
				</dsp:include>
			</div>


			<h1 class="mb20"><fmt:message key="account.statements.header.view_statements"/></h1>

			<section>

				<div class="well">
					<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
						<dsp:param name="userId" bean="Profile.id"/>
						<dsp:param name="role" value="superAdmin"/>
						<dsp:oparam name="false">
							<dsp:getvalueof var="customerNumber" bean="Profile.parentOrganization.id"/>
						</dsp:oparam>
						<dsp:oparam name="true">
							<dsp:droplet name="/atg/dynamo/droplet/IsNull">
								<dsp:param name="value" bean="Profile.CSRSelectedOrg"/>
								<dsp:oparam name="true">
									<dsp:getvalueof var="customerNumber" bean="Profile.parentOrganization.id"/>
								</dsp:oparam>
								<dsp:oparam name="false">
									<dsp:getvalueof var="customerNumber" bean="Profile.CSRSelectedOrg.id"/>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>

					<dsp:form action="" method="post" id="request-statement-form" formid="request-statement-form">
						<dsp:input type="hidden" bean="RequestStatementFormHandler.value.customerNumber" value="${customerNumber}" />
						<dsp:input type="hidden" bean="RequestStatementFormHandler.value.email" value="${email}" />
						<dsp:input type="hidden" bean="RequestStatementFormHandler.requestStatement" value="true" priority="-10"/>
					</dsp:form>

					<p><a href="#" data-event-click-id="eventRequestStatement"><strong>Request a Current Statement</strong></a> - a copy of your current statement
						will be mailed to ${email}.</p>

					<p>To view statements older than one year,
						<a href="/global/contact-us.jsp">contact your Sales Team</a>
					</p>
				</div>


					<dsp:getvalueof var="pOrganization" bean="Profile.parentOrganization" />
					<c:if test="${not empty pOrganization}">
						<dsp:getvalueof var="orgList" bean="Profile.parentOrganization.childOrganizations" />
						<c:if test="${not empty orgList}">
							<dsp:getvalueof var="billingAccounts" bean="Profile.billingAccounts" />
								<form class="form-inline">
									<div class="form-group">
										<label class="sr-only" for="billingAddress">Select Billing Address</label>
										<select name="" id="billingAddress" class="form-control">

											<option value="${customerNumber}">Select Billing Address</option>
												<dsp:droplet name="/atg/dynamo/droplet/ForEach">
													<dsp:param name="array" value="${billingAccounts}" />
													<dsp:param name="elementName" value="org" />
													<dsp:oparam name="output">
														<dsp:getvalueof var="name" param="org.name" />
														<dsp:getvalueof var="orgId" param="org.id" />
														<option value="${orgId}" title="${name}">
																${name}
														</option>
													</dsp:oparam>
												</dsp:droplet>
										</select>
									</div>
									<button type="submit" class="btn btn-primary" data-event-click-id="eventSearchStatementsButton">Search</button>
									<button type="submit" class="btn disabled resetBillingAddressButton" id="resetStatementsButton"  data-event-click-id="eventResetStatementsButton">Reset</button>
								</form>

                        </c:if>
					</c:if>
			</section>
			<div class="col-xs-12" id="statements-list"></div>

		</div>
		</main>

		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-request-statement-confirm.jsp"/>

		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(
					function() {
						customerNumber = "${customerNumber}";
						$(document).ready(
								function() {
									$("#statements-list").load(
											"/account/gadgets/statements-list-display.jsp?customerNumber="
													+ '${customerNumber}');
									validateBillingAddress();
								});
					});
		</script>

	</cp:pageContainer>

</dsp:page>