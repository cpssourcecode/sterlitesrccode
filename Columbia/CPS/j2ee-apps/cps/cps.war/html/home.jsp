<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    
    <dsp:include page="includes/header-unlogged.jsp"/>

    <!-- header end -->
    <div class="owl-home hide">
        <div>
            <div class="image" style="background: url(assets/images/home-slider/slide-bg.png);">
                <!-- <img src="${staticContentPrefix}../assets/images/slide-bg.png" alt=""> -->
            </div>
            <div class="lead-block">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-xs-8">
                            <h2>Big Headline 1</h2>
                            <p class="lead">Smaller sub-copy that promotes something for viewer to take action on.</p>
                        </div>
                        <div class="col-sm-4 col-xs-4 text-right">
                            <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="image" style="background: url(assets/images/home-slider/black-and-white-sky-construction-bridge.jpg)">
                <!-- <img src="${staticContentPrefix}../assets/images/home-slider/black-and-white-sky-construction-bridge.jpg" alt=""> -->
            </div>
            <div class="lead-block">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-xs-8">
                            <h2>Big Headline 2</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis pariatur numquam quasi nemo.</p>
                        </div>
                        <div class="col-sm-4 col-xs-4 text-right">
                            <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="image" style="background: url(assets/images/home-slider/landmark-bridge-metal-architecture.jpg)">
                <!-- <img src="${staticContentPrefix}../assets/images/home-slider/landmark-bridge-metal-architecture.jpg" alt=""> -->
            </div>
            <div class="lead-block">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-xs-9">
                            <h2>Big Headline 3</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt quos perspiciatis suscipit saepe laudantium vel nihil possimus temporibus ducimus.</p>
                        </div>
                        <div class="col-sm-4 col-xs-4 text-right">
                            <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-3 hidden-xs">
                <div class="well">
                    <h3>Our Customers Love Us</h3>
                    <blockquote>
                        <p>Their service is fantastic. Our branch manager is knowledgeable and has great technical expertise.”</p>
                        <footer>Someone famous in
                            <cite title="Source Title">Source Title</cite>
                        </footer>
                    </blockquote>
                    <a class="testimonial-detail" href="#"><span>Read More testimonials</span> <span class="glyphicon glyphicon-menu-right"></span></a>
                </div>
            </div>
            <div class="col-sm-9">
                <h3 class="page-title">Explore Popular Products</h3>
                <div class="row equalize">
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <div class="image">
                                        <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4>Pipe</h4>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Carbon Steel Pipe</a></li>
                                            <li><a href="#">Carbon Steel</a></li>
                                            <li><a href="#">Soil Pipe & Fittings</a></li>
                                            <li><a href="#">SS Pipe</a></li>
                                        </ul>
                                        <a href="#" class="btn btn-warning floated-bottom" data-toggle="modal" data-target="#findInvoice">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <div class="image">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4>Water Heaters</h4>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Carbon Steel Pipe</a></li>
                                            <li><a href="#">Carbon Steel</a></li>
                                            <li><a href="#">Soil Pipe & Fittings</a></li>
                                            <li><a href="#">SS Pipe</a></li>
                                        </ul>
                                        <a href="#" class="btn btn-warning floated-bottom">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <div class="image">
                                        <img src="${staticContentPrefix}assets/images/plumbing.png" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4>Plumbing</h4>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Carbon Steel Pipe</a></li>
                                            <li><a href="#">Carbon Steel</a></li>
                                            <li><a href="#">Soil Pipe & Fittings</a></li>
                                            <li><a href="#">SS Pipe</a></li>
                                        </ul>
                                        <a href="#" class="btn btn-warning floated-bottom">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <h3>Services</h3>
                <div class="row equalize">
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <a href="#">
                                        <div class="image">
                                            <img src="${staticContentPrefix}assets/images/kitting.png" alt="">
                                        </div>
                                        <h4 class="hidden-xs">Kitting</h4>
                                    </a>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbmail-content">
                                        <h4 class="visible-xs">Kitting</h4>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil, id totam sint dolor molestiae natus minus dolores consectetur pariatur eligendi</small>
                                        <a href="#" class="btn btn-warning floated-bottom">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <a href="#">
                                        <div class="image">
                                            <img src="${staticContentPrefix}assets/images/standartization.png" alt="">
                                        </div>
                                        <h4 class="hidden-xs">Standardization</h4>
                                    </a>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4 class="visible-xs">Standardization</h4>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil, id totam sint dolor molestiae natus minus dolores consectetur pariatur eligendi, iure, accusamus quisquam dolorum accusantium. Rerum illo, sapiente?</small>
                                        <a href="#" class="btn btn-warning floated-bottom">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <a href="#">
                                        <div class="image">
                                            <img src="${staticContentPrefix}assets/images/after-sales-service.png" alt="">
                                        </div>
                                        <h4 class="hidden-xs">After Sales Service</h4>
                                    </a>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4 class="visible-xs">After Sales Service</h4>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                        <a href="#" class="btn btn-warning floated-bottom">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <h3>You May Be Interested In</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="owl-thumbnails">
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->

    <dsp:include page="includes/footer.jsp"/>
    <dsp:include page="includes/scripts.jsp"/>
    
    
</body>

</html>

<dsp:include page="includes/modals.jsp"/>
</dsp:page>