<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <dsp:include page="includes/header-logged.jsp"/>
    <!-- header end -->

	<div class="container main-content">
		<div class="row">
			<div class="col-sm-8">
				<ol class="breadcrumb">
					<li><a href="my-account.php">My Account</a></li>
					<li class="active">Edit Profile</li>
				</ol>
			</div>
			<div class="col-sm-4 page-actions">
				<ul class="list-inline">
					<li><a href="#" class="action"> <i
							class="fa fa-envelope-o"></i><span>Email Page</span>
					</a></li>
					<li><a href="#" class="action"> <i class="fa fa-print"></i><span>Print
								Page</span>
					</a></li>
				</ul>
			</div>
			<div class="col-xs-12">
				<h1>Edit Profile</h1>
			</div>
			<div class="col-xs-12">
				<div class="material-list detail profile">
					<div class="row">
						<div class="col-sm-12 info-block profile-block">
							<h4 class="title">Personal Information</h4>
							<div class="info">
								<div>
									<strong>Name</strong> <span>Frank Jones</span>
								</div>
								<div>
									<strong>Title</strong> <span>Purchasing Manager</span>
								</div>
							</div>
							<button class="btn btn-default btn-lg profile-button"
								type="button" data-toggle="collapse"
								data-target="#collapseExample0" aria-expanded="false"
								aria-controls="collapseExample0">Edit</button>
						</div>
						<div class="col-sm-12 collapse" id="collapseExample0">
							<div class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-9 col-md-7">
										<h4>Edit Personal Information</h4>
										<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true"> <span
													class="glyphicon glyphicon-remove-circle"></span>
												</span>
											</button>
											Personal information changed successfully
										</div>
										<div class="alert alert-danger">
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true"> <span
													class="glyphicon glyphicon-remove-circle"></span>
												</span>
											</button>
											Please, fill in <a class="error-anchor" href="#job-anchor"><span>Job</span></a>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<div class="row">
											<div class="col-sm-6 col-xs-6">
												<input type="text" class="form-control input-lg"
													placeholder="" value="Frank">
												<p class="help-block">First Name</p>
											</div>
											<div class="col-sm-6 col-xs-6">
												<input type="text" class="form-control input-lg"
													placeholder="" value="Jones">
												<p class="help-block">Last Name</p>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group has-error">
									<div class="col-md-7 col-sm-9">
										<input type="text" id="job-anchor"
											class="form-control input-lg" placeholder=""
											value="Job Title"> <br>
									</div>
									<div class="col-md-5 col-sm-12">
										<ul class="list-inline">
											<li><a href="#" class="btn btn-warning btn-lg">Save</a>
											</li>
											<li>
												<button class="btn btn-default btn-lg" type="button"
													data-toggle="collapse" data-target="#collapseExample0"
													aria-expanded="false" aria-controls="collapseExample0">Cancel</button>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-12 info-block profile-block">
							<h4 class="title">Contact Information</h4>
							<div class="info">
								<div>
									<strong>Email</strong> <span>frank@company-name.com</span>
								</div>
								<div>
									<strong>Mobile Phone</strong> <span>000-000-0000</span>
								</div>
								<div>
									<strong>Direct Phone</strong> <span>000-000-0000</span>
								</div>
								<div>
									<strong>Company Address</strong> <span>1234 Company
										Drive, Suite C, Chicago, IL 60605</span>
								</div>
							</div>
							<button class="btn btn-default btn-lg profile-button"
								type="button" data-toggle="collapse"
								data-target="#collapseExample" aria-expanded="false"
								aria-controls="collapseExample">Edit</button>
						</div>
						<div class="col-sm-12 collapse" id="collapseExample">
							<div class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-9 col-md-7">
										<h4>Contact Information</h4>
										<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true"> <span
													class="glyphicon glyphicon-remove-circle"></span>
												</span>
											</button>
											Data changed successfully
										</div>
										<div class="alert alert-danger">
											<button type="button" class="close" data-dismiss="alert"
												aria-label="Close">
												<span aria-hidden="true"> <span
													class="glyphicon glyphicon-remove-circle"></span>
												</span>
											</button>
											Please, fill in <a class="error-anchor" href="#email-anchor"><span>Email</span></a>,
											<a class="error-anchor" href="#mobile-anchor"><span>Mobile
													Phone</span></a>
										</div>
									</div>
								</div>
								<div class="form-group has-error">
									<div class="col-md-7 col-sm-9">
										<input id="email-anchor" type="email"
											class="form-control input-lg" placeholder="" value="Email">
									</div>
								</div>
								<div class="form-group has-error">
									<div class="col-md-7 col-sm-9">
										<input id="mobile-anchor" type="text"
											class="form-control input-lg" placeholder=""
											value="Mobile Phone">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="text" class="form-control input-lg"
											placeholder="" value="Direct Phone">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="text" class="form-control input-lg"
											placeholder="" value="Company Address 1">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="text" class="form-control input-lg"
											placeholder="" value="Company Address 2">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<div class="row">
											<div class="col-sm-4 col-xs-4">
												<input type="text" class="form-control input-lg"
													placeholder="" value="City">
												<p class="help-block">Washington</p>
											</div>
											<div class="col-sm-4 col-xs-4">
												<select class="form-control input-lg" name="" id="">
													<option value="">DC</option>
												</select>
												<p class="help-block">State</p>
											</div>
											<div class="col-sm-4 col-xs-4">
												<input type="text" class="form-control input-lg"
													placeholder="" value="12345">
												<p class="help-block">ZIP</p>
											</div>
										</div>
									</div>
									<div class="col-md-5 col-sm-12">
										<ul class="list-inline">
											<li><a href="#" class="btn btn-warning btn-lg">Save</a>
											</li>
											<li>
												<button class="btn btn-default btn-lg" type="button"
													data-toggle="collapse" data-target="#collapseExample"
													aria-expanded="false" aria-controls="collapseExample">Cancel</button>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-12 info-block profile-block">
							<h4 class="title">Login / Security Information</h4>
							<div class="info">
								<div>
									<strong>User ID</strong> <span>frank@company-name.com</span>
								</div>
								<div>
									<strong>Password</strong> <span>************</span>
								</div>
								<div>
									<strong>Security Question</strong> <span>Place of Birth</span>
								</div>
								<div>
									<strong>Answer</strong> <span>************</span>
								</div>
							</div>
							<button class="btn btn-default btn-lg profile-button"
								type="button" data-toggle="collapse"
								data-target="#collapseExample3" aria-expanded="false"
								aria-controls="collapseExample3">Edit</button>
						</div>
						<div class="col-sm-12 collapse" id="collapseExample3">
							<div class="form-horizontal">
								<div class="form-group">
									<div class="col-sm-12">
										<h4>Login / Security Information</h4>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<div class="info">
											<div>
												<strong>User ID</strong> <span>frank@company-name.com</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="email" class="form-control input-lg"
											placeholder="Old Password" value="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="text" class="form-control input-lg"
											placeholder="New Password" value="" data-toggle="popover"
											data-trigger="focus"
											data-content="Passwords are case sensitive and must contain a minimum of six characters consisting of letters, numbers, and/or symbols.">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="text" class="form-control input-lg"
											placeholder="Confirm New Password" value="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="text" class="form-control input-lg"
											placeholder="Security Question" value="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-7 col-sm-9">
										<input type="text" class="form-control input-lg"
											placeholder="Security Question Answer" value=""> <br>
									</div>
									<div class="col-md-5 col-sm-12">
										<ul class="list-inline">
											<li><a href="#" class="btn btn-warning btn-lg">Save</a>
											</li>
											<li>
												<button class="btn btn-default btn-lg profile-button"
													type="button" data-toggle="collapse"
													data-target="#collapseExample3" aria-expanded="false"
													aria-controls="collapseExample3">Cancel</button>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- main container end -->
    <dsp:include page="includes/footer.jsp"/>
    <dsp:include page="includes/scripts.jsp"/>
</body>

</html>

<dsp:include page="includes/modals.jsp"/>
</dsp:page>