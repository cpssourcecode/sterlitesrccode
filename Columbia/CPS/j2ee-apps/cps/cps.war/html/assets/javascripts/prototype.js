$('.dropdown-hover').dropdownHover();

jQuery(document).ready(function(){
    jQuery('.scrollbar-inner').scrollbar();
});

$( "#forgotPassword" ).click(function() {
  $("#logIn").modal('hide');
});

$('#logIn').on('shown.bs.modal', function () {
  $('.autofocus-input').focus();
  console.log("sdfas");
})

$(function () {
  $('[data-toggle="popover"]').popover()
})

$(".error-anchor").click(function(event) {
      var anchorValue = $(this).attr("href");
      $("input" + anchorValue).focus();
      $("input" + anchorValue).addClass('animated').addClass('pulse');
      setTimeout(function(){
        $("input" + anchorValue).removeClass('animated').removeClass('pulse');
      }, 1500);
      // console.log(anchorValue);
      event.preventDefault();
});

$( document ).ready(function() {
    $('.owl-home').removeClass('hide');
	$('.owl-home').owlCarousel({
	  	items : 1,
      itemsCustom : false,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsTabletSmall: false,
      itemsMobile : [479,1],
      singleItem : false,
      itemsScaleUp : false,
	  	loop: true,
	  	// animateOut: 'fadeOut',
	  	// animateIn: 'fadeIn',
	  	autoplay: true,
	  	autoplayTimeout: 7000,
	  	dots: false
	 });

});

$(document).ready(function(){
  $(".owl-thumbnails").owlCarousel({
  	items : 3,
    itemsCustom : false,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,2],
    itemsTablet: [768,1],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    singleItem : false,
    itemsScaleUp : false,
    // Navigation
    navigation : true,
    navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>","<span class='glyphicon glyphicon-chevron-right'></span>"],
    rewindNav : true,
    scrollPerPage : false,
    //Pagination
    pagination : true,
    paginationNumbers: true,
    //Autoplay
  	autoplay: true,
    autoplayTimeout: 5000,
  });
});

$(document).ready(function(){
  $(".owl-full-width").owlCarousel({
    items : 4,
    itemsCustom : false,
    itemsDesktop : [1199,4],
    itemsDesktopSmall : [980,3],
    itemsTablet: [768,1],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    singleItem : false,
    itemsScaleUp : false,
    // Navigation
    navigation : true,
    navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>","<span class='glyphicon glyphicon-chevron-right'></span>"],
    rewindNav : true,
    scrollPerPage : false,
    //Pagination
    pagination : true,
    paginationNumbers: true,
    //Autoplay
    autoplay: true,
    autoplayTimeout: 5000,
  });
});

$(window).on('load resize', function() {
       var row=$('.equalize');
       $.each(row, function() {
           var maxh=0;
           $.each($(this).find('div[class^="col-"] .thumbnail'), function() {
               if($(this).height() > maxh) {
                   maxh=$(this).height();
               }
           });
           $.each($(this).find('div[class^="col-"] .thumbnail'), function() {
               $(this).height(maxh);
           });
       });

      var windowWidth = $(window).width();
      if (windowWidth <= 768) {
        $(".mainmenu-dropdown").css('width', windowWidth);
        $(".dropdown-hover-trigger").removeClass('dropdown-hover');
        $('.equalize').find('div[class^="col-"] .thumbnail').css('height', 'auto');
      } else {
        $(".mainmenu-dropdown").css('width', 'auto');
        $(".dropdown-hover-trigger").addClass('dropdown-hover');
      };
});



$(".close-this-modal").click(function() {
  console.log("dasdfas");
});

$('#select-item-material-check input').on('change', function () {
  if ($(this).checkbox('isChecked')) {
    console.log("cheked");
    $('.content-list').find('.content').removeClass('selected');
    $('#select-item-material-check input').checkbox('uncheck');
    $(this).closest('.content').addClass('selected');
    $(this).checkbox('check');

  } else {
    console.log("mazafaka");
  };
});

$('.scrollbar-scrollable input').on('change', function () {
  if ($(this).checkbox('isChecked')) {
    console.log("cheked");
    $('.scrollbar-inner').find('.radio').removeClass('checked');
    $('.scrollbar-scrollable input').checkbox('uncheck');
    $(this).closest('.scrollbar-scrollable').addClass('checked');
    $(this).checkbox('check');

  } else {
    console.log("mazafaka");
  };
});


$(document).ready(function() {

        $(".fancybox").fancybox({
          caption : {
            type : 'outside'
          },
          openEffect  : 'elastic',
          closeEffect : 'elastic',
          nextEffect  : 'elastic',
          prevEffect  : 'elastic'
        });
});

// $( ".typeahead-main" ).keypress(function() {
//   $(this).parent().addClass('open');
// });

$(".header-ship li label").click(function() {
  $(".login-box-select .btn .text").text( $(this).children('.addr-title').text() );
  $(".header-ship .current p .addr-title").text( $(this).children('.addr-title').text() );
  $(".header-ship .current p .addr1").text( $(this).children('.addr1').text() );
  $(".header-ship .current p .addr2").text( $(this).children('.addr2').text() );
  // addrTemp = $(this).children('.addr-title').text();
  // console.log(addrTemp);
});

$(".header-ship .close").click(function() {
  $(this).parent().parent().removeClass('open');
});