$(".fileupload").fileinput({
//         uploadUrl: '/', // you must set a valid URL here else you will get an error
        overwriteInitial: true,
        showPreview: true,
        showCancel: false,
        showRemove: false,
        showUpload: false,
        uploadLabel: 'Upload',
        uploadIcon: '',
        uploadClass: 'btn btn-warning btn-lg',
        browseLabel: 'Browse',
        browseIcon: '',
        browseClass: 'btn btn-warning btn-lg',
        // removeClass: 'hide',
        minFileCount: '0',
        maxFileCount: '10',
        maxFileSize: '1024',
        dropZoneEnabled: false,
        allowedFileExtensions: ['tif', 'tiff', 'gif', 'jpeg', 'jpg', 'jif', 'jfif', 'jp2', 'jpx', 'j2k', 'j2c', 'fpx', 'pcd', 'png', 'pdf', 'doc', 'docx', 'csv', 'xls', 'xlsx'],
        fileActionSettings: {
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        },
        layoutTemplates: {
            caption: '<div tabindex="-1" class="form-control file-caption input-sm {class}">\n' +
                '   <div class="file-caption-name"></div>\n' +
                '</div>',
            footer: '<div class="file-thumbnail-footer">\n' +
                '    <div class="file-caption-name" style="width:{width}">{caption}</div>\n' +
                '    {actions}\n' +
                '<div class="file-actions"><div class="file-footer-buttons"><button type="button" class="kv-file-remove btn btn-xs btn-default" title="Remove file"><i class="glyphicon glyphicon-remove"></i></button></div><div class="clearfix"></div></div>' +
                '</div>',
            actions: '<div class="file-actions">\n' +
                '    <div class="file-footer-buttons">\n' +
                '        {delete}' +
                '    </div>\n' +
                '    <div class="clearfix"></div>\n' +
                '</div>',
            actionDelete: '<button type="button" class="kv-file-remove {removeClass}" title="{removeTitle}"{dataUrl}{dataKey}>{removeIcon}</button>\n',
            actionUpload: '<button type="button" class="kv-file-upload {uploadClass}" title="{uploadTitle}">{uploadIcon}</button>\n',

            progress: '<div class="progress">\n' +
                '    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="{percent}" aria-valuemin="0" aria-valuemax="100" style="width:{percent}%;">\n' +
                '        {percent}%\n' +
                '     </div>\n' +
                '</div>',

            main1: '{preview}\n' +
                '<div class="kv-upload-progress hide"></div>\n' +
                '<div class="input-group {class}">\n' +
                '   {caption}\n' +
                '   <div class="input-group-btn">\n' +
                '       {remove}\n' +
                '       {cancel}\n' +
                '       {upload}\n' +
                '       {browse}\n' +
                '   </div>\n' +
                '</div>',
            main2: '{preview}\n<div class="kv-upload-progress hide"></div>\n{remove}\n{cancel}\n{upload}\n{browse}\n',
            preview: '<div class="file-preview {class}">\n' +
                '    <div class="{dropClass}">\n' +
                '    <div class="file-preview-thumbnails">\n' +
                '    </div>\n' +
                '    <div class="clearfix"></div>' +
                '    <div class="file-preview-status text-success"></div>\n' +
                '    <div class="kv-fileinput-error"></div>\n' +
                '    </div>\n' +
                '</div>',
            icon: '<span class="glyphicon glyphicon-file kv-caption-icon"></span>',
            
            modal: '<div id="{id}" class="modal fade">\n' +
                '  <div class="modal-dialog modal-lg">\n' +
                '    <div class="modal-content">\n' +
                '      <div class="modal-header">\n' +
                '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>\n' +
                '        <h3 class="modal-title">Detailed Preview <small>{title}</small></h3>\n' +
                '      </div>\n' +
                '      <div class="modal-body">\n' +
                '        <textarea class="form-control" style="font-family:Monaco,Consolas,monospace; height: {height}px;" readonly>{body}</textarea>\n' +
                '      </div>\n' +
                '    </div>\n' +
                '  </div>\n' +
                '</div>'

        }
	});

