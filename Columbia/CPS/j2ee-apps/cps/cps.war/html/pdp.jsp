<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
	<dsp:getvalueof param="prodId" var="prodId"/>
	<dsp:droplet name="ProductLookup">
		<dsp:param name="id" value="${prodId}"/>
		<dsp:param name="filterBySite" value="false"/>
		<dsp:param name="filterByCatalog" value="false"/>
		<dsp:oparam name="output">
			<dsp:setvalue param="product" paramvalue="element"/>
			<cp:pageContainer page="pdp">
			<body>
				<div class="container main-content">
					<div class="row">
						<dsp:include page="/catalog/gadgets/pdp-breadcrumb.jsp">
							<dsp:param name="product" param="product"/>
						</dsp:include>
						<div class="col-sm-4 page-actions">
							<ul class="list-inline">
								<li>
									<a href="#" class="action">
										<i class="fa fa-envelope-o"></i><span>Email Page</span>
									</a>
								</li>
								<li>
									<a href="#" class="action">
										<i class="fa fa-print"></i><span>Print Page</span>
									</a>
								</li>
							</ul>
						</div>
						<dsp:include page="/catalog/gadgets/pdp-title.jsp">
							<dsp:param name="product" param="product"/>
						</dsp:include>
						<dsp:include page="/catalog/gadgets/pdp-tabs.jsp">
							<dsp:param name="product" param="product"/>
						</dsp:include>
						<dsp:include page="/catalog/gadgets/pdp-suggested.jsp">
							<dsp:param name="product" param="product"/>
						</dsp:include>
					</div>
				</div>
			</body>
			</cp:pageContainer>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>