<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <dsp:include page="includes/header-logged.jsp"/>
    <!-- header end -->
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Contact Us</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>Contact Us</h1>
            </div>
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-5">
                        <h2>Your Sales Team</h2>
                        <br>
                        <h4>Outside Sales</h4>
                        <p>Joe Smith
                            <br> Title
                            <br> Phone: 1-800-572-1904</p>
                        <hr>
                        <h4>Inside sales</h4>
                        <p>Frank Smith
                            <br> Title
                            <br> Phone: 1-800-572-1904</p>
                        <hr>
                        <h4>Billing</h4>
                        <p>Tom Smith
                            <br> Title
                            <br> Phone: 1-800-572-1904</p>
                        <hr>
                        <h2>General</h2>
                        <p>1120 West Pershing Road
                            
                            <a href="#" class="text-muted"><i class="fa fa-map-marker"></i></a> <br>
                            Chicago, IL 60609 <br>
                            <br> Phone: 1-800-572-1904</p>
                    </div>
                    <div class="col-sm-7">
                        <div class="well well-gray">
                            <form>
                                <div class="form-group">
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">
                                                  <span class="glyphicon glyphicon-remove-circle"></span>
                                              </span>
                                            </button>
                                        Please, fill in <a class="error-anchor" href="#name-anchor"><span>Name</span></a> and <a class="error-anchor" href="#company-anchor"><span>Your Compay Name</span></a> field
                                    </div>
                                </div>
                                <div class="form-group has-error">
                                    <input id="name-anchor" type="text" class="form-control input-lg" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" placeholder="Email Address">
                                </div>
                                <div class="form-group has-error">
                                    <input id="company-anchor" type="text" class="form-control input-lg" value="My Company Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" value="Account Number">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control input-lg" placeholder="Phone Number">
                                </div>
                                <div class="form-group">
                                    <select name="" id="" class="form-control input-lg">
                                        <option value="">Subject</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <p class="form-control-static">Preferred Method of Contact</p>
                                    <label class="radio-custom radio-inline" data-initialize="radio" id="myCustomRadioLabel5">
                                      <input class="sr-only"  checked="checked" name="radioEx2" type="radio" value="option1"> Email
                                    </label>
                                    <label class="radio-custom radio-inline" data-initialize="radio" id="myCustomRadioLabel6">
                                      <input class="sr-only" name="radioEx2" type="radio" value="option2"> Phone
                                    </label>
                                </div>
                                <div class="form-group">
                                    <textarea name="" id="" cols="5" rows="7" class="form-control" placeholder="Comments"></textarea>
                                </div>
                                <div class="form-group">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="btn btn-warning btn-lg">Submit</a>
                                        </li>
                                        <li>
                                            <a href="#" class="btn btn-default btn-lg">Cancel</a>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->

    <dsp:include page="includes/footer.jsp"/>
    <dsp:include page="includes/scripts.jsp"/>
</body>

</html>

<dsp:include page="includes/modals.jsp"/>
</dsp:page>