<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <dsp:include page="includes/header-logged.jsp"/>
    <!-- header end -->
    
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                  <li><a href="#">My Account</a></li>
                  <li class="active">My Material Lists</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>My Material Lists</h1>
            </div>
            <div class="col-xs-12">
                <div class="well well-gray">
                    <h4>Search Material Lists</h4>
                    <form class="row">
                        <div class="col-md-8 col-sm-12">
                            <div class="row smaller">
                                <div class="col-xs-6">
                                    <input type="text" class="form-control input-lg" placeholder="Material List Name or Keyword ">
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" class="btn btn-lg btn-warning btn-block">Search</a>
                                </div>
                                <div class="col-xs-3">
                                    <a href="#" class="btn btn-lg btn-default btn-block">Reset</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <a href="#" class="btn btn-lg btn-warning btn-block">Create New Material Lists</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="material-list">
                    <div class="row">
                        <div class="col-sm-8 info-block">
                        <h4 class="title"><a href="material-lists-detail.php">Material List Name</a></h4>
                        <div class="info">
                            <div class="box">
                                <strong>Created</strong>
                                <span>01/2/2015</span>
                            </div>
                            <div class="box">
                                <strong># of Items</strong>
                                <span>25</span>
                            </div>
                        </div>
                        <p>This is a job is for NW Hospital and is managed by John Smith and his team. We can add more detail but not over 125 characters for best fit in the space.</p>
                        <ul class="list-inline buttons">
                            <li>
                                <a href="#" class="btn btn-default btn-lg">View/Edit Material List</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-warning btn-lg">Add all Items to Cart</a>
                            </li>
                        </ul>
                        </div>
                        <div class="col-sm-4 actions">
                            <div class="list-group">
                              <a href="#" class="list-group-item">Check Price and Availability</a>
                              <a href="#" class="list-group-item">Share</a>
                              <a href="#" class="list-group-item">Download</a>
                              <a href="#" class="list-group-item">Delete Material List</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="material-list">
                    <div class="row">
                        <div class="col-sm-8 info-block">
                        <h4 class="title"><a href="material-lists-detail.php">Material List Name</a></h4>
                        <div class="info">
                            <div class="box">
                                <strong>Created</strong>
                                <span>01/2/2015</span>
                            </div>
                            <div class="box">
                                <strong># of Items</strong>
                                <span>25</span>
                            </div>
                        </div>
                        <p>This is a job is for NW Hospital and is managed by John Smith and his team. We can add more detail but not over 125 characters for best fit in the space.</p>
                        <ul class="list-inline buttons">
                            <li>
                                <a href="#" class="btn btn-default btn-lg">View/Edit Material List</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-warning btn-lg">Add all Items to Cart</a>
                            </li>
                        </ul>
                        </div>
                        <div class="col-sm-4 actions">
                            <div class="list-group">
                              <a href="#" class="list-group-item">Check Price and Availability</a>
                              <a href="#" class="list-group-item">Share</a>
                              <a href="#" class="list-group-item">Download</a>
                              <a href="#" class="list-group-item">Delete Material List</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="material-list">
                    <div class="row">
                        <div class="col-sm-8 info-block">
                        <h4 class="title"><a href="material-lists-detail.php">Material List Name</a></h4>
                        <div class="info">
                            <div class="box">
                                <strong>Created</strong>
                                <span>01/2/2015</span>
                            </div>
                            <div class="box">
                                <strong># of Items</strong>
                                <span>25</span>
                            </div>
                        </div>
                        <p>This is a job is for NW Hospital and is managed by John Smith and his team. We can add more detail but not over 125 characters for best fit in the space.</p>
                        <ul class="list-inline buttons">
                            <li>
                                <a href="#" class="btn btn-default btn-lg">View/Edit Material List</a>
                            </li>
                            <li>
                                <a href="#" class="btn btn-warning btn-lg">Add all Items to Cart</a>
                            </li>
                        </ul>
                        </div>
                        <div class="col-sm-4 actions">
                            <div class="list-group">
                              <a href="#" class="list-group-item">Check Price and Availability</a>
                              <a href="#" class="list-group-item">Share</a>
                              <a href="#" class="list-group-item">Download</a>
                              <a href="#" class="list-group-item">Delete Material List</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 text-center">
                <nav>
                  <ul class="pagination pagination-lg">
                    <li class="prev">
                      <a href="#" aria-label="Previous">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><span>…</span></li>
                    <li class="next">
                      <a href="#" aria-label="Next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                      </a>
                    </li>
                  </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <dsp:include page="includes/footer.jsp"/>
    <dsp:include page="includes/scripts.jsp"/>
</body>

</html>

<dsp:include page="includes/modals.jsp"/>
</dsp:page>