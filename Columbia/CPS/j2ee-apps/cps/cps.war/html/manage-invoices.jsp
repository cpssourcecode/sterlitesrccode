<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <dsp:include page="includes/header-logged.jsp"/>
    <!-- header end -->
    
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                  <li><a href="#">My Account</a></li>
                  <li class="active">Manage Invoices</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>Manage Invoices</h1>
            </div>
            <div class="col-xs-12">
                <div class="well well-gray well-invoices">
                    <h4>Search Invoices</h4>
                    <form class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="row smaller">
                                        <div class="col-sm-2 col-xs-12">
                                            <div class="radio">
                                              <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                                                <input class="sr-only" name="radioEx1" type="radio" value="option1">
                                                PO#
                                              </label>
                                            </div>
                                            <div class="radio checked">
                                              <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                                                <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="option2">
                                                Invoice#
                                              </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-xs-12">
                                            <input type="text" class="form-control input-lg" placeholder="#">
                                            <br>
                                        </div>
                                        <div class="col-sm-5 col-xs-12">
                                            <select name="" id="" class="form-control input-lg">
                                                <option value="">Ship to</option>
                                                <option value="">NW Hosp — 17th Floor, Imaging (Current)</option>
                                            </select>
                                            <br>
                                        </div>
                                        <div class="col-sm-3 col-xs-6">
                                            <select name="" id="" class="form-control input-lg">
                                                <option value="">Date Range</option>
                                                <option value="">30 days</option>
                                            </select>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="row smaller">
                                        <div class="col-sm-6 col-xs-6">
                                            <a href="#" class="btn btn-lg btn-warning btn-block btn-invoices-search">Search</a>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <a href="#" class="btn btn-lg btn-default btn-block btn-invoices-search">Reset</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><strong>Invoice #</strong> <i class="fa fa-sort"></i></th>
                            <th><strong>Invoice date</strong> <i class="fa fa-sort"></i></th>
                            <th><strong>Ship to</strong></th>
                            <th><strong>Amount</strong></th>
                            <th colspan="2"><strong>Status</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                344364
                            </td>
                            <td>
                                01/13/2015
                            </td>
                            <td>
                                My Name
                            </td>
                            <td>
                                498.45
                            </td>
                            <td>
                                <a href="#" class="table-icon-link"><span>Get Status</span></a>
                            </td>
                            <td>
                                <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                344364
                            </td>
                            <td>
                                01/13/2015
                            </td>
                            <td>
                                My Name
                            </td>
                            <td>
                                498.45
                            </td>
                            <td>
                                <a href="#" class="table-icon-link"><span>Get Status</span></a>
                            </td>
                            <td>
                                <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                344364
                            </td>
                            <td>
                                01/13/2015
                            </td>
                            <td>
                                My Name
                            </td>
                            <td>
                                498.45
                            </td>
                            <td>
                                <a href="#" class="table-icon-link"><span>Get Status</span></a>
                            </td>
                            <td>
                                <a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a>
                            </td>
                        </tr>
                    </tbody>
                </table>                
            </div>
            <div class="col-xs-12 text-center">
                <nav>
                  <ul class="pagination pagination-lg">
                    <li class="prev">
                      <a href="#" aria-label="Previous">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><span>…</span></li>
                    <li class="next">
                      <a href="#" aria-label="Next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                      </a>
                    </li>
                  </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <dsp:include page="includes/footer.jsp"/>
    <dsp:include page="includes/scripts.jsp"/>
</body>

</html>

<dsp:include page="includes/modals.jsp"/>
</dsp:page>