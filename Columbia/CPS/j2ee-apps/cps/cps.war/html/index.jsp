<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>CPS index</h1>
                <ul class="list-unstyled">
                    <li><a href="home.jsp">Home</a></li>
                    <li><a href="home-logged-in.jsp">Home Logged In</a></li>
                    <li><a href="contact-us.jsp">Contact Us</a></li>
                    <li><a href="manage-invoices.jsp">Manage Invoices</a></li>
                    <li><a href="material-lists.jsp">Material Lists</a></li>
                    <li><a href="material-lists-detail.jsp">Material List Detail</a></li>
                    <li><a href="no-material-lists.jsp">No Material Lists</a></li>
                    <li><a href="my-account.jsp">My Account</a></li>
                    <li><a href="pdp.jsp">PDP</a> <small class="text-muted">Not finished</small></li>
                    <li><a href="view-address.jsp">View Address</a></li>
                    <li><a href="view-profile.jsp">View Profile</a></li>
                </ul>
            </div>
        </div>
    </div>
</body>

</html>
</dsp:page>
