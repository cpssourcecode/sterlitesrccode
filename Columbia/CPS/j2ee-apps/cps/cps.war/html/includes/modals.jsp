<!-- Find Invoice Modal -->
<div class="modal fade" id="findInvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Find an Invoice</h2>
        <div class="row">
            <div class="col-xs-8 col-sm-10">
                <input type="text" class="form-control" placeholder="Enter Order # or P.O. #">
            </div>
            <div class="col-xs-4 col-sm-2">
                <a href="#" class="btn btn-warning btn-block">Go</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Find Packing Slip -->
<div class="modal fade" id="findPackingSlip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Find a Packing Slip</h2>
        <div class="row">
            <div class="col-xs-8 col-sm-10">
                <input type="text" class="form-control" placeholder="Enter Order # or P.O. #">
            </div>
            <div class="col-xs-4 col-sm-2">
                <a href="#" class="btn btn-warning btn-block">Go</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Find Order -->
<div class="modal fade" id="findOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Find an Order</h2>
        <div class="row">
            <div class="col-xs-8 col-sm-10">
                <input type="text" class="form-control" placeholder="Enter Order # or P.O. #">
            </div>
            <div class="col-xs-4 col-sm-2">
                <a href="#" class="btn btn-warning btn-block">Go</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Find Order -->
<div class="modal fade" id="requestQuote" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-search"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Request a Quote</h2>
        <p><small>To request a quote, upload a document in one of the folloiwng formats: .doc, .pdf, .exl.</small></p>
        <div class="row">
            <div class="col-xs-8 col-sm-10">
                <input type="text" class="form-control" placeholder="Enter Order # or P.O. #">
            </div>
            <div class="col-xs-4 col-sm-2">
                <a href="#" class="btn btn-warning btn-block">Go</a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Log In -->
<div class="modal fade" id="logIn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-user"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Log In to Your Account</h2>
        <form>
          <div class="form-group">
            <input type="email" class="form-control input-lg autofocus-input" id="exampleInputEmail1" placeholder="Email" autofocus>
          </div>
          <div class="form-group">
            <input type="password" autocomplete="new-password" class="form-control input-lg" id="exampleInputPassword1" placeholder="Password">
          </div>
          <div class="form-group">
            <div class="checkbox checked" id="myCheckbox">
              <label class="checkbox-custom checked" data-initialize="checkbox">
                <input class="sr-only" type="checkbox" value="" checked>
                <span class="checkbox-label">Remember Me</span>
              </label>
            </div>
          </div>
          <div class="form-group">
            <p>Enter the following characters exactly how they appear.</p>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <div class="well well-sm text-center">
                  <p>Captcha Code</p>
                </div>
              </div>
              <div class="col-sm-6">
                <input type="text" class="form-control input-lg" placeholder="Enter captcha code">
              </div>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-warning btn-huge btn-block">Log In</button>
          </div>
          <div class="form-group">
            <p><a class="text-link close-this-modal" href="#" data-toggle="modal" data-target="#forgotPassword"><span>Forgot Password</span></a></p>
            <p>Don't have a login? <a class="text-link" href="#"><span>Request an account</span></a>.</p>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Forgot Password -->
<div class="modal fade" id="forgotPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-user"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Forgot Password</h2>
        <form>
          <div class="form-group">
            <p>Enter the email you use to login and we will send a temporary password to the email address on file. </p>
          </div>
          <div class="form-group">
            <input type="email" class="form-control input-lg" id="exampleInputPassword1" placeholder="Email">
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-warning btn-huge btn-block" data-toggle="modal" data-target="#resetPassword">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Reset Password -->
<div class="modal fade" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-user"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Reset Your Password</h2>
        <form>
          <div class="form-group">
            <input type="password" autocomplete="new-password" class="form-control input-lg" id="exampleInputPa" placeholder="Enter New Password">
          </div>
          <div class="form-group">
            <input type="password" autocomplete="new-password" class="form-control input-lg" id="exampleInputPa" placeholder="Re-enter New Password">
          </div>
          <div class="form-group">
            <div class="checkbox checked" id="myCheckbox">
              <label class="checkbox-custom checked" data-initialize="checkbox">
                <input class="sr-only" type="checkbox" value="" checked>
                <span class="checkbox-label">Remember Me</span>
              </label>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-warning btn-huge btn-block">Login</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Select a ship to address -->
<div class="modal fade" id="selectShipAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-page-name='modals.jsp'>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-align-justify"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Select a Ship To Address</h2>
        <form>
          <div class="form-group">
            <p><strong>For this session</strong>, confirm your default Ship To Address or choose an alternate Ship To Address from the list below. </p>
            <p>Your Default Ship To Address</p>
          </div>
          <div class="form-group">
            <div class="radio checked">
              <label class="radio-custom checked" data-initialize="radio" id="myCustomRadioLabel">
                <input class="sr-only" name="radioEx1" type="radio" value="option1"checked>
                NW Hospital — 17th Floor, Imaging
              </label>
            </div>
          </div>
          <hr>
          <div class="form-group">
            <p>Select an Alternate Ship To Address for this Session</p>
            <div class="row">
              <div class="col-sm-6">
                <input type="text" class="form-control input-lg" placeholder="Search for Ship To Address">
              </div>
              <div class="col-sm-3">
                <a href="#" class="btn btn-block btn-warning btn-lg">Search</a>
              </div>
              <div class="col-sm-3">
                <a href="#" class="btn btn-block btn-default btn-lg">Reset</a>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="scrollable">
              <div class="head">
                <span>Select</span>
                <span>Ship To</span>
              </div>
              <div class="content scrollbar-inner">
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option2">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio checked">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option3">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option4">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option5">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option6">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option7">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option8">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option9">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
                <div class="radio">
                  <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                    <input class="sr-only" name="radioEx1" type="radio" value="option10">
                    NW Hospital — 17th Floor, Imaging
                  </label>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-warning btn-lg pull-left">Confirm</a>
      </div>
    </div>
  </div>
</div>

<!-- Check Price and Availability -->
<div class="modal fade" id="myCheckPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-th-list"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Import/Upload List</h2>
        <div class="row">
            <div class="col-lg-12">
              <p>To add items to the Material List, upload a spreadsheet in .exl format.</p>
            </div>
            <div class="col-lg-12">
              <div class="kv-main">
                <form enctype="multipart/form-data">
                  <div class="form-group">
                    <input type="file" multiple class="file fileupload" data-overwrite-initial="false" data-min-file-count="1">
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-xs-6 text-left">
            <ul class="list-inline">
              <li>
                <a href="#" class="btn btn-lg btn-warning">Upload</a>
              </li>
              <li>
                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
              </li>
            </ul>
          </div>
          <div class="col-xs-6">
            <span class="modal-footer-noreload"><span class="text-muted glyphicon glyphicon-download-alt"></span> <a href="#" class="gray-noreload"><span>Download Spreadsheet Template</span></a></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Check Availability -->
<div class="modal fade" id="myModalCheck" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-ok"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Check Availability</h2>
        <div class="row">
            <div class="col-lg-12">
              <div class="media media-product">
                <div class="media-left">
                  <div class="image">
                    <a href="#">
                      <img class="media-object" src="${staticContentPrefix}/assets/images/pipe.png" alt="...">
                    </a>
                  </div>
                </div>
                <div class="media-body">
                  <h3 class="media-heading"><a href="#">Std Blk, Galv PE&T&C, XH Blk PE</a></h3>
                  <span>Item #   Part #   UPC   Customer Alias</span>
                </div>
              </div>
              <div>
                <div class="pull-left">
                  <div class="radio checked">
                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel2">
                      <input class="sr-only" checked="checked" name="radioEx1" type="radio" value="option2">
                      Have it Shipped
                    </label>
                  </div>
                  <div class="radio">
                    <label class="radio-custom" data-initialize="radio" id="myCustomRadioLabel">
                      <input class="sr-only" name="radioEx1" type="radio" value="option1">
                      Pick it Up
                    </label>
                  </div>
                </div>
                <div class="pull-left">
                  <form class="form-inline form-check-availability">
                    <div class="form-group">
                      <input type="text" class="form-control input-lg text-center" id="" placeholder="Qty" value="1">
                    </div>
                    <button class="btn btn-warning btn-lg" type="button" data-toggle="collapse" data-target="#collapseCheck" aria-expanded="false" aria-controls="collapseCheck">Check Availability</button>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-lg-12 modal-check-collapse collapse" id="collapseCheck">
              <hr>
              <h3>Have it Shipped</h3>
              <p>Expected Arrival Date:  <strong>Monday, April 27</strong></p>
              <a href="#" class="btn btn-lg btn-warning">Add to Cart</a>
              <br>
              <p><small>Disclaimer copy</small></p>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Check Availability -->
<div class="modal fade" id="addToMaterialList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <i class="fa fa-plus-square"></i>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Add to Material List</h2>
        <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-xs-8">
                  
                </div>
                <div class="col-xs-4">
                  <a href="#" class="btn btn-block btn-lg btn-warning">Add to List</a>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- myModalEditDescription -->
<div class="modal fade" id="myModalEditDescription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-th-list"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Edit Name and Description</h2>
        <div class="row">
          <div class="form">
            <div class="form-group">
              <div class="col-sm-12">
                <input type="text" class="form-control input-lg" value="NW Hospital job #2">
                <br>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <textarea class="form-control input-lg" name="" id="" cols="4" rows="5">This is a job is for NW Hospital and is managed by John Smith and his team. We can add more detail but not over 125 characters for best fit in the space.</textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-xs-12 text-left">
            <ul class="list-inline">
              <li>
                <a href="#" class="btn btn-lg btn-warning">Save Edits</a>
              </li>
              <li>
                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Share list -->
<div class="modal fade" id="myModalShareList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-th-list"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Share Material List</h2>
        <div class="row">
          <div class="form">
            <div class="form-group">
              <div class="col-sm-12">
                <select name="" id="" class="form-control input-lg">
                  <option value="">Select</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-xs-12 text-left">
            <ul class="list-inline">
              <li>
                <a href="#" class="btn btn-lg btn-warning">Share</a>
              </li>
              <li>
                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Delete List -->
<div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <div class="header-symbol">
            <span class="glyphicon glyphicon-alert"></span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Find an Invoice</h4> -->
      </div>
      <div class="modal-body">
        <h2 class="modal-title">Delete Material List</h2>
        <div class="row">
          <div class="col-sm-12">
            <p>Are you sure you want to delete this Material List?</p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-xs-12 text-left">
            <ul class="list-inline">
              <li>
                <a href="#" class="btn btn-lg btn-warning">Proceed</a>
              </li>
              <li>
                <a href="#" class="btn btn-lg btn-default" data-dismiss="modal">Cancel</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>