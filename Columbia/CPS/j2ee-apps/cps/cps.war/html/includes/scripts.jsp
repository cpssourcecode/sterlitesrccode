    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="${staticContentPrefix}assets/javascripts/jquery-1.11.3.min.js"></script>
    <script src="${staticContentPrefix}assets/javascripts/jquery-migrate-1.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${staticContentPrefix}assets/javascripts/bootstrap.js"></script>
    <%--<script src="${staticContentPrefix}assets/javascripts/bootstrap/collapse.js"></script>--%>
    <script src="${staticContentPrefix}assets/javascripts/bootstrap/tooltip.js"></script>
    <script src="${staticContentPrefix}assets/javascripts/bootstrap/popover.js"></script>
    <script src="${staticContentPrefix}assets/javascripts/bootstrap/alert.js"></script>
    <!-- // <script src="${staticContentPrefix}assets/javascripts/bootstrap/modal.js"></script> -->
    <!-- // <script src='assets/javascripts/bootstrap/dropdown.js'></script> -->
    <script src="${staticContentPrefix}assets/javascripts/bootstrap-hover-dropdown.min.js"></script>
    <!-- fuel ux -->
    <script src="${staticContentPrefix}assets/javascripts/fuelux/checkbox.js"></script>
    <script src="${staticContentPrefix}assets/javascripts/fuelux/radio.js"></script>
    <!-- owl -->
    <script src='assets/javascripts/owl/owl.carousel.min.js'></script>
    <!-- custom scroll -->
    <!-- dropdown-enchancements -->
    <script src='assets/javascripts/jquery.scrollbar.min.js'></script>
    <!-- // <script src='assets/javascripts/owl/owl.support.modernizr.js'></script> -->
    <!-- fileinput -->
    <script src='assets/javascripts/bootstrap-fileinput/fileinput.min.js'></script>
    <script src='assets/javascripts/fileupload-ui.js'></script>
    <!-- custom -->
    <script src="${staticContentPrefix}assets/javascripts/prototype.js"></script>