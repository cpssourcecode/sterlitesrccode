<footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 visible-xs">
                    <h4><span class="text-warning">Questions?</span><br>
                    <span class="phone">777-927-6600</span>
                    </h4>
                    <br>
                    <h4 class="text-warning">Recieve Special Offers</h4>
                    <form class="subscribe">
                        <div class="row">
                            <div class="col-lg-8 col-xs-8">
                                <input type="email" class="form-control" placeholder="Enter Email Address">
                            </div>
                            <div class="col-lg-4 col-xs-4">
                                <button type="submit" class="btn btn-warning btn-block">Sign Up</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3">
                    <strong>Products</strong>
                    <ul class="list-unstyled">
                        <li><a href="#">Carbon Steel Pipe</a></li>
                        <li><a href="#">SS Pipe & Fittings</a></li>
                        <li><a href="#">Plastic Pipe & Fittings</a></li>
                        <li><a href="#">Soil Pipe & Fittings</a></li>
                        <li><a href="#">Fittings & Nipples</a></li>
                        <li><a href="#">Hangers/Rod/Strut</a></li>
                        <li><a href="#">Weld Fittings & Flanges</a></li>
                        <li><a href="#">Valves</a></li>
                        <li><a href="#">Plumbing Fixtures</a></li>
                        <li><a href="#">Plumbing Brass</a></li>
                        <li><a href="#">Water Heaters</a></li>
                        <li><a href="#">Pumps</a></li>
                        <li><a href="#">Hydronic Equipment</a></li>
                        <li><a href="#">Hydronic Misc.</a></li>
                        <li><a href="#">HVAC Equipment</a></li>
                        <li><a href="#">HVAC Assoc. Products</a></li>
                        <li><a href="#">Maintenance Supplies</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <strong>Services</strong>
                    <ul class="list-unstyled">
                        <li><a href="#">Custom Product Service & Support</a></li>
                        <li><a href="#">Inventory Solutions </a></li>
                        <li><a href="#">Kitting </a></li>
                        <li><a href="#">Standardization</a></li>
                        <li><a href="#">CAPS Valve Automation</a></li>
                        <li><a href="#">Inter-Branch Transfer System</a></li>
                        <li><a href="#">Stock Solutions </a></li>
                    </ul>
                    <strong>Our Company</strong>
                    <ul class="list-unstyled">
                        <li><a href="#">Locations</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Support</a></li>
                        <li><a href="#">News and Events</a></li>
                        <li><a href="#">Affiliations</a></li>
                    </ul>
                </div>
                <div class="col-sm-3">
                    <strong>My Account</strong>
                    <ul class="list-unstyled">
                        <li><a href="#">Orders</a></li>
                        <li><a href="#">Packing Slips</a></li>
                        <li><a href="#">Manage Ship To Addresses</a></li>
                        <li><a href="#">Manage Users</a></li>
                        <li><a href="#">Invoices</a></li>
                        <li><a href="#">Statements</a></li>
                        <li><a href="#">My Material Lists</a></li>
                        <li><a href="#">Company Settings</a></li>
                        <li><a href="#">Edit Profile</a></li>
                    </ul>
                </div>
                <div class="col-sm-3 hidden-xs">
                    <h4><span class="text-warning">Questions?</span><br>
                    <span class="phone">777-927-6600</span>
                    </h4>
                    <br>
                    <h4 class="text-warning">Recieve Special Offers</h4>
                    <form class="subscribe">
                        <div class="row">
                            <div class="col-lg-8 col-xs-8 col-sm-12">
                                <input type="email" class="form-control" placeholder="Enter Email Address">
                            </div>
                            <div class="col-lg-4 col-xs-4 col-sm-12">
                                <button type="submit" class="btn btn-warning btn-block">Sign Up</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <p>Copyright © 2008&ndash;2015 Columbia Pipe & Supply Co.</p>
                </div>
            </div>
        </div>
    </footer>