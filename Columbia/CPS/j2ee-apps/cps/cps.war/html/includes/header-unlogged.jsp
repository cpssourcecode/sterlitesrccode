<header>
        <div class="login-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        <a href="#" class="pull-right" data-toggle="modal" data-target="#logIn"><span>Log In</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="brand-box">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 logo-box">
                        <a href="home-logged-in.php" class="logo"><img src="${staticContentPrefix}assets/images/logo.png" alt=""></a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 user-actions-box">
                        <ul class="list-unstyled pull-right">
                            <li class="text-right hidden-xs"><a class="link" href="#" data-toggle="modal" data-target="#selectShipAddress">Locations</a><span class="line">|</span><a class="link" href="#">Contact</a></li>
                            <li class="text-right dropdowns">
                                <div class="dropdown">
                                    <button class="btn btn-default btn-lg btn-services" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Services
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-services pull-right" role="menu" aria-labelledby="dLabel">
                                        <li><a href="#">Custom Product Service and Support</a></li>
                                        <li><a href="#">Inventory Solutions</a></li>
                                        <li><a href="#">Kitting</a></li>
                                        <li><a href="#">Standardization</a></li>
                                        <li><a href="#">CAPS Valve Automation</a></li>
                                        <li><a href="#">Inter-Branch Trans-fer System — Stock Solutions</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-inverse">
            <div class="container">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="navbar-collapse navbar-menu">
                    <ul class="nav navbar-nav navbar-menu">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><span class="visible-xs glyphicon glyphicon-align-justify"></span><span class="hidden-xs">Products</span> <span class="caret hidden-xs"></span></a>
                            <ul class="dropdown-menu mainmenu-dropdown" role="menu">
                                <li class="dropdown-submenu dropdown-hover-trigger dropdown-hover">
                                    <a href="#" tabindex="0" data-toggle="dropdown" aria-expanded="false">Carbon Steel Pipe</a>
                                    <ul class="dropdown-menu dropdown-second-level">
                                        <li><a href="#">Domestic Seamless Pipe 2-12</a></li>
                                        <li><a href="#">Domestic OD SMLS 14-24 Pipe</a></li>
                                        <li><a href="#">Pressure Pipe A106 & Global</a></li>
                                        <li><a href="#">Fusion Bond Pipe</a></li>
                                        <li><a href="#">Kottler HSC Items</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu dropdown-hover-trigger dropdown-hover">
                                    <a href="#" tabindex="0" data-toggle="dropdown" aria-expanded="false">SS Pipe & Fitting</a>
                                    <ul class="dropdown-menu dropdown-second-level">
                                        <li><a href="#">Domestic Seamless Pipe 2-12</a></li>
                                        <li><a href="#">Domestic OD SMLS 14-24 Pipe</a></li>
                                        <li><a href="#">Pressure Pipe A106 & Global</a></li>
                                        <li><a href="#">Fusion Bond Pipe</a></li>
                                        <li><a href="#">Kottler HSC Items</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Plastic Pipe & Fittings</a></li>
                                <li><a href="#">Soli Pipe & Fittings</a></li>
                                <li><a href="#">Fittings & Nipples</a></li>
                                <li><a href="#">Hangers/Rood/Strut</a></li>
                                <li><a href="#">Weld Fittings & Flanges</a></li>
                                <li><a href="#">Valves</a></li>
                                <li><a href="#">Plumbing Fixtures</a></li>
                                <li><a href="#">Plumbing Brass</a></li>
                                <li><a href="#">Water Heters</a></li>
                                <li><a href="#">Pumps</a></li>
                                <li><a href="#">Hydronic Equipment</a></li>
                                <li><a href="#">Hydronic Misc.</a></li>
                                <li><a href="#">HVAC Equipement</a></li>
                                <li><a href="#">MVAC Assoc. Products</a></li>
                                <li><a href="#">Mainteance Supplies</a></li>
                            </ul>
                        </li>
                    </ul>
                    <form class="navbar-form navbar-left" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" placeholder="Search by keyword, item #">
                        </div>
                        <button type="submit" class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-search"></span></button>
                    </form>
                    <ul class="nav navbar-nav navbar-right navbar-cart">
                        <li><a href="#"><span class="hidden-xs">Cart</span> <span class="glyphicon glyphicon-shopping-cart text-warning"></span> (48)</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </header>