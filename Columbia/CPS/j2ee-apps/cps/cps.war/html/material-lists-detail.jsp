<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <dsp:include page="includes/header-logged.jsp"/>
    <!-- header end -->
    
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                  <li><a href="#">My Account</a></li>
                  <li><a href="#">My Material Lists</a></li>
                  <li class="active">Material List Detail</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>Material List Detail</h1>
            </div>
            <div class="col-xs-12">
                <div class="material-list detail">
                    <div class="row">
                        <div class="col-sm-8 info-block">
                        <h4 class="title">Material List Name</h4>
                        <div class="info">
                            <div class="box">
                                <strong>Created</strong>
                                <span>01/2/2015</span>
                            </div>
                            <div class="box">
                                <strong># of Items</strong>
                                <span>25</span>
                            </div>
                        </div>
                        <p>This is a job is for NW Hospital and is managed by John Smith and his team. We can add more detail but not over 125 characters for best fit in the space.</p>
                        </div>
                        <div class="col-sm-4 actions">
                            <div class="list-group">
                              <a href="#myCheckPrice" class="list-group-item" data-toggle="modal">Check Price and Availability</a>
                              <a href="#myModalEditDescription" class="list-group-item" data-toggle="modal">Edit Name and Description</a>
                              <a href="#myModalShareList" class="list-group-item" data-toggle="modal">Share</a>
                              <a href="#" class="list-group-item">Download</a>
                              <a href="#myModalDelete" class="list-group-item" data-toggle="modal">Delete Material List</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="well well-gray">
                    <h4>Search Material Lists</h4>
                    <form class="row">
                        <div class="col-md-12">
                            <div class="row smaller">
                                <div class="col-sm-6 col-xs-12">
                                    <input type="text" class="form-control input-lg" placeholder="Material List Name or Keyword ">
                                    <br>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <a href="#" class="btn btn-lg btn-warning btn-block">Search</a>
                                </div>
                                <div class="col-sm-3 col-xs-6">
                                    <a href="#" class="btn btn-lg btn-default btn-block">Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12">
                <ul class="list-inline">
                    <li>
                        <a class="btn btn-warning btn-lg" href="#">Add Selected to Cart</a>
                    </li>
                    <li>
                        <a class="btn btn-warning btn-lg" href="#">Delete Selected</a>
                    </li>
                    <li>
                        <a class="btn btn-warning btn-lg" href="#">Check Price and Availability of Selected</a>
                    </li>
                </ul>
                <br>
            </div>
            <div class="col-xs-12">
                <div class="material-items-list">
                    <div class="header">
                        <div class="check">
                            <div class="checkbox" id="select-item-material">
                              <label class="checkbox-custom" data-initialize="checkbox">
                                <input class="sr-only" type="checkbox" value="">
                                <span class="checkbox-label"><strong>Select All</strong></span>
                              </label>
                            </div>
                        </div>
                        <div class="name">
                            <span class="title"><strong>Item Name</strong></span>
                        </div>
                        <div class="item-number">
                            <span class="title"><strong>Item#</strong></span>
                        </div>
                        <div class="qty">
                            <span class="title"><strong>Quantity</strong></span>
                        </div>
                    </div>
                    <div class="content-list scrollbar-inner">
                        <div class="content">
                            <div class="check">
                                <div class="checkbox image" id="select-item-material-check">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </span>
                                  </label>
                                </div>
                            </div>
                            <div class="name">
                                <a class="product-title" href="#">Name of product</a>
                            </div>
                            <div class="item-number">
                                4356346
                            </div>
                            <div class="qty">
                                <form class="form-inline">
                                  <div class="form-group">
                                    <input type="text" class="form-control text-center" id="qty1" placeholder="" value="1">
                                  </div>
                                  <div class="form-group">
                                    <label for="qty1"><a href="#" class="gray-noreload"><span>Update</span></a></label>
                                  </div>
                                </form>
                            </div>
                        </div>
                        <div class="content">
                            <div class="check">
                                <div class="checkbox image" id="select-item-material-check">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </span>
                                  </label>
                                </div>
                            </div>
                            <div class="name">
                                <a class="product-title" href="#">Name of product</a>
                            </div>
                            <div class="item-number">
                                4356346
                            </div>
                            <div class="qty">
                                <form class="form-inline">
                                  <div class="form-group">
                                    <input type="text" class="form-control text-center" id="qty1" placeholder="" value="1">
                                  </div>
                                  <div class="form-group">
                                    <label for="qty1"><a href="#" class="gray-noreload"><span>Update</span></a></label>
                                  </div>
                                </form>
                            </div>
                        </div>
                        <div class="content">
                            <div class="check">
                                <div class="checkbox image" id="select-item-material-check">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </span>
                                  </label>
                                </div>
                            </div>
                            <div class="name">
                                <a class="product-title" href="#">Name of product</a>
                            </div>
                            <div class="item-number">
                                4356346
                            </div>
                            <div class="qty">
                                <form class="form-inline">
                                  <div class="form-group">
                                    <input type="text" class="form-control text-center" id="qty1" placeholder="" value="1">
                                  </div>
                                  <div class="form-group">
                                    <label for="qty1"><a href="#" class="gray-noreload"><span>Update</span></a></label>
                                  </div>
                                </form>
                            </div>
                        </div>
                        <div class="content">
                            <div class="check">
                                <div class="checkbox image" id="select-item-material-check">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </span>
                                  </label>
                                </div>
                            </div>
                            <div class="name">
                                <a class="product-title" href="#">Name of product</a>
                            </div>
                            <div class="item-number">
                                4356346
                            </div>
                            <div class="qty">
                                <form class="form-inline">
                                  <div class="form-group">
                                    <input type="text" class="form-control text-center" id="qty1" placeholder="" value="1">
                                  </div>
                                  <div class="form-group">
                                    <label for="qty1"><a href="#" class="gray-noreload"><span>Update</span></a></label>
                                  </div>
                                </form>
                            </div>
                        </div>
                        <div class="content">
                            <div class="check">
                                <div class="checkbox image" id="select-item-material-check">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </span>
                                  </label>
                                </div>
                            </div>
                            <div class="name">
                                <a class="product-title" href="#">Name of product</a>
                            </div>
                            <div class="item-number">
                                4356346
                            </div>
                            <div class="qty">
                                <form class="form-inline">
                                  <div class="form-group">
                                    <input type="text" class="form-control text-center" id="qty1" placeholder="" value="1">
                                  </div>
                                  <div class="form-group">
                                    <label for="qty1"><a href="#" class="gray-noreload"><span>Update</span></a></label>
                                  </div>
                                </form>
                            </div>
                        </div>
                        <div class="content">
                            <div class="check">
                                <div class="checkbox image" id="select-item-material-check">
                                  <label class="checkbox-custom" data-initialize="checkbox">
                                    <input class="sr-only" type="checkbox" value="">
                                    <span class="checkbox-label">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </span>
                                  </label>
                                </div>
                            </div>
                            <div class="name">
                                <a class="product-title" href="#">Name of product</a>
                            </div>
                            <div class="item-number">
                                4356346
                            </div>
                            <div class="qty">
                                <form class="form-inline">
                                  <div class="form-group">
                                    <input type="text" class="form-control text-center" id="qty1" placeholder="" value="1">
                                  </div>
                                  <div class="form-group">
                                    <label for="qty1"><a href="#" class="gray-noreload"><span>Update</span></a></label>
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <dsp:include page="includes/footer.jsp"/>
    <dsp:include page="includes/scripts.jsp"/>
</body>

</html>

<dsp:include page="includes/modals.jsp"/>
</dsp:page>