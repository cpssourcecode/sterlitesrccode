<dsp:page>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>CPS</title>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="assets/stylesheets/styles.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <dsp:include page="includes/header-logged.jsp"/>
    <!-- header end -->
    
    <div class="container main-content">
        <div class="row">
            <div class="col-sm-8">
                <ol class="breadcrumb">
                  <li class="active">My Account Overview</li>
                </ol>
            </div>
            <div class="col-sm-4 page-actions">
                <ul class="list-inline">
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-envelope-o"></i><span>Email Page</span>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="action">
                            <i class="fa fa-print"></i><span>Print Page</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12">
                <h1>My Account Overview</h1>
            </div>
            <div class="col-xs-12">
                <div class="my-account-links">
                    <div class="item">
                        <h4>Manage Orders</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Orders</a></li>
                            <li><a href="#">Packing slips</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <h4>Billing & Payment</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Invoices</a></li>
                            <li><a href="#">Statements</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <h4>Admin Tools</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Company Settings</a></li>
                            <li><a href="#">Manage Users</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <h4>Lists</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">My Material Lists</a></li>
                        </ul>
                    </div>
                    <div class="item">
                        <h4>My Profile</h4>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">View Addresses</a></li>
                            <li><a href="#">Edit Profile</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div>
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#rinvoices" aria-controls="rinvoices" role="tab" data-toggle="tab">Recent Invoices</a></li>
                    <li role="presentation"><a href="#rorders" aria-controls="rorders" role="tab" data-toggle="tab">Recent Orders</a></li>
                  </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="rinvoices">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th><strong>Invoice #</strong></th>
                              <th><strong>Invoice Date</strong> <i class="fa fa-sort"></i></th>
                              <th><strong>Ship To</strong></th>
                              <th><strong>Amount</strong></th>
                              <th colspan="2"><strong>Status</strong></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>343432</th>
                              <td>01/13/2015</td>
                              <td>My Name</td>
                              <td>$498.45</td>
                              <td><span class="label label-success">Paid</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                            <tr>
                              <th>343432</th>
                              <td>01/13/2015</td>
                              <td>My Name</td>
                              <td>$498.45</td>
                              <td><span class="label label-success">Paid</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                            <tr>
                              <th>343432</th>
                              <td>01/13/2015</td>
                              <td>My Name</td>
                              <td>$498.45</td>
                              <td><span class="label label-success">Paid</span></td>
                              <td><a href="#" class="table-icon-link"><i class="fa fa-file-text-o"></i><span>View Details</span></a></td>
                            </tr>
                          </tbody>
                        </table>
                        <a href="#" class="btn btn-warning btn-lg">Search Invoices</a>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="rorders">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th><strong>Order Number</strong></th>
                              <th><strong>Date Placed</strong> <i class="fa fa-sort"></i></th>
                              <th><strong>Total</strong></th>
                              <th><strong>Ship To</strong></th>
                              <th><strong>Status</strong>  <i class="fa fa-sort"></i></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>243432</th>
                              <td>01/13/2015</td>
                              <td>$498.45</td>
                              <td>John Smith</td>
                              <td><span class="label label-danger">Pending Approval</span></td>
                            </tr>
                            <tr>
                              <th>243432</th>
                              <td>01/13/2015</td>
                              <td>$498.45</td>
                              <td>John Smith</td>
                              <td>Shipped</td>
                            </tr>
                            <tr>
                              <th>243432</th>
                              <td>01/13/2015</td>
                              <td>$498.45</td>
                              <td>John Smith</td>
                              <td>Shipped</td>
                            </tr>
                          </tbody>
                        </table>
                        
                        <a href="#" class="btn btn-warning btn-lg">Search Orders</a>
                    </div>
                  </div>

                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->
    <dsp:include page="includes/footer.jsp"/>
    <dsp:include page="includes/scripts.jsp"/>
</body>

</html>

<dsp:include page="includes/modals.jsp"/>
</dsp:page>