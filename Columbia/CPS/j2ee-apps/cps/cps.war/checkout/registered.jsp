<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/commerce/order/FullShoppingCartFormHandler"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />

	<dsp:importbean bean="/cps/droplet/StoreLookupDroplet"/>
	<dsp:importbean bean="/cps/util/CPSCheckoutSessionBean"/>

	<dsp:droplet name="Switch">
		<dsp:param bean="ShoppingCart.current.allowCheckout" name="value"/>
		<dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/checkout/cart.jsp"/>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/"/>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

	<%--<dsp:getvalueof var="orderBeenPlaced" bean="FullShoppingCartFormHandler.orderBeenPlaced"/>--%>

	<cp:pageContainer page="checkout" disable="true">

		<div id="order-reprice">
			<dsp:include page="${originatingRequest.contextPath}/checkout/gadgets/order-reprice.jsp" />
		</div>

		<dsp:include page="${originatingRequest.contextPath}/checkout/gadgets/breadcrumb.jsp">
			<dsp:param name="selectedStep" value="registered"/>
		</dsp:include>

		<dsp:form name="paymentForm" id="paymentForm" action="" method="POST">

			<div class="container">
				<div class="row">
					<div class="col-lg-12 block-bordered">
						<h2>Checkout</h2>

						<div class="alert alert-danger" style="display:none;" id="error-messages"></div>

					</div>

					<dsp:droplet name="/atg/dynamo/droplet/RQLQueryForEach">
						<dsp:param name="repository" value="/cps/registry/repository/BlackoutDatesRepository"/>
						<dsp:param name="itemDescriptor" value="blackoutDates"/>
						<dsp:param name="queryRQL" value="ALL"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="blackoutDate" param="element.blackoutDate"/>
							<dsp:getvalueof var="count" param="count"/>
							<fmt:formatDate var="currentBlackout" value="${blackoutDate}" pattern="MM/dd/yyyy"/>
							<input type="hidden" value="${currentBlackout}" id="hiddenBlackOut${count}"/>
						</dsp:oparam>
					</dsp:droplet>

					<!-- Addresses -->
					<dsp:include page="${originatingRequest.contextPath}/checkout/gadgets/addresses.jsp"/>

                    <!-- Delivery options -->
					<dsp:include page="${originatingRequest.contextPath}/checkout/gadgets/delivery-options.jsp"/>

					<!-- Payment -->
					<dsp:include page="${originatingRequest.contextPath}/checkout/gadgets/payment-options.jsp"/>


					<div class="col-lg-6">
						<%--<dsp:input type="hidden" bean="FullShoppingCartFormHandler.moveToConfirmationSuccessURL" value="${originatingRequest.contextPath}/checkout/review.jsp"/>--%>
						<%--<dsp:input type="hidden" bean="FullShoppingCartFormHandler.moveToConfirmationErrorURL" value="${originatingRequest.contextPath}/checkout/registered.jsp"/>--%>
						<dsp:input bean="FullShoppingCartFormHandler.moveToConfirmation" type="hidden" value="Review Order" priority="-10" />
						<a href="#" data-event-click-id="checkout17"  class="btn btn-success btn-lg pull-right">Review Order</a>
					</div>
				</div>
			</div>

		</dsp:form>

	</cp:pageContainer>
</dsp:page>