<dsp:page>
	<dsp:importbean bean="/cps/droplet/StoreLookupDroplet"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:getvalueof var="id" param="id"/>
	<dsp:getvalueof var="time" param="time"/>
	<dsp:getvalueof var="when" param="when"/>

	<%@ page import="java.util.Date" %>
	<c:set var="today" value="<%=new Date()%>"/>
	<dsp:droplet name="StoreLookupDroplet">
		<dsp:param name="storeId" value="${id}"/>
		<dsp:param name="time" value="${time}"/>
		<dsp:oparam name="output">
			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="element"/>
				<dsp:oparam name="outputStart">
					<dsp:getvalueof var="timezone" param="timezone"/>
					<input type="hidden" id="timezoneVar" value="${timezone}"/>
					<fmt:formatDate var="now" type="time" pattern="HH:mm a" timeZone="${timezone}" value='${today}'/>
					<select id="pickUpTime" data-width="100%" class="selectpicker">
						<option selected="selected" disabled="disabled" value="">Select...</option>
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:getvalueof var="data" param="element"/>

					<c:set var="storeTimes" value="${fn:split(data,'-')}"/>
					<c:set var="openTime" value="${fn:split(storeTimes[0],' ')}"/>
					<c:set var="closeTime" value="${fn:split(storeTimes[1],' ')}"/>
					<c:set var="currentTime" value="${fn:split(now,' ')}"/>
					<c:set var="openParts" value="${fn:split(storeTimes[0],':')}"/>
					<c:set var="closeParts" value="${fn:split(storeTimes[1],':')}"/>

					<c:choose>
						<c:when test="${ openParts[0] >= '13' }">
							<c:choose>
								<c:when test="${ openParts[0] > '22' }">
									<c:set var="displayOpenTime" value="${openParts[0]}:${openParts[1]}"/>
								</c:when>
								<c:otherwise>
									<c:set var="displayOpenTime" value="0${openParts[0]-12}:${openParts[1]}"/>
								</c:otherwise>
							</c:choose>

						</c:when>
						<c:otherwise>
							<c:set var="displayOpenTime" value="${openParts[0]}:${openParts[1]}"/>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${ closeParts[0] >= '13' }">

							<c:choose>
								<c:when test="${ closeParts[0] > '22' }">
									<c:set var="displayCloseTime" value="${closeParts[0]}:${closeParts[1]}"/>
								</c:when>
								<c:otherwise>
									<c:set var="displayCloseTime" value="0${closeParts[0]-12}:${closeParts[1]}"/>
								</c:otherwise>
							</c:choose>

						</c:when>
						<c:otherwise>
							<c:set var="displayCloseTime" value="${closeParts[0]}:${closeParts[1]}"/>
						</c:otherwise>
					</c:choose>

					<c:set var="closeParts" value="${fn:split(storeTimes[1],':')}"/>

					<c:choose>
						<c:when test="${ ( currentTime[0] < openTime[0] ) || when != 'today' }">
							<option value="${displayOpenTime} to ${displayCloseTime}">
							${displayOpenTime} to ${displayCloseTime}
							</option>
						</c:when>
						<c:otherwise>
							<option value="${displayOpenTime} to ${displayCloseTime}" disabled="disabled">
							${displayOpenTime} to ${displayCloseTime}
							</option>
						</c:otherwise>
					</c:choose>

				</dsp:oparam>
				<dsp:oparam name="outputEnd">
					</select>
				</dsp:oparam>
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="empty">
			<select id="pickUpTime" disabled="disabled" data-width="100%" class="selectpicker">
				<option selected="selected" disabled="disabled">Select...</option>
			</select>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>

<script type="text/javascript" nonce="${requestScope.nonce}">
	$(document).ready(function() {
		$("#pickUpTime").change(function() {
			$("#pickupTime").val($("#pickUpTime").val());
			//checkOptions();
		});
		$("#pickUpTime").val($("#pickupTime").val());
		$("#timezone").html($("#timezoneVar").val());
		$('.selectpicker').selectpicker();
		$('.selectpicker-large').selectpicker('setStyle', 'btn-lg', 'add');
		$('.selectpicker-small').selectpicker('setStyle', 'btn-sm', 'add');
	});


	// 	alert(numAvail);
	// 	var numAvail = $("#pickUpTime option:enabled").length;
	// // 	alert(numAvail);
	//  	if ( numAvail == 0 ) {
	//  	 	alert("disable");
	// 		$("#pickup-date1").attr('disabled',true);
	// 		$("#pickup-date2").attr('disabled',true);
	// 		$("#pickup-date3").attr('disabled',true);
	// 		$("#pickupDate").val();

	// 		//$("#pickup-date2").removeattr('disabled');
	//  	} else {
	//  		alert("enable");
	//  		$("#pickup-date1").removeattr('disabled');
	// // 		$("#pickup-date1").attr('disabled',false);
	//  	}


</script>