<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>

	<dsp:importbean bean="/cps/droplet/CPSPriceDroplet"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet"/>

	<dsp:droplet name="CPSPriceDroplet">
		<dsp:param bean="ShoppingCart.current" name="order"/>
	</dsp:droplet>
	<dsp:droplet name="RepriceOrderDroplet">
		<dsp:param value="ORDER_SHIPPING" name="pricingOp"/>
	</dsp:droplet>
</dsp:page>