<dsp:page>
	<dsp:getvalueof var="formHandler" param="formHandler"/>
	<dsp:getvalueof var="formExceptions" vartype="java.lang.Object" param="formHandler.formExceptions"/>
	<c:if test="${not empty formExceptions}">
		<div class="alert alert-danger" role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">&times;</span>
				<span class="sr-only">Close</span>
			</button>
			<p>
				<c:forEach var="formException" items="${formExceptions}">
					<dsp:param name="formException" value="${formException}"/>
					<dsp:getvalueof var="errorCode" param="formException.errorCode"/>
					<script type="text/javascript" nonce="${requestScope.nonce}">
						jQuery(document).ready(function(){
							loadErrors('${errorCode}');
						});
					</script>
					<c:if test="${formException.message != ''}">
						<dsp:valueof param="formException.message" valueishtml="true"/></br>
					</c:if>
				</c:forEach>
			</p>
		</div>
	</c:if>
</dsp:page>