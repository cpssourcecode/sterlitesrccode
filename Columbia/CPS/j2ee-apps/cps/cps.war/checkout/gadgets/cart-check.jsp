<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/CartValidationHandler"/>
	<dsp:form id="checkOrderForm" formid="checkOrderForm"
			  action="${originatingRequest.requestURI}" method="post">
		<dsp:input type="hidden" value="true" priority="-10" bean="CartValidationHandler.checkCart"/>
	</dsp:form>
	<script type="text/javascript" nonce="${requestScope.nonce}">
		var options = {
			type: "POST",
			dataType : 'json',
			success: function(data) {
				if(typeof data.errors != "undefined" && data.errors.length > 0) {
					reloadMinicartCount();
					reloadCart();
					$("#check-cart-notification").show().append(data.errors[0]);
				}
			}
		};
		var checkOrderForm = $("#checkOrder");
		checkOrderForm.ajaxForm(options);
		checkOrderForm.submit();
	</script>
</dsp:page>