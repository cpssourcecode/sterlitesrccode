<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>

	<div class="col-lg-12 col-md-12 col-sm-12 page-content block-bordered">
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-6">
				<dsp:droplet name="/cps/droplet/RoleLookupDroplet">
					<dsp:param name="userId" bean="Profile.id"/>
					<dsp:param name="role" value="superAdmin"/>
					<dsp:oparam name="false">
						<dsp:getvalueof var="jdeAddressNumber" bean="Profile.parentOrganization.id"/>
						<dsp:getvalueof var="address" bean="Profile.parentOrganization.derivedBillingAddress"/>
					</dsp:oparam>
					<dsp:oparam name="true">
						<dsp:droplet name="/atg/dynamo/droplet/IsNull">
							<dsp:param name="value" bean="Profile.selectedOrg"/>
							<dsp:oparam name="true">
								<dsp:getvalueof var="jdeAddressNumber" bean="Profile.parentOrganization.id"/>
								<dsp:getvalueof var="address" bean="Profile.parentOrganization.derivedBillingAddress"/>
							</dsp:oparam>
							<dsp:oparam name="false">
								<dsp:getvalueof var="jdeAddressNumber" bean="Profile.selectedOrg.id"/>
								<dsp:getvalueof var="address" bean="Profile.selectedOrg.derivedBillingAddress"/>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>

				<dsp:include page="${originatingRequest.contextPath}/checkout/includes/address-view.jsp">
					<dsp:param name="type" value="billing"/>
					<dsp:param name="address" value="${address}"/>
					<dsp:param name="jdeId" value="${jdeAddressNumber}"/>
				</dsp:include>

			</div>
			<div class="col-lg-8 col-md-6 col-sm-6 col-xs-6">
				<dsp:include page="${originatingRequest.contextPath}/checkout/includes/address-view.jsp">
					<dsp:param name="type" value="shipping"/>
					<dsp:param name="address" bean="Profile.selectedCS"/>
					<dsp:param name="jdeId" bean="Profile.selectedCS.id"/>
				</dsp:include>
			</div>
		</div>
	</div>

</dsp:page>