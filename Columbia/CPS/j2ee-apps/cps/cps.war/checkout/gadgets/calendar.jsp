<dsp:page>
	<dsp:getvalueof var="build_version" bean="/cps/version/Build.version" />
	<link href="/assets/stylesheets/styles${build_version}.css" rel="stylesheet" type="text/css" />
	<link href="/assets/stylesheets/modal${build_version}.css" rel="stylesheet" type="text/css" />

	<div id="body" class="calendar">
		<%@ page import="java.util.Date" %>
		<c:set var="today" value="<%=new Date()%>"/>
		<fmt:formatDate var="formattedToday" pattern="MM/dd/yyyy" value='${today}'/>
		<c:set var="tomorrow" value="<%=new Date(new Date().getTime() + 60*60*24*1000)%>"/>
		<fmt:formatDate var="formattedTomorrow" pattern="MM/dd/yyyy" value='${tomorrow}'/>
		<h2>Choose a Date for Pickup</h2>
		<p></p>
		<dsp:form action="" method="POST" name="changeDate" id="changeDateId" >
			<input type="hidden" id="blackOuts" value="${formattedToday},${formattedTomorrow},${blackOutDateList}" />  <%-- BlackOut Dates  --%> 
			<input type="hidden" id="availableDates" value="${availDateList}" /> <%-- available Dates --%>
		</dsp:form>
		
		<div class="calendar-grid" id="multimonth">
			<ul class="calendar-pagination">
				<li class="calendar-prev"><a href="#" title="Previous Months">Previous</a></li>
				<li class="calendar-next"><a href="#" title="Next Months">Next</a></li>
			</ul>
		</div>
	</div> <!--  end of div class calendar -->
	
	<script type="text/javascript" src="${staticContentPrefix}/js/jquery${build_version}.js"></script>
	<script	type="text/javascript" charset="utf-8">
		Date.firstDayOfWeek = 0;
		Date.format = 'mm/dd/yyyy';
		$(function() {
			var now = new Date();
			$('#multimonth').datePickerMultiMonth( {
				numMonths : 1,
				inline : true,
				renderCallback : function($td, thisDate, month, year) {
					var blackOutDates = parent.getBlackOutList("modal");
					var availableDates = document.getElementById("availableDates").value;
					var calDate = $.format.date(thisDate,"MM/dd/yyyy");
					if (blackOutDates.indexOf(calDate) > -1) {
						$td.addClass('blackOut');
						$td.addClass('disabled');
					} else if (availableDates.indexOf(calDate) > -1) {
						$td.addClass('displayAlt1');
						if($.format.date(now,"MM/dd/yyyy") == calDate) {
							$td.addClass('selected');
						}
							$td.bind('click', function() {
							$("#selectedDateId").val(calDate);
							$("#selectedShipMethodId").val("Overnight");
						});
					}
				}
			}).bind('dateSelected', function(e, selectedDate, $td) {
				parent.setDate("futureDate",$.format.date(selectedDate,"MM/dd/yyyy"));
				parent.$.modal.close();
			});
		});
	</script>
	<script type="text/javascript" src="${staticContentPrefix}/js/date${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/jquery.datePicker${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/jquery.datePickerMultiMonth${build_version}.js"></script>
	<script type="text/javascript" src="${staticContentPrefix}/js/jquery.dateFormat${build_version}.js"></script>
</dsp:page>