<dsp:page>

    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>

    <dsp:getvalueof var="isHidden" param="isHidden"/>

    <dsp:droplet name="IsNull">
        <dsp:param name="value" bean="Profile.parentOrganization"/>
        <dsp:oparam name="true">
            <dsp:getvalueof var="companyName" bean="Profile.derivedBillingAddress.companyName"/>
            <dsp:getvalueof var="address1" bean="Profile.derivedBillingAddress.address1"/>
            <dsp:getvalueof var="address2" bean="Profile.derivedBillingAddress.address2"/>
            <dsp:getvalueof var="address3" bean="Profile.derivedBillingAddress.address3"/>
            <dsp:getvalueof var="city" bean="Profile.derivedBillingAddress.city"/>
            <dsp:getvalueof var="state" bean="Profile.derivedBillingAddress.state"/>
            <dsp:getvalueof var="postalCode" bean="Profile.derivedBillingAddress.postalCode"/>
            <%--<dsp:getvalueof var="phoneNumber" bean="Profile.derivedBillingAddress.phoneNumber"/>--%>
            <%--<dsp:getvalueof var="emailAddress" bean="Profile.derivedBillingAddress.emailAddress"/>--%>
            <dsp:getvalueof var="firstName" bean="Profile.derivedBillingAddress.firstName"/>
            <dsp:getvalueof var="lastName" bean="Profile.derivedBillingAddress.lastName"/>
            <dsp:getvalueof var="id" bean="Profile.derivedBillingAddress.id"/>
        </dsp:oparam>
        <dsp:oparam name="false">
            <dsp:getvalueof var="companyName" bean="Profile.parentOrganization.billingAddress.companyName"/>
            <dsp:getvalueof var="address1" bean="Profile.parentOrganization.billingAddress.address1"/>
            <dsp:getvalueof var="address2" bean="Profile.parentOrganization.billingAddress.address2"/>
            <dsp:getvalueof var="address3" bean="Profile.parentOrganization.billingAddress.address3"/>
            <dsp:getvalueof var="city" bean="Profile.parentOrganization.billingAddress.city"/>
            <dsp:getvalueof var="state" bean="Profile.parentOrganization.billingAddress.state"/>
            <dsp:getvalueof var="postalCode" bean="Profile.parentOrganization.billingAddress.postalCode"/>
            <%--<dsp:getvalueof var="phoneNumber" bean="Profile.parentOrganization.billingAddress.phoneNumber"/>--%>
            <%--<dsp:getvalueof var="emailAddress" bean="Profile.parentOrganization.billingAddress.emailAddress"/>--%>
            <dsp:getvalueof var="firstName" bean="Profile.parentOrganization.billingAddress.firstName"/>
            <dsp:getvalueof var="lastName" bean="Profile.parentOrganization.billingAddress.lastName"/>
            <dsp:getvalueof var="id" bean="Profile.parentOrganization.billingAddress.id"/>
        </dsp:oparam>
    </dsp:droplet>

    <c:if test="${not isHidden}">
        <h4>Billing address</h4>
        <p>
            <c:if test="${!empty companyName}">${companyName} <br/></c:if>
            <c:if test="${!empty firstName}">${firstName}</c:if>&nbsp;<c:if test="${!empty lastName}">${lastName}</c:if>
            <br/>${address1}&nbsp;<br/>
            <c:if test="${!empty address2}">${address2}<br/></c:if>
            ${city},&nbsp;${state}&nbsp;${postalCode}
        </p>
    </c:if>

    <input type="hidden" id="billingId" value="${id}" />

</dsp:page>