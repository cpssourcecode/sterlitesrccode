<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CouponFormHandler"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:getvalueof var="couponCodeMaps" vartype="java.lang.Object" bean="CouponFormHandler.currentCouponCodesMap"/>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:getvalueof var="formExceptions" bean="CouponFormHandler.formExceptions"/>
	<dsp:getvalueof var="displayRemoveOption" param="displayRemoveOption"/>
	<dsp:getvalueof var="displayStyle" param="displayStyle"/>

	<c:choose>
		<c:when test="${displayRemoveOption}">
			<dsp:form id="couponCodeRemove" formid="couponCodeRemove" action="${originatingRequest.contextPath}/checkout/cart.jsp">
				<c:if test="${not empty couponCodeMaps}">
					<dt class="totals-final">Item total:</dt>
					<dd class="totals-final">
						<dsp:valueof bean="ShoppingCart.current.priceInfo.rawSubtotal" converter="currencyConversion"/>
					</dd>
					<c:forEach var="couponItem" items="${couponCodeMaps}">
						<dsp:setvalue param="couponItem" value="${couponItem}"/>
						<dt class="${vsg_utils:escapeHtml(displayStyle)}">
							<dsp:valueof param="couponItem.key"/>
							<c:if test="${displayRemoveOption}">
								<dsp:input type="hidden" bean="CouponFormHandler.couponCode" paramvalue="couponItem.key"/>
								<dsp:input bean="CouponFormHandler.removeCoupon" type="submit" iclass="cart-coupon-remove"/>
							</c:if>
						</dt>
						<dd class="${vsg_utils:escapeHtml(displayStyle)} red">
							- <dsp:valueof param="couponItem.value" converter="currencyConversion"/>
						</dd>
					</c:forEach>
				</c:if>
				<%-- Additional input fields, success/error URLs. --%>
				<dsp:input bean="CouponFormHandler.removeCouponSuccessURL" type="hidden" value="${originatingRequest.contextPath}/checkout/cart.jsp"/>
				<dsp:input bean="CouponFormHandler.removeCouponErrorURL" type="hidden" value="${originatingRequest.contextPath}/checkout/cart.jsp"/>
			</dsp:form>
		</c:when>
		<c:otherwise>
			<c:if test="${not empty couponCodeMaps}">
				<dt class="totals-final">Item total:</dt>
				<dd class="totals-final">
					<dsp:valueof bean="ShoppingCart.current.priceInfo.rawSubtotal" converter="currencyConversion"/>
				</dd>
				<c:forEach var="couponItem" items="${couponCodeMaps}">
					<dsp:setvalue param="couponItem" value="${couponItem}"/>
					<dt class="${vsg_utils:escapeHtml(displayStyle)}"><dsp:valueof param="couponItem.key"/></dt>
					<dd class="${vsg_utils:escapeHtml(displayStyle)} red">- <dsp:valueof param="couponItem.value" converter="currencyConversion"/></dd>
				</c:forEach>
			</c:if>
		</c:otherwise>
	</c:choose>
</dsp:page>