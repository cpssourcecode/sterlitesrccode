<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/droplet/ContactInfoLookupDroplet"/>

	<dsp:getvalueof var="shippingInfoId" param="shippingInfoId"/>

	<a href="#" data-toggle="modal" data-target="#selectShipAddress" style="display: none;" id="link-selectShipAddress"></a>
	<div class="modal fade" id="selectShipAddress" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-page-name='modal-change-cs.jsp'>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
					<span class="modal-icon"><i class="fa fa-truck"></i></span>
				</div>
				<div class="modal-body">
					<h2 class="modal-title">Select a Ship To Address</h2>

					<form>
						<div class="form-group">
							<p><strong>For this session</strong>, confirm your default Ship To Address or choose an
								alternate Ship To Address from the list below. </p>

							<p>Your Default Ship To Address</p>

						</div>
						<div class="form-group">
							<dsp:droplet name="ContactInfoLookupDroplet">
								<dsp:param name="id" param="shippingInfoId"/>
								<dsp:setvalue paramvalue="element" param="contactInfo"/>
								<dsp:oparam name="output">
									<dsp:tomap var="contactInfo" param="contactInfo"/>
									<dsp:droplet name="IsEmpty">
										<dsp:param name="value" param="contactInfo"/>
										<dsp:oparam name="false">
											<div class="radio checked">
												<label class="radio-custom checked" data-initialize="radio" id="myCustomRadioLabel">
													<input class="sr-only" name="radioEx1" type="radio" value="option1" checked>
													<strong>${contactInfo.address1} </strong>
												</label>
											</div>
										</dsp:oparam>
									</dsp:droplet>
								</dsp:oparam>
							</dsp:droplet>

						</div>
						<hr class="huge">
						<div class="form-group">
							<p><strong>Select an Alternate Ship To Address for this Session</strong></p>
							<p><span class="glyphicon glyphicon-alert text-danger"></span> Changing your Ship to Address may affect prices and availablity of items in your cart.</p>
							<div class="row smaller">
								<div class="col-sm-6">
									<input type="text" class="form-control input-lg" id="modal-cs-input-search"
										   placeholder="Search for Ship To Address">
								</div>
								<div class="col-sm-3">
									<a href="#" class="btn btn-block btn-warning btn-lg" data-event-click-id="checkout27" >Search</a>
								</div>
								<div class="col-sm-3">
									<a href="#" class="btn btn-block btn-default btn-lg" data-event-click-id="checkout28" >Reset</a>
								</div>
							</div>
						</div>
						<div id="modal-cs-list-content">
							<dsp:include page="/checkout/gadgets/modal-cs-list.jsp"/>
						</div>

					</form>
				</div>
				<div class="modal-footer">
					<a href="#" class="btn btn-warning btn-lg pull-left" data-event-click-id="checkout29" >Confirm</a>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" nonce="${requestScope.nonce}">
		$('input').keypress(function (e) {
			if (e.which == 13) {
				doModalCS_Search();
			}
		});
	</script>
</dsp:page>