<dsp:page>

	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/droplet/CPSExtPriceDroplet"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/droplet/CPSSessionCartPriceList"/>

	<dsp:getvalueof var="source" param="source"/>

	<c:choose>
		<c:when test="${source == 'review' || source == 'complete'}">
			<dsp:getvalueof var="isCart" value="${false}" />
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="isCart" value="${true}" />
		</c:otherwise>
	</c:choose>

	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<c:choose>
			<c:when test="${source == 'complete'}">
				<dsp:param name="array" bean="ShoppingCart.last.commerceItems"/>
			</c:when>
			<c:otherwise>
				<dsp:param name="array" bean="ShoppingCart.current.commerceItems"/>
			</c:otherwise>
		</c:choose>

		<dsp:param name="elementName" value="commerceItem"/>
		<dsp:oparam name="output">

			<dsp:getvalueof var="ciId" param="commerceItem.id"/>
			<dsp:tomap var="productMap" param="commerceItem.auxiliaryData.productRef"/>
			<dsp:getvalueof var="skuId" param="commerceItem.auxiliaryData.catalogRef.id"/>
			<dsp:getvalueof var="qty" param="commerceItem.quantity"/>

			<div class="media">
				<div class="media-checkbox">
					<c:if test="${isCart}">
						<div class="checkbox">
							<label class="checkbox-custom checkbox-select-product" data-initialize="checkbox">
								<input type="checkbox" class="sr-only" id="${ciId}" name="itemsInCart"/>
								<span class="checkbox-label">
									<i class="text-muted"></i>
								</span>
							</label>
						</div>
					</c:if>
				</div>

				<%-- <dsp:droplet name="/atg/repository/seo/CatalogItemLink">
					<dsp:param name="item" param="commerceItem.auxiliaryData.productRef"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
					</dsp:oparam>
				</dsp:droplet> --%>
				<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
					<dsp:param name="prodId" value="${productMap.id}"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
					</dsp:oparam>
				</dsp:droplet> 

				<dsp:a iclass="media-left" href="${productUrl}">
					<img src="${staticContentPrefix}/content_images/pdp3.jpg" alt="...">
				</dsp:a>

				<div class="media-body">
					<h4 class="media-heading">
						<dsp:a href="${productUrl}">
							<dsp:valueof value="${productMap.displayName}" converter="valueishtml"/>
						</dsp:a>
					</h4>
					<ul class="list-inline">
						<li>
							<span class="text-muted">Item #: <dsp:valueof value="${skuId}"/></span>
						</li>
						<li>
							<span class="text-muted">Short Code: <dsp:valueof value="${skuId}"/></span>
						</li>
						<dsp:droplet name="/cps/droplet/AliasNumberLookup">
							<dsp:param name="skuId" param="sku.repositoryId"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="prodAlias" param="alias"/>
                    			<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>								<li>
									<span class="text-muted">Alias: <dsp:valueof value="${alias}"/></span>
								</li>
							</dsp:oparam>
						</dsp:droplet>
					</ul>
				</div>

				<c:choose>
					<c:when test="${isCart}">
						<dsp:getvalueof var="mediaClassQty" value="media-price-qty" />
					</c:when>
					<c:otherwise>
						<dsp:getvalueof var="mediaClassQty" value="" />
					</c:otherwise>
				</c:choose>

				<div class="media-price ${mediaClassQty}">
					<p class="single-price">
						<dsp:include page="availability-message.jsp">
							<dsp:param name="skuId" value="${skuId}"/>
							<dsp:param name="itemQty" value="${qty}"/>
							<dsp:param name="pageFrom" value="cartPage"/>
							<dsp:param name="tag" value="span"/>
							<dsp:param name="parentClass" value="visible-xs-inline label"/>
						</dsp:include>
						<dsp:droplet name="CPSExtPriceDroplet">
							<dsp:param name="priceList" bean="CPSSessionCartPriceList"/>
							<dsp:param name="sku" value="${skuId}"/>
							<dsp:oparam name="output">
								<dsp:getvalueof param="element" var="extPrice"/>
								<dsp:droplet name="Switch">
									<dsp:param bean="Profile.transient" name="value"/>
									<dsp:oparam name="false">
										<dsp:getvalueof var="prices" bean="/cps/droplet/CPSSessionPriceList.values"/>
										<dsp:valueof value="${extPrice}" converter="currencyConversion"/>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</p>
					<hr class="divider">
					<ul class="list-inline">
						<c:if test="${!isCart}">
							<li>
								<p class="text-muted">QTY:<dsp:valueof value="${qty}"/></p>
							</li>
						</c:if>
						<li class="text-right pull-right">
							<strong><dsp:valueof param="commerceItem.priceInfo.amount" converter="currencyConversion"/></strong>
						</li>
					</ul>
					<c:if test="${isCart}">

						<div class="spinbox spinbox-cart" data-initialize="spinbox" data-min="1" id="mySpinbox">
							<input id="${ciId}" name="updateqty" type="text" value="${qty}" class="form-control spinbox-input" maxlength="3"/>
							<div class="spinbox-buttons btn-group btn-group-vertical">
								<button type="button" class="btn btn-default spinbox-up">
								<i class="fa fa-angle-up"></i>
								<span class="sr-only">Increase</span>
								</button>
								<button type="button" class="btn btn-default spinbox-down">
								<i class="fa fa-angle-down"></i>
								<span class="sr-only">Decrease</span>
								</button>
							</div>
							<span class="description">QTY</span>
						</div>
						<ul class="list-inline qty-box">
							<li>
								<a href="#" data-event-click-id="checkout25" data-event-click-param0="${ciId}" >
									<i class="fa fa-refresh"></i>
									<span>Update</span>
								</a>
							</li>
							<li class="pull-right">
								<a class="remove" href="#" data-event-click-id="checkout26" data-event-click-param0="${ciId}" >
									<i class="fa fa-times"></i>
									<span>Remove</span>
								</a>
							</li>
						</ul>
					</c:if>

					<dsp:include page="availability-message.jsp">
						<dsp:param name="skuId" value="${skuId}"/>
						<dsp:param name="itemQty" value="${qty}"/>
						<dsp:param name="pageFrom" value="cartPage"/>
						<dsp:param name="tag" value="p"/>
						<dsp:param name="parentClass" value="hidden-xs prod-status label"/>
					</dsp:include>
				</div>

			</div>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>