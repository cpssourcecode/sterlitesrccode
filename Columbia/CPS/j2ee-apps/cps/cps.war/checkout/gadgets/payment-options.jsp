<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/FullShoppingCartFormHandler"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>

	<div class="col-lg-12 block-bordered">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-12">
							<h3>Payment</h3>
						</div>
					</div>
					<div class="form-group" id="paymentTypeDiv">
						<label for="" class="col-sm-2 control-label">Payment Method</label>

						<div class="col-sm-10">
							<dsp:select id="paymentType" iclass="selectpicker selectpicker-payment-registered" bean="FullShoppingCartFormHandler.values.paymentType">
								<option value="checkout-payment-cc">Credit Card</option>
								<option value="checkout-payment-po">On Account</option>
							</dsp:select>
						</div>
					</div>
					<div class="well well-sm credit-block">
						<div class="form-group" id="creditCardTypeDiv">
							<label for="" class="col-sm-2 control-label">Card Type</label>

							<div class="col-sm-10">
								<dsp:select id="creditCardType" bean="FullShoppingCartFormHandler.values.creditCardType" iclass="selectpicker">
									<dsp:tagAttribute name="data-width" value="100%"/>
									<option disabled="disabled" selected="selected">Select...</option>
									<%@ include file="/checkout/gadgets/cc-types.jspf" %>
								</dsp:select>
							</div>
						</div>
						<div class="form-group" id="firstNameDiv">
							<label for="" class="col-sm-2 control-label">Name on Card</label>
							<div class="col-sm-10">
								<div class="row">
									<div class="col-xs-6">
										<dsp:input id="firstName" type="text" iclass="form-control" bean="FullShoppingCartFormHandler.values.firstName" maxlength="40">
											<dsp:tagAttribute name="placeholder" value="First"/>
										</dsp:input>
									</div>
									<div class="col-xs-6">
										<dsp:input id="lastName" type="text" iclass="form-control" bean="FullShoppingCartFormHandler.values.lastName" maxlength="40">
											<dsp:tagAttribute name="placeholder" value="Last"/>
										</dsp:input>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group" id="creditCardNumberDiv">
							<label for="" class="col-sm-2 control-label">Card Number</label>

							<div class="col-sm-10">
								<dsp:input type="text" id="creditCardNumber" iclass="form-control" bean="FullShoppingCartFormHandler.values.creditCardNumber" maxlength="16"/>
							</div>
						</div>
						<div class="form-group" id="creditCardExpMonthDiv">
							<label for="" class="col-sm-2 control-label">Expiration Date</label>
							<div class="col-sm-6">
								<dsp:select id="creditCardExpMonth" iclass="selectpicker" bean="FullShoppingCartFormHandler.values.expirationMonth">
									<dsp:tagAttribute name="data-width" value="100%"/>
									<option selected="selected" disabled="disabled">Month...</option>
									<%@ include file="/checkout/gadgets/cc-months.jspf" %>
								</dsp:select>
							</div>
							<div class="col-sm-4">
								<dsp:select id="creditCardExpYear" bean="FullShoppingCartFormHandler.values.expirationYear" iclass="selectpicker">
									<dsp:tagAttribute name="data-width" value="100%"/>
									<option selected="selected" disabled="disabled">Year...</option>
									<%@ include file="/checkout/gadgets/cc-years.jspf" %>
								</dsp:select>
							</div>
						</div>
						<div class="form-group" id="creditCardBillingZipcodeDiv">
							<label for="" class="col-sm-2 control-label">Billing Zip Code</label>
							<div class="col-sm-4">
								<dsp:input id="creditCardBillingZipcode" iclass="form-control" type="text" bean="FullShoppingCartFormHandler.values.creditCardPostalCode" maxlength="10"/>
							</div>
						</div>
					</div>
					<div class="well well-sm onaccount-block hide" id="poReferenceNumberDiv">
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">PO Reference</label>

							<div class="col-sm-4">
								<dsp:input id="poReferenceNumber" iclass="form-control" type="text" bean="FullShoppingCartFormHandler.values.poReferenceNumber" maxlength="25"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</dsp:page>