<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/droplet/ContactInfoLookupDroplet"/>
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>

	<dsp:getvalueof var="addressId" param="addressId"/>
	<dsp:getvalueof var="withoutForm" param="withoutForm"/>

	<c:choose>
		<c:when test="${empty addressId}">
			<dsp:tomap var="shippingAddress" bean="Profile.SelectedCs"/>
			<dsp:getvalueof var="addressId" value="${shippingAddress.id}"/>
		</c:when>
		<c:otherwise>
			<dsp:droplet name="ContactInfoLookupDroplet">
				<dsp:param name="id" param="addressId"/>
				<dsp:setvalue paramvalue="element" param="contactInfo"/>
				<dsp:oparam name="output">
					<dsp:tomap var="shippingAddress" param="contactInfo"/>
				</dsp:oparam>
			</dsp:droplet>
		</c:otherwise>
	</c:choose>


	<c:if test="${not empty shippingAddress}">
		${shippingAddress.address1}<br/>
		<c:if test="${!empty shippingAddress.address2}">
			${shippingAddress.address2}<br/>
		</c:if>
		${shippingAddress.city},&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}<br/>
		<input type="hidden" id="shippingId" value="${shippingAddress.id}"/>
	</c:if>

	<c:if test="${empty withoutForm || not withoutForm}">
		<dsp:form method="post" id="cart-cs-form">
			<dsp:input type="hidden" value="${addressId}" id="cart_shipping_id" bean="ProfileDefaultShippingAddressFormHandler.selectedId"/>
			<dsp:input type="hidden" value="true" bean="ProfileDefaultShippingAddressFormHandler.setShippingAddressOnCart" priority="-10"/>
		</dsp:form>
	</c:if>


</dsp:page>
