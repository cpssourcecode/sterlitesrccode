<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/droplet/ContactInfoLookupDroplet"/>
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>
	<dsp:getvalueof var="order" param="order"/>


	<dsp:droplet name="CartAddressDroplet">
		<c:choose>
			<c:when test="${not empty order}">
				<dsp:param param="order" name="order"/>
			</c:when>
			<c:otherwise>
				<dsp:param bean="ShoppingCart.last" name="order"/>
			</c:otherwise>
		</c:choose>
		<dsp:param bean="Profile" name="profile"/>
		<dsp:param value="review" name="page"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="deliveryMethod" param="deliveryMethod"/>
			<dsp:getvalueof var="address" param="address"/>
			<dsp:getvalueof var="addressId" param="addressId"/>
			<dsp:getvalueof var="store" param="store"/>
			<dsp:getvalueof var="storeId" param="storeId"/>
		</dsp:oparam>
	</dsp:droplet>

	<%--
		<dsp:getvalueof var="order" bean="ShoppingCart.last"/>
		<dsp:tomap var="shippingGroup" bean="ShoppingCart.last.shippingGroups[0]"/>
		<dsp:getvalueof var="shippingAddress" value="${shippingGroup.shippingAddress}"/>
	--%>

	<c:choose>
		<c:when test="${not empty addressId}">
			<strong>Ship to Address</strong>
			<dsp:tomap var="shippingAddress" value="${address}"/>
			${not empty order ? '<span>':'<p>'}
			<c:if test="${not empty shippingAddress}">
				${shippingAddress.address1}<br/>
				<c:if test="${!empty shippingAddress.address2}">
					${shippingAddress.address2}<br/>
				</c:if>
				${shippingAddress.city},&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}<br/>
			</c:if>
			${not empty order ? '</span>':'</p>'}
		</c:when>
		<c:when test="${not empty storeId}">
			<strong>Pick up in store</strong>
			<dsp:tomap var="pickUpStore" value="${store}"/>
			${not empty order ? '<span>':'<p>'}
			<c:if test="${not empty pickUpStore}">
				${pickUpStore.address1}<br/>
				<c:if test="${!empty pickUpStore.address2}">
					${pickUpStore.address2}<br/>
				</c:if>
				${pickUpStore.city},&nbsp;${pickUpStore.stateAddress}&nbsp;${pickUpStore.postalCode}<br/>
			</c:if>
			${not empty order ? '</span>':'</p>'}
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>


</dsp:page>
