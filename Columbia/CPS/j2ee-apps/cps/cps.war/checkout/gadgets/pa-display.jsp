<dsp:page>

<dsp:importbean bean="/atg/commerce/ShoppingCart" />
<dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
<dsp:importbean bean="/atg/repository/RepositoryItem"/>
<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
<dsp:importbean bean="/cps/util/CPSSessionBean"/>
<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
<dsp:importbean bean="/atg/commerce/catalog/custom/CatalogLookup" />
<dsp:importbean bean="/cps/integrations/pricing/CPSDummyPricingClient"/>
<dsp:importbean bean="/cps/commerce/order/purchase/CPSPriceAvailabilityFormHandler" />
<ul class="item item-list item-cart">			
	<dsp:getvalueof value="0.00" var="subtotal"/>
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" bean="CPSSessionBean.palist" />
		<dsp:param name="elementName" value= "item"/>
		<dsp:param name="count" value="count"/>
	<dsp:oparam name="output">
		<dsp:getvalueof param="count" var="count"/>
		<dsp:getvalueof var="prices" bean="/cps/droplet/CPSSessionPriceList.values"/>
	<li class="item-item">
	
	<label class="item-select">
		<dsp:getvalueof var="paId" param="count">
		<dsp:valueof param="paId"/>
			<input type="checkbox" id="${count-1}" name="check"/>
		</dsp:getvalueof>
	</label>
	<div class="item-order">
	<dsp:getvalueof var="qty" param="item.qty"/>
	<label>Qty</label>
	<input class="form-qty" type="text" size="3" maxlength="3" name="qtys" value="${qty}" id="qtys_${count-1}"/>
	</div>
	<ul class="item-sku">
	 <li>Item #:
	<dsp:valueof  param="item.sku"/></li>
	<li>Short Code:
	<dsp:valueof  param="item.sku"/></li>
	<dsp:droplet name="/cps/droplet/AliasNumberLookup">
											<dsp:param name="skuId" param="item.sku"/>
											<dsp:oparam name="output">
											<dsp:getvalueof var="prodAlias" param="alias"/>
                    						<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
												<li>Alias: <dsp:valueof value="${alias}"/></li>
											</dsp:oparam>
										</dsp:droplet>
	<dsp:getvalueof param="item.sku" var="skuid"/>
	</ul>
	
	
	<span class="item-title">
		<dsp:droplet name="/atg/targeting/RepositoryLookup">
		<dsp:param name="id" param="item.sku"/>
		<dsp:param name="elementName" value="sku" />
		<dsp:param name="repository" bean="/atg/commerce/catalog/ProductCatalog" />
		<dsp:param name="itemDescriptor" value="sku" />
		<dsp:oparam name="output">
		<dsp:droplet name="/atg/commerce/catalog/ProductLookup">
		<dsp:param name="sku" value="id"/>
		<dsp:param name="elementName" value="product" />
		<dsp:oparam name="output">
		<dsp:valueof param="product.displayName" converter="valueishtml"/> 
		</dsp:oparam>
		</dsp:droplet>
		</dsp:oparam>
		</dsp:droplet>
	</span>	
	<span class="item-stock">
		<dsp:include page="availability-message.jsp">
			<dsp:param name="skuId" param="item.sku"/>
			<dsp:param name="itemQty" value="${qty}"/>
			<dsp:param name="pageFrom" value="paPage"/>
		</dsp:include>
	</span>
<dsp:droplet name="Switch">
		<dsp:param bean="Profile.transient" name="value"/>
			<dsp:oparam name="false">
<span class="item-price item-price-unit">$<dsp:valueof param = "item.unitprice"/>&nbsp;ea.</span>
<span class="item-price item-price-total"><dsp:valueof param = "item.amount" converter="currencyConversion"/></span>
<dsp:getvalueof param="item.amount" var="price"/>
<dsp:getvalueof var="subtotal" value="${subtotal+price}"/>	
</dsp:oparam>
</dsp:droplet>	
	<ul class="item-actions">
	<li><a href="javascript:submitWithUpdateItemAction('${count-1}')">Update</a></li>
	<li><a href="javascript:submitWithRemoveItemAction('${count-1}')"> Remove</a></li>
	
	</ul>
</li>

</dsp:oparam>
</dsp:droplet>
</ul>
<dl class="totals cart-totals">
			<dt class="totals-final">Subtotal:</dt>
			<dsp:droplet name="Switch">
			<dsp:param bean="Profile.transient" name="value"/>
				<dsp:oparam name="false">
			<dd class="totals-final"><dsp:valueof value="${subtotal}" converter="currencyConversion"/></dd>
			</dsp:oparam>
			<dsp:oparam name="true">
			<dd class="totals-final">$0.00</dd>
			</dsp:oparam>
			</dsp:droplet>
			<dd class="clear">&nbsp;</dd>
</dl>
			<dsp:form action="/checkout/pa.jsp" method="post" id="pa_reorder_details_add" formid="pa_reorder_details_add" name="pa_reorder_details_add">
				<dsp:input type="hidden" bean="CartModifierFormHandler.paitemlist" value="" id="addpaitemlist" name="paitemlist"/>
				<dsp:input type="hidden" bean="CartModifierFormHandler.paIds" value="" id="paIds" name="paIds"/>
				<dsp:input type="hidden" bean="CartModifierFormHandler.addItemToOrder" value="true"/>
			</dsp:form>
			<dsp:form name="pa_addselected_form" id="pa_addselected_form" formid="pa_addselected_form" action="/checkout/pa.jsp" method="POST">
				<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.addselected" priority="10" type="hidden" name="addselected" id="addselected" value="addselected" />
				<dsp:input type="hidden" bean="CPSPriceAvailabilityFormHandler.additems" id="additems" name="additems" value=""/>
				<dsp:input bean="CPSPriceAvailabilityFormHandler.DelegatePaAction" priority="-1" type="hidden" value="Submit" />
			</dsp:form>
			<dsp:form name="pa_removeselected_form" id="pa_removeselected_form" formid="pa_removeselected_form" action="/checkout/pa.jsp" method="POST">
				<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.removeselected" priority="10" type="hidden" name="removeselected" id="removeselected" value="removeselected" />
				<dsp:input type="hidden" bean="CPSPriceAvailabilityFormHandler.removeitems" id="removeitems" name="removeitems" value=""/>
				<dsp:input bean="CPSPriceAvailabilityFormHandler.DelegatePaAction" priority="-1" type="hidden" value="Submit" />
			</dsp:form>
			<dsp:form name="pa_remove_form" id="pa_remove_form" formid="pa_remove_form" action="/checkout/pa.jsp" method="POST">
				<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.remove" priority="10" type="hidden" name="remove" id="remove" value="removeOneItem" />
				<dsp:input type="hidden" bean="CPSPriceAvailabilityFormHandler.itemcount" id="itemcount" name="itemcount"/>
				<dsp:input bean="CPSPriceAvailabilityFormHandler.DelegatePaAction" priority="-1" type="hidden" value="Submit" />
			</dsp:form>
			<dsp:form name="pa_update_form" id="pa_update_form" formid="pa_update_form" action="/checkout/pa.jsp" method="POST">
				<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.update" priority="10" type="hidden" name="update" id="update"  value="updatepa" />
				<dsp:input type="hidden" bean="CPSPriceAvailabilityFormHandler.itemcount" id="updatecount" name="updatecount"/>
				<dsp:input type="hidden" bean="CPSPriceAvailabilityFormHandler.updatedqty" id="updatedqty" name="updatedqty" value=""/>
				<dsp:input bean="CPSPriceAvailabilityFormHandler.DelegatePaAction" priority="-1" type="hidden" value="Submit" />
			</dsp:form>
</dsp:page>