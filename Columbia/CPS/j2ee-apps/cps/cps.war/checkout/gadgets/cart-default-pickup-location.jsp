<dsp:page>

	<dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>

	<dsp:droplet name="CartAddressDroplet">
		<dsp:param bean="ShoppingCart.current" name="order"/>
		<dsp:param bean="Profile" name="profile"/>
		<dsp:param value="pickup" name="page"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="store" param="store"/>
			<dsp:tomap var="pickUpStore" param="store"/>
			<dsp:getvalueof var="storeId" param="storeId"/>
		</dsp:oparam>
	</dsp:droplet>

	<%--
			store:${store}<br>
			storeId:${store.repositoryId}<br>
			pickUpStore:${pickUpStore}<br>
	--%>
	<c:if test="${not empty pickUpStore}">
		${pickUpStore.address1}&nbsp;<c:if
			test="${!empty pickUpStore.address2}">${pickUpStore.address2}</c:if><br/>
		<c:if test="${!empty pickUpStore.address3}">${pickUpStore.address3}<br/></c:if>
		${pickUpStore.city},&nbsp;${pickUpStore.stateAddress}&nbsp;${pickUpStore.postalCode}<br/>
		<input type="hidden" id="pickUpId" value="${storeId}"/>
	</c:if>

	<dsp:form method="post" id="cart-pickup-form">
		<dsp:input type="hidden" value="${storeId}" id="cart_store_id"
				   bean="ProfileDefaultShippingAddressFormHandler.selectedId"/>
		<dsp:input type="hidden" value="true"
				   bean="ProfileDefaultShippingAddressFormHandler.setPickupLocationOnCart"/>
	</dsp:form>


</dsp:page>
