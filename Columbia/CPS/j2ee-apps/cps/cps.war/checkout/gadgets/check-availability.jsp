<dsp:page>
    <dsp:getvalueof var="skuId" param="s"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="eCommerceDisplay" param="e"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>

    <c:if test="${not isTransient}">
        <dsp:tomap var="profile" bean="Profile.dataSource"/>
        <dsp:droplet name="/atg/dynamo/droplet/IsNull">
            <dsp:param name="value" value="${profile.selectedOrg}"/>
            <dsp:oparam name="true">
                <dsp:getvalueof var="customerId" bean="Profile.parentOrganization.id"/>
            </dsp:oparam>
            <dsp:oparam name="false">
                <dsp:getvalueof var="customerId" bean="Profile.selectedOrg.id"/>
            </dsp:oparam>
        </dsp:droplet>
        <dsp:getvalueof var="quantityAvailable" param="quantityAvailable"/>
    </c:if>

    <dsp:droplet name="/cps/droplet/CPSInventoryDroplet">
        <dsp:param name="productId" value="${skuId}"/>
        <dsp:param name="customerId" value="${customerId}"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="quantityAvailable" param="quantityAvailable"/>
            <dsp:getvalueof var="leadTime" param="leadTime"/>
            <dsp:getvalueof var="defaultBranchQuantity" param="defaultBranchQuantity" />
            <dsp:getvalueof var="productDisabled" value="${false}"/>
            <dsp:getvalueof var="stockingType" param="productMap.stockingType"/>
            <c:if test="${stockingType == 'O' || stockingType == 'U' || stockingType == 'K' || stockingType == 'X'}">
                <dsp:getvalueof var="productDisabled" value="${true}"/>
            </c:if>
            <c:choose>
                <c:when test="${not eCommerceDisplay}">
                    <span class="text-danger">No Longer Offered</span>
                </c:when>
                <c:when test="${productDisabled}">
                    <span class="text-danger">
                        Unavailable
                    </span>
                </c:when>
                <c:otherwise>
                     <c:choose>
	                     <c:when test="${quantityAvailable > 0}">
	                     <%-- <span>${quantityAvailable}</span> --%>
	                     <span>Now : ${defaultBranchQuantity} Unit(s)<br/>
	                     Within Days : ${quantityAvailable - defaultBranchQuantity} Unit(s)</span>
	                     </c:when>
	                     <c:otherwise>
	                     	<span>Now : ${defaultBranchQuantity} Unit(s)<br/>
	                     	Within Days : ${quantityAvailable - defaultBranchQuantity} Unit(s)</span>
	                       <%-- <dsp:valueof value="${leadTime}"/> business days --%>
	                     </c:otherwise>
	                 </c:choose>
                </c:otherwise>
            </c:choose>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>