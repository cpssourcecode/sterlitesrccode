<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/cps/droplet/ContactInfoLookupDroplet"/>
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>
	<dsp:getvalueof var="addressId" param="addressId"/>

	<dsp:droplet name="CartAddressDroplet">
		<dsp:param bean="ShoppingCart.current" name="order"/>
		<dsp:param bean="Profile" name="profile"/>
		<dsp:param value="review" name="page"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="deliveryMethod" param="deliveryMethod"/>
			<dsp:getvalueof var="address" param="address"/>
			<dsp:getvalueof var="addressId" param="addressId"/>
			<dsp:getvalueof var="store" param="store"/>
			<dsp:getvalueof var="storeId" param="storeId"/>
		</dsp:oparam>
	</dsp:droplet>

	<c:choose>
		<c:when test="${not empty addressId}">
			<h4>Ship To Address</h4>
			<dsp:tomap var="shippingAddress" value="${address}"/>
			<c:if test="${not empty shippingAddress}"><p>
				${shippingAddress.address1}<br/>
				<c:if test="${!empty shippingAddress.address2}">${shippingAddress.address2}<br/></c:if>
				${shippingAddress.city},&nbsp;${shippingAddress.state}&nbsp;${shippingAddress.postalCode}<br/><br/></p>
			</c:if>
		</c:when>
		<c:when test="${not empty storeId}">
			<h4>Pick up in store</h4>
			<dsp:tomap var="pickUpStore" value="${store}"/>
			<c:if test="${not empty pickUpStore}"><p>
				${pickUpStore.address1}<br/>
				<c:if test="${!empty pickUpStore.address2}">${pickUpStore.address2}<br/></c:if>
				${pickUpStore.city},&nbsp;${pickUpStore.stateAddress}&nbsp;${pickUpStore.postalCode}<br/><br/></p>
			</c:if>
		</c:when>
		<c:otherwise>
		</c:otherwise>
	</c:choose>

</dsp:page>
