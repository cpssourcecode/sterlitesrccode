<dsp:page>

    <dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

    <dsp:getvalueof var="step" param="step"/>

    <ol class="breadcrumb">
        <c:choose>
            <c:when test="${step eq 'cart'}">
                <li class="active">Shopping Cart</li>
            </c:when>
            <c:when test="${step eq 'review'}">
                <li><a href="${originatingRequest.contextPath}/checkout/cart.jsp">Shopping Cart</a></li>
                <li class="active">Order Review</li>
            </c:when>
            <c:when test="${step eq 'confirmation'}">
                <li class="active">Order Confirmation</li>
            </c:when>
            <c:otherwise>
            </c:otherwise>
        </c:choose>
    </ol>

</dsp:page>
