<dsp:page>
	<dsp:include page="/global/modals/modal-cs-cart.jsp"/>
	<dsp:include page="/global/modals/modal-pick-up-loc.jsp"/>
    <dsp:getvalueof var="deliveryMethod" param="deliveryMethod"/>
    <dsp:getvalueof var="addressId" param="addressId"/>

    <dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>

    <input type="hidden" id="deliveryMethodHidden" value="${deliveryMethod}"/>
    <input type="hidden" id="deliveryMethodAddressIdHidden" value="${addressId}"/>
	
    <div class="row">
        <div class="col-sm-4">
            <div class="radio">
                <label class="${deliveryMethod=='shipped' ? 'checked':''} ${deliveryMethod=='shipped' ? 'active':''}">
                    <input class="radio-toggle-delivery-method" ${deliveryMethod=='shipped' ? 'checked="checked"':''}
                           name="radioEx1"
                           type="radio"
                           value="delivery-method-shipped"
                           data-event-click-id="checkout21">
                    Have it Shipped
                </label>
            </div>
            <div class="radio">
                <label class=" ${deliveryMethod=='pick-up' ? 'checked':''} ${deliveryMethod=='pick-up' ? 'active':''}">
                    <input class="radio-toggle-delivery-method" ${deliveryMethod=='pick-up' ? 'checked="checked"':''}
                           name="radioEx1"
                           type="radio"
                           value="delivery-method-pick-up"
                           data-event-click-id="checkout22">
                    Pick it Up
                </label>
            </div>
        </div>

        <c:choose>
            <c:when test="${not isTransient}">
                <div class="col-sm-4 delivery-method-shipped-form"
                     style="display: ${deliveryMethod=='shipped' ? 'block':'none'};">
                    <h6>Ship To Address:</h6>
                    <p>
                        <dsp:include page="/checkout/gadgets/cart-ship-to-address.jsp">
                            <dsp:param name="addressId" value="${addressId}"/>
                            <%--<dsp:param name="address" value="${address}"/>--%>
                        </dsp:include>
                    </p>
                    <p><cp:repositoryMessage key="haveItShipNote"></cp:repositoryMessage></p>

                </div>
                <div class="col-sm-4 delivery-method-shipped-form"
                     style="display: ${deliveryMethod=='shipped' ? 'block':'none'};">
                    <p class="text-center-xs">
           					<a href="#" class="btn btn-info" data-toggle="modal" data-dismiss="modal" data-target="#selectShipAddress">
                            Edit Ship To<span class="hidden-sm"> Address</span></a>
                    </p>
                    <p><strong class="text-upper">Note: </strong> Changing your Ship To Address may affect pricing and
                        availability of items in your cart.</p>
                </div>
            </c:when>
            <c:otherwise>
                <dsp:form method="post" id="cart-cs-form">
                    <dsp:input type="hidden" value="${addressId}" id="cart_shipping_id"
                               bean="ProfileDefaultShippingAddressFormHandler.selectedId"/>
                    <dsp:input type="hidden" value="true"
                               bean="ProfileDefaultShippingAddressFormHandler.setShippingAddressOnCart" priority="-10"/>
                </dsp:form>
            </c:otherwise>
        </c:choose>

        <div class="col-sm-4 delivery-method-pick-up-form"
             style="display: ${deliveryMethod=='pick-up' ? 'block':'none'};">
            <h6>Pickup Location:</h6>
            <p class="pick-up-location">
                <dsp:include page="/checkout/gadgets/cart-default-pickup-location.jsp">
                </dsp:include>
            </p>
            <p><cp:repositoryMessage key="pickItUpNote"></cp:repositoryMessage></p>
        </div>
        <div class="col-sm-4 delivery-method-pick-up-form"
             style="display: ${deliveryMethod=='pick-up' ? 'block':'none'};">
            <p class="text-center-xs">
                <a href="#" class="btn btn-info" data-toggle="modal" data-dismiss="modal" data-target="#changePickupLocation">
                    Change
                    Pick Up<span class="hidden-sm"> Location</span></a>
            </p>
            <p><strong class="text-upper">Note: </strong>Changing your pick up location will not affect pricing, but may
                affect availability</p>
        </div>
    </div>

</dsp:page>