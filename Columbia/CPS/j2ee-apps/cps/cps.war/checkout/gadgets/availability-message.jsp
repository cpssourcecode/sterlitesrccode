<dsp:page>
	<dsp:importbean bean="/cps/droplet/CPSInventoryLookupDroplet"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:getvalueof var="skuId" param="skuId"/>
	<dsp:getvalueof var="itemQty" param="itemQty"/>
	<dsp:getvalueof var="pageFrom" param="pageFrom"/>
	<dsp:getvalueof var="tag" param="tag"/>
	<dsp:getvalueof var="parentClass" param="parentClass"/>

	<dsp:droplet name="CPSInventoryLookupDroplet">
		<dsp:param name="itemId" value="${skuId}"/>
		<dsp:param name="order" bean="ShoppingCart.current"/>
		<dsp:param name="paList" bean="CPSSessionBean.palist"/>
		<dsp:param name="pageFrom" value="${pageFrom}"/>

		<dsp:oparam name="output">
			<dsp:getvalueof var="stockLevel" param="stockLevel"/>
			<dsp:getvalueof var="availabilityStatus" param="availabilityStatus"/>

			<%--<dsp:valueof param="availabilityMessage"/>--%>
			<%--<c:choose>--%>
				<%--<c:when test="${stockLevel <= 0}">--%>
					<%--Ships from Manufacturer--%>
				<%--</c:when>--%>
				<%--<c:otherwise>--%>
					<%--<c:choose>--%>
						<%--<c:when test="${stockLevel > 0 && stockLevel >= itemQty}">--%>
							<%--Available for Delivery--%>
						<%--</c:when>--%>
						<%--<c:when test="${stockLevel > 0 && stockLevel < itemQty}">--%>
							<%--Partially Available--%>
						<%--</c:when>--%>
					<%--</c:choose>--%>
				<%--</c:otherwise>--%>
			<%--</c:choose>--%>

			<dsp:getvalueof var="availabilityMessage" param="availabilityMessage"/>
			<dsp:getvalueof var="labelClass" value="label-success"/>
			<c:choose>
				<c:when test="${availabilityMessage eq 'Ships from Manufacturer'}">
					<dsp:getvalueof var="labelClass" value="label-warning"/>
					<%--label-danger ?--%>
				</c:when>
				<c:when test="${availabilityMessage eq 'Partially Available'}">
					<dsp:getvalueof var="labelClass" value="label-warning"/>
				</c:when>
				<c:when test="${availabilityMessage eq 'Available for Delivery'}">
					<dsp:getvalueof var="labelClass" value="label-success"/>
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>

			<c:choose>
				<c:when test="${tag eq 'p'}">
					<p class="${vsg_utils:escapeHtml(parentClass)} ${labelClass}">
						<dsp:valueof param="availabilityMessage"/>
					</p>
				</c:when>
				<c:otherwise>
					<span class="${vsg_utils:escapeHtml(parentClass)} ${labelClass}">
						<dsp:valueof param="availabilityMessage"/>
					</span>
				</c:otherwise>
			</c:choose>

		</dsp:oparam>
	</dsp:droplet>

</dsp:page>
