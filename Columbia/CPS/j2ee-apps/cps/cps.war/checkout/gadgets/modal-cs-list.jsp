<dsp:page>
	<dsp:droplet name="/cps/droplet/CSSelectionDroplet">
		<dsp:param name="filter" param="filter"/>
		<dsp:oparam name="output">
			<div class="form-group">
				<div class="scrollable">
					<div class="head">
						<span>Select</span>
						<span>Ship To</span>
					</div>
					<div class="content scrollbar-inner">
						<dsp:droplet name="/atg/dynamo/droplet/ForEach">
							<dsp:param name="array" param="associatedCS"/>
							<dsp:param name="elementName" value="cs"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="address1" param="cs.address1"/>
								<dsp:getvalueof var="csId" param="cs.id"/>

								<div class="radio">
									<label class="radio-custom" data-initialize="radio" name="addressSelectRadio"
											value="${csId}" data-event-click-id="checkout30" >
											${address1}
									</label>
								</div>
							</dsp:oparam>
						</dsp:droplet>

					</div>
				</div>
			</div>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>