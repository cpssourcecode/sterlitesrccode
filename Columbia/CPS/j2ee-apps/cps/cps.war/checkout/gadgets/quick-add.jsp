<dsp:page>

    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest" scope="request"/>
    <dsp:form id="quick-add-form" formid="quick-add-form" method="POST" action="${originatingRequest.contextPath}/">
        <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListErrorURL"
                   value="/checkout/cart.jsp"/>
        <dsp:input type="hidden" bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListSuccessURL"
                   value="/checkout/cart.jsp?qo=t" id="quick-add-form-s-url"/>
        <dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" name="nextpageval" type="hidden"
                   value="/checkout/cart.jsp?qo=t" id="quick-add-form-n-url"/>
        <dsp:input type="hidden" id="invalidQuickPagelink"
                   bean="CartModifierFormHandler.quickOrderAddItemsToOrderFromListInvalidURL"
                   value="/checkout/cart.jsp?invalidskumodal=true"/>
        <dsp:input id="items-map-quick-add" type="hidden" bean="CartModifierFormHandler.itemsInfo" converter="map"
                   value=""/>
        <dsp:input bean="CartModifierFormHandler.quickAddItemToOrder" priority="-10" type="hidden" value="Submit"/>
    </dsp:form>

    <section class="quick-add">
        <div class="text-center">
            <label class="mr10 control-label">Quick Add</label>
            <div class="form-group-inline w35">
                <input type="text" class="form-control q-add-item-input quick-order-item quick-add-control"
                       placeholder="Item # or My Part #">
            </div>
            <div class="form-group-inline w20">
                <input type="text" class="form-control q-add-qty-input quick-add-control qty-input"
                       placeholder="Qty">
            </div>
            <button class="btn btn-primary xs-block btn-invoices-search quick-add-control addToCartButton"
               data-event-click-id="checkout31" >Add to Cart</button>
        </div>
        <c:if test="${not param['quickodropdown']}">
            <dsp:include page="/account/gadgets/account-error-message.jsp">
                <dsp:param name="formHandler" bean="CartModifierFormHandler"/>
            </dsp:include>
        </c:if>
    </section>

</dsp:page>