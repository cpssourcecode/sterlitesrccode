<dsp:page>

	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

	<dsp:getvalueof var="selectedStep" param="selectedStep" />

	<div class="breadcrumbs-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<c:choose>
						<c:when test="${selectedStep eq 'cart'}">
							<ol class="breadcrumb">
								<li>
									<a href="${originatingRequest.contextPath}/index.jsp">
										<span>Home</span>
									</a>
								</li>
								<li class="active">Shopping Cart</li>
							</ol>
						</c:when>
						<c:when test="${selectedStep eq 'login'}">
							<ol class="breadcrumb breadcrumb-wizard">
								<li>
									<a href="${originatingRequest.contextPath}/checkout/cart.jsp">
										<span>Shopping Cart</span>
									</a>
								</li>
								<li class="active">Log In</li>
								<li class="disabled">
									Checkout
								</li>
								<li class="disabled">
									Review Order
								</li>
								<li class="disabled">
									Order Complete
								</li>
							</ol>
						</c:when>
						<c:when test="${selectedStep eq 'registered'}">
							<ol class="breadcrumb breadcrumb-wizard">
								<li>
									<a href="${originatingRequest.contextPath}/checkout/cart.jsp">
										<span>Shopping Cart</span>
									</a>
								</li>
								<li class="active">
									Checkout
								</li>
								<li class="disabled">
									Review Order
								</li>
								<li class="disabled">
									Order Complete
								</li>
							</ol>
						</c:when>
						<c:when test="${selectedStep eq 'review'}">
							<ol class="breadcrumb breadcrumb-wizard">
								<li>
									<a href="${originatingRequest.contextPath}/checkout/cart.jsp">
										<span>Shopping Cart</span>
									</a>
								</li>
								<li class="disabled">
									<a href="${originatingRequest.contextPath}/checkout/registered.jsp">
										<span>Checkout</span>
									</a>
								</li>
								<li class="active">
									Review Order
								</li>
								<li class="disabled">
									Order Complete
								</li>
							</ol>
						</c:when>
						<c:when test="${selectedStep eq 'complete'}">
							<ol class="breadcrumb breadcrumb-wizard">
								<li class="disabled">
									Shopping Cart
								</li>
								<li class="disabled">
									Checkout
								</li>
								<li class="disabled">
									Review Order
								</li>
								<li class="active">
									Order Complete
								</li>
							</ol>
						</c:when>
						<c:otherwise>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>

</dsp:page>
