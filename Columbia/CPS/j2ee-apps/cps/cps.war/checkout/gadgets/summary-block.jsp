<dsp:page>

	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>

	<dsp:getvalueof var="order" param="order"/>
	<dsp:getvalueof var="poNumber" param="order.invoiceRequestPoNumber"/>

	<dsp:getvalueof var="source" param="source"/>
	
	<dsp:droplet name="/cps/droplet/CPSPricingServiceAvailableDroplet">
        <dsp:oparam name="output">
            <dsp:getvalueof var="isAvailable" param="available"/>
        </dsp:oparam>
    </dsp:droplet>
	
	<div class="panel panel-default">
		
		<div class="panel-heading">Summary</div>
		<div class="panel-body">			
			<p>
				# Items: ${vsg_utils:escapeHtml(fn:length(order.commerceItems))}<br>
			 	<c:choose>
					<c:when test="${isAvailable}">
						<strong>Subtotal: <dsp:valueof value="${order.priceInfo.amount}" converter="currencyConversion"/></strong>
					</c:when>
					<c:otherwise>
						<span>
							<i class="glyphicon glyphicon-warning-sign"></i>
							<fmt:message key="checkout.review.summary.subtotal.unavailable"/>
						</span>
					</c:otherwise>
				</c:choose>
			</p>
			<p>
				<!-- <strong class="text-upper">Note:</strong> Information on shipping, tax, and other charges is not available 
				until you complete your order. Your Order Acknowledgement will reflect final pricing. -->
				<dsp:include page="/includes/gadgets/info-message.jsp">
					<dsp:param name="key" value="info_cart_note_acknowledgment"/>
					<dsp:param name="notWrap" value="true"/>
				</dsp:include>
				<a href="#" data-dismiss="modal" data-toggle="modal" data-target="#infoShipTaxOther">Read More</a>
			</p>
		</div>
	</div>

	<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-info-ship-tax.jsp"/>
</dsp:page>