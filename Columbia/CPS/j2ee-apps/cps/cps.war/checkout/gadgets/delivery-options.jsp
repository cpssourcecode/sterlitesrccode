<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/commerce/order/FullShoppingCartFormHandler"/>
	<dsp:importbean bean="/cps/droplet/StoreLookupDroplet"/>

	<div class="col-lg-12 block-bordered">
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<div class="form-horizontal">
					<div class="form-group">
						<div class="col-sm-12">
							<h3>Delivery Options</h3>
						</div>
					</div>
					<div class="form-group" id="deliveryMethodDiv">
						<label for="" class="col-sm-2 control-label">Shipping Method</label>

						<div class="col-sm-10">
							<dsp:select id="deliveryMethod" iclass="selectpicker selectpicker-delivery" bean="FullShoppingCartFormHandler.values.deliveryMethod">
								<dsp:tagAttribute name="data-width" value="100%"/>
								<option value="checkout-delivery-ground">Standard Ground</option>
								<option value="checkout-delivery-pickup">Pickup (Will Call)</option>
								<option value="checkout-delivery-collect">Collect</option>
							</dsp:select>
						</div>

					</div>

					<div class="form-group show-pickup-only hide" id="branchDiv">
						<label for="" class="col-sm-2 control-label">Branch</label>
						<div class="col-sm-10">
							<dsp:select id="branch" bean="FullShoppingCartFormHandler.values.branchId" iclass="selectpicker">
								<dsp:tagAttribute name="data-width" value="100%"/>
								<option selected="selected" disabled="disabled" value="0">Select...</option>
								<dsp:droplet name="StoreLookupDroplet">
									<dsp:oparam name="output">
										<dsp:droplet name="ForEach">
											<dsp:setvalue param="address" paramvalue="element"/>
											<dsp:param name="array" param="element"/>
											<dsp:oparam name="output">
												<dsp:getvalueof var="currentArea" param="address.area"/>
												<c:if test="${currentArea ne lastArea}">
													<dsp:getvalueof var="lastArea" param="address.area"/>
													<option disabled="disabled">
														${currentArea}
													</option>
												</c:if>
												<dsp:getvalueof var="addressId" param="address.id"/>
												<option value="${addressId}">
													&nbsp;&nbsp;&nbsp;&nbsp;
													<dsp:valueof param="address.address"/>&nbsp;
													<dsp:valueof param="address.city"/>,&nbsp;
													<dsp:valueof param="address.state"/>&nbsp;
													<dsp:valueof param="address.zip"/>&nbsp;
												</option>
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:select>
						</div>
					</div>

					<div class="form-group show-pickup-only hide" id="Div">
						<label for="" class="col-sm-2 control-label">Date</label>

						<%@ page import="java.util.Date" %>
						<c:set var="today" value="<%=new Date()%>"/>
						<fmt:formatDate var="formattedToday" pattern="MM/dd/yyyy" value='${today}'/>
						<c:set var="tomorrow" value="<%=new Date(new Date().getTime() + 60*60*24*1000)%>"/>
						<fmt:formatDate var="formattedTomorrow" pattern="MM/dd/yyyy" value='${tomorrow}'/>
						<div class="col-sm-10">
							<label class="radio-inline">
								<input type="radio" id="pickup-date1" name="pickup-date" checked="true" value='${formattedToday}'/>
								Today
							</label>
							<label class="radio-inline">
								<input type="radio" id="pickup-date2" name="pickup-date" value='${formattedTomorrow}'/>
								Tomorrow
							</label>
							<label class="radio-inline">
								<%--<a class="modal-calendar" id="pickup-chooser" href="/checkout/gadgets/calendar.jsp?modal=true">--%>
								<a class="modal-calendar" id="pickup-chooser">
									<input type="radio" id="pickup-date3" name="pickup-date"/>
									Other Date...
								</a>
							</label>
<%--
							<label class="radio-inline">
								<a class="modal-calendar" id="pickup-chooser" href="/checkout/gadgets/calendar.jsp?modal=true">
									<input type="radio" id="pickup-date3" name="pickup-date"/>
									Other Date...
								</a>
							</label>
--%>

							<dsp:input id="pickupDate" readonly="true" iclass="form-s" bean="FullShoppingCartFormHandler.values.pickupDate"/>
						</div>
					</div>

					<div class="form-group show-pickup-only hide">
						<label for="" class="col-sm-2 control-label">Time:</label>
						<div class="col-sm-6">
							<dsp:input id="pickupTime" type="hidden" bean="FullShoppingCartFormHandler.values.pickupTime"/>
							<div id="timesToLoad">
								<select id="pickUpTime" class="selectpicker" data-width="100%"></select>
							</div>
						</div>
						<div class="col-sm-4">
							<span id="timezone"></span>
							<%--&nbsp;&nbsp;--%>
							<%--<label class="message-red" id="customMessage"></label>--%>
						</div>
					</div>

					<div id="customMessage" class="form-group show-pickup-only hide has-error" style="display:none;">
						<label for="" class="col-sm-11 control-label">Branch is closed for today.  Please choose a different branch or date.</label>
					</div>

					<div class="form-group show-collect-only hide" id="carrierDiv">
						<label for="" class="col-sm-2 control-label">Carrier:</label>
						<div class="col-sm-10">
							<dsp:select id="carrier" iclass="selectpicker" bean="FullShoppingCartFormHandler.values.carrier">
								<dsp:tagAttribute name="data-width" value="100%"/>
								<option selected="selected" disabled="disabled">Select...</option>
								<option value="UPS">UPS</option>
								<option value="FedEx">FedEx</option>
							</dsp:select>
						</div>
					</div>

					<div class="form-group show-collect-only hide" id="accountNumberDiv">
						<label for="" class="col-sm-2 control-label">Account#:</label>
						<div class="col-sm-10">
							<dsp:input id="accountNumber" iclass="form-control" type="text" bean="FullShoppingCartFormHandler.values.carrierAcctNumber" maxlength="20"/>
						</div>
					</div>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-2 control-label">Shipping Instructions</label>

						<div class="col-sm-10">
							<div class="checkbox">
								<label class="checkbox-custom checkbox-toggle-prodict" data-initialize="checkbox">
									<dsp:input type="checkbox" bean="FullShoppingCartFormHandler.values.mtrRequested" iclass="sr-only" value="false"/>
									<span class="checkbox-label">
										Please provide applicable Material Test Reports (MTRs) with this order.
									</span>
								</label>
							</div>
							<dsp:textarea bean="FullShoppingCartFormHandler.values.specialInstruction" maxlength="512" iclass="form-control" cols="30" rows="5"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</dsp:page>