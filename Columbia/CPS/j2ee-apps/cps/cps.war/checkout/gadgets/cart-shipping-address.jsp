<dsp:page>

    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>

    <dsp:droplet name="/atg/dynamo/droplet/IsNull">
        <dsp:param name="value" bean="Profile.selectedCS"/>
        <dsp:oparam name="true">
            <dsp:getvalueof var="shippingAddress1" bean="Profile.derivedShippingAddress.address1"/>
            <dsp:getvalueof var="shippingAddress2" bean="Profile.derivedShippingAddress.address2"/>
            <dsp:getvalueof var="shippingAddress3" bean="Profile.derivedShippingAddress.address3"/>
            <dsp:getvalueof var="shippingCity" bean="Profile.derivedShippingAddress.city"/>
            <dsp:getvalueof var="shippingState" bean="Profile.derivedShippingAddress.state"/>
            <dsp:getvalueof var="shippingPostalCode" bean="Profile.derivedShippingAddress.postalCode"/>
        </dsp:oparam>
        <dsp:oparam name="false">
            <dsp:getvalueof var="shippingAddress1" bean="Profile.selectedCS.address1"/>
            <dsp:getvalueof var="shippingAddress2" bean="Profile.selectedCS.address2"/>
            <dsp:getvalueof var="shippingAddress3" bean="Profile.selectedCS.address3"/>
            <dsp:getvalueof var="shippingCity" bean="Profile.selectedCS.city"/>
            <dsp:getvalueof var="shippingState" bean="Profile.selectedCS.state"/>
            <dsp:getvalueof var="shippingPostalCode" bean="Profile.selectedCS.postalCode"/>
        </dsp:oparam>
    </dsp:droplet>

    <p><strong>Ship To Address:</strong></p>
    <p>
        ${shippingAddress1}&nbsp;<c:if test="${!empty shippingAddress2}">${shippingAddress2}</c:if><br/>
        <c:if test="${!empty shippingAddress3}">${shippingAddress3}<br/></c:if>
        ${shippingCity},&nbsp;${shippingState}&nbsp;${shippingPostalCode}<br/>
    </p>

</dsp:page>