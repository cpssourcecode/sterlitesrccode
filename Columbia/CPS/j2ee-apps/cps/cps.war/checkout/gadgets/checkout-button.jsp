<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:getvalueof var="formId" param="formId" />
	<dsp:form name="${formId}" id="${formId}" formid="${formId}" action="${originatingRequest.contextPath}/checkout/registered.jsp" method="POST">
		<dsp:input type="hidden" bean="CartModifierFormHandler.handleCheckoutErrorURL" value="${originatingRequest.contextPath}/checkout/cart.jsp"/>
		<dsp:input type="hidden" bean="CartModifierFormHandler.handleCheckoutSuccessURL" value="${originatingRequest.contextPath}/checkout/registered.jsp"/>
		<dsp:input type="submit" iclass="btn btn-success btn-lg btn-block" bean="CartModifierFormHandler.checkInventoryForCheckout" value="Checkout"/>
	</dsp:form>
</dsp:page>