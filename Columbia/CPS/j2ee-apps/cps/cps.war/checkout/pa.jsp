<dsp:page>
  <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
  <dsp:importbean bean="/atg/commerce/catalog/custom/CatalogLookup" />
  <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
  <dsp:importbean bean="/atg/userprofiling/Profile"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/CPSPriceAvailability"/>
  <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
  <dsp:importbean bean="/atg/dynamo/droplet/Switch" />
  <dsp:importbean bean="/cps/droplet/CPSPriceDroplet"/>
  <dsp:importbean bean="/cps/util/CPSSessionBean"/> 
  <dsp:importbean bean="/cps/droplet/CPSSessionPriceList"/>
  <dsp:importbean bean="/cps/droplet/PAaddPriceDroplet"/>
  <dsp:importbean bean="/cps/commerce/order/purchase/CPSPriceAvailabilityFormHandler" />
  

  <cp:pageContainer page="pa">
 <dsp:getvalueof bean = "CPSSessionBean.palist" var="pricelist"/>
 <c:if test="${not empty pricelist}">
  <dsp:droplet name="Switch">
		<dsp:param bean="Profile.transient" name="value"/>
			<dsp:oparam name="false">
				<dsp:droplet name="CPSPriceDroplet">
					<dsp:param bean="CPSSessionBean.palist" name="priceAvailabilityList"/>
				</dsp:droplet>
				<dsp:droplet name="PAaddPriceDroplet">
					<dsp:param bean="CPSSessionBean" name="sessionBean"/>
					<dsp:param bean="CPSSessionBean.palist" name="priceAvailabilityPrices"/>
					<dsp:param bean="CPSSessionPriceList.values" name="priceAvailabilityPriceValues"/>
					<dsp:param bean="Profile.selectedCS" name="selectedCS"/>
				</dsp:droplet>
			</dsp:oparam>
	</dsp:droplet>
	</c:if>
	<div id="breadcrumb">
		<div class="center">
		<p><a href="#">Home</a> &raquo; <a href="#">Price & Availability</a></p>
		</div>
		</div>
<div id="body" class="checkout">
		<div class="center">
	<div id="content" class="full cart">
						<dsp:form name="pa_form" id="pa_form" formid="pa_form" action="/checkout/pa.jsp" method="POST">
						<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.action" priority="10" type="hidden" id="action" name="action" value="" />
						<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.removeOneItem" priority="10" type="hidden" id="removeItem" name="removeItem" value="" />
						<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.removeMultipleItems" priority="10" type="hidden" id="removeMultipleItems" name="removeMultipleItems" value="" />
						<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.updateItems" priority="10" type="hidden" id="updateItems" name="updateItems" value="" />
						<dsp:include page="/checkout/gadgets/cart-error-message.jsp">
								<dsp:param name="formHandler" bean="CPSPriceAvailabilityFormHandler"/>
						</dsp:include>
						<dsp:include page="/checkout/gadgets/cart-error-message.jsp">
							<dsp:param name="formHandler" bean="CartModifierFormHandler"/>
						</dsp:include>
	<h1>Price &amp; Availability</h1>
		<div class="listings">
			<ul class="bar">
				<li class="bar-selected">
				<label>Selected items</label>
						<input data-event-click-id="checkout11"  type="button" value="Add to Cart" class="button" id="Add to cart" />
		<dsp:droplet name="Switch">
			<dsp:param bean="Profile.transient" name="value"/>
					<dsp:oparam name="false">
						<input type="button" class="button modal-list-pa" value="Add to List" />
					</dsp:oparam>
					<dsp:oparam name="true">
					<input data-event-click-id="checkout12"  type="button" value="Add to List" class="button" id="Add to List" />
					</dsp:oparam>
		</dsp:droplet>
					<input data-event-click-id="checkout13"  type="button" value="Remove" class="button" id="Remove" />
				</li>
				<li class="cart-add">
						<label>Quick Check</label>
							<dsp:input bean="CPSPriceAvailabilityFormHandler.qty" maxlength="3" type="text" iclass="form-qty" id="pa-qty" value="Qty" />	
							<dsp:input bean="CPSPriceAvailabilityFormHandler.sku"  maxlength="25" type="text" iclass="form-m" id="pa-ref" value="Item # or Alias"/>
							<input data-event-click-id="checkout14"  type="submit" value="Add" class="button button-action" id="addToCart" />
				</li>
			</ul>
		</div>
		<div class="header">
				<label class="header-check">
				<input type="checkbox" data-event-click-id="checkout15"  />
				Toggle All</label>
		</div>
			<dsp:input bean="CPSPriceAvailabilityFormHandler.DelegatePaAction" priority="-1" type="hidden" value="Submit" />
		</dsp:form>			
			<dsp:include page="gadgets/pa-display.jsp"/>
			 <div class="divider">-</div>
		<div class="form-submit">
			<p><a href="javascript: history.go(-1)">Continue Browsing</a></p>
			<input data-event-click-id="checkout16"  type="button" class="button button-big" value="Add All to Cart" />
		</div>
	</div>
		 <div class="clear">&nbsp;</div>
	 </div>
</div>
			<dsp:form name="pa_removeselected_form" id="pa_removeselected_form" formid="pa_removeselected_form" action="/checkout/pa.jsp" method="POST">
			<dsp:input bean="CPSPriceAvailabilityFormHandler.priceActionMap.removeselected" priority="10" type="hidden" name="removeselected" id="removeselected" value="removeselected" />
			<dsp:input type="hidden" bean="CPSPriceAvailabilityFormHandler.removeitems" id="removeitems" name="removeitems" value=""/>
			<dsp:input bean="CPSPriceAvailabilityFormHandler.DelegatePaAction" priority="-1" type="hidden" value="Submit" />
			</dsp:form>
			
			<script type="text/javascript" nonce="${requestScope.nonce}">
			
			$(document).ready(function() {
				$("#add-error").hide();
				$("#pa-ref").click(function(){
					if($(this).val() == 'Item # or Alias'){
						$(this).val('');
					}
				}).blur(function(){
					if($(this).val() == ''){
						$(this).val('Item # or Alias');
					} else {
						$(this).val($(this).val().toLowerCase());
					}
				})
				$("#pa-qty").click(function(){
					if($(this).val() == 'Qty'){
						$(this).val('');
					}
				}).blur(function(){
					if($(this).val() == ''){
						$(this).val('Qty');
					}
				})
			});
			</script>
  </cp:pageContainer>
</dsp:page>


