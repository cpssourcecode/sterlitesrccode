<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>

	<dsp:droplet name="Switch">
		<dsp:param bean="Profile.transient" name="value"/>
		<dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/index.jsp"/>
			</dsp:droplet>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:getvalueof var="showCaptcha" param="captcha"/>

	<cp:pageContainer page="checkout">

		<dsp:include page="${originatingRequest.contextPath}/checkout/gadgets/breadcrumb.jsp">
			<dsp:param name="selectedStep" value="cart"/>
		</dsp:include>

		<div class="container">
			<div class="row">
				<div class="col-lg-12 block-bordered">
					<h2>Log In</h2>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 page-content">

					<div class="row">
						<div class="col-lg-12 block-bordered">
							<dsp:include page="/checkout/gadgets/cart-error-message.jsp">
								<dsp:param name="formHandler" bean="ProfileFormHandler"/>
							</dsp:include>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<dsp:form action="${originatingRequest.contextPath}/" method="post" id="login-form" iclass="form-horizontal" role="form">

								<dsp:droplet name="/cps/droplet/RememberMeDroplet">
									<dsp:oparam name="output">
										<dsp:getvalueof var="email" param="email"/>
										<dsp:getvalueof var="remember" param="isRemember"/>

										<div class="form-group">
											<div class="col-sm-12">
												<h3>I Have an Account</h3>
											</div>
										</div>
										<div class="form-group">
											<label for="login" class="col-sm-2 control-label">Email</label>
											<div class="col-sm-10">
												<dsp:input value="${email}" type="email" bean="ProfileFormHandler.value.login" id="login" maxlength="100" iclass="form-control input-lg"/>
											</div>
										</div>
										<div class="form-group">
											<label for="password" class="col-sm-2 control-label">Password</label>

											<div class="col-sm-10">
												<dsp:input type="password" autocomplete="new-password" value="" bean="ProfileFormHandler.value.password" id="password" maxlength="15" iclass="form-control input-lg"/>
											</div>
										</div>

										<c:if test="${showCaptcha}">
											<div class="form-group">
												<div class="col-sm-7 col-sm-offset-2">
													<div class="row">
														<div class="col-sm-5 col-xs-6">
															<dsp:input name="captcha" type="text" id="captcha" maxlength="20" value="" bean="ProfileFormHandler.value.captcha" iclass="form-control input-lg"/>
														</div>
														<div class="col-sm-7">
															<img id="captchaimg" src="/account/captcha/simpleCaptcha.png"/>
														</div>
													</div>
													<label for="captcha" class="col-sm-12 control-label">
														<small class="text-muted">Write symbols from image</small>
														<p>Can't read captcha? <a href="javascript:void(0)" data-event-click-id="checkout10" >
															Try a new one</a></p>
													</label>
												</div>
											</div>
										</c:if>

										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-10">
												<div class="checkbox">
													<label class="checkbox-custom" data-initialize="checkbox">
														<c:choose>
															<c:when test="${remember eq true}">
																<dsp:input type="checkbox" bean="ProfileFormHandler.value.rememberMe" id="rememberMe" checked="true" iclass="sr-only"/>
															</c:when>
															<c:otherwise>
																<dsp:input type="checkbox" bean="ProfileFormHandler.value.rememberMe" id="rememberMe" checked="false" iclass="sr-only"/>
															</c:otherwise>
														</c:choose>
														<span class="checkbox-label">Remember Me</span>
													</label>
												</div>
											</div>
										</div>

										<hr>

										<div class="form-group">
											<div class="col-sm-6 col-xs-6">
												<a href="${originatingRequest.contextPath}/global/modals/modal-password.jsp" class="btn btn-lg btn-link">
													<span>Forgot Password</span>
												</a>
											</div>
											<div class="col-sm-6 col-xs-6">
												<!-- <button type="submit" class="btn btn-success btn-lg pull-right">Log In</button> -->
												<dsp:input type="hidden" bean="ProfileFormHandler.value.redirect" value="cart"/>
												<dsp:input type="hidden" bean="ProfileFormHandler.loginSuccessURL" value="${originatingRequest.contextPath}/checkout/registered.jsp"/>
												<dsp:input type="hidden" bean="ProfileFormHandler.loginErrorURL" value="${originatingRequest.contextPath}/"/>
												<dsp:input type="submit" bean="ProfileFormHandler.login" iclass="btn btn-success btn-lg pull-right validate" value="Log In"/>

												<%--<a href="#" class="btn btn-success btn-lg pull-right validate">Log In</a>--%>
											</div>
										</div>

									</dsp:oparam>
								</dsp:droplet>

							</dsp:form>
						</div>

						<div class="col-lg-6 col-md-6 col-sm-6">
							<h3>I Don't Have An Account</h3>
							<article>
								<h4>
									<a class="link-underline" href="${originatingRequest.contextPath}/account/register.jsp?c=true">
										<span>Register Now</span>
									</a>
								</h4>

								<p>Create a Columbia Account to luctus mi et enim lobortis malesuada. Aliquam porta
									sagittis semper.</p>
							</article>
							<article>
								<h4>
									<a class="link-underline" href="${originatingRequest.contextPath}/checkout/guest.jsp">
										<span>Continue As a Guest</span>
									</a>
								</h4>

								<p>Lorem ipsum dolor sit amet?</p>
							</article>
						</div>
					</div>
				</div>
			</div>
		</div>

	</cp:pageContainer>

</dsp:page>
