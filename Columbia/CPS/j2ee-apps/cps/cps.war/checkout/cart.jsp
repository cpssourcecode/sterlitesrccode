<dsp:page>

    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/util/CPSSessionBean"/>
    <dsp:importbean bean="/cps/droplet/CartAddressDroplet"/>
    <dsp:importbean bean="/cps/droplet/RoleLookupDroplet" />
    <dsp:getvalueof var="invalidItemList" bean="CPSSessionBean.invalidItemList"/>
    <dsp:getvalueof var="isAvailablePricesWebService" bean="CPSSessionBean.availablePricesWebService" />
	<dsp:importbean bean="/cps/util/MaterialListDownloadHandler"/>

    <dsp:getvalueof var="qo" param="qo"/>
    <dsp:getvalueof var="productId" param="productId"/> <!-- for qo productId -->

    <dsp:getvalueof var="commerceItemCount" bean="ShoppingCart.current.commerceItemCount"/>
    <dsp:getvalueof var="isEmptyCart" value="${commerceItemCount == 0}"/>

    <dsp:getvalueof var="isTransient" bean="Profile.transient" />

    <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
    <c:if test="${not empty selectedOrg}">
        <dsp:getvalueof var="csrClass" value="admin-access"/>
    </c:if>

    <dsp:droplet name="/cps/droplet/RoleLookupDroplet">
        <dsp:param name="userId" bean="Profile.id"/>
        <dsp:param name="role" value="superAdmin"/>
        <dsp:oparam name="true">
            <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
            <c:if test="${not empty selectedOrg}">
                <dsp:getvalueof var="isAdmin" value="${true}"/>
            </c:if>
        </dsp:oparam>
    </dsp:droplet>
    

    
    <div id="reprice-order">
		<dsp:include page="/checkout/gadgets/reprice-order.jsp"/>
	</div>

        <cp:pageContainer page="checkout">
            <main id="body">
                <div class="container">
                    <dsp:include page="/checkout/gadgets/checkout-breadcrumb.jsp">
                        <dsp:param name="step" value="cart"/>
                    </dsp:include>

                    <dsp:droplet name="CartAddressDroplet">
                        <dsp:param bean="ShoppingCart.current" name="order"/>
                        <dsp:param bean="Profile" name="profile"/>
                        <dsp:param value="cart" name="page"/>
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="deliveryMethod" param="deliveryMethod"/>
                            <dsp:getvalueof var="address" param="address"/>
                            <dsp:getvalueof var="addressId" param="addressId"/>
                            <dsp:getvalueof var="store" param="store"/>
                            <dsp:getvalueof var="storeId" param="storeId"/>
                        </dsp:oparam>
                    </dsp:droplet>

                    <h1 class="mb20">Shopping Cart</h1>

                    <div class="panel panel-default">
                        <div class="panel-heading">Delivery Method</div>
                        <div class="panel-body" id="panel-body-delivery-method">
							<dsp:getvalueof var="selectedCS" bean="Profile.selectedCS.id"/>
                            <dsp:include page="/checkout/gadgets/cart-delivery-method.jsp">
                                <dsp:param name="deliveryMethod" value="${deliveryMethod}"/>
                                <dsp:param name="addressId" value="${addressId}"/>
                            </dsp:include>
                        </div>
                    </div>

                    <dsp:include page="/checkout/gadgets/cart-billing-address.jsp">
                        <dsp:param name="isHidden" value="${true}"/>
                    </dsp:include>

                    <div id="modal-on-hold-show-div"></div>

                    <dsp:include page="/checkout/gadgets/quick-add.jsp"/>

                    <div class="important-message panel-group hidden" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" href="#importantMsg"
                                       aria-expanded="true" aria-controls="importantMsg">
                                        <i class="fa fa-exclamation-triangle"></i>
                                        Important Messages About Item in Your Cart.
                                    </a>
                                </h4>
                            </div>
                            <div id="importantMsg" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="importantMsg">
                                <div class="panel-body">
                                    <strong>Columbia Pipe no longer offers these items:</strong>
                                    <ul>
                                    </ul>
                                    <a href="#">Show More</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <c:if test="${not isTransient}">
                        <div class="alert alert-icon" id="availabilityImportantInfo">
                            <i class="fa fa-exclamation-triangle red"></i>
                            <strong class="red text-uppercase">Important:</strong> <strong>When checking
                            availability,</strong> quantity added to your cart can EXCEED quantity shown in the Availablity
                            column below. Exceeded quantities will be placed on backorder. You can contact your Sales Team
                            for information about backordered items.
                        </div>
                    </c:if>

                    <section>
					<dsp:form action="/checkout/cart.jsp" method="post" id="exportCart" formid="exportCart">
						<dsp:input type="hidden" bean="MaterialListDownloadHandler.quoteAcknowldge" value="true" />
						<dsp:input type="hidden" bean="MaterialListDownloadHandler.cIds" id="commerceItemToPrint" />
						<dsp:input type="hidden" bean="MaterialListDownloadHandler.createSpreadsheet" value="true" priority="-10"/>
					</dsp:form>
                       <div class="text-center">
                           <c:if test="${not isTransient}">
                               <button class="btn-cart-toggle btn btn-info check-avail-submit xs-block"
                                  data-event-click-id="checkout0" >Refresh Availability</button>
                           </c:if>
                           <button  class="btn-cart-toggle btn btn-info xs-block" data-event-click-id="eventExportCart" id="quoteSelected">Quote Selected</button>
                           <c:choose>
                               <c:when test="${isTransient}">
                                   <button  class="btn-cart-toggle btn btn-info xs-block" data-toggle="modal" data-target="#logIn" >Add Selected to Material
                                       List</button>
                               </c:when>
                               <c:otherwise>
                                   <button  class="btn-cart-toggle btn btn-info xs-block"
                                      data-event-click-id="checkout1" >Add Selected to Material
                                       List</button>
                               </c:otherwise>
                           </c:choose>

                           <button  class="btn-cart-toggle btn btn-info xs-block"
                              data-event-click-id="checkout2" >Remove Selected</button>

                           <button id="cart-proceed-to-checkout-top"
                              class="btn-cart-toggle btn btn-primary btn-proceed-checkout xs-block" data-is-transient="${isTransient}"
                              data-event-click-id="checkout8" ><span
                                   class="hidden-sm">Proceed to </span>Checkout</button>
                       </div>
                   </section>
                    <input type="hidden" id="cart-sort-option" value=""/>
                    <input type="hidden" id="cart-sort-option-type" value="asc"/>

                    <section style="display: none" id="empty-cart-message">
                        <div class="well text-center">
                            <h4>Your cart is empty</h4>
                        </div>
                    </section>

					<div class="container preload-cover">
						<div class="loading preload-spinner custom-loading">
							<i class="fa fa-spinner fa-spin"></i>
						</div>
					</div>
                    <dsp:form id="update-schedule-days" formid="update-schedule-days" method="POST">
                    
					<div class="col-md-12 alert-danger alert-no-available" id="products_obsolete_msg" style="display: none;">
						<div class="alert-icon">
							<i class="fa fa-exclamation-triangle red" style="position: static"></i>
							<strong style="color: #333333;">
								<dsp:include page="/includes/gadgets/info-message.jsp">
									<dsp:param name="key" value="errProductsObsoleteNoLongerAvailable"/>
									<dsp:param name="notWrap" value="true"/>
								</dsp:include>
								<!-- One or more items in your cart is now obsolete and no longer available for purchase. Obsolete items will be removed from cart at checkout. -->
							</strong>
						</div>
					</div>
					
                    <table class="table table-striped table-mobile table-check cart-items-list" style="display: none;">
                        <thead class="header">
                        <tr>
                            <th class="text-center">Select All</th>
                            <th class="col check checkbox-select-all">
                                <label data-initialize="checkbox"><input type="checkbox"><span
                                        class="visible-xs-inline ml5">All</span></label>
                            </th>
                            <th class="col desc">
                                <a href="#" data-event-click-id="checkout4" >
                                    Description <i class="fa fa-sort" id="sort-option-description"></i>
                                </a>
                            </th>
                            <th class="col item-number">
                                <a href="#" data-event-click-id="checkout5" >
                                    Item # <i class="fa fa-sort" id="sort-option-item-number"></i>
                                </a>
                            </th>
                            <c:if test="${not isTransient}">
                                <th class="col alias hidden-xs">
                                    <a href="#" data-event-click-id="checkout6" >
                                        My Part #  <i class="fa fa-sort" id="sort-option-alias-number"></i>
                                    </a>
                                </th>
                                <th class="col text-center">
                                    Auto-Reorder
                                </th>
                            </c:if>
                            <th class="col qty text-center">
                                Qty
                            </th>
                            <th class="col u-price text-center">
                                <a href="#" data-event-click-id="checkout7" >Unit Price <i class="fa fa-sort" id="sort-option-price"></i></a>
                            </th>
                            <th class="col t-price text-center">
                                Total Price
                            </th>
                            <th class="col availability text-center">
                                Availability
                            </th>
                        </tr>
                        </thead>

                        <tbody class="content-list table-content content empty-cart">
                        </tbody>
                    </table>
                        <dsp:input id="items-map-schedule" type="hidden"
                                   bean="CartModifierFormHandler.itemsInfo" converter="map" value=""/>
                        <dsp:input bean="CartModifierFormHandler.updateScheduleDays" priority="-10"
                                   type="hidden" value="Submit"/>
                    </dsp:form>
                    <c:if test="${!isTransient && !isEmptyCart}">
						<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
							<dsp:param name="messageKey" value="reorderDisclaimerMessage" />
							<dsp:oparam name="output">
								<dsp:getvalueof var="reorderDisclaimerMessage" param="message" />
								<p class="reorder-disclaimer">${reorderDisclaimerMessage},<a href="/termsOfSale">Terms of Sale</a>.</p>
							</dsp:oparam>
						</dsp:droplet>
					</c:if>
                    <section>
                        <div class="subtotal text-right">
                            <h4 class="text-nocase">Subtotal:
                                <span class="red summary-subtotal">
                                    <dsp:getvalueof var="total" bean="ShoppingCart.current.priceInfo.amount"/>
                                    <dsp:valueof value="${total}" converter="currencyConversion"/>
                                </span>
                            </h4>
                            <button id="cart-proceed-to-checkout-bottom" data-event-click-id="checkout8" data-is-transient="${isTransient}" class="btn-proceed-checkout btn-cart-toggle btn btn-primary xs-block">Proceed to Checkout</button>
                        </div>
                    </section>

                    <!-- Recently Viewed section -->

                    <dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
                        <dsp:param name="contentCollection" value="/content/Shared/Recently Viewed"/>
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
                            <c:forEach var="renderContent" items="${contentItem.contents}">
                                <dsp:renderContentItem contentItem="${renderContent}"/>
                            </c:forEach>
                        </dsp:oparam>
                    </dsp:droplet>

                </div>

                <div id="modal-add-to-material-list-div"></div>
				<script nonce="${requestScope.nonce}">
			        $(document).ready(function () {
			            initCartPage(${isAdmin});
			
			            var isQuickOrder = $("#isQuickOrder").val();
			            if (isQuickOrder == 't' && !isModalAddToCartShow) {
                            isModalAddToCartShow = true;
			                var productId = '${productId}';
                            if(productId != null && productId != ''  ) {
                                addedToCartWithRelatedHangerShow();
                            } else {
                                addedToCartHangerShow();
                            }
			            }
			            
			            $(".btn-proceed-checkout").attr("disabled", true);
			        });
			    </script>
            <%-- </main>
        </cp:pageContainer> --%>

	        <dsp:form id="update-qty-and-reprice" formid="update-qty-and-reprice" method="POST">
	            <dsp:input id="items-map-reprice" type="hidden" bean="CartModifierFormHandler.itemsInfo" converter="map"
	                       value=""/>
	            <dsp:input bean="CartModifierFormHandler.updateWithRepriceReview" priority="-10" type="hidden"
	                       value="Submit"/>
	        </dsp:form>
	
	        <dsp:form id="check-availability-form" formid="check-availability-form" method="POST">
	            <dsp:input id="items-map-availability" type="hidden" bean="CartModifierFormHandler.itemsInfo"
	                       converter="map" value=""/>
	            <dsp:input bean="CartModifierFormHandler.resetCheckAvailability" value="false" type="hidden" id="resetCheckAvailability"/>
	            <dsp:input bean="CartModifierFormHandler.checkAvailability" priority="-10" type="hidden" value="Submit"/>
	        </dsp:form>
	
	        <dsp:form id="remove-items-form" formid="remove-items-form" method="POST">
	            <dsp:input id="items-map-remove" type="hidden" bean="CartModifierFormHandler.itemsInfo" converter="map"
	                       value=""/>
	            <dsp:input bean="CartModifierFormHandler.itemsRemoveWithReprice" priority="-10" type="hidden"
	                       value="Submit"/>
	        </dsp:form>
	
	        <dsp:form id="move-to-purchase-form" formid="move-to-purchase-form" method="POST">
	            <dsp:input id="purchase-shipping-info" type="hidden" bean="CartModifierFormHandler.shippingAddressId"
	                       value=""/>
	            <dsp:input id="purchase-billing-info" type="hidden" bean="CartModifierFormHandler.billingAddressId"
	                       value=""/>
	            <dsp:input id="purchase-delivery-method" type="hidden" bean="CartModifierFormHandler.deliveryMethod"
	                       value=""/>
	            <dsp:input id="purchase-is-ch-qty" type="hidden" bean="CartModifierFormHandler.checkForChangedQuantity"
	                       value="false"/>
	            <dsp:input bean="CartModifierFormHandler.moveToPurchaseInfo" priority="-10" type="hidden" value="Submit"/>
	        </dsp:form>
	
	        <div class="modal fade" id="alertCartPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-dialog modal-narrow" role="dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
							<span class="modal-icon"><i class="fa fa-info"></i></span>
	                    </div>
	                    <div class="modal-body">
	                        <h4 class="modal-title">
	                            <dsp:include page="/includes/gadgets/info-message.jsp">
	                                <dsp:param name="key" value="err_item_not_found"/>
	                                <dsp:param name="notWrap" value="true"/>
	                            </dsp:include>
	                        </h4>
	
							<button class="btn btn-info" data-dismiss="modal"><fmt:message key="modal.cart.button.okay"/></button>
	                    </div>
	                </div>
	            </div>
	        </div>
	
	        <dsp:include page="/global/modals/modal-err-qty.jsp"/>
	        <dsp:include page="/global/modals/modal-err-not-selecte.jsp"/>
	        <dsp:include page="/global/modals/modal-rem-sel-from-cart.jsp"/>
	        <dsp:include page="/global/modals/gadgets/modal-err-not-selecte-to-print.jsp"/>
	
	        <div class="modal fade" id="confirmCartPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	             aria-hidden="true">
	            <div class="modal-dialog modal-narrow">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	                        <span class="modal-icon"><i class="fa fa-exclamation-triangle"></i></span>
	                    </div>
	                    <div class="modal-body">
	                        <h4 class="modal-title"></h4>
	                        <a href="javascript:void(0);" class="btn btn-primary"><fmt:message
	                                key="modal.cart.button.yes"/></a>
	                        <a href="#" class="btn btn-info" data-dismiss="modal"><fmt:message
	                                key="modal.cart.button.no"/></a>
	                    </div>
	                </div>
	            </div>
	        </div>
	
	        <div id="modal-cs-cart-show-div"></div>
	
	        <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
	            <dsp:param name="messageId" value="err_no_select"/>
	            <dsp:param name="isReset" value="false"/>
	            <dsp:param name="divId" value="_no_select"/>
	        </dsp:include>
	
	        <dsp:include page="${originatingRequest.contextPath}/global/modals/modal-no-list-found.jsp">
	            <dsp:param name="messageId" value="err_po"/>
	            <dsp:param name="isReset" value="false"/>
	            <dsp:param name="divId" value="_po"/>
	        </dsp:include>
	
	        <div aria-hidden="true">
	            <input id="isQuickOrder" type="hidden" value="${qo}">
                <input id="isPricingUnavailable" type="hidden" value="${isAvailablePricesWebService}">
	        </div>
        
		</main>
    </cp:pageContainer>
</dsp:page>
