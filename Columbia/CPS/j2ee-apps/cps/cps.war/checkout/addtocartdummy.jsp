<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/catalog/custom/CatalogLookup" />
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/OriginatingRequest" var="orgRequest" />

	<table border=1>
		<tr>
			<td colspan="2" align="center">
				<dsp:droplet name="ErrorMessageForEach">
					<dsp:param name="exceptions" bean="CartModifierFormHandler.formExceptions"/>
					
					<dsp:oparam name="outputStart">
						<div class="message message-red">
					</dsp:oparam>
					
					<dsp:oparam name="output">
						<dsp:valueof param="message" valueishtml="true"/>
					</dsp:oparam>
					
					<dsp:oparam name="outputEnd">
						</div>
					</dsp:oparam>
					
				</dsp:droplet>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<dsp:droplet name="CatalogLookup">
					<dsp:param name="id" value="homeStoreCatalog" />
					<dsp:param name="elementName" value="catalog" />
					<dsp:oparam name="output">
						<dsp:setvalue bean="Profile.catalog" paramvalue="catalog" />
						<dsp:valueof param="catalog" />
					</dsp:oparam>
				</dsp:droplet>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<dsp:valueof bean="/atg/multisite/Site.id">No SiteId</dsp:valueof>
			</td>
		</tr>
		<tr>
			<td>
				Product 1
				<dsp:droplet name="ProductLookup">
					<dsp:param name="id" value="prod10001" />
					<dsp:setvalue param="product" paramvalue="element"/>
					<dsp:oparam name="output">
						<ul>
							<li><dsp:valueof param="product.displayName"/></li>
							<li><dsp:valueof param="product.id"/></li>
							
							<dsp:droplet name="ForEach">
								<dsp:param name="array" param="product.childSKUs" />
								<dsp:oparam name="empty">
									No Sku's Available
								</dsp:oparam>
								<dsp:oparam name="output">
									<li>
										<dsp:valueof param="element.id"/>
											<dsp:form name="addToCart10001" formid="addToCart10001" action="${orgRequest.requestURI}" method="post">
											<dsp:input bean="CartModifierFormHandler.sessionExpirationURL" type="hidden" value="${orgRequest.contextPath}/checkout/addtocartdummy.jsp"/>
											<dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" name="nextpageval" type="hidden" value="${orgRequest.contextPath}/checkout/cart.jsp?qo=t"/>
											<dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden" value="${orgRequest.contextPath}/checkout/addtocartdummy.jsp"/>
											<dsp:input bean="CartModifierFormHandler.productId" paramvalue="product.id" type="hidden"/>
											<dsp:input bean="CartModifierFormHandler.catalogRefIds" paramvalue="element.id" type="hidden"/>
											<dsp:input bean="CartModifierFormHandler.quantity" size="4" type="hidden" value="1"/>
											<dsp:input type="submit" value="Add to Cart" bean="CartModifierFormHandler.addItemToOrder"/>
										</dsp:form>
									</li>
								</dsp:oparam>
							</dsp:droplet>
							
						</ul>
						
					</dsp:oparam>
					<dsp:oparam name="empty">
						No Product Found
						<dsp:valueof param="element.error"/>
					</dsp:oparam> 
				</dsp:droplet>
			</td>
			<td>
				Product 2
				<dsp:droplet name="ProductLookup">
					<dsp:param name="id" value="prod10002" />
					<dsp:setvalue param="product" paramvalue="element"/>
					<dsp:oparam name="output">
						<ul>
							<li><dsp:valueof param="product.displayName"/></li>
							<li><dsp:valueof param="product.id"/></li>
							
							<dsp:droplet name="ForEach">
								<dsp:param name="array" param="product.childSKUs" />
								<dsp:oparam name="empty">
									No Sku's Available
								</dsp:oparam>
								<dsp:oparam name="output">
									<li>
										<dsp:valueof param="element.id"/>
											<dsp:form name="addToCart10002" formid="addToCart10002" action="${orgRequest.requestURI}" method="post">
											<dsp:input bean="CartModifierFormHandler.sessionExpirationURL" type="hidden" value="${orgRequest.contextPath}/checkout/addtocartdummy.jsp"/>
											<dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" name="nextpageval" type="hidden" value="${orgRequest.contextPath}/checkout/cart.jsp?qo=t"/>
											<dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden" value="${orgRequest.contextPath}/checkout/addtocartdummy.jsp"/>
											<dsp:input bean="CartModifierFormHandler.productId" paramvalue="product.id" type="hidden"/>
											<dsp:input bean="CartModifierFormHandler.catalogRefIds" paramvalue="element.id" type="hidden"/>
											<dsp:input bean="CartModifierFormHandler.quantity" size="4" type="hidden" value="1"/>
											<dsp:input type="submit" value="Add to Cart" bean="CartModifierFormHandler.addItemToOrder"/>
										</dsp:form>
									</li>
								</dsp:oparam>
							</dsp:droplet>
							
						</ul>
						
					</dsp:oparam>
					<dsp:oparam name="empty">
						No Product Found
						<dsp:valueof param="element.error"/>
					</dsp:oparam> 
				</dsp:droplet>
			</td>
		</tr>
	</table>
</dsp:page>