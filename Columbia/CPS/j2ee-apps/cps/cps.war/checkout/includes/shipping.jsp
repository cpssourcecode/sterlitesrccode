<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>

	<dsp:getvalueof var="showEditLink" param="showEditLink"/>

	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
		<dsp:tomap var="shippingGroup" param="order.shippingGroups[0]"/>
		<dsp:getvalueof var="shippingMethod" value="${shippingGroup.shippingMethod}"/>
		<c:choose>
			<c:when test="${shippingMethod eq 'Pickup'}">
				<dsp:include page="${originatingRequest.contextPath}/checkout/includes/address-view.jsp">
					<dsp:param name="type" value="ispu"/>
					<dsp:param name="address" param="order.shippingGroups[0].shippingAddress"/>
					<dsp:param name="showEditLink" value="${showEditLink}"/>
					<dsp:param name="complete" value="${true}"/>
				</dsp:include>
			</c:when>
			<c:otherwise>
				<dsp:include page="${originatingRequest.contextPath}/checkout/includes/address-view.jsp">
					<dsp:param name="type" value="shipping"/>
					<dsp:param name="address" param="order.shippingGroups[0].shippingAddress"/>
					<dsp:param name="showEditLink" value="${showEditLink}"/>
					<dsp:param name="complete" value="${true}"/>
				</dsp:include>
			</c:otherwise>
		</c:choose>
		<p>
			<c:choose>
				<c:when test="${shippingMethod eq 'Ground Delivery'}">
					<strong>
						Shipping via UPS Ground
					</strong>
				</c:when>
				<c:when test="${shippingMethod eq 'Pickup'}">
					Date: <dsp:valueof param="order.shippingGroups[0].pickupDate" date="MM/dd/yyyy"/>
					<br>
					Time: <dsp:valueof param="order.shippingGroups[0].pickupTime" />
				</c:when>
				<c:when test="${shippingMethod eq 'Collect Delivery'}">
					<strong>
						Shipped Collect with <dsp:valueof param="order.shippingGroups[0].carrier"/><br>
						Account #:
					</strong>
					<dsp:valueof param="order.shippingGroups[0].carrierAccountNumber"/>
				</c:when>
				<c:otherwise>s
					<strong>
						Unknown Delivery Method
					</strong>
				</c:otherwise>
			</c:choose>
		</p>
	</div>

</dsp:page>