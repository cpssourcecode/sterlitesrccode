<dsp:page>
    <dsp:importbean bean="/cps/droplet/CPSCartCommerceItemDroplet"/>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
    <dsp:importbean bean="/cps/droplet/CPSInventoryDroplet"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:getvalueof var="leadTimeThreshold" bean="CPSInventoryDroplet.leadTimeThreshold"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>
    <dsp:getvalueof var="source" param="source"/>
    <dsp:getvalueof var="itemId" param="itemId"/>

    <dsp:getvalueof var="isTransient" bean="Profile.transient" />

    <dsp:getvalueof var="isCart" value="${source eq 'cart'}"/>
    <dsp:getvalueof var="isReview" value="${source eq 'review'}"/>
	<dsp:droplet name="/cps/droplet/CartAddressDroplet">
		<dsp:param bean="/atg/commerce/ShoppingCart.current" name="order" />
		<dsp:param bean="Profile" name="profile" />
		<dsp:param value="cart" name="page" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="deliveryMethod" param="deliveryMethod" />
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="/cps/droplet/CPSCartCommerceItemDroplet">
        <c:choose>
            <c:when test="${source == 'complete'}">
                <dsp:param name="order" bean="ShoppingCart.last"/>
            </c:when>
            <c:otherwise>
                <dsp:param name="order" bean="ShoppingCart.current"/>
            </c:otherwise>
        </c:choose>
        <dsp:param name="itemId" param="itemId"/>
        <dsp:oparam name="output">

            <dsp:tomap var="commerceItem" param="commerceItem"/>
            <dsp:getvalueof var="ciId" param="commerceItem.id"/>
            <dsp:tomap var="productMap" param="commerceItem.auxiliaryData.productRef"/>
            <dsp:getvalueof var="skuId" param="commerceItem.auxiliaryData.catalogRef.id"/>
            <dsp:getvalueof var="qty" param="commerceItem.quantity"/>
            <dsp:getvalueof var="isAvailabilityChecked" value="${commerceItem.isAvailabilityChecked}"/>
            <dsp:getvalueof var="isScheduledItem" value="${commerceItem.isScheduledItem}"/>
            <dsp:getvalueof var="scheduleDays" value="${commerceItem.scheduleDays}"/>
			<dsp:getvalueof var="stockingType1" value="${productMap.stockingType}"/>
			<dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType1)}"/>
            <dsp:getvalueof var="parentCategory" param="commerceItem.auxiliaryData.productRef.parentCategory"/>
            <c:if test="${parentCategory == null}">
                <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                    <dsp:param name="array" param="commerceItem.auxiliaryData.productRef.parentCategories" />
                    <dsp:param name="elementName" value="category"/>
                    <dsp:oparam name="output">
                        <dsp:getvalueof var="index" param="index"/>
                        <c:if test="${index eq 0}">
                            <dsp:getvalueof var="eCommerceDisplay" param="category.eCommerceDisplay"/>
                        </c:if>
                    </dsp:oparam>
                </dsp:droplet>
            </c:if>
            <c:if test="${parentCategory != null}">
                <dsp:getvalueof var="eCommerceDisplay" param="commerceItem.auxiliaryData.productRef.parentCategory.eCommerceDisplay"/>
            </c:if>

            <dsp:getvalueof var="stockingType" value="${productMap.stockingType}"/>
            <dsp:getvalueof var="productPurchasable" value="${productMap.productPurchasable}"/>
            <dsp:getvalueof var="unitPrice" param="commerceItem.priceInfo.listPrice"/>
            <dsp:getvalueof var="imageUrl" vartype="java.lang.String"
                            value="${empty productMap.thumbnail_url ? '/assets/images/plp-placeholder.png' : productMap.thumbnail_url}"/>
            <%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String" value="${originatingRequest.contextPath}/catalog/pdp.jsp?prodId=${skuId}"/> --%>
            <%--<dsp:getvalueof var="productDisabled" value="${stockingType == 'O' || stockingType == 'U' || stockingType == 'K' || not eCommerceDisplay || unitPrice == 0}"/>--%>
            <dsp:getvalueof var="productDisabled"
                            value="${stockingType == 'O' || stockingType == 'U' || stockingType == 'K' || stockingType == 'X' || not eCommerceDisplay}"/>
            <dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
                <dsp:param name="prodId" value="${productMap.id}"/>
                <dsp:oparam name="output">
                    <dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
                    <dsp:getvalueof var="prodId" param="prodId"/>
                </dsp:oparam>
            </dsp:droplet>
            <%--
                        productDisabled:${productDisabled}<br>
                        stockingType:${stockingType}<br>
                        commerceItem:${commerceItem}<br>
                        eCommerceDisplay:${eCommerceDisplay}<br>
                        unitPrice:${unitPrice}<br>
                        availableQuantity:${commerceItem.availableQuantity}<br>
                        isAvailabilityChecked:${isAvailabilityChecked}<br>
            --%>

            <dsp:getvalueof var="quantityAvailable" param="commerceItem.availableQuantity"/>
            <dsp:getvalueof var="leadTime" param="commerceItem.leadTime"/>

            <dsp:getvalueof var="showAvailability" value="${commerceItem.availableQuantity >= 0}"/>
            <%-- according to CPS-483
                                    <c:if test="${showAvailability eq true && eCommerceDisplay}">
                                        <dsp:getvalueof var="productDisabled" value="${productDisabled || quantityAvailable == 0}"/>
                                    </c:if>
            --%>

            <dsp:getvalueof var="disabledClass" value="${productDisabled || !productPurchasable ? 'disabled' : ''}"/>
            <dsp:droplet name="/cps/droplet/AliasNumberLookup">
                <dsp:param name="skuId" value="${skuId}"/>
                <dsp:oparam name="output">
					<dsp:getvalueof var="prodAlias" param="alias"/>
                    <dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',<br/>')}"/>
                </dsp:oparam>
            </dsp:droplet>

            <c:set var="combinedDescription">
                <dsp:valueof value="${productMap.description}" converter="valueishtml"/>
            </c:set>

            <%--Item start--%>
            <c:choose>
                <c:when test="${isCart}">
                    <dsp:getvalueof var="stockingAvailable" value="${stockingType == 'O' || stockingType == 'U' || stockingType == 'K' || stockingType == 'X'}"/>
                    <tr class="content ${disabledClass} ${showAvailability eq true ? ' av' : ''} ${(stockingAvailable || !productPurchasable) ? ' obsolete' : ''}"
                        id="row_${ciId}">
                        <c:if test="${productDisabled || !productPurchasable || !isProductPurchasableStockingType}">
                            <input type="hidden" name="productDisabled" id="productDisabled" value="${productDisabled || !productPurchasable || isProductPurchasableStockingType}"/>
                        </c:if>

                        <td class="text-center">
                            <span class="item-thumb"><cp:readerimg src="${imageUrl}"/></span>
                        </td>
                        <td class="col check">
                            <label>
                                <input type="checkbox" value="${ciId}" id="${skuId}"/>
                                <input type="hidden" value="${ciId}" id="${skuId}"/>
                                <span class="visible-xs-inline ml5" style="white-space: normal">
                                    ${combinedDescription}
                                    
                                    <c:if test="${productDisabled}">
                                    	<dsp:include page="/includes/gadgets/product-not-available.jsp"/>
									</c:if>
									<c:if test="${!productPurchasable || isProductPurchasableStockingType }">
                                    	<dsp:include page="/includes/gadgets/product-not-purchasable.jsp"/>
									</c:if>
                                </span>
                            </label>
                        </td>
                        <td class="hidden-xs col desc">
                            <a class="product-title" href="${productUrl}">
                                ${combinedDescription}
                            </a>
                            <dsp:getvalueof var="message" param="commerceItem.auxiliaryData.productRef.substituteBrandMessage.message"/>
                            <c:if test="${ not empty message}">
                                <span class="product-not-available">
                                    <div>
                                        <i class="fa fa-exclamation-triangle gold"></i>
                                        <strong style="color: #333333;font-size: 13;">
                                                ${message}
                                        </strong>
                                    </div>
                                </span>
                            </c:if>
                            <c:if test="${productDisabled}">
								<dsp:include page="/includes/gadgets/product-not-available.jsp"/>
							</c:if>
							<c:if test="${!productPurchasable}">
                                <dsp:include page="/includes/gadgets/product-not-purchasable.jsp"/>
							</c:if>
                        </td>
                        <td>
                            <span class="caption">Item #</span><dsp:valueof
                                value="${productMap.displayName}" converter="valueishtml"/>
                        </td>
                        <c:if test="${not isTransient}">
                            <td><span class="caption">My Part #</span>${alias}</td>
                            <td <c:if test="${productDisabled || !product.productPurchasable || isProductPurchasableStockingType }">style="color: darkgray;"</c:if> >
                                <span class="caption">Auto-Reorder</span>
                                <div class="reorder">
                                    <label>
                                        <input class="checked" type="radio" name="ctr_reorder_${skuId}" id="ctr_reorder_${skuId}"
                                               value="once" <c:if test="${empty scheduleDays || scheduleDays < 1}">checked</c:if>
                                               data-event-click-id="checkout32" data-event-click-param0="${skuId}" >
                                        <span class="ml5 small">Order once</span>
                                    </label>
                                    <span class="clearfix">
                                            <label>
                                                <input class="checked" type="radio" name="ctr_reorder_${skuId}" id="ctr_reorder_${skuId}" value="every"
                                                       <c:if test="${not empty scheduleDays && scheduleDays > 0}">checked</c:if> data-event-click-id="checkout33" data-event-click-param0="${skuId}"
                                                       <c:if test="${productDisabled || !productPurchasable || isProductPurchasableStockingType}">disabled</c:if> >
                                                <span class="ml5 small">Reorder every</span>
                                            </label>
                                            <div class="form-group form-group-inline small ml5">
                                                <input type="number" id="scheduled-days-field-${skuId}" name="scheduled-days-field-${skuId}"
                                                       class="display-inline-xs form-control input-qty input-sm text-center"
                                                       value="<c:if test="${not empty scheduleDays && scheduleDays > 0}">${scheduleDays}</c:if>"
                                                       <c:if test="${empty scheduleDays || scheduleDays < 1}">disabled</c:if>> days
                                            </div>
                                        </span>
                                </div>
                            </td>
                        </c:if>
                        <td class="text-center text-left-xs col qty" <c:if test="${productDisabled || !productPurchasable || isProductPurchasableStockingType}">style="color: darkgray;"</c:if> >
                            <span class="caption">Qty</span>
                            <c:choose>
								<c:when test="${isCart && not productDisabled && productPurchasable && !isProductPurchasableStockingType}">
									<input autocomplete="off" id="qty-input-${skuId}" type="text"
										class="form-control input-qty text-center display-block display-inline-xs qty-input"
										maxlength="5" value="${qty}" 
										data-event-change-id="checkout34"
										data-event-change-param0="${skuId}"
										data-event-change-param1="${qty}"
										data-event-change-param2="${prodId}"
										data-cid="${ciId}">
									<%-- <a class="btn-link btn-sm" href="#"
										data-event-click-id="checkout34"
										data-event-click-param0="${skuId}"
										data-event-click-param1="${qty}"
										data-event-click-param2="${prodId}">Update Item</a> --%>
								</c:when>
								<c:otherwise>
									<input type="text"
										class="form-control input-qty text-center display-block display-inline-xs"
										value="${qty}" disabled>
									<%-- <c:if test="${productDisabled}">Update Item</c:if> --%>
								</c:otherwise>
							</c:choose>
						</td>
                        <td class="text-center text-left-xs col u-price" <c:if test="${productDisabled || !productPurchasable || isProductPurchasableStockingType}">style="color: darkgray;"</c:if> >
                        	<span class="caption">Unit Price</span>
                        	<dsp:valueof param="commerceItem.priceInfo.listPrice" converter="currencyConversion"/>
                        </td>
                        <td class="text-center text-left-xs col t-price" <c:if test="${productDisabled || !productPurchasable || isProductPurchasableStockingType}">style="color: darkgray;"</c:if> >
                        	<span class="caption">Total Price</span><dsp:valueof param="commerceItem.priceInfo.amount" converter="currencyConversion"/>
                        </td>
                        <td class="text-center text-left-xs col availability">
                            <c:choose>
                                <c:when test="${not eCommerceDisplay}">
                                    <span class="caption">Availability</span>
                                    <span class="text-danger">No Longer Offered</span>
                                </c:when>
                                <c:when test="${productDisabled}">
                                    <span class="caption">Availability</span>
                                    <span class="text-danger">
								        Unavailable
							        </span>
                                </c:when>
                                <c:when test="${!productPurchasable || isProductPurchasableStockingType}">
                                    <span class="caption">Availability</span>
                                    <span class="text-danger">
								        Unavailable for online purchase
							        </span>
                                </c:when>
                                <c:when test="${unitPrice > 0}">
                                    <span class="caption">Availability</span>
                                    <c:choose>
                                        <c:when test="${deliveryMethod == 'shipped' && quantityAvailable >= 0}">
                                            <span>Now : <dsp:valueof param="commerceItem.defaultBranchQty"/> Unit(s)</span><br/> 
                                            <span>Within Days : ${quantityAvailable} Unit(s)</span>
                                        </c:when>
                                        <c:when test="${deliveryMethod == 'pick-up' && quantityAvailable > 0}">
                                        	<p>${quantityAvailable}</p>
                                        </c:when>
                                        <c:otherwise>
                                        	<dsp:valueof value="${leadTime}"/> <br/> business days
<%--                                             <c:choose> --%>
<%--                                                 <c:when test="${leadTime < leadTimeThreshold}"> --%>
<%--                                                     <dsp:getvalueof var="leadTimeMessageKey" value="cart-ships-message"/> --%>
<%--                                                 </c:when> --%>
<%--                                                 <c:otherwise> --%>
<%--                                                     <dsp:getvalueof var="leadTimeMessageKey" value="cart-lead-time-message"/> --%>
<%--                                                 </c:otherwise> --%>
<%--                                             </c:choose> --%>
<!--                                             <span> -->
<%--                                                 <dsp:include page="/includes/gadgets/info-message.jsp"> --%>
<%--                                                     <dsp:param name="key" value="${leadTimeMessageKey}"/> --%>
<%--                                                     <dsp:param name="notWrap" value="true"/> --%>
<%--                                                 </dsp:include> --%>
<!--                                             </span> -->
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <span></span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:when>
                
                <c:otherwise>
					<tr>
						<td class="text-center">
							<span class="item-thumb">
								<cp:readerimg src="${imageUrl}"/>
							</span>
						</td>
						<td>
							<a class="product-title" href="${productUrl}"> ${combinedDescription} </a> 
							<span class="hidden">${productMap.description}</span>
						</td>
						<td>
							<span class="caption">Item #</span>
							<dsp:valueof value="${productMap.displayName}" converter="valueishtml" />
						</td>
                        <c:if test="${not isTransient}">
                            <td><span class="caption">My Part #</span>${alias}</td>
                            <td>
                                <span class="caption">Auto-Reorder</span>
                                <c:choose>
                                <c:when test="${not empty scheduleDays && scheduleDays > 0}">
                                    <span class="text-center ml5 small">Reorder every ${commerceItem.scheduleDays} days</span>
                                </c:when>
                                <c:otherwise>
                                    <span></span>
                                </c:otherwise>
                                </c:choose>
                            </td>
                        </c:if>
						<td class="text-center text-left-xs">
							<span class="caption">Qty</span>
                            <dsp:valueof value="${qty}" converter="valueishtml" />
						</td>
						<td class="text-center text-left-xs">
							<span class="caption">Unit Price</span>
							<c:if test="${unitPrice > 0}">
                                <dsp:valueof param="commerceItem.priceInfo.listPrice" converter="currencyConversion"/>
                            </c:if>
						</td>
						<td class="text-center text-left-xs">
							<span class="caption">Total Price</span>
							<c:if test="${unitPrice > 0}">
                                <dsp:valueof param="commerceItem.priceInfo.amount" converter="currencyConversion"/>
                            </c:if>	
						</td>
					</tr>
                </c:otherwise>
            </c:choose>
            <%--Item end--%>
			
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>