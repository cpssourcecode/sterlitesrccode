<dsp:page>

    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>

    <dsp:droplet name="ForEach">
        <dsp:param name="array" value="${fn:split(param.cids, ',')}"/>
        <dsp:param name="elementName" value="cid"/>
        <dsp:oparam name="output">
            <dsp:include page="/checkout/includes/cart-table-row.jsp">
                <dsp:param name="source" value="${param.source}"/>
                <dsp:param name="itemId" param="cid"/>
            </dsp:include>
        </dsp:oparam>
        <dsp:oparam name="empty">
            <div class="content empty-cart">
                Your Cart is Empty
            </div>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>