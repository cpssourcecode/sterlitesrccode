<dsp:importbean bean="/atg/userprofiling/Profile"/>
<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
<dsp:importbean bean="/cps/droplet/SnapPayFormDroplet"/>

<dsp:droplet name="SnapPayFormDroplet">
    <dsp:param name="order" bean="ShoppingCart.current"/>
    <dsp:param name="profile" bean="Profile"/>
    <dsp:param name="maintainCreditCards" value="${false}"/>
    <dsp:oparam name="output">
        <div class="row">
            <div class="col-md-12">
                <dsp:getvalueof var="iframeUrl" bean="SnapPayFormDroplet.iframeUrl"/>
                <dsp:getvalueof var="requestNumber" param="requestNumber"/>
                <iframe id="cc-iframe" src="${iframeUrl}${requestNumber}" style="height: 100%; width: 100%;"></iframe>
            </div>
        </div>
    </dsp:oparam>
    <dsp:oparam name="error">
        Error
    </dsp:oparam>
</dsp:droplet>