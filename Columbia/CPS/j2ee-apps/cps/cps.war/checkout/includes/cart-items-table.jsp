<dsp:page>
  	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull"/>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />

    <dsp:getvalueof var="source" param="source"/>
    <dsp:getvalueof var="sortOption" param="sortOption"/>
    <dsp:getvalueof var="sortOptionType" param="sortOptionType"/>
    <dsp:setvalue bean="CPSSessionBean.productPricesCache" value="${null}"/>
    <dsp:droplet name="/cps/droplet/CPSMultiPriceDroplet">
        <dsp:param bean="ShoppingCart.current" name="order"/>
        <dsp:param bean="Profile" name="profile"/>
        <dsp:param bean="ShoppingCart" name="shoppingCart"/>
    </dsp:droplet>

	<dsp:droplet name="/cps/droplet/CPSSortCommerceItemsDroplet">
		<dsp:param name="sortOption" value="${sortOption}"/>
		<dsp:param name="sortOptionType" value="${sortOptionType}"/>
		<c:choose>
			<c:when test="${source == 'complete'}">
				<dsp:param name="commerceItems" bean="ShoppingCart.last.commerceItems" />
			</c:when>
			<c:otherwise>
				<dsp:param name="commerceItems" bean="ShoppingCart.current.commerceItems" />
			</c:otherwise>
		</c:choose>
		<dsp:oparam name="output">
			<dsp:getvalueof var="sortedCis" param="sortedCommerceItems"/>
		</dsp:oparam>
		<dsp:oparam name="empty">
			<c:choose>
				<c:when test="${source == 'complete'}">
					<dsp:getvalueof var="sortedCis" bean="ShoppingCart.last.commerceItems" />
				</c:when>
				<c:otherwise>
					<dsp:getvalueof var="sortedCis" bean="ShoppingCart.current.commerceItems" />
				</c:otherwise>
			</c:choose>
		</dsp:oparam>
	</dsp:droplet>

    <dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" value="${sortedCis}"/>
        <dsp:param name="elementName" value="item"/>

        <dsp:oparam name="output">

            <dsp:include page="/checkout/includes/cart-table-row.jsp">
                <dsp:param name="source" value="${source}"/>
                <dsp:param name="itemId" param="item.id"/>
            </dsp:include>
        </dsp:oparam>
        <dsp:oparam name="empty">
        	<c:choose>
        	<c:when test="${source == 'cart'}">
	            <div class="content empty-cart">
	                Your Cart is Empty
	            </div>
	        </c:when>
            <c:otherwise>            
            </c:otherwise>
            </c:choose>
        </dsp:oparam>
    </dsp:droplet>
    
    <dsp:getvalueof var="isAvailablePricesWebService" bean="CPSSessionBean.availablePricesWebService" />
    <input type="hidden" name="isAvailablePricesWebService" id="isAvailablePricesWebService" value="${isAvailablePricesWebService}"/>

</dsp:page>