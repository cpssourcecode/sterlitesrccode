<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/FullShoppingCartFormHandler"/>

	<dsp:getvalueof var="showEditLink" param="showEditLink"/>
	<dsp:getvalueof var="complete" param="complete"/>
    <dsp:getvalueof var="type" param="type"/>
	<dsp:getvalueof var="jdeId" param="jdeId"/>
	<dsp:tomap var="address" param="address"/>

	<c:choose>
		<c:when test="${type eq 'billing'}">
			<dsp:getvalueof var="headerType" value="Billing Address"/>
			<c:if test="${empty complete || !complete}">
				<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.jdeAddressNumberCB" value="${jdeId}"/>
			</c:if>
		</c:when>
		<c:when test="${type eq 'ispu'}">
			<dsp:getvalueof var="headerType" value="Store Pickup"/>
			<c:if test="${empty complete || !complete}">
				<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.jdeAddressNumberCB" value="${jdeId}"/>
			</c:if>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="headerType" value="Shipping Address"/>
			<c:if test="${empty complete || !complete}">
				<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.jdeAddressNumberCS" value="${jdeId}"/>
			</c:if>
		</c:otherwise>
	</c:choose>

	<c:if test="${empty complete || !complete}">
		<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.${type}CompanyName" value="${address.companyName}"/>
		<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.${type}Address1" value="${address.address1}"/>
		<c:if test="${!empty address.address2}">
			<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.${type}Address2" value="${address.address2}"/>
		</c:if>
		<c:if test="${!empty address.address3}">
			<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.${type}Address3" value="${address.address3}"/>
		</c:if>
		<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.${type}City" value="${address.city}"/>
		<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.${type}State" value="${address.state}"/>
		<dsp:input type="hidden" bean="FullShoppingCartFormHandler.values.${type}Zip" value="${address.postalCode}"/>
	</c:if>

	<h3>
		${headerType}
		<c:if test="${showEditLink}">
			<small class="subheader">
				<a class="link-underline" href="${originatingRequest.contextPath}/checkout/registered.jsp">
					<span>Edit</span>
				</a>
			</small>
		</c:if>
	</h3>

	<address>
		<c:if test="${!empty address.companyName}"><strong>${vsg_utils:escapeHtml(address.companyName)}</strong><br></c:if>
		${vsg_utils:escapeHtml(address.address1)}&nbsp;<c:if test="${!empty address.address2}">${vsg_utils:escapeHtml(address.address2)}</c:if><br>
		<c:if test="${!empty address.address3}">${vsg_utils:escapeHtml(address.address3)}<br></c:if>
		${vsg_utils:escapeHtml(address.city)},&nbsp;${vsg_utils:escapeHtml(address.state)}&nbsp;${vsg_utils:escapeHtml(address.postalCode)}<br>
		<c:if test="${!empty address.phoneNumber}"><abbr title="Phone">P:</abbr>&nbsp;${vsg_utils:escapeHtml(address.phoneNumber)}</c:if>
	</address>

</dsp:page>