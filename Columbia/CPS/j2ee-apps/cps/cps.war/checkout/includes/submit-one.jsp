<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>

	<dsp:getvalueof var="total" bean="ShoppingCart.current.priceInfo.amount"/>
	<dsp:getvalueof var="orderPriceLimit" bean="Profile.orderPriceLimit"/>


	<fmt:bundle basename="web.resources.WebAppResources">

		<a id="submit1" href="#" class="btn btn-warning btn-lg btn-block" data-event-click-id="checkout35" >
			<c:choose>
				<c:when test="${total > orderPriceLimit }">
					<fmt:message key="checkout.review.submitForApproval.order.button"/>
				</c:when>
				<c:otherwise>
					<fmt:message key="checkout.review.place.order.button"/>
				</c:otherwise>
			</c:choose>
		</a>

	</fmt:bundle>

</dsp:page>