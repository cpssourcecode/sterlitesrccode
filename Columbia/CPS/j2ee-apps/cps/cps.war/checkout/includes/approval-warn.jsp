<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>

	<dsp:getvalueof var="total" bean="ShoppingCart.current.priceInfo.amount"/>
	<dsp:getvalueof var="orderPriceLimit" bean="Profile.orderPriceLimit"/>

	<fmt:bundle basename="web.resources.WebAppResources">
		
		<dsp:droplet name="/cps/droplet/IsApprovalRequired">
			<dsp:param name="order" bean="ShoppingCart.current"/>
			<dsp:oparam name="true">
				<dsp:getvalueof var="isApprovalRequired" value="${true}"/>
			</dsp:oparam>
			<dsp:oparam name="false">
				<dsp:getvalueof var="isApprovalRequired" value="${false}"/>
			</dsp:oparam>
		</dsp:droplet>
		
		<div class="col-xs-12" id="approvalWarn">
			<c:if test="${isApprovalRequired}">
				<div class="important-message panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<div class="nolink">
									<i class="fa fa-exclamation-triangle"></i>
									<fmt:message key="checkout.review.approval.warn"/>
								</div>
							</h4>
						</div>
					</div>
				</div>
			</c:if>
		</div>

	</fmt:bundle>

</dsp:page>