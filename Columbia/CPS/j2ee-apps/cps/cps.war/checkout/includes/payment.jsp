<dsp:page>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/payment/CreditCardTools"/>

	<dsp:getvalueof var="showEditLink" param="showEditLink"/>

	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
		<dsp:include page="${originatingRequest.contextPath}/checkout/includes/address-view.jsp">
			<dsp:param name="type" value="billing"/>
			<dsp:param name="address" param="order.paymentGroups[0].billingAddress"/>
			<dsp:param name="showEditLink" value="${showEditLink}"/>
			<dsp:param name="complete" value="${true}"/>
		</dsp:include>
		<dsp:tomap var="paymentGroup" param="order.paymentGroups[0]"/>
		<p>
			<c:choose>
				<c:when test="${paymentGroup.paymentGroupClassType eq 'invoiceRequest'}">
					<dsp:getvalueof var="PONumber" value="${paymentGroup.PONumber}"/>
					<strong>
						On Account
						<br />
						<c:if test="${PONumber != 'On Account'}">
							PO #
						</c:if>
					</strong>
					<c:if test="${PONumber != 'On Account'}">
						${vsg_utils:escapeHtml(PONumber)}
					</c:if>
				</c:when>
				<c:when test="${paymentGroup.paymentGroupClassType eq 'creditCard'}">
					<strong>
						By Credit Card
					</strong>
					<br>
					<dsp:valueof bean="CreditCardTools.cardTypesMap.${paymentGroup.creditCardType}"/>
					&nbsp;
					<dsp:valueof value="${vsg_utils:escapeHtml(paymentGroup.creditCardNumber)}"/>
				</c:when>
				<c:otherwise>
					<strong>
						Unknown Payment Type
					</strong>
				</c:otherwise>
			</c:choose>
		</p>
	</div>

</dsp:page>