<dsp:page>

    <dsp:importbean bean="/atg/commerce/ShoppingCart"/>

    <dsp:getvalueof var="total" bean="ShoppingCart.current.priceInfo.amount"/>
    <dsp:valueof value="${empty total ? 0 : total}" converter="currencyConversion"/>

</dsp:page>