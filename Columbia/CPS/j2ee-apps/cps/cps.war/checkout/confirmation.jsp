<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/ShoppingCart"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/droplet/CleanProfileCartPropertiesDroplet"/>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>

	<dsp:getvalueof var="isTransient" bean="Profile.transient" />

	<cp:pageContainer page="checkout">
		<body>
			<div class="container main-content ${csrClass}">
			<section>
				<dsp:include page="/checkout/gadgets/checkout-breadcrumb.jsp">
					<dsp:param name="step" value="confirmation" />
				</dsp:include>
			</section>
			
			<dsp:getvalueof var="order" bean="ShoppingCart.last" />
			<dsp:tomap var="paymentGroup" bean="ShoppingCart.last.paymentGroups[0]" />
			<dsp:getvalueof var="orderState" bean="ShoppingCart.last.stateAsString" />
			<dsp:getvalueof var="isApprovalRequired" value="${false}" />
			<c:if test="${orderState == 'PENDING_APPROVAL'}">
				<dsp:getvalueof var="isApprovalRequired" value="${true}" />
			</c:if>
			<dsp:getvalueof var="jobNumberOrName" value="${order.jobName}"/>

			<c:choose>
				<c:when test="${isApprovalRequired}">
					<h1>Order Submitted for Approval.</h1>

					<p>Your online order has been submitted for approval. You will receive a status update
						when this order is approved or rejected.</p>
				</c:when>
				<c:otherwise>
					<h1 class="mb20">Thank You</h1>

					<p>Your order has been placed. An Order Acknowledgment with shipping, tax, and other
						potential charges will be emailed to you once your order has been completed.
						<c:if test="${not isTransient}">Or, you may go
						to the Order History page in this website and select the "Acknowledgment PDF" from the list.
						</c:if>
						Please note it can take up to 15 minutes for an acknowledgement to generate.</p>
				</c:otherwise>
			</c:choose>

			<section>
				<div class="well">
					<h2 class="h4 text-nocase">
						Web Order Confirmation Number:
						<dsp:valueof value="${order.webOrderId}" />
					</h2>
					<div class="row">
						<div class="col-md-2 col-sm-3">
							<dl class="row row-narrow">
								<dt class="col-sm-12 col-xs-3">Ordered On</dt>
								<dd class="col-sm-12 col-xs-9">
									<dsp:valueof bean="ShoppingCart.last.submittedDate" converter="date" format="MM/dd/YYYY" />
								</dd>
							</dl>
						</div>
						<c:if test="${paymentGroup.paymentGroupClassType eq 'invoiceRequest'}">
						<div class="col-md-2 col-sm-3">
							<dl class="row row-narrow">
								<dt class="col-sm-12 col-xs-3">PO#</dt>
								<dd class="col-sm-12 col-xs-9">
									<dsp:getvalueof var="PONumber" value="${paymentGroup.PONumber}" />									
									<p id="textWrap">${PONumber}</p>
								</dd>
							</dl>
						</div>
						<c:if test="${not empty jobNumberOrName}">
							<div class="col-md-2 col-sm-3">
								<dl class="row row-narrow">
									<dt class="col-sm-12 col-xs-3">Job Name/Number</dt>
									<dd class="col-sm-12 col-xs-9">									
										<p id="textWrap">${jobNumberOrName}</p>
									</dd>
								</dl>
							</div>
						</c:if>						
						</c:if>
						<div class="col-md-2 col-sm-3">
							<dl class="row row-narrow">
								<dt class="col-sm-12 col-xs-3"># of Items</dt>
								<dd class="col-sm-12 col-xs-9">
									<dsp:getvalueof var="orderItems" value="${order.commerceItems}" />
									<p>${fn:length(orderItems)}</p>
								</dd>
							</dl>
						</div>
						<div class="col-md-2 col-sm-3">
							<dl class="row row-narrow">
								<dd class="col-sm-12 col-xs-9">
									<dsp:include page="/checkout/gadgets/confirmation-ship-to-address.jsp" />
								</dd>
							</dl>
						</div>
						<div class="col-md-2 col-sm-3">
							<dl class="row row-narrow">
								<dt class="col-sm-12 col-xs-3">Total Amount</dt>
								<dd class="col-sm-12 col-xs-9">
									<p>
										<dsp:valueof bean="ShoppingCart.last.priceInfo.amount" converter="currencyConversion" />
									</p>
								</dd>
							</dl>
						</div>
					</div>
				</div>
			</section>

			<c:if test="${isApprovalRequired}">
				<div class="form-group hidden-print">
					<p>
						<a href="/account/order-requests.jsp" class="btn btn-primary btn-lg">View Pending Order Requests</a>
					</p>
				</div>
			</c:if>
			
			<c:if test="${!isApprovalRequired}">
				<div class="form-group hidden-print">
					<p>
					<c:choose>
						<c:when test="${isTransient}">
							<a href="/" class="btn btn-primary btn-lg" >Continue Shopping</a>
						</c:when>
						<c:otherwise>
							<a href="#" class="btn btn-primary btn-lg" data-event-click-id="checkout9" data-event-click-param0="${order.id}" >View Order Details</a>
							<div id="view_order_div"></div>
						</c:otherwise>
					</c:choose>
					</p>	
				</div>
			</c:if>
		</div>			
		
		<dsp:droplet name="CleanProfileCartPropertiesDroplet">
			<dsp:param bean="Profile" name="profile"/>
		</dsp:droplet>
		</body>
	</cp:pageContainer>
	<dsp:droplet name="/cps/droplet/OrderCompleteDataLayerDroplet">
		<dsp:param name="order" bean="ShoppingCart.last" />
		<dsp:oparam name="output">
			<dsp:getvalueof var="outputObject" param="outputObject" />
			<script type="text/javascript" nonce="${requestScope.nonce}">
                window.dataLayer = window.dataLayer || [];
                window.dataLayer.push(
                                ${outputObject}
                );
                </script>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>
