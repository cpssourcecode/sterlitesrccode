<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/cps/userprofiling/ProfileDefaultShippingAddressFormHandler"/>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/GuestAddressFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/RepriceOrderDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/cps/droplet/CartAddressDroplet" />
	<dsp:importbean bean="/cps/droplet/IsApprovalRequired" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CreditCardFormHandler" />
	<dsp:importbean bean="/cps/droplet/SnapPayFormDroplet" />
	<dsp:importbean bean="/cps/droplet/AllowCreditCardOnApprovalDroplet" />
	<dsp:importbean bean="/cps/droplet/CreditCardEnabledDroplet" />


	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<input type="hidden" name="isTransient" id="isTransientHidden" value="${isTransient}"/>

	<div id="reprice-order">
		<dsp:include page="/checkout/gadgets/reprice-order.jsp"/>
	</div>

	<dsp:getvalueof var="mtrRequested" bean="ShoppingCart.current.mtrRequested" />
	<dsp:getvalueof var="total" bean="ShoppingCart.current.priceInfo.amount" />

	<dsp:droplet name="IsApprovalRequired">
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="true">
			<dsp:getvalueof var="isApprovalRequired" value="${true}" />
			<dsp:getvalueof var="alert_display_class" value="block" />
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="isApprovalRequired" value="${false}" />
			<dsp:getvalueof var="alert_display_class" value="none" />
		</dsp:oparam>
	</dsp:droplet>
	<input type="hidden" name="isApprovalRequired" id="isApprovalRequiredHidden" value="${isApprovalRequired}"/>

	<dsp:droplet name="CreditCardEnabledDroplet">
		<dsp:oparam name="true">
			<dsp:getvalueof var="isCreditCardEnabled" value="${true}"/>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="isCreditCardEnabled" value="${false}"/>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:droplet name="AllowCreditCardOnApprovalDroplet">
		<dsp:oparam name="true">
			<dsp:getvalueof var="isCreditCardAllowed" value="${true and isCreditCardEnabled}"/>
		</dsp:oparam>
		<dsp:oparam name="false">
			<dsp:getvalueof var="isCreditCardAllowed" value="${not isApprovalRequired and isCreditCardEnabled}"/>
		</dsp:oparam>
	</dsp:droplet>

	<dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>
	<cp:pageContainer page="checkout">
		<main id="body">
		<div class="container">

			<dsp:include page="/checkout/gadgets/checkout-breadcrumb.jsp">
				<dsp:param name="step" value="review" />
			</dsp:include>
			<h1 class="mb20">Checkout</h1>

			<div class="alert alert-danger alert-icon" style="display: ${alert_display_class}">
				<c:if test="${isApprovalRequired}">
					<i class="fa fa-exclamation-triangle red"></i>
					<cp:repositoryMessage key="orderApprovalMsg"/>
				</c:if>
				<p></p>
				<span id="po-empty" style="display: none">
					<fmt:message key="checkout.review.po.empty" />
				</span>
				<span id="po-toolong" style="display: none">
					<fmt:message key="checkout.review.po.toolong" />
				</span>
				<span id="onsite-name-empty" style="display: none">
					<fmt:message key="checkout.review.onsite.name.empty" />
				</span>
				<span id="onsite-name-toolong" style="display: none">
					<fmt:message key="checkout.review.onsite.name.toolong" />
				</span>
				<span id="onsite-phone-empty" style="display: none">
					<fmt:message key="checkout.review.onsite.phone.empty" />
				</span>
				<span id="onsite-phone-toolong" style="display: none">
					<fmt:message key="checkout.review.onsite.phone.toolong" />
				</span>
			</div>

			<div class="row">
				<div class="col-sm-3 col-sm-push-9 mb30">
					<dsp:include page="/checkout/gadgets/summary-block.jsp">
						<dsp:param name="order" bean="ShoppingCart.current" />
					</dsp:include>

					<button id="submit1" class="btn btn-lg btn-primary btn-place-order btn-block" data-event-click-id="checkout18">
						<c:choose>
							<c:when test="${isApprovalRequired}">
								<fmt:message key="checkout.review.submitForApproval.order.button" />
							</c:when>
							<c:otherwise>
								<fmt:message key="checkout.review.place.order.button" />
							</c:otherwise>
						</c:choose>
					</button>

				</div>


				<div class="col-sm-9 col-sm-pull-3">
					<div class="panel panel-default" id="panel-delivery-method">
						<div class="panel-heading">Delivery Method</div>
						<div class="panel-body">

							<dsp:droplet name="CartAddressDroplet">
								<dsp:param bean="ShoppingCart.current" name="order" />
								<dsp:param bean="Profile" name="profile" />
								<dsp:param value="review" name="page" />
								<dsp:oparam name="output">
									<dsp:getvalueof var="deliveryMethod" param="deliveryMethod" />
									<dsp:getvalueof var="address" param="address" />
									<dsp:getvalueof var="addressId" param="addressId" />
									<dsp:getvalueof var="store" param="store" />
									<dsp:getvalueof var="storeId" param="storeId" />
								</dsp:oparam>
							</dsp:droplet>

							<dsp:include page="/checkout/gadgets/cart-delivery-method.jsp">
								<dsp:param name="deliveryMethod" value="${deliveryMethod}" />
								<dsp:param name="addressId" value="${addressId}" />
								<dsp:param name="isReviewPage" value="true"/>
							</dsp:include>

							<c:if test="${isTransient}">
								<div class="checkout-address-form delivery-method-shipped-form" style="display: ${deliveryMethod=='shipped' ? 'block':'none'};">
									<dsp:form method="post" id="checkout-address-form">
										<dsp:input type="hidden" value="true" bean="GuestAddressFormHandler.setAddressOnCart" priority="-10"/>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="contactName">Contact Name*</label>
													<dsp:input type="text" iclass="form-control" id="contactName" value="${address.firstName} ${address.lastName}" bean="GuestAddressFormHandler.contactName"/>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="companyName">Company*</label>
													<dsp:input type="text" iclass="form-control" id="companyName" value="${address.companyName}" bean="GuestAddressFormHandler.companyName"/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="address1">Address 1*</label>
													<dsp:input type="text" iclass="form-control" id="address1" value="${address.address1}" bean="GuestAddressFormHandler.address1"/>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="address2">Address 2</label>
													<dsp:input type="text" iclass="form-control" id="address2" value="${address.address2}" bean="GuestAddressFormHandler.address2"/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="city">City*</label>
													<dsp:input type="text" iclass="form-control" id="city" value="${address.city}" bean="GuestAddressFormHandler.city"/>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="row">
													<div class="col-lg-4 col-md-4 col-sm-5 col-xs-5">
														<div class="form-group">
															<label class="control-label" for="state">State*</label>
															<dsp:input type="hidden" bean="GuestAddressFormHandler.state" value="${address.state}" name="state" id="stateHidden"/>
															<select class="form-control" id="state">
																<option disabled selected>Select...</option>
																<%@include file="/includes/states-html.jspf"%>
															</select>
														</div>
													</div>
													<div class="col-lg-8 col-md-8 col-sm-7 col-xs-7">
														<div class="form-group">
															<label for="zip">Zip Code*</label>
															<dsp:input type="number" iclass="form-control" value="${address.postalCode}" id="zip" bean="GuestAddressFormHandler.zipCode">
																<dsp:tagAttribute name="maxlength" value="5"/>
															</dsp:input>
															
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
												<div class="form-group">
													<label for="email">Email Address*</label>
													<dsp:input type="email" iclass="form-control" value="${address.email}" id="email" bean="GuestAddressFormHandler.email"/>
												</div>
											</div>
										</div>
									</dsp:form>
								</div>
							</c:if>

							<div class="checkbox">
								<label>
									<input id="order-mtr-inc" type="checkbox" value="${mtrRequested}">
									Include Materials Test Report(s) with shipment for applicable items.
								</label>
							</div>

							<c:if test="${isTransient}">
								<dsp:form method="post" id="checkout-po-number">
									<dsp:input type="hidden" value="true" bean="CartModifierFormHandler.setPONumber" priority="-10"/>
									<label for="commit-order-po-value">PO # *</label>
									<div class="checkout-payment-detail row row-narrow" id="checkout-payment-po">
										<div class="form-group col-sm-4">
											<label class="sr-only">Enter PO #</label>
											<input type="text" placeholder="Enter PO #" class="form-control" id="commit-order-po-value" maxlength="25" name="poNumber">
										</div>
										<p class="mt15"><small>If PO Number is not available, please enter your name in this field.</small></p>
									</div>
									<div class="checkout-payment-detail row row-narrow" id="checkout-onsite-contact" >
											<div class="form-group col-sm-4">
												<label for="commit-order-onsite-name-value">Onsite Contact Name *</label>
												<input type="text" class="form-control" placeholder="Enter Contact Name" maxLength="30" id="commit-order-onsite-name-value"/>
											</div>
											<p class="mt15">
												<small>This field is for contact information only, please contact your sales team for special delivery requests.
												</small>
											</p>
										</div>
										<div class="checkout-payment-detail row row-narrow" id="checkout-onsite-contact" >
											<div class="form-group col-sm-4">
												<label for="commit-order-onsite-phone-value">Onsite Contact Phone # *</label>
												<input type="text" class="form-control" placeholder="Enter Contact Phone" maxLength="30" id="commit-order-onsite-phone-value"/>
											</div>
										</div>
										<label for="commit-order-job-name-value"><fmt:message key="checkout.review.jobname.label" /></label>
										<div class="checkout-payment-detail row row-narrow" id="checkout-payment-jobname">
											<div class="form-group col-sm-4">
												<label class="sr-only"><fmt:message key="checkout.review.jobname.placeholder" /></label>
												<input type="text" placeholder="<fmt:message key="checkout.review.jobname.placeholder" />" class="form-control" id="commit-order-job-name-value" maxlength="25" name="jobName">
											</div>
											<p class="mt15"><small><fmt:message key="checkout.review.jobname.optional" /></small></p>
										</div>
								</dsp:form>
							</c:if>

							<c:if test="${isTransient}">
								<a href="#" class="btn btn-lg btn-primary xs-block btn-next-to-payment">
									Next
								</a>
							</c:if>

						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">Payment</div>
						<div class="panel-body">

							<div class="payment-triggers">
								<c:if test="${not isTransient}">
									<dsp:form method="post" id="checkout-po-number">
										<dsp:input type="hidden" value="true" bean="CartModifierFormHandler.setPONumber"
												   priority="-10"/>
										<label for="commit-order-po-value">PO # *</label>
										<div class="checkout-payment-detail row row-narrow" id="checkout-payment-po">
											<div class="form-group col-sm-4">
												<label class="sr-only">Enter PO #</label>
												<input type="text" placeholder="Enter PO #" class="form-control"
													   id="commit-order-po-value" name="poNumber" maxlength="25"
													   data-event-blur-id="checkoutOnBlurPOnumber">
											</div>
											<p class="mt15">
												<small>If PO Number is not available, please enter your name in this field.
												</small>
											</p>
										</div>
										<div class="checkout-payment-detail row row-narrow" id="checkout-onsite-contact" >
											<div class="form-group col-sm-4">
												<label for="commit-order-onsite-name-value">Onsite Contact Name *</label>
												<input type="text" class="form-control" placeholder="Enter Contact Name" maxLength="30" id="commit-order-onsite-name-value"/>
											</div>
											<p class="mt15">
												<small>This field is for contact information only, please contact your sales team for special delivery requests.
												</small>
											</p>
										</div>
										<div class="checkout-payment-detail row row-narrow" id="checkout-onsite-contact" >
											<div class="form-group col-sm-4">
												<label for="commit-order-onsite-phone-value">Onsite Contact Phone # *</label>
												<input type="text" class="form-control" placeholder="Enter Contact Phone" maxLength="30" id="commit-order-onsite-phone-value"/>
											</div>
										</div>
										<label for="commit-order-job-name-value"><fmt:message key="checkout.review.jobname.label"/></label>
										<div class="checkout-payment-detail row row-narrow" id="checkout-payment-jobname">
											<div class="form-group col-sm-4">
												<label class="sr-only"><fmt:message key="checkout.review.jobname.placeholder" /></label>
												<input type="text" placeholder="<fmt:message key="checkout.review.jobname.placeholder" />" class="form-control" id="commit-order-job-name-value" maxlength="25" name="jobName">
											</div>
											<p class="mt15"><small><fmt:message key="checkout.review.jobname.optional" /></small></p>
										</div>
									</dsp:form>

									<c:if test="${isCreditCardAllowed}">
										<div class="radio">
											<label class="active"
												   style="display: ${not isCreditCardAllowed ? "none" : "block"}">
												<input type="radio" name="checkout-payment" id="onAccountPaymentRadio"
													   value="onAccount">
												On Account
											</label>
										</div>
										<dsp:form id="commit-order-remove-credit-card"
												  formid="commit-order-remove-credit-card">
											<dsp:input bean="CreditCardFormHandler.removeCreditCard" type="hidden"
													   priority="-10" value=""/>
										</dsp:form>
									</c:if>
								</c:if>

								<c:if test="${isCreditCardAllowed}">
									<div class="radio">
										<label>
											<input type="radio" ${isTransient ? "checked" : ""} name="checkout-payment"
												   id="creditCardPaymentRadio" value="creditCard">
											By Credit Card
										</label>
									</div>
									<div style="display: none">
										<dsp:form method="post" id="create-credit-card-form">
											<dsp:input type="hidden" bean="CreditCardFormHandler.mtrRequested" value=""
													   id="credit-card-form-include-mtr"/>
											<dsp:input type="hidden" bean="CreditCardFormHandler.createCreditCard"
													   value="true" priority="-10"/>
										</dsp:form>
									</div>
								</c:if>

							</div>

							<c:if test="${isCreditCardAllowed}">
								<div class="checkout-payment-detail" id="checkout-payment-cc" style="display: none;">
									<c:if test="${not isTransient}">
										<dsp:include page="/checkout/includes/credit-card-iframe.jsp"/>
									</c:if>
								</div>
							</c:if>

						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Review Order</div>
				<div class="panel-body p0 pt10">

					<input type="hidden" id="cart-sort-option" value="" />
					<input type="hidden" id="cart-sort-option-type" value="asc" />

					<table class="table table-striped table-white table-mobile">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Description</th>
								<th>
									<span data-event-click-id="checkout19"  style="cursor: pointer;">
										Item #
										<i class="fa fa-sort" id="sort-option-item-number"></i>
									</span>
								</th>
								<c:if test="${not isTransient}">
									<th>My Part #</th>
									<th class="text-center">Auto-Reorder</th>
								</c:if>
								<th class="text-center">Qty</th>
								<th>Unit Price</th>
								<th>Total Price</th>
							</tr>
						</thead>
						<tbody class="content-list table-content content empty-cart">
						</tbody>
					</table>
				</div>
				<c:if test="${not isTransient}">
					<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
						<dsp:param name="messageKey" value="reorderDisclaimerMessage" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="reorderDisclaimerMessage" param="message" />
							<p class="reorder-disclaimer">${reorderDisclaimerMessage}</p>
						</dsp:oparam>
					</dsp:droplet>
				</c:if>
			</div>

			<div class="container preload-cover">
				<div class="loading preload-spinner">
					<i class="fa fa-spinner fa-spin"></i>
				</div>
			</div>

			<p>
				<a id="submit2" href="#" class="btn btn-lg btn-primary xs-block btn-place-order" data-event-click-id="checkout20">
					<c:choose>
						<c:when test="${isApprovalRequired}">
							<fmt:message key="checkout.review.submitForApproval.order.button" />
						</c:when>
						<c:otherwise>
							<fmt:message key="checkout.review.place.order.button" />
						</c:otherwise>
					</c:choose>
				</a>

				<a href="/checkout/cart.jsp" class="btn btn-lg btn-info xs-block">Update Shopping Cart</a>

				<dsp:form id="commit-order" formid="commit-order" method="POST">
					<dsp:input type="hidden" bean="CommitOrderFormHandler.paymentMethod" value="" id="commit-order-method" />
					<dsp:input type="hidden" bean="CommitOrderFormHandler.poNumber" value="" id="commit-order-po" />
					<dsp:input type="hidden" bean="CommitOrderFormHandler.mtrRequested" value="" id="commit-order-include-mtr" name="mtrRequested"/>
					<dsp:input type="hidden" bean="CommitOrderFormHandler.onsiteName" value="" id="commit-order-onsite-name"/>
					<dsp:input type="hidden" bean="CommitOrderFormHandler.onsitePhone" value="" id="commit-order-onsite-number"/>
					<dsp:input type="hidden" bean="CommitOrderFormHandler.jobName" value="" id="commit-order-jobName"/>
					<dsp:input bean="CommitOrderFormHandler.commitOrder" type="hidden" priority="-10" value="" />
				</dsp:form>

			</p>

		</div>
		<script type="text/javascript" nonce="${requestScope.nonce}">
			$(document).ready(function() {
				initReviewPage();
			});
		</script> </main>
	</cp:pageContainer>


</dsp:page>
