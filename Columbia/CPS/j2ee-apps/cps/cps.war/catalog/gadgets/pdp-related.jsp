<dsp:page>
	<dsp:getvalueof var="relatedProducts" param="product.relatedProducts"/>
	<c:if test="${not empty relatedProducts}">

		<h3>Related Items</h3>
		<hr>

		<div class="category-list">

			<dsp:droplet name="ForEach">
				<dsp:param name="array" param="product.relatedProducts"/>
				<dsp:param name="elementName" value="relatedProduct"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="productPurchasable" param="relatedProduct.productPurchasable"/>
					<dsp:getvalueof var="skuId" param="relatedProduct.childSKUs[0].repositoryId"/>

					<div class="media">

						<dsp:droplet name="/atg/repository/seo/CatalogItemLink">
							<dsp:param name="item" param="product.productRef"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
							</dsp:oparam>
						</dsp:droplet>

						<dsp:a iclass="media-left" href="${productUrl}">
							<img src="${staticContentPrefix}/content_images/img_placeholder.jpg" alt="...">
						</dsp:a>

						<div class="media-body">
							<h4 class="media-heading">
								<a href="${productUrl}">
									<dsp:valueof param="product.displayName" converter="valueishtml" />
								</a>
							</h4>
							<ul class="list-inline">
								<li>
									<span class="text-muted">Item #: <dsp:valueof value="${vsg_utils:escapeHtml(skuId)}"/></span>
								</li>
								<li>
									<span class="text-muted">Short Code: <dsp:valueof value="${vsg_utils:escapeHtml(skuId)}"/></span>
								</li>
								<dsp:droplet name="/cps/droplet/AliasNumberLookup">
									<dsp:param name="skuId" value="${skuId}"/>
									<dsp:oparam name="output">
										<dsp:getvalueof var="prodAlias" param="alias"/>
                    					<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
										<li>
											<span class="text-muted">Alias: <dsp:valueof value="${alias}"/></span>
										</li>
									</dsp:oparam>
								</dsp:droplet>
							</ul>
						</div>
						<div class="media-count">
							<ul class="vertical-alignment-fix list-inline">
								<li class="middle">
									<p class="text-muted">QTY</p>
								</li>
								<li>
									<div class="spinbox" data-initialize="spinbox" data-min="1" id="mySpinbox">
										<input type="text" value="1" class="form-control spinbox-input">

										<div class="spinbox-buttons btn-group btn-group-vertical">
											<button type="button" class="btn btn-default spinbox-up">
												<i class="fa fa-angle-up"></i>
												<span class="sr-only">Increase</span>
											</button>
											<button type="button" class="btn btn-default spinbox-down">
												<i class="fa fa-angle-down"></i>
												<span class="sr-only">Decrease</span>
											</button>
										</div>
									</div>
								</li>
							</ul>
							<c:if test="${productPurchasable}">
								<button class="btn btn-success btn-lg btn-block">Add to Cart</button>
							</c:if>
						</div>
					</div>

				</dsp:oparam>
			</dsp:droplet>

		</div>

	</c:if>

</dsp:page>