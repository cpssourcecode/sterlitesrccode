<dsp:page>
	<dsp:getvalueof var="content" param="contentItem"/>
	<dsp:getvalueof var="totalNumRecs" value="${content.totalNumRecs}"/>

	<fmt:bundle basename="web.resources.WebAppResources">

		<div class="text-right hidden-print pagination-container">
			<div class="pagination">
				<nav>
					<c:if test="${totalNumRecs >= 10}">
						<dsp:include src="listing-pagination.jsp">
							<dsp:param name="start" value="${content.firstRecNum-1}"/>
							<dsp:param name="howMany" value="${content.recsPerPage}"/>
							<dsp:param name="size" value="${content.totalNumRecs}"/>
							<dsp:param name="pagingActionTemplate" value="${content.pagingActionTemplate}"/>
							<dsp:param name="content" param="contentItem"/>
						</dsp:include>
					</c:if>
				</nav>
			</div>
		</div>

	</fmt:bundle>
</dsp:page>