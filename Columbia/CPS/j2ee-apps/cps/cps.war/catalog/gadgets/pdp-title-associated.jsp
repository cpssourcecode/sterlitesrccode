<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<%--<dsp:getvalueof var="prodId" param="product.id"/>--%>

	<dsp:getvalueof var="productId" param="productId"/>
	<%--<dsp:getvalueof var="skuId" param="product.childSKUs[0].repositoryId"/>--%>


	<c:set var="contentCollection" value="/content/Shared/Pdp/Associated Products"/>
	<dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
		<dsp:param name="contentCollection" value="${contentCollection}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="contents" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem.contents"/>
			<c:if test="${fn:length(contents) > 0}">
				<dsp:getvalueof var="apConfig" vartype="com.endeca.infront.assembler.ContentItem" value="${contents[0]}"/>
				<dsp:droplet name="/cps/droplet/AssociatedProductsDroplet">
					<%--<dsp:param name="product" param="product"/>--%>
					<dsp:param name="productId" param="productId"/>
					<dsp:param name="productsNumber" value="${apConfig.productsNumberMax}"/>
					<dsp:oparam name="output">

						<%--<div id="associatedProductsDiv" class="col-md-4 associated-products-container hidden-sm hidden-xs pdp-related">--%>
						<div class="panel panel-default panel-associated-products">
							<div class="panel-heading box-title-side-bar">${apConfig.name}</div>
							<div class="panel-body">
								<dsp:getvalueof var="apArray" param="associatedProducts"/>


								<ul class="item item-related">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="associatedProducts"/>
										<dsp:param name="elementName" value="ap"/>
										<dsp:oparam name="output">

											<dsp:getvalueof var="skuId" param="ap.childSKUs[0].repositoryId"/>
											<dsp:getvalueof var="prodId" param="ap.id"/>
											<dsp:getvalueof var="index" param="index"/>
											<dsp:getvalueof var="minimum_order_qty" param="ap.minimum_order_qty"/>
											<dsp:getvalueof var="productPurchasable" param="ap.productPurchasable"/>

											<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
												<dsp:param name="prodId" param="ap.id"/>
												<dsp:oparam name="output">
													<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
												</dsp:oparam>
											</dsp:droplet>

											<li class="item-item">
												<div class="row row-narrow">
													<div class="col-xs-4"><span class="item-thumb">
														<dsp:getvalueof var="thumbnail_img" param="ap.thumbnail_url"/>
															<c:set var="thumbnail_img"
																   value="${empty thumbnail_img ? '/assets/images/plp-placeholder.png' : thumbnail_img}"/>
															<a href="${productUrl}"><cp:readerimg src="${vsg_utils:escapeHtml(thumbnail_img)}" /></a>
												 		</span>
													</div>
													<div class="col-xs-8">
                                     					<span class="item-title">
															<a href="${productUrl}">
																<dsp:valueof param="ap.description" converter="valueishtml"/>
																<%-- <dsp:getvalueof var="descriptionLine2" param="ap.descriptionLine2"/>
																<c:if test="${not empty descriptionLine2}">
																	, <dsp:valueof value="${descriptionLine2}" converter="valueishtml"/>
																</c:if> --%>
															</a>
														</span>

														<div class="row row-narrow">
															<dsp:form id="addToCartHeader${vsg_utils:escapeHtml(prodId)}"
																	  formid="addToCartHeader${vsg_utils:escapeHtml(prodId)}"
																	  action="/checkout/cart.jsp" method="post"
																	  style="display: inline-block;">
																<dsp:input bean="CartModifierFormHandler.addItemCount"
																		   type="hidden" value="1" priority="10"/>

																<div class="col-xs-4">
																	<dsp:input id="qty${vsg_utils:escapeHtml(prodId)}"
																			   bean="CartModifierFormHandler.items[0].quantity"
																			   iclass="form-control text-center" maxlength="5"
																			   type="text" value="${minimum_order_qty}" >
																		<dsp:tagAttribute name="placeholder" value="Qty"/>
																		<dsp:tagAttribute name="data-event-keydown-id" value="catalog58"/>
																	</dsp:input>
																</div>

																<div class="col-xs-8">
																  <c:if test="${productPurchasable}">
																	<c:choose>
																		<c:when test="${isTransient}">
																			<dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
																				<dsp:oparam name="true">
																					<button type="button" class="btn btn-info btn-block pl0 pr0"
																							data-event-click-id="catalog55" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" >
																						Add to Cart
																					</button>
																				</dsp:oparam>
																				<dsp:oparam name="false">
																					<button class="btn btn-info btn-block pl0 pr0" data-dismiss="modal"
																							data-toggle="modal" data-target="#logIn" data-event-click-id="catalog56" >
																						Add to Cart
																					</button>
																				</dsp:oparam>
																			</dsp:droplet>
																		</c:when>
																		<c:when test="${permissionDenied}">
																			<button type="button" class="btn btn-info btn-block pl0 pr0"
																					data-toggle="modal" data-dismiss="modal"
																					data-event-click-id="catalog54"
																					data-target="#permissionDenied">Add to Cart
																			</button>
																		</c:when>
																		<c:otherwise>
																			<button type="button" class="btn btn-info btn-block pl0 pr0"
																					data-event-click-id="catalog55" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" >
																				Add to Cart
																			</button>
																		</c:otherwise>
																	</c:choose>
																 </c:if> 
																</div>
																<div id="minQtyError${prodId}associated" class="red col-xs-12" style="display:none;"></div>

																<dsp:input bean="CartModifierFormHandler.items[0].productId"
																		   type="hidden" value="${prodId}"/>
																<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId"
																		   type="hidden" value="${skuId}"/>
																<dsp:input type="hidden" value="true" priority="-10"
																		   bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
															</dsp:form>
														</div>
													</div>
												</div>
											</li>

										</dsp:oparam>
									</dsp:droplet>
									<button class="btn btn-default btn-link" data-event-click-id="catalog57" >View Less</button>
								</ul>

							</div>
						</div>
						<%--</div>--%>
					</dsp:oparam>
				</dsp:droplet>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>


</dsp:page>