<dsp:page>
	<dsp:getvalueof var="category" param="category" />
	<dsp:getvalueof var="product" param="product" />
	<dsp:getvalueof var="addTitle" param="addTitle" />

	<div class="col-md-8 col-xs-12">
		<ol class="breadcrumb">
			<c:if test="${not empty category}">
				<dsp:include page="breadcrumb-category-entry.jsp">
					<dsp:param name="category" value="${category}" />
					<dsp:param name="isTopCategory" value="true" />
					<dsp:param name="addTitle" value="${addTitle}" />
				</dsp:include>
			</c:if>
			<c:if test="${not empty product}">
				<c:if test="${empty parentCategory}">
					<dsp:getvalueof var="parentCategories" param="product.parentCategories" />
					<c:forEach items="${parentCategories}" var="pCategory" end="1">
						<c:set var="parentCategory" value="${pCategory}" scope="page" />
					</c:forEach>
				</c:if>
				<dsp:include page="breadcrumb-category-entry.jsp">
					<dsp:param name="category" value="${parentCategory}" />
					<dsp:param name="product" value="${product}" />
				</dsp:include>
			</c:if>
			<li class="active">
				<dsp:valueof param="product.displayName"/>
			</li>
		</ol>
	</div>
</dsp:page>