<dsp:page>
	<c:set var="contentCollection" value="/content/Shared/Recently Viewed"/>
	<dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
		<dsp:param name="contentCollection" value="${contentCollection}"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="contentItem" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
			<c:forEach var="renderContent" items="${contentItem.contents}">
				<dsp:renderContentItem contentItem="${renderContent}"/>
			</c:forEach>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>