<dsp:page>
	<dsp:getvalueof var="product" param="product" />
	<dsp:getvalueof var="isOverview" param="isOverview"/>

	<c:choose>
		<c:when test="${isOverview}">
			<article>
				<p>
					<span>This is test overview</span>
					<%--<span>Quos cum tempore consectetur illo expedita reprehenderit explicabo aliquid minima cumque ea, eaque laudantium sequi ratione magnam, neque veritatis quisquam facere eum error fugit praesentium saepe suscipit facilis fugiat. Ab!</span>--%>
					<%--<span>Sint, explicabo corporis et dolorem eveniet ullam delectus iure, earum, dolore necessitatibus at rem incidunt perspiciatis asperiores inventore ipsam quis. Rerum explicabo optio ut excepturi minus laboriosam quasi deserunt velit.</span>--%>
					<%--<span>Magnam illo, quia rem excepturi minima aut recusandae voluptatum nemo corrupti similique ad eligendi nostrum natus tempora laborum possimus libero mollitia nesciunt, dicta quibusdam. Possimus repellat maxime sequi odio impedit!</span>--%>
				</p>
			</article>
		</c:when>
		<c:otherwise>
			<dsp:getvalueof var="skuId" param="product.childSKUs[0].repositoryId"/>
			<div class="col-lg-4">
				<dl class="dl-horizontal">
					<dt>Item #:</dt>
					<dd><dsp:valueof value="${vsg_utils:escapeHtml(skuId)}"/></dd>
				</dl>
				<dl class="dl-horizontal">
					<dt>Short Code:</dt>
					<dd><dsp:valueof value="${vsg_utils:escapeHtml(skuId)}"/></dd>
				</dl>

				<dsp:droplet name="/cps/droplet/AliasNumberLookup">
					<dsp:param name="skuId" param="sku.repositoryId"/>
					<dsp:oparam name="output">
					<dsp:getvalueof var="prodAlias" param="alias"/>
                    <dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
						<dl class="dl-horizontal">
							<dt>Alias:</dt>
							<dd><dsp:valueof value="${alias}"/></dd>
						</dl>
					</dsp:oparam>
				</dsp:droplet>

				<%--<h4>Overview</h4>--%>
				<%--<dl class="dl-horizontal">--%>
					<%--<dt>Attribute:</dt>--%>
					<%--<dd>Value 1</dd>--%>
				<%--</dl>--%>
				<%--<dl class="dl-horizontal">--%>
					<%--<dt>Attribute:</dt>--%>
					<%--<dd>Value 2</dd>--%>
				<%--</dl>--%>
				<%--<dl class="dl-horizontal">--%>
					<%--<dt>Attribute:</dt>--%>
					<%--<dd>Value 3</dd>--%>
				<%--</dl>--%>
				<%--<dl class="dl-horizontal">--%>
					<%--<dt>Attribute:</dt>--%>
					<%--<dd>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt est enim, quia minus numquam--%>
						<%--perferendis aliquam sapiente optio incidunt labore, hic laudantium, tempore dolor consequuntur--%>
						<%--distinctio in suscipit non recusandae.--%>
					<%--</dd>--%>
				<%--</dl>--%>
			</div>
		</c:otherwise>
	</c:choose>

</dsp:page>