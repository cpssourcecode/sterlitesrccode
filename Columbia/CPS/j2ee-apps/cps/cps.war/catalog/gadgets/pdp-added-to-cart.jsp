<dsp:page>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
    <dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CPSLastProductsAddedToCart"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CPSFailedProductsNotAddedToCart"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:getvalueof var="lastProductsAddedToCart" bean="CPSLastProductsAddedToCart"/>

    <h4 class="modal-title" id="modal-added-to-cart-title">Item(s) Added to Cart</h4>

    <dsp:getvalueof var="products" bean="CPSLastProductsAddedToCart.products"/>
    <dsp:getvalueof var="failedProducts" bean="CPSFailedProductsNotAddedToCart.failedProducts"/>
    <dsp:droplet name="ForEach">
        <dsp:param name="array" value="${products}"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="productId" param="key" />
            <dsp:getvalueof var="quantity" param="element" />
            <dsp:droplet name="ProductLookup">
                <dsp:param name="id" value="${productId}"/>
                <dsp:setvalue param="product" paramvalue="element"/>
                <dsp:oparam name="output">
                    <dsp:getvalueof var="displayName" param="product.displayName"/>
                    <dsp:getvalueof var="description" param="product.description"/>
                    <dsp:getvalueof var="vendorItem" param="product.vendorItem"/>
                    <dsp:getvalueof var="detailImg" param="product.web_url"/>
                    <dsp:getvalueof var="defaultImg" value="${staticContentPrefix}/assets/images/pdp-placeholder.png"/>
                    <div class="row modal-product-list-item">
                        <div class="col-lg-6 col-md-5">
                            <div class="media">
                                <div class="media-left">
                                <c:if test="${empty detailImg }">
                                <dsp:getvalueof var="detailImg" value="${defaultImg}" />
                                </c:if>
                                    <cp:readerimg iclass="media-object" style="max-height: 60px; max-width: 60px" src="${detailImg}"  alt="image" />
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading productTitleName">${description}</h4>
                                    <p><strong>Mfr #:</strong> ${vendorItem}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <p><strong>QTY:</strong> ${quantity}</p>
                                </div>
                                <dsp:droplet name="/cps/droplet/CPSPriceDroplet">
                                    <dsp:param name="productId" param="product.id"/>
                                    <c:if test="${not isTransient}">
                                        <dsp:param name="profile" bean="Profile"/>
                                    </c:if>
                                    <dsp:oparam name="outputPrice">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <dsp:valueof param="currentPrice" converter="currencyConversion"/>
                                            <dsp:getvalueof var="currentPrice" param="currentPrice"/>
                                            <dsp:getvalueof var="uomPrimary" param="product.uomPrimary"/>
                                            <c:if test="${not empty uomPrimary}">/<strong>${uomPrimary}</strong></c:if>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <dsp:valueof value="${currentPrice * quantity}" converter="currencyConversion"/>
                                        </div>
                                    </dsp:oparam>
                                </dsp:droplet>
                            </div>
                        </div>
                    </div>
                    <br>
                </dsp:oparam>
            </dsp:droplet>
        </dsp:oparam>
    </dsp:droplet>

    <c:if test="${not empty failedProducts}">
      	<h4 class="modal-title" id="modal-added-to-cart-title" style="color:red">Item(s) Failed </h4>
        <dsp:getvalueof var="defaultImg" value="${staticContentPrefix}/assets/images/pdp-placeholder.png"/>
    	<dsp:droplet name="ForEach">
        <dsp:param name="array" value="${failedProducts}"/>
        <dsp:param name="elementName" value="failedProduct"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="productId" param="failedProduct.productId" />
            <dsp:getvalueof var="quantity" param="failedProduct.productQty" />
            <dsp:getvalueof var="errorMessage" param="failedProduct.errorMessage"/>
            <dsp:droplet name="ProductLookup">
                <dsp:param name="id" value="${productId}"/>
                <dsp:param name="elementName" value="product"/>
                <dsp:oparam name="output">
                    <dsp:getvalueof var="displayName" param="product.displayName"/>
                    <dsp:getvalueof var="description" param="product.description"/>
                    <dsp:getvalueof var="vendorItem" param="product.vendorItem"/>
                    <dsp:getvalueof var="detailImg" param="product.web_url"/>
                    <div class="row modal-product-list-item">
                        <div class="col-lg-6 col-md-5">
                            <div class="media">
                                <div class="media-left">
                                 <c:if test="${empty detailImg }">
                                <dsp:getvalueof var="detailImg" value="${defaultImg}" />
                                </c:if>
                                    <cp:readerimg iclass="media-object" style="max-height: 60px; max-width: 60px"
                                         src="${detailImg}" alt="image" />
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading productTitleName">${description}</h4>
                                    <p><strong>Mfr #:</strong> ${vendorItem}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <p><strong>QTY:</strong> ${quantity}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7">
                            <div class="media">
                               
                                <div class="media-body">
                                    <p><strong>Error Message:</strong> ${errorMessage}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   </dsp:oparam>
                   <dsp:oparam name="empty">
                   	<div class="row modal-product-list-item">
                        <div class="col-lg-6 col-md-5">
                            <div class="media">
                                <div class="media-left">
                                    <img class="media-object" style="max-height: 60px; max-width: 60px" src="${defaultImg}" alt="image">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">${productId}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <p><strong>QTY:</strong> ${quantity}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7">
                            <div class="media">
                               
                                <div class="media-body">
                                    <p><strong>Error Message:</strong> ${errorMessage}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                   </dsp:oparam>
                  </dsp:droplet>
                 </dsp:oparam>
                </dsp:droplet>
               </c:if>
	<p><strong>NOTE : </strong>
	<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
							<dsp:param name="messageKey" value="infoOrderMessage" />
							<dsp:oparam name="output">
								<dsp:valueof param="message" valueishtml="true" />
							</dsp:oparam></dsp:droplet></p><br/>
    <button class="btn btn-primary" data-dismiss="modal">Keep Shopping</button>
    <a href="${originatingRequest.contextPath}/checkout/cart.jsp">
        <button class="btn btn-info">View My Cart</button>
    </a>

    <dsp:getvalueof var="relatedProducts" bean="CPSLastProductsAddedToCart.relatedProducts"/>

    <c:if test="${not empty relatedProducts}">
        <dsp:include page="pdp-related-products.jsp"/>
    </c:if>
    <%--  This is added to trigger beforeGet method in CartModifierFormHandler to clear the CPSFailedProductsNotAddedToCart.failedProducts --%>
    <dsp:form name="clearFailedProductsForm" method="POST" action="/">
        <dsp:input type="hidden" bean="CartModifierFormHandler.giftIds"/>
    </dsp:form>
</dsp:page>