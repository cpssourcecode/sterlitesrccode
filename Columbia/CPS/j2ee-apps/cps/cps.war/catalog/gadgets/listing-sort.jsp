<dsp:page>
	<dsp:getvalueof var="content" param="contentItem"/>
	<span>
		<label>Sort By</label>
	<!-- 	<span class="select-wrap"> -->
			<select id="feat" class="form-control" data-event-change-id="catalog31" >
				<c:forEach items="${content.sortOptions}" var="sortOption">
					<dsp:getvalueof var="url" value="${vsg_utils:getUrlForAction(sortOption)}"/>
					<c:if test="${not empty url}">
						<option value="${url}" ${sortOption.selected ? 'selected':''}>
							<fmt:message key="${sortOption.label}"/>
						</option>
					</c:if>
				</c:forEach>
			</select>
			<!-- <span class="caret caret-select"></span>
		</span> -->
	</span>
	<span>
		<label>Results Per Page</label>
	<!-- 	<span class="select-wrap"> -->
			<select class="form-control" data-event-change-id="catalog32" >
				<c:forEach items="${content.recordsPerPageOptions}" var="rppOption">
					<dsp:getvalueof var="uri" value="${vsg_utils:getUrlForAction(rppOption)}"/>
					<c:if test="${not empty uri}">
						<option value="${uri}" ${content.recsPerPage eq rppOption.recordsPerPage ? 'selected': ''}>
							${vsg_utils:escapeHtml(rppOption.label)}
						</option>
					</c:if>
				</c:forEach>
			</select>
			<!-- <span class="caret caret-select"></span>
		</span> -->
	</span>
</dsp:page>