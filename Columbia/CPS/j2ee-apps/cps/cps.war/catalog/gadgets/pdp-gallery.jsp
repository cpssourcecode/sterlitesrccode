<dsp:page>
	<dsp:getvalueof var="smallImg" param="product.smallImg"/>
	<dsp:getvalueof var="largeImg" param="product.largeImg"/>
	<dsp:getvalueof var="thumbnailImg" param="product.thumbnailImg"/>
	<dsp:getvalueof var="detailImg" param="product.detailImg"/>
	<dsp:getvalueof var="productName" param="product.displayName"/>
	<dsp:getvalueof var="addlImages" vartype="java.lang.Object" param="product.addlImages"/>
	<c:if test="${empty detailImg}">
		<c:set var="detailImg" value="${contextPath}/assets/images/D_COMINGSOON.jpg"/>
	</c:if>

	<c:set var="setThumbDefault" value="loadDefImg(this,'${contextPath}/content_images/T_COMINGSOON.jpg');"/>
	<c:set var="setDetailDefault" value="loadDefImg(this,'${contextPath}/content_images/D_COMINGSOON.jpg');"/>
	<c:set var="setLargeDefault" value="loadDefImg(this,'${contextPath}/content_images/Z_COMINGSOON.jpg');"/>
	<c:set var="setLargeDefaultStage" value="loadDefStageImage('${contextPath}/content_images/Z_COMINGSOON.jpg');"/>

	<div class="col-lg-8">
		<%--<div class="fotorama visible-xs visible-sm" data-autoplay="false" data-transition="fade" data-nav="thumbs" data-clicktransition="fade" data-swipetransition="slide" data-fit="contain" data-loop="true" data-width="100%" data-height="430px" data-minheight="250px" data-thumbheight="80px" data-thumbwidth="80px" data-thumbmargin="5" data-thumbborderwidth="4" data-allowfullscreen="true">--%>
		<div class="fotorama" data-autoplay="false" data-transition="fade" data-nav="thumbs" data-clicktransition="fade" data-swipetransition="slide" data-fit="contain" data-loop="true" data-width="100%" data-height="430px" data-minheight="250px" data-thumbheight="80px" data-thumbwidth="80px" data-thumbmargin="5" data-thumbborderwidth="4" data-allowfullscreen="true">
			<img src="${staticContentPrefix}/content_images/pdp3.jpg">
			<img src="${staticContentPrefix}/content_images/pdp2.png">
			<img src="${staticContentPrefix}/content_images/pdp1.png">
			<img src="${staticContentPrefix}/content_images/pdp4.jpg">
			<img src="${staticContentPrefix}/content_images/pdp5.jpg">
			<img src="${staticContentPrefix}/content_images/pdp6.jpg">
			<img src="${staticContentPrefix}/content_images/pdp7.jpg">
		</div>

		<%--<div class="gallery border-right hidden-xs hidden-sm">--%>
			<%--<div class="image" id="stageImage">--%>
				<%--<a href="${largeImg}" id="mainImage" class="MagicZoomPlus" rel="zoom-position: inner; show-title: bottom; group: ex4;">--%>
					<%--<img id="imagePlus" class="img" src="${largeImg}" onerror="${setLargeDefaultStage}" default="${setLargeDefaultStage}">--%>
				<%--</a>--%>
			<%--</div>--%>
			<%--<div class="image-carousel-wrapper">--%>
				<%--<div id="imageCarousel" class="image-carousel">--%>
					<%--<ul>--%>
						<%--<li>--%>
							<%--<a href="#" data-event-click-id="catalog40" >--%>
								<%--<img id="imgFirst" src="${largeImg}" onerror="${setLargeDefault}" default="${setLargeDefault}"/>--%>
							<%--</a>--%>
						<%--</li>--%>
						<%--<c:forEach var="addlImage" items="${addlImages}" varStatus="status">--%>
							<%--<dsp:setvalue param="addlImage" value="${addlImage}"/>--%>
							<%--<dsp:getvalueof var="addlThumbnailImg" param="addlImage.thumbnailImg"/>--%>
							<%--<dsp:getvalueof var="addlLargeImg" param="addlImage.largeImg"/>--%>
							<%--<dsp:getvalueof var="addlDetailImg" param="addlImage.detailImg"/>--%>
							<%--<dsp:getvalueof var="addlImageCount" value="${status.count}"/>--%>

							<%--<li id="photo${addlImageCount}">--%>
								<%--<a href="#" data-event-click-id="catalog41" data-event-click-param0="${addlImageCount}" >--%>
									<%--<img id="img${addlImageCount}" src="${addlLargeImg}" onerror="${setLargeDefault}" default="${setLargeDefault}"/>--%>
								<%--</a>--%>
							<%--</li>--%>
						<%--</c:forEach>--%>

						<%--&lt;%&ndash;<li><img src="${staticContentPrefix}content_images/pdp3.jpg" alt=""></li>&ndash;%&gt;--%>
						<%--&lt;%&ndash;<li><img src="${staticContentPrefix}content_images/pdp2.png" alt=""></li>&ndash;%&gt;--%>
						<%--&lt;%&ndash;<li><img src="${staticContentPrefix}content_images/pdp1.png" alt=""></li>&ndash;%&gt;--%>
						<%--&lt;%&ndash;<li><img src="${staticContentPrefix}content_images/pdp4.jpg" alt=""></li>&ndash;%&gt;--%>
						<%--&lt;%&ndash;<li><img src="${staticContentPrefix}content_images/pdp5.jpg" alt=""></li>&ndash;%&gt;--%>
						<%--&lt;%&ndash;<li><img src="${staticContentPrefix}content_images/pdp6.jpg" alt=""></li>&ndash;%&gt;--%>
						<%--&lt;%&ndash;<li><img src="${staticContentPrefix}content_images/pdp7.jpg" alt=""></li>&ndash;%&gt;--%>
					<%--</ul>--%>
				<%--</div>--%>
				<%--<a href="#" class="image-carousel-control-prev">&lsaquo;</a>--%>
				<%--<a href="#" class="image-carousel-control-next">&rsaquo;</a>--%>
			<%--</div>--%>
		<%--</div>--%>
	</div>


	<%--<meta itemprop='image' content='${detailImg}'/>--%>
	<%--<div class="pdp-gallery">--%>
		<%--<ul class="pdp-gallery-hero">--%>
			<%--<li><dsp:a href="${largeImg}" id="mainImage" iclass="MagicZoomPlus"><img id="imagePlus" alt="${productName}"--%>
																					 <%--src="${detailImg}"/></dsp:a></li>--%>
		<%--</ul>--%>
		<%--<c:if test="${not empty addlImages}">--%>
			<%--<ul class="pdp-gallery-thumbs">--%>
				<%--<li id="photo0" class="activeSlide"><a href="#" class="activeSlide"--%>
													   <%--data-event-click-id="catalog42" data-event-click-param0="${detailImg}" data-event-click-param1="${largeImg}" ><img--%>
						<%--src="<c:out value="${thumbnailImg}" default="/images/fpo/product-60x60.gif"/>" /></a></li>--%>
				<%--<input type="hidden" name="selectedPhoto" id="selectedPhoto" value="photo0"/>--%>
				<%--<c:forEach var="addlImage" items="${addlImages}">--%>
					<%--<dsp:setvalue param="addlImage" value="${addlImage}"/>--%>
					<%--<dsp:getvalueof param="addlImage.thumbnailImg" var="addlThumbnailImg"/>--%>
					<%--<dsp:getvalueof param="addlImage.largeImg" var="addlLargeImg"/>--%>
					<%--<dsp:getvalueof param="addlImage.detailImg" var="addlDetailImg"/>--%>
					<%--<dsp:getvalueof var="addlImageCount" param="count"/>--%>
					<%--<li id="photo${addlImageCount}"><a href="#"--%>
													   <%--data-event-click-id="catalog43" data-event-click-param0="${addlDetailImg}" data-event-click-param1="${addlLargeImg}" data-event-click-param2="${addlPhotoCount}" ><img--%>
							<%--src="<c:out value="${addlThumbnailImg}" default="/images/fpo/product-60x60.gif"/>" /></a>--%>
					<%--</li>--%>
				<%--</c:forEach>--%>
			<%--</ul>--%>
		<%--</c:if>--%>
	<%--</div>--%>

</dsp:page>