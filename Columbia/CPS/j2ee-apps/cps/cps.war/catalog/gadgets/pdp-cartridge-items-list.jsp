<dsp:page>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
    

    <dsp:getvalueof var="isTitle" param="isTitle"/>
    <dsp:getvalueof var="sectionName" param="sectionName"/>
    <dsp:getvalueof var="sectionType" param="sectionType"/>
	<dsp:getvalueof var="enableCustomerAlsoBought" bean="CPSGlobalProperties.enableCustomerAlsoBought" />
    <dsp:droplet name="/cps/droplet/AccessRightDroplet">
        <dsp:param name="profile" bean="Profile"/>
        <dsp:param name="accessRightKey" value="plp-pdp-cart"/>
        <dsp:oparam name="false">
            <dsp:getvalueof var="permissionDenied" value="true"/>
        </dsp:oparam>
    </dsp:droplet>

    <dsp:getvalueof var="divClass" value="col-md-4 associated-products-container hidden-sm hidden-xs pdp-related"/>
    <dsp:getvalueof var="showContent" value="true"/>
    <c:choose>
        <c:when test="${sectionType eq 'associated'}">
            <dsp:getvalueof var="divId" value="associatedProductsDiv"/>
            <dsp:getvalueof var="divInnerId" value=""/>
            <dsp:getvalueof var="divPanelClass" value="panel-associated-products"/>
        </c:when>
        <c:when test="${sectionType eq 'also-bought'}">
            <dsp:getvalueof var="divId" value="alsoBoughtProductsDiv"/>
            <dsp:getvalueof var="divInnerId" value="alsoBoughtProductsDivInner"/>
            <dsp:getvalueof var="divPanelClass" value="panel-also-bought"/>
            <c:if test="${!isTitle}">
                <dsp:getvalueof var="divClass" value="col-md-4 col-sm-6 col-xs-12"/>
            </c:if>
            <c:if test="${!enableCustomerAlsoBought}">
            <dsp:getvalueof var="showContent" value="false"/>
            </c:if>
        </c:when>
    </c:choose>
<c:if test="${showContent}">
    <div id="${divId}" class="${divClass}">
        <div id="${divInnerId}">
            <div class="panel panel-default ${divPanelClass}">
                <div class="panel-heading box-title-side-bar">${sectionName}</div>
                <div class="panel-body">

                    <ul class="item item-related">
                        <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                            <dsp:param name="array" param="associatedProducts"/>
                            <dsp:param name="elementName" value="ap"/>
                            <dsp:oparam name="output">

                                <dsp:getvalueof var="skuId" param="ap.childSKUs[0].repositoryId"/>
                                <dsp:getvalueof var="prodId" param="ap.id"/>
                                <dsp:getvalueof var="index" param="index"/>
                                <dsp:getvalueof var="minimum_order_qty" param="ap.minimum_order_qty"/>
								<dsp:getvalueof var="productPurchasable" param="ap.productPurchasable"/>
                                <dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
                                    <dsp:param name="prodId" param="ap.id"/>
                                    <dsp:oparam name="output">
                                        <dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
                                    </dsp:oparam>
                                </dsp:droplet>

                                <li class="item-item">
                                    <div class="row row-narrow">
                                        <div class="col-xs-4">
                                            <span class="item-thumb">
                                                <dsp:getvalueof var="thumbnail_img" param="ap.thumbnail_url"/>
												<c:set var="thumbnail_img" value="${empty thumbnail_img ? '/assets/images/plp-placeholder.png' : thumbnail_img}"/>
												<a href="${productUrl}">
                                                    <cp:readerimg src="${vsg_utils:escapeHtml(thumbnail_img)}" />
                                                </a>
											</span>
                                        </div>
                                        <div class="col-xs-8">
                                        	<span class="item-title">
												<a href="${productUrl}">
													<dsp:valueof param="ap.description" converter="valueishtml"/>
													<%-- <dsp:getvalueof var="descriptionLine2" param="ap.descriptionLine2"/>
													<c:if test="${not empty descriptionLine2}">
													, <dsp:valueof value="${descriptionLine2}" converter="valueishtml"/>
													</c:if> --%>
												</a>
											</span>
                                            <div class="row row-narrow">
                                                <dsp:form id="addToCartHeader${vsg_utils:escapeHtml(prodId)}"
                                                          formid="addToCartHeader${vsg_utils:escapeHtml(prodId)}"
                                                          action="/checkout/cart.jsp" method="post"
                                                          style="display: inline-block;">
                                                    <dsp:input bean="CartModifierFormHandler.addItemCount"
                                                               type="hidden" value="1" priority="10"/>

                                                    <div class="col-xs-4">
                                                        <dsp:input id="qty${vsg_utils:escapeHtml(prodId)}"
                                                                   bean="CartModifierFormHandler.items[0].quantity"
                                                                   iclass="form-control text-center"
                                                                   maxlength="5"
                                                                   type="text" value="${minimum_order_qty}">
                                                            <dsp:tagAttribute name="placeholder" value="Qty"/>
                                                            <dsp:tagAttribute name="data-event-keydown-id" value="catalog38"/>
                                                        </dsp:input>
                                                    </div>

                                                    <div class="col-xs-8">
                                                    <c:if test="${productPurchasable}">
                                                        <c:choose>
                                                            <c:when test="${isTransient}">
                                                                <dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
                                                                    <dsp:oparam name="true">
                                                                        <button type="button" class="btn btn-info btn-block pl0 pr0 addToCartButton"
                                                                                data-event-click-id="catalog34" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" data-event-click-param1="${sectionType}" >
                                                                            Add to Cart
                                                                        </button>
                                                                    </dsp:oparam>
                                                                    <dsp:oparam name="false">
                                                                        <button class="btn btn-info btn-block pl0 pr0 addToCartButton" data-dismiss="modal"
                                                                                data-toggle="modal" data-target="#logIn" data-event-click-id="catalog35" >
                                                                            Add to Cart
                                                                        </button>
                                                                    </dsp:oparam>
                                                                </dsp:droplet>
                                                            </c:when>
                                                            <c:when test="${permissionDenied}">
                                                                <button type="button" class="btn btn-info btn-block pl0 pr0 addToCartButton"
                                                                        data-toggle="modal" data-dismiss="modal"
                                                                        data-event-click-id="catalog33"
                                                                        data-target="#permissionDenied">Add to Cart
                                                                </button>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <button type="button" class="btn btn-info btn-block pl0 pr0 addToCartButton"
                                                                        data-event-click-id="catalog34" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" data-event-click-param1="${sectionType}" >
                                                                    Add to Cart
                                                                </button>
                                                            </c:otherwise>
                                                        </c:choose>
													 </c:if>
                                                    </div>
                                                    <div id="minQtyError${prodId}${sectionType}" class="red col-xs-12" style="display:none;"></div>
                                                    <dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden" value="${prodId}"/>
                                                    <dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" value="${skuId}"/>
                                                    <dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
                                                </dsp:form>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </dsp:oparam>
                        </dsp:droplet>

                        <c:choose>
                            <c:when test="${sectionType eq 'associated'}">
                                <button class="btn btn-default btn-link" data-event-click-id="catalog36" >View More</button>
                            </c:when>
                            <c:when test="${sectionType eq 'also-bought'}">
                                <button class="btn btn-default btn-link" data-event-click-id="catalog37" >View More</button>
                            </c:when>
                        </c:choose>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</c:if>
</dsp:page>