<dsp:page>
	<dsp:getvalueof var="record" param="record"/>
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" param="attributes" />
		<dsp:param name="elementName" value="attributeValue" />
		<dsp:oparam name="outputStart">
			<table style="width: 100%;" class="table table-responsive">
			<tbody>
		</dsp:oparam>
		<dsp:oparam name="output">
			<dsp:getvalueof var="attributeKey" param="key"/>
			<dsp:getvalueof var="attributesMap" param="record.attributes"/>
			<dsp:getvalueof var="attributesValue" value="${attributesMap[attributeKey]}"/>
			<c:if test="${!empty attributesValue}">
				<tr>
					<td style="width: 50%;"><dsp:valueof param="attributeValue"/></td>
					<td style="width: 50%;"><dsp:valueof value="${vsg_utils:escapeHtml(attributesValue)}"/></td>
				</tr>
			</c:if>
		</dsp:oparam>
		<dsp:oparam name="outputEnd">
			</tbody>
			</table>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>