<dsp:page>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
<!-- 	<div class="container preload-cover loader-acitive"> -->
<!-- 		<div class="loading"> -->
<!-- 			<i class="fa fa-spinner fa-spin"></i> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/commerce/catalog/comparison/ProductListHandler"/>
	<dsp:importbean bean="/cps/droplet/GetPriceFromMapDroplet"/>
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>

	<param name="norelogin" id="norelogin" value="true"/>
	<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem"/>
	<dsp:getvalueof var="itemPrices" vartype="Map" param="contentItem.itemPrices"/>

    <dsp:form id="addToCompareForm" formid="addToCompareForm" method="POST">
		<%--<dsp:input type="hidden" bean="CPSSessionBean.comparisonReferer" id="comparisonReferer"/>--%>
        <dsp:input type="hidden" bean="ProductListHandler.productID" id="addCompareProdId"/>
        <dsp:input type="hidden" bean="ProductListHandler.categoryID" id="addCompareCatId" disabled="true"/>
        <dsp:input type="hidden" bean="ProductListHandler.addProduct" value="submit" priority="-10"/>
    </dsp:form>

    <dsp:form id="removeFromCompareForm" formid="removeFromCompareForm" method="POST">
        <dsp:input type="hidden" bean="ProductListHandler.productID" id="removeCompareProdId"/>
        <dsp:input type="hidden" bean="ProductListHandler.categoryID" id="removeCompareCatId"  disabled="true"/>
        <dsp:input type="hidden" bean="ProductListHandler.removeProduct" value="submit" priority="-10"/>
    </dsp:form>

    <dsp:form id="clearFromCompareForm" formid="clearFromCompareForm" method="POST">
        <dsp:input type="hidden" bean="ProductListHandler.clearList" value="submit" priority="-10"/>
    </dsp:form>

	<c:if test="${content.totalNumRecs != 0 || empty content.ZeroResultsContent }">

		<div class="plp-controls">
			<form class="form-inline plp-left">
				<dsp:include src="${contextPath}/catalog/gadgets/listing-sort.jsp"/>
			</form>
			<span class="plp-right hidden-xs">
				<nav class="plp-paging">
					<dsp:include src="listing-pagination.jsp">
						<dsp:param name="start" value="${content.firstRecNum-1}"/>
						<dsp:param name="howMany" value="${content.recsPerPage}"/>
						<dsp:param name="size" value="${content.totalNumRecs}"/>
						<dsp:param name="pagingActionTemplate" value="${content.pagingActionTemplate}"/>
						<dsp:param name="content" param="contentItem"/>
					</dsp:include>
				</nav>
				<ul class="plp-view hidden-sm">
					<li><a class="plp-view-list" data-event-click-id="catalog28" ><i class="fa fa-th-list mr5"></i> List</a></li>
					<li><a class="plp-view-grid" data-event-click-id="catalog29" ><i class="fa fa-th-large mr5"></i> Grid</a></li>
				</ul>
			</span>
			<ul class="plp-compare">
				<li id="compareItemsCount"></li>
				<li><a href="#" data-event-click-id="catalog30" >Clear List</a></li>
			</ul>
		</div>
		
	</c:if>

	<fmt:bundle basename="web.resources.WebAppResources">
		<c:choose>
			<c:when test="${content.totalNumRecs == 0 and not empty content.ZeroResultsContent }">
				<dsp:renderContentItem contentItem="${content.ZeroResultsContent}"/>
				<input type="hidden" id="isZeroResults" value="true"/>
			</c:when>
			<c:otherwise>
				<ul class="item item-plp row row-narrow">
					<c:forEach items="${content.records}" var="record" varStatus="status">
						<dsp:getvalueof var="productId" value="${record.attributes['product.repositoryId']}"/>
						<dsp:getvalueof var="productUrl" value="${fn:toLowerCase(record.attributes['product.seoUrl'])}"/>
						
						<dsp:droplet name="GetPriceFromMapDroplet">
							<dsp:param name="productId" value="${productId}"/>
							<dsp:param name="itemPrices" value="${itemPrices}"/>
							<dsp:oparam name="outputPrice">
								<dsp:getvalueof var="priceValue" param="itemPrice"/>
							</dsp:oparam>
						</dsp:droplet>
						<dsp:getvalueof var="lazyLoadViewPortImages" bean="CPSGlobalProperties.lazyLoadViewPortImages"/>
						 <c:choose>
                            <c:when test="${status.count <= lazyLoadViewPortImages}">
                                <dsp:getvalueof var="lazyLoadImagesEnabled" value="false"/>                        
                            </c:when>    
                            <c:otherwise>
                                <dsp:getvalueof var="lazyLoadImagesEnabled" value="true"/>
                            </c:otherwise>
                        </c:choose>
						<dsp:include src="${contentPath}/catalog/gadgets/product-tile.jsp">
							<dsp:param name="id" value="${productId}"/>
							<dsp:param name="skuId" value="${record.attributes['sku.repositoryId']}"/>
							<dsp:param name="displayName" value="${record.attributes['product.displayName']}"/>
							<dsp:param name="image" value="${record.attributes['product.thumbnailImage']}"/>
							<dsp:param name="seoLink" value="${productUrl}"/>
							<dsp:param name="multiSku" value="${fn:length(record.attributes['sku.repositoryId']) > 1}"/>
							<dsp:param name="attributes" value="${attributes}"/>
							<dsp:param name="record" value="${record}"/>
							<dsp:param name="priceValue" value="${priceValue}"/>
                            <dsp:param name="lazyLoadImagesEnabled" value="${lazyLoadImagesEnabled}"/>
						</dsp:include>
						
					</c:forEach>
				</ul>
			</c:otherwise>
		</c:choose>
	</fmt:bundle>

	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>

	<dsp:form id="addToCartFromModal" formid="addToCartFromModal" action="" method="post">
		<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10"/>
		<dsp:input id="qtyFromModal" bean="CartModifierFormHandler.items[0].quantity" type="hidden" value="1"/>
		<dsp:input id="prodIdFromModal" bean="CartModifierFormHandler.items[0].productId" type="hidden" value=""/>
		<dsp:input id="skuIdFromModal" bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" value=""/>
		<dsp:input id="shippingMethod" bean="CartModifierFormHandler.shippingMethodOnAvailabilityModal" type="hidden" value=""/>
		<dsp:input id="locationId" bean="CartModifierFormHandler.selectedLocationId" type="hidden" value=""/>
		<dsp:input id="fromAvailability" bean="CartModifierFormHandler.fromAvailabilityModal" type="hidden" value="true"/>
		<dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
	</dsp:form>

	<c:if test="${content.totalNumRecs != 0 || empty content.ZeroResultsContent }">
		<div class="text-center">
			<nav class="plp-paging">
				<dsp:include src="listing-pagination.jsp">
					<dsp:param name="start" value="${content.firstRecNum-1}"/>
					<dsp:param name="howMany" value="${content.recsPerPage}"/>
					<dsp:param name="size" value="${content.totalNumRecs}"/>
					<dsp:param name="pagingActionTemplate" value="${content.pagingActionTemplate}"/>
					<dsp:param name="content" param="contentItem"/>
				</dsp:include>
			</nav>
		</div>
	</c:if>

	<dsp:importbean bean="/cps/util/CPSSessionBean" />
	<dsp:getvalueof var="isAvailablePricesWebService" bean="CPSSessionBean.availablePricesWebService" />
	<input type="hidden" name="isAvailablePricesWebService" id="isAvailablePricesWebService" value="${isAvailablePricesWebService}"/>

	<dsp:include page="/global/modals/compare-number-less.jsp"/>
	<dsp:include page="/global/modals/compare-number-bigger.jsp"/>


</dsp:page>
