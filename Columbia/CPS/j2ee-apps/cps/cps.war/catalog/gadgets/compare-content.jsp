<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<dsp:page>
    <dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/atg/dynamo/droplet/BeanProperty"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/cps/droplet/AliasNumberLookupFromIndex"/>
    <dsp:getvalueof var="defaultImg" value="${staticContentPrefix}/assets/images/pdp-placeholder.png"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient" />

    <dsp:importbean bean="/atg/commerce/catalog/comparison/ProductListHandler"/>
    <table class="table table-compare" id="table-compare-products">
        <dsp:droplet name="ForEach">
            <dsp:param bean="ProductList.tableColumns" name="array"/>
            <dsp:param name="elementName" value="column"/>
            <dsp:oparam name="output">
                <dsp:getvalueof var="column" param="column"/>
                <tr>
                    <td>
                        <dsp:valueof param="column.heading"/>
                    </td>
                    <dsp:droplet name="ForEach">
                        <dsp:param bean="ProductList.items" name="array"/>
                        <dsp:param name="elementName" value="item"/>
                        <dsp:oparam name="output">
                            <dsp:getvalueof var="item" param="item"/>

                            <c:choose>
                                <c:when test="${column.name eq ''}">
                                    <dsp:getvalueof var="productID" param="item.product.id"/>
                                    <dsp:getvalueof var="skuId" param="item.product.repositoryId"/>
                                    <dsp:getvalueof var="categoryId" param="item.category.repositoryId"/>
                                    <dsp:getvalueof var="minimum_order_qty" param="item.product.minimum_order_qty"/>
                                    <%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String" value="${originatingRequest.contextPath}/catalog/pdp.jsp?prodId=${skuId}" /> --%>
                                    <dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
                                        <dsp:param name="prodId" value="${productID}"/>
                                        <dsp:oparam name="output">
                                            <dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
                                        </dsp:oparam>
                                    </dsp:droplet>
                                    <dsp:getvalueof var="prod_img" param="item.product.web_url"/>
                                    <td id="remove-button">
                                        <div class="item-item">
                                            <a class="item-remove-product"
                                               data-event-click-id="catalog1" data-event-click-param0="${productID}" data-event-click-param1="${categoryId}" ><strong
                                                    class="item-x">X</strong> Remove</a>
                                            <a href="${productUrl}" class="item-thumb">
                                            	<dsp:getvalueof var="lowerProd_img" value="${fn:toLowerCase(prod_img)}"/>
												<c:choose>
													<c:when test="${fn:startsWith(lowerProd_img,'http')}">
														<dsp:getvalueof var="prod_img" value="${prod_img}"/>
													</c:when>
													<c:otherwise>
														<dsp:getvalueof var="prod_img" value="${staticContentPrefix}${prod_img}"/>
													</c:otherwise>
												</c:choose>	
                                                <img class="img-responsive" data-src="<c:out value="${vsg_utils:escapeHtml(prod_img)}" default="${defaultImg}"/>"
                                                     data-fallback-image="${defaultImg}" >
                                            </a>
                                            <span class="item-order">
												<dsp:form id="addToCart${vsg_utils:escapeHtml(productID)}"
													formid="addToCart${vsg_utils:escapeHtml(productID)}"
													action="" method="post">
													<dsp:input bean="CartModifierFormHandler.addItemCount"
														type="hidden" value="1" priority="10">
														<dsp:tagAttribute name="placeholder" value="Qty" />
													</dsp:input>
													<dsp:input id="qty${vsg_utils:escapeHtml(productID)}"
														bean="CartModifierFormHandler.items[0].quantity"
														iclass="form-control text-center item-qty" 
														type="text" value="${minimum_order_qty}" maxlength="5">
														<dsp:tagAttribute name="data-event-keydown-id" value="catalog3" />
													</dsp:input>
													<dsp:droplet name="/cps/droplet/AccessRightDroplet">
														<dsp:param name="profile" bean="Profile" />
														<dsp:param name="accessRightKey" value="plp-pdp-cart" />
														<dsp:oparam name="false">
															<dsp:getvalueof var="permissionDenied" value="true" />
														</dsp:oparam>
													</dsp:droplet>

													<c:choose>
														<c:when test="${isTransient}">
															<dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
																<dsp:oparam name="true">
																	<button type="button"
																		class="btn btn-default addToCartButton"
																		data-event-click-id="catalog2"
																		data-event-click-param0="${vsg_utils:escapeJS(productID)}">
																		<fmt:message key="listing.product.tile.addToCart" />
																	</button>
																</dsp:oparam>
																<dsp:oparam name="false">
																	<a href="#" class="btn btn-default"
																		data-productId="${productID}" data-skuId="${skuId}"
																		data-toggle="modal" data-target="#logIn">
																		<fmt:message key="listing.product.tile.addToCart" />
																	</a>
																</dsp:oparam>
															</dsp:droplet>
														</c:when>
														<%-- SM-532 Finance account - Issues --%>
														<c:when test="${permissionDenied}">
			                                                        <a href="#" class="btn btn-default"
				                                                        data-productId="${productID}" data-skuId="${skuId}"
				                                                        data-toggle="modal" data-target="#permissionDenied">
				                                                        <fmt:message key="listing.product.tile.addToCart" />
			                                                        </a>
                                                        </c:when>
														<c:otherwise>
															<button type="button" class="btn btn-default addToCartButton"
																data-event-click-id="catalog2"
																data-event-click-param0="${vsg_utils:escapeJS(productID)}">
																<fmt:message key="listing.product.tile.addToCart" />
															</button>
														</c:otherwise>
													</c:choose>
													<dsp:input bean="CartModifierFormHandler.items[0].productId"
														type="hidden" value="${productID}" />
													<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId"
														type="hidden" value="${skuId}" />
													<dsp:input type="hidden" value="true" priority="-10"
														bean="CartModifierFormHandler.addMultipleItemsToOrder" />

												</dsp:form>
											</span>
                                        </div>
                                        <div id="minQtyError${productID}" class="red" style="display:none;"></div>
                                    </td>
                                </c:when>

                                <c:when test="${column.name eq 'Price'}">
                                    <td id="price_<dsp:valueof param="item.product.id"/>"></td>
                                </c:when>

                                <c:otherwise>
                                    <td id="properties">
                                        <dsp:droplet name="BeanProperty">
                                            <dsp:param param="item" name="bean"/>
                                            <dsp:param param="column.property" name="propertyName"/>
                                            <dsp:oparam name="output">
                                                <dsp:getvalueof var="propvalue" param="propertyValue"/>
                                                <c:catch var="exception">
                                                    <c:choose>
                                                        <c:when test="${column.name eq 'Individual Weight' || column.name eq 'Description' || column.name eq 'Item Name'}">
                                                            ${propvalue}
                                                        </c:when>
                                                        <c:when test="${propvalue eq 'true'}">
                                                            Yes
                                                        </c:when>
                                                        <c:when test="${propvalue eq 'false'}">
                                                            No
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:forEach items="${propvalue}" var="entry">
                                                                <c:if test="${column.name eq entry.key}">
                                                                    <c:if test="${not empty entry.value}">
                                                                        ${entry.value}
                                                                    </c:if>
                                                                </c:if>
                                                            </c:forEach>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:catch>
                                            </dsp:oparam>
                                        </dsp:droplet>
                                    </td>
                                </c:otherwise>
                            </c:choose>
                        </dsp:oparam>
                    </dsp:droplet>
                </tr>
            </dsp:oparam>
        </dsp:droplet>
        <tr>
            <td>Availability</td>
            <dsp:droplet name="ForEach">
                <dsp:param bean="ProductList.items" name="array"/>
                <dsp:param name="elementName" value="item"/>
                <dsp:oparam name="output">
                    <td id="avlbt_<dsp:valueof param="item.product.id"/>"></td>
                </dsp:oparam>
            </dsp:droplet>
        </tr>
    </table>
</dsp:page>