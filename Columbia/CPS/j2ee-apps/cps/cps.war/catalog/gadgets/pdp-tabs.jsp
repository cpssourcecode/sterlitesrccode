<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:getvalueof var="ftdesc" param="product.fulltech_description"/>
	<dsp:getvalueof var="longdesc" param="product.long_desc1"/>
	<dsp:getvalueof var="desc" param="product.description"/>
	<dsp:getvalueof var="featured" param="product.item_features"/>
	<dsp:getvalueof var="pdfUrl" param="product.pdf_url"/>
	<dsp:getvalueof var="specificationSheetUrl" param="product.specification_sheet_url"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>
	<dsp:getvalueof var="productId" param="product.id"/>
	<dsp:getvalueof var="showDocuments" value="${!empty pdfUrl || !empty specificationSheetUrl}"/>

    <c:set var="showAssociated" value="${false}"/>
    <c:set var="contentCollection" value="/content/Shared/Pdp/Associated Products"/>
    <dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
        <dsp:param name="contentCollection" value="${contentCollection}"/>
        <dsp:oparam name="output">
            <dsp:getvalueof var="contents" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem.contents"/>
            <c:if test="${fn:length(contents) > 0}">
                <dsp:getvalueof var="apConfig" vartype="com.endeca.infront.assembler.ContentItem" value="${contents[0]}"/>
                <dsp:droplet name="/cps/droplet/AssociatedProductsDroplet">
                    <dsp:param name="product" param="product"/>
                    <dsp:param name="productsNumber" value="${apConfig.productsNumber}"/>
                    <dsp:oparam name="output">
                        <c:set var="showAssociated" value="${true}"/>
                    </dsp:oparam>
                </dsp:droplet>
            </c:if>
        </dsp:oparam>
    </dsp:droplet>

	<dsp:droplet name="/cps/droplet/CustomerAlsoBoughtProductsDroplet">
		<dsp:param name="product" param="product"/>
		<dsp:param name="productsNumber" value="3"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="apArray" param="associatedProducts"/>
			<c:if test="${showAssociated && fn:length(apArray) > 1}">
				<c:set var="featuresStyle" value="col-md-8 col-sm-6 "/>
			</c:if>
		</dsp:oparam>
	</dsp:droplet>

	<%-- <dsp:getvalueof var="desc2" param="product.descriptionLine2"/> --%>
	<dsp:droplet name="/cps/droplet/SpecificationTabDroplet">
		<dsp:param name="product" param="product"/>
		<dsp:param name="hideTabTest" value="testme"/>
		<dsp:oparam name="outputTabHeader">
			<dsp:getvalueof var="showSpecs" value="true"/>
		</dsp:oparam>
	</dsp:droplet>
	<section class="pdp-details hidden-sm hidden-xs">
		<div class="row">
			<div id="featuresSectionDetailsDiv" class="col-xs-12 features-section-pdp-details ${featuresStyle}">
				<c:if test="${not empty featured}">
				<hr class="gold">
					<h4>Features</h4>
					<%@include file="/catalog/gadgets/pdp-tabs-overview.jspf" %>
				</c:if>
				<c:if test="${showSpecs eq 'true'}">
					<hr class="gold">
					<h4>Specifications</h4>
					<%@include file="/catalog/gadgets/pdp-tabs-specs.jspf" %>
				</c:if>
			</div>

            <c:if test="${showAssociated}">
                <c:set var="contentCollection" value="/content/Shared/Pdp/Customer Also Bought Products"/>
                <dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
                    <dsp:param name="contentCollection" value="${contentCollection}"/>
                    <dsp:oparam name="output">
                        <dsp:getvalueof var="contents" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem.contents"/>
                        <c:if test="${fn:length(contents) > 0}">
                            <dsp:getvalueof var="apConfig" vartype="com.endeca.infront.assembler.ContentItem" value="${contents[0]}"/>
                            <dsp:droplet name="/cps/droplet/CustomerAlsoBoughtProductsDroplet">
                                <dsp:param name="product" param="product"/>
                                <dsp:param name="productsNumber" value="${apConfig.productsNumber}"/>
                                <dsp:oparam name="output">

                                    <dsp:include page="/catalog/gadgets/pdp-cartridge-items-list.jsp">
                                        <dsp:param name="isTitle" value="${false}"/>
                                        <dsp:param name="sectionType" value="also-bought"/>
                                        <dsp:param name="sectionName" value="${apConfig.name}"/>
                                        <dsp:param name="associatedProducts" param="associatedProducts"/>
                                    </dsp:include>

                                </dsp:oparam>
                            </dsp:droplet>
                        </c:if>
                    </dsp:oparam>
                </dsp:droplet>
            </c:if>

		</div>
	</section>

	<section class="pdp-details-mobile visible-sm visible-xs">
		<div class="expand">

			<c:if test="${not empty featured}">
				<div class="expand-item">
					<span class="expand-label">Features<span
							class="expand-control"><i class="fa fa-angle-down"></i><i
							class="fa fa-angle-right"></i></span></span>
					<div class="expand-content" style="display: none;">
						<%@include file="/catalog/gadgets/pdp-tabs-overview.jspf" %>
					</div>
				</div>
			</c:if>
			<c:if test="${showSpecs eq 'true'}">
				<div class="expand-item">
					<span class="expand-label">Specifications<span
							class="expand-control"><i class="fa fa-angle-down"></i><i
							class="fa fa-angle-right"></i></span></span>
					<div class="expand-content" style="display: none;">
						<%@include file="/catalog/gadgets/pdp-tabs-specs.jspf" %>
					</div>
				</div>
			</c:if>

			<c:if test="${showDocuments}">
				<div class="expand-item">
					<span class="expand-label">Documents
						<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span>
					</span>

					<div class="expand-content" style="display: none;">
						<ul class="pdp-docs">
							<c:if test="${!empty specificationSheetUrl}">
								<li>
									<a href="${specificationSheetUrl}" target="_blank"><i class="icon icon-paper-page"></i>Specification Sheet</a>
								</li>
							</c:if>
							<c:if test="${!empty pdfUrl}">
								<li>
									<a href="${pdfUrl}" target="_blank"><i class="icon icon-paper-page"></i>Brochure/Catalog</a>
								</li>
							</c:if>
						</ul>
					</div>
				</div>
			</c:if>

			<c:set var="contentCollection" value="/content/Shared/Pdp/Associated Products"/>
			<dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
				<dsp:param name="contentCollection" value="${contentCollection}"/>
				<dsp:oparam name="output">
					<dsp:getvalueof var="contents" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem.contents"/>
					<c:if test="${fn:length(contents) > 0}">
						<dsp:getvalueof var="apConfig" vartype="com.endeca.infront.assembler.ContentItem" value="${contents[0]}"/>
						<dsp:droplet name="/cps/droplet/AssociatedProductsDroplet">
							<dsp:param name="product" param="product"/>
							<dsp:param name="productsNumber" value="${apConfig.productsNumber}"/>
							<dsp:oparam name="output">
								<div class="expand-item">
								<span class="expand-label">${apConfig.name}<span
										class="expand-control"><i class="fa fa-angle-down"></i><i
										class="fa fa-angle-right"></i></span></span>
									<div class="expand-content p0" style="display: none;">
										<ul class="item item-related">
											<dsp:droplet name="/atg/dynamo/droplet/ForEach">
												<dsp:param name="array" param="associatedProducts"/>
												<dsp:param name="elementName" value="ap"/>
												<dsp:oparam name="output">
													<dsp:getvalueof var="skuId" param="ap.childSKUs[0].repositoryId"/>
													<dsp:getvalueof var="id" param="ap.id"/>
													<dsp:getvalueof var="minimum_order_qty" param="ap.minimum_order_qty"/>
													<dsp:getvalueof var="productPurchasable" param="ap.productPurchasable"/>
													<dsp:getvalueof var="stockingType" param="ap.stockingType"/>
													<dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType)}"/>
													<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
														<dsp:param name="prodId" param="ap.id"/>
														<dsp:oparam name="output">
															<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
														</dsp:oparam>
													</dsp:droplet>
													<dsp:form id="addToCart${vsg_utils:escapeHtml(id)}" formid="addToCart${vsg_utils:escapeHtml(id)}"
															  action="" method="post">
														<li class="item-item">
															<div class="row row-narrow">
																<div class="col-xs-2">
																<span class="item-thumb">
																	<dsp:getvalueof var="thumbnail_img" param="ap.thumbnail_url"/>
																	<c:set var="thumbnail_img"
																		   value="${empty thumbnail_img ? '/assets/images/plp-placeholder.png' : thumbnail_img}"/>
																	<a href="${productUrl}"><cp:readerimg src="${vsg_utils:escapeHtml(thumbnail_img)}" /></a>
																</span>
																</div>
																<div class="col-xs-5">
																<span class="item-title">
																	<a href="${productUrl}">
																		<dsp:valueof param="ap.description" converter="valueishtml"/>
																		<%-- <dsp:getvalueof var="descriptionLine2" param="ap.descriptionLine2"/>
																		<c:if test="${not empty descriptionLine2}">
																			, <dsp:valueof value="${descriptionLine2}" converter="valueishtml"/>
																		</c:if> --%>
																	</a>
																</span>
																</div>
																<div class="col-xs-5 item-order text-right">
																	<div class="input-group">
																		<!-- <input type="text" class="form-control text-center" value="1"> -->
																		<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1"
																				   priority="10">
																			<dsp:tagAttribute name="placeholder" value="Qty"/>
																		</dsp:input>
																		<dsp:input id="qty${vsg_utils:escapeHtml(id)}"
																				   bean="CartModifierFormHandler.items[0].quantity"
																				   iclass="form-control text-center" type="text"
																				   value="${minimum_order_qty}"
																				   maxlength="5" >
																			<dsp:tagAttribute name="data-event-keydown-id" value="catalog47"/>
																		</dsp:input>
																		<c:if test="${productPurchasable && !isProductPurchasableStockingType}">
																		<c:choose>
																			<c:when test="${not isTransient}">
																				<div class="input-group-btn">
																					<button class="btn btn-default addToCartButton"
																							data-event-click-id="catalog46" data-event-click-param0="${vsg_utils:escapeJS(id)}" >
																						Add<span class="hidden-xs"> to Cart</span>
																					</button>
																				</div>
																			</c:when>
																			<c:otherwise>
																				<dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
																					<dsp:oparam name="true">
																						<div class="input-group-btn">
																							<button class="btn btn-default addToCartButton"
																									data-event-click-id="catalog46" data-event-click-param0="${vsg_utils:escapeJS(id)}" >
																								Add<span class="hidden-xs"> to Cart</span>
																							</button>
																						</div>
																					</dsp:oparam>
																					<dsp:oparam name="false">
																						<div class="input-group-btn">
																							<a class="btn btn-default"
																							   data-productId="${id}"
																							   data-skuId="${skuId}"
																							   data-toggle="modal" data-target="#logIn">
																								Add<span class="hidden-xs"> to Cart</span>
																							</a>
																						</div>
																					</dsp:oparam>
																				</dsp:droplet>
																			</c:otherwise>
																		</c:choose>
																	</c:if>
																	</div>
																</div>
															</div>
														</li>

														<dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden" value="${id}"/>
														<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden"
																   value="${skuId}"/>
														<dsp:input type="hidden" value="true" priority="-10"
																   bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
													</dsp:form>
												</dsp:oparam>
											</dsp:droplet>
										</ul>
									</div>
								</div>


							</dsp:oparam>
						</dsp:droplet>
					</c:if>
				</dsp:oparam>
			</dsp:droplet>


		</div>
	</section>

</dsp:page>
