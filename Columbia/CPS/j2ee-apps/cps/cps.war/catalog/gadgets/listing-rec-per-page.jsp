<dsp:page>
	<dsp:getvalueof var="content" param="contentItem"/>
	<dsp:getvalueof var="Nrpp" param="Nrpp"/>
	<c:set var="currRecsPerPage" value="${content.recsPerPage}"/>
	<dsp:getvalueof var="recSize" value="${fn:length(content.records)}"/>
	<dsp:getvalueof var="totalNumRecs" value="${content.totalNumRecs}"/>


	<fmt:bundle basename="web.resources.WebAppResources">
		<div class="count">
				<span class="text-block">
					Showing Results:
					<strong>${vsg_utils:escapeHtml(recSize)}</strong>
					of
					<strong>${vsg_utils:escapeHtml(content.totalNumRecs)}</strong>
				</span>

			<c:if test="${totalNumRecs >= 10}">
				<span class="text-block hidden-print">
					Results Per Page:
					<c:forEach items="${content.recordsPerPageOptions}" var="rppOption">
						<dsp:getvalueof var="uri" value="${vsg_utils:getUrlForAction(rppOption)}"/>

						<c:if test="${('20' == rppOption.label )|| ('30' == rppOption.label )}">
							&nbsp;|&nbsp;
						</c:if>

						<c:choose>
							<c:when test="${Nrpp == rppOption.label || (empty Nrpp && '20' == rppOption.label )}">
								<b> <a href="#" id="view10" data-event-click-id="catalog26" data-event-click-param0="${uri}" >${vsg_utils:escapeHtml(rppOption.label)}</a> </b>
							</c:when>
							<c:otherwise>
								<a href="#" id="view10" data-event-click-id="catalog27" data-event-click-param0="${uri}" >${vsg_utils:escapeHtml(rppOption.label)}</a>
							</c:otherwise>
						</c:choose>

					</c:forEach>
				</span>
			</c:if>
		</div>

	</fmt:bundle>

</dsp:page>