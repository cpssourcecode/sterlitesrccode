<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:getvalueof var="prodId" param="product.id"/>
	<dsp:getvalueof var="productId" param="product.id"/>
	<dsp:getvalueof var="skuId" param="product.childSKUs[0].repositoryId"/>
	<dsp:getvalueof var="ftdesc" param="product.fulltech_description"/>
	<dsp:getvalueof var="longdesc" param="product.long_desc1"/>
	<dsp:getvalueof var="productDescription" param="product.description"/>
	<dsp:getvalueof var="productName" param="product.product_name"/>
	<dsp:getvalueof var="altText" param="product.altText"/>
	<dsp:getvalueof var="parentCategory" param="product.parentCategory"/>
	<c:if test="${parentCategory == null}">
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" param="product.parentCategories" />
			<dsp:param name="elementName" value="category"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="index" param="index"/>
				<c:if test="${index eq 0}">
					<dsp:getvalueof var="eCommerceDisplay" param="category.eCommerceDisplay"/>
				</c:if>
			</dsp:oparam>
		</dsp:droplet>
	</c:if>
	<c:if test="${parentCategory != null}">
		<dsp:getvalueof var="eCommerceDisplay" param="product.parentCategory.eCommerceDisplay"/>
	</c:if>
	<dsp:getvalueof var="stockingType" param="product.stockingType"/>
	<dsp:getvalueof var="productDisabled"
					value="${stockingType eq 'O' || stockingType eq 'U' || stockingType eq 'K' || stockingType eq 'X' || not eCommerceDisplay}"/>
	<dsp:getvalueof var="productPurchasable" param="product.productPurchasable"/>
	<dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType)}"/>
	<!-- Check Availability -->
	<div id="modal-check-availability-div"></div>

	<span class="visible-xs pdp-title h1title" itemprop="name">
		<dsp:valueof param="product.description" converter="valueishtml"/>
	</span>


	<div class="row">
		<div class="col-md-12 hidden-xs">
			<h1 class="pdp-title">
				<dsp:valueof param="product.description" converter="valueishtml"/>
			</h1>
		</div>
	</div>
	
	<hr class="gold">
	<c:if test="${productDisabled}">
		<div class="col-md-12 alert-danger alert-no-available">
			<div class="alert-icon">
				<i class="fa fa-exclamation-triangle red" style="position: static"></i>
				<strong style="color: #333333;">
				<c:choose>
					<c:when test="${stockingType eq 'K'}">
						<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
							<dsp:param name="messageKey" value="errCallSalesTeam" />
							<dsp:oparam name="output">
								<dsp:valueof param="message" valueishtml="true" />
							</dsp:oparam>
							<dsp:oparam name="empty">
								<dsp:valueof param="message" valueishtml="true" />
							</dsp:oparam>
						</dsp:droplet>
					</c:when>
					<c:otherwise>
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="errProductNoLongerAvailable"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include>
						<!-- This product is no longer available -->
					</c:otherwise>					
				</c:choose>
				</strong>
			</div>
		</div>
	</c:if>
	<c:if test="${!productDisabled && (!productPurchasable || isProductPurchasableStockingType)}">
		<div class="col-md-12 alert-danger alert-no-available">
			<div class="alert-icon">
				<i class="fa fa-exclamation-triangle red" style="position: static"></i>
				<strong style="color: #333333;">
				
						<dsp:include page="/includes/gadgets/info-message.jsp">
							<dsp:param name="key" value="errProductNotPurchasable"/>
							<dsp:param name="notWrap" value="true"/>
						</dsp:include>
						<!-- This product is not purchasable online -->
				</strong>
			</div>
		</div>
	</c:if>
	<div class="row pdp-title-container">
		<div class="col-md-4 col-sm-6 col-xs-12 pdp-gallery">

			<dsp:getvalueof var="detailImg" param="product.web_url"/>
			<dsp:getvalueof var="lowerDetailImg" value="${fn:toLowerCase(detailImg)}"/>
				<c:choose>
					<c:when test="${fn:startsWith(lowerDetailImg,'http')}">
						<dsp:getvalueof var="detailImg" value="${detailImg}"/>
					</c:when>
					<c:otherwise>
						<c:if test="${not empty detailImg}">
							<dsp:getvalueof var="detailImg" value="${staticContentPrefix}${detailImg}"/>
						</c:if>
					</c:otherwise>
				</c:choose>	
			<dsp:getvalueof var="defaultImg" value="${staticContentPrefix}/assets/images/pdp-placeholder.png"/>

			<ul class="  cycle-slideshow" data-cycle-timeout=0
				data-cycle-slides="> li" data-cycle-pager=".pdp-gallery-thumbs"
				data-cycle-pager-template="">
				<li><a href="#"><img class="pdpThumbnailImage" alt="${vsg_utils:escapeHtml(altText)}"
						itemprop="image" data-src="<c:out value="${vsg_utils:escapeHtml(detailImg)}" default="${defaultImg}"/>"
						data-fallback-image="${defaultImg}" ></a></li>
			</ul>

			<div class="pdp-gallery-thumbs row row-narrow">
				<div class="col-xs-4">
					<img class="pdp-page-small-image" alt="${vsg_utils:escapeHtml(altText)}" itemprop="image" data-src="<c:out value="${vsg_utils:escapeHtml(detailImg)}" default="${defaultImg}"/>"
						 data-fallback-image="${defaultImg}" >
				</div>
			</div>
			 <p class="pdp-disclaimer">
				<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
					<dsp:param name="messageKey" value="pdpDisclaimerMessage" />
					<dsp:oparam name="output">
						<dsp:valueof param="message" valueishtml="true" />
					</dsp:oparam>
					<dsp:oparam name="empty">
						<dsp:valueof param="message" valueishtml="true" />
					</dsp:oparam>
				</dsp:droplet>
			</p>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 pdp-intro">


			<ul class="pdp-skus">

				<dsp:getvalueof var="vendorItem" param="product.vendorItem"/>
				<li><strong>Mfr #:</strong> ${vsg_utils:escapeHtml(vendorItem)}</li>

				<dsp:getvalueof var="displayName" param="product.displayName"/>
				<li><strong>Item #:</strong> ${vsg_utils:escapeHtml(displayName)}</li>

				<dsp:getvalueof var="pricesvc_upc" param="product.pricesvc_upc"/>
				<c:if test="${not empty pricesvc_upc}">
					<li><strong>UPC:</strong> ${vsg_utils:escapeHtml(pricesvc_upc)}</li>
				</c:if>

				<dsp:getvalueof var="brand_name" param="product.brand_name"/>
				<li><strong>Brand:</strong> ${vsg_utils:escapeHtml(brand_name)}</li>
				<br/>
				<dsp:getvalueof var="minimum_order_qty" param="product.minimum_order_qty"/>
				<li><strong>Min Order Qty:</strong> ${vsg_utils:escapeHtml(minimum_order_qty)}</li>

				<dsp:getvalueof var="order_qty_interval" param="product.order_qty_interval"/>
				<li><strong>Qty Interval:</strong> ${vsg_utils:escapeHtml(order_qty_interval)}</li>

				<dsp:droplet name="/cps/droplet/CustomerAliasDroplet">
					<dsp:param name="product" param="product"/>
					<dsp:param name="profile" bean="Profile"/>
					<dsp:oparam name="outputAlias">
					<dsp:getvalueof var="prodAlias" param="customerAlias"/>
                    <dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
						<li><strong>Customer Alias:</strong>&nbsp;<dsp:valueof value="${alias}"/></li>
					</dsp:oparam>
				</dsp:droplet>


			</ul>
			<dsp:getvalueof var="message" param="product.substituteBrandMessage.message"/>
			<c:if test="${ not empty message}">
				<i class="fa fa-exclamation-triangle gold"></i>
				<strong style="color: #333333;">
						${message}
				</strong>
			</c:if>


			<hr class="gold">

			<!-- <p>Apollo Valves 83-200 Carbon Steel Lever Handle 3-Piece Full-Port In-Line Standard Ball Valve, 1/2 IN, Socket Weld</p> -->
			<!-- long_description -->
			<div id="cover-spin"></div>
			<p>
				<dsp:valueof param="product.long_desc1" converter="valueishtml"/>
			</p>

			<dsp:droplet name="/cps/droplet/CPSProductSpecificMessagesDroplet">
                <dsp:param name="product" param="product"/>
                <dsp:param name="type" value="all"/>
				<dsp:oparam name="output">

                        <dsp:droplet name="/atg/dynamo/droplet/ForEach">
                            <dsp:param name="array" param="messages"/>
                            <dsp:param name="elementName" value="item"/>
                            <dsp:oparam name="output">
                                <dsp:getvalueof var="message" param="item.message"/>
                                <c:if test="${ not empty message }">
                                    <p>
                                        <i class="fa fa-exclamation-triangle gold" aria-hidden="true"></i>
                                        <span >
                                                ${message}
                                        </span>
                                    </p>
                                </c:if>
                            </dsp:oparam>
                            <dsp:oparam name="outputStart">
                                <div class="well well-info-orange">
                            </dsp:oparam>
                            <dsp:oparam name="outputEnd">
                                </div>
                            </dsp:oparam>
                        </dsp:droplet>




				</dsp:oparam>
			</dsp:droplet>


			<dsp:droplet name="/cps/droplet/AccessRightDroplet">
				<dsp:param name="profile" bean="Profile"/>
				<dsp:param name="accessRightKey" value="plp-pdp-cart"/>
				<dsp:oparam name="false">
					<dsp:getvalueof var="permissionDenied" value="true"/>
				</dsp:oparam>
			</dsp:droplet>

			<div class="pdp-order">
				<c:if test="${!productDisabled}">
					<c:if test="${productPurchasable && !isProductPurchasableStockingType}">
					<dsp:droplet name="/cps/droplet/CPSPriceDroplet">
						<dsp:param name="productId" param="product.id"/>
						<c:if test="${not isTransient}">
							<dsp:param name="profile" bean="Profile"/>
						</c:if>
						<dsp:oparam name="outputPrice">
							<div class="pdp-price text-center">
								<dsp:valueof param="currentPrice" converter="currencyConversion"/>
								<dsp:getvalueof var="uomPrimary" param="product.uomPrimary"/>
								<c:if test="${not empty uomPrimary}">/${uomPrimary}</c:if>
							</div>

							<dsp:getvalueof var="pdpToolsStyle" value="margin-top: 10px;"/>
							<dsp:form id="addToCart${vsg_utils:escapeHtml(prodId)}" formid="addToCart${vsg_utils:escapeHtml(prodId)}"
									  action="/checkout/cart.jsp" method="post" iclass="input-group input-group-lg" style="margin-bottom: 5px;">
								<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10"/>
								<dsp:input id="qty${vsg_utils:escapeHtml(prodId)}" bean="CartModifierFormHandler.items[0].quantity"
										   iclass="form-control text-center qty${vsg_utils:escapeHtml(prodId)}"
										   maxlength="5" type="text" value="${minimum_order_qty}">
									<dsp:tagAttribute name="placeholder" value="Qty"/>
									<dsp:tagAttribute name="data-event-keydown-id" value="catalog64"/>
								</dsp:input>
								<div class="input-group-btn">
									<c:choose>
										<c:when test="${isTransient}">
											<dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
												<dsp:oparam name="true">
													<button type="button" class="btn btn-primary addToCartButton"
															data-event-click-id="catalog60" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" >Add to
														Cart
													</button>
												</dsp:oparam>
												<dsp:oparam name="false">
													<button type="button" class="btn btn-primary addToCartButton"
															data-productId="${prodId}"
															data-skuId="${skuId}"
															data-toggle="modal" data-target="#logIn">Add to Cart
													</button>
												</dsp:oparam>
											</dsp:droplet>
										</c:when>
										<c:when test="${permissionDenied}">
											<button type="button" class="btn btn-primary addToCartButton" data-toggle="modal" data-target="#permissionDenied">Add
												to Cart
											</button>
										</c:when>
										<c:otherwise>
											<button type="button" class="btn btn-primary addToCartButton"
													data-event-click-id="catalog60" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" >Add to
												Cart
											</button>
										</c:otherwise>
									</c:choose>
								</div>
								<dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden" value="${prodId}"/>
								<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" value="${skuId}"/>
								<dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
							</dsp:form>
						</dsp:oparam>
					</dsp:droplet>
					</c:if>
					<div id="minQtyError${prodId}" class="red" style="display:none;"></div>

					<ul class="pdp-tools mt-10" style="${pdpToolsStyle}">
						<c:choose>
							<c:when test="${not isTransient}">
								<c:choose>
									<c:when test="${permissionDenied}">
										<li>
											<a href="#myModalCheck" data-toggle="modal" data-target="#permissionDenied">
												<i class="fa fa-check"></i>Check Availablity
											</a>
										</li>
										<li>
											<a href="#" data-toggle="modal" data-target="#permissionDenied"><i class="icon icon-paper-list"></i>Add to
												Material List</a>
										</li>
									</c:when>
									<c:otherwise>
										<li>
											<a id="checkAv" href="#myModalCheck" data-toggle="modal"
											   data-event-click-id="catalog61" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" >
												<i class="fa fa-check"></i>Check Availablity
											</a>
										</li>
										<li>
											<a id="addToML" href="#"
											   data-event-click-id="catalog62" data-event-click-param0="${vsg_utils:escapeJS(prodId)}" 
											   			data-event-click-param1="${vsg_utils:escapeJS(skuId)}" 
											   			data-event-click-param2="false"><i
													class="icon icon-paper-list"></i>Add to Material List</a>
										</li>
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<li>
									<a href="#myModalCheck" data-toggle="modal" data-target="#logIn">
										<i class="fa fa-check"></i>Check Availablity
									</a>
								</li>
								<li>
									<a href="#" data-toggle="modal" data-target="#logIn" data-event-click-id="catalog63" ><i
											class="icon icon-paper-list"></i>Add to Material List</a>
								</li>
							</c:otherwise>
						</c:choose>
					</ul>
				</c:if>
				<dsp:getvalueof var="specificationSheetUrl" param="product.specification_sheet_url"/>
				<dsp:getvalueof var="pdfUrl" param="product.pdf_url"/>
				<ul class="pdp-tools mt-10">
					<c:if test="${not empty specificationSheetUrl}">
						<li><a href="${specificationSheetUrl}" target="_blank"><i class="icon icon-paper-list"></i>Specification Sheet</a></li>
					</c:if>
					<c:if test="${not empty pdfUrl}">
						<li><a href="${pdfUrl}" target="_blank"><i class="icon icon-paper-list"></i>Brochure/Catalog</a></li>
					</c:if>
				</ul>
			</div>
			
			<dsp:form id="addToCartFromModal" formid="addToCartFromModal" action="" method="post">
				<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10"/>
				<dsp:input id="qtyFromModal" bean="CartModifierFormHandler.items[0].quantity" type="hidden" value="1"/>
				<dsp:input id="prodIdFromModal" bean="CartModifierFormHandler.items[0].productId" type="hidden" value=""/>
				<dsp:input id="skuIdFromModal" bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" value=""/>
				<dsp:input id="shippingMethod" bean="CartModifierFormHandler.shippingMethodOnAvailabilityModal" type="hidden" value=""/>
				<dsp:input id="locationId" bean="CartModifierFormHandler.selectedLocationId" type="hidden" value=""/>
				<dsp:input id="fromAvailability" bean="CartModifierFormHandler.fromAvailabilityModal" type="hidden" value="true"/>
				<dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
			</dsp:form>
		</div>

        <c:set var="showAssociated" value="${false}"/>
		<c:set var="contentCollection" value="/content/Shared/Pdp/Associated Products"/>
		<dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
			<dsp:param name="contentCollection" value="${contentCollection}"/>
			<dsp:oparam name="output">
				<dsp:getvalueof var="contents" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem.contents"/>
				<c:if test="${fn:length(contents) > 0}">
					<dsp:getvalueof var="apConfig" vartype="com.endeca.infront.assembler.ContentItem" value="${contents[0]}"/>
					<dsp:droplet name="/cps/droplet/AssociatedProductsDroplet">
						<dsp:param name="product" param="product"/>
						<dsp:param name="productsNumber" value="${apConfig.productsNumber}"/>
						<dsp:oparam name="output">
                            <c:set var="showAssociated" value="${true}"/>
                            <c:set var="sectionType" value="associated"/>
                            <c:set var="sectionName" value="${apConfig.name}"/>
                            <dsp:getvalueof var="apArray" param="associatedProducts"/>
						</dsp:oparam>
					</dsp:droplet>
				</c:if>
			</dsp:oparam>
		</dsp:droplet>

        <c:if test="${!showAssociated}">
            <c:set var="contentCollection" value="/content/Shared/Pdp/Customer Also Bought Products"/>
            <dsp:droplet name="/atg/endeca/assembler/droplet/InvokeAssembler">
                <dsp:param name="contentCollection" value="${contentCollection}"/>
                <dsp:oparam name="output">
                    <dsp:getvalueof var="contents" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem.contents"/>
                    <c:if test="${fn:length(contents) > 0}">
                        <dsp:getvalueof var="apConfig" vartype="com.endeca.infront.assembler.ContentItem" value="${contents[0]}"/>
                        <dsp:droplet name="/cps/droplet/CustomerAlsoBoughtProductsDroplet">
                            <dsp:param name="product" param="product"/>
                            <dsp:param name="productsNumber" value="${apConfig.productsNumber}"/>
                            <dsp:oparam name="output">
                                <c:set var="sectionType" value="also-bought"/>
                                <c:set var="sectionName" value="${apConfig.name}"/>
                                <dsp:getvalueof var="apArray" param="associatedProducts"/>
                            </dsp:oparam>
                        </dsp:droplet>
                    </c:if>
                </dsp:oparam>
            </dsp:droplet>
        </c:if>

        <c:if test="${!empty sectionType}">
            <dsp:include page="/catalog/gadgets/pdp-cartridge-items-list.jsp">
				<dsp:param name="isTitle" value="${true}"/>
                <dsp:param name="sectionType" value="${sectionType}"/>
                <dsp:param name="sectionName" value="${sectionName}"/>
                <dsp:param name="associatedProducts" value="${apArray}"/>
            </dsp:include>
        </c:if>

	</div>

	<script type="text/javascript" nonce="${requestScope.nonce}">
		$(document).ready(function () {
			addTitleAndDescription('${vsg_utils:escapeJS(productName)}', '${vsg_utils:escapeJS(productDescription)}');
		});

		function setPanelsHeight() {
					if ($('.panel-associated-products').length && $('.panel-also-bought').length) {
						$('#featuresSectionDetailsDiv').addClass("col-md-8");
						$('#featuresSectionDetailsDiv').addClass("col-sm-6");
						var associatedProductsOffset = $('.panel-associated-products').offset().top + $('.panel-associated-products').outerHeight();
						var PanelAlsoBoughtOffset = $('.panel-also-bought').offset().top;
						if (associatedProductsOffset >= PanelAlsoBoughtOffset) {
							$('#alsoBoughtProductsDivInner').css('margin-top', (associatedProductsOffset - PanelAlsoBoughtOffset) + 45);
				} else {
					$('#alsoBoughtProductsDivInner').css('margin-top', (PanelAlsoBoughtOffset - associatedProductsOffset) + 45);
						}
					} else {
						$('.pdp-title-container').css('min-height', $('.panel-associated-products').outerHeight());
					}
		}

		var assotiatedItemsCount = 4;
		//    sidebar		
		function reloadAssociated() {
			assotiatedItemsCount = $('#associatedProductsDiv').find('.item-item').leght || 4;

			if ($(window).outerWidth() > 992) {
				$('#associatedProductsDiv').load("/catalog/gadgets/pdp-title-associated.jsp?productId=${productId}", function () {
					$('#associatedProductsDiv').addClass("absolute");
                    eventsPdpTitleAssociated();
					setPanelsHeight();
				});
			}
		}

		function reloadLessAssociated(event) {
			var items = $('#associatedProductsDiv').find('.item-item');
			items.slice(assotiatedItemsCount, items.lenght).remove();
			
			$(event).text("View More");
			$(event).attr("onclick", "reloadAssociated();");
			
			setPanelsHeight();
		}
	
		var alsoItemsCount = 4;
		//    sidebar
		function reloadAlsoBought() {
			alsoItemsCount = $('#alsoBoughtProductsDivInner').find('.item-item').leght || 4;
			$('#alsoBoughtProductsDivInner').load("/catalog/gadgets/pdp-title-also-bought.jsp?productId=${productId}", function () {
                eventsPdpTitleAlsoBought();
            });
		}

		function reloadLessAlsoBought(event) {
			var items = $('#alsoBoughtProductsDivInner').find('.item-item');
			items.slice(alsoItemsCount, items.lenght).remove();
			
			$(event).text("View More");
			$(event).attr("onclick", "reloadAlsoBought();");
		}

	</script>


</dsp:page>