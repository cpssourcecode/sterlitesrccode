<dsp:page>
	<article>
		<h3>Documents & Downloads</h3>
		<hr>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-link"><i class="fa fa-file-pdf-o text-danger fa-2x"></i> <span>Document Download</span> <i class="text-muted">(Adobe Acrobat)</i></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-link"><i class="fa fa-file-excel-o text-danger fa-2x"></i> <span>Document Download</span> <i class="text-muted">(Microsoft Exel)</i></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-link"><i class="fa fa-file-word-o text-danger fa-2x"></i> <span>Document Download</span> <i class="text-muted">(Microsoft Word)</i></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-link"><i class="fa fa-file-zip-o text-danger fa-2x"></i> <span>Document Download</span> <i class="text-muted">(Archive)</i></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<a href="#" class="btn btn-link"><i class="fa fa-file-o text-danger fa-2x"></i> <span>Document Download</span> <i class="text-muted">(Any File)</i></a>
			</div>
		</div>
	</article>
</dsp:page>