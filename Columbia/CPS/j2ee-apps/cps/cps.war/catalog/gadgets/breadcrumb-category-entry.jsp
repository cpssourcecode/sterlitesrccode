<dsp:page>
	<dsp:importbean bean="/atg/commerce/endeca/cache/DimensionValueCacheDroplet"/>
	<dsp:getvalueof var="category" param="category"/>
	<dsp:getvalueof var="product" param="product"/>
	<dsp:setvalue param="product" value="${null}"/>
	<dsp:getvalueof var="addTitle" param="addTitle"/>
	<dsp:setvalue param="addTitle" value="${null}"/>
	<dsp:getvalueof var="isTopCategory" param="isTopCategory"/>
	<dsp:setvalue param="isTopCategory" value="${null}"/>

	<c:if test="${not empty category}">
		<dsp:getvalueof var="parentCategory" param="category.parentCategory" scope="page"/>
		<c:if test="${empty parentCategory}">
			<dsp:getvalueof var="parentCategories" param="category.fixedParentCategories"/>
			<c:if test="${not empty parentCategories}">
				<c:forEach items="${parentCategories}" var="pCategory" end="1">
					<c:set var="parentCategory" value="${pCategory}" scope="page"/>
				</c:forEach>
			</c:if>
		</c:if>

		<dsp:getvalueof var="categoryId" param="category.id"/>
		<c:choose>
			<c:when test="${categoryId == 'ZZZ' or categoryId == 'zzz'}">
				<li>
					<span><a href="#">Home</a></span>
				</li>
				<li>
					<span><a href="#">Search results</a></span>
				</li>
			</c:when>
			<c:otherwise>
				<c:if test="${not empty parentCategory}">
					<dsp:include page="breadcrumb-category-entry.jsp">
						<dsp:param name="category" value="${parentCategory}"/>
					</dsp:include>
				</c:if>

				<dsp:droplet name="DimensionValueCacheDroplet">
					<dsp:param name="repositoryId" param="category.id"/>
					<dsp:oparam name="output">
						<dsp:getvalueof var="url" param="dimensionValueCacheEntry.url"/>
						<dsp:getvalueof var="categoryLink" value="${url}"/>
					</dsp:oparam>
				</dsp:droplet>

				<c:choose>
					<c:when test="${empty isTopCategory or !empty product or !empty addTitle}">
						<li>
							<dsp:a page="${categoryLink}">
								<span><dsp:valueof param="category.displayName"/></span>
							</dsp:a>
						</li>
						<c:choose>
							<c:when test="${!empty addTitle}">
								<li>
									<span><a href="#"><c:out value="${vsg_utils:escapeHtml(addTitle)}"/></a></span>
								</li>
							</c:when>
						</c:choose>
					</c:when>
					<c:otherwise>
						<li>
							<dsp:a page="${categoryLink}">
								<span><dsp:valueof param="category.displayName"/></span>
							</dsp:a>
						</li>
					</c:otherwise>
				</c:choose>

			</c:otherwise>
		</c:choose>


	</c:if>
</dsp:page>