<dsp:page>
	<%-- Display records listing pagination. Params:
		size - total items count,
		start - index of first item on current page,
		howMany - count items per page,
		pagingActionTemplate - paging action template with placeholders for offset and record per page --%>

	<dsp:getvalueof var="start" param="start"/>
	<dsp:getvalueof var="count" param="howMany"/>
	<dsp:getvalueof var="allCount" param="size"/>
	<dsp:getvalueof var="pagingTemplate" param="pagingActionTemplate"/>
	<dsp:getvalueof var="content" param="contentItem"/>
	<%--<dsp:getvalueof var="length" param="pagingLength" />--%>

	<fmt:bundle basename="web.resources.WebAppResources">

		<c:set var="defCount" value="16"/>
		<c:set var="count" value="${empty count ? defCount : count}"/>
		<c:set var="pageNum" value="${vsg_utils:currentPage(start, count)}"/>
		<c:set var="pagesCount" value="${vsg_utils:numberPages(allCount, count)}"/>
		<c:if test="${pageNum > pagesCount}">
			<c:set var="pageNum" value="${pagesCount}"></c:set>
		</c:if>


		<dsp:getvalueof var="pageRadius" param="pageRadius"/>
		<dsp:getvalueof var="farPage" param="farPage"/>
		<dsp:getvalueof var="sPageRadius" param="sPageRadius"/>
		<c:set var="pageRadius"
			   value="${empty pageRadius ? 1 : pageRadius}"/><%-- display 'pageRadius' pages before and 'pageRadius' after current --%>
		<c:set var="farPage"
			   value="${empty farPage ? 97 : farPage}"/><%--use smaller radius for pages after 'farPage' --%>
		<c:set var="sPageRadius"
			   value="${empty sPageRadius ? 1 : sPageRadius}"/><%--pageRadius for page gt 'farPage' --%>
		<c:set var="radius" value="${pageNum > farPage ? spageRadius : pageRadius}"/>
		<c:set var="length" value="5"/>

		<ul class="pagination">
			<c:choose>
				<c:when test="${start + count <= allCount}">
					<li><span>${start + 1}-${start + count} of ${allCount}</span></li>
				</c:when>
				<c:otherwise>
					<li><span>${start + 1}-${allCount} of ${allCount}</span></li>
				</c:otherwise>
			</c:choose>
			<c:choose>
				<%-- If page count <= lenght --%>
				<c:when test="${pagesCount <= length}">
					<c:choose>
						<c:when test="${1 == pageNum}">
							<li class="prev disabled">
								<a aria-label="Previous" data-event-click-id="catalog8"  href="#">
									<span class="fa fa-angle-left"></span>
								</a>
							</li>
						</c:when>
						<c:when test="${1 > pageNum}">
							<li class="prev">
								<a aria-label="Previous" data-event-click-id="catalog9"  href="#">
									<span class="fa fa-angle-left"></span>
								</a>
							</li>
						</c:when>
						<c:otherwise>
							<c:set var="uri"
								   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,pageNum-1,count,defCount))}"/>
							<li class="prev">
								<a aria-label="Previous" data-event-click-id="catalog10" data-event-click-param0="${uri}"  href="#">
									<span class="fa fa-angle-left"></span>
								</a>
							</li>
						</c:otherwise>
					</c:choose>

					<c:set var="beginInd" value="1"/>
					<c:set var="endInd" value="${pagesCount}"/>

					<c:forEach var="num" begin="${beginInd}" end="${endInd}">
						<c:choose>
							<c:when test="${num eq pageNum}">
								<li class="active"><a href="#" data-event-click-id="catalog11" >${num}</a></li>
							</c:when>
							<c:otherwise>
								<c:set var="uri"
									   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,num,count,defCount))}"/>
								<li><a href="#" data-event-click-id="catalog12" data-event-click-param0="${uri}" >${num}</a></li>
							</c:otherwise>
						</c:choose>
					</c:forEach>

					<c:choose>
						<c:when test="${pagesCount == pageNum}">
							<li class="next disabled">
								<a href="#" aria-label="Next" data-event-click-id="catalog13" >
									<span class="fa fa-angle-right"></span>
								</a>
							</li>
						</c:when>
						<c:when test="${pagesCount < pageNum}">
							<li class="next">
								<a href="#" aria-label="Next" data-event-click-id="catalog14" >
									<span class="fa fa-angle-right"></span>
								</a>
							</li>
						</c:when>
						<c:otherwise>
							<c:set var="uri"
								   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,pageNum+1,count,defCount))}"/>
							<li class="next">
								<a aria-label="Next" href="#" data-event-click-id="catalog15" data-event-click-param0="${uri}" >
									<span class="fa fa-angle-right"></span>
								</a>
							</li>
						</c:otherwise>
					</c:choose>
				</c:when>
				<%-- If page count > lenght --%>
				<c:otherwise>
					<c:if test="${1 < pagesCount }">
						<c:choose>
							<c:when test="${1 == pageNum}">
								<li class="prev disabled">
									<a href="#" data-event-click-id="catalog16"  aria-label="Previous">
										<span class="fa fa-angle-left"></span>
									</a>
								</li>
							</c:when>
							<c:when test="${1 > pageNum}">
								<li class="prev">
									<a href="#" data-event-click-id="catalog17"  aria-label="Previous">
										<span class="fa fa-angle-left"></span>
									</a>
								</li>
							</c:when>
							<c:otherwise>
								<c:set var="uri"
									   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,pageNum-1,count,defCount))}"/>
								<li class="prev">
									<a aria-label="Previous" href="#" data-event-click-id="catalog18" data-event-click-param0="${uri}" >
										<span class="fa fa-angle-left"></span>
									</a>
								</li>
							</c:otherwise>
						</c:choose>

						<%--pagination variant #2 --%>
						<c:if test="${pageNum > 1+radius + 1}">
							<c:set var="uri"
								   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,1,count,defCount))}"/>
							<li><a href="#" data-event-click-id="catalog19" data-event-click-param0="${uri}" >1</a></li>
						</c:if>
						<c:if test="${pageNum > 2 + radius}">
							<li>
								<span>…</span>
							</li>
						</c:if>


						<c:choose>
							<c:when test="${pageNum < 4}">
								<c:set var="beginInd" value="1"/>
								<c:set var="endInd" value="${pagesCount < length ? pagesCount : length}"/>
							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${pagesCount - pageNum < 3}">
										<c:set var="beginInd" value="${pagesCount - 4}"/>
										<c:set var="endInd" value="${pagesCount}"/>
									</c:when>
									<c:otherwise>
										<c:set var="beginInd"
											   value="${(pageNum - radius) > 1 ? (pageNum - radius) : 1}"/>
										<c:set var="endInd"
											   value="${(pageNum + radius) < pagesCount ? (pageNum + radius) : pagesCount}"/>
									</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>


						<c:forEach var="num" begin="${beginInd}" end="${endInd}">
							<c:choose>
								<c:when test="${num eq pageNum}">
									<li class="active"><a href="#" data-event-click-id="catalog20" >${num}</a></li>
								</c:when>
								<c:otherwise>
									<c:set var="uri"
										   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,num,count,defCount))}"/>
									<li><a href="#" data-event-click-id="catalog21" data-event-click-param0="${uri}" >${num}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>

						<c:if test="${(pageNum < pagesCount-radius-1)}">
							<li>
								<span>…</span>
							</li>
						</c:if>
						<c:if test="${pageNum < pagesCount-radius-1}">
							<c:set var="uri"
								   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,pagesCount,count,defCount))}"/>
							<li><a href="#" data-event-click-id="catalog22" data-event-click-param0="${uri}" >${vsg_utils:escapeHtml(pagesCount)}</a></li>
						</c:if>

						<c:choose>
							<c:when test="${pagesCount == pageNum}">
								<li class="next disabled">
									<a href="#" aria-label="Next" data-event-click-id="catalog23" >
										<span class="fa fa-angle-right"></span>
									</a>
								</li>
							</c:when>
							<c:when test="${pagesCount < pageNum}">
								<li class="next">
									<a href="#" aria-label="Next" data-event-click-id="catalog24" >
										<span class="fa fa-angle-right"></span>
									</a>
								</li>
							</c:when>
							<c:otherwise>
								<c:set var="uri"
									   value="${vsg_utils:getUrlForAction(vsg_utils:paginationAction(pagingTemplate,pageNum+1,count,defCount))}"/>
								<li class="next">
									<a href="#" aria-label="Next" data-event-click-id="catalog25" data-event-click-param0="${uri}"  rel="next">
										<span class="fa fa-angle-right"></span>
									</a>
								</li>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:otherwise>
			</c:choose>
		</ul>
	</fmt:bundle>
</dsp:page>