<dsp:page>
    <dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList"/>
    <dsp:getvalueof var="items" bean="ProductList.items"/>
    <c:choose>
        <c:when test="${items.size() < 2}">
            <a href data-toggle="modal" data-target="#illegalCompareItemNumber">Compare (${items.size()}) Items</a>
        </c:when>
        <c:when test="${items.size() > 3}">
            <a href data-toggle="modal" data-target="#biggerCompareItemNumber">Compare (${items.size()}) Items</a>
        </c:when>
        <c:otherwise>
            <a href="#" data-event-click-id="catalog71">Compare (${items.size()}) Items</a>
        </c:otherwise>
    </c:choose>
    <input type="hidden" id="compare-item-number" value="${items.size()}"/>
</dsp:page>