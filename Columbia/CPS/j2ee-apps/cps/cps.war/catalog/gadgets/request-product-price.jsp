<dsp:page>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:getvalueof var="prodId" param="s"/>
    <dsp:droplet name="/cps/droplet/CPSPriceDroplet">
        <dsp:param name="productId" param="s"/>
        <dsp:param name="profile" bean="Profile"/>
        <dsp:oparam name="outputPrice">
            <dsp:valueof param="currentPrice" converter="currencyConversion"/>
        </dsp:oparam>
    </dsp:droplet>
</dsp:page>