<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>

	<dsp:getvalueof var="transient" bean="Profile.transient"/>

	<div class="pdp-order">
		<c:if test="${isTransient}">
			<!-- <div class="pdp-order-price pdp-order-section"> <span class="pdp-order-price-label">Net Price</span>
			  <p>$123.40</p>
			</div>

			            <div class="pdp-order-price pdp-order-section">
			                <div class="header">
			                    <span class="header-qty">Qty</span>
			                    <span class="header-price">Your Price</span>
			                </div>
			                <dl class="pdp-order-price-tiers">
			                    <dt>1-9</dt>
			                    <dd>$250.00</dd>
			                    <dt>10-19</dt>
			                    <dd>$245.00</dd>
			                    <dt>20-49</dt>
			                    <dd>$225.00</dd>
			                    <dt>50 +</dt>
			                    <dd>$205.00</dd>
			                    <dd class="clear">&nbsp;</dd>
			                </dl>
			            </div>

			  <div class="pdp-order-stock pdp-order-section">
			    <p>Usually ships within 48 hours</p>
			  </div> -->
		</c:if>

		<div class="well well-pdp">
			<dsp:getvalueof var="prodId" param="product.repositoryId"/>
			<dsp:getvalueof var="productPurchasable" param="product.productPurchasable"/>
			<dsp:form id="addToCart${prodId}" formid="addToCart${prodId}" action="" method="post" iclass="form-horizontal row">
				<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10"/>

				<!-- <div class="col-lg-12">
					<div class="main-price">
						<p><strong>123<sup>99</sup></strong> USD</p>
					</div>
				</div> -->
				<div class="col-lg-12">
					<div class="form-group">
						<div class="col-sm-12 text-center">
							<div class="spinbox spinbox-pdp" data-initialize="spinbox" data-min="1" id="mySpinbox">
								<button type="button" class="btn btn-default spinbox-down">
									<i class="fa fa-minus-circle"></i>
									<span class="sr-only">Decrease</span>
								</button>
								<dsp:input bean="CartModifierFormHandler.items[0].quantity" iclass="form-control input-lg spinbox-input" type="text" value="1"/>
								<%--<input type="text" value="1" class="form-control input-lg spinbox-input">--%>
								<button type="button" class="btn btn-default spinbox-up">
									<i class="fa fa-plus-circle"></i>
									<span class="sr-only">Increase</span>
								</button>
							</div>
								<!-- <label for="inputCount" class="control-label"><p class="text-muted">QTY</p></label> -->
						</div>
					</div>
				</div>
				<div class="col-lg-12">
				<c:if test="${productPurchasable}">
					<button href="#" class="btn btn-success btn-lg btn-block addToCartButton" data-event-click-id="catalog44" data-event-click-param0="${prodId}" >
						<i class="fa fa-shopping-cart"></i>Add to Cart
					</button>
				</c:if>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
					<%--<a class="btn btn-midnight btn-block btn-sm" href="modals/add-to-list-modal.jsp" data-toggle="modal" data-target="#add-to-list-modal">--%>
					<a class="btn btn-midnight btn-block btn-sm" href="#" data-toggle="modal" data-target="#add-to-list-modal">
						Add to List
					</a>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
					<a href="#" class="btn btn-link btn-block btn-sm">
						<span>Add to Price/Avail.</span>
					</a>
				</div>
				<dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden" paramvalue="product.repositoryId"/>
				<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" paramvalue="product.childSKUs[0].repositoryId"/>
				<dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>

			</dsp:form>

		</div>

	</div>
</dsp:page>