<dsp:page>

    <dsp:importbean bean="/vsg/commerce/order/purchase/AddToCartFormHandler"/>
    <dsp:importbean bean="/atg/userprofiling/Profile"/>
    <dsp:importbean bean="/cps/droplet/GetPriceFromMapDroplet"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
    <dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
    <dsp:importbean bean="/atg/commerce/order/purchase/CPSLastProductsAddedToCart"/>
    <dsp:getvalueof var="isTransient" bean="Profile.transient"/>
    <dsp:getvalueof var="lastProductsAddedToCart" bean="CPSLastProductsAddedToCart"/>

    <dsp:getvalueof var="relatedProducts" bean="CPSLastProductsAddedToCart.relatedProducts"/>

    <c:if test="${not empty relatedProducts}">

        <h3>Related Items</h3>
        <hr>

        <div class="category-list">
            <dsp:getvalueof var="ulClass" value="item item-ymal row row-eq-height row-narrow"/>
                <%--
                            <dsp:getvalueof var="liClass" value="item-item col-md-20 col-sm-4 col-xs-12"/>
                --%>
            <dsp:getvalueof var="liClass" value="item-item col-md-4"/>

            <ul class="${ulClass}">

                <c:forEach var="relatedProductId" items="${relatedProducts}" end="2">

                    <dsp:droplet name="ProductLookup">
                        <dsp:param name="id" value="${relatedProductId}"/>
                        <dsp:param name="elementName" value="product"/>
                        <dsp:oparam name="output">

                            <dsp:getvalueof var="displayName" param="product.displayName"/>
                            <dsp:getvalueof var="description" param="product.description"/>
                            <dsp:getvalueof var="detailImg" param="product.thumbnail_url"/>
                            <dsp:getvalueof var="defaultImg" value="/assets/images/pdp-placeholder.png"/>
                            <dsp:getvalueof var="skuId" param="product.childSKUs[0].repositoryId"/>

                            <%--<c:forEach var="relatedProduct" items="${relatedProducts}" end="2">--%>
                            <%--<dsp:tomap var="productMap" value="${relatedProduct}"/>--%>

                            <%--<dsp:getvalueof var="prodId" value="${productMap.repositoryId}"/>--%>
                            <%--<dsp:getvalueof var="displayName" value="${productMap.displayName}"/>--%>
                            <%--<dsp:getvalueof var="web_url" value="${productMap.web_url}"/>--%>
                            <%--<dsp:getvalueof var="description" value="${productMap.description}"/>--%>
                            <%--<dsp:getvalueof var="skuId" value="${productMap.childSKUs[0].repositoryId}"/>--%>
                            <%--<c:set var="web_url" value="${empty web_url ? '/assets/images/plp-placeholder.png' : web_url}"/>--%>

                            <div style="display: none">
                                <dsp:form id="addToCart${relatedProductId}" formid="addToCart${relatedProductId}"
                                          action=""
                                          method="post">
                                    <dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1"
                                               priority="10"/>
                                    <dsp:input bean="CartModifierFormHandler.items[0].quantity" type="hidden"
                                               value="1"/>
                                    <dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden"
                                               value="${relatedProductId}"/>
                                    <dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden"
                                               value="${skuId}"/>
                                    <dsp:input type="hidden" value="true" priority="-10"
                                               bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
                                </dsp:form>
                            </div>

                            <dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
                                <dsp:param name="prodId" value="${relatedProductId}"/>
                                <dsp:oparam name="output">
                                    <dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
                                </dsp:oparam>
                            </dsp:droplet>

                            <dsp:droplet name="/cps/droplet/AccessRightDroplet">
                                <dsp:param name="profile" bean="Profile"/>
                                <dsp:param name="accessRightKey" value="plp-pdp-cart"/>
                                <dsp:oparam name="false">
                                    <dsp:getvalueof var="permissionDenied" value="true"/>
                                </dsp:oparam>
                            </dsp:droplet>

                            <li class="${liClass}">
                                    <span class="item-frame">
                                        <div class="row row-flush">
                                            <div class="col-sm-12 col-sm-push-0 col-xs-4 col-xs-push-8 col-eq-height"><a
                                                    href="${productUrl}" class="item-thumb">
                                            <cp:readerimg src="<c:out value='${detailImg}' default='${defaultImg}'/>"
                                                 alt="image" /></a></div>
                                            <div class="col-sm-12 col-sm-pull-0 col-xs-8 col-xs-pull-4">
                                    <span class="item-meta">
                                        <span class="item-title title-eq-height"><a
                                                href="${productUrl}">${description}</a></span>
                                        <span class="item-sku">${displayName}</span>



                                        <dsp:droplet name="/cps/droplet/CPSPriceDroplet">
                                            <dsp:param name="productId" value="${relatedProductId}"/>
                                            <dsp:oparam name="outputPrice">
                                                <dsp:getvalueof var="uomPrimary" param="product.uomPrimary"/>
                                                <span class="item-price">
                                                    Your Price: <dsp:valueof param="currentPrice" converter="currencyConversion"/>
                                                </span>
                                            </dsp:oparam>
                                        </dsp:droplet>
<%--
                                        <span class="item-price">Your Price: <dsp:valueof value="${priceValue}"
                                                                                          converter="currencyConversion"/></span>
--%>
                                    </span>
                                     <c:choose>
                                         <c:when test="${transient}">
                                             <dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
                                                 <dsp:oparam name="true">
                                                    <span class="item-order"><a
                                                          href="#" style="color: #fff;"
                                                          data-event-click-id="catalog45" data-event-click-param0="${relatedProductId}"
                                                          class="btn btn-default btn-block addToCartButton"><fmt:message
                                                          key="listing.product.tile.addToCart"/></a>
                                                    </span>
                                                 </dsp:oparam>
                                                 <dsp:oparam name="false">
                                                     <span class="item-order"><a
                                                             href="#" style="color: #fff;"
                                                             data-toggle="modal"
                                                             data-target="#logIn"
                                                             data-productId="${relatedProductId}"
                                                             data-skuId="${skuId}"
                                                             class="btn btn-default btn-block"><fmt:message
                                                             key="listing.product.tile.addToCart"/></a>
                                                    </span>
                                                 </dsp:oparam>
                                             </dsp:droplet>
                                         </c:when>
                                         <c:when test="${permissionDenied}">
                                               <span class="item-order"><a
                                                       href="#" style="color: #fff;"
                                                       data-toggle="modal"
                                                       data-target="#permissionDenied"
                                                       class="btn btn-default btn-block"><fmt:message
                                                       key="listing.product.tile.addToCart"/></a>
                                                </span>
                                         </c:when>
                                         <c:otherwise>
                                                <span class="item-order"><button
                                                        style="color: #fff;"
                                                        data-event-click-id="catalog45" data-event-click-param0="${relatedProductId}" 
                                                        class="btn btn-default btn-block addToCartButton"><fmt:message
                                                        key="listing.product.tile.addToCart"/></button>
                                                </span>
                                         </c:otherwise>
                                     </c:choose>
                                </div>
                            </div>
                        </span>
                            </li>
                        </dsp:oparam>
                    </dsp:droplet>
                </c:forEach>
            </ul>

        </div>

    </c:if>

</dsp:page>