<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
	<dsp:importbean bean="/cps/droplet/AliasNumberLookupFromIndex"/>
	<dsp:importbean bean="/cps/util/CPSGlobalProperties"/>
	<dsp:getvalueof var="id" param="id"/>
	<dsp:getvalueof var="priceValue" param="priceValue"/>
	<dsp:getvalueof var="skuId" param="skuId"/>
	<dsp:getvalueof var="displayName" param="displayName"/>
	<dsp:getvalueof var="img" param="image"/>
	<dsp:getvalueof var="seoLink" param="seoLink"/>
	<dsp:getvalueof var="multiSku" param="multiSku"/>
	<dsp:getvalueof var="record" param="record"/>
	<dsp:getvalueof var="isTransient" bean="Profile.transient"/>
	<dsp:getvalueof var="lazyLoadEnabled" bean="CPSGlobalProperties.lazyLoadEnabled"/>
    <dsp:getvalueof var="lazyLoadImagesEnabled" param="lazyLoadImagesEnabled"/>
    <dsp:getvalueof var="stockingTypesNotPurchasable" bean="CPSGlobalProperties.stockingTypesNotPurchasable"/>
    

	<c:set var="defImg" value="/assets/img/thumb_scroller.png"/>
	<c:set var="img" value="${empty img ? defImg : img}"/>


	<dsp:droplet name="/atg/commerce/catalog/comparison/ProductListContains">
		<dsp:param name="productList" bean="/atg/commerce/catalog/comparison/ProductList"/>
		<dsp:param name="productID" param="id"/>
		<dsp:oparam name="true"><c:set var="inCompare" value="true"/></dsp:oparam>
		<dsp:oparam name="false"><c:set var="inCompare" value="false"/></dsp:oparam>
	</dsp:droplet>
	<dsp:getvalueof var="altText" value=""/>
	<c:if test="${record.attributes['product.altText'] != null}">
			<dsp:getvalueof var="altText" value="${fn:toLowerCase(record.attributes['product.altText'])}"/>
	</c:if>

	<fmt:bundle basename="web.resources.WebAppResources">

		<dsp:getvalueof var="thumbnail_img" value="${record.attributes['product.web_url']}"/>
		<%--<dsp:getvalueof var="thumbnail_img" value="${record.attributes['product.thumbnail_url']}"/>--%>
		<c:set var="thumbnail_img" value="${empty thumbnail_img ? '/assets/images/plp-placeholder.png' : thumbnail_img}"/>
		
		<li class="item-item">
			<div class="item-frame">
				<div class="row row-narrow">
					<div class="item-col-left">
						<a href="${seoLink}" class="item-thumb">
						<c:choose>
								<c:when test="${lazyLoadEnabled && lazyLoadImagesEnabled}">
									<dsp:getvalueof var="thumbnail"
										value="${vsg_utils:escapeHtml(thumbnail_img)}" />
									<dsp:getvalueof var="lowerThumbnail"
										value="${fn:toLowerCase(thumbnail) }" />
									<c:choose>
										<c:when test="${fn:startsWith(lowerThumbnail,'http')}">
											<img class="lazy" alt="${vsg_utils:escapeHtml(altText)}" data-src="${thumbnail}">
										</c:when>
										<c:otherwise>
											<img class="lazy" alt="${vsg_utils:escapeHtml(altText)}"
												data-src="${staticContentPrefix}${thumbnail}">
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<cp:readerimg alt="${vsg_utils:escapeHtml(altText)}" src="${vsg_utils:escapeHtml(thumbnail_img)}" />
								</c:otherwise>
							</c:choose>
						</a>
						<div class="checkbox item-compare">
							<label ><input type="checkbox" value="${id}" data-event-click-id="catalog66"  
								<c:if test="${inCompare}">checked</c:if> >Add to Compare</label>
							<br>
							<a id="compareItemsCountProduct_${id}">Compare Now</a>
						</div>
					</div>
					<div class="item-col-center">
						<div class="item-title fade_description">
							<dsp:getvalueof var="description1" value="${record.attributes['product.description']}"/>
							<%-- <dsp:getvalueof var="description2" value="${record.attributes['product.descriptionLine2']}"/> --%>
							<a href="${seoLink}" title="${description1}">
								<dsp:valueof value="${description1}" converter="valueishtml"/>
							</a>
						</div>
						
						<dsp:getvalueof var="vendorItem" value="${record.attributes['product.vendorItem']}"/>
						<dsp:getvalueof var="pricesvc_upc" value="${record.attributes['product.pricesvc_upc']}"/>
						<dsp:droplet name="AliasNumberLookupFromIndex">
							<dsp:param name="prodAliasMap" value="${record.attributes['product.prodAliasMap']}"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="customerAlias" param="alias"/>
								<dsp:getvalueof var="prodAlias" param="customerAlias"/>
                    			<dsp:getvalueof var="alias" value="${fn:replace(prodAlias, '|', ',')}"/>
							</dsp:oparam>
						</dsp:droplet>
						<dsp:droplet name="/cps/droplet/CPSProductSpecificMessagesDroplet">
						    <dsp:param name="productId" param="id"/>
						    <dsp:param name="type" value="one"/>
						    <dsp:oparam name="output">
						        <dsp:getvalueof var="message" param="message" />
						    </dsp:oparam>
						</dsp:droplet>
						<ul class="item-skus">
							<li><strong>Mfr #:</strong> ${vendorItem}</li>
							<li><strong>Item #:</strong> ${vsg_utils:escapeHtml(displayName)}</li>
							<c:if test="${not empty pricesvc_upc}">
								<li><strong>UPC:</strong> ${pricesvc_upc}</li>
							</c:if>
							<c:if test="${customerAlias}">
								<li><strong>Customer Alias:</strong> ${alias}</li>
							</c:if>
							<c:if test="${not empty message}">
                                <li class = "listView">
								    <p style="display: flex; flex-shrink: 0;">
									    <i class="fa fa-exclamation-triangle gold" aria-hidden="true" style="font-size: 18px;margin-right: 10px;"></i>
									    <span>${message}</span>
								    </p>
                                </li>
                            </c:if>
						</ul>
					</div>

					<div class="item-col-right">
						<div class="row row-narrow">
							<div class="item-price col-sm-12 col-xs-5 text-center text-left-xs">
                                 <c:if test="${not empty message}">
								    <div class = "gridView col-sm-2 text-left">
                                        <input type="hidden" id="plp-warn-message-${id}" value="${message}" title="${message}"/>
								        <i class="fa fa-exclamation-triangle gold plp-warn-icon" title="${message}" data-warn-msg-id="plp-warn-message-${id}"></i>
								    </div>
                                </c:if>
							    <div class = "gridView text-right">
								    <dsp:valueof value="${priceValue}" converter="currencyConversion"/> / ${record.attributes['product.uomPrimary']}
                                </div>
							    <div class = "listView text-center">
								    <dsp:valueof value="${priceValue}" converter="currencyConversion"/> / ${record.attributes['product.uomPrimary']}
                                </div>
							</div>
							<div class="item-order col-sm-12 col-xs-7">
								<dsp:form id="addToCart${vsg_utils:escapeHtml(id)}" formid="addToCart${vsg_utils:escapeHtml(id)}" action="" method="post">
									<div class="input-group mb-5" style="margin-bottom: 5px;">
										<dsp:input bean="CartModifierFormHandler.addItemCount" type="hidden" value="1" priority="10">
											<dsp:tagAttribute name="placeholder" value="Qty"/>
										</dsp:input>
										<dsp:getvalueof var="minimum_order_qty" value="${record.attributes['product.minimum_order_qty']}"/>
										<dsp:getvalueof var="productPurchasable" value="${record.attributes['product.onlinePurchasable'][0]}"/>
										<dsp:getvalueof var="stockingType" value="${record.attributes['product.stockingType'][0]}"/>
										<dsp:getvalueof var="isProductPurchasableStockingType" value="${vsg_utils:containsTag(stockingTypesNotPurchasable,stockingType)}"/>
                                        <c:if test="${productPurchasable && !isProductPurchasableStockingType}">
											<dsp:input id="qty${vsg_utils:escapeHtml(id)}" bean="CartModifierFormHandler.items[0].quantity"
													   iclass="form-control text-center qty${vsg_utils:escapeHtml(id)}" type="text" value="${minimum_order_qty}"
													   maxlength="5" >
												<dsp:tagAttribute name="data-event-keydown-id" value="catalog70"/>
											</dsp:input>
											<dsp:droplet name="/cps/droplet/AccessRightDroplet">
												<dsp:param name="profile" bean="Profile" />
												<dsp:param name="accessRightKey" value="plp-pdp-cart" />
												<dsp:oparam name="false">
													<dsp:getvalueof var="permissionDenied" value="true"/>
												</dsp:oparam>
											</dsp:droplet>
											<div class="input-group-btn">
												<c:choose>
													<c:when test="${isTransient}">
														<dsp:droplet name="/cps/droplet/GuestCheckoutAllowedDroplet">
															<dsp:oparam name="true">
																<button type="button" class="btn btn-default addToCartButton" data-event-click-id="catalog67" data-event-click-param0="${vsg_utils:escapeJS(id)}" >
																	<fmt:message key="listing.product.tile.addToCart"/>
																</button>
															</dsp:oparam>
															<dsp:oparam name="false">
																<a href="#" class="btn btn-default"
																   data-productId="${id}"
																   data-skuId="${skuId}"
																   data-toggle="modal" data-target="#logIn">
																	<fmt:message key="listing.product.tile.addToCart"/>
																</a>
															</dsp:oparam>
														</dsp:droplet>
													</c:when>
													<c:when test="${permissionDenied}">
														<a href="#" class="btn btn-default" data-toggle="modal" data-target="#permissionDenied">
															<fmt:message key="listing.product.tile.addToCart"/>
														</a>
													</c:when>
													<c:otherwise>
	                                                   <button type="button" class="btn btn-default addToCartButton" data-event-click-id="catalog67" data-event-click-param0="${vsg_utils:escapeJS(id)}">
	                                                       <fmt:message key="listing.product.tile.addToCart"/>
	                                                   </button>
													</c:otherwise>
				                               </c:choose>
											</div>
   										</c:if>
									    

										<dsp:input bean="CartModifierFormHandler.items[0].productId" type="hidden" value="${id}"/>
										<dsp:input bean="CartModifierFormHandler.items[0].catalogRefId" type="hidden" value="${skuId}"/>
										<dsp:input type="hidden" value="true" priority="-10" bean="CartModifierFormHandler.addMultipleItemsToOrder"/>
									</div>
								</dsp:form>
								<div id="cover-spin"></div>
								<div id="minQtyError${id}" class="red" style="display:none;"></div>
								<c:if test="${!productPurchasable || isProductPurchasableStockingType}">
									<div class="col-xs-12">
									    <div class="nolink">
										    <i class="fa fa-exclamation-triangle red"></i>
										    <fmt:message key="listing.product.notPurchasableOnline"/>
                                            <br/><br/>
									    </div>
									</div>
								</c:if>

								<ul class="item-tools mt-10" style="margin-top: 10px;">
									<li>
										<c:choose>
											<c:when test="${isTransient}">
												<a href="#myModalCheck" data-toggle="modal" data-target="#logIn">
													<i class="fa fa-check"></i>
													<fmt:message key="listing.product.tile.checkAvailability"/>
												</a>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${permissionDenied}">
														<a href="#myModalCheck" data-toggle="modal" data-target="#permissionDenied">
															<i class="fa fa-check"></i>
															<fmt:message key="listing.product.tile.checkAvailability"/>
														</a>
													</c:when>
													<c:otherwise>
														<a href="#myModalCheck" data-toggle="modal" data-event-click-id="catalog68" data-event-click-param0="${vsg_utils:escapeJS(id)}" >
															<i class="fa fa-check"></i>
															<fmt:message key="listing.product.tile.checkAvailability"/>
														</a>
													</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</li>
									<li>
										<c:choose>
											<c:when test="${isTransient}">
												<a href="#addToMaterialList" data-toggle="modal" data-target="#logIn">
													<i class="icon icon-paper-list"></i>
													<fmt:message key="listing.product.tile.addToMaterialList"/>
												</a>
											</c:when>
											<c:otherwise>
												<c:choose>
													<c:when test="${permissionDenied}">
														<a href="#addToMaterialList" data-toggle="modal" data-target="#permissionDenied">
															<i class="icon icon-paper-list"></i>
															<fmt:message key="listing.product.tile.addToMaterialList"/>
														</a>
													</c:when>
													<c:otherwise>
														<a href="#addToMaterialList" data-toggle="modal" data-event-click-id="catalog69" data-event-click-param0="${vsg_utils:escapeJS(id)}" data-event-click-param1="${vsg_utils:escapeJS(skuId)}" >
															<i class="icon icon-paper-list"></i>
															<fmt:message key="listing.product.tile.addToMaterialList"/>
														</a>
													</c:otherwise>
												</c:choose>
											</c:otherwise>
										</c:choose>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
	</fmt:bundle>
</dsp:page>