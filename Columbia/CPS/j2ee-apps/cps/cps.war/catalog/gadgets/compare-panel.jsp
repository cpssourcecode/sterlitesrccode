<dsp:page>
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductListHandler"/>
	<%--<dsp:importbean bean="/vsg/commerce/catalog/comparison/AddProductToCompareHandler"/>--%>
	<%--<dsp:importbean bean="/vsg/commerce/catalog/comparison/RemoveProductFromCompareHandler"/>--%>
	<%--<dsp:importbean bean="/vsg/commerce/catalog/comparison/GoToCompareHandler"/>--%>
	
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList"/>

<%--
	<dsp:form id="addToCompareForm" formid="addToCompareForm" method="POST">
		<dsp:input type="hidden" bean="AddProductToCompareHandler.productID" id="addCompareProdId"/>
		<dsp:input type="hidden" bean="AddProductToCompareHandler.categoryID" id="addCompareCatId" disabled="true"/>
		<dsp:input type="hidden" bean="AddProductToCompareHandler.addProduct" value="submit" priority="-10"/>
	</dsp:form>
--%>
<%--
	<dsp:form id="removeFromCompareForm" formid="removeFromCompareForm" method="POST">
		<dsp:input type="hidden" bean="RemoveProductFromCompareHandler.productID" id="removeCompareProdId"/>
		<dsp:input type="hidden" bean="RemoveProductFromCompareHandler.categoryID" id="removeCompareCatId"  disabled="true"/>
		<dsp:input type="hidden" bean="RemoveProductFromCompareHandler.removeProduct" value="submit" priority="-10"/>
	</dsp:form>
--%>
	<dsp:form id="clearCompareForm" formid="clearCompareForm" method="POST">
		<dsp:input type="hidden" bean="ProductListHandler.clearList" value="submit" priority="-10"/>
	</dsp:form>
<%--
	<dsp:form id="goToCompareForm" formid="goToCompareForm" method="POST">
		<dsp:input type="hidden" bean="GoToCompareHandler.goToCompare" value="submit" priority="-10"/>
	</dsp:form>
--%>

	<dsp:getvalueof var="currentUrl" value="${requestScope['javax.servlet.forward.request_uri']}"/>
	<dsp:getvalueof var="currentUrlParams" value="${requestScope['javax.servlet.forward.query_string']}"/>
	<c:if test="${not empty currentUrlParams}">
		<dsp:getvalueof var="currentUrl" value="${currentUrl}?${currentUrlParams}"/>
	</c:if>
<%--
	<dsp:droplet name="/vsg/droplet/UserLastVisitedListingPage">
		<dsp:param name="profile" bean="/atg/userprofiling/Profile"/>
		<dsp:param name="inputUrl" value="${currentUrl}"/>
	</dsp:droplet>
--%>

	<fmt:bundle basename="web.resources.WebAppResources">
		<dsp:droplet name="/atg/dynamo/droplet/ForEach">
			<dsp:param name="array" bean="ProductList.items" />
			<dsp:oparam name="outputStart">
<!-- 				<div class="row"> -->
					<div class="col-lg-12 hidden-xs">
						<div class="well well-compare">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12">
									<h4 style="margin-top: 11px;">
										<a href="/catalog/compare.jsp" data-event-click-id="catalog4" ><fmt:message key="listing.compare.title"/></a>
										<span class="text-muted"><small><fmt:message key="listing.compare.maxCountNote"/></small></span>
									</h4>
								</div>
						
								<div class="col-lg-8 col-md-8 col-sm-12">
									<ul class="list-inline pull-right">
										<li>
											<a href="#" style="width: 140px;" class="btn btn-default" data-event-click-id="catalog5" >
												<span><fmt:message key="listing.compare.clearLnk"/></span></a>
										</li>
										<li>
											<a href="/catalog/compare.jsp" style="width: 140px;" data-event-click-id="catalog6"  class="btn btn-primary btn-nopadding">
												<fmt:message key="listing.compare.compareBtn"/></a>
										</li>
									</ul>
								</div>
							</div>
							
							<hr>
							<ul class="list-inline list-items">
			</dsp:oparam>
			<dsp:oparam name="output">
				<dsp:getvalueof var="prodId" param="element.product.repositoryId" />
				<dsp:getvalueof var="displayName" param="element.product.displayName" />
								<li>
									<a href="${productUrl}" class="compared-item">
										<div class="image">
											<dsp:include src="/catalog/gadgets/product-img.jsp">
												<dsp:param name="product" param="element.product"/>
												<dsp:param name="type" value="thumb"/>
											</dsp:include>
										</div>
										<span><dsp:valueof param="element.product.displayName" valueishtml="true"/></span>
									</a>
									<button class="btn-remove" data-event-click-id="catalog7" data-event-click-param0="${prodId}" >
										<i class="fa fa-times-circle-o"></i>
									</button>
								</li>
			</dsp:oparam>
			<dsp:oparam name="outputEnd">
							</ul>
						</div>
					</div>
<!-- 				</div> -->
			</dsp:oparam>
		</dsp:droplet>
	</fmt:bundle>
</dsp:page>