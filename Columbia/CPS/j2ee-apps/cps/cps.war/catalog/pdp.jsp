<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/commerce/catalog/ProductLookup"/>
    <dsp:getvalueof var="showCS" param="showCS"/>
    <dsp:getvalueof var="open" param="open"/>
	<dsp:importbean bean="/cps/droplet/CPSAddRecentlyViewedDroplet"/>
	<dsp:importbean bean="/cps/userprofiling/service/ProfileRecentlyViewedService"/>

	<dsp:getvalueof param="prodId" var="prodId"/>

	<dsp:droplet name="ProductLookup">
		<dsp:param name="id" value="${prodId}" />
		<dsp:param name="filterBySite" value="false" />
		<dsp:param name="filterByCatalog" value="false" />
		<dsp:oparam name="output">
			<dsp:setvalue param="product" paramvalue="element" />
			<dsp:getvalueof var="eCommerceDisplay" param="product.parentCategory.eCommerceDisplay" />

			<dsp:droplet name="CPSAddRecentlyViewedDroplet">
				<dsp:param name="product" param="product" />
				<dsp:param name="profileRecentlyViewedService" bean="ProfileRecentlyViewedService" />
			</dsp:droplet>

			<dsp:getvalueof var="selectedOrg" bean="/atg/userprofiling/Profile.CSRSelectedOrg" />
			<c:if test="${not empty selectedOrg}">
				<dsp:getvalueof var="csrClass" value="admin-access" />
			</c:if>

			<dsp:getvalueof var="productDescription" param="product.description" />

			<cp:pageContainer page="pdp" title="${productDescription}">
				<param name="norelogin" id="norelogin" value="true"/>
				<param name="showModal" id="showModal" value="${open}"/>
				<main id="body">
				<div class="container ${csrClass}" itemscope itemtype="http://schema.org/Product">
					<div class="row">
						<dsp:include page="/catalog/gadgets/pdp-breadcrumb.jsp">
							<dsp:param name="product" param="product" />
						</dsp:include>
						<dsp:include page="/global/gadgets/page-actions.jsp">
							<dsp:param name="isShowEmail" value="true" />
							<dsp:param name="isShowPDPErrorForm" value="true" />
						</dsp:include>
					</div>

					<div class="container preload-cover loader-active">
						<div class="loading">
							<i class="fa fa-spinner fa-spin"></i>
						</div>
					</div>

					<dsp:include page="/catalog/gadgets/pdp-title.jsp">
						<dsp:param name="product" param="product" />
					</dsp:include>
					<dsp:include page="/catalog/gadgets/pdp-tabs.jsp">
						<dsp:param name="product" param="product" />
					</dsp:include>
					<dsp:include page="/catalog/gadgets/pdp-suggested.jsp">
						<dsp:param name="product" param="product" />
					</dsp:include>
				</div>
				</main>
			</cp:pageContainer>

			<input type="hidden" id="showModalCSPDP" value="${not isTransient ? vsg_utils:escapeJS(showCS) : " " }"/>
			<input type="hidden" id="showModalOpenPDP" value="${not isTransient ? vsg_utils:escapeJS(open) : " " }"/>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>