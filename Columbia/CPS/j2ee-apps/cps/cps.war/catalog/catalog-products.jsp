<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
	<%--<cp:pageContainer page="account">--%>
		<main id="body">
			<div class="container">
				<ol class="breadcrumb">
					<li class="active">All Products</li>
				</ol>

				<h1 class="mb20">All Products</h1>
				
				<section class="sitemap-list">
					<div class="row">
						<dsp:droplet name="/cps/droplet/AllProductsDroplet">
							<dsp:param name="catalog" bean="/atg/multisite/Site.defaultCatalog" />
							<dsp:param name="catCount" value="-1" />
							<dsp:param name="subCatCount" value="-1" />
							<dsp:param name="sortProperties" value="displayName" />
							<dsp:oparam name="output">
								
								<dsp:getvalueof var="navLinkText" value="${content.navLinkText}" />
								<dsp:getvalueof var="rootCategoriesMap" param="rootCategoriesMap"/>
								
								<dsp:getvalueof var="modVal" value="${fn:length(rootCategoriesMap) % 3}"/>
								<dsp:getvalueof var="divVal" value="${fn:length(rootCategoriesMap) / 3}"/>
								
								<fmt:formatNumber var="sizeLeft" value="${divVal + (modVal > 0 ? 1 : 0)}" maxFractionDigits="0"/>
								<fmt:formatNumber var="sizeCenter" value="${divVal}" maxFractionDigits="0"/>
								<fmt:formatNumber var="sizeRight" value="${divVal + (modVal > 1 ? 1 : 0)}" maxFractionDigits="0"/>
								
								
								<fmt:formatNumber var="categoriesInColumn" value="${fn:length(rootCategoriesMap) / 3}" maxFractionDigits="0" />
								
								<dsp:getvalueof var="closeDivIndex" value="0"/>
								<dsp:droplet name="ForEach">
									<dsp:param name="array" param="rootCategoriesMap" />
									<dsp:param name="elementName" value="rootCat"/>
									<dsp:oparam name="output">
										
										<dsp:getvalueof var="rootCatIndex" param="index"/>
										<dsp:getvalueof var="rootCatName" param="rootCat"/>
										<dsp:getvalueof var="rootCatURL" param="key"/>
										
										<c:if test="${rootCatIndex eq 0 || rootCatIndex eq sizeLeft || rootCatIndex eq (sizeLeft + sizeCenter) || rootCatIndex eq (sizeLeft + sizeCenter + sizeRight)}">
											<div class="col-sm-4">
										</c:if>

                                        <h4>
                                            <a href="${rootCatURL}">${vsg_utils:formatName(rootCatName)}</a>
                                        </h4>
										
										<dsp:droplet name="ForEach">
											<dsp:param name="array" param="childCategoriesMap" />
											<dsp:param name="elementName" value="subCatMap"/>
											<dsp:oparam name="output">
												<dsp:getvalueof var="subCountIndex" param="index"/>
		
												<c:if test="${rootCatIndex == subCountIndex}">
													<dsp:droplet name="ForEach">
														<dsp:param name="array" param="subCatMap" />
														<dsp:param name="elementName" value="subCat"/>
														<dsp:oparam name="output">
															<dsp:getvalueof var="subCat" param="subCat"/>
															<dsp:getvalueof var="subCatUrl" param="key"/>
															<dsp:getvalueof var="subCatDisplayName" param="subCat.displayName" />
															<c:if test="${!empty subCat}">
																<li><a href="${vsg_utils:escapeHtml(subCatUrl)}">${vsg_utils:formatName(vsg_utils:escapeHtml(subCatDisplayName))}</a></li>
															</c:if>
														</dsp:oparam>
														<dsp:oparam name="outputStart">
															<ul class="list-unstyled">
														</dsp:oparam>
														<dsp:oparam name="outputEnd">
															</ul>
														</dsp:oparam>
													</dsp:droplet>
												</c:if>
											</dsp:oparam>
										</dsp:droplet>

										<c:if test="${rootCatIndex eq (sizeLeft - 1)  || rootCatIndex eq (sizeLeft + sizeCenter - 1) || rootCatIndex eq (sizeLeft + sizeCenter + sizeRight - 1)}">
											</div>
										</c:if>
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
					</div>
				</section>
			</div>
		</main>
	<%--</cp:pageContainer>--%>
</dsp:page>