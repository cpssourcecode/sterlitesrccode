<dsp:page>
	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductList" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/BeanProperty" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/cps/droplet/AliasNumberLookupFromIndex" />
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:getvalueof var="items" bean="ProductList.items" />
	<dsp:getvalueof var="isTransient" bean="Profile.transient" />
	<dsp:getvalueof var="defaultImg" value="/assets/images/pdp-placeholder.png" />
	<dsp:getvalueof param="backUrl" var="backUrl"/>

	<dsp:importbean bean="/atg/commerce/catalog/comparison/ProductListHandler"/>
	<dsp:getvalueof var="comparisonReferer" bean="CPSSessionBean.comparisonReferer"/>
	
	<cp:pageContainer page="catalog">
		<dsp:form id="removeFromCompareForm" formid="removeFromCompareForm" method="POST">
			<dsp:input type="hidden" bean="ProductListHandler.productID" id="removeCompareProdId"/>
			<dsp:input type="hidden" bean="ProductListHandler.categoryID" id="removeCompareCatId"  disabled="true"/>
			<dsp:input type="hidden" bean="ProductListHandler.removeProduct" value="submit" priority="-10"/>
		</dsp:form>
	
		<param name="norelogin" id="norelogin" value="true"/>
		<main id="body">
		<div class="container">
			<ol class="breadcrumb">
				<li class="active">Compare Products</li>
			</ol>

			<h1 class="mb20">Compare Products</h1>

			<div class="back-to-product">
				<a class="text-nodec" href="${comparisonReferer}"  ><strong class="item-x"><</strong> Back to Products</a>
			</div>

			<div id="compare-content">
				<dsp:include page="gadgets/compare-content.jsp"/>
				<script nonce="${requestScope.nonce}">
					$(document).ready(function(){
                        onCompareContentLoaded();
					});
				</script>
			</div>
		</div>
		</main>
		<script nonce="${requestScope.nonce}">
			function returnBack() {
				if ('referrer' in document) {
			        window.location = document.referrer;
			    } else {
			        window.history.back();
			    }
			}
		
		</script>
	</cp:pageContainer>
</dsp:page>