<dsp:page>
	<dsp:getvalueof var="selectedOrg" bean="/atg/userprofiling/Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>
	<cp:pageContainer page="location">
		<dsp:getvalueof var="location" param="l" />
		<main id="body">
		<div class="container ${csrClass}">
			<div class="row">
				<div class="col-md-8">
					<ol class="breadcrumb">
						<li class="active"><a href="/locations/locations.jsp">Locations</a></li>
                        <li class="active">
                            <dsp:droplet name="/cps/droplet/LocationLookupDroplet">
                                <dsp:param name="location" value="${location}" />
                                <dsp:oparam name="output">
                                    <dsp:valueof param="store.name"/>,&nbsp;<dsp:valueof param="store.stateAddress"/>
                                </dsp:oparam>
                            </dsp:droplet>
                        </li>
					</ol>
				</div>
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="true" />
					<dsp:param name="print" value="false" />
				</dsp:include>
			</div>
			<dsp:droplet name="/cps/droplet/LocationLookupDroplet">
				<dsp:param name="location" value="${location}" />
				<dsp:oparam name="output">
					<h1 class="mb20">
						<dsp:valueof param="store.name" />,&nbsp;<dsp:valueof param="store.stateAddress" />
					</h1>
					<div class="well well-grey location-meta">
						<div class="row">
							<div class="col-sm-4 col-xs-12 location-thumb">
								<dsp:getvalueof var="storeIMG" param="storeIMG" />
										<cp:readerimg alt="" src="${storeIMG}" />
														</div>
							<div class="col-sm-4 col-xs-6">
								<h6>Address</h6>
								<p>
									<dsp:valueof param="store.address1" /><br>
									<dsp:valueof param="store.city" />, <dsp:valueof param="store.stateAddress" />&nbsp;<dsp:valueof param="store.postalCode" />
								</p>
								<p>
									<i class="fa fa-map-marker"></i>
									<a href="#drivingDirections" data-dismiss="modal" data-toggle="modal">Get Driving Directions</a>
								</p>
							</div>
							<div class="col-sm-4 col-xs-6">
								<h6>Normal Business Hours</h6>
								<p>
									Sales Office:&nbsp;
									<dsp:valueof param="store.hours" valueishtml="true" />
									<br>
									Will Call:&nbsp;<dsp:valueof param="store.willCallHours" valueishtml="true" />
								</p>
								<h6>Phone</h6>
								<p>
									<dsp:valueof param="store.phoneNumber" />
								</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8 col-md-offset-4">
							<p>
								<dsp:valueof param="store.description" valueishtml="true" />
							</p>
						</div>
					</div>
					<dsp:include page="/global/modals/modal-driving-directions.jsp">
						<dsp:param name="address1" param="store.address1" />
						<dsp:param name="city" param="store.city" />
						<dsp:param name="state" param="store.stateAddress" />
						<dsp:param name="zip" param="store.postalCode" />
					</dsp:include>
				</dsp:oparam>
			</dsp:droplet>
		</div>
		</main>
	</cp:pageContainer>
</dsp:page>