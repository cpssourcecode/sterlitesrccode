<dsp:page>
	<dsp:getvalueof var="selectedOrg" bean="/atg/userprofiling/Profile.CSRSelectedOrg" />
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access" />
	</c:if>
	<cp:pageContainer page="location" title="landing">
		<main id="body">
		<div class="container ${csrClass}">
			<div class="row">
				<div class="col-md-8">
					<ol class="breadcrumb">
						<li class="active">Locations</li>
					</ol>
				</div>
				<dsp:include page="${originatingRequest.contextPath}/account/gadgets/page-actions.jsp">
					<dsp:param name="email" value="true" />
				</dsp:include>
			</div>
			<h1 class="mb20">Locations</h1>

			<script type="text/javascript" nonce="${requestScope.nonce}">
                    var myArray = [];
                </script>
			<section>
				<div class="row">
					<div class="col-sm-8 col-sm-push-4">
						<dsp:include page="/locations/location-map.jsp" />
					</div>
					<dsp:getvalueof var="totalCount" value="0" />
					<dsp:droplet name="/cps/droplet/LocationLookupDroplet">
						<dsp:oparam name="output">
							<dsp:getvalueof var="locationMap" param="locationMap" />
							<div class="col-sm-4 col-sm-pull-8 location-list">
								<dsp:droplet name="/atg/dynamo/droplet/ForEach">
									<dsp:param name="array" value="${locationMap}" />
									<dsp:param name="sortProperties" value="_key" />
									<dsp:setvalue param="storeList" paramvalue="element" />
									<dsp:oparam name="output">
										<dsp:getvalueof var="index" param="index" />
										<dsp:getvalueof var="region" param="key" />
										<%--AD hoc to implement design requirments of to links beeing kept in one UL tag--%>
										<c:if test="${(totalCount % 2) == 0 }">
											<ul class="col-sm-12 col-xs-4 list-unstyled">
										</c:if>
										<li>
											<a href="#${region}">${region}</a>
										</li>
										<c:if test="${(totalCount % 2) != 0 }">
											</ul>
										</c:if>
										<dsp:getvalueof var="totalCount" value="${totalCount+1}" />
									</dsp:oparam>
								</dsp:droplet>
								<%--AD hoc to implement design requirments of to links beeing kept in one UL tag--%>
								<c:if test="${(totalCount % 2) != 0 }">
									</ul>
								</c:if>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</section>
			<dsp:getvalueof var="totalCount" value="0" />
			<dsp:droplet name="/cps/droplet/LocationLookupDroplet">
				<dsp:oparam name="output">
					<dsp:getvalueof var="locationMap" param="locationMap" />
					<dsp:droplet name="/atg/dynamo/droplet/ForEach">
						<dsp:param name="array" value="${locationMap}" />
						<dsp:param name="sortProperties" value="_key" />
						<dsp:setvalue param="storeList" paramvalue="element" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="index" param="index" />
							<dsp:getvalueof var="region" param="key" />
							<section class="location-state" name="${region}" id="${region}">
								<h4 class="bar mb20">${region}</h4>

								<div class="row">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="storeList" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="lng" param="element.longitude" />
											<dsp:getvalueof var="lat" param="element.latitude" />
											<dsp:getvalueof var="name" param="element.name" />
											<dsp:getvalueof var="add1" param="element.address1" />
											<dsp:getvalueof var="city" param="element.city" />
											<dsp:getvalueof var="state" param="element.stateAddress" />
											<dsp:getvalueof var="zip" param="element.postalCode" />
											<div class="col-sm-4 col-xs-6">
												<h6>
													<dsp:getvalueof var="locationId" param="element.locationId" />
													<a href="/locations/locations-detail.jsp?l=${locationId}">
														<strong>
															<dsp:valueof param="element.name" />
														</strong>
													</a>
												</h6>
												<p>
													<dsp:valueof param="element.address1" />
													<br>
													<dsp:valueof param="element.city" />
													,
													<dsp:valueof param="element.stateAddress" />
													&nbsp;
													<dsp:valueof param="element.postalCode" />
													<br>
													<dsp:valueof param="element.phoneNumber" />
												</p>
												<script type="text/javascript" nonce="${requestScope.nonce}">
                                                        myArray[${totalCount}] = new Array(${lng}, ${lat}, '${name}', '${add1}<br>${city}, ${state}&nbsp;${zip}', '${locationId}');
                                                    </script>
											</div>

											<dsp:getvalueof var="totalCount" value="${totalCount+1}" />
										</dsp:oparam>
									</dsp:droplet>
								</div>

							</section>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</div>
		</main>
	</cp:pageContainer>
</dsp:page>