<%--
  This tag accepts the following attributes
  title      - page title which will be use on the page <title> tag.
  page		- page parameter to identify page type that are baing used
--%>
<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<%@ tag language="java"%>
<%@ attribute name="title"%>
<%@ attribute name="page"%>

<%@ attribute name="type"%>
<%@ attribute name="endecaDrivenPage"%>
<%@ attribute name="metaKeywords"%>
<%@ attribute name="metaDescription"%>

<%@ attribute name="disable"%>
<%@ attribute name="breadcrumb"%>

<%@ attribute name="canonicalLink"%>

<%-- page start --%>
<dsp:include page="/includes/pageStart.jsp">
	<dsp:param name="title" value="${title}" />
	<dsp:param name="page" value="${page}" />
	<dsp:param name="metaKeywords" value="${metaKeywords}"/>
	<dsp:param name="metaDescription" value="${metaDescription}"/>
</dsp:include>


<%-- page body--%>
<jsp:doBody />
<%-- end page body--%>


<%-- page end --%>
<dsp:include page="/includes/pageEnd.jsp" >
	<dsp:param name="page" value="${page}" />
	<dsp:param name="title" value="${title}" />
</dsp:include>