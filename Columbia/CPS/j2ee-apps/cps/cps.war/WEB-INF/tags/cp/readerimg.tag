<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<%@ tag language="java"%>
<%@ tag body-content="empty" %>
<%@ attribute name="src" required="true" %>
<%@ attribute name="width"%>
<%@ attribute name="height"%>
<%@ attribute name="alt"%>
<%@ attribute name="style"%>
<%@ attribute name="iclass"%>
<dsp:getvalueof var="staticContentPrefixURL"
	bean="/atg/multisite/Site.staticContentPrefix" />
<dsp:getvalueof var="staticContentPrefix" value="${fn:trim(staticContentPrefixURL)}" />
<c:if test="${empty staticContentPrefix }">
	<dsp:getvalueof var="staticContentPrefix" value="" />
</c:if>
<c:set var="lowerSrc" value="${fn:toLowerCase(src)}"/>
<c:set var="imgAttrs" value=""/>
<c:choose>
<c:when test="${fn:startsWith(lowerSrc,'http')}">
<c:set var="imgAttrs" value="src=${src}"/>
</c:when>
<c:otherwise>
<c:set var="imgAttrs" value="src=${staticContentPrefix}${fn:escapeXml(src)}"/>
</c:otherwise>
</c:choose>

<c:if test="${not empty alt}">
<c:set var="imgAttrs" value="${imgAttrs} alt='${alt}'"/>
</c:if>

<c:if test="${not empty width}">
<c:set var="imgAttrs" value="${imgAttrs} width='${width}'"/>
</c:if>

<c:if test="${not empty height}">
<c:set var="imgAttrs" value="${imgAttrs} height='${height}'"/>
</c:if>
<c:if test="${not empty style}">
<c:set var="imgAttrs" value="${imgAttrs} style='${style}'"/>
</c:if>
<c:if test="${not empty iclass}">
<dsp:getvalueof var="imgAttrs" value="${imgAttrs} class='${iclass}'"/>
</c:if>

<img ${imgAttrs}>