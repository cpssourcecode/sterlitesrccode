<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<%@ tag language="java"%>
<%@ tag body-content="empty" %>
<%@ attribute name="key" required="true" %>

<dsp:droplet name="/cps/droplet/MessageLookupDroplet">
	<dsp:param name="id" value="${key}" />
	<dsp:param name="elementName" value="messageItem"/>
	<dsp:oparam name="output">
		<dsp:valueof param="messageItem.message" valueishtml="true"/>
	</dsp:oparam>
</dsp:droplet>
