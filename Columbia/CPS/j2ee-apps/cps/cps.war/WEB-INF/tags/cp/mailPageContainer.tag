<%@ include file="/includes/utils/taglibs.jspf" %>
<%@ include file="/includes/utils/context.jspf" %>

<%@ tag language="java" %>

<%@ attribute name="url"%>

<dsp:include src="/email/gadgets/template-header.jsp">
	<dsp:param name="productionURL" value="${url}" />
</dsp:include>

<jsp:doBody />

<dsp:include src="/email/gadgets/template-footer.jsp">
	<dsp:param name="productionURL" value="${url}" />
</dsp:include>