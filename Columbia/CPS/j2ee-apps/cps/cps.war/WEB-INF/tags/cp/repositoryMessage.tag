<%@ include file="/includes/utils/taglibs.jspf"%>
<%@ include file="/includes/utils/context.jspf"%>
<%@ tag language="java"%>
<%@ attribute name="key" required="true"%>
<dsp:droplet name="/cps/droplet/RepositoryMessagesLookupDroplet">
	<dsp:param name="messageKey" value="${key}" />
	<dsp:oparam name="output">
		<dsp:valueof param="message" valueishtml="true" />
	</dsp:oparam>
	<dsp:oparam name="empty">
		<dsp:valueof param="message" valueishtml="true" />
	</dsp:oparam>
</dsp:droplet>
