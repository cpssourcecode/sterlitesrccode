var acc;

function searchCompanies() {
    var searchTerm = $("#searchValue").val();
    if (searchTerm.length > 0) {
        document.getElementById("search-companies-reset-btn").disabled = false;
        var letter = $("#currentLetter").val();
        var currentSort = $("#currentSort").val();
        var currentOrder = $("#currentOrder").val();
        $('.preload-cover').addClass('loader-active');
        $('.preload-cover .loading').show();
        loadDisplay(searchTerm, currentSort, currentOrder, 1, letter);
    } else {
        loadDisplay(searchTerm);
    }
}

function resetSearch() {
    $("#searchValue").val('');
    document.getElementById("search-companies-reset-btn").disabled = true;
    $('.preload-cover').removeClass('loader-active');
    $('.preload-cover .loading').hide();
    loadDisplay("", "companyName", "%2B", 1, "");
}

function filterLetter(letter) {
    var current = $("#currentLetter").val();
    var searchTerm = $("#searchValue").val();
    var currentSort = $("#currentSort").val();
    var currentOrder = $("#currentOrder").val();
    if (current == letter) {
        letter = "";
    }
    loadDisplay(searchTerm, currentSort, currentOrder, 1, letter);
}

function sortPage(sort, order) {
    var searchTerm = $("#searchValue").val();
    var letter = $("#currentLetter").val();
    var currentPage = $("#currentPage").val();
    loadDisplay(searchTerm, sort, order, currentPage, letter);
}

function changePage(page) {
    var searchTerm = $("#searchValue").val();
    var letter = $("#currentLetter").val();
    var currentSort = $("#currentSort").val();
    var currentOrder = $("#currentOrder").val();
    loadDisplay(searchTerm, currentSort, currentOrder, page, letter);
}

function onManageCompaniesLoaded() {
    $('.preload-cover').removeClass('loader-active');
    $('.preload-cover .loading').hide();
}

function loadDisplay(search, sort, order, page, letter) {
    var url = "/account/gadgets/manage-companies-display.jsp";
    var params = "searchTerm=" + encodeURIComponent(search) + "&sort=" + encodeURIComponent(sort) + "&page=" + encodeURIComponent(page) +
        "&order=" + encodeURIComponent(order) + "&letter=" + encodeURIComponent(letter);
    $("#company-display").load(url + "?" + params, function () {
        onManageCompaniesLoaded();
        eventsManageCompaniesDisplay();
    });
}

function setAccount(account) {
    acc = account;
}

function accessAccount() {
    $("#access-account").val(acc);
    $("#hide-warning").val($("#warning-check").prop("checked"));
    console.log("Don't show warning again - " + $("#hide-warning").val());
    $("#accessAccount").submit();
}

function accessWithoutWarning(account) {
    $("#access-account").val(account);
    $("#accessAccount").submit();
}
