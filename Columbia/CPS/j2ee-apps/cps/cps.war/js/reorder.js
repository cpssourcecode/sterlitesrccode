/*
// js file for javascript dealing with functionality on reorder list and detail pages
*/
var displayName=true;
var description=false;
var aliasNumber=false;
var accendGlobal = true;
var classGlobal = 'fa-sort-asc';
var prId=false;
var cs=false;
var date=false;
var qty=false;
var lastSortAccend = false;
var wasSorted = false;
var sort = 'eventName';
var sortDetails = 'displayName';
var globalSearchTerm = '';
var globalSearchCS = '';
var detailsSearchTerm = '';
var showPA = false;

function sortLists(param){
	resetSort();
	var searchTerm = $("#searchTerm").val();
	if (param == 'name'){
		if (displayName != null && displayName){
			displayName = false;
		}else if (displayName != null && !displayName){
			displayName = true;
		}else{
			displayName = true;
		}
		
		if (displayName){
			$("#sort-name").addClass(" sort-up");
		}else{
			$("#sort-name").addClass(" sort-down");
		}
		sort='eventName';
		lastSortAccend = !displayName;
	}else if (param == 'cs'){
		if (cs != null && cs){
			cs = false;
		}else if (cs != null && !cs){
			cs = true;
		}else{
			cs = true;
		}
		if (cs){
			$("#sort-cs").addClass(" sort-up");
		}else{
			$("#sort-cs").addClass(" sort-down");
		}
		sort='cs';
		lastSortAccend = !cs;
	}else if (param == 'date'){
		if (date != null && date){
			date = false;
		}else if (date != null && !date){
			date = true;
		}else{
			date = true;
		}
		if (date){
			$("#sort-date").addClass(" sort-up");
		}else{
			$("#sort-date").addClass(" sort-down");
		}
		sort='creationDate';
		lastSortAccend = !date;
	}else if (param == 'qty'){
		if (qty != null && qty){
			qty = false;
		}else if (qty != null && !qty){
			qty = true;
		}else{
			qty = true;
		}
		if (qty){
			$("#sort-qty").addClass(" sort-up");
		}else{
			$("#sort-qty").addClass(" sort-down");
		}
		sort='items';
		lastSortAccend = !qty;
	}
	
	$("#reorder-lists").load("/account/gadgets/reorder-list-display.jsp?page="+currentPage+
			"&sort="+sort+"&accend="+!lastSortAccend+"&searchTerm="+encodeURIComponent(globalSearchTerm)+"&searchCS="+encodeURIComponent(globalSearchCS));
}

function resetSort (){
	$("#sort-name").removeClass("sort-up sort-down");
	$("#sort-cs").removeClass("sort-up sort-down");
	$("#sort-date").removeClass("sort-up sort-down");
	$("#sort-qty").removeClass("sort-up sort-down");
}

function deleteMaterialList(id){
		$("#listToDelete").val(id);

		dataString = $("#deleteListForm").serialize();
		$.ajax({
			type: "POST",
			data: dataString,
			dataType: "json",
			success: deleteListSuccess
			//success: resetReorderListSearch
		});
}

function editMaterialList(){
	dataString = $("#editListForm").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: editListSuccess
	});
}

function editListSuccess(data){
	if (typeof data.errors == "undefined"){
		var d = new Date();
		var t = d.getTime();
		var giftlistIdValue = $("#giftlistIdValue").val();
		window.location.href = "/account/material-detail.jsp?giftlistId="+giftlistIdValue+"&edit=success&t="+t;
	}else{
		showErrorsId(data, "#error-edit-messages");
	}
}

function deleteListSuccess(data){
	var d = new Date();
	var t = d.getTime();
	window.location.href = "/account/material-lists.jsp?t="+t;
//	if (typeof data.errors == "undefined"){
//		window.location.href="/account/material-lists.jsp?del=success";
//	}else{
//		showErrorsId(data, "#reorder-errors");
//	}
}

function searchReorderLists(){
	$("#emptyResults").hide();
	searchTerm = $("#reorderListSearchTerm").val();
	//searchCS = $("#reorderListCSTerm").val();
	//if (searchTerm == 'Name, Item, Keyword'){
	if (searchTerm == 'Material List Name or Keyword'){
		searchTerm = '';
	}
	if (searchTerm == '') {
		showNoResultsModalPopup();
		return;
	}
	$('#btn-reset').removeAttr('disabled');
/*
	if (searchCS == 'Select...'){
		searchCS = '';
	}
*/

	globalSearchTerm = searchTerm;
	//globalSearchCS = searchCS;
	
	resetSort();
	displayName=true;
	
	$("#material-lists").load("/account/gadgets/material-list-display.jsp?page=1&sort=eventName&accend=true&searchTerm="+encodeURIComponent(globalSearchTerm), function () {
        materialListDisplay(true);
		eventsMaterialListDisplay();
		importHelpInit();
        eventMaterialListUpload();
    });
}

function resetReorderListSearch(){
	location.reload(true);
}

function resetMaterialListSearch(){
    var d = new Date();
    var t = d.getTime();
    $("#material-lists").load("/account/gadgets/material-list-display.jsp?page=1&sort=eventName&accend=true&t=" + t, function () {
         
    	/*fix SM-529 ML page pagination is not showing any results*/
    	currentPage=1;
    	globalSearchTerm = ''; 
    	
    	materialListDisplay();
        eventsMaterialListDisplay();
        importHelpInit();
        $("#reorderListSearchTerm").val("");
        $("#btn-reset").attr("disabled", true);
        eventMaterialListUpload();
    });
}

function onMaterialDetailDisplayLoaded(isSearch, nonSelectedItemsMap){
    $(document).ready(function () {
        $("#total_items_count").text($("#total_desired_qty").val());

        if($("#total_count").val() == "0"){
            $(".ml-buttons").hide();
			if (isSearch) {
				$("#emptyResults").show();
			}
		} else {
        	if($("#shouldCheckPrice").val() == 'true') {
                var isAvailable = $("#isAvailablePricesWebService").val();
                priceAvailableMsg(isAvailable);
                if (isAvailable === 'false') {
                    $('.container.main-content').css('padding-top', '30px');
                }
            }

            if($("#productDisabled").val() == 'true') {
                $('#products_obsolete_msg').show();
            }

            $(".ml-buttons").show();
            $("#emptyResults").hide();
            $("#errorsSelected").hide();

            $(".qtyInput").on('keypress', function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
            });
            initValidation();
            if (isElementExist('.content')) {
                $.each($('.content'), function(index, val) {
                    $('.checkbox .checkbox-custom .checkbox-label img', val).each(function() {
                        $(this).on('load', function(){
                            if ($(this).height() > $(this).width()) {
                                $(this).css('height', 'auto');
                                $(this).css('width', 'auto');
                                $(this).css('max-height', '80px');
                            }
                        });
                    });
                });
            }

            $('td[id^="price_"]')
                .each(
                    function() {
                        var id = this.id;
                        var product_id = id.split("_")[1];
                        var url = "/catalog/gadgets/request-product-price.jsp?s=" + product_id;
                        var ths = this;

                        if (nonSelectedItemsMap && nonSelectedItemsMap[product_id]) {
                            ths.innerHTML = nonSelectedItemsMap[product_id][1];
                        } else {
                            $.ajax({
                                type: "GET",
                                url: url,
                                dataType: 'html',
                                success: function (data) {
                                    data = $.trim(data);
                                    ths.innerHTML = data;
                                }
                            });
                        }

                    });
            $('td[id^="avlbt_"]').each(function() {
                var id = this.id;
                var product_id = id.split("_")[1];

                if (nonSelectedItemsMap && nonSelectedItemsMap[product_id]) {
                    this.innerHTML = '<span>' + nonSelectedItemsMap[product_id][0] + '</span>';
                } else {
                    getAvailability(true, product_id, this, true);
                }
            });
		}

        loadPagination();
    });
}

function searchListDetails() {
	$("#emptyResults").hide();
	$('#btn-reset').removeAttr('disabled');
	hideUpdateAllItemsSuccessMsg();
	
	searchTerm = $("#reorderDetailsSearchTerm").val();
	giftlistId = $("#giftlistIdValue").val();
	detailsSearchTerm = searchTerm;
	$("#material-items-list").load("/account/gadgets/material-detail-display.jsp?modal=true&giftlistId=" + giftlistId + 
									"&class=" + classGlobal + 
									"&sort=" + sortDetails + 
									"&numPerPage=" + numPerPage.value + 
									"&page=" + currentPage +
									"&accend=" + accendGlobal + 
									"&showPA=" + showPA + 
									"&searchTerm=" + encodeURIComponent(detailsSearchTerm),
			function() {

                onMaterialDetailDisplayLoaded(true);
                eventsMaterialDetailDisplay();
				jQuery('.scrollbar-inner').scrollbar();
				initializeCheckboxes();
				importHelpInit();
				enableUpdateAllButton();
			}
	);
}

function resetDetailsSearch(){
	hideUpdateAllItemsSuccessMsg();
	giftlistId = $("#giftlistIdValue").val();
	location.replace("/account/material-detail.jsp?giftlistId="+giftlistId);
}

function sortMaterialDetails(sortOption){
	s = sortOption;
	giftlistId = $("#giftlistIdValue").val();
	sortDetails = s;
	
	var cssClass = "";
	var accend = false;
	if (sortOption == 'displayName'){

		if (displayName != null && displayName){
			displayName = false;
		} else if (displayName != null && !displayName) {
			displayName = true;
		} else {
			displayName = true;
		}

		if (displayName){
			cssClass = "fa-sort-asc";
			accend = true;
		}else{
			cssClass = "fa-sort-desc";
			accend = false;
		}
		if(wasSorted){
			lastSortAccend = !displayName;
		}
		wasSorted = true;

	} else if (sortOption == 'description'){
		if (description != null && description){
			description = false;
		} else if (description != null && !description){
			description = true;
		} else {
			description = true;
		}
		if (description){
			cssClass = "fa-sort-asc";
			accend = true;
		}else{
			cssClass = "fa-sort-desc";
			accend = false;
		}
		lastSortAccend = !description;
		wasSorted = true;
	} else if (sortOption == 'aliasNumber'){
		if (aliasNumber != null && aliasNumber){
			aliasNumber = false;
		} else if (aliasNumber != null && !aliasNumber){
			aliasNumber = true;
			accend = true;
		} else {
			aliasNumber = true;
			accend = true;
		}
		if (aliasNumber){
			cssClass = "fa-sort-asc";
		}else{
			cssClass = "fa-sort-desc";
		}
		lastSortAccend = !aliasNumber;
		wasSorted = true;
	}
	
	accendGlobal = accend;
	classGlobal = cssClass;
	$("#material-items-list").load("/account/gadgets/material-detail-display.jsp?modal=true&showPA=" + showPA + 
										"&giftlistId=" + giftlistId + 
										"&class=" + cssClass + 
										"&sort=" + sortDetails + 
										"&numPerPage=" + numPerPage.value + 
										"&page=" + currentPage +
										"&accend=" + accend + 
										"&searchTerm="+encodeURIComponent(detailsSearchTerm),
		function() {
            onMaterialDetailDisplayLoaded();
            eventsMaterialDetailDisplay();
			jQuery('.scrollbar-inner').scrollbar();
			initializeCheckboxes();
			importHelpInit();
		}
	);
}

function selectList(){
	var options = document.getElementById("cs").options;
	var list = $("#currentCS").val();
	for (i = 0; i < options.length; i++){
		if (list == options[i].value){
			options[i].selected = true;
		}
	}
}

function deleteSelected() {
	hideUpdateAllItemsSuccessMsg();
	var itemIds = gatherListChecked();
    var selectAllBoxesCheck = $('#selectAllBoxesCheck')[0];
    if(selectAllBoxesCheck.type === "checkbox" && selectAllBoxesCheck.checked){
        itemIds = getIds($('#allGiftListIds').val());
    }

	if (itemIds.length > 0) {
		$("#removeList").val(itemIds);
		dataString = $("#material-details-form").serialize();
		$.ajax({
			type: "POST",
			data: dataString,
			dataType: "json",
			success: deleteSelectedSuccess
		});
	} else {
		setTimeout(function(){
			$('.popover.warn').on('click', function(event) {
				event.preventDefault();
				$('.warn-popover').popover('hide');
				$('.popover.warn').popover('hide');
			});
		}, 500);
		var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
		if ( isSafari ) {
			$('.popover.warn').remove();
		}

		var thisButton = $('#removeMaterial');

		var placement = 'right';
		if ($(window).width() < 992) {
			placement = 'bottom';
		}
		$(thisButton).attr('data-placement', placement);
		$(thisButton).attr('data-trigger', 'focus');
		$(thisButton).popover({
			template: '<div class="popover warn" role="tooltip">\
							<div class=""></div>\
							<h3 class="popover-title"></h3>\
							<div class="popover-content"></div>\
							<button class=close><span class="glyphicon glyphicon-remove"></span></button>\
						</div>'
		});
		$(thisButton).popover('show');
		$(thisButton).on('hidden.bs.popover', function () {
			$(thisButton).popover('destroy');
		});
	}
}

function updateList($this, itemId) {
	
	$("#error-qty-"+itemId+"_qty").html("");
	$("#"+itemId+"_qtyDiv").removeClass("has-error");
	$("#error-qty-"+itemId+"_qty").hide();
	$(".error-msg").hide();
	hideUpdateAllItemsSuccessMsg();
	disableUpdateAllButton();
	
	$("#input-ml-item-qty").val($("#"+ itemId + "_qty").val());
	$("#input-ml-item-id").val(itemId);

    var id = $("input[type='hidden'][id=priceInfo_"+itemId+"]")[0].value;
    var nonSelectedItemsMap = getNonSelectedMLItemsMap();
    delete nonSelectedItemsMap[id];

    dataString = $("#update-ml-item-qty").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
        success: function(data) {
            updateSuccess(data, nonSelectedItemsMap);
        }
	});

}

function updateSuccess(data, nonSelectedItemsMap){

	giftlistId = $("#giftlistIdValue").val();

	if ('success' == data.code) {
		
		var accend;
		if(!wasSorted){
			accend = true;
		} else {
			accend = lastSortAccend;
		}
		
		$("#material-items-list").load("/account/gadgets/material-detail-display.jsp?giftlistId=" + giftlistId + 
										"&class=" + classGlobal + 
										"&sort=" + sortDetails + 
										"&accend=" + accendGlobal +
										"&numPerPage=" + numPerPage.value + 
										"&page=" + currentPage +
										"&showPA=" + showPA, function() {
            onMaterialDetailDisplayLoaded(null, nonSelectedItemsMap);
            eventsMaterialDetailDisplay();
			var totalCount = $("#total_desired_qty").val();
			$("#itemsNumber").html(totalCount);
			jQuery('.scrollbar-inner').scrollbar();
			initializeCheckboxes();
            enableUpdateAllButton();
		});
	} else {
        enableUpdateAllButton();
		$("#updateItemErrorOccured").show();
		highlightErrors(data.properties);
		var errorMessage = "";
		var errorsArray = [];
		for(var i=0; i<data.errors.length; i++){
			if (indexOf(errorsArray,data.errors[i]) == -1){
				errorsArray.push(data.errors[i]);
				if (errorMessage == ""){
					errorMessage = data.errors[i];
				}else{
					errorMessage += "<br/>" + data.errors[i];
				}
			}
		}
		for (var i=0; i<data.properties.length; i++){
			$("#error-qty-"+data.properties[i]).html(errorMessage);
			$("#error-qty-"+data.properties[i]).show();
			$("#"+data.properties[i]+"caption").attr("style","");
		}
	}

}

function deleteSelectedSuccess(){

	giftlistId = $("#giftlistIdValue").val();

	$("#material-items-list").load("/account/gadgets/material-detail-display.jsp?giftlistId=" + giftlistId + 
										"&class=" + classGlobal + 
										"&sort=" + sortDetails + 
										"&accend=" + accendGlobal + 
										"&numPerPage=" + numPerPage.value + 
										"&page=" + currentPage +
										"&showPA=" + showPA, function() {
        onMaterialDetailDisplayLoaded();
        eventsMaterialDetailDisplay();
		var totalCount = $("#total_desired_qty").val();
		$("#itemsNumber").html(totalCount);
		jQuery('.scrollbar-inner').scrollbar();
		initializeCheckboxes();
	});

}


function updateAllList(){

giftlistId = $("#giftlistIdValue").val();

$("#material-items-list").load("/account/gadgets/material-detail-display.jsp?giftlistId=" + giftlistId + 
										"&class=" + classGlobal + 
										"&sort=" + sortDetails + 
										"&accend=" + accendGlobal +
										"&numPerPage=" + numPerPage.value + 
										"&page=" + currentPage +
										"&showPA=" + showPA, function() {
        onMaterialDetailDisplayLoaded();
        eventsMaterialDetailDisplay();
		var totalCount = $("#total_desired_qty").val();
		$("#itemsNumber").html(totalCount);
		jQuery('.scrollbar-inner').scrollbar();
		initializeCheckboxes();
        enableUpdateAllButton();
	});

}


function checkPriceSelectedSuccess(nonSelectedItemsMap){
	showPA = true;
	giftlistId = $("#giftlistIdValue").val();
    //enabled spinner
    $('#spinner-on-start').addClass('loader-active');
    $('#spinner-on-start .loading').show();
	$("#material-items-list").load("/account/gadgets/material-detail-display.jsp?showPA=true&giftlistId=" + giftlistId + 
										"&class=" + classGlobal + 
										"&sort=" + sortDetails +
										"&numPerPage=" + numPerPage.value + 
										"&page=" + currentPage +
										"&accend=" + accendGlobal, function() {
		onMaterialDetailDisplayLoaded(null, nonSelectedItemsMap);
        eventsMaterialDetailDisplay();
		jQuery('.scrollbar-inner').scrollbar();
		initializeCheckboxes();
		$("#availabilityImportantInfo").show();
        $('#spinner-on-start').removeClass('loader-active');
        $('#spinner-on-start .loading').hide();
	});
}

function importMaterialListSuccess(data) {

	if (typeof data.errors == "undefined") {
		var giftlistIdValue = $("#giftlistIdValue").val();
		window.location.href = "/account/material-detail.jsp?giftlistId=" + giftlistIdValue;
	} else {
		showErrorsId(data, "#errorIncorrect");
		//$("#errorIncorrect").show();
	}

}

function importMaterialList(giftListId) {
    var modalId = "#modalImportMaterial" + giftListId;
    $('.preload-cover').addClass('loader-active');
    $('.preload-cover .preload-spinner').show();
    $(modalId + " .errors").hide();

    var fileName = $(modalId + " .fileinput-filename").html();
    var fileExt = '';
    if (fileName != undefined && fileName != '') {
        fileExt = fileName.substr(fileName.lastIndexOf('.') + 1);
    }

    $(".material-list-upload-btn").attr("disabled", true);
   

    if (fileExt == "xls" || fileExt == "xlsx" || fileExt == "csv") {
        $(modalId + " #import-material-list-form").submit();
    } else {
    	$('.preload-cover').removeClass('loader-active');
        $('.preload-cover .preload-spinner').hide();   
        $(modalId + " #errorIncorrect").show();
    }

    $('.preload-cover').removeClass('loader-active');
    $('.preload-cover .preload-spinner').hide();
    $(".material-list-upload-btn").attr("disabled", false);

}

function getIds(itemIds){
    var items = itemIds.split(",");
    var ids = [];
    if(items && items.length > 0){
        for(var i = 0;i < items.length;i++){
            ids.push(items[i].split(":")[0]);
        }
    }
    return ids;
}

function getItems(itemIds){
    var itemsStr = itemIds.split(",");
    var items = {};
    if(itemsStr && itemsStr.length > 0){
        for(var i = 0;i < itemsStr.length;i++){
           var itemSplt = itemsStr[i].split(":");
           items[itemSplt[0]] = itemSplt[1];
        }
    }
    return items;
}


function checkPriceSelected() {
	$('#removeMaterial').popover('destroy');
	$("#errorsSelected").hide();
	hideUpdateAllItemsSuccessMsg();
	
	var itemIds = gatherListChecked();
    var selectAllBoxesCheck = $('#selectAllBoxesCheck')[0];
    if(selectAllBoxesCheck.type === "checkbox" && selectAllBoxesCheck.checked){
        itemIds = getIds($('#allGiftListIds').val());
    }
	if (itemIds.length == 0) {
		setTimeout(function(){
			$('.popover.warn').on('click', function(event) {
				event.preventDefault();
				$('.popover.warn').popover('hide');
			});
		}, 500);
		var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
		if ( isSafari ) {
			$('.popover.warn').remove();
		}
		
		var thisButton = $('#checkPriceSelected');
		$(thisButton).popover({
			template: '<div class="popover warn" role="tooltip">\
						<div class=""></div>\
						<h3 class="popover-title"></h3>\
						<div class="popover-content"></div>\
						<button class="close"><span class="glyphicon glyphicon-remove"></span></button>\
					</div>'
		});

		$(thisButton).popover('show');
		$(thisButton).on('hidden.bs.popover', function () {
			console.log('desctroy');
			$(thisButton).popover('destroy');
		})

	} else {
		var qtyStr = "";
		if(selectAllBoxesCheck.type === "checkbox" && selectAllBoxesCheck.checked){
		    var items = getItems($('#allGiftListIds').val());
            for (i = 0; i < itemIds.length; i++) {
                if (i == 0)
                    qtyStr = items[itemIds[i]];
                else
                    qtyStr += "," + items[itemIds[i]];
            }
        }else{
            for (i = 0; i < itemIds.length; i++) {
                if (i == 0)
                    qtyStr = $("#" + itemIds[i] + "_qty").val();
                else
                    qtyStr += "," + $("#" + itemIds[i] + "_qty").val();
            }
        }

		$("#paSkuIdsList").val(itemIds);
		$("#paQtyList").val(qtyStr);

		var nonSelectedItemsMap = getNonSelectedMLItemsMap();

        dataString = $("#material-details-check-price").serialize();
		$.ajax({
			type: "POST",
			data: dataString,
			dataType: "json",
			success: checkPriceSelectedSuccess(nonSelectedItemsMap)
		});
	}
}

function getNonSelectedMLItemsMap() {
    var itemIds = {};
    var elem = document.getElementsByTagName('input');
    var inputs = [], att;
    for (i = 0, iarr = 0; i < elem.length; i++) {
        att = elem[i].getAttribute("name");
        if (att && att.includes("priceInfo_")) {
            inputs[iarr] = elem[i];
            iarr++;
        }
    }

    for (var i = inputs.length - 1; i >= 0; i--) {
        if (inputs[i].type === "hidden") {
            var fullName = inputs[i].id;
        	var giftItemId = fullName.substring(fullName.lastIndexOf('_') + 1);
            var checkboxInput = $("input[type='checkbox'][id="+giftItemId+"]")[0];

            if (!checkboxInput.checked) {
            	var itemId = inputs[i].value;
            	var tdRow = document.getElementById('avlbt_' + itemId);
                var itemAvailability = tdRow.getElementsByTagName("span")[0].innerHTML;
            	var itemPrice = document.getElementById('price_' + itemId).innerHTML;
                var rowValue = [];

                rowValue.push(itemAvailability, itemPrice);
                itemIds[itemId] = rowValue;
            }
        }
    }

    return itemIds;
}

function addSelected() {
    if ($('#error-message-connection-JDE').css('display') !== 'none') {
    	return;
    }

	$('#removeMaterial').popover('destroy');
	$("#errorsSelected").hide();
	$(".error-msg").hide();
	hideUpdateAllItemsSuccessMsg();
	
	var itemIds = gatherListChecked();
    var selectAllBoxesCheck = $('#selectAllBoxesCheck')[0];
    if(selectAllBoxesCheck.type === "checkbox" && selectAllBoxesCheck.checked){
        itemIds = getIds($('#allGiftListIds').val());
    }
	if (itemIds.length == 0) {
		setTimeout(function(){
			$('.popover.warn').on('click', function(event) {
				event.preventDefault();
				$('.popover.warn').popover('hide');
			});
		}, 500);
		var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
		if ( isSafari ) {
			$('.popover.warn').remove();
		}
		
		var thisButton = $('#addSelected');
		$(thisButton).popover({
			template: '<div class="popover warn" role="tooltip">\
							<div class=""></div>\
							<h3 class="popover-title"></h3>\
							<div class="popover-content"></div>\
							<button class=close><span class="glyphicon glyphicon-remove"></span></button>\
						</div>'
		});

		$(thisButton).popover('show');
		$(thisButton).on('hidden.bs.popover', function () {
			$(thisButton).popover('destroy');
		})
	} else {
		var isCCAccountAccepted = $('#isAcceptedCCPayment').val();
		var isCCNotificationShown = $('#isCCNotificationShown').val();
		if(isCCAccountAccepted=='false' && isCCNotificationShown=='true'){
			$('#ccAccountNotificationModal').modal('show');
		} else {
		var qtyStr = "";
        if(selectAllBoxesCheck.type === "checkbox" && selectAllBoxesCheck.checked){
            var items = getItems($('#allGiftListIds').val());
            for (i = 0; i < itemIds.length; i++) {
                if (i == 0)
                    qtyStr = items[itemIds[i]];
                else
                    qtyStr += "," + items[itemIds[i]];
            }
        }else{
            for (i = 0; i < itemIds.length; i++) {
                if (i == 0)
                    qtyStr = $("#" + itemIds[i] + "_qty").val();
                else
                    qtyStr += "," + $("#" + itemIds[i] + "_qty").val();
            }
        }
		$("#addSkuIdsListGL").val(itemIds);
		$("#addQtyListGL").val(qtyStr);
		//$("#giftIds").val(true);
		//$("#material-details-add").submit();

		dataString = $("#material-details-add").serialize();
		$.ajax({
			method: "POST",
			cache: false,
			data: dataString,
			dataType: "json",
			success: function (data) {
                if (null !== data && typeof data.errors !== "undefined" && data.errors.length > 0 && data.error === "true") {
                	$("#addToCartErrorOccured").show();
                    showErrorsOnAddToCart(data, "", itemIds, "");
				} else {
					return $.when(searchListDetails(), reloadMinicartCount(), addedToCartHangerShow());
				}
			}
		});
	}
	}
}

function addItemToCart(item){
	$("#addSkuIdsList").val(item);
	$("#addQtyList").val($("#"+item+"_qty").val());
	$("#giftIds").val(true);
	$("#reorder-details-add").submit();
}

function ajaxError (jqXHR, textStatus, errorThrown){
	alert(jqXHR+", "+textStatus+", "+errorThrown)
}

function showErrorsId(data, id) {
	var errorMessage = "";
	var propertiesArray = new Array();
	for (var i = 0; i < data.errors.length; i++){
		if (errorMessage == "") {
			errorMessage = data.errors[i];
		} else {
			errorMessage = errorMessage + "<br />" + data.errors[i];
		}
		propertiesArray.push(data.properties[i]);
	}
	if (errorMessage != "") {
		openErrorMessageId(errorMessage, id);
	}
}

// Shows the error message
function openErrorMessageId(message, id) {
	$(""+id).html(message);
	$(""+id).show();
}


// download material list
function exportMaterialList(giftlistId) {
	$("#exportList" + giftlistId).submit();
}

$(document).ready(function() {
	$('.importHelpNML').click(function() {
    	$("#importHelpNML").toggle();
    	$('#newMaterialListLeft').toggleClass('col-lg-6 col-md-6');
    	$('#newMaterialListDocument').toggleClass('modal-narrow').toggleClass('modal-lg');
    });
	
	importHelpInit();
	/* SM-358 update all material list item */
	disableUpdateAllButton();
	updateAllItemsQty();
	
});

function importHelpInit() {
	$('.importHelpMLlink').click(function() {
    	$(".importHelpML").toggle();
    	$('.importMaterialListLeft').toggleClass('col-lg-6 col-md-6');
    	$('.importMaterialListDocument').toggleClass('modal-narrow').toggleClass('modal-lg');
    });
}

/* SM-358 Update all material list items*/
$(window).load(function () {
	enableUpdateAllButton();
});

function updateAllItemsQty(){
    var itemsToUpdate;
    hideUpdateAllItemsSuccessMsg();
    $('button#updateAllItemQty').on('click', function() {
	
		itemsToUpdate = $("input.input-qty").map(function () {
	        var giftItemId = $(this).attr('name');
	        var qty = $(this).val();
	        return giftItemId + "=" + qty;
	    }).get().join(",");
		
		hideUpdateAllItemsSuccessMsg();
	    $("#update-all-ml-item-qty").val(itemsToUpdate);
	    
	    dataString = $("#update-all-ml-items-qty").serialize();
	    $.ajax({
		    type: "POST",
		    data: dataString,
		    dataType: "json",
	       /* success: updateMLItemsSuccess,*/
		    success: function(data){
                updateMLItemsSuccess(data);
                if ('success' === data.code) {
                updateAllList();
                }
    
            },
	        error: updateMLItemsFailure
	    });
    });
    $('button#updateAllItemQtyBottom').on('click', function() {
    	$('#updateAllItemQty').trigger('click');
    });
    
}

function updateMLItemsSuccess(data){
	var itemIds = getIds($('#allGiftListIds').val());
	if ('success' === data.code) {
		$("#updateAllItemsSuccess").show();
		removeHighlightErrors(itemIds);
		disableUpdateAllButton();
    } else {
    	hideUpdateAllItemsSuccessMsg();
        removeHighlightErrors(itemIds);
    	highlightErrors(data.properties);
        var propertiesArray = [];
        var errorsArray = [];
        for (var i = 0; i < data.errors.length; i++) {
            errorsArray.push(data.errors[i]);
            propertiesArray.push(data.properties[i]);
        }
        for (var i = 0; i < propertiesArray.length; i++) {
            var errorMessageSmall = $("#error-qty-" + propertiesArray[i]);
            errorMessageSmall.html(errorsArray[i]);
            errorMessageSmall.show();
            $("#error-qty-"+propertiesArray[i]).show();
            $("#"+propertiesArray[i]+"_qtycaption").attr("style","");
            errorMessageSmall.parent().addClass("has-error");
        }
    }
}

function updateMLItemsFailure(x, s, e) {
    console.log("xhr: " + x + "; s: " + s + "; e: " + e);
}

function removeHighlightErrors(itemIds) {
	for (var i=0; i<itemIds.length; i++){
    	var errorQty = $("#error-qty-"+itemIds[i]+"_qty");
    	errorQty.html("");
    	errorQty.parent().removeClass("has-error");
    	errorQty.hide();
        $("#"+itemIds[i]+"_qty").removeClass("has-error");
	    $("#"+itemIds[i]+"_qtyDiv").removeClass("has-error");
    	$(".error-msg").hide();
	}
}

function hideUpdateAllItemsSuccessMsg() {
	$("#updateAllItemsSuccess").hide();
}

function disableUpdateAllButton() {
	$("#updateAllItemQty,#updateAllItemQtyBottom").attr("disabled", true);
}

function enableUpdateAllButton() {
	$("input.input-qty").on('change keyup paste', function() {
		$("#updateAllItemQty,#updateAllItemQtyBottom").attr("disabled", false);
	});
}
