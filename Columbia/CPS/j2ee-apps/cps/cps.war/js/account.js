$(document).ready(function() {
	$('ul.account-nav li:nth-child(2n), ul.address-list li:nth-child(3n)').after('<li class="clear">&nbsp;</li>');
	$('input#reg-business-show').click(showBizOptions).ready(showBizOptions);

	$('#primaryPhoneExt').focus(function(){
		if($(this).val() == 'Ext.'){
			$(this).val('');
		}
	}).blur(function(){
		if ($(this).val() == ''){
			$(this).val('Ext.');
		}
	});
	$('#alternatePhoneExt').focus(function(){
		if($(this).val() == 'Ext.'){
			$(this).val('');
		}
	}).blur(function(){
		if ($(this).val() == ''){
			$(this).val('Ext.');
		}
	});

    $("#emailform").submit(function (event ) {
        event.preventDefault();
        doSignUp();
    });
    $(document).on('click','#modal_body #btnUpdateOrderDetails',function() {
        updateApprovedPONumber($('#hiddenOrderId').val());
    });
    $(document).on('click','#modal_body #btnEditOrderDetails',function() {
    	$('.viewOrderDetails').hide();
        $('.editOrderDetails').show();
        $('#btnEditOrderDetails').hide();
        $('#btnUpdateOrderDetails').show();
    });
    $(document).on('click','#btnUpdateUser',function() {
    	if(validateUpdateUser()){
    		updateUser();
    	}
    });
    $(document).on('click','#modal_body #btnApproveOrderDetails',function(event) {
    	$('.modalViewOrder').modal('hide');
    	$("#btnUpdateOrderDetails").trigger( "click" );
    	showApproveOrderModalPopup($(event.target).data("orderId"));
        return false;
    });
    $(document).on('click','#modal_body #btnRejectOrderDetails',function(event) {
    	 $('.modalViewOrder').modal('hide');
    	 showRejectOrderModalPopup($(event.target).data("orderId"));
         return false;
    });
    $(document).on('click','#btnCreateUser',function() {
    	createUser();
    });
    $(document).on('click','#modalBtnApprove',function(event) {
    	approveOrder($(event.target).data("eventClickParam0"));
    });
    $(document).on('click','#modalBtnReject',function(event) {
    	rejectOrder($(event.target).data("eventClickParam0"));
    });
    $('input[name=radioEx2]').bind('click mousedown', (function() {
        var isChecked;
        return function(event) {
            if(event.type == 'click') {
                if(isChecked) {
                    isChecked = this.checked = false;
                } else {
                    isChecked = true;
                }
        } else {
            isChecked = this.checked;
        }
    }})());
    
    $('#addressConfirm').on('hide.bs.modal', function () {
		location.reload(true);
    });
    $('#addressConfirmChange').on('hide.bs.modal', function () {
    	$("#shipAddressSubmit").off( "click");
    });
});

var csGlobalSearchTerm = '';

function showBizOptions() {
	if ($("#reg-business-show").attr("checked")) {
		$("#reg-business").show();
	} else {
		$("#reg-business").hide();
	}
}

function loadErrors(errorCode){
	$("#"+errorCode).addClass("has-error");
}

function refreshCaptchaAudio(){
	var sound = $("#captchaaudio")[0];
	sound.load();
	sound.play();
}

function refreshCaptchaImage(){
	d =new Date();
	$("#captchaimg").attr("src", "/account/captcha/simpleCaptcha.png?"+d.getTime());
}


var sortId=false;
var sortCS=false;
var sortDate=true;
var sortTotal=false;
var sortStatus=false;
var sort = 'date';
var lastSortAccend = false;

function submitProfileUpdate() {

	clearHighlighting();
	hideErrors();
	hideSuccess();
	closeErrorMessage();

	dataString = $("#update-profile").serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: function(data){
			if (data.error == "false"){
				$(".alert-success").show();
			} else if (data.error == "true"){
				showErrors(data);
			}
		}
	});
}

function handleCreateUser() {
	var dataString;
	var hasAccount = $('[name="reg-status"]:checked').val() === "hasaccount";
	
	if(hasAccount) {
		$("#createUserForm_firstName").val($("#firstName").val());
		$("#createUserForm_lastName").val($("#lastName").val());
		$("#createUserForm_email").val($("#email").val());
		$("#createUserForm_password").val($("#password").val());
		$("#createUserForm_confirmedPassword").val($("#confirmPassword").val());
		$("#createUserForm_newsletterSignUp").val($("#newsletterSignUp").val());
		
		$("#createUserForm_accountNumber").val($("#accountNumber").val());
		$("#createUserForm_zipCode").val($("#zip").val());
		$("#createUserForm_supervisorName").val($("#supervisorName").val());
		$("#createUserForm_supervisorEmail").val($("#supervisorEmail").val());
		dataString = $("#create-user-form").serialize();
		createUser(dataString, "/register/confirmation.jsp");
	} else {
		$("#createGuestUserForm_firstName").val($("#firstName").val());
		$("#createGuestUserForm_lastName").val($("#lastName").val());
		$("#createGuestUserForm_email").val($("#email").val());
		$("#createGuestUserForm_password").val($("#password").val());
		$("#createGuestUserForm_confirmedPassword").val($("#confirmPassword").val());
		$("#createGuestUserForm_newsletterSignUp").val($("#newsletterSignUp").val());
		
		$("#createGuestUserForm_companyName").val($("#companyName").val());
		$("#createGuestUserForm_phoneNumber").val($("#phoneNumber").val());
		$("#createGuestUserForm_companyBillingAddress1").val($("#address1").val());
		$("#createGuestUserForm_address2").val($("#address2").val());
		$("#createGuestUserForm_city").val($("#city").val());
		$("#createGuestUserForm_state").val($("#state").val());
		$("#createGuestUserForm_zipCode").val($("#zipCode").val());
		dataString = $("#create-guest-user-form").serialize();
		createUser(dataString, "/register/confirmation.jsp?appNeed=true");
	}
	return false;
}

function createUser(dataString, path) {
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: function(data){
			if ('success' == data.code){
				if (path != "") {
					formDataLayerPush("create-user-form");
					document.location.href = path;
				}
			} else if ('error' == data.code){
				var errorMessagesDiv = $("#error-messages-create-user");
				loadErrorsForUserCreation(data, errorMessagesDiv);
			}
		}
	});
}

function loadErrorsForUserCreation(data, target) {
	var errorMessage = "";
	var propertiesArray = new Array();
	var errorCount = 0;
	for ( var i = 0; i < data.errors.length; i++) {
		if (data.errors[i] != "") {
			errorCount++;
			errorMessage = errorMessage + 
			'<div class="alert alert-danger">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">\
				<span aria-hidden="true">\
					<span class="glyphicon glyphicon-remove-circle"></span>\
				</span>\
			</button>'+
			data.errors[i] + 
			"</div>";
		}
	}
	if (errorMessage != "") {
		$(target).html(errorMessage);
		$(target).show();
		for ( var i = 0; i < data.properties.length; i++) {
			if (data.properties[i] != undefined
					&& data.properties[i].length > 0) {
				propertiesArray.push(data.properties[i]);
			}
		}
		$(".has-error").removeClass("has-error");
		var prefix = "#";
		for (i = 0; i < propertiesArray.length; i++) {
			$(prefix + propertiesArray[i]).parent().addClass("has-error");
		}
	}
}

function clearErrorsForUserCreation() {
	var errorMessagesDiv = $("#error-messages-create-user");
	$(errorMessagesDiv).html("");
	$(".has-error").removeClass("has-error");
}

function handleLogout(){
	if (logoutAllow) {
		var dataString = $('#form-logout').serialize();
		$.ajax({
			url: '/',
			type: "POST",
			data: dataString,
			dataType: "json",
			success: function(response) {
				document.location.href = "/";
			},
			error: function(response) {
				showGenericError(response);
			}
		});
	}
	return false;
}

function handleSessionTimeout(){
	var dataString = $('#form-logout').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: function(response) {
			$("#link-sessionIdleModal").click();
		},
		error: function(response) {
			showGenericError(response);
		}
	});
	return false;
}

function handleLogin(suffix){
    if(typeof suffix === 'undefined'){
        suffix = '';
    }
	clearHighlighting(suffix);
	
	var norelogin=$('#norelogin').val();
 	var dataString = $('#login-form' + suffix).serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: function(loginData){
			if( isProductShouldBeAddedToCart() && loginData.code == 'success') {
				handleLoginSuccessAddProductToCart(norelogin, loginData, suffix);
			} else {
				handleLoginSuccess(norelogin, loginData, suffix);
			}
		},
		error: ajaxErrorLogin
	});
	return false;
}

function handleLoginSuccess(norelogin, data, suffix) {
	if(norelogin){
		loginSuccessNoRedirect(data, suffix);
	} else {
		loginSuccess(data, suffix);
	}
}

function isProductShouldBeAddedToCart() {
	return $("#modal-login-productId").val() != '';
}

function handleLoginPage(){
	clearHighlighting();
	var dataString = $('#login-form-page').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: loginSuccessPage,
		error: ajaxErrorLogin
	});
	return false;
}

function showOnHold(showCS) {

	$.ajax({
		type: "GET",
		url: "/global/modals/modal-on-hold.jsp?showCS="+showCS,
		dataType: 'html',
		data: ({action: 'loadModalDiv'}),
		complete: function () {
			$("#link-onHoldAccount").click();
		},
		success: function (data) {
			data = $.trim(data);
			$("#modal-on-hold-show-div").html(data);
		}
	});

}

function showSelectShippingAddress() {

	openCSModal();
	//$.ajax({
	//	type: "GET",
	//	url: "/global/modals/modal-cs.jsp",
	//	dataType: 'html',
	//	data: ({action: 'loadModalDiv'}),
	//	complete: function () {
	//		$("#link-selectShipAddress").click();
	//	},
	//	success: function (data) {
	//		data = $.trim(data);
	//		$("#modal-cs-show-div").html(data);
	//	}
	//});

}
function loginSuccessNoRedirect(data, suffix){
	if (typeof suffix === 'undefined'){
		suffix = '';
	}
	if ('success' == data.code) {
		var url = document.location.href;
		var indexStr = url.indexOf('&loadSection=inner');
		if (indexStr != -1) {
			url = url.substr(0, indexStr);
		}

		var delimeter = '?';
		if(url.indexOf('?') !== -1)
			delimeter = "&";

		if (data.showCS) {
			url += delimeter + "showCS=true";
		}
        var modal = $('#showModal').val();
        if (modal != null){
            url += delimeter + "open=" + modal;
        }
		if (data.onHold) {
			url += "&onHold=true";
		}
		if (data.redirect){
			url = data.redirectUrl;
		}
		document.location.href = url;
	} else {
		showErrors(data, $('#logIn' + suffix + ' #error-messages' + suffix));
		if (data.showCaptcha){
			expireRecaptchaDefault();
			$("#login-form-captcha" + suffix).show();
		}
		checkEnableLoginButton(suffix);
	}
}

function handleLoginSuccessAddProductToCart(norelogin, loginData, suffix){
	var dataString = $('#modal-login-addToCart').serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: function(data) {
			console.log("product is added to cart");
			handleLoginSuccess(norelogin, loginData, suffix);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("unable to add product to cart");
			handleLoginSuccess(norelogin, loginData, suffix);
		}
	});
}

window.defaultLoginUrl = "/";

function loginSuccess(data, suffix){
	if (typeof suffix === 'undefined'){
		suffix = '';
	}
	if ('success' == data.code) {
		var url = document.location.href;
		if (url.indexOf("browse") != -1 || url.indexOf("search") != -1) {
			var indexStr = url.indexOf('&loadSection=inner');
			if (indexStr != -1) {
				url = url.substr(0, indexStr);
			}
			if (data.showCS) {
				url += "&showCS=true";
			}
			if (data.onHold) {
				url += "&onHold=true";
			}
		} else {
			url = window.defaultLoginUrl;
			
			var originalURL =$.trim($('#originalURL').val());
			if(originalURL != '' && originalURL.length != 0){					
				url=originalURL;
			}
			var sep = "";
			if (data.showCS) {
				url += "?showCS=true";
				sep = "&"
			} else {
				sep = "?"
			}
			if (data.onHold) {
				url += (sep + "onHold=true");
			}
		}

		if (data.redirect){
			url = data.redirectUrl;
		}
		document.location.href = url;
	} else {
		showErrors(data, $('#logIn' + suffix + ' #error-messages' + suffix));
		if (data.showCaptcha){
			if(cps_login_captcha_default == null){
			  checkEnableLoginModalButton();
			  renderCaptchaCallbacklogin();
			  }else{
		    expireRecaptchaDefaultlogin();
			  }
		    $("#login-form-captcha" + suffix).show();
		    
		}
		
	}
}

function loginSuccessPage(data) {
	if ('success' == data.code) {
		var url = document.location.href;
		var indexStr = url.indexOf('&loadSection=inner');
		if (indexStr != -1) {
			url = url.substr(0, indexStr);
		}
/*
		url += "&showCS=true";
		if (data.onHold) {
			url += "&onHold=true";
		}
*/

		if (data.redirect) {
			url = data.redirectUrl;
		}
		document.location.href = url;
	} else {
		showErrors(data, $('#logInPage #error-messages'));
		if (data.showCaptcha) {
			expireRecaptchaPage();
			$("#login-form-page-captcha").show();
		}
		checkEnableLoginPageButton();
	}
}

function rememberMeClick(checkbox) {
	var label = checkbox.parentNode;
	var rememberMe = $("#change-password-rememberMe");
	var className = "checked";
	if(label.classList.contains(className)) {
		label.classList.remove(className);
		rememberMe.val(false);
	} else {
		label.classList.add(className);
		rememberMe.val(true);
	}
}

function handleResetPassword() {
	clearHighlighting();
	var dataString = $('#change-password-form').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: 'json',
		success: function(data) {
/*
			console.log('success');
*/
			resetPasswordSuccess(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
/*
			console.log(XMLHttpRequest);
			console.log(textStatus);
			console.log(errorThrown);
*/
			showGenericError(XMLHttpRequest);
		}
	});
}

function requestStatement() {

	var dataString = $('#request-statement-form').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: 'json',
		success: function(data) {
			$("#link-requestStatementModal").click();
			//$("#modal-content").load("/global/modals/modal-request-statement-confirm.jsp");
		}
	});

}

function resetPasswordSuccess(data){
	if ('success' == data.code) {
		document.location.href = "/?rp=true";
	} else {
		var errorMessagesDiv = $("#error-messages-reset-password");
		showErrors(data, errorMessagesDiv);
	}
}

function handleNewUserLogin() {
	clearHighlighting();
	var dataString = $('#new-user-login-form').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: 'json',
		success: function(data) {
			newUserLoginSuccess(data);
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			showGenericError(XMLHttpRequest);
		}
	});
}

function newUserLoginSuccess(data) {
	if ('success' == data.code) {
		document.location.href = "/account/account-landing.jsp";
	} else {
		var errorMessagesDiv = $("#error-messages");
		showErrors(data, errorMessagesDiv);
	}
}

// -------------- contact us --------------

function contactUs_methodChange(checkboxLabel) {
	var checkedClass = "checked";
	$("label[name=contactUs_method]").removeClass(checkedClass);
	checkboxLabel.classList.add(checkedClass);
	$("#cu_input_method").val(checkboxLabel.getAttribute("value"));
}

function contactUs_Cancel() {
	// input controls
	$("#cu_name").val("");
	$("#cu_email").val("");
	$("#cu_company").val("");
	$("#cu_account").val("");
	$("#cu_phone").val("");
	$("#cu_method").val("");
	$("#cu_comments").val("");

	// view controls
	$("#cu_input_fname").val("");
    $("#cu_input_lname").val("");
	$("#cu_input_email").val("");
	$("#cu_input_company").val("");
	$("#cu_input_accountNumber").val("");
	$("#cu_input_phone").val("");
	$("#cu_input_method").val("");
	$("#cu_input_comments").val("");

	window.location.reload(true);
}

function contactUs_InitForm() {
	// view controls
	$("#cu_lname").val($("#cu_input_fname").val());
	$("#cu_fname").val($("#cu_input_lname").val());
	$("#cu_email").val($("#cu_input_email").val());
	$("#cu_company").val($("#cu_input_company").val());
	$("#cu_account").val($("#cu_input_accountNumber").val());
	$("#cu_phone").val($("#cu_input_phone").val());
	$("#cu_subject").val($("#cu_input_subject").val());
	$("#cu_method").val($("#cu_input_method").val());
	$("#cu_comments").val($("#cu_input_comments").val());
}

function contactUs_Submit() {

	closeAllMessages();
	contactUs_InitForm();

	var dataString = $('#contact-us-form').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: 'json',
		success: function(response) {
			if ('success' == response.code) {
				contactUs_SubmitSuccess();
			} else {
				contactUs_SubmitError(response);
			}

		},
		error: function(response, textStatus, errorThrown) {
			ajaxErrorResponse();
		}
	});
}

function contactUs_SubmitSuccess() {
	// showInfoMessages("You request is successfully sent. Thank you.")
	$("#link-contactUsModal").click();
	setTimeout( function() { document.location.href = "/"; }, 5000);
}

function contactUs_SubmitError(response) {
	showError( response );
}

// -------------- ~contact us~ --------------

// -------------- ~add gift list to cart~ >>> --------------

var canAddItemsToCart = true;

function addGiftlistToCart(addGiftlistToCartFormName, itemsNumber, element) {
	//if (canAddItemsToCart) {
	//	canAddItemsToCart = false;
		//hideAllMessages('false');
		var options = {
			type : "POST",
			dataType : "json",
			success : function(data){
				if (null != data && typeof data.errors != "undefined" && data.error == "true") {
					var errorDiv = "add-item-error-message";
					showErrorsOnAddToCart(data, errorDiv);
				} else if (itemsNumber > 0) {
                    reloadMinicartCount();
					// addedToCartHangerShow();
					addedToCartWithRelatedHangerShow();
				} else {
					setTimeout(function(){
						$('.popover.warn').on('click', function(event) {
							event.preventDefault();
							$('.warn-popover').popover('hide');
							$('.popover.warn').popover('hide');
						});
					}, 500);
					var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || safari.pushNotification);
					if ( isSafari ) {
						$('.popover.warn').remove();
					}

					var thisButton = $(element);

					var placement = 'right';
					if ($(window).width() < 992) {
						placement = 'bottom';
					}
					$(thisButton).attr('data-placement', placement);
					$(thisButton).attr('data-trigger', 'focus');
					$(thisButton).popover({
						template: 
							'<div class="popover warn" role="tooltip" style="width: 250px"> \
								<div class=""></div> \
								<h3 class="popover-title"> \
								</h3> \
								<div class="popover-content"></div> \
								<button class=close><span class="glyphicon glyphicon-remove"></span></button> \
							</div>'
					});
					$(thisButton).popover('show');
					$(thisButton).on('hidden.bs.popover', function () {
						$(thisButton).popover('destroy');
					});
				}
				
			},
			error: function(){
				//setTimeout(1000);
/*
				setTimeout(function() {
					canAddItemsToCart = true;
				}, 1000);
*/
			}
		};
		var isCCAccountAccepted = $('#isAcceptedCCPayment').val();
		var isCCNotificationShown = $('#isCCNotificationShown').val();
		if(isCCAccountAccepted=='false' && isCCNotificationShown=='true'){
			$('#ccAccountNotificationModal').modal('show');
		} else {
			var form = $("#"+addGiftlistToCartFormName);
			form.ajaxSubmit(options);
		}
	//}
}


function addToCartResponse(data, isModal){
	 //location.href = "/checkout/cart.jsp?qo=t";
/*
	if (isError(data)) {
		showErrorMessages(data, isModal);
	} else if (isSuccess(data)) {
		reloadAndExpandMinicart();
		if ('true' == isModal) {
			$("#containerModalLarge").modal('hide');
		}
		setTimeout(function(){  minicartContract(); }, 2000 );
		reloadCartContent();
	}
*/

/*
	setTimeout(function() {
		canAddItemsToCart = true;
	}, 1000);
*/
}

// -------------- ~add gift list to cart~ <<< --------------

// View Addresses
function resetPage(isReset){
	if (isReset){
		location.reload(true);
	} else{

	}

}


function searchCS(){
    //$("#emptyResults").hide();
    var searchTerm = $("#csListSearchTerm").val();
    //searchCS = $("#reorderListCSTerm").val();
    //if (searchTerm == 'Name, Item, Keyword'){
    if (searchTerm == 'Search for Ship To Address'){
        searchTerm = '';
    }
    enableShipToAddressResetButton();
    csGlobalSearchTerm = searchTerm;

    $("#addresses-list").load("/account/gadgets/addresses-list-display.jsp?searchTerm="+encodeURIComponent(csGlobalSearchTerm), function () {
        eventsAddressListDisplay();

    });
}

// ----------------- MATERIAL LISTS -----------------

function materialList_showNew() {
	$("#new-material-list-form").show();
}

// -- add item

function materialList_addItemToList(prodId) {
    hideErrorModalMessages();

	$("#input-ml-a-giftlist").val( $("#control-ml-giftlist").val() );
	var qty = $("#input-ml-a-qty").val();

	var dataString = $('#add-to-material-list-form').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: 'json',
		success: function(response) {
			if ('success' == response.code) {
				materialList_showAddedItem(
					response, prodId, qty
				);
			} else {
				materialList_addItemToListError(response);
			}
		},
		error: function(response, textStatus, errorThrown) {
			ajaxModalErrorResponse();
		}
	});
}

function materialList_addItemToListError(response) {
	if (typeof response.properties != 'undefined') {
		highlightErrorInputs(response.properties);
	}
	showErrorModalMessages( getErrorMessageFromResponse(response) );
}


// -- create and add item

function showNewMaterialListModalPopup() {
    var file = document.getElementById("importFile");
    file.value = file.defaultValue;
    $('.inputfile').next().next('label').find('span').html("");
    $("#control-ml-name").val("");
    $("#control-ml-description").val("");
    $("#modal-message-error-div").hide();
    $("#link-newMaterialListModal").click();
    $("#newMLFileError").hide();
//    $("#newMLEmptyFileError").hide();
}

function materialList_createNew(prodId) {
    hideErrorModalMessages();

	$("#input-ml-ca-eventName").val( $("#control-ml-name").val() );
	$("#input-ml-ca-description").val( $("#control-ml-description").val() );
	var qty = $('#input-ml-ca-qty').val();

	var dataString = $('#create-add-to-material-list-form').serialize();

	$.ajax({
		type: "POST",
		data: dataString,
		dataType: 'json',
		success: function(response) {
			if ('success' == response.code) {
				materialList_showAddedItem(response, prodId, qty);
			} else {
				materialList_createNewError(response);
			}

		},
		error: function(response, textStatus, errorThrown) {
			ajaxModalErrorResponse();
		}
	});
}

function materialList_createNewError(response) {
	if (typeof response.properties != 'undefined') {
		highlightErrorInputs(response.properties);
	}
	showErrorModalMessages( getErrorMessageFromResponse(response) );
}

function materialList_showAddedItem(response, prodId, qty) {
	$.ajax({
		type: "GET",
		url: "/global/modals/add-to-material-list-added-content.jsp",
		dataType: 'html',
		data: ({
			prodId: prodId,
			giftListId: response.giftListId,
			giftItemId: response.giftItemId,
			qty: qty
		} ),
		complete: function () {
			$("#add-to-material-list-body1").hide();
			$("#add-to-material-list-body1-footer").hide();
			$("#add-to-material-list-body2").show();
			$("#add-to-material-list-body2-footer").show();
		},
		success: function (data) {
			data = $.trim(data);
			$("#add-to-material-list-body2").html(data);
		}
	});
}



/*** Multi create and add */

function materialList_addItemsToList() {
    hideErrorModalMessages();

    $("#input-ml-m-a-giftlist").val($("#control-ml-giftlist").val());

    var dataString = $('#multi-add-to-material-list-form').serialize();

    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function(response) {
            console.log(response);
            if ('success' === response.code) {
                var options = {
                    giftItemIds:response.giftItemIds.join(","),
                    itemsCount:response.itemsCount,
                    giftListId:response.giftListId,
                    addedSkuCount:response.giftItemIds.length
                };
                materialList_showAddedItemsModal(options);
            } else {
                materialList_addItemToListError(response);
            }
        },
        error: function(response, textStatus, errorThrown) {
            ajaxModalErrorResponse();
        }
    });
}

function materialList_createAddItems() {
    hideErrorModalMessages();

    $("#input-ml-m-ca-eventName").val( $("#control-ml-name").val() );
    $("#input-ml-m-ca-description").val( $("#control-ml-description").val() );

    var dataString = $('#create-multi-add-to-material-list-form').serialize();

    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function(response) {
            if ('success' == response.code) {
                var options = {
                    giftItemIds:response.giftItemIds.join(","),
                    itemsCount:response.itemsCount,
                    giftListId:response.giftListId,
                    addedSkuCount:response.giftItemIds.length
                };
                materialList_showAddedItemsModal(options);
            } else {
                materialList_createNewError(response);
            }

        },
        error: function(response, textStatus, errorThrown) {
            ajaxModalErrorResponse();
        }
    });
}

function materialList_showAddedItemsModal(response) {
	document.getElementById("modal-select").className = "modal-dialog";
    $.ajax({
        type: "GET",
        url: "/global/modals/modal-multi-content-added-to-material-list.jsp",
        dataType: 'html',
        data: response,
        complete: function () {
            $("#add-to-material-list-body1").hide();
        },
        success: function (data) {
            data = $.trim(data);
			$("#add-to-material-list-body1-footer").show();
			$("#add-to-material-list-body2").show();
            $("#add-to-material-list-body2").html(data);
            eventsModalMultiContent();
			jQuery('.scrollbar-inner').scrollbar();
        }
    });
}

function showErrors(data){
	var errorMessage = "";
	var propertiesArray = [];
	var errorsArray = [];
	for(var i=0; i<data.errors.length; i++){
		if (indexOf(errorsArray,data.errors[i]) == -1){
			errorsArray.push(data.errors[i]);
			if (errorMessage == ""){
				errorMessage = data.errors[i];
			}else{
				errorMessage += "<br/>" + data.errors[i];
			}
		}
		propertiesArray.push(data.properties[i]);
	}
	if (errorMessage != ""){
		openErrorMessage(errorMessage);
		//highlightErrors(propertiesArray);
	}
}

function showErrors(data, divTag){
	var errorMessage = "";
	var propertiesArray = [];
	var errorsArray = [];
	for(var i=0; i<data.errors.length; i++){
		if (indexOf(errorsArray,data.errors[i]) == -1){
			errorsArray.push(data.errors[i]);
			if (errorMessage == ""){
				errorMessage = data.errors[i];
			}else{
				errorMessage += "<br/>" + data.errors[i];
			}
		}
		propertiesArray.push(data.properties[i]);
	}
	if (errorMessage != ""){
		openErrorMessageDiv(errorMessage, divTag);
		//highlightErrors(propertiesArray);
	}
}

function openErrorMessage(errorMessage){
	var errorMessagesDiv = $("#error-messages");
	errorMessagesDiv.html(errorMessage);
	errorMessagesDiv.show();
}

function openErrorMessageDiv(errorMessage, divTag){
	divTag.html(errorMessage);
	divTag.show();
}

function duplicateAddToMaterialList(){
	var productchoices = document.getElementsByClassName('dupematerialproduct');
	//var quantities = [];
	var quantitychoices = document.getElementsByName('dupematerialqty');
	var pclength=productchoices.length;
	var itemIds = "";
	var qtyStr = "";
	var count = 0;
	var checkedcount=0;
	for(var i=0;i<pclength;i++){
		if(productchoices[i].checked){
			checkedcount++;
		}
	}
	for(var i=0;i<pclength;i++){
 		if(productchoices[i].checked){
		var elementid=productchoices[i].value; //product id
		var qty=quantitychoices[i].value; // qty for product
		itemIds = itemIds.concat(elementid);
        qtyStr=qtyStr.concat(qty);
		if( ++count < checkedcount ){
			itemIds = itemIds.concat(",");
			qtyStr = qtyStr.concat(",");
		}
 		}
		
	}
	$("#dupeMaterialSkuIdsList").val(itemIds);
	$("#dupeMaterialQtyList").val(qtyStr);
	$("#dupeAddToMaterial").submit();
}

function doSignUp() {
    var dataString = $('#emailform').serialize();

    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: function(data){
            if (data.code == "success"){
                $("#modal-emailsignup").modal('show');
                $("#emailFormError").hide();
                $('#emailform .form-group').removeClass("has-error");
            } else {
                //$("#emailFormError").html(data.showError);
                showErrors(data, $("#emailFormError"));
                $('#emailform .form-group').addClass("has-error");
            }
        }
    });
    return false;
}

function materialListDisplay(isSearch){
    $(document).ready(function () {
        if($("#total_count").val() == 0 && isSearch) {
            showNoResultsModalPopup();
        }
        var isAvailable = $("#isAvailablePricesWebService").val();
        priceAvailableMsg(isAvailable);
        if (isAvailable === 'false') {
            $('.container.main-content').css('padding-top', '30px');
        }
        loadPagination();
    });
}

function doCS_CheckShippingAddress(checkboxLabel) {
    $("#selected_shipping_id").val(checkboxLabel.getAttribute("value"));
    $("#selected_default_shipping_id").val(checkboxLabel.getAttribute("value"));
    showConfirmPopup("Change Shipping Address");
}

function showConfirmPopup(modal){
	$("#addressConfirmChange .modal-title").text();
	$("#addressConfirmChange").modal("show");
     $("#shipAddressSubmit").on("click", function(e){
        e.preventDefault();
    	doCS_SelectShippingAddress();
    	$( "#shipAddressSubmit").unbind( "click" );
	});
    $("#shipAddressCancel , #addressConfirmChange .close").on("click", function(e){
        e.preventDefault();
        AddressModalClose();
    });
}

function addressesListDisplay() {
    if ($("#emptyResult").val() == 'true') {
        $(document).ready(function () {
            showNoResultsModalPopup();
        });
    }
}

function handleCheckFieldData(type, searchId) {
	if (type == 'orderId' || type == 'salesOrderId' || type == 'invoiceId') {
		var value = $(searchId).val();
		$(searchId).val(value.replace(/\D+/g, ""));
		$(searchId).on('input', function (event) {
			this.value = this.value.replace(/\D+/g,'');
		});
	} else {
		$(searchId).off('input');
	}
}

function searchStatementsSearch(event) {
	event.preventDefault();
	var id = $("#billingAddress").val();
	var billAddrRowCount = $("#billingAddress option").size();
	$('section .alert-danger').remove();
	$('section br').remove();
	var validateBillingAddr = validateBillingAddress();
	if (validateBillingAddr == true) {
		if (id != customerNumber) {
			$('section .alert-danger').remove();
			$('section br').remove();
			$("#statements-list").load("/account/gadgets/statements-list-display.jsp?customerNumber="+ id,function(response, status, xhr) {
								if (status == "success") {
									if ($("#errStatement").val() != undefined) {
										$('section .alert-danger').remove();
										$('section br').remove();
										$('section')
												.append(
														"<br/><br/><div class='alert alert-danger'>"
																+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
																+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
																+ "</span></button> "
																+ $("#errStatement").val()
																+ " </div>");
									}
								}
							});
			$("#resetStatementsButton").removeClass("disabled");
		} else {
			$("#resetStatementsButton").addClass("disabled");
		}
	}
}
function validateBillingAddress() {
	var id = $("#billingAddress").val();
	var billAddrRowCount = $("#billingAddress option").size();
	if (id == customerNumber && billAddrRowCount > 1) {
		$('section div #statements-list').remove();
		$('section')
				.append(
						"<br/><br/><div class='alert alert-danger'>"
								+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
								+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
								+ "</span></button> Please select a Billing Account from the Dropdown </div>");
		return false;
	} else if (id == customerNumber && billAddrRowCount == 1) {
		$('section div #statements-list').remove();
		$('section')
				.append(
						"<br/><br/><div class='alert alert-danger'>"
								+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
								+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
								+ "</span></button> Please contact your admin to add Billing Account </div>");
		return false;
	}
	return true;
}

function resetStatementsSearch(){
    $("#statements-list").load("/account/gadgets/statements-list-display.jsp?customerNumber=" + customerNumber);
    $("#resetStatementsButton").addClass("disabled");
    $("#billingAddress").val(customerNumber);
}

function qoPadFileUploadSuccess(data) {
    if (data && data.code === "error") {
        if (isAjaxResponseContainsError(data, "errMinOrderQty")) {
            var loc = location.href;
            if (loc.indexOf("?") === -1) {
                loc += "?";
            } else {
                loc += "&";
            }
            location.href = loc + "invalidskumodal=true";
        } else {
            showErrors(data, $("#qo-pad-file-error"));
            $("#qo-pad-file-error-div").show();
            highlightErrors(data.properties);
        }
    } else {
        location.reload();
    }
}

function isAjaxResponseContainsError(data, error) {
	if (data && data.properties) {
        for (i = 0; i < data.properties.length; i++) {
            if (data.properties[i] === error) {
                return true;
            }
        }
    }
    return false;
}

function getUrlParameters() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function validateField(field, errMsg) {
	if (field.val() == "") {
		var errorMessage = '<div class="alert alert-danger">\
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">\
								<span aria-hidden="true">\
									<span class="glyphicon glyphicon-remove-circle"></span>\
								</span>\
							</button>' + errMsg + "</div>";

		$("#error-messages-request-access").append(errorMessage);
		$("#error-messages-request-access").show();
		
		field.parent().addClass("has-error");
		
		return false;
	}

	return true;
}

function validateFieldEmail(field, errMsg) {
    var accessFormEmail = field.val();
	if (accessFormEmail != "") {
		// fix SM-566 Email addresses with a "+" are VALID
        regex  = /^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
       
        if(!regex.test(accessFormEmail)){
		var errorMessage = '<div class="alert alert-danger">\
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">\
								<span aria-hidden="true">\
									<span class="glyphicon glyphicon-remove-circle"></span>\
								</span>\
							</button>' + errMsg + "</div>";

		$("#error-messages-request-access").append(errorMessage);
		$("#error-messages-request-access").show();
		
		field.parent().addClass("has-error");
		
		return false;
        }
	}

    return true;
}

function requestAccess() {
	$("#error-messages-request-access").empty();
	$("#error-messages-request-access").hide();
	$(".has-error").removeClass("has-error");
	
	var isValid = validateField($("#requestAccessForm_firstName"), "Please enter First Name");
	isValid = validateField($("#requestAccessForm_lastName"), "Please enter Last Name") && isValid;
	isValid = validateField($("#requestAccessForm_phoneNumber"), "Please enter Phone Number") && isValid;
	isValid = validateField($("#requestAccessForm_email"), "Please enter Email") && isValid;
	isValid = validateFieldEmail($("#requestAccessForm_email"), "Please enter Valid Email") && isValid;
	isValid = validateField($("#requestAccessForm_approverFirstName"), "Please enter Contact First Name") && isValid;
	isValid = validateField($("#requestAccessForm_approverLastName"), "Please enter Contact Last Name") && isValid;
	isValid = validateField($("#requestAccessForm_approverPhoneNumber"), "Please enter Contact Phone Number") && isValid;
	isValid = validateField($("#requestAccessForm_approverEmail"), "Please enter Contact Email") && isValid;
	isValid = validateFieldEmail($("#requestAccessForm_approverEmail"), "Please enter Valid Contact Email") && isValid;
	// SM-520 New User Role 
	if ($('[name="requestAccessForm_role_buyer"]:checked').length == 0 && $('[name="requestAccessForm_role_finance"]:checked').length == 0 && $('[name="requestAccessForm_role_admin"]:checked').length == 0) {
		$('[name="requestAccessForm_role_finance"]').addClass('has-error');
		var errorMessage = '<div class="alert alert-danger">\
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">\
								<span aria-hidden="true">\
									<span class="glyphicon glyphicon-remove-circle"></span>\
								</span>\
							</button>Please choice permissions</div>';

		$("#error-messages-request-access").append(errorMessage);
		$("#error-messages-request-access").show();
		
		isValid = false;
	}
	
	if (isValid) {
		var dataString = $('#request-access-form').serialize();
		$.ajax({
			type: "POST",
			data: dataString,
			dataType: "json",
			success: function(data){
				if ('success' == data.code) {
					$('#infoRequestAccessSent').on('hide.bs.modal', function () {
						document.location.href = "/";
				    });
					
					$('#infoRequestAccessSent').modal('show');
				} else if ('error' == data.code){
					var errorMessagesDiv = $("#error-messages-request-access");
					loadErrorsForUserCreation(data, errorMessagesDiv);
				}
			}
		});
	}
}

function goToRegistrationPage() {
	var url= window.location.pathname + window.location.search;
	if ($('#originalURL').val() != undefined) {
		url= window.location.pathname;
	}
    $("#preRegisterPage").val(url);

    var dataString = $('#preRegisterPageHandler').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function () {
            window.location = "/register/registration.jsp";
        },
    });
}

function handleActivateUser() {
	var dataString = $('#activateUser').serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: function(data){
			console.log(data);
			if (data.userActivated) {
				$("#activatedUser").show();
				$('#user_thank').hide();
				$('#confirmationLinks').hide();
			} else {
				$("#message").html("You have confirmed your email succesfully. You will be logged in and redirected to homepage shortly.");
				window.location.href = '/';
			}
		}
	});
}

function AddressModalClose(){
	 var defaultAddress=$("#defaultSelectedAddress").val();
	 if(defaultAddress == ''){
		 $( "input[name=radioEx]").prop( "checked", false );
	 }
	 var address=defaultAddress.split(":");
	 defaultAddress=address[1];
	 $("input[name=radioEx][value=" + defaultAddress + "]").prop('checked', true);
     $("#selected_shipping_id").val(defaultAddress);
     $("#selected_default_shipping_id").val(defaultAddress);
}

function updateApprovedPONumber(orderId) {
    
    var url = window.location.href;
    if (url.indexOf(window.location.pathname) > -1) {
        url = url.substr(url.indexOf(window.location.pathname));
    }

    var dataString = $('#updateOrderDetailsForm').serialize();
    console.log(dataString);
    $.ajax({
        url: $('#updateOrderDetailsForm').attr('action'),
        type: "POST",
        data: dataString,
        dataType: "json",
        success: approvedPONumberSuccess,
        error: approvedPONumberFailure
    });
    return false;
}

function approvedPONumberSuccess(data) {
    if ('success' === data.code) {
    	$('.modalViewOrder').modal('hide');
    }
    else {
        var propertiesArray = [];
        var errorsArray = [];
        for (var i = 0; i < data.errors.length; i++) {
            errorsArray.push(data.errors[i]);
            propertiesArray.push(data.properties[i]);
        }
        for (var i = 0; i < propertiesArray.length; i++) {
            var errorMessageSmall = $("#err-message-" + propertiesArray[i]);
            errorMessageSmall.html(errorsArray[i]);
            errorMessageSmall.show();
            errorMessageSmall.parent().addClass("has-error");

        }
    }
}

function approvedPONumberFailure(x, s, e) {
    console.log("xhr: " + x + "; s: " + s + "; e: " + e);
}

function validateUpdateUser(){
    var editFormValues = [];
	if ($('#first').val().trim() == "") {
		editFormValues.push("enter your first name.");
	}
	if ($('#last').val().trim() == "") {
		editFormValues.push("enter your last name.");
		}
	if ($('#email').val().trim() == "") {
		editFormValues.push("enter your email as your user ID.");
		}
	if ($('#spendingLimit').val().trim() != "" && !$("input[name='radioEx2']").is(':checked')
			&& $("input[id='buyer-role']").is(':checked')) {
		editFormValues.push("select spending limit frequency.");
		}
	if ($('#spendingLimit').val().trim() != "" && $("input[name='radioEx2']").is(':checked') && 
			!$("input[name='check']").is(':checked') 
			&& $("input[id='buyer-role']").is(':checked')){
		editFormValues.push("select at least one approver for user with buyer role.");
		}
    if (editFormValues.length > 0) {
    	$('.editUserError .alert-danger').remove();
    	$("html, body").animate({scrollTop: 0}, "slow");
    	editFormValues.forEach(function(item) {
			throwsEditError(item)
	});
	}else{
        return true;
    }

}

function throwsEditError(message) {
		$('.editUserError').append("<div class='alert alert-danger'>"
	+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
	+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
	+ "</span></button> "
	+ "Please "+message
	+ " </div>");
}
