$(document).ready(function() {
//	$.fn.cpsTab = function(options) {
//		// show tabs closed in small screen mode
//		$(window).smartresize(function() {
//			if($('ul.tabnav', tabElement).is(':visible')) {
//				if($('.tabcontent', tabElement).is(':visible') == false) {
//					tabNav.removeClass('active');
//					$('li', tabNav).removeClass('active');
//					$($('li', tabNav).get(0)).addClass('active');
//					$($('.tabcontent', tabElement).get(0)).addClass('active');
//				}
//			}
//		});
//	};

//	$(function() {
//		//$('#pdpTab').cpsTab({});
//
//		/*Image carousel */
//		var imageCarousel = $('#imageCarousel').jcarousel();
//		imageCarousel.delegate('li img', 'click', function(e) {
//			e.preventDefault();
//			$('#stageImage img').attr('src', $(e.target).attr('src'));
//		});
//
//		imageCarousel.on('jcarousel:reload jcarousel:create', function () {
//			var pageWidth = $(window).width();
//			var width = imageCarousel.innerWidth();
//			if (pageWidth >= 760) {
//				width = width / 4;
//			} else {
//				width = width;
//			}
//
//			imageCarousel.jcarousel('items').css('width', width + 'px');
//		}).jcarousel({
//			wrap: 'circular'
//		});
//
//
//		$('.image-carousel-control-prev')
//				.on('jcarouselcontrol:active', function() {
//					$(this).removeClass('inactive');
//				})
//				.on('jcarouselcontrol:inactive', function() {
//					$(this).addClass('inactive');
//				})
//				.jcarouselControl({
//					target: '-=1'
//				});
//
//		$('.image-carousel-control-next')
//				.on('jcarouselcontrol:active', function() {
//					$(this).removeClass('inactive');
//				})
//				.on('jcarouselcontrol:inactive', function() {
//					$(this).addClass('inactive');
//				})
//				.jcarouselControl({
//					target: '+=1'
//				});
//
//		$('.image-carousel-pagination')
//				.on('jcarouselpagination:active', 'a', function() {
//					$(this).addClass('active');
//				})
//				.on('jcarouselpagination:inactive', 'a', function() {
//					$(this).removeClass('active');
//				})
//				.jcarouselPagination();
//	});

});

function setPressed(id) {
    $('#showModal').val(id);
}

function openModalCSPdp() {
    var url = "/global/modals/modal-cs.jsp?t=" + (new Date()).getTime();
    if (close == "true") {
        url = url + "&close=true";
    }
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'html',
        data: ({action: 'loadModalDiv'}),
        complete: function () {
            $("#link-selectShipAddress").click();
            $('.scrollbar-inner').scrollbar({
                ignoreMobile: true
            });
        },
        success: function (data) {
            data = $.trim(data);
            $("#modal-cs-show-div").html(data);
            eventsModalCS();
            onModalCsLoaded();
            $(document).ready(function () {
                $('#confirmBtn1, #confirmBtn2').click(function () {
                    selectShippingAddressOnPdp();
                });
            });

        }
    });
}

function selectShippingAddressOnPdp() {
    clearModalHighlighting();
    var selectedAddress = $("#selected_shipping_id").val();
    if(selectedAddress == null || selectedAddress==""){
        closeModalDialog();
        $("#link-noResultsModal_showMessEmpty").click();
    }
    else{
        var dataString = $('#modal-cs-form').serialize();
        $.ajax({
            cache: false,
            type: "POST",
            data: dataString,
            dataType: "json",
            success: function(response) {
                if ('success' == response.code) {
                    // prevent ie from caching
                    d = new Date();
                    t = d.getTime();
                    $("#header-cs-selection").load("/includes/gadgets/header-cs-selection.jsp?t=" + t, function () {
						eventsHeaderCsSelection();
                    });
                    closeModalDialog();
                    var url = parent.location.href;
                    console.log("URL: "+url);
                    url = removeURLParameter(url, "showCS");
                    url = removeURLParameter(url, "onHold");
                    console.log("Replace url: "+url);
                    parent.location.replace(url);
                }
            },
            error: ajaxErrorLogin
        });
    }
    return false;
}

function loadDefStageImage(imgUrl) {
	$("#mainImage").attr("href", "" + imgUrl + "");
	$("#imagePlus").attr("src", "" + imgUrl + "");
}

function loadTumbnailImage(imgId) {
	var image = $('#'+imgId).attr("src");
	$("#mainImage").attr("href", "" + image + "");
	$("#imagePlus").attr("src", "" + image + "");
	MagicZoomPlus.refresh();
}

function reloadMinicartCount() {
	$.ajax({
		url: "/includes/gadgets/navbar-cart.jsp",
		cache: false
	}).done(function(html) {
		$("#navbar-cart .nav-cart").empty();
		$("#navbar-cart .nav-cart").html(html);
	});
}

function showErrorsOnAddToCart(data, errorDiv, prodId, postfix) {

	var errorCodes = data.errorCodes;
	var errors = data.errors;

	var showMinQtyError = false;
	var error_messages = "";

	if (postfix === undefined || postfix === null) {
		postfix = "";
	}
	if (prodId !== undefined && prodId.length > 0) {
		for (var i = 0; i < errorCodes.length; i++){
			if (errorCodes[i] === 'minQtyError') {
				showMinQtyError = true;
				if (error_messages === "") {
					error_messages = errors[i].split('-')[0];
				} else {
					error_messages = error_messages + "<br />" + errors[i].split('-')[0];
				}
			}
			if (i + 1 === errorCodes.length || errors[i].split('-')[1] !== errors[i + 1].split('-')[1]) {
                prodId = errors[i].split('-')[1];
                $("#minQtyError" + prodId + postfix).html(error_messages);
                $("#minQtyError" + prodId + postfix).show();
                error_messages = "";
			}
		}
	}

	if (!showMinQtyError) {
		var qtyError = 'Invalid value for a number';
		for (i = 0; i < errors.length; i++) {
			if ("" === error_messages) {
				error_messages = errors[i].split('-')[0];
				if (error_messages === 'Invalid value for a number') {
					error_messages = qtyError;
				}
			} else {
				if (error_messages !== qtyError) {
					error_messages = error_messages + "<br />" + errors[i].split('-')[0];
				}
			}
		}

		$("#modal-add-to-cart-error").modal("show");
		var $errorMessage = $("#add-item-error-message");
		$errorMessage.html(error_messages);
        $errorMessage.show();
	}

}

var canAddItemsToCart = true;

function addToCartAssociated(formName) {
	var apQtys = document.getElementsByName('apQty');
	for(var i = 0; i < apQtys.length; i++){
		if(apQtys[i].value){
			$("#" + apQtys[i].id + "-hidden").val(apQtys[i].value);
		} else {
			$("#" + apQtys[i].id + "-hidden").val("0");
		}
	}
	catalogAddToCart(formName, "");
}

function catalogAddToCart(formName, prodId, postfix) {
	var isCCAccountAccepted = $('#isAcceptedCCPayment').val();
	var isCCNotificationShown = $('#isCCNotificationShown').val();
	if(isCCAccountAccepted=='false' && isCCNotificationShown=='true'){
		$('#ccAccountNotificationModal').modal('show');
	} else {
		if (canAddItemsToCart) {
			canAddItemsToCart = false;
	
			if (postfix == undefined || postfix == null) {
				postfix = "";
			}
			var errorDiv = "add-item-error-message";
			$("#"+errorDiv).empty();
			$("#"+errorDiv).hide();
			$("#minQtyError" + prodId + postfix).empty();
			$("#minQtyError" + prodId + postfix).hide();
	
			var formId = '#' + formName + prodId;
	//		var successUrl = "/catalog/catalog-interrupt.jsp?prodId=" + prodId;
	
			var dataString = $(formId).serialize();
			$.ajax({
				type : "POST",
				data : dataString,
				dataType : "json",
				success : function(data) {
					if (null != data && typeof data.errors != "undefined" && data.error == "true") {
						showErrorsOnAddToCart(data, errorDiv, prodId, postfix);
					} else {
						reloadMinicartCount();
						// addedToCartHangerShow();
						addedToCartWithRelatedHangerShow(prodId);
	
						if(typeof reloadShoppingCartTable === 'function'){
	                        reloadShoppingCartTable();
						}
	//					window.location.href = successUrl;
					}
					setTimeout(function() {canAddItemsToCart = true;}, 1000);
				}
			});
			return false;
		} else {
			return false;
		}
	}
}

function addToCartFromModal(prodId, qtyFieldId, method, locationId) {
	var isCCAccountAccepted = $('#isAcceptedCCPayment').val();
	var isCCNotificationShown = $('#isCCNotificationShown').val();
	if(isCCAccountAccepted=='false' && isCCNotificationShown=='true'){
		$('#ccAccountNotificationModal').modal('show');
	} else {
	$('#qtyFromModal').val($("#"+qtyFieldId).val());
	$('#prodIdFromModal').val(prodId);
	$('#skuIdFromModal').val(prodId);
	$('#shippingMethod').val(method);
	$('#locationId').val(locationId);
	
	if (canAddItemsToCart) {
		canAddItemsToCart = false;

		var errorDiv = "add-item-error-message";
		$("#"+errorDiv).empty();
		$("#"+errorDiv).hide();

		var dataString = $('#addToCartFromModal').serialize();
		$.ajax({
			type : "POST",
			data : dataString,
			dataType : "json",
			success : function(data) {
				$("#dismiss-modal").click();
				if (null != data && typeof data.errors != "undefined" && data.error == "true") {
					showErrorsOnAddToCart(data, errorDiv);
				} else {
					reloadMinicartCount();
					// addedToCartHangerShow();
					addedToCartWithRelatedHangerShow(prodId);
				}
				setTimeout(function() {canAddItemsToCart = true;}, 1000);
			}
		});
		return false;
	} else {
		return false;
	}
	}
}

function pdp_addToMaterialList(prodId, skuId, fromHeader) {
	var action = "addToMaterialList"; // check for IUserCheckForActionConstants
	$("#check-user-action").val(action);
	var dataString = $("#check-user-action-form").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: function (response) {
			if ('success' == response.code) {
				var qty = 1;
				if (fromHeader == false) {
					qty = $("#addToCart" + skuId).children("#qty"+skuId).val();
				} else {
					qty = $("#qty"+skuId).val();
				}
				$.ajax({
					type: "GET",
					url: "/global/modals/modal-add-to-material-list.jsp",
					dataType: 'html',
					data: ({prodId: prodId, skuId: skuId, qty: qty} ),
					complete: function () {
						$("#modal-add-to-material-list-link").click();
					},
					success: function (data) {
						data = $.trim(data);
						$("#modal-add-to-material-list-div").html(data);
                        onModalAddToMaterialListLoaded(prodId);
                        $('.bootstrap-select').selectpicker({
							style: 'form-control',
							size: 7
						});
						$('.create-new-list-btn').on('click', function() {
							$(this).parents('.modal').find('.modal-footer').hide();
							if ( $('.create-new-list-collapse').hasClass('in') ) {
								$(this).parents('.modal').find('.modal-footer').show();
							}
						});
					}
				});

			} else {
				location.href = '/';
			}
		}
	});
}

$(document).ready(function () {
    $('.preload-cover').removeClass('loader-active');
    $('.preload-cover .loading').hide();

    if($("#showModalCSPDP").val() === 'true'){
        openModalCSPdp();
    }
    else if($("#showModalOpenPDP").val() != ''){
    	$("#" + $("#showModalOpenPDP").val()).click();
	}
});