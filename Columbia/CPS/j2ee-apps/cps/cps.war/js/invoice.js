var sort = 'INVOICE_DATE';
var lastSortAccend = true;
var sortId = false;
var sortOrder = false;
var sortPO = false;
var sortAddress = false;
var sortDate = true;
var sortFiledId = "#sort-date";
var sortOption = "DESC";

function resetInvoiceListSearch() {
	window.location.href = "/account/invoices.jsp";
}

function resetInvoiceSearch() {
	document.getElementById("reset-button").disabled = true;
	// window.location.href = "/account/invoices.jsp";
	$('#invoiceSearchOrderNumber').val("");
	$('#searchCS').val("");
	$('#autocompleteSearchCS').val("");
	$("button[data-id='searchCS']").attr("title", "Ship To Address");
	$("button[data-id='searchCS'] span.filter-option").html(
			"Ship To Address");
	$('#invoiceSearchTime').val("");
	$("button[data-id='invoiceSearchTime']").attr("title", "Date Range");
	$("button[data-id='invoiceSearchTime'] span.filter-option").html(
			"Date Range");
	$('#invoiceSearchStartIndex').val("0");
	
	$(".radio").each(function() {
        this.checked = false;
    });
	
	if (!$('#reset-button').attr('search')) {
		searchInvoices();
	} else {
		$('#reset-button').removeAttr('search');
	}

}

function searchInvoiceButton() {
	$('#reset-button').removeAttr('search');
	$("#is-search-button").val("true");
	$('#invoiceSearchStartIndex').val('0');
    if($.trim($('#autocompleteSearchCS').val())==''){
        $('#searchCS').val("");
    }
	searchInvoices();
	enableResetButton();
	$("#is-search-button").val("false");
}

function searchInvoices() {
	
	dataString = $('#invoice-search').serialize();
	$.ajax({
		type : "POST",
		data : dataString,
		dataType : "json",
		success : loadInvoicesSuccess
	});
}

function loadInitialInvoices() {
	dataString = $('#invoice-search').serialize();
	$.ajax({
		type : "POST",
		data : dataString,
		dataType : "json",
		success : loadInvoicesSuccess
	});
}

function requestInvoiceStatus(invoiceId) {
		$.ajax({
		type : "POST",
		// url : "/account/gadgets/request-invoice-status.jsp?invoiceId=37040",
		url : "/account/gadgets/request-invoice-status.jsp?invoiceId="
				+ invoiceId,
		dataType : "json",
		success : function(data) {
			if ("Paid" == data.status) {
				$("#status" + invoiceId).addClass("label label-success");
				$("#status" + invoiceId).html("Paid");
			} else if ("Unpaid" == data.status || "Not Paid" == data.status) {
				$("#status" + invoiceId).addClass("label label-danger");
				$("#status" + invoiceId).html("Unpaid");
			} else if ("Pending" == data.status) {
				$("#status" + invoiceId).addClass("label label-warning");
				$("#status" + invoiceId).html("Pending");
			} else {
                $("#status" + invoiceId).html(data.status);
            }
		}
	});
}

function enableResetButton() {
	document.getElementById("reset-button").disabled = false;
}

function loadInvoicesSuccess(data) {
	if (typeof data.errors == "undefined") {
		$("#invoices-list").load("/account/gadgets/invoices-list-display.jsp", function() {
            calculateCurrentPage();
            loadInvoicePagination();
			eventsInvoicesListDisplay();
			if (sortOption == "DESC") {
				$(sortFiledId + " i").attr("class", "fa fa-sort-down");
			} else {
				$(sortFiledId + " i").attr("class", "fa fa-sort-up");
			}
		});
	} else {
		$('#invoices-list').html('');
		$('#pagination').empty();
		var billAddrRowCount= $("#selectedBillTo option").length;
		for (var i = 0; i <= data.errors.length; i++) {
			if (data.errors[i] != "Select One Option to Search Invoices."
					&& data.errors[i] != undefined) {
                if(billAddrRowCount == 2){
				$('#invoices-list')
						.append(
								"<div class='alert alert-danger'>"
										+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
										+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
										+ "</span></button>" + "Please contact your admin to add Billing Account"
										+ "</div>");
                }
                else if(billAddrRowCount > 2){
				$('#invoices-list')
						.append(
								"<div class='alert alert-danger'>"
										+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
										+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
										+ "</span></button>" + data.errors[i]
										+ "</div>");
                }
				document.getElementById("reset-button").disabled = true;
				setTimeout('$("#dismiss-err_search_invoice").click()', 2000);
				break;
			} else if (data.errors[i] == "Select One Option to Search Invoices.") {
				document.getElementById("reset-button").disabled = true;
				$("#link-err_search_invoice").click();
				setTimeout('$("#dismiss-err_search_invoice").click()', 2000);
				break;
			}
		}
	}
}



function loadInvoiceMessage() {
	$("#pending-message").load("/account/gadgets/invoice-message-display.jsp");
}

function exportInvoices() {
	$("#exportInvoices").submit();
}

function resetSort() {
	$("#sort-id").removeClass("up down");
	$("#sort-date").removeClass("up down");
}

function sortInvoiceNumber() {
	resetSort();
	sortId = !sortId;
	sortFiledId = "#sort-id";
	if (sortId) {
		sortOption = "ASC";
	} else {
		sortOption = "DESC";
	}
	lastSortAccend = !sortId;
	sort = 'INVOICE_NUMBER';

	$('#invoiceSearchSortField').val(sort);
	$('#invoiceSearchSortOption').val(sortOption);
	searchInvoices();

	// $("#invoices-list").load("/account/gadgets/invoices-list-display.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortId);
}

function sortOrderNumber() {
	resetSort();
	sortOrder = !sortOrder;
	sortFiledId = "#sort-order";
	if (sortOrder) {
		sortOption = "ASC";
	} else {
		sortOption = "DESC";
	}
	lastSortAccend = !sortOrder;
	sort = 'ORDER_NUMBER';

	$('#invoiceSearchSortField').val(sort);
	$('#invoiceSearchSortOption').val(sortOption);
	searchInvoices();

	// $("#invoices-list").load("/account/gadgets/invoices-list-display.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortId);
}

function sortPONumber() {
	resetSort();
	sortPO = !sortPO;
	sortFiledId = "#sort-po";
	if (sortPO) {
		sortOption = "ASC";
	} else {
		sortOption = "DESC";
	}
	lastSortAccend = !sortPO;
	sort = 'CUSTOMER_PO_NUMBER';

	$('#invoiceSearchSortField').val(sort);
	$('#invoiceSearchSortOption').val(sortOption);
	searchInvoices();

	// $("#invoices-list").load("/account/gadgets/invoices-list-display.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortId);
}

function sortDateInvoice() {
	resetSort();
	sortDate = !sortDate;
	sortFiledId = "#sort-date";
	if (sortDate) {
		sortOption = "ASC";
	} else {
		sortOption = "DESC";
	}
	lastSortAccend = !sortDate;
	sort = 'INVOICE_DATE';

	$('#invoiceSearchSortField').val(sort);
	$('#invoiceSearchSortOption').val(sortOption);
	searchInvoices();

	// $("#invoices-list").load("/account/gadgets/invoices-list-display.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortDate);
}

function sortInvoiceAddress() {
	resetSort();
	sortAddress = !sortAddress;
	sortFiledId = "#sort-address";
	if (sortAddress) {
		sortOption = "ASC";
	} else {
		sortOption = "DESC";
	}
	lastSortAccend = !sortAddress;
	sort = 'SHIP_TO_NAME';

	$('#invoiceSearchSortField').val(sort);
	$('#invoiceSearchSortOption').val(sortOption);
	searchInvoices();

	// $("#invoices-list").load("/account/gadgets/invoices-list-display.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortDate);
}

function changeInvoicePage(page) {

	var pageSize = $("#invoiceSearchPageSize").val();
	var startIndex = (page - 1) * pageSize;
	$("#invoiceSearchStartIndex").val(startIndex);
	searchInvoices();

}

function calculateCurrentPage() {
	var startIndex = $("#invoiceSearchStartIndex").val();
	var pageSize = $("#invoiceSearchPageSize").val();
	var total = $("#total_count").val();
	var currentPage = (startIndex / pageSize | 0) + 1;
	$("#page").val(currentPage);
}

function loadInvoicePagination() {
	var total = $("#total_count").val();
	var numPerPage = $("#invoiceSearchPageSize").val();
	var currentPage = $("#page").val();
	$("#pagination").load(
			"/account/gadgets/invoice-pagination.jsp?modal=true&page="
					+ currentPage + "&numPerPage=" + numPerPage + "&total="
					+ total, function () {
			eventsInvoicesPagination();
        });
}

function selectOrg(org, id) {
    console.log('invoice selected org = ' + org);
    $('.spinner-on-start').addClass('loader-active');
    $('.spinner-on-start .loading').show();
    var date = new Date();
    $(id).load("/global/gadgets/cs-list.jsp?filter=&nocache=" + date.getTime() + "&orgId=" + org,  function(){
    	autoCompleteShipToAddress();
        $('.spinner-on-start').removeClass('loader-active');
        $('.spinner-on-start .loading').hide();
    },initScrollbar);    
}

