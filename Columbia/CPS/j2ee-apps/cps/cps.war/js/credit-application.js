function addReference() {
    var numberReferences = $("input[uid = 'referenceNumber']").length / 6 + 1;
    var references = document.getElementById("references");
    var divElementReferenceRow = document.createElement("div");
    divElementReferenceRow.setAttribute("class", "row row-narrow");
    divElementReferenceRow.setAttribute("id", "business_trade_" + numberReferences);
    references.appendChild(divElementReferenceRow);
    addInputBusinessTrade("Name", numberReferences, divElementReferenceRow);
    addInputBusinessTrade("Address", numberReferences, divElementReferenceRow);
    addInputBusinessTrade("City", numberReferences, divElementReferenceRow);
    addInputBusinessTradeState("State", numberReferences, divElementReferenceRow);
    addInputBusinessTrade("Zip", numberReferences, divElementReferenceRow);
    addInputBusinessTrade("Phone", numberReferences, divElementReferenceRow);
    addInputBusinessTrade("Email", numberReferences, divElementReferenceRow);
    var id_phone = "#ca_input_business_trade_phone_" + numberReferences;
    $(id_phone).mask("(999) 999-9999");
}

function removeReference() {
    var numberReferences = $("input[uid = 'referenceNumber']").length / 6;
    if (numberReferences > 1) {
        var references = document.getElementById("references");
        var divElementReferenceLastRow = document.getElementById("business_trade_" + numberReferences);
        references.removeChild(divElementReferenceLastRow);
    }
}

function addInputBusinessTrade(type, numberReferences, divElementReferenceRow) {
    var typeLowCase = type.toLowerCase();
    var divElementReference = document.createElement("div");
    var inputType = "text";
    var maxLength = "100";
    if (type === 'Zip') {
        divElementReference.setAttribute("class", "form-group col-sm-1");
        inputType = "text";
        maxLength = "5";
        divElementReference.setAttribute("oninput", "this.value=this.value.replace(/[^0-9]/g,'');");
    } else {
        divElementReference.setAttribute("class", "form-group col-sm-2");
    }
    divElementReference.setAttribute("id", "business_trade_" + typeLowCase + "_" + numberReferences);

    var labelElement = document.createElement("label");
    labelElement.setAttribute("class", "control-label");
    type = type + '*';
    labelElement.innerHTML = type;

    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_business_trade_" + typeLowCase + "_" + numberReferences);
    inputElement.setAttribute("name", "ca_input_business_trade_" + typeLowCase + "_" + numberReferences);
    inputElement.setAttribute("class", "form-control");

    if (type === 'Email') {
        maxLength = "50";
    }
    if (type === 'Phone') {
        maxLength = "15";
    }

    inputElement.setAttribute("type", inputType);
    inputElement.setAttribute("maxlength", maxLength);
    inputElement.setAttribute("uid", "referenceNumber");

    divElementReference.appendChild(labelElement);
    divElementReference.appendChild(inputElement);

    divElementReferenceRow.appendChild(divElementReference);
}

function addInputBusinessTradeState(type, numberReferences, divElementReferenceRow) {
    var typeLowCase = type.toLowerCase();
    var divElementReference = document.createElement("div");
    divElementReference.setAttribute("class", "form-group col-sm-1");
    divElementReference.setAttribute("id", "business_trade_" + typeLowCase + "_" + numberReferences);

    var labelElement = document.createElement("label");
    labelElement.setAttribute("class", "control-label");
    labelElement.innerHTML = type + "*";

    var spanCaretSelect = document.createElement("span");
    spanCaretSelect.setAttribute("class", "caret caret-select");

    var spanSelectWrap = document.createElement("span");
    spanSelectWrap.setAttribute("class", "select-wrap");

    var selectElement = document.getElementById("ca_input_business_trade_state_1");
    var newSelectElement = selectElement.cloneNode(true);
    newSelectElement.setAttribute("id", "ca_input_business_trade_" + typeLowCase + "_" + numberReferences);
    newSelectElement.setAttribute("name", "ca_input_business_trade_" + typeLowCase + "_" + numberReferences);

    spanSelectWrap.appendChild(newSelectElement);
    spanSelectWrap.appendChild(spanCaretSelect);
    divElementReference.appendChild(labelElement);
    divElementReference.appendChild(spanSelectWrap);

    divElementReferenceRow.appendChild(divElementReference)
}

function addNextCustomer() {
    var numberCustomers = $("input[cid = 'customerNumber']").length / 4 + 1;
    addInputCustomer("Name", numberCustomers);
    addInputCustomer("Title", numberCustomers);
    addInputCustomer("Phone", numberCustomers);
    addInputCustomer("Email", numberCustomers);
    var id_phone = "#ca_input_profile_customer_phone_" + numberCustomers;
    $(id_phone).mask("(999) 999-9999");
}

function addInputCustomer(type, numberCustomers) {
    var typeLowCase = type.toLowerCase();
    var divElementCustomer = document.createElement("div");
    divElementCustomer.setAttribute("class", "col-sm-3 col-xs-6");
    divElementCustomer.setAttribute("id", "profile_customer_" + typeLowCase + "_" + numberCustomers);

    var labelElement = document.createElement("label");
    labelElement.setAttribute("class", "control-label");
    var maxLength = "100";
    if (type === "Phone") {
        type = type + "#";
        maxLength = "15";
    }
    labelElement.innerHTML = type;
    if (type === "Email") {
        maxLength = "50";
    }
    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_profile_customer_" + typeLowCase + "_" + numberCustomers);
    inputElement.setAttribute("name", "ca_input_profile_customer_" + typeLowCase + "_" + numberCustomers);
    inputElement.setAttribute("class", "form-control");
    inputElement.setAttribute("maxlength", maxLength);
    inputElement.setAttribute("type", "text");
    inputElement.setAttribute("cid", "customerNumber");

    var customers = document.getElementById("profile_customer");
    divElementCustomer.appendChild(labelElement);
    divElementCustomer.appendChild(inputElement);

    customers.appendChild(divElementCustomer)
}

function removeLastCustomer() {
    var customerNumber = $("input[cid = 'customerNumber']").length / 4;
    if (customerNumber > 1) {
        var references = document.getElementById("profile_customer");
        var inputName = document.getElementById("profile_customer_name_" + customerNumber);
        references.removeChild(inputName);

        var inputAddress = document.getElementById("profile_customer_title_" + customerNumber);
        references.removeChild(inputAddress);

        var inputPhone = document.getElementById("profile_customer_phone_" + customerNumber);
        references.removeChild(inputPhone);

        var inputEmail = document.getElementById("profile_customer_email_" + customerNumber);
        references.removeChild(inputEmail);
    }
}

function addRegisteredState() {
    var numberStates = $("input[sid = 'stateNumber']").length + 1;
    var divElementRow = document.createElement("div");
    divElementRow.setAttribute("class", "row");
    divElementRow.setAttribute("id", "tax_registered_" + numberStates);
    var registers = document.getElementById("tax_registered");
    registers.appendChild(divElementRow);
    addNameInputRegisteredState("State", divElementRow);
    addSelectRegisteredState("state", numberStates, divElementRow);
    addNameInputRegisteredState("id", divElementRow);
    addInputRegisteredState("id", numberStates, divElementRow);
}

function addSelectRegisteredState(type, numberStates, divElementRow) {
    var divElementRegister = document.createElement("div");
    divElementRegister.setAttribute("class", "col-sm-3");
    divElementRegister.setAttribute("id", "tax_registered_" + type + "_" + numberStates);

    var spanCaretSelect = document.createElement("span");
    spanCaretSelect.setAttribute("class", "caret caret-select");

    var spanSelectWrap = document.createElement("span");
    spanSelectWrap.setAttribute("class", "select-wrap");

    var selectElement = document.getElementById("ca_input_tax_registered_state_1");
    var newSelectElement = selectElement.cloneNode(true);
    newSelectElement.setAttribute("id", "ca_input_tax_registered_" + type + "_" + numberStates);
    newSelectElement.setAttribute("name", "ca_input_tax_registered_" + type + "_" + numberStates);

    spanSelectWrap.appendChild(newSelectElement);
    spanSelectWrap.appendChild(spanCaretSelect);
    divElementRegister.appendChild(spanSelectWrap);

    divElementRow.appendChild(divElementRegister)
}

function addNameInputRegisteredState(type, divElementRow) {
    var divElementFormGroup = document.createElement("div");
    var hElement = document.createElement("h5");
    hElement.setAttribute("align", "right");
    if (type === "id") {
        divElementFormGroup.setAttribute("class", "col-sm-4");
        type = "State registration or ID no.";
    } else {
        divElementFormGroup.setAttribute("class", "col-sm-2");
    }
    hElement.innerHTML = type;

    //var registers = document.getElementById("tax_registered");
    divElementFormGroup.appendChild(hElement);
    divElementRow.appendChild(divElementFormGroup);
}

function addInputRegisteredState(type, numberStates, divElementRow) {
    var divElementRegister = document.createElement("div");
    divElementRegister.setAttribute("class", "col-sm-3");
    divElementRegister.setAttribute("id", "tax_registered_" + type + "_" + numberStates);

    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_tax_registered_" + type + "_" + numberStates);
    inputElement.setAttribute("name", "ca_input_tax_registered_" + type + "_" + numberStates);
    inputElement.setAttribute("class", "form-control");
    inputElement.setAttribute("maxlength", "100");
    inputElement.setAttribute("type", "text");
    inputElement.setAttribute("sid", "stateNumber");

    //var registers = document.getElementById("tax_registered");
    divElementRegister.appendChild(inputElement);

    divElementRow.appendChild(divElementRegister)
}

function removeRegisteredState() {
    var customerNumber = $("input[sid = 'stateNumber']").length;
    if (customerNumber > 1) {
        var divElementRow = document.getElementById("tax_registered_" + customerNumber);
        var register = document.getElementById("tax_registered");
        register.removeChild(divElementRow);
    }
}

function creditApplicationChangeOrganization(checkboxLabel) {
    var checkedClass = "checked";
    $("label[name=credit_application]").removeClass(checkedClass);
    checkboxLabel.classList.add(checkedClass);
    $("#cu_input_organization").val(checkboxLabel.getAttribute("value"));
}

function creditApplicationChangeCustomer(checkboxLabel) {
    var checkedClass = "checked";
    $("label[name=credit_application]").removeClass(checkedClass);
    checkboxLabel.classList.add(checkedClass);
    $("#cu_input_customer").val(checkboxLabel.getAttribute("value"));
}

function creditApplicationChangePO(checkboxLabel) {
    var checkedClass = "checked";
    $("label[name=credit_application]").removeClass(checkedClass);
    checkboxLabel.classList.add(checkedClass);
    $("#cu_input_po").val(checkboxLabel.getAttribute("value"));
}

function creditApplicationChangeTaxExempt(checkboxLabel) {
    var checkedClass = "checked";
    $("label[name=credit_application]").removeClass(checkedClass);
    checkboxLabel.classList.add(checkedClass);
    if (checkboxLabel.getAttribute("value") === 'Yes' && document.getElementById("yes_tax_exempt") === null) {
        addInputTaxExempt();
    }
    if (checkboxLabel.getAttribute("value") !== 'Yes' && document.getElementById("yes_tax_exempt") !== null) {
        removeInputTaxExempt();
    }
    $("#cu_input_tax_exempt").val(checkboxLabel.getAttribute("value"));
}

function removeInputTaxExempt() {
    var requiredTaxExempt = document.getElementById("required_tax");
    var divElementRequiredTax = document.getElementById("yes_tax_exempt");
    requiredTaxExempt.removeChild(divElementRequiredTax);
}

function addInputTaxExempt() {
    var divElementRequiredTax = document.createElement("div");
    // divElementRequiredTax.setAttribute("class", "col-sm-4");
    divElementRequiredTax.setAttribute("id", "yes_tax_exempt");

    var labelElement = document.createElement("label");
    labelElement.setAttribute("class", "control-label");
    labelElement.innerHTML = "List tax number & fax resale certificate to (xxx) xxx-xxxx. *";

    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_tax_exempt_number");
    inputElement.setAttribute("name", "ca_input_tax_exempt_number");
    inputElement.setAttribute("class", "form-control");
    inputElement.setAttribute("maxlength", "100");
    inputElement.setAttribute("type", "text");
    //inputElement.setAttribute("value", "${taxNumber}");
    //inputElement.setAttribute("bean", "UserCreditApplication.taxNumber");

    var requiredTaxExempt = document.getElementById("required_tax");
    divElementRequiredTax.appendChild(labelElement);
    divElementRequiredTax.appendChild(inputElement);

    requiredTaxExempt.appendChild(divElementRequiredTax)
}

function creditApplicationExemptStatus(checkboxLabel) {
	var exemptStatusValue = checkboxLabel.getAttribute("value");
    $(checkboxLabel).val(exemptStatusValue);
    var checkedValue =$("input[name='ca_input_tax_exempt_status']:checked").val();
    if(checkedValue == "Yes"){
        $("#showStep3").show(); 	
    }else{
        $("#showStep3").hide();		
    }
}

function creditApplicationChangeEngagedRegistered(value) {
    if (value === 'Other (specify)' && document.getElementById("other_engaged") === null) {
        addInputOtherEngagedRegistered();
    }
    if (value !== 'Other (specify)' && document.getElementById("other_engaged") !== null) {
        removeInputOtherEngagedRegistered();
    }
}

function removeInputOtherEngagedRegistered() {
    var engagedRegistered = document.getElementById("engaged_registered");
    var divElementOtherEngaged = document.getElementById("other_engaged");
    engagedRegistered.removeChild(divElementOtherEngaged);
}

function addInputOtherEngagedRegistered() {
    var divElementOtherEngaged = document.createElement("div");
    divElementOtherEngaged.setAttribute("class", "col-sm-4");
    divElementOtherEngaged.setAttribute("id", "other_engaged");

    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_tax_other_engaged");
    inputElement.setAttribute("name", "ca_input_tax_other_engaged");
    inputElement.setAttribute("class", "form-control");
    inputElement.setAttribute("maxlength", "100");
    inputElement.setAttribute("type", "text");

    var engagedRegistered = document.getElementById("engaged_registered");
    divElementOtherEngaged.appendChild(inputElement);

    engagedRegistered.appendChild(divElementOtherEngaged)
}

function creditApplicationChangePurchase(value) {
    // if (value === 'Other (explain)' && document.getElementById("purchase_other") === null) {
    //     addInputTaxPurchase('other');
    // }
    //
    // if (value !== 'Other (explain)' && document.getElementById("purchase_other") !== null) {
    //     removeInputTaxPurchase('other');
    // }
    //
    // if (value === 'Exempt under direct payment permit #' && document.getElementById("purchase_exempt") === null) {
    //     addInputTaxPurchase('exempt');
    // }
    //
    // if (value !== 'Exempt under direct payment permit #' && document.getElementById("purchase_exempt") !== null) {
    //     removeInputTaxPurchase('exempt');
    // }

    if ((value === 'Exempt under direct payment permit #' || value === 'Other (explain)') && document.getElementById("purchase_specify") === null) {
        addInputTaxPurchase();
    }

    if (value !== 'Exempt under direct payment permit #' && value !== 'Other (explain)' && document.getElementById("purchase_specify") !== null) {
        removeInputTaxPurchase();
    }
}

function removeInputTaxPurchase() {
    var taxPurchase = document.getElementById("tax-purchase");
    var divElementOtherEngaged = document.getElementById("purchase_specify"); //+ value);
    taxPurchase.removeChild(divElementOtherEngaged);
}

function addInputTaxPurchase() {
    var divElementOtherEngaged = document.createElement("div");
    divElementOtherEngaged.setAttribute("class", "col-sm-6");
    divElementOtherEngaged.setAttribute("id", "purchase_specify"); // + value);

    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_tax_purchase_specify");// + value);
    inputElement.setAttribute("name", "ca_input_tax_purchase_specify");// + value);
    inputElement.setAttribute("class", "form-control");
    inputElement.setAttribute("maxlength", "100");
    inputElement.setAttribute("type", "text");

    var engagedRegistered = document.getElementById("tax-purchase");
    divElementOtherEngaged.appendChild(inputElement);

    engagedRegistered.appendChild(divElementOtherEngaged)
}

function creditApplicationResaleChangePurchaserRegistered(checkboxLabel) {
    var checkedClass = "checked";
    $("label[name=credit_application]").removeClass(checkedClass);
    checkboxLabel.classList.add(checkedClass);
    var value = checkboxLabel.getAttribute("value");
    if (value === 'retailer') {
        addInputRetailerNumber(value);
    }
    if (value === 'reseller') {
        addInputResellerNumber(value);
    }
    if (value === 'other') {
        if (document.getElementById("registered_reseller") !== null) {
            removeInputNumber("reseller");
        }
        if (document.getElementById("registered_retailer") !== null) {
            removeInputNumber("retailer");
        }
    }
}

function addInputRetailerNumber(value) {
    if (document.getElementById("registered_reseller") !== null) {
        removeInputNumber("reseller");
    }
    if (document.getElementById("registered_" + value) === null) {
        addInputNumber(value);
    }
}

function addInputResellerNumber(value) {
    if (document.getElementById("registered_retailer") !== null) {
        removeInputNumber("retailer");
    }
    if (document.getElementById("registered_" + value) === null) {
        addInputNumber(value);
    }
}

function addInputNumber(value) {
    var divElementNumber = document.createElement("div");
    divElementNumber.setAttribute("class", "col-sm-5");
    divElementNumber.setAttribute("id", "registered_" + value);

    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_certificate_registered_number");
    inputElement.setAttribute("name", "ca_input_certificate_registered_number");
    inputElement.setAttribute("class", "form-control");
    inputElement.setAttribute("maxlength", "8");
    inputElement.setAttribute("text-align", "center");
    // inputElement.setAttribute("type", "number");
    if (value === 'retailer') {
        inputElement.setAttribute("placeholder", "Registration number");
    } else {
        inputElement.setAttribute("placeholder", "Resale number");
    }

    var purchaserType = document.getElementById(value);
    divElementNumber.appendChild(inputElement);

    purchaserType.appendChild(divElementNumber)
    //$("#ca_input_certificate_registered").val(value);
    $("#ca_input_certificate_registered_number").mask("9999-9999");
}

function removeInputNumber(value) {
    var engagedRegistered = document.getElementById(value);
    var divElementOtherEngaged = document.getElementById("registered_" + value);
    engagedRegistered.removeChild(divElementOtherEngaged);
}

function creditApplicationResaleChangePercentage(checkboxLabel) {
    var checkedClass = "checked";
    $("label[name=credit_application]").removeClass(checkedClass);
    checkboxLabel.classList.add(checkedClass);
    if (checkboxLabel.getAttribute("value") === 'fully' && document.getElementById("percentage_partly") !== null) {
        removeInputResalePercentage();
    }
    if (checkboxLabel.getAttribute("value") === 'partly' && document.getElementById("percentage_partly") === null) {
        addInputResalePercentage();
    }
    $("#ca_input_certificate_percentage").val(checkboxLabel.getAttribute("value"));
}

function addInputResalePercentage() {
    var divElementPercentagePartly = document.createElement("div");
    divElementPercentagePartly.setAttribute("id", "percentage_partly");
    //divElementPercentagePartly.setAttribute("class", "col-sm-2");

    var divElementInputGroup = document.createElement("div");
    divElementInputGroup.setAttribute("class", "input-group");

    var divElementCurrency = document.createElement("div");
    divElementCurrency.setAttribute("class", "input-group-addon");
    divElementCurrency.innerHTML = "%";

    var inputElement = document.createElement("input");
    inputElement.setAttribute("id", "ca_input_certificate_percentage_partly");
    inputElement.setAttribute("name", "ca_input_certificate_percentage_partly");
    inputElement.setAttribute("class", "form-control");
    inputElement.setAttribute("maxlength", "5");
    inputElement.setAttribute("type", "number");

    divElementInputGroup.appendChild(divElementCurrency);
    divElementInputGroup.appendChild(inputElement);

    var resellerPartly = document.getElementById("reseller_partly");
    divElementPercentagePartly.appendChild(divElementInputGroup);

    resellerPartly.appendChild(divElementPercentagePartly)
}

function removeInputResalePercentage() {
    var resellerPartly = document.getElementById("reseller_partly");
    var divElementPercentagePartly = document.getElementById("percentage_partly");
    resellerPartly.removeChild(divElementPercentagePartly);
}

function creditApplicationSubmit() {
    closeAllMessages();
    var dataString = $('#credit-application-form').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function (response) {
            if ('success' == response.code) {
                creditApplicationSubmitSuccess();
            } else {
                creditApplicationSubmitError(response);
            }
        },
        error: function (response, textStatus, errorThrown) {
            ajaxErrorResponse();
        }
    });
}

function addBreadcrumb(stepNumber) {
    var liElementBreadcrumb = document.createElement("li");
    var aElementBreadcrumb = document.createElement("a");
    aElementBreadcrumb.setAttribute("style", "cursor: pointer;");

    switch (stepNumber) {
        case 2:
            aElementBreadcrumb.innerHTML = "Step 2";
            liElementBreadcrumb.setAttribute("id", "ca_breadcrumb_step_2");
            $(aElementBreadcrumb).click(creditApplicationReturnStep2);
            break;
        case 3:
            aElementBreadcrumb.innerHTML = "Step 3";
            liElementBreadcrumb.setAttribute("id", "ca_breadcrumb_step_3");
            $(aElementBreadcrumb).click(creditApplicationReturnStep3);
            break;
        case 4:
            aElementBreadcrumb.innerHTML = "Step 4";
            liElementBreadcrumb.setAttribute("id", "ca_breadcrumb_step_4");
            $(aElementBreadcrumb).click(creditApplicationReturnStep4);
            break;
    }
    liElementBreadcrumb.appendChild(aElementBreadcrumb);
    var breadcrumb = document.getElementById("breadcrumb-credit");
    breadcrumb.appendChild(liElementBreadcrumb);
}

function creditApplicationProfileSubmit() {
    closeAllMessages();
    var dataString = $('#credit-application-form').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function (response) {
            if ('success' == response.code) {
                creditApplicationProfileSubmitSuccess();
            } else {
                creditApplicationSubmitError(response);
            }

        },
        error: function (response, textStatus, errorThrown) {
            ajaxErrorResponse();
        }
    });
}

function creditApplicationTaxSubmit() {
    closeAllMessages();
    var dataString = $('#credit-application-form').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function (response) {
            if ('success' == response.code) {
                creditApplicationTaxSubmitSuccess();
            } else {
                creditApplicationSubmitError(response);
            }

        },
        error: function (response, textStatus, errorThrown) {
            ajaxErrorResponse();
        }
    });
}

var urlDocusign;

function creditApplicationResaleSubmit(selectedOption) {
    $("#addSelectedOption").val(selectedOption);
    var dataString = $('#credit-application-form').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function (response) {
            if ('success' === response.code && selectedOption === 'sign') {
                // document.location.href = response.url;
                urlDocusign = "/";
                $("#link-creditApplicationModal").click();
                // creditApplicationResaleSubmitSuccess(url);
            } else if ('success' === response.code && selectedOption === 'save') {
                urlDocusign = "/";
                $("#link-creditApplicationModal").click();
            } else {
                creditApplicationSubmitError(response);
            }

        },
        error: function (response, textStatus, errorThrown) {
            ajaxErrorResponse();
        }
    });
}

function onProfileContentLoaded(){
    $("#ca_input_profile_customer_phone_1").mask("(999) 999-9999");

    $(document).ready(function () {
        var productTypeList = $("#productTypeList").val().split(",");
        for(var i = 0; i < productTypeList.length; i++){
            var productType = productTypeList[i];
            switch (productType) {
                case "Pipe, Valves, & Fittings (PVC)" :
                    $('[name="ca_input_product_pipe"]').val([productType]); break;
                case "Plumbing" :
                    $('[name="ca_input_product_plumbing"]').val([productType]); break;
                case "Pipe, Hangers & Supports" :
                    $('[name="ca_input_product_support"]').val([productType]); break;
                case "Automated Products Specialties" :
                    $('[name="ca_input_product_automated"]').val([productType]); break;
                case "Heating, Ventilation & Air Conditioning Products (HVAC)" :
                    $('[name="ca_input_product_ventilation"]').val([productType]); break;
            }
        }
        var number = 1;
        var supposedCustomerListLength = parseInt($("#supposedCustomerListLength").val());
        for (var i = 0; i < supposedCustomerListLength; i++){
            var supposedCustomerName = $("#supposedCustomerName" + i).val();
            var supposedCustomerTitle = $("#supposedCustomerTitle" + i).val();
            var supposedCustomerPhoneNumber = $("#supposedCustomerPhoneNumber" + i).val();
            var supposedCustomerEmail = $("#supposedCustomerEmail" + i).val();

            if (supposedCustomerName !== "" && number > 1) {
                addNextCustomer();
            }
            var id_supposedCustomerName = "#ca_input_profile_customer_name_" + number;
            $(id_supposedCustomerName).val(supposedCustomerName);
            var id_supposedCustomerTitle = "#ca_input_profile_customer_title_" + number;
            $(id_supposedCustomerTitle).val(supposedCustomerTitle);
            var id_supposedCustomerPhoneNumber = "#ca_input_profile_customer_phone_" + number;
            $(id_supposedCustomerPhoneNumber).val(supposedCustomerPhoneNumber);
            var id_supposedCustomerEmail = "#ca_input_profile_customer_email_" + number;
            $(id_supposedCustomerEmail).val(supposedCustomerEmail);
            number++;
        }
    });
}

function onTaxContentLoaded(){
	 var taxExemptStatus = $("#taxExemptStatus").val();
     if($.trim($('#taxExemptStatus').val()) == ''){
         $("#showStep3").hide();
     }else{
         $(`input[name=ca_input_tax_exempt_status][value=${taxExemptStatus}]`).prop("checked",true);
     }
     var exemptStatusVal = $("input[name='ca_input_tax_exempt_status']:checked").val();
     if(exemptStatusVal == undefined){
         $("#showStep3").hide();
     }
     else if(exemptStatusVal == "Yes"){
         $("#showStep3").show();
     }
     else{
     	$("#showStep3").hide();
     }
    $(document).ready(function () {
    	var firmState = $("#firmState").val();
        var registeredType = $("#registeredType").val();
        var specifyRegisteredType = $("#specifyRegisteredType").val();
        var buyerActivityType = $("#buyerActivityType").val();
        var specifyBuyerActivityType = $("#specifyBuyerActivityType").val();

        $("#ca_input_tax_state").val(firmState);
        $("#ca_input_tax_engaged").val(registeredType);

        if (registeredType === "Other (specify)") {
            addInputOtherEngagedRegistered();
            $("#ca_input_tax_other_engaged").val(specifyRegisteredType);
        }

        $("#ca_input_tax_buyer_activity").val(buyerActivityType);
        if (buyerActivityType === "Exempt under direct payment permit #" || buyerActivityType === "Other (explain)") {
            addInputTaxPurchase();
            $("#ca_input_tax_purchase_specify").val(specifyBuyerActivityType);
        }

        var number = 1;

        var registeredStateListLength = parseInt($("#registeredStateListLength").val());
        for(var i = 0; i < registeredStateListLength; i++){
            var registeredState = $("#registeredState" + i).val();
            var registeredStateId = $("#registeredStateId" + i).val();

            if (registeredState !== "" && number > 1) {
                addRegisteredState();
            }
            var id_registeredState = "#ca_input_tax_registered_state_" + number;
            $(id_registeredState).val(registeredState);
            var id_registeredStateId = "#ca_input_tax_registered_id_" + number;
            $(id_registeredStateId).val(registeredStateId);
            number++;
        }
    });
}

function onApplicationContentLoaded(){
    $(document).ready(function () {
        $("#ca_input_phone").mask("(999) 999-9999");
        $("#ca_input_fax").mask("(999) 999-9999");
        $("#ca_input_principal_phone_1").mask("(999) 999-9999");
        $("#ca_input_principal_phone_2").mask("(999) 999-9999");
        $("#ca_input_business_trade_phone_1").mask("(999) 999-9999");
        $("#ca_input_business_trade_fax_1").mask("(999) 999-9999");
        $("#ca_input_bank_reference_phone").mask("(999) 999-9999");

        $(function () {
            $('.input-group.form-date').datetimepicker({
                format: 'YYYY'
            });
        });

        var organizationType = $("#organizationType").val();
        var customerType = $("#customerType").val();
        var requiredPO = $("#requiredPO").val();
        var taxExempt = $("#taxExempt").val();
        var taxNumber = $("#taxNumber").val();
        var businessState = $("#businessState").val();
        var billingState = $("#billingState").val();
        var bankReferenceState = $("#bankReferenceState").val();

        if (organizationType) {
            $("#ca_input_organization").val(organizationType);
        }
        if (customerType) {
            $("#ca_input_customer").val(customerType);
        }
        $('[name="ca_input_po"]').val([requiredPO]);
        $('[name="ca_input_tax_exempt"]').val([taxExempt]);
        if (taxExempt === "Yes") {
            addInputTaxExempt();
            $("#ca_input_tax_exempt_number").val(taxNumber);
        }
        if (businessState) {
            $("#ca_input_business_state").val(businessState);
        }
        if (billingState) {
            $("#ca_input_billing_state").val(billingState);
        }
        if (bankReferenceState) {
            $("#ca_input_bank_reference_state").val(bankReferenceState);
        }

        var number = 1;
        var principalOrganizationListLength = parseInt($("#principalOrganizationListLength").val());
        for(var i = 0; i < principalOrganizationListLength; i++){
            var principalName = $("#principalName" + i).val();
            var principalTitle = $("#principalTitle" + i).val();
            var socialSecurityNumber = $("#socialSecurityNumber" + i).val();
            var principalEmail = $("#principalEmail" + i).val();
            var principalPhoneNumber = $("#principalPhoneNumber" + i).val();
            var principalStreet = $("#principalStreet" + i).val();
            var principalCity = $("#principalCity" + i).val();
            var principalState = $("#principalState" + i).val();
            var principalCode = $("#principalCode" + i).val();

            var id_principalName = "#ca_input_principal_name_" + number;
            $(id_principalName).val(principalName);
            var id_principalTitle = "#ca_input_principal_title_" + number;
            $(id_principalTitle).val(principalTitle);
            var id_socialSecurityNumber = "#ca_input_principal_social_" + number;
            $(id_socialSecurityNumber).val(socialSecurityNumber);
            var id_principalEmail = "#ca_input_principal_email_" + number;
            $(id_principalEmail).val(principalEmail);
            var id_principalPhoneNumber = "#ca_input_principal_phone_" + number;
            $(id_principalPhoneNumber).val(principalPhoneNumber);
            var id_principalStreet = "#ca_input_principal_address_" + number;
            $(id_principalStreet).val(principalStreet);
            var id_principalCity = "#ca_input_principal_city_" + number;
            $(id_principalCity).val(principalCity);
            var id_principalState = "#ca_input_principal_state_" + number;
            $(id_principalState).val(principalState);
            var id_principalCode = "#ca_input_principal_code_" + number;
            $(id_principalCode).val(principalCode);
            number++;
        }

        number = 1;
        var businessTradeReferenceListLength = parseInt($("#businessTradeReferenceListLength").val());
        for(var i = 0; i < businessTradeReferenceListLength; i++){
            var referenceName = $("#referenceName" + i).val();
            var referenceStreet = $("#referenceStreet" + i).val();
            var referenceCity = $("#referenceCity" + i).val();
            var referenceState = $("#referenceState" + i).val();
            var referenceCode = $("#referenceCode" + i).val();
            var referencePhone = $("#referencePhone" + i).val();
            var referenceEmail = $("#referenceEmail" + i).val();

            if (referenceName !== "" && number > 1) {
                addReference();
            }
            var id_referenceName = "#ca_input_business_trade_name_" + number;
            $(id_referenceName).val(referenceName);
            var id_referenceStreet = "#ca_input_business_trade_address_" + number;
            $(id_referenceStreet).val(referenceStreet);
            var id_referenceCity = "#ca_input_business_trade_city_" + number;
            $(id_referenceCity).val(referenceCity);
            var id_referenceState = "#ca_input_business_trade_state_" + number;
            $(id_referenceState).val(referenceState);
            var id_referenceCode = "#ca_input_business_trade_zip_" + number;
            $(id_referenceCode).val(referenceCode);
            var id_referencePhone = "#ca_input_business_trade_phone_" + number;
            $(id_referencePhone).val(referencePhone);
            var id_referenceEmail = "#ca_input_business_trade_email_" + number;
            $(id_referenceEmail).val(referenceEmail);
            number++;
        }
    });
}

function onResaleContentLoaded(){
    $(document).ready(function () {
        var sellerState = $("#sellerState").val();
        var purchaserState = $("#purchaserState").val();
        var purchaserRegisteredType = $("#purchaserRegisteredType").val();
        var registrationNumber = $("#registrationNumber").val();
        var describePropertyResale = $("#describePropertyResale").val();
        var purchasesResaleType = $("#purchasesResaleType").val();
        var percentagePurchasesResale = $("#percentagePurchasesResale").val();

        $("#ca_input_certificate_state_step_1").val(sellerState);
        $("#ca_input_certificate_state_step_2").val(purchaserState);

        $('[name="ca_input_certificate_registered"]').val([purchaserRegisteredType]);

        if (purchaserRegisteredType === "retailer") {
            addInputNumber("retailer");
            $("#ca_input_certificate_registered_number").val(registrationNumber);
        }
        if (purchaserRegisteredType === "reseller") {
            addInputNumber("reseller");
            $("#ca_input_certificate_registered_number").val(registrationNumber);
        }

        $("#ca_input_certificate_describe_property").val(describePropertyResale);

        $('[name="ca_input_certificate_purchases_resale_type"]').val([purchasesResaleType]);
        if (purchasesResaleType === "partly") {
            addInputResalePercentage();
            $("#ca_input_certificate_percentage_partly").val(percentagePurchasesResale);
        }
    });
}

function creditApplicationSignSubmit() {
    document.location.href = urlDocusign;
}

function creditApplicationSubmitSuccess() {
    $('.preload-cover').removeClass('loader-active');
    $('.preload-cover .loading').hide();
    setTimeout(function () {
        $('#credit-application').load("profile-content.jsp", function () {
            onProfileContentLoaded();
            eventsProfileContent();
        });
    }, 500);
    if (document.getElementById("ca_breadcrumb_step_2") === null) {
        addBreadcrumb(2);
    }
}

function creditApplicationProfileSubmitSuccess() {
    setTimeout(function () {
        $('#credit-application').load("tax-content.jsp", function () {
        	onTaxContentLoaded();
            eventsTaxContent();
        });
    }, 500);
    if (document.getElementById("ca_breadcrumb_step_3") === null) {
        addBreadcrumb(3);
    }
}

function creditApplicationTaxSubmitSuccess() {
    setTimeout(function () {
        $('#credit-application').load("resale-content.jsp", function () {
            onResaleContentLoaded();
            eventsResaleContent();
            captchaCredit();
        });
    }, 500);
    if (document.getElementById("ca_breadcrumb_step_4") === null) {
        addBreadcrumb(4);
    }
}

function creditApplicationResaleSubmitSuccess(url) {
    $("#link-creditApplicationModal").click();
    setTimeout(function () {
        document.location.href = "/";
    }, 5000);
}

function creditApplicationSubmitError(response) {
    showError(response);
}

function creditApplicationReturnStep1() {
    $('#credit-application').load("application-content.jsp", function () {
        eventsApplicationContent();
        onApplicationContentLoaded();
    });
}

function creditApplicationReturnStep2() {
    $('#credit-application').load("profile-content.jsp", function () {
        onProfileContentLoaded();
        eventsProfileContent();
    });
}

function creditApplicationReturnStep3() {
    $('#credit-application').load("tax-content.jsp", function () {
        onTaxContentLoaded();
        eventsTaxContent();
    });
}

function creditApplicationReturnStep4() {
    $('#credit-application').load("resale-content.jsp", function () {
        onResaleContentLoaded();
        eventsResaleContent();
        captchaCredit();
    });
}

function creditApplicationCancel() {
    location.href = "/";
}
function captchaCredit(){
    $(document).ready(function() {
        checkCreditApplicationSubmitButton();
        renderCaptchaCallbackCreditApp();
    });
        
    var credit_application_captcha_success_default = false;
    var credit_application_captcha_default = null;

    function checkCreditApplicationSubmitButton() {
        //console.log('inside check submit button');
        //console.log('credit_application_captcha_success_default :: ' + credit_application_captcha_success_default);
        var enableLoginButton = true;
        if ($('#credite-application-form-captcha').length > 0) {
            enableLoginButton = credit_application_captcha_success_default;
        }

        if (enableLoginButton) {
            $('#creditApplicationSubmit').removeClass('disabled');
        } else {
            $('#creditApplicationSubmit').addClass('disabled');
        }
    }

    function renderCaptchaCallbackCreditApp() {
        var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
        credit_application_captcha_default = grecaptcha.render(
            'recaptchaCrediteApplication', {
                'sitekey' : sitekey,
                'callback' : validateRecaptchaDefaultCreditApp,
                'expired-callback' : expireRecaptchaDefaultCreditApp
        });
    }

    function validateRecaptchaDefaultCreditApp(captchaResponse) {
        credit_application_captcha_success_default = true;
        checkCreditApplicationSubmitButton();
    }

    function expireRecaptchaDefaultCreditApp() {
        //console.log('inside expireRecaptchaDefault');
        //console.log('credit_application_captcha_success_default b4 :: ' + credit_application_captcha_success_default);
        grecaptcha.reset(credit_application_captcha_default);
        credit_application_captcha_success_default = false;
        checkCreditApplicationSubmitButton();
        //console.log('credit_application_captcha_success_default after :: ' + credit_application_captcha_success_default);
    }
}