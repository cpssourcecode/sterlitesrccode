// Dynamically fetch SVG to avoid including in each HTML page.
$(document).ready(function () {
    
    $( ".plp-warn-icon" ).tooltip({
        position: { my: "left top", at:"right bottom-150" },
        open: function (event, ui) {
            // console.log("inside tooltip " + $(this).parent(".item-price").width());
            ui.tooltip.css("width", $(this).parent(".item-price").width());
        }
    });
 $(document).on('show.bs.modal','#sharePageModal', function () {
    if(share_captcha_default == null && $("#isTransient").val() == 'true'){
    renderCaptchaCallbackShare();
    }
  });
    $(document).on('hide.bs.modal','#sharePageModal', function () {
       if( $("#isTransient").val() == 'true'){
         expireRecaptchaDefaultShare();
       }
     });
     $(document).on('show.bs.modal','#priceListUpdate', function () {
         if($("#isTransient").val() == 'true'){
         price_list_captcha_success_default = false;
         checkPriceListSubmitButton();
         grecaptcha.reset(price_list_captcha_default);
         }
          });
    $(document).on('show.bs.modal','#myModalCheck', function () {
        $('div').removeClass("custom-loading");
      });
    $.get('/assets/images/subway-icons/icons-svgdefs.svg', function (svg) {
        var div = document.createElement('div');
        div.innerHTML = new XMLSerializer().serializeToString(svg.documentElement);
        document.body.insertBefore(div, document.body.childNodes[0]);
    });

    $(window).on('resize load', function () {
        var inputs = $(".placeholder_change");
        if ($(window).width() < 768) {
            inputs.each(function () {
                $(this).attr('placeholder', $(this).data('placeholderShort'))
            });
        }
        else {
            inputs.each(function () {
                $(this).attr('placeholder', $(this).data('placeholderLong'))
            });
        }
    });

    var isAjaxEnabled = $('#ajaxEnabled').val();
    if(isAjaxEnabled == 'true'){
        $.ajax({
            url: '/xhr/productPricingAvailabilityInfo.jsp',
            type: 'post',
            dataType: 'html',
            success: function(data){
                $("#jdeContent").append(data);
                var isPriceAvailable = $('#isAvailablePrices_searchForm').val();
                priceAvailableMsg(isPriceAvailable);
           },
           error: function (xhr, ajaxOptions, thrownError) {
               var errorMsg = 'Ajax request failed: ' + xhr.responseText;
               $('#jdeContent').html(errorMsg);
           }
        });
    } else {
        var isPriceAvailable = $('#isAvailablePrices_searchForm').val();
        priceAvailableMsg(isPriceAvailable);
    }    

    // if expired or non-existent show Legal Disclaimer
    var cookieTOU = 'cookieTOU';
    var cookieExists = document.cookie.indexOf(cookieTOU) !== -1;

    if (!cookieExists) {
        var legalDisclaimer = document.querySelector('.js-legal-disclaimer');

        if (legalDisclaimer) {
            legalDisclaimer.classList.add('is-active');

            legalDisclaimer.querySelector('button').addEventListener('click', function () {
                localStorage.setItem('legalDisclaimer', true);
                legalDisclaimer.classList.remove('is-active');
                agreeWithTOU(cookieTOU);
            });
        }
    }
    
    $('#headerSearchQuestion').on('keyup', function (e) {

    }); 

     $('#headerSearchCategory').on('click', function(e) {
         if ($('#ta').length) {
            $("#ta").hide();
        }
    }); 
    
    $('#headerSearchCategory').on('focus', function (e) {
        $("#ta").hide();
    }); 
    
    $('#headerSearchQuestion').on('focus',function (e) {
        $("#ta").hide();
    }); 

    $('.quick-order-item').autocomplete({
        minLength:3,   
        source: function (request, response) {
            console.log('inside source autocomplete');
            // request.term is the term searched for.
            // response is the callback function you must call to update the autocomplete's 
            // suggestion list.
            $.ajax({
                url: "/xhr/fetchMatchingProducts.jsp",
                data: { 
                    Ntt: request.term +"*",
                    Ntk: $('#lookup').val(),
                    Nrpp: 5
                },
                dataType: "json",
                success: response,
                error: function () {
                    response([]);
                }
            });
        },
        create: function () {
            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                    .append('<a>' + item.label + ' [' + item.desc + ' / ' + item.value + ']' + '</a></li>')
                    .appendTo(ul);
            };
            $(this).data('ui-autocomplete')._resizeMenu = function () {
                var ul = this.menu.element;
                ul.outerWidth(this.element.outerWidth() * (1.5));
            }
        },
        open: function () {
            //$(this).autocomplete('widget').css('z-index', 9999);
            $('.ui-widget-content').css('position', 'relative').css('z-index', 1100);
        },
        select: function( event, ui ) {
            $( this ).val( ui.item.desc );
            event.stopPropagation();
            return false;
        }
    });  
});

$(document).click(function(event) {
      // hide a menu only in case if both menu and element have lost the focus
      if (!$(event.target).closest(".ta-results,#search-result-cont,.input-group").length) {
          $("#ta").hide();
      } 
});
$(document).on('keydown', function(e){
    typeAheadKeyEvents(e);
});

    function typeAheadKeyEvents(event) {
        if (event.keyCode == 40) {
            if($('.active a').find(':focus').length == 0){            
                var target = $(event.target);
                event.preventDefault();
                var activelink = target.is('.activeLink');
                if (activelink) {
                    target.removeClass('activeLink');
                    var parentClass = target.parents();
                    parentClass.removeClass('active');
                    var nextActive = parentClass.nextAll('.focusable');
                    var nextFocusable;
                    if(nextActive.length > 0){
                        nextFocusable = nextActive;
                    } else {
                        nextFocusable = parentClass.nextAll().find('.focusable');
                    }
                    var nextActiveClass = nextFocusable[0].className;
                    var activeClass = nextActiveClass.split(" ");
                    target.removeClass('activeLink');
                    $('.'+activeClass[0]).addClass('active');
                    $('.active a').addClass('activeLink');
                    $('.active a').focus();
                    $("#brands_list_div").show();
                    
                } else {
                    // Get all focusable elements on the page
                    // var $canfocus = $(':focusable');
                    var elm = $('.focusable:first');
                    if(elm.length > 0){
                        var crntClass = elm[0].className.split(" ");
                        var active = crntClass[0];                  
                        setTimeout(function(){
                            $('.' + active).addClass('active');
                            $('.active a').addClass('activeLink');
                            $("#brands_list_div").show();
                            $('.active a').focus();
                        },700)
                    }
                    
                }
            } 
        }
        if (event.keyCode == 38) {
            event.preventDefault();
            var target = $(event.target);
            console.log(target);
            var activelink = target.is('.activeLink');
            if (activelink) {
                target.removeClass('activeLink')
                var parentClass = target.parents();
                parentClass.removeClass('active');
                var prevActive =parentClass.prevAll('.focusable');
                var prevFocusable;
                if(prevActive.length > 0){
                   var lastElm = prevActive;
                   prevFocusable = lastElm[0];
                    
                } else {
                    var lastElm = parentClass.prevAll().find('.focusable');
                    prevFocusable = lastElm[lastElm.length-1];
                    console.log(prevFocusable);
                }
                var prevActiveClass = prevFocusable.className;
                var activeClass = prevActiveClass.split(" ");
                $('.'+activeClass[0]).addClass('active');
                $('.active a').addClass('activeLink');
                $('.active a').focus();
                $("#brands_list_div").show();
            }
        }
        if (event.keyCode == 27) { // escape to hide suggession       
              $("#ta").hide();
        }
    }

function reloadMinicartCount() {
    $.ajax({
        url: "/includes/gadgets/navbar-cart.jsp",
        cache: false
    }).done(function (html) {
        $("#navbar-cart .nav-cart").empty();
        $("#navbar-cart .nav-cart").html(html);
    });
}

function getErrorMessageFromResponse(response) {
    var errorMessage = "";

    var errorsArray = []; // to prevent error duplicates
    for (var i = 0; i < response.errors.length; i++) {
        if (indexOf(errorsArray, response.errors[i]) == -1) {
            errorsArray.push(response.errors[i]);
            if (errorMessage == "") {
                errorMessage = response.errors[i];
            } else {
                errorMessage += "<br/>" + response.errors[i];
            }
        }
    }
    if (errorMessage.length == 0) {
        errorMessage = "An unexpected error has occurred. Please contact customer service or try again at another time.";
    }
    return errorMessage;
}

function highlightErrorInputs(propertiesArray) {
    for (var i = 0; i < propertiesArray.length; i++) {
        $("#" + propertiesArray[i]).parent().addClass("has-error");
    }
}


function hideErrorModalMessages() {
    $("#message-modal-error-div").hide();
    $("#message-modal-error").html("");
}

function showErrorModalMessages(message) {
    $("#message-modal-error-div").show();
    $("#message-modal-error").html(message);
}

function ajaxModalErrorResponse() {
    var errorMessage = "An unexpected error has occurred. Please contact customer service or try again at another time.";
    showErrorModalMessages(errorMessage);
}

function ajaxErrorResponse() {
    var errorMessage = "An unexpected error has occurred. Please contact customer service or try again at another time.";
    showErrorMessages(errorMessage);
}

function submitAjaxForm(formId, responseHandler) {
    closeInfoMessages();
    var options = {
        success: responseHandler,
        dataType: 'json'
    };
    var form = $("#" + formId);
    form.ajaxForm(options);
    form.submit();
    return false;
}

function scrollToTop() {
    $("html, body").animate({
        scrollTop: 0
    }, 500);
    return false;
}

function isSuccess(data) {
    return (null != data && typeof data.code != "undefined" && data.code == "success");
}

var loggingDebug = false;

function logDebug(msg) {
    if (loggingDebug) {
        console.log(msg);
    }
}

function startLoader() {
    $('#status-loader').fadeIn();
}

function stopLoader() {
    $('#status-loader').fadeOut();
}

function linkToUrl(url) {
    window.location.href = url;
}

var usePartialReloads = true;
var needReloadAfterPop = false;

function handleManualPushedState(e) {
    if (needReloadAfterPop) {
        document.location.reload();
    }
}
// commented for SM-304
//window.addEventListener('popstate', handleManualPushedState, false);

function typeAheadAddToCart(elem) {
    var $btn = $(elem);
    var $qtyField = $btn.siblings('input.item-qty');

    var qty = $qtyField.val();
    var productId = $btn.data('productId');
    var skuId = $btn.data('skuId');

    $("#qtyFromTypeahead").val(qty);
    $("#productIdFromTypeahead").val(productId);
    $("#skuIdFromTypeahead").val(skuId);

    $('.popover.warn').popover('hide');
    if (qty == null || qty == '' || qty == undefined) {
        setTimeout(function () {
            $('.popover.warn').on('click', function (event) {
                event.preventDefault();
                $('.popover.warn').popover('hide');
            });
        }, 500);
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 || (function (p) {
            return p.toString() === "[object SafariRemoteNotification]";
        })(!window['safari'] || safari.pushNotification);
        if (isSafari) {
            $('.popover.warn').remove();
        }

        var qtyField = $('#qty' + elem.id);
        $(qtyField).popover({
            template: '<div class="popover warn" role="tooltip">\
                            <div class=""></div>\
                            <h3 class="popover-title"></h3>\
                            <strong><div class="popover-content"></div></strong>\
                            <button class=close><span class="glyphicon glyphicon-remove"></span></button>\
                        </div>'
        });

        $(qtyField).popover('show');
        $(qtyField).on('hidden.bs.popover', function () {
            $(qtyField).popover('destroy');
        });

        return;
    }

    var postfix = "";

    var errorDiv = "add-item-error-message";
    $("#" + errorDiv).empty();
    $("#" + errorDiv).hide();
    $("#minQtyError" + productId + postfix).empty();
    $("#minQtyError" + productId + postfix).hide();

    var dataString = $("#addToCartFromTypeahead").serialize();

    $.ajax({
        url: '/',
        type: "POST",
        data: dataString,
        dataType: "json",
        success: function (data) {
            if (null !== data && typeof data.errors !== "undefined" && data.error === "true") {
                showErrorsOnAddToCart(data, errorDiv, productId, "");
            } else {
                reloadMinicartCount();
                // addedToCartHangerShow();
                addedToCartWithRelatedHangerShow(productId);

                if (typeof reloadShoppingCartTable === 'function') {
                    reloadShoppingCartTable();
                }
            }
        },
        error: function (response) {
            showGenericError(response);
        }
    });
}

var ajaxGetBrands = function (searchTerm) {
    return $.ajax({
        url: "/global/gadgets/search-brands.jsp",
        dataType: "html",
        data: {
            Dy: "1",
            Nty: "1",
            Ntt: $.trim(searchTerm) + "*"
        },
        success: function (data) {
            var dataStr = $.trim(data);
            console.dir(data);
            if (dataStr) {
                $("#brands_list_div").html(data);
                showSearchedBrands();
            } else {

            }
        }
    });
}

function showSearchedBrands() {
    var $ta = $("#brands_list_div");
    $ta.show();
    $('section.allBrandsWide').hide();

    var item = $('#brandsSearch');
    var checkToHide = function () {
        if (item.val().length < 2) {
            $ta.hide();
            $('section.allBrandsWide').show();
            $('#brands_list_div').hide();
        } else {
            setTimeout(checkToHide, 150);
        }
    };
    setTimeout(checkToHide, 150);
}

function activateSearchBrand() {
    var lastReq = '';

    var item = $('#brandsSearch');
    if (item.val().length >= 2) {
        ajaxGetBrands(item.val());
        showSearchedBrands();
    }

    item.autocomplete({
        source: function (request, response) {
            console.log("autocomplete ajax send...");
            ajaxGetBrands(request.term);
        },
        minLength: 2,
        delay: 1000,
        select: function (event, ui) {
            return false;
        },
        search: function (event, ui) {
        }
    });
}

function callSearchBrand() {
    var item = $('#brandsSearch');
    if (item.val().length >= 2) {
    	$("#brands_list_div").empty();
        ajaxGetBrands(item.val());
        showSearchedBrands();
    }
}

function activateTypeAhead() {

    var lastReq = '';

    function showTypeahed() {
        var $ta = $("#ta");
        $ta.show();

        var $form = $("#searchform");
        lastReq = $("#headerSearchQuestion").val();

        var checkToHide = function () {


        };

        setTimeout(checkToHide, 150);

    }


    var item = $('#headerSearchQuestion');
    var enableAutoSuggestion=$('#enableAutoSuggestion').val();

    var $ta = $("#ta");
    item.on('keyup', function(){
    if(item.val().length<3) {
        $ta.hide(); 
    }
    })
    item.on('focus', function () {
       if ($ta.children().length > 0 && lastReq == item.val() && item.val().length>=3) {
            showTypeahed(); 
       }  
    });
    if(enableAutoSuggestion == 'true'){
    item.autocomplete({
        source: function (request, response) {
            var dimValId=$('#dimVal').val();
            console.log("select cat dimValId :: " + dimValId);
            console.log("autocomplete ajax send...");
            $("#headerSearchCategory").prop("disabled", true);
            $.ajax({
                url: "/includes/gadgets/typeahead.jsp",
                dataType: "html",
                data: {
                    N:dimValId,
                    Dy: "1",
                    Nty: "1",
                    Ntt: $.trim(request.term) + "*"
                },
                success: function (data) {
                    var dataStr = $.trim(data);
                    if (dataStr.length > 0) {
                        $("#ta").html(data);
                        eventsTypeahead();
                        showTypeahed();
                        $("#headerSearchCategory").prop("disabled", false);
                        $("#ta-suggession li").on("mouseover focusin", function(e) {
                            typeaheadHover($(this));
                        });
                        $("#ta-suggession .activeLink").on("focusin", function(e) {
                            typeaheadHover($(this));
                        });
                        $("#ta-brands li").on("mouseover focusin", function(e) {
                            e.stopImmediatePropagation();
                            $("#search-result-cont").hide();
                        });
                       $("#ta-categories li").on("mouseover focusin", function(e) {
                            e.stopImmediatePropagation();
                            $("#search-result-cont").hide();
                        });
                        var isPriceAvailable = $('#isAvailablePrices_searchForm').val();
                        priceAvailableMsg(isPriceAvailable);
                    } else {
                         $("#headerSearchCategory").prop("disabled", false);
                         $("#ta").html('');
                         $("#ta").hide();
                    }
                }
            });
        },
        minLength: 3,
        delay: 200,
        select: function (event, ui) {
//          var item = ui.item;
//          if (item && item.link) {
//              window.location = item.link;
//          }
            return false;
        },
    });
    }
}



function typeaheadHover(elem) {
        $("#search-result-cont").show();
        var attribute = elem.attr('class'); 
        var focusedAttr = attribute.split(" ");
        var hoverTerm = elem.attr('data-value');
        var searchContents = $(".div-"+focusedAttr[0]).attr('data-status');
        if (searchContents == 'populated') {         
            $('#search-result-cont').html($(".div-"+focusedAttr[0]).html());
                eventsTypeahead();
        } else {  
            $.ajax({
                type: "GET",
                url: "/includes/gadgets/typeahead-fetch-products.jsp?Dy=1&Nty=1&Ntt="+hoverTerm+"*&_=1544188007402",
                dataType: "html",
                data: '',
                success: function(results) {    
                                    $(".div-"+focusedAttr[0]).html(results);   
            $(".div-"+focusedAttr[0]).attr('data-status', 'populated')
        $('#search-result-cont').html($(".div-"+focusedAttr[0]).html());
                eventsTypeahead();
                }
            });
        }
    
       // elem.on("mouseleave", function() {
      //      $('#search-result-cont .' + attribute).hide();
    //    })          
}

function taLabelFilter(initalLabel) {
    var label = initalLabel.replace('<b>', '');
    label = label.replace('</b>', '');
    label = label.replace('&amp;nbsp;', '');
    label = label.replace('  ', '');
    label = label.replace('&lt;b&gt;', '');
    label = label.replace('&lt;/b&gt;', '');

    /*

        $('.dropdown-typeahead li a').click(
            function () {
                $('.typeahead-main').val($(this).html());
                $('.typeahead-main').val(function (i, old) {
                    return old
                        // $(this)
                        .replace('<b>', '')
                        .replace('</b>', '')
                        .replace('&nbsp;', '')
                        .replace('  ', '');
                });
            }
        );
    */


    return label;
}


/**
 * Reload content from source, but mark request that way it will return only
 * part of content. That content will be loaded in specified section
 *
 * @param link -
 *            link to source page
 * @param section -
 *            ID of section to reload (one of innerContent, secondaryContent,
 *            mainContent)
 * @param targetId -
 *            html element ID, which content will be replaced
 */
function partialReload(link, section, targetId, callbackFunction) {
    link = link || window.location.pathname;
    if (usePartialReloads) {
        //startLoader();
        $('.preload-cover').toggleClass('loader-active');
        $('.preload-cover .preload-spinner').toggle();

        // Remove loadSection parameter from URL to prevent users from "going back" to only the dynamic portion of the page
        historyUrl = removeURLParameter(link, 'loadSection');
        historyUrl = removeURLParameter(historyUrl, "Ssp");

        // push url
        window.history.pushState({
            urlPath: historyUrl,
            loadSection: section
        }, window.document.title, historyUrl);
        needReloadAfterPop = true;

        $.ajax({
            url: link,
            dataType: "html",
            data: {
                loadSection: section
            }
        }).done(function (responseText) {
            setTimeout(function() {
                loadMessage();
                imageLazyLoad();
            }, 1000);        
        	var noResultsFound = false;
            $('.preload-cover').toggleClass('loader-active');
            $('.preload-cover .preload-spinner').toggle();

            $('#spinner-on-start').removeClass('loader-active');
            $('#spinner-on-start .loading').hide();

            //stopLoader();
            if (responseText.indexOf("redirect-link") > -1) {
                var redirectLink = $("<div>").append($.parseHTML(responseText)).find("#redirect-link");
            } else if (responseText.indexOf("zero-results-container") > -1) {
                noResultsFound = true;
            }

            if (noResultsFound) {
                // show modal
                $("#link-noResultsModal_noResultsFound").click();
            } else {
                if (redirectLink && redirectLink.val()) {
                    logDebug("Redirect link: " + redirectLink.val());
                    linkToUrl(redirectLink.val());
                } else {
                    $("#" + targetId).html(responseText);
                    $('.preload-cover').removeClass('loader-active');
                    $('.preload-cover .loading').hide();
                    callbackFunction.call();
                }
            }
        });

        // scrollToTop();
        
    } else {
        linkToUrl(link);
    }
    
}

/* =================================================================================== */
/*
 * Gathers ids from all checked checkboxes
 * /*===================================================================================
 */
/*
function gatherChecked() {
    itemIds = [];
    var elem = document.getElementsByTagName('input');
    var inputs = new Array();
    for (i = 0, iarr = 0; i < elem.length; i++) {
        att = elem[i].getAttribute("name");
        if (att == 'check' || att == 'itemsInCart' || att == 'checkboxlisting') {
            inputs[iarr] = elem[i];
            iarr++;
        }
    }

    for (var i = inputs.length - 1; i >= 0; i--) {
        if (inputs[i].type === "checkbox" && inputs[i].checked) {
            if (inputs[i].id != "select-all") {
                itemIds.push(inputs[i].id);
            }
        }
    }
    return itemIds;
}

function gatherCheckedLists() {
    itemIds = [];
    var elem = document.getElementsByTagName('input');
    var inputs = new Array();
    for (i = 0, iarr = 0; i < elem.length; i++) {
        att = elem[i].getAttribute("name");
        if (att == 'check-list') {
            inputs[iarr] = elem[i];
            iarr++;
        }
    }

    for (var i = inputs.length - 1; i >= 0; i--) {
        if (inputs[i].type === "checkbox" && inputs[i].checked) {
            if (inputs[i].id != "select-all") {
                itemIds.push(inputs[i].id);
            }
        }
    }
    return itemIds;
}
*/

// function showErrors(data, extn){
// var errorMessage = "";
// var propertiesArray = new Array();
// var errorsArray = new Array();
// for(var i=0; i<data.errors.length; i++){
// if (indexOf(errorsArray,data.errors[i]) == -1){
// errorsArray.push(data.errors[i]);
// if (errorMessage == ""){
// errorMessage = data.errors[i];
// }else{
// errorMessage += "<br/>" + data.errors[i];
// }
// }
// propertiesArray.push(data.properties[i]);
// }
// if (errorMessage != ""){
// openErrorMessage(errorMessage, extn);
// }
// if (propertiesArray != ""){
// highlightErrors(propertiesArray, extn);
// }
// }

/*
 * Need to add this method to loop through errors added via ajax utils through
 * vsg module
 */
/*
function showVSGErrors(data, extn) {
    var errorMessage = "";
    var propertiesArray = new Array();
    var errorsArray = new Array();
    for (var i = 0; i < data.details.length; i++) {
        if (indexOf(errorsArray, data.details[i]) == -1) {
            errorsArray.push(data.details[i]);
            if (errorMessage == "") {
                errorMessage = data.details[i];
            } else {
                errorMessage += "<br/>" + data.details[i];
            }
        }
        propertiesArray.push(data.properties[i]);
    }
    if (errorMessage != "") {
        openErrorMessage(errorMessage, extn);
    }
    if (propertiesArray != "") {
        highlightErrors(propertiesArray, extn);
    }
}
*/

/* =================================================================================== */
/*
 * Adds error messages to error-messages div and shows it
 * /*===================================================================================
 */

// function openErrorMessage(errorMessage, extn){
// $("#error-messages"+extn).html(errorMessage);
// $("#error-messages"+extn).show();
// }
/* =================================================================================== */
/*
 * Remove error messages from error-messages div and hide it
 * /*===================================================================================
 */

// function closeErrorMessage(extn){
// $("#error-messages"+extn).html("");
// $("#error-messages"+extn).hide();
// clearHighlighting();
// }
/* =================================================================================== */
/*
 * Adds form error class to all inputs with id = passed
 * /*===================================================================================
 */

/*
function highlightErrors(propertiesArray, extn) {
    for (var i = 0; i < propertiesArray.length; i++) {
        var el = $("#" + propertiesArray[i] + extn).parent();
        el.addClass("has-error");
    }
}
*/

/* =================================================================================== */
/*
 * Generic method of pulling element count
 * /*===================================================================================
 */

/*
function indexOf(array, element) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == element) {
            return i;
        }
    }
    return -1;
}
*/

/* =================================================================================== */
/*
 * Remove highlighting from all inputs
 * /*===================================================================================
 */
/*
function clearHighlighting() {
    inputs = document.getElementsByTagName('div');
    for (var i = 0; i < inputs.length; i++) {
        removeClass(inputs[i], "has-error");
    }
    inputs = document.getElementsByTagName('select');
    for (var i = 0; i < inputs.length; i++) {
        removeClass(inputs[i], "has-error");
    }
}
*/

/* =================================================================================== */
/*
 * Remove class from element
 * /*===================================================================================
 */
/*
function removeClass(ele, cls) {
    if (hasClass(ele, cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        ele.className = ele.className.replace(reg, '');
    }
}
*/

/* =================================================================================== */
/*
 * Checks if element has class
 * /*===================================================================================
 */

/*
function hasClass(ele, cls) {
    return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
}

function toggleCheckboxes(check) {
    var elem = document.getElementsByTagName('input');
    var checkboxes = new Array();
    for (i = 0, c = 0; i < elem.length; i++) {
        att = elem[i].getAttribute("name");
        if (att == 'check' || att == 'checkboxlisting') {
            checkboxes[c] = elem[i];
            c++; // haha
        }
    }
    for ( var i in checkboxes) {
        if (checkboxes[i].checked != check.checked) {
            checkboxes[i].checked = check.checked;
        }
    }
}
*/

function executeFunctionByName(functionName, context) {
    var args = [].slice.call(arguments).splice(2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(this, args);
}


function formSubmitOnEnter(event, callback) {
    if (event.which == 10 || event.which == 13) {
        event.preventDefault();
        executeFunctionByName(callback, window);
        return false;
    }
}

/*
//header search
function handleSearch() {
    return submitAjaxForm("globalSearchForm", handleSearchResponse);
}

function handleSearchResponse(data) {
    if (isError(data)) {
    } else if (isSuccess(data)) {
        linkToUrl(data.successUrl);
    }
}

// Project Lists
function checkFormRedirect(data, args) {
    var flag = false;
    if (typeof data.param !== "undefined") {
        if (typeof data.param.redirectUrl !== "undefined") {
            var url = data.param.redirectUrl;
            if (typeof args === "undefined") {
                linkToUrl(url);
            } else {
                linkToUrlPost(url, args);
                //linkToUrl(url + "?to=" + returnUrl);
            }
            flag = true;
        }
    }
    return flag;
}

function selectNewPrjl() {
    $("#newPrjlRadio").radio('check');
}

function selectExistPrjl() {
    $("#existPrjlRadio").radio('check');
}

function addItemToProjectList() {
    startLoader();
    closeInfoMessages();
    $("#formCreateNewGiftlist").val($("input[name='radioNewGiftlist']:checked").val());
    $("#formProjectListId").val($("#projectListId").val());
    $("#formProjectListName").val($("#projectListName").val());
    $("#formProjectListPoNumberValue").val($("#projectListPoNumberValue").val());
    $("#formProjectListNotes").val($("#projectListNotes").val());
    
    return submitAjaxForm("project-list-add", addItemToProjectListHandler); 
}

function addItemToProjectListHandler(data) {
    stopLoader();
    if (!checkFormRedirect(data, {message: data.details})) {
        if (isError(data)) {
            showErrorsInCustomDIV(data, 'prjl-error');
        } else if (isSuccess(data)) {
            closeModal("addToProjectListModal");
            openSuccessMessage(data);
            scrollToTop();
        }
    }
}

function moveToAddToProjectModalPdp() {
    closeInfoMessages();
    startLoader();
    $("#prjlCatalogRefIds").val($("#itemSkuId").val());
    $("#sku-qty").attr("name", $("#itemSkuId").val());
    $('#sku-qty').val($('#itemQty').val());
    return submitAjaxForm("moveToAddModalPrjl", moveToAddToProjectModalResponse);
    
}

function moveToAddToProjectModalCart() {
    closeInfoMessages();
    startLoader();
    $("input[name='ciCheck']:checked").each(function() {
        var cid = $(this).val();
        var qty = $("input[name='" + cid + "']").val();
        $("#prjl" + cid).val(qty);
        $("#prjlCid" + cid).prop('disabled', false);
        $("#prjl" + cid).prop('disabled', false);
    });
    
    return submitAjaxForm("moveToAddModalPrjl", moveToAddToProjectModalCartResponse);
}

function moveToAddToProjectModalResponse(data) {
    stopLoader();
//  $("input[name='ciCheck']").each(function() {
//      var cid = $(this).val();
//      $("#prjlCid" + cid).prop('disabled', false);
//      $("#prjl" + cid).prop('disabled', false);
//  });
    if (!checkFormRedirect(data)) {
        if (isError(data)) {
            showErrors(data);
            scrollToTop();
        } else if (isSuccess(data)) {
            closeInfoMessages();
            reloadMinicart();

            var itemsList = $("#itemSkuId").val() + "=" + $('#itemQty').val();
            var params = {
                productId : $("#prjlProductId").val(),
                catalogRefId : $("#itemSkuId").val(),
                quantity : $("#itemQty").val()
            };
            loadModal("/modal/project-list-add.jsp", params,
                    "addToProjectListModal");
        }
    }
}

function moveToAddToProjectModalCartResponse(data) {
    stopLoader();
    $("input[name='ciCheck']:checked").each(function() {
        var cid = $(this).val();
        $("#prjlCid" + cid).prop('disabled', true);
        $("#prjl" + cid).prop('disabled', true);
    });
    if (!checkFormRedirect(data, {to: "/checkout/cart.jsp"})) {
        if (isError(data)) {
            showErrors(data);
        } else if (isSuccess(data)) {
            closeInfoMessages();
            //reloadMinicart();

            var paramList = "";
            $("input[name='ciCheck']:checked").each(function() {
                var cid = $(this).val();
                var qty = $("input[name='" + cid + "']").val();
                paramList = paramList.concat(cid, "=", qty, ",");
            });
            var params = {
                commerceItemIds : paramList
            };
            loadModal("/modal/project-list-add.jsp", params,
                    "addToProjectListModal");
        }
    }
}

// Live Chat
function setLifeChatConfiguration(pause, availabilityUrl, idleTimeoutToShowLCButton, idleTimeoutToShowLCWindow) {
    checkLifeChatAvailability(availabilityUrl);
    setInterval(function() {checkLifeChatAvailability(availabilityUrl);}, pause * 1000);
    
    if (idleTimeoutToShowLCButton > 0) {
        $.idleTimer(idleTimeoutToShowLCButton * 1000);
        $(document).bind("idle.idleTimer", function(){
            showLiveChatButton();
            $.idleTimer('destroy');
            setupIdleTimer(idleTimeoutToShowLCWindow, openLiveChatWindow);
        });
    } else {
        if (idleTimeoutToShowLCButton == 0) {
            showLiveChatButton();
        }
        setupIdleTimer(idleTimeoutToShowLCWindow, openLiveChatWindow);
    }
    
    */
/*setTimeout(function() {
        showLiveChatButton()
    }, 60000);*/
/*

    $(".contact-us-cover .close-button").click(function() {
        $(this).parent().parent().remove();
        closeLiveChatButton();
    });
}

function setupIdleTimer(timeout, idleFunction) {
    if (timeout == 0) {
        idleFunction.call();
    } else if (timeout > 0) {
        $.idleTimer(timeout * 1000);
        $(document).bind("idle.idleTimer", function(){
            idleFunction.call();
            $.idleTimer('destroy');
        });
    }
}

function showLiveChatButton() {
    logDebug("Show button on Idle " + new Date());
    $(".contact-us-button").removeClass('hide');
    $(".contact-us-button").children('.btn').addClass('animated bounceInUp');
}

function openLiveChatWindow() {
    logDebug("Open chat on Idle " + new Date());
    $(".contact-us-cover a:visible").click();
}

function closeLiveChatButton() {
    return submitAjaxForm("closeLiveChatButton", closeLiveChatButtonResponse);
}

function closeLiveChatButtonResponse(data) {
    if (isError(data)) {
        console.log(data);
    }
}

function checkLifeChatAvailability(availabilityUrl) {
    $.ajax({
        type: "GET",
        url: availabilityUrl,
        dataType: "json",
        cache: false,
        success: function(data) {
            updateLifeChat(data.availability, "._lpChatBtnActive");
            updateLifeChat(!data.availability, "._lpChatBtnNotActive");
        },
        error: function(data) {}
    });
}

function updateLifeChat(flag, selector) {
    $(selector).each(function() {
        if(flag) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

function setCheckBoxToggleOption() {
    $("input[name='gicheckbox']").click(function(event) {
        event.stopPropagation();
    });

    $("input[name='ciCheck']").click(function(event) {
        event.stopPropagation();
    });
    
    $(".checkbox-cell").click(function() {
        var checkBoxes = $(this).children('.checkbox').children('label').children('input[type=checkbox]');
        checkBoxes.prop("checked", !checkBoxes.prop("checked"));
    });

    $(".checkbox-toggle-cart").click(function() {
        if ($(this).children('label').children('input[type=checkbox]:checked').length > 0) {
            $(".checkbox-cell").children('.checkbox').children('label').children('input[type=checkbox]').prop('checked', 'checked');

        } else {
            $(".checkbox-cell").children('.checkbox').children('label').children('input[type=checkbox]').removeAttr('checked');
        };
    });
}
*/

// Share Page
function handleSharePage() {
    clearHighlighting();

    var url = window.location.href;
    if (url.indexOf(window.location.pathname) > -1) {
        url = url.substr(url.indexOf(window.location.pathname));
    }

    $('#share-page-url').val(url);
    var dataString = $('#share-page-form').serialize();

    $.ajax({
        url: $('#share-page-form').attr('action'),
        type: "POST",
        data: dataString,
        dataType: "json",
        success: sharePageSuccess,
        error: sharePageError
    });
    return false;
}

function sharePageSuccess(data) {
    if ('success' === data.code) {
        $('#sharePageModal').modal('hide');
    }
    else {
        var propertiesArray = [];
        var errorsArray = [];
        for (var i = 0; i < data.errors.length; i++) {
            errorsArray.push(data.errors[i]);
            propertiesArray.push(data.properties[i]);
        }
        for (var i = 0; i < propertiesArray.length; i++) {
            var errorMessageSmall = $("#share-error-message-" + propertiesArray[i]);
            errorMessageSmall.html(errorsArray[i]);
            errorMessageSmall.show();
            errorMessageSmall.parent().addClass("has-error");
        }
    }
    
}

function sharePageError(x, s, e) {
    console.log("xhr: " + x + "; s: " + s + "; e: " + e);
//    share_captcha_success_default = false;
//    checkShareSubmitButton();
}

function openInNewTab(url) {
    window.open(url, '_blank');
}

function getPriceListUpdate() {
    var url = $("#recentPriceListUpd").val();
    openInNewTab(url);
}

function getPriceBooksSection() {
    var url = $("#priceBook").val();
    openInNewTab(url);
}

function priceListUpdatesSubmit() {
    closeAllMessages();
    var dataString = $('#price-list-update-form').serialize();

    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function (response) {
            if ('success' == response.code) {
                pl_updates_SubmitSuccess();
            } else {
                pl_updates_SubmitError(response);
            }

        },
        error: function (response, textStatus, errorThrown) {
            ajaxErrorResponse();
        }
    });
}

function pl_updates_SubmitSuccess() {
    $("#link-priceListUpdate").click();
    $('#price-list-update-form')[0].reset();
}

function pl_updates_SubmitError(response) {
    showError(response);
}

function testimonialSubmit(form) {
    if($("#isTransient").val() == 'true'){
    testimonial_captcha_success_default = false;
    checkTestimonialSubmitButton();
    grecaptcha.reset(testimonial_captcha_default);
    }
    $("#testimonialFirstName_" + form).parent().removeClass("has-error");
    $("#testimonialLastName_" + form).parent().removeClass("has-error");
    $("#testimonialCompany_" + form).parent().removeClass("has-error");
    $("#testimonialMessage_" + form).parent().removeClass("has-error");
    $("#testimonialEmail_" + form).parent().removeClass("has-error");
    closeAllMessages();
    $("#firstName_hidden_" + form).val($("#testimonialFirstName_" + form).val());
    $("#lastName_hidden_" + form).val($("#testimonialLastName_" + form).val());
    $("#email_hidden_" + form).val($("#testimonialEmail_" + form).val());
    $("#company_hidden_" + form).val($("#testimonialCompany_" + form).val());
    $("#message_hidden_" + form).val($("#testimonialMessage_" + form).val());
    var dataString = $('#testimonial-form_' + form).serialize();
    if ($("#testimonialFirstName_" + form).val() == "") {
        $("#testimonialFirstName_" + form).parent().addClass("has-error");
    }
    if ($("#testimonialLastName_" + form).val() == "") {
        $("#testimonialLastName_" + form).parent().addClass("has-error");
    }
    if ($("#testimonialCompany_" + form).val() == "") {
        $("#testimonialCompany_" + form).parent().addClass("has-error");
    }
    if ($("#testimonialMessage_" + form).val() == "") {
        $("#testimonialMessage_" + form).parent().addClass("has-error");
    }
    var testimonialMail = $("#testimonialEmail_" + form).val();
    if (testimonialMail != "") {
        regex  = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
        if(!regex.test(testimonialMail)){
        $("#testimonialEmail_" + form).parent().addClass("has-error");}
    }
    if($("#message-error-div").html().trim() === ""){
        $("#message-error-div").html('<div class="alert alert-danger">\
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                        <span aria-hidden="true">\
                            <span class="glyphicon glyphicon-remove-circle"></span>\
                        </span>\
                    </button><div id="message-error"></div>\
                    </div>');
    }
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: 'json',
        success: function (response) {
            if ('success' == response.code) {
                testimonial_SubmitSuccess(form);
            } else {
                testimonial_SubmitError(response);
            }

        },
        error: function (response, textStatus, errorThrown) {
            ajaxErrorResponse();
        }
    });
}

function testimonial_SubmitSuccess(form) {
    $("#link-testimonialModal").click();
     clearTestimonialFields(form);
    //setTimeout( function() { document.location.href = "/"; }, 5000);
}

function testimonial_SubmitError(response) {
    showError(response);
    scrollToTop();
}

function clearTestimonialFields(form) {
    $("#testimonialFirstName_" + form).val("");
    $("#testimonialLastName_" + form).val("");
    $("#testimonialEmail_" + form).val("");
    $("#testimonialCompany_" + form).val("");
    $("#testimonialMessage_" + form).val("");
}

function validateNumeric(event) {
    return event.ctrlKey
        || event.altKey
        || (47 < event.keyCode && event.keyCode < 58 && event.shiftKey == false)
        || (95 < event.keyCode && event.keyCode < 106)
        || (event.keyCode == 8) || (event.keyCode == 9)
        || (event.keyCode > 34 && event.keyCode < 40) || (event.keyCode == 46)
}

function removeAreaExpanded(source) {
    var haveItShipped = source.value;
    if (haveItShipped == 'haveItShip') {
        $('#addToCartButton').show();
        $('#haveItShipNoteText').show();
        $('#pickItUpNoteText').hide();
    } else {
        $('#addToCartButton').hide();
        $('#haveItShipNoteText').hide();
        $('#pickItUpNoteText').show();
    }

    $("#collapseCheck").hide();
    $("#collapsePickItUp").hide();
    $('#only-one-msg').show();
}


var previousQty = $("#addToCartFromModalQty").val() || 1;

function checkAvailability(prodId) {
    var haveItShipped = $("#myCustomRadioLabel2").hasClass("checked");
    var expandedPickitUp = $("#collapsePickItUp").css("display") == "block";
    var expandedHaveItShipped = $("#collapseCheck").css("display") == "block";

    if (haveItShipped) {
        qty = $("#addToCartFromModalQty").val() || 1;
    }
    if ((haveItShipped && expandedHaveItShipped && (previousQty === qty)) || (!haveItShipped && expandedPickitUp)) {
        //$("#link-collapseCheck").click();
    } else {
        if (haveItShipped && !isNormalInteger(qty)) {
            $("#dismiss-modal").click();
            $("#modal-add-to-cart-error").modal("show");

            var qtyError = 'Invalid value for a number';
            var $errorMessage = $("#add-item-error-message");

            $errorMessage.html(qtyError);
            $errorMessage.show();
        } else {
            $("#collapseCheck").html("");
            var qty = $(".qty" + prodId).val() || 1;
            if (haveItShipped) {
                qty = $("#addToCartFromModalQty").val() || 1;
                previousQty = qty;
            }
            $('input[name="radioEx1"]').attr('disabled',true);
            var url = "/global/modals/modal-collapse-check.jsp?prodId=" + prodId + "&qty="
                + qty + "&haveItShipped=" + haveItShipped;
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'html',
                data: ({
                    action: 'loadModalDiv'
                }),
                complete: function () {
                    //$("#link-collapseCheck").click();
                },
                success: function (data) {
                    data = $.trim(data);
                    if (haveItShipped) {
                        $("#collapseCheck").html(data);
                        $("#collapseCheck").show();
                        $("#collapsePickItUp").removeClass("in");
                        $('#only-one-msg').hide();

                        var availableQty = 0;
                        if ($('#available-qty')) {
                            availableQty = parseInt($('#available-qty').text());
                        }
                        if (!availableQty || availableQty < qty) {
                            $('#qty-important').show();
                        } else {
                            $('#qty-important').hide();
                        }

                        if (availableQty) {
                            $('#addToCartFromModalQty').keyup(function (event) {
                                var quantity = $('#addToCartFromModalQty').val();
                                if (quantity > availableQty) {
                                    if (!$('#qty-important').is(":visible")) {
                                        $('#qty-important').show();
                                    }
                                } else {
                                    $('#qty-important').hide();
                                }
                            });
                        }
                    } else {
                        $("#collapsePickItUp").html(data);
                        $("#collapsePickItUp").show();
                        $("#collapseCheck").removeClass("in");
                    }
                    eventsModalCheckAvailabilitAddtnlControls();
                    $('.scrollbar-inner').scrollbar({
                        ignoreMobile: false,
                        ignoreOverlay: true
                    });
                    $('#myModalCheck').modal('handleUpdate');
                    $('input[name="radioEx1"]').attr('disabled',false);
                }
            });
        }
    }
}

function isNormalInteger(str) {
    var integerRegExPattern = /^[1-9]\d*$/;
    return integerRegExPattern.test(str);
}

function showCheckAvailabilityModal(prodId) {
    $("#modal-check-availability-div").html("");
    $.ajax({
        type: "GET",
        url: "/global/modals/modal-check-availability.jsp?prodId=" + prodId,
        dataType: 'html',
        data: ({action: 'loadModalDiv'}),
        complete: function () {
            $("#link-myModalCheck").click();
        },
        success: function (data) {
            data = $.trim(data);
            $("#modal-check-availability-div").html(data);
            $("#addToCartFromModalQty").val($("#qty" + prodId).val() || 1);
            eventsModalCheckAvailabilityMainControls();

            var isPriceAvailable = $('#isAvailablePrices_searchForm').val();
            priceAvailableMsg(isPriceAvailable);
            if (isPriceAvailable === 'true') {
                checkAvailability(prodId);
            }
        }
    });
}

function addNonSelectedMaterialListListener(buttonId) {
    if (buttonId) {
        $(buttonId).click(function () {
                if (document.getElementById("control-ml-giftlist").value === 'default-material-list-selection') {
                    document.getElementById("select-material-list-warning").style.display = "block";
                }
            }
        )
    }
}

function removeNonSelectedMaterialListListener(buttonId) {
    if (buttonId) {
        $(buttonId).unbind("click");
    }
}

function onModalAddToMaterialListLoaded(prodId) {
    var isMulti = $("#modalAddToMaterialIsMulti").val();
    $('.newselect-addnew').hide();

    if (isMulti === 'true') {
        addNonSelectedMaterialListListener("#add-to-list-multi");
    } else {
        addNonSelectedMaterialListListener("#add-to-list-single");
    }

    $('.newselect-list').change(
        function () {
            document.getElementById("select-material-list-warning").style.display = "none";
            if (isMulti === 'true') {
                removeNonSelectedMaterialListListener("#add-to-list-multi");
                } else {
                removeNonSelectedMaterialListListener("#add-to-list-single");
            }

            if ($(this).val() == 'addnew') {
                $('.newselect-addnew').show();
                if (isMulti == 'true') {
                    document.getElementById("add-to-list-multi").onclick = function () {
                        materialList_createAddItems();
                        return false;
                    };
                } else {
                    document.getElementById("add-to-list-single").onclick = function () {
                        materialList_createNew(prodId);
                    };
                }
            } else {
                $('.newselect-addnew').hide();
                if (isMulti == 'true') {
                    document.getElementById("add-to-list-multi").onclick = function () {
                        materialList_addItemsToList();
                        return false;
                    };
                } else {
                    document.getElementById("add-to-list-single").onclick = function () {
                        materialList_addItemToList(prodId);
                    };
                }
            }
        });
}

function showAddToListModal(prodId, skuId) {

    $("#modal-add-to-material-list-div").html("");
    var qty = $("#qty" + prodId).val();
    $.ajax({
        type: "GET",
        url: "/global/modals/modal-add-to-material-list.jsp",
        dataType: 'html',
        data: ({prodId: prodId, skuId: skuId, qty: qty}),
        complete: function () {
            $("#modal-add-to-material-list-link").click();
        },
        success: function (data) {
            data = $.trim(data);
            $("#modal-add-to-material-list-div").html(data);
            onModalAddToMaterialListLoaded(prodId);
            $('.bootstrap-select').selectpicker({
                style: 'form-control',
                size: 7
            });
            $('.create-new-list-btn').on('click', function () {
                $(this).parents('.modal').find('.modal-footer').hide();
                if ($('.create-new-list-collapse').hasClass('in')) {
                    $(this).parents('.modal').find('.modal-footer').show();
                }
            });
        }
    });
}

/* Services Page Contact Us */
$(document).ready(function () {
    $('.services-contact-container .services-contact-trigger-link').on('click', function () {
        var container = $(this).parents('.services-contact-container');

        container.children('.services-contact-trigger').slideUp();
        container.children('.services-contact-form').slideDown();
    });

    $('.manager .rm-contact-trigger-link').on('click', function () {
        $(this).parents('.manager').find('.rm-contact-container .rm-contact-form').slideDown();
    });

    $('.services-contact-container .services-contact-submit').on('click', function () {
        var form = $(this).parents('form');
        var dataString = form.serialize();
        if($("#isTransient").val() == "true"){  
        service_captcha_success_default = false;
        checkServiceSubmitButton();
        grecaptcha.reset(service_captcha_default);
        }
        form.find('.has-error').removeClass('has-error');

        $.ajax({
            url: '/',
            type: 'POST',
            data: dataString,
            dataType: 'json',
            success: inlineContactSuccess('services', true, form.parents('.services-contact-container')),
            error: inlineContactError
        });
    });

    $('.rm-contact-container .rm-contact-submit').on('click', function () {
        var form = $(this).parents('form');
        var dataString = form.serialize();

        form.find('.has-error').removeClass('has-error');

        $.ajax({
            url: '/',
            type: 'POST',
            data: dataString,
            dataType: 'json',
            success: inlineContactSuccess('rm', false, form.parents('.rm-contact-container')),
            error: inlineContactError
        });
    });

    $('.services-contact-container .services-contact-cancel').on('click', function () {
        var container = $(this).parents('.services-contact-container');
        $(this).parents('form')[0].reset();
        if($("#isTransient").val() == "true"){ 
        service_captcha_success_default = false;
        checkServiceSubmitButton();
        grecaptcha.reset(service_captcha_default);
        }
        container.children('.services-contact-form').slideUp();
        container.children('.services-contact-trigger').slideDown();
    });

    $('.rm-contact-container .rm-contact-cancel').on('click', function () {
        $(this).parents('.rm-contact-container').children('.rm-contact-form').slideUp();
        $(this).parents('form')[0].reset();

        $(this).parents('form').find('.has-error').removeClass('has-error');
        $(this).parents('.manager').find('.rm-contact-container .rm-contact-form-errors').css('display', 'none');
    });
});

function inlineContactSuccess(classbase, triggershow, container) {
    return function (data) {
        if ('success' === data.code) {
            container.children('.' + classbase + '-contact-form').slideUp();
            container.find('.' + classbase + '-contact-form form')[0].reset();
            container.children('.' + classbase + '-contact-confirmation').slideDown();

            var closeConfirmation = function () {
                container.children('.' + classbase + '-contact-confirmation').slideUp();
                if (triggershow) {
                    container.children('.' + classbase + '-contact-trigger').slideDown();
                }
            };

            container.find('.' + classbase + '-contact-confirmation .' + classbase + '-contact-confirm').on('click', closeConfirmation);
            window.setTimeout(closeConfirmation, 3000);
        }
        else {
            var errorMessage = "";
            var propertiesArray = [];
            var errorsArray = [];
            for (var i = 0; i < data.errors.length; i++) {
                if (indexOf(errorsArray, data.errors[i]) == -1) {
                    errorsArray.push(data.errors[i]);
                    if (errorMessage == "") {
                        errorMessage = data.errors[i];
                    } else {
                        errorMessage += "<br/>" + data.errors[i];
                    }
                }
                propertiesArray.push(data.properties[i]);
            }
            if (errorMessage != "") {
                var errorMessagesDiv = container.find('.' + classbase + '-contact-form-errors');
                errorMessagesDiv.html(errorMessage);
                errorMessagesDiv.show();

                // Highlight fields with errors
                for (var i = 0; i < propertiesArray.length; i++) {
                    container.find('.' + propertiesArray[i]).parent().addClass("has-error");
                }
            }
        }
    };

}

function inlineContactError(x, s, e) {

    console.log("xhr: " + x + "; s: " + s + "; e: " + e);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function getGuestAccessCount() {
    return Number(getCookie('guestAccessCount'));
}

function setGuestAccessCount(newCount) {
    setCookie('guestAccessCount', newCount, 365);
}

function getGuestLastAccess() {
    return Number(getCookie('guestLastAccess'));
}

function setGuestLastAccess(time) {
    setCookie('guestLastAccess', time, 365);
}

function updateGuestLastAccessCookie() {
    var MILLISECONDS_IN_MINUTE = 60000;

    var timeDiff = (new Date().getTime()) - getGuestLastAccess();
    if (timeDiff > window.siteProperties.accessCountInterval * MILLISECONDS_IN_MINUTE) {
        setGuestAccessCount(getGuestAccessCount() + 1);
    }

    if (getGuestAccessCount() > window.siteProperties.accessCountBeforeModalShowed) {
        $("#modalRegister").modal('show');
        setGuestAccessCount(0);
    }

    setGuestLastAccess(new Date().getTime());
}

function addTitleAndDescription(productName, productDescription) {
    // var title = document.createElement("title");
    var titleText;
    var descriptionText;
    if (productName !== null && productName !== "") {
        titleText = productName + " - " + productDescription + " - Columbia Pipe & Supply Co.";
        descriptionText = "Shop " + productName + " - " + productDescription + " at Columbia Pipe & Supply Co. for one-stop shopping that saves you valuable time and money - less paperwork and more productivity";
    } else {
        titleText = productDescription + " - Columbia Pipe & Supply Co.";
        descriptionText = "Shop " + productDescription + " at Columbia Pipe & Supply Co. for one-stop shopping that saves you valuable time and money - less paperwork and more productivity";
    }
    // title.innerText = titleText;
    //document.getElementsByTagName('head')[0].appendChild(title);

    $('meta[name="description"]').attr("content", descriptionText);
}

function priceAvailableMsg(isAvailable) {
    if (typeof isAvailable === 'undefined') {
        return;
    }
    var $errorPricesMessage = $("#error-message-connection-JDE");

    if (isAvailable === 'true') {
        $("#isPricingUnavailable").val('false');
        $(".btn-proceed-checkout").attr("disabled", false);
        $(".addToCartButton").attr("disabled", false);
        $(".check-avail-submit").attr("disabled", false);
        $errorPricesMessage.hide();
    } else {
        $("#isPricingUnavailable").val('true');
        $(".btn-proceed-checkout").attr("disabled", true);
        $(".addToCartButton").attr("disabled", true);
        $(".check-avail-submit").attr("disabled", true);
        $errorPricesMessage.show();

        if ($('#body').innerWidth() > 992) {
            $('#body').css('padding-top', '60px');
        }
    }
}

function onModalCsLoaded() {
    $(document).ready(function () {
        if ($('#myCustomRadioLabel').length > 0) {
            $('#myCustomRadioLabel').click();
        }
        $("#confirmBtn, #confirmBtn1, #confirmBtn2").click(function () {
            doModalCS_SelectShippingAddress(doModalCS_SelectShippingAddressSuccess);
            $("#differentSessionAddress").load("/global/modals/different-shipto.jsp?close=false");
        });
    });

    $('#modal-cs-input-search').keypress(function (e) {
        if (e.which == 13) {
            doModalCS_Search();
            return false;
        }
    });
}

function submitRequestAQuote() {
    var message = $('#request-quote-textarea').val();
    $('#request').val(message);
    $('span #reqSubUpload').click();
}

function removeParameterFromUrl(url, parameter) {
    return url
        .replace(new RegExp('[?&]' + parameter + '=[^&#]*(#.*)?$'), '$1')
        .replace(new RegExp('([?&])' + parameter + '=[^&]*&'), '$1');
}

function removeURLParameterRQ(params, parameter) {
    params = (params.indexOf('?') === 0 ? params.substr(1) : params);
    var pars = params.split(/[&;]/g);
    var prefix = encodeURIComponent(parameter) + '=';
    //reverse iteration as may be destructive
    for (var i = pars.length; i-- > 0;) {
        //idiom for string.startsWith
        if (pars[i].lastIndexOf(prefix, 0) !== -1) {
            pars.splice(i, 1);
        }
    }

    params = pars.join('&');
    return params;
}

// Adds error messages to error-messages div and shows it
function openErrorMessage(errorMessage) {
    $("#error-messages").html(errorMessage);
    $("#error-messages").show();
}

// Handles upload/confirmation button toggling
function updateOrderPadButtons(btn) {
    console.log(btn);
    if (btn === 'importUpload') {
        $('#qoManualSubmit').hide();
        $('#qoUploadSubmit').show();
    }
    else {
        $('#qoUploadSubmit').hide();
        $('#qoManualSubmit').show();
    }
}

function onRequestQuoteModalLoaded() {
    $(document).ready(function () {
        var baseParams = removeURLParameterRQ(window.location.search, "sreqmodal");
        baseParams = removeURLParameterRQ(baseParams, "_requestid");
        baseParams = removeURLParameterRQ(baseParams, "productId");
        baseParams = removeURLParameterRQ(baseParams, "qo");
        baseParams = removeURLParameterOP(baseParams, 'quickomodal');
        orbaseParams = removeURLParameterRQ(baseParams, 'showCS');
        if (baseParams == "") {
            //?
            $('#rqsuccess').val(window.location.pathname + "?sreqmodal=true&scess=true");
            $('#rqfail').val(window.location.pathname + "?sreqmodal=true");
        }
        else {
            //&
            $('#rqsuccess').val(window.location.pathname + "?" + baseParams + "&sreqmodal=true&scess=true");
            $('#rqfail').val(window.location.pathname + "?" + baseParams + "&sreqmodal=true");
        }

        var url = window.location.href;
        url = removeParameterFromUrl(url, "sreqmodal");
        url = removeParameterFromUrl(url, "scess");

        $('#requestQuote').on('hidden.bs.modal', function (e) {
            window.history.pushState('page', 'title', url);
            $("#request-quote-modal").load("/global/modals/request-quote.jsp?scess=false", function () {
                onRequestQuoteModalLoaded();
                eventsRequestQuote();
                initFileUpload();
            });
        })

        $('#orderPadModal .nav-tabs li').click(function () {
            updateOrderPadButtons($(this).children('a').attr('aria-controls'));
        });
        updateOrderPadButtons($('#orderPadModal .nav-tabs li.active a').attr('aria-controls'));

        var errorCodesSize = parseInt($("#errorCodesSize").val());
        if (errorCodesSize) {
            for (var i = 0; i < errorCodesSize; i++) {
                var errorCode = $("#exceptionErrorCode" + i).val();
                loadErrors(errorCode);
            }
        }
    });
}

function showViewOrderModalPopup(orderId,page) {
    $('#view_order_div').load("/global/modals/modal-view-order.jsp?orderId=" + orderId+"&page="+page, function () {
        eventsModalViewOrder();

        var orderId = $("#hiddenOrderId").val();

        $("#link-modalViewOrder" + orderId).click();
        jQuery('.scrollbar-inner').scrollbar();
        if ($(window).width() > 463) {
            $('#modalViewOrder' + orderId).on('shown.bs.modal', function () {
                var firstBlockHeight = $(".well-view-order-detail").innerHeight();
                var secondBlockHeight = $(".well-view-order-detail-summary").innerHeight();
                var maxHeight = Math.max(firstBlockHeight, secondBlockHeight);
                $(".well-view-order-detail").attr("style", "height: " + maxHeight + "px");
                $(".well-view-order-detail-summary").attr("style", "height: " + maxHeight + "px");
            })
        }

        $('#modalViewOrder' + orderId).on('shown.bs.modal', function (e) {

            if (isElementExist('.content')) {
                $.each($('.content'), function (index, val) {
                    $('.check img', val).each(function () {
                        if ($(this).height() > $(this).width()) {
                            $(this).css('height', 'auto');
                            $(this).css('width', 'auto');
                            $(this).css('max-height', '80px');
                        }
                    });
                });
            }
        });

        $('#order-tbl th').click(function () {
            sortOrderDetails(this);
        });
        $('#order-tbl tr').find('td:last').click();
    });
}

function agreeWithTOU(cookieName) {
    var cookieLifeCycleDays = 30;

    var date = new Date();
    date.setTime(date.getTime() + (cookieLifeCycleDays * 24 * 60 * 60 * 1000));
    var expires = "; expires=" + date.toUTCString();

    document.cookie = cookieName + "=" + expires + ";path=/";
}

function clearSelection() {
    if (window.getSelection) {
        if (window.getSelection().empty) {  // Chrome
            window.getSelection().empty();
        } else if (window.getSelection().removeAllRanges) {  // Firefox
            window.getSelection().removeAllRanges();
        }
    } else if (document.selection) {  // IE
        document.selection.empty();
    }
}

function showConfirmPopup(callback) {
    $("#cancelReOrder").modal("show");
    if(callback) {
        $('#cancelReOrder').one('hidden.bs.modal', function (e) {
            $("#cancelReOrder .modal-body .btn-primary").off("click.callback");
        });
        $("#cancelReOrder .modal-body .btn-primary").one( "click.callback", function(e) {
            $("#cancelReOrder").modal("hide");
            callback(e);
        });
    }
}

function disableHistoryPageContent() {
    $(".quick-add-control").attr("disabled", true);
    $('.preload-cover').addClass('loader-active');
    $('.preload-cover .preload-spinner').show();
}

function cancelAutoOrder(autoOrderId) {
    disableHistoryPageContent();
    $('#successCancelAutoOrder').on('hide.bs.modal', function () {
        window.location.reload();
    });
    $("#addAutoOrderID").val(autoOrderId);
    dataString = $('#cancel-auto-order').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: function() {
            $("#successCancelAutoOrder").modal("show");
        }
    });
}


//PDP error modal 
function handleSendPDPError() {
    //clearHighlighting();

    var url = window.location.href;
    if (url.indexOf(window.location.pathname) > -1) {
        url = url.substr(url.indexOf(window.location.pathname));
    }

    var dataString = $('#error-found-pdp-page-form').serialize();
    $.ajax({
        url: $('#error-found-pdp-page-form').attr('action'),
        type: "POST",
        data: dataString,
        dataType: "json",
        success: pdpErrorFoundSuccess,
        error: pdpErrorFoundFailure
    });
    return false;
}

function pdpErrorFoundSuccess(data) {
    if ('success' === data.code) {
        $('#errorFoundOnPDPModal').modal('hide');
    }
    else {
        var propertiesArray = [];
        var errorsArray = [];
        for (var i = 0; i < data.errors.length; i++) {
            errorsArray.push(data.errors[i]);
            propertiesArray.push(data.properties[i]);
        }
        for (var i = 0; i < propertiesArray.length; i++) {
            var errorMessageSmall = $("#pdp-err-message-" + propertiesArray[i]);
            errorMessageSmall.html(errorsArray[i]);
            errorMessageSmall.show();
            errorMessageSmall.parent().addClass("has-error");
        }
    }
}

function pdpErrorFoundFailure(x, s, e) {
    console.log("xhr: " + x + "; s: " + s + "; e: " + e);
}
