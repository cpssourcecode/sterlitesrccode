function eventsComparePanel(){
    clickEventHandler('catalog4', function(event){
        return handleGoToCompare();
    });

    clickEventHandler('catalog5', function(event){
        return handleClearCompare();
    });

    clickEventHandler('catalog6', function(event){
        return handleGoToCompare();
    });

    clickEventHandler('catalog7', function(event){
        return handleRemoveProductFromCompare($(event.target).data("eventClickParam0"))
    });
}
eventsComparePanel();

function eventsCompareContent(){
    clickEventHandler('catalog1', function(event){
        return handleRemoveProductAndGoToCompare($(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"));
    });

    clickEventHandler('catalog2', function(event){
        catalogAddToCart('addToCart',$(event.target).data("eventClickParam0"));
        return false;
    });
    keydownEventHandler('catalog3', function(event){
        return validateNumeric(event)
    });
}
eventsCompareContent();

function eventsResultList(){
    clickEventHandler('catalog0', function(event){
        if ('referrer' in document) {
            window.location = document.referrer;
        } else {
            window.history.back();
        }
    });

    clickEventHandler('catalog8', function(event){
        return false;
    });

    clickEventHandler('catalog9', function(event){
        return false;
    });

    clickEventHandler('catalog10', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        navigate(dataElem.data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog11', function(event){
        return false;
    });

    clickEventHandler('catalog12', function(event){
        navigate($(event.target).data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog13', function(event){
        return false;
    });

    clickEventHandler('catalog14', function(event){
        return false;
    });

    clickEventHandler('catalog15', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        navigate(dataElem.data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog16', function(event){
        return false;
    });

    clickEventHandler('catalog17', function(event){
        return false;
    });

    clickEventHandler('catalog18', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        navigate(dataElem.data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog19', function(event){
        navigate($(event.target).data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog20', function(event){
        return false;
    });

    clickEventHandler('catalog21', function(event){
        navigate($(event.target).data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog22', function(event){
        navigate($(event.target).data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog23', function(event){
        return false;
    });

    clickEventHandler('catalog24', function(event){
        return false;
    });

    clickEventHandler('catalog25', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        navigate(dataElem.data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog28', function(event){
        changeView('list')
    });

    clickEventHandler('catalog29', function(event){
        changeView('grid')
    });

    clickEventHandler('catalog30', function(event){
        clearFromCompare();
    });

    changeEventHandler('catalog31', function(event){
        navigate(this.options[this.selectedIndex].value,true);
    });

    changeEventHandler('catalog32', function(event){
        navigate(this.options[this.selectedIndex].value,true);
    });

    clickEventHandler('catalog66', function(event){
        compareItemList(this);
    });

    clickEventHandler('catalog67', function(event){
        catalogAddToCart('addToCart', $(event.target).data("eventClickParam0"));
        return false;
    });

    clickEventHandler('catalog68', function(event){
    	event.preventDefault();
    	$("body").append('<div class="custom-loading">Loading</div>');
        showCheckAvailabilityModal($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('catalog69', function(event){
        showAddToListModal($(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"));
    });

    keydownEventHandler('catalog70', function(event){
        return validateNumeric(event)
    });

    clickEventHandler('cartridges13', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        return navigate(dataElem.data("eventClickParam0"))
    });

    clickEventHandler('cartridges14', function(event){
        handleSecondarySearch();
    });

    clickEventHandler('cartridges16', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        return navigate(removeSecondarySearch(dataElem.data("eventClickParam0")))
    });

    clickEventHandler('cartridges17', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        return navigate(dataElem.data("eventClickParam0"))
    });

    clickEventHandler('cartridges18', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        navigate('?Ntt=' + dataElem.data("eventClickParam0"));
    });

    clickEventHandler('cartridges19', function(event){
    	var dataElem = $(event.target).closest('[data-event-click-id]');

        return navigate(removeSecondarySearch(dataElem.data("eventClickParam0")))
    });

    clickEventHandler('cartridges20', clearFacetClick);
    clickEventHandler('cartridges21', clearFacetClick);
	
	function clearFacetClick(event) {
		var dataElem = $(event.target).closest('[data-event-click-id]');
        var URL = dataElem.data("eventClickParam0");

        if (isLastOptionCleared(URL)) {
            URL = document.location.origin + "/all-brands";
            window.location = URL;
        } else {
            return navigate(URL);
        }
	}

    changeEventHandler('cartridges1', function(event){
        var dataElem = $(event.target).closest('[data-event-change-id]');

        navigate(dataElem.data("eventChangeParam0"));
    });

    changeEventHandler('cartridges9', function(event){
        var dataElem = $(event.target).closest('[data-event-change-id]');

        navigate(dataElem.data("eventChangeParam0"), false , dataElem);
    });

    clickEventHandler('cartridges7', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');
        var title = $(this).find('.seoRepositoryTitle').text();
        if(title === ""){
        	title =  ejectTitle(dataElem)
        }
        updateMeta(dataElem.data("eventClickParam1"), dataElem.data("eventClickParam2"), title);
        return navigate(dataElem.data("eventClickParam0"));
    });

    clickEventHandler('cartridges8', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        return navigate(dataElem.data("eventClickParam0"));
    });

    clickEventHandler('cartridges3', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        navigate(dataElem.data("eventClickParam0"));
        updateMeta(dataElem.data("eventClickParam1"), dataElem.data("eventClickParam2"), dataElem.context.innerText);
        return false;
    });

    eventsFallbackImage();
  //fix SM-531 PLP Grid view is not working 
    onResultListLoaded();
}
eventsResultList();

function eventsPdpTitleAssociated(){
    clickEventHandler('catalog54', function(event){
        return false;
    });

    clickEventHandler('catalog55', function(event){
        catalogAddToCart('addToCartHeader',$(event.target).data("eventClickParam0"), 'associated');
        return false;
    });

    clickEventHandler('catalog56', function(event){
        return false;
    });

    clickEventHandler('catalog57', function(event){
        reloadLessAssociated(this);
    });

    keydownEventHandler('catalog58', function(event){
        return validateNumeric(event)
    });

    keydownEventHandler('catalog59', function(event){
        return validateNumeric(event)
    });
}
eventsPdpTitleAssociated();

function eventsPdpTitleAlsoBought(){
    clickEventHandler('catalog48', function(event){
        return false;
    });

    clickEventHandler('catalog49', function(event){
        catalogAddToCart('addToCartHeader',$(event.target).data("eventClickParam0"), 'also-bought');
        return false;
    });

    clickEventHandler('catalog50', function(event){
        return false;
    });

    clickEventHandler('catalog51', function(event){
        reloadLessAlsoBought(this);
    });

    keydownEventHandler('catalog52', function(event){
        return validateNumeric(event)
    });

    keydownEventHandler('catalog53', function(event){
        return validateNumeric(event)
    });
}
eventsPdpTitleAlsoBought();

clickEventHandler('catalog26', function(event){
	navigate($(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('catalog27', function(event){
	navigate($(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('catalog33', function(event){
	return false;
});

clickEventHandler('catalog34', function(event){
	catalogAddToCart('addToCartHeader',$(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"));
	return false;
});

clickEventHandler('catalog35', function(event){
	return false;
});

clickEventHandler('catalog36', function(event){
	reloadAssociated(); 
});

clickEventHandler('catalog37', function(event){
	reloadAlsoBought(); 
});

clickEventHandler('catalog40', function(event){
	loadTumbnailImage('imgFirst');
});

clickEventHandler('catalog41', function(event){
	loadTumbnailImage('img' + $(event.target).data("eventClickParam0"));
});

clickEventHandler('catalog42', function(event){
	loadTumbnailImage($(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"));
	selectPhoto('photo0');
});

clickEventHandler('catalog43', function(event){
	loadTumbnailImage($(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"));
	selectPhoto('photo' + $(event.target).data("eventClickParam2"));
});

clickEventHandler('catalog44', function(event){
	catalogAddToCart('addToCart',$(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('catalog45', function(event){
	catalogAddToCart('addToCart',$(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('catalog46', function(event){
	catalogAddToCart('addToCart',$(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('catalog60', function(event){
	catalogAddToCart('addToCart',$(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('catalog61', function(event){
	event.preventDefault();
	$("body").append('<div class="custom-loading">Loading</div>');
	showCheckAvailabilityModal($(event.target).data("eventClickParam0"));
});

clickEventHandler('catalog62', function(event){
	pdp_addToMaterialList($(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"), $(event.target).data("eventClickParam2"))
});

clickEventHandler('catalog63', function(event){
	setPressed('addToML');
});

clickEventHandler('catalog72', function(event){
    quickOrderPadUploadFile();
    return false;
});

keydownEventHandler('catalog38', function(event){
    return validateNumeric(event)
});

keydownEventHandler('catalog39', function(event){
    return validateNumeric(event)
});

keydownEventHandler('catalog47', function(event){
    return validateNumeric(event)
});

keydownEventHandler('catalog64', function(event){
    return validateNumeric(event)
});

keydownEventHandler('catalog65', function(event){
    return validateNumeric(event)
});

function eventProductComparison(){
    clickEventHandler('catalog71', function(event){
        compareProducts();
    });
}