
function eventsCaseStudiesPagination(){
    clickEventHandler('content14', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content15', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content16', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content17', function(event){
        changePage(1);
    });

    clickEventHandler('content18', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content19', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content20', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content21', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content22', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content23', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content24', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content25', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content26', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content27', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content28', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });
}
eventsCaseStudiesPagination();

function eventsContentPagesPagination(){
    clickEventHandler('content29', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content30', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content31', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content32', function(event){
        changePage(1);
    });

    clickEventHandler('content33', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content34', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content35', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content36', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content37', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content38', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content39', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content40', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content41', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content42', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('content43', function(event){
        changePage($(event.target).data("eventClickParam0"));
    });
}
eventsContentPagesPagination();

clickEventHandler('content0', function(event){
	handleLogout()
});

clickEventHandler('content1', function(event){
	handleLogout()
});

clickEventHandler('content2', function(event){
	handleLogout()
});

clickEventHandler('content3', function(event){
	checkSearchTermTyped();
	return false;
});

clickEventHandler('content4', function(event){
	location.href = '/all-brands'
});

clickEventHandler('content5', function(event){
	location.href = '/all-brands'
});

clickEventHandler('content6', function(event){
	catalogAddToCart('addToCartHeader',$(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('content7', function(event){
	pdp_addToMaterialList($(event.currentTarget).data("eventClickParam0"), $(event.currentTarget).data("eventClickParam1"), $(event.currentTarget).data("eventClickParam2"))
});

clickEventHandler('content11', function(event){
	priceListUpdatesSubmit();
	return false;
});

clickEventHandler('content12', function(event){
	testimonialSubmit('secondary');
});

clickEventHandler('content13', function(event){
	testimonialSubmit('main');
});

clickEventHandler('content44', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content45', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content46', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content47', function(event){
	changePage(1);
});

clickEventHandler('content48', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content49', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content50', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content51', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content52', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content53', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content54', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content55', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content56', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content57', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

clickEventHandler('content58', function(event){
	changePage($(event.target).data("eventClickParam0"));
});

keydownEventHandler('content9', function(event){
    return validateNumeric(event)
});

keydownEventHandler('content10', function(event){
    return validateNumeric(event)
});

keypressEventHandler('content8', function(event){
    formSubmitOnEnter(event, 'checkSearchTermTyped');
});
