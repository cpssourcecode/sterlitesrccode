function eventsCartTableRow(){
    clickEventHandler('checkout32', function(event){
        scheduledDaysChange(this, $(event.target).data("eventClickParam0"))
    });

    clickEventHandler('checkout33', function(event){
        scheduledDaysChange(this, $(event.target).data("eventClickParam0"))
    });
    
    var qtyFlag = true;
    $('.qty-input').bind({
  	focusin:function(){
    	qtyFlag = true;
        focus = false;
    },
    keyup: function(event) {
    	var elem = $(this);
    	qtyFlag = true;
        focus = true;
        clearTimeout(window.timer);
        window.timer = setTimeout(function () {
          if(qtyFlag){
             qtyFlag = false;
             var qty = elem.val();
		     var cid = elem.attr('data-cid');
		     var skuId = $(event.target).data("eventChangeParam0");
		     var oldQty= $(event.target).data("eventChangeParam1")
		     onQtyUpdateClick(elem, 'cart', skuId, oldQty, $(event.target).data("eventClickParam2"));
		     $(event.target).blur();
          }    	
        }, 3000);
    },
    focusout: function(event) {
    	if(qtyFlag && focus) {
    		qtyFlag = false;
    		var elem = $(this)
    		var qty = elem.val();
    		var cid = elem.attr('data-cid');
    		var skuId = $(event.target).data("eventChangeParam0");
    		var oldQty= $(event.target).data("eventChangeParam1")
    		onQtyUpdateClick(elem, 'cart', skuId, oldQty, $(event.target).data("eventClickParam2"));      	
    	}    	
    	}
    });
    
    
}

eventsCartTableRow();

function eventsCartDeliveryMethod(){
    if (typeof setDeliveryMethodRadioButtonsActivated === "function") {
        setDeliveryMethodRadioButtonsActivated();
    }

    clickEventHandler('checkout21', function(event){
        onSelectShippingOnCart();
    });

    clickEventHandler('checkout22', function(event){
        onSelectPickupOnCart();
    });
    
}
eventsCartDeliveryMethod();
clickEventHandler('eventExportCart', function(event){
    exportCart();
});
clickEventHandler('checkout0', function(event){
	onCheckAvailabilityClick();
	return false;
});

clickEventHandler('checkout1', function(event){
	onAddToMaterialLClick();
	return false;
});

clickEventHandler('checkout2', function(event){
	onRemoveSelectedClick();
	return false;
});

clickEventHandler('checkout4', function(event){
	changeCartSorting('sort-option-description','cart');
	return false
});

clickEventHandler('checkout5', function(event){
	changeCartSorting('sort-option-item-number','cart');
	return false
});

clickEventHandler('checkout6', function(event){
	changeCartSorting('sort-option-alias-number','cart');
	return false
});

clickEventHandler('checkout7', function(event){
	changeCartSorting('sort-option-price','cart');
	return false
});

clickEventHandler('checkout8', function(event){
	onProceedToCheckoutClick($(event.target).data('isTransient'));
	return false;
});

clickEventHandler('checkout9', function(event){
	showViewOrderModalPopup($(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('checkout10', function(event){
	refreshCaptcha();
});

clickEventHandler('checkout11', function(event){
	paAddselected()
});

clickEventHandler('checkout12', function(event){
	needToLoginList()
});

clickEventHandler('checkout13', function(event){
	removeSelected()
});

clickEventHandler('checkout14', function(event){
	submitWithAddAction()
});

clickEventHandler('checkout15', function(event){
	toggleCheckBoxes(this)
});

clickEventHandler('checkout16', function(event){
	paAddAll()
});

clickEventHandler('checkout17', function(event){
	moveToReview();
	return false;
});

clickEventHandler('checkout18', function(event){
	onPlaceOrderClick();
	return false;
});

clickEventHandler('checkout19', function(event){
	changeCartSorting('sort-option-item-number', 'review');
});

clickEventHandler('checkout20', function(event){
	onPlaceOrderClick();
	return false;
});

clickEventHandler('checkout25', function(event){
	submitWithUpdateAction($(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('checkout26', function(event){
	submitWithRemoveItemAction($(event.target).data("eventClickParam0"));
	return false;
});

clickEventHandler('checkout27', function(event){
	doModalCS_Search()
});

clickEventHandler('checkout28', function(event){
	doModalCS_Reset()
});

clickEventHandler('checkout29', function(event){
	cartOnConfirmChangeCSBtnClick()
});

clickEventHandler('checkout31', function(event){
	// fix SM-398 Quick order pad - Issue in Add to Cart functionality
	$(".addToCartButton").attr("disabled", true);
	onQuickAddIClick();
	return false;
});

clickEventHandler('checkout35', function(event){
	onPlaceOrderClick();
	return false;
});

clickEventHandler('checkout36', function(event){
	onPlaceOrderClick();
	return false;
});
