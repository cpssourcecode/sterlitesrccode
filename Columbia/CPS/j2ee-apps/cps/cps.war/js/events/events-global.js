
function eventsModalMultiContent(){
    clickEventHandler('global213', function(event){
        location.href = '/account/material-detail.jsp?giftlistId=' + $(event.target).data("eventClickParam0")
    });
}
eventsModalMultiContent();

function eventsAddShipToList(){
    changeEventHandler('global246', function(event){
        selectAll(this);
    });
}
eventsAddShipToList();

function eventsModalViewOrder(){
    clickEventHandler('global237', function(event){
        getAvailability($(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"), this)
    });
}
eventsModalViewOrder();

function eventsModalApproveOrdersSuccessDiv(){
    clickEventHandler('global183', function(event){
        approveOrders();
    });
}
eventsModalApproveOrdersSuccessDiv();

function eventsModalRejectOrdersSuccessDiv(){
    clickEventHandler('global223', function(event){
        rejectOrders();
    });
}
eventsModalRejectOrdersSuccessDiv();

/*function eventsModalAddToMaterialList(){
    clickEventHandler('global181', function(event){
        materialList_addItemsToList();
        return false;
    });

    clickEventHandler('global182', function(event){
        materialList_addItemToList($(event.target).data("eventClickParam0"));
    });
}
eventsModalAddToMaterialList();*/

function eventsModalCS(){
    clickEventHandler('global195', function(event){
        doModalCS_CheckShippingAddress(this)
    });

    clickEventHandler('global196', function(event){
        doModalCS_Search($(event.target).data("eventClickParam0"))
    });

    clickEventHandler('global197', function(event){
        doModalCS_Reset($(event.target).data("eventClickParam0"))
    });

    changeEventHandler('global198', function(event){
        selectBilLTo(this.value, $(event.target).data("eventChangeParam0"))
    });

    clickEventHandler('checkout30', function(event){
        cartOnShippingAddressRadioSelect(this)
    });

    clickEventHandler('global254', function(event){
        doModalCS_CheckShippingAddress(this)
    });
}
eventsModalCS();

function eventsModalCSCart(){
    clickEventHandler('global192', function(event){
        doModalCS_CheckShippingAddress_Cart(this)
    });

    clickEventHandler('global193', function(event){
        doModalCS_Cart_Search();
        return false;
    });

    clickEventHandler('global194', function(event){
        doModalCS_Cart_Reset();
        return false;
    });

    clickEventHandler('global253', function(event){
        doModalCS_CheckShippingAddress_Cart(this)
    });
}
eventsModalCSCart();

function eventsModalPickupLoc(){
    clickEventHandler('global220', function(event){
        doModalCS_CheckShippingAddress(this)
    });

    clickEventHandler('global221', function(event){
        doModalPickup_Search();
        return false;
    });

    clickEventHandler('global222', function(event){
        doModalPickup_Reset();
        return false;
    });
    clickEventHandler('global255', function(event){
        doModalCS_CheckPickupAddress(this)
    });
}
eventsModalPickupLoc();

function eventsProfileContent(){
    clickEventHandler('global106', function(event){
        return addNextCustomer();
    });

    clickEventHandler('global107', function(event){
        return removeLastCustomer();
    });

    clickEventHandler('global108', function(event){
        creditApplicationReturnStep1()
    });

    clickEventHandler('global109', function(event){
        creditApplicationProfileSubmit()
    });
    keydownEventHandler('global110', function(event){
        limitText(this.form.ca_input_annual_purchases,this.form.countdown,10);
    });

    keydownEventHandler('global111', function(event){
        limitText(this.form.ca_input_profile_customer_name_1,this.form.countdown,100);
    });

    keydownEventHandler('global112', function(event){
        limitText(this.form.ca_input_profile_customer_title_1,this.form.countdown,100);
    });

    keydownEventHandler('global113', function(event){
        limitText(this.form.ca_input_profile_customer_phone_1,this.form.countdown,15);
    });

    keydownEventHandler('global114', function(event){
        limitText(this.form.ca_input_profile_customer_email_1,this.form.countdown,100);
    });

    keydownEventHandler('global115', function(event){
        limitText(this.form.ca_input_credit_request,this.form.countdown,100);
    });
    keyupEventHandler('global116', function(event){
        limitText(this.form.ca_input_annual_purchases,this.form.countdown,10);
    });

    keyupEventHandler('global117', function(event){
        limitText(this.form.ca_input_profile_customer_name_1,this.form.countdown,100);
    });

    keyupEventHandler('global118', function(event){
        limitText(this.form.ca_input_profile_customer_title_1,this.form.countdown,100);
    });

    keyupEventHandler('global119', function(event){
        limitText(this.form.ca_input_profile_customer_phone_1,this.form.countdown,15);
    });

    keyupEventHandler('global120', function(event){
        limitText(this.form.ca_input_profile_customer_email_1,this.form.countdown,100);
    });

    keyupEventHandler('global121', function(event){
        limitText(this.form.ca_input_credit_request,this.form.countdown,100);
    });
}
eventsProfileContent();

function eventsTaxContent(){
    clickEventHandler('global144', function(event){
    	creditApplicationExemptStatus(this)
    });

    clickEventHandler('global145', function(event){
    	creditApplicationExemptStatus(this)
    });

    clickEventHandler('global146', function(event){
        return addRegisteredState();
    });

    clickEventHandler('global147', function(event){
        return removeRegisteredState();
    });

    clickEventHandler('global148', function(event){
        creditApplicationReturnStep2()
    });

    clickEventHandler('global149', function(event){
        creditApplicationTaxSubmit()
    });
    keydownEventHandler('global150', function(event){
        limitText(this.form.ca_input_tax_firm_name,this.form.countdown,100);
    });

    keydownEventHandler('global151', function(event){
        limitText(this.form.ca_input_tax_address,this.form.countdown,100);
    });

    keydownEventHandler('global152', function(event){
        limitText(this.form.ca_input_tax_city,this.form.countdown,100);
    });

    keydownEventHandler('global153', function(event){
        limitText(this.form.ca_input_tax_registered_id_1,this.form.countdown,100);
    });

    keydownEventHandler('global154', function(event){
        limitText(this.form.ca_input_tax_action,this.form.countdown,100);
    });

    keydownEventHandler('global155', function(event){
        limitText(this.form.ca_input_tax_product_description,this.form.countdown,100);
    });

    keyupEventHandler('global156', function(event){
        limitText(this.form.ca_input_tax_firm_name,this.form.countdown,100);
    });

    keyupEventHandler('global157', function(event){
        limitText(this.form.ca_input_tax_address,this.form.countdown,100);
    });

    keyupEventHandler('global158', function(event){
        limitText(this.form.ca_input_tax_city,this.form.countdown,100);
    });

    keyupEventHandler('global159', function(event){
        limitText(this.form.ca_input_tax_registered_id_1,this.form.countdown,100);
    });

    keyupEventHandler('global160', function(event){
        limitText(this.form.ca_input_tax_action,this.form.countdown,100);
    });

    keyupEventHandler('global161', function(event){
        limitText(this.form.ca_input_tax_product_description,this.form.countdown,100);
    });

    changeEventHandler('global162', function(event){
        creditApplicationChangeEngagedRegistered(this.value)
    });

    changeEventHandler('global163', function(event){
        creditApplicationChangePurchase(this.value)
    });

    inputEventHandler('global164', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });
}
eventsTaxContent();

function eventsResaleContent(){
    clickEventHandler('global122', function(event){
        creditApplicationResaleChangePurchaserRegistered(this)
    });

    clickEventHandler('global123', function(event){
        creditApplicationResaleChangePurchaserRegistered(this)
    });

    clickEventHandler('global124', function(event){
        creditApplicationResaleChangePurchaserRegistered(this)
    });

    clickEventHandler('global125', function(event){
        creditApplicationResaleChangePercentage(this)
    });

    clickEventHandler('global126', function(event){
        creditApplicationResaleChangePercentage(this)
    });

    clickEventHandler('global127', function(event){
        creditApplicationReturnStep3()
    });

    clickEventHandler('global128', function(event){
        creditApplicationResaleSubmit('save');
        return false;
    });

    clickEventHandler('global129', function(event){
        creditApplicationResaleSubmit('sign');
        return false;
    });

    keydownEventHandler('global130', function(event){
        limitText(this.form.ca_input_certificate_name_step_1,this.form.countdown,100);
    });

    keydownEventHandler('global131', function(event){
        limitText(this.form.ca_input_certificate_address_step_1,this.form.countdown,100);
    });

    keydownEventHandler('global132', function(event){
        limitText(this.form.ca_input_certificate_city_step_1,this.form.countdown,100);
    });

    keydownEventHandler('global133', function(event){
        limitText(this.form.ca_input_certificate_name_step_2,this.form.countdown,100);
    });

    keydownEventHandler('global134', function(event){
        limitText(this.form.ca_input_certificate_address_step_2,this.form.countdown,100);
    });

    keydownEventHandler('global135', function(event){
        limitText(this.form.ca_input_certificate_city_step_2,this.form.countdown,100);
    });

    keyupEventHandler('global136', function(event){
        limitText(this.form.ca_input_certificate_name_step_1,this.form.countdown,100);
    });

    keyupEventHandler('global137', function(event){
        limitText(this.form.ca_input_certificate_address_step_1,this.form.countdown,100);
    });

    keyupEventHandler('global138', function(event){
        limitText(this.form.ca_input_certificate_city_step_1,this.form.countdown,100);
    });

    keyupEventHandler('global139', function(event){
        limitText(this.form.ca_input_certificate_name_step_2,this.form.countdown,100);
    });

    keyupEventHandler('global140', function(event){
        limitText(this.form.ca_input_certificate_address_step_2,this.form.countdown,100);
    });

    keyupEventHandler('global141', function(event){
        limitText(this.form.ca_input_certificate_city_step_2,this.form.countdown,100);
    });

    inputEventHandler('global142', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });

    inputEventHandler('global143', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });
}
eventsResaleContent();

function eventsApplicationContent(){
    clickEventHandler('global19', function(event){
        creditApplicationChangePO(this)
    });

    clickEventHandler('global20', function(event){
        creditApplicationChangePO(this)
    });

    clickEventHandler('global21', function(event){
        creditApplicationChangeTaxExempt(this)
    });

    clickEventHandler('global22', function(event){
        creditApplicationChangeTaxExempt(this)
    });

    clickEventHandler('global23', function(event){
        return addReference();
    });

    clickEventHandler('global24', function(event){
        return removeReference();
    });

    clickEventHandler('global25', function(event){
        creditApplicationCancel()
    });

    clickEventHandler('global26', function(event){
        creditApplicationSubmit()
    });
    keydownEventHandler('global27', function(event){
        limitText(this.form.ca_input_business_name,this.form.countdown,100);
    });

    keydownEventHandler('global28', function(event){
        limitText(this.form.ca_input_trade_name,this.form.countdown,100);
    });

    keydownEventHandler('global29', function(event){
        limitText(this.form.ca_input_phone,this.form.countdown,15);
    });

    keydownEventHandler('global30', function(event){
        limitText(this.form.ca_input_fax,this.form.countdown,15);
    });

    keydownEventHandler('global31', function(event){
        limitText(this.form.ca_input_business_type,this.form.countdown,100);
    });

    keydownEventHandler('global32', function(event){
        limitText(this.form.ca_input_business_license,this.form.countdown,100);
    });

    keydownEventHandler('global33', function(event){
        limitText(this.form.ca_input_business_street,this.form.countdown,100);
    });

    keydownEventHandler('global34', function(event){
        limitText(this.form.ca_input_business_city,this.form.countdown,100);
    });

    keydownEventHandler('global35', function(event){
        limitText(this.form.ca_input_billing_street,this.form.countdown,100);
    });

    keydownEventHandler('global36', function(event){
        limitText(this.form.ca_input_billing_city,this.form.countdown,100);
    });

    keydownEventHandler('global37', function(event){
        limitText(this.form.ca_input_principal_name_1,this.form.countdown,100);
    });

    keydownEventHandler('global38', function(event){
        limitText(this.form.ca_input_principal_title_1,this.form.countdown,100);
    });

    keydownEventHandler('global39', function(event){
        limitText(this.form.ca_input_principal_social_1,this.form.countdown,100);
    });

    keydownEventHandler('global40', function(event){
        limitText(this.form.ca_input_principal_email_1,this.form.countdown,50);
    });

    keydownEventHandler('global41', function(event){
        limitText(this.form.ca_input_principal_phone_1,this.form.countdown,15);
    });

    keydownEventHandler('global42', function(event){
        limitText(this.form.ca_input_principal_address_1,this.form.countdown,100);
    });

    keydownEventHandler('global43', function(event){
        limitText(this.form.ca_input_principal_city_1,this.form.countdown,100);
    });

    keydownEventHandler('global44', function(event){
        limitText(this.form.ca_input_principal_name_2,this.form.countdown,100);
    });

    keydownEventHandler('global45', function(event){
        limitText(this.form.ca_input_principal_title_2,this.form.countdown,100);
    });

    keydownEventHandler('global46', function(event){
        limitText(this.form.ca_input_principal_social_2,this.form.countdown,100);
    });

    keydownEventHandler('global47', function(event){
        limitText(this.form.ca_input_principal_email_2,this.form.countdown,50);
    });

    keydownEventHandler('global48', function(event){
        limitText(this.form.ca_input_principal_phone_2,this.form.countdown,15);
    });

    keydownEventHandler('global49', function(event){
        limitText(this.form.ca_input_principal_address_2,this.form.countdown,100);
    });

    keydownEventHandler('global50', function(event){
        limitText(this.form.ca_input_principal_city_2,this.form.countdown,100);
    });

    keydownEventHandler('global51', function(event){
        limitText(this.form.ca_input_business_trade_name_1,this.form.countdown,100);
    });

    keydownEventHandler('global52', function(event){
        limitText(this.form.ca_input_business_trade_address_1,this.form.countdown,100);
    });

    keydownEventHandler('global53', function(event){
        limitText(this.form.ca_input_business_trade_city_1,this.form.countdown,100);
    });

    keydownEventHandler('global54', function(event){
        limitText(this.form.ca_input_business_trade_phone_1,this.form.countdown,15);
    });

    keydownEventHandler('global55', function(event){
        limitText(this.form.ca_input_business_trade_email_1,this.form.countdown,50);
    });

    keydownEventHandler('global56', function(event){
        limitText(this.form.ca_input_bank_reference_name,this.form.countdown,100);
    });

    keydownEventHandler('global57', function(event){
        limitText(this.form.ca_input_bank_reference_acct,this.form.countdown,100);
    });

    keydownEventHandler('global58', function(event){
        limitText(this.form.ca_input_bank_reference_officer,this.form.countdown,100);
    });

    keydownEventHandler('global59', function(event){
        limitText(this.form.ca_input_bank_reference_address,this.form.countdown,100);
    });

    keydownEventHandler('global60', function(event){
        limitText(this.form.ca_input_bank_reference_city,this.form.countdown,100);
    });

    keydownEventHandler('global61', function(event){
        limitText(this.form.ca_input_bank_reference_phone,this.form.countdown,15);
    });

    keydownEventHandler('global62', function(event){
        limitText(this.form.ca_input_bank_reference_email,this.form.countdown,50);
    });
    keyupEventHandler('global63', function(event){
        limitText(this.form.ca_input_business_name,this.form.countdown,100);
    });

    keyupEventHandler('global64', function(event){
        limitText(this.form.ca_input_trade_name,this.form.countdown,100);
    });

    keyupEventHandler('global65', function(event){
        limitText(this.form.ca_input_phone,this.form.countdown,15);
    });

    keyupEventHandler('global66', function(event){
        limitText(this.form.ca_input_fax,this.form.countdown,15);
    });

    keyupEventHandler('global67', function(event){
        limitText(this.form.ca_input_business_type,this.form.countdown,100);
    });

    keyupEventHandler('global68', function(event){
        limitText(this.form.ca_input_business_license,this.form.countdown,100);
    });

    keyupEventHandler('global69', function(event){
        limitText(this.form.ca_input_business_street,this.form.countdown,100);
    });

    keyupEventHandler('global70', function(event){
        limitText(this.form.ca_input_business_city,this.form.countdown,100);
    });

    keyupEventHandler('global71', function(event){
        limitText(this.form.ca_input_billing_street,this.form.countdown,100);
    });

    keyupEventHandler('global72', function(event){
        limitText(this.form.ca_input_billing_city,this.form.countdown,100);
    });

    keyupEventHandler('global73', function(event){
        limitText(this.form.ca_input_principal_name_1,this.form.countdown,100);
    });

    keyupEventHandler('global74', function(event){
        limitText(this.form.ca_input_principal_title_1,this.form.countdown,100);
    });

    keyupEventHandler('global75', function(event){
        limitText(this.form.ca_input_principal_social_1,this.form.countdown,100);
    });

    keyupEventHandler('global76', function(event){
        limitText(this.form.ca_input_principal_email_1,this.form.countdown,50);
    });

    keyupEventHandler('global77', function(event){
        limitText(this.form.ca_input_principal_phone_1,this.form.countdown,15);
    });

    keyupEventHandler('global78', function(event){
        limitText(this.form.ca_input_principal_address_1,this.form.countdown,100);
    });

    keyupEventHandler('global79', function(event){
        limitText(this.form.ca_input_principal_city_1,this.form.countdown,100);
    });

    keyupEventHandler('global80', function(event){
        limitText(this.form.ca_input_principal_name_2,this.form.countdown,100);
    });

    keyupEventHandler('global81', function(event){
        limitText(this.form.ca_input_principal_title_2,this.form.countdown,100);
    });

    keyupEventHandler('global82', function(event){
        limitText(this.form.ca_input_principal_social_2,this.form.countdown,100);
    });

    keyupEventHandler('global83', function(event){
        limitText(this.form.ca_input_principal_email_2,this.form.countdown,50);
    });

    keyupEventHandler('global84', function(event){
        limitText(this.form.ca_input_principal_phone_2,this.form.countdown,15);
    });

    keyupEventHandler('global85', function(event){
        limitText(this.form.ca_input_principal_address_2,this.form.countdown,100);
    });

    keyupEventHandler('global86', function(event){
        limitText(this.form.ca_input_principal_city_2,this.form.countdown,100);
    });

    keyupEventHandler('global87', function(event){
        limitText(this.form.ca_input_business_trade_name_1,this.form.countdown,100);
    });

    keyupEventHandler('global88', function(event){
        limitText(this.form.ca_input_business_trade_address_1,this.form.countdown,100);
    });

    keyupEventHandler('global89', function(event){
        limitText(this.form.ca_input_business_trade_city_1,this.form.countdown,100);
    });

    keyupEventHandler('global90', function(event){
        limitText(this.form.ca_input_business_trade_phone_1,this.form.countdown,15);
    });

    keyupEventHandler('global91', function(event){
        limitText(this.form.ca_input_business_trade_email_1,this.form.countdown,50);
    });

    keyupEventHandler('global92', function(event){
        limitText(this.form.ca_input_bank_reference_name,this.form.countdown,100);
    });

    keyupEventHandler('global93', function(event){
        limitText(this.form.ca_input_bank_reference_acct,this.form.countdown,100);
    });

    keyupEventHandler('global94', function(event){
        limitText(this.form.ca_input_bank_reference_officer,this.form.countdown,100);
    });

    keyupEventHandler('global95', function(event){
        limitText(this.form.ca_input_bank_reference_address,this.form.countdown,100);
    });

    keyupEventHandler('global96', function(event){
        limitText(this.form.ca_input_bank_reference_city,this.form.countdown,100);
    });

    keyupEventHandler('global97', function(event){
        limitText(this.form.ca_input_bank_reference_phone,this.form.countdown,15);
    });

    keyupEventHandler('global98', function(event){
        limitText(this.form.ca_input_bank_reference_email,this.form.countdown,50);
    });
    inputEventHandler('global99', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });

    inputEventHandler('global100', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });

    inputEventHandler('global101', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });

    inputEventHandler('global102', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });

    inputEventHandler('global103', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });

    inputEventHandler('global104', function(event){
        this.value=this.value.replace(/[^0-9]/g,'');
    });
}
eventsApplicationContent();

function eventsModalCSCartList(){
    clickEventHandler('global253', function(event){
        doModalCS_CheckShippingAddress_Cart(this)
    });
}

function eventsModalCSList(){
    clickEventHandler('global254', function(event){
        doModalCS_CheckShippingAddress(this)
    });
}

function eventsModalPickupList(){
    clickEventHandler('global255', function(event){
        doModalCS_CheckPickupAddress(this)
    });
}

function eventsHeaderCsSelection(){
    clickEventHandler('includes1', function(event){
        doModalCS_CheckShippingAddress_Header(this)
    });

    clickEventHandler('includes2', function(event){
        openCSModal('true');
    });
}
eventsHeaderCsSelection();

function eventsRequestQuote(){
    clickEventHandler('global245', function(event){
        submitRequestAQuote();
    });
}
eventsRequestQuote();

function eventsModalNewMaterialList(){
    clickEventHandler('global214', function(event){
        createNewMaterialList();
        return false;
    });

    uploadMaterialListFile();
}
eventsModalNewMaterialList();

function eventsTypeahead(){
    clickEventHandler('includes3', function(event){
        typeAheadAddToCart(this);
        return false;
    });
}
eventsTypeahead();

function eventsModalCheckAvailabilityMainControls(){
    clickEventHandler('global185', function(event){
        checkAvailability($(event.target).data('productId'));
    });

    changeEventHandler('global188', function(event){
        removeAreaExpanded(this);
    });

    changeEventHandler('global189', function(event){
        removeAreaExpanded(this);
    });
}
eventsModalCheckAvailabilityMainControls();

function eventsModalCheckAvailabilitAddtnlControls(){
    clickEventHandler('global186', function(event){
        addToCartFromModal($(event.target).data("eventClickParam0"),'addToCartFromModalQty','haveItShipped');
        return false;
    });
    keydownEventHandler('global187', function(event){
        return validateNumeric(event)
    });
    
    clickEventHandler('global250', function(event){
        addToCartFromModal($(event.target).data("eventClickParam0"),'addToCartFromModalQty-' + $(event.target).data("eventClickParam1"), 'pickItUp', $(event.target).data("eventClickParam2"));
        return false;
    });

    clickEventHandler('global247', function(event){
        addToCartFromModal($(event.target).data("eventClickParam0"),'addToCartFromModalQty-' + $(event.target).data("eventClickParam1"),'pickItUp', $(event.target).data("eventClickParam2"));
        return false;
    });
}
eventsModalCheckAvailabilitAddtnlControls();

clickEventHandler('global0', function(event){
	contactUs_methodChange(this)
});

clickEventHandler('global1', function(event){
	contactUs_methodChange(this)
});

clickEventHandler('global2', function(event){
	contactUs_Submit()
});

clickEventHandler('global3', function(event){
	contactUs_Cancel()
});

clickEventHandler('global16', function(event){
	checkUnsubs();
});

clickEventHandler('global17', function(event){
	switchSubmit(this);
});

clickEventHandler('global18', function(event){
	doSignUp();
});

clickEventHandler('global105', function(event){
	creditApplicationReturnStep1()
});

clickEventHandler('global165', function(event){
	// fix SM-398 Quick order pad - Issue in Add to Cart functionality
    $('#quickOrderAddToPad').attr("disabled", true);
	quickOrderAddToCart('#quickAddToCartDropdown', 'Pad');
});

clickEventHandler('global172', function(event){
	window.print();
});

clickEventHandler('global175', function(event){
	accessAccount();
});

clickEventHandler('global176', function(event){
	hideErrorMessage('select-address-error')
});

clickEventHandler('global177', function(event){
	searchAddresses();
});

clickEventHandler('global178', function(event){
	resetSearch();
});

clickEventHandler('global179', function(event){
	addShipAddresses();
});

clickEventHandler('global180', function(event){
	addItemsToLists();
});



clickEventHandler('global190', function(event){
	location.href = '/';
});

clickEventHandler('global191', function(event){
	creditApplicationSignSubmit()
});

clickEventHandler('global201', function(event){
	resetReorderListSearch();
});

clickEventHandler('global203', function(event){
	mapDirections();
});

clickEventHandler('global204', function(event){
	editStartingAddress()
});

clickEventHandler('global205', function(event){
	printDirections();
});

clickEventHandler('global206', function(event){
	editMaterialList($(event.target).data("eventClickParam0"));
});

clickEventHandler('global209', function(event){
	duplicateAddToMaterialList();
});

clickEventHandler('global210', function(event){
	sendEmails();
});

clickEventHandler('global211', function(event){
	handleLogin($(event.target).data('suffixId'));
});

clickEventHandler('global215', function(event){
	handleNewUserLogin();
});

clickEventHandler('global217', function(event){
	handleForgotPasswordStep1()
});

clickEventHandler('global218', function(event){
	rememberMeClick(this)
});

clickEventHandler('global219', function(event){
	handleResetPassword();
});


clickEventHandler('global228', function(event){
	handleSharePage();
});

clickEventHandler('global229', function(event){
	resetPage($(event.target).data("eventClickParam0"))
});

clickEventHandler('global230', function(event){
	clearTestimonialFields();
});

clickEventHandler('global231', function(event){
	changeViewAllModalPage($(event.target).data("eventClickParam0"), $(event.target).data("eventClickParam1"))
});

clickEventHandler('global232', function(event){
	saveMultiSelections();
});

clickEventHandler('global233', function(event){
	updateNavigationOnModalClose($(event.target).data("eventClickParam0"));
});

clickEventHandler('global238', function(event){
	quickOrderAddToCart('#quickOrderAddToCart', 'Page');
});

clickEventHandler('pdpFoundAnErrModalSubmit', function(event){
	handleSendPDPError();
});

changeEventHandler('global234', function(event){
    toggleRefinementOnModal($(event.target).data("eventChangeParam0"), 'remove-dimval-ids');
});

changeEventHandler('global235', function(event){
    toggleRefinementOnModal($(event.target).data("eventChangeParam0"), 'add-dimval-ids');
});

changeEventHandler('global236', function(event){
    selectSingleOnModal($(event.target).data("eventChangeParam0"),$(event.target).data("eventChangeParam1"));
});

changeEventHandler('global249', function(event){

});

changeEventHandler('global252', function(event){

});



keydownEventHandler('global4', function(event){
    limitText(this.form.cu_input_fname,this.form.countdown,100);
});

keydownEventHandler('global5', function(event){
    limitText(this.form.cu_input_lname,this.form.countdown,100);
});

keydownEventHandler('global6', function(event){
    limitText(this.form.cu_input_email,this.form.countdown,100);
});

keydownEventHandler('global7', function(event){
    limitText(this.form.cu_input_company,this.form.countdown,100);
});

keydownEventHandler('global8', function(event){
    limitText(this.form.cu_input_accountNumber,this.form.countdown,100);
});

keydownEventHandler('global9', function(event){
    limitText(this.form.cu_input_phone,this.form.countdown,15);
});

keydownEventHandler('global248', function(event){
    return validateNumeric(event)
});

keydownEventHandler('global251', function(event){
    return validateNumeric(event)
});

keypressEventHandler('global166', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global167', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global168', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global169', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global170', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global171', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global239', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global240', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global241', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global242', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global243', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('global244', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keyupEventHandler('global10', function(event){
    limitText(this.form.cu_input_fname,this.form.countdown,100);
});

keyupEventHandler('global11', function(event){
    limitText(this.form.cu_input_lname,this.form.countdown,100);
});

keyupEventHandler('global12', function(event){
    limitText(this.form.cu_input_email,this.form.countdown,100);
});

keyupEventHandler('global13', function(event){
    limitText(this.form.cu_input_company,this.form.countdown,100);
});

keyupEventHandler('global14', function(event){
    limitText(this.form.cu_input_accountNumber,this.form.countdown,100);
});

keyupEventHandler('global15', function(event){
    limitText(this.form.cu_input_phone,this.form.countdown,15);
});

clickEventHandler('includes0', function(event){
    stopAccess();
});


