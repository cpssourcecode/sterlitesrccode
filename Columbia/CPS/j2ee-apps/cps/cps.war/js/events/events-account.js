function eventsModalNoListsFound(){
    clickEventHandler('global216', function(event){
        resetPage($(event.target).data("eventClickParam0"));
    });
}

function eventsAddressListDisplay(){
    clickEventHandler('eventDoCS_CheckShippingAddress', function(event){
        doCS_CheckShippingAddress(this)
    });

    eventsModalNoListsFound();
}
eventsAddressListDisplay();

function eventsOrderApprovalDisplay(){
    clickEventHandler('eventSelectAll', function(event){
        selectAll(this);
    });
    clickEventHandler('eventSortProfileName', function(event){
        sortProfileName($(event.target).data("approvals"));
    });
    clickEventHandler('eventSortOrderDate', function(event){
        sortOrderDate($(event.target).data("approvals"));
    });
    clickEventHandler('eventShowViewOrderModalPopup', function(event){
        showViewOrderModalPopup($(event.target).data("orderId"),$(event.target).data("page"));
        return false;
    });

    clickEventHandler('eventShowApproveOrderModalPopup', function(event){
        showApproveOrderModalPopup($(event.target).data("orderId"));
        return false;
    });

    clickEventHandler('eventShowRejectOrderModalPopup', function(event){
        showRejectOrderModalPopup($(event.target).data("orderId"));
        return false;
    });
    clickEventHandler('global184', function(event){
        approveOrder($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('global224', function(event){
        rejectOrder($(event.target).data("eventClickParam0"));
    });
}
eventsOrderApprovalDisplay();

function eventsOrderRequestsDisplay(){
    clickEventHandler('eventSortOrderIdOrderList', function(event){
        sortOrderId();
    });
    clickEventHandler('eventSortOrderRequestor', function(event){
        sortOrderRequestor();
    });
    clickEventHandler('eventSortOrderDateOrderList', function(event){
        sortOrderDate();
    });
    clickEventHandler('eventShowViewOrderModalPopup', function(event){
        showViewOrderModalPopup($(event.target).data("orderId"));
        return false;
    });
    clickEventHandler('global225', function(event){
        resubmitOrder($(event.target).data("eventClickParam0"));
    });
    clickEventHandler('global207', function(event){
        editOrder($(event.target).data("eventClickParam0"));
    });
    clickEventHandler('global202', function(event){
        deleteOrder($(event.target).data("eventClickParam0"));
    });
}
eventsOrderRequestsDisplay();

function eventsOrderCartsDisplay(){
    clickEventHandler('eventSortOrderSavedDate', function(event){
        sortOrderSavedDate();
    });
    clickEventHandler('eventSortAddress', function(event){
        sortAddress();
    });
    clickEventHandler('eventShowMoveToCartModalPopup', function(event){
        showMoveToCartModalPopup($(event.target).data("orderId"));
        return false;
    });
    clickEventHandler('eventShowViewCartModalPopup', function(event){
        showViewCartModalPopup($(event.target).data("orderId"));
        return false;
    });
    clickEventHandler('eventShowDeleteCartModalPopup', function(event){
        showDeleteCartModalPopup($(event.target).data("orderId"));
        return false;
    });
    clickEventHandler('global212', function(event){
        editOrder($(event.target).data("eventClickParam0"));
    });
    clickEventHandler('global199', function(event){
        deleteOrder($(event.target).data("eventClickParam0"));
    });
}
eventsOrderCartsDisplay();

function eventsSavedCartPagination(){
    clickEventHandler('eventChangeSavedCartsPage', function(event){
        changeSavedCartsPage($(event.target).data("page"));
        return false;
    });
}
eventsSavedCartPagination();

function eventsApprovalPagination(){
    clickEventHandler('eventChangeApprovalsPage', function(event){
        changeApprovalsPage($(event.target).data("eventClickParamPage"));
    });

    clickEventHandler('eventChangeApprovalsPageToFirst', function(event){
        changeApprovalsPage(1);
    });
}
eventsApprovalPagination();

function eventsRequetsPagination(){
    clickEventHandler('eventChangeRequestsPage', function(event){
        changeRequestsPage($(event.target).data("page"));
        return false;
    });
}
eventsRequetsPagination();

function eventsMaterialPagination(){
    clickEventHandler('eventChangePage', function(event){
    	//SM-529 ML page pagination is not showing any results
        //changePage($(event.target).data("page"));
    	changePage($(event.currentTarget).data("page"));
    });
}
eventsMaterialPagination();

function eventsInvoicesListDisplay(){
    clickEventHandler('eventSortInvoiceNumber', function(event){
        sortInvoiceNumber();
    });

    clickEventHandler('eventSortOrderNumber', function(event){
        sortOrderNumber();
    });

    clickEventHandler('eventSortPONumber', function(event){
        sortPONumber();
    });

    clickEventHandler('eventSortDateInvoice', function(event){
        sortDateInvoice();
    });

    clickEventHandler('eventSortInvoiceAddress', function(event){
        sortInvoiceAddress();
    });

    clickEventHandler('eventRequestInvoiceStatus', function(event){
        var dataElem = $(event.target).closest('[data-event-click-id]');

        requestInvoiceStatus(dataElem.data("invoiceId"));

        return false;
    });
	
	$('#searchCS').change(function(ev) {
    	$('#reset-button').attr('disabled', null);
		$('#reset-button').attr('search', true);
    });
	
	$('#invoiceSearchTime').change(function(ev) {
    	$('#reset-button').attr('disabled', null);
		$('#reset-button').attr('search', true);
    });
	
	
}
eventsInvoicesListDisplay();

function eventsInvoicesPagination(){
    clickEventHandler('eventChangeInvoicePage', function(event){
        changeInvoicePage($(event.target).data("eventClickParamPage"));
    });
}
eventsInvoicesPagination();

function eventsManageCompaniesDisplay(){
    clickEventHandler('eventFirstLetter', function(event){
        filterLetter($(event.target).data("key"));
    });

    clickEventHandler('eventAccessWithoutWarning', function(event){
        accessWithoutWarning($(event.target).data("companyId"));
    });

    clickEventHandler('eventSetAccount', function(event){
        setAccount($(event.target).data("companyId"));
    });

    clickEventHandler('global174', function(event){
        accessAccount();
    });

    clickEventHandler('eventSortPage', function(event){
        sortPage($(event.target).data("sortKey"), $(event.target).data("sortDir"));
    });

    clickEventHandler('eventChangePage', function(event){
        changePage($(event.target).data("page"));
    });
}
eventsManageCompaniesDisplay();

function eventsManageUsersList(){
    clickEventHandler('eventSortPage', function(event){
        sortPage($(event.target).data("sortKey"), $(event.target).data("sortDir"));
    });

    clickEventHandler('eventChangePage', function(event){
        changePage($(event.target).data("page"));
    });
}

function eventsManageAddUserShipping(){
    clickEventHandler('eventDefaultShipping', function(event){
        defaultShipping($(event.target).data("csId"))
    });

    clickEventHandler('eventRemoveShipping', function(event){
        removeShipping($(event.target).data("csId"));
    });
}
eventsManageAddUserShipping();

function eventsMtrListDisplay(){
    clickEventHandler('eventSelectAll', function(event){
        selectAll(this);
    });
}

function eventsOrderListDisplay(){
    clickEventHandler('eventSortOrderDateOrderList', function(event){
        sortOrderDate();
    });
    clickEventHandler('eventSortOrderIdOrderList', function(event){
        sortOrderId();
    });
    clickEventHandler('eventSortOrderPOOrderList', function(event){
        sortOrderPO();
    });
    clickEventHandler('eventSortOrderTotalOrderList', function(event){
        sortOrderTotal();
    });
    clickEventHandler('eventCancelAutoOrder', function(event){
        cancelAutoOrder($(event.target).data("autoOrderId"));
        return false;
    });
}
eventsOrderListDisplay();

function eventsOrdersPagenation(){
    clickEventHandler('eventChangeOrdersPage', function(event){
        changeOrdersPage($(event.target).data("page"));
    });
}
eventsOrdersPagenation();

function eventsPackingPagination(){
    clickEventHandler('eventChangePackingPage', function(event){
        changePackingPage($(event.target).data("page"));
    });
}
eventsPackingPagination();

function eventsMaterialListDisplay(){
    clickEventHandler('eventAddGiftlistToCart', function(event){
        addGiftlistToCart('addGiftlistToCartForm' + $(event.target).data("id"), $(event.target).data("count"), this);
        return false;
    });

    clickEventHandler('eventShowShareMaterialModalPopup', function(event){
        showShareMaterialModalPopup($(event.target).data("id"));
        return false;
    });

    clickEventHandler('eventExportMaterialList', function(event){
        exportMaterialList($(event.target).data("id"));
    });

    clickEventHandler('eventShowImportMaterialListModalPopup', function (event) {
    	$('#errorIncorrect').hide();
    	$('.preload-cover').removeClass('loader-active');
    	$('.preload-cover .preload-spinner').hide();
        showImportMaterialListModalPopup($(event.target).data("giftlistId"));
        return false;
    });

    clickEventHandler('eventShowDeleteMaterialListModalPopup', function(event){
        showDeleteMaterialListModalPopup($(event.target).data("id"));
    });

    clickEventHandler('global200', function(event){
        deleteMaterialList($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('global226', function(event){
        sendEmails($(event.target).data("eventClickParam0"));
    });

    clickEventHandler('global227', function(event){
        resetReorderListSearch();
    });
    eventsModalNoListsFound();
}
eventsMaterialListDisplay();

function eventMaterialListUpload() {
    clickEventHandler('global208', function (event) {
        importMaterialList($(event.target).data("giftlistId"));
        return false;
    });
}
eventMaterialListUpload();

function eventsMaterialDetailDisplay(){
    clickEventHandler('eventSelectAll', function(event){
        selectAll(this);
    });
    clickEventHandler('eventSortMaterialDetails', function(event){
        var dataElem = $(event.target).closest("[data-event-click-id]");

        sortMaterialDetails(dataElem.data("sortBy"));
    });

    clickEventHandler('eventUpdateList', function(event){
        updateList(this, $(event.target).data("giftItemId"));
    });
}
eventsMaterialDetailDisplay();

function eventsReorderDetailDisplay(){
    clickEventHandler('eventAddItemToCart', function(event){
        addItemToCart($(event.target).data("giftItemId"));
    });
}
eventsReorderDetailDisplay();

clickEventHandler('eventSubmitAddAdmin', function(event){
	submitAddAdmin();
});

clickEventHandler('eventSearchCS', function(event){
	searchCS();
});

clickEventHandler('eventLocationReload', function(event){
	location.reload();
});

clickEventHandler('eventSelectedShippingAddress', function(event){
	doCS_SelectShippingAddress();
});

clickEventHandler('eventRememberMe', function(event){
	rememberMeClick(this)
});

clickEventHandler('eventHandleResetPassword', function(event){
	handleResetPassword();
});

clickEventHandler('eventSearchInvoiceButton', function(event){
	searchInvoiceButton();
	return false;
});

clickEventHandler('eventResetInvoiceSearch', function(event){
	resetInvoiceSearch();
	return false;
});

clickEventHandler('eventSearchStatementsButton', function(event){
    searchStatementsSearch(event);
    return false;
});

clickEventHandler('eventResetStatementsButton', function(event){
    resetStatementsSearch();
    return false;
});


clickEventHandler('eventSearchCompanies', function(event){
	searchCompanies();
});

clickEventHandler('eventResetSearch', function(event){
	resetSearch();
	return false;
});

clickEventHandler('eventResetMaterialListSearch', function(event){
	resetMaterialListSearch();
});

clickEventHandler('eventShowNewMaterialListModalPopup', function(event){
	showNewMaterialListModalPopup();
	return false;
});

clickEventHandler('eventSearchReorderLists', function(event){
	searchReorderLists();
	return false;
});

clickEventHandler('eventRequestMTR', function(event){
	requestMTR();
	return false;
});

clickEventHandler('eventClearMTRForm', function(event){
	clearMTRForm();
	return false;
});

clickEventHandler('eventDeleteSelectedMTR', function(event){
	deleteSelectedMTR();
	return false;
});

clickEventHandler('eventOpenApproveOrdersModal', function(event){
	openApproveOrdersModal();
	return false;
});

clickEventHandler('eventOpenRejectOrdersModal', function(event){
	openRejectOrdersModal();
	return false;
});

clickEventHandler('eventAddSelectedToReorder', function(event){
	addSelectedToReorder();
});

clickEventHandler('eventAddSelectedToCart', function(event){
	addSelectedToCart();
});

clickEventHandler('eventSearchOrdersButton', function(event){
	searchOrdersButton();
	return false;
});

clickEventHandler('eventSearchPackingButton', function(event){
	searchPackingButton();
	return false;
});

clickEventHandler('eventQuickOrderAddToCart', function(event){
	$('#quickOrderAddToCart').prop('disabled', true);
	quickOrderAddToCart('#quickAddToCart', 'Cart');
	return false;
});

clickEventHandler('eventClearQuickOrder', function(event){
	clearQuickOrder();
});

clickEventHandler('eventSearchListDetails', function(event){
	searchListDetails();
});

clickEventHandler('eventResetDetailsSearch', function(event){
	resetDetailsSearch();
});

clickEventHandler('eventSubmitRequestAQuotewithFile', function(event){
    if(quoteSubmitValidation()){
        submitRequestAQuotewithFile();
    }
});

clickEventHandler('eventSubmitRequestAQuote', function(event){
    if(quoteSubmitValidation()){	
        submitRequestAQuote();
    }
});
function quoteSubmitValidation(){
        var qouteFormValues = [];
		$("#quoteError").empty();
		if ($('#cu_input_fname').val().trim() == "") {
		qouteFormValues.push("First Name");
		}
		if ($('#cu_input_lname').val().trim() == "") {
		qouteFormValues.push("Last Name");
		}
		if ($('#cu_input_phone').val().trim()  == "") {
		qouteFormValues.push("Phone Number");
		}
		if ($('#cu_input_email').val().trim() == "") {
		qouteFormValues.push("Email Address");
		}
	   
        if (qouteFormValues.length > 0) {
			qouteFormValues.forEach(function(item) {
					throwsError(item)
			});
		}else{
            return true;
        }

}
        function throwsError(message) {
			$('.quoteError').append('<div class="col-md-12 hasError alert alert-danger">Please Enter the '+message+'</div>')
		}
clickEventHandler('eventRequestStatement', function(event){
	requestStatement(this);
	return false;
});

clickEventHandler('eventShowRoleTips', function(event){
	showRoleTips();
});

clickEventHandler('eventCreateUser', function(event){
	createUser();
});

clickEventHandler('eventResendInvite', function(event){
	resendInvite();
});

clickEventHandler('eventUpdateUser', function(event){
	if(validateUpdateUser()){
		updateUser();
	}
});

clickEventHandler('eventSearchUsers', function(event){
	searchUsers();
});

clickEventHandler('eventUpdateSpendingLimit', function(event){
	updateSpendingLimit();
});

clickEventHandler('eventShowEditMaterialListModalPopup', function (event) {
    showEditMaterialListModalPopup($(event.target).data("giftlistId"));
    return false;
});

clickEventHandler('eventShowShareMaterialModalPopup', function(event){
	showShareMaterialModalPopup($(event.target).data("giftlistId"));
	return false;
});

clickEventHandler('eventExportMaterialList', function(event){
	exportMaterialList($(event.target).data("giftlistId"));
});

clickEventHandler('eventShowDeleteMaterialListModalPopup', function(event){
	showDeleteMaterialListModalPopup($(event.target).data("giftlistId"));
});

clickEventHandler('eventCheckPriceSelected', function(event){
	checkPriceSelected();
	return false;
});

clickEventHandler('eventAddSelected', function(event){
	addSelected();
	return false;
});

clickEventHandler('eventDeleteSelected', function(event){
	deleteSelected();
	return false;
});

clickEventHandler('eventSelectBillingAddress', function(event){
	selectBillingAddress($(event.target).data("type"), this.value);
});

clickEventHandler('eventWindowPrint', function(event){
	window.print();
});

clickEventHandler('eventSubmitProfileContactUpdate', function(event){
	submitProfileContactUpdate();
});

clickEventHandler('eventSubmitProfilePersonalUpdate', function(event){
	submitProfilePersonalUpdate();
});

clickEventHandler('eventSubmitProfileSecurityUpdate', function(event){
	submitProfileSecurityUpdate();
});

changeEventHandler('selectOrg', function(event){
	$('#autocompleteSearchCS').val("");
    selectOrg(this.value, '#searchCS');
});

changeEventHandler('eventSortReorderDetails', function(event){
    sortReorderDetails();
});

changeEventHandler('eventUpdateCheckbox', function(event){
    updateCheckbox(this.checked);
});
changeEventHandler('eventDisplayOptions', function(event){
    displayOptions(this.checked);
});

keydownEventHandler('account42', function(event){
    limitText(this.form.cu_input_company,this.form.countdown,100);
});

keydownEventHandler('account43', function(event){
    limitText(this.form.cu_input_accountNumber,this.form.countdown,100);
});

keydownEventHandler('account44', function(event){
    limitText(this.form.cu_input_fname,this.form.countdown,100);
});

keydownEventHandler('account45', function(event){
    limitText(this.form.cu_input_lname,this.form.countdown,100);
});

keydownEventHandler('account46', function(event){
    limitText(this.form.cu_input_phone,this.form.countdown,15);
});

keydownEventHandler('account47', function(event){
    limitText(this.form.cu_input_email,this.form.countdown,100);
});

keydownEventHandler('account48', function(event){
    limitText(this.form.cu_input_postal_code,this.form.countdown,100);
});

keydownEventHandler('account49', function(event){
    limitText(this.form.cu_input_shipping_address,this.form.countdown,100);
});

keypressEventHandler('account11', function(event){
    if(event.charCode==10||event.charCode==13){
        searchCompanies();
        return false;
    }
});

keypressEventHandler('account16', function(event){
    if(event.charCode==10||event.charCode==13){
        searchReorderLists();
        return false;
    }
});

/*
keypressEventHandler('account32', function(event){
    return event.charCode === 0 || /^\w+$/.test(String.fromCharCode(event.charCode));
});
*/

keypressEventHandler('account35', function(event){
    return event.charCode === 0 || /\d/.test(String.fromCharCode(event.charCode));
});

keypressEventHandler('account156', function(event){
    if(event.charCode==10||event.charCode==13){
        searchListDetails();
        return false;
    }
});

keyupEventHandler('account50', function(event){
    limitText(this.form.cu_input_company,this.form.countdown,100);
});

keyupEventHandler('account51', function(event){
    limitText(this.form.cu_input_accountNumber,this.form.countdown,100);
});

keyupEventHandler('account52', function(event){
    limitText(this.form.cu_input_fname,this.form.countdown,100);
});

keyupEventHandler('account53', function(event){
    limitText(this.form.cu_input_lname,this.form.countdown,100);
});

keyupEventHandler('account54', function(event){
    limitText(this.form.cu_input_phone,this.form.countdown,15);
});

keyupEventHandler('account55', function(event){
    limitText(this.form.cu_input_email,this.form.countdown,100);
});

keyupEventHandler('account56', function(event){
    limitText(this.form.cu_input_postal_code,this.form.countdown,100);
});

keyupEventHandler('account57', function(event){
    limitText(this.form.cu_input_shipping_address,this.form.countdown,100);
});

clickEventHandler('register0', function(event){
	$("#registrationEmail").text("Your Valid Business Email Address*");
    return clearErrorsForUserCreation();
});

clickEventHandler('register1', function(event){
	$("#registrationEmail").text("Email Address*");

	return clearErrorsForUserCreation();
});

clickEventHandler('register2', function(event){
    return handleCreateUser();
});

clickEventHandler('eventOrderSearchType', function(event){
    return handleCheckFieldData($(event.target).attr('id'), '#orderSearchTerm');
});

clickEventHandler('eventPackSlipSearchType', function(event){
    return handleCheckFieldData($(event.target).attr('id'), '#packingSearchTerm');
});

clickEventHandler('eventInvoiceSearchType', function(event){
    return handleCheckFieldData($(event.target).attr('id'), '#invoiceSearchOrderNumber');
});

clickEventHandler('onStartCreditApp', function(event){
	window.location = '/global/credit/application.jsp';
});

clickEventHandler('eventRequestAccess', function(event){
	requestAccess();
});

clickEventHandler('eventGoToRegistrationPage', function (event) {
   goToRegistrationPage();
   return false;
});