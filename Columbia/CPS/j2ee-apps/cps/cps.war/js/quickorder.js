var fieldsNdx = 2; // initial page
var rowsShown = 1; // initial page
var setByFile = false;

var minRows = 10; // change this to set minimum rows
var maxShown = 150; // change this to set maximum rows

$(document).ready(function() {
	if ( setByFile && minRows > rowsShown ) {
		addRows(Math.ceil(minRows-rowsShown));
	} else {
		if (rowsShown == 1) {
			addRows(Math.ceil(minRows-rowsShown));
		} else {
			addRows(Math.ceil(rowsShown-2));
		}
	}


	for ( i = 0 ; i <= fieldsNdx ; i++ ) {
		var value = $("#LoadItemP"+i).val();
		if ( value != "" ) {
			$("#itemNumP"+i).val($("#LoadItemP"+i).val());
		}
	}

	for ( i = 0 ; i <= fieldsNdx ; i++ ) {
		var value = $("#LoadItemP"+i).val();
		if ( value != "" && $("#LoadQtyP"+i).val() > 0 ) {
			$("#qtyP"+i).val($("#LoadQtyP"+i).val());
		}
	}

	for ( i = 0 ; i < fieldsNdx ; i++ ) {
		$("#itemNumP"+i).blur(function(){
			$(this).val($(this).val());
		})

		$("#qtyP"+i).focus(function(){
			$(this).val('');
		}).mask("9?99");
	}

	if ($('#numberToAdd').val()=='' ){
		$('#numberToAdd').val('1');
	}

	$('#numberToAdd').focus(function(){
		$(this).val('');
	}).blur(function(){
		if($('#numberToAdd').val() != "") {
			var value = $('#numberToAdd').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var intRegex = /^\d+$/;
			if(!intRegex.test(value)) {
				$(this).val('1');
			}
		} else {
			$(this).val('1');
		}
	})


	$('#addmore').click(function(){
		var numToAdd = parseInt($("#numberToAdd").val())
		if ( rowsShown+numToAdd < maxShown ) {
			addRows(numToAdd);
		} else {
			var realNumToAdd = maxShown - rowsShown;
			addRows(realNumToAdd);
			$("#addFields").remove();
			$("#addMessage").text("Maximum of 300 item numbers has been reached");
		}
	})


	if (setByFile) {
		submitPage();
	}
});

function loadPage() {
	addRows(rowsShown);
}

function addRows(rowsToAdd) {
	for ( i = 0 ; i < rowsToAdd ; i++ ) {
		var insertPoint = $(".qo-list li").length;
		if (fieldsNdx < maxShown*2) {
			$(".qo-list li:nth-child(" + insertPoint + ")").before("<li>" +
					" <input id='itemNumP"+ fieldsNdx+"' value='' type='text' class='form-l' maxlength='25'/>" +
					" <input id='qtyP"+fieldsNdx++ +"' value='' type='text' class='form-qty' maxlength='3'/>" +
					"</li>" +
					"<li>" +
					" <input id='itemNumP"+ fieldsNdx+"' value='' type='text' class='form-l' maxlength='25'/>" +
					" <input id='qtyP"+fieldsNdx++ +"' value='' type='text' class='form-qty' maxlength='3'/>" +
			"</li>");
		} else {
			$("#errors").addClass('message').addClass('message-red').text("Your file has exceeded the 300 product number limit, Only the first 300 products were loaded.").show();
			break;
		}
	}
	rowsShown +=rowsToAdd;
}

function getItemCount(){
    return $(".qo-list > div").length*2;
}

function addMoreItem(){
    var itemCount = getItemCount();
    for(i = 0 ; i < 9 ; i=i+2){
        renderRow(itemCount+i, itemCount+i+1);
    }
}

function renderRow(id1, id2){
    var rowHtml = "<div class='row'>" +
        " <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>" +
    " <div class='row'>" +
    " <div class='col-lg-6 md-6 col-sm-7 col-xs-7'>" +
    " <div class='form-group'>" +
    " <label for='inputItem3' class='sr-only'>Item #</label>" +
    " </div>" +
    " <div class='form-group'>" +
    " <input type='text' class='form-control' id='itemNumP" + id1 + "' placeholder='Item #'>" +
    " </div>" +
    " </div>" +
    " <div class='col-lg-4 md-6 col-sm-5 col-xs-5'>" +
    " <div class='form-group'>" +
    " <label for='inputQty3' class='sr-only'>QTY</label>" +
    " </div>" +
    " <div class='form-group'>" +
    " <input type='text' class='form-control' id='qtyP" + id1 + "' placeholder='QTY'>" +
    " </div>" +
    " </div>" +
    " </div>" +
    " </div>" +
    " <div class='col-lg-6 col-md-6 col-sm-6 col-xs-12'>" +
    " <div class='row'>" +
    " <div class='col-lg-6 md-6 col-sm-7 col-xs-7'>" +
    " <div class='form-group'>" +
    " <label for='inputItem3' class='sr-only'>Item #</label>" +
    " </div>" +
    " <div class='form-group'>" +
    " <input type='text' class='form-control' id='itemNumP" + id2 + "' placeholder='Item #'>" +
    " </div>" +
    " </div>" +
    " <div class='col-lg-4 md-6 col-sm-5 col-xs-5'>" +
    " <div class='form-group'>" +
    " <label for='inputQty3' class='sr-only'>QTY</label>" +
    " </div>" +
    " <div class='form-group'>" +
    " <input type='text' class='form-control' id='qtyP" + id2 + "' placeholder='QTY'>" +
    " </div>" +
    " </div>" +
    " </div>" +
    " </div>" +
    " </div>";

    $( ".qo-list" ).append( rowHtml );
}


function submitPage(){
    var itemCount = getItemCount();
	for ( i = 0 ; i < itemCount ; i++ ) {
		if ( $("#qtyP"+i).val() == '' && $("#itemNumP"+i).val() != "" ) {
			$("#qtyP"+i).val(1);
		}
	}
	addListToCart(gatherSkus(),gatherQtys());
}

function gatherSkus(){
	itemIds = [];
	count = 0;
    var itemCount = getItemCount();
	for ( i = 0 ; i < itemCount ; i++ ) {
		if ( $("#itemNumP"+i).val() != "" ) {
			itemIds[count++] = $("#itemNumP"+i).val();
		}
	}
	return itemIds;
}

function gatherQtys(){
	qtys = [];
	count = 0;
    var itemCount = getItemCount();
	for ( i = 0 ; i < itemCount ; i++ ) {
		if ( $("#itemNumP"+i).val() != "" ) {
			qtys[count++] = $("#qtyP"+i).val();
		}
	}
	return qtys;
}

function setImportFlag(flag){
	$("#importFlag").val(flag);
}

function getImportFlag(){
	return $("#importFlag").val();
}

function setByFile(flag){
	$("#byFile").val(flag);
}

function getByFile(){
	return $("#byFile").val();
}

function setFirstLoad(flag){
	$("#firstLoad").val(flag);
}

function getFirstLoad(){
	return $("#firstLoad").val();
}

function addListToCart(itemIds, qtys){
	$("#add-item-error-message").remove();
	$("#addSkus").val(itemIds);
	$("#addQtys").val(qtys);
	
	$('#addToCartQuickOrder').submit();
	
	return false;
}


function showSuccess(text){

    var successMessageHtml = "<div class='alert alert-success' role='alert'>"+
    " <button type='button' class='close' data-dismiss='alert'>"+
    " <span aria-hidden='true'>&times;</span>"+
    " <span class='sr-only'>Close</span>"+
    " </button>"+
    " <span>" + text + "</span>"+
    " </div>";

    $( ".edcol-lg-12" ).append( successMessageHtml );
}

function showDanger(text){

    var dangerMessageHtml = "<div class='alert alert-danger' role='alert'>"+
        " <button type='button' class='close' data-dismiss='alert'>"+
    " <span aria-hidden='true'>&times;</span>"+
    " <span class='sr-only'>Close</span>"+
    " </button>"+
    " <span>" + text + "</span>"+
    " </div>";

    $( ".edcol-lg-12" ).append( dangerMessageHtml );
}