var map;

/*var myArray = [];
myArray[0] = new Array(-87.652526, 41.823364, 'Chicago', '1120 West Pershing Road<br>Chicago, Illinois 60609');
myArray[1] = new Array(-87.90105, 42.378826, 'Gurnee', '3900 Grove Avenue<br>Gurnee, Illinois 60031');
myArray[2] = new Array(-88.286478, 42.043626, 'Elgin', '60 Ann Street<br>Elgin, Illinois 60120');
myArray[3] = new Array(-88.12561, 41.504808, 'Joliet', '1803 Moen Avenue<br>Rockdale, Illinois 60436');
myArray[4] = new Array(-88.329479, 41.751544, 'Aurora', '544 South Lake Street<br>Aurora, Illinois 60506');
myArray[5] = new Array(-87.507656, 41.595511, 'Hammond', '850 165th Street<br>Hammond, Indiana 46325');
myArray[6] = new Array(-89.004864, 42.240946, 'Rockford', '5730 Columbia Pkwy<br>Rockford, IL 61108');
myArray[7] = new Array(-89.67771, 41.770362, 'Rock Falls', '900 Industrial Avenue<br>Rock Falls, Illinois 61071');
myArray[8] = new Array(-88.419008, 42.303177, 'Woodstock', '1000 Courtaulds Drive<br>Woodstock, Illinois 60098');
myArray[9] = new Array(-89.533346, 40.712218, 'Peoria', '125 Thunderbird Lane<br>East Peoria, Illinois 61611');
myArray[10] = new Array(-88.913298, 39.871977, 'Decatur', '2881 Parkway Drive<br>Decatur, Illinois 62526');
myArray[11] = new Array(-88.493072, 44.246681, 'Appleton', '5624 Technology Circle<br>Appleton, Wisconsin 54914');
myArray[12] = new Array(-87.980343, 43.00666, 'Milwaukee', '2100 S. 54th Street<br>West Allis, Wisconsin 53219');
myArray[13] = new Array(-88.101768, 43.002693, 'New Berlin', '16150 W. Lincoln Ave Unit C<br>New Berlin, Wisconsin 53151');
myArray[14] = new Array(-89.559335, 44.904648, 'Wausau', '5107 Westfair Ave<br>Schofield, Wisconsin 54476-4249');
myArray[15] = new Array(-87.613026, 45.151625, 'Menominee', '1108 56th Avenue<br>Menominee, Michigan 49858');
myArray[16] = new Array(-85.679362, 43.007925, 'Grand Rapids', '2400-A Turner Avenue NW<br>Grand Rapids, Michigan 49544');
myArray[17] = new Array(-85.543989, 42.245035, 'Kalamazoo', '3225 E. Kilgore Rd<br>Kalamazoo, Michigan 49001');
myArray[18] = new Array(-84.174561, 43.602288, 'Midland', '102 Fast Ice Drive<br>Midland, MI 48642');
myArray[19] = new Array(-83.450421, 42.342087, 'Canton', '41554 Koppernick<br>Canton, Michigan 48187');
myArray[20] = new Array(-93.134363, 44.84491, 'Eagan', '920 Apollo Road<br>Eagan, Minnesota 55121');*/

function initMap() {
	// Display the map, with some controls and set the initial location
	map = new google.maps.Map(document.getElementById('map'), {
		center: {lat: 42.601619944327965, lng: -87.51708984375},
		zoom: 6
	});
	loadData();
}

// Loads data from xml source
function loadData() {
	if (myArray != null) {
		for (var i = 0; i < myArray.length; i++) {
			var lon = myArray[i][0];// Longitude
			var lat = myArray[i][1];// Latitude
			var point = {lat: lat, lng: lon};
			var name = myArray[i][2];
			var html = '<div class="google_pop"><h2 style="color: green;font-style: italic;">'+ myArray[i][2] + 
					'</h2>' + myArray[i][3]+ 
					'<br /><a style="text-decoration:underline;" href="/locations/locations-detail.jsp?l='+ myArray[i][4] + 
					'">Go to Location page</a></div>';
			var marker = new google.maps.Marker({
				position: point,
				map: map,
				title: name
			});
			var infowindow = new google.maps.InfoWindow();
			google.maps.event.addListener(marker, 'click', (function(marker, html) {
				return function() {
					infowindow.setContent(html);
					infowindow.open(map, marker);
				}
			})(marker, html));
		}
	}
}