var infowindow;
function initialize()
{
var mapProp = {
  center:new google.maps.LatLng(42.996612,-87.802734),
  zoom:6,
  scrollwheel:false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };
this.map=new google.maps.Map(document.getElementById("googleMaps")
  ,mapProp);
}
function miniInitialize(lat,lng)
{
var mapMini = {
  center:new google.maps.LatLng(lat,lng),
  zoom:11,
  scrollwheel:false,
  streetViewControl: false,
  navigationControl: false,
  mapTypeControl: false,
  scaleControl: false,
  draggable: false,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };
var maps=new google.maps.Map(document.getElementById("miniMaps"),mapMini);
var iconBase = '../img/marker_green.png';
var point = new google.maps.LatLng(lat,lng);
var marker = new google.maps.Marker({position:point,map:maps,icon: iconBase});
}

function markers(lat,lng,count)
{
	//var lats = document.getElementById('lat');
	//var lngs= document.getElementById('lng');
	var iconBase = '../img/marker_green.png';
	var lats = parseFloat(lat);
	var lngs = parseFloat(lng);
	var city=document.getElementById('city'+count).value;
	var address=document.getElementById('address'+count).value;
	var state=document.getElementById('state'+count).value;
	var zip=document.getElementById('zip'+count).value;
	var id=document.getElementById('id'+count).value;
	//alert(address);
	var content= ('<font color="006400"size="4">'+city +'</font>'+ '</br>'+address +'</br>'+ city +', '+ state + ' '+ zip+'</br>'+'<a href="location-detail.jsp?loc=' + id +'">Go to Location Page</a>')
	var point = new google.maps.LatLng(lats,lngs);
	var marker = new google.maps.Marker({position:point,map:map,title:city, icon: iconBase});
		// Create info window. In content you can pass simple text or html code.
		infowindow = new google.maps.InfoWindow({
			content: content
		});
		 //Add listner for marker. You can add listner for any object. It is just an example in which I am specifying that infowindow will be open on marker mouseover
		google.maps.event.addListener(marker, "click", function() {
			infowindow.setContent(content);
			infowindow.open(map, marker);
		});
		
}