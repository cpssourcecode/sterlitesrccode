var logoutAllow = true;

$(document).ready(function() {
	$('#noResultsModal_noResultsFound').on('hide.bs.modal', function() {
	   $("#noResultsModal_noResultsFound #modal-matnone-title").html("<span id='noResultsModalAddText_noResultsFound'></span>");
            keypressEventHandler('cartridges15', function(event){
            formSubmitOnEnter(event, 'handleSecondarySearch');
       });
	});
	$(window).bind('popstate', function(){
    	  window.location.href = window.location.href;
    });
   // $('input:checkbox').removeAttr('checked');
    checkIsDifferentShipto();
    $('#orderApproval').on('hide.bs.modal', function() {
        if($('#selectSessionShipAddress').hasClass('in'))
        {
           $('#selectSessionShipAddress').focus();
           $('#selectSessionShipAddress').addClass("overflow_scroll");
        }
        });
    $('#selectSessionShipAddress').on('hide.bs.modal', function() {
        $("#differentSessionAddress").load("/global/modals/different-shipto.jsp?close=false",function(){
           $('.modal-backdrop').remove();
         });
    });
//    fix SM-555 Allow popups to be disabled in BCC 
    var welcomeModalValue = $('#welcomeModalValue').val();
    if(welcomeModalValue == 'true'){
    	welcomeModal();
    }
    sessionTimeoutLogin();
    $("#noResultsModal_noResultsFound").on('hide.bs.modal', function() {
        noResultFound();
    });
   	$('#password-forgot-form-step-1').on('keypress',
			function(event) {
		if (event.keyCode == 13) {
		event.stopImmediatePropagation();
		event.preventDefault();
		$(".send_instruction").trigger("click");
		}
	});
	$(document).tooltip();
	$.ajaxSetup ({
		cache: false
	});
		$("body").removeClass('nojs');
		$("ul.hover>li").hover(hoverOn,hoverOff);
		activateTypeAhead();
		$('.header-tools-label, .nav-drop .nav-label, .nav-subnav-label').append('<span class="ui-icon">&nbsp;</span>');
		//$('ul.nav-subnav>li:first-child').addClass("active");
		$('ul.header-tools-cart-list>li:first-child').css('border', 'none');
		$('.address-right ul li:first-child').addClass("first");
		$('ul.home-cats li:nth-child(3n), ul.cats li:nth-child(4n)').after('<li class="clear">&nbsp;</li>');

		// browser things

		//The .browser call has been removed in jquery 1.9
		/*if ($.browser.msie) {
			$('body').addClass('ie');
			if ($.browser.version.substr(0,1)==6) {
				$('body').addClass('ie6');
			}
			if ($.browser.version.substr(0,1)==7) {
				$('body').addClass('ie7');
			}
		} else if ($.browser.mozilla) {
			$('body').addClass('ff');
		} else if ($.browser.webkit) {
			$('body').addClass('wk');
		}*/


		// clear inputs, I doubt we want this
		/*$("input[type=text], input[type=email], input[type=password]").focus(function() {
			if( this.value == this.defaultValue ) {
				this.value = "";
			}
		}).blur(function() {
			if( !this.value.length ) {
				this.value = this.defaultValue;
			}
		});*/

		$("table.stripe tr:nth-child(2n)").addClass('alt');

		// feature slideshow

		if ( $('.slideshow')[0] ) {
			$('.slideshow ul')
			.after('<div class="slideshow-nav">')
			.cycle({
				fx: 'fade',
				timeout: 5000,
				pause: 1,
				pager:  '.slideshow-nav'
			});
		}

		// tabs

		if ( $('.tab')[0] ) {
			$(".tab").tabs();

			//	if ($.browser.msie) {
			//		$(".tab>ul>li>a").prepend('<span class="corner-left">&nbsp;</span>').append('<span class="corner-right">&nbsp;</span>');
			//	};
		}

		// accordion

		if ( $('.accordion')[0] ) {
			$("ul.accordion").accordion({
				active: false,
				alwaysOpen: true,
				collapsible: true,
				autoHeight: false,
				navigation: true,
				animated: false,
				header: '.subnav-header'
			});
			$(".subnav-header a").click(function() {
				window.location = $(this).attr('href');
				return false;
			});
		}

		// expand/contract (subnav)

		$("ul.expand ul:not(.active)").hide();
		$("ul.expand .subnav-header").append('<span class="ui-icon">&nbsp;</span>');

		$("ul.expand .active ul").show();
		$("ul.expand .active .subnav-header").addClass('ui-state-active');

		$("ul.expand .subnav-header").click(function (e) {
			if ( $(this).hasClass('ui-state-active') ) {
				$(this).removeClass('ui-state-active');
				$(this).siblings('ul').hide();
			} else {
				$(this).addClass('ui-state-active');
				$(this).siblings('ul').show();
			}
		});

		// expand/contract (content area)

		$("#content .expand-content").hide();
		$("#content .expand h2").append('<span class="ui-icon">Show/Hide</span>').click(function (e) {
			if ( $(this).hasClass('ui-state-active') ) {
				$(this).removeClass('ui-state-active');
				$(this).siblings('.expand-content').hide();
			} else {
				$(this).addClass('ui-state-active');
				$(this).siblings('.expand-content').show();
			}
		});

		// toggle registration
		function regCheck() {
			var regStatus = $('input[name="reg-status"]:checked').attr("value");
			$('.reg-options').hide();
			$('#reg-options-'+regStatus).show();
		}
		regCheck();
		$("#noAccountError").hide();
		$(".padding-5").hide();
		$("#noAccountInfo").hide();
		var registerStatus = $("input[type=radio][name=reg-status]:checked").val();
			if(registerStatus == 'hasaccount'){
				showRegistrationDetails();
			}
			else{
				hideRegistrationDetails();
				}
	   
			$("input[name='reg-status']").click(function() {
				var accountValue = $(this).val();
					if (accountValue == 'noaccount'){
						hideRegistrationDetails();	
		        	}
					else{
						showRegistrationDetails();
					}
			 });
					function showRegistrationDetails() {
						$("#regButton").show();
						$("#submitButton").hide();
						$("#hideNoAccountForm").show();
						$("#noAccountError").hide();
						$(".padding-5").hide();
					}
					function hideRegistrationDetails() {
						$(".padding-5").show();
						$("#noAccountError").show();
						$("#submitButton").show();
						$("#regButton").hide();
						$("#hideNoAccountForm").hide();
					}
					// expanding
		$('.expand-item .expand-label:not(.active)').siblings('.expand-content').hide();
		$('.expand-content').siblings('.expand-item .expand-label').append('<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span>').click(function() {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active').siblings('.expand-content').slideUp();
			} else {
				$(this).addClass('active').siblings('.expand-content').slideDown();
			}
		});

		// parents

		// $("ul.parents li a").append('<span class="icon"> (Remove)</span>');

		// subnav

		$('ul.subnav>li>a').addClass('subnav-header');
		$('ul.subnav a.subnav-header').append('<span class="ui-icon">&nbsp;</span>');

		// actions

		$('ul.actions li a').prepend('<span class="icon">&nbsp;</span>');

		// modals

		$('.item-qv').click(function (e) {
			$.modal('<iframe src="' + this.href + '" height="380" width="820" scrolling="no">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-xsell').click(function (e) {
			$.modal('<iframe src="modal-cart.html" height="540" width="820" scrolling="no">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-list').click(function (e) {
			items = gatherChecked();
			quantities = ""; // Finish this after pdp and listing pages are done
			url = "/global/modals/modal-list-add.jsp?modal=true&items="+items+"&quanitities="+quantities;
			$.modal('<iframe src="'+url+'" height="400" width="620" scrolling="yes">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-list-cart').click(function (e) {
			items = gatherChecked();
			url = "/global/modals/modal-list-add.jsp?modal=true&citems="+items;
			$.modal('<iframe src="'+url+'" height="400" width="620" scrolling="yes">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-list-pa').click(function (e) {
			items = gatherChecked();
			url = "/global/modals/modal-list-add.jsp?modal=true&indexs="+items;
			$.modal('<iframe src="'+url+'" height="400" width="620" scrolling="yes">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-delivery').click(function (e) {
			$.modal('<iframe src="' + this.href + '" height="540" width="820" scrolling="no">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-address').click(function (e) {
			$.modal('<iframe src="' + this.href + '" height="540" width="620" scrolling="no">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-text').click(function (e) {
			$.modal('<iframe src="' + this.href + '" height="400" width="620" scrolling="yes">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-short').click(function (e) {
			$.modal('<iframe src="' + this.href + '" height="300" width="500" scrolling="auto">', {
				overlayClose:true
			});
			return false;
		});

		$('.modal-calendar').click(function (e) {
			$("#pickup-date3").prop("checked", true);
			$.modal('<iframe src="/checkout/gadgets/calendar.jsp?modal=true" height="300" width="320" scrolling="auto">', {
				overlayClose:true
			});
			return true;
		});
		// If login=true then show cs modal
		var query = getQueryParams(document.location.search);
		if (query.login == 'true'){
			var showModalHref = "/global/modals/modal-cs.jsp?t=" + (new Date()).getTime();
			$('#showModalLink').attr('href', showModalHref);
			$('#showModalLink').click();
			$("#modal-cs-show-div .cs-btn-confirm").click(function(){
				doModalCS_SelectShippingAddress();
			});
		}
		// call selectpicker plugin after a modal appears
		$('body').on("loaded.bs.modal", function(){
			$('.selectpicker').selectpicker();
			$('.selectpicker-large').selectpicker('setStyle', 'btn-lg', 'add');
			$('.selectpicker-small').selectpicker('setStyle', 'btn-sm', 'add');
		});

		// overflow facets
		$('ul.facet-refine li:nth-child(6)').nextAll()
			.addClass('facet-overflow').hide().parent('ul')
			.append('<li class="facet-toggle"><a>Show <span class="facet-toggle-more">More</span><span class="facet-toggle-less">Less</span></a></li>');

		$("body").delegate(".facet-toggle a", "click", function() {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active').parent('li').siblings('.facet-overflow').slideUp();
			} else {
				$(this).addClass('active').parent('li').siblings('.facet-overflow').slideDown();
			}
		  });

		/*$('.facet-toggle a').click(function() {
			if ($(this).hasClass('active')) {
				$(this).removeClass('active').parent('li').siblings('.facet-overflow').slideUp();
			} else {
				$(this).addClass('active').parent('li').siblings('.facet-overflow').slideDown();
			}
		});*/

		// add items - header
		var $listItems = $('#list-manual>.quick-order-pad-auto-scroll>.well>.row:first-child');
		$('.list-add').on('click', function() {
			var newItems = $listItems.clone(true);
			newItems.find('input').val('');
			newItems.insertAfter($(".quick-order-pad-auto-scroll>.well>.row").last());
			newItems.find('.has-error').removeClass('has-error');
			return false;
		});
		   /*$('#brandsSearchBtn').click(function () {
			   //$qs.go();
		       $('input#brandsSearch').trigger("keyup");
		       return false;
		    });*/

    $(window).on('load resize scroll', function(event){
        productMenuAutoHeight();
    });

    function productMenuAutoHeight() {
        var availableHeight = $(window).outerHeight() - ( $('#stick').outerHeight() + 15 );
        if ($(window).outerWidth() > 975) {
            // e.g. 991 without scroll
            $('.dropdown-menu-autoheight')
                .css('max-height', availableHeight)
                .addClass('scrollable-y');
            if ($('.dropdown-menu-autoheight').hasClass('subdropdown-menu-second-level')) {
                $('.subdropdown-menu-second-level').css('height', availableHeight);
            }
        } else {
            $('.dropdown-menu-autoheight')
                .removeAttr('style')
                .removeClass('scrollable-y');
        }
    }

    $('.nav-products').mouseleave(function(event) {
        $('.dropdown-menu-first-level > li').removeClass('open');
        $('.subdropdown-menu-second-level').removeClass('show');
    });

    $('.dropdown-menu-first-level > li').mouseenter(function(event) {
        $('.dropdown-menu-first-level > li').removeClass('open');
        $(this).addClass('open');
        var triggeredItem = $(this).data('trigger');
        $('.subdropdown-menu-second-level').addClass('show');
        $('.subdropdown-menu-second-level > li').each(function(index, val) {
            if ($(val).data('toggler') == triggeredItem) {
                $(val).addClass('show');
            } else {
                $(val).removeClass('show');
            }
        });
    });
$('#logIn').on('shown.bs.modal', function() {
	var remember=$('#input-rememberMe').val();
	if(!remember){
		$('#input-email').val("");
	}
	$('#input-password').val("");
	$('#logIn #error-messages').attr("style","display:none;");
	$('#logIn #modal-success-messages').attr("style","display:none;");
	$('#logIn #login-form-captcha').attr("style","display:none;");
	$('#input-email').focus();
	});
$('#forgotPasswordModal').on('shown.bs.modal', function() {
	$('#password-forgot-input-email').val("");
	$('#forgotPasswordModal #modal-error-messages').attr("style","display:none;");
	$('#forgotPasswordModal #modal-success-messages').attr("style","display:none;");
		});
$('#sessionIdleModal').on('hide.bs.modal', function() {
	window.location.reload();
	});
$("#sessionIdleModal").on('keypress', function(event) {
		if (event.keyCode == 27) {
			window.location.reload();
			}
		});
		
		autoCompleteShipToAddress();
		//SM-356 Credit card only account notification
		loadCCAccountNotificationModal();
		$('#ccAccountNotificationModal').on('hide.bs.modal', function() {
	        if($('#myModalCheck').hasClass('in')) {
	           $('#myModalCheck').focus();
	           $('#myModalCheck').addClass("overflow_scroll");
	        }
	     });
		
		$("#productAddedToCartModal").on('hide.bs.modal', function() {
	        $('.preload-cover').removeClass('loader-active');
	        $('.preload-cover .preload-spinner').hide();
	    });

	});

	// Method that gets params from request and returns a
	// map of params and values
	// i.e. - query = getQueryParams(document.location.search);
	//		query.param == true
	function getQueryParams(qs) {
		qs = qs.split("+").join(" ");

		var params = {}, tokens,
			re = /[?&]?([^=]+)=([^&]*)/g;

		while (tokens = re.exec(qs)) {
			params[decodeURIComponent(tokens[1])]
				= decodeURIComponent(tokens[2]);
		}

		return params;
	}

	function changeCS(){
		dataString = $('#cs-selctor').serialize();
		$.ajax({
			type: "POST",
			data: dataString,
			dataType: "json",
			success: changeCSSuccess
		});
	}

	function changeCSSuccess(data){
		if (typeof data.errors == "undefined"){
			location.reload(true);
		}
	}

	function selectAll(check){
		var elem = document.getElementsByTagName('input');
		var checkboxes = new Array();
		for(i=0, iarr=0; i<elem.length; i++){
			att = elem[i].getAttribute("name");
			if (att == 'check'){
				checkboxes[iarr] = elem[i];
				iarr++;
			}
		}
		for(var i in checkboxes) {
			if ( checkboxes[i].checked != check.checked ) {
				checkboxes[i].click();
				checkboxes[i].checked = check.checked;
			}
		}
	}

function selectAllBoxes(elem){
	var elem = document.getElementsByTagName('input');
	var checkboxes = new Array();
	for(i=0, iarr=0; i<elem.length; i++){
		att = elem[i].getAttribute("name");
		if (att == 'check'){
			checkboxes[iarr] = elem[i];
			iarr++;
		}
	}
	for (var i in checkboxes) {
		if (checkboxes[i].checked != $("#"+checkBoxId).checked) {
			checkboxes[i].click();
			//checkboxes[i].checked = $("#"+checkBoxId).checked;
		}
	}
}

	function gatherChecked(){
		itemIds = [];
		var elem = document.getElementsByTagName('input');
		var inputs = new Array();
		for(i=0, iarr=0; i<elem.length; i++){
			att = elem[i].getAttribute("name");
			if (att =='itemsInCart'){
				inputs[iarr] = elem[i];
				iarr++;
			}
		}

		for (var i = inputs.length -1 ; i>= 0; i--)
			if (inputs[i].type === "checkbox" && inputs[i].checked)
				itemIds.push(inputs[i].id);

		return itemIds;
	}

function gatherListChecked(){
	itemIds = [];
	var elem = document.getElementsByTagName('input');
	var inputs = new Array();
	for(i=0, iarr=0; i<elem.length; i++){
		att = elem[i].getAttribute("name");
		if (att == 'check'){
			inputs[iarr] = elem[i];
			iarr++;
		}
	}

	for (var i = inputs.length -1 ; i>= 0; i--)
		if (inputs[i].type === "checkbox" && inputs[i].checked)
			itemIds.push(inputs[i].id);

	return itemIds;
}

function gatherListCheckedByName(name){
	itemIds = [];
	var elem = document.getElementsByTagName('input');
	var inputs = new Array();
	for(i=0, iarr=0; i<elem.length; i++){
		att = elem[i].getAttribute("name");
		if (att == name){
			inputs[iarr] = elem[i];
			iarr++;
		}
	}

	for (var i = inputs.length -1 ; i>= 0; i--)
		if (inputs[i].type === "checkbox" && inputs[i].checked)
			itemIds.push(inputs[i].id);

	return itemIds;
}

	function indexOf(array, element) {
		for(var i=0; i<array.length; i++){
			if (array[i]==element){
				return i;
			}
		}
		return -1;
	}

	// pagination methods
	function loadPagination(){
		var total=$("#total_count").val();
		var numPerPage = $("#numPerPage").val();
		$("#pagination").load("/account/gadgets/material-pagination.jsp?modal=true&page="+currentPage+"&numPerPage="+numPerPage+"&total="+total, function () {
			eventsMaterialPagination();
        });
	}

	// hover states

	function hoverOn(){
		$(this).siblings().removeClass("active");
		$(this).addClass("active");
	}
	function hoverOff(){
		$(this).removeClass("active");
	}

	function subnavOn(){
		$(this).siblings().removeClass("active");
		$(this).addClass("active");
	}
	function subnavOff(){}

	function addToCartItemsCat(pId, skuId, url){
	      $("#add-item-error-message").remove();
	      if(pId == '' && skuId == ''){
	         dataString = $('#addToCartQuickOrderHeader').serialize();
	      }else{
	      var qty = $("#"+skuId+"_qty").val();
	      dataString = $('#addToCart'+pId).serialize();
	      }
	      $.ajax({
	          type: "POST",
	          url: url,
	          data: dataString,
	          dataType: "json",
	          success: function(data) {
	        	  alert("Items Added");
	          },

	          error: function (xhr, ajaxOptions, thrownError) {
	            alert(xhr.statusText);
	            alert(xhr.responseText);
	            alert(xhr.status);
	            alert(ajaxOptions);
	            alert(thrownError);
	          }


	      });
	      //$(formId).submit();
	      return false;
	  }

	// Common ajax error message
	// Alerts the error, this shouldn't happen
	var cps_http_conflict_retry_login = true;
	function ajaxErrorLogin(xhr, ajaxOptions, thrownError){
		if (xhr.status === 409 && cps_http_conflict_retry_login) { // HTTP Conflict, for session confirmation mismatch
			cps_http_conflict_retry_login = false;

			// Try to extract updated confirmation number from the returned result in case of HTML
			var sessConfRegEx = /<input name="_dynSessConf" value="(.*?)" type="hidden"\/>/;
			var sessConfMatch = sessConfRegEx.exec(xhr.responseText);

			if (sessConfMatch != null && sessConfMatch.length > 1) {
				$('input[name=_dynSessConf]').val(sessConfMatch[1]);
				handleLogin();
			}
		} if (xhr.status === 200) {
//			openErrorMessage("Please refresh page and try again");
			window.location.href = '/';
		} else {
			showGenericError();
		}
	}

	function showGenericError(data){
		errorMessage = "An unexpected error has occurred. Please contact customer service or try again at another time.";
		openErrorMessage(errorMessage);
	}

	function showModal(url) {
		var showModalHref = url;
		$('#showModalLink').attr('href', showModalHref);
		$('#showModalLink').click();

		return false;
	}

	function loadForgotPasswordModal(url) {
		$("#modal-content").load("/global/modals/modal-password.jsp");
	}


function hideErrors() {
	$("#error").hide();
	$(".alert-danger").hide();
}

function hideSuccess() {
	$(".alert-success").hide();
}

function closeErrorMessage() {
	$(".error").removeClass("error");
	$(".form-error").removeClass("form-error");
};

//---------------------------------------------

function searchRedirect(){
	var dataString = $('#searchform').serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: redirectSuccess,
		error: showGenericError()
	});
}

function redirectSuccess(data){
	if ('success' == data.code) {
		if (data.redirectTo){
			document.location.href = data.redirectTo;
		}
	}
}

function limitText(limitField, limitCount, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    } else {
    	if(limitCount) {
            limitCount.value = limitNum - limitField.value.length;
        }
    }
}

function initializeCheckboxes(){

	$('#select-item-material-check input').on('change', function () {
	  if ($(this).checkbox('isChecked')) {
		$('.content-list').find('.content').removeClass('selected');
		// $('#select-item-material-check input').checkbox('uncheck');
		$(this).closest('.content').addClass('selected');
		$(this).checkbox('check');

	  }
	});

	$('#select-item-material-name input').on('change', function () {
	  if ($(this).checkbox('isChecked')) {
		$('.content-list').find('.content').removeClass('selected');
		// $('#select-item-material-check input').checkbox('uncheck');
		$(this).closest('.content').addClass('selected');
		$(this).checkbox('check');

	  }
	});

	$('#select-item-material-check-all input').on('change', function () {
	  if ($(this).checkbox('isChecked')) {
		// $('.content-list').find('.content').removeClass('selected');
		  $('#select-item-material-check input').each(function(){
			  var $this = $(this);
			  var disabled = $this.attr('disabled');
			  if (!disabled) {
				  $this.checkbox('check');
			  }
		  });
		  $('#select-item-material-name input').each(function(){
			  var $this = $(this);
			  var disabled = $this.attr('disabled');
			  if (!disabled) {
				  $this.checkbox('check');
			  }
		  });

	  } else {
		$('#select-item-material-check input').checkbox('uncheck');
		$('#select-item-material-name input').checkbox('uncheck');
	  }
	});

	$('#select-approvals-check-all input').on('change', function () {
		if ($(this).checkbox('isChecked')) {
			$('#select-approval-check input').each(function(){
				var $this = $(this);
				var disabled = $this.attr('disabled');
				if (!disabled) {
					$this.checkbox('check');
				}
			});
		} else {
			$('#select-approval-check input').checkbox('uncheck');
		}
	});

	$('#select-approvals-check-all-mobile input').on('change', function () {
		if ($(this).checkbox('isChecked')) {
			$('#select-approval-check-mobile input').each(function(){
				var $this = $(this);
				var disabled = $this.attr('disabled');
				if (!disabled) {
					$this.checkbox('check');
				}
			});
		} else {
			$('#select-approval-check-mobile input').checkbox('uncheck');
		}
	});

}

$(window).on('load resize', function() {
	var windowWidth = $(window).width();
	if (windowWidth <= 768) {
        $('.sidebar-collapsed')
            .addClass('collapse')
            .removeAttr('style');
        $('.equalize').find('div[class^="col-"] .thumbnail').css('height', 'auto');
    } else {
        $('.sidebar-collapsed')
            .removeClass('collapse')
            .css('height', 'auto');
        $(".dropdown-hover-trigger").addClass('dropdown-hover');
    };
    if (windowWidth <= 480) {
        $('.dropdown-table-actions').width(windowWidth - 50);
    } else {
        $('.dropdown-table-actions').removeAttr('style');
    };
});


// scrolling
		$('body').addClass('ontop');
		$(window).scroll(function(){
			if ($(window).scrollTop() > 0){
				$('body').removeClass('ontop').addClass('offtop');
			} else {
				$('body').removeClass('offtop').addClass('ontop');
                setTimeout(function(){
                    productMenuAutoHeight();
                }, 300);
			}
		});




function changeEventHandler(eventName, func) {
	$("[data-event-change-id=" + eventName + ']').off( "change");
	$("[data-event-change-id=" + eventName + ']').change(func);
}

function clickEventHandler(eventName, func) {
	$("[data-event-click-id=" + eventName + ']').off('click');
    $("[data-event-click-id=" + eventName + ']').click(func);
}

function keydownEventHandler(eventName, func) {
    $("[data-event-click-id=" + eventName + ']').off('keydown');
    $("[data-event-keydown-id=" + eventName + ']').keydown(func);
}

function keypressEventHandler(eventName, func) {
    $("[data-event-click-id=" + eventName + ']').off('keypress');
	$("[data-event-keypress-id=" + eventName + "]").keypress(func);
}

function keyupEventHandler(eventName, func) {
    $("[data-event-keyup-id=" + eventName + "]").keyup(func);
}

function inputEventHandler(eventName, func) {
    $("[data-event-input-id=" + eventName + "]").on('input', (func));
}

function loadEventHandler(eventName, func) {
    $("[data-event-load-id=" + eventName + "]").load(func);
}

function eventsFallbackImage(){
	$("[data-fallback-image]").each(function(){
		var img = $(this).data("fallbackImage");
		$(this).error(function () {
			$(this).attr('src', img);
        });
		$(this).attr('src', $(this).data('src'));
	});
}

window.addEventListener('DOMContentLoaded', function(){
	eventsFallbackImage();
});
document.addEventListener("DOMContentLoaded", imageLazyLoad );
function imageLazyLoad() {
	  var lazyloadImages = document.querySelectorAll("img.lazy");    
	  var lazyloadThrottleTimeout;
	  
	  function lazyload () {
	    if(lazyloadThrottleTimeout) {
	      clearTimeout(lazyloadThrottleTimeout);
	    }    
	    
	    lazyloadThrottleTimeout = setTimeout(function() {
	        var scrollTop = window.pageYOffset;
	        
            for(var i=0; i<lazyloadImages.length; i++){
	            if(lazyloadImages[i].offsetTop < (window.innerHeight + scrollTop)) {
	              lazyloadImages[i].src = lazyloadImages[i].dataset.src;
	              lazyloadImages[i].classList.remove('lazy');
	            }
	        }
	        if(lazyloadImages.length == 0) { 
	          document.removeEventListener("scroll", lazyload);
	          window.removeEventListener("resize", lazyload);
	          window.removeEventListener("orientationChange", lazyload);
	        }
	    }, 20);
	  }
	  
	  document.addEventListener("scroll", lazyload);
	  window.addEventListener("resize", lazyload);
	  window.addEventListener("orientationChange", lazyload);
	}
function noResultFound(){
    var url = window.location.href;
    var navState = $("#secondarySearchHidden").val();
	var link = updateURLSearchParam(url, navState);
    link = updateURLParameter(link, "Ssp", 'true');
    var indexNo = link.indexOf('No');
	if (indexNo > 0) {
		var startLink = link.substring(0, indexNo);
		var endLink = link.substring(indexNo);
		link = startLink + endLink.substring(endLink.indexOf('&') + 1);
	}
	navigate(link, null, null, true);
}

function autoCompleteShipToAddress(){
	var data=[];	
	$('#searchCS option').each(function(){
		var value = $(this).val();
		var label= $(this).attr('data-content');
		var item = {};
		item['value'] = value;
		item['label'] = label;
		data.push(item);		
	});
	$("#autocompleteSearchCS").autocomplete({
		source: data,
	    minLength: 0,
		select: function(event, ui) {
			event.preventDefault();
			$(this).val(ui.item.label);
			$("#searchCS").val(ui.item.value);
		}
	}).focus(function () {
		$(this).autocomplete("search");
	});
}
function sessionTimeoutLogin() {
	if ($('#originalURL').val() != undefined) {
		$('#loggedIn span').trigger('click');
	}
}

function welcomeModal(){
	  // Check if user saw the modal
    var key = 'newUser';
	var newUser = localStorage.getItem(key);
	var localTime='timestamp';
	var timestamp=localStorage.getItem(localTime);
	var currentDate=new Date();
	//check modal creation date 
	if(timestamp){
	var timeDiff = Math.abs(currentDate.getTime() - timestamp);
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
	var datelimit =$('#expireModal').val();
	if(diffDays >= datelimit){
		localStorage.clear();
	}

	}
    // Show the modal only if new user
    if (!newUser && $("#isHomePage").val() == "true" && $("#transient").val() == "true")  {
        $('#welcomeModal').modal('show');
         localStorage.setItem(localTime, currentDate.getTime());
    }
// If modal is displayed, store that in localStorage
    $('#welcomeModal').on('shown.bs.modal', function () {
        localStorage.setItem(key, true);
    });
}
function checkIsDifferentShipto(){
    var differentAddress = $('#differentAddress').val();
    var isHomePage=$('#isHomePage').val();
    var sessionAddressFlag=$('#sessionAddressFlag').val();
    if(differentAddress =="true" && isHomePage == "true" && sessionAddressFlag == "true" ){
      $('#selectSessionShipAddress').modal('show');
    }
}

//SM-356 Credit card only account notification
function loadCCAccountNotificationModal(){
	$('#ccNotificationSubmit').click(function(e){
		$("#modalCCNotification").load("/global/modals/modal-cc-account-notification.jsp?isAgreed=true&isCCAccountModalShown=false",function(){
	        $(".modal-backdrop.fade.in").remove();
			$("body.modal-open").removeClass("modal-open");
	      });
	});		
}