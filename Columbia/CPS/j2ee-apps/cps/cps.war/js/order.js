var sortId = false;
var sortPO = false;
var sortDate = true;
var sortTotal = false;
var sort = 'ORDER_DATE';
var lastSortAccend = false;
var sortOption;

function sortOrderId(){
	resetSort();
	sortId = !sortId;
	if (sortId){
		sortOption = "ASC";
	}else{
		sortOption = "DESC";
	}
	lastSortAccend = !sortId;
	sort = 'ORDER_NUMBER';

	$('#orderSearchSortField').val(sort);
	$('#orderSearchSortOption').val(sortOption);
	searchOrders();
}

function sortOrderDate(){
	resetSort();
	sortDate = !sortDate;
	if (sortDate){
		sortOption = "DESC";
	}else{
		sortOption = "ASC";
	}
	lastSortAccend = !sortDate;
	sort = 'ORDER_DATE';

	$('#orderSearchSortField').val(sort);
	$('#orderSearchSortOption').val(sortOption);
	searchOrders();
}

function sortOrderPO(){
	resetSort();
	sortPO = !sortPO;
	if (sortPO){
		sortOption = "ASC";
	}else{
		sortOption = "DESC";
	}
	lastSortAccend = !sortPO;
	sort = 'CUSTOMER_PO_NUMBER';

	$('#orderSearchSortField').val(sort);
	$('#orderSearchSortOption').val(sortOption);
	searchOrders();
}

function sortOrderTotal(){
	resetSort();
	sortTotal = !sortTotal;
	if (sortTotal){
		sortOption = "ASC";
	}else{
		sortOption = "DESC";
	}
	lastSortAccend = !sortTotal;
	sort = 'ORDER_TOTAL';

	$('#orderSearchSortField').val(sort);
	$('#orderSearchSortOption').val(sortOption);
	searchOrders();
}

function setCurrentOrderListSortingPosition() {
    if(sort == 'ORDER_NUMBER'){
        $("#sort-id i").removeClass("fa-sort");
        if(sortOption=="ASC"){
            $("#sort-id i").addClass("fa-sort-asc");
        }
        else{
            $("#sort-id i").addClass("fa-sort-desc");
        }
    }
    if(sort == 'ORDER_DATE'){
        $("#sort-date i").removeClass("fa-sort");
        $("#sort-date i").addClass("fa-sort-desc");
        if(sortOption=="ASC"){
            $("#sort-date i").removeClass("fa-sort-desc");
            $("#sort-date i").addClass("fa-sort-asc");
        }
    }
    if(sort == 'CUSTOMER_PO_NUMBER'){
        $("#sort-cs i").removeClass("fa-sort");
        if(sortOption=="ASC"){
            $("#sort-cs i").addClass("fa-sort-asc");
        }
        else{
            $("#sort-cs i").addClass("fa-sort-desc");
        }
    }
    if(sort == 'ORDER_TOTAL'){
        $("#sort-total i").removeClass("fa-sort");
        if(sortOption=="ASC"){
            $("#sort-total i").addClass("fa-sort-asc");
        }
        else{
            $("#sort-total i").addClass("fa-sort-desc");
        }
    }
}

function resetSort (){
	$("#sort-id").removeClass("up down");
	$("#sort-po").removeClass("up down");
	$("#sort-date").removeClass("up down");
	$("#sort-total").removeClass("up down");

}

function searchOrdersButton(){
	$("#is-search-button").val("true");
	$("#orderSearchStartIndex").val(0);
    if($.trim($('#autocompleteSearchCS').val())==''){
        $('#searchCS').val("");
    }
	searchOrders();
    resetSearchButton();

	$("#is-search-button").val("false");
}

function searchOrders() {
	dataString = $('#search-orders').serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: loadOrdersSuccess
	});
}

function loadInitialOrders(){
	dataString = $('#search-orders').serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: loadOrdersSuccess
	});
}

function onOrderListDisplayLoaded(){
    calculateOrdersCurrentPage();
    loadOrdersPagination();
    setCurrentOrderListSortingPosition();
}

function loadOrdersSuccess(data){
	if (typeof data.errors == "undefined") {
		$("#order-list").load("/account/gadgets/order-list-display.jsp",
				function() {
					onOrderListDisplayLoaded();
					eventsOrderListDisplay();
				});
	} else {
		$('#order-list').html('');
		$('#pagination').empty();
		var billAddrRowCount= $("#selectedBillTo option").length;
		for (var i = 0; i <= data.errors.length; i++) {
			if (data.errors[i] != "Select One Option to Search for Order."
					&& data.errors[i] != undefined) {
				 if (billAddrRowCount == 2) {
					$('#order-list')
							.append(
									"<div class='alert alert-danger'>"
											+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
											+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
											+ "</span></button>"
											+ "Please contact your admin to add Billing Account"
											+ "</div>");
				} else if (billAddrRowCount > 2) {
					$('#order-list')
							.append(
									"<div class='alert alert-danger'>"
											+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
											+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
											+ "</span></button>"
											+ data.errors[i] + "</div>");
				}
				document.getElementById("reset-button").disabled = true;
				setTimeout('$("#dismiss-err_search_order").click()', 2000);
				uncheckSearchParameters();
				break;
			} else if (data.errors[i] == "Select One Option to Search for Order.") {
				document.getElementById("reset-button").disabled = true;
				$("#link-err_search_order").click();
				setTimeout('$("#dismiss-err_search_order").click()', 2000);
				break;
			}
		}
	}
}


function resetSearch(){
	//location.reload(true);


	$('#orderSearchTerm').val("");
	$('#searchCS').val("");
	$('#autocompleteSearchCS').val("");
	$("button[data-id='searchCS']").attr("title", "Ship To Address");
	$("button[data-id='searchCS'] span.filter-option").html("Ship To Address");
	$('#orderSearchTime').val("");
	$("button[data-id='orderSearchTime']").attr("title", "Date Range");
	$("button[data-id='orderSearchTime'] span.filter-option").html("Date Range");
	$('#orderSearchStartIndex').val("0");
    document.getElementById("reset-button").disabled = true;
    uncheckSearchParameters();
	searchOrders();

}

function changeOrdersPage(page) {

	var pageSize = $("#orderSearchPageSize").val();
    var startIndex = (page - 1) * pageSize;
	$("#orderSearchStartIndex").val(startIndex);
	searchOrders();

}

function calculateOrdersCurrentPage(){
	var startIndex = $("#orderSearchStartIndex").val();
	var pageSize = $("#orderSearchPageSize").val();
	var total = $("#total_count").val();
	var currentPage = (startIndex / pageSize | 0) + 1;
	$("#page").val(currentPage);
}

function loadOrdersPagination(){
	var total = $("#total_count").val() || 0;
	var numPerPage = $("#orderSearchPageSize").val();
	var currentPage = $("#page").val() || 1;
	$("#pagination").load("/account/gadgets/orders-pagination.jsp?modal=true&page="+currentPage+"&numPerPage="+numPerPage+"&total="+total, function () {
		eventsOrdersPagenation();
    });
}

function selectOrg(org, id) {
    console.log('order selected org = ' + org);
    $('.spinner-on-start').addClass('loader-active');
    $('.spinner-on-start .loading').show();
    var date = new Date();
    $(id).find('option').remove().end();
    $(id).load("/global/gadgets/cs-list.jsp?filter=&nocache=" + date.getTime() + "&orgId=" + org,  function(){
    	autoCompleteShipToAddress();
        $('.spinner-on-start').removeClass('loader-active');
        $('.spinner-on-start .loading').hide();
    },initScrollbar);
}