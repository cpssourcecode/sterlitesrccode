function changeHomeHeight(){

	$(".main-content").removeAttr("style");
	var categoryListHeight = $(".mainmenu-dropdown-home").height();
	var testimonialsHeight = $("#products .well").height();	
	var carouselHeight = 0;
	$("#carousel-example-generic .item").each(function( index ) {
		if(carouselHeight < $(this).height()){
			carouselHeight = $(this).height()
		}
	});	
	var menuHeight = categoryListHeight + testimonialsHeight + carouselHeight + 110;
	var bannerHeight = $(".owl-home").innerHeight();
	var mainContentHeight = $(".main-content").innerHeight();
	if(menuHeight > bannerHeight + mainContentHeight){
		var newMainHeight = menuHeight - bannerHeight;
		$(".main-content").attr("style", "height: " + newMainHeight + "px" );
	}
}

$( document ).ready(function() {
	$(window).on('load', function() {
		changeHomeHeight();
		setTimeout(function(){
			$(window).trigger('resize');
		}, 500);
	});
	$(window).on('resize', function() {
		changeHomeHeight();
	});
});

