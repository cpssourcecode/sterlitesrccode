/* Javascript for all csr functionality*/

var sort = 'email';
var lastSortAccend = false;
var sortFirst=false;
var sortLast=false;
var sortEmail=true;
var sortRole=false;

function sortFirstName(){
	resetSort();
	sortFirst = !sortFirst;
	if (sortFirst){
		$("#sort-first").addClass(" sort-up");
	}else{
		$("#sort-first").addClass(" sort-down");
	}
	lastSortAccend = !sortFirst;
	sort = 'firstName';
	$("#user-list").load("/csr/gadgets/user-list.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortFirst+"&ran="+new Date().getTime());
}

function sortLastName(){
	resetSort();
	sortLast = !sortLast;
	if (sortLast){
		$("#sort-last").addClass(" sort-up");
	}else{
		$("#sort-last").addClass(" sort-down");
	}
	lastSortAccend = !sortLast;
	sort = 'lastName';
	$("#user-list").load("/csr/gadgets/user-list.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortLast+"&ran="+new Date().getTime());
}

function sortEmailAddress(){
	resetSort();
	sortEmail = !sortEmail;
	if (sortEmail){
		$("#sort-email").addClass(" sort-up");
	}else{
		$("#sort-email").addClass(" sort-down");
	}
	lastSortAccend = !sortEmail;
	sort = 'email';
	$("#user-list").load("/csr/gadgets/user-list.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortEmail+"&ran="+new Date().getTime());
}

function sortUserRole(){
	resetSort();
	sortRole = !sortRole;
	if (sortRole){
		$("#sort-role").addClass(" sort-up");
	}else{
		$("#sort-role").addClass(" sort-down");
	}
	lastSortAccend = !sortRole;
	sort = 'roles';
	$("#user-list").load("/csr/gadgets/user-list.jsp?page="+currentPage+"&sort="+sort+"&accend="+sortRole+"&ran="+new Date().getTime());
}

function resetSort (){
	$("#sort-first").removeClass("sort-up sort-down");
	$("#sort-last").removeClass("sort-up sort-down");
	$("#sort-email").removeClass("sort-up sort-down");
	$("#sort-role").removeClass("sort-up sort-down");
}

function setAccount(orgId){
	$("#orgId").val(orgId);
	$("#selectAccount").submit();
}

function searchUsers(){
	clearHighlighting();
	dataString = $("#search-user").serialize();
	$.ajax({ type: "POST",
		url:"/csr/searchUser.jsp",
		data: dataString,
		dataType: "json",
		success: searchUsersSuccess,
		error: ajaxError
	});
}

function searchUsersSuccess(data){
	if (typeof data.errors == "undefined"){
		$("#user-display").show();
		$("#user-display").load("/csr/gadgets/search-user-display.jsp");
	}else{
		$("#user-display").hide();
		showErrors(data);
	}
}

function impersonateUser(userId){
	$("#userId").val(userId);
	$("#impersonateUserForm").submit();
}

function changeUser(){
	dataString = $("#change-user").serialize();
	$.ajax({ type: "POST",
		url:"/csr/searchUser.jsp",
		data: dataString,
		dataType: "json",
		success: changeUsersSuccess,
		error: ajaxError
	});
}

function changeUsersSuccess(data){
	if (typeof data.errors == "undefined"){
		user = data.user;
		window.location.href="/csr/gadgets/csr-service.jsp?userId="+user;
	}
}

function loginCSRUser(){
	dataString = $("#login-csr-user").serialize();
	$.ajax({ type: "POST",
		url:"/csr/searchUser.jsp",
		data: dataString,
		dataType: "json",
		success: loginCSRUserSuccess,
		error: ajaxError
	});
}

function loginCSRUserSuccess(data){
	if (typeof data.errors == "undefined"){
		window.location.href="/csr/searchUser.jsp";
	}
}

//Common ajax error message
//Alerts the error, this shouldn't happen
function ajaxError(xhr, ajaxOptions, thrownError){
	alert("StatusText: "+xhr.statusText);
	alert("Status: "+xhr.status);
	alert("thrownError: "+thrownError);
}

//Common show error method, all ajax calls can use this to
//display form errors during successful ajax call
function showErrors(data){
	showErrorsCommon(data, false);
}

function showErrorsModal(data){
	showErrorsCommon(data, true);
}

function showSuccess(message){
	openSuccessMessage(message);
}

function showSuccessModal(message){
	openSuccessMessageModal(message);
}

function showErrorsCommon(data, isModal){
	var errorMessage = "";
	var propertiesArray = [];
	var errorsArray = [];
	for(var i=0; i<data.errors.length; i++){
		if (indexOf(errorsArray,data.errors[i]) == -1){
			errorsArray.push(data.errors[i]);
			if (errorMessage == ""){
				errorMessage = data.errors[i];
			}else{
				errorMessage += "<br/>" + data.errors[i];
			}
		}
		propertiesArray.push(data.properties[i]);
	}
	if (errorMessage != ""){
		if(isModal) {
			openErrorMessageModal(errorMessage);
		} else {
			openErrorMessage(errorMessage);
		}
		highlightErrors(propertiesArray);
	}
}



//Adds error messages to error-messages div and shows it

function openErrorMessageDiv(errorMessage, divId){
	var errorMessagesDiv = $("#" + divId);
	errorMessagesDiv.html(errorMessage);
	errorMessagesDiv.show();
}

function openErrorMessage(errorMessage){
	var errorMessagesDiv = $("#error-messages");
	errorMessagesDiv.html(errorMessage);
	errorMessagesDiv.show();
}

function openErrorMessageModal(errorMessage){
	var errorMessagesDiv = $("#modal-error-messages");
	errorMessagesDiv.html(errorMessage);
	errorMessagesDiv.show();
}

function openSuccessMessage(successMessage){
	var successMessagesDiv = $("#success-messages");
	successMessagesDiv.html(successMessage);
	successMessagesDiv.show();
}

function openSuccessMessageModal(successMessage){
	var successMessagesDiv = $("#modal-success-messages");
	successMessagesDiv.html(successMessage);
	successMessagesDiv.show();
}

//Adds form error class to all inputs with id = passed
function highlightErrors(propertiesArray){
	for (var i=0; i<propertiesArray.length; i++){
		$("#"+propertiesArray[i]).addClass("has-error");
		$("#"+propertiesArray[i]+"Div").addClass("has-error");
	}
}

//Remove highlighting from all inputs
function clearHighlighting(suffix){
	if (typeof suffix === 'undefined'){
		suffix = '';
	}
	$("#error-messages" + suffix).hide();
	$("#success-messages" + suffix).hide();
	$(".text-danger").hide();

	$("div.form-group.has-error").removeClass('has-error');
	$(".has-error").removeClass('has-error');
}

function clearModalHighlighting(){
	$("#modal-error-messages").hide();
	$("#modal-success-messages").hide();

	$("div.form-group.has-error").removeClass('has-error');
	$(".has-error").removeClass('has-error');
}

function hasClass(ele,cls){
	return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function removeClass(ele,cls){
	if(hasClass(ele,cls)){
		var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
		ele.className=ele.className.replace(reg, '');
	}
}

// ------------ error & success message ------------

function closeAllMessages() {
	closeInfoMessages();
	closeErrorMessages();
}

function closeInfoMessages() {
	$("#message-info-div").hide();
	$(".has-error").removeClass('has-error');
}

function closeErrorMessages() {
	$("#message-error-div").hide();
}

function showInfoMessages(message) {
	$("#message-info-div").hide();
	$("#message-info").html(message);
}

function showErrorMessages(message) {
	$("#message-error-div").show();
	$("#message-error").html(message);
}

function highlightErrorInputDiv(propertiesArray){
	for (var i=0; i<propertiesArray.length; i++){
		var parent = $("#"+propertiesArray[i]).parent();
		
		while(parent.hasClass("select-wrap")) {
            if(parent.hasClass("find-parent")){
                parent = parent.parent().parent().parent();
            }else{
			parent = parent.parent();
            }
		}
		parent.addClass("has-error");
	}
}

function showError(data){
	var errorMessage = "";

	var errorsArray = []; // to prevent error duplicates
	for(var i=0; i<data.errors.length; i++){
		if (indexOf(errorsArray,data.errors[i]) == -1){
			errorsArray.push(data.errors[i]);
			if (errorMessage == ""){
				errorMessage = data.errors[i];
			}else{
				errorMessage += "<br/>" + data.errors[i];
			}
		}
	}
	if(errorMessage.length == 0) {
		errorMessage = "An unexpected error has occurred. Please contact customer service or try again at another time.";
	}

	showErrorMessages(errorMessage);
	if (typeof data.properties != 'undefined') {
		highlightErrorInputDiv(data.properties);
	}
}

function showErrorWithMessage(data, message){
	showErrorMessages(message);
	if (typeof data.properties != 'undefined') {
		highlightErrorInputDiv(data.properties);
	}
}

// ------------ ~ error & success message ~ ------------