var sortId = false;
var sortDate = false;
var sortPO = false;
var sortCS = false;
var sortShipment = false;
var sort = 'ORDER_DATE';
var lastSortAccend = false;
var sortOption = "DESC";
var sortFiledId = "#sort-date";

function resetSearch(){
	location.reload(true);
}

function searchPackingButton(){
	$("#is-search-button").val("true");
	$('#packingSearchStartIndex').val("0");
    if($.trim($('#autocompleteSearchCS').val())==''){
        $('#searchCS').val("");
    }
	searchPackingSlips();
	$("#is-search-button").val("false");
    $("#resetBtn").prop("disabled", false);
}

function searchPackingSlips(){
	
	// check for selected
	dataString = $('#packing-search').serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: loadPackingSlipsSuccess
	});

}

function onPackingListDisplayLoaded() {
    calculatePackingCurrentPage();
    loadPackingPagination();
}

function loadPackingSlipsSuccess(data){
	if (typeof data.errors == "undefined"){
		$("#packing-list").load("/account/gadgets/packing-list-display.jsp", function() {
            onPackingListDisplayLoaded();
			if (sortOption == "DESC") {
				$(sortFiledId + " i").attr("class", "fa fa-sort-down");
			} else {
				$(sortFiledId + " i").attr("class", "fa fa-sort-up");
			}
        });
	} else {
		$('#packing-list').html('');
		$('#pagination').empty();
		var billAddrRowCount= $("#selectedBillTo option").length;
		for (var i = 0; i <= data.errors.length; i++) {
			if (data.errors[i] != "Select One Option to Search Packing Slips."
					&& data.errors[i] != undefined) {
				if (billAddrRowCount == 2) {
					$('#packing-list')
							.append(
									"<div class='alert alert-danger'>"
											+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
											+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
											+ "</span></button>"
											+ "Please contact your admin to add Billing Account"
											+ "</div>");
				} else if (billAddrRowCount > 2) {
					$('#packing-list')
							.append(
									"<div class='alert alert-danger'>"
											+ "<button class='close' aria-label='Close' data-dismiss='alert' type='button'>"
											+ "<span aria-hidden='true'><span class='glyphicon glyphicon-remove-circle'></span>"
											+ "</span></button>"
											+ data.errors[i] + "</div>");
				}
				document.getElementById("reset-button").disabled = true;
				setTimeout('$("#dismiss-err_search_packingslip").click()', 2000);
				break;
			} else if (data.errors[i] == "Select One Option to Search Packing Slips.") {
				document.getElementById("resetBtn").disabled = true;
				$("#link-err_search_packingslip").click();
				setTimeout('$("#dismiss-err_search_order").click()', 2000);
				break;
			}

		}
	}
}

function changePackingPage(page) {

	var pageSize = $("#packingSearchPageSize").val();
    var startIndex = (page - 1) * pageSize;
	$("#packingSearchStartIndex").val(startIndex);
	searchPackingSlips();

}

function calculatePackingCurrentPage(){
	var startIndex = $("#packingSearchStartIndex").val();
	var pageSize = $("#packingSearchPageSize").val();
	var total = $("#total_count").val();
	var currentPage = (startIndex / pageSize | 0) + 1;
	$("#page").val(currentPage);
}

function loadPackingPagination(){
	var total = $("#total_count").val();
	var numPerPage = $("#packingSearchPageSize").val();
	var currentPage = $("#page").val();
	$("#pagination").load("/account/gadgets/packing-pagination.jsp?modal=true&page="+currentPage+"&numPerPage="+numPerPage+"&total="+total, function () {
		eventsPackingPagination();
    });
}


function selectOrg(org, id) {
    console.log('packing selected org = ' + org);
    $('.spinner-on-start').addClass('loader-active');
    $('.spinner-on-start .loading').show();
    var date = new Date();
    $(id).find('option').remove().end();
    $(id).load("/global/gadgets/cs-list.jsp?filter=&nocache=" + date.getTime() + "&orgId=" + org,  function(){
    	autoCompleteShipToAddress();
        $('.spinner-on-start').removeClass('loader-active');
        $('.spinner-on-start .loading').hide();

    },initScrollbar);
}

