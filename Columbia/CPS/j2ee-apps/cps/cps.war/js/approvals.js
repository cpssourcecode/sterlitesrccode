var sortId = false;
var sortName = false;
var sortRequestor = false;
var sortDate = true;
var sort = 'submittedDate';
var lastSortAccend = false;

function sortOrderId(approvals){
	resetSort();
	sortId = !sortId;
	if (sortId){
		$("#sort-id").addClass(" up");
	}else{
		$("#sort-id").addClass(" down");
	}
	lastSortAccend = !sortId;
	sort = 'webOrderId';

	if (approvals) {
		loadApprovals();
	} else {
		loadRequests();
	}
}

function sortOrderRequestor(approvals){
	resetSort();
	sortRequestor = !sortRequestor;
	if (sortRequestor){
		$("#sort-requestor").addClass(" up");
	}else{
		$("#sort-requestor").addClass(" down");
	}
	lastSortAccend = !sortRequestor;
	sort = 'requestor';

	if (approvals) {
		loadApprovals();
	} else {
		loadRequests();
	}
}

function sortOrderDate(approvals){
	resetSort();
	sortDate = !sortDate;
	if (sortDate){
		$("#sort-date").addClass(" up");
	}else{
		$("#sort-date").addClass(" down");
	}
	lastSortAccend = !sortDate;
	sort = 'submittedDate';

	if (approvals) {
		loadApprovals();
	} else {
		loadRequests();
	}
}

function sortProfileName(approvals){
	resetSort();
	sortName = !sortName;
	if (sortName){
		$("#sort-name").addClass(" up");
	}else{
		$("#sort-name").addClass(" down");
	}
	lastSortAccend = !sortName;
	sort = 'profileName';

	if (approvals) {
		loadApprovals();
	} else {
		loadRequests();
	}

}

function sortOrderSavedDate(){
	resetSort();
	sortDate = !sortDate;
	if (sortDate){
		$("#sort-date").addClass(" up");
	}else{
		$("#sort-date").addClass(" down");
	}
	lastSortAccend = !sortDate;
	sort = 'savedDate';

	loadSavedCarts();

}

function sortAddress(){
	resetSort();
	sortName = !sortName;
	if (sortName){
		$("#sort-address").addClass(" up");
	}else{
		$("#sort-address").addClass(" down");
	}
	lastSortAccend = !sortName;
	sort = 'address.address1';

	loadSavedCarts();

}

function onModalSavedCartLoaded(orderId){
    $(document).ready(function(){
        jQuery('.scrollbar-inner').scrollbar();
        if ($( window ).width() > 463){
            var orderId = $("#hiddenOrderId").val();
            $('#modalViewOrder'+orderId).on('shown.bs.modal', function() {
                var firstBlockHeight = $(".well-view-order-detail").innerHeight();
                var secondBlockHeight = $(".well-view-order-detail-summary").innerHeight();
                var maxHeight = Math.max(firstBlockHeight, secondBlockHeight);
                $(".well-view-order-detail").attr("style", "height: " + maxHeight + "px");
                $(".well-view-order-detail-summary").attr("style", "height: " + maxHeight + "px");
            })
        }

        $('#modalViewOrder' + orderId).on('shown.bs.modal', function (e) {

            if (isElementExist('.content')) {
                $.each($('.content'), function(index, val) {
                    $('.check img', val).each(function() {
                        if ($(this).height() > $(this).width()) {
                            $(this).css('height', 'auto');
                            $(this).css('width', 'auto');
                            $(this).css('max-height', '80px');
                        }
                    });
                });
            }
        });

        $("#saved-cart-tbl th").click(function () {
            sortOrderDetails(this);
        });
    });
}

function orderCartsDisplay(){
	var ordersCount = parseInt($("#savedCartsOrdersCount").val());
	if(ordersCount) {
        for (var i = 0; i < ordersCount; i++) {
        	var orderId = $("#savedCartsOrderId" + i).val();
			(function(orderId) {
                $.get("/global/modals/modal-saved-cart.jsp?orderId=" + orderId, function (data) {
                    $("body").append(data);
                    onModalSavedCartLoaded(orderId);
                });
            })(orderId)
        }
    }
    loadSavedCartsPagination();
}

var currentPage = 1;
function changeApprovalsPage(page) {
	currentPage = page;
	$("#order-list").load("/account/gadgets/order-approvals-display.jsp?sort=" + sort + "&accend=" + !lastSortAccend + "&page=" + currentPage, function() {
		loadApprovalsPagination();
		eventsOrderApprovalDisplay();
		initializeCheckboxes();
	});
}

function changeRequestsPage(page) {
	currentPage = page;
	$("#order-list").load("/account/gadgets/order-requests-display.jsp?sort=" + sort + "&accend=" + !lastSortAccend + "&page=" + currentPage, function () {
        loadRequestsPagination();
		eventsOrderRequestsDisplay();
    });
}

function changeSavedCartsPage(page) {
	currentPage = page;
	$("#order-list").load("/account/gadgets/order-carts-display.jsp?sort=" + sort + "&accend=" + !lastSortAccend + "&page=" + currentPage, function () {
        orderCartsDisplay();
		eventsOrderCartsDisplay();
    });
}

function resetSort (){
	$("#sort-id").removeClass("up down");
	$("#sort-name").removeClass("up down");
	$("#sort-date").removeClass("up down");
	$("#sort-address").removeClass("up down");
}

function loadRequests(){
	$("#order-list").load("/account/gadgets/order-requests-display.jsp?sort=" + sort + "&accend=" + !lastSortAccend, function () {
		loadRequestsPagination();
        eventsOrderRequestsDisplay();
    });
}

function loadSavedCarts(){
	$("#order-list").load("/account/gadgets/order-carts-display.jsp?sort=" + sort + "&accend=" + !lastSortAccend, function () {
		orderCartsDisplay();
		eventsOrderCartsDisplay();
    });
}

function loadApprovals(){
	$("#order-list").load("/account/gadgets/order-approvals-display.jsp?sort=" + sort + "&accend=" + !lastSortAccend, function() {
		loadApprovalsPagination();
		eventsOrderApprovalDisplay();
		initializeCheckboxes();
	});
}

function resetSearch(){
	location.reload(true);
}

function loadSavedCartsPagination(){
	var total = $("#total_count").val();
	var numPerPage = $("#numPerPage").val();
	var currentPage = $("#page").val();
	$("#pagination").load("/account/gadgets/saved-carts-pagination.jsp?page="+currentPage+"&numPerPage="+numPerPage+"&total="+total, function () {
		eventsSavedCartPagination();
    });
}

function loadApprovalsPagination(){
	var total = $("#total_count").val();
	var numPerPage = $("#numPerPage").val();
	var currentPage = $("#page").val();
	$("#pagination").load("/account/gadgets/approvals-pagination.jsp?page="+currentPage+"&numPerPage="+numPerPage+"&total="+total, function () {
		eventsApprovalPagination();
    });
}

function loadRequestsPagination(){
	var total = $("#total_count").val();
	var numPerPage = $("#numPerPage").val();
	var currentPage = $("#page").val();
	$("#pagination").load("/account/gadgets/requests-pagination.jsp?page="+currentPage+"&numPerPage="+numPerPage+"&total="+total, function () {
		eventsRequetsPagination();
    });
}

function showMoveToCartModalPopup(orderId) {
	$("#link-modalMoveToCart" + orderId).click();
}

function showViewCartModalPopup(orderId) {
	$("#link-modalSavedCart" + orderId).click();
}

function showDeleteCartModalPopup(orderId) {
	$("#link-modalDeleteCart" + orderId).click();
}

function showApproveOrderModalPopup(orderId) {
	$("#link-modalApproveSelectedPendingOrder" + orderId).click();
}

function showRejectOrderModalPopup(orderId) {
	$("#link-modalRejectOrder" + orderId).click();
}

function showResubmitOrderModalPopup(orderId) {
	$("#link-modalResubmitOrder" + orderId).click();
}

function showEditOrderModalPopup(orderId) {
	$("#link-modalEditOrder" + orderId).click();
}

function showDeleteOrderModalPopup(orderId) {
	$("#link-modalDeleteOrder" + orderId).click();
}

function openApproveOrdersModal() {
	var orderIds = gatherListChecked();
	if (orderIds.length == 0) {
		$("#approve-order-div").load("/global/modals/modal-approve-order-error-div.jsp", function() {
			$("#link-modalApproveSelectedPendingOrders").click();
		});
	} else {
		$("#approve-order-div").load("/global/modals/modal-approve-order-success-div.jsp", function() {
			eventsModalApproveOrdersSuccessDiv();
			$("#link-modalApproveSelectedPendingOrders").click();
		});
	}
}

function openRejectOrdersModal() {
	var orderIds = gatherListChecked();
	if (orderIds.length == 0) {
		$("#reject-order-div").load("/global/modals/modal-approve-order-error-div.jsp", function() {
			$("#link-modalRejectOrders").click();
		});
	} else {
		$("#reject-order-div").load("/global/modals/modal-reject-order-success-div.jsp", function() {
			eventsModalRejectOrdersSuccessDiv();
			$("#link-modalRejectOrders").click();
		});
	}
}

function approveOrder(orderId) {
	$("#approveOrderId").val(orderId);
	dataString = $("#approveOrder").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: approvalsFormSuccess,
		error: ajaxError
	});
}

function approveOrders() {

	var orderIds = gatherListChecked();
	$("#approveList").val(orderIds);

	dataString = $("#approveOrders").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: approvalsFormSuccess,
		error: ajaxError
	});
}

function rejectOrder(orderId) {
	$("#rejectOrderId").val(orderId);
    $("#rejectApproverMessage").val($("#reject-approver-message").val());

	dataString = $("#rejectOrder").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: approvalsFormSuccess,
		error: ajaxError
	});
}

function rejectOrders() {

	$("#rejectMessage").val( $("#rejectMessTA").val() );

	var orderIds = gatherListChecked();
	$("#rejectList").val(orderIds);
	dataString = $("#rejectOrders").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: approvalsFormSuccess,
		error: ajaxError
	});
}

function resubmitOrder(orderId) {
	$("#resubmitOrderId").val(orderId);
	dataString = $("#resubmitOrder").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: requestsFormSuccess,
		error: ajaxError
	});
}

function deleteOrder(orderId) {
	$("#deleteOrderId").val(orderId);
	dataString = $("#deleteOrder").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: requestsFormSuccess,
		error: ajaxError
	});
}

function editOrder(orderId) {
	$("#editOrderId").val(orderId);
	dataString = $("#editOrder").serialize();
	$.ajax({
		type: "POST",
		data: dataString,
		dataType: "json",
		success: editFormSuccess,
		error: ajaxError
	});
}

function approvalsFormSuccess(data) {
	if (typeof data.errors == "undefined") {
		//loadApprovals();
		location.reload(true);
	}
}

function requestsFormSuccess(data) {
	if (typeof data.errors == "undefined") {
		//loadRequests();
		location.reload(true);
	}
}

function editFormSuccess(data) {
	if (typeof data.errors == "undefined") {
		window.location.href="/checkout/cart.jsp";
	}
}

