$(document).ready(function() {
	var checkboxes = $("input[type='checkbox']");
	var submitButt = $("#price-book-section");
	checkboxes.click(function() {
		submitButt.attr("disabled", !checkboxes.is(":checked"));
	});
	$("#priceBookAll").click(function() {
		if (this.checked) {
			$(':checkbox').each(function() {
				this.checked = true;
			});
		} else {
			submitButt.attr("disabled", true);
			$(':checkbox').each(function() {
				this.checked = false;
			});
		}
	});
	$("#price-book-section").click(function() {
		var result = [];
		$('input[name="priceBook"]:checked').each(function() {
			result.push($(this).data('value'));
		});
		downloadFilesInZip(result);
	});
	$('input[name="priceBook"]').click(function () {
	     if ($('input[name="priceBook"]:checked').length == $('input[name="priceBook"]').length) {
	       $("#priceBookAll").prop("checked", true);
	    }else{
	      $("#priceBookAll").prop("checked", false);
	}  });
});
function downloadFilesInZip(files) {
	var downloadUrl = '/content/zip/download-zip.jsp?contentUrls='
			+ files.join(',') + '&zipName=result.zip';
	var down = document.createElement("a");
	var a = document.createElement('a');
	a.href = downloadUrl; // Set the file name.
	a.style.display = 'none';
	document.body.appendChild(a);
	a.click();
	delete a;
}
