$(document).on("click", ".viewAllOptionsModal", function(e) {
	var container = $("#" + $("#current-modal").val() + " .modal-content");
	if (!container.is(e.target) && container.has(e.target).length === 0) {
		updateNavigationOnModalClose($("#current-url").val());
	}
});

function getLocation() {
	return location.pathname + location.search;
}

var currentLocation = getLocation();

function handleManualPushedState(e) {
	var newLocation = getLocation();
	if (newLocation != currentLocation) {
		document.location.reload();
	}
}
//commented for SM-304
//$(document).ready(function() {
//	window.addEventListener('popstate', handleManualPushedState, false);
//});


function ejectTitle(dataElem) {
    var title;
    if (dataElem.context.localName === 'h5') {
        title = dataElem.context.innerText;
    } else if (dataElem.context.localName === 'a') {
        title = dataElem.context.lastElementChild.innerText;
    } else {
        title = dataElem.context.alt;
    }
    return title;
}

function setTitle(title) {
    if (title) {
        title = title.trim();
        document.title = title + " - Columbia Pipe & Supply Co.";
    }
}

function updateMeta(metaDescription, metaKeywords, title) {
    setTitle(title);

    $('meta[name=description]').remove();
	$('meta[name=keywords]').remove();
	$('head').append(
			'<meta name="description" content="' + metaDescription + '">');
	$('head').append('<meta name="keywords" content="' + metaKeywords + '">');
}

var resultListView = 'list';

function changeView(view) {
	resultListView = view;
}

function onResultListLoaded(){
//	$(document).ready(function(){
		$('.gridView').hide();
        $('.listView').show();
        $('.plp-view a').click(function(e) {
            e.stopPropagation();
            if ($(this).hasClass('plp-view-grid')) {
                $('.item-plp').removeClass('item-list').addClass('item-grid');
                $('.listView').hide();
                $('.gridView').show();
            } else {
                $('.item-plp').removeClass('item-grid').addClass('item-list');
                $('.gridView').hide();
                $('.listView').show();
            }
        });

        if($("#isZeroResults").val() == 'true'){
            $("#sidebar").attr("style", "display: none");
            $("#showing-total-num-results").attr("style", "display: none");
            $("#mainContent").attr("class","col-sm-12");
		}

        $('.preload-cover').removeClass('loader-active');
        $('.preload-cover .loading').hide();

        var isAvailablePricesWebService = $("#isAvailablePricesWebService").val();
        priceAvailableMsg(isAvailablePricesWebService);

        loadCompareItemsCount();
        //loadCompareItemsCountProduct();

        $(".cat-image").one("load", function() {
			equalizeCategory();
		}).each(function() {
			if(this.complete){
				$(this).load();
			}
		});
  //  });
}

function navigate(link, isSorting, refinementItem, skipClearContent) {
//    if (!skipClearContent) {
//		$('#pageContent').html('<div class="loading">\
//									<i class="fa fa-spinner fa-spin"></i>\
//								</div>');
//	}
//	$('#spinner-on-start').addClass('loader-active');
//    $('#spinner-on-start .loading').show();
    
	var isShowMoreActive = false;
	if ($(refinementItem).length != 0) {
		if ($(refinementItem).parents('ul.facet-refine').length != 0) {
			var showHideMore = $(refinementItem).parents('ul.facet-refine').find('li.facet-toggle a');
			if ($(showHideMore).hasClass('active')) {
				isShowMoreActive = true;
			}
		}
	}
	// IE11 does not support startsWith
	String.prototype.startsWith = function(searchString, position) {
		position = position || 0;
		return this.indexOf(searchString, position) === position;
	};

	if (link.startsWith('?')) {
		if (window.location.pathname.startsWith('/search')) {
			link = "/search" + link;
		} else if (!isSorting) {
			link = "/browse" + link;
		}
	}
	window.scrollTo(0,0);
	partialReload(link, "inner", "pageContent", function() {
        clearSelection();
        onResultListLoaded();
        eventsResultList();
        $('ul.facet-refine li:nth-child(6)').nextAll().addClass('facet-overflow').hide().parent('ul')
			.append('<li class="facet-toggle"><a>Show <span class="facet-toggle-more">More</span><span class="facet-toggle-less">Less</span></a></li>');

		var showHideMore = $('ul.facet-refine').find('li.facet-toggle a');
		if (isShowMoreActive) {
//			$(showHideMore).addClass('active');
		} else {
			$(showHideMore).removeClass('active').parent('li').siblings('.facet-overflow').slideUp();
		}
		alignResultTails();
		$(".modal-backdrop.fade.in").remove();
		$("body.modal-open").removeClass("modal-open");
		if (resultListView == "grid") {
			$('.item-plp').removeClass('item-list').addClass('item-grid');
		} else {
			$('.item-plp').removeClass('item-grid').addClass('item-list');
		}
		refreshFacets();
       /*
        *   fix  SM-539 Internet Explorer issues
          keypressEventHandler('cartridges15', function(event){
            formSubmitOnEnter(event, 'handleSecondarySearch');
       });*/
        setTimeout(imageLazyLoad(),3000);
    });

	return false;
}

function updateNavigationOnModalClose(link) {
	partialReload(link, "inner", "pageContent", function() {
        onResultListLoaded();
        eventsResultList();
        alignResultTails();
		openFacet();
		$(".modal-backdrop.fade.in").remove();
		$("body.modal-open").removeClass("modal-open");
	});
	return false;
}

// secondary search
function handleSecondarySearch() {

	if ($('#secondarySearchQuestion').val().length > 0) {
		var url = window.location.href;
		// console.log("url: "+url);
		var questionValue = $("#secondarySearchQuestion").val();
		var question=$("#question").val();
		var multipleMessage=$("#multipleMessage").val();
        var singleMessage=$("#singleMessage").val();
        var tags = []
        $('*#refinementQuestion').each(function () {
        	tags.push($(this).text());
        });
        var refinement=tags.join(',');
     	// set text prior to search
        var searchURL="/xhr/productSearchReultList.jsp?noResult=true&Ntt="+encodeURIComponent(questionValue);
        $("#noResultsModal_noResultsFound #modal-matnone-title").html("<span id='noResultsModalAddText_noResultsFound'></span>");
        if ($('#question').val() != ''){            
            if(refinement==''){
    		    $("#noResultsModalAddText_noResultsFound").load(searchURL);
                $("#noResultsModalAddText_noResultsFound").before(multipleMessage+' "' + questionValue +','+question+'".');
            }else{
    		    $("#noResultsModalAddText_noResultsFound").load(searchURL);
    		    $("#noResultsModalAddText_noResultsFound").before(multipleMessage+' "' + questionValue +','+question+','+refinement+'".');
            }
        }else{           
            if(refinement==''){
    		    $("#noResultsModalAddText_noResultsFound").load(searchURL);
                $("#noResultsModalAddText_noResultsFound").before(singleMessage+' "' + questionValue +','+question+'".');
            }else{
    		    $("#noResultsModalAddText_noResultsFound").load(searchURL);
                $("#noResultsModalAddText_noResultsFound").before(multipleMessage+' "' + questionValue +','+refinement+'".');
            }
        }
		// console.log("QuestionValue: "+questionValue);
		var navState = $("#secondarySearchHidden").val();
		// console.log("navState: "+navState);
		$('#secondarySearchQuestion').val("");
		var link = null;
		if (navState.indexOf(questionValue) == -1) {
			var newSearchTerm = questionValue + "|" + navState;
			var link = updateURLSearchParam(url, newSearchTerm);
		} else {
			$('#secondarySearchQuestion').val('');
		}

        link = updateURLParameter(link, "Ssp", 'true');
        var indexNo = link.indexOf('No');
		if (indexNo > 0) {
			var startLink = link.substring(0, indexNo);
			var endLink = link.substring(indexNo);
			link = startLink + endLink.substring(endLink.indexOf('&') + 1);
		}
      // setTimeout(loadMessage(),1000);
		// console.log("link: "+link);
		navigate(link, null, null, true);
	} else {
		// show modal
		$("#link-noResultsModal_noSearchTerm").click();
	}

}
function loadMessage(){
    $("#seconday_search_info").html(" ");
	var isFirstSearch=$("#isFirstSearch").val();
	if(isFirstSearch == "true"){
	  	$('.searchQuestion').each(function () {
		    if(($(this).text()).trim()){
			    var resultSearchUrl="/xhr/productSearchReultList.jsp?Ntt="+encodeURIComponent($(this).text());
		      		 $.ajax({
                        url: resultSearchUrl,
                        success: function(data) {
                        $("#seconday_search_info").append(data);
                        }
                        });             
			        }
	            });
	       }
	
        onResultListLoaded();
        eventsResultList();
        keypressEventHandler('cartridges15', function(event){
            formSubmitOnEnter(event, 'handleSecondarySearch');
       });
     }

function loadHeaderMessage(resultSearchUrl){
	 $.ajax({
	      url: resultSearchUrl,
	      success: function(data) {
	        $("#seconday_search_info").append(data);         
	      }
	    });
}
function removeSecondarySearch(searchToRemove) {
	var url = window.location.href;
	url = removeURLParameter(url, "Ssp");

	// console.log("URL: "+url);
	var currentSearchTerm = $("#secondarySearchHidden").val();
	// console.log("CurrentSearchTerm: "+currentSearchTerm);
	var tempArray = currentSearchTerm.split("|");
	var newSearchTerm = "";
	// console.log("SearchToRemove: "+searchToRemove);
	for (i = 0; i < tempArray.length; i++) {
		// console.log("SearchTerm: "+tempArray[i]);
		if (tempArray[i] != searchToRemove) {
			// console.log("Don't remove");
			if (newSearchTerm == "") {
				newSearchTerm = tempArray[i];
			} else {
				newSearchTerm = newSearchTerm + "|" + tempArray[i];
			}
		}
	}

	var link = updateURLSearchParam(url, newSearchTerm);
	// console.log("Return: "+link);
	return link;
}

function updateURLSearchParam(url, searchTerm) {
	return updateURLParameter(url, "Ntt", searchTerm);
}

function updateURLParameter(url, param, paramVal) {
	var TheAnchor = null;
	var newAdditionalURL = "";
	var tempArray = url.split("?");
	var baseURL = tempArray[0];
	var additionalURL = tempArray[1];
	var temp = "";

	if (additionalURL) {
		var tmpAnchor = additionalURL.split("#");
		var TheParams = tmpAnchor[0];
		TheAnchor = tmpAnchor[1];
		if (TheAnchor) {
			additionalURL = TheParams;
		}

		tempArray = additionalURL.split("&");

		for (i = 0; i < tempArray.length; i++) {
			if (tempArray[i].split('=')[0] != param) {
				newAdditionalURL += temp + tempArray[i];
				temp = "&";
			}
		}
	} else {
		var tmpAnchor = baseURL.split("#");
		var TheParams = tmpAnchor[0];
		TheAnchor = tmpAnchor[1];

		if (TheParams)
			baseURL = TheParams;
	}

	if (TheAnchor)
		paramVal += "#" + TheAnchor;

	var rows_txt = temp + "" + param + "=" + paramVal;
	return baseURL + "?" + newAdditionalURL + rows_txt;
}

// Compare

function reloadCompareArea() {
	var comparePanel = $("#comparePanel");
	if (comparePanel) {
		comparePanel.load("/catalog/gadgets/compare-panel.jsp", function () {
			eventsComparePanel();
        });
	}
}

function changeCompare(productId, categoryId) {
	var checkbox = $("#cmpr-" + productId);
	if (checkbox) {
		startLoader();
		if (checkbox.prop("checked")) {
			handleAddProductToCompare(productId, categoryId);
		} else {
			handleRemoveProductFromCompare(productId, categoryId);
		}
		stopLoader();
	}
}

function enableInputWithValue(inputId, inputValue) {
	if (inputValue) {
		$("#" + inputId).prop("disabled", false);
		$("#" + inputId).val(inputValue);
	}
}

function changeCompareCheckbox(productId, checked) {
	var checkbox = $("#cmpr-" + productId);
	if (checkbox) {
		if (typeof checked == "undefined") {
			checked = !checkbox.prop("checked");
		}
		// checkbox.prop("checked", checked);
		checkbox.checkbox(checked ? "check" : "uncheck");
	}
}

function compareItemList(obj) {
    // loadCompareItemsCountProduct();
	var prodId = $(obj).prop("value");
	if ($('#compare-item-number').val() > 2 && (obj.checked === true)) {
		$('#biggerCompareItemNumber').modal('show');
		obj.checked = false;
		return;
	}
	if ($(obj).is(':checked')) {
        handleAddProductToCompare(prodId);
		// $(obj).removeAttr('checked','checked');
	} else {
        handleRemoveProductFromCompare(prodId);
	}
}

function loadCompareItemsCount() {
    loadCompareItemsCountProduct();
	var url = "/catalog/gadgets/request-compare-items-count.jsp";
	$.ajax({
		type : "GET",
		url : url,
		dataType : 'html',
		success : function(data) {
			data = $.trim(data);
			$('#compareItemsCount').html(data);
            eventProductComparison();
        }
	});
}

function loadCompareItemsCountProduct() {
    var url = "/catalog/gadgets/request-compare-items-catalog.jsp";
    $.ajax({
        type : "GET",
        url : url,
        dataType : 'html',
        success : function(data) {
            data = $.trim(data);
            var id = '';
            $('a[id^="compareItemsCountProduct_"]').each(function () {
                id = '#' + this.id;
                $(id).html(data);
            });
            eventProductComparison();
        }
    });
}

function handleAddProductToCompare(productId) {
	$("#addCompareProdId").val(productId);
	$("#comparisonReferer").val(window.currentLocation);
	// enableInputWithValue("addCompareCatId", categoryId);
	var dataString = $('#addToCompareForm').serialize();
	$.ajax({
		url : '/',
		type : "POST",
		data : dataString,
		dataType : "json",
		success : function(response) {
			loadCompareItemsCount();
		},
		error : function(response) {
			showGenericError(response);
		}
	});
}

function handleRemoveProductFromCompare(productId, categoryId) {
	$("#removeCompareProdId").val(productId);
	// enableInputWithValue("removeCompareCatId", categoryId);
	var dataString = $('#removeFromCompareForm').serialize();
	$.ajax({
		url : '/',
		type : "POST",
		data : dataString,
		dataType : "json",
		success : function(response) {
			loadCompareItemsCount();
		},
		error : function(response) {
			showGenericError(response);
		}
	});
}

function clearFromCompare() {
	var dataString = $('#clearFromCompareForm').serialize();
	$.ajax({
		url : '/',
		type : "POST",
		data : dataString,
		dataType : "json",
		success : function(response) {
			loadCompareItemsCount();
			$('input:checkbox').removeAttr('checked', 'checked');
		},
		error : function(response) {
			showGenericError(response);
		}
	});
	return false;
}

function addParamToResponseFunction(handleResponseFunction, parameter) {
	return function(data) {
		handleResponseFunction.call(null, data, parameter);
	};
}

function handleAddProductToCompareResponse(data, productId) {
	if (isError(data)) {
		changeCompareCheckbox(productId, false);
		showErrors(data);
		scrollToTop();
	} else if (isSuccess(data)) {
		reloadCompareArea();
		changeCompareCheckbox(productId, true);
	}
}

function handleRemoveProductFromCompareResponse(data, productId) {
	if (isError(data)) {
		changeCompareCheckbox(productId, true);
		showErrors(data);
		scrollToTop();
	} else if (isSuccess(data)) {
		reloadCompareArea();
		changeCompareCheckbox(productId, false);
	}
}

function handleClearCompareResponse(data) {
	if (isError(data)) {
		showErrors(data);
		scrollToTop();
	} else if (isSuccess(data)) {
		reloadCompareArea();
		$('.compare-checkbox-label').each(function() {
			$(this).checkbox('uncheck');
			$(this).find('.compare-checkbox-plp').prop('checked', false);
		});
	}
}

function handleGoToCompare() {
	return submitAjaxForm("goToCompareForm", handleGoToCompareResponse);
}

function handleRemoveProductAndGoToCompare(productId, categoryId) {
    $("#removeCompareProdId").val(productId);
    enableInputWithValue("removeCompareCatId", categoryId);
    // var respFunc = handleRemoveProductFromCompareOnCompareResponse;
    return submitAjaxForm("removeFromCompareForm", loadCompareItemsCountForRemove);
}

function loadCompareItemsCountForRemove() {
    var url = "/catalog/gadgets/compare-content.jsp";
    $.ajax({
        type : "GET",
        url : url,
        dataType : 'html',
        success : function(data) {
            data = $.trim(data);
            $('#compare-content').html(data);
            eventsCompareContent();
            onCompareContentLoaded();
        }
    });
}

function handleGoToCompareResponse(data) {
	defaultAjaxFormResponseRedirectOnSuccess(data);
}

function continueShopping() {
	window.history.back();
	return false;
}

function changeCompareMode(pRadioNode) {
	var classToHighliht = pRadioNode.value;
	$(".table-compare td").removeClass("accent");
	$(".table-compare td." + classToHighliht).addClass("accent");
}

function handleRemoveProductFromCompareOnCompare(productId, categoryId) {
	$("#removeCompareProdId").val(productId);
	enableInputWithValue("removeCompareCatId", categoryId);
	var respFunc = handleRemoveProductFromCompareOnCompareResponse;
	return submitAjaxForm("removeFromCompareForm", respFunc);
}

function reloadCompareTable() {
	$("#compare-container").load("/catalog/gadgets/compare-table.jsp",
			alignCompareHead);
}

function handleRemoveProductFromCompareOnCompareResponse(data, productId) {
	if (isError(data)) {
		showErrors(data);
		scrollToTop();
	} else if (isSuccess(data)) {
		reloadCompareTable();
	}
}

function alignResultTails() {
	var row = $('.results-list');
	$.each(row, function() {
		var maxh = 0;
		$.each($(this).find('.prod-title'), function() {
			if ($(this).height() > maxh)
				maxh = $(this).height();
		});
		$.each($(this).find('.prod-title'), function() {
			$(this).height(maxh);
		});
	});
}

function alignCompareHead() {
	var maxHeight = 0;
	$(".compare-product").each(function() {
		if ($(this).height() > maxHeight) {
			maxHeight = $(this).height();
		}
	});
	$(".compare-product").height(maxHeight);
	if ($(window).width() <= 768) {
		$(".compare-product").height(220);
	} else {
		$(".compare-product").height(maxHeight);
	}
	$(".compare-description .large").css({
		'height' : (0 + $(".compare-product").height() + 'px')
	});
}

function handleSendContactUs() {
	startLoader();
	$("#subtopicHidden").val($("#subtopic").val());
	$("#topicHidden").val($("#topic").val());
	return submitAjaxForm("contactUsForm", handleSendContactUsResponse);
}

function handleSendContactUsResponse(data) {
	defaultAjaxFormResponseMsgOnSuccess(data);
}

$(".btn-quick-view")
		.click(
				function(event) {
					event.preventDefault();
					$("body").find(".quick-view-content").remove();
					$("body").find(".quick-view-content-carousel").remove();
					$parentCover = $(this).parent().parent();
					$parentCoverMobile = $(this).parent();

					if ($(window).width() <= 992) {
						$($parentCoverMobile)
								.after(
										"<div class='quick-view-content row' id='quick-view-href'></div>");
					} else {
						$($parentCover)
								.after(
										"<div class='quick-view-content row' id='quick-view-href'></div>");
					}
					;
					// console.log($parentCoverMobile);

					var id = $(this).attr('id');
					$
							.ajax({
								url : "/catalog/gadgets/quick-view-content.jsp?productId="
										+ id,
								context : document.body,
								success : function(result) {
									$(".quick-view-content").html(result);
									$(".quick-view-close-action").click(
											function() {
												$(this).closest(
														".quick-view-content")
														.remove();
											});
									// Tab initialization
									$('#myTab a').click(function(e) {
										e.preventDefault();
										$(this).tab('show');
									});
								}
							});
				});

function selectSku(element, prodId) {
	var value = $(element).val();
	// $("#pdpPrice").load('/catalog/gadgets/pdp-pricing.jsp?sku='+value+"&product="+prodId,
	// function() {reloadPdpPrice(prodId, value);});
	reloadPdpPrice(prodId, value);
	reloadAvailability(value);
	$("#product_sku_id").html("Item: " + value);
}

function reloadPdpPrice(productId, skuId) {
	var qty = $("#itemQty").val();
	var params = {
		productId : productId,
		skuId : skuId,
		qty : qty
	};
	$("#pdpPrice").load("/catalog/gadgets/pdp-pricing.jsp", params);
}

function reloadAvailability(skuId) {
	var qty = $("#itemQty").val();
	var params = {
		skuId : skuId,
		quantity : qty
	};
	$("#pdp-availability")
			.load("/catalog/gadgets/pdp-availability.jsp", params);
}

function handleAddToCart() {
	$('#itemSkuIdHid').val($('#itemSkuId').val());
	$('#itemQtyHid').val($('#itemQty').val());
	return submitAjaxForm("addToCartForm", handleAddToCartResponse);
}

function handleAddToCartResponse(data) {
	if (isError(data)) {
		// showErrorsInCustomDIV(data, "errorModal");
		showErrors(data);
	} else if (isSuccess(data)) {
		closeInfoMessages();
		reloadMinicart();
		// showAddedProductConfirmation($('#itemProdIdHid').val(),
		// $('#itemSkuIdHid').val(), $('#itemQtyHid').val());
	}
}

// Add to cart on Home page "You May Be Interested In" block

function addToCartSpotlight(addToCartFormName, prodId, transient) {
	var qty = $("#quantity" + prodId).val();
	$("#quantityBean" + prodId).val(qty);
	var options = {
		type : "POST",
		dataType : "json",
		success : function(data) {
			addToCartSpotlightResponse(data, 'false', transient);
		},
		error : function() {
		}
	};
	var form = $("#" + addToCartFormName);
	form.ajaxSubmit(options);
}

function addToCartSpotlightResponse(data, isModal, transient) {
	if (transient == "true") {
		location.href = "/";
	} else {
		location.href = "/checkout/cart.jsp?qo=t";
	}

	/*
	 * if (isError(data)) { showErrorMessages(data, isModal); } else if
	 * (isSuccess(data)) { reloadAndExpandMinicart(); if ('true' == isModal) {
	 * $("#containerModalLarge").modal('hide'); } setTimeout(function(){
	 * minicartContract(); }, 2000 ); reloadCartContent(); }
	 */

}

function equalizeCategory() {
	var equalRowCategory = $('.equalize-category');
	$.each(equalRowCategory, function() {
		var maxhEqCat = 0;
		$.each($(this).find('div[class^="col-"] .thumbnail'), function() {
			var imageHeight = $(this).find(".image").height();
			var textHeight = $(this).find(".prod-title").height();
			if (imageHeight + textHeight > maxhEqCat) {
				maxhEqCat = imageHeight + textHeight;
			}
		});
		$.each($(this).find('div[class^="col-"] .thumbnail'), function() {
			$(this).height(maxhEqCat + 10);
		});
	});
}

function changeViewAllModalPage(modalKey, newSelected) {
	var oldSelected = $("#active_tab_" + modalKey).val();
	$("#active_tab_" + modalKey).val(newSelected);
	$("#tab_" + modalKey + "_" + newSelected).attr("style", "");
	$("#tab_" + modalKey + "_" + oldSelected).attr("style", "display: none");
	$("#link_" + modalKey + "_" + newSelected).addClass("active");
	$("#link_" + modalKey + "_" + oldSelected).removeClass("active");
}

function selectCurrentModal(id) {
	$("#current-modal").val(id);
	$("#" + id + " .view-all-abc-pagination").attr("style",
			"display: inline-block");
}

function toggleRefinementOnModal(dimvalId, id) {
	var currentSelected = $("#" + id).val();
	if (currentSelected.indexOf(dimvalId) != -1) {
		currentSelected = currentSelected.replace(dimvalId + "###", "");
	} else {
		currentSelected = currentSelected + dimvalId + "###"
	}
	$("#" + id).val(currentSelected);

}

function selectSingleOnModal(uri, id) {
	$("#select-single-button-" + id)
			.attr("onclick", "navigate('" + uri + "');");
}

function saveOpenFacet(id) {
	$("#opened_facet").val(id);
}

function openFacet() {
	var id = $("#opened_facet").val();
	$("#ref-" + id).addClass("in");
	$("#ref-" + id).attr("style", "");
	$("#heading-link-" + id).attr("aria-expanded", "true");
	$("#heading-link-" + id).removeClass("collapsed");
}

function saveMultiSelections() {

	closeAllMessages();
	$("#add_dimvals_hidden").val($("#add-dimval-ids").val());
	$("#remove_dimvals_hidden").val($("#remove-dimval-ids").val());
	// $("#add_dimvals_single_hidden").val($("#add-dimval-ids-single").val());
	var dataString = $('#nav-state-form').serialize();

	$.ajax({
		type : "POST",
		data : dataString,
		dataType : 'json',
		success : function(response) {
			if ('success' == response.code) {
				saveMultiSelections_SubmitSuccess(response);
			} else {
				saveMultiSelections_SubmitError(response);
			}

		},
		error : function(response, textStatus, errorThrown) {
			ajaxErrorResponse();
		}
	});
}

function saveMultiSelections_SubmitSuccess(response) {
	navigate(response.navState);
}

function saveMultiSelections_SubmitError(response) {
	showError(response);
}

function getAvailability(eCommerceDisplay, skuId, td) {
	var cell = td;
	var url = "/checkout/gadgets/check-availability.jsp?s=" + skuId + "&e="
			+ eCommerceDisplay;
	$.ajax({
		type : "GET",
		url : url,
		dataType : 'html',
		success : function(data) {
			data = $.trim(data);
			cell.innerHTML = data;
		}
	});
}

function refreshFacets() {
	$('.expand-item .expand-label:not(.active)').siblings('.expand-content').hide();
	$('.expand-content').siblings('.expand-item .expand-label').append('<span class="expand-control"><i class="fa fa-angle-down"></i><i class="fa fa-angle-right"></i></span>').click(function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active').siblings('.expand-content').slideUp();
		} else {
			$(this).addClass('active').siblings('.expand-content').slideDown();
		}
	});
}

function onCompareContentLoaded(){
    $('td[id^="price_"]')
        .each(
            function () {
                var id = this.id;
                var product_id = id.split("_")[1];
                var url = "/catalog/gadgets/request-product-price.jsp?s="
                    + product_id;
                var ths = this;
                $.ajax({
                    type: "GET",
                    url: url,
                    dataType: 'html',
                    success: function (data) {
                        data = $.trim(data);
                        ths.innerHTML = data;
                    }
                });

            });
    $('td[id^="avlbt_"]').each(function () {
        var id = this.id;
        var product_id = id.split("_")[1];
        getAvailability(true, product_id, this);
    });

    var rows = $('#table-compare-products tbody tr').get();

    $.each(rows, function(index, row) {
        var displayRow = false;
        var cellText = '';
        $(row).find('td').each(function(index) {
            if (index > 0) {
                cellText = $(this).html().trim();
                if (cellText.length > 0 && cellText !== '&nbsp;') {
                    displayRow = true;
                }
                if (this.id.split('_')[0] === 'price' || this.id.split('_')[0] === 'avlbt') {
                    displayRow = true;
                }
            }
        });
        if (!displayRow) {
            $(row).remove();
        }
    });

    rows = $('#table-compare-products tbody tr').get();
    var k = 1;
    $.each(rows, function(index, row) {
        if (index > 0) {
            if ((k % 2) !== 0) {
                $(row).css('background-color', '#e6e9ea;');
            }
            k++;
        }
    });
    
    eventsFallbackImage();
}

//Need for IE
if (!String.prototype.startsWith) {
    String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}

function isLastOptionCleared(URL) {
    return URL.startsWith("?Nr=AND");
}

function compareProducts() {
    $("#comparisonReferer").val(window.location.pathname + window.location.search);

    var dataString = $('#compareProductHandler').serialize();
    $.ajax({
        type : "POST",
        data : dataString,
        dataType : 'json',
        success : function() {
            window.location.pathname = "/catalog/compare.jsp";
        },
    });
}

function quickOrderPadUploadFile() {
	var isCCAccountAccepted = $('#isAcceptedCCPayment').val();
	var isCCNotificationShown = $('#isCCNotificationShown').val();
	if(isCCAccountAccepted=='false' && isCCNotificationShown=='true'){
		$('#ccAccountNotificationModal').modal('show');
	} else {
    $('#qorderUploadDropdown').ajaxSubmit({
        cache: false,
        headers: {
            "Ajax-File-Upload": "true",
            "X-No-Redirect": "true"
        },
        success: qoPadFileUploadSuccess
    });
	}
}