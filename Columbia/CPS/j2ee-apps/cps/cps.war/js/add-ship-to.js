function searchAddresses(){
	var userId = $("#currentUserId").val();
	var companyId = $("#company").val();
	var searchTerm = $("#searchTerm").val();
	loadSearch(searchTerm, userId, companyId);
    $("#resetBtn").prop("disabled", false);
}

function resetSearch(){
	var userId = $("#currentUserId").val();
	var companyId = $("#company").val();
	$("#searchTerm").val('');
	loadSearch('', userId, companyId);
    $("#resetBtn").prop("disabled", true);
}

function loadSearch(st, uid, cid){
	var url = "/global/modals/gadgets/add-ship-to-list.jsp";
	var params = "searchTerm="+encodeURIComponent(st)+"&userId="+encodeURIComponent(uid)+"&companyId="+encodeURIComponent(cid);
	$("#shippingList").load(url+"?"+params, function () {
        eventsAddShipToList();
    });
}

function selectAll(check){
	console.log("Select All");
	var elem = document.getElementsByTagName('input');
	var checkboxes = new Array();
	for(i=0, iarr=0; i<elem.length; i++){
		att = elem[i].getAttribute("name");
		if (att == 'modal-check'){
			checkboxes[iarr] = elem[i];
			iarr++;
		}
	}
	for(var i in checkboxes) {
		if ( checkboxes[i].checked != check.checked ) {
			checkboxes[i].click();
			checkboxes[i].checked = check.checked;
			// if (check.checked){
			// 	checkboxes[i].parentNode.setAttribute('class', 'checkbox-custom checked');
			// 	checkboxes[i].parentNode.parentNode.setAttribute('class', 'checkbox checked');
			// } else {
			// 	checkboxes[i].parentNode.setAttribute('class', 'checkbox-custom');
			// 	checkboxes[i].parentNode.parentNode.setAttribute('class', 'checkbox');
			// }
		}
	}
}

function gatherChecked(){
	itemIds = [];
	var elem = document.getElementsByTagName('input');
	var inputs = new Array();
	for(i=0, iarr=0; i<elem.length; i++){
		att = elem[i].getAttribute("name");
		if (att == 'modal-check'){
			inputs[iarr] = elem[i];
			iarr++;
		}
	}

	for (var i = inputs.length -1 ; i>= 0; i--)
		if (inputs[i].type === "checkbox" && inputs[i].checked)
			itemIds.push(inputs[i].value);

	return itemIds;
}