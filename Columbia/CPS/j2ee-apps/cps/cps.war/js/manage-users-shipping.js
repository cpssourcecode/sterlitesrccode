$(document).ready(function() {
	loadUsersShipping($("#page-type").val(), $("#company").val(), $("#currentUserId").val(), "", "", "", "", $("#billingAccounts").val());
});

function removeShipping(removeId) {
	loadUsersShipping($("#page-type").val(), $("#company").val(), $("#currentUserId").val(), "", "", removeId, $("#searchText").val(), $("#billingAccounts").val());
}

function defaultShipping(defaultId) {
	showShipToPopup("Change Shipping Address",defaultId);
}

function loadUsersShipping(pageType, company, user, addressList, defaultId, removeId, searchText, companyIds) {
    searchText = searchText == null ? "" : searchText;
	var d = new Date();
	var t = d.getTime();
	var url = "/account/gadgets/manage-add-user-shipping.jsp";
	var params = "companyId="+encodeURIComponent(company)+"&userId="+encodeURIComponent(user)
			+"&addressList="+encodeURIComponent(addressList)+"&defaultId="+encodeURIComponent(defaultId)
			+"&removeId="+encodeURIComponent(removeId)+"&pageType="+encodeURIComponent(pageType)
			+"&searchText="+encodeURIComponent(searchText);
	if (companyIds != null && companyIds != undefined) {
		params = params +"&companyIds="+encodeURIComponent(companyIds);
	}

	$("#manageUsersShipping").load(url+"?"+params+"&t="+t, function () {
		eventsManageAddUserShipping();
		$(".modal-backdrop.fade.in").remove();
		$("body.modal-open").removeClass("modal-open");
		$("#addressAdminConfirm").on('hide.bs.modal', function () {
		        $("input[name=radioEx11]").prop('checked', false);
		});
		$(".remove-shipping-btn").each(function () {
            var csId = $(this).data('csId');
            if($(this).data('isActive') == 'true'){
                restore(csId);
            } else {
                greyout(csId);
            }
        });
    });
	/*if (removeId != ""){
		greyout(removeId);
	}*/

}

function addShipAddresses(){
	var addressIds = gatherChecked();
	console.log("addressIds: "+addressIds);
	if(addressIds.length > 0){
		loadUsersShipping($("#page-type").val(), $("#company").val(), $("#currentUserId").val(), addressIds, "", "", "");
		$("#addShipToAddress").modal('hide');
	} else {
		$("#select-address-error").attr("style","display:block");
	}
	
}

function refreshShipping(userId, companyId){
	$("#shippingList").load("/global/modals/gadgets/add-ship-to-list.jsp?userId="+
		encodeURIComponent(userId)+"&companyId="+encodeURIComponent(companyId), function () {
        eventsAddShipToList();
    });
}

function searchShipping(){
	loadUsersShipping($("#page-type").val(), $("#company").val(), $("#currentUserId").val(), "", "", "", $("#searchText").val(), $("#billingAccounts").val());
}

function resetShipping(){
	$("#searchText").val("");
	loadUsersShipping($("#page-type").val(), $("#company").val(), $("#currentUserId").val(), "", "", "", $("#searchText").val(), $("#billingAccounts").val());
}

/*function greyout(addressId) {
	console.log("Before: "+$("#address-"+addressId).css('opacity'));
	if ($("#address-"+addressId).css('opacity') == "0.6"){
		$("#address-"+addressId).css({opacity:"1"});
		$("#make-default-"+addressId).css({opacity:"1"});
	} else {
		$("#address-"+addressId).css({opacity:"0.6"});
		$("#make-default-"+addressId).css({opacity:"0.6"});
	}
	console.log("After: "+$("#address-"+addressId).css('opacity'));
}*/

function greyout(addressId) {
	$("#address-"+addressId).css({opacity:"0.6"});
	$("#make-default-"+addressId).css({opacity:"0.6"});
}

function restore(addressId) {
	$("#address-"+addressId).css({opacity:"1"});
	$("#make-default-"+addressId).css({opacity:"1"});
}

function selectBillingAddress(pType, org) {
    var pageType = 'add';
    if (pType) {
        pageType = pType;
    }
    console.log('add/edit user selected org = ' + org + ' pageType = ' + pageType);

	var billingAccountIds = gatherListCheckedByName('billingIds');
	$("#billingAccounts").val(billingAccountIds);

    loadUsersShipping(pageType, $("#company").val(), $("#currentUserId").val(), "", "", "", "", billingAccountIds);
}

function showShipToPopup(modal,shippingId){
	$("#addressAdminConfirm .modal-title").text();
	$("#addressAdminConfirm").modal("show");
     $("#shipToAddressSubmit").on("click", function(e){
        e.preventDefault();
        loadUsersShipping($("#page-type").val(), $("#company").val(), $("#currentUserId").val(), "", shippingId, "", $("#searchText").val(), $("#billingAccounts").val());
     });
}