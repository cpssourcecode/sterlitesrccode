// *******************************************************
// This should be included on all modals
// A place to put functions that modals use
// *******************************************************

function closeModalDialog() {
     //parent.$.modal.close();
    //$(".close").click();
    $('#selectShipAddress').modal('hide');
}


function createList(){
    clearHighlighting();
    dataString = $('#createList').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: createListSuccess,
        error: ajaxError
    });
}

function createListSuccess(data){
    if (typeof data.errors == "undefined"){
        parent.location.href=data.url;
    }else{
        showErrors(data);
    }
}

// Select CS Modal Submit
function selectCS(){
    clearHighlighting();
    dataString = $('#select-cs').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: selectCSSuccess,
        error: ajaxError
    });
}

// Select CS Modal Success
function selectCSSuccess(data){
    if (typeof data.errors == "undefined"){
        // Reload parent page w/o login param
        var url = removeURLParam(parent.location.href, 'login');
        parent.location.replace(url);
    }else{
        showErrors(data);
    }
}

//Check is Email exist in Forgot Password Modal
function checkEmail() {
    clearHighlighting();
    dataString = $('#checkEmailForm').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: checkEmailSuccess,
        error: ajaxError
    });
}

//Check Email Success
function checkEmailSuccess(data) {
    if (typeof data.errors == "undefined"){
        $('#checkEmailForm').hide();
        $("#forgotPasswordForm").show();
        var email = $("#inputEmailFirstStep").val();
        $("#inputEmailSecondStep").val(email);
    } else {
        showErrors(data);
    }
}

function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function handleForgotPasswordStep1() {
    clearModalHighlighting();
    var email=$("#password-forgot-input-email").val();
    if(email.trim() == "" || !validateEmail(email)){
        $("#modal-error-messages").show();
        $("#modal-error-messages").html("Please enter a valid email address");
    }else{
    var dataString = $('#password-forgot-form-step-1').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: handleForgotPasswordStep1Success,
        error: ajaxErrorModal
    });
    }
}

function handleForgotPasswordStep2() {
    clearHighlighting();
    var dataString = $('#password-forgot-form-step-2').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: handleForgotPasswordStep2Success,
        error: ajaxError
    });
}

function handleForgotPasswordStep1Success(data) {
    if ('success' == data.code) {
        // $('#password-forgot-form-step-1-div').hide();
        hideErrors();
        $('#forgotPasswordModal').modal('hide');
        $('#forgotPasswordSuccess').modal('show');
        //showSuccessModal("Email has been sent. Please check.");
        // $('#password-forgot-form-step-2-div').show();
    } else {
        showErrorsModal(data);
    }
}

function handleForgotPasswordStep2Success(data) {
    if ('success' == data.code) {
        closeModalDialog();
    } else {
        showErrorsModal(data);
    }
}

function sendEmails(giftlistId) {
    dataString = $("#shareList" + giftlistId).serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: shareListSuccess,
        error: ajaxError
    });
}

function shareListSuccess(data) {
    if (typeof data.errors == "undefined") {
        location.reload(true);
    } else {
        showErrorsGL(data);
    }
}


function showErrorsGL(data) {
    showErrorsCommonGL(data, false);
}

function showErrorsCommonGL(data, isModal) {
    var errorMessage = "";
    var propertiesArray = [];
    var errorsArray = [];
    for (var i = 0; i < data.errors.length; i++) {
        if (indexOf(errorsArray, data.errors[i]) == -1) {
            errorsArray.push(data.errors[i]);
            if (errorMessage == "") {
                errorMessage = data.errors[i];
            } else {
                errorMessage += "<br/>" + data.errors[i];
            }
        }
        propertiesArray.push(data.properties[i]);
    }
    if (errorMessage != "") {
        if (isModal) {
            openErrorMessageModal(errorMessage);
        } else {
            openErrorMessageGL(errorMessage, data.giftListId);
        }
        highlightErrors(propertiesArray);
    }
}

//Adds error messages to error-messages div and shows it
function openErrorMessageGL(errorMessage, giftlistId) {
    var errorMessagesDiv = $("#error-messages" + giftlistId);
    errorMessagesDiv.html(errorMessage);
    errorMessagesDiv.show();
}

function onModalAddToListLoaded(){
    $('#list-add input').keypress(function (e) {
        if (e.which == 13) {
            $('#submitSaveItem').click();
            return false;
        }
    });
}

function addItemsToLists(){
    listIds = gatherListChecked();
    $("#selectedLists").val(listIds);
    dataString = $("#list-add").serialize();
    $.ajax({ type: "POST",
        url:"/global/modals/modal-add-to-list.jsp?modal=true",
        data: dataString,
        dataType: "json",
        success: addItemsSuccess,
        error: ajaxError
    });
}
function addItemsSuccess(data){
    onModalAddToListLoaded();
    if (typeof data.errors == "undefined"){
        parent.location.href="/account/reorder-list.jsp";
    }else{
        showErrors(data);
    }
}
//Function to remove a param from url
function removeURLParam(url, param){
    var urlParts = url.split('?');
    if (urlParts.length >= 2){
        var prefix = encodeURIComponent(param)+'=';
        var pars = urlParts[1].split(/[&;]/g);
        for (var i=pars.length; i-- > 0;)
            if (pars[i].indexOf(prefix, 0)==0)
                pars.splice(i, 1);
        if (pars.length > 0)
            return urlParts[0]+'?'+pars.join('&');
        else
            return urlParts[0];
    }else
        return url;
}

// Common ajax error message
// Alerts the error, this shouldn't happen
function ajaxError(xhr, ajaxOptions, thrownError){
    showGenericError();
    //alert("StatusText: "+xhr.statusText);
    //alert("Status: "+xhr.status);
    //alert("thrownError: "+thrownError);
}

function ajaxErrorModal(xhr, ajaxOptions, thrownError){
    var errorMessage = "An unexpected error has occurred. Please contact customer service or try again at another time.";
    openErrorMessageModal(errorMessage);
}

function showGenericError(data){
    errorMessage = "An unexpected error has occurred. Please contact customer service or try again at another time.";
    openErrorMessage(errorMessage);
}

function hasClass(ele,cls){
    return ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
}

function removeClass(ele,cls){
    if(hasClass(ele,cls)){
        var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
        ele.className=ele.className.replace(reg, '');
    }
}

// select shipping address modal

function doModalCS_Search(cs) {
    var date = new Date();
    var sterm = encodeURIComponent($("#modal-cs-input-search").val().trim());
    $("#modal-cs-list-content").load(
        "/global/modals/gadgets/modal-cs-list.jsp?cs=" + cs + "&nocache=" + date.getTime() + "&filter=" + sterm,  function () {
            eventsModalCSList();
            initScrollbar();
        });
}

function doModalCS_Reset(cs) {
    $("#modal-cs-input-search").val("");
    doModalCS_Search(cs);
}

function selectBilLTo(org, cs) {
    var date = new Date();
    console.log('selectBilLTo org = '+org+" cs = "+cs);
    $("#modal-cs-list-content").load(
        "/global/modals/gadgets/modal-cs-list.jsp?cs=" + cs + "&nocache=" + date.getTime() + "&filter=" + $("#modal-cs-input-search").val() + "&orgId=" + org, function () {
            eventsModalCSList();
            initScrollbar();
        });
}

function doModalCS_Cart_Search() {
    var date = new Date();
    $("#modal-cs-cart-list-content").load(
        "/global/modals/gadgets/modal-cs-cart-list.jsp?nocache=" + date.getTime() + "&filter=" + $("#modal-cs-cart-input-search").val() + "&cs=" + $("#myCustomRadioLabelCart").val(), function(){
            eventsModalCSCartList();
            initScrollbar();
        });
}

function doModalCS_Cart_Reset() {
    $("#modal-cs-cart-input-search").val("");
    doModalCS_Cart_Search();
}

function doModalPickup_Search() {
    var date = new Date();
    $("#modal-pickup-list-content").load(
        "/global/modals/gadgets/modal-pickup-list.jsp?nocache=" + date.getTime() + "&filter=" + $("#modal-pickup-input-search").val(), function () {
            eventsModalPickupList();
            initScrollbar();
        });
}

function doModalPickup_Reset() {
    $("#modal-pickup-input-search").val("");
    doModalPickup_Search();
}

function initScrollbar() {
    $('.scrollbar-inner').scrollbar({
        ignoreMobile: true
    });
}

function doModalCS_SelectShippingAddress_Header() {
    var selectedAddress = $("#selected_shipping_id_header").val();
    if(selectedAddress == null || selectedAddress==""){
        closeModalDialog();
        $("#link-noResultsModal_showMessEmpty").click();
    }
    else{
        var dataString = $('#header-cs-form').serialize();
        $.ajax({
            type: "POST",
            data: dataString,
            dataType: "json",
            success: function(response) {
                doModalCS_SelectShippingAddressSuccess(response)
            },
            error: ajaxErrorLogin
        });
    }
    return false;
}

function doModalCS_CheckShippingAddress(checkboxLabel) {
    var checkedClass = "active";
    $("[id^=addressSelectRadio]").removeClass(checkedClass);
    //checkboxLabel.classList.add(checkedClass);
    checkboxLabel.setAttribute("class", checkedClass);
    $("#selected_shipping_id").val(checkboxLabel.getAttribute("value"));
}

function doModalCS_CheckShippingAddress_Cart(checkboxLabel) {
    var checkedClass = "active";
    $("label[name=addressSelectRadio]").removeClass(checkedClass);
    //checkboxLabel.classList.add(checkedClass);
    checkboxLabel.setAttribute("class", checkboxLabel.className + " " + checkedClass);
    $("#selected_shipping_cart_id").val(checkboxLabel.getAttribute("value"));
}

function doModalCS_CheckPickupAddress(checkboxLabel) {
    var checkedClass = "active";
    $("input[name=addressSelectRadio]").removeClass(checkedClass);
    //checkboxLabel.classList.add(checkedClass);
    checkboxLabel.setAttribute("class", checkboxLabel.className + " " + checkedClass);
    console.log("check pickup: " + checkboxLabel.getAttribute("value"));
    $("#selected_pickup_id").val(checkboxLabel.getAttribute("value"));
}

function doModalCS_SelectPickupAddress(successCallback) {
    clearModalHighlighting();
    var selectedAddress = $("#selected_pickup_id").val();
    if(selectedAddress == null || selectedAddress==""){
        $("#changePickupLocation").modal('hide');
        $("#link-noResultsModal_showMessEmpty").click();
    }
    else{
        var deliveryMethod = $("#deliveryMethodHidden").val();
        if(deliveryMethod){
            $("#select_address_delivery_method").val(deliveryMethod);
        }
        var dataString = $('#modal-pickup-form').serialize();
        console.dir(dataString);
        $.ajax({
            cache: false,
            type: "POST",
            data: dataString,
            dataType: "json",
            success: function(response) {
                if(successCallback) {
                    successCallback(response);
                }
                console.dir(response);
            },
            error: ajaxErrorLogin
        });
    }
    return false;
}

function doModalCS_SelectShippingAddress(successCallback) {
    clearModalHighlighting();
    var selectedAddress = $("#selected_shipping_id").val();
    if(selectedAddress == null || selectedAddress==""){
        closeModalDialog();
        $("#link-noResultsModal_showMessEmpty").click();
    }
    else{
        var deliveryMethod = $("#deliveryMethodHidden").val();
        if(deliveryMethod){
            $("#select_address_delivery_method").val(deliveryMethod);
        }
        var dataString = $('#modal-cs-form').serialize();
        console.dir(dataString);
        $.ajax({
            cache: false,
            type: "POST",
            data: dataString,
            dataType: "json",
            success: function(response) {
                if(successCallback) {
                    successCallback(response);
                }
                doModalCS_SelectShippingAddressSuccess(response)
            },
            error: ajaxErrorLogin
           // error: doModalCS_SelectShippingAddressSuccess
        });
    }
    return false;
}

function doModalCS_CheckShippingAddress_Header(checkboxLabel) {
    $("#selected_shipping_id_header").val(checkboxLabel.getAttribute("value"));
    doModalCS_SelectShippingAddress_Header();
}

function doModalCS_SelectShippingAddressSuccess(response) {
    var result;
    if ('success' == response.code) {
        result="success";
        // prevent ie from caching
        d = new Date();
        t = d.getTime();
        $("#header-cs-selection").load("/includes/gadgets/header-cs-selection.jsp?t="+t, function () {
            eventsHeaderCsSelection();
        });
        closeModalDialog(); 
        var url = parent.location.href;
        console.log("URL: "+url);
        url = removeURLParameter(url, "showCS");
        url = removeURLParameter(url, "onHold");
        //url = removeURLParameter(url, "result");
        //url=url+"?result=" + result;
        console.log("Replace url: "+url);
        parent.location.replace(url);
        location.reload();
    } else {
        result="error";
        console.dir(response);
        showErrors(response);
    }
    
}

function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');   
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(parameter)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {    
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                pars.splice(i, 1);
            }
        }

        url= urlparts[0]+'?'+pars.join('&');
        return url;
    } else {
        return url;
    }
}

function openCSModal(close) {
    var url = "/global/modals/modal-cs.jsp?t=" + (new Date()).getTime();
    if (close == "true") {
        url = url + "&close=true";
    }
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'html',
        data: ({action: 'loadModalDiv'}),
        complete: function () {
            $("#link-selectShipAddress").click();
            $('.scrollbar-inner').scrollbar({
                ignoreMobile: true
            });
        },
        success: function (data) {
            data = $.trim(data);
            $("#modal-cs-show-div").html(data);
            eventsModalCS();
            onModalCsLoaded();
            $(document).ready(function () {
                $('#confirmBtn1, #confirmBtn2').click(function () {
                    doModalCS_SelectShippingAddress(doModalCS_SelectShippingAddressSuccess);
                });
            });
        }
    });
}

// ~ select shipping address modal ~

function showNewMaterialListErrors(data){
    var errorMessage = "";
    var propertiesArray = [];
    var errorsArray = [];
    for(var i=0; i<data.errors.length; i++){
        if (indexOf(errorsArray,data.errors[i]) == -1){
            errorsArray.push(data.errors[i]);
            if (errorMessage == ""){
                errorMessage = data.errors[i];
            }else{
                errorMessage += "<br/>" + data.errors[i];
            }
        }
        propertiesArray.push(data.properties[i]);
    }
    if (errorMessage != ""){
        openErrorMessageDiv(errorMessage, "create-list-error-messages");
        highlightErrors(propertiesArray);
    }
}

function clearCreateListHighlighting(){
    $("#create-list-error-messages").hide();
    $("#create-list-success-messages").hide();

    $("div.form-group.has-error").removeClass('has-error');
    $(".has-error").removeClass('has-error');
}

//Create new Material list 2015/07/20
function createNewMaterialList() {
    var file = $("#material-list-file");
    var fileName = file.text();
    var emptyFileErrorMessage=$("#emptyFileErrorMessage").val();

    if (file && fileName !== undefined && fileName !== '') {
        createMaterialListFromFile(file, fileName);
    } else {
        $("#modal-message-error-div").show();
        $("#modal-message-error").html(emptyFileErrorMessage);
        //$("#newMLEmptyFileError").show();
        //createEmptyMaterialList();
    }
}

function uploadMaterialListFile() {
    $('.inputfile').each(function () {
        var $input = $(this),
            $label = $input.next().next('label'),
            labelVal = $label.html();

        $input.on('change', function (e) {
            var fileName = '';

            if (this.files && this.files.length > 1) {
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            } else if (e.target.value) {
                fileName = e.target.value.split('\\').pop();
            }

            if (fileName) {
                $label.find('span').html(fileName);
            } else {
                $label.html(labelVal);
            }
        });

        // Firefox bug fix
        $input
            .on('focus', function () {
                $input.addClass('has-focus');
            })
            .on('blur', function () {
                $input.removeClass('has-focus');
            });
    });
}

function createMaterialListFromFile(file, fileName) {
    $('.preload-cover').addClass('loader-active');
    $('.preload-cover .loading').show();
    $(".errors").hide();
    $("#modal-message-error-div").attr("style", "display: none");
    $("#modal-message-error").html("");
    
    var fileExt = fileName.substr(fileName.lastIndexOf('.') + 1);
    if (fileExt === "xls" || fileExt === "xlsx" || fileExt === "csv") {
        $('#listNameFromFile').val($('#control-ml-name').val());
        $('#listDescriptionFromFile').val($('#control-ml-description').val());
        clearCreateListHighlighting();

        $('#createListFromFile').ajaxSubmit({
            cache: false,
            success: createMaterialListFromFileSuccess
        });

    } else {
        $('.preload-cover').removeClass('loader-active');
        $('.preload-cover .loading').hide();
        document.getElementById("newMLFileError").style.display = "block";
    }
}

function createEmptyMaterialList() {
    $("#modal-message-error-div").attr("style","display: none");
    $("#modal-message-error").html("");
    $('#eventName').val($('#control-ml-name').val());
    $('#description').val($('#control-ml-description').val());

    clearCreateListHighlighting();

    dataString = $('#createList').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: createNewSuccess
    });
}

function createNewSuccess(data){
    if ('success' == data.code) {
        closeNewMaterialListModal();
        resetMaterialListSearch();
    } else {
        showErrors(data,$("#modal-message-error"));
        $("#modal-message-error-div").show();
        highlightErrors(data.properties);
    }
}

function createMaterialListFromFileSuccess(data) {
    if (data.code === "success") {
        closeNewMaterialListModal();
        resetMaterialListSearch();
        $("#modal-giftlist-count").load("/global/modals/modal-list-found.jsp?messageId=ml-added&divId=_materialListCreated");
        setTimeout(function(){
            $("#link-noResultsModal_materialListCreated").click();
        },5000);
    } else {
        var isFileDataError = data.properties.indexOf("invalid_fields_format") > -1;
        if (isFileDataError) {
            closeNewMaterialListModal();
            resetMaterialListSearch();
            showMaterialListFileErrorModal();
        } else {
            $('.preload-cover').removeClass('loader-active');
            $('.preload-cover .loading').hide();
            showErrors(data,$("#modal-message-error"));
            $("#modal-message-error-div").show();
            highlightErrors(data.properties);
        }
    }
}

function closeNewMaterialListModal() {
    var closeButton = document.getElementById("closeNewMLModal");
    $('.preload-cover').removeClass('loader-active');
    $('.preload-cover .loading').hide();
    if (closeButton) {
        closeButton.click();
    }
}

function showMaterialListFileErrorModal() {
    $("#materialListFileErrorModal").load("/global/modals/modal-items-not-added-to-ml.jsp", function () {
        $('#itemsAddtoMl').modal('show');
    });

}

function showNoResultsModalPopup() {
    $("#link-noResultsModal").click();
}

function showDeleteMaterialListModalPopup(giftlistId) {
    $("#link-modalDeleteMaterial" + giftlistId).click();
}

function showShareMaterialModalPopup(giftlistId) {
    $("#link-modalShareMaterial" + giftlistId).click();
}

function showEditMaterialListModalPopup(giftlistId) {
    $("#link-modalEditMaterial" + giftlistId).click();
}

function showImportMaterialListModalPopup(giftlistId) {
    $("#link-modalImportMaterial" + giftlistId).click();
}

// Login Captcha-related functions
var cps_login_captcha_success_default = false;
var cps_login_captcha_success_guest = false;
var cps_login_captcha_success_page = false;
var share_captcha_success_default = false;
var cps_login_captcha_default = null;
var cps_login_captcha_guest = null;
var cps_login_captcha_page = null;
var share_captcha_default = null;

// Avoid conflict in IE with Bootstrap focus enforcer on Recaptcha iframe
$.fn.modal.Constructor.prototype.enforceFocus = function() {
    $(document).off('focusin.bs.modal').on('focusin.bs.modal', $.proxy(function(e){
            if (this.$element[0] !== e.target && !this.$element.has(e.target).length && e.target.title.indexOf('recaptcha') === -1) {
                this.$element.trigger('focus');
            }
        }, this)
    );
}
function checkEnableLoginModalButton(){
    var enableLoginButton = true;
    if ($('#login-form-captcha').length > 0) {
        enableLoginButton = cps_login_captcha_default;
    }

    if (enableLoginButton) {
        $('#submitLoginForm').removeAttr('disabled');
    } else {
        $('#submitLoginForm').attr('disabled', 'disabled');
    }
}
function renderCaptchaCallbacklogin() {
    var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
    cps_login_captcha_default = grecaptcha.render('recaptchaDefault', {
      'sitekey' : sitekey,
      'callback' : validateRecaptchaDefaultlogin,
      'expired-callback' : expireRecaptchaDefaultlogin
  });
}
function validateRecaptchaDefaultlogin(){
    cps_login_captcha_success_default = true;
    checkEnableLoginModalButton();
}

function expireRecaptchaDefaultlogin(){
    grecaptcha.reset(cps_login_captcha_default);
    cps_login_captcha_success_default = false;
    checkEnableLoginModalButton();

    
}
function renderCaptchaCallback() {
    var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
    cps_login_captcha_guest = grecaptcha.render('recaptchaDefault-guest', {
        'sitekey' : sitekey,
        'callback' : validateRecaptchaDefault,
        'expired-callback' : expireRecaptchaDefault
    });
   
    
}
function renderCaptchaCallbackShare(){
    var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
 share_captcha_default = grecaptcha.render('recaptchaShare', {
            'sitekey' : sitekey,
            'callback' : validateRecaptchaDefaultShare,
            'expired-callback' : expireRecaptchaDefaultShare
    });
}
function checkShareSubmitButton() {
    var enableLoginButton = true;
    if ($('#share-form-captcha').length > 0) {
        enableLoginButton = share_captcha_success_default;
    }

    if (enableLoginButton) {
        $('#shareModalSubmit').removeAttr('disabled');
    } else {
        $('#shareModalSubmit').attr('disabled', 'disabled');
    }
}
function checkEnableLoginButton(suffix) {
    if (typeof suffix === 'undefined'){
        suffix = '';
    }
    var enableLoginButton = true;
    if ($('#login-form-captcha' + suffix + ':visible').length > 0) {

        var success = suffix === '-guest' ? cps_login_captcha_success_guest : cps_login_captcha_success_default;

        enableLoginButton = success && $('#logIn' + suffix  + ' #input-email' + suffix).val().length > 0
            && $('#logIn' + suffix  + ' #input-password' + suffix).val().length > 0;
    }
    
    if (enableLoginButton) {
        $('#logIn' + suffix + ' #submitLoginForm' + suffix).removeAttr('disabled');
    }
    else {
        $('#logIn' + suffix + ' #submitLoginForm' + suffix).attr('disabled', 'disabled');
    }
}

function checkEnableLoginPageButton() {
    var enableLoginButton = true;
    if ($('#login-form-page-captcha:visible').length > 0) {

        enableLoginButton = cps_login_captcha_success_page
            && $('#logInPage #input-email').val().length > 0
            && $('#logInPage #input-password').val().length > 0;
    }

    if (enableLoginButton) {
        $('#logInPage #submitLoginPageForm').removeAttr('disabled');
    }
    else {
        $('#logInPage #submitLoginPageForm').attr('disabled', 'disabled');
    }
}
function validateRecaptchaDefaultShare(captchaResponse) {
    share_captcha_success_default = true;
    checkShareSubmitButton();
}

function expireRecaptchaDefaultShare() {
    grecaptcha.reset(share_captcha_default);
    share_captcha_success_default = false;
    checkShareSubmitButton();
}
function expireRecaptchaDefault() {
    grecaptcha.reset(cps_login_captcha_default);
    grecaptcha.reset(cps_login_captcha_guest);
    cps_login_captcha_success_default = false;
    cps_login_captcha_success_guest = false;
    checkEnableLoginButton();
    checkEnableLoginButton('-guest');
}

function expireRecaptchaPage() {
    grecaptcha.reset(cps_login_captcha_page);
    cps_login_captcha_success_page = false;
    checkEnableLoginPageButton();
}

function validateRecaptchaDefault(captchaResponse) {
    cps_login_captcha_success_default = true;
    cps_login_captcha_success_guest = true;
    checkEnableLoginButton();
    checkEnableLoginButton('-guest');
}

function validateRecaptchaPage(captchaResponse) {
    cps_login_captcha_success_page = true;
    checkEnableLoginPageButton();
}

//sort orders on view order modal
function sortOrderDetails(sortColumn){
    column = sortColumn;
    if (!column.id) {
        console.log('exit');
        return;
    }
    sortOption = column.id;
    var cssClass = "";
    var el = $('#order-tbl').find('i'); 
    el.removeClass().addClass('fa fa-sort');
    
    column.asc = !column.asc;
    if (column.asc) {
        cssClass = "fa fa-sort-asc";
    } else {
        cssClass = "fa fa-sort-desc";
    }
    $(column).find('#' + sortOption + '-i').removeClass().addClass(cssClass);

    var table = $(column).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(column).index()))
    if (!column.asc){
        rows = rows.reverse();
    }
    for (var i = 0; i < rows.length; i++){
        table.append(rows[i]);
    }

    function comparer(index) {
        return function(a, b) {
            var valA = getCellValue(a, index), valB = getCellValue(b, index)
            return $.isNumeric(valA.trim().substring(1)) && $.isNumeric(valB.trim().substring(1)) ? valA.trim().substring(1) - valB.trim().substring(1) : valA.localeCompare(valB)
        }
    }
    
    function getCellValue(row, index){ return $(row).children('td').eq(index).html() }
}

function modalNewMaterialListLoaded(){
    $('#newMaterialListModal input').keypress(function (e) {
        if (e.which == 13) {
            $('#submitNewMaterialListForm').click();
            return false;
        }
    });

    $("#newMaterialListModal").on("hidden.bs.modal", function(){
        $("#add-new-list-modal").load('/global/modals/modal-new-material-list.jsp', function () {
            eventsModalNewMaterialList();
        });
    });
}
