function resetMTRSearch() {

//	$('#myCustomRadioLabel2').click();
//	$('#invoiceSearchOrderNumber').val("");
//	$('.show-title').text('Ship to');
//	$('#invoiceSearchCS').val("");
//	$('#invoiceSearchTime').val("");
//	$('#invoiceSearchStartIndex').val("0");

	requestMTR();
}

function clearFound(heatNums) {
	for (var i = 0; i < heatNums.length; i++) {
		$("#" + heatNums[i]).val("");
		//$("#" + heatNums[i]).removeClass("state-danger");
		$("#heatNumDiv" + heatNums[i].substring(7)).removeClass("has-error");
	}
}

function checkNotFound() {
	for (var i = 1; i <= 6; i++) {
		var val = $("#heatNum" + i).val();
		if (val.length > 0 && val.indexOf("NOT FOUND") == -1) {
			$("#heatNum" + i).val(val + " NOT FOUND");
			$("#heatNumDiv" + i).addClass("has-error")
		}
	}
}

function requestMTR() {
	var search = false;
	for (var i = 1; i <= 6; i++) {
		if ($.trim($("#heatNum" + i).val()) != '') {
			search = true;
		}
		$("#heatNum" + i).val($.trim($("#heatNum" + i).val()));
		var val = $("#heatNum" + i).val();
		if (val.length > 0 && val.indexOf("NOT FOUND") == -1) {
			$("#f-heatNum" + i).val(val);
		} else {
			$("#f-heatNum" + i).val("");
		}
	}
	
	if (search) {
		dataString = $('#mtr-search').serialize();
		$.ajax({
			type: "POST",
			data: dataString,
			dataType: "json",
			success: loadMTRSuccess
		});	
	} else {
		$("#link-modalNoHeatEntered").click();
	}
}

function onMtrListDisplayLoaded(){
    $(document).ready(function() {
        var heatNumsStr = $("#heatNums").val();
        var heatNums = [];
        if(heatNumsStr) {
        	heatNums = heatNumsStr.split(',');
        }
        clearFound(heatNums);
        checkNotFound();
        if ($("table:not(:has(>tbody>tr))").length) {
            $('#mtr-list').hide();
            $('#mtr-preview').hide();
            $('#btnDeleteSelected').hide();
         }else{
            $('#mtr-list').show();
            $('#mtr-preview').show();
            $('#btnDeleteSelected').show();
         }
        $(".resultsMTRSection").show();
    });
}

function loadMTRSuccess(data){
	if (typeof data.errors == "undefined"){
		var d = new Date();
		var t = d.getTime();
		$("#mtr-list").load("/account/gadgets/mtr-list-display.jsp?t="+t, function() {
            onMtrListDisplayLoaded();
			eventsMtrListDisplay();
			jQuery('.scrollbar-inner').scrollbar();
			initializeCheckboxes();
            $("#mtr-preview").html('<p class="text-center mtr_preview">MTR Preview </p>');
            loadMTRPreview();
		});
	}
}
function loadMTRPreview(){
    $(".mtr_url").on('click',function(e){
        e.preventDefault();
        var url=$(this).attr("href");
        console.log(url);
        var iframe = '<iframe id="mtr-iframe" src="'+url+'" style="height: 500px; width: 100%;"></iframe>';
        $("#mtr-preview").html(iframe);
        $('#mtr-search')[0].scrollIntoView(true);
    });
}

function loadMTRSuccessAfterDelete(data){
	if (typeof data.errors == "undefined"){
		var d = new Date();
		var t = d.getTime();
		$("#mtr-list").load("/account/gadgets/mtr-list-display.jsp?t="+t, function() {
            onMtrListDisplayLoaded();
			eventsMtrListDisplay();
			jQuery('.scrollbar-inner').scrollbar();
			initializeCheckboxes();
            $("#mtr-preview").html('<p class="text-center mtr_preview">MTR Preview </p>');
            loadMTRPreview();
		});
	} else {
		// show error popup
	}
}

function clearMTRForm(){
	$('.heatNum').val("");
	//$('.heatNum').removeClass("state-danger");
	$('.order-pad-item').removeClass("has-error");
}

function deleteSelectedMTR(){
	var itemIds = gatherListChecked();

	if (itemIds.length > 0) {
		$("#removeList").val(itemIds);

		dataString = $("#remove-mtr-form").serialize();
		$.ajax({
			type: "POST",
			data: dataString,
			dataType: "json",
			success: loadMTRSuccessAfterDelete
		});
	} else {
		$("#link-modalDeleteMTRNoSelect").click();
	}

}
