$(document).ready(function() {
	$("#add-error").hide();
	$("#pa-ref").click(
			function() {
				if ($(this).val() == 'Item # or Alias') {
					$(this).val('');
				}
			}).blur(function() {
		if ($(this).val() == '') {
			$(this).val('Item # or Alias');
		} else {
			$(this).val($(this).val().toLowerCase());
		}
	})
	$("#pa-qty").click(
			function() {
				if ($(this).val() == 'Qty') {
					$(this).val('');
				}
			}).blur(function() {
		if ($(this).val() == '') {
			$(this).val('Qty');
		}
	})
});

function submitWithAddAction() {
	document.getElementById('action').value = 'addItemToPA';
	$('#pa-ref').val($.trim($('#pa-ref').val()));
	$('#pa-qty').val($.trim($('#pa-qty').val()));
	var skuId = $("#pa-ref").val();
	var qty = $("#pa-qty").val();

	if ((skuId != '' && skuId != 'Item # or Alias') && (qty == '' || qty == 'Qty')) {
		$("#pa-qty").val('1');
	}
	document.getElementById('pa_form').submit();
}

function submitWithUpdateItemAction(count) {

	document.getElementById('action').value = "updatepa";
	document.getElementById('pa-qty').value = 0;
	document.getElementById('pa-ref').value = "";
	document.getElementById('updatedqty').value = document.getElementById('qtys_' + count).value
	document.getElementById('updatecount').value = count;
	document.getElementById('pa_update_form').submit();
}

function paAddAll() {
	itemIds = [];
	var elem = document.getElementsByTagName('input');
	var inputs = new Array();
	for (i = 0,iarr = 0; i < elem.length; i++) {
		att = elem[i].getAttribute("name");
		if (att == 'check') {
			inputs[iarr] = elem[i];
			iarr++;
		}
	}

	for (var i = inputs.length - 1; i >= 0; i--)
		if (inputs[i].type === "checkbox")
			itemIds.push(inputs[i].id);
	$("#paIds").val(true);
	$("#addpaitemlist").val(itemIds);
	$("#pa_reorder_details_add").submit();
}

function paAddselected() {
	var itemIds = gatherChecked();
	$("#paIds").val(true);
	$("#addpaitemlist").val(itemIds);
	$("#pa_reorder_details_add").submit();
}

function submitWithClearCartAction() {
	document.getElementById('action').value = 'clearPA';
	document.getElementById('pa-qty').value = 0;
	document.getElementById('pa-ref').value = "";
	document.getElementById('pa_form').submit();

}

function submitWithRemoveItemAction(count) {

	document.getElementById('action').value = "removeOneItem";

	document.getElementById('pa-qty').value = 0;
	document.getElementById('pa-ref').value = "";
	document.getElementById('itemcount').value = count;
	document.getElementById('pa_remove_form').submit();

}

function needToLoginList() {

	document.getElementById('action').value = 'needToLoginList';
	document.getElementById('qo-qty').value = 0;
	document.getElementById('qo-ref').value = "";

	document.getElementById('pa_form').submit();

}

function removeSelected() {
	var listIds = gatherChecked();
	document.getElementById('pa-qty').value = 0;
	document.getElementById('pa-ref').value = "";
	document.getElementById('action').value = "removeselected";
	document.getElementById('removeitems').value = listIds;


	document.getElementById('pa_removeselected_form').submit();
}

function toggleCheckBoxes(check) {
	//alert("selecting all");
	var elem = document.getElementsByTagName('input');
	var checkboxes = new Array();
	for (i = 0,iarr = 0; i < elem.length; i++) {
		att = elem[i].getAttribute("name");
		if (att == 'check') {
			checkboxes[iarr] = elem[i];
			iarr++;
		}
	}
	for (var i in checkboxes) {
		if (checkboxes[i].checked != check.checked) {
			checkboxes[i].click();
			checkboxes[i].checked = check.checked;
		}
	}
}