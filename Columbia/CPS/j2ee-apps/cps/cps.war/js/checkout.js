//Checkout common
$(document).ready(function () {
	eventsModalPickupLoc();
	$(".pickup-btn-confirm").click(function () {
        onChangePickUpLocation();
    });
    $('.scrollbar-inner').scrollbar({
        ignoreMobile: true
    });
    $(".carts-steps a[href=#]").click(function (e) {
        e.preventDefault();
    });
    $(".table-content a[href=#]").click(function (e) {
        e.preventDefault();
    });
    $('#modalError').on('hidden.bs.modal', function () {
    	$('.modal-backdrop').addClass('modal');
    	$('.modal').removeClass('modal-backdrop');
    });
});
function exportCart(){
	
	var cids = new Array();
	$(".table-content .check :checkbox:checked").each(function(){
		cids.push($(this).val());
	 });
	 if (cids.length == 0) {
		 	$("#alertNotSelectedCartItem").modal("show");
		    resetTableCheckboxes();
	        return;
	    }
	$("#commerceItemToPrint").val(cids);
	$("#exportCart").submit();
}

function onPaymentMethodChanged() {
    var onAccountPaymentRadio = $("#onAccountPaymentRadio");
    var creditCardPaymentRadio = $("#creditCardPaymentRadio");

    if (onAccountPaymentRadio.is(":checked")) {
        $("#checkout-payment-cc").hide();

        ajaxPostFormSubmit("#commit-order-remove-credit-card").done(function (data) {
            if (data.code === 'error') {
                processSubmitErrors(data);
            }
        });

        var placeOrderButton = $(".btn-place-order");
        placeOrderButton.removeAttr("disabled");
        placeOrderButton.removeClass("disabled");

    } else if (creditCardPaymentRadio.is(":checked")) {
        if (checkPOnumber()) {
            $("#checkout-payment-cc").show();

            $("#credit-card-form-include-mtr").val($("#order-mtr-inc").checkbox("isChecked"));
            ajaxPostFormSubmit("#create-credit-card-form").done(function (data) {
                if (data.code === 'error') {
                    processSubmitErrors(data);
                }
            });

            var placeOrderButton = $(".btn-place-order");
            placeOrderButton.attr("disabled", "disabled");
            placeOrderButton.addClass("disabled");
        } else {
            creditCardPaymentRadio.attr('checked', null);
            $('#commit-order-po-value').focus();
        }
    }
}

function initPaymentMethodEvents() {
    $('#onAccountPaymentRadio, #creditCardPaymentRadio').on('change', onPaymentMethodChanged);
}

function initReviewPage() {
    disablePageContent();
    loadTable("review", $("#cart-sort-option").val(), $("#cart-sort-option-type").val()).then(enablePageContent, enablePageContent);
    initTableSorting();
    initPaymentMethodEvents();
    onPaymentMethodChanged();
}

function reloadShoppingCartTable() {
    disablePageContent();
    loadTable("cart", $("#cart-sort-option").val(), $("#cart-sort-option-type").val(),true).then(enablePageContent, enablePageContent);
}

function initCartPage(isAdmin) {
    if (isAdmin) {
        $('body').addClass('admin');
    }
    disablePageContent();
    loadTable("cart", $("#cart-sort-option").val(), $("#cart-sort-option-type").val(), true).then(enablePageContent, enablePageContent);
    checkboxer({
        delegate: ".cart-items-list",
        header: ".header .checkbox-select-all :checkbox",
        content: ".table-content .check :checkbox:enabled"
    });
    initTableSorting();
    initValidation();
    initToggleDeliveryMethod();
}

function initToggleDeliveryMethod() {
    $(".radio-toggle-delivery-method").click(function () {
        if ($(".radio-toggle-delivery-method:checked").val() === 'delivery-method-pick-up') {
            $(".delivery-method-pick-up-form").show();
            $(".delivery-method-shipped-form").hide();
        }
        if ($(".radio-toggle-delivery-method:checked").val() === 'delivery-method-shipped') {
            $(".delivery-method-shipped-form").show();
            $(".delivery-method-pick-up-form").hide();
        }
    });
}

function initTableSorting() {
    $(".cart-items-list .item-number i.fa-sort").click(function () {
        this.sortDirection = this.sortDirection === "+" ? "-" : "+";
        sortTable(comparator(selectableParser(".item-number", parseInt), this.sortDirection));
    });
    $(".cart-items-list .alias i.fa-sort").click(function () {
        this.sortDirection = this.sortDirection === "+" ? "-" : "+";
        sortTable(comparator(selectableParser(".alias", parseInt), this.sortDirection));
    });
    $(".cart-items-list .u-price i.fa-sort").click(function () {
        this.sortDirection = this.sortDirection === "+" ? "-" : "+";
        sortTable(comparator(selectableParser(".u-price", parseFloatFromPrice), this.sortDirection));
    });

    function parseFloatFromPrice(price) {
        if (price) {
            var pureFloatStr = price.trim().replace(/[$,]/g, "");
            return pureFloatStr == "" ? 0.0 : parseFloat(pureFloatStr);
        }
        return 0.0;
    }

    function comparator(parser, direction) {
        if (direction === "-") {
            return function (a, b) {
                return parser(a) < parser(b) ? 1 : -1;
            }
        } else {
            return function (a, b) {
                return parser(a) > parser(b) ? 1 : -1;
            }
        }
    }

    function selectableParser(selector, parser) {
        return function (a) {
            return parser($(selector, a).text());
        }
    }

    function sortTable(comporator) {
        var rows = $(".table-content > .content");
        rows.sort(comporator);
        $(".table-content").empty();
        $(".table-content").append(rows);
    }
}

function checkboxer(options) {

    var delegate = options.delegate;
    var header = options.header;
    var content = options.content;
    var checkListener = options.checkListener;

    $(delegate).on("change", header, function () {
        var newState = $(this).checkbox("isChecked") ? "check" : "uncheck";
        if (checkListener) {
            $(content).each(function () {
                $(this).checkbox(newState);
                $.proxy(checkListener, this)();
            });
        } else {
            $(content).checkbox(newState);
        }
    });

    $(delegate).on("change", content, function () {
        var checkboxCount = $(content).length;
        var checkedCount = $(content + ":checked").length;
        var state = $(this).checkbox("isChecked") ? "check" : "uncheck";
        if (checkedCount == checkboxCount) {
            $(header).checkbox(state);
        } else if (!$(this).checkbox("isChecked") && (checkedCount + 1) == checkboxCount) {
            $(header).checkbox(state);
        }
        if (checkListener) {
            $.proxy(checkListener, this)();
        }
    });
}

function wrapLoadToDeferred(to, from, options) {
    var def = $.Deferred();
    $(to).load(from + "?_=" + new Date().getTime(), options, function () {
        def.resolve();
    });
    return def;
}

function ajaxPostFormSubmit(formSelector) {
    return $.ajax({
        method: "POST",
        cache: false,
        dataType: "json",
        data: $(formSelector).formSerialize()
    });
}

function ajaxPostFormSubmitUpdateQty(formSelector, skuId, oldQty, cid) {
    return $.ajax({
        method: "POST",
        cache: false,
        dataType: "json",
        data: $(formSelector).formSerialize(),
        success: function (data) {
            if (null !== data && typeof data.errors !== "undefined" && data.error === "true" && data.errorCodes.length !== 0) {
                var errorCodes = data.errorCodes;
                var errors = data.errors;
                var error_messages = "";

                for (var i = 0; i < errorCodes.length; i++) {
                    if (errorCodes[i] === 'minQtyError') {
                        if (error_messages === "") {
                            error_messages = errors[i].split('-')[0];
                        } else {
                            error_messages = error_messages + "<br />" + errors[i].split('-')[0];
                        }
                    }
                }

                $("#modal-add-to-cart-error").modal("show");
                var $errorMessage = $("#add-item-error-message");
                $errorMessage.html(error_messages);
                $errorMessage.show();
            }
        }
    });
}

function updatePONumber() {
    return ajaxPostFormSubmit("#checkout-po-number").done(function (data) {
        if (data.errors && data.errors.length > 0) {
            $("html, body").animate({scrollTop: 0}, "slow");
            processSubmitErrors(data);
        }
        return data;
    });
}

function connectToSequentialChain(firstDiff, secondFunct) {
    var def = $.Deferred();
    firstDiff.done(function (data) {
        secondFunct(data).then(def.resolve, def.resolve);
    });
    return $.when(firstDiff, def);
}

function resetTableCheckboxes() {
    $(".cart-items-list :checkbox").checkbox("uncheck");
}

function getSelectedCheckboxesCids() {
    return $(".table-content .check :checkbox:checked").map(function () {
        return this.value;
    });
}

function disablePageContent() {
	logoutAllow = false;
    $(".i-switch-state").attr("disabled", true);
    //$(".btn-proceed-checkout").attr("disabled", true); // according to CPS-647
    $(".quick-add-control").attr("disabled", true);
    $('.preload-cover').addClass('loader-active');
    $('.preload-cover .preload-spinner').show();
    
}

function enablePageContent() {
	logoutAllow = true;
    if ($(".empty-cart").length === 0) {
        $(".i-switch-state").attr("disabled", false);
    }
    $(".quick-add-control").attr("disabled", false);
    var isPricingUnavailable = $("#isPricingUnavailable").val();
    if (isPricingUnavailable === 'true') {
        $(".btn-proceed-checkout").attr("disabled", false);
    } else if ($(".table-content .content:not(.disabled):not(.av)").length === 0) {
        $(".btn-proceed-checkout").attr("disabled", false);
    }
    if ($('#cart-proceed-to-checkout-bottom').attr('disabled') === 'disabled' && isPricingUnavailable === 'false') {
        $('.quick-add-control.addToCartButton').attr('disabled', true);
    } else {
        $('.quick-add-control.addToCartButton').attr('disabled', false);
    }

    $('.preload-cover').removeClass('loader-active');
    $('.preload-cover .preload-spinner').hide();

    priceAvailableMsg($("#isAvailablePricesWebService").val());
}

///*** Loaders and submitters***///
function loadTable(source, sortOption, sortOptionType, isCheckAvailability) {
    resetTableCheckboxes();
    return connectToSequentialChain(
        wrapLoadToDeferred(".table-content", "/checkout/includes/cart-items-table.jsp", {
            source: source,
            sortOption: sortOption,
            sortOptionType: sortOptionType
        }).done(function () {
            eventsCartTableRow();
            var obsoleteItems = $(".cart-items-list .table-content .obsolete");
            if (obsoleteItems && obsoleteItems.length >= 3) {
                obsoleteItems.map(function () {
                    var prodDescription = $(".col.desc span", this).text();
                    $("#importantMsg .panel-body ul").append("<li>" + prodDescription + "</li>");
                });
                $(".important-message").removeClass("hidden");
            }
            priceAvailableMsg($("#isAvailablePricesWebService").val());
            if ($("#productDisabled").val()) {
                $('#products_obsolete_msg').show();
            }
            
            if ($(".product-not-available").length > 0 && $('.text-danger:contains(Unavailable)').text().length != 0){
				$('#products_obsolete_msg').show();
			} else {
				$('#products_obsolete_msg').hide();
			}		
            
			if (source == 'cart' && isCheckAvailability) {
            	var cids =  $(".table-content .check :checkbox").map(function () {
            	        return this.value;
            	    });
            	onCheckAvailabilityClick(cids);
            }
        }).fail(function () {
            $(".table-content").append("<div class='content empty-cart'>Error data loading. Please, <a onclick='onReloadBtnClick();'>refresh</a> the page.</div>");
        }),
        function () {
        	if (source == 'review') {
        		return $.when(refreshOrderSubtotal(), refreshCartItemsCount());
        	}
        }
    );
}

function refreshCartItemsCount() {
    $.ajax({
        method: "GET",
        url: "/checkout/gadgets/cart-items-count-summary.jsp",
        cache: false,
        dataType: "text",
        success: function (data) {
            if (data) {
                $(".summary-items-count").text(data);
            } else {
                console.log("Item count wasn't received");
            }
        },
        error: function (data) {
            console.log(data);
        }
    });

    return $.ajax({
        method: "GET",
        url: "/checkout/gadgets/cart-items-count.jsp",
        cache: false,
        dataType: "text",
        success: function (data) {
            $(".btn-cart-toggle").removeAttr("disabled");
            if (data) {
                $("#navbar-cart .cart-items-count").text("(" + data + ")");
                if (data <= 0) {
                    $(".btn-cart-toggle").attr("disabled", "");
                    $("#empty-cart-message").show();
                    $(".cart-items-list").hide();
                    $(".reorder-disclaimer").hide();
                }
                else {
                    $("#empty-cart-message").hide();
                    $(".cart-items-list").show();
                    $(".reorder-disclaimer").show();
                }
            } else {
                console.log("Item count wasn't received");
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function refreshApprovalWarn() {
    return $.ajax({
        method: "GET",
        url: "/checkout/includes/approval-warn.jsp",
        cache: false,
        dataType: "text",
        success: function (data) {
            $("#approvalWarn").replaceWith(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function refreshSubmitOne() {
    return $.ajax({
        method: "GET",
        url: "/checkout/includes/submit-one.jsp",
        cache: false,
        dataType: "text",
        success: function (data) {
            $("#submit1").replaceWith(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function refreshSubmitTwo() {
    return $.ajax({
        method: "GET",
        url: "/checkout/includes/submit-two.jsp",
        cache: false,
        dataType: "text",
        success: function (data) {
            $("#submit2").replaceWith(data);
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function refreshTableData(cids, options) {
    return $.ajax({
        method: "GET",
        url: "/checkout/includes/cart-partial-table.jsp",
        data: options,
        cache: false,
        dataType: "html",
        success: function (data) {
            resetTableCheckboxes();
            var htmlData = $(data).wrap("<div>").parent();
            jQuery.map(cids, function (val) {
                if ($("#row_" + val).length > 0) {
                    $("#row_" + val).replaceWith(htmlData.find("#row_" + val));
                    console.dir(htmlData.find("#row_" + val));
                } else {
                    $(".empty-cart").remove();
                    $(".table-content").append(htmlData.find("#row_" + val))
                }
            });
            eventsCartTableRow();
            if ($("#productDisabled").val()) {
                $('#products_obsolete_msg').show();
            }
            
            if ($(".product-not-available").length > 0 && $('.text-danger:contains(Unavailable)').text().length != 0){
				$('#products_obsolete_msg').show();
			} else {
				$('#products_obsolete_msg').hide();
			}
            
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function refreshOrder(cids, source) {
    var tableOptions = {cids: cids.join(","), source: source};
    if ("review" == source) {
        return $.when(refreshOrderSubtotal(), refreshTableData(cids, tableOptions), refreshCartItemsCount(), refreshApprovalWarn(), refreshSubmitOne(), refreshSubmitTwo());
    } else {
        return $.when(refreshOrderSubtotal(), refreshTableData(cids, tableOptions), refreshCartItemsCount(), refreshApprovalWarn(), $("#availabilityImportantInfo").show());
    }
}

function refreshOrderSubtotal() {
    return $.ajax({
        method: "GET",
        url: "/checkout/includes/cart-order-amount.jsp",
        cache: false,
        dataType: "text",
        success: function (data) {
            if (data) {
                $(".subtotal .text-success").text(data);
                $(".summary-subtotal").text(data);
            } else {
                console.log("Price wasn't received");
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function updateQty(cid, page, skuId, oldQty) {
    var formName = "#update-qty-and-reprice";
    if (page == 'review') {
        formName = "#update-qty-and-reprice-review";
    }


    return connectToSequentialChain(ajaxPostFormSubmitUpdateQty(formName, skuId, oldQty, cid), function () {
        if (page == 'cart') {
            return refreshOrder($.makeArray(cid), "cart");
        } else if (page == 'review') {
            //location.href = "/checkout/review.jsp"
            return refreshOrder($.makeArray(cid), "review");

        }

    });
}

function removeItems(cids) {
    $("#items-map-remove").val(jQuery.map(cids,
        function (val) {
            return val + "=''";
        }).join(","));
    return ajaxPostFormSubmit("#remove-items-form").done(function (data) {
        resetTableCheckboxes();
        jQuery.map(cids, function (val) {
            $("#row_" + val).remove();
        });
        $.when(refreshOrderSubtotal(), refreshCartItemsCount(), refreshApprovalWarn(), refreshSubmitOne(), refreshSubmitTwo());
        reloadMinicartCount();
    });
}

/// Event Listeners ***///

function onAddToMaterialLClick() {
    if ($(".table-content .check :checkbox:checked").length == 0) {
        $("#link-noResultsModal_no_select").click();
        return;
    }
    var itemsInfo = $(".table-content .check :checkbox:checked").parents("tr.content").map(function () {
        var cid = $(".check :checkbox:checked", this).attr("id");
        var qty = $(".qty :text", this).val();
        return cid + "=" + qty;
    }).get().join(",");
    resetTableCheckboxes();
    showAddToMaterialListModal(itemsInfo);
}

function showAddToMaterialListModal(itemsInfo) {
    $("#modal-add-to-material-list-div").empty();

    $.ajax({
        type: "GET",
        url: "/global/modals/modal-add-to-material-list.jsp",
        dataType: 'html',
        data: {itemsInfo: itemsInfo},
        complete: function () {
            $("#modal-add-to-material-list-link").click();
        },
        success: function (data) {
            data = $.trim(data);
            $("#modal-add-to-material-list-div").append(data);
            // eventsModalAddToMaterialList();
            onModalAddToMaterialListLoaded();
            $('.bootstrap-select').selectpicker({
                style: 'form-control',
                size: 7
            });
            $('.create-new-list-btn').on('click', function () {
                $(this).parents('.modal').find('.modal-footer').hide();
                if ($('.create-new-list-collapse').hasClass('in')) {
                    $(this).parents('.modal').find('.modal-footer').show();
                }
            });
        }
    });
}

			
            $('#modal-pickup-input-search').keypress(function (e) {
                if (e.which == 13) {
                    doModalPickup_Search();
                    e.preventDefault();
                    return false;
                }
            });
   

function setDeliveryMethodRadioButtonsActivated() {
    var shippedRB = document.getElementById("shipped-radio");
    var pickUpRB = document.getElementById("pick-up-radio");

    if (shippedRB && pickUpRB) {
        var jspName = getCurrentJSPName();
        var buttonsState = jspName === "review";

        shippedRB.disabled = buttonsState;
        pickUpRB.disabled = buttonsState;
    }
}

function onChangePickUpLocation() {
    $("#select_address_update_sg").val('true');
    doModalCS_SelectPickupAddress(function () {
        $('#changePickupLocation').modal('hide');
        disablePageContent();
        location.reload();
        $.when(
            loadTable("cart", $("#cart-sort-option").val(), $("#cart-sort-option-type").val(),true)
            //wrapLoadToDeferred("#header-cs-selection", "/includes/gadgets/header-cs-selection.jsp", {t:new Date().getTime()}),
        ).then(enablePageContent, enablePageContent).then(function () {
            $("#panel-body-delivery-method").load("/checkout/gadgets/cart-delivery-method.jsp",
                {
                    deliveryMethod: $("#deliveryMethodHidden").val(),
                    addressId: $("#deliveryMethodAddressIdHidden").val(),
                    nocache: (new Date()).getTime()
                }, function () {
                    eventsCartDeliveryMethod();
                });
        });
    });
}

function onReloadBtnClick() {
    window.location.reload(true);
}

function onQtyUpdateClick(elem, page, skuId, oldQty) {
    /*var qty = $($this).parent().find(":text").val();
    var cid = $(".check :hidden", $($this).closest(".content")).val();*/
	var qty = elem.val();
    var cid = elem.attr('data-cid');
    if (!isNumericText(qty) || qty <= 0) {
        showConfirmPopup(skuId, oldQty, "This Item will be deleted from your cart", function () {
            disablePageContent();
            removeItems($.makeArray(cid)).then($.when(refreshApprovalWarn(), refreshSubmitOne(), refreshSubmitTwo()).then(enablePageContent, enablePageContent));
        });
        return;
    }

    $("#items-map-reprice").val(cid + "=" + qty);
    //disablePageContent();

    updateQty(cid, page, skuId, oldQty).then(enablePageContent, enablePageContent);
}

function onQuickAddIClick() {
    if ($('#error-message-connection-JDE').css('display') !== 'none') {
        return;
    }
	
    var qty = $(".quick-add .q-add-qty-input").val();
    var sku = $(".quick-add .q-add-item-input").val();
    if (!isValidAlias(sku)) {
        showAlertPopup();
        return;
    } else if (!isNumericText(qty) || qty <= 0 || qty == "") {
        showAlertQuickAddQtyPopup();
        return;
    }
    var isCCAccountAccepted = $('#isAcceptedCCPayment').val();
	var isCCNotificationShown = $('#isCCNotificationShown').val();
	if(isCCAccountAccepted=='false' && isCCNotificationShown=='true'){
		$('#ccAccountNotificationModal').modal('show');
	} else {
	    $("#items-map-quick-add").val(sku + "=" + qty);
	    $("#quick-add-form").submit();
	}
}

function onChangeShippingAddress() {
    $("#select_address_update_sg").val('true');
    doModalCS_SelectShippingAddress(function () {
        closeModalDialog();
//        location.reload();
        disablePageContent();
        $.when(
            loadTable("cart", $("#cart-sort-option").val(), $("#cart-sort-option-type").val(),true),
            //wrapLoadToDeferred("#header-cs-selection", "/includes/gadgets/header-cs-selection.jsp", {t:new Date().getTime()}),
            wrapLoadToDeferred(".delivery-method-shipped-form .shipping-info-block", "/checkout/gadgets/cart-ship-to-address.jsp")
        ).then(enablePageContent, enablePageContent);
    });
}

function onSelectShippingOnCart() {
	disablePageContent();
	//location.reload();
    $("#deliveryMethodHidden").val('shipped');
    $("#select_address_delivery_method").val('shipped');
    $(".delivery-method-shipped-form").show();
    $(".delivery-method-pick-up-form").hide();
    doSelectShippingOnCart(function () {
         $.when(
            loadTable(getCurrentJSPName(), $("#cart-sort-option").val(), $("#cart-sort-option-type").val(),true)
            //,
            //wrapLoadToDeferred(".delivery-method-shipped-form .shipping-info-block", "/checkout/gadgets/cart-ship-to-address.jsp")
        ).then(enablePageContent, enablePageContent);
        $("#reprice-order").load("/checkout/gadgets/reprice-order.jsp");
    });
}

function doSelectShippingOnCart(successCallback) {
    var dataString = $('#cart-cs-form').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: function (response) {
            if (successCallback) {
                successCallback(response);
            }
        },
        error: ajaxErrorLogin
    });
    return false;
}

function onSelectPickupOnCart() {
	disablePageContent();
	$(".delivery-method-pick-up-form").show();
    $(".delivery-method-shipped-form").hide();
    $("#deliveryMethodHidden").val('pick-up');
    $("#select_address_delivery_method").val('pick-up');
    doSelectPickupOnCart(function () {
        $.when(
            loadTable(getCurrentJSPName(), $("#cart-sort-option").val(), $("#cart-sort-option-type").val(),true)
            //,
            //wrapLoadToDeferred("#pick-up-location", "/checkout/gadgets/cart-default-pickup-location.jsp")
        ).then(enablePageContent, enablePageContent);
        $("#reprice-order").load("/checkout/gadgets/reprice-order.jsp");
    });
}

function doSelectPickupOnCart(successCallback) {
    var dataString = $('#cart-pickup-form').serialize();
    $.ajax({
        type: "POST",
        data: dataString,
        dataType: "json",
        success: function (response) {
            if (successCallback) {
                successCallback(response);
            }
        },
        error: ajaxErrorLogin
    });
    return false;
}

function onCheckAvailabilityClick(ids) {
    var cids = getSelectedCheckboxesCids();
    if (ids) {
    	cids = ids;
    }
   
	if (!ids) {
		cids = $(".table-content .check :checkbox").map(function () {
 	        return this.value;
 	    });
		$('#resetCheckAvailability').val(true);
	}
    $("#items-map-availability").val(cids.map(function () {
        return this + "=''"
    }).get().join(","));
	disablePageContent();
	connectToSequentialChain(ajaxPostFormSubmit("#check-availability-form"), function () {
		return refreshOrder(cids.get(), "cart");
	}).then(enablePageContent, enablePageContent);
}

function submitMoveToPurchase() {
    disablePageContent();

    connectToSequentialChain(ajaxPostFormSubmit("#move-to-purchase-form"), function (data) {
        if (data.error === "true") {
            var onHold = false;
            var properties = data.properties;
            if (properties.length > 0) {
                onHold = (properties[0] == 'onHold');
            }
            if (onHold) {
                return $.when(showOnHoldOnCart(), loadTable("cart", $("#cart-sort-option").val(), $("#cart-sort-option-type").val()));
            } else {
                return $.when(showErrorOnCart(data), loadTable("cart", $("#cart-sort-option").val(), $("#cart-sort-option-type").val()));
            }
        } else {
            window.location = "/checkout/review.jsp";
        }
    }).then(enablePageContent, enablePageContent);
}

function onProceedToCheckoutClick(isTransient) {

    var selectedShippingType = $(".radio-toggle-delivery-method:checked").val();
    var shippingId = $("#shippingId").val();

    if (selectedShippingType === "delivery-method-pick-up") {
        $("#purchase-shipping-info").val($("#pickUpId").val());
        $("#purchase-delivery-method").val("pick-up")
    } else {
        $("#purchase-shipping-info").val($("#shippingId").val());
        $("#purchase-delivery-method").val("shipped")
    }
    $("#purchase-billing-info").val($("#billingId").val());

    if (isTransient) {
        window.defaultLoginUrl = "/checkout/cart.jsp";
        showLoginGuestModal(function () {
            submitMoveToPurchase();
            window.defaultLoginUrl = "/";
        });
    } else {
    	var isCartPage = $("#cartPage").val();         
        if(isCartPage == 'true'){            
             $('#defaultShipToAddress').modal('show');
        }
        var isCCAccountAccepted = $('#isAcceptedCCPayment').val();
 		var isCCNotificationShown = $('#isCCNotificationShown').val();
 		if(isCCAccountAccepted=='false' && isCCNotificationShown=='true'){
 			$('#ccAccountNotificationModal').modal('show');
 		} else {
 			ajaxPostFormSubmit("#update-schedule-days");
 	        submitMoveToPurchase();
 		}
    }
}

function showOnHoldOnCart() {

    return $.ajax({
        type: "GET",
        url: "/global/modals/modal-on-hold.jsp",
        dataType: 'html',
        data: ({action: 'loadModalDiv'}),
        complete: function () {
            $("#link-onHoldAccount").click();
        },
        success: function (data) {
            data = $.trim(data);
            $("#modal-on-hold-show-div").html(data);
        }
    });

}

function showErrorOnCart(data) {

    var errors = data.errors;
    var error_messages = "";
    for (var i = 0; i < errors.length; i++) {
        if ("" == error_messages) {
            error_messages = errors[i];
        } else {
            error_messages = error_messages + "<br />" + errors[i];
        }
    }
    $("#modal-title-error").html(error_messages);
    $("#modalError").modal("show");

}

function processSubmitErrors(data) {
    if (data.errors) {
        $(".alert-danger p").empty();
        $(".alert-danger").not('.jde-error').css('display', 'block');
        $(".alert-danger").not('.jde-error').show();
        while (data.errors.length) {
            var errMsg = data.errors.pop();
            $(".alert-danger p").append("<br/>" + errMsg);
        }
        if (data.properties) {
            for (var i = 0; i < data.properties.length; i++) {
                var property = data.properties[i];
                if (property === 'lastName' || property === 'firstName') {
                    property = 'contactName';
                }
                $("#" + property).closest(".form-group").addClass('has-error');
            }
        }
    } else {
        window.location.reload(true);
    }
}

function submitCommitOrder() {
    var paymentMethod = $("input[name=checkout-payment]:checked").val();
    if (paymentMethod == undefined) {
        paymentMethod = "onAccount";
    }

    $("#commit-order-po").val($("#commit-order-po-value").val());
    $("#commit-order-onsite-name").val($("#commit-order-onsite-name-value").val());    
    $("#commit-order-onsite-number").val($("#commit-order-onsite-phone-value").val());
    $("#commit-order-method").val(paymentMethod);
    $("#commit-order-include-mtr").val($("#order-mtr-inc").checkbox("isChecked"));
    $("#commit-order-jobName").val($("#commit-order-job-name-value").val());

    var validForm = true;
    validForm = checkPOnumber();

    if (validForm) {
        ajaxPostFormSubmit("#commit-order").done(function (data) {
            if (data.error === "true") {
            	$("html, body").animate({scrollTop: 0}, "slow");
                processSubmitErrors(data);
            } else {
                window.location = "/checkout/confirmation.jsp";
            }
        });
    }
}

function checkPOnumber() {
    var validForm = true;
    $(".alert-danger p").empty();
    $("#po-empty").css('display','none');
    $("#po-toolong").css('display','none');
    var poNumber = $("#commit-order-po-value").val();
    $(".alert-danger").not('.jde-error').hide();
    if (poNumber == null || poNumber.length == 0) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $(".alert-danger").not('.jde-error').css('display', 'block');
        $("#po-empty").css('display', 'block');
        $("#po-toolong").css('display', 'none');
        validForm = false;
    } else if (poNumber.length > 30) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $(".alert-danger").not('.jde-error').css('display', 'block');
        $("#po-toolong").css('display', 'block');
        $("#po-empty").css('display', 'none');
        validForm = false;
    }

    return validForm;
}
function checkOnsiteContact() {
    var validForm = true;
    $("#onsite-name-empty").css('display','none');
    $("#onsite-name-toolong").css('display','none');
    $("#onsite-phone-empty").css('display','none');
    $("#onsite-phone-toolong").css('display','none');
    
    var onsiteName = $("#commit-order-onsite-name-value").val();
    var onsitePhone = $("#commit-order-onsite-phone-value").val();

    $(".alert-danger").not('.jde-error').hide();
    if (onsiteName == null || onsiteName.length == 0) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $(".alert-danger").not('.jde-error').css('display', 'block');
        $("#onsite-name-empty").css('display', 'block');
        $("#onsite-name-toolong").css('display', 'none');
        validForm = false;
    } else if (onsiteName.length > 30) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $(".alert-danger").not('.jde-error').css('display', 'block');
        $("#onsite-name-toolong").css('display', 'block');
        $("#onsite-name-empty").css('display', 'none');
        validForm = false;
    }
    if (onsitePhone == null || onsitePhone.length == 0) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $(".alert-danger").not('.jde-error').css('display', 'block');
        $("#onsite-phone-empty").css('display', 'block');
        $("#onsite-phone-toolong").css('display', 'none');
        validForm = false;
    } else if (onsitePhone.length > 30) {
        $("html, body").animate({scrollTop: 0}, "slow");
        $(".alert-danger").not('.jde-error').css('display', 'block');
        $("#onsite-phone-toolong").css('display', 'block');
        $("#onsite-phone-empty").css('display', 'none');
        validForm = false;
    }

    return validForm;
}

function submitCheckoutAddress() {
    $("#stateHidden").val($("#state").val());

    $("#checkout-address-form .form-group").removeClass('has-error');
    $(".alert-danger p").empty();
    $(".alert-danger").not('.jde-error').hide();

    return ajaxPostFormSubmit("#checkout-address-form").done(function (data) {
        if (data.errors && data.errors.length > 0) {
            $("html, body").animate({scrollTop: 0}, "slow");
            processSubmitErrors(data);
        }
        return data;
    });
}

function expandPaymentSection() {
    $("#checkout-payment-cc").load("/checkout/includes/credit-card-iframe.jsp", {
        nocache: (new Date()).getTime()
    }, function () {
        var controls = $("#panel-delivery-method .form-control, .btn-next-to-payment");
        controls.attr('disabled', 'disabled');
        controls.addClass("disabled");
    });
}

function onNextButtonClick() {
    var deliveryMethod = $("#deliveryMethodHidden").val();

    if (deliveryMethod == "shipped") {
        submitCheckoutAddress().done(function (data) {
            if (data.code !== "error") {
                if (checkPOnumber()) {
                    updatePONumber().done(function (data) {
                        if (data.code !== "error") {
                            expandPaymentSection();
                        }
                    });
                }
            }
        });
    } else {
        if (checkPOnumber()) {
            updatePONumber().done(function (data) {
                if (data.code !== "error") {
                    expandPaymentSection();
                }
            });
        }
    }
}

function onPlaceOrderClick() {
    var isTransient = $("#isTransientHidden").val();
    var deliveryMethod = $("#deliveryMethodHidden").val();
    if (isTransient === "true" && deliveryMethod === 'shipped') {
        submitCheckoutAddress().done(function (data) {
            if (data.code !== "error") {
                if (checkPOnumber()) {
                    submitCommitOrder();
                }
            }
        });
    } else {
		if (checkPOnumber() && checkOnsiteContact()) {
            submitCommitOrder();
        }
    	
    }
}

function onRemoveSelectedClick() {
    var cids = getSelectedCheckboxesCids();
    if (cids.length == 0) {
        showAlertNotSelectedPopup();
        return;
    }

    showRemoveSelectedFromCartPopup(function () {
        disablePageContent();
        removeItems(cids).then(function () {
            enablePageContent();
            if ($('.product-not-available').length == 0) {
                $('#products_obsolete_msg').hide();
            }
        }, enablePageContent);
    });
}

//**** Validation ***************///

function initValidation() {

    addTextInputKeydownValidation("body", ":text.qty-input", function (e, val) {
        return isNumericKeyPressed(e) && val.length < 5;
    });

    addTextInputKeydownValidation("body", ":text.alphanumeric", function (e, val) {
        return isAlphaNumericKeyPressed(e) && val.length < 30;
    });

    (function () {
        var prevKeyCode = undefined;
        addTextInputKeydownValidation("body", ":text.q-add-item-input", function (e, val) {
            if (e.keyCode == 173) {
                if (prevKeyCode == e.keyCode) {
                    return false;
                } else {
                    prevKeyCode = e.keyCode;
                }
            } else {
                prevKeyCode = undefined;
            }
            return (e.keyCode == 173 || isAlphaNumericKeyPressed(e)) && val.length < 30;
        });
    })();

    addPasteValidation("body", ":text.q-add-item-input", isValidAlias, function (val) {
        var trimmedVal = val.trim();
        return trimmedVal.length > 30 ? trimmedVal.substring(0, 30) : trimmedVal;
    });
    addPasteValidation("body", ":text.alphanumeric", isAlphaNumericText, function (val) {
        var trimmedVal = val.trim();
        return trimmedVal.length > 30 ? trimmedVal.substring(0, 30) : trimmedVal;
    });
    addPasteValidation("body", ":text.qty-input", isNumericText, function (val) {
        var trimmedVal = val.trim();
        return trimmedVal.length > 5 ? trimmedVal.substring(0, 5) : trimmedVal;
    });
}

function addPasteValidation(delegate, element, validator, formatter) {
    $(delegate).on("paste", element, function (e) {
        var pastedText = extractPastedText(e);
        if (e.preventDefault) {
            e.stopPropagation();
            e.preventDefault();
        }
        if (pastedText) {
            var value = formatter(pastedText);
            if (validator(value)) {
                this.value = value;
            }
        }
    });
}

function addTextInputKeydownValidation(delegate, element, validator) {
    $(delegate).on("keydown", element, function (e) {
        if (!isAllowTextManipulationCombination(e)) {
            if (!validator(e, this.value)) {
                e.preventDefault();
            }
        }
    });
}

function isValidAlias(alias) {
    return /^[a-z0-9]*([a-z0-9]+\-?)*[a-z0-9]+$/i.test(alias);
}

function isNumericKeyPressed(e) {
    return !((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105));
}

function isLowerAlphaKeyPressed(e) {
    return e.keyCode > 64 && e.keyCode < 91;
}

function isUpperAlphaKeyPressed(e) {
    return e.keyCode > 96 && e.keyCode < 123;
}

function isAlphaKeyPressed(e) {
    return isLowerAlphaKeyPressed(e) || isUpperAlphaKeyPressed(e);
}

function isAlphaNumericKeyPressed(e) {
    return isNumericKeyPressed(e) || isAlphaKeyPressed(e);
}

function isAlphaNumericText(value) {
    return /^[a-z0-9]+$/i.test(value);
}

function isNumericText(value) {
    return /^\d+$/i.test(value);
}

function extractPastedText(e) {
    if (window.clipboardData && window.clipboardData.getData) { // IE
        return window.clipboardData.getData("Text");
    } else if (e.clipboardData && e.clipboardData.getData) {
        return e.clipboardData.getData("text/plain");
    } else if (e.originalEvent && e.originalEvent.clipboardData && e.originalEvent.clipboardData.getData) {
        return e.originalEvent.clipboardData.getData("text/plain");
    }
}

function isAllowTextManipulationCombination(e) {
    // Allow: backspace, delete, tab, escape, enter and
    return $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: Ctrl+V
        (e.keyCode == 86 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)
}

// *** Popup ***//

function showConfirmPopup(skuId, oldQty, msg, callback) {
    $("#confirmCartPopup .modal-title").text(msg);
    $("#confirmCartPopup").modal("show");
    if (callback) {
        $('#confirmCartPopup').one('hidden.bs.modal', function (e) {
            $("#confirmCartPopup .modal-body .btn-primary").off("click.callback");
            $("#qty-input-" + skuId).val(oldQty);
        });
        $("#confirmCartPopup .modal-body .btn-primary").one("click.callback", function (e) {
            $("#confirmCartPopup").modal("hide");
            callback(e);
        });
    }
}

function showRemoveSelectedFromCartPopup(callback) {
    $("#removeFromCart").modal("show");
    if (callback) {
        $('#removeFromCart').one('hidden.bs.modal', function (e) {
            $("#removeFromCart .modal-body .btn-primary").off("click.callback");
        });
        $("#removeFromCart .modal-body .btn-primary").one("click.callback", function (e) {
            $("#removeFromCart").modal("hide");
            callback(e);
        });
    }
}

function showAlertPopup() {
    $("#alertCartPopup").modal("show");
    resetTableCheckboxes();
}

function showAlertQuickAddQtyPopup() {
    $("#alertQuickAddQty").modal("show");
    resetTableCheckboxes();
}

function showAlertNotSelectedPopup() {
    $("#alertNotSelected").modal("show");
    resetTableCheckboxes();
}

function showAlertItemNotSelectedPopup() {
    $("#alertItemNotSelected").modal("show");
    resetTableCheckboxes();
}

function addSelectedToCart() {
    var itemIds = gatherChecked();
    if (itemIds.length > 0) {
        var qtyStr = "";
        for (i = 0; i < itemIds.length; i++) {
            if (i == 0)
                qtyStr = $("#" + itemIds[i] + "_qty").val();
            //qtyStr = "1";
            else
                qtyStr += "," + $("#" + itemIds[i] + "_qty").val();
            //qtyStr += ",1";
        }
        $("#addSkuIdsListGL").val(itemIds);
        $("#addQtyListGL").val(qtyStr);
        //$("#giftIds").val(true);

        dataString = $("#order-details-add").serialize();
        $.ajax({
            method: "POST",
            cache: false,
            data: dataString,
            dataType: "json",
            success: function (data) {
                if (data.error === "true") {

                } else {
                    return $.when(reloadMiniCartCountAndUncheckSelectedCheckbox(), addedToCartHangerShow());
                }
            }
        });
    } else {
        showAlertItemNotSelectedPopup();
    }
}

function addSelectedToReorder() {
    var itemIds = gatherChecked();
    if (itemIds.length > 0) {
        var qtyStr = "";
        for (i = 0; i < itemIds.length; i++) {
            if (i == 0)
                qtyStr = $("#" + itemIds[i] + "_qty").val();
            //qtyStr = "1";
            else
                qtyStr += "," + $("#" + itemIds[i] + "_qty").val();
            //qtyStr += ",1";
        }
        $("#addSkuIdsListGLAuto").val(itemIds);
        $("#addQtyListGLAuto").val(qtyStr);
        var scheduleDays = $("#schedule-days").val();
        $("#scheduleDays").val(scheduleDays);
        //$("#giftIds").val(true);

        dataString = $("#auto-order-details-add").serialize();
        $.ajax({
            method: "POST",
            cache: false,
            data: dataString,
            dataType: "json",
            success: function (data) {
                if (data.error === "true") {

                } else {
                    return $.when(reloadMiniCartCountAndUncheckSelectedCheckbox(), addedToCartHangerShow());
                }
            }
        });
    } else {
        showAlertItemNotSelectedPopup();
    }
}

function reloadMiniCartCountAndUncheckSelectedCheckbox() {
    $.ajax({
        url: "/includes/gadgets/navbar-cart.jsp",
        cache: false
    }).done(function (html) {
        $.each($(':checkbox'), function () {
            $(this).removeAttr("checked");
        });
        $("#navbar-cart .nav-cart").empty();
        $("#navbar-cart .nav-cart").html(html);
    });
}

function changeCartSorting(sortOption, page) {

    //wrapLoadToDeferred(".table-content", "/checkout/includes/cart-items-table.jsp", {source:source}).done(function(){
    $("#sort-option-item-number").attr("class", "fa fa-sort");
    $("#sort-option-alias-number").attr("class", "fa fa-sort");
    $("#sort-option-price").attr("class", "fa fa-sort");
    $("#sort-option-description").attr('class', "fa fa-sort");

    var currentSortOption = $("#cart-sort-option").val();
    var currentSortOptionType = $("#cart-sort-option-type").val();

    if (currentSortOption == "" || !currentSortOption) {
        $("#cart-sort-option").val(sortOption);
        $("#" + sortOption).attr("class", "fa fa-sort-asc");
    } else if (currentSortOption == sortOption) {
        if (currentSortOptionType == "asc") {
            $("#cart-sort-option-type").val("desc");
            $("#" + sortOption).attr("class", "fa fa-sort-desc");
        } else {
            $("#cart-sort-option-type").val("asc");
            $("#" + sortOption).attr("class", "fa fa-sort-asc");
        }
    } else {
        $("#cart-sort-option").val(sortOption);
        $("#cart-sort-option-type").val("asc");
        $("#" + sortOption).attr("class", "fa fa-sort-asc");
    }
    disablePageContent();
    loadTable(page, $("#cart-sort-option").val(), $("#cart-sort-option-type").val()).then(enablePageContent, enablePageContent);
}

function scheduledDaysChange(checkboxLabel, ciId) {
    var checkedClass = "checked";
    $("label[name=scheduled_days]").removeClass(checkedClass);
    checkboxLabel.classList.add(checkedClass);
    if (checkboxLabel.getAttribute("value") === 'every' && document.getElementById("scheduled-days-field-" + ciId) !== null) {// && document.getElementById("yes_tax_exempt") === null) {
        document.getElementById("scheduled-days-field-" + ciId).removeAttribute("disabled");
    }
    if (checkboxLabel.getAttribute("value") === 'once' && document.getElementById("scheduled-days-field-" + ciId) !== null) {
        document.getElementById("scheduled-days-field-" + ciId).setAttribute("disabled", "disabled");
    }
    $("#cu_input_tax_exempt").val(checkboxLabel.getAttribute("value"));
}

function onScheduleDaysUpdateClick(skuId) {
    var id_schedule_field = "#scheduled-days-field-" + skuId;
    var newScheduleDays = $(id_schedule_field).val();
    $("#items-map-schedule").val(skuId + "=" + newScheduleDays);
    disablePageContent();

    // updateQty(cid, page).then(enablePageContent, enablePageContent);
}

function getCurrentJSPName() {
    var url = window.location.pathname;
    return url.substring(url.lastIndexOf('/') + 1, url.lastIndexOf('.'));
}

$(document).ready(function () {
    $("#checkout-address-form .form-control").on('input', function (event) {
        var formGroup = $(event.target).closest('.form-group');
        if (formGroup.hasClass('has-error')) {
            formGroup.removeClass('has-error');
        }
    });
    var state = $("#stateHidden").val();
    $("#state").val([state]);
});

$(document).ready(function () {
    $(".btn-next-to-payment").click(onNextButtonClick);
    $("[data-event-blur-id=checkoutOnBlurPOnumber]").on('blur', function (event) {
        updatePONumber();
    });
});