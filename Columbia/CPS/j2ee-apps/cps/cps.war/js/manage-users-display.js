function searchUsers(){
	document.getElementById("clear-button").disabled = false; 
	var searchTerm = $("#searchValue").val();
	var role = $("#role").val();
	var currentSort = $("#currentSort").val();
	var currentOrder = $("#currentOrder").val();
	loadDisplay(searchTerm, currentSort, currentOrder, 1, role);
}
function sortPage(sort, order){
	var searchTerm = $("#searchValue").val();
	var role = $("#role").val();
	var currentPage = $("#currentPage").val();
	loadDisplay(searchTerm, sort, order, currentPage, role);
}
function changePage(page){
	var searchTerm = $("#searchValue").val();
	var role = $("#role").val();
	var currentSort = $("#currentSort").val();
	var currentOrder = $("#currentOrder").val();
	loadDisplay(searchTerm, currentSort, currentOrder, page, role);
}

function loadDisplay(search, so, o, p, r){
	var url = "/account/gadgets/manage-users-list.jsp";
	var params = "searchTerm="+encodeURIComponent(search)+"&sort="+encodeURIComponent(so)+"&page="+encodeURIComponent(p)+
				"&order="+encodeURIComponent(o)+"&role="+encodeURIComponent(r);
	$("#user-list").load(url+"?"+params, function () {
		eventsManageUsersList();
    });
}

function notifyUserOrder(userId) {
    
    var url = window.location.href;
    if (url.indexOf(window.location.pathname) > -1) {
        url = url.substr(url.indexOf(window.location.pathname));
    }

    var dataString = $('#notifyUserOrders').serialize();
    $.ajax({
        url: $('#notifyUserOrders').attr('action'),
        type: "POST",
        data: dataString,
        dataType: "json",
        success: notifyUserOrderSuccess,
        error: notifyUserOrderFailure
    });
    return false;
}

function notifyUserOrderSuccess(data) {
    if ('success' === data.code) {
        console.log(data.code);
    }
    else {
        console.log(data.code);
        var propertiesArray = [];
        var errorsArray = [];
        for (var i = 0; i < data.errors.length; i++) {
            errorsArray.push(data.errors[i]);
            propertiesArray.push(data.properties[i]);
        }
        for (var i = 0; i < propertiesArray.length; i++) {
            var errorMessageSmall = $("#err-message-" + propertiesArray[i]);
            errorMessageSmall.html(errorsArray[i]);
            errorMessageSmall.show();
            errorMessageSmall.parent().addClass("has-error");

        }
    }
}

function notifyUserOrderFailure(x, s, e) {
//    console.log("xhr: " + x + "; s: " + s + "; e: " + e);
}

$(document).ready(function(){
	$("#clear-button").click(function(){
        location.href='/account/manage-users.jsp';
	});
	//SM-345 Account notification option
    $('#selectAllUsersNotification').click(function (event) {
        if (this.checked) {
            $('.userNotification').each(function() {
                $(this).prop('checked', true);
            });
        } else {
            $('.userNotification').each(function() {
                $(this).prop('checked', false);
            });
        }
        submitOrderNotification();
    });

    // If admin notification checkbox is checked then only User's notification checkbox will be checked.
    $('#adminNotification').click(function (event){
        if(this.checked){
            $('#selectAllUsersNotification').prop('disabled', false);
            $('.userNotification').each(function() {
                $(this).prop('checked', false);
                $(this).prop('disabled', false);
            });
        } else {
            $('.userNotification').each(function() {
                $(this).prop('checked', false);
                $(this).prop('disabled', true);
            });
            $('#selectAllUsersNotification').prop('checked', false);
            $('#selectAllUsersNotification').prop('disabled', true);
        }
        submitOrderNotification();
    });

    //if all users checkbox is checked, header checkbox will be checked otherwise it will be unchecked 
    $('.userNotification').click(function() {
    	if($(".userNotification").length == $(".userNotification:checked").length) {
    		$("#selectAllUsersNotification").prop("checked", true);
    	}else {
            $("#selectAllUsersNotification").prop("checked", false);            
        }
        submitOrderNotification();
    });

    //form submit 
    function submitOrderNotification(){
        var userIds = gatherListCheckedByName('checkUserOrder');
        console.log("User Ids : "+userIds);
        $("#notify-userId").val(userIds);
        notifyUserOrder(userIds);
    }
    
    var userIds = gatherListCheckedByName('checkUserOrder');
    if (userIds.length != 0) {
     $('#adminNotification').prop('checked', true);
    }else{
                $('.userNotification').prop('disabled', true);
    $('#selectAllUsersNotification').prop('disabled', true);
    }
    if ($('.userNotification:checked').length == $('.userNotification').length) {
    $('#selectAllUsersNotification').prop('checked', true);
    } 
    //SM-345 end 
});