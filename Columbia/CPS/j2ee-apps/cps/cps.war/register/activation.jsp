<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<cp:pageContainer page="activation">
		<main id="body">
		<div class="container">
			<ol class="breadcrumb">
				<li class="active">Email confirmation</li>
			</ol>
			<h1 class="mb20">Email confirmation</h1>
			<dsp:form name="activateUser">
				<dsp:input type="hidden" iclass="form-control" bean="ProfileFormHandler.activationToken" id="email" value="${param.token}"/>
				<dsp:input id="submitConfirmation" iclass="hidden" type="submit" value="Submit" bean="ProfileFormHandler.activateUser" priority="-10"/>
			</dsp:form>
		</div>
		</main>
	</cp:pageContainer>
	<script nonce="${requestScope.nonce}">
		$(document).ready(function() {
			$("#submitConfirmation").click();
		});
	</script>
</dsp:page>