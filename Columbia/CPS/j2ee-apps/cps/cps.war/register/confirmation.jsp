<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean bean="/cps/content/service/ContentPagesService" />
	<dsp:importbean bean="/cps/util/CPSSessionBean"/>
	<dsp:importbean bean="/atg/endeca/assembler/droplet/InvokeAssembler" />

	<dsp:getvalueof var="preRegisterPage" bean="CPSSessionBean.preRegisterPage"/>

	<dsp:getvalueof var="video" bean="ContentPagesService.requestAccessLearnVideo" />
	
	<dsp:getvalueof var="isAppNeed" param="appNeed"/>
	<cp:pageContainer page="confirmation">
		
		<main id="body">
		<nav id="user_thank" class="alert-success">
			<div id="message" class="container" style="margin-top: -30px;padding: 40px;">
				<h2><span class="glyphicon glyphicon-ok-sign"></span> Thank You For Registering</h2>
				<h4 class="text-nocase"><strong>Please follow the link on the confirmation email to start using our site.</strong></h4>
				<p>For questions, please contact us at 773-927-6600.</p>
			</div>
		</nav>
		
		<div class="container">
			<div id="activatedUser" class="alert alert-danger" style="display: none">
				<p>You already used registration link to complete registration. Please login to continue.</p>
			</div>
			<c:if test="${isAppNeed}">
				<section>
					<div id="message" class="start-credit-app">
					    <h3 class="text-nocase mb10">Get more from your online account and apply for a line of credit with Columbia Pipe</h3>
					     
					    <div class="row">
					        <div class="col-md-9 mb10 col-xs-12">
					            If you`re interested in establishing a credit account with Columbia Pipe &amp; Supply, please submit a credit application and someone from our team will contact you within two business days
					        </div>
					        <div class="col-md-3 col-xs-12">
					            <a href="#" class="btn btn-primary xs-block" data-event-click-id="onStartCreditApp">Start Credit App</a>
					        </div>
					    </div>
					</div>
				</section>
			</c:if>
			<c:set var="contentCollection" value="/content/Shared/Confirmation" />			
			<dsp:droplet name="InvokeAssembler">
				<dsp:param name="contentCollection" value="${contentCollection}" />
				<dsp:oparam name="output">
					<dsp:getvalueof var="content" vartype="com.endeca.infront.assembler.ContentItem" param="contentItem" />
					<dsp:renderContentItem contentItem="${content}" />					
				</dsp:oparam>
			</dsp:droplet>
			
			
			<section id="confirmationLinks">
				<div class="text-center">
					<a href="/" class="btn btn-primary xs-block" >Return to site</a><br>
					<%-- <c:choose>
						<c:when test="${not empty preRegisterPage}">
							<a href="${preRegisterPage}" style="color: #005c35;">Continue Shopping</a>
						</c:when>
						<c:otherwise>
							<a href="/" style="color: #005c35;">Continue Shopping</a>
						</c:otherwise>
					</c:choose> --%>
				</div>
			</section>
			
			<dsp:form id="activateUser" formid="activateUser">
				<dsp:input type="hidden" bean="ProfileFormHandler.activationToken" id="email" value="${param.token}"/>
				<dsp:input type="hidden" value="true" bean="ProfileFormHandler.activateUser" />
			</dsp:form>
			<c:if test="${not empty param.token}">
				<script nonce="${requestScope.nonce}">
					handleActivateUser();
				</script>
			</c:if>
		</div>
		</main>
	</cp:pageContainer>
</dsp:page>