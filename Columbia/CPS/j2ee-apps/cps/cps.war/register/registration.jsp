	<dsp:page>
		<dsp:importbean var="originatingRequest" bean="/OriginatingRequest" />
		<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
		<dsp:getvalueof var="isShowCaptcha" bean="ProfileFormHandler.showCaptchaReCaptcha" />
		<dsp:importbean bean="/cps/userprofiling/GuestRegistrationFormHandler" />
		<dsp:importbean bean="/atg/userprofiling/ProfileTools"/>
	
		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:getvalueof var="isTransient" bean="Profile.transient" />
		<c:if test="${!isTransient}">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${originatingRequest.contextPath}/index.jsp" />
			</dsp:droplet>
		</c:if>
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
			<dsp:param bean="ProfileTools.allowNewUserSelfRegistration" name="value" />
			<dsp:oparam name="false">
				<dsp:droplet name="/atg/dynamo/droplet/Redirect">
					<dsp:param name="url" value="${originatingRequest.contextPath}/" />
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
		
		<cp:pageContainer page="registration">
			<main id="body">
			<div class="container">
				<ol class="breadcrumb">
					<li class="active">Registration</li>
				</ol>
				<h1 class="mb20">Register for Online Access</h1>
				<section>
					<div class="row">
						<div class="col-sm-2 hidden-xs">
							<div class="icon-large">
								<i class="fa fa-building"></i>
							</div>
						</div>
						<div class="col-md-6 col-sm-10">
							<h4 class="text-nocase">
								Register to do business with us in a few easy steps.
								<br>
								As a registered Columbia Pipe user, you will be able to:
							</h4>
							<ul>
								<li>Conveniently look-up invoices, monthly statements, order history, and packing slips</li>
								<li>View real-time item availability specific to your location</li>
								<li>Manage Material Lists to store, share, and reorder products</li>
							</ul>
						</div>
						<div class="col-md-4 col-sm-12" style="background-color: #ffc8d544; display:none;">
							<h4 class="text-nocase">
								Not Ready to Register?
							</h4>
							You can still browse our products as a guest.<br>
							<button id="continueAsGuestBtn" class="btn btn-primary btn-lg mt5 mb15" type="button">Continue as Guest</button>						
						</div>
					</div>
				</section>
				
			<div id="error-messages-create-user"></div>
	
			<section>
				<h4 class="text-nocase">Does your company have an account with Columbia Pipe &amp; Supply?</h4>
	
			<div class="radio"><label><input type="radio" name="reg-status" value="hasaccount" checked data-event-click-id="register0" > Yes, my company has a Columbia Pipe line of credit.</label>
			</div>
			
			<div class="radio"><label><input type="radio" name="reg-status" value="noaccount" data-event-click-id="register1" > No, my company does not have a Columbia Pipe line of credit.</label>
			</div>
			<p><strong>* Please note that you need to be a licensed business in order to purchase from us.</strong>
			</p>
			</section>
		
			<dsp:form method="post" id="create-user-form" formid="create-user-form">
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.firstName" id="createUserForm_firstName"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.lastName" id="createUserForm_lastName"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.accountNumber" id="createUserForm_accountNumber"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.zipCode" id="createUserForm_zipCode"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.email" id="createUserForm_email"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.password" id="createUserForm_password"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.confirmedPassword" id="createUserForm_confirmedPassword"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.supervisorName" id="createUserForm_supervisorName"/>
				<dsp:input type="hidden" value="" bean="ProfileFormHandler.supervisorEmail" id="createUserForm_supervisorEmail"/>
				
				<dsp:input type="hidden" bean="ProfileFormHandler.create" value="true" priority="-10"/>
			</dsp:form>
			
			<dsp:form method="post" id="create-guest-user-form" formid="create-guest-user-form">
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.firstName" id="createGuestUserForm_firstName"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.lastName" id="createGuestUserForm_lastName"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.companyName" id="createGuestUserForm_companyName"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.phoneNumber" id="createGuestUserForm_phoneNumber"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.companyBillingAddress" id="createGuestUserForm_companyBillingAddress1"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.address2" id="createGuestUserForm_address2"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.city" id="createGuestUserForm_city"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.state" id="createGuestUserForm_state"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.zipCode" id="createGuestUserForm_zipCode"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.email" id="createGuestUserForm_email"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.password" id="createGuestUserForm_password"/>
				<dsp:input type="hidden" value="" bean="GuestRegistrationFormHandler.confirmedPassword" id="createGuestUserForm_confirmedPassword"/>
				
				<dsp:input type="hidden" bean="GuestRegistrationFormHandler.create" value="true" priority="-10"/>
			</dsp:form>
			<div id="hideNoAccountForm">
			<div class="row">
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">First Name*</label>
					<input type="text" class="form-control" id="firstName" maxlength="50">
				</div>
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Last Name*</label>
					<input type="text" class="form-control" id="lastName" maxlength="50">
				</div>
			</div>
			
			<div class="reg-options" id="reg-options-hasaccount">
			<div class="row">
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Account Number*
					<i   title="<fmt:message key='account.register.info'/>" class="gold glyphicon glyphicon-info-sign" ></i>
					</label>
					<input type="text" class="form-control" id="accountNumber" maxlength="6">
				</div>
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Company Billing Zip Code*</label>
					<input type="text" class="form-control" id="zip" maxlength="10">
				</div>
				<input type="hidden" value="${isShowCaptcha}" id="show-captcha" />
				
			</div>
			<div class="row">
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Supervisor Name*</label>
					<input type="text" class="form-control" id="supervisorName" maxlength="40">
				</div>
				<div class="form-group col-sm-6 col-xs-12">
					<label id="regSupervisorEmail" class="control-label">Supervisor Email Address*</label>
					<input type="email" class="form-control" id="supervisorEmail" maxlength="50">
				</div>
				
			</div>
			</div>
			<div class="reg-options" id="reg-options-noaccount">
			<div class="row">
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Company Name*</label>
					<input type="text" class="form-control" id="companyName" maxlength="50">
				</div>
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Phone Number*</label>
					<input type="text" class="form-control" id="phoneNumber" maxlength="50">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Company Billing Address 1*</label>
					<input type="text" class="form-control" id="address1" maxlength="50">
				</div>
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">Address 2</label>
					<input type="text" class="form-control" id="address2" maxlength="50">
				</div>
			</div>
			<div class="row">
				<div class="form-group col-sm-6 col-xs-12">
					<label class="control-label">City</label>
					<input type="text" class="form-control" id="city" maxlength="50">
				</div>
				<div class="form-group col-sm-2 col-xs-4">
					<label class="control-label">State</label>
					<select class="form-control" id="state">
						<option disabled selected>Select...</option>
						<%@include file="/includes/states-html.jspf"%>
					</select>
				</div>
				<div class="form-group col-sm-4 col-xs-8">
					<label class="control-label">Zip Code</label>
					<input type="text" class="form-control" id="zipCode" maxlength="10">
				</div>
			</div>
			</div>
			
			<div id="register-form-password">
			    <div class="row">
				    <div class="form-group col-sm-6 col-xs-12">
					    <label id="registrationEmail" class="control-label">Your Valid Business Email Address*</label>
					    <input type="email" class="form-control" id="email" maxlength="50">
				    </div>
			    </div>
			    <div class="row">
				    <div class="form-group col-sm-6 col-xs-12">
			            <label class="control-label">Password*</label>
					    <input type="password" autocomplete="new-password" class="form-control" id="password" minlength="6" maxlength="15">
				    </div>
				    <div class="form-group col-sm-6 col-xs-12">
					    <div class="help-block-reg">Password should contain at least 6 characters, at least 1 uppercase alphabetic character, and at least 1 numeric character and at least 1 special character (examples: !, @, #, $, %, &, *, _).</div>
				    </div>
			    </div>
				<div class="row">
					<div class="form-group col-sm-6 col-xs-12">
				        <label class="control-label">Confirm Password*</label>
						<input type="password" autocomplete="new-password" class="form-control" id="confirmPassword" minlength="6" maxlength="15">
					</div>
				</div>
			</div>
				
			<p>by registering, I accept the Columbia Pipe <a href="/termsOfUse">Terms of Use</a>.</p>
			
			<%--ReCaptcha section--%>
	
				<c:if test="${isShowCaptcha}">
					<div id="register-form-captcha">
						<div class="form-group">
							<div class="row">
								<div class="col-sm-6">
									<div id="recaptchaRegister"></div>
								</div>
							</div>
						</div>
					</div>
				</c:if>
			
			<p id="regButton"><button class="btn btn-lg btn-primary xs-block" data-event-click-id="register2" >Register for Online Access</button></p>
			<p id="submitButton" style="display:none;"><button class="btn btn-lg btn-primary xs-block" data-event-click-id="register2" >Submit</button></p>
			</div>
			<div class="alert alert-danger" id="noAccountError">
			To set up a Columbia Pipe line of credit please <br/> contact our sales team at 773-927-6600.		
			</div>
			<div class="padding-5">
				<a class="btn btn-primary xs-block" href="/global/credit/application.jsp">Credit Application</a>
			</div>
			</div>
			</main>
	
			<script src="https://www.google.com/recaptcha/api.js?onload=renderCaptchaCallback&render=explicit" async defer></script>
	
			<script type="text/javascript" nonce="${requestScope.nonce}">
				$(document).ready(function() {
					checkSubmitButton();
				});
	
				$("#phoneNumber").mask("(999) 999-9999");
				
				var register_captcha_success_default = false;
				var register_captcha_default = null;
	
				function checkSubmitButton() {
					if ($('#show-captcha').val() == "true") {
						var enableLoginButton = true;
						if ($('#register-form-captcha').length > 0) {
							enableLoginButton = register_captcha_success_default
									&& $('#accountNumber').val().length > 0
									&& $('#zip').val().length > 0;
						}
	
						if (enableLoginButton) {
							$('#submit-form').removeAttr('disabled');
						} else {
							$('#submit-form').attr('disabled', 'disabled');
						}
					}
				}
	
				function renderCaptchaCallback() {
					var sitekey = '6LcLQAwTAAAAAHzijxYeVfo71BNXYF6t_0YYf04l';
					register_captcha_default = grecaptcha.render(
							'recaptchaRegister', {
								'sitekey' : sitekey,
								'callback' : validateRecaptchaDefault,
								'expired-callback' : expireRecaptchaDefault
							});
				}
	
				function validateRecaptchaDefault(captchaResponse) {
					register_captcha_success_default = true;
					checkSubmitButton();
				}
	
				function expireRecaptchaDefault() {
					grecaptcha.reset(register_captcha_default);
					register_captcha_success_default = false;
					checkSubmitButton();
				}
				
				window.continueAsGuestCallback = function() { window.history.back(); };
			    $("#continueAsGuestBtn").click(function(){
			        window.continueAsGuestCallback();
			        window.continueAsGuestCallback = function() { };
			    });
			</script>
			<style>
			.ui-tooltip{
				width: 30% !important;
			}
			</style>
		</cp:pageContainer>
	</dsp:page>