<dsp:page>

x-forword-for : <% out.print( request.getHeader("x-forwarded-for") ); %><br/>
Scheme : <% out.print(request.getScheme()); %><br/> 
Server Port : <% out.print(request.getServerPort()); %><br/>
Session Id : <%= session.getId() %><br/>

Arbitrary Header <br/>
The user agent is <%= request.getHeader("user-agent") %><br/>

Implicit Headers:<br/>

Request Method : <%= request.getMethod() %><br/>

Request URI : <%= request.getRequestURI() %><br/>

Request Protocol : <%= request.getProtocol() %><br/>

Remote Host : <%= request.getRemoteHost() %><br/>

Remote Address : <%= request.getRemoteAddr() %><br/>

</dsp:page>