<dsp:page>
<!DOCTYPE html>
<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	
	<dsp:getvalueof var="productionURL" vartype="java.lang.String" param="productionURL" />
	<dsp:getvalueof var="httpServer" vartype="java.lang.String" value="${productionURL}" />

	<%-- Try the default site if we didn't get anything passed from the e-mail template properties --%>
	<%-- This is mostly for preview purposes. --%>
	<c:if test="${empty productionURL}">
		<!-- ALT-URL -->
		<dsp:importbean bean="/atg/multisite/Site"/>
		<dsp:getvalueof var="additionalURLs" bean="Site.additionalProductionURLs" />
		<c:if test="${not empty additionalURLs}">
			<dsp:getvalueof var="httpServer" vartype="java.lang.String" bean="Site.additionalProductionURLs[0]" />
			<dsp:getvalueof var="httpServer" vartype="java.lang.String" value="https://${httpServer}" />
		</c:if>
	</c:if>

	<!-- <dsp:valueof value="${productionURL}" /> || <dsp:valueof value="${httpServer}" /> -->

<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title></title>
		 <!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css" /><!--<![endif]-->
	</head>
	<body>
		<div bgcolor="#fff" lang="EN-US" link="blue" vlink="purple" style="margin: 0 auto; max-width: 800px; border-top: 20px solid #1b5c35; padding: 20px;">
			<!-- <table border="0" cellspacing="0" cellpadding="0" width="100%" style="background:#fafafa;border-collapse:collapse;font-family:'Lato',Verdana,Geneva,Helvetica,Arial,sans-serif;font-size:14px;line-height:1.34em;"> -->
			<table border="0" cellspacing="0" cellpadding="0" width="100%" style="border-collapse: collapse; font-family: 'Open Sans', Verdana, sans-serif; font-size: 13px; line-height: 1.5">
				<!-- E-mail Header Banner -->
				<tr>
					<td style="width: 50%; padding: 0 0 20px; ">
						<a href="${httpServer}/index.jsp"><img src="${staticContentPrefix}/assets/images/header-logo.png" style="width: 100%; height: auto;"></a>
					</td>
					<td style="width: 50%; text-align: right; vertical-align: middle;">Phone: 1-800-572-1904<br>Available 7am - 5pm
					</td>
				</tr>
				<!-- End E-mail Header Banner -->
				
				<!-- Begin E-mail Body Content -->
				<tr>
					<td colspan="2" style="padding: 30px; border: 1px solid #000;">			
</dsp:page>