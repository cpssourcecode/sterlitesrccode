<%@ include file="/includes/utils/taglibs.jspf" %>
<%@ include file="/includes/utils/context.jspf" %>
<dsp:page>
	<dsp:importbean var="msgutils" bean="/vsg/util/MessageUtils" />
	<dsp:tomap var="params" param="mailParams" />
	<dsp:getvalueof var="productionUrl" value="${params['PARAM_PRODUCTION_URL']}" />

	<cp:mailPageContainer url="${productionUrl}">
		<jsp:body>
			<!-- MP: ${mailParams} -->
			<!-- P: ${params} -->
			<!-- URL: ${productionUrl} -->
			<dsp:valueof value="${vsg_utils:prepareMessage(msgutils, 'emailBodyAccountActivated', params)}" valueishtml="true" />
		</jsp:body>
	</cp:mailPageContainer>
</dsp:page>