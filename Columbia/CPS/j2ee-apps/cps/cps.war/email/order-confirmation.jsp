<%@ include file="/includes/utils/taglibs.jspf" %>
<%@ include file="/includes/utils/context.jspf" %>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean var="msgutils" bean="/vsg/util/MessageUtils" />
	<dsp:tomap var="params" param="mailParams" />
	<dsp:getvalueof var="productionUrl" value="${params['PARAM_PRODUCTION_URL']}" />
	<dsp:setvalue param="order" paramvalue="orderObj" />
 
 	<cp:mailPageContainer url="${productionUrl}">
		<jsp:body>
			<dsp:valueof value="${vsg_utils:prepareMessage(msgutils, 'emailBodyOrderConfirmation', params)}" valueishtml="true" />

			<br />
			<h3>Order Detail</h3>

			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="order.commerceItems"/>
				<dsp:param name="elementName" value="item"/>
				<dsp:oparam name="empty">
					<!-- COMMERCE ITEMS LIST EMPTY -->
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:tomap var="productMap" param="item.auxiliaryData.productRef"/>
					<dsp:tomap var="commerceItem" param="item"/>
					<dsp:getvalueof var="skuId" value="${commerceItem.catalogRefId}"/>
					<%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String"
						value="${productionUrl}/catalog/pdp.jsp?prodId=${skuId}"/> --%>
					<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
						<dsp:param name="prodId" value="${productMap.id}"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
						</dsp:oparam>
					</dsp:droplet>
					
					<span>
						<a href="${productionUrl}${productUrl}">
							<dsp:valueof value="${productMap.description}" />
							<c:if test="${not empty productMap.descriptionLine2}">, <dsp:valueof value="${productMap.descriptionLine2}" /></c:if>
						</a><br>
						
						Item Number <dsp:valueof value="${productMap.displayName}" /><br>
						
						Quantity <dsp:valueof value="${commerceItem.quantity}" /><br>
						
						Price <dsp:valueof value="${commerceItem.priceInfo.listPrice}" converter="currencyConversion"/><br>
						
						Total Price <dsp:valueof value="${commerceItem.priceInfo.amount}" converter="currencyConversion"/><br>
					</span>
					<br>
				</dsp:oparam>
			</dsp:droplet>
		</jsp:body>
	</cp:mailPageContainer>
</dsp:page>