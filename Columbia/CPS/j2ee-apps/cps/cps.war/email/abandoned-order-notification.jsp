<%@ include file="/includes/utils/taglibs.jspf" %>
<%@ include file="/includes/utils/context.jspf" %>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean var="msgutils" bean="/vsg/util/MessageUtils" />
	<dsp:tomap var="params" param="mailParams" />
	<dsp:getvalueof var="productionUrl" value="${params['PARAM_PRODUCTION_URL']}" />
	<dsp:setvalue param="order" paramvalue="orderObj" />
 
 	<cp:mailPageContainer url="${productionUrl}">
		<jsp:body>
			<dsp:valueof value="${vsg_utils:prepareMessage(msgutils, 'emailBodyAbandonedOrderNotification', params)}" valueishtml="true" />

			<p>
				<a href="${productionUrl}/checkout/cart.jsp"
					style="background-color: #e7b50b; color: #010003; border: 1px solid #030303 !important; font-weight: 700; padding: 5px 20px; text-decoration: none; text-shadow: none !important;">Checkout Now</a>
			</p>
			<h3>Cart Detail</h3>
			<table>
			  <col width="100">
  			  <col width="auto">
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" param="order.commerceItems"/>
				<dsp:param name="elementName" value="item"/>
				<dsp:oparam name="empty">
					<!-- COMMERCE ITEMS LIST EMPTY -->
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:tomap var="productMap" param="item.auxiliaryData.productRef"/>
					<dsp:tomap var="commerceItem" param="item"/>
					<dsp:getvalueof var="skuId" value="${commerceItem.catalogRefId}"/>
					<%-- <dsp:getvalueof var="productUrl" vartype="java.lang.String"
						value="${productionUrl}/catalog/pdp.jsp?prodId=${skuId}"/> --%>
					<dsp:droplet name="/cps/seo/ProductSeoUrlDroplet">
						<dsp:param name="prodId" value="${productMap.id}"/>
						<dsp:oparam name="output">
							<dsp:getvalueof var="productUrl" vartype="java.lang.String" param="url"/>
						</dsp:oparam>
					</dsp:droplet>
					<dsp:getvalueof var="imageUrl" vartype="java.lang.String"
                            value="${empty productMap.thumbnail_url ? '/assets/images/plp-placeholder.png' : productMap.thumbnail_url}"/>					
					 <tr>
						<td height="100">
							<cp:readerimg src="${imageUrl}" height="60" width="60"/></br>
						</td>
						<td height="100">
							<a href="${productionUrl}${productUrl}">
								<dsp:valueof value="${productMap.description}" />
								<c:if test="${not empty productMap.descriptionLine2}">, <dsp:valueof value="${productMap.descriptionLine2}" /></c:if>
							</a><br>
							Item Number <dsp:valueof value="${productMap.displayName}" /><br>
							Quantity <dsp:valueof value="${commerceItem.quantity}" />							
						</td>
					</tr>
				</dsp:oparam>
			</dsp:droplet>
			</table>
			<p>
				<a href="${productionUrl}/all-products" style="background-color: #e7b50b;color: #010003;border: 1px solid #030303 !important;font-weight: 700;padding: 5px 20px;text-decoration: none;text-shadow: none !important;">Continue Shopping</a>
			</p>
		</jsp:body>
	</cp:mailPageContainer>
</dsp:page>