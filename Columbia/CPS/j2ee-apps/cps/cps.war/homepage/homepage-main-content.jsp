<dsp:page>
	<dsp:importbean var="originatingRequest" bean="/OriginatingRequest"/>
	<dsp:importbean bean="/atg/userprofiling/Profile"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch"/>
	<dsp:importbean bean="/cps/userprofiling/util/LoginSessionInfo"/>


	<cp:pageContainer page="account">

    <div class="owl-home hide">
        <div>
            <div class="image" style="background: url(assets/images/home-slider/slide-bg.png);">
                <!-- <img src="${staticContentPrefix}../assets/images/slide-bg.png" alt=""> -->
            </div>
            <div class="lead-block">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <h2>Big Headline 1</h2>
                            <p class="lead">Smaller sub-copy that promotes something for viewer to take action on.</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right">
                            <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="image" style="background: url(assets/images/home-slider/black-and-white-sky-construction-bridge.jpg)">
                <!-- <img src="${staticContentPrefix}../assets/images/home-slider/black-and-white-sky-construction-bridge.jpg" alt=""> -->
            </div>
            <div class="lead-block">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <h2>Big Headline 2</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis pariatur numquam quasi nemo.</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right">
                            <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="image" style="background: url(assets/images/home-slider/landmark-bridge-metal-architecture.jpg)">
                <!-- <img src="${staticContentPrefix}../assets/images/home-slider/landmark-bridge-metal-architecture.jpg" alt=""> -->
            </div>
            <div class="lead-block">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <h2>Big Headline 3</h2>
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt quos perspiciatis suscipit saepe laudantium vel nihil possimus temporibus ducimus.</p>
                        </div>
                        <div class="col-sm-4 col-xs-12 text-right">
                            <a href="#" class="btn btn-warning btn-lg">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <dsp:getvalueof var="selectedOrg" bean="Profile.CSRSelectedOrg"/>
	<c:if test="${not empty selectedOrg}">
		<dsp:getvalueof var="csrClass" value="admin-access"/>
	</c:if>
    <div class="container main-content ${csrClass}">
        <div class="row">
            <div class="col-sm-3 hidden-xs">
                <div class="well">
                    <h3>Our Customers Love Us</h3>
                    <blockquote>
                        <p>Their service is fantastic. Our branch manager is knowledgeable and has great technical expertise.”</p>
                        <footer>Someone famous in
                            <cite title="Source Title">Source Title</cite>
                        </footer>
                    </blockquote>
                    <a class="testimonial-detail" href="#"><span>Read More testimonials</span> <span class="glyphicon glyphicon-menu-right"></span></a>
                </div>
            </div>
            <div class="col-sm-9">
                <h3 class="page-title">Explore Popular Products</h3>
                <div class="row equalize">
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <div class="image">
                                        <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4>Pipe</h4>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Carbon Steel Pipe</a></li>
                                            <li><a href="#">Carbon Steel</a></li>
                                            <li><a href="#">Soil Pipe & Fittings</a></li>
                                            <li><a href="#">SS Pipe</a></li>
                                        </ul>
                                        <a href="#" class="btn btn-warning floated-bottom" data-toggle="modal" data-target="#findInvoice">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <div class="image">
                                        <img src="${staticContentPrefix}assets/images/water-hitters.png" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4>Water Heaters</h4>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Carbon Steel Pipe</a></li>
                                            <li><a href="#">Carbon Steel</a></li>
                                            <li><a href="#">Soil Pipe & Fittings</a></li>
                                            <li><a href="#">SS Pipe</a></li>
                                        </ul>
                                        <a href="#" class="btn btn-warning floated-bottom">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <div class="image">
                                        <img src="${staticContentPrefix}assets/images/plumbing.png" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4>Plumbing</h4>
                                        <ul class="list-unstyled">
                                            <li><a href="#">Carbon Steel Pipe</a></li>
                                            <li><a href="#">Carbon Steel</a></li>
                                            <li><a href="#">Soil Pipe & Fittings</a></li>
                                            <li><a href="#">SS Pipe</a></li>
                                        </ul>
                                        <a href="#" class="btn btn-warning floated-bottom">View All</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <h3>Services</h3>
                <div class="row equalize">
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <a href="#">
                                        <div class="image">
                                            <img src="${staticContentPrefix}assets/images/kitting.png" alt="">
                                        </div>
                                        <h4 class="hidden-xs">Kitting</h4>
                                    </a>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbmail-content">
                                        <h4 class="visible-xs">Kitting</h4>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil, id totam sint dolor molestiae natus minus dolores consectetur pariatur eligendi</small>
                                        <a href="#" class="btn btn-warning floated-bottom">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <a href="#">
                                        <div class="image">
                                            <img src="${staticContentPrefix}assets/images/standartization.png" alt="">
                                        </div>
                                        <h4 class="hidden-xs">Standardization</h4>
                                    </a>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4 class="visible-xs">Standardization</h4>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil, id totam sint dolor molestiae natus minus dolores consectetur pariatur eligendi, iure, accusamus quisquam dolorum accusantium. Rerum illo, sapiente?</small>
                                        <a href="#" class="btn btn-warning floated-bottom">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="thumbnail">
                            <div class="row">
                                <div class="col-sm-12 col-xs-5">
                                    <a href="#">
                                        <div class="image">
                                            <img src="${staticContentPrefix}assets/images/after-sales-service.png" alt="">
                                        </div>
                                        <h4 class="hidden-xs">After Sales Service</h4>
                                    </a>
                                </div>
                                <div class="col-sm-12 col-xs-7">
                                    <div class="thumbnail-content">
                                        <h4 class="visible-xs">After Sales Service</h4>
                                        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                        <a href="#" class="btn btn-warning floated-bottom">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <h3>You May Be Interested In</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="owl-thumbnails">
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="thumbnail">
                                    <div class="row">
                                        <div class="col-sm-12 col-xs-5">
                                            <a href="#">
                                                <div class="image">
                                                    <img src="${staticContentPrefix}assets/images/pipe.png" alt="">
                                                </div>
                                                <h4 class="hidden-xs">After Sales Service</h4>
                                            </a>
                                        </div>
                                        <div class="col-sm-12 col-xs-7">
                                            <div class="thumbnail-content">
                                                <a href="#">
                                                    <h4 class="visible-xs">After Sales Service</h4>
                                                </a>
                                                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim facere nihil</small>
                                                <big><span class="text-success">$54.60</span><span>/unit</span></big>
                                                <div class="row counter">
                                                    <div class="col-sm-5 col-xs-4">
                                                        <input type="text" class="form-control text-center" placeholder="Qty" value="1">
                                                    </div>
                                                    <div class="col-sm-7 col-xs-8">
                                                        <a href="#" class="btn btn-warning btn-block">Add to Cart</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- main container end -->



		<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-password-forgot.jsp"/>
		<%--<dsp:include page="${originatingRequest.contextPath}/global/modals/modal-password-reset.jsp"/>--%>

	</cp:pageContainer>

</dsp:page>

<script type="text/javascript" nonce="${requestScope.nonce}">
	// catch enter click and trigger correct button click instead of form submit
	$('input').keypress(function (e) {
		if (e.which == 13) {
			$('#submitForm').click();
			return false;
		}
	});
</script>
