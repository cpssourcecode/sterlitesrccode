$(document).ready(function() {
    
	// browsers	
	
	if (bowser.webkit==true) { $('html').addClass('wk ' + bowser.name); }
	if (bowser.firefox==true) { $('html').addClass('ff'); }
	if (bowser.msie==true) { $('html').addClass('ie'); }
	if (bowser.opera==true) { $('html').addClass('opera'); }
	
	$('a[data-toggle="tab"]').click(function (e) {
		e.stopPropagation()        
		$(this).tab('show');
		return false;
	})
	
	// clear inputs
	
	$(".input-clear").on('click', function(event) {
    	event.preventDefault();
        $(this).parent('div').siblings('input').val('');
    });


	
	// fancy labels
	function labelCheck() {
		$('label').removeClass('active');
		$('input[type=radio]:checked').parents('label').addClass('active');
	}
	labelCheck();
	$('input[type=radio]').click(labelCheck);
	
	// well with tools 
	
	$('ul.tool-list').parents('.well').addClass('has-tools');
	
	
	// select
	$('.wk select.form-control, .ff select.form-control').wrap('<span class="select-wrap"></span>').after('<span class="caret caret-select"></span>');
	
	// labels 
	$('.floatlabel input[type=text], .floatlabel input[type=email], .floatlabel input[type=number], .floatlabel input[type=tel], .floatlabel input[type=password], .floatlabel').floatlabel({
		labelEndTop: '-8px',
		slideInput: false
	});
	
	// homepage
	
	 $('.home-brands .carousel').not('.slick-initialized').slick({
				  infinite: true,
				  dots: false,
				  arrows: true,
				  slidesToShow: 5,
				  slidesToScroll: 1,
				  prevArrow: '<button type="button" class="btn slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
				  nextArrow: '<button type="button" class="btn slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
				  responsive: [
					{
						breakpoint: 990,
						settings: {
							slidesToShow: 4,
						}
					},		
					{
						breakpoint: 767,
						settings: {
							slidesToShow: 2,
						}
					}
					]
			  });
	
	
	// datepicker - range
	/*
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

	var startdate = $('#date-start').datepicker({
	  onRender: function(date) {
		return date.valueOf() > now.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  if (ev.date.valueOf() > enddate.date.valueOf()) {
		var newDate = new Date(ev.date)
		newDate.setDate(newDate.getDate() + 1);
		enddate.setValue(newDate);
	  }
	  startdate.hide();
	  $('#date-end')[0].focus();
	}).data('datepicker');
	var enddate = $('#date-end').datepicker({
	  onRender: function(date) {
		return date.valueOf() <= startdate.date.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  enddate.hide();
	}).data('datepicker');
	*/
	// facets 
	$('.facet-label:not(.active)').siblings('.facet-refine').hide();
	$('.facet-label').append('<span class="facet-control"><i class="fa fa-chevron-down"></i><i class="fa fa-chevron-up"></i></span>').click(function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active').siblings('.facet-refine').slideUp();
		} else {
			$(this).addClass('active').siblings('.facet-refine').slideDown();
		}
	});

	$('.facet-trigger')/* .prepend('<i class="fa fa-filter"></i> <span class="facet-show">Show</span><span class="facet-hide">Hide</span> ') */.click(function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active').siblings('.facet').addClass('hidden-xs hidden-sm');
		} else {
			$(this).addClass('active').siblings('.facet').removeClass('hidden-xs hidden-sm');
		}
	});
	/*
	$('.item-grid .item:nth-of-type(2n)').after('<div class="clear visible-xs"> </div>');
	$('.item-grid .item:nth-of-type(4n)').after('<div class="clear hidden-xs"> </div>');
	*/
	// pdp sections
	$('.pdp-section h3').append('<span class="pdp-section-arrow visible-xs"><i class="fa fa-caret-down"></i><i class="fa fa-caret-up"></i></span>').click(function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active').siblings('.pdp-section-content').addClass('hidden-xs');
		} else {
			$('.pdp-section h3').removeClass('active').siblings('.pdp-section-content').addClass('hidden-xs');
			$(this).addClass('active').siblings('.pdp-section-content').removeClass('hidden-xs');
		}
	});
	
	
	// toggle spending limit
    if($('input[name="splimit"]').attr("value")=="nospending"){
		$('.splimit-options').hide();
	}
	$('input[name="splimit"]').click(function(){
        if($(this).attr("value")=="nospending"){
            $('.splimit-options').hide();
		} else {
			$('.splimit-options').show();
		}
    });
	
	
	
	// toggle payment method
	function paymentMethod() {
		$('.payment>.radio').siblings().hide();
		$('.payment>.radio input:checked').parents('.radio').siblings().show();
	}
	paymentMethod();
	$('.payment>.radio input').click(paymentMethod);
	
	if ($('.payment-address>.checkbox input').is(':checked')) {
		$('.payment-address-new').hide();
		$('.payment-address-old').show();
	} else {
		$('.payment-address-new').show();
		$('.payment-address-old').hide();
	}
	
	// toggle billing address
	function billingAddress() {
		if ($('.payment-address>.checkbox input').is(':checked')) {
			$('.payment-address-new').hide();
			$('.payment-address-old').show();
		} else {
			$('.payment-address-new').show();
			$('.payment-address-old').hide();
		}	
	}
	billingAddress();
	$('.payment-address>.checkbox input').click(billingAddress);
	
	// toggle products
	$('.review-trigger').click(function() {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$('.review-trigger .fa-chevron-down').removeClass('hidden-xs');
			$('.review-trigger .fa-chevron-up, .review-products').addClass('hidden-xs');
		} else {
			$(this).addClass('open');
			$('.review-trigger .fa-chevron-down').addClass('hidden-xs');
			$('.review-trigger .fa-chevron-up, .review-products').removeClass('hidden-xs');
		}
	});
	
	$('.footer-collapse h5').append('<div class="footer-trigger visible-xs">Show/Hide</div>');
	$('.footer-collapse ul').addClass('hidden-xs');  
	  
	$('.footer-collapse h5').click(function() {
		if ($(this).hasClass('active')) {
			$(this).removeClass('active').siblings('ul').addClass('hidden-xs');
		} else {
			$('.footer-collapse h5').removeClass('active').siblings('ul').addClass('hidden-xs');
			$(this).addClass('active').siblings('ul').removeClass('hidden-xs');
		}
	});

    // regional managers

    $('.collapse-description').on('click', '.more', function(event) {
        event.preventDefault();
        var parentCover = $(this).parent('.collapse-description'), newHeight = 0;

        parentCover.children().each(function(){newHeight += $(this).height();});

        parentCover.toggleClass('full');

        if ( parentCover.hasClass('full') ) {
            parentCover.css('height', newHeight);
            $(this).children('span').text('Less');
        } else {
            parentCover.css('height', '');
            $(this).children('span').text('More');
        }
        $(this).blur();
    });
	
	
});


function assignslider(el) {
	var n = el.find(".fdataid").html();
	var min = el.find(".fdatamin").html();
	var max = el.find(".fdatamax").html();
	var val0 = el.find(".fdataval0").html();
	var val1 = el.find(".fdataval1").html();
	var pace = el.find(".fdatapace").html();
	initializeSlider(n, min, max, val0, val1, pace);
}

function initializeSlider(n, min, max, val0, val1, pace) {
	var refc = $("#facet-slider-control-" + n);
	var minc = $("#facet-size-min-" + n);
	var maxc = $("#facet-size-max-" + n);
	var sliderprice = new sliderControl(n, min, max, minc, maxc, refc, refineRangeResult, 100, pace, val0, val1);
	refc.slider(sliderprice);
	minc.val(Number(refc.slider("values", 0))/100);
	maxc.val(Number(refc.slider("values", 1))/100);
	maxc.bind("keypress", sliderprice.checkMax);
	minc.bind("keypress", sliderprice.checkMin);
}

function formDataLayerPush(form_id){
	
	 window.dataLayer = window.dataLayer || [];
     window.dataLayer.push(
     { event: 'formSubmissionSuccess', formId: form_id }
    );
    console.log("{event:formSubmissionSuccess, formId"+form_id+"}");
	
}

