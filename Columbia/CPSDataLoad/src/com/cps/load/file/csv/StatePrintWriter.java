package com.cps.load.file.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

/**
 * 
 * @author VSG
 *
 */
public class StatePrintWriter extends PrintWriter {

	public StatePrintWriter(File pFile, String pCsn) throws FileNotFoundException, UnsupportedEncodingException {
		super(pFile, pCsn);
	}

	public StatePrintWriter(File pFile) throws FileNotFoundException {
		super(pFile);
	}

	public StatePrintWriter(OutputStream pOut, boolean pAutoFlush) {
		super(pOut, pAutoFlush);
	}

	public StatePrintWriter(OutputStream pOut) {
		super(pOut);
	}

	public StatePrintWriter(String pFileName, String pCsn) throws FileNotFoundException, UnsupportedEncodingException {
		super(pFileName, pCsn);
	}

	public StatePrintWriter(String pFileName) throws FileNotFoundException {
		super(pFileName);
	}

	public StatePrintWriter(Writer pOut, boolean pAutoFlush) {
		super(pOut, pAutoFlush);
	}

	public StatePrintWriter(Writer pOut) {
		super(pOut);
	}

	public boolean isOpen() {
		return out != null;
	}
}
