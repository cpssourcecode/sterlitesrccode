package com.cps.load.file.csv;

import vsg.load.info.PathInfo;

/**
 * 
 * @author VSG
 *
 */
public interface ICSVFileNameGenerator {

	String generate();
	
	String encoding();
}
