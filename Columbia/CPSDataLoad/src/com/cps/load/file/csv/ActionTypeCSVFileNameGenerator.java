package com.cps.load.file.csv;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import atg.nucleus.GenericService;
import vsg.load.info.PathInfo;
import vsg.load.loader.CommonSettings;

/**
 * 
 * @author VSG
 *
 */
public class ActionTypeCSVFileNameGenerator extends GenericService implements ICSVFileNameGenerator {

	private static final String CREATE = "CREATE";
	private static final String DELETE = "DELETE";
	private static final String UPDATE = "UPDATE";
	
	/** Action type string */
	private String mActionType;
	/** The date format for file name */
	private String mDateFormat;
	/** The common settings */
	private CommonSettings mCommonSettings;
	/** Path info */
	private PathInfo mPathInfo;
	
	@Override
	public String generate() {
		String actionPostfix = null;
		switch (getActionType()) {
		case CREATE:
			actionPostfix = getPathInfo().getPostfixInsert();
			break;
		case DELETE:
			actionPostfix = getPathInfo().getPostfixDelete();
			break;
		case UPDATE:
			actionPostfix = getPathInfo().getPostfixUpdate();
			break;
		default:
			break;
		}
		if (actionPostfix != null) {
			return createFileName(getPathInfo(), actionPostfix);
		}
		return null;

	}

	@Override
	public String encoding() {
		return getCommonSettings().getEncoding();
	}
	/**
	 * concat file name
	 * @param pPathInfo - PathInfo
	 * @param actionPostfix - String
	 * @return String
	 */
	private String createFileName(PathInfo pPathInfo, String actionPostfix) {
		SimpleDateFormat format = new SimpleDateFormat(getDateFormat());
		String date = format.format(new Date());
		StringBuilder sb = new StringBuilder();
		sb.append(pPathInfo.getInputDir()).append(File.separator).append(pPathInfo.getFilePrefix());
		sb.append(pPathInfo.getImportFileNameDivider()).append(date).append(pPathInfo.getImportFileNameDivider());
		sb.append(actionPostfix).append(pPathInfo.getFileExt());
		return sb.toString();
	}

	public String getActionType() {
		return mActionType;
	}

	public void setActionType(String pActionType) {
		mActionType = pActionType;
	}

	public String getDateFormat() {
		return mDateFormat;
	}

	public void setDateFormat(String pDateFormat) {
		mDateFormat = pDateFormat;
	}

	public CommonSettings getCommonSettings() {
		return mCommonSettings;
	}

	public void setCommonSettings(CommonSettings pCommonSettings) {
		mCommonSettings = pCommonSettings;
	}

	public PathInfo getPathInfo() {
		return mPathInfo;
	}

	public void setPathInfo(PathInfo pPathInfo) {
		mPathInfo = pPathInfo;
	}
	
}
