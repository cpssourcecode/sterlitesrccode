package com.cps.load.file.csv;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import vsg.load.file.csv.CSVWriter;
import vsg.load.tools.ILoggerTools;

/**
 * 
 * @author VSG
 *
 */
public class CSVBatchWriter extends CSVWriter {

	/** Count of rows per file */
	private int mBatchSize;
	
	/** Current count of rows in the file */
	private AtomicInteger mCurrentBatchSize;
	
	/** File name generator */
	private ICSVFileNameGenerator mFileNameGenerator;
	
	public CSVBatchWriter() {
		super();
		mCurrentBatchSize = new AtomicInteger();
	}

	public CSVBatchWriter(int pBatchSize, ILoggerTools pLoggerTools, ICSVFileNameGenerator pFileNameGenerator) {
		super();
		mBatchSize = pBatchSize;
		mCurrentBatchSize = new AtomicInteger();
		mFileNameGenerator = pFileNameGenerator;
	}

	@Override
	public void writeNext(String pNextLine) {
		checkFile();
		super.writeNext(pNextLine);
	}
	
	@Override
	public void writeAll(List<String[]> pAllLines) {
		for (String[] nextLine : pAllLines) {
			checkFile();
			writeNext(nextLine);
		}
	}
	
	@Override
	public void writeNext(String[] pNextLines) {
		checkFile();
		super.writeNext(pNextLines);
	}
	
	/**
	 * check if file opened or closed
	 */
	private void checkFile() {
		int currentValue = mCurrentBatchSize.incrementAndGet();
		if(currentValue >= getBatchSize()) {
			close();
		}
		if(mPrintWriter instanceof StatePrintWriter) {
			if(!((StatePrintWriter) mPrintWriter).isOpen()) {
				File file = new File(getFileNameGenerator().generate());
				setWriter(file, getFileNameGenerator().encoding());
			}
		} else {
			File file = new File(getFileNameGenerator().generate());
			setWriter(file, getFileNameGenerator().encoding());
		}
	}
	
	@Override
	public void setWriter(File pFile, String pEncoding) {
		if(pEncoding == null) {
			try {
				mPrintWriter = new StatePrintWriter(pFile);
			} catch (FileNotFoundException e) {
				if(mLoggerTools!=null) { mLoggerTools.logError(e); }
			}
		} else {
			try {
				mPrintWriter = new StatePrintWriter(pFile, pEncoding);
			} catch (Exception e) {
				pEncoding = null;
				if(mLoggerTools!=null) { mLoggerTools.logError(e); }
				try {
					mPrintWriter = new StatePrintWriter(pFile);
				} catch (FileNotFoundException e1) {
					if(mLoggerTools!=null) { mLoggerTools.logError(e); }
				}
			}
		}
		if(mLoggerTools!=null && mLoggerTools.isLoggingDebug()) {
			String readerEncoding = pEncoding == null?"":pEncoding;
			mLoggerTools.logDebug(pFile.getName()+" - ready for write ["+readerEncoding+"]");
		}
		mCurrentBatchSize.set(0);
		setFile(pFile);
	}
	
	@Override
	public void close() {
		super.close();
		mCurrentBatchSize.set(0);
	}

	public int getBatchSize() {
		return mBatchSize;
	}

	public void setBatchSize(int pBatchSize) {
		mBatchSize = pBatchSize;
	}

	public ICSVFileNameGenerator getFileNameGenerator() {
		return mFileNameGenerator;
	}

	public void setFileNameGenerator(ICSVFileNameGenerator pFileNameGenerator) {
		mFileNameGenerator = pFileNameGenerator;
	}
	
}
