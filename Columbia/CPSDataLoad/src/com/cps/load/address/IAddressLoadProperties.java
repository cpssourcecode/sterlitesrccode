package com.cps.load.address;

/**
 * @author Dmitry Golubev
 */
public interface IAddressLoadProperties {
	/**
	 * ALPHAlphaName
	 */
	String ALPHALPHA_NAME = "ALPHAlphaName";
	/**
	 * AC03CollMgr
	 */
	String AC03COLLMGR = "AC03CollMgr";
	/**
	 * AC08OutsideRep
	 */
	String AC08OUTSIDEREP = "AC08OutsideRep";
	/**
	 * AC17RegionalManager
	 */
	String AC17REGIONALMANAGER = "AC17RegionalManager";
	/**
	 * AC21Buyer
	 */
	String AC21BUYER = "AC21Buyer";
	/**
	 * employee type CollMgr
	 */
	String EMPLOYEE_TYPE_CollMgr = "CollMgr";
	/**
	 * employee type OutsideRep
	 */
	String EMPLOYEE_TYPE_OutsideRep = "OutsideRep";
	/**
	 * employee type RegionalManager
	 */
	String EMPLOYEE_TYPE_RegionalManager = "RegionalManager";
	/**
	 * employee type Buyer
	 */
	String EMPLOYEE_TYPE_Buyer= "Buyer";
	/**
	 * address record type
	 */
	String AT1SCHTYP = "AT1SchTyp";
	/**
	 * area code for phone
	 */
	String AREACODE = "AreaCode";
	/**
	 * phone number
	 */
	String PHONENUMBER = "PhoneNumber";

	/**
	 * org addresses - org id property
	 */
	String ORG_ADDR_ORG_ID = "o_as_id";
	/**
	 * org addresses - address id property
	 */
	String ORG_ADDR_ADDR_ID = "o_as_addr_id";
	/**
	 * imported address record type property
	 */
	String IMPORTED_RECORD_TYPE = "importedRecordType";
	/**
	 * import address record type 'CB'
	 */
	String ADDRESS_RECORD_TYPE_CB = "CB";
	/**
	 * import address record type 'CS'
	 */
	String ADDRESS_RECORD_TYPE_CS = "CS";
	/**
	 * import address record type 'E'
	 */
	String ADDRESS_RECORD_TYPE_E = "E";
}
