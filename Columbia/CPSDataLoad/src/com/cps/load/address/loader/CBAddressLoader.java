package com.cps.load.address.loader;

import java.util.ArrayList;
import java.util.List;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;
import vsg.load.validator.impl.ColumnExistSQLValidator;

/**
 * on this loader we should analyze and fill org, address, their relations
 * 
 * @author Dmitry Golubev
 */
public class CBAddressLoader extends AddressLoader {
	private ColumnExistSQLValidator workGroupExistValidator;
    private List<String> insertIds = new ArrayList<String>();

    /**
     * @return the insertIds
     */
    public List<String> getInsertIds() {
        return insertIds;
    }

    /**
     * @param insertIds
     *            the insertIds to set
     */
    public void setInsertIds(List<String> insertIds) {
        this.insertIds = insertIds;
    }

    /**
     * @return the workGroupExistValidator
     */
    public ColumnExistSQLValidator getWorkGroupExistValidator() {
        return workGroupExistValidator;
    }

    /**
     * @param workGroupExistValidator
     *            the workGroupExistValidator to set
     */
    public void setWorkGroupExistValidator(ColumnExistSQLValidator workGroupExistValidator) {
        this.workGroupExistValidator = workGroupExistValidator;
    }
	
	
	/**
	 * insert record
	 * 
	 * @param pRecord
	 *            record to be inserted
	 */
	@Override
	public void insert(Record pRecord) {
		pRecord.getValues().remove(AT1SCHTYP);

		Record rec = pRecord.getSubRecords().get(SR_DPS_ORG_CHLDORG);
		if(rec != null && StringUtils.isBlank(rec.getId())) {
			pRecord.getSubRecords().remove(SR_DPS_ORG_CHLDORG);
		}
		Record workGroup = pRecord.getSubRecord(SR_CPS_WORK_GROUP);
        pRecord.getSubRecords().remove(SR_CPS_WORK_GROUP);

        if (workGroup != null) {
            boolean validated = getWorkGroupExistValidator().isValid(workGroup.getId(), "ID", workGroup);
            if (validated) { // exists
                super.update(workGroup);
            } else {
                if (!getInsertIds().contains(workGroup.getId())) {
                    getInsertIds().add(workGroup.getId());
                    super.insert(workGroup);
                }
            }
        }
		super.insert(pRecord);
	}

	/**
	 * insert record
	 * 
	 * @param pRecord
	 *            record to be inserted
	 */
	@Override
	public void update(Record pRecord) {
		pRecord.getValues().remove(AT1SCHTYP);

		pRecord.getSubRecords().remove(SR_DBC_ORGANIZATION);
		pRecord.getSubRecords().remove(SR_DCS_ORG_ADDRESS);

		pRecord.getSubRecords().remove(SR_CPS_ADDRESS_IMPORT_TYPE);
		pRecord.getSubRecords().remove(SR_DPS_ORG_CHLDORG);
		
		Record workGroup = pRecord.getSubRecord(SR_CPS_WORK_GROUP);
        pRecord.getSubRecords().remove(SR_CPS_WORK_GROUP);

        if (workGroup != null) {
            boolean validated = getWorkGroupExistValidator().isValid(workGroup.getId(), "ID", workGroup);
            if (validated) { // exists
                super.update(workGroup);
            } else {
                if (!getInsertIds().contains(workGroup.getId())) {
                    getInsertIds().add(workGroup.getId());
                    super.insert(workGroup);
                }
            }
        }
		super.update(pRecord);
	}

	@Override
	public void delete(Record pRecord) {
		throw new UnsupportedOperationException("CB Loader does not support DELETE operation");
	}
	@Override
    protected void postLoad() {
        super.postLoad();
        getInsertIds().clear();
    }
}
