package com.cps.load.address.loader;

import javax.transaction.TransactionManager;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import vsg.load.loader.Record;

/**
 * on this loader we should analyze and fill org, contact, their relations
 * @author Dmitry Golubev
 */
public class CIAddressLoader extends AddressLoader {
    private static final String SHIPPING_ADDRESS = "shippingAddress";
    private static final String BILLING_ADDRESS = "billingAddress";
    private static final String ORGANIZATION = "organization";
    private Repository profileRepository;
    private TransactionManager transactionManager;
    private boolean checkAndRemoveCSRelation;
    private boolean checkAndRemoveCBRelation;
	@Override
	public void delete(Record pRecord) {

		if(ADDRESS_RECORD_TYPE_CB.equals( pRecord.getValue(IMPORTED_RECORD_TYPE))) {
			processCBRecord(pRecord);
		} else
		if(ADDRESS_RECORD_TYPE_CS.equals( pRecord.getValue(IMPORTED_RECORD_TYPE))) {
			processCSRecord(pRecord);
		} else
		if(ADDRESS_RECORD_TYPE_E.equals( pRecord.getValue(IMPORTED_RECORD_TYPE))) {
			processERecord(pRecord);
		} else {
			pRecord.setUseToImport(false); // ?
		}

		pRecord.getValues().remove( AT1SCHTYP );

		super.delete(pRecord);
	}

	/**
	 * update formed Record according to CI requirements
	 * @param pRecord record to use for DB change
	 */
	private void processCBRecord(Record pRecord) {
		pRecord.getSubRecords().remove(SR_CPS_CONTACT);
		pRecord.getSubRecords().remove(SR_CPS_ORG_CONTACT);
		if(isCheckAndRemoveCBRelation()){
		checkAndRemoveAddressRelation(pRecord.getId(),BILLING_ADDRESS);
		}

	}

	/**
	 * update formed Record according to CI requirements
	 * @param pRecord record to use for DB change
	 */
	private void processCSRecord(Record pRecord) {
	    	
		pRecord.getSubRecords().remove(SR_CPS_ORG_CONTACT);
		pRecord.getSubRecords().remove(SR_CPS_CONTACT);
		pRecord.getSubRecords().remove(SR_DBC_ORG_COSTCTR);
		pRecord.getSubRecords().remove(SR_DBC_ORG_CONTACT);
		pRecord.getSubRecords().remove(SR_DBC_ORG_APPROVER);
		pRecord.getSubRecords().remove(SR_DBC_ORG_PREFVNDR);
		pRecord.getSubRecords().remove(SR_DBC_ORG_PAYMENT);
		pRecord.getSubRecords().remove(SR_DPS_ORG_CHLDORG);
		pRecord.getSubRecords().remove(SR_DPS_ORG_CHLDORG2);
		pRecord.getSubRecords().remove(SR_DPS_ORG_ANCESTORS);
		pRecord.getSubRecords().remove(SR_DPS_RELATIVEROLE);
		pRecord.getSubRecords().remove(SR_DPS_ROLE_REL_ORG);
		pRecord.getSubRecords().remove(SR_DPS_USER_ORG_ANC);
		pRecord.getSubRecords().remove(SR_DPS_USER_ORG);
		pRecord.getSubRecords().remove(SR_DPS_USER_SEC_ORGS);
		pRecord.getSubRecords().remove(SR_CPS_B2B_SALES_REP_ANC_ORG);
		pRecord.getSubRecords().remove(SR_CPS_ORGANIZATION);
		pRecord.getSubRecords().remove(SR_DPS_ORG_ROLE);
		pRecord.getSubRecords().remove(SR_DPS_ORGANIZATION);
		pRecord.getSubRecords().remove(SR_DBC_ORGANIZATION__ORG);
		vlogDebug("sub recordes Map..{0}", pRecord.getSubRecords().toString());
		// left CPS_CONTACT_INFO and DPS_CONTACT_INFO, for update DBC_ORGANIZATION
		Record sr_DBC_ORGANIZATION = pRecord.getSubRecord(SR_DBC_ORGANIZATION__ADDR);
		pRecord.getSubRecords().remove(SR_DBC_ORGANIZATION__ADDR);
		if(isCheckAndRemoveCSRelation()){
		checkAndRemoveAddressRelation(pRecord.getId(),SHIPPING_ADDRESS);
		}
		vlogDebug("sr_DBC_Organization value {0}", sr_DBC_ORGANIZATION.toString());
		super.update(sr_DBC_ORGANIZATION);
	}

	/**
	 * update formed Record according to CI requirements
	 * @param pRecord record to use for DB change
	 */
	private void processERecord(Record pRecord) {
		pRecord.getSubRecords().remove(SR_CPS_CONTACT_INFO);
		pRecord.getSubRecords().remove(SR_DPS_CONTACT_INFO);
	}
	private void checkAndRemoveAddressRelation(String addressId,String addressType){
	    Object[] params = new Object[1];
	    params[0]=addressId;
	    StringBuilder sb= new StringBuilder();
	    sb.append(addressType);
	    sb.append("=?0");
	    
		try {
		    RepositoryView view = getProfileRepository().getView(ORGANIZATION);
			RqlStatement rqlStatement = RqlStatement.parseRqlStatement(sb.toString());
			RepositoryItem[] items = rqlStatement.executeQuery(view, params);
			if(items != null && items.length != 0){
			    for (RepositoryItem repositoryItem : items) {
				MutableRepository mutableRepo = (MutableRepository)getProfileRepository();
				MutableRepositoryItem mutableRepoItem = mutableRepo.getItemForUpdate(repositoryItem.getRepositoryId(),ORGANIZATION);
				TransactionDemarcation td=new TransactionDemarcation();
				td.begin(getTransactionManager(),TransactionDemarcation.REQUIRES_NEW);
				mutableRepoItem.setPropertyValue(addressType, null);
				mutableRepo.updateItem(mutableRepoItem);
				td.end();
			    }
			}


		} catch (RepositoryException e) {
		    e.printStackTrace();
		} catch (TransactionDemarcationException e) {
		    e.printStackTrace();
		}
	}
	@Override
	public void insert(Record pRecord) {
		throw new UnsupportedOperationException("CI Loader does not support CREATE operation");
	}

	@Override
	public void update(Record pRecord) {
		throw new UnsupportedOperationException("CI Loader does not support UPDATE operation");
	}

	public Repository getProfileRepository() {
	    return profileRepository;
	}

	public void setProfileRepository(Repository profileRepository) {
	    this.profileRepository = profileRepository;
	}

	public TransactionManager getTransactionManager() {
	    return transactionManager;
	}

	public void setTransactionManager(TransactionManager transactionManager) {
	    this.transactionManager = transactionManager;
	}

	public boolean isCheckAndRemoveCSRelation() {
	    return checkAndRemoveCSRelation;
	}

	public void setCheckAndRemoveCSRelation(boolean checkAndRemoveCSRelation) {
	    this.checkAndRemoveCSRelation = checkAndRemoveCSRelation;
	}

	public boolean isCheckAndRemoveCBRelation() {
	    return checkAndRemoveCBRelation;
	}

	public void setCheckAndRemoveCBRelation(boolean checkAndRemoveCBRelation) {
	    this.checkAndRemoveCBRelation = checkAndRemoveCBRelation;
	}
}
