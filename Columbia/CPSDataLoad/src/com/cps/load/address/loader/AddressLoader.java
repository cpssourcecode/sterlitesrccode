package com.cps.load.address.loader;

import com.cps.load.address.IAddressLoadProperties;
import vsg.load.loader.impl.SQLLoader;

/**
 * @author Dmitry Golubev
 */
public abstract class AddressLoader  extends SQLLoader implements IAddressLoadProperties {
	/**
	 * sub record - organization/contact
	 */
	protected static final String SR_CPS_ORG_CONTACT = "CPS_ORG_CONTACT";
	/**
	 * sub record - organization
	 */
	protected static final String SR_DPS_ORGANIZATION = "DPS_ORGANIZATION";
	/**
	 * sub record - organization-addresses
	 */
	protected static final String SR_DCS_ORG_ADDRESS = "DCS_ORG_ADDRESS";
	/**
	 * sub record - organization-addresses
	 */
	protected static final String SR_DCS_ORG_ADDRESS__ORG = "DCS_ORG_ADDRESS__ORG";
	/**
	 * sub record - organization-addresses
	 */
	protected static final String SR_DCS_ORG_ADDRESS__ADDR = "DCS_ORG_ADDRESS__ADDR";
	/**
	 * sub record - address/billing-shipping addresses
	 */
	protected static final String SR_DBC_ORGANIZATION = "DBC_ORGANIZATION";
	/**
	 * sub record - address/billing-shipping addresses
	 */
	protected static final String SR_DBC_ORGANIZATION__ORG = "DBC_ORGANIZATION__ORG";
	/**
	 * sub record - address/billing-shipping addresses
	 */
	protected static final String SR_DBC_ORGANIZATION__ADDR = "DBC_ORGANIZATION__ADDR";
	/**
	 * sub record - import address records types
	 */
	protected static final String SR_CPS_ADDRESS_IMPORT_TYPE = "CPS_ADDRESS_IMPORT_TYPE";
	/**
	 * sub record - organization/cost center
	 */
	protected static final String SR_DBC_ORG_COSTCTR = "DBC_ORG_COSTCTR";
	/**
	 * sub record - organization/contact info
	 */
	protected static final String SR_DBC_ORG_CONTACT = "DBC_ORG_CONTACT";
	/**
	 * sub record - organization/approver
	 */
	protected static final String SR_DBC_ORG_APPROVER = "DBC_ORG_APPROVER";
	/**
	 * sub record - organization/vendors
	 */
	protected static final String SR_DBC_ORG_PREFVNDR = "DBC_ORG_PREFVNDR";
	/**
	 * sub record - organization/payment
	 */
	protected static final String SR_DBC_ORG_PAYMENT = "DBC_ORG_PAYMENT";
	/**
	 * sub record - organization/child organization
	 */
	protected static final String SR_DPS_ORG_CHLDORG = "DPS_ORG_CHLDORG";
	/**
	 * sub record - organization/child organization
	 */
	protected static final String SR_DPS_ORG_CHLDORG2 = "DPS_ORG_CHLDORG2";
	/**
	 * sub record - organization/ancestors
	 */
	protected static final String SR_DPS_ORG_ANCESTORS = "DPS_ORG_ANCESTORS";
	/**
	 * sub record - organization/role/function
	 */
	protected static final String SR_DPS_RELATIVEROLE = "DPS_RELATIVEROLE";
	/**
	 * sub record - organization/roles
	 */
	protected static final String SR_DPS_ROLE_REL_ORG = "DPS_ROLE_REL_ORG";
	/**
	 * sub record - user/organization anc
	 */
	protected static final String SR_DPS_USER_ORG_ANC = "DPS_USER_ORG_ANC";
	/**
	 * sub record - user/organization
	 */
	protected static final String SR_DPS_USER_ORG = "DPS_USER_ORG";
	/**
	 * sub record - user/secondary organizations
	 */
	protected static final String SR_DPS_USER_SEC_ORGS = "DPS_USER_SEC_ORGS";
	/**
	 * sub record - org roles
	 */
	protected static final String SR_DPS_ORG_ROLE = "DPS_ORG_ROLE";
	/**
	 * sub record for store import record types
	 */
	protected static final String SR_CPS_B2B_SALES_REP_ANC_ORG = "CPS_B2B_SALES_REP_ANC_ORG";
	/**
	 * sub record - organization info, extend on CPS
	 */
	protected static final String SR_CPS_ORGANIZATION = "CPS_ORGANIZATION";
	/**
	 * sub record - organization contact
	 */
	protected static final String SR_CPS_CONTACT = "CPS_CONTACT";
	/**
	 * sub record - contact info, extend of CPS
	 */
	protected static final String SR_CPS_CONTACT_INFO = "CPS_CONTACT_INFO";
	/**
	 * sub record - contact info
	 */
	protected static final String SR_DPS_CONTACT_INFO = "DPS_CONTACT_INFO";
	
	protected static final String SR_CPS_WORK_GROUP = "CPS_WORK_GROUP";
}
