package com.cps.load.address.loader;


import vsg.load.info.FileInfo;
import vsg.load.info.StatisticsLoader;
import vsg.load.loader.Record;
import vsg.load.loader.impl.Loader;

import java.util.Date;

/**
 * @author Andy Porter
 */
public class CMoveAddressLoader extends Loader {

	@Override
	public void load(FileInfo pFileInfo) {
		setFileInfo(pFileInfo);

		setStatistics(new StatisticsLoader(this));
		setImportFailed(false);
		setReader(null);

		Date startTime = new Date();
		Date endTime = new Date();
		getReportingTools().sendReport(this, startTime, endTime);
		getReportingTools().storeReport(this, startTime, endTime);
		logDebug(getTime("Duration time", endTime.getTime() - startTime.getTime()));
	}

	@Override
	public void insert(Record pRecord) {
	}

	@Override
	public void update(Record pRecord) {
	}

	@Override
	public void delete(Record pRecord) {
	}
}
