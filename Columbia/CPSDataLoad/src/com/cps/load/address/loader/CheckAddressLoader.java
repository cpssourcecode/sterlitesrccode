package com.cps.load.address.loader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.cps.email.CommonEmailSender;
import com.cps.parse.InitialParser;
import com.cps.util.CPSGlobalProperties;

import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import vsg.load.file.IReader;
import vsg.load.file.IWriter;
import vsg.load.file.csv.CSVBase;
import vsg.load.file.csv.CSVWriter;
import vsg.load.info.FieldsInfo;
import vsg.load.info.FileInfo;
import vsg.load.info.StatisticsLoader;
import vsg.load.info.StatisticsService;
import vsg.load.loader.ILoader;
import vsg.load.loader.Record;
import vsg.load.tools.IParserTools;

/**
 * @author Andy Porter
 */
public class CheckAddressLoader extends InitialParser implements ILoader {

    private static final String ADDRESS_TYPE_FOR_ROW_MSG = "address type for this row {0} is {1}";
    private static final String CI_ADDRESS_DELETE_MSG = "{0} this is a CI address. so, create/add to a _DELETE file";
    private static final String ADDRESS_TYPE_CI = "CI";
    /**
     * category item descriptor name
     */
    private static final String ITEM_DESC_CONTACT_INFO = "contactInfo";
    private static final String ITEM_DESC_ORGANIZATION = "organization";
    private static final String ITEM_DESC_CONTACT = "contact";
    private CommonEmailSender emailManager;
    private CPSGlobalProperties globalProperty;

    /**
     * number of lines to skip
     */
    private int mLineToSkip = 0;

    /**
     * import file reader
     */
    private IReader mReader;

    /**
     * file info
     */
    private FileInfo mFileInfo;

    /**
     * ProductCatalog repository
     */
    protected Repository mRepository;

    /**
     * import AuditReport file writer
     */
    private IWriter mAuditReportWriter;

    /**
     * performJsonProcessing
     */
    @Override
    public synchronized List<String> performProcessing() {
        List<String> initialImportFiles = new ArrayList<String>();

        File[] files = getImportTools().getFilesList();
        // if (checkLockFile() && null != files && files.length > 0) {
        List<FileInfo> fileInfoList = getImportTools().getFileList();
        if (checkLockFile() && fileInfoList.size() > 0) {
            vlogDebug("*******************************");
            vlogDebug("performJsonProcessing: " + getName());
            vlogDebug("*******************************");
            // for (File file : files) {
            for (FileInfo fileInfo : fileInfoList) {
                initializeCategoryCategoriesWriters(fileInfo);

                performCheck(fileInfo);

                getImportTools().archiveFile(fileInfo.getFile());
                closeAllWriters();
            }
            releaseLockFile();

        }

        return initialImportFiles;
    }

    private void closeAllWriters() {
        if (null != mAddWriter) {
            getJsonExportTools().getAddWriter().close();
            mAddWriter = null;
        }
        if (null != mUpdateWriter) {
            getJsonExportTools().getUpdateWriter().close();
            mUpdateWriter = null;
        }
        if (null != mDeleteWriter) {
            getJsonExportTools().getDeleteWriter().close();
            mDeleteWriter = null;
        }
    }

    private void initializeCategoryCategoriesWriters(FileInfo pFileInfo) {

        getJsonExportTools().setAddWriter(null);
        getJsonExportTools().setUpdateWriter(null);
        getJsonExportTools().setDeleteWriter(null);

        mAddWriter = getJsonExportTools().getAddWriter();
        ((CSVWriter) mAddWriter).setQuoteChar(CSVBase.NO_QUOTE_CHARACTER);

        mUpdateWriter = getJsonExportTools().getUpdateWriter();
        ((CSVWriter) mUpdateWriter).setQuoteChar(CSVBase.NO_QUOTE_CHARACTER);

        mDeleteWriter = getJsonExportTools().getDeleteWriter();
        ((CSVWriter) mDeleteWriter).setQuoteChar(CSVBase.NO_QUOTE_CHARACTER);

        setReader(null);
    }

    /**
     * Check Category Exist or not
     *
     * @param pFileInfo
     *            file info
     */
    private void performCheck(FileInfo pFileInfo) {
	boolean isValidFile = true;
        try {
            IReader reader = null;
            try (FileInputStream inputStream = new FileInputStream(pFileInfo.getFile())) {
                setFileInfo(pFileInfo);
                reader = getImportTools().getReader(getFileInfo().getFile(), inputStream);
                setReader(reader);

                reader.setSkipLinesNumber(getLineToSkip());
                Repository profileReposiotry = getRepository();

                String[] row;
                while ((row = reader.readNext()) != null) {
                    String id = row[0];
                    if (!StringUtils.isBlank(id)) {
                	String addressType="";
                	if( row.length >= 11){
                	    addressType = row[10]; // this will give the address type as CS/CB/CI
                	}else{
                	    isValidFile = false;
                	}
                        vlogDebug(ADDRESS_TYPE_FOR_ROW_MSG, id, addressType);
                        if (ADDRESS_TYPE_CI.equals(addressType)) {
                            vlogDebug(CI_ADDRESS_DELETE_MSG, id);
                            if (null != mDeleteWriter) {
                                mDeleteWriter.writeNext(row);
                            } else {
                                vlogError("performCheck(): mDeleteWriter=null");
                            }
                            continue;
                        }
                        try {
                            RepositoryItem contact = profileReposiotry.getItem(id, ITEM_DESC_CONTACT);
                            RepositoryItem contactInfo = profileReposiotry.getItem(id, ITEM_DESC_CONTACT_INFO);
                            RepositoryItem organization = profileReposiotry.getItem(id, ITEM_DESC_ORGANIZATION);
                            if (null == contact || null == contactInfo || null == organization) {
                                if (null != mAddWriter) {
                                    mAddWriter.writeNext(row);
                                } else {
                                    vlogError("performCheck(): mAddWriter=null");
                                }
                            }
                            if (null != mUpdateWriter) {
                                mUpdateWriter.writeNext(row);
                            } else {
                                vlogError("performCheck(): mUpdateWriter=null");
                            }

                        } catch (RepositoryException re) {
                            vlogError("performCheck()", re);

                            if (null != mAddWriter) {
                                mAddWriter.writeNext(row);
                            }
                            if (null != mUpdateWriter) {
                                mUpdateWriter.writeNext(row);
                            }
                        }
                    }
                }
            } finally {
                if (null != reader) {
                    reader.close();
                }
                if(!isValidFile){
                  getGlobalProperty().setErrorAddressLoaderFile(pFileInfo.getFile().getName());  
                  getEmailManager().sendAddressLoaderFileErrorEmail();
                }
            }
        } catch (IOException ie) {
            if (getImportTools().getLoggerTools().isLoggingError()) {
                getImportTools().getLoggerTools().logError(ie);
            }
        }

    }

    @Override
    public String getLoaderName() {
        return "CheckAddressLoader";
    }

    @Override
    public void deleteRecords(IReader pReader) {

    }

    @Override
    public IParserTools getParserTools() {
        return null;
    }

    @Override
    public FieldsInfo getFieldsInfo() {
        return null;
    }

    @Override
    public void insert(Record pRecord) {

    }

    @Override
    public void update(Record pRecord) {

    }

    @Override
    public void delete(Record pRecord) {

    }

    @Override
    public void insertRecords(IReader pReader) {

    }

    @Override
    public void updateRecords(IReader pReader) {

    }

    @Override
    public void load(StatisticsService pStatisticsService) {
        performProcessing();
    }

    @Override
    public void load(FileInfo pFileInfo) {

    }

    @Override
    public StatisticsLoader getStatistics() {
        return null;
    }

    @Override
    public IWriter getErrorWriter() {
        return null;
    }

    @Override
    public void setErrorWriter(IWriter pErrorWriter) {

    }

    @Override
    public void terminateImport() {

    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * init number of lines to skip
     *
     * @param pLineToSkip
     *            number of lines to skip
     */

    public void setLineToSkip(int pLineToSkip) {
        mLineToSkip = pLineToSkip;
    }

    /**
     * return number of lines to skip
     *
     * @return number of lines to skip
     */
    public int getLineToSkip() {
        return mLineToSkip;
    }

    /**
     * create new one if it is needed and return import file reader
     *
     * @return import file reader
     */
    // @Override
    @Override
    public IReader getReader() {
        /*
         * if (mReader == null) { setReader(getImportTools().getReader(getFileInfo().getFile())); }
         */
        return mReader;
    }

    /**
     * init import file reader
     *
     * @param pReader
     *            import file reader
     */
    // @Override
    @Override
    public void setReader(IReader pReader) {
        mReader = pReader;
    }

    /**
     * return file info
     *
     * @return file info
     */
    @Override
    public FileInfo getFileInfo() {
        return mFileInfo;
    }

    /**
     * init file info
     *
     * @param pFileInfo
     *            file info
     */
    protected void setFileInfo(FileInfo pFileInfo) {
        mFileInfo = pFileInfo;
    }

    /**
     * Sets new ProductCatalog repository.
     *
     * @param pRepository
     *            New value of ProductCatalog repository.
     */
    public void setRepository(Repository pRepository) {
        mRepository = pRepository;
    }

    /**
     * Gets ProductCatalog repository.
     *
     * @return Value of ProductCatalog repository.
     */
    public Repository getRepository() {
        return mRepository;
    }

    /**
     * create new one if it is needed and return Audit Report file writer
     *
     * @return AuditReport file writer
     */
    @Override
    public IWriter getAuditReportWriter(String pFileNamePrefix) {
        return null;
    }

    /**
     * create new one if it is needed and return Audit Report file writer
     *
     * @return AuditReport file writer
     */
    @Override
    public IWriter getAuditReportWriter(String pFileNamePrefix, String pFileType) {
        return null;
    }

    @Override
    public void setAuditReportWriter(IWriter pAuditReportWriter) {
        mAuditReportWriter = pAuditReportWriter;
    }

    public CommonEmailSender getEmailManager() {
	return emailManager;
    }

    public void setEmailManager(CommonEmailSender emailManager) {
	this.emailManager = emailManager;
    }

    public CPSGlobalProperties getGlobalProperty() {
	return globalProperty;
    }

    public void setGlobalProperty(CPSGlobalProperties globalProperty) {
	this.globalProperty = globalProperty;
    }
}
