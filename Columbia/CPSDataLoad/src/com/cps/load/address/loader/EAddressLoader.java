package com.cps.load.address.loader;

import vsg.load.loader.Record;

/**
 * on this loader we should analyze and fill org, contact, their relations
 * @author Dmitry Golubev
 */
public class EAddressLoader extends AddressLoader {
	/**
	 * insert record
	 * @param pRecord record to be inserted
	 */
	@Override
	public void insert(Record pRecord) {
		pRecord.getValues().remove(AT1SCHTYP);
		pRecord.getValues().remove(ALPHALPHA_NAME);
		pRecord.getValues().remove(AC03COLLMGR);
		pRecord.getValues().remove(AC08OUTSIDEREP);
		pRecord.getValues().remove(AC17REGIONALMANAGER);
		pRecord.getValues().remove(AC21BUYER);

		super.insert(pRecord);
	}

	/**
	 * insert record
	 * @param pRecord record to be inserted
	 */
	@Override
	public void update(Record pRecord) {
		pRecord.getValues().remove(AT1SCHTYP);
		pRecord.getValues().remove(ALPHALPHA_NAME);
		pRecord.getValues().remove(AC03COLLMGR);
		pRecord.getValues().remove(AC08OUTSIDEREP);
		pRecord.getValues().remove(AC17REGIONALMANAGER);
		pRecord.getValues().remove(AC21BUYER);

		pRecord.getSubRecords().remove(SR_CPS_ORG_CONTACT);

		pRecord.getSubRecords().remove(SR_CPS_ADDRESS_IMPORT_TYPE);

		super.update(pRecord);
	}

	@Override
	public void delete(Record pRecord) {
		throw new UnsupportedOperationException("E Loader does not support DELETE operation");
	}

}
