package com.cps.load.address.wrapper;

import vsg.load.loader.Record;
import vsg.load.wrapper.IRecordValueWrapper;
import vsg.load.wrapper.impl.ValueWrapperImpl;

/**
 * retrieves field value (ALPHALPHA_NAME) from inited record to form result
 * @author Dmitry Golubev
 */
public class EmployeeLastNameWrapper extends EmployeeBaseWrapper {
	/**
	 * init value by specified fields
	 * @param pRecord inited record
	 */
	@Override
	public String getValue(Record pRecord) {
		String ALPHAlphaName = (String) pRecord.getValue( ALPHALPHA_NAME );
		String lastName = null;
		if(ALPHAlphaName != null){
			String[] nameParts = ALPHAlphaName.split(" ");
			if(nameParts.length > 0) {
				lastName = nameParts[0];
			}
		}

		return lastName;
	}
}
