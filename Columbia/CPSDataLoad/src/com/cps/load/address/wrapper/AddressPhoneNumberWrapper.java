package com.cps.load.address.wrapper;

import com.cps.load.address.IAddressLoadProperties;
import vsg.load.loader.Record;
import vsg.load.wrapper.IRecordValueWrapper;
import vsg.load.wrapper.impl.ValueWrapperImpl;

/**
 * retrieves two fields values (AREACODE, PHONENUMBER) from inited record to form result
 * @author Dmitry Golubev
 */
public class AddressPhoneNumberWrapper extends ValueWrapperImpl<String>
	implements IRecordValueWrapper<String>, IAddressLoadProperties {

	/**
	 *
	 * @param pValueObj value to wrap
	 * @return formed from inited record value
	 */
	@Override
	public String getValue(Object pValueObj) {
		return getTrimmedValue((String) pValueObj);
	}

	/**
	 * init value by specified fields
	 * @param pRecord inited record
	 */
	@Override
	public String getValue(Record pRecord) {
		String areaCode = (String) pRecord.getValue(AREACODE);
		String phoneNumber = (String) pRecord.getValue(PHONENUMBER); 
        	pRecord.getValues().remove(AREACODE);
        	pRecord.getValues().remove(PHONENUMBER);
		return ( areaCode != null ? areaCode : "" ) + " " + ( phoneNumber != null ? phoneNumber : "" );
	}
}
