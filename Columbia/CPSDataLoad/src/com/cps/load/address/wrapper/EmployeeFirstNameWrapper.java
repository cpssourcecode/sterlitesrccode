package com.cps.load.address.wrapper;

import vsg.load.loader.Record;
import vsg.load.wrapper.IRecordValueWrapper;
import vsg.load.wrapper.impl.ValueWrapperImpl;

/**
 * retrieves field value (ALPHALPHA_NAME) from inited record to form result
 * @author Dmitry Golubev
 */
public class EmployeeFirstNameWrapper extends EmployeeBaseWrapper {
	/**
	 * init value by specified fields
	 * @param pRecord inited record
	 */
	@Override
	public String getValue(Record pRecord) {
		String ALPHAlphaName = (String) pRecord.getValue( ALPHALPHA_NAME );
		String firstName = null;
		if(ALPHAlphaName != null) {
			String[] nameParts = ALPHAlphaName.split(" ");
			if (nameParts.length > 1) {
				firstName = nameParts[1];
			}
		}

		return firstName;
	}
}
