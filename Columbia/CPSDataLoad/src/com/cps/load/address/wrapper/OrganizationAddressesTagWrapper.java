package com.cps.load.address.wrapper;

import com.cps.load.address.IAddressLoadProperties;
import vsg.load.loader.Record;
import vsg.load.wrapper.IRecordValueWrapper;
import vsg.load.wrapper.impl.ValueWrapperImpl;

/**
 * retrieves two fields values (o_as_id, o_as_addr_id) from inited record to form result
 * @author Dmitry Golubev
 */
public class OrganizationAddressesTagWrapper extends ValueWrapperImpl<String>
	implements IRecordValueWrapper<String>, IAddressLoadProperties {

	/**
	 *
	 * @param pValueObj value to wrap
	 * @return formed from inited record value
	 */
	@Override
	public String getValue(Object pValueObj) {
		return getTrimmedValue((String) pValueObj);
	}

	/**
	 * init value by specified fields
	 * @param pRecord inited record
	 */
	@Override
	public String getValue(Record pRecord) {
		String orgId = (String) pRecord.getValue( ORG_ADDR_ORG_ID );
		String addrId = (String) pRecord.getValue( ORG_ADDR_ADDR_ID );
		return ( orgId != null ? orgId : "" ) + "-" + ( addrId != null ? addrId : "" );
	}
}
