package com.cps.load.address.wrapper;

import com.cps.load.address.IAddressLoadProperties;
import vsg.load.wrapper.IRecordValueWrapper;
import vsg.load.wrapper.impl.ValueWrapperImpl;

/**
 * retrieves field values from inited record to form result
 * @author Dmitry Golubev
 */
public abstract class EmployeeBaseWrapper extends ValueWrapperImpl<String>
	implements IRecordValueWrapper<String>, IAddressLoadProperties {

	/**
	 *
	 * @param pValueObj value to wrap
	 * @return formed from inited record value
	 */
	@Override
	public String getValue(Object pValueObj) {
		return getTrimmedValue((String) pValueObj);
	}
}
