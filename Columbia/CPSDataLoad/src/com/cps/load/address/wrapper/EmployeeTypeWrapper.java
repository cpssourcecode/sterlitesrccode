package com.cps.load.address.wrapper;

import atg.core.util.StringUtils;
import vsg.load.loader.Record;

/**
 * retrieves field values (AC03CollMgr, AC08OutsideRep, AC17RegionalManager, AC21Buyer ) from inited record to form result
 * @author Dmitry Golubev
 */
public class EmployeeTypeWrapper extends EmployeeBaseWrapper {
	/**
	 * init value by specified fields
	 * @param pRecord inited record
	 */
	@Override
	public String getValue(Record pRecord) {
		String AC03CollMgr = (String) pRecord.getValue( AC03COLLMGR );
		String AC08OutsideRep = (String) pRecord.getValue( AC08OUTSIDEREP );
		String AC17RegionalManager = (String) pRecord.getValue( AC17REGIONALMANAGER );
		String AC21Buyer = (String) pRecord.getValue( AC21BUYER );

		if(!StringUtils.isBlank(AC03CollMgr)) {
			return EMPLOYEE_TYPE_CollMgr;
		} else
		if(!StringUtils.isBlank(AC08OutsideRep)) {
			return EMPLOYEE_TYPE_OutsideRep;
		} else
		if(!StringUtils.isBlank(AC17RegionalManager)) {
			return EMPLOYEE_TYPE_RegionalManager;
		} else
		if(!StringUtils.isBlank(AC21Buyer)) {
			return EMPLOYEE_TYPE_Buyer;
		}

		return null;
	}
}
