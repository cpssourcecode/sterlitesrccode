package com.cps.load.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.cps.ftp.FTPConfig;
import com.cps.ftp.FTPService;

import atg.adapter.gsa.GSARepository;
import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONException;
import atg.json.JSONObject;
import vsg.constants.VSGConstants;
import vsg.load.connection.IConnector;
import vsg.load.file.IWriter;
import vsg.load.file.csv.CSVBase;
import vsg.load.info.ItemStatus;
import vsg.load.info.StatisticsLoader;
import vsg.load.info.StatisticsService;
import vsg.load.loader.ILoader;
import vsg.load.loader.impl.LoaderBase;
import vsg.load.loader.impl.ProjectRepositoryLoader;
import vsg.load.service.impl.ImportServiceMultiLoaders;

/**
 * @author Dmitry Golubev
 */
public class CatalogProjectService extends ImportServiceMultiLoaders {

    /**
     * audit report name
     */
    private static final String AUDIT_REPORT = "Audit_Report_";
    private static final String STOCK_COUNT_BEFORE = "Stock_count_before_";
    private static final String STOCK_COUNT_AFTER = "Stock_count_after_";

    /**
     * stock report file type
     */
    private static final String STOCK_REPORT_FILE_TYPE = ".csv";

    public static final String PRODUCT_COUNT_QUERY = "SELECT count(*) \n" + "FROM dcs_product dp \n" + "WHERE DP.IS_HEAD=1\n";

    public static final String STOCK_QUERY = "SELECT DISTINCT STOCKING_TYPE, count(*) \n" + "FROM CPS_PRODUCT_ALL cp, dcs_product dp \n"
                    + "WHERE cp.ASSET_VERSION=DP.ASSET_VERSION AND cp.PRODUCT_ID=DP.PRODUCT_ID AND DP.IS_HEAD=1\n" + "GROUP BY STOCKING_TYPE \n"
                    + "HAVING count(*) > 0";

    public static final String CATEGORY_OVERALL_COUNT_QUERY = "SELECT count(*) \n" + "FROM DCS_CATEGORY dc\n" + "WHERE DC.IS_HEAD=1 ";

    public static final String CATEGORY_SGC_COUNT_QUERY = "SELECT DISTINCT CC.ECOMMERCE_DISPLAY, count(*) \n" + "FROM DCS_CATEGORY dc, CPS_CATEGORY cc\n"
                    + "WHERE DC.IS_HEAD=1 AND DC.ASSET_VERSION=CC.ASSET_VERSION AND CC.CATEGORY_ID=DC.CATEGORY_ID\n" + "GROUP BY ECOMMERCE_DISPLAY \n"
                    + "HAVING count(*) > 0";

    private String productAuditReportQuery;
    private String productAliasAuditReportQuery;
    private FTPService ftpService;
    private boolean ftpAuditEnabled;
    private FTPConfig productAuditReportFtpConfig;
    private FTPConfig productAliasAuditReportFtpConfig;

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return the ftpService
     */
    public FTPService getFtpService() {
        return ftpService;
    }

    /**
     * @param ftpService
     *            the ftpService to set
     */
    public void setFtpService(FTPService ftpService) {
        this.ftpService = ftpService;
    }

    /**
     * @return the ftpAuditEnabled
     */
    public boolean isFtpAuditEnabled() {
        return ftpAuditEnabled;
    }

    /**
     * @param ftpAuditEnabled
     *            the ftpAuditEnabled to set
     */
    public void setFtpAuditEnabled(boolean ftpAuditEnabled) {
        this.ftpAuditEnabled = ftpAuditEnabled;
    }

    /**
     * @return the productAuditReportQuery
     */
    public String getProductAuditReportQuery() {
        return productAuditReportQuery;
    }

    /**
     * @param productAuditReportQuery
     *            the productAuditReportQuery to set
     */
    public void setProductAuditReportQuery(String productAuditReportQuery) {
        this.productAuditReportQuery = productAuditReportQuery;
    }

    /**
     * @return the productAliasAuditReportQuery
     */
    public String getProductAliasAuditReportQuery() {
        return productAliasAuditReportQuery;
    }

    /**
     * @param productAliasAuditReportQuery
     *            the productAliasAuditReportQuery to set
     */
    public void setProductAliasAuditReportQuery(String productAliasAuditReportQuery) {
        this.productAliasAuditReportQuery = productAliasAuditReportQuery;
    }

    /**
     * @return the productAuditReportFtpConfig
     */
    public FTPConfig getProductAuditReportFtpConfig() {
        return productAuditReportFtpConfig;
    }

    /**
     * @param productAuditReportFtpConfig
     *            the productAuditReportFtpConfig to set
     */
    public void setProductAuditReportFtpConfig(FTPConfig productAuditReportFtpConfig) {
        this.productAuditReportFtpConfig = productAuditReportFtpConfig;
    }

    /**
     * @return the productAliasAuditReportFtpConfig
     */
    public FTPConfig getProductAliasAuditReportFtpConfig() {
        return productAliasAuditReportFtpConfig;
    }

    /**
     * @param productAliasAuditReportFtpConfig
     *            the productAliasAuditReportFtpConfig to set
     */
    public void setProductAliasAuditReportFtpConfig(FTPConfig productAliasAuditReportFtpConfig) {
        this.productAliasAuditReportFtpConfig = productAliasAuditReportFtpConfig;
    }

    private void saveAuditReport(StatisticsService pStatistics, String pFileNamePrefix) {
        if (!isSaveAuditReport()) {
            getLoggerTools().logDebug("saveAuditReport(): isSaveAuditReport() = false, exit.");
            return;
        }
        IWriter auditReportWriter = getAuditReportSaver().getAuditReportWriter(pFileNamePrefix);
        if (null == auditReportWriter) {
            getLoggerTools().logDebug("saveAuditReport(): auditReportWriter = null, exit.");
            return;
        }

        // save Reference to JSON file that was processed
        String[] auditLine = new String[1];
        auditLine[0] = "Initial import files processed (F0005/JSON): ";
        auditReportWriter.writeNext(auditLine);
        logDebugAuditLine("AuditReport: ", auditLine);

        List<String> initialImportFiles = pStatistics.getInitialImportFiles();
        Set<String> initialImportFilesSet = new HashSet<String>();
        initialImportFilesSet.addAll(initialImportFiles);
        for (String fileName : initialImportFilesSet) {
            auditLine[0] = fileName;
            auditReportWriter.writeNext(auditLine);
            logDebugAuditLine("AuditReport: ", auditLine);
        }
        auditLine[0] = " ";
        auditReportWriter.writeNext(auditLine);

        // walk over all loaderStatistics
        List<StatisticsLoader> loaderStatistics = pStatistics.getLoaderStatistics();
        for (StatisticsLoader statisticsLoader : loaderStatistics) {

            // file name/status/fail reason/items processed
            auditLine = new String[1];
            auditLine[0] = statisticsLoader.getStatisticsPresentation();
            auditReportWriter.writeNext(auditLine);
            logDebugAuditLine("AuditReport: ", auditLine);

            // error file
            String errorFileName = statisticsLoader.getErrorFileName();
            if (!StringUtils.isBlank(errorFileName)) {
                auditLine = new String[2];
                auditLine[0] = "Error file name: ";
                auditLine[1] = statisticsLoader.getErrorFileName();
                auditReportWriter.writeNext(auditLine);
                logDebugAuditLine("AuditReport: ", auditLine);
            }

            // deployment Start Time
            auditLine = new String[2];
            auditLine[0] = "Deployment start time: ";
            auditLine[1] = formatDate(statisticsLoader.getStartTime());
            auditReportWriter.writeNext(auditLine);
            logDebugAuditLine("AuditReport: ", auditLine);

            // deployment End Time
            auditLine = new String[2];
            auditLine[0] = "Deployment end time: ";
            auditLine[1] = formatDate(statisticsLoader.getEndTime());
            auditReportWriter.writeNext(auditLine);
            logDebugAuditLine("AuditReport: ", auditLine);

            List<ItemStatus> itemStatuses = statisticsLoader.getItemStatuses();
            if (itemStatuses.size() > 0) {
                auditLine = new String[4];
                auditLine[0] = "item_number";
                auditLine[1] = "action";
                auditLine[2] = "old ST";
                auditLine[3] = "new ST";
                auditReportWriter.writeNext(auditLine);
                logDebugAuditLine("AuditReport: ", auditLine);

                for (ItemStatus itemStatus : itemStatuses) {
                    auditLine = new String[4];
                    auditLine[0] = itemStatus.getItemNumber();
                    auditLine[1] = itemStatus.getAction();
                    auditLine[2] = itemStatus.getOldST();
                    auditLine[3] = itemStatus.getNewST();
                    auditReportWriter.writeNext(auditLine);
                    logDebugAuditLine("AuditReport: ", auditLine);

                }
            }
            auditLine = new String[1];
            auditLine[0] = " ";
            auditReportWriter.writeNext(auditLine);
        }

        if (auditReportWriter != null) {
            auditReportWriter.close();
            getAuditReportSaver().setAuditReportWriter(null);
        }
    }

    private JSONObject getStringJson(String pObjName, String pValue) {
        JSONObject stringJson = null;
        try {
            stringJson = new JSONObject();
            stringJson.put(pObjName, pValue);
        } catch (JSONException ex) {
            getLoggerTools().logError(new StringBuilder().append("getStringJson(): ").append(pObjName).append(": ").append(pValue).toString());
        }
        return stringJson;
    }

    /**
     * show statistics
     *
     * @return statistics string presentation
     */
    public JSONObject getStatisticsPresentationJSON(StatisticsLoader pStatisticsLoader) throws JSONException {
        JSONObject statisticsItemJson = new JSONObject();

        try {
            statisticsItemJson.put("loader", pStatisticsLoader.getLoader().getLoaderName());
            statisticsItemJson.put("file_name", pStatisticsLoader.getFileNameOnly());
            statisticsItemJson.put("action", pStatisticsLoader.getAction());
            statisticsItemJson.put("success", pStatisticsLoader.isSucceeded());
            if (!pStatisticsLoader.isSucceeded()) {
                statisticsItemJson.put("fail_reason", pStatisticsLoader.getFailReason());
            }
            statisticsItemJson.put("total", pStatisticsLoader.getCountProceeded());
            statisticsItemJson.put("delete", pStatisticsLoader.getCountDelete());
            statisticsItemJson.put("insert", pStatisticsLoader.getCountInsert());
            statisticsItemJson.put("update", pStatisticsLoader.getCountUpdate());
            statisticsItemJson.put("skip", pStatisticsLoader.getCountSkipped());
            statisticsItemJson.put("error", pStatisticsLoader.getCountError());

            String deployment_start_time = formatDate(pStatisticsLoader.getStartTime());
            statisticsItemJson.put("deployment_start_time", deployment_start_time);

            String deployment_end_time = formatDate(pStatisticsLoader.getEndTime());
            statisticsItemJson.put("deployment_end_time", deployment_end_time);

            // error file
            String errorFileName = pStatisticsLoader.getErrorFileName();
            if (!StringUtils.isBlank(errorFileName)) {
                statisticsItemJson.put("error_file_name", errorFileName);
            }

            JSONArray itemStatuses_array = new JSONArray();
            List<ItemStatus> itemStatuses = pStatisticsLoader.getItemStatuses();
            if (itemStatuses.size() > 0) {
                for (ItemStatus itemStatus : itemStatuses) {
                    JSONObject itemStatusJson = new JSONObject();
                    itemStatusJson.put("item_number", itemStatus.getItemNumber());
                    itemStatusJson.put("action", itemStatus.getAction());
                    itemStatusJson.put("old_st", itemStatus.getOldST());
                    itemStatusJson.put("new_st", itemStatus.getNewST());
                    itemStatuses_array.put(itemStatusJson);
                }
            }

            statisticsItemJson.put("product_updates", itemStatuses_array);
        } catch (JSONException ex) {
            getLoggerTools().logError(
                            new StringBuilder().append("getStatisticsPresentationJSON(): ").append(pStatisticsLoader.getLoader().getLoaderName()).toString());
            getLoggerTools().logError(ex);
        }

        return statisticsItemJson;
    }

    private void saveAuditReportJson(StatisticsService pStatistics, String pFileNamePrefix) throws JSONException {
        if (!isSaveAuditReport()) {
            getLoggerTools().logDebug("saveAuditReport(): isSaveAuditReport() = false, exit.");
            return;
        }
        IWriter auditReportWriter = getAuditReportSaver().getAuditReportWriter(pFileNamePrefix);
        if (null == auditReportWriter) {
            getLoggerTools().logDebug("saveAuditReport(): auditReportWriter = null, exit.");
            return;
        }

        JSONObject reportMain = new JSONObject(); // main "report" document
        JSONObject reportItems = new JSONObject(); // main "report" document items
        JSONArray initial_import_files_array = new JSONArray(); // "initial_import_files" document

        // save Reference to JSON file that was processed
        String[] auditLine = new String[1];
        auditLine[0] = "Initial import files processed (F0005/JSON): ";
        logDebugAuditLine("AuditReport: ", auditLine);

        List<String> initialImportFiles = pStatistics.getInitialImportFiles();
        Set<String> initialImportFilesSet = new HashSet<String>();
        initialImportFilesSet.addAll(initialImportFiles);
        for (String fileName : initialImportFilesSet) {
            JSONObject fileJsonObj = getStringJson("name", fileName);
            initial_import_files_array.put(fileJsonObj);

            // debug
            auditLine[0] = fileName;
            logDebugAuditLine("AuditReport: ", auditLine);
        }
        reportItems.put("initial_import_files", initial_import_files_array);

        JSONArray statistics_items_array = new JSONArray(); // "statistics_items" document

        // walk over all loaderStatistics
        List<StatisticsLoader> loaderStatistics = pStatistics.getLoaderStatistics();
        for (StatisticsLoader statisticsLoader : loaderStatistics) {
            JSONObject statisticsItemJson = getStatisticsPresentationJSON(statisticsLoader);
            statistics_items_array.put(statisticsItemJson);
        }
        reportItems.put("statistics_items", statistics_items_array);
        reportMain.put("report", reportItems);

        auditReportWriter.writeNext(reportMain.toString());

        if (auditReportWriter != null) {
            auditReportWriter.close();
            getAuditReportSaver().setAuditReportWriter(null);
        }
    }

    public void saveAuditReportTest() {
        saveAuditReportTest(AUDIT_REPORT);
    }

    public void saveStockBeforeReportTest() {
        saveStockReport(STOCK_COUNT_BEFORE);
    }

    public void saveStockAfterReportTest() {
        saveStockReport(STOCK_COUNT_AFTER);
    }

    private void saveStockReport(String pFileNamePrefix) {
        if (!isSaveAuditReport()) {
            getLoggerTools().logDebug("saveAuditReport(): isSaveAuditReport() = false, exit.");
            return;
        }
        IWriter auditReportWriter = getAuditReportSaver().getAuditReportWriter(pFileNamePrefix, STOCK_REPORT_FILE_TYPE);
        if (null == auditReportWriter) {
            getLoggerTools().logDebug("saveAuditReport(): auditReportWriter = null, exit.");
            return;
        }

        // Query from DB:
        IConnector dbConnector = ((LoaderBase) getAuditReportSaver()).getConnector();

        String[] auditLine;
        try (Connection connection = ((GSARepository) dbConnector.getRepository()).getDataSource().getConnection();) {
            // products
            try (PreparedStatement ps = connection.prepareStatement(PRODUCT_COUNT_QUERY); ResultSet rs = ps.executeQuery();) {

                rs.next();
                String quantityOfProducts = String.valueOf(rs.getInt(1));
                auditLine = new String[2];
                auditLine[0] = "Product count:";
                auditLine[1] = quantityOfProducts;
                auditReportWriter.writeNext(auditLine);
                logDebugAuditLine("AuditReport: ", auditLine);
                rs.close();
                ps.close();
            }

            auditLine = new String[2];
            auditLine[0] = "S";
            auditLine[1] = "COUNT(*)";
            auditReportWriter.writeNext(auditLine);
            logDebugAuditLine("AuditReport: ", auditLine);

            try (PreparedStatement ps = connection.prepareStatement(STOCK_QUERY); ResultSet rs = ps.executeQuery();) {
                while (rs.next()) {
                    int i = 1;
                    String stockName = rs.getString(1);
                    String stockValue = String.valueOf(rs.getInt(2));

                    auditLine = new String[2];
                    auditLine[0] = stockName;
                    auditLine[1] = stockValue;
                    auditReportWriter.writeNext(auditLine);
                    logDebugAuditLine("AuditReport: ", auditLine);
                }
                rs.close();
                ps.close();
            }

            // blank line
            auditLine = new String[1];
            auditLine[0] = "";
            auditReportWriter.writeNext(auditLine);

            // category
            try (PreparedStatement ps = connection.prepareStatement(CATEGORY_OVERALL_COUNT_QUERY); ResultSet rs = ps.executeQuery();) {
                rs.next();
                String quantityOfCategory = String.valueOf(rs.getInt(1));
                auditLine = new String[2];
                auditLine[0] = "Category count:";
                auditLine[1] = quantityOfCategory;
                auditReportWriter.writeNext(auditLine);
                logDebugAuditLine("AuditReport: ", auditLine);
                rs.close();
                ps.close();
            }

            auditLine = new String[2];
            auditLine[0] = "ECOMMERCE_DISPLAY";
            auditLine[1] = "COUNT(*)";
            auditReportWriter.writeNext(auditLine);
            logDebugAuditLine("AuditReport: ", auditLine);

            try (PreparedStatement ps = connection.prepareStatement(CATEGORY_SGC_COUNT_QUERY); ResultSet rs = ps.executeQuery();) {
                while (rs.next()) {
                    int i = 1;
                    String stockName = String.valueOf(rs.getInt(1));
                    String stockValue = String.valueOf(rs.getInt(2));

                    auditLine = new String[2];
                    auditLine[0] = stockName;
                    auditLine[1] = stockValue;
                    auditReportWriter.writeNext(auditLine);
                    logDebugAuditLine("AuditReport: ", auditLine);
                }
                rs.close();
                ps.close();
            }

            connection.close();
        } catch (Exception e) {
            getLoggerTools().logError(e);
        }

        if (auditReportWriter != null) {
            auditReportWriter.close();
            getAuditReportSaver().setAuditReportWriter(null);
        }
    }

    protected void logDebugAuditLine(String pRowStart, String[] auditLine) {
        StringBuilder sb = new StringBuilder().append(pRowStart);
        for (String lineItem : auditLine) {
            sb.append(lineItem).append("\t");
        }
        getLoggerTools().logDebug(sb.toString());
    }

    private void saveAuditReportTest(String pFileNamePrefix) {
        IWriter auditReportWriter = getAuditReportSaver().getAuditReportWriter(pFileNamePrefix);
        if (null == auditReportWriter) {
            getLoggerTools().logDebug("saveAuditReport(): auditReportWriter = null, exit.");
            return;
        }
        if (!isSaveAuditReport()) {
            getLoggerTools().logDebug("saveAuditReport(): isSaveAuditReport() = false, exit.");
            return;
        }

        // save Reference to JSON file that was processed
        String[] auditLine = new String[2];
        auditLine[0] = "Initial import files processed (F0005/JSON): ";
        auditReportWriter.writeNext(auditLine);
        logDebugAuditLine("AuditReport: ", auditLine);

        auditLine = new String[4];
        auditLine[0] = "item_number";
        auditLine[1] = "action";
        auditLine[2] = "old ST";
        auditLine[3] = "new ST";
        auditReportWriter.writeNext(auditLine);
        logDebugAuditLine("AuditReport: ", auditLine);

        auditLine = new String[1];
        auditLine[0] = " ";
        auditReportWriter.writeNext(auditLine);

        if (auditReportWriter != null) {
            auditReportWriter.close();
            getAuditReportSaver().setAuditReportWriter(null);
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * launch loaders to work
     *
     * @param pStatistics
     *            statistics for service's loaders work
     */
    @Override
    protected void performService(StatisticsService pStatistics) {

        saveStockReport(STOCK_COUNT_BEFORE);
        setTerminate(false);
        for (String orderNumber : getLoaders().keySet()) {
            if (isTerminate()) {
                break;
            }
            ILoader loader = getLoaders().get(orderNumber);

            loader.load(pStatistics);
        }

        // saveAuditReport(pStatistics, AUDIT_REPORT);

        try {
            if (pStatistics.getInitialImportFiles() != null && pStatistics.getInitialImportFiles().size() > 0) {
                getLoggerTools().logDebug("pStatistics.getInitialImportFiles() :: " + pStatistics.getInitialImportFiles());
                generateProductAuditReport(pStatistics);
                generateAliasAuditReport(pStatistics);
                saveAuditReportJson(pStatistics, AUDIT_REPORT);
            } else {
                getLoggerTools().logDebug("Statistics show no initial import files, not writing any Audit Report.");
            }
        } catch (JSONException ex) {
            getLoggerTools().logError(new StringBuilder().append("performService(): ").toString());
            getLoggerTools().logError(ex);
        }

        saveStockReport(STOCK_COUNT_AFTER);

        setTerminate(false);
    }

    public void generateProductAuditReport(StatisticsService pStatistics) {
        getLoggerTools().logDebug("Entering generateProductAuditReport");
        List<String> initialImportFiles = pStatistics.getInitialImportFiles();
        Set<String> initialImportFilesSet = new HashSet<String>();
        initialImportFilesSet.addAll(initialImportFiles);
        String fileNameSuffix = null;
        for (String fileName : initialImportFilesSet) {
            if (fileName.toLowerCase().contains("cps_base_export")) {
                fileNameSuffix = fileName + "_";
                break;
            }
        }
        // List<FileInfo> fileInfoList = ((ProjectRepositoryLoader) getLoaders().get("12")).getImportTools().getFileList();
        // if (fileInfoList.size() > 0) {
        if (fileNameSuffix != null) {
            generateAuditReport("12", "ProductAuditReport_" + fileNameSuffix, getProductAuditReportQuery(), getProductAuditReportFtpConfig());
        } else {
            getLoggerTools().logDebug("No input files for Base, not generating Product Audit Report");
        }
        // }
        getLoggerTools().logDebug("Exiting generateProductAuditReport");
    }

    public void generateAliasAuditReport(StatisticsService pStatistics) {
        getLoggerTools().logDebug("Entering generateAliasAuditReport");
        // List<FileInfo> fileInfoList = ((ProjectRepositoryLoader) getLoaders().get("13")).getImportTools().getFileList();
        // if (fileInfoList.size() > 0) {
        List<String> initialImportFiles = pStatistics.getInitialImportFiles();
        Set<String> initialImportFilesSet = new HashSet<String>();
        initialImportFilesSet.addAll(initialImportFiles);
        String fileNameSuffix = null;
        for (String fileName : initialImportFilesSet) {
            if (fileName.toLowerCase().contains("cps_alias_export")) {
                fileNameSuffix = fileName + "_";
                break;
            }
        }
        if (fileNameSuffix != null) {
            generateAuditReport("13", "ProductAliasReport_" + fileNameSuffix, getProductAliasAuditReportQuery(), getProductAliasAuditReportFtpConfig());
        } else {
            getLoggerTools().logDebug("No input files for Alias, not generating Product Alias Audit Report");
        }
        // }
        getLoggerTools().logDebug("Exiting generateAliasAuditReport");
    }

    public void generateAuditReport(String loaderKey, String filePrefix, String query, FTPConfig ftpConfig) {
        getLoggerTools().logDebug("Entering generateAuditReport");
        IWriter auditReportWriter = ((ProjectRepositoryLoader) getLoaders().get(loaderKey)).getAuditReportWriter(filePrefix, ".csv");
        if (null == auditReportWriter) {
            getLoggerTools().logDebug("generateAuditReport(): auditReportWriter = null, exit.");
            return;
        }
        ((CSVBase) auditReportWriter).setQuoteChar(CSVBase.NO_QUOTE_CHARACTER);
        // Query from DB:

        IConnector dbConnector = ((ProjectRepositoryLoader) getLoaders().get(loaderKey)).getConnectorProd();
        Connection connection = null;
        // products
        PreparedStatement ps = null;
        ResultSet rset = null;
        String[] auditLine;
        try {
            connection = ((GSARepository) dbConnector.getRepository()).getDataSource().getConnection();
            // products
            ps = connection.prepareStatement(query);
            rset = ps.executeQuery();

            ResultSetMetaData rsmd = rset.getMetaData();
            rset.next();
            auditLine = new String[rsmd.getColumnCount()];
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                auditLine[i - 1] = rsmd.getColumnName(i);
            }
            auditReportWriter.writeAuditNext(auditLine);
            while (rset.next()) {
                auditLine = new String[rsmd.getColumnCount()];
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    StringBuilder sb = new StringBuilder();
                    if (rset.getString(i) != null) {
                        sb.append(rset.getString(i));
                    } else {
                        sb.append("");
                    }
                    auditLine[i - 1] = sb.toString();
                }
                auditReportWriter.writeAuditNext(auditLine);
            }

        } catch (Exception e) {
            getLoggerTools().logError(e);
        } finally {
            try {
                rset.close();
                ps.close();
                connection.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        if (auditReportWriter != null) {
            auditReportWriter.close();
            if (isFtpAuditEnabled()) {
                try {
                    getFtpService().uploadFile(ftpConfig.getFtpHostName(), ftpConfig.getFtpPort(), ftpConfig.getFtpUserName(), ftpConfig.getFtpUserPassword(),
                                    ftpConfig.getFtpRemoteDirectory(), auditReportWriter.getFile());
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            ((ProjectRepositoryLoader) getLoaders().get(loaderKey)).setAuditReportWriter(null);
        }
        getLoggerTools().logDebug("Exiting generateAuditReport");
    }
}
