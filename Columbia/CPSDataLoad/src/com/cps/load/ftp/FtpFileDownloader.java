package com.cps.load.ftp;

import atg.core.util.StringUtils;
import com.cps.ftp.FTPConfig;
import com.cps.ftp.FTPService;
import vsg.load.info.StatisticsService;
import vsg.load.loader.Record;
import vsg.load.loader.impl.Loader;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Andy Porter
 *         <p>
 *         Class that download files from ftp location
 */
public class FtpFileDownloader extends Loader {

	private FTPConfig mFtpConfig;
	private boolean mFtpEnabled = false;
	/**
	 * FTP Service property
	 */
	private FTPService mFtpService;
	private boolean mUseFilePrefix = false;
	private boolean mDeleteFile = false;

	@Override
	public synchronized void load(StatisticsService pStatisticsService) {
		vlogDebug(new StringBuilder().append(getAbsoluteName()).append("load() - start").toString());
		if (isFtpEnabled() && null != getFtpConfig() && null != getImportTools()) {
			vlogDebug(new StringBuilder().append(getAbsoluteName()).append(".performTask").append(" - start").toString());

			boolean successfulDownload = false;
			try {
				// check for files in the ftp upload directory
				List<String> ftpFilenames = getFtpService().retrieveFileList(getFtpHost(), getFtpPort(), getFtpUsername(), getFtpPassword(), getFtpTarget());

				if (ftpFilenames != null && ftpFilenames.size() > 0) {
					Iterator fileIterator = ftpFilenames.iterator();
					while (fileIterator.hasNext()) {
						String fileName = (String) fileIterator.next();
						vlogDebug(new StringBuilder().append("Found filename:").append(fileName).append(" on ftpHost:").append(getFtpHost())
								.append(". Check for filtering on filePrefix:").append(getFilePrefix()).toString());

						// validate local directory exists
						File localDirectory = new File(getLocalDirectory());

						if (!localDirectory.exists()) {
							vlogDebug(new StringBuilder().append("Creating local directory:").append(getLocalDirectory()).toString());
							localDirectory.mkdirs();
						} else {
							vlogDebug(new StringBuilder().append("Local directory exists:").append(getLocalDirectory())
									.append(", proceed attempting to downloading.").toString());
						}

						// check if filtering files by prefix
						if (!StringUtils.isBlank(getFilePrefix())) {
							if (isLoggingDebug())
								logDebug(new StringBuilder().append("Filtering files in directory on prefix:").append(getFilePrefix()).toString());
							if (fileName.contains(getFilePrefix())) {
								vlogDebug(new StringBuilder().append("Prefix match, download file:").append(fileName)
										.append(" to localDirectory:").append(getLocalDirectory()).append(".").toString());

								successfulDownload = getFtpService().downloadFile(getFtpHost(), getFtpPort(), getFtpUsername(), getFtpPassword(), getFtpTarget(), fileName, getLocalDirectory());
							} else {
								vlogDebug(new StringBuilder().append("Skipping file:").append(fileName)
										.append(" since it does not match the correct prefix filter:").append(getFilePrefix()).append(".").toString());
							}
						} else {
							vlogDebug(new StringBuilder().append("No file prefix to filer on, download file:").append(fileName).append(".").toString());
							successfulDownload = getFtpService().downloadFile(getFtpHost(), getFtpPort(), getFtpUsername(), getFtpPassword(), getFtpTarget(), fileName, getLocalDirectory());
						}
						// report if download was success
						vlogDebug(new StringBuilder().append("File named:").append(fileName).append(" successfully downloaded:").append(successfulDownload)
								.append(" from ftpHost:").append(getFtpHost()).append(" with target directory:").append(getFtpTarget())
								.append(" to local directory:").append(getLocalDirectory()).toString());

						if (isDeleteFile()) {
							// Delete file from remote directory
							boolean successfulDeleted = getFtpService().deleteFile(getFtpHost(), getFtpPort(), getFtpUsername(), getFtpPassword(), getFtpTarget(), fileName);
							// report if delete was success
							vlogDebug(new StringBuilder().append("File named:").append(fileName).append(" successfully deleted:").append(successfulDeleted)
									.append(" from ftpHost:").append(getFtpHost()).append(" with target directory:").append(getFtpTarget()).toString());
						}

						// reset success flag to false
						successfulDownload = false;
					}
				} else {
					// No files on ftp server
					vlogDebug(new StringBuilder().append(getAbsoluteName()).append(".performTask - No files on ftp server under target:")
							.append(getFtpTarget()).toString());
				}
			} catch (Exception e) {
				vlogError(e, new StringBuilder().append(getAbsoluteName()).append(".performTask Exception: ").append(e).toString());
			}
			vlogDebug(new StringBuilder().append(getAbsoluteName()).append(".performTask").append(" - exit").toString());

		}
		vlogDebug(new StringBuilder().append(getAbsoluteName()).append("load() - end").toString());
	}


	//	*************************************************************
	public String getFtpHost() {
		return getFtpConfig().getFtpHostName();
	}

	int getFtpPort() {
		return getFtpConfig().getFtpPort();
	}

	public String getFtpUsername() {
		return getFtpConfig().getFtpUserName();
	}

	public String getFtpPassword() {
		return getFtpConfig().getFtpUserPassword();
	}

	public String getFtpTarget() {
		String ftpRemoteDirectory = getFtpConfig().getFtpRemoteDirectory();
		String localDirectory = getImportTools().getPathInfo().getInputDir();
		String remotePath = new StringBuilder().append(ftpRemoteDirectory).append(localDirectory).toString();
		return remotePath;
	}

	public String getLocalDirectory() {
		String localDirectory = getImportTools().getPathInfo().getInputDir();
		return localDirectory;
	}

	public String getFilePrefix() {
		String filePrefix = null;
		if (isUseFilePrefix()) {
			filePrefix = getImportTools().getPathInfo().getFilePrefix();
		}
		return filePrefix;
	}

	//	*************************************************************
	@Override
	public void insert(Record pRecord) {
	}

	@Override
	public void update(Record pRecord) {
	}

	@Override
	public void delete(Record pRecord) {
	}


	public FTPConfig getFtpConfig() {
		return mFtpConfig;
	}

	public void setFtpConfig(FTPConfig pFtpConfig) {
		mFtpConfig = pFtpConfig;
	}

	public boolean isFtpEnabled() {
		return mFtpEnabled;
	}

	public void setFtpEnabled(boolean pFtpEnabled) {
		mFtpEnabled = pFtpEnabled;
	}

	public FTPService getFtpService() {
		return mFtpService;
	}

	public void setFtpService(FTPService pFtpService) {
		mFtpService = pFtpService;
	}

	public boolean isUseFilePrefix() {
		return mUseFilePrefix;
	}

	public void setUseFilePrefix(boolean pUseFilePrefix) {
		mUseFilePrefix = pUseFilePrefix;
	}

	public boolean isDeleteFile() {
		return mDeleteFile;
	}

	public void setDeleteFile(boolean pDeleteFile) {
		mDeleteFile = pDeleteFile;
	}
}
