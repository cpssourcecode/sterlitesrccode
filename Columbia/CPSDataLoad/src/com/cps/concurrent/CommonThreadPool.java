/**
 *
 */
package com.cps.concurrent;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;

/**
 * @author R.Srinivasan An utility class to create and maintain ThreadPools.
 */
@SuppressWarnings("PMD")
public class CommonThreadPool extends GenericService {

    private static ThreadFactory factory = new ExceptionThreadFactory(new CPSExceptionHandler());

    private ExecutorService threadPool;

    /**
     * executes when this component is started by the nucleus. The ThreadPool is created with the ThreadFactory class, which in turn is passed our Custom
     * Exception Handler to handle uncaught Exceptions during async calls.
     */
    @Override
    public void doStartService() throws ServiceException {
        // threadPool = Executors.newCachedThreadPool();
        threadPool = Executors.newCachedThreadPool(factory);
    }

    /**
     * executes when the component is stopped
     */
    @Override
    public void doStopService() throws ServiceException {

        super.doStopService();
        shutDownThreadPool();
    }

    /**
     * An utility method to shutdown the Executor Service
     */
    public void shutDownThreadPool() {
        if (threadPool != null) {
            threadPool.shutdown();
        }
    }

    /**
     *
     * @return ExecutorService
     *
     */
    public ExecutorService getThreadPool() {
        return threadPool;
    }

    /**
     * when the Future task is throwing any exception in its run() method, then the only way to catch it is by wrapping the future.get() method in a try/catch
     * block and catch the ExecutionException but, in an asynchronous task, we dont call the get() method, since it is a blocking one. in that case, the
     * exception is not throws/reported back to the caller by the child thread running inside the run() method to log those exceptions, this
     * ThreadFactory+UncaughtExceptionHandler must be used when creating the ThreadPool
     *
     * @author R.Srinivasan
     *
     */
    @SuppressWarnings("PMD")
    public static class ExceptionThreadFactory implements ThreadFactory {

        private static ThreadFactory defaultFactory = Executors.defaultThreadFactory();
        private Thread.UncaughtExceptionHandler handler;

        /**
         *
         * @param handler
         */
        public ExceptionThreadFactory(Thread.UncaughtExceptionHandler handler) {
            this.handler = handler;
        }

        /**
         *
         */
        @Override
        public Thread newThread(Runnable run) {
            Thread thread = defaultFactory.newThread(run);
            thread.setUncaughtExceptionHandler(handler);
            return thread;
        }
    }

    /**
     * An ExceptionHandler class to handle UncaughtExceptions from the run() method especially when doing an async call.
     *
     * @author R.Srinivasan
     *
     */
    public static class CPSExceptionHandler extends GenericService implements Thread.UncaughtExceptionHandler {
        // ...

        @Override
        public void uncaughtException(Thread thread, Throwable thr) {
            vlogError("errors occurred in the child thread - {0} - inside run() method - {1}", thread, getName(), thr.getMessage());
            logError(thr);
        }
    }

}
