package com.cps.ftp;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.apache.commons.net.PrintCommandListener;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

import atg.nucleus.GenericService;

/**
 * FTP Service
 * <p>
 * <h4>Description</h4> This class will handle all FTP services need for the
 * application.
 * <p>
 * <h4>Notes</h4>
 *
 * @author Andy Porter
 */
public class FTPService extends GenericService {

	/**
	 * component name for debugging
	 */
	private static final String COMPONENT_NAME = "/cps/ftp/FTPService";

	/**
	 * Binary transfer property
	 */
	private boolean mBinaryTransfer;

	/**
	 * Default port property - if no port is specified
	 */
	private int mDefaultPort;

	// ~ Methods
	// ----------------------------------------------------------------------------------------------------------------------------

	/**
	 * @return the binaryTransfer
	 */
	public boolean isBinaryTransfer() {
		return this.mBinaryTransfer;
	}

	/**
	 * @param pBinaryTransfer the binaryTransfer to set
	 */
	public void setBinaryTransfer(boolean pBinaryTransfer) {
		this.mBinaryTransfer = pBinaryTransfer;
	}

	/**
	 * @return the defaultPort
	 */
	public int getDefaultPort() {
		return this.mDefaultPort;
	}

	/**
	 * @param pDefaultPort the defaultPort to set
	 */
	public void setDefaultPort(int pDefaultPort) {
		this.mDefaultPort = pDefaultPort;
	}

	/**
	 * Helper method to get connected to the FPT Host
	 *
	 * @param pClient
	 * @param pHostName
	 * @param pPort
	 * @param pUserName
	 * @param pPassword
	 * @throws Exception
	 */
	public void connectToFTP(FTPClient pClient, String pHostName, int pPort, String pUserName, String pPassword) throws Exception {
		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".connectToFTP").append(" - start").toString());

		StringBuilder errorMessage = new StringBuilder();
		// connect to client
		pClient.connect(pHostName, (pPort > 0) ? pPort : getDefaultPort());

		// test connection to client
		int replyCode = pClient.getReplyCode();
		if (!FTPReply.isPositiveCompletion(replyCode)) {
			vlogDebug(new StringBuilder().append("Connection to client failed with replyCode:").append(replyCode).toString());

			pClient.disconnect();
			errorMessage.append("FTP Server refused connection");
			throw new Exception(errorMessage.toString());
		} else {
			vlogDebug(new StringBuilder().append("Connected to client with replyCode:").append(replyCode).toString());

			if (pClient.login(pUserName, pPassword)) {
				vlogDebug(new StringBuilder().append("Logged into FTP service:").append(pHostName).toString());
			} else {
				errorMessage.append("Could not login to FTP:").append(pHostName).append(" with userName:").append(pUserName);
				vlogDebug(errorMessage.toString());

				pClient.logout();
				pClient.disconnect();
				throw new Exception(errorMessage.toString());
			}
		}

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".connectToFTP").append(" - exit").toString());

	}

	/**
	 * Ftp to to the host and check for files based on the FILE_TYPE 0.
	 *
	 * @param pHostName
	 * @param pUserName
	 * @param pPassword
	 * @param pRemoteTargetDirectory - name of the directory to check for files on the server
	 * @return ArrayList of file names
	 */
	public List<String> retrieveFileList(String pHostName, int pPort, String pUserName, String pPassword, String pRemoteTargetDirectory)
			throws Exception {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".retrieveFileList - start").append("\n pHostName=").append(pHostName)
				.append(", pPort=").append(pPort).append(", pUserName=").append(pUserName).append(", pPassword= XXXXXXX")
				.append(", pRemoteTarget=").append(pRemoteTargetDirectory).toString());

		StringBuilder errorMessage = new StringBuilder();

		FTPClient client = new FTPClient();
		ArrayList<String> fileList = null;
		try (PrintWriter printWriter = new PrintWriter(System.out);) {
			client.addProtocolCommandListener(new PrintCommandListener(printWriter));
			try {
				// connect to the FTP host
				connectToFTP(client, pHostName, pPort, pUserName, pPassword);

				// Use passive mode as default because most of us are
				// behind firewalls these days.
				client.enterLocalPassiveMode();

				boolean directoryExists = client.changeWorkingDirectory(pRemoteTargetDirectory);
				if (directoryExists) {
					vlogDebug(new StringBuilder().append("Target directory:").append(pRemoteTargetDirectory)
							.append(" exists on the FTP server:").append(pHostName).toString());
					// Obtain a list of filenames in the current working
					// directory. When no file found an empty array will
					// be returned.

					fileList = new ArrayList<String>();
					FTPFile[] ftpFiles = client.listFiles(pRemoteTargetDirectory);
					for (FTPFile ftpFile : ftpFiles) {
						//
						// Check if FTPFile is a regular file (0)
						if (ftpFile.getType() == FTPFile.FILE_TYPE) {
							vlogDebug(new StringBuilder().append("Found file on server:").append(ftpFile.getName()).append(" to add to fileList.").toString());
							fileList.add(ftpFile.getName());
						} else {
							vlogDebug(new StringBuilder().append("FTPFile:").append(ftpFile.getName()).append(" has type:").append(ftpFile.getType())
									.append(".  It is not a file type, it does not need to be listed for potential download.").toString());
						}
					}
				} else {
					errorMessage.append("Target directory:").append(pRemoteTargetDirectory).append(" does not exist on FTP server:").append(pHostName);
					logError(errorMessage.toString());
					throw new Exception(errorMessage.toString());
				}
			} finally {
				if (client.isConnected()) {
					try {
						client.logout();
						client.disconnect();
					} catch (IOException ignoreException) {
						errorMessage.append("retrieveFileList() ignore IOException:").append(ignoreException);
						vlogWarning(errorMessage.toString());
					}
				}
			}
		}

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".retrieveFileList").append(" - exit").toString());
		return fileList;
	}

	/**
	 * The method to move a file in the remote system.
	 *
	 * @param pHostName
	 * @param pPort
	 * @param pUserName
	 * @param pPassword
	 * @param pRemoteSourceDirectory
	 * @param pFilename
	 * @param pRemoteTargetDirectory
	 * @return
	 * @throws Exception
	 */
	public synchronized boolean moveFile(String pHostName, int pPort,
										 String pUserName, String pPassword, String pRemoteSourceDirectory,
										 String pFilename, String pRemoteTargetDirectory) throws Exception {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".moveFile - start")
				.append("\n pHostName=").append(pHostName).append(", pPort=").append(pPort).append(", pUserName=").append(pUserName)
				.append(", pPassword= XXXXXXX").append(", pRemoteSourceDirectory=").append(pRemoteSourceDirectory)
				.append(", pFilename=").append(pFilename).append(", pRemoteTargetDirectory=").append(pRemoteTargetDirectory).toString());

		StringBuilder errorMessage = new StringBuilder();
		boolean fileDownloaded = false;

		FTPClient client = new FTPClient();
		try (PrintWriter printWriter = new PrintWriter(System.out);) {
			client.addProtocolCommandListener(new PrintCommandListener(printWriter));

			try {
				// connect to the FTP host
				connectToFTP(client, pHostName, pPort, pUserName, pPassword);

				// default is FTP.ASCII_FILE_TYPE
				if (this.isBinaryTransfer()) {
					client.setFileType(FTP.BINARY_FILE_TYPE);
				}
				// Use passive mode as default because most of us are
				// behind firewalls these days.
				client.enterLocalPassiveMode();

				boolean directoryExists = client.changeWorkingDirectory(pRemoteTargetDirectory);

				if (directoryExists) {
					directoryExists = client.changeWorkingDirectory(pRemoteSourceDirectory);

					if (directoryExists) {
						vlogDebug(new StringBuilder().append("Changed to source directory:").append(pRemoteSourceDirectory)
								.append(" on the FTP server:").append(pHostName).toString());

						String from = new StringBuilder().append(pRemoteSourceDirectory).append("/").append(pFilename).toString();
						String to = new StringBuilder().append(pRemoteTargetDirectory).append("/").append(pFilename).toString();

						vlogDebug(new StringBuilder().append("Moving:").append(from).append(" on the FTP server to:").append(to).toString());

						client.rename(from, to);
					} else {
						logError(new StringBuilder().append("Source directory:").append(pRemoteSourceDirectory)
								.append(" does not exist on FTP server:").append(pHostName).toString());
						fileDownloaded = false;
					}
				} else {
					logError(new StringBuilder().append("Target directory:").append(pRemoteTargetDirectory)
							.append(" does not exist on FTP server:").append(pHostName).toString());
					fileDownloaded = false;
				}
			} finally {
				if (client.isConnected()) {
					try {
						client.logout();
						client.disconnect();
					} catch (IOException ignoreException) {
						errorMessage.append("downloadFile() ignore IOException:").append(ignoreException);
						vlogWarning(errorMessage.toString());
					}
				}
			}
		}

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".downloadFile").append(" - exit").toString());
		return fileDownloaded;
	}

	/**
	 * The method to upload a file and save it onto the remote drive of the
	 * client.
	 *
	 * @param pHostName
	 * @param pPort
	 * @param pUserName
	 * @param pPassword
	 * @param pRemoteTargetDirectory
	 * @param pFile
	 * @return
	 * @throws Exception
	 */
	public synchronized boolean uploadFile(String pHostName, int pPort,
										   String pUserName, String pPassword, String pRemoteTargetDirectory,
										   File pFile) throws Exception {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".uploadFile - start").append("\n pHostName=").append(pHostName)
				.append(", pPort=").append(pPort).append(", pUserName=").append(pUserName).append(", pPassword= XXXXXXX")
				.append(", pRemoteTarget=").append(pRemoteTargetDirectory).append(", pFile=").append(pFile).toString());

		StringBuilder errorMessage = new StringBuilder();
		boolean fileDownloaded = false;
		FTPClient client = new FTPClient();

		try (PrintWriter printWriter = new PrintWriter(System.out);) {
			client.addProtocolCommandListener(new PrintCommandListener(printWriter));
			try {
				// connect to the FTP host
				connectToFTP(client, pHostName, pPort, pUserName, pPassword);

				// default is FTP.ASCII_FILE_TYPE
				if (this.isBinaryTransfer()) {
					client.setFileType(FTP.BINARY_FILE_TYPE);
				}
				// Use passive mode as default because most of us are
				// behind firewalls these days.
				client.enterLocalPassiveMode();

				boolean directoryExists = client.changeWorkingDirectory(pRemoteTargetDirectory);
				if (directoryExists) {
					vlogDebug(new StringBuilder().append("Changed to target directory:").append(pRemoteTargetDirectory)
							.append(" on the FTP server:").append(pHostName).toString());

					try (FileInputStream fis = new FileInputStream(pFile);) {
						client.storeFile(pFile.getName(), fis);
						fis.close();
					}
				} else {
					logError(new StringBuilder().append("Target directory:").append(pRemoteTargetDirectory)
							.append(" does not exist on FTP server:").append(pHostName).toString());
					fileDownloaded = false;
				}
			} finally {
				if (client.isConnected()) {
					try {
						client.logout();
						client.disconnect();
					} catch (IOException ignoreException) {
						errorMessage.append("downloadFile() ignore IOException:").append(ignoreException);
						vlogWarning(errorMessage.toString());
					}
				}
			}
		}

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".downloadFile").append(" - exit").toString());
		return fileDownloaded;
	}


	/**
	 * The method to download a file and save it onto the local drive of the
	 * client.
	 *
	 * @param pHostName
	 * @param pPort
	 * @param pUserName
	 * @param pPassword
	 * @param pRemoteTargetDirectory
	 * @param pFilename
	 * @param pLocalDirectory
	 * @return
	 * @throws Exception
	 */
	public synchronized boolean downloadFile(String pHostName, int pPort, String pUserName, String pPassword, String pRemoteTargetDirectory,
											 String pFilename, String pLocalDirectory) throws Exception {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".downloadFile - start").append("\n pHostName=").append(pHostName)
				.append(", pPort=").append(pPort).append(", pUserName=").append(pUserName).append(", pPassword= XXXXXXX")
				.append(", pRemoteTarget=").append(pRemoteTargetDirectory).append(", pFilename=").append(pFilename)
				.append(", pLocalDirectory=").append(pLocalDirectory).toString());

		StringBuilder errorMessage = new StringBuilder();
		boolean fileDownloaded = false;

		FTPClient client = new FTPClient();

		try (PrintWriter printWriter = new PrintWriter(System.out);) {
			client.addProtocolCommandListener(new PrintCommandListener(printWriter));
			try {    // connect to the FTP host
				connectToFTP(client, pHostName, pPort, pUserName, pPassword);

				// default is FTP.ASCII_FILE_TYPE
				if (this.isBinaryTransfer()) {
					client.setFileType(FTP.BINARY_FILE_TYPE);
				}
				// Use passive mode as default because most of us are
				// behind firewalls these days.
				client.enterLocalPassiveMode();

				boolean directoryExists = client.changeWorkingDirectory(pRemoteTargetDirectory);
				if (directoryExists) {
					vlogDebug(new StringBuilder().append("Changed to target directory:").append(pRemoteTargetDirectory)
							.append(" on the FTP server:").append(pHostName).toString());
					// output file
					StringBuilder localPath = new StringBuilder();
//					localPath.append(pLocalDirectory).append("/").append(pFilename);
					localPath.append(pLocalDirectory).append(pFilename);

					try (FileOutputStream output = new FileOutputStream(localPath.toString());) {
						StringBuilder remotePath = new StringBuilder();
//						remotePath.append(pRemoteTargetDirectory).append("/").append(pFilename);
						remotePath.append(pRemoteTargetDirectory).append(pFilename);

						vlogDebug(new StringBuilder().append("Downloading file:").append(remotePath.toString()).toString());
						fileDownloaded = client.retrieveFile(remotePath.toString(), output);
						output.close();
					}
				} else {
					logError(new StringBuilder().append("Target directory:").append(pRemoteTargetDirectory)
							.append(" does not exist on FTP server:").append(pHostName).toString());
					fileDownloaded = false;
				}
			} finally {
				if (client.isConnected()) {
					try {
						client.logout();
						client.disconnect();
					} catch (IOException ignoreException) {
						errorMessage.append("downloadFile() ignore IOException:").append(ignoreException);
						vlogWarning(errorMessage.toString());
					}
				}
			}
		}

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".downloadFile").append(" - exit").toString());
		return fileDownloaded;
	}

	/**
	 * This method will delete the file on the remote FTP host.
	 *
	 * @param pHostName
	 * @param pPort
	 * @param pUserName
	 * @param pPassword
	 * @param pRemoteTargetDirectory
	 * @param pFilename
	 * @return
	 * @throws Exception
	 */
	public synchronized boolean deleteFile(String pHostName, int pPort,
										   String pUserName, String pPassword, String pRemoteTargetDirectory,
										   String pFilename) throws Exception {

		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".deleteFile - start").append("\n pHostName=").append(pHostName)
				.append(", pPort=").append(pPort).append(", pUserName=").append(pUserName).append(", pPassword= XXXXXXX")
				.append(", pRemoteTarget=").append(pRemoteTargetDirectory).append(", pFilename=").append(pFilename).toString());

		boolean fileDeleted = false;
		StringBuilder errorMessage = new StringBuilder();
		FTPClient client = new FTPClient();

		try (PrintWriter printWriter = new PrintWriter(System.out);) {
			client.addProtocolCommandListener(new PrintCommandListener(printWriter));
			try {
				// connect to the FTP host
				connectToFTP(client, pHostName, pPort, pUserName, pPassword);
				boolean directoryExists = client.changeWorkingDirectory(pRemoteTargetDirectory);
				if (directoryExists) {

					vlogDebug(new StringBuilder().append("Target directory:").append(pRemoteTargetDirectory)
							.append(" exists on the FTP server:").append(pHostName).toString());
					StringBuilder path = new StringBuilder();
					path.append("/").append(pRemoteTargetDirectory).append("/").append(pFilename);

					vlogDebug(new StringBuilder().append("Deleting file:").append(path.toString()).toString());
					fileDeleted = client.deleteFile(path.toString());
				} else {
					logError(new StringBuilder().append("Target directory:").append(pRemoteTargetDirectory)
							.append(" does not exist on FTP server:").append(pHostName).toString());
					fileDeleted = false;
				}
			} finally {
				if (client.isConnected()) {
					try {
						client.logout();
						client.disconnect();
					} catch (IOException ignoreException) {
						errorMessage.append("deleteFile() ignore IOException:").append(ignoreException);
						vlogWarning(errorMessage.toString());
					}
				}
			}
		}


		vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".deleteFile").append(" - exit").toString());
		return fileDeleted;
	}
}
//571