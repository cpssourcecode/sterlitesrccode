package com.cps.ftp;

import atg.nucleus.GenericService;

/**
 * @author Andy Porter
 */
public class FTPConfig extends GenericService {


	private String mFtpHostName;
	private int mFtpPort;
	private String mFtpUserName;
	private String mFtpUserPassword;
	private String mFtpRemoteDirectory;


	public String getFtpHostName() {
		return mFtpHostName;
	}

	public void setFtpHostName(String pFtpHostName) {
		mFtpHostName = pFtpHostName;
	}

	public int getFtpPort() {
		return mFtpPort;
	}

	public void setFtpPort(int pFtpPort) {
		mFtpPort = pFtpPort;
	}

	public String getFtpUserName() {
		return mFtpUserName;
	}

	public void setFtpUserName(String pFtpUserName) {
		mFtpUserName = pFtpUserName;
	}

	public String getFtpUserPassword() {
		return mFtpUserPassword;
	}

	public void setFtpUserPassword(String pFtpUserPassword) {
		mFtpUserPassword = pFtpUserPassword;
	}

	public String getFtpRemoteDirectory() {
		return mFtpRemoteDirectory;
	}

	public void setFtpRemoteDirectory(String pFtpRemoteDirectory) {
		mFtpRemoteDirectory = pFtpRemoteDirectory;
	}
}
