package com.cps.taxonomy.parser;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import vsg.load.file.csv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anton Nadezhkin
 */
public class ImageParser {

	private static final ApplicationLogging mLogger = ClassLoggingFactory.getFactory().getLoggerForClass(ImageParser.class);

	private static final String HEADER = "/atg/commerce/catalog/SecureProductCatalog:product,,, ,LOCALE=en_US\n" + "ID,web_url,thumbnail_url,,\n";

	private static final String IMAGE_CONTEXT_PATH = "/assets/images/product/";

	private static final String csvInputFile = "d:\\111\\CPS_Images\\Image_Mapping_040918.csv";

	private static final String csvOutputFile = "d:\\111\\CPS_Images\\Image_Mapping_040918-out.csv";

	private static final int INPUT_ID_COLUMN = 1;

	private static final int INPUT_IMAGE_NAME_COLUMN = 6;

	private static final String DELIMITER = ",";

	private static final String END_OF_LINE = "\n";

	public static void main(String[] args){
		CSVReader reader = null;
		PrintWriter writer = null;

		try{
			reader = new CSVReader(new FileReader(csvInputFile));
			writer = new PrintWriter(new File(csvOutputFile));
			String[] line;
			reader.readNext();
			writer.write(HEADER);
			List<StringBuilder> stringBuilders = new ArrayList<>();
			List<String> ids = new ArrayList<>();
			while ((line = reader.readNext()) != null) {
				StringBuilder sb = new StringBuilder();
				sb.append(line[INPUT_ID_COLUMN]);
				sb.append(DELIMITER);
				sb.append(IMAGE_CONTEXT_PATH + line[INPUT_IMAGE_NAME_COLUMN]);
				sb.append(DELIMITER);
				sb.append(IMAGE_CONTEXT_PATH + line[INPUT_IMAGE_NAME_COLUMN]);
				sb.append(DELIMITER);
				sb.append(DELIMITER);
				sb.append(END_OF_LINE);
				writer.write(sb.toString());
			}
		} catch (IOException e) {
			mLogger.logError(e);
		} finally {
			if (writer != null){
				writer.close();
			}
		}

	}

}
