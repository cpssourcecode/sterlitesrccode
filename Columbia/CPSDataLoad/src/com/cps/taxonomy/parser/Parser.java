package com.cps.taxonomy.parser;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import vsg.load.file.csv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Parser {

	private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(Parser.class);

	private static final String HEADER = "/atg/commerce/catalog/SecureProductCatalog:category, ,TIMEFORMAT=M/d/yyyy h:mm a, ,LOCALE=en_US,,,,,,\nID,displayName,categoryFacets,description,eCommerceDisplay,fixedChildCategories,adCategoryCode,longDescription,metaDescription,metaKeywords,thumbnail_url\n";

	private static final String IMAGE_CONTEXT_PATH = "/assets/images/category-images/";

	private static final int CP_WEB_CAT_POSITION = 11;

	private static final int AD_CATEGORY_CODE_POSITION = 12;

	private static final int LVL_POSITION = 13;

	private static final int IMAGE_POSITION = 15;

	private static final int FIRST_FACET_POSITION = 16;

	private static final int LAST_FACET_POSITION = 22;

	public static void main(String[] args) {
		String csvInputFile = "C:\\Users\\user\\Downloads\\CPS_ATG_Taxonomy_VSG_032818.csv";
		String csvOutputFile = "C:\\Users\\user\\Downloads\\CPS_ATG_Taxonomy_VSG_032818-out.csv";
		List<String[]> categoryList = new ArrayList<>();

		CSVReader reader = null;
		PrintWriter writer = null;


		try {
			reader = new CSVReader(new FileReader(csvInputFile));
			writer = new PrintWriter(new File(csvOutputFile));

			String[] line;
			reader.readNext();
			writer.write(HEADER);
			List<StringBuilder> stringBuilders = new ArrayList<>();
			List<String> ids = new ArrayList<>();
			while ((line = reader.readNext()) != null) {
				StringBuilder sb = new StringBuilder();
				categoryList.add(line);

				//offset check
				int offset = 1;
				if (Objects.equals(line[0], "X") || Objects.equals(line[0], "x")) {
					offset = 0;
				}

				//skipping of incorrect data
				if (line[CP_WEB_CAT_POSITION - offset].length() == 0) {
					continue;
				}
				//id
				sb.append(line[CP_WEB_CAT_POSITION - offset]).append(",");
				//display name
				sb.append("\"").append(line[line[CP_WEB_CAT_POSITION - offset].length() * 2 - offset]).append("\",");

				//facets
				sb.append("\"");
				boolean hasFacets = false;
				for (int i = FIRST_FACET_POSITION; i <= LAST_FACET_POSITION; i++) {
					int position = i - offset;
					if (line.length >= position && !Objects.equals(line[position], "")) {
						sb.append(line[position]).append(",");
						hasFacets = true;
					}
				}
				if (hasFacets) {
					sb.deleteCharAt(sb.lastIndexOf(","));
				}
				sb.append("\"");
				sb.append(",,TRUE,,,,,,");

				sb.append(IMAGE_CONTEXT_PATH).append(line[IMAGE_POSITION - offset]);

				sb.append('\n');
				stringBuilders.add(sb);
				ids.add(line[CP_WEB_CAT_POSITION - offset]);
				//writer.write(String.valueOf(sb));
			}
			//sort
			for (int i = 0; i <= ids.size() - 2; i++) {
				for (int j = i + 1; j <= ids.size() - 1; j++) {
					if (ids.get(i).length() < ids.get(j).length()) {
						String tmpString = ids.get(i);
						ids.set(i, ids.get(j));
						ids.set(j, tmpString);
						StringBuilder tmpBuilder = stringBuilders.get(i);
						stringBuilders.set(i, stringBuilders.get(j));
						stringBuilders.set(j, tmpBuilder);
					}
				}
			}

			//inserting fixed child categories
			for (int i = ids.size() - 1; i >= 1; i--) {
				StringBuilder stringBuilder = new StringBuilder("\"");
				for (int j = i - 1; j > 0; j--) {
					if ((ids.get(j).indexOf(ids.get(i)) == 0) && (ids.get(j).length() - ids.get(i).length() == 1)) {
						stringBuilder.append(ids.get(j)).append(",");
					}
				}
				if (stringBuilder.lastIndexOf(",") != -1) {
					stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
				}
				stringBuilder.append("\"");
				stringBuilders.get(i).insert(stringBuilders.get(i).lastIndexOf("TRUE,") + 5, stringBuilder);
			}

			for (int i = 0; i < stringBuilders.size() - 1; i++) {
				writer.write(String.valueOf(stringBuilders.get(i)));
			}

		} catch (IOException e) {
			mLogging.logError(e);
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}
}
