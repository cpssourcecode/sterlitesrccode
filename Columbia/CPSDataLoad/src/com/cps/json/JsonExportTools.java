package com.cps.json;

import atg.nucleus.GenericService;
import vsg.load.file.IWriter;
import vsg.load.file.csv.CSVWriter;
import vsg.load.info.PathInfo;
import vsg.load.tools.IImportTools;

import java.io.File;
import java.text.SimpleDateFormat;

import static vsg.load.info.FileInfo.ACTION_DELETE;
import static vsg.load.info.FileInfo.ACTION_INSERT;
import static vsg.load.info.FileInfo.ACTION_UPDATE;


/**
 * @author Andy Porter
 */
public class JsonExportTools extends GenericService {


	/**
	 * path info of preprocessed files prepared for import by CatalogLoader
	 */
	private PathInfo mPathInfo;


	/**
	 * import tools
	 */
	private IImportTools mImportTools;


	/**
	 * AddWriter
	 */
	protected IWriter mAddWriter;

	/**
	 * UpdateWriter
	 */
	protected IWriter mUpdateWriter;

	/**
	 * DeleteWriter
	 */
	protected IWriter mDeleteWriter;


	/**
	 * getAddWriter
	 *
	 * @return error file writer
	 */
	public IWriter getAddWriter() {
		if (mAddWriter == null) {
			getImportTools().checkFileDir(mPathInfo.getInputDir());
			File addFile = new File(getPreProcessedFileName(ACTION_INSERT));
			CSVWriter csvWriter = (CSVWriter) getImportTools().getWriter(addFile);
//			csvWriter.setSeparator(TAB_CHARACTER);
			csvWriter.setSeparator(getImportTools().getFileSeparator());
			setAddWriter(csvWriter);
		}
		return mAddWriter;
	}


	/**
	 * Sets new AddWriter.
	 *
	 * @param pAddWriter New value of AddWriter.
	 */
	public void setAddWriter(IWriter pAddWriter) {
		mAddWriter = pAddWriter;
	}


	/**
	 * Gets DeleteWriter.
	 *
	 * @return Value of DeleteWriter.
	 */
	public IWriter getDeleteWriter() {
		if (mDeleteWriter == null) {
			getImportTools().checkFileDir(mPathInfo.getInputDir());
			File deleteFile = new File(getPreProcessedFileName(ACTION_DELETE));
			CSVWriter csvWriter = (CSVWriter) getImportTools().getWriter(deleteFile);
//			csvWriter.setSeparator(TAB_CHARACTER);
			csvWriter.setSeparator(getImportTools().getFileSeparator());
			setDeleteWriter(csvWriter);
		}
		return mDeleteWriter;
	}

	/**
	 * Sets new DeleteWriter.
	 *
	 * @param pDeleteWriter New value of DeleteWriter.
	 */
	public void setDeleteWriter(IWriter pDeleteWriter) {
		mDeleteWriter = pDeleteWriter;
	}


	/**
	 * Gets UpdateWriter.
	 *
	 * @return Value of UpdateWriter.
	 */
	public IWriter getUpdateWriter() {
		if (mUpdateWriter == null) {
			getImportTools().checkFileDir(mPathInfo.getInputDir());
			File updateFile = new File(getPreProcessedFileName(ACTION_UPDATE));
			CSVWriter csvWriter = (CSVWriter) getImportTools().getWriter(updateFile);
//			csvWriter.setSeparator(TAB_CHARACTER);
			csvWriter.setSeparator(getImportTools().getFileSeparator());
			setUpdateWriter(csvWriter);
		}
		return mUpdateWriter;
	}

	/**
	 * Sets new UpdateWriter.
	 *
	 * @param pUpdateWriter New value of UpdateWriter.
	 */
	public void setUpdateWriter(IWriter pUpdateWriter) {
		mUpdateWriter = pUpdateWriter;
	}


	/**
	 * return new preprocessed file name
	 *
	 * @param pAction@return new preprocessed file name
	 */
	public String getPreProcessedFileName(int pAction) {
		String postfix = "";

		switch (pAction) {
			case ACTION_INSERT:
				postfix = getPathInfo().getPostfixInsert();
				break;
			case ACTION_UPDATE:
				postfix = getPathInfo().getPostfixUpdate();
				break;
			case ACTION_DELETE:
				postfix = getPathInfo().getPostfixDelete();
				break;
		}

		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String date = format.format(new java.util.Date());

		return getPathInfo().getInputDir() + "/" + getPathInfo().getFilePrefix() + getPathInfo().getImportFileNameDivider()
				+ date + getPathInfo().getImportFileNameDivider() + postfix + getPathInfo().getFileExt();
	}


	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return path info
	 *
	 * @return path info
	 */
	public PathInfo getPathInfo() {
		return mPathInfo;
	}

	/**
	 * init path info
	 *
	 * @param pPathInfo path info
	 */
	public void setPathInfo(PathInfo pPathInfo) {
		mPathInfo = pPathInfo;
	}


	/**
	 * return import tools
	 *
	 * @return import tools
	 */
	public IImportTools getImportTools() {
		return mImportTools;
	}

	/**
	 * init import tools
	 *
	 * @param pImportTools import tools
	 */
	public void setImportTools(IImportTools pImportTools) {
		mImportTools = pImportTools;
	}


}
