package com.cps.json;

/**
 * @author Andy Porter
 */
public class JsonConstants {

	/**
	 * pre processed file type
	 */
//	public static final String PRE_PROCESSED_FILE_TYPE = ".txt";
	public static final String JSON_PATH_DELIMITER = ".";
	public static final String LIST_PROPERTY_DELIMITER = "|";

	public static final String ACTION_FIELD_ADD = "ADD";
	public static final String ACTION_FIELD_DELETE = "DELETE";
	public static final String ACTION_FIELD_UPDATE = "UPDATE";
	public static final String ACTION_FIELD_CHANGE = "Change";

	public static final String EMPTY = "";
	public static final String SPACE = " ";
	public static final String DISPLAY_NAME = "second_item_number";
	public static final String ITEM_NUMBER = "item_number";
}
