package com.cps.json;

import atg.nucleus.GenericService;

import java.util.List;

/**
 * @author Andy Porter
 */
public class JsonFieldsInfo extends GenericService {

	/**
	 * list of all fields from import file
	 */
	private List<String> mFields;

	/**
	 * ActionField
	 */
	private String mActionField;

	/**
	 * path under each item to array to map (item_aliases)
	 * Default=item_aliases
	 */
	private String mJsonArrayToMapPath;

	/**
	 * JsonArrayToMapKeyName:
	 * Default=CB_Number
	 */
	private String mJsonArrayToMapKeyName;

	/**
	 * mJsonArrayToMapValueName:
	 * Default=Item_Alias_Number
	 */
	private String mJsonArrayToMapValueName;

	/**
	 * path under each item to JSONObject under which pairs ATTRIBUTE_NAME{i}/ATTRIBUTE_VALUE{i}
	 * Default=unilog
	 */
	private String mUnilogAttributesToMapPath;

	/**
	 * full name pattern is ATTRIBUTE_NAME{i}
	 * Default=ATTRIBUTE_NAME
	 */
	private String mUnilogAttributesToMapKeyName;

	/**
	 * full name pattern is ATTRIBUTE_VALUE{i}
	 * Default=ATTRIBUTE_VALUE
	 */
	private String mUnilogAttributesToMapValueName;

	/**
	 * full path to items JSONArray from the document root
	 * Default=cps.item_master.items
	 */
	private String mMainJsonArrayPath;


	/**
	 * importItemAliases
	 */
	private boolean mImportItemAliases = true;

	/**
	 *
	 */
	private boolean mImportUnilogAttributes = true;


	private String mProductIdName;
	private String mCategoryIdName;

	// -----------------------------------------------------------------------------------------------------------------

	/**
	 * return list of fields
	 *
	 * @return list of fields
	 */
	public List<String> getFields() {
		return mFields;
	}

	/**
	 * init list of fields
	 *
	 * @param pFields list of fields
	 */
	public void setFields(List<String> pFields) {
		mFields = pFields;
	}


	/**
	 * Sets new ActionField.
	 *
	 * @param pActionField New value of ActionField.
	 */
	public void setActionField(String pActionField) {
		mActionField = pActionField;
	}

	/**
	 * Gets ActionField.
	 *
	 * @return Value of ActionField.
	 */

	public String getActionField() {
		return mActionField;
	}


	/**
	 * Gets JsonArrayToMapKeyName:
	 * Default=CB_Number.
	 *
	 * @return Value of JsonArrayToMapKeyName:
	 * Default=CB_Number.
	 */

	public String getUnilogAttributesToMapKeyName() {
		return mUnilogAttributesToMapKeyName;
	}

	/**
	 * Sets new JsonArrayToMapKeyName:
	 * Default=CB_Number.
	 *
	 * @param pUnilogAttributesToMapKeyName New value of JsonArrayToMapKeyName:
	 *                                      Default=CB_Number.
	 */
	public void setUnilogAttributesToMapKeyName(String pUnilogAttributesToMapKeyName) {
		mUnilogAttributesToMapKeyName = pUnilogAttributesToMapKeyName;
	}


	/**
	 * Sets new path under each item to array to map item_aliases
	 * Default=item_aliases.
	 *
	 * @param pJsonArrayToMapPath New value of path under each item to array to map item_aliases
	 *                            Default=item_aliases.
	 */
	public void setJsonArrayToMapPath(String pJsonArrayToMapPath) {
		mJsonArrayToMapPath = pJsonArrayToMapPath;
	}

	/**
	 * Gets path under each item to array to map item_aliases
	 * Default=item_aliases.
	 *
	 * @return Value of path under each item to array to map item_aliases
	 * Default=item_aliases.
	 */
	public String getJsonArrayToMapPath() {
		return mJsonArrayToMapPath;
	}


	/**
	 * Gets mJsonArrayToMapValueName:
	 * Default=Item_Alias_Number.
	 *
	 * @return Value of mJsonArrayToMapValueName:
	 * Default=Item_Alias_Number.
	 */
	public String getJsonArrayToMapValueName() {
		return mJsonArrayToMapValueName;
	}


	/**
	 * Sets new mJsonArrayToMapValueName:
	 * Default=Item_Alias_Number.
	 *
	 * @param pJsonArrayToMapValueName New value of mJsonArrayToMapValueName:
	 *                                 Default=Item_Alias_Number.
	 */
	public void setJsonArrayToMapValueName(String pJsonArrayToMapValueName) {
		mJsonArrayToMapValueName = pJsonArrayToMapValueName;
	}


	/**
	 * Sets new JsonArrayToMapKeyName:
	 * Default=CB_Number.
	 *
	 * @param pJsonArrayToMapKeyName New value of JsonArrayToMapKeyName:
	 *                               Default=CB_Number.
	 */
	public void setJsonArrayToMapKeyName(String pJsonArrayToMapKeyName) {
		mJsonArrayToMapKeyName = pJsonArrayToMapKeyName;
	}

	/**
	 * Gets JsonArrayToMapKeyName:
	 * Default=CB_Number.
	 *
	 * @return Value of JsonArrayToMapKeyName:
	 * Default=CB_Number.
	 */
	public String getJsonArrayToMapKeyName() {
		return mJsonArrayToMapKeyName;
	}


	/**
	 * Sets new path under each item to JSONObject under which pairs ATTRIBUTE_NAME{i}ATTRIBUTE_VALUE{i}
	 * Default=unilog.
	 *
	 * @param pUnilogAttributesToMapPath New value of path under each item to JSONObject under which pairs ATTRIBUTE_NAME{i}ATTRIBUTE_VALUE{i}
	 *                                   Default=unilog.
	 */
	public void setUnilogAttributesToMapPath(String pUnilogAttributesToMapPath) {
		mUnilogAttributesToMapPath = pUnilogAttributesToMapPath;
	}

	/**
	 * Gets path under each item to JSONObject under which pairs ATTRIBUTE_NAME{i}ATTRIBUTE_VALUE{i}
	 * Default=unilog.
	 *
	 * @return Value of path under each item to JSONObject under which pairs ATTRIBUTE_NAME{i}ATTRIBUTE_VALUE{i}
	 * Default=unilog.
	 */
	public String getUnilogAttributesToMapPath() {
		return mUnilogAttributesToMapPath;
	}


	/**
	 * Sets new full name pattern is ATTRIBUTE_VALUE{i}
	 * Default=ATTRIBUTE_VALUE.
	 *
	 * @param pUnilogAttributesToMapValueName New value of full name pattern is ATTRIBUTE_VALUE{i}
	 *                                        Default=ATTRIBUTE_VALUE.
	 */
	public void setUnilogAttributesToMapValueName(String pUnilogAttributesToMapValueName) {
		mUnilogAttributesToMapValueName = pUnilogAttributesToMapValueName;
	}

	/**
	 * Gets full name pattern is ATTRIBUTE_VALUE{i}
	 * Default=ATTRIBUTE_VALUE.
	 *
	 * @return Value of full name pattern is ATTRIBUTE_VALUE{i}
	 * Default=ATTRIBUTE_VALUE.
	 */
	public String getUnilogAttributesToMapValueName() {
		return mUnilogAttributesToMapValueName;
	}


	/**
	 * Gets full path to items JSONArray from the document root
	 * Default=cps.item_master.items.
	 *
	 * @return Value of full path to items JSONArray from the document root
	 * Default=cps.item_master.items.
	 */
	public String getMainJsonArrayPath() {
		return mMainJsonArrayPath;
	}

	/**
	 * Sets new full path to items JSONArray from the document root
	 * Default=cps.item_master.items.
	 *
	 * @param pMainJsonArrayPath New value of full path to items JSONArray from the document root
	 *                           Default=cps.item_master.items.
	 */
	public void setMainJsonArrayPath(String pMainJsonArrayPath) {
		mMainJsonArrayPath = pMainJsonArrayPath;
	}


	/**
	 * Sets new importItemAliases.
	 *
	 * @param pImportItemAliases New value of importItemAliases.
	 */
	public void setImportItemAliases(boolean pImportItemAliases) {
		mImportItemAliases = pImportItemAliases;
	}

	/**
	 * Gets importItemAliases.
	 *
	 * @return Value of importItemAliases.
	 */
	public boolean isImportItemAliases() {
		return mImportItemAliases;
	}


	/**
	 * Gets mImportUnilogAttributes.
	 *
	 * @return Value of mImportUnilogAttributes.
	 */
	public boolean isImportUnilogAttributes() {
		return mImportUnilogAttributes;
	}

	/**
	 * Sets new mImportUnilogAttributes.
	 *
	 * @param pImportUnilogAttributes New value of mImportUnilogAttributes.
	 */
	public void setImportUnilogAttributes(boolean pImportUnilogAttributes) {
		mImportUnilogAttributes = pImportUnilogAttributes;
	}

	/**
	 * Sets new mCategoryIdName.
	 *
	 * @param pCategoryIdName New value of mCategoryIdName.
	 */
	public void setCategoryIdName(String pCategoryIdName) {
		mCategoryIdName = pCategoryIdName;
	}

	/**
	 * Gets mCategoryIdName.
	 *
	 * @return Value of mCategoryIdName.
	 */
	public String getCategoryIdName() {
		return mCategoryIdName;
	}


	/**
	 * Gets mProductIdName.
	 *
	 * @return Value of mProductIdName.
	 */
	public String getProductIdName() {
		return mProductIdName;
	}

	/**
	 * Sets new mProductIdName.
	 *
	 * @param pProductIdName New value of mProductIdName.
	 */
	public void setProductIdName(String pProductIdName) {
		mProductIdName = pProductIdName;
	}


}
