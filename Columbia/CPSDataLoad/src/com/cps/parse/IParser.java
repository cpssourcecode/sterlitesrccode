package com.cps.parse;

import com.cps.json.JsonExportTools;
import com.cps.json.JsonFieldsInfo;
import vsg.load.tools.impl.ImportTools;

import java.util.List;

/**
 * IParser
 *
 * @author Andy Porter
 */
public interface IParser {

	List<String> performProcessing();

	JsonExportTools getJsonExportTools();

	void setJsonExportTools(JsonExportTools pJsonExportTools);

	JsonFieldsInfo getJsonFieldsInfo();

	void setJsonFieldsInfo(JsonFieldsInfo pJsonFieldsInfo);

	void setImportTools(ImportTools pImportTools);

	ImportTools getImportTools();

	boolean isFtpEnabled();


}
