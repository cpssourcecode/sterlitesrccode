package com.cps.parse;

import atg.nucleus.GenericService;
import com.cps.json.JsonExportTools;
import com.cps.json.JsonFieldsInfo;
import vsg.load.file.IWriter;
import vsg.load.tools.impl.ImportTools;

import java.util.*;

import static com.cps.json.JsonConstants.LIST_PROPERTY_DELIMITER;

/**
 * @author Andy Porter
 */
public class InitialParser extends GenericService implements IParser {
	/**
	 * AddWriter
	 */
	protected IWriter mAddWriter;
	/**
	 * UpdateWriter
	 */
	protected IWriter mUpdateWriter;
	/**
	 * DeleteWriter
	 */
	protected IWriter mDeleteWriter;
	/**
	 * JsonExportTools
	 */
	private JsonExportTools mJsonExportTools;
	/**
	 * JsonFieldsInfo
	 */
	private JsonFieldsInfo mJsonFieldsInfo;
	/**
	 * ImportTools
	 */
	private ImportTools mImportTools;

	private boolean mFtpEnabled;


	private boolean mAddWriterOn = true;
	private boolean mUpdateWriterOn = true;

	protected int mFileMaxLines = 5000;
	protected int mItemsProcessed = 0;


	/**
	 * performJsonProcessing
	 */
	@Override
	public synchronized List<String> performProcessing() {
		List<String> initialImportFiles = new ArrayList<String>();
		return initialImportFiles;
	}

	/**
	 * check if lock file exist
	 *
	 * @return lock file exist
	 */
	protected boolean checkLockFile() {
		return getImportTools().checkLockFileExist(getImportTools().getPathInfo().getInputDir());
	}

	/**
	 * release lock file
	 */
	protected void releaseLockFile() {
		getImportTools().deleteLockFile(getImportTools().getPathInfo().getInputDir());
	}

	/**
	 * Gets JsonExportTools.
	 *
	 * @return Value of JsonExportTools.
	 */
	@Override
	public JsonExportTools getJsonExportTools() {
		return mJsonExportTools;
	}

	/**
	 * Sets new JsonExportTools.
	 *
	 * @param pJsonExportTools New value of JsonExportTools.
	 */
	@Override
	public void setJsonExportTools(JsonExportTools pJsonExportTools) {
		mJsonExportTools = pJsonExportTools;
	}

	/**
	 * Gets JsonFieldsInfo.
	 *
	 * @return Value of JsonFieldsInfo.
	 */
	@Override
	public JsonFieldsInfo getJsonFieldsInfo() {
		return mJsonFieldsInfo;
	}

	/**
	 * Sets new JsonFieldsInfo.
	 *
	 * @param pJsonFieldsInfo New value of JsonFieldsInfo.
	 */
	@Override
	public void setJsonFieldsInfo(JsonFieldsInfo pJsonFieldsInfo) {
		mJsonFieldsInfo = pJsonFieldsInfo;
	}

	/**
	 * Sets new ImportTools.
	 *
	 * @param pImportTools New value of ImportTools.
	 */
	@Override
	public void setImportTools(ImportTools pImportTools) {
		mImportTools = pImportTools;
	}

	/**
	 * Gets ImportTools.
	 *
	 * @return Value of ImportTools.
	 */
	@Override
	public ImportTools getImportTools() {
		return mImportTools;
	}


	/**
	 * Gets mAddWriterOn.
	 *
	 * @return Value of mAddWriterOn.
	 */
	public boolean isAddWriterOn() {
		return mAddWriterOn;
	}

	/**
	 * Sets new mAddWriterOn.
	 *
	 * @param pAddWriterOn New value of mAddWriterOn.
	 */
	public void setAddWriterOn(boolean pAddWriterOn) {
		mAddWriterOn = pAddWriterOn;
	}


	/**
	 * Gets mUpdateWriterOn.
	 *
	 * @return Value of mUpdateWriterOn.
	 */
	public boolean isUpdateWriterOn() {
		return mUpdateWriterOn;
	}

	/**
	 * Sets new mUpdateWriterOn.
	 *
	 * @param pUpdateWriterOn New value of mUpdateWriterOn.
	 */
	public void setUpdateWriterOn(boolean pUpdateWriterOn) {
		mUpdateWriterOn = pUpdateWriterOn;
	}


	protected List<List<String>> getCategoriesChildrenListFromMap(Map<String, List<String>> pCategoryChildProductsMap) {
		List<List<String>> categoriesChildProductsList = new ArrayList<List<String>>();

		Set<String> categoriesSet = pCategoryChildProductsMap.keySet();
		for (Iterator<String> iterator = categoriesSet.iterator(); iterator.hasNext(); ) {
			String categoryId = iterator.next();
			List<String> categoryChildProductsList = pCategoryChildProductsMap.get(categoryId);
			StringBuilder fixedChildProductsSB = new StringBuilder();
			if (null != categoryChildProductsList) {
				int index = 0;
				for (String productId : categoryChildProductsList) {
					if (index > 0) {
						fixedChildProductsSB.append(LIST_PROPERTY_DELIMITER);
					}
					fixedChildProductsSB.append(productId);
					index++;
				}

			}
			String fixedChildProducts = fixedChildProductsSB.toString();
			vlogDebug("category=" + categoryId + ", fixedChilds=" + fixedChildProducts);
			List<String> processedFields = new ArrayList<String>();
			processedFields.add(categoryId);
			processedFields.add(fixedChildProducts);
			categoriesChildProductsList.add(processedFields);
		}

		return categoriesChildProductsList;
	}

	/**
	 * Gets mFileMaxLines.
	 *
	 * @return Value of mFileMaxLines.
	 */
	public int getFileMaxLines() {
		return mFileMaxLines;
	}

	/**
	 * Sets new pFileMaxLines.
	 *
	 * @param pFileMaxLines New value of mFileMaxLines.
	 */
	public void setFileMaxLines(int pFileMaxLines) {
		mFileMaxLines = pFileMaxLines;
	}

	@Override
	public boolean isFtpEnabled() {
		return mFtpEnabled;
	}

	public void setFtpEnabled(boolean pFtpEnabled) {
		mFtpEnabled = pFtpEnabled;
	}
}
