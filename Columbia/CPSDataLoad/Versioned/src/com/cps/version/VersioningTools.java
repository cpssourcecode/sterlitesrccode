package com.cps.version;

import java.util.Map;

import javax.transaction.TransactionManager;

import com.cps.dataload.exception.FeedProcessException;

import atg.deployment.common.Constants;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.epub.project.Process;
import atg.epub.project.ProcessHome;
import atg.epub.project.Project;
import atg.epub.project.ProjectConstants;
import atg.nucleus.GenericService;
import atg.process.action.ActionConstants;
import atg.repository.RepositoryItem;
import atg.security.Persona;
import atg.security.ThreadSecurityManager;
import atg.security.User;
import atg.userdirectory.UserDirectoryUserAuthority;
import atg.versionmanager.VersionManager;
import atg.versionmanager.WorkingContext;
import atg.versionmanager.Workspace;
import atg.workflow.WorkflowConstants;
import atg.workflow.WorkflowManager;
import atg.workflow.WorkflowView;

/**
 * Utility class to manage BCC projects
 *
 * @author Srilakshmi
 *
 */
@SuppressWarnings("PMD")
public class VersioningTools extends GenericService {

    /* Member Variables */

    private TransactionManager transactionManager;
    private WorkflowManager workflowManager;
    private String taskOutcomeId;
    private String workflowName;
    private String activityId;
    private VersionManager versionManager;
    private UserDirectoryUserAuthority userAuthority;
    private String personaPrefix;
    private String userName;

    /* Member Methods */

    public TransactionManager getTransactionManager() {
        return transactionManager;
    }

    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    public WorkflowManager getWorkflowManager() {
        return workflowManager;
    }

    public void setWorkflowManager(WorkflowManager workflowManager) {
        this.workflowManager = workflowManager;
    }

    public String getTaskOutcomeId() {
        return taskOutcomeId;
    }

    public void setTaskOutcomeId(String taskOutcomeId) {
        this.taskOutcomeId = taskOutcomeId;
    }

    public String getWorkflowName() {
        return workflowName;
    }

    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public VersionManager getVersionManager() {
        return versionManager;
    }

    public void setVersionManager(VersionManager versionManager) {
        this.versionManager = versionManager;
    }

    public UserDirectoryUserAuthority getUserAuthority() {
        return userAuthority;
    }

    public void setUserAuthority(UserDirectoryUserAuthority userAuthority) {
        this.userAuthority = userAuthority;
    }

    public String getPersonaPrefix() {
        return personaPrefix;
    }

    public void setPersonaPrefix(String personaPrefix) {
        this.personaPrefix = personaPrefix;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    /* Service Methods */

    /**
     *
     * @param projectName
     * @return
     * @throws Exception
     */
    public Process createProject(String projectName) throws FeedProcessException {
        Process process = null;
        TransactionDemarcation td = null;
        boolean rollback = true;
        try {
            td = createTransactionDemarcation();
            td.begin(getTransactionManager());
            assumeUserIdentity();

            ProcessHome processHome = getProcessHome();

            if (getActivityId() != null) {
                process = processHome.createProcessForImport(projectName, getWorkflowName(), getActivityId());
            } else {
                process = processHome.createProcessForImport(projectName, getWorkflowName());
            }

            String workspaceName = process.getProject().getWorkspace();
            Workspace workspace = getVersionManager().getWorkspaceByName(workspaceName);
            pushDevelopmentLine(workspace);
            rollback = false;
        } catch (Exception e) {
            throw new FeedProcessException(e);
        } finally {
            try {
                td.end(rollback);
            } catch (TransactionDemarcationException tde) {
                throw new FeedProcessException(tde);
            }
        }
        return process;

    }

    /**
     *
     * @return
     */
    protected boolean assumeUserIdentity() {
        if (getUserAuthority() == null) {
            return false;
        }

        User newUser = new User();
        Persona persona = getUserAuthority().getPersona(getPersonaPrefix() + getUserName());
        if (persona == null) {
            return false;
        }

        // create a temporary User object for the identity
        newUser.addPersona(persona);

        // replace the current User object
        ThreadSecurityManager.setThreadUser(newUser);

        return true;
    }

    /**
     * Advances the workflow of the incoming process, based on the preconfigured task outcome
     *
     * @param pProcess,
     *            process whose workflow needs to be advanced
     * @throws Exception
     */
    protected void advanceWorkflow(Process pProcess) throws FeedProcessException {
        RepositoryItem processWorkflow = pProcess.getProject().getWorkflow();
        String workflowProcessName = processWorkflow.getPropertyValue("processName").toString();
        String subjectId = pProcess.getId();

        try {
            // an alternative would be to use the global workflow view at
            WorkflowView wv = getWorkflowManager().getWorkflowView(getCurrentUser());

            wv.fireTaskOutcome(workflowProcessName, WorkflowConstants.DEFAULT_WORKFLOW_SEGMENT, subjectId, getTaskOutcomeId(),
                            ActionConstants.ERROR_RESPONSE_DEFAULT);

        } catch (Exception e) {
            throw new FeedProcessException(e);
        }
    }

    /**
     * Advances the workflow to approve and deploy the project
     *
     * @param process,
     *            process to be approved and deployed
     * @throws Exception
     */
    public void approveAndDeployProject(Process process) throws FeedProcessException {
        TransactionDemarcation td = createTransactionDemarcation();
        boolean rollback = true;
        try {
            td.begin(getTransactionManager());
            advanceWorkflow(process);
            rollback = false;
        } catch (TransactionDemarcationException e) {
            throw new FeedProcessException(e);
        } finally {
            releaseUserIdentity();
            try {
                td.end(rollback);
            } catch (TransactionDemarcationException tde) {
                throw new FeedProcessException(tde);
            }
        }
    }

    /**
     * Add the given development line to the stack of lines, and make it current in this thread.
     *
     * @see WorkingContext
     * @param pWorkspace
     *            -
     */
    public void pushDevelopmentLine(Workspace pWorkspace) {
        WorkingContext.pushDevelopmentLine(pWorkspace);
    }

    /**
     * Remove the current line from the stack, make the previous line current, and return the line that was removed.
     *
     * @see WorkingContext
     */
    public void popDevelopmentLine() {
        WorkingContext.popDevelopmentLine();
    }

    /**
     *
     * @return
     */
    public User getCurrentUser() {
        return ThreadSecurityManager.currentUser();
    }

    /**
     *
     */
    protected void releaseUserIdentity() {
        ThreadSecurityManager.setThreadUser(null);
    }

    /**
     *
     * @return
     */
    public ProcessHome getProcessHome() {
        return ProjectConstants.getPersistentHomes().getProcessHome();
    }

    /**
     *
     * @return
     */
    protected TransactionDemarcation createTransactionDemarcation() {
        return new TransactionDemarcation();
    }

    /**
     * Check if the project has already been deployed to target
     *
     * @param project
     *            - Project to be checked for deployment completeness
     * @return true if project is deployed
     */
    public boolean isProjectDeployed(Project project) {
        boolean isProjectDeployed = true;
        Map deploymentStatuses = project.getDeploymentStatuses();
        if (deploymentStatuses != null && deploymentStatuses.values() != null) {
            for (Object status : deploymentStatuses.values()) {
                Integer statusVal = (Integer) status;
                if (statusVal < Constants.DEPLOYED_TO_TARGET) {
                    isProjectDeployed = false;
                }
            }
        }
        return isProjectDeployed;
    }
}
