package com.cps.json;

import static vsg.constants.VSGConstants.MAP_KEY_VALUE_DELIMITER;
import static vsg.constants.VSGConstants.MAP_PAIR_DELIMITER;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import atg.core.util.StringUtils;
import com.cps.parse.InitialParser;
import org.json.JSONException;
import org.apache.commons.io.IOUtils;

import org.json.JSONObject;
import org.json.JSONArray;
import vsg.load.file.IWriter;
import vsg.util.json.InputStreamStripDecorator;
import vsg.util.json.JsonStreaminHelper;


/**
 * @author Andy Porter
 */
public class JsonParser extends InitialParser {

	boolean mEnabled = true;
	private boolean mWorkspaceParser = false;


	/*
{
  "cps": {
    "item_master": {
      "total": 16869,
      "items": [
        "catalog": { },
        "trade_svc_imageonly": {},
        "trade_svc_pricesvc":{},
        "item_aliases": [],
        "unilog": {}
      ]
    }
  }
}
*/

	/**
	 * performJsonProcessing
	 */
	@Override
	public synchronized List<String> performProcessing() {
		List<String> initialImportFiles = new ArrayList<String>();


		if (!isEnabled()) {
			vlogDebug("JsonParser disabled.");
			return initialImportFiles;
		}

		File[] files = getImportTools().getFilesList();
		if (checkLockFile() && null != files && files.length > 0) {
			vlogDebug("*******************************");
			vlogDebug("performJsonProcessing: " + getName());
			vlogDebug("*******************************");

			for (File file : files) {
				getJsonExportTools().setAddWriter(null);
				getJsonExportTools().setUpdateWriter(null);
				getJsonExportTools().setDeleteWriter(null);

				FileInputStream fileInputStream = null;
				try {
					fileInputStream = new FileInputStream(file);
				} catch (FileNotFoundException fnfe) {
					vlogError("performJsonProcessing():" + file.getAbsolutePath() + " does not exist.", fnfe);
				}

				if (null != fileInputStream) {
					try {
						load(fileInputStream);
					} catch (IOException ioe) {
						vlogError("performJsonProcessing():" + file.getAbsolutePath() + " loading.", ioe);
					} catch (JSONException jse){
						vlogError("performJsonProcessing():" + file.getAbsolutePath() + " loading.", jse);
					}

					try {
						fileInputStream.close();
					} catch (IOException ioe) {
						vlogError("performJsonProcessing():" + file.getAbsolutePath() + " trying to close.", ioe);
					}
					getImportTools().archiveFile(file);
				}

				if (null != mAddWriter) {
					getJsonExportTools().getAddWriter().close();
					mAddWriter = null;
				}
				if (null != mUpdateWriter) {
					getJsonExportTools().getUpdateWriter().close();
					mUpdateWriter = null;
				}
				if (null != mDeleteWriter) {
					getJsonExportTools().getDeleteWriter().close();
					mDeleteWriter = null;
				}

				initialImportFiles.add(file.getName());
			}
			releaseLockFile();

		}
		return initialImportFiles;
	}


	/**
	 * load
	 *
	 * @param pFile FileInputStream
	 * @return boolean
	 */
	protected boolean load(FileInputStream pFile) throws IOException, JSONException {
		boolean result = false;

		InputStreamStripDecorator dec = new InputStreamStripDecorator(pFile);
		JsonStreaminHelper.gotoToken(dec, "\"items\": [");
		String jsonStr = null;
		JSONObject jsonObject = null;
		int i = 0;
		while ((jsonStr = JsonStreaminHelper.nextItem(dec)) != null) {
			jsonObject = new JSONObject(jsonStr);
			processItem(jsonObject);

			if (isLoggingDebug()) {
				logDebug(new StringBuilder("CATALOG item ").append(i).append(":").append(jsonStr).toString());
			}
			i++;
			if (i > 0) {
				result = true;
			}
		}

		return result;
	}


	protected void processItem(JSONObject pItem) throws JSONException {
		List<String> processedFields = new ArrayList<String>();
		List<String> fieldNames = getJsonFieldsInfo().getFields();
		for (String fieldPath : fieldNames) {
			String fieldValue = getStringValueByPath(pItem, fieldPath);
			if (null == fieldValue) {
				fieldValue = "";
			}
			processedFields.add(fieldValue);
		}

		if (getJsonFieldsInfo().isImportItemAliases()) {
			String itemAliases = getArrayToMap(pItem);
			processedFields.add(itemAliases);
		}

		if (getJsonFieldsInfo().isImportUnilogAttributes()) {
			String attributes = getAttributesToMap(pItem);
			processedFields.add(attributes);
		}

		//check the type of row and add to the proper writer
		String actionFieldValue = getStringValueByPath(pItem, getJsonFieldsInfo().getActionField());
		if (null == actionFieldValue || isWorkspaceParser()) {
			actionFieldValue = JsonConstants.ACTION_FIELD_ADD;
		}
		IWriter iWriter = null;
		if (JsonConstants.ACTION_FIELD_ADD.equals(actionFieldValue)) {
			mAddWriter = getJsonExportTools().getAddWriter();
			iWriter = mAddWriter;
		} else if (JsonConstants.ACTION_FIELD_UPDATE.equals(actionFieldValue) || JsonConstants.ACTION_FIELD_CHANGE.equals(actionFieldValue)) {
			mUpdateWriter = getJsonExportTools().getUpdateWriter();
			iWriter = mUpdateWriter;
		} else if (JsonConstants.ACTION_FIELD_DELETE.equals(actionFieldValue)) {
			mDeleteWriter = getJsonExportTools().getDeleteWriter();
			iWriter = mDeleteWriter;
		} else {      // ADD is default value
			mAddWriter = getJsonExportTools().getAddWriter();
			iWriter = mAddWriter;
		}
		if (null != iWriter) {
			String[] processedFieldsArr = new String[processedFields.size()];
			processedFieldsArr = processedFields.toArray(processedFieldsArr);
			iWriter.writeNext(processedFieldsArr);

			mItemsProcessed++;
			if (mItemsProcessed >= mFileMaxLines) {
				if (JsonConstants.ACTION_FIELD_ADD.equals(actionFieldValue)) {
					mAddWriter = getJsonExportTools().getAddWriter();
					getJsonExportTools().getAddWriter().close();
					getJsonExportTools().setAddWriter(null);
					mAddWriter = null;

				} else if (JsonConstants.ACTION_FIELD_UPDATE.equals(actionFieldValue) || JsonConstants.ACTION_FIELD_CHANGE.equals(actionFieldValue)) {
					mUpdateWriter = getJsonExportTools().getUpdateWriter();
					getJsonExportTools().getUpdateWriter().close();
					getJsonExportTools().setUpdateWriter(null);
					mUpdateWriter = null;

				} else if (JsonConstants.ACTION_FIELD_DELETE.equals(actionFieldValue)) {
					mDeleteWriter = getJsonExportTools().getDeleteWriter();
					getJsonExportTools().getDeleteWriter().close();
					getJsonExportTools().setDeleteWriter(null);
					mDeleteWriter = null;
				} else {      // ADD is default value
					mAddWriter = getJsonExportTools().getAddWriter();
					getJsonExportTools().getAddWriter().close();
					getJsonExportTools().setAddWriter(null);
					mAddWriter = null;

				}
				mItemsProcessed = 0;
			}
		}
	}


	/**
	 * getArrayToMap
	 *
	 * @param pParentJSONObject item
	 * @return string presentation of Map item_aliases: CB_Number -> Item_Alias_Number
	 */
	protected String getArrayToMap(JSONObject pParentJSONObject) throws JSONException {
		String value = null;
		StringBuilder sbValue = new StringBuilder();

		Map<String, String> aliasMap = new HashMap<String, String>();

		JSONArray item_aliases = getJsonArrayByPath(pParentJSONObject, getJsonFieldsInfo().getJsonArrayToMapPath());
		if (null != item_aliases) {
			int aliasesArraySize = item_aliases.length();
			for (int i = 0; i < aliasesArraySize; i++) {
				JSONObject alias = (JSONObject) item_aliases.get(i);
				String keyValue = alias.getString(getJsonFieldsInfo().getJsonArrayToMapKeyName());
				String valueValue = alias.getString(getJsonFieldsInfo().getJsonArrayToMapValueName()).trim();

				if (!StringUtils.isBlank(keyValue) && !StringUtils.isBlank(valueValue)) {
					String aliasValue = aliasMap.get(keyValue);
					if (null == aliasValue) {

						aliasMap.put(keyValue, valueValue);
					} else {
						aliasValue = aliasValue + "," + valueValue;
						aliasMap.put(keyValue, aliasValue);
					}
				}
			}

			Set<String> aliasKeySet = aliasMap.keySet();
			int i = 0;
			for (String aliasKey : aliasKeySet) {
				if (i > 0) {
					sbValue.append(MAP_PAIR_DELIMITER);
				}
				sbValue.append(aliasKey).append(MAP_KEY_VALUE_DELIMITER).append(aliasMap.get(aliasKey));
				i++;
			}
		}
		value = sbValue.toString();
		vlogDebug("getArrayToMap():" + value);
		return value;
	}

	protected String getArrayToMapOld(JSONObject pParentJSONObject) throws JSONException {
		String value = null;
		StringBuilder sbValue = new StringBuilder();

		JSONArray item_aliases = getJsonArrayByPath(pParentJSONObject, getJsonFieldsInfo().getJsonArrayToMapPath());
		if (null != item_aliases) {
			int aliasesArraySize = item_aliases.length();
			for (int i = 0; i < aliasesArraySize; i++) {
				if (i > 0) {
					sbValue.append(MAP_PAIR_DELIMITER);
				}
				JSONObject alias = (JSONObject) item_aliases.get(i);
				String keyValue = alias.getString(getJsonFieldsInfo().getJsonArrayToMapKeyName());
				String valueValue = alias.getString(getJsonFieldsInfo().getJsonArrayToMapValueName()).trim();
				if (!StringUtils.isBlank(keyValue) && !StringUtils.isBlank(valueValue)) {
					sbValue.append(keyValue).append(MAP_KEY_VALUE_DELIMITER).append(valueValue);
				}
			}
		}
		value = sbValue.toString();
		vlogDebug("getArrayToMap():" + value);
		return value;
	}


	/**
	 * @param pParentJSONObject item
	 * @return string presentation of Map unilog: ATTRIBUTE_NAME{i} -> ATTRIBUTE_VALUE{i}
	 */
	protected String getAttributesToMap(JSONObject pParentJSONObject) {
		String value = null;
		StringBuilder sbValue = new StringBuilder();

		JSONObject unilog = getJsonObjectByPath(pParentJSONObject, getJsonFieldsInfo().getUnilogAttributesToMapPath());
		if (null != unilog) {
			boolean hasNext = true;
			int index = 1;
			while (hasNext) {
				if (index > 1) {
					sbValue.append(MAP_PAIR_DELIMITER);
				}
				String indexStr = String.valueOf(index);
				String keyValue = null;
				String valueValue = null;
				try {
					keyValue = unilog.getString(getJsonFieldsInfo().getUnilogAttributesToMapKeyName() + indexStr);
					valueValue = unilog.getString(getJsonFieldsInfo().getUnilogAttributesToMapValueName() + indexStr);
				} catch (JSONException ex) {
					hasNext = false;
				}
				if (hasNext) {
					if (!StringUtils.isBlank(keyValue) && !StringUtils.isBlank(valueValue)) {
						sbValue.append(keyValue).append(MAP_KEY_VALUE_DELIMITER).append(valueValue);
					}
				}

				index++;
			}
		}

		value = sbValue.toString();
		vlogDebug("getAttributesToMap():" + value);
		return value;
	}

	/**
	 * createJsonObjectFromFile
	 *
	 * @param pFile FileInputStream
	 * @return main file's JSONObject
	 */
	protected JSONObject createJsonObjectFromFile(FileInputStream pFile) throws JSONException {
		JSONObject jsonObject = null;
		String jsonStr = null;
		try {
			jsonStr = IOUtils.toString(pFile);
		} catch (IOException e) {
			vlogError("createJsonObjectFromFile()", e);
		}

		if (null != jsonStr) {
			jsonObject = new JSONObject(jsonStr);
		}

		return jsonObject;
	}


	/**
	 * getJsonObjectByPath
	 *
	 * @param pParentJSONObject pParentJSONObject : parent  JSONObject from which path to object
	 * @param pPath             path to object  from  pParentJSONObject
	 * @return JSONObject
	 */
	protected JSONObject getJsonObjectByPath(JSONObject pParentJSONObject, String pPath) {
		JSONObject jsonObject = pParentJSONObject;
		StringTokenizer st = new StringTokenizer(pPath, JsonConstants.JSON_PATH_DELIMITER);
		try {
			while (st.hasMoreTokens()) {
				jsonObject = jsonObject.getJSONObject(st.nextToken());
			}
		} catch (JSONException ex) {
			vlogDebug("getJsonObjectByPath(). Element does not exit path=" + pPath);
			jsonObject = null;
		}
//		vlogDebug("Path=" + pPath + ". value=" + jsonObject);
		return jsonObject;
	}

	/**
	 * getJsonArrayByPath
	 *
	 * @param pParentJSONObject pParentJSONObject : parent  JSONObject from which path to object
	 * @param pPath             path to object  from  pParentJSONObject
	 * @return JSONArray
	 */
	protected JSONArray getJsonArrayByPath(JSONObject pParentJSONObject, String pPath) {
		JSONArray jsonArray = null;
		JSONObject jsonObject = pParentJSONObject;
		StringTokenizer st = new StringTokenizer(pPath, JsonConstants.JSON_PATH_DELIMITER);
		int levels = st.countTokens();
		int currentLevel = 1;
		try {
			while (st.hasMoreTokens()) {
				if (currentLevel < levels) {
					jsonObject = jsonObject.getJSONObject(st.nextToken());
				} else {
					jsonArray = jsonObject.getJSONArray(st.nextToken());
				}
				currentLevel++;
			}
		} catch (JSONException ex) {
			vlogDebug("getJsonArrayByPath(). Element does not exit path=" + pPath);
			jsonArray = null;
		}

		return jsonArray;
	}


	/**
	 * getStringValueByPath
	 *
	 * @param pParentJSONObject pParentJSONObject : parent  JSONObject from which path to object
	 * @param pPath             path to object  from  pParentJSONObject
	 * @return StringValue by path
	 */
	protected String getStringValueByPath(JSONObject pParentJSONObject, String pPath) {
		String value = null;
		JSONObject jsonObject = pParentJSONObject;
		StringTokenizer st = new StringTokenizer(pPath, JsonConstants.JSON_PATH_DELIMITER);
		int levels = st.countTokens();
		int currentLevel = 1;
		try {
			while (st.hasMoreTokens()) {
				if (currentLevel < levels) {
					jsonObject = jsonObject.getJSONObject(st.nextToken());
				} else {
					String nextToken = st.nextToken();
					Object oValue = jsonObject.get(nextToken);
					if (null != oValue) {
						value = String.valueOf(oValue);
						if ("DESCRIPTION".equals(nextToken) || "GROUP_DESC".equals(nextToken) || "DESCRIPTION_LINE_TWO".equals(nextToken)) {
							value = value + " ";
						}
					} else {
						value = "";
					}
//					value = jsonObject.getString(st.nextToken());
				}
				currentLevel++;
			}
		} catch (JSONException ex) {
			vlogDebug("getStringValueByPath(). Element does not exit path=" + pPath);
			value = null;
		}
		vlogDebug("Path=" + pPath + ". value=" + value);

		return value;
	}


	// -----------------------------------------------------------------------------------------------------------------


	/**
	 * Gets mEnabled.
	 *
	 * @return Value of mEnabled.
	 */
	public boolean isEnabled() {
		return mEnabled;
	}

	/**
	 * Sets new mEnabled.
	 *
	 * @param pEnabled New value of mEnabled.
	 */
	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}


	/**
	 * Sets new mWorkspaceParser.
	 *
	 * @param pWorkspaceParser New value of mWorkspaceParser.
	 */
	public void setWorkspaceParser(boolean pWorkspaceParser) {
		mWorkspaceParser = pWorkspaceParser;
	}

	/**
	 * Gets mWorkspaceParser.
	 *
	 * @return Value of mWorkspaceParser.
	 */
	public boolean isWorkspaceParser() {
		return mWorkspaceParser;
	}

}
