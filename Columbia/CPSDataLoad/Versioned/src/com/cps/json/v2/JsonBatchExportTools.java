package com.cps.json.v2;

import com.cps.json.JsonExportTools;
import com.cps.load.file.csv.CSVBatchWriter;
import com.cps.load.file.csv.ICSVFileNameGenerator;
import vsg.load.file.IWriter;

/**
 * 
 * @author VSG.
 *
 */
public class JsonBatchExportTools extends JsonExportTools {

	/** Count of linex in csv file */
	private int mBatchSize = 5000;
	/** Craete file name generator */
	private ICSVFileNameGenerator mAddFileNameGenerator;
	/** Update file name generator */
	private ICSVFileNameGenerator mUpdateFileNameGenerator;
	/** Delete file name generator */
	private ICSVFileNameGenerator mDeleteFileNameGenerator;

	/**
	 * getAddWriter
	 *
	 * @return error file writer
	 */
	public IWriter getAddWriter() {
		if (mAddWriter == null) {
			getImportTools().checkFileDir(getPathInfo().getInputDir());
			CSVBatchWriter batchWriter = new CSVBatchWriter(getBatchSize(), getImportTools().getLoggerTools(),
					getAddFileNameGenerator());
			batchWriter.setSeparator(getImportTools().getFileSeparator());
			mAddWriter = batchWriter;
		}
		return mAddWriter;
	}

	/**
	 * Gets DeleteWriter.
	 *
	 * @return Value of DeleteWriter.
	 */
	public IWriter getDeleteWriter() {
		if (mDeleteWriter == null) {
			getImportTools().checkFileDir(getPathInfo().getInputDir());
			CSVBatchWriter batchWriter = new CSVBatchWriter(getBatchSize(), getImportTools().getLoggerTools(),
					getDeleteFileNameGenerator());
			batchWriter.setSeparator(getImportTools().getFileSeparator());
			mDeleteWriter = batchWriter;
		}
		return mDeleteWriter;
	}

	/**
	 * Gets UpdateWriter.
	 *
	 * @return Value of UpdateWriter.
	 */
	public IWriter getUpdateWriter() {
		if (mUpdateWriter == null) {
			getImportTools().checkFileDir(getPathInfo().getInputDir());
			CSVBatchWriter batchWriter = new CSVBatchWriter(getBatchSize(), getImportTools().getLoggerTools(),
					getUpdateFileNameGenerator());
			batchWriter.setSeparator(getImportTools().getFileSeparator());
			mUpdateWriter = batchWriter;
		}
		return mUpdateWriter;
	}

	public int getBatchSize() {
		return mBatchSize;
	}

	public void setBatchSize(int pBatchSize) {
		mBatchSize = pBatchSize;
	}

	public ICSVFileNameGenerator getAddFileNameGenerator() {
		return mAddFileNameGenerator;
	}

	public void setAddFileNameGenerator(ICSVFileNameGenerator pAddFileNameGenerator) {
		mAddFileNameGenerator = pAddFileNameGenerator;
	}

	public ICSVFileNameGenerator getUpdateFileNameGenerator() {
		return mUpdateFileNameGenerator;
	}

	public void setUpdateFileNameGenerator(ICSVFileNameGenerator pUpdateFileNameGenerator) {
		mUpdateFileNameGenerator = pUpdateFileNameGenerator;
	}

	public ICSVFileNameGenerator getDeleteFileNameGenerator() {
		return mDeleteFileNameGenerator;
	}

	public void setDeleteFileNameGenerator(ICSVFileNameGenerator pDeleteFileNameGenerator) {
		mDeleteFileNameGenerator = pDeleteFileNameGenerator;
	}

}
