package com.cps.json.v2.impl;

import java.util.List;

import com.cps.json.v2.JsonItemConverter;
import com.cps.json.v2.exception.JsonParseException;
import org.json.JSONObject;

import vsg.load.file.IWriter;

/**
 * 
 * @author VSG
 *
 */
public class CommonUpdateJsonConverter extends JsonItemConverter {

	@Override
	public void convert(JSONObject pJsonObject, IWriter pAddWriter, IWriter pUpdateWriter, IWriter pDeleteWriter)
			throws JsonParseException {
		List<String> processedFields = convertToFieldsArray(pJsonObject);
		if (processedFields != null && !processedFields.isEmpty()) {
			String[] fieldsArray = processedFields.toArray(new String[processedFields.size()]);
			pUpdateWriter.writeNext(fieldsArray);
		} else {
			throw new JsonParseException(pJsonObject);
		}
	}

}
