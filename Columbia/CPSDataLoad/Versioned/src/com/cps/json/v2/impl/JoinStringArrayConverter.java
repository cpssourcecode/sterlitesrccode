package com.cps.json.v2.impl;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import atg.core.util.StringUtils;
import com.cps.json.JsonConstants;
import com.cps.json.v2.IJsonFieldConverter;
import com.cps.json.v2.JsonFieldConverter;

/**
 * 
 * @author VSG
 *
 */
public class JoinStringArrayConverter extends JsonFieldConverter implements IJsonFieldConverter {

	/** Separator */
	private String mSeparator;
	
	@Override
	public String convert(JSONObject pJsonObject, String pFieldPath) {
		Object value = getObjectValueByPath(pJsonObject, pFieldPath);
		if (value instanceof JSONArray) {
			StringBuilder sb = new StringBuilder();
			JSONArray array = (JSONArray) value;
			for (int i = 0; i < array.length(); i++) {
				Object obj = null;
				try {
					obj = array.get(i);
				} catch (JSONException e) {
					vlogError(e, "Error");
				}
				if (obj != null) {
					String trimmedValue = obj.toString().trim();
					if(StringUtils.isNotBlank(trimmedValue)) {
						sb.append(StringEscapeUtils.escapeCsv(trimmedValue)).append(getSeparator());
					}
				}
			}
			if(sb.length() > 0) {
				int len = getSeparator() != null ? getSeparator().length() : 0;
				sb.replace(sb.length() - len, sb.length(), JsonConstants.EMPTY);
			}
			return sb.toString();
		}
		return null;
	}

	public String getSeparator() {
		return mSeparator;
	}

	public void setSeparator(String pSeparator) {
		mSeparator = pSeparator;
	}
	
}
