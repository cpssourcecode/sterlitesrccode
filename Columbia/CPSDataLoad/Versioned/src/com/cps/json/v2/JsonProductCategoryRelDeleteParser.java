package com.cps.json.v2;

import java.util.*;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import org.json.JSONObject;

import atg.core.util.StringUtils;
import com.cps.json.JsonFieldsInfo;

/**
 * @author VSG
 */
public class JsonProductCategoryRelDeleteParser extends JsonProductCategoryParser {

	private final static String PRODUCT_ITEM_DESCRIPTOR = "product";
	private final static String PRODUCT_PARENT_CATEGORIES = "parentCategories";


	/**
	 * write category - products mapping to csv file
	 *
	 * @param pCategoryProducts
	 */
	protected void writeCategories(Map<String, List<String>> pCategoryProducts) {
		Map<String, List<String>> newCategoryProducts = new HashMap<>();

		for (Map.Entry<String, List<String>> entry : pCategoryProducts.entrySet()) {
			String categoryId = entry.getKey();
			RepositoryItem categoryItem = null;
			try {
				categoryItem = getProductCatalog().getItem(categoryId, ITEM_DESC_CATEGORY);
			} catch (RepositoryException re) {
				logError(re);
			}
			if ((null != categoryItem) && (!WEB_CATEGORY_CODE_EMPTY_CATEGORY.equalsIgnoreCase(categoryId))) {
				newCategoryProducts.put(categoryId, entry.getValue());
			} else {
				if (!newCategoryProducts.containsKey(WEB_CATEGORY_CODE_EMPTY_CATEGORY)) {
					newCategoryProducts.put(WEB_CATEGORY_CODE_EMPTY_CATEGORY, new ArrayList<String>());
				}
				newCategoryProducts.get(WEB_CATEGORY_CODE_EMPTY_CATEGORY).addAll(entry.getValue());
			}
		}

		for (Map.Entry<String, List<String>> entryN : newCategoryProducts.entrySet()) {
			String[] line = convertCategoryProductsToArray(entryN.getKey(), entryN.getValue());
			getJsonExportTools().getDeleteWriter().writeNext(line);
		}
	}


	/**
	 * add mapping category - product
	 * TODO reimplement to find parent categories by productId and then fill map Category->List_products
	 *
	 * @param pJsonObject       - {@link JSONObject}
	 * @param pCategoryProducts - {@link Map}
	 */
	protected void addProductToMapping(JSONObject pJsonObject, Map<String, List<String>> pCategoryProducts) {
		JsonFieldsInfo fieldsInfo = getJsonFieldsInfo();
		if (fieldsInfo instanceof JsonFieldsConverters) {
			JsonFieldsConverters fieldsConverters = (JsonFieldsConverters) fieldsInfo;
			String productIdField = fieldsConverters.getProductIdName();
			String categoryIdField = fieldsConverters.getCategoryIdName();
			IJsonFieldConverter productConverter = fieldsConverters.getFieldConverters().get(productIdField);
			IJsonFieldConverter categoryConverter = fieldsConverters.getFieldConverters().get(categoryIdField);
			if (productConverter != null && categoryConverter != null) {
				String productId = productConverter.convert(pJsonObject, productIdField);

				String categoryIdCorrect = categoryConverter.convert(pJsonObject, categoryIdField);

				RepositoryItem productItem = null;
				try {
					productItem = getProductCatalog().getItem(productId, PRODUCT_ITEM_DESCRIPTOR);
				} catch (RepositoryException re) {
					logError(re);
				}

				if (null != productItem) {
					Set parentCategoriesSet = (Set) productItem.getPropertyValue(PRODUCT_PARENT_CATEGORIES);

					if (null != parentCategoriesSet) {
						Set<RepositoryItem> parentCategories = (Set<RepositoryItem>) parentCategoriesSet;
						for (RepositoryItem category : parentCategories) {
							if (null != category) {
								String categoryId = category.getRepositoryId();
//								if (StringUtils.isNotBlank(categoryIdCorrect) && !categoryIdCorrect.equals(categoryId) ||
//										StringUtils.isBlank(categoryIdCorrect)) {
								if (StringUtils.isBlank(categoryIdCorrect) || !categoryIdCorrect.equals(categoryId)) {
									if (StringUtils.isNotBlank(productId) && StringUtils.isNotBlank(categoryId)) {
										if (!pCategoryProducts.containsKey(categoryId)) {
											pCategoryProducts.put(categoryId, new ArrayList<String>());
										}
										pCategoryProducts.get(categoryId).add(productId);
									} else if (StringUtils.isNotBlank(productId) && !StringUtils.isNotBlank(categoryId)) {
										if (!pCategoryProducts.containsKey(WEB_CATEGORY_CODE_EMPTY_CATEGORY)) {
											pCategoryProducts.put(WEB_CATEGORY_CODE_EMPTY_CATEGORY, new ArrayList<String>());
										}
										pCategoryProducts.get(WEB_CATEGORY_CODE_EMPTY_CATEGORY).add(productId);
									}
								}
							}
						}
					}

				}

			}
		}
	}

}
