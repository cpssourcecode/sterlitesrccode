package com.cps.json.v2;

import org.json.JSONObject;

/**
 * 
 * @author VSG
 * 
 */
public interface IJsonFieldConverter {
	
	/**
	 * converts json field to string representation for csv
	 * @param pJsonObject - json object for process
	 * @param pFieldPath - field path
	 * @return - String
	 */
	String convert(JSONObject pJsonObject, String pFieldPath);
}
