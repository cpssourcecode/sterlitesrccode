package com.cps.json.v2;

import org.json.JSONObject;

import com.cps.json.v2.exception.JsonParseException;
import vsg.load.file.IWriter;

/**
 * 
 * @author VSG
 *
 */
public interface IJsonItemConverter {

	/**
	 * convert json to csv
	 * @param pJsonObject - object for canvertation
	 * @param pAddWriter - output writer for add new item
	 * @param pUpdateWriter - output writer for update existing item
	 * @param pDeleteWriter - output writer for removing item
	 * @throws JsonParseException - JsonParseException
	 */
	void convert(JSONObject pJsonObject, IWriter pAddWriter, IWriter pUpdateWriter, IWriter pDeleteWriter)
			throws JsonParseException;
	
	/**
	 * return json fields to convert
	 * @return JsonFieldsInfo
	 */
	JsonFieldsConverters getJsonFieldsInfo();
}
