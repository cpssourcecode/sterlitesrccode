package com.cps.json.v2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.cps.json.JsonExportTools;
import com.cps.load.ftp.FtpFileDownloader;
import org.json.JSONException;
import org.json.JSONObject;

import atg.nucleus.GenericService;
import com.cps.json.JsonFieldsInfo;
import com.cps.json.v2.exception.JsonParseException;
import com.cps.parse.IParser;
import vsg.load.info.StatisticsService;
import vsg.load.tools.impl.ImportTools;
import vsg.util.json.InputStreamStripDecorator;
import vsg.util.json.JsonStreaminHelper;

/**
 * @author VSG
 */
public class JsonCommonParser extends GenericService implements IParser {

	/**
	 * The enabled flag
	 */
	private boolean mEnabled = true;
	/**
	 * The json item converter
	 */
	private IJsonItemConverter mConverter;
	/**
	 * The starting array point
	 */
	private String mArrayStartToken;
	/**
	 * Sgould archive files
	 */
	private boolean mArchiveFiles = false;
	/**
	 * Should release lock file
	 */
	private boolean mReleaseLockFile = false;
	/**
	 * JsonExportTools
	 */
	private JsonExportTools mJsonExportTools;
	/**
	 * JsonFieldsInfo
	 */
	private JsonFieldsInfo mJsonFieldsInfo;
	/**
	 * ImportTools
	 */
	private ImportTools mImportTools;

	private boolean mFtpEnabled;

	private FtpFileDownloader mFtpFileDownloader;

	@Override
	public synchronized List<String> performProcessing() {
		vlogDebug(new StringBuilder().append(getAbsoluteName()).append("performProcessing() - start").toString());
		if (isEnabled()) {

			// download files from FTP
			if (isFtpEnabled() && null != getFtpFileDownloader()) {
				getFtpFileDownloader().load((StatisticsService) null);
			}


			List<String> initialImportFiles = new ArrayList<String>();
			try {
				File[] files = getImportTools().getFilesList();
				if (checkLockFile() && null != files && files.length > 0) {
					vlogDebug("*******************************");
					vlogDebug("performJsonProcessing: " + getName());
					vlogDebug("*******************************");

					for (File file : files) {
						FileInputStream fileInputStream = null;
						try {
							fileInputStream = new FileInputStream(file);
						} catch (FileNotFoundException fnfe) {
							vlogError("performJsonProcessing():" + file.getAbsolutePath() + " does not exist.", fnfe);
						}

						if (null != fileInputStream) {
							try {
								convertFile(fileInputStream);
							} catch (IOException e) {
								vlogError("performJsonProcessing():" + file.getAbsolutePath() + " loading.", e);
							} catch (JSONException e) {
								vlogError("performJsonProcessing():" + file.getAbsolutePath() + " loading.", e);
							}

							try {
								fileInputStream.close();
							} catch (IOException ioe) {
								vlogError("performJsonProcessing():" + file.getAbsolutePath() + " trying to close.", ioe);
							}
							if (isArchiveFiles()) {
								getImportTools().archiveFile(file);
							}
						}
						initialImportFiles.add(file.getName());
					}
				}
			} finally {
				closeExportFiles();
				if (isReleaseLockFile()) {
					releaseLockFile();
				}
			}
			vlogDebug(new StringBuilder().append(getAbsoluteName()).append("performProcessing() - end. normal").toString());
			return initialImportFiles;
		} else {
			vlogDebug(new StringBuilder().append(getAbsoluteName()).append("performProcessing() - end. with emptyList()").toString());
			return Collections.emptyList();
		}

	}

	/**
	 * close exposrt files
	 */
	protected void closeExportFiles() {
		getJsonExportTools().getAddWriter().close();
		getJsonExportTools().getUpdateWriter().close();
		getJsonExportTools().getDeleteWriter().close();
	}

	/**
	 * converts json file to csv
	 *
	 * @param pFile - FileInputStream
	 * @throws IOException - IOException
	 */
	protected void convertFile(FileInputStream pFile) throws IOException, JSONException {
		InputStreamStripDecorator dec = new InputStreamStripDecorator(pFile);
		JsonStreaminHelper.gotoToken(dec, getArrayStartToken());
		String jsonStr = null;
		JSONObject jsonObject = null;
		int i = 0;
		while ((jsonStr = JsonStreaminHelper.nextItem(dec)) != null) {
			jsonObject = new JSONObject(jsonStr);
			try {
				getConverter().convert(jsonObject, getJsonExportTools().getAddWriter(),
						getJsonExportTools().getUpdateWriter(), getJsonExportTools().getDeleteWriter());
			} catch (JsonParseException e) {
				vlogError(e, "Error with item: {0}", jsonObject);
			}

			if (isLoggingDebug()) {
				logDebug(new StringBuilder("CATALOG item ").append(i).append(":").append(jsonStr).toString());
			}
			i++;
		}
	}

	/**
	 * check if lock file exist
	 *
	 * @return lock file exist
	 */
	protected boolean checkLockFile() {
		return getImportTools().checkLockFileExist(getImportTools().getPathInfo().getInputDir());
	}

	/**
	 * release lock file
	 */
	protected void releaseLockFile() {
		getImportTools().deleteLockFile(getImportTools().getPathInfo().getInputDir());
	}

	public boolean isEnabled() {
		return mEnabled;
	}

	public void setEnabled(boolean pEnabled) {
		mEnabled = pEnabled;
	}

	public IJsonItemConverter getConverter() {
		return mConverter;
	}

	public void setConverter(IJsonItemConverter pConverter) {
		mConverter = pConverter;
	}

	public String getArrayStartToken() {
		return mArrayStartToken;
	}

	public void setArrayStartToken(String pArrayStartToken) {
		mArrayStartToken = pArrayStartToken;
	}

	public boolean isArchiveFiles() {
		return mArchiveFiles;
	}

	public void setArchiveFiles(boolean pArchiveFiles) {
		mArchiveFiles = pArchiveFiles;
	}

	public boolean isReleaseLockFile() {
		return mReleaseLockFile;
	}

	public void setReleaseLockFile(boolean pReleaseLockFile) {
		mReleaseLockFile = pReleaseLockFile;
	}

	public JsonExportTools getJsonExportTools() {
		return mJsonExportTools;
	}

	public void setJsonExportTools(JsonExportTools pJsonExportTools) {
		mJsonExportTools = pJsonExportTools;
	}

	public JsonFieldsInfo getJsonFieldsInfo() {
		return mJsonFieldsInfo;
	}

	public void setJsonFieldsInfo(JsonFieldsInfo pJsonFieldsInfo) {
		mJsonFieldsInfo = pJsonFieldsInfo;
	}

	public ImportTools getImportTools() {
		return mImportTools;
	}

	public void setImportTools(ImportTools pImportTools) {
		mImportTools = pImportTools;
	}

	@Override
	public boolean isFtpEnabled() {
		return mFtpEnabled;
	}

	public void setFtpEnabled(boolean pFtpEnabled) {
		mFtpEnabled = pFtpEnabled;
	}

	public FtpFileDownloader getFtpFileDownloader() {
		return mFtpFileDownloader;
	}

	public void setFtpFileDownloader(FtpFileDownloader pFtpFileDownloader) {
		mFtpFileDownloader = pFtpFileDownloader;
	}
}
