package com.cps.json.v2;

import java.util.HashMap;
import java.util.Map;

import atg.nucleus.Nucleus;
import com.cps.json.JsonFieldsInfo;

/**
 * 
 * @author VSG
 *
 */
public class JsonFieldsConverters extends JsonFieldsInfo {

	/**
	 * map of field converters
	 */
	private Map<String, IJsonFieldConverter> mFieldConverters;
	/**
	 * default field converter
	 */
	private IJsonFieldConverter mDefaultFieldConverter;

	public Map<String, IJsonFieldConverter> getFieldConverters() {
		return mFieldConverters;
	}

	public void setFieldConverters(Map<String, String> pFieldConverters) {
		if (pFieldConverters == null) {
			mFieldConverters = null;
		} else {
			mFieldConverters = new HashMap<String, IJsonFieldConverter>();
			Nucleus nucleus = Nucleus.getGlobalNucleus();
			for (String fieldName : pFieldConverters.keySet()) {
				IJsonFieldConverter validator = (IJsonFieldConverter) nucleus
						.resolveName(pFieldConverters.get(fieldName));
				mFieldConverters.put(fieldName, validator);
			}
		}
	}

	public IJsonFieldConverter getDefaultFieldConverter() {
		return mDefaultFieldConverter;
	}

	public void setDefaultFieldConverter(IJsonFieldConverter pDefaultFieldConverter) {
		mDefaultFieldConverter = pDefaultFieldConverter;
	}

}