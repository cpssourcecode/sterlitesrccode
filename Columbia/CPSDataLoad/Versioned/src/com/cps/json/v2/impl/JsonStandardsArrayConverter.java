package com.cps.json.v2.impl;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import atg.core.util.StringUtils;
import com.cps.json.JsonConstants;

/**
 * 
 * @author VSG
 *
 */
public class JsonStandardsArrayConverter extends JoinStringArrayConverter {

	/** Split each string in array */
	private String mSplitStr;
	
	@Override
	public String convert(JSONObject pJsonObject, String pFieldPath) {
		Object value = getObjectValueByPath(pJsonObject, pFieldPath);
		if (value instanceof JSONArray) {
			StringBuilder sb = new StringBuilder();
			JSONArray array = (JSONArray) value;
			for (int i = 0; i < array.length(); i++) {
				Object obj = null;
				try {
					obj = array.get(i);
				} catch (JSONException e) {
					vlogError(e,"Error");
				}
				if (obj != null) {
					String val = obj.toString();
					String[] splitted = val.split(getSplitStr());
					if(splitted != null && splitted.length > 0) {
						for(String subValue: splitted) {
							String trimmedSubValue = subValue.toString().trim();
							if(StringUtils.isNotBlank(trimmedSubValue)) {
								sb.append(StringEscapeUtils.escapeCsv(trimmedSubValue)).append(getSeparator());
							}
						}
					}
				}
			}
			if(sb.length() > 0) {
				int len = getSeparator() != null ? getSeparator().length() : 0;
				sb.replace(sb.length() - len, sb.length(), JsonConstants.EMPTY);
			}
			return sb.toString();
		}
		return null;
	}
	
	public String getSplitStr() {
		return mSplitStr;
	}

	public void setSplitStr(String pSplitStr) {
		mSplitStr = pSplitStr;
	}
	
	
}
