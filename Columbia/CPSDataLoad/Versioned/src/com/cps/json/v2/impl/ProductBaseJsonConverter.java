package com.cps.json.v2.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cps.json.v2.JsonItemConverter;
import org.json.JSONObject;

import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import com.cps.json.JsonConstants;
import com.cps.json.v2.exception.JsonParseException;
import com.cps.util.CPSGlobalProperties;

import vsg.load.file.IWriter;

/**
 * 
 * @author VSG
 *
 */
public class ProductBaseJsonConverter extends JsonItemConverter {
	
	/** The string value converter */
	private CommonStringConverter mStringConverter;
	/** private id field */
	private String mIdField;
	/** The item descriptor to cheeck if exist */
	private String mItemDescriptor;
	/** The catalog repository */
	private Repository mCatalogRepository;
	private CPSGlobalProperties cpsGlobalProperties;
	
	@Override
	public void convert(JSONObject pJsonObject, IWriter pAddWriter, IWriter pUpdateWriter, IWriter pDeleteWriter)
			throws JsonParseException {
		List<String> processedFields = convertToFieldsArray(pJsonObject);
		vlogInfo("processedFields : {0}", processedFields);
		if (processedFields != null && !processedFields.isEmpty()) {
			String[] fieldsArray = processedFields.toArray(new String[processedFields.size()]);
			String action = getStringConverter().convert(pJsonObject, getJsonFieldsInfo().getActionField());
			String displayName = getStringConverter().convert(pJsonObject, JsonConstants.DISPLAY_NAME);
			String itemNumber = getStringConverter().convert(pJsonObject, JsonConstants.ITEM_NUMBER);
			
			if (StringUtils.isBlank(action)) {
				action = JsonConstants.ACTION_FIELD_ADD;
			}
			boolean isExits = isAlreadyExist(pJsonObject);
			if(StringUtils.isNotBlank(displayName)){
				switch (action) {
				case JsonConstants.ACTION_FIELD_ADD:
					if(isExits) {
						pUpdateWriter.writeNext(fieldsArray);
					} else {
						pAddWriter.writeNext(fieldsArray);
					}
					break;
				case JsonConstants.ACTION_FIELD_DELETE:
					pDeleteWriter.writeNext(fieldsArray);
					break;
				case JsonConstants.ACTION_FIELD_UPDATE:
					if(isExits) {
						pUpdateWriter.writeNext(fieldsArray);
					} else {
						pAddWriter.writeNext(fieldsArray);
					}
					break;
				case JsonConstants.ACTION_FIELD_CHANGE:
					if(isExits) {
						pUpdateWriter.writeNext(fieldsArray);
					} else {
						pAddWriter.writeNext(fieldsArray);
					}
					break;
				default:
					break;
				}
			}else{
				
				if(getCpsGlobalProperties()!=null){
					if(!getCpsGlobalProperties().getSkippedItems().contains(itemNumber)){
						getCpsGlobalProperties().getSkippedItems().add(itemNumber);
					}
				}
			}
		} else {
			throw new JsonParseException(pJsonObject);
		}
	}
	
	private boolean isAlreadyExist(JSONObject pJsonObject) {
		boolean result = false;
		try {
			String id = getStringConverter().convert(pJsonObject, getIdField());
			if (StringUtils.isNotBlank(id)) {
				RepositoryItem item = getCatalogRepository().getItem(id, getItemDescriptor());
				result = item != null;
			}
		} catch (RepositoryException e) {
			vlogError(e, "Error in ProductBaseJsonConverter.isAlreadyExist");
		}
		return result;
	}

	public CommonStringConverter getStringConverter() {
		return mStringConverter;
	}

	public void setStringConverter(CommonStringConverter pStringConverter) {
		mStringConverter = pStringConverter;
	}

	public String getItemDescriptor() {
		return mItemDescriptor;
	}

	public void setItemDescriptor(String pItemDescriptor) {
		mItemDescriptor = pItemDescriptor;
	}

	public Repository getCatalogRepository() {
		return mCatalogRepository;
	}

	public void setCatalogRepository(Repository pCatalogRepository) {
		mCatalogRepository = pCatalogRepository;
	}

	public String getIdField() {
		return mIdField;
	}

	public void setIdField(String pIdField) {
		mIdField = pIdField;
	}

	/**
	 * @return the cpsGlobalProperties
	 */
	public CPSGlobalProperties getCpsGlobalProperties() {
		return cpsGlobalProperties;
	}

	/**
	 * @param cpsGlobalProperties the cpsGlobalProperties to set
	 */
	public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
		this.cpsGlobalProperties = cpsGlobalProperties;
	}


}
