package com.cps.json.v2.impl;

import org.json.JSONObject;

import java.util.AbstractMap;
import java.util.Map;

/**
 * 
 * @author VSG.
 *
 */
public class AttributesArrayToMapConverter extends ArrayToMapConverter {

	public static final String ATTRIBUTE_NAME = "ATTRIBUTE_NAME";
	public static final String PROCESS_TWICE_POSTFIX = "_SECOND_TIME";


	private String mValueName;

    @Override
    public String convert(JSONObject pJsonObject, String pFieldPath) {
        if(pFieldPath.endsWith(PROCESS_TWICE_POSTFIX)){
            pFieldPath = pFieldPath.replace(PROCESS_TWICE_POSTFIX,"");
        }
        return super.convert(pJsonObject,pFieldPath);
    }


	/**
	 * convert to pair key - value for attributes map
	 * @param pAttribute - JSONObject
	 * @return Map.Entry<String, String>
	 */
	protected Map.Entry<String, String> createAttributeMapEntry(JSONObject pAttribute) {
		Map.Entry<String, String> entry = null;
		StringBuilder value = new StringBuilder();
		StringBuilder name = new StringBuilder();

		if (pAttribute != null) {
			Object valueObj = getObjectValueByPath(pAttribute, getValueName());
			Object nameObj = getObjectValueByPath(pAttribute, ATTRIBUTE_NAME);

			if (valueObj instanceof String || valueObj instanceof Number || valueObj instanceof Boolean) {
				String valueStr = String.valueOf(valueObj);
				value.append(valueStr.trim());
			}
			if (nameObj instanceof String) {
				String nameStr = (String) nameObj;
				name.append(nameStr.trim().toUpperCase());
			}

			if (value.length() > 0 && name.length() > 0) {
				entry = new AbstractMap.SimpleEntry<String, String>(name.toString(), value.toString());
			}
		}
		return entry;
	}


	public String getValueName() {
		return mValueName;
	}

	public void setValueName(String pValueName) {
		mValueName = pValueName;
	}
}
