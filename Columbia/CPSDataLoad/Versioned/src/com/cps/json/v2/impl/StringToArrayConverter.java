package com.cps.json.v2.impl;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONObject;

import com.cps.json.JsonConstants;

import atg.core.util.StringUtils;

/**
 *
 * @author VSG
 *
 */
public class StringToArrayConverter extends JoinStringArrayConverter {

    /** Split each string in array */
    private String mSplitStr;

    private String replaceSplitStr;

    @Override
    public String convert(JSONObject pJsonObject, String pFieldPath) {
        Object value = getObjectValueByPath(pJsonObject, pFieldPath);
        if (value instanceof String) {
            String val = (String) value;
            vlogDebug("fieldPath :: {0}, incoming value :: {1}", pFieldPath, val);
            StringBuilder sb = new StringBuilder();
            val = val.replaceAll(getReplaceSplitStr(), getSplitStr());
            vlogDebug("fieldPath :: {0}, incoming value after replace before split :: {1} ", pFieldPath, val);
            String[] splitted = val.split(getSplitStr());
            if (splitted != null && splitted.length > 0) {
                for (String subValue : splitted) {
                    String trimmedSubValue = subValue.toString().trim();
                    if (StringUtils.isNotBlank(trimmedSubValue)) {
                        sb.append(StringEscapeUtils.escapeCsv(trimmedSubValue)).append(getSeparator());
                    }
                }
            }
            if (sb.length() > 0) {
                int len = getSeparator() != null ? getSeparator().length() : 0;
                sb.replace(sb.length() - len, sb.length(), JsonConstants.EMPTY);
            }
            vlogDebug("fieldPath :: {0}, converted value :: {1} ", pFieldPath, sb.toString());
            return sb.toString();
        }
        return null;
    }

    public String getSplitStr() {
        return mSplitStr;
    }

    public void setSplitStr(String pSplitStr) {
        mSplitStr = pSplitStr;
    }

    public String getReplaceSplitStr() {
        return replaceSplitStr;
    }

    public void setReplaceSplitStr(String replaceSplitStr) {
        this.replaceSplitStr = replaceSplitStr;
    }

    public static String testMethod(String val) {
        System.out.println("incoming value = " + val);
        StringBuilder sb = new StringBuilder();
        val = val.replaceAll(";", " ; ");
        System.out.println("incoming value after replace before split = " + val);
        String[] splitted = val.split(" ; ");
        if (splitted != null && splitted.length > 0) {
            for (String subValue : splitted) {
                String trimmedSubValue = subValue.toString().trim();
                if (StringUtils.isNotBlank(trimmedSubValue)) {
                    sb.append(StringEscapeUtils.escapeCsv(trimmedSubValue)).append("|");
                }
            }
        }
        if (sb.length() > 0) {
            int len = "|" != null ? "|".length() : 0;
            sb.replace(sb.length() - len, sb.length(), JsonConstants.EMPTY);
        }
        System.out.println("converted value = " + sb.toString());
        return sb.toString();

    }

    public static void main(String[] args) {
        String one = "one;two;three";
        String two = "one ; two ; three";
        String three = "one ; two ; three";
        String four = "one; two;three ; four";
        String five = "Chicago Faucet;Chicago Faucet®;ChicagoFaucetreg;Chicago Faucet reg ;1100-GN2AE3-317VAB;1100GN2AE3317VAB;1100 GN2AE3 317VAB;PLBG_Item_9747;PLBGItem9747;PLBG Item 9747;USA;611943579401;Chicago Faucet® 1100-GN2AE3-317VAB Hot and Cold Water Sink Faucet, 2.2 gpm, 8 in Center, 2 Handles, Chrome Plated, Domestic, Commercial ; Utility Sink Faucets ; Utility Sink Faucets ; Utility Sink Faucets";
        System.out.println(testMethod(one));
        System.out.println(testMethod(two));
        System.out.println(testMethod(three));
        System.out.println(testMethod(four));
        System.out.println(testMethod(five));
    }

}
