package com.cps.json.v2.impl;

import com.cps.json.JsonConstants;
import com.cps.json.v2.IJsonFieldConverter;
import com.cps.json.v2.JsonFieldConverter;
import org.json.JSONObject;

/**
 * 
 * @author VSG
 * 
 */
public class CommonStringConverter extends JsonFieldConverter implements IJsonFieldConverter {

	@Override
	public String convert(JSONObject pJsonObject, String pFieldPath) {
		Object value = getObjectValueByPath(pJsonObject, pFieldPath);
		if(value != null) {
			return String.valueOf(value).trim();
		} else {
			return JsonConstants.EMPTY;
		}
	}
	
}
