package com.cps.json.v2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import org.json.JSONException;
import org.json.JSONObject;

import atg.core.util.StringUtils;
import com.cps.json.JsonConstants;
import com.cps.json.JsonFieldsInfo;
import vsg.util.json.InputStreamStripDecorator;
import vsg.util.json.JsonStreaminHelper;

/**
 * @author VSG
 */
public class JsonProductCategoryParser extends JsonCommonParser {


	protected final static String WEB_CATEGORY_CODE_EMPTY_CATEGORY = "ZZZ";
	protected final static String ITEM_DESC_CATEGORY = "category";

	private Repository productCatalog;


	@Override
	public synchronized List<String> performProcessing() {
		if (isEnabled()) {
			List<String> initialImportFiles = new ArrayList<String>();
			try {
				File[] files = getImportTools().getFilesList();
				if (checkLockFile() && null != files && files.length > 0) {
					vlogDebug("*******************************");
					vlogDebug("performJsonProcessing: " + getName());
					vlogDebug("*******************************");

					Map<String, List<String>> categoryProducts = new HashMap<>();
					for (File file : files) {
						FileInputStream fileInputStream = null;
						try {
							fileInputStream = new FileInputStream(file);
						} catch (FileNotFoundException fnfe) {
							vlogError("performJsonProcessing():" + file.getAbsolutePath() + " does not exist.", fnfe);
						}

						if (null != fileInputStream) {
							try {
								collectCategoryProducts(fileInputStream, categoryProducts);
							} catch (IOException ioe) {
								vlogError("performJsonProcessing():" + file.getAbsolutePath() + " loading.", ioe);
							} catch (JSONException e) {
								vlogError("performJsonProcessing():" + file.getAbsolutePath() + " loading.", e);
							}

							try {
								fileInputStream.close();
							} catch (IOException ioe) {
								vlogError("performJsonProcessing():" + file.getAbsolutePath() + " trying to close.", ioe);
							}
							if (isArchiveFiles()) {
								getImportTools().archiveFile(file);
							}
						}
						initialImportFiles.add(file.getName());
					}
					writeCategories(categoryProducts);


				}
			} finally {
				closeExportFiles();
				if (isReleaseLockFile()) {
					releaseLockFile();
				}
			}
			return initialImportFiles;
		} else {
			return Collections.emptyList();
		}
	}

	/**
	 * write category - products mapping to csv file
	 *
	 * @param pCategoryProducts
	 */
	protected void writeCategories(Map<String, List<String>> pCategoryProducts) {
		Map<String, List<String>> newCategoryProducts = new HashMap<>();

		for (Map.Entry<String, List<String>> entry : pCategoryProducts.entrySet()) {
			String categoryId = entry.getKey();
			RepositoryItem categoryItem = null;
			try {
				categoryItem = getProductCatalog().getItem(categoryId, ITEM_DESC_CATEGORY);
			} catch (RepositoryException re) {
				logError(re);
			}
			if ((null != categoryItem) && (!WEB_CATEGORY_CODE_EMPTY_CATEGORY.equalsIgnoreCase(categoryId))) {
				newCategoryProducts.put(categoryId, entry.getValue());
			} else {
				if (!newCategoryProducts.containsKey(WEB_CATEGORY_CODE_EMPTY_CATEGORY)) {
					newCategoryProducts.put(WEB_CATEGORY_CODE_EMPTY_CATEGORY, new ArrayList<String>());
				}
				newCategoryProducts.get(WEB_CATEGORY_CODE_EMPTY_CATEGORY).addAll(entry.getValue());
			}
		}

		for (Map.Entry<String, List<String>> entryN : newCategoryProducts.entrySet()) {
			String[] line = convertCategoryProductsToArray(entryN.getKey(), entryN.getValue());
			getJsonExportTools().getUpdateWriter().writeNext(line);
		}
	}

	/**
	 * return string array represantation
	 *
	 * @param pCategoryId - category id
	 * @param pProducts   - products
	 * @return String[]
	 */
	protected String[] convertCategoryProductsToArray(String pCategoryId, List<String> pProducts) {
		List<String> array = new ArrayList<>();
		array.add(pCategoryId);
		StringBuilder sb = new StringBuilder();
		for (String productId : pProducts) {
			sb.append(productId).append(JsonConstants.LIST_PROPERTY_DELIMITER);
		}
		if (sb.length() > 0) {
			sb.replace(sb.length() - 1, sb.length(), JsonConstants.EMPTY);
		}
		array.add(sb.toString());
		return array.toArray(new String[array.size()]);
	}

	/**
	 * collects json file to map
	 *
	 * @param pFile - FileInputStream
	 * @param
	 * @throws IOException - IOException
	 */
	protected void collectCategoryProducts(FileInputStream pFile, Map<String, List<String>> pCategoryProducts) throws IOException, JSONException {
		InputStreamStripDecorator dec = new InputStreamStripDecorator(pFile);
		JsonStreaminHelper.gotoToken(dec, getArrayStartToken());
		String jsonStr = null;
		JSONObject jsonObject = null;
		int i = 0;
		while ((jsonStr = JsonStreaminHelper.nextItem(dec)) != null) {
			jsonObject = new JSONObject(jsonStr);
			addProductToMapping(jsonObject, pCategoryProducts);
			if (isLoggingDebug()) {
				logDebug(new StringBuilder("CATALOG item ").append(i).append(":").append(jsonStr).toString());
			}
		}
	}

	/**
	 * add mapping category - product
	 *
	 * @param pJsonObject       - {@link JSONObject}
	 * @param pCategoryProducts - {@link Map}
	 */
	protected void addProductToMapping(JSONObject pJsonObject, Map<String, List<String>> pCategoryProducts) {
		JsonFieldsInfo fieldsInfo = getJsonFieldsInfo();
		if (fieldsInfo instanceof JsonFieldsConverters) {
			JsonFieldsConverters fieldsConverters = (JsonFieldsConverters) fieldsInfo;
			String productIdField = fieldsConverters.getProductIdName();
			String categoryIdField = fieldsConverters.getCategoryIdName();
			IJsonFieldConverter productConverter = fieldsConverters.getFieldConverters().get(productIdField);
			IJsonFieldConverter categoryConverter = fieldsConverters.getFieldConverters().get(categoryIdField);
			if (productConverter != null && categoryConverter != null) {
				String productId = productConverter.convert(pJsonObject, productIdField);

				String categoryId = categoryConverter.convert(pJsonObject, categoryIdField);
				if (StringUtils.isNotBlank(productId) && StringUtils.isNotBlank(categoryId)) {
					if (!pCategoryProducts.containsKey(categoryId)) {
						pCategoryProducts.put(categoryId, new ArrayList<String>());
					}
					pCategoryProducts.get(categoryId).add(productId);
				} else if (StringUtils.isNotBlank(productId) && !StringUtils.isNotBlank(categoryId)) {
					if (!pCategoryProducts.containsKey(WEB_CATEGORY_CODE_EMPTY_CATEGORY)) {
						pCategoryProducts.put(WEB_CATEGORY_CODE_EMPTY_CATEGORY, new ArrayList<String>());
					}
					pCategoryProducts.get(WEB_CATEGORY_CODE_EMPTY_CATEGORY).add(productId);
				}
			}
		}
	}

	public Repository getProductCatalog() {
		return productCatalog;
	}

	public void setProductCatalog(Repository pProductCatalog) {
		productCatalog = pProductCatalog;
	}
}
