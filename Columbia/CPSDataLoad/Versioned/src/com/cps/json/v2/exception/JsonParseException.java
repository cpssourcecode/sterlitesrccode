package com.cps.json.v2.exception;

import org.json.JSONObject;

/**
 * 
 * @author VSG
 *
 */
public class JsonParseException extends Exception {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8891043173385728820L;
	
	/**
	 * json object which cause and error
	 */
	private JSONObject mErroredJsonObject;
	
	public JsonParseException(JSONObject pErroredJsonObject) {
		super();
		mErroredJsonObject = pErroredJsonObject;
	}

	public JsonParseException(String pMessage, Throwable pCause, boolean pEnableSuppression,
			boolean pWritableStackTrace) {
		super(pMessage, pCause, pEnableSuppression, pWritableStackTrace);
	}

	public JsonParseException(String pMessage, Throwable pCause) {
		super(pMessage, pCause);
	}

	public JsonParseException(String pMessage) {
		super(pMessage);
	}

	public JsonParseException(Throwable pCause) {
		super(pCause);
	}

	public JSONObject getErroredJsonObject() {
		return mErroredJsonObject;
	}

	public void setErroredJsonObject(JSONObject pErroredJsonObject) {
		mErroredJsonObject = pErroredJsonObject;
	}

}
