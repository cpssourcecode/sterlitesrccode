package com.cps.json.v2;

import org.json.JSONException;
import org.json.JSONObject;

import atg.core.util.StringUtils;
import atg.nucleus.GenericService;

/**
 * 
 * @author VSG
 *
 */
public abstract class JsonFieldConverter extends GenericService implements IJsonFieldConverter {

	/**
	 * 
	 * @param pJsonObject - JSONObject
	 * @param pPath - field path
	 * @return {@link Object}
	 */
	protected Object getObjectValueByPath(JSONObject pJsonObject, String pPath) {
		Object result = null;
		try {
			if (StringUtils.isNotBlank(pPath) && pJsonObject != null) {
				String[] dimensions = pPath.split("\\.");
				if (dimensions != null && dimensions.length > 0) {
					result = pJsonObject;
					for (String dimension : dimensions) {
						if (result instanceof JSONObject) {
							if (((JSONObject) result).has(dimension)) {
								result = ((JSONObject) result).get(dimension);
							} else {
								result = null;
								break;
							}
						} else {
							result = null;
							break;
						}
					}
				}
			}
		} catch (JSONException e) {
			vlogError(e, "Error in JsonFieldConverter.getObjectValueByPath pJsonObject: {0}, pPath: {1}", pJsonObject,
					pPath);
		}
		return result;
	}

}
