package com.cps.json.v2.impl;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vsg.constants.VSGConstants;

/**
 * 
 * @author VSG
 *
 */
public class AliasArrayToMapConverter extends ArrayToMapConverter {

    public static final String ALIAS_NUMBER = "alias_number";
    public static final String CB_NUMBER = "cb_number";

    @Override
    protected String convertMapToString(Map<String, String> pMap) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Map.Entry<String, String> entry : pMap.entrySet()) {
            if (i > 0) {
                sb.append(VSGConstants.MAP_PAIR_DELIMITER);
            }
            sb.append(entry.getKey()).append(VSGConstants.MAP_KEY_VALUE_DELIMITER).append(entry.getValue());
            i++;
        }
        return sb.toString();
    }

    @Override
    public String convert(JSONObject pJsonObject, String pFieldPath) {
        Object value = getObjectValueByPath(pJsonObject, pFieldPath);
        if (value instanceof JSONArray) {
            Map<String, String> map = new HashMap<>();
            JSONArray array = (JSONArray) value;
            for (int i = 0; i < array.length(); i++) {
                Object obj = null;
                try {
                    obj = array.get(i);
                } catch (JSONException e) {
                    vlogError(e, "Error");
                }
                if (obj instanceof JSONObject) {
                    JSONObject arrayItem = (JSONObject) obj;
                    Map.Entry<String, String> attribute = createAttributeMapEntry(arrayItem);
                    if (attribute != null) {
                        String attrKey = attribute.getKey();
                        String attrValue = attribute.getValue();
                        String mapValue = map.get(attrKey);
                        if (mapValue != null) {
                            StringBuilder alias = new StringBuilder(mapValue);
                            alias.append("|").append(attrValue);
                            map.put(attrKey, alias.toString());
                        } else {
                            map.put(attrKey, attrValue);
                        }
                    }
                }
            }
            return convertMapToString(map);
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        String inputJson = " { 'item_number': '71826', 'item_aliases': [ { 'alias_number': '57161.72 ', 'cb_number': 106155 }, { 'alias_number': '806GN6565 ', 'cb_number': 110451 }, { 'alias_number': 'FL4L08\"300RW ', 'cb_number': 244592 }, { 'alias_number': '103073 ', 'cb_number': 100732 } ] }";
        AliasArrayToMapConverter converter = new AliasArrayToMapConverter();
        System.out.println("converted  == " + converter.convert(new JSONObject(inputJson), "item_aliases"));
    }

    /**
     * convert to pair key - value for attributes map
     * 
     * @param pAlias
     *            - JSONObject
     * @return Map.Entry<String, String>
     */
    protected Map.Entry<String, String> createAttributeMapEntry(JSONObject pAlias) {
        Map.Entry<String, String> entry = null;
        StringBuilder value = new StringBuilder();
        StringBuilder name = new StringBuilder();

        if (pAlias != null) {
            Object valueObj = getObjectValueByPath(pAlias, ALIAS_NUMBER);
            Object nameObj = getObjectValueByPath(pAlias, CB_NUMBER);

            if (valueObj instanceof String) {
                String valueStr = (String) valueObj;
                value.append(valueStr.trim());
            }
            if (nameObj instanceof String) {
                String nameStr = (String) nameObj;
                name.append(nameStr.trim());
            } else if (nameObj instanceof Integer) {
                String nameStr = String.valueOf(nameObj);
                name.append(nameStr.trim());
            }

            if (value.length() > 0 && name.length() > 0) {
                entry = new AbstractMap.SimpleEntry<String, String>(name.toString(), value.toString());
            }
        }
        return entry;
    }
}
