package com.cps.json.v2;

import static com.cps.json.JsonConstants.EMPTY;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import atg.nucleus.GenericService;

/**
 * 
 * @author VSG
 *
 */
public abstract class JsonItemConverter extends GenericService implements IJsonItemConverter {

	/** The json fields info for conversion */
	private JsonFieldsConverters mJsonFieldsInfo;
	
	/**
	 * converts json item to fields array
	 * @param pJsonObject - JSONObject
	 * @return List<String>
	 */
	protected List<String> convertToFieldsArray(JSONObject pJsonObject) {
		List<String> processedFields = new ArrayList<String>();
		List<String> fieldNames = getJsonFieldsInfo().getFields();
		Map<String, IJsonFieldConverter> convertersMap = getJsonFieldsInfo().getFieldConverters();
		for (String fieldPath : fieldNames) {
			IJsonFieldConverter converter = null;
			if(convertersMap != null) {
				converter = getJsonFieldsInfo().getFieldConverters().get(fieldPath);
			}
			if(converter == null) {
				converter = getJsonFieldsInfo().getDefaultFieldConverter();
			}
			String fieldValue = converter.convert(pJsonObject, fieldPath);
			if (fieldValue == null) {
				fieldValue = EMPTY;
			}
			processedFields.add(fieldValue);
		}
		return processedFields;
	}
	
	@Override
	public JsonFieldsConverters getJsonFieldsInfo() {
		return mJsonFieldsInfo;
	}

	public void setJsonFieldsInfo(JsonFieldsConverters pJsonFieldsInfo) {
		mJsonFieldsInfo = pJsonFieldsInfo;
	}
}
