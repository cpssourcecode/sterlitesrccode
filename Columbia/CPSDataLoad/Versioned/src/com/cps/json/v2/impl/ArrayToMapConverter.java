package com.cps.json.v2.impl;

import java.util.HashMap;
import java.util.Map;

import com.cps.json.v2.IJsonFieldConverter;
import com.cps.json.v2.JsonFieldConverter;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import vsg.constants.VSGConstants;

/**
 * 
 * @author VSG.
 *
 */
public abstract class ArrayToMapConverter extends JsonFieldConverter implements IJsonFieldConverter {

	@Override
	public String convert(JSONObject pJsonObject, String pFieldPath) {
		Object value = getObjectValueByPath(pJsonObject, pFieldPath);
		if (value instanceof JSONArray) {
			Map<String, String> map = new HashMap<>();
			JSONArray array = (JSONArray) value;
			for (int i = 0; i < array.length(); i++) {
				Object obj = null;
				try {
					obj = array.get(i);
				} catch (JSONException e) {
					vlogError(e,"Error");
				}
				if (obj instanceof JSONObject) {
					JSONObject arrayItem = (JSONObject) obj;
					Map.Entry<String, String> attribute = createAttributeMapEntry(arrayItem);
					if (attribute != null) {
						map.put(attribute.getKey(), attribute.getValue());
					}
				}
			}
			return convertMapToString(map);
		}
		return null;
	}
	
	/**
	 * convert map to string representation
	 * @param pMap - Map
	 * @return String
	 */
	protected String convertMapToString(Map<String, String> pMap) {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		for(Map.Entry<String, String> entry: pMap.entrySet()) {
			if(i > 0) {
				sb.append(VSGConstants.MAP_PAIR_DELIMITER);
			}
			sb.append(entry.getKey()).append(VSGConstants.MAP_KEY_VALUE_DELIMITER).append(StringEscapeUtils.escapeCsv(entry.getValue()));
			i++;
		}
		return sb.toString();
	}

	/**
	 * convert to pair key - value for attributes map
	 * @param pItem - JSONObject
	 * @return Map.Entry<String, String>
	 */
	protected abstract Map.Entry<String, String> createAttributeMapEntry(JSONObject pItem);
}
