package com.cps.json;

import atg.service.scheduler.SingletonSchedulableService;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

import com.cps.email.CommonEmailSender;
import com.cps.parse.IParser;
import vsg.load.service.IImportService;

import java.util.ArrayList;
import java.util.List;


/**
 * JsonParserScheduler
 *
 * @author Andy Porter
 */

public class JsonParserScheduler extends SingletonSchedulableService {

	/**
	 * array of loaders
	 */
	private IParser[] mLoaders;

	/**
	 * mEnableJob
	 */
	private boolean mEnableJob;
	/**
	 * mActive
	 */
	private boolean mActive = false;
	/**
	 * SCHEDULER_NAME
	 */
	private static final String SCHEDULER_NAME = "Json Parser Scheduler";

	/**
	 * import service
	 */
	private IImportService mImportService;

	/**
	 * return import service to be scheduled
	 *
	 * @return import service to be scheduled
	 */
	public IImportService getImportService() {
		return mImportService;
	}

	/**
	 * init import service to be scheduled
	 *
	 * @param pImportService import service to be scheduled
	 */
	public void setImportService(IImportService pImportService) {
		mImportService = pImportService;
	}


	private boolean mWorkspaceScheduler = false;

	private boolean mFtpEnabled;
	private CommonEmailSender commonEmailSender;
	
	/**
	 * doScheduledTask
	 *
	 * @param arg0 Scheduler
	 * @param arg1 ScheduledJob
	 */
	@Override
	public void doScheduledTask(Scheduler arg0, ScheduledJob arg1) {
		int success = 1;
		try {
			if (isLoggingDebug()) {
				logDebug(SCHEDULER_NAME + " Job enabled : " + isEnableJob());
			}

			if (isEnableJob()) {
				setActive(true);
				success = runService();
				setActive(false);
			}

			if (isLoggingDebug()) {
				logDebug(SCHEDULER_NAME + " Job ends....");
			}
			success = 0;
		} catch (Exception ex) {
			if (isLoggingError()) {
				logError(ex);
			}
		}


	}

	/**
	 * runService
	 *
	 * @return int
	 */
	private int runService() {
		int success = 1;

		try {
			long startTime = System.currentTimeMillis();
			if (isLoggingDebug()) {
				logDebug("Start performScheduledTask invoked ----------------------starting->" + startTime);
			}

			performService();

			long endTime = System.currentTimeMillis();
			long totalTime = endTime - startTime;

			if (isLoggingDebug()) {
				logDebug("End performScheduledTask invoked ----------> complete = " + success);
			}

			if (isLoggingDebug()) {
				logDebug(new StringBuilder().append("performScheduledTask - exit ")
						.append("Job:")
						.append(getJobName())
						.append(" took: ")
						.append(processTime(totalTime))
						.toString());
			}

		} catch (Exception ex) {
			if (isLoggingError()) {
				logError(ex);
			}
		}
		return success;
	}

	/**
	 * launch loaders to work
	 */
	protected void performService() {
		List<String> initialImportFilesAll = new ArrayList<String>();

		for (IParser initialParser : mLoaders) {
//			if (initialParser instanceof JsonParser) {
//				((JsonParser) initialParser).setWorkspaceParser(isWorkspaceScheduler());
//			}
			List<String> initialImportFiles = initialParser.performProcessing();
			initialImportFilesAll.addAll(initialImportFiles);
		}
		getCommonEmailSender().sendJsonBaseFeedStatusEmail();
		synchronized (getImportService()) {
			getImportService().service(initialImportFilesAll);
		}
	}


	/**
	 * processTime
	 *
	 * @param pTime long
	 * @return String
	 */
	public String processTime(long pTime) {
		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(".processTime")
					.append(" - start")
					.toString());
		}
		StringBuilder timeValue = new StringBuilder();
		if (pTime > 60000) {
			// scale is in the mintues
			timeValue.append(pTime / 60000).append(" minute").append((pTime / 60000 > 1) ? "s" : "");
			if (pTime % 60000 > 0) {
				timeValue.append(" ").append((pTime % 60000) / 1000).append(" second").append(((pTime % 60000) / 1000 > 1) ? "s" : "");
			}
		} else {
			// scale is in the seconds
			timeValue.append(pTime / 1000).append(" second").append((pTime / 1000 > 1) ? "s" : "");
		}
		if (isLoggingDebug()) {
			logDebug(new StringBuilder().append(".processTime")
					.append(" - exit")
					.toString());
		}
		return timeValue.toString();
	}

	/**
	 * runJobOnce
	 */
	public void runJobOnce() {
		setEnableJob(true);
		performScheduledTask(null, null);
		setEnableJob(false);
	}

	public boolean isEnableJob() {
		return mEnableJob;
	}

	public void setEnableJob(boolean pEnableJob) {
		mEnableJob = pEnableJob;
	}

	public boolean isActive() {
		return mActive;
	}

	protected void setActive(boolean pActive) {
		mActive = pActive;
	}


	/**
	 * Sets new array of loaders.
	 *
	 * @param pLoaders New value of array of loaders.
	 */
	public void setLoaders(IParser[] pLoaders) {
		mLoaders = pLoaders;
	}

	/**
	 * Gets array of loaders.
	 *
	 * @return Value of array of loaders.
	 */
	public IParser[] getLoaders() {
		return mLoaders;
	}


	/**
	 * Gets mWorkspaceScheduler.
	 *
	 * @return Value of mWorkspaceScheduler.
	 */
	public boolean isWorkspaceScheduler() {
		return mWorkspaceScheduler;
	}

	/**
	 * Sets new mWorkspaceScheduler.
	 *
	 * @param pWorkspaceScheduler New value of mWorkspaceScheduler.
	 */
	public void setWorkspaceScheduler(boolean pWorkspaceScheduler) {
		mWorkspaceScheduler = pWorkspaceScheduler;
	}

	public boolean isFtpEnabled() {
		return mFtpEnabled;
	}

	public void setFtpEnabled(boolean pFtpEnabled) {
		mFtpEnabled = pFtpEnabled;
	}

	/**
	 * @return the commonEmailSender
	 */
	public CommonEmailSender getCommonEmailSender() {
		return commonEmailSender;
	}

	/**
	 * @param commonEmailSender the commonEmailSender to set
	 */
	public void setCommonEmailSender(CommonEmailSender commonEmailSender) {
		this.commonEmailSender = commonEmailSender;
	}
	
}
