package com.cps.json;

import atg.core.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vsg.util.json.InputStreamStripDecorator;
import vsg.util.json.JsonStreaminHelper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;


/**
 * @author Andy Porter
 */
public class JsonListParser extends JsonParser {

	/**
	 * load
	 *
	 * @param pFile FileInputStream
	 * @return boolean
	 */

	/**
	 * load
	 *
	 * @param pFile FileInputStream
	 * @return boolean
	 */
	protected boolean load(FileInputStream pFile) throws IOException, JSONException {
		boolean result = false;
		// category to fixedChildProducts Map
		Map<String, List<String>> categoryChildProductsMap = new HashMap<String, List<String>>();
		List<List<String>> categoriesChildProductsList = new ArrayList<List<String>>();

		if (isAddWriterOn()) {
			mAddWriter = getJsonExportTools().getAddWriter();
		}
		if (isUpdateWriterOn()) {
			mUpdateWriter = getJsonExportTools().getUpdateWriter();
		}

		String categoryIdName = getJsonFieldsInfo().getCategoryIdName();
		String productIdName = getJsonFieldsInfo().getProductIdName();
		if (null != productIdName && null != categoryIdName) {
			InputStreamStripDecorator dec = new InputStreamStripDecorator(pFile);
			JsonStreaminHelper.gotoToken(dec, "\"items\": [");
			String jsonStr = null;
			JSONObject jsonObject = null;
			int i = 0;
			while ((jsonStr = JsonStreaminHelper.nextItem(dec)) != null) {
				jsonObject = new JSONObject(jsonStr);

/*
				String actionFieldValue = getStringValueByPath(jsonObject, getJsonFieldsInfo().getActionField());
				if (StringUtils.isBlank(actionFieldValue)) {
					actionFieldValue = ACTION_FIELD_ADD;
				}
				if (ACTION_FIELD_DELETE.equals(actionFieldValue)) {
					continue;
				}
*/

				String categoryId = getStringValueByPath(jsonObject, categoryIdName);
				if (categoryId.length() == 8) {
					categoryId = "0" + categoryId;
				}
				String productId = getStringValueByPath(jsonObject, productIdName);
				if (!StringUtils.isBlank(categoryId) && !StringUtils.isBlank(productId)) {
					List<String> categoryChildProducts = null;
					if (categoryChildProductsMap.containsKey(categoryId)) {
						categoryChildProducts = categoryChildProductsMap.get(categoryId);
					}
					if (null == categoryChildProducts) {
						categoryChildProducts = new ArrayList<String>();
					}
					categoryChildProducts.add(productId);
					categoryChildProductsMap.put(categoryId, categoryChildProducts);
				}


				if (isLoggingDebug()) {
					logDebug(new StringBuilder("CATALOG item ").append(i).toString());
				}
				i++;
			}
			categoriesChildProductsList = getCategoriesChildrenListFromMap(categoryChildProductsMap);

			for (List<String> categoryChildProductsList : categoriesChildProductsList) {
				String[] categoryChildProductsArr = new String[2];
				categoryChildProductsArr = categoryChildProductsList.toArray(categoryChildProductsArr);
				if (null != mAddWriter) {
					mAddWriter.writeNext(categoryChildProductsArr);
				}
				if (null != mUpdateWriter) {
					mUpdateWriter.writeNext(categoryChildProductsArr);
				}
			}

		}
		return result;
	}

	/**
	 * load
	 *
	 * @param pFile FileInputStream
	 * @return boolean
	 */
	protected boolean loadOld(FileInputStream pFile) throws JSONException {
		boolean result = false;
		// category to fixedChildProducts Map
		Map<String, List<String>> categoryChildProductsMap = new HashMap<String, List<String>>();
		List<List<String>> categoriesChildProductsList = new ArrayList<List<String>>();

		if (isAddWriterOn()) {
			mAddWriter = getJsonExportTools().getAddWriter();
		}
		if (isUpdateWriterOn()) {
			mUpdateWriter = getJsonExportTools().getUpdateWriter();
		}

		JSONObject jsonDocument = createJsonObjectFromFile(pFile);
		String categoryIdName = getJsonFieldsInfo().getCategoryIdName();
		String productIdName = getJsonFieldsInfo().getProductIdName();
		if (null != jsonDocument && null != productIdName && null != categoryIdName) {
			JSONArray itemsArray = getJsonArrayByPath(jsonDocument, getJsonFieldsInfo().getMainJsonArrayPath());
			if (null != itemsArray) {
				int itemsArraySize = itemsArray.length();
				for (int i = 0; i < itemsArraySize; i++) {


					JSONObject item = (JSONObject) itemsArray.get(i);

/*
					String actionFieldValue = getStringValueByPath(item, getJsonFieldsInfo().getActionField());
					if (StringUtils.isBlank(actionFieldValue)) {
						actionFieldValue = ACTION_FIELD_ADD;
					}
					if (ACTION_FIELD_DELETE.equals(actionFieldValue)) {
						continue;
					}
*/

					String categoryId = getStringValueByPath(item, categoryIdName);
					if (categoryId.length() == 8) {
						categoryId = "0" + categoryId;
					}
					String productId = getStringValueByPath(item, productIdName);
					if (!StringUtils.isBlank(categoryId) && !StringUtils.isBlank(productId)) {
						List<String> categoryChildProducts = null;
						if (categoryChildProductsMap.containsKey(categoryId)) {
							categoryChildProducts = categoryChildProductsMap.get(categoryId);
						}
						if (null == categoryChildProducts) {
							categoryChildProducts = new ArrayList<String>();
						}
						categoryChildProducts.add(productId);
						categoryChildProductsMap.put(categoryId, categoryChildProducts);
					}
				}
			}

			categoriesChildProductsList = getCategoriesChildrenListFromMap(categoryChildProductsMap);
		}

		for (List<String> categoryChildProductsList : categoriesChildProductsList) {
			String[] categoryChildProductsArr = new String[2];
			categoryChildProductsArr = categoryChildProductsList.toArray(categoryChildProductsArr);
			if (null != mAddWriter) {
				mAddWriter.writeNext(categoryChildProductsArr);
			}
			if (null != mUpdateWriter) {
				mUpdateWriter.writeNext(categoryChildProductsArr);
			}
		}

		return result;
	}

	// -----------------------------------------------------------------------------------------------------------------

}
