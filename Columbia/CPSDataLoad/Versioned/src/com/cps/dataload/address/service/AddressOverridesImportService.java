package com.cps.dataload.address.service;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

import com.cps.dataload.base.service.AbstractDynamicRecordImportService;
import com.cps.dataload.exception.FeedProcessException;
import com.cps.util.CPSConstants;

import atg.repository.MutableRepository;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/**
 * @author R.Srinivasan
 *
 */
public class AddressOverridesImportService extends AbstractDynamicRecordImportService {


    private Repository profileRepository;
    private static final char ZERO = '0';
    private String fillText;
    private int maxLength;

    /**
     * @return the fillText
     */
    public String getFillText() {
        return fillText;
    }

    /**
     * @param fillText
     *            the fillText to set
     */
    public void setFillText(String fillText) {
        this.fillText = fillText;
    }

    /**
     * @return the maxLength
     */
    public int getMaxLength() {
        return maxLength;
    }

    /**
     * @param maxLength
     *            the maxLength to set
     */
    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
        setFillText(StringUtils.repeat(ZERO, maxLength));
    }

    /**
     * @return the profileRepository
     */
    public Repository getProfileRepository() {
        return profileRepository;
    }

    /**
     * @param profileRepository
     *            the profileRepository to set
     */
    public void setProfileRepository(Repository profileRepository) {
        this.profileRepository = profileRepository;
    }

    /* (non-Javadoc)
     * @see com.cps.dataload.pricing.service.AbstractDynamicRecordImportService#getItemDescriptorNameForProcessing()
     */
    @Override
    protected String getItemDescriptorNameForProcessing() {
        return getProcessedItemDescriptor();
    }

    /*
     * * override the validate method to call the super first and then validate the contact info. if there is no contactInfo with that ID, ignore it
     */
    @Override
    protected String validateRecord(CSVRecord record) throws FeedProcessException {
        boolean invalidRecord = false;
        String invalidReason = super.validateRecord(record);
        String addressId = record.get("ABAN8_Address");
        RepositoryItem contactInfoItem = getContactInfoItem(addressId);
        if (contactInfoItem == null) {
            invalidRecord = true;
        }

        if (invalidRecord) {
            invalidReason = new StringBuilder(invalidReason).append("Invalid address").toString();
        }
        return invalidReason;
    }

    private RepositoryItem getContactInfoItem(String addressId) {
        RepositoryItem addressItem = null;
        try {
            addressItem = getProfileRepository().getItem(addressId, getItemDescriptorNameForProcessing());
        } catch (RepositoryException re) {
            vlogError(re, "Error while getting product item");
        }
        return addressItem;
    }

    protected MutableRepository getRepositoryForProcessing() {
        return (MutableRepository) getProfileRepository();
    }

    /**
     * The PriceList contains properties like lp005, lp101 etc., but the feed will be coming up only with the matrix number so, we will check whether zero
     * padding is needed and if yes, pad zeros and then prefix with 'lp' so that during runtime straightaway the matrix value returned from contactInfo can be
     * used for getting price.
     * 
     * @param value
     * @return padded and prefixed matrix property name
     */
    @Override
    protected Object handleCustomDataType(String thisKey, Object value) throws FeedProcessException {
        String output = null;
        if (value != null && CPSConstants.ABAC06_BASE_MATRIX.equals(thisKey)) {
        String input = String.valueOf(value);
            output = input.length() < getMaxLength() ? getFillText().substring(input.length()) + input : input;
            output = "lp" + output;
        }
        return output;
    }

}
