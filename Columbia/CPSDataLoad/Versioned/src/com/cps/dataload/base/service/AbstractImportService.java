package com.cps.dataload.base.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cps.dataload.exception.FeedProcessException;
import com.cps.dataload.logging.FeedProcessingLogger;
import com.cps.dataload.pricing.bean.CatalogItem;
import com.cps.dataload.util.FeedUtil;
import com.cps.dataload.util.FilePermissionsHelper;
import com.cps.util.RepositoryUtils;
import com.cps.version.VersioningTools;

import atg.epub.project.Process;
import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.RunProcessException;

/**
 * This class acts as the base class for all import services with common logic
 *
 * @author Srilakshmi
 *
 */
@SuppressWarnings("PMD")
public abstract class AbstractImportService extends GenericService {

    public static final String WAITING_FOR_PROJECT_0_TO_GET_CHECKED_IN = "Waiting for this Project [{0}] to get Checked In....";
    public static final String WAITING_FOR_PROJECT_0_DEPLOYMENT_TO_COMPLETE = "Waiting for this Project [{0}] Deployment to Complete....";

    private String feedFilePath;
    private String lockFilePath;
    private String projectName;
    private VersioningTools versioningTools;
    private boolean versionedImport;
    private FeedUtil feedUtil;
    private String feedName;
    private long deploymentCheckSleepTimeInMillis;
    private RepositoryUtils repositoryUtils;
    protected int noOfitemsPerProject;
    protected int batchNumber;
    private String itemIdParamName;
    protected Map<String, String> recordToItemPropertiesMap;

    private Map<String, String> repositoryPropertyDataTypeMap;
    private String dateFormatPattern;
    protected FeedProcessingLogger feedProcessingLogger;
    private String processedFolderName = "/success";
    private String errorFolderName = "/error";
    private String filesFinalDestinationFolderName;
    private FilePermissionsHelper filePermissionsHelper;
    private boolean useBOMInputStream = true;

    /**
     * @return the useBOMInputStream
     */
    public boolean isUseBOMInputStream() {
        return useBOMInputStream;
    }

    /**
     * @param useBOMInputStream
     *            the useBOMInputStream to set
     */
    public void setUseBOMInputStream(boolean useBOMInputStream) {
        this.useBOMInputStream = useBOMInputStream;
    }

    /**
     * @return the filePermissionsHelper
     */
    public FilePermissionsHelper getFilePermissionsHelper() {
        return filePermissionsHelper;
    }

    /**
     * @param filePermissionsHelper
     *            the filePermissionsHelper to set
     */
    public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
        this.filePermissionsHelper = filePermissionsHelper;
    }

    /**
     * @return the filesFinalDestinationFolderName
     */
    public String getFilesFinalDestinationFolderName() {
        return filesFinalDestinationFolderName;
    }

    /**
     * @param filesFinalDestinationFolderName
     *            the filesFinalDestinationFolderName to set
     */
    public void setFilesFinalDestinationFolderName(String filesFinalDestinationFolderName) {
        this.filesFinalDestinationFolderName = filesFinalDestinationFolderName;
    }

    /**
     * @return the processedFolderName
     */
    public String getProcessedFolderName() {
        return processedFolderName;
    }

    /**
     * @param processedFolderName
     *            the processedFolderName to set
     */
    public void setProcessedFolderName(String processedFolderName) {
        this.processedFolderName = processedFolderName;
    }

    /**
     * @return the errorFolderName
     */
    public String getErrorFolderName() {
        return errorFolderName;
    }

    /**
     * @param errorFolderName
     *            the errorFolderName to set
     */
    public void setErrorFolderName(String errorFolderName) {
        this.errorFolderName = errorFolderName;
    }

    /**
     * @return the feedProcessingLogger
     */
    public FeedProcessingLogger getFeedProcessingLogger() {
        return feedProcessingLogger;
    }

    /**
     * @param feedProcessingLogger
     *            the feedProcessingLogger to set
     */
    public void setFeedProcessingLogger(FeedProcessingLogger feedProcessingLogger) {
        this.feedProcessingLogger = feedProcessingLogger;
    }

    /**
     * @return the dateFormatPattern
     */
    public String getDateFormatPattern() {
        return dateFormatPattern;
    }

    /**
     * @param dateFormatPattern
     *            the dateFormatPattern to set
     */
    public void setDateFormatPattern(String dateFormatPattern) {
        this.dateFormatPattern = dateFormatPattern;
    }

    /**
     * @param recordToItemPropertiesMap
     *            the recordToItemPropertiesMap to set
     */
    public void setRecordToItemPropertiesMap(Map<String, String> recordToItemPropertiesMap) {
        this.recordToItemPropertiesMap = recordToItemPropertiesMap;
    }

    /**
     * @return the repositoryPropertyDataTypeMap
     */
    public Map<String, String> getRepositoryPropertyDataTypeMap() {
        return repositoryPropertyDataTypeMap;
    }

    /**
     * @param repositoryPropertyDataTypeMap
     *            the repositoryPropertyDataTypeMap to set
     */
    public void setRepositoryPropertyDataTypeMap(Map<String, String> repositoryPropertyDataTypeMap) {
        this.repositoryPropertyDataTypeMap = repositoryPropertyDataTypeMap;
    }

    /**
     * @return the itemIdParamName
     */
    public String getItemIdParamName() {
        return itemIdParamName;
    }

    /**
     * @param itemIdParamName
     *            the itemIdParamName to set
     */
    public void setItemIdParamName(String itemIdParamName) {
        this.itemIdParamName = itemIdParamName;
    }

    /**
     * @return the recordToItemMap
     */
    public Map getRecordToItemPropertiesMap() {
        return recordToItemPropertiesMap;
    }

    /**
     * @return the batchNumber
     */
    public int getBatchNumber() {
        return batchNumber;
    }

    /**
     * @param batchNumber
     *            the batchNumber to set
     */
    public void setBatchNumber(int batchNumber) {
        this.batchNumber = batchNumber;
    }

    /**
     * @return the noOfitemsPerProject
     */
    public int getNoOfitemsPerProject() {
        return noOfitemsPerProject;
    }

    /**
     * @param noOfitemsPerProject
     *            the noOfitemsPerProject to set
     */
    public void setNoOfitemsPerProject(int noOfitemsPerProject) {
        this.noOfitemsPerProject = noOfitemsPerProject;
    }

    /**
     * @return the feedFilePath
     */
    public String getFeedFilePath() {
        return feedFilePath;
    }

    /**
     * @param feedFilePath
     *            the feedFilePath to set
     */
    public void setFeedFilePath(String feedFilePath) {
        this.feedFilePath = feedFilePath;
    }

    public String getLockFilePath() {
        return lockFilePath;
    }

    public void setLockFilePath(String lockFilePath) {
        this.lockFilePath = lockFilePath;
    }

    /**
     * @return the projectName
     */
    public String getProjectName() {
        return projectName;
    }

    /**
     * @param projectName
     *            the projectName to set
     */
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    /**
     * @return the versioningTools
     */
    public VersioningTools getVersioningTools() {
        return versioningTools;
    }

    /**
     * @param versioningTools
     *            the versioningTools to set
     */
    public void setVersioningTools(VersioningTools versioningTools) {
        this.versioningTools = versioningTools;
    }

    /**
     * @return the versionedImport
     */
    public boolean isVersionedImport() {
        return versionedImport;
    }

    /**
     * @param versionedImport
     *            the versionedImport to set
     */
    public void setVersionedImport(boolean versionedImport) {
        this.versionedImport = versionedImport;
    }

    /**
     * @return the feedUtil
     */
    public FeedUtil getFeedUtil() {
        return feedUtil;
    }

    /**
     * @param feedUtil
     *            the feedUtil to set
     */
    public void setFeedUtil(FeedUtil feedUtil) {
        this.feedUtil = feedUtil;
    }

    /**
     * @return the feedName
     */
    public String getFeedName() {
        return feedName;
    }

    /**
     * @param feedName
     *            the feedName to set
     */
    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    /**
     * @return the deploymentCheckSleepTimeInMillis
     */
    public long getDeploymentCheckSleepTimeInMillis() {
        return deploymentCheckSleepTimeInMillis;
    }

    /**
     * @param deploymentCheckSleepTimeInMillis
     *            the deploymentCheckSleepTimeInMillis to set
     */
    public void setDeploymentCheckSleepTimeInMillis(long deploymentCheckSleepTimeInMillis) {
        this.deploymentCheckSleepTimeInMillis = deploymentCheckSleepTimeInMillis;
    }

    /**
     * @return the repositoryUtils
     */
    public RepositoryUtils getRepositoryUtils() {
        return repositoryUtils;
    }

    /**
     * @param repositoryUtils
     *            the repositoryUtils to set
     */
    public void setRepositoryUtils(RepositoryUtils repositoryUtils) {
        this.repositoryUtils = repositoryUtils;
    }

    /**
     * @return the waitingForProject0ToGetCheckedIn
     */
    public static String getWaitingForProject0ToGetCheckedIn() {
        return WAITING_FOR_PROJECT_0_TO_GET_CHECKED_IN;
    }

    /**
     * @return the waitingForProject0DeploymentToComplete
     */
    public static String getWaitingForProject0DeploymentToComplete() {
        return WAITING_FOR_PROJECT_0_DEPLOYMENT_TO_COMPLETE;
    }

    /**
     *
     * @return CSV parser object representing the input CSV File.
     */
    public CSVParser getParser() throws FeedProcessException {
        File feedFile = new File(getFeedFilePath());

        // CSVFormat format = CSVFormat.newFormat('|').withQuote('"').withFirstRecordAsHeader().withIgnoreHeaderCase().withIgnoreEmptyLines()
        // .withIgnoreSurroundingSpaces();

        CSVFormat format = CSVFormat.TDF.withQuote(null).withFirstRecordAsHeader().withIgnoreHeaderCase().withIgnoreEmptyLines().withIgnoreSurroundingSpaces();
        CSVParser parser = null;

        try {
            if (isUseBOMInputStream()) {
                parser = new CSVParser(new InputStreamReader(new BOMInputStream(new FileInputStream(feedFile))), format);
            } else {
                parser = new CSVParser(new FileReader(feedFile), format);
            }

        } catch (IOException ex) {
            // reason Info and not error is being logged is to record this in the custom log file.
            feedProcessingLogger.vlogFeed("feed-error", "problem reading the feed file  {0}", feedFile.getAbsolutePath());
            feedProcessingLogger.setErrorProcessingFeed(true);
            logError(ex);
            throw new FeedProcessException(ex.getMessage());
        }
        return parser;
    }

    protected boolean checkLockFileExists() {
        if (StringUtils.isBlank(getLockFilePath())) {
            return true;
        } else {
            File lockFile = new File(getLockFilePath());
            if (lockFile.exists()) {
                return true;
            } else {
                return false;
            }
        }

    }

    /**
     *
     * @return CSV parser object representing the input CSV File for the additional feed file specified by the name
     */
    public CSVParser getParser(String additionalFilePath) {
        File feedFile = new File(additionalFilePath);

        CSVFormat format = CSVFormat.newFormat('|').withQuote('"').withFirstRecordAsHeader().withIgnoreHeaderCase().withIgnoreEmptyLines()
                        .withIgnoreSurroundingSpaces();
        CSVParser parser = null;

        try {
            parser = new CSVParser(new FileReader(feedFile), format);
        } catch (IOException ex) {
            // goes to the main server log file
            vlogError("problem reading the feed file  {0}", feedFile.getAbsolutePath());
            // sendthe message to the custom Feed File
            feedProcessingLogger.vlogFeed("feed-error", "problem reading the feed file  {0}", feedFile.getAbsolutePath());
            logError(ex);
            feedProcessingLogger.setErrorProcessingFeed(true);

        }
        return parser;
    }

    /**
     * This method creates a project,processes the feed file in the corresponding feed import service and deploys the project and also does the check in of the
     * project
     */
    public void importData(List inProcessRecordList) throws FeedProcessException {

            Process process = null;
            if (isVersionedImport()) {
                process = createProcess();
            }
        executeImport(inProcessRecordList);
            if (isVersionedImport()) {
                advanceWorkflow(process);
            }

    }



    /**
     *
     * @param expression
     * @return
     */
    public NodeList getXMLParser() {
        vlogDebug("Inside getXMLParser method");
        File inputFile = new File(getFeedFilePath());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        NodeList nodeList = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            XPath xPath = XPathFactory.newInstance().newXPath();

            // String expression = "/Feed/Product";
            nodeList = (NodeList) xPath.compile("/Feed/Product").evaluate(doc, XPathConstants.NODESET);

            vlogDebug("Node List length:: {0}", nodeList.getLength());
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
            // goes to the main server log file
            vlogError("problem reading the feed file  {0}", getFeedFilePath());
            logError(ex);
        }
        vlogDebug("End of getXMLParser method");
        return nodeList;
    }

    /*
     * public void importXMLData(List inProcessXmlRecordList) {
     *
     * NodeList xmlParser = getXMLParser(); Process process = null; try { if (isVersionedImport()) { process = createProcess(); } executeImport(xmlParser,
     * inProcessXmlRecordList); if (isVersionedImport()) { advanceWorkflow(process); } } catch (FeedProcessException e) { vlogError(
     * "Error occured while import xml record :: {0}", e.getMessage()); }
     *
     * }
     */

    protected Process createProcess() throws FeedProcessException {
        Process process = null;
        try {
            String projectName = getProjectName();
            process = getVersioningTools().createProject(projectName);
        } catch (FeedProcessException e) {
            e.printStackTrace();
            logError("Error while creating project");
            getFeedProcessingLogger().vlogFeed("feed-error", e.getMessage());
            throw new FeedProcessException(e.getMessage());
        }
        return process;
    }

    protected void advanceWorkflow(Process process) throws FeedProcessException {
        try {
            getVersioningTools().approveAndDeployProject(process);
            String completeMessage = MessageFormat.format(WAITING_FOR_PROJECT_0_DEPLOYMENT_TO_COMPLETE, projectName).toString();
            feedProcessingLogger.vlogFeed("feed-info", "waiting for deployment to complete");
            while (!getVersioningTools().isProjectDeployed(process.getProject())) {
                // wait for project deployment to complete
                Thread.sleep(getDeploymentCheckSleepTimeInMillis());
                vlogInfo(completeMessage);
            }
            feedProcessingLogger.vlogFeed("feed-info", "deployment completed");
        } catch (Exception e) {// NOPMD
            try {
                throw new RunProcessException(e);
            } catch (RunProcessException e1) { // NOPMD
                vlogError("Error while completing project deployment");
                getFeedProcessingLogger().vlogFeed("feed-error", e.getMessage());
                throw new FeedProcessException(e1.getMessage());
            }
        } finally { // NOPMD
            getVersioningTools().popDevelopmentLine();
            feedProcessingLogger.vlogFeed("feed-info", "waiting for project  {0} to checkin", projectName);
            String checkedInMessage = MessageFormat.format(WAITING_FOR_PROJECT_0_TO_GET_CHECKED_IN, projectName).toString();
            while (!process.getProject().isCheckedIn()) {
                try {
                    // wait for assets in the project to be checkedin
                    Thread.sleep(getDeploymentCheckSleepTimeInMillis());
                    logInfo(checkedInMessage);
                } catch (InterruptedException e) {
                    try {
                        throw new RunProcessException(e);
                    } catch (RunProcessException e1) { // NOPMD
                        vlogError("Error while checking in project");
                        getFeedProcessingLogger().vlogFeed("feed-error", e.getMessage());
                        throw new FeedProcessException(e1.getMessage());
                    }
                }
            }
            feedProcessingLogger.vlogFeed("feed-info", "project  {0} checkin complete", projectName);
        }
    }

    /**
     *
     * @param mutableRepository
     * @param itemDesc
     * @param item
     * @throws FeedProcessException
     */
    public void insertItem(MutableRepository mutableRepository, String itemDesc, CatalogItem item) throws FeedProcessException {

        vlogDebug("insert item start");
        boolean success;

        try {
            MutableRepositoryItem mri = mutableRepository.createItem(itemDesc);

            success = populateData(mri, item);
            if (success) {
                mutableRepository.addItem(mri);
            } else {
                vlogError("Skipping record with id :: {0} due to missing related data", item.getItemId());
            }
        } catch (RepositoryException re) {
            vlogError("Error while inserting item :: {0}", re.getMessage());
        }
        vlogDebug("insert item end----");
    }

    /**
     *
     * @param repository
     * @param itemDesc
     * @param item
     * @throws FeedProcessException
     */
    public void updateItem(MutableRepository mutableRepository, String itemDesc, CatalogItem item) throws FeedProcessException {

        vlogDebug("update item start-----");
        boolean success;
        try {

            MutableRepositoryItem mutItem = mutableRepository.getItemForUpdate(item.getItemId(), itemDesc);

            success = populateData(mutItem, item);
            if (success) {
                mutableRepository.updateItem(mutItem);
            } else {
                vlogError("Skipping updating the record with id :: {0} due to missing data", item.getItemId());
            }
        } catch (RepositoryException re) {
            vlogError("Error while updating item::{0}", re.getMessage());
        }
        vlogDebug("update item END");
    }

    public void copyFeedFileToFinalDestination() {
        try {

            File currentFile = new File(getFeedFilePath());
            Path sourcePath = currentFile.toPath();

            File destDir = new File(currentFile.getParent() + getFilesFinalDestinationFolderName());
            if (!destDir.isDirectory()) {
                destDir.mkdirs();
            }
            File destFile = new File(destDir, currentFile.getName());
            Path destPath = destFile.toPath();
            Files.copy(sourcePath, destPath, StandardCopyOption.REPLACE_EXISTING);
            Files.deleteIfExists(sourcePath);

            FilePermissionsHelper fpHelper = getFilePermissionsHelper();
            if (fpHelper != null) {
                fpHelper.setFilePermissions(destPath);
            }
        } catch (IOException e) {

            vlogError("error copying the files ");
            logError(e);
        }
    }

    /**
     * copy a file with the given name to the final destination
     *
     * @param currentFileName
     */
    public void copyFeedFileToFinalDestination(String currentFileName) {
        try {

            File currentFile = new File(currentFileName);
            Path sourcePath = currentFile.toPath();

            File destDir = new File(currentFile.getParent() + getFilesFinalDestinationFolderName());
            if (!destDir.isDirectory()) {
                destDir.mkdirs();
            }
            File destFile = new File(destDir, currentFile.getName());
            Path destPath = destFile.toPath();
            Files.copy(sourcePath, destPath, StandardCopyOption.REPLACE_EXISTING);
            Files.deleteIfExists(sourcePath);

            FilePermissionsHelper fpHelper = getFilePermissionsHelper();
            if (fpHelper == null) {
                fpHelper = (FilePermissionsHelper) Nucleus.getGlobalNucleus().resolveName("/cps/util/FilePermissionsHelper");
            }
            fpHelper.setFilePermissions(destPath);

        } catch (IOException e) {
            vlogError("error copying the log file ");
            logError(e);
        }
    }

    /**
     *
     * @param repository
     * @param itemDescriptor
     * @param feedId
     * @return
     * @throws RepositoryException
     */

    public RepositoryItem lookupRepositoryItem(Repository repository, String itemDescriptor, String feedId) throws RepositoryException {
        return repository.getItem(feedId, itemDescriptor);
    }

    /**
     *
     * @param repository
     * @param itemDescriptor
     * @param repositoryIds
     * @return
     * @throws RepositoryException
     *
     *             makes a repository.getItems query and returns an array of RepositoryItems
     */

    public RepositoryItem[] lookupRepositoryItems(Repository repository, String itemDescriptor, String... repositoryIds) throws RepositoryException {
        return repository.getItems(repositoryIds, itemDescriptor);
    }

    public boolean preImport() {
        boolean lockFileExists = checkLockFileExists();
        return lockFileExists;
    }

    /**
     * start import
     */
    public boolean startImport() {

        return initFeedLogFile();
    }

    /**
     * helper method to set the final folder as error folder in case this is called from outside this class by using object reference
     */
    public void setErrorFolderAsFinalDestination() {
        setFilesFinalDestinationFolderName(getErrorFolderName());
    }

    /**
     * helper method to set the final folder as success folder in case this is called from outside this class by using object reference
     */

    public void setSuccessFolderAsFinalDestination() {
        setFilesFinalDestinationFolderName(getProcessedFolderName());
    }

    /**
     * initialise the log file using the default feedfile path
     *
     * @return
     */
    public boolean initFeedLogFile() {
        return initFeedLogFile(getFeedFilePath());
    }

    /**
     * initialize the feed log file for the passed feed file name
     *
     * @param feedFilePath
     * @return
     */
    public boolean initFeedLogFile(String feedFilePath) {
        File feedFile = new File(feedFilePath);
        boolean success = true;
        // set the current component's FileLogger to the FeedProcessingLogger.
        FeedProcessingLogger fpLogger = getFeedProcessingLogger();
        try {
            fpLogger.initializeFeedLogger(feedFile.getName(), feedFile.getParent());
        } catch (IOException e) {
            vlogError("errors initializing the log system ");
            success = false;
        }
        return success;
    }

    /**
     *
     * @param mutableRepositoryItem
     * @param catalogItem
     * @return
     * @throws FeedProcessException
     */
    public abstract boolean populateData(MutableRepositoryItem mutableRepositoryItem, CatalogItem catalogItem) throws FeedProcessException;

    /**
     *
     * @param parser
     * @throws FeedProcessException
     */
    public abstract void executeImport(List inProcessRecordList) throws FeedProcessException;

    /**
     *
     * @param nodeList
     * @param inProcessXMLRecordList
     * @throws FeedProcessException
     */
    public void executeImport(NodeList nodeList, List inProcessXMLRecordList) throws FeedProcessException {
    }

}