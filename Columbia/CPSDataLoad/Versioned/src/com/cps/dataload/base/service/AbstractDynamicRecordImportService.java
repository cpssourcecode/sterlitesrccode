package com.cps.dataload.base.service;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.transaction.TransactionManager;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.BooleanUtils;

import com.cps.concurrent.CommonThreadPool;
import com.cps.dataload.exception.FeedProcessException;
import com.cps.dataload.helper.AbstractImportServiceHelper;
import com.cps.dataload.pricing.bean.CatalogItem;
import com.cps.dataload.pricing.bean.DynamicRecordItem;
import com.cps.dataload.util.FeedConstants;
import com.cps.rmi.CacheInvalidationManager;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;

import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.epub.project.Process;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;

/**
 * This class converts the CSV feed into DynamicRecordItem. the persisting of the DynamicRecordItem would be handled by specific import services.
 *
 * @author R.Srinivasan
 */
@SuppressWarnings("PMD")
public abstract class AbstractDynamicRecordImportService extends AbstractImportService {

    // protected List<DynamicRecordItem> inProcessRecordList;

    protected String processedItemDescriptor;

    protected List<String> mandatoryRecordColumns = new ArrayList<>();
    protected Map<String,List<String>> errorProducts =new  HashMap<String,List<String>>();

    private boolean populateAdditionalDataUsingPrePopulate;
    private boolean runPostImportSuccess;
    private boolean runPostProcessBatchImportSuccess;
    private boolean createRecordsGroupMap;
    private boolean enableStrictValidation;

    private int noOfColumns;

    private AbstractImportServiceHelper importHelper;
    private List<String> skipDynamicPropertiesForRepositoryItem = new ArrayList<>();

    // this property can be set from the child classes to false, in case the repositoryId is to be generated
    // during the call to the createItem method
    private boolean usePredefinedKey = true;
    private int maxThreads = 1;
    private boolean multiThreaded = false;
    private CommonThreadPool threadPool;
    private boolean insertUnavailableItems = true;
    private boolean someRecordsFailedValidation = false;
    private boolean switchingImport;
    private CacheInvalidationManager cacheInvalidationManager;
    private String invalidateCacheRepositoryPath;
    private MutableRepository lookupRepository;
    private MutableRepository importRepository;

    public boolean isSwitchingImport() {
        return switchingImport;
    }

    public void setSwitchingImport(boolean switchingImport) {
        this.switchingImport = switchingImport;
    }

    public MutableRepository getLookupRepository() {
        return lookupRepository;
    }

    public void setLookupRepository(MutableRepository lookupRepository) {
        this.lookupRepository = lookupRepository;
    }

    public MutableRepository getImportRepository() {
        return importRepository;
    }

    public void setImportRepository(MutableRepository importRepository) {
        this.importRepository = importRepository;
    }

    /**
     * @return the someRecordsFailedValidation
     */
    public boolean isSomeRecordsFailedValidation() {
        return someRecordsFailedValidation;
    }

    /**
     * @param someRecordsFailedValidation
     *            the someRecordsFailedValidation to set
     */
    public void setSomeRecordsFailedValidation(boolean someRecordsFailedValidation) {
        this.someRecordsFailedValidation = someRecordsFailedValidation;
    }

    /**
     * @return the enableStrictValidation
     */
    public boolean isEnableStrictValidation() {
        return enableStrictValidation;
    }

    /**
     * @param enableStrictValidation
     *            the enableStrictValidation to set
     */
    public void setEnableStrictValidation(boolean enableStrictValidation) {
        this.enableStrictValidation = enableStrictValidation;
    }

    /**
     * @return the createRecordsGroupMap
     */
    public boolean isCreateRecordsGroupMap() {
        return createRecordsGroupMap;
    }

    /**
     * @param createRecordsGroupMap
     *            the createRecordsGroupMap to set
     */
    public void setCreateRecordsGroupMap(boolean createRecordsGroupMap) {
        this.createRecordsGroupMap = createRecordsGroupMap;
    }

    /**
     * @return the runPostImportSuccess
     */
    public boolean isRunPostImportSuccess() {
        return runPostImportSuccess;
    }

    /**
     * @param runPostImportSuccess
     *            the runPostImportSuccess to set
     */
    public void setRunPostImportSuccess(boolean runPostImportSuccess) {
        this.runPostImportSuccess = runPostImportSuccess;
    }

    public String getInvalidateCacheRepositoryPath() {
        return invalidateCacheRepositoryPath;
    }

    public void setInvalidateCacheRepositoryPath(String invalidateCacheRepositoryPath) {
        this.invalidateCacheRepositoryPath = invalidateCacheRepositoryPath;
    }

    /**
     * @return the runPostProcessBatchImportSuccess
     */
    public boolean isRunPostProcessBatchImportSuccess() {
        return runPostProcessBatchImportSuccess;
    }

    /**
     * @param runPostProcessBatchImportSuccess
     *            the runPostProcessBatchImportSuccess to set
     */
    public void setRunPostProcessBatchImportSuccess(boolean runPostProcessBatchImportSuccess) {
        this.runPostProcessBatchImportSuccess = runPostProcessBatchImportSuccess;
    }

    /**
     * @return the insertUnavailableItems
     */
    public boolean isInsertUnavailableItems() {
        return insertUnavailableItems;
    }

    /**
     * @param insertUnavailableItems
     *            the insertUnavailableItems to set
     */
    public void setInsertUnavailableItems(boolean insertUnavailableItems) {
        this.insertUnavailableItems = insertUnavailableItems;
    }

    private static final String EXECUTE_IMPORT_METHOD = "executeImportMethod";

    /**
     * @return the threadPool
     */
    public CommonThreadPool getThreadPool() {
        return threadPool;
    }

    /**
     * @param threadPool
     *            the threadPool to set
     */
    public void setThreadPool(CommonThreadPool threadPool) {
        this.threadPool = threadPool;
    }

    /**
     * @return the maxThreads
     */
    public int getMaxThreads() {
        return maxThreads;
    }

    /**
     * @param maxThreads
     *            the maxThreads to set
     */
    public void setMaxThreads(int maxThreads) {
        this.maxThreads = maxThreads;
    }

    /**
     * @return the multiThreaded
     */
    public boolean isMultiThreaded() {
        return multiThreaded;
    }

    /**
     * @param multiThreaded
     *            the multiThreaded to set
     */
    public void setMultiThreaded(boolean multiThreaded) {
        this.multiThreaded = multiThreaded;
    }

    /**
     * @return the noOfColumns
     */
    public int getNoOfColumns() {
        return noOfColumns;
    }

    /**
     * @param noOfColumns
     *            the noOfColumns to set
     */
    public void setNoOfColumns(int noOfColumns) {
        this.noOfColumns = noOfColumns;
    }

    /**
     * @return the usePredefinedKey
     */
    public boolean isUsePredefinedKey() {
        return usePredefinedKey;
    }

    /**
     * @param usePredefinedKey
     *            the usePredefinedKey to set
     */
    public void setUsePredefinedKey(boolean usePredefinedKey) {
        this.usePredefinedKey = usePredefinedKey;
    }

    /**
     * @return the importHelper
     */
    public AbstractImportServiceHelper getImportHelper() {
        return importHelper;
    }

    /**
     * @param importHelper
     *            the importHelper to set
     */
    public void setImportHelper(AbstractImportServiceHelper importHelper) {
        this.importHelper = importHelper;
    }

    /**
     * There could be some properties that are read from the CSV and populated into the DynamicRecordItem initially, but then the value of that would be taken
     * to derived additional properties. so, the initially loaded property may not be needed. Either remove that property from DynamicRecordItem or add it to
     * the list below in the respective configuration file.
     */

    /**
     * @return the skipDynamicPropertiesForRepositoryItem
     */
    public List<String> getSkipDynamicPropertiesForRepositoryItem() {
        return skipDynamicPropertiesForRepositoryItem;
    }

    /**
     * @param skipDynamicPropertiesForRepositoryItem
     *            the skipDynamicPropertiesForRepositoryItem to set
     */
    public void setSkipDynamicPropertiesForRepositoryItem(List<String> skipDynamicPropertiesForRepositoryItem) {
        this.skipDynamicPropertiesForRepositoryItem = skipDynamicPropertiesForRepositoryItem;
    }

    /**
     * @return the populateAdditionalDataUsingPrePopulate
     */
    public boolean isPopulateAdditionalDataUsingPrePopulate() {
        return populateAdditionalDataUsingPrePopulate;
    }

    /**
     * @param populateAdditionalDataUsingPrePopulate
     *            the populateAdditionalDataUsingPrePopulate to set
     */
    public void setPopulateAdditionalDataUsingPrePopulate(boolean populateAdditionalDataUsingPrePopulate) {
        this.populateAdditionalDataUsingPrePopulate = populateAdditionalDataUsingPrePopulate;
    }

    /**
     * @return the mandatoryRecordColumns
     */
    public List<String> getMandatoryRecordColumns() {
        return mandatoryRecordColumns;
    }

    /**
     * @param mandatoryRecordColumns
     *            the mandatoryRecordColumns to set
     */
    public void setMandatoryRecordColumns(List<String> mandatoryRecordColumns) {
        this.mandatoryRecordColumns = mandatoryRecordColumns;
    }

    /**
     * @return the processedItemDescriptor
     */
    public String getProcessedItemDescriptor() {
        return processedItemDescriptor;
    }

    /**
     * @param processedItemDescriptor
     *            the processedItemDescriptor to set
     */
    public void setProcessedItemDescriptor(String processedItemDescriptor) {
        this.processedItemDescriptor = processedItemDescriptor;
    }

    public CacheInvalidationManager getCacheInvalidationManager() {
        return cacheInvalidationManager;
    }

    public void setCacheInvalidationManager(CacheInvalidationManager cacheInvalidationManager) {
        this.cacheInvalidationManager = cacheInvalidationManager;
    }
    /**
     * Get the project name and append the current date
     *
     * @return projectName
     */
    @Override
    public String getProjectName() {
        return super.getProjectName() + "--" + getBatchNumber() + "~~" + new Date().toString();
    }

    /**
     * start import method is starting point of class. invoke startImport method in dyn/admin
     */

    @Override
    public boolean startImport() {
        long startTime = System.currentTimeMillis();
        boolean success = true;
        setSomeRecordsFailedValidation(false); // set it to false everytime an import starts.in the validate method if there are failed validations,
        // this will be set to true.
        setFilesFinalDestinationFolderName(getProcessedFolderName());
        boolean lockFileExists = preImport();
        if (lockFileExists) {
            super.startImport();// to initialize the feed logger
            try {
                vlogDebug("startImport method start");
                if (getImportHelper() != null) {
                    getImportHelper().setFeedProcessingLogger(getFeedProcessingLogger());
                }
                CSVParser csvParser = getParser();

                if (csvParser == null) {
                    vlogError("the parser is null. Feed file not found");
                    return false;
                }
                vlogDebug("csv parser :: {0}", csvParser.getRecordNumber());
                List<DynamicRecordItem> dynamicRecordListFull = populateDynamicRecordItems(csvParser);

                String logText = MessageFormat.format("{0} RecordList  size @ start import :: {1}", getFeedName(), dynamicRecordListFull.size());
                vlogDebug(logText);
                getFeedProcessingLogger().vlogFeed(FeedConstants.FEED_INFO, logText);
                // close the parser and release the feed file, since the data has been populated into List of DynamicRecordItem objects
                try {
                    if (csvParser != null) {
                        csvParser.close();
                    }
                } catch (IOException ioe) {
                    logError("Error while closing the parser :: {0}", ioe);
                }

                if (isSwitchingImport()) {
                    // import into shadow datasource - sleeping catalog
                    success = importDataFromDynamicRecordList(dynamicRecordListFull);
                    // create a dummy project and deploy the same so we can mimic a catalog switch
                    if (success) {
                        Process process = createProcess();
                        advanceWorkflow(process);
                        // import into the previously live now switched to shadow datasource (by the dummy deploy above)
                        success = importDataFromDynamicRecordList(dynamicRecordListFull);
                    }
                } else {
                    success = importDataFromDynamicRecordList(dynamicRecordListFull);
                }

                if (isRunPostImportSuccess()) {
                    postImportSuccess();
                }
                getFeedProcessingLogger().vlogFeed(FeedConstants.FEED_INFO, "import completed successfully");

                if (isSomeRecordsFailedValidation()) {
                    setFilesFinalDestinationFolderName(getErrorFolderName());
                } else {
                    // set the log file destination as processed folder since there are no errors processing the feed.
                    setFilesFinalDestinationFolderName(getProcessedFolderName());
                }
                getCacheInvalidationManager().invalidateCache(getInvalidateCacheRepositoryPath());

                // success = true;
            } catch (Exception ee) {
                ee.printStackTrace();
                getFeedProcessingLogger().vlogFeed(FeedConstants.FEED_ERROR, ee.getMessage());
                setFilesFinalDestinationFolderName(getErrorFolderName());
                success = false;
                vlogError("errors occurred {0}", ee.getMessage());
                vlogError(ee, "errors occurred");

            } finally {
                try {
                    getFeedProcessingLogger().closeAndCopyLogFile(getFilesFinalDestinationFolderName());
                    copyFeedFileToFinalDestination();
                    File lockFile = new File(getLockFilePath());
                    if (lockFile.exists()) {
                        lockFile.delete();
                    }
                } catch (IOException e) {
                    vlogError("error copying the log file " + e);
                }
            }
            if (getErrorFolderName().equals(getFilesFinalDestinationFolderName())) {
                success = false;
            }
            long endTime = System.currentTimeMillis();
            System.out.println("import completed in " + (endTime - startTime));
        } else {
            vlogError("Lock file configured but not present.So cannot process feed file");
            return false;
        }
        return success;
    }

    protected boolean importDataFromDynamicRecordList(List<DynamicRecordItem> dynamicRecordListFull) {
        boolean success = true;

        Map<String, ArrayList<DynamicRecordItem>> sortedItems = new HashMap<>();
        if (isCreateRecordsGroupMap()) {
            sortedItems = createRecordGroupsByInputValue(dynamicRecordListFull);
        }
        int counter = 0;
        int importItemCount = dynamicRecordListFull.size();
        int batchStart = 0;
        int batchEnd = 0;
        while (counter < importItemCount) {
            batchNumber++;
            batchStart = counter;
            batchEnd = counter + getNoOfitemsPerProject();
            if (batchEnd >= importItemCount) {
                batchEnd = importItemCount;
            }
            vlogInfo("Batch number :: " + batchNumber);
            vlogInfo("batch Start => " + batchStart + " %%%%   batch End =>" + batchEnd);

            // Assign sub list to instance level list
            // List inProcessRecordList = dynamicRecordListFull.subList(batchStart, batchEnd);
            List inProcessRecordList = getSubListForBatch(dynamicRecordListFull, batchStart, batchEnd, sortedItems);
            TransactionDemarcation tDem = new TransactionDemarcation();
            TransactionManager tMgr = null;

            boolean rollback = true;
            try {
                tMgr = getFeedUtil().getTransactionManager();
                tDem.begin(tMgr, TransactionDemarcation.REQUIRES_NEW);
                importData(inProcessRecordList);
                // increment the counter
                // counter = counter + noOfitemsPerProject;
                counter = counter + inProcessRecordList.size();
                vlogInfo("startImport method batch end for count {0} ", counter);
                rollback = false;
            } catch (TransactionDemarcationException tde) {
                vlogError("Transaction Error occured while importing price batch");
                success = false;
            } catch (Exception e) {
                vlogError("Error occured while importing price batch");
                success = false;
            }

            finally {
                try {
                    tDem.end(rollback);
                } catch (TransactionDemarcationException tde) {
                    vlogError(tde, "Exception occured while commiting/rolling back transaction demarcation");
                }
            }
        }
        return success;
    }

    /**
     * if need be, create the input records into a map based on some property of the input values
     *
     * @param dynamicRecordListFull
     * @return
     */
    protected Map<String, ArrayList<DynamicRecordItem>> createRecordGroupsByInputValue(List<DynamicRecordItem> dynamicRecordListFull) {

        return null;
    }

    public void postImportSuccess() throws FeedProcessException {
        // override if needed to create any post import business logic
        vlogInfo("inside post import success");
    }

    protected List getSubListForBatch(List<DynamicRecordItem> fullList, int batchStart, int batchEnd, Map<String, ArrayList<DynamicRecordItem>> groupedMap) {
        // Assign sub list to instance level list
        // may be overridden by child classes to change the batch size logic.

        return fullList.subList(batchStart, batchEnd);

    }

    protected List<DynamicRecordItem> populateDynamicRecordItems(CSVParser csvParser) throws FeedProcessException {

        vlogDebug("populate map method start for record {0}", getFeedName());
        Map additionalRelatedValues = prePopulateRelatedValuesNotInFeed();
        List<DynamicRecordItem> dynamicRecordsItemList = new ArrayList<>();
        // List<DynamicRecordItem> dynamicRecordsItemList = new ArrayList<>();
        boolean allRecordsValid = true;
        
        try {
            for (CSVRecord record : csvParser.getRecords()) {
                vlogDebug("CSVRecord {0}", record);
                String recordInvalidErrorMessage = validateRecord(record);
                if (recordInvalidErrorMessage == null || recordInvalidErrorMessage.length() < 1) {
                    // feedProcessingLogger.vlogFeed(FeedConstants.FEED_INFO, "read the record successfully {0}", record.get(0));
                    DynamicRecordItem recordItem = instantiateDynamicRecordItem(record);
                    if (populateAdditionalDataUsingPrePopulate) {
                        if (additionalRelatedValues != null) {
                            populateAdditionalPropertiesNotInThisFeed(recordItem, additionalRelatedValues);
                        }
                    } else {
                        // if the additional values need not be populated via the prePopulate method
                        // but directly by this method, the above flag will be turned to false
                        populateAdditionalPropertiesNotInThisFeed(recordItem, additionalRelatedValues);
                    }
                    dynamicRecordsItemList.add(recordItem);
                } else {
                    // log error messages here
                    vlogDebug("record is invalid with reason {0}", recordInvalidErrorMessage);
                    feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, " {0} record is invalid with reason {1}", record.get(0), recordInvalidErrorMessage);
                    if (!recordInvalidErrorMessage.contains(FeedConstants.IGNORE)) {
                    allRecordsValid = false;
                    setSomeRecordsFailedValidation(true);
                    setFilesFinalDestinationFolderName(getErrorFolderName());
                    } else {
                        // the validation message must be ignored and the feed file should still go to success folder.
                        allRecordsValid = false;
                        setFilesFinalDestinationFolderName(getProcessedFolderName());
                    }
                }

            }
            if (isEnableStrictValidation()) {
                if (!allRecordsValid) {
                    throw new FeedProcessException("some records failed validation. check the log file ");
                }
            }
            // add additional dynamic record items itself to the list.
            dynamicRecordsItemList = postPopulateDynamicRecords(dynamicRecordsItemList);

        } catch (NumberFormatException | IOException | FeedProcessException ex) {
            setFilesFinalDestinationFolderName(getErrorFolderName());
            vlogError("Error while getting parser records :: for record {0}  ", ex.getMessage());
            logError(ex);
            throw new FeedProcessException("some records failed parsing. ");
        }
        vlogDebug("populate map method for record {0}  completed", getFeedName());

        return dynamicRecordsItemList;
    }

    /**
     * @param dynamicRecordsItemList
     * @return
     */
    protected List<DynamicRecordItem> postPopulateDynamicRecords(List<DynamicRecordItem> dynamicRecordsItemList) throws FeedProcessException {
        // this method is to make additional dynamic record from those populated from the CSV.
        // for example, in the MediaImportService, based on the product code and color code, five different media images have to
        // be created. at the minimum this method must return the same list that is passed as the argument.

        vlogDebug("nothing to do here. returning the passed list ");
        return dynamicRecordsItemList;
    }

    /**
     * @return
     *
     *         To be implemented by sub classes to load additional related data that is not part of this feed file. For example, SizeChartImportService will
     *         load the size_sequence file into a Map with size_chart_id as the key.
     */
    protected Map prePopulateRelatedValuesNotInFeed() throws FeedProcessException {
        vlogDebug("nothing to add additionally. default implementation");

        return null;

    }

    /**
     * @param recordItem
     *
     *            There could be need for populating additional properties in the DynamicRecordItem with values that are not part of the feed but, could be got
     *            from any other additional feed files or could be derived from the values given in the feed file. This method has to be implemented by the sub
     *            classes to add those additional values into the DynamicRecordItem. If there are no additional values, then simply have a log statement and
     *            return the same recordItem that is being passed.
     * @throws FeedProcessException
     */
    protected DynamicRecordItem populateAdditionalPropertiesNotInThisFeed(DynamicRecordItem recordItem, Map additionalRelatedRecordValues)
                    throws FeedProcessException {
        vlogDebug("no additional properties. hence returing the same record item");
        return recordItem;
    }

    private DynamicRecordItem instantiateDynamicRecordItem(CSVRecord record) {

        vlogInfo("inside instantiateDynamicRecordItem {0}", record);

        Set recordHeadersSet = recordToItemPropertiesMap.keySet();

        Iterator<String> recordHeaderIter = recordHeadersSet.iterator();
        DynamicRecordItem recordItem = new DynamicRecordItem();
        while (recordHeaderIter.hasNext()) {
            String recordKey = recordHeaderIter.next();
            String repoItemPropName = recordToItemPropertiesMap.get(recordKey);
            if(!(org.apache.commons.lang.StringUtils.trim(record.get(recordKey)).isEmpty())){
            recordItem.setRecordValue(repoItemPropName, org.apache.commons.lang.StringUtils.trim(record.get(recordKey)));
            recordItem.setRecordIndex(record.getRecordNumber());
            }
        }
        recordItem.setItemId(record.get(getItemIdParamName()));
        return recordItem;
    }

    /**
     *
     *
     * This method will populate the data from the DynamicRecordItem into the Repository item
     *
     * @throws FeedProcessException
     */

    public boolean populateDataFromDynamicRecord(MutableRepositoryItem mutableRepositoryItem, CatalogItem catalogItem, boolean itemInserted)
                    throws FeedProcessException {
        // the boolean flag is used to determine whether the populate method is being fired for a new item being added from the CSV
        // or existing item is updated. for example, when a product is added newly, its overrideShowOnSite should be set to '0'
        // when the enrichment is being fed, it will be turned to '1'
        vlogInfo("populateData method START");
        DynamicRecordItem recordItem = (DynamicRecordItem) catalogItem;
        boolean recordProcessStatus = true;
        vlogDebug("Item id :: {0}", mutableRepositoryItem.getRepositoryId());
        vlogDebug("dynamic record item values are  :: {0}", catalogItem);

        // mutableRepositoryItem.setPropertyValue("id", recordItem.getItemId());

        Iterator<String> repoItemPropIter = recordItem.getValues().keySet().iterator();
        while (repoItemPropIter.hasNext()) {
            String thisKey = repoItemPropIter.next();
            vlogDebug("this key is {0}", thisKey);
            if (!skipDynamicPropertiesForRepositoryItem.contains(thisKey)) {
                try {
                    Object processedValue = getProcessedValue(recordItem, thisKey);
                    mutableRepositoryItem.setPropertyValue(thisKey, processedValue);
                } catch (FeedProcessException fe) {
                    vlogInfo("error in processing input value for {0}", thisKey);
                    feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "error in processing input value for {0}", thisKey);
                    recordProcessStatus = false;
                }
            }
        }
        vlogInfo("populateData method END");
        return recordProcessStatus;
    }

    protected Object getProcessedValue(DynamicRecordItem recordItem, String thisKey) throws FeedProcessException {// NOPMD
        String dataTypeDefinition = getRepositoryPropertyDataTypeMap().get(thisKey);

        // if the data type definition is representing a repository item, then the syntax would be
        // repositoryItem:<itemType>. for other types the second value would not be there. it would be
        // simply string,boolean etc., hence we are splitting and taking the first value here.

        String[] dataTypes = dataTypeDefinition.split(":");
        String dataType = dataTypes[0];
        Object value = recordItem.getRecordValue(thisKey);
        Object processedValue = null;
        switch (dataType.toLowerCase(Locale.getDefault())) {
        case "string":
            processedValue = String.valueOf(value);
            break;
        case "boolean":
            processedValue = BooleanUtils.toBoolean((String) value, "1", "0");
            break;
        case "date":
            SimpleDateFormat inputDateFormat = new SimpleDateFormat(getDateFormatPattern(), Locale.getDefault());
            try {
                processedValue = inputDateFormat.parse((String) value);
            } catch (ParseException e) {
                vlogError("passed value is not a valid date {0}", value);
                feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "passed value is not a valid {0}", "date");
            }
            break;
        case "long":
            try {
                processedValue = Long.parseLong((String) value);
            } catch (NumberFormatException nfe) {
                processedValue = Long.valueOf(0);
                feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "passed value is not a valid {0}", "date");

            }
            break;
        case "double":
            try {
                processedValue = Double.parseDouble((String) value);
            } catch (NumberFormatException nfe) {
                processedValue = Double.valueOf(0.0);
            }
            break;
        case "integer":
            try {
                processedValue = Integer.parseInt((String) value);
            } catch (NumberFormatException nfe) {
                processedValue = Integer.valueOf(0);
            }
            break;
        case "repositoryitem":
            vlogDebug("implement the logic to get the repo item for the given ID {0}", value);
            String itemType = dataTypes[1];
            processedValue = getRelatedRepositoryItemValue(itemType, value);
            break;

        case "custom":
            processedValue = handleCustomDataType(thisKey, value);
            break;
        default:
            vlogDebug("implement the logic to get the other item types may be: not sure whether this is needed {0}", value);
            break;
        }
        return processedValue;
    }

    /**
     * @param value
     * @return This method must be implemented properly by the sub classes to handle their own custom data type. This has been given in a broad manner to
     *         accommodate any specific business logic of populating the MutableRepository Item before insert/update is attempted
     */
    protected Object handleCustomDataType(String thisKey, Object value) throws FeedProcessException {
        vlogDebug("this method must be implemented by sub classes in case custom data has to be added");
        return null;
    }

    /**
     * the implementing classes will either implement this method and return the actual repository item which has to be related to the current repository item
     * that is being handled via feed. for example, in a product feed, the parentcategory Id/some other attribute could be coming up as a record property. in
     * that case, this method would be overridden to return the category repository item for the given Id/other attribute. The default implementation give here
     * would be sufficient for most of the cases. only when the repository is different or instead of ID, some other field comes in the feed,then the
     * implementing class has to override this method and use RQL or anything appropriate.
     */
    protected Object getRelatedRepositoryItemValue(String itemType, Object itemId) throws FeedProcessException {
        RepositoryItem thisItem = null;
        if (itemId != null) {
            try {
                thisItem = getImportRepository().getItem((String) itemId, itemType);
            } catch (RepositoryException re) {
                vlogError("some errors occurred retrieving item {0}", re);
                feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "errors occurred getting related repo items ");
                throw new FeedProcessException(re.getMessage()); // NOPMD
            }
        }
        return thisItem;
    }

    /**
     * set the sku value
     *
     * @param mutableRepositoryItem
     * @param sku
     */
    @Override
    public boolean populateData(MutableRepositoryItem mutableRepositoryItem, CatalogItem catalogItem) {
        // to be implemented
        return true;
    }

    /**
     * Get the value from csv file
     *
     * @param parser
     */
    @Override
    public void executeImport(List inProcessRecordList) throws FeedProcessException {
        PerformanceMonitor.startOperation(EXECUTE_IMPORT_METHOD);
        vlogDebug("import Data start");

        if (isMultiThreaded()) {
            // decide how many thread to create
            int threadCounts = inProcessRecordList.size() / 10;
            if (threadCounts >= this.maxThreads && isMultiThreaded()) {
                threadCounts = this.maxThreads;
            } else {
                threadCounts = 1;
            }
            if (threadCounts < 1) {
                threadCounts = 1;
            }
            ThreadedExecutor executor = new ThreadedExecutor();
            LinkedList<DynamicRecordItem> subList = new LinkedList<>(inProcessRecordList);
            executor.setItemQueue(subList);
            executor.setMutableRepository(getImportRepository());
            Future[] futures = new Future[threadCounts];
            for (int i = 0; i < threadCounts; i++) {
                futures[i] = threadPool.getThreadPool().submit(executor);
            }
            for (int i = 0; i < threadCounts; i++) {
                try {
                    futures[i].get();

                } catch (InterruptedException | ExecutionException e) {
                    vlogError("errors occurred while waiting for thread to complete {0}", futures[i]);
                    logError(e);
                }
            }
            Vector statuses = executor.getStatuses();

            if (!statuses.isEmpty()) {
                throw new FeedProcessException(statuses.toString());
            }

        } else {
            try {
                for (Object thisRecord : inProcessRecordList) {
                    DynamicRecordItem currentProcessedRecord = (DynamicRecordItem) thisRecord;
                    MutableRepository mutableRepository = getImportRepository();
                    String currentProcessedId = currentProcessedRecord.getItemId();
                    String descriptorOfThisItem = getItemDescriptorNameForProcessing();

                    RepositoryItem currentProcessedItem = lookupRepositoryItem(mutableRepository, descriptorOfThisItem, currentProcessedRecord);
                    RepositoryItem currentItem = null;
                    vlogDebug("Current Processed Item :: {0} and id :: {1}", currentProcessedItem, currentProcessedId);
                    // insertUnavailableItems would be true by default. for feeds like ProductPrecedenceImportService,
                    // we need to turn it off to false since that feed might contain product Ids that are not there in the catalog
                    // and an attempt to insert them should not be done
                    if (currentProcessedItem == null) {
                        if (insertUnavailableItems) {
                            vlogDebug("inserting unavailable item {0}", currentProcessedRecord);
                            currentItem = insertItemFromDynamicRecord(mutableRepository, descriptorOfThisItem, currentProcessedRecord, isUsePredefinedKey());
                        } else {
                            feedProcessingLogger.vlogFeed(FeedConstants.FEED_INFO, "record not available and not created based on flag {0}",
                                            currentProcessedRecord.getItemId());
                        }
                    } else {
                        currentItem = updateItemFromDynamicRecord(mutableRepository, descriptorOfThisItem, currentProcessedRecord);
                    }
                    if (currentItem != null) {
                        createRelatedRepositoryItemsIfAny(currentProcessedRecord, currentItem);
                    }
                }
            } catch (RepositoryException re) {
                vlogError("Error while retrieving repository item :: {0}", re.getMessage());
                feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "repository exception: " + re.getMessage());
                logError(re);
                throw new FeedProcessException("repository exception creating/updating data: " + re.getMessage());
            }
        }
        if (isRunPostProcessBatchImportSuccess()) {
            postProcessBatchImportSuccess(inProcessRecordList);
        }
        vlogDebug("import Data end");
        PerformanceMonitor.endOperation(EXECUTE_IMPORT_METHOD);
    }

    /**
     * do something after the one batch of import is complete. would be useful for import service like inventory import service where the batch would be
     * determined by the storeID
     *
     * @throws FeedProcessException
     */
    public void postProcessBatchImportSuccess(List inProcessRecordList) throws FeedProcessException {
        vlogInfo("inside post process of successful batch import. will befired after every batch of import");
    }

    /**
     * this method can be used to create/update additional related records to this current record item For example, for every media created, the media can be
     * set to the product/sku appropriately here easily by retrieving that product/sku and setting this current record. The two arguments passed here are the
     * current repositoryItem that is being updated and the current dynamic record. so from the dynamic record the other related repository items can be
     * created/updated with the current repository item.
     *
     * @param currentProcessedRecord
     */
    protected void createRelatedRepositoryItemsIfAny(DynamicRecordItem currentProcessedRecord, RepositoryItem currentItem)
                    throws RepositoryException, FeedProcessException {
        vlogDebug("this method can be used to create/update additional related records to this current record item ");

    }

    /**
     * @return
     *
     *         This method has to be overwritten by the derived classes which handle specific feed. it is their responsibility to return the correct item
     *         descriptor for insert/update
     */
    protected abstract String getItemDescriptorNameForProcessing();

    /**
     *
     * @param mutableRepository
     * @param itemDesc
     * @param item
     *
     *            This method will create a new MutableRepositoryItem with the specified ID rather than using ID generator
     * @throws FeedProcessException
     */
    public RepositoryItem insertItemFromDynamicRecord(MutableRepository mutableRepository, String itemDesc, CatalogItem item, boolean usePredefinedKey)
                    throws FeedProcessException {

        vlogDebug("insert item start");
        boolean success;
        MutableRepositoryItem mutItem = null;

        try {
            if (usePredefinedKey) {
                mutItem = mutableRepository.createItem(item.getItemId(), itemDesc);
            } else {
                mutItem = mutableRepository.createItem(itemDesc);
            }
            success = populateDataFromDynamicRecord(mutItem, item, true);
            if (success) {
                mutableRepository.addItem(mutItem);
                feedProcessingLogger.vlogFeed(FeedConstants.FEED_INFO, "added record successfully {0}", item.getItemId());
            } else {
                vlogError("Skipping record with id :: {0} due to missing related data", item.getItemId());

                feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "Skipping record with id :: {0} due to missing related data", item.getItemId());
                throw new FeedProcessException("error populating data " + item.getItemId());
            }
        } catch (RepositoryException re) {
            vlogError("Error while inserting item :: {0}", re.getMessage());
            feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "Skipping record with id :: {0} due to missing related data", item.getItemId());
            logError(re);
            throw new FeedProcessException("error populating data " + item.getItemId());
        }
        return mutItem;
    }

    /**
     *
     * @param repository
     * @param itemDesc
     * @param item
     * @throws FeedProcessException
     */
    public RepositoryItem updateItemFromDynamicRecord(MutableRepository mutableRepository, String itemDesc, CatalogItem item) throws FeedProcessException {

        vlogDebug("update item start-----");
        boolean success;
        MutableRepositoryItem mutItem = null;

        try {
            mutItem = mutableRepository.getItemForUpdate(item.getItemId(), itemDesc);
            success = populateDataFromDynamicRecord(mutItem, item, false);
            if (success) {
                mutableRepository.updateItem(mutItem);
                feedProcessingLogger.vlogFeed(FeedConstants.FEED_INFO, "record updated successfully {0}", item.getItemId());
            } else {
                vlogError("Skipping updating the record with id :: {0} due to missing data", item.getItemId());
                feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "Skipping record with id :: {0} ", item.getItemId());
                throw new FeedProcessException("error updating  data " + item.getItemId());
            }
        } catch (RepositoryException re) {
            vlogError("Error while updating item::{0}", re.getMessage());
            feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "Skipping updating record with id :: {0} due to missing related data or exception - {1}",
                            item.getItemId(), re.getMessage());
            logError(re);

            throw new FeedProcessException("error updating data " + item.getItemId());
        }

        vlogDebug("update item END");
        return mutItem;
    }

    /**
     *
     * @param record
     * @return
     *
     *         This method has to be overridden by the respective Import Service to handle input record validation as needed by their requirement. The base
     *         implementation just checks whether the mandatory values are available.
     */

    protected String validateRecord(CSVRecord record) throws FeedProcessException {
        StringBuilder sBuilder = new StringBuilder();
        if (record.size() == noOfColumns) {
            // feedProcessingLogger.vlogFeed(FeedConstants.FEED_INFO, "processed the record for {0}", record);
            List<String> errorColumn=new ArrayList<String>();
            for (String recordColumn : mandatoryRecordColumns) {
                String value = null;
                try {
                    value = record.get(recordColumn);
                } catch (Exception ee) {
                    // actually either IllegalStateException || IllegalArgumentException would be thrown if the current Record is not proper
                    feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "unable to retrieve the value for column {0}", recordColumn);
                    logError(ee);
                }
                if (value == null || value.length() < 1) {
                  //  sBuilder.append(" [validation Failed for column: ] ").append(recordColumn).append(" for record -").append(record.getRecordNumber());
                    feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, sBuilder.toString());
                    errorColumn.add(recordColumn);
                    //throw new FeedProcessException(sBuilder.toString());
                }
            }
            if(!errorColumn.isEmpty()){
        	getErrorProducts().put(record.get("IMITM"),errorColumn);
            }
            
            // feedProcessingLogger.vlogFeed(FeedConstants.FEED_INFO, "processed the row for {0}", record.get(0));

        } else {
            // check here for the availability of minimum number of columns in the record
            // this minimum number must be configurable for different feeds.
            // this is needed,otherwise if even one record is corrupted, then the entire processing will fail.

            sBuilder.append("record is not having the desired number of columns - ").append(noOfColumns).append('-').append(record.get(0));
            getFeedProcessingLogger().vlogFeed(FeedConstants.FEED_ERROR, sBuilder.toString());
            throw new FeedProcessException(sBuilder.toString());
        }

        return sBuilder.toString();
    }


    public Map<String, List<String>> getErrorProducts() {
        return errorProducts;
    }

    public void setErrorProducts(Map<String, List<String>> errorProducts) {
        this.errorProducts = errorProducts;
    }

    /**
     * * Overloading the parent class method with a different argument by passing the DynamicRecordItem itself
     */

    public RepositoryItem lookupRepositoryItem(Repository repository, String itemDescriptor, DynamicRecordItem currentItem) throws RepositoryException {
        String feedId = currentItem.getItemId();
        return repository.getItem(feedId, itemDescriptor);
    }

    class ThreadedExecutor implements Runnable {
        private Queue<DynamicRecordItem> itemQueue;
        private MutableRepository mutableRepository;
        private Vector<String> statuses = new Vector<>();

        /**
         * @return the statuses
         */
        public Vector<String> getStatuses() {
            return statuses;
        }

        /**
         * @param statuses
         *            the statuses to set
         */
        public void setStatuses(Vector<String> statuses) {
            this.statuses = statuses;
        }

        /**
         * @return the mutableRepository
         */
        public MutableRepository getMutableRepository() {
            return mutableRepository;
        }

        /**
         * @param mutableRepository
         *            the mutableRepository to set
         */
        public void setMutableRepository(MutableRepository mutableRepository) {
            this.mutableRepository = mutableRepository;
        }

        /**
         * @return the itemQueue
         */
        public Queue<DynamicRecordItem> getItemQueue() {
            return itemQueue;
        }

        /**
         * @param itemQueue
         *            the itemQueue to set
         */
        public void setItemQueue(Queue<DynamicRecordItem> itemQueue) {
            this.itemQueue = itemQueue;
        }

        /**
         * override the run() method to provide the logic to be executed.
         *
         */
        @Override
        public void run() {
            DynamicRecordItem currentProcessedRecord = null;
            while (true) {
                synchronized (itemQueue) {
                    currentProcessedRecord = itemQueue.poll();
                }
                if (currentProcessedRecord == null) {
                    break;
                }

                String currentProcessedId = currentProcessedRecord.getItemId();
                String descriptorOfThisItem = getItemDescriptorNameForProcessing();

                try {
                    RepositoryItem currentProcessedItem = lookupRepositoryItem(mutableRepository, descriptorOfThisItem, currentProcessedRecord);
                    RepositoryItem currentItem = null;
                    vlogDebug("Current Processed Item :: {0} and id :: {1}", currentProcessedItem, currentProcessedId);
                    if (currentProcessedItem == null) {
                        if (insertUnavailableItems) {
                            currentItem = AbstractDynamicRecordImportService.this.insertItemFromDynamicRecord(mutableRepository, descriptorOfThisItem,
                                            currentProcessedRecord, isUsePredefinedKey());
                        }
                    } else {
                        currentItem = AbstractDynamicRecordImportService.this.updateItemFromDynamicRecord(mutableRepository, descriptorOfThisItem,
                                        currentProcessedRecord);
                    }

                    createRelatedRepositoryItemsIfAny(currentProcessedRecord, currentItem);

                } catch (RepositoryException re) {
                    statuses.addElement(re.getMessage());
                    vlogError("Error while retrieving  item :: {0}", re.getMessage());
                    feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "repository exception creating/updating data: " + re.getMessage());
                    logError(re);
                    // throw new FeedProcessException("repository exception creating related data ");
                } catch (FeedProcessException fe) {
                    statuses.addElement(fe.getMessage());
                    vlogError("Error while populating item :: {0}", fe.getMessage());
                    feedProcessingLogger.vlogFeed(FeedConstants.FEED_ERROR, "repository exception creating/updating data: " + fe.getMessage());
                    logError(fe);
                }

            }

        }

    }

}
