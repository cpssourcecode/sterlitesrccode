/**
 *
 */
package com.cps.dataload.exception;

/**
 *
 */
public class FeedProcessException extends Exception {

    /**
     *
     */
    public FeedProcessException() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public FeedProcessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     * @param cause
     */
    public FeedProcessException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param message
     */
    public FeedProcessException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param cause
     */
    public FeedProcessException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
