package com.cps.dataload.schedule;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.cps.dataload.base.service.AbstractImportService;
import com.cps.email.CommonEmailSender;

import atg.nucleus.ServiceException;
import atg.service.scheduler.SchedulableService;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;

public class BaseFeedImportScheduler extends SchedulableService {

    private boolean enabled;
    private Scheduler scheduler;
    private Schedule schedule;
    private int jobId;
    private AbstractImportService importService;
    private boolean running;
    private CommonEmailSender emailManager;
    // private List<String> emailIds = new ArrayList();

    private String feedFileDir;
    private String fileExtension = ".txt";
    private String lockFileExtension = ".complete";
    private String environment;
    private String feedFileNamePrefix;

    /**
     * @return the feedFileNamePrefix
     */
    public String getFeedFileNamePrefix() {
        return feedFileNamePrefix;
    }

    /**
     * @param feedFileNamePrefix
     *            the feedFileNamePrefix to set
     */
    public void setFeedFileNamePrefix(String feedFileNamePrefix) {
        this.feedFileNamePrefix = feedFileNamePrefix;
    }

    /**
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * @param environment
     *            the environment to set
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    /**
     * @return the lockFileExtension
     */
    public String getLockFileExtension() {
        return lockFileExtension;
    }

    /**
     * @param lockFileExtension
     *            the lockFileExtension to set
     */
    public void setLockFileExtension(String lockFileExtension) {
        this.lockFileExtension = lockFileExtension;
    }

    /**
     * @return the feedFileDir
     */
    public String getFeedFileDir() {
        return feedFileDir;
    }

    /**
     * @param feedFileDir
     *            the feedFileDir to set
     */
    public void setFeedFileDir(String feedFileDir) {
        this.feedFileDir = feedFileDir;
    }

    /**
     * @return the fileExtension
     */
    public String getFileExtension() {
        return fileExtension;
    }

    /**
     * @param fileExtension
     *            the fileExtension to set
     */
    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }


    /**
     * @return the emailManager
     */
    public CommonEmailSender getEmailManager() {
        return emailManager;
    }

    /**
     * @param emailManager
     *            the emailManager to set
     */
    public void setEmailManager(CommonEmailSender emailManager) {
        this.emailManager = emailManager;
    }

    /**
     * @return the running
     */
    @Override
    public boolean isRunning() {
        return running;
    }

    /**
     * @param running
     *            the running to set
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * @return the importService
     */
    public AbstractImportService getImportService() {
        return importService;
    }

    /**
     * @param importService
     *            the importService to set
     */
    public void setImportService(AbstractImportService importService) {
        this.importService = importService;
    }

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     *            the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the scheduler
     */
    @Override
    public Scheduler getScheduler() {
        return scheduler;
    }

    /**
     * @param scheduler
     *            the scheduler to set
     */
    @Override
    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    /**
     * @return the schedule
     */
    @Override
    public Schedule getSchedule() {
        return schedule;
    }

    /**
     * @param schedule
     *            the schedule to set
     */
    @Override
    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    /**
     * @return the jobId
     */
    @Override
    public int getJobId() {
        return jobId;
    }

    /**
     * @param jobId
     *            the jobId to set
     */
    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    /**
     * Starting point of this class
     */
    @Override
    public void performScheduledTask(Scheduler scheduler, ScheduledJob scheduledJob) {
        if (isEnabled()) {
            startImport();

        }
    }

    /**
     * Create the ScheduledJob with job name, job description, etc..,
     */
    @Override
    public void doStartService() throws ServiceException {
        if (schedule != null & scheduler != null) {
            ScheduledJob thisJob = new ScheduledJob("price", "Import price", getAbsoluteName(), getSchedule(), this, ScheduledJob.REUSED_THREAD);
            jobId = getScheduler().addScheduledJob(thisJob);
        }
    }

    /**
     * Remove the ScheduledJob using jobId
     */
    @Override
    public void doStopService() throws ServiceException {
        getScheduler().removeScheduledJob(jobId);
    }

    /**
     * this method will trigger the configured Import service
     */
    public void startImport() {
        String subject = null;
        String emailBody = null;
        if (isRunning()) {
            vlogInfo("another instance already running ");
            subject = getSubjectForEmail("another instance already running");
            emailBody = "another instance already running";
            getEmailManager().sendFeedStatusEmailNotification(getEmailIds(), subject, emailBody);
            return;
        }

        boolean status = true;
        String feedStatus = "Success";
        String fullPath = null;
        String feedStatusItemId = null;
        try {
            File feedDir = new File(feedFileDir);
            if (feedDir.isDirectory()) {

                FilenameFilter filter = getFeedFileNameFilter();

                File[] feedFiles = feedDir.listFiles(filter);
                File[] lockFiles = feedDir.listFiles(getLockFileNameFilter());
                AbstractImportService importService = getImportService();

                if (ArrayUtils.isNotEmpty(feedFiles) && ArrayUtils.isNotEmpty(lockFiles)) {
                    importService.setLockFilePath(lockFiles[0].getAbsolutePath());
                    for (File feedFile : feedFiles) {
                        fullPath = feedFile.getAbsolutePath();
                        importService.setFeedFilePath(fullPath);
                        setRunning(true);
                        vlogInfo("start  import for type {0}", importService.getAbsoluteName());
                        setRunning(true);
                        subject = getSubjectForEmail(new StringBuilder().append("import started ").append(importService.getAbsoluteName()).toString()); // NOPMD
                        emailBody = new StringBuilder().append("start importing ").append(feedFile).toString(); // NOPMD

                        getEmailManager().sendFeedStatusEmailNotification(getEmailIds(), subject, emailBody);
                        status = importService.startImport();
                        subject = getSubjectForEmail(importService.getAbsoluteName() + "import status");
                        emailBody = getEmailBodyForStatus(status, feedFile, feedStatus);
                        getEmailManager().sendFeedStatusEmailNotification(getEmailIds(), subject, emailBody);
                    }
                } else {
                    vlogInfo("no files to process in the feed directory");
                    status = false;
                    emailBody = new StringBuilder().append("no files to import under ").append(feedDir).toString();
                    subject = getSubjectForEmail(importService.getAbsoluteName() + "import status");
                    getEmailManager().sendFeedStatusEmailNotification(getEmailIds(), subject, emailBody);
                }
            } else {
                vlogInfo("no such  feed directory - {0}", feedDir);
                status = false;
                emailBody = new StringBuilder().append("no feed directory ").append(feedDir).toString();
                subject = getSubjectForEmail(importService.getAbsoluteName() + "import status");
                getEmailManager().sendFeedStatusEmailNotification(getEmailIds(), subject, emailBody);
            }
        } catch (Exception ee) {
            vlogError("errors occurred in the Import Scheduler {0}", getAbsoluteName());
            logError(ee);

        } finally {
            setRunning(false);
        }
    }

    protected String getEmailBodyForStatus(boolean status, File feedFile, String feedStatus) {
        String subject;
        if (status) {
            subject = new StringBuilder().append("import of  ").append(feedFile).append(" completed successfully").toString(); // NOPMD
            feedStatus = "Success";
        } else {
            subject = new StringBuilder().append("import of  ").append(feedFile).append(" encountered an error. refer log file").toString(); // NOPMD
            feedStatus = "Failed";
        }
        return subject;
    }

    protected String getSubjectForEmail(String subject) {
        return new StringBuilder(getEnvironment()).append(subject).toString();
    }

    protected FilenameFilter getFeedFileNameFilter() {

        FilenameFilter filter = new FilenameFilter() {
            /**
             * true if and only if the name should be included in the file list; false otherwise.
             */
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(getFileExtension()) && name.startsWith(getFeedFileNamePrefix());
            }
        };
        return filter;

    }

    protected FilenameFilter getLockFileNameFilter() {

        FilenameFilter filter = new FilenameFilter() {
            /**
             * true if and only if the name should be included in the file list; false otherwise.
             */
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(getLockFileExtension());
            }
        };
        return filter;

    }

    private List<String> getEmailIds() {
        List<String> emailsIds = new ArrayList<>();
        String configuredEmaislIds = getEmailManager().getEmailForFeedStatusmails();
        if (configuredEmaislIds != null) {
            emailsIds = Arrays.asList(configuredEmaislIds.split("\\s*,\\s*"));
        }
        return emailsIds;
    }

}
