/**
 *
 */
package com.cps.dataload.helper;

import com.cps.dataload.exception.FeedProcessException;
import com.cps.dataload.logging.FeedProcessingLogger;
import com.cps.dataload.pricing.bean.DynamicRecordItem;

import atg.nucleus.GenericService;

/**
 * This is the base class for all ImportService helper classes. this will define a generic method for populating additional properties into the
 * DynamicRecordItem and return the same DynamicRecordItem.
 */
public abstract class AbstractImportServiceHelper extends GenericService {

    private FeedProcessingLogger feedProcessingLogger;

    /**
     * @return the feedProcessingLogger
     */
    public FeedProcessingLogger getFeedProcessingLogger() {
        return feedProcessingLogger;
    }

    /**
     * @param feedProcessingLogger
     *            the feedProcessingLogger to set
     */
    public void setFeedProcessingLogger(FeedProcessingLogger feedProcessingLogger) {
        this.feedProcessingLogger = feedProcessingLogger;
    }

    /**
     * this method would be implemented by the sub classes and their ImportService would pass any source object. it could be a RepositoryItem or any other
     * thing. the sub classes are free to do any modification by adding/updating/removing record properties in the DynamicRecordItem
     *
     * @param recorditem
     * @param source
     * @return
     * @throws FeedProcessException
     */
    public abstract DynamicRecordItem populateAdditionalPropertiesForRecordItem(DynamicRecordItem recordItem, Object source) throws FeedProcessException;
}
