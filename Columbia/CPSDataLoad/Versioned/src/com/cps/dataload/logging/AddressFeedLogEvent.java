/**
 *
 */
package com.cps.dataload.logging;

public class AddressFeedLogEvent extends FeedLogEvent {

    private String identifier = "**** feed ";

    /**
     * constructor to call the super class one
     *
     * @param pMessage
     */
    public AddressFeedLogEvent(String pMessage) {
        super(pMessage);
    }

    /**
     *
     * @param pMessage
     * @param pOriginator
     */
    public AddressFeedLogEvent(String pMessage, String pOriginator) {
        super(pMessage, pOriginator);
    }

    /**
     *
     * @param pMessage
     * @param pThrowable
     */
    public AddressFeedLogEvent(String pMessage, Throwable pThrowable) {
        super(pMessage, pThrowable);
    }

    /**
     *
     * @param pMessage
     * @param pOriginator
     * @param pThrowable
     */
    public AddressFeedLogEvent(String pMessage, String pOriginator, Throwable pThrowable) {
        super(pMessage, pOriginator, pThrowable);
    }

    /**
     *
     */
    @Override
    public String getIdentifier() {
        return identifier;
    }

    /**
     * set the log identifier that will go into the log file
     *
     * @param identifier
     *            the identifier to set
     */
    @Override
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

}
