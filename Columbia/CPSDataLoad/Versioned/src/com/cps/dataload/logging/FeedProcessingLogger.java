/**
 *
 */
package com.cps.dataload.logging;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.MessageFormat;

import com.cps.dataload.util.FilePermissionsHelper;

import atg.nucleus.GenericService;
import atg.nucleus.Nucleus;
import atg.nucleus.logging.DispatchLogger;
import atg.nucleus.logging.FileLogger;
import atg.nucleus.logging.LogListener;

/**
 *
 * @author Srilakshmi
 *
 */

public class FeedProcessingLogger extends GenericService {

    protected boolean loggingFeed = true;
    protected boolean errorProcessingFeed;

    protected FileLogger fileLogger;
    protected DispatchLogger dispatchLogger;
    protected FilePermissionsHelper filePermissionsHelper;

    /**
     * @return the filePermissionsHelper
     */
    public FilePermissionsHelper getFilePermissionsHelper() {
        return filePermissionsHelper;
    }

    /**
     * @param filePermissionsHelper
     *            the filePermissionsHelper to set
     */
    public void setFilePermissionsHelper(FilePermissionsHelper filePermissionsHelper) {
        this.filePermissionsHelper = filePermissionsHelper;
    }

    /**
     * @return the errorProcessingFeed
     */
    public boolean isErrorProcessingFeed() {
        return errorProcessingFeed;
    }

    /**
     * @param errorProcessingFeed
     *            the errorProcessingFeed to set
     */
    public void setErrorProcessingFeed(boolean errorProcessingFeed) {
        this.errorProcessingFeed = errorProcessingFeed;
    }

    /**
     * @return the loggingFeed
     */
    public boolean isLoggingFeed() {
        return loggingFeed;
    }

    /**
     * @param loggingFeed
     *            the loggingFeed to set
     */
    public void setLoggingFeed(boolean loggingFeed) {
        this.loggingFeed = loggingFeed;
    }

    /**
     * @return the fileLogger
     */
    public FileLogger getFileLogger() {
        return fileLogger;
    }

    /**
     * @param fileLogger
     *            the fileLogger to set
     */
    public void setFileLogger(FileLogger fileLogger) {
        this.fileLogger = fileLogger;
    }

    /**
     * @return the dispatchLogger
     */
    public DispatchLogger getDispatchLogger() {
        return dispatchLogger;
    }

    /**
     * @param dispatchLogger
     *            the dispatchLogger to set
     */
    public void setDispatchLogger(DispatchLogger dispatchLogger) {
        this.dispatchLogger = dispatchLogger;
    }

    /**
     * pass the message to the other logFeed method
     *
     * @param message
     */
    public void logFeed(String msgType, String message) {
        logFeed(msgType, message, null);
    }

    /**
     * pass the Throwable to the other logFeed method with a Null message
     *
     * @param throwable
     */
    public void logFeed(String msgType, Throwable throwable) {
        logFeed(msgType, null, throwable);
    }

    /**
     * create the logEvent and send it to the internal dispatcher. also set the log identifier. the same logger is used to identify info as well as error events
     *
     * @param message
     * @param throwable
     */
    public void logFeed(String msgType, String message, Throwable throwable) {
        if (isLoggingFeed()) {
            FeedLogEvent logEvent = new FeedLogEvent(message, getAbsoluteName(), throwable);
            logEvent.setIdentifier(msgType + " *** ");
            sendLogEvent(logEvent);
        }
    }

    /**
     * helper method to have a formatted string sent to the log file.
     *
     * @param unformattedStr
     * @param args
     */
    public void vlogFeed(String msgType, String unformattedStr, Object... args) {
        String formattedMessage = MessageFormat.format(unformattedStr, args);
        logFeed(msgType, formattedMessage);
    }

    /**
     * open the log file for the current Feed processing with given pattern. before that,close the old file if it is still open Also, add the current specific
     * Log component for this Feed processing into the LogDispatcher destination
     *
     * @throws IOException
     */
    public void initializeFeedLogger(String currentFeedFileName, String logPath) throws IOException {
        CPFileLogger fiLogger = (CPFileLogger) getFileLogger();
        fiLogger.close(); // in case the previous file is still open.
        // String logFileName = MessageFormat.format(fiLogger.getLogFileName(), currentFeedFileName);
        String logFileName = MessageFormat.format(fiLogger.getLogFilePattern(), currentFeedFileName);
        fiLogger.setLogFileName(logFileName);
        fiLogger.setLogFilePath(new File(logPath));
        fiLogger.open();
        Path logFilePath = fiLogger.getLogFilePath().toPath();

        FilePermissionsHelper fpHelper = getFilePermissionsHelper();
        if (fpHelper == null) {
            fpHelper = (FilePermissionsHelper) Nucleus.getGlobalNucleus().resolveName("/cps/dataload/util/FilePermissionsHelper");
        }
        fpHelper.setFilePermissions(logFilePath + "/" + logFileName);

        getDispatchLogger().setDefaultDestinations(new LogListener[] { fiLogger });

    }

    /**
     * close the log file and move it to the passed folder. if the process is successful, then the file will be transferred to the success folder. if the
     * processing fails, the file will be transferred to the error folder
     *
     * @param folder
     * @throws IOException
     */
    public void closeAndCopyLogFile(String folder) throws IOException {
        FileLogger fiLogger = getFileLogger();

        fiLogger.close();
        File currentFile = new File(fiLogger.getLogFilePath(), fiLogger.getLogFileName());
        Path sourcePath = currentFile.toPath();
        // File destDir = new File(fileLogger.getLogFilePath() + "/error");
        File destDir = new File(fiLogger.getLogFilePath() + folder);
        if (!destDir.isDirectory()) {
            destDir.mkdirs();
        }
        File destFile = new File(destDir, fiLogger.getLogFileName());
        Path destPath = destFile.toPath();
        Files.copy(sourcePath, destPath, StandardCopyOption.REPLACE_EXISTING);
        Files.deleteIfExists(sourcePath);
        getFilePermissionsHelper().setFilePermissions(destPath);
    }

}
