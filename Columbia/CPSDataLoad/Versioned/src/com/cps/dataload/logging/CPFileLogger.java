/**
 *
 */
package com.cps.dataload.logging;

import atg.nucleus.logging.FileLogger;

/**
 * An extension to the base class to provide a pattern to the log file name.
 *
 *
 */
public class CPFileLogger extends FileLogger {

    private String logFilePattern;

    /**
     * @return the logFilePattern
     */
    public String getLogFilePattern() {
        return logFilePattern;
    }

    /**
     * @param logFilePattern
     *            the logFilePattern to set
     */
    public void setLogFilePattern(String logFilePattern) {
        this.logFilePattern = logFilePattern;
    }

}
