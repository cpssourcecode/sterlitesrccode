/**
 *
 */
package com.cps.dataload.logging;

/**
 *
 * @author Srilakshmi
 *
 */

public class AddressFeedProcessingLogger extends FeedProcessingLogger {

    /**
     * create the logEvent and send it to the internal dispatcher. also set the log identifier. the same logger is used to identify info as well as error events
     *
     * @param message
     * @param throwable
     */
    @Override
    public void logFeed(String msgType, String message, Throwable throwable) {
        if (isLoggingFeed()) {
            AddressFeedLogEvent logEvent = new AddressFeedLogEvent(message, getAbsoluteName(), throwable);
            logEvent.setIdentifier(msgType + " *** ");
            sendLogEvent(logEvent);
        }
    }

}
