/**
 *
 */
package com.cps.dataload.logging;

/**  A custom Log Event object to handle our Feed events and log them separately into different configurable log files
 * @author R.Srinivasan
 *
 */
import atg.nucleus.logging.LogEvent;

public class FeedLogEvent extends LogEvent {

    private String identifier = "**** feed ";

    /**
     * constructor to call the super class one
     *
     * @param pMessage
     */
    public FeedLogEvent(String pMessage) {
        super(pMessage);
    }

    /**
     *
     * @param pMessage
     * @param pOriginator
     */
    public FeedLogEvent(String pMessage, String pOriginator) {
        super(pMessage, pOriginator);
    }

    /**
     *
     * @param pMessage
     * @param pThrowable
     */
    public FeedLogEvent(String pMessage, Throwable pThrowable) {
        super(pMessage, pThrowable);
    }

    /**
     *
     * @param pMessage
     * @param pOriginator
     * @param pThrowable
     */
    public FeedLogEvent(String pMessage, String pOriginator, Throwable pThrowable) {
        super(pMessage, pOriginator, pThrowable);
    }

    /**
     *
     */
    @Override
    public String getIdentifier() {
        return identifier;
    }

    /**
     * set the log identifier that will go into the log file
     *
     * @param identifier
     *            the identifier to set
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

}
