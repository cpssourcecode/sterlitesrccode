/**
 *
 */
package com.cps.dataload.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.util.HashSet;
import java.util.Set;

import atg.nucleus.GenericService;

/**
 * A class that is used to set the file permissions on the files that are created by the application. needed, because the application puts only the user and the
 * group permissions and for others it would be none.
 *
 *
 */
public class FilePermissionsHelper extends GenericService {

    private boolean groupRead;
    private boolean groupWrite;
    private boolean groupExecute;

    private boolean othersRead;
    private boolean othersWrite;
    private boolean othersExecute;

    /**
     * @return the groupRead
     */
    public boolean isGroupRead() {
        return groupRead;
    }

    /**
     * @param groupRead
     *            the groupRead to set
     */
    public void setGroupRead(boolean groupRead) {
        this.groupRead = groupRead;
    }

    /**
     * @return the groupWrite
     */
    public boolean isGroupWrite() {
        return groupWrite;
    }

    /**
     * @param groupWrite
     *            the groupWrite to set
     */
    public void setGroupWrite(boolean groupWrite) {
        this.groupWrite = groupWrite;
    }

    /**
     * @return the groupExecute
     */
    public boolean isGroupExecute() {
        return groupExecute;
    }

    /**
     * @param groupExecute
     *            the groupExecute to set
     */
    public void setGroupExecute(boolean groupExecute) {
        this.groupExecute = groupExecute;
    }

    /**
     * @return the othersRead
     */
    public boolean isOthersRead() {
        return othersRead;
    }

    /**
     * @param othersRead
     *            the othersRead to set
     */
    public void setOthersRead(boolean othersRead) {
        this.othersRead = othersRead;
    }

    /**
     * @return the othersWrite
     */
    public boolean isOthersWrite() {
        return othersWrite;
    }

    /**
     * @param othersWrite
     *            the othersWrite to set
     */
    public void setOthersWrite(boolean othersWrite) {
        this.othersWrite = othersWrite;
    }

    /**
     * @return the othersExecute
     */
    public boolean isOthersExecute() {
        return othersExecute;
    }

    /**
     * @param othersExecute
     *            the othersExecute to set
     */
    public void setOthersExecute(boolean othersExecute) {
        this.othersExecute = othersExecute;
    }

    /**
     * sets the POSIX File permissions. will not work on Windows OS
     *
     * @param destFilePath
     * @throws IOException
     */
    public void setFilePermissions(Path destFilePath) throws IOException {
        Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
        // add owners permission
        perms.add(PosixFilePermission.OWNER_READ);
        perms.add(PosixFilePermission.OWNER_WRITE);
        perms.add(PosixFilePermission.OWNER_EXECUTE);
        // add group permissions
        if (isGroupRead()) {
            perms.add(PosixFilePermission.GROUP_READ);
        }
        if (isGroupWrite()) {
            perms.add(PosixFilePermission.GROUP_WRITE);
        }
        if (isGroupExecute()) {
            perms.add(PosixFilePermission.GROUP_EXECUTE);
        }
        // add others permissions
        if (isOthersRead()) {
            perms.add(PosixFilePermission.OTHERS_READ);
        }
        if (isOthersWrite()) {
            perms.add(PosixFilePermission.OTHERS_WRITE);
        }
        if (isOthersExecute()) {
            perms.add(PosixFilePermission.OTHERS_EXECUTE);
        }

        Files.setPosixFilePermissions(destFilePath, perms);
    }

    /**
     * sets the POSIX File permissions. will not work on Windows OS
     *
     * @param destinationFile
     * @throws IOException
     */
    public void setFilePermissions(String destinationFile) throws IOException {
        File destFile = new File(destinationFile);
        Path destPath = destFile.toPath();
        setFilePermissions(destPath);
    }

}
