/**
 * 
 */
package com.cps.dataload.util;

/**
 * @author R.Srinivasan Defines the often used common values in here
 *
 */
public interface FeedConstants {

    public static final String IGNORE = ":IGNORE";
    public static final String FEED_INFO = "feed-info";
    public static final String FEED_ERROR = "feed-error";

}
