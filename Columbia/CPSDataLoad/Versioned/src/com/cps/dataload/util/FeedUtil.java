package com.cps.dataload.util;

import javax.transaction.TransactionManager;

import atg.nucleus.GenericService;
import atg.repository.MutableRepository;

/***
 * FeedUtil class has utility of the feeds items
 *
 */
@SuppressWarnings({ "PMD.TooManyFields" })
public class FeedUtil extends GenericService {

    private TransactionManager transactionManager;
    private MutableRepository productCatalogRepository;
    private String productItemDescriptor;
    private String skuItemDescriptor;
    private MutableRepository priceRepository;
    private String priceItemDescriptor;
    private String priceListItemDescriptor;
    private String priceList;
    private MutableRepository productCatalogRepoStaging;
    private MutableRepository pricingRepoStaging;

    public static final String FEED_TYPE_IDENTIFIER = "feedType";
    public static final String FEED_EMAIL_IDENTIFIER = "emailId";
    public static final String FEED_STATUS_SUCCESS = "Success";
    public static final String FEED_STATUS_FAILURE = "Failed";
    public static final String FEED_STATUS_IN_PROGRESS = "In Progress";

    /**
     *
     * @return productCatalogRepository
     */
    public MutableRepository getProductCatalogRepository() {
        return productCatalogRepository;
    }

    /**
     *
     * @param productCatalogRepository
     */
    public void setProductCatalogRepository(MutableRepository productCatalogRepository) {
        this.productCatalogRepository = productCatalogRepository;
    }

    /**
     *
     * @return productItemDescriptor
     */
    public String getProductItemDescriptor() {
        return productItemDescriptor;
    }

    /**
     *
     * @param productItemDescriptor
     */
    public void setProductItemDescriptor(String productItemDescriptor) {
        this.productItemDescriptor = productItemDescriptor;
    }

    /**
     * @return the skuItemDescriptor
     */
    public String getSkuItemDescriptor() {
        return skuItemDescriptor;
    }

    /**
     * @param skuItemDescriptor
     *            the skuItemDescriptor to set
     */
    public void setSkuItemDescriptor(String skuItemDescriptor) {
        this.skuItemDescriptor = skuItemDescriptor;
    }

    /**
     * @return the priceRepository
     */
    public MutableRepository getPriceRepository() {
        return priceRepository;
    }

    /**
     * @param priceRepository
     *            the priceRepository to set
     */
    public void setPriceRepository(MutableRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    /**
     * @return the priceItemDescriptor
     */
    public String getPriceItemDescriptor() {
        return priceItemDescriptor;
    }

    /**
     * @param priceItemDescriptor
     *            the priceItemDescriptor to set
     */
    public void setPriceItemDescriptor(String priceItemDescriptor) {
        this.priceItemDescriptor = priceItemDescriptor;
    }

    /**
     * @return the priceListItemDescriptor
     */
    public String getPriceListItemDescriptor() {
        return priceListItemDescriptor;
    }

    /**
     * @param priceListItemDescriptor
     *            the priceListItemDescriptor to set
     */
    public void setPriceListItemDescriptor(String priceListItemDescriptor) {
        this.priceListItemDescriptor = priceListItemDescriptor;
    }

    /**
     * @return the priceList
     */
    public String getPriceList() {
        return priceList;
    }

    /**
     * @param priceList
     *            the priceList to set
     */
    public void setPriceList(String priceList) {
        this.priceList = priceList;
    }

    public MutableRepository getProductCatalogRepoStaging() {
        return productCatalogRepoStaging;
    }

    public void setProductCatalogRepoStaging(MutableRepository productCatalogRepoStaging) {
        this.productCatalogRepoStaging = productCatalogRepoStaging;
    }

    public MutableRepository getPricingRepoStaging() {
        return pricingRepoStaging;
    }

    public void setPricingRepoStaging(MutableRepository pricingRepoStaging) {
        this.pricingRepoStaging = pricingRepoStaging;
    }

    /**
     *
     * @return transactionManager
     */
    public TransactionManager getTransactionManager() {
        return transactionManager;
    }

    /**
     *
     * @param transactionManager
     */
    public void setTransactionManager(TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

}
