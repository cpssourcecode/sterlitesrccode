package com.cps.dataload.pricing.bean;

/**
 *
 * @author Srilakshmi
 *
 */
public class CatalogItem {
    private String itemId;
    private long recordIndex;

    /**
     * @return the recordIndex
     */
    public long getRecordIndex() {
        return recordIndex;
    }

    /**
     * @param recordIndex
     *            the recordIndex to set
     */
    public void setRecordIndex(long recordIndex) {
        this.recordIndex = recordIndex;
    }

    /**
     *
     * @return
     */
    public String getItemId() {
        return itemId;
    }

    /**
     *
     * @param itemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     *
     */
    @Override
    public String toString() {
        return "CatalogItem [id=" + itemId + "]";
    }

}