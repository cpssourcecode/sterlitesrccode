package com.cps.dataload.pricing.bean;

import atg.repository.RepositoryItem;

/**
 *
 * @author Srilakshmi
 *
 */
public class PriceBean extends CatalogItem {

    private RepositoryItem priceList;
    private String skuId;
    private Double listPrice;

    /**
     * @return the priceList
     */
    public RepositoryItem getPriceList() {
        return priceList;
    }

    /**
     * @param priceList
     *            the priceList to set
     */
    public void setPriceList(RepositoryItem priceList) {
        this.priceList = priceList;
    }

    /**
     * @return the skuId
     */
    public String getSkuId() {
        return skuId;
    }

    /**
     * @param skuId
     *            the skuId to set
     */
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    /**
     * @return the listPrice
     */
    public Double getListPrice() {
        return listPrice;
    }

    /**
     * @param listPrice
     *            the listPrice to set
     */
    public void setListPrice(Double listPrice) {
        this.listPrice = listPrice;
    }

}