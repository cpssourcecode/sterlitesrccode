package com.cps.dataload.pricing.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVRecord;

import com.cps.dataload.base.service.AbstractDynamicRecordImportService;
import com.cps.dataload.exception.FeedProcessException;
import com.cps.dataload.pricing.bean.DynamicRecordItem;
import com.cps.dataload.util.FeedConstants;
import com.cps.email.CommonEmailSender;
import com.cps.util.CPSConstants;
import com.cps.util.CPSGlobalProperties;

import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.rql.RqlStatement;

/**
 * This class handles the import of the price feed - parses the input CSV, converts rows to price beans and imports the same
 *
 * @author Srilakshmi Sampath
 */
@SuppressWarnings({ "PMD.GodClass" })
public class PriceImportService extends AbstractDynamicRecordImportService {

    private List<String> priceList;

    private RqlStatement priceRQL;

    private int noOfitemsPerProject;
    
    private boolean ignoreMissingProductAsValidationError=true;
    private CommonEmailSender emailManager;
    private CPSGlobalProperties cpsGlobalProperties;
    // private List<PriceBean> priceBeanItemList;

    
    
    /**
     * @return the priceList
     */
    public List<String> getPriceList() {
        return priceList;
    }

    /**
     * @return the ignoreMissingProductAsValidationError
     */
    public boolean isIgnoreMissingProductAsValidationError() {
        return ignoreMissingProductAsValidationError;
    }

    /**
     * @param ignoreMissingProductAsValidationError the ignoreMissingProductAsValidationError to set
     */
    public void setIgnoreMissingProductAsValidationError(boolean ignoreMissingProductAsValidationError) {
        this.ignoreMissingProductAsValidationError = ignoreMissingProductAsValidationError;
    }

    /**
     * @param priceList
     *            the priceList to set
     */
    public void setPriceList(List<String> priceList) {
        this.priceList = priceList;
    }

    /**
     * @return the priceRQL
     */
    public RqlStatement getPriceRQL() {
        return priceRQL;
    }

    /**
     * @param priceRQL
     *            the priceRQL to set
     */
    public void setPriceRQL(RqlStatement priceRQL) {
        this.priceRQL = priceRQL;
    }

    /**
     * @return the noOfitemsPerProject
     */
    @Override
    public int getNoOfitemsPerProject() {
        return noOfitemsPerProject;
    }

    public CPSGlobalProperties getCpsGlobalProperties() {
	return cpsGlobalProperties;
    }

    public void setCpsGlobalProperties(CPSGlobalProperties cpsGlobalProperties) {
	this.cpsGlobalProperties = cpsGlobalProperties;
    }

    /**
     * @param noOfitemsPerProject
     *            the noOfitemsPerProject to set
     */
    @Override
    public void setNoOfitemsPerProject(int noOfitemsPerProject) {
        this.noOfitemsPerProject = noOfitemsPerProject;
    }
    /*
     * * override the validate method to call the super first and then validate send the report email of missing products.
     */
    @Override
    public boolean startImport() {
	boolean success = super.startImport();
	if (getErrorProducts().size() != 0) {
	    getCpsGlobalProperties().getErrorProducts().putAll(getErrorProducts());
	    getEmailManager().sendPriceFeedStatusEmail();
	}
	return success;
    }

    /*
     * * override the validate method to call the super first and then validate the product and sku. if product or sku is not a valid, ignore the record.
     */
    @Override
    protected String validateRecord(CSVRecord record) throws FeedProcessException {
        boolean productRecordNotAvailable = false;
        String invalidReason = super.validateRecord(record);
        String productId = record.get("IMITM");
        RepositoryItem productItem = getProductItem(productId);
        if (productItem == null) {
            productRecordNotAvailable = true;
        }
        if (productRecordNotAvailable) {
            // the trailing :IGNORE is added to indicate that this error can be ignored and hence the feed file can go to the success folder
            invalidReason = new StringBuilder(invalidReason).append("Invalid product").toString();
            if (isIgnoreMissingProductAsValidationError() && invalidReason.trim().length() > 1) {
                invalidReason = invalidReason.concat(FeedConstants.IGNORE);
            }
        } else {
            // product record is available. but check whether the base matrix price or any other matrix price is 0.0 or null.
            // if yes, continue the feed, but mark the feed as error with error reason

            Set<String> recordProperties = getRecordToItemPropertiesMap().keySet();

            for (String recordProperty : recordProperties) {
                String value = org.apache.commons.lang.StringUtils.trim(record.get(recordProperty));
                double doubleValue = org.apache.commons.lang.math.NumberUtils.toDouble(value);
                if (doubleValue == 0.0) {
                 //   invalidReason = new StringBuilder(invalidReason).append("Invalid Price for Matrix -").append(recordProperty).toString();
                }
            }
        }
        return invalidReason;
    }

    /**
     *
     * @param productId
     * @return
     */
    private RepositoryItem getProductItem(String productId) {
        RepositoryItem productItem = null;
        try {
            productItem = getLookupRepository().getItem(productId, getFeedUtil().getProductItemDescriptor());
        } catch (RepositoryException re) {
            vlogError(re, "Error while getting product item");
        }
        return productItem;
    }

    /**
     *
     */
    @Override
    protected DynamicRecordItem populateAdditionalPropertiesNotInThisFeed(DynamicRecordItem recordItem, Map additionalRelatedRecordValues) {
        if (recordItem != null) {
            String productId = recordItem.getRecordValueAsString("productId");
            if (productId != null) {
                recordItem.setRecordValue("skuId", productId);
                recordItem.setItemId("LP" + productId);
            }
            recordItem.setRecordValue("priceList", "listPrices");
        }
        return recordItem;
    }

    @Override
    protected String getItemDescriptorNameForProcessing() {
        return getProcessedItemDescriptor();
    }

    public CommonEmailSender getEmailManager() {
	return emailManager;
    }

    public void setEmailManager(CommonEmailSender emailManager) {
	this.emailManager = emailManager;
    }

}
