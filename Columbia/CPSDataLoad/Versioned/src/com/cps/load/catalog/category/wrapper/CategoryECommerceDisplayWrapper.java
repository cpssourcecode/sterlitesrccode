package com.cps.load.catalog.category.wrapper;

import vsg.load.wrapper.impl.ValuesMapBooleanWrapper;

/**
 * @author Dmitry Golubev
 **/
public class CategoryECommerceDisplayWrapper extends ValuesMapBooleanWrapper {
	@Override
	public Boolean getEmptyValue() {
		return true;
	}
}
