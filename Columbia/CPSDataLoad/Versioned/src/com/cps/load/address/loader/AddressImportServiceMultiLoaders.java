/**
 * 
 */

package com.cps.load.address.loader;

import com.cps.rmi.CacheInvalidationManager;

import vsg.load.info.StatisticsService;
import vsg.load.service.impl.ImportServiceMultiLoaders;

/**
 * @author R.Srinivasan
 * 
 *         This class extends the VSG ImportServiceMultiLoaders and does the cache invalidation after all the imports are complete
 *
 */
public class AddressImportServiceMultiLoaders extends ImportServiceMultiLoaders {

    private CacheInvalidationManager cacheInvalidationManager;
    private String invalidateCacheRepositoryPath;

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @return the invalidateCacheRepositoryPath
     */
    public String getInvalidateCacheRepositoryPath() {
        return invalidateCacheRepositoryPath;
    }

    /**
     * @param invalidateCacheRepositoryPath
     *            the invalidateCacheRepositoryPath to set
     */
    public void setInvalidateCacheRepositoryPath(String invalidateCacheRepositoryPath) {
        this.invalidateCacheRepositoryPath = invalidateCacheRepositoryPath;
    }

    /**
     * @return the cacheInvalidationManager
     */
    public CacheInvalidationManager getCacheInvalidationManager() {
        return cacheInvalidationManager;
    }

    /**
     * @param cacheInvalidationManager
     *            the cacheInvalidationManager to set
     */
    public void setCacheInvalidationManager(CacheInvalidationManager cacheInvalidationManager) {
        this.cacheInvalidationManager = cacheInvalidationManager;
    }

    @Override
    protected void performService(StatisticsService pStatistics) {
        super.performService(pStatistics);
        vlogDebug("now going to invalidate the cache of - {0}", getInvalidateCacheRepositoryPath());
        getCacheInvalidationManager().invalidateCache(getInvalidateCacheRepositoryPath());

    }

}
