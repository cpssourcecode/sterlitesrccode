package com.cps.load.loader.impl.project;

//import static com.cps.util.CPSConstants.*;

import atg.repository.MutableRepositoryItem;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import vsg.load.info.ItemStatus;
import vsg.load.info.StatisticsLoader;
import vsg.load.loader.Record;
import vsg.load.loader.impl.project.ProductProjectLoader;

import java.util.HashMap;

/**
 * @author Andy Porter
 */
public class CPSProductProjectLoader extends ProductProjectLoader {

	public static final String STOCKING_TYPE = "stockingType";

	/**
	 * perform update repository item based on passed record
	 *
	 * @param pRecord         record to be updated
	 * @param pRepository     items repository
	 * @param pItemDescriptor item descriptor name
	 * @return map of updated records
	 * @throws Exception if error occurs
	 */
	protected HashMap<String, RepositoryItem> update(Record pRecord, Repository pRepository, String pItemDescriptor)
			throws Exception {
		HashMap<String, RepositoryItem> items = new HashMap<String, RepositoryItem>();
		RepositoryItem item = getRepositoryItem(pRecord, pRepository, pItemDescriptor);
		String oldStockingTypeValue = (String) item.getPropertyValue(STOCKING_TYPE);
		String newStockingTypeValue = (String) pRecord.getValues().get(STOCKING_TYPE);

		if (pRecord.isUseToImport() && updateCheckItem(pRecord, item)) {
			initRepositoryItem(pRecord, getRepository(), (MutableRepositoryItem) item);
			getRepository().updateItem((MutableRepositoryItem) item);
			getStatistics().incrementUpdate();

			ItemStatus itemStatus = new ItemStatus(pRecord.getId(), StatisticsLoader.ACTION_CHANGE, oldStockingTypeValue, newStockingTypeValue);
			getStatistics().getItemStatuses().add(itemStatus);

			items.put(pItemDescriptor, item);
			if (pRecord.getSubRecords() != null) {
				for (String subRecordName : pRecord.getSubRecords().keySet()) {
					items.putAll(update(pRecord.getSubRecords().get(subRecordName), pRepository, subRecordName));
				}
			}
		}
		return items;
	}


	/**
	 * perform create repository item based on passed record
	 *
	 * @param pRecord         record to be inserted
	 * @param pRepository     items repository
	 * @param pItemDescriptor item descriptor name
	 * @return map of inserted records
	 * @throws Exception if error occurs
	 */
	protected HashMap<String, RepositoryItem> insert(Record pRecord, Repository pRepository, String pItemDescriptor)
			throws Exception {
		HashMap<String, RepositoryItem> items = new HashMap<String, RepositoryItem>();
		RepositoryItem item = getRepositoryItem(pRecord, pRepository, pItemDescriptor);

		String oldStockingTypeValue = " ";
		String newStockingTypeValue = (String) pRecord.getValues().get(STOCKING_TYPE);

		if (pRecord.isUseToImport() && insertCheckItem(pRecord, item)) {
			item = insertCreateItem(pRecord, pRepository, pItemDescriptor);
			initRepositoryItem(pRecord, pRepository, (MutableRepositoryItem) item);
			insertProceedItem(pRepository, item);

			ItemStatus itemStatus = new ItemStatus(pRecord.getId(), StatisticsLoader.ACTION_ADD, oldStockingTypeValue, newStockingTypeValue);
			getStatistics().getItemStatuses().add(itemStatus);

			items.put(pItemDescriptor, item);
			if (pRecord.getSubRecords() != null) {
				for (String subRecordName : pRecord.getSubRecords().keySet()) {
					items.putAll(insert(pRecord.getSubRecords().get(subRecordName), pRepository, subRecordName));
				}
			}
		}
		return items;
	}


	@Override
	protected void postLoad() {
		super.postLoad();

	}

/*
	private void saveReport(){
		StatisticsLoader statisticsLoader = getStatistics();
		statisticsLoader.get

		getAuditReportWriter().writeNext();



	}
*/


	/**
	 * add error records to error file
	 */
/*
	protected void trackError() {
		try {
			for(RecordValidationInfo validationInfo : getStatistics().getErrorRecords()) {
				getErrorWriter().writeNext(getRecordInfoWithReason(
						validationInfo.getRecord(),
						validationInfo.getRecord().getIndex()+" line:",
						validationInfo.getValidationMsg()));
			}
			getStatistics().getErrorRecords().clear();
			getStatistics().setErrorFileName(getErrorWriter().getFile().getName());
		} catch (Exception e) {
			logError(e);
		}
	}
*/


}
