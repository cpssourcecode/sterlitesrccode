-- CB clean
delete from DBC_ORG_COSTCTR;
delete from DBC_ORG_CONTACT;
delete from DBC_ORGANIZATION;
delete from DBC_ORG_APPROVER;
delete from DBC_ORG_PREFVNDR;
delete from DBC_ORG_PAYMENT;
delete from DCS_ORG_ADDRESS;
delete from DPS_ORG_CHLDORG;
delete from DPS_ORG_ANCESTORS;
delete from DPS_RELATIVEROLE;
delete from DPS_ROLE_REL_ORG;
delete from DPS_USER_ORG_ANC;
delete from DPS_USER_ORG;
delete from DPS_USER_SEC_ORGS;
delete from CPS_B2B_SALES_REP_ANC_ORG;
delete from CPS_ORGANIZATION;
delete from DPS_ORG_ROLE;
delete from DPS_ORGANIZATION;

delete from CPS_B2B_USER_MAPPED_CS;
delete from CPS_CONTACT_INFO;
delete from dps_contact_info;


-- CS clean
delete from DCS_ORG_ADDRESS where addr_id = '152152';
delete from DBC_ORGANIZATION where dflt_shipping_addr = '152152';
delete from CPS_CONTACT_INFO where id = '152152';
delete from DPS_CONTACT_INFO where id = '152152';
delete from CPS_ADDRESS_IMPORT_TYPE where id = '152152';
commit;

-- E clean
delete from CPS_ORG_CONTACT;;
delete from CPS_CONTACT;
delete from DPS_ORGANIZATION where ORG_ID in ('7480', '7479');
commit;


----------------------------


-- clean all
delete from CPS_ADDRESS_IMPORT_TYPE;
delete from CPS_ORG_CONTACT;
delete from CPS_CONTACT;
delete from DBC_ORG_COSTCTR;
delete from DBC_ORG_CONTACT;
delete from DBC_ORGANIZATION;
delete from DBC_ORG_APPROVER;
delete from DBC_ORG_PREFVNDR;
delete from DBC_ORG_PAYMENT;
delete from DCS_ORG_ADDRESS;
delete from DPS_ORG_CHLDORG;
delete from DPS_ORG_ANCESTORS;
delete from DPS_RELATIVEROLE;
delete from DPS_ROLE_REL_ORG;
delete from DPS_USER_ORG_ANC;
delete from DPS_USER_ORG;
delete from DPS_USER_SEC_ORGS;
delete from CPS_B2B_SALES_REP_ANC_ORG;
delete from CPS_ORGANIZATION;
delete from DPS_ORG_ROLE;
delete from DPS_ORGANIZATION;
delete from CPS_B2B_USER_MAPPED_CS;
delete from CPS_CONTACT_INFO;
delete from DPS_CONTACT_INFO;
commit;
