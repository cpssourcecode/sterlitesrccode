package com.cps.invoice;

import vsg.order.VSGOrderInfo;

import java.util.Date;

/**
 * Bean that represents information for invoice
 *
 * @author Steve Neverov
 */
public class InvoiceInfo extends VSGOrderInfo {
//public class InvoiceInfo implements Comparable{

	public InvoiceInfo() {
		super();
	}

	private String mInvoiceNumber;

	public void setInvoiceNumber(String pInvoiceNumber) {
		mInvoiceNumber = pInvoiceNumber;
	}

	public String getInvoiceNumber() {
		return mInvoiceNumber;
	}

	private Date mInvoiceDate;

	public void setInvoiceDate(Date pInvoiceDate) {
		mInvoiceDate = pInvoiceDate;
	}

	public Date getInvoiceDate() {
		return mInvoiceDate;
	}

	private long mEpochInvoiceDate;

	public void setEpochInvoiceDate(long pEpochInvoiceDate) {
		mEpochInvoiceDate = pEpochInvoiceDate;
	}

	public long getEpochInvoiceDate() {
		return mEpochInvoiceDate;
	}

	private double mInvoiceAmount;

	public void setInvoiceAmount(double pInvoiceAmount) {
		mInvoiceAmount = pInvoiceAmount;
	}

	public double getInvoiceAmount() {
		return mInvoiceAmount;
	}

	private String mPONumber;

	public String getPONumber() {
		return mPONumber;
	}

	public void setPONumber(String pPONumber) {
		this.mPONumber = pPONumber;
	}

//	public String toString() {
//
//		StringBuilder result = new StringBuilder();
//		result.append("Id: ").append(getId()).append("; ");
//		return result.toString();
//
//	}

//	public int compareTo(Object pInfo) {
//		return Long.compare(((InvoiceInfo)pInfo).getEpochModifiedDate(), getEpochModifiedDate());
//	}

}
