package com.cps.integrations.invoice;

/**
 * CPSInvoiceConstants
 * 
 * @author Steve Neverov
 * 
 */
public final class CPSInvoiceConstants {

	/**
	 * 
	 */
	private CPSInvoiceConstants() {
		// restrict instantiation
	}
	
	// Fields
	public static final String INVOICE_AMOUNT = "invoiceAmount";
	public static final String INVOICE_LINK = "invoiceLink";
	public static final String INVOICE_NUMBER = "invoiceNumber";
	public static final String INVOICE_CS = "invoiceCS";
	public static final String INVOICED_DATE = "invoicedDate";
	public static final String DUE_DATE = "dueDate";
	public static final String INVOICE_STATUS = "invoiceStatus";
	
	// status
	public static final String INVOICE_STATUS_OPEN = "open";
	public static final String INVOICE_STATUS_PAID = "paid";
	public static final String INVOICE_STATUS_PENDING = "pending";

	public static final String PARAM_PAGE_SIZE = "pageSize";
	public static final String PARAM_FROM = "from";
	public static final String PARAM_CUSTOMER_ID = "customerId";
	public static final String PARAM_QUERY = "query";
	public static final String PARAM_ORDER_NUMBER = "orderNum";
	public static final String PARAM_INVOICE_NUMBER = "invoiceNum";
	public static final String PARAM_SHIP_TO_NUMBER = "shipToNum";
	public static final String PARAM_DATE_FROM = "orderDateFrom";
	public static final String PARAM_DATE_TO = "orderDateTo";
	public static final String PARAM_PO_NUMBER = "purchaseOrderNum";
	public static final String PARAM_SHIP_TO_ADDRESS = "shipToAddress";
	public static final String PARAM_SORT_FIELD = "sortField";
	public static final String PARAM_SORT_OPTION = "sortOrder";
	public static final String PARAM_BILL_TO_NUMBER = "billToNum";

	public static final String NUM_FOUND = "numFound";
	public static final String RESULTS = "results";
	public static final String URL = "url";
	public static final String DOCID  = "DOCID";
	public static final String INVOICE_DATE = "INVOICE_DATE";
	public static final String INVOICE_NUM = "INVOICE_NUMBER";
	public static final String SHIP_TO_NUMBER = "SHIP_TO_NUMBER";
	public static final String SHIP_TO_NAME = "SHIP_TO_NAME";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String INVOICE_TOTAL_AMOUNT = "INVOICE_TOTAL_AMOUNT";
	public static final String CUSTOMER_PO_NUMBER = "CUSTOMER_PO_NUMBER";
	
	public static final String PROP_ADDRESS_LINE1 = "address1";

}
