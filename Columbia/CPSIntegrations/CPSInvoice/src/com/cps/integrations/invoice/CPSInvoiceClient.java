package com.cps.integrations.invoice;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.axis2.AxisFault;

import com.cps.invoice.InvoiceInfo;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import jp5503b1.bssv.e1.columbiapipe.com.*;
import vsg.exception.WebServiceException;
import vsg.userprofiling.address.VSGAddressTools;
import vsg.util.json.VSGJsonReader;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSInvoiceClient
 * <p>
 * <h4>Description</h4>
 * <p>
 * <h4>Notes</h4>
 *
 * @author Steve Neverov
 */
public class CPSInvoiceClient extends WebServiceClient {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/integrations/invoice/CPSInvoiceClient";

    private boolean mAddressFromRepository;

    /**
     * This method will return a list of invoices
     *
     * @return
     * @throws WebServiceException
     */
    public Map<String, Object> requestInvoices(String pCustomerNumber, Calendar pStartDate, Calendar pEndDate, String pInvoiceNumber, String pOrderNumber,
                    String pPONumber, List<String> pShipTo, String pBillTo, int pPageSize, int pStartIndex, String pSortField, String pSortOption)
                    throws WebServiceException {

        final String operationName = COMPONENT_NAME + ".requestInvoices";
        final String operationParam = pCustomerNumber;

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoices").append(" - start").toString());
        }
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        Map<String, Object> result = new HashMap<String, Object>();
        List<InvoiceInfo> infos = new ArrayList<InvoiceInfo>();

        WebServiceConfig config = getWebServiceConfig();
        try {
            String url = new StringBuilder(config.getHistoryServiceAddress()).toString();

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add(CPSInvoiceConstants.PARAM_PAGE_SIZE, pPageSize);
            builder.add(CPSInvoiceConstants.PARAM_FROM, pStartIndex);
            builder.add(CPSInvoiceConstants.PARAM_CUSTOMER_ID, pCustomerNumber);
            if (!StringUtils.isBlank(pSortField)) {
                builder.add(CPSInvoiceConstants.PARAM_SORT_FIELD, pSortField);
            } else {
                builder.add(CPSInvoiceConstants.PARAM_SORT_FIELD, CPSInvoiceConstants.INVOICE_DATE);
            }
            if (!StringUtils.isBlank(pSortOption)) {
                builder.add(CPSInvoiceConstants.PARAM_SORT_OPTION, pSortOption);
            } else {
                builder.add(CPSInvoiceConstants.PARAM_SORT_OPTION, "DESC");
            }

            JsonObjectBuilder queryBuilder = Json.createObjectBuilder();
            if (!StringUtils.isBlank(pInvoiceNumber)) {
                queryBuilder.add(CPSInvoiceConstants.PARAM_INVOICE_NUMBER, Integer.valueOf(pInvoiceNumber));
            }
            if (!StringUtils.isBlank(pOrderNumber)) {
                queryBuilder.add(CPSInvoiceConstants.PARAM_ORDER_NUMBER, pOrderNumber);
            }
            if (!StringUtils.isBlank(pPONumber)) {
                queryBuilder.add(CPSInvoiceConstants.PARAM_PO_NUMBER, pPONumber);
            }
            if (pStartDate != null) {
                queryBuilder.add(CPSInvoiceConstants.PARAM_DATE_FROM, pStartDate.getTimeInMillis());
            }
            if (pEndDate != null) {
                queryBuilder.add(CPSInvoiceConstants.PARAM_DATE_TO, pEndDate.getTimeInMillis());
            }

            if (pShipTo != null && pShipTo.size() > 0) {
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                for (String shipTo : pShipTo) {
                    arrayBuilder.add(shipTo);
                }
                queryBuilder.add(CPSInvoiceConstants.PARAM_SHIP_TO_NUMBER, arrayBuilder.build());
            }

            if (pBillTo != null) {
                queryBuilder.add(CPSInvoiceConstants.PARAM_BILL_TO_NUMBER, pBillTo);
            } else {
                queryBuilder.add(CPSInvoiceConstants.PARAM_BILL_TO_NUMBER, pCustomerNumber);
            }
            builder.add(CPSInvoiceConstants.PARAM_QUERY, queryBuilder.build());
            JsonObject params = builder.build();

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request: ").append(url).append(" ; params: ").append(params).toString());
            }

            JsonObject response = VSGJsonReader.readJsonObjectFromUrl(url, params, config.getTimeOut(), config.getSoTimeOut());

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Response: ").append(response).toString());
            }

            if (response != null) {
                Integer pages = response.getInt(CPSInvoiceConstants.NUM_FOUND);
                result.put(CPSInvoiceConstants.NUM_FOUND, pages);

                JsonArray results = response.getJsonArray(CPSInvoiceConstants.RESULTS);
                for (int i = 0; i < results.size(); i++) {
                    JsonObject invoiceData = results.getJsonObject(i);
                    InvoiceInfo invoiceInfo = new InvoiceInfo();
                    invoiceInfo.setId(Integer.valueOf(invoiceData.getInt(CPSInvoiceConstants.DOCID)).toString());
                    invoiceInfo.setUrl(new StringBuilder().append(config.getDocumentExtractionServiceAddress())
                                    .append(invoiceData.getString(CPSInvoiceConstants.URL)).toString());
                    Long epochInvoiceDate = invoiceData.getJsonNumber(CPSInvoiceConstants.INVOICE_DATE).longValue();
                    invoiceInfo.setEpochInvoiceDate(epochInvoiceDate);
                    invoiceInfo.setInvoiceDate(new Date(epochInvoiceDate));
                    invoiceInfo.setOrderNumber(Integer.valueOf(invoiceData.getInt(CPSInvoiceConstants.ORDER_NUMBER)).toString());
                    invoiceInfo.setInvoiceNumber(Integer.valueOf(invoiceData.getInt(CPSInvoiceConstants.INVOICE_NUM)).toString());
                    invoiceInfo.setPONumber(invoiceData.getString(CPSInvoiceConstants.CUSTOMER_PO_NUMBER));
                    String shipTo = Integer.valueOf(invoiceData.getInt(CPSInvoiceConstants.SHIP_TO_NUMBER)).toString();
                    invoiceInfo.setShipTo(shipTo);
                    invoiceInfo.setShipToName(invoiceData.getString(CPSInvoiceConstants.SHIP_TO_NAME));
                    if (isAddressFromRepository()) {
                        RepositoryItem shipAddress = VSGAddressTools.getShippingNameByShipTo(shipTo);
                        if (shipAddress != null) {
                            invoiceInfo.setShipToName((String) shipAddress.getPropertyValue(CPSInvoiceConstants.PROP_ADDRESS_LINE1));
                        }
                    }

                    invoiceInfo.setInvoiceAmount(invoiceData.getJsonNumber(CPSInvoiceConstants.INVOICE_TOTAL_AMOUNT).doubleValue());
                    infos.add(invoiceInfo);
                }
                result.put("invoices", infos);
            }

        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            PerformanceMonitor.cancelOperation(operationName, operationParam);
            throw new WebServiceException(e);
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoices").append(" - exit").toString());
        }

        return result;
    }

    /**
     * This method will return a invoice status details
     *
     * @param pParams
     * @return
     * @throws WebServiceException
     */
    public Map<String, Object> requestInvoiceStatus(Map<String, Object> pParams) throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoiceStatus").append(" - start").toString());
        }

        Map<String, Object> reply = new HashMap<String, Object>();

        final String operationName = COMPONENT_NAME + ".requestInvoiceStatus";
        final String operationParams = "invoiceNumber: " + pParams.get(CPSInvoiceConstants.INVOICE_NUMBER);
        PerformanceMonitor.startOperation(operationName, operationParams);
        boolean success = true;

        try {
            InvoiceStatusManagerServiceStub service = new InvoiceStatusManagerServiceStub(getWebServiceConfig().getServiceAddress());
            service._getServiceClient().addHeader(createSecurityHeader(getWebServiceConfig().getUserId(), getWebServiceConfig().getPassword()));

            ProcessInvoiceStatusDocument request = ProcessInvoiceStatusDocument.Factory.newInstance();
            InvoiceStatusVO statusVO = InvoiceStatusVO.Factory.newInstance();
            statusVO.setDocumentNumber(Integer.valueOf((String) pParams.get(CPSInvoiceConstants.INVOICE_NUMBER)));
            request.setProcessInvoiceStatus(statusVO);

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request: ").append(request.toString()).toString());
            }

            ProcessInvoiceStatusResponseDocument response = service.processInvoiceStatus(request);

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Response: ").append(response.toString()).toString());
            }

            ConfirmInvoiceStatusVO invoiceStatusVO = response.getProcessInvoiceStatusResponse();
            if (invoiceStatusVO != null) {
                reply.put(CPSInvoiceConstants.INVOICE_STATUS, invoiceStatusVO.getInvoiceStatus());
            }

        } catch (AxisFault af) {
            if (isLoggingError()) {
                if (af.getDetail() != null) {
                    String faultString = af.getDetail().toString();
                    logError(faultString);
                }
                logError(af);
            }
            success = false;
            throw new WebServiceException(af.getMessage());
        } catch (RemoteException | BusinessServiceExceptionException e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e.getMessage());
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParams);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParams);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoiceStatus").append(" - exit").toString());
        }

        return reply;

    }

    /**
     * This method will return a list containing a map of invoice data based on a start and end date and CS
     *
     * @throws WebServiceException
     */
    public void testRequestInvoiceStatus() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestInvoiceStatus").append(" - start").toString());
        }

        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(CPSInvoiceConstants.INVOICE_NUMBER, "37040");
            Map<String, Object> reply = null;
            reply = requestInvoiceStatus(params);
            String status = (String) reply.get(CPSInvoiceConstants.INVOICE_STATUS);
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Returned status: ").append(status).toString());
            }

        } catch (WebServiceException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestInvoiceStatus").append(" - exit").toString());
        }

    }

    public void testRequestInvoices() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestInvoices").append(" - start").toString());
        }

        List<String> shipto = new ArrayList<String>();
        shipto.add("110145");

        try {
            Calendar startDate = Calendar.getInstance();
            startDate.add(Calendar.DAY_OF_MONTH, -120);
            // startDate.setTimeInMillis(1422856800000l);
            Calendar endDate = Calendar.getInstance();
            // endDate.setTimeInMillis(1422856800000l);

            Map<String, Object> reply = requestInvoices("110144", startDate, endDate, null, null, null, shipto, "110144", 5, 0, null, null);
            List<InvoiceInfo> invoices = (List<InvoiceInfo>) reply.get("invoices");
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Returned invoices: ").append(invoices).toString());
            }

        } catch (WebServiceException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestInvoices").append(" - exit").toString());
        }

    }

    public boolean isAddressFromRepository() {
        return mAddressFromRepository;
    }

    public void setAddressFromRepository(boolean pAddressFromRepository) {
        mAddressFromRepository = pAddressFromRepository;
    }

}
