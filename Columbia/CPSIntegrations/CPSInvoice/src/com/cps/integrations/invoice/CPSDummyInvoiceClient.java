package com.cps.integrations.invoice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import vsg.exception.WebServiceException;
import vsg.webservices.DummyClient;

/**
 * CPSDummyInvoiceClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class CPSDummyInvoiceClient extends DummyClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/invoice/CPSDummyInvoiceClient";

    /** Test Amount property */
    private double mTestAmount;

    /** Test Link property */
    private String mTestLink;

    /** Test Invoice Number property */
    private String mTestInvoiceNumber;

    /** Test CS property */
    private String mTestCS;

    /**
     * 
     * @return
     */
    public double getTestAmount() {
        return mTestAmount;
    }

    /**
     * 
     * @param pTestAmount
     */
    public void setTestAmount(double pTestAmount) {
        mTestAmount = pTestAmount;
    }

    /**
     * @return
     */
    public String getTestLink() {
        return mTestLink;
    }

    /**
     * @param pTestLink
     */
    public void setTestLink(String pTestLink) {
        mTestLink = pTestLink;
    }

    /**
     * @return
     */
    public String getTestInvoiceNumber() {
        return mTestInvoiceNumber;
    }

    /**
     * @param pTestInvoiceNumber
     */
    public void setTestInvoiceNumber(String pTestInvoiceNumber) {
        mTestInvoiceNumber = pTestInvoiceNumber;
    }

    /**
     * @return
     */
    public String getTestCS() {
        return mTestCS;
    }

    /**
     * @param pTestCS
     */
    public void setTestCS(String pTestCS) {
        mTestCS = pTestCS;
    }

    /**
     * This method will return a list containing a map of invoice data based on a start and end date and CS
     * 
     * @param pStartDate
     * @param pEndDate
     * @param pCustomerSpecificIdentifier
     * @param pJdeAccountNumber
     * @param pStatus
     * @return
     * @throws WebServiceException
     */
    public List<HashMap<String, Object>> requestInvoices(Calendar pStartDate, Calendar pEndDate, String pCustomerSpecificIdentifier, String pJdeAccountNumber,
                    String pStatus) throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoices").append(" - start").toString());
        }

        ArrayList<HashMap<String, Object>> invoices = new ArrayList<HashMap<String, Object>>();
        // build invoices for status of Open, Paid for varying dates
        // open invoices
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 10));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 11));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 20));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 21));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 30));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 31));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 40));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 41));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 50));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_OPEN, 51));

        // paid invoices
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 10));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 11));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 20));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 21));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 30));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 31));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 40));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 41));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 50));
        invoices.add(generateInvoiceMap(CPSInvoiceConstants.INVOICE_STATUS_PAID, 51));

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInvoices").append(" - exit").toString());
        }
        return invoices;
    }

    /**
     * @param pStatus
     * @param pDateVariance
     * @return
     */
    private HashMap<String, Object> generateInvoiceMap(String pStatus, int pDateVariance) {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".generateInvoiceMap").append(" - start").toString());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        HashMap<String, Object> invoiceMap = new HashMap<String, Object>();

        // build dummy data set to be returned to application
        invoiceMap.put(CPSInvoiceConstants.INVOICE_AMOUNT, getTestAmount() + pDateVariance);
        invoiceMap.put(CPSInvoiceConstants.INVOICE_LINK, getTestLink());
        invoiceMap.put(CPSInvoiceConstants.INVOICE_NUMBER, getTestInvoiceNumber() + pDateVariance);
        invoiceMap.put(CPSInvoiceConstants.INVOICE_CS, getTestCS() + pDateVariance);
        invoiceMap.put(CPSInvoiceConstants.INVOICE_STATUS, pStatus);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -pDateVariance);
            invoiceMap.put(CPSInvoiceConstants.INVOICED_DATE, calendar);
            calendar.add(Calendar.DATE, -(pDateVariance + 7));
            invoiceMap.put(CPSInvoiceConstants.DUE_DATE, calendar);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".generateInvoiceMap").append(" - exit").toString());
        }
        return invoiceMap;
    }
}
