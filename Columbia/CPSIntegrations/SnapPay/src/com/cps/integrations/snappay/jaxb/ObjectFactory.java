
package com.cps.integrations.snappay.jaxb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cps.integrations.snappay.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cps.integrations.snappay.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Param }
     * 
     */
    public Param createParam() {
        return new Param();
    }

    /**
     * Create an instance of {@link Param }
     *
     */
    public Param createParam(String name, String value) {
        return new Param(name, value);
    }

    /**
     * Create an instance of {@link Request }
     * 
     */
    public Request createRequest() {
        return new Request();
    }

    /**
     * Create an instance of {@link Request }
     *
     */
    public Request createRequest(Method method) {
        return new Request(method);
    }

    /**
     * Create an instance of {@link Response }
     *
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link Response }
     *
     */
    public Response createResponse(Method method) {
        return new Response(method);
    }

    /**
     * Create an instance of {@link Method }
     * 
     */
    public Method createMethod() {
        return new Method();
    }

    /**
     * Create an instance of {@link Method }
     *
     */
    public Method createMethod(String name) {
        return new Method(name);
    }

    /**
     * Create an instance of {@link Params }
     * 
     */
    public Params createParams() {
        return new Params();
    }

    /**
     * Create an instance of {@link Level2 }
     * 
     */
    public Level2 createLevel2() {
        return new Level2();
    }

    /**
     * Create an instance of {@link Card }
     * 
     */
    public Card createCard() {
        return new Card();
    }

    /**
     * Create an instance of {@link Payment }
     * 
     */
    public Payment createPayment() {
        return new Payment();
    }

    /**
     * Create an instance of {@link CallMethod }
     *
     */
    public CallMethod createCallMethod() {
        return new CallMethod();
    }

    /**
     * Create an instance of {@link CallMethod }
     *
     */
    public CallMethod createCallMethod(String name) {
        return new CallMethod(name);
    }

    /**
     * Create an instance of {@link WebResponse }
     *
     */
    public WebResponse createWebResponse() {
        return new WebResponse();
    }

    /**
     * Create an instance of {@link WebResponse }
     *
     */
    public WebResponse createWebResponse(CallMethod callMethod) {
        return new WebResponse(callMethod);
    }

    /**
     * Create an instance of {@link WebRequests }
     *
     */
    public WebRequests createWebRequests() {
        return new WebRequests();
    }

    /**
     * Create an instance of {@link WebRequests }
     *
     */
    public WebRequests createWebRequests(CallMethod callMethod) {
        return new WebRequests(callMethod);
    }

}
