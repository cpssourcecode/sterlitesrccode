package com.cps.integrations.snappay;

public class SnapPayConstants {

    // methods
	public static final String SET_TRANSACTION_DETAILS = "SetTransactionDetails";
    public static final String GET_TRANSACTION_DETAILS = "GetTransactionDetails";
    public static final String EXTERNAL_ORDER_UPDATE = "ExternalOrderUpdate";

    // params
	public static final String SHARED_SECRET_KEY = "SharedSecretKey";
	public static final String REQUEST_NUMBER = "RequestNumber";
    public static final String CUSTOMER_ID = "CustomerId";
    public static final String CURRENCY_CODE = "CurrencyCode";
    public static final String COMPANY_CODE = "CompanyCode";
    public static final String USER_ID = "UserId";
    public static final String SAVE_AT_USER = "SaveAtUser";
    public static final String SAVE_AT_CUSTOMER = "SaveAtCustomer";
    public static final String DISPLAY_CARDS_SAVED_AT_CUSTOMER = "DisplayCardsSavedAtCustomer";
    public static final String REDIRECT_URL = "RedirectURL";
    public static final String CVV_REQUIRED = "CVVRequired";
    public static final String REQUEST_FOR_MAINTAIN_CARDS = "RequestForMaintainCards";

    public static final String ORDER_NUMBER = "OrderNumber";
    public static final String ORDER_TYPE = "OrderType";
    public static final String COMPANY = "Company";
    public static final String SUFFIX = "Suffix";

    public static final String STATUS = "Status";
    public static final String DESCRIPTION = "Description";
    public static final String RETURN_MESSAGE = "ReturnMessage";

    // level2
    public static final String TRANSACTION_TYPE = "TRANSACTION_TYPE";
    public static final String ORDER_ID = "ORDER_ID";
    public static final String CUSTOM_ID = "CUSTOM_ID";
    public static final String CUSTOM_ID2 = "CUSTOM_ID2";
    public static final String INVOICE_ID = "INVOICE_ID";
    public static final String COMMENT = "COMMENT";
    public static final String AMOUNT = "AMOUNT";

    // card
	public static final String TOKEN_ID = "TokenId";
	public static final String LAST_4 = "Last4";
	public static final String CARD_TYPE = "CardType";
	public static final String FIRST_NAME = "FirstName";
	public static final String LAST_NAME = "LastName";
	public static final String EXPIRATION_MONTH = "ExpirationMonth";
	public static final String EXPIRATION_YEAR = "ExpirationYear";
	public static final String ADDRESSS_LINE_1 = "AddressLine1";
	public static final String CITY = "City";
	public static final String STATE = "State";
	public static final String ZIP = "Zip";
	public static final String COUNTRY = "Country";
	public static final String EMAIL = "Email";

    // payment
    public static final String PG_STATUS = "PGStatus";
    public static final String PG_TRANSACTION_ID = "PGTransactionId";
    public static final String PG_RETURN_DESCRIPTION = "PGReturnDescription";
    public static final String AUTH_CODE = "AuthCode";
    public static final String PG_AMOUNT = "Amount";
    public static final String ZIP_CODE_VERIFICATION = "ZipCodeVerification";
    public static final String ADDRESS_VERIFICATION = "AddressVerification";
    public static final String CVV_VERIFICATION = "CVVVerification";
    public static final String AUTH_DATE = "AuthDate";
    public static final String AUTH_TIME = "AuthTime";

    // values
    public static final String AUTH_DATE_FORMAT = "mm/dd/yyyy";
    public static final String AUTH_TRANSACTION_TYPE = "A";
    public static final String YES = "Y";
    public static final String NO = "N";
    public static final String ORDER_TYPE_SO = "SO";
    public static final String CURRENCY_CODE_USD = "USD";

}
