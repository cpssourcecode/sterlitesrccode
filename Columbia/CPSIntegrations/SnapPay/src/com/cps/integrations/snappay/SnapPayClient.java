package com.cps.integrations.snappay;

import static com.cps.integrations.snappay.SnapPayConstants.AMOUNT;
import static com.cps.integrations.snappay.SnapPayConstants.AUTH_TRANSACTION_TYPE;
import static com.cps.integrations.snappay.SnapPayConstants.COMMENT;
import static com.cps.integrations.snappay.SnapPayConstants.COMPANY;
import static com.cps.integrations.snappay.SnapPayConstants.COMPANY_CODE;
import static com.cps.integrations.snappay.SnapPayConstants.CURRENCY_CODE;
import static com.cps.integrations.snappay.SnapPayConstants.CURRENCY_CODE_USD;
import static com.cps.integrations.snappay.SnapPayConstants.CUSTOMER_ID;
import static com.cps.integrations.snappay.SnapPayConstants.CUSTOM_ID;
import static com.cps.integrations.snappay.SnapPayConstants.CUSTOM_ID2;
import static com.cps.integrations.snappay.SnapPayConstants.CVV_REQUIRED;
import static com.cps.integrations.snappay.SnapPayConstants.DISPLAY_CARDS_SAVED_AT_CUSTOMER;
import static com.cps.integrations.snappay.SnapPayConstants.EXTERNAL_ORDER_UPDATE;
import static com.cps.integrations.snappay.SnapPayConstants.GET_TRANSACTION_DETAILS;
import static com.cps.integrations.snappay.SnapPayConstants.INVOICE_ID;
import static com.cps.integrations.snappay.SnapPayConstants.NO;
import static com.cps.integrations.snappay.SnapPayConstants.ORDER_ID;
import static com.cps.integrations.snappay.SnapPayConstants.ORDER_NUMBER;
import static com.cps.integrations.snappay.SnapPayConstants.ORDER_TYPE;
import static com.cps.integrations.snappay.SnapPayConstants.ORDER_TYPE_SO;
import static com.cps.integrations.snappay.SnapPayConstants.REDIRECT_URL;
import static com.cps.integrations.snappay.SnapPayConstants.REQUEST_FOR_MAINTAIN_CARDS;
import static com.cps.integrations.snappay.SnapPayConstants.REQUEST_NUMBER;
import static com.cps.integrations.snappay.SnapPayConstants.SAVE_AT_CUSTOMER;
import static com.cps.integrations.snappay.SnapPayConstants.SAVE_AT_USER;
import static com.cps.integrations.snappay.SnapPayConstants.SET_TRANSACTION_DETAILS;
import static com.cps.integrations.snappay.SnapPayConstants.SHARED_SECRET_KEY;
import static com.cps.integrations.snappay.SnapPayConstants.STATUS;
import static com.cps.integrations.snappay.SnapPayConstants.SUFFIX;
import static com.cps.integrations.snappay.SnapPayConstants.TRANSACTION_TYPE;
import static com.cps.integrations.snappay.SnapPayConstants.USER_ID;
import static com.cps.integrations.snappay.SnapPayConstants.YES;

import java.io.StringReader;
import java.io.StringWriter;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;

import com.cditechnology.www.products.payments.ExternalOrderUpdateDocument;
import com.cditechnology.www.products.payments.ExternalOrderUpdateResponseDocument;
import com.cditechnology.www.products.payments.ExternalRequestDocument;
import com.cditechnology.www.products.payments.ExternalRequestResponseDocument;
import com.cditechnology.www.products.payments.ManageServicesStub;
import com.cditechnology.www.products.payments.PaymentDetailsRequestDocument;
import com.cditechnology.www.products.payments.PaymentDetailsRequestResponseDocument;
import com.cps.integrations.snappay.jaxb.CallMethod;
import com.cps.integrations.snappay.jaxb.Card;
import com.cps.integrations.snappay.jaxb.Level2;
import com.cps.integrations.snappay.jaxb.Method;
import com.cps.integrations.snappay.jaxb.ObjectFactory;
import com.cps.integrations.snappay.jaxb.Params;
import com.cps.integrations.snappay.jaxb.Payment;
import com.cps.integrations.snappay.jaxb.Request;
import com.cps.integrations.snappay.jaxb.Response;
import com.cps.integrations.snappay.jaxb.WebRequests;
import com.cps.integrations.snappay.jaxb.WebResponse;

import atg.service.perfmonitor.PerformanceMonitor;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * SnapPayClient
 *
 */
public class SnapPayClient extends WebServiceClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/snappay/SnapPayClient";

    private boolean mCvvRequired = true;

    public boolean isCvvRequired() {
        return mCvvRequired;
    }

    public void setCvvRequired(boolean pCvvRequired) {
        mCvvRequired = pCvvRequired;
    }

    private boolean mDisplaySavedCards = true;

    public boolean isDisplaySavedCards() {
        return mDisplaySavedCards;
    }

    public void setDisplaySavedCards(boolean pDisplaySavedCards) {
        mDisplaySavedCards = pDisplaySavedCards;
    }

    private String mCompanyCode;

    public String getCompanyCode() {
        return mCompanyCode;
    }

    public void setCompanyCode(String pCompanyCode) {
        mCompanyCode = pCompanyCode;
    }

    private ManageServicesStub getServiceStub() throws AxisFault {
        WebServiceConfig config = getWebServiceConfig();
        ManageServicesStub serviceStub = new ManageServicesStub(config.getServiceAddress());
        // serviceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(config.getTimeOut());
        serviceStub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, config.getTimeOut());
        serviceStub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getSoTimeOut());
        serviceStub._getServiceClient().getOptions().setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);
        return serviceStub;
    }

    private String marshal(JAXBContext pContext, Object pBean) throws JAXBException {
        Marshaller marshaller = pContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(pBean, stringWriter);
        return stringWriter.toString();
    }

    private Object unmarshal(JAXBContext pContext, String pXMLStr) throws JAXBException {
        Unmarshaller unmarshaller = pContext.createUnmarshaller();
        StringReader reader = new StringReader(pXMLStr);
        return unmarshaller.unmarshal(reader);
    }

    private String createSetTransactionDetailsRequest(Map<String, String> pParams, boolean pMaintainCreditCards) throws JAXBException {

        ObjectFactory of = new ObjectFactory();
        Method methodBean = of.createMethod(SET_TRANSACTION_DETAILS);

        Params params = of.createParams();
        params.getParam().add(of.createParam(SHARED_SECRET_KEY, getWebServiceConfig().getSecretKey()));
        params.getParam().add(of.createParam(REQUEST_NUMBER, pParams.get(REQUEST_NUMBER)));
        params.getParam().add(of.createParam(CUSTOMER_ID, pParams.get(CUSTOMER_ID)));
        params.getParam().add(of.createParam(CURRENCY_CODE, CURRENCY_CODE_USD));
        params.getParam().add(of.createParam(COMPANY_CODE, getCompanyCode()));
        params.getParam().add(of.createParam(USER_ID, pParams.get(USER_ID)));
        params.getParam().add(of.createParam(SAVE_AT_USER, YES));
        params.getParam().add(of.createParam(SAVE_AT_CUSTOMER, YES));
        params.getParam().add(of.createParam(DISPLAY_CARDS_SAVED_AT_CUSTOMER, isDisplaySavedCards() ? YES : NO));
        params.getParam().add(of.createParam(REDIRECT_URL, pParams.get(REDIRECT_URL)));
        params.getParam().add(of.createParam(CVV_REQUIRED, isCvvRequired() ? YES : NO));
        params.getParam().add(of.createParam(REQUEST_FOR_MAINTAIN_CARDS, pMaintainCreditCards ? YES : NO));
        methodBean.setParams(params);

        Level2 level2 = of.createLevel2();
        level2.getParam().add(of.createParam(TRANSACTION_TYPE, AUTH_TRANSACTION_TYPE));
        level2.getParam().add(of.createParam(ORDER_ID, pParams.get(ORDER_ID)));
        level2.getParam().add(of.createParam(CUSTOM_ID, ""));
        level2.getParam().add(of.createParam(CUSTOM_ID2, ""));
        level2.getParam().add(of.createParam(INVOICE_ID, pParams.get(INVOICE_ID)));
        String comment = pMaintainCreditCards ? "Credit card maintenance for user: " + pParams.get(CUSTOMER_ID)
                        : "Order Authorization " + pParams.get(ORDER_ID);
        level2.getParam().add(of.createParam(COMMENT, comment));
        level2.getParam().add(of.createParam(AMOUNT, pParams.get(AMOUNT)));
        methodBean.setLevel2(level2);

        return marshal(JAXBContext.newInstance(Request.class), of.createRequest(methodBean));

    }

    public Map<String, String> setTransactionDetails(Map<String, String> pParams, boolean pMaintainCreditCards) {

        vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".setTransactionDetails").append(" - start").toString());

        String operationName = COMPONENT_NAME + ".setTransactionDetails";
        String operationParam = "RequestNumber: " + pParams.get(REQUEST_NUMBER);
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = false;
        Map<String, String> results = new HashMap<String, String>();
        try {
            ManageServicesStub serviceStub = getServiceStub();

            ExternalRequestDocument requestDocument = ExternalRequestDocument.Factory.newInstance();
            ExternalRequestDocument.ExternalRequest request = requestDocument.addNewExternalRequest();
            request.setStrInputXML(createSetTransactionDetailsRequest(pParams, pMaintainCreditCards));
            vlogDebug(new StringBuilder().append("Request: ").append(requestDocument).toString());

            ExternalRequestResponseDocument responseDocument = serviceStub.externalRequest(requestDocument);
            vlogDebug(new StringBuilder().append("Response: ").append(responseDocument).toString());
            if (responseDocument != null) {
                Object result = unmarshal(JAXBContext.newInstance(Response.class), responseDocument.getExternalRequestResponse().getExternalRequestResult());
                if (result instanceof Response) {
                    results.putAll(((Response) result).getMethod().getParams().getParamMap());
                }
            }
            success = true;

        } catch (AxisFault af) {
            if (af.getDetail() != null) {
                vlogError(af.getDetail().toString());
            }
            vlogError(af, "Error");
        } catch (RemoteException e) {
            vlogError(e, "Error");
        } catch (PropertyException e) {
            vlogError(e, "Error");
        } catch (JAXBException e) {
            vlogError(e, "Error");
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".setTransactionDetails").append(" - exit").toString());

        return results;
    }

    private String createGetTransactionDetailsRequest(Map<String, String> pParams) throws JAXBException {

        ObjectFactory of = new ObjectFactory();
        Method methodBean = of.createMethod(GET_TRANSACTION_DETAILS);

        Params params = of.createParams();
        params.getParam().add(of.createParam(SHARED_SECRET_KEY, getWebServiceConfig().getSecretKey()));
        params.getParam().add(of.createParam(REQUEST_NUMBER, pParams.get(REQUEST_NUMBER)));
        methodBean.setParams(params);

        return marshal(JAXBContext.newInstance(Request.class), of.createRequest(methodBean));

    }

    private void processGetTransactionDetailsResponse(Response pResponse, Map<String, String> pResults) throws JAXBException {

        Method method = pResponse.getMethod();
        Params params = method.getParams();
        Map<String, String> paramsMap = params.getParamMap();
        String status = paramsMap.get(STATUS);
        pResults.putAll(paramsMap);
        if (YES.equals(status)) {
            Card card = method.getCard();
            if (card != null) {
                pResults.putAll(card.getParamMap());
            }
            Payment payment = method.getPayment();
            if (payment != null) {
                pResults.putAll(payment.getParamMap());
            }
        }

    }

    public Map<String, String> getTransactionDetails(Map<String, String> pParams) {

        vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".getTransactionDetails").append(" - start").toString());

        String operationName = COMPONENT_NAME + ".getTransactionDetails";
        String operationParam = "RequestNumber: " + pParams.get(REQUEST_NUMBER);
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = false;
        Map<String, String> results = new HashMap<String, String>();
        try {
            ManageServicesStub serviceStub = getServiceStub();

            PaymentDetailsRequestDocument requestDocument = PaymentDetailsRequestDocument.Factory.newInstance();
            PaymentDetailsRequestDocument.PaymentDetailsRequest request = requestDocument.addNewPaymentDetailsRequest();
            request.setStrInputXML(createGetTransactionDetailsRequest(pParams));
            vlogDebug(new StringBuilder().append("Request: ").append(requestDocument).toString());

            PaymentDetailsRequestResponseDocument responseDocument = serviceStub.paymentDetailsRequest(requestDocument);
            vlogDebug(new StringBuilder().append("Response: ").append(responseDocument).toString());
            if (responseDocument != null) {
                Object result = unmarshal(JAXBContext.newInstance(Response.class),
                                responseDocument.getPaymentDetailsRequestResponse().getPaymentDetailsRequestResult());
                if (result instanceof Response) {
                    processGetTransactionDetailsResponse((Response) result, results);
                }
            }
            success = true;

        } catch (AxisFault af) {
            if (af.getDetail() != null) {
                vlogError(af.getDetail().toString());
            }
            vlogError(af, "Error");
        } catch (RemoteException e) {
            vlogError(e, "Error");
        } catch (PropertyException e) {
            vlogError(e, "Error");
        } catch (JAXBException e) {
            vlogError(e, "Error");
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".getTransactionDetails").append(" - exit").toString());

        return results;
    }

    private String createExternalOrderUpdateRequest(String pRequestNumber, String pOrderNumber, String pSuffix) throws JAXBException {

        ObjectFactory of = new ObjectFactory();
        CallMethod methodBean = of.createCallMethod(EXTERNAL_ORDER_UPDATE);

        Params params = of.createParams();
        params.getParam().add(of.createParam(SHARED_SECRET_KEY, getWebServiceConfig().getSecretKey()));
        params.getParam().add(of.createParam(REQUEST_NUMBER, pRequestNumber));
        params.getParam().add(of.createParam(ORDER_NUMBER, pOrderNumber));
        params.getParam().add(of.createParam(ORDER_TYPE, ORDER_TYPE_SO));
        params.getParam().add(of.createParam(COMPANY, getCompanyCode()));
        params.getParam().add(of.createParam(SUFFIX, pSuffix != null ? pSuffix : ""));
        methodBean.setParams(params);

        return marshal(JAXBContext.newInstance(WebRequests.class), of.createWebRequests(methodBean));

    }

    public boolean externalOrderUpdate(String pRequestNumber, String pOrderNumber) {

        vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".externalOrderUpdate").append(" - start").toString());

        String operationName = COMPONENT_NAME + ".externalOrderUpdate";
        String operationParam = "RequestNumber: " + pRequestNumber;
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = false;
        boolean orderUpdated = false;
        try {
            ManageServicesStub serviceStub = getServiceStub();

            ExternalOrderUpdateDocument requestDocument = ExternalOrderUpdateDocument.Factory.newInstance();
            ExternalOrderUpdateDocument.ExternalOrderUpdate request = requestDocument.addNewExternalOrderUpdate();
            request.setInputXML(createExternalOrderUpdateRequest(pRequestNumber, pOrderNumber, null));
            vlogDebug(new StringBuilder().append("Request: ").append(requestDocument).toString());

            ExternalOrderUpdateResponseDocument responseDocument = serviceStub.externalOrderUpdate(requestDocument);
            vlogDebug(new StringBuilder().append("Response: ").append(responseDocument).toString());
            if (responseDocument != null) {
                Object result = unmarshal(JAXBContext.newInstance(WebResponse.class),
                                responseDocument.getExternalOrderUpdateResponse().getExternalOrderUpdateResult());
                if (result instanceof WebResponse) {
                    WebResponse webResponse = (WebResponse) result;
                    Params responseParams = webResponse.getCallMethod().getParams();
                    if (responseParams != null) {
                        orderUpdated = YES.equals(responseParams.getParamValue(STATUS));
                    }
                }
            }
            success = true;

        } catch (AxisFault af) {
            if (af.getDetail() != null) {
                vlogError(af.getDetail().toString());
            }
            vlogError(af, "Error");
        } catch (RemoteException e) {
            vlogError(e, "Error");
        } catch (PropertyException e) {
            vlogError(e, "Error");
        } catch (JAXBException e) {
            vlogError(e, "Error");
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        vlogDebug(new StringBuilder().append(COMPONENT_NAME).append(".externalOrderUpdate").append(" - exit").toString());

        return orderUpdated;
    }

}
