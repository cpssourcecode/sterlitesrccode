package com.cps.mtr;

import java.util.LinkedHashSet;
import java.util.Set;

public class MTRInfo {

	private String mId;

	public void setId(String pId) {
		mId = pId;
	}

	public String getId() {
		return mId;
	}

	private String mUrl;

	public void setUrl(String pUrl) {
		mUrl = pUrl;
	}

	public String getUrl() {
		return mUrl;
	}

	private String mHeatNum;

	public void setHeatNum(String pHeatNum) {
		mHeatNum = pHeatNum;
	}

	public String getHeatNum() {
		return mHeatNum;
	}

	private Set<String> mFields = new LinkedHashSet<String>();

	public void setFields(Set<String> pFields) {
		mFields = pFields;
	}

	public Set<String> getFields() {
		return mFields;
	}

	public boolean equals(Object other) {

		if (!(other instanceof MTRInfo)) {
			return false;
		}
		MTRInfo that = (MTRInfo) other;
		return (getId() == null && that.getId() == null) || getId().equals(that.getId());

	}

	public int hashCode() {
		if (getId() == null) {
			return super.hashCode();
		}
		return getId().hashCode();
	}

}
