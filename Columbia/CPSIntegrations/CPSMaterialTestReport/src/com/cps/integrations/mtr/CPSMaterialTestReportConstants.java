package com.cps.integrations.mtr;

/**
 * CPSMaterialTestReportConstants
 * 
 * <h4>Description</h4> This class is used to hold constants for Material Test Report
 * integration.
 * 
 * @author Steve Neverov
 * 
 */
public final class CPSMaterialTestReportConstants {

	/**
	 * 
	 */
	private CPSMaterialTestReportConstants() {
		// restrict instantiation
	}
	
	public static final String PARAM_PAGE_SIZE = "pageSize";
	public static final String PARAM_FROM = "from";
	public static final String PARAM_CUSTOMER_ID = "customerId";
	public static final String PARAM_QUERY = "query";

	public static final String PARAM_HEAT_NUMBER = "heatNum";

	public static final String PARAM_SORT_FIELD = "sortField";
	public static final String PARAM_SORT_OPTION = "sortOrder";

	public static final String NUM_FOUND = "numFound";
	public static final String RESULTS = "results";
	public static final String ID = "id";
	public static final String FILENAME  = "filename";
	public static final String FIELD_1 = "field1";
	public static final String FIELD_2 = "field2";
	public static final String FIELD_3 = "field3";
	public static final String FIELD_4 = "field4";
	public static final String FIELD_5 = "field5";
	public static final String FIELD_6 = "field6";

}
