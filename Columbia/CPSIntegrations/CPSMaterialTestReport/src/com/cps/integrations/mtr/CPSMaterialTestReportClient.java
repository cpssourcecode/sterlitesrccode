package com.cps.integrations.mtr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.cps.mtr.MTRInfo;

import atg.core.util.StringUtils;
import atg.service.perfmonitor.PerformanceMonitor;
import vsg.exception.WebServiceException;
import vsg.util.json.VSGJsonReader;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSMaterialTestReportClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Steve Neverov
 *
 */
public class CPSMaterialTestReportClient extends WebServiceClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/mtr/CPSMaterialTestReportClient";

    /**
     * This method will return a list of packing slips
     *
     * @return
     * @throws WebServiceException
     */
    public Map<String, Object> requestMTRs(String pCustomerNumber, Map<String, String> pHeatNumbers, int pPageSize, int pStartIndex, String pSortField,
                    String pSortOption) throws WebServiceException {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestMTRs").append(" - start").toString());
        }

        final String operationName = COMPONENT_NAME + ".requestMTRs";
        final String operationParam = "customerNumber: " + pCustomerNumber;
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        Map<String, Object> result = new HashMap<String, Object>();
        List<MTRInfo> infos = new ArrayList<MTRInfo>();

        WebServiceConfig config = getWebServiceConfig();
        try {
            String url = new StringBuilder(config.getServiceAddress()).toString();

            int numFound = 0;
            if (pHeatNumbers != null && pHeatNumbers.size() > 0) {
                // send separate call for each heat
                for (Entry<String, String> heat : pHeatNumbers.entrySet()) {
                    if (StringUtils.isBlank(heat.getValue())) {
                        continue;
                    }

                    JsonObjectBuilder builder = Json.createObjectBuilder();
                    builder.add(CPSMaterialTestReportConstants.PARAM_PAGE_SIZE, pPageSize);
                    builder.add(CPSMaterialTestReportConstants.PARAM_FROM, pStartIndex);
                    builder.add(CPSMaterialTestReportConstants.PARAM_CUSTOMER_ID, pCustomerNumber);

                    if (!StringUtils.isBlank(pSortField)) {
                        builder.add(CPSMaterialTestReportConstants.PARAM_SORT_FIELD, pSortField);
                    }
                    if (!StringUtils.isBlank(pSortOption)) {
                        builder.add(CPSMaterialTestReportConstants.PARAM_SORT_OPTION, pSortOption);
                    }
                    JsonObjectBuilder queryBuilder = Json.createObjectBuilder();
                    queryBuilder.add(heat.getKey(), heat.getValue());

                    builder.add(CPSMaterialTestReportConstants.PARAM_QUERY, queryBuilder.build());

                    JsonObject params = builder.build();

                    if (isLoggingDebug()) {
                        logDebug(new StringBuilder().append("Request for heat " + heat.getKey() + ": ").append(url).append(" ; params: ").append(params)
                                        .toString());
                    }

                    JsonObject response = VSGJsonReader.readJsonObjectFromUrl(url, params, config.getTimeOut(), config.getSoTimeOut());

                    if (isLoggingDebug()) {
                        logDebug(new StringBuilder().append("Response: ").append(response).toString());
                    }

                    if (response != null) {
                        Integer pages = response.getInt(CPSMaterialTestReportConstants.NUM_FOUND);
                        numFound += pages;

                        JsonArray results = response.getJsonArray(CPSMaterialTestReportConstants.RESULTS);
                        for (int i = 0; i < results.size(); i++) {
                            JsonObject recordData = results.getJsonObject(i);
                            MTRInfo info = new MTRInfo();
                            try {
                                info.setId(String.valueOf(recordData.getInt(CPSMaterialTestReportConstants.ID)));

                                info.setUrl(new StringBuilder().append(config.getDocumentExtractionServiceAddress())
                                                .append(recordData.getString(CPSMaterialTestReportConstants.FILENAME)).toString());

                                info.getFields().add(recordData.getString(CPSMaterialTestReportConstants.FIELD_1));
                                info.getFields().add(recordData.getString(CPSMaterialTestReportConstants.FIELD_2));
                                info.getFields().add(recordData.getString(CPSMaterialTestReportConstants.FIELD_3));
                                info.getFields().add(recordData.getString(CPSMaterialTestReportConstants.FIELD_4));
                                info.getFields().add(recordData.getString(CPSMaterialTestReportConstants.FIELD_5));
                                info.getFields().add(recordData.getString(CPSMaterialTestReportConstants.FIELD_6));
                                infos.add(info);
                            } catch (Exception e) {
                                if (isLoggingError()) {
                                    logError("Wrong data: " + recordData.toString());
                                }
                            }
                        }
                    }
                }
            }

            result.put(CPSMaterialTestReportConstants.NUM_FOUND, numFound);
            result.put("mtrs", infos);

        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e);
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestMTRs").append(" - exit").toString());
        }

        return result;
    }

    public void testRequestMTRs() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestMTRs").append(" - start").toString());
        }

        Map<String, String> heatNumbers = new HashMap<String, String>();
        heatNumbers.put("heatNum1", "123456");
        try {
            Map<String, Object> reply = requestMTRs("101010", heatNumbers, 100000, 0, null, null);
            List<MTRInfo> mtrs = (List<MTRInfo>) reply.get("mtrs");
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Returned mtrs: ").append(mtrs).toString());
            }

        } catch (WebServiceException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestMTRs").append(" - exit").toString());
        }

    }

}
