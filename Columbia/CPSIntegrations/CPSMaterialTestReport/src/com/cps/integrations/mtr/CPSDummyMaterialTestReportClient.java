package com.cps.integrations.mtr;

import java.io.BufferedInputStream;
import java.io.File;

import atg.core.util.StringUtils;
import vsg.exception.WebServiceException;
import vsg.webservices.DummyClient;

/**
 * CPSDummyMaterialTestReportClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class CPSDummyMaterialTestReportClient extends DummyClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/mtr/CPSDummyMaterialTestReportClient";

    /** Test Heat Number property */
    private String mTestHeatNumber;

    /** Test Part Number property */
    private String mTestPartNumber;

    /** Test File Name property */
    private String mTestFileName;

    /**
     * 
     * @return
     */
    public String getTestHeatNumber() {
        return mTestHeatNumber;
    }

    /**
     * 
     * @param pTestHeatNumber
     */
    public void setTestHeatNumber(String pTestHeatNumber) {
        mTestHeatNumber = pTestHeatNumber;
    }

    /**
     * 
     * @return
     */
    public String getTestPartNumber() {
        return mTestPartNumber;
    }

    /**
     * 
     * @param pTestPartNumber
     */
    public void setTestPartNumber(String pTestPartNumber) {
        mTestPartNumber = pTestPartNumber;
    }

    /**
     * 
     * @return
     */
    public String getTestFileName() {
        return mTestFileName;
    }

    /**
     * 
     * @param pTestFileName
     */
    public void setTestFileName(String pTestFileName) {
        mTestFileName = pTestFileName;
    }

    /**
     * This method will return file path to a pdf generated from the input stream from the client.
     * 
     * @param pFileName
     * @return
     * @throws WebServiceException
     */
    public File requestDocument(String pFileName) throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestDocument").append(" - start").toString());
        }
        File pdf = null;
        if (!StringUtils.isBlank(pFileName)) {
            pdf = new File(new StringBuilder().append("Tim is Great! Here is your fileName:").append(pFileName).toString());
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestDocument").append(" - exit").toString());
        }
        return pdf;
    }

    /**
     * This method will return the input stream from the client.
     * 
     * @param pFileName
     * @return
     * @throws WebServiceException
     */
    public BufferedInputStream requestDocumentInputStream(String pFileName) throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestDocumentInputStream").append(" - start").toString());
        }
        BufferedInputStream inputStream = null;
        if (!StringUtils.isBlank(pFileName)) {
            inputStream = null;
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestDocumentInputStream").append(" - exit").toString());
        }
        return inputStream;
    }

}
