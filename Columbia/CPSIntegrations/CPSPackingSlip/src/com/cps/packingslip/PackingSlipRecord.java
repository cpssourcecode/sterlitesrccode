package com.cps.packingslip;

import java.util.List;

/**
 * Bean that represents information for packing slip record
 *
 * @author Steve Neverov
 */
public class PackingSlipRecord {

	private String mOrderNumber;

	public void setOrderNumber(String pOrderNumber) {
		mOrderNumber = pOrderNumber;
	}

	public String getOrderNumber() {
		return mOrderNumber;
	}

	private String mShipTo;

	public void setShipTo(String pShipTo) {
		mShipTo = pShipTo;
	}

	public String getShipTo() {
		return mShipTo;
	}

	public int getSize() {
		int size = 0;
		if (getPackingSlips() != null) {
			size = getPackingSlips().size();
		}
		return size;
	}

	private List<PackingSlipInfo> mPackingSlips;

	public void setPackingSlips(List<PackingSlipInfo> pPackingSlips) {
		mPackingSlips = pPackingSlips;
	}

	public List<PackingSlipInfo> getPackingSlips() {
		return mPackingSlips;
	}

}
