package com.cps.packingslip;

import vsg.order.VSGOrderInfo;

import java.util.Date;

/**
 * Bean that represents information for packing slip
 *
 * @author Steve Neverov
 */
public class PackingSlipInfo extends VSGOrderInfo {

	public PackingSlipInfo() {
		super();
	}

	private String mShipmentNumber;

	public void setShipmentNumber(String pShipmentNumber) {
		mShipmentNumber = pShipmentNumber;
	}

	public String getShipmentNumber() {
		return mShipmentNumber;
	}

	private String mCustomerPONumber;

	public void setCustomerPONumber(String pCustomerPONumber) {
		mCustomerPONumber = pCustomerPONumber;
	}

	public String getCustomerPONumber() {
		return mCustomerPONumber;
	}

	private Date mDate;

	public void setDate(Date pDate) {
		mDate = pDate;
	}

	public Date getDate() {
		return mDate;
	}

	private long mEpochDate;

	public void setEpochDate(long pEpochDate) {
		mEpochDate = pEpochDate;
	}

	public long getEpochDate() {
		return mEpochDate;
	}

	private double mAmount;

	public void setAmount(double pAmount) {
		mAmount = pAmount;
	}

	public double getAmount() {
		return mAmount;
	}

//	public String toString() {
//
//		StringBuilder result = new StringBuilder();
//		result.append("Id: ").append(getId()).append("; ");
//		return result.toString();
//
//	}

//	public int compareTo(Object pInfo) {
//		return Long.compare(((InvoiceInfo)pInfo).getEpochModifiedDate(), getEpochModifiedDate());
//	}

}
