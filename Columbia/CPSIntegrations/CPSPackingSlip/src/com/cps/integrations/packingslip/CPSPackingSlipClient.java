package com.cps.integrations.packingslip;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.cps.packingslip.PackingSlipInfo;
import com.cps.packingslip.PackingSlipRecord;

import atg.core.util.StringUtils;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import vsg.exception.WebServiceException;
import vsg.userprofiling.address.VSGAddressTools;
import vsg.util.json.VSGJsonReader;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSPackingSlipClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class CPSPackingSlipClient extends WebServiceClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/packingslip/CPSPackingSlipClient";

    private boolean mAddressFromRepository;

    /**
     * This method will return a list of packing slips
     *
     * @return
     * @throws WebServiceException
     */
    public Map<String, Object> requestPackingSlips(String pCustomerNumber, Calendar pStartDate, Calendar pEndDate, String pOrderNumber, String pPONumber,
                    List<String> pShipTo, int pPageSize, int pStartIndex, String pSortField, String pSortOption) throws WebServiceException {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestPackingSlips").append(" - start").toString());
        }

        Map<String, Object> result = new HashMap<String, Object>();
        List<PackingSlipRecord> records = new ArrayList<PackingSlipRecord>();

        final String operationName = COMPONENT_NAME + ".requestPackingSlips";
        final String operationParam = "customerNumber: " + pCustomerNumber;
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        WebServiceConfig config = getWebServiceConfig();
        try {

            String url = new StringBuilder(config.getHistoryServiceAddress()).toString();

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add(CPSPackingSlipConstants.PARAM_PAGE_SIZE, pPageSize);
            builder.add(CPSPackingSlipConstants.PARAM_FROM, pStartIndex);
            builder.add(CPSPackingSlipConstants.PARAM_CUSTOMER_ID, pCustomerNumber);
            if (!StringUtils.isBlank(pSortField)) {
                builder.add(CPSPackingSlipConstants.PARAM_SORT_FIELD, pSortField);
            } else {
                builder.add(CPSPackingSlipConstants.PARAM_SORT_FIELD, CPSPackingSlipConstants.ORDER_DATE);
            }
            if (!StringUtils.isBlank(pSortOption)) {
                builder.add(CPSPackingSlipConstants.PARAM_SORT_OPTION, pSortOption);
            } else {
                builder.add(CPSPackingSlipConstants.PARAM_SORT_OPTION, "DESC");
            }

            JsonObjectBuilder queryBuilder = Json.createObjectBuilder();
            if (!StringUtils.isBlank(pOrderNumber)) {
                queryBuilder.add(CPSPackingSlipConstants.PARAM_ORDER_NUMBER, Integer.valueOf(pOrderNumber));
            }
            if (!StringUtils.isBlank(pPONumber)) {
                queryBuilder.add(CPSPackingSlipConstants.PARAM_PO_NUMBER, pPONumber);
            }
            if (pStartDate != null) {
                queryBuilder.add(CPSPackingSlipConstants.PARAM_DATE_FROM, pStartDate.getTimeInMillis());
            }
            if (pEndDate != null) {
                queryBuilder.add(CPSPackingSlipConstants.PARAM_DATE_TO, pEndDate.getTimeInMillis());
            }

            if (pShipTo != null && pShipTo.size() > 0) {
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                for (String shipTo : pShipTo) {
                    arrayBuilder.add(shipTo);
                }
                queryBuilder.add(CPSPackingSlipConstants.PARAM_SHIP_TO_NUMBER, arrayBuilder.build());
            }
            queryBuilder.add(CPSPackingSlipConstants.PARAM_BILL_TO_NUMBER, pCustomerNumber);

            builder.add(CPSPackingSlipConstants.PARAM_QUERY, queryBuilder.build());
            JsonObject params = builder.build();

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request: ").append(url).append(" ; params: ").append(params).toString());
            }

            JsonObject response = VSGJsonReader.readJsonObjectFromUrl(url, params, config.getTimeOut(), config.getSoTimeOut());

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Response: ").append(response).toString());
            }

            if (response != null) {
                Integer pages = response.getInt(CPSPackingSlipConstants.NUM_FOUND);
                result.put(CPSPackingSlipConstants.NUM_FOUND, pages);

                JsonArray results = response.getJsonArray(CPSPackingSlipConstants.RESULTS);
                for (int i = 0; i < results.size(); i++) {
                    JsonObject recordData = results.getJsonObject(i);
                    PackingSlipRecord record = new PackingSlipRecord();
                    record.setOrderNumber(Integer.valueOf(recordData.getInt(CPSPackingSlipConstants.ORDER_NUMBER)).toString());
                    record.setShipTo(Integer.valueOf(recordData.getInt(CPSPackingSlipConstants.SHIP_TO_NUMBER)).toString());

                    List<PackingSlipInfo> infos = new ArrayList<PackingSlipInfo>();
                    JsonArray packingSlips = recordData.getJsonArray(CPSPackingSlipConstants.PACKING_SLIPS);
                    for (int j = 0; j < packingSlips.size(); j++) {
                        JsonObject data = packingSlips.getJsonObject(j);
                        PackingSlipInfo info = new PackingSlipInfo();
                        info.setId(Integer.valueOf(data.getInt(CPSPackingSlipConstants.DOCID)).toString());
                        info.setUrl(new StringBuilder().append(config.getDocumentExtractionServiceAddress()).append(data.getString(CPSPackingSlipConstants.URL))
                                        .toString());
                        Long epochDate = data.getJsonNumber(CPSPackingSlipConstants.ORDER_DATE).longValue();
                        info.setEpochDate(epochDate);
                        info.setDate(new Date(epochDate));
                        info.setOrderNumber(Integer.valueOf(data.getInt(CPSPackingSlipConstants.ORDER_NUMBER)).toString());
                        String shipTo = Integer.valueOf(data.getInt(CPSPackingSlipConstants.SHIP_TO_NUMBER)).toString();
                        info.setShipTo(shipTo);
                        info.setShipToName(data.getString(CPSPackingSlipConstants.SHIP_TO_NAME));
                        if (isAddressFromRepository()) {
                            RepositoryItem shipAddress = VSGAddressTools.getShippingNameByShipTo(shipTo);
                            if (shipAddress != null) {
                                info.setShipToName((String) shipAddress.getPropertyValue(CPSPackingSlipConstants.PROP_ADDRESS_LINE1));
                            }
                        }
                        info.setShipmentNumber(Integer.valueOf(data.getInt(CPSPackingSlipConstants.SHIPMENT_NUMBER)).toString());
                        info.setCustomerPONumber(data.getString(CPSPackingSlipConstants.CUSTOMER_PO_NUMBER));
                        infos.add(info);
                    }
                    record.setPackingSlips(infos);
                    records.add(record);
                }

                result.put("packingSlips", records);
            }

        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e);
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestPackingSlips").append(" - exit").toString());
        }

        return result;
    }

    public void testRequestPackingSlips() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestPackingSlips").append(" - start").toString());
        }

        List<String> shipto = new ArrayList<String>();
        shipto.add("110145");

        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.DAY_OF_MONTH, -120);
        // startDate.setTimeInMillis(1422856800000l);
        Calendar endDate = Calendar.getInstance();
        // endDate.setTimeInMillis(1422856800000l);

        try {
            Map<String, Object> reply = requestPackingSlips("110144", startDate, endDate, null, null, shipto, 5, 0, null, null);
            List<PackingSlipRecord> packingSlips = (List<PackingSlipRecord>) reply.get("packingSlips");
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Returned packing slips: ").append(packingSlips).toString());
            }

        } catch (WebServiceException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestPackingSlips").append(" - exit").toString());
        }

    }

    public boolean isAddressFromRepository() {
        return mAddressFromRepository;
    }

    public void setAddressFromRepository(boolean pAddressFromRepository) {
        mAddressFromRepository = pAddressFromRepository;
    }

}
