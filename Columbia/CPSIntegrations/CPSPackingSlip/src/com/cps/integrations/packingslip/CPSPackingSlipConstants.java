package com.cps.integrations.packingslip;

/**
 * CPSPackingSlipConstants
 * 
 * <h4>Description</h4> This class is used to hold constants for Packing Slip
 * integration.
 * 
 * <h4>Notes</h4>
 * <p>
 * Note before using this one have a look at /atg/userprofiling/PropertyManager
 * an instance of atg.commerce.profile.CommercePropertyManager.
 * <p>
 * Constants class identifying item descriptor names and property names.
 * <p>
 * A naming convention is used to aid the developer in finding the particular
 * property or item descriptor name. All item descriptor names will be prefixed
 * with <code>IN_</code>. All property names will be prefixed with
 * <code>PN_${ItemDescriptorName}</code> where
 * <code>${ItemDescriptorName}</code> is the name of the item descriptor whose
 * property name you are accessing. The item descriptor name in this case may be
 * abbreviated to keep the variable names reasonably short. Just be sure to use
 * the same abbreviated name for all property names of that item descriptor.
 * 
 * @author Tim Harshbarger
 * 
 */
public final class CPSPackingSlipConstants {

	/**
	 * 
	 */
	private CPSPackingSlipConstants() {
		// restrict instantiation
	}
	
	// Fields
	public static final String PACKING_SLIP_LINK = "packingSlipLink";
	public static final String PACKING_SLIP_NUMBER = "packingSlipNumber";
	public static final String PACKING_SLIP_CS = "packingSlipCS";
	public static final String PACKING_SLIP_DATE = "packingSlipDate";

	public static final String PARAM_PAGE_SIZE = "pageSize";
	public static final String PARAM_FROM = "from";
	public static final String PARAM_CUSTOMER_ID = "customerId";
	public static final String PARAM_QUERY = "query";
	public static final String PARAM_ORDER_NUMBER = "orderNum";
	public static final String PARAM_SHIP_TO_NUMBER = "shipToNum";
	public static final String PARAM_DATE_FROM = "orderDateFrom";
	public static final String PARAM_DATE_TO = "orderDateTo";
	public static final String PARAM_PO_NUMBER = "purchaseOrderNum";
	public static final String PARAM_SHIP_TO_ADDRESS = "shipToAddress";
	public static final String PARAM_SORT_FIELD = "sortField";
	public static final String PARAM_SORT_OPTION = "sortOrder";
	public static final String PARAM_BILL_TO_NUMBER = "billToNum";

	public static final String NUM_FOUND = "numFound";
	public static final String RESULTS = "results";
	public static final String URL = "url";
	public static final String DOCID  = "DOCID";
	public static final String ORDER_DATE = "ORDER_DATE";
	public static final String SHIP_TO_NUMBER = "SHIP_TO_NUMBER";
	public static final String SHIP_TO_NAME = "SHIP_TO_NAME";
	public static final String ORDER_NUMBER = "ORDER_NUMBER";
	public static final String CUSTOMER_PO_NUMBER = "CUSTOMER_PO_NUMBER";
	public static final String SHIPMENT_NUMBER = "SHIPMENT_NUMBER";
	public static final String PACKING_SLIPS = "PACKING_SLIPS";
	
	public static final String PROP_ADDRESS_LINE1 = "address1";

}
