package com.cps.integrations.packingslip;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vsg.exception.WebServiceException;
import vsg.webservices.DummyClient;

/**
 * CPSDummyPackingSlipClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class CPSDummyPackingSlipClient extends DummyClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/packingslip/CPSDummyPackingSlipClient";

    /** Test Packing Slip Link property */
    private String mTestPackingSlipLink;

    /** Test Packing Slip Number property */
    private String mTestPackingSlipNumber;

    /** Test Packing Slip CS property */
    private String mTestPackingSlipCS;

    /**
     * @return
     */
    public String getTestPackingSlipLink() {
        return mTestPackingSlipLink;
    }

    /**
     * @param pTestPackingSlipLink
     */
    public void setTestPackingSlipLink(String pTestPackingSlipLink) {
        mTestPackingSlipLink = pTestPackingSlipLink;
    }

    /**
     * @return
     */
    public String getTestPackingSlipNumber() {
        return mTestPackingSlipNumber;
    }

    /**
     * @param pTestPackingSlipNumber
     */
    public void setTestPackingSlipNumber(String pTestPackingSlipNumber) {
        mTestPackingSlipNumber = pTestPackingSlipNumber;
    }

    /**
     * @return
     */
    public String getTestPackingSlipCS() {
        return mTestPackingSlipCS;
    }

    /**
     * @param pTestPackingSlipCS
     */
    public void setTestPackingSlipCS(String pTestPackingSlipCS) {
        mTestPackingSlipCS = pTestPackingSlipCS;
    }

    /**
     * This method will return a list containing a map of packing slip data based on a start and end date, CS, and search parameter
     * 
     * @param pStartDate
     * @param pEndDate
     * @param pCustomerSpecificIdentifier
     * @param pSearchParameter
     * @return
     * @throws WebServiceException
     */
    public List<Map<String, Object>> requestPackingSlips(Calendar pStartDate, Calendar pEndDate, String pCustomerSpecificIdentifier, String pSearchParameter)
                    throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestPackingSlips").append(" - start").toString());
        }
        ArrayList<Map<String, Object>> packingSlips = new ArrayList<Map<String, Object>>();
        // build packing slips for varying dates
        packingSlips.add(generatePackingSlipMap(5));
        packingSlips.add(generatePackingSlipMap(10));
        packingSlips.add(generatePackingSlipMap(11));
        packingSlips.add(generatePackingSlipMap(15));
        packingSlips.add(generatePackingSlipMap(20));
        packingSlips.add(generatePackingSlipMap(21));
        packingSlips.add(generatePackingSlipMap(30));
        packingSlips.add(generatePackingSlipMap(31));
        packingSlips.add(generatePackingSlipMap(40));
        packingSlips.add(generatePackingSlipMap(41));
        packingSlips.add(generatePackingSlipMap(50));
        packingSlips.add(generatePackingSlipMap(51));
        packingSlips.add(generatePackingSlipMap(55));
        packingSlips.add(generatePackingSlipMap(56));

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestPackingSlips").append(" - exit").toString());
        }
        return packingSlips;
    }

    /**
     * @param pDateVariance
     * @return
     */
    private Map<String, Object> generatePackingSlipMap(int pDateVariance) {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".generatePackingSlipMap").append(" - start").toString());
        }

        // build dummy data set to be returned to application
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        Map<String, Object> packingSlipMap = new HashMap<String, Object>();
        packingSlipMap.put(CPSPackingSlipConstants.PACKING_SLIP_LINK, getTestPackingSlipLink());
        packingSlipMap.put(CPSPackingSlipConstants.PACKING_SLIP_NUMBER, getTestPackingSlipNumber() + pDateVariance);
        packingSlipMap.put(CPSPackingSlipConstants.PACKING_SLIP_CS, getTestPackingSlipCS() + pDateVariance);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -pDateVariance);
            packingSlipMap.put(CPSPackingSlipConstants.PACKING_SLIP_DATE, calendar);
        } catch (Exception e) {
            vlogError(e, "Error");
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".generatePackingSlipMap").append(" - exit").toString());
        }
        return packingSlipMap;
    }
}
