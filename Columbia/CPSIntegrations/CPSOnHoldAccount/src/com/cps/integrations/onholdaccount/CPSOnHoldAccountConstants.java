package com.cps.integrations.onholdaccount;

/**
 * CPSOnHoldAccountConstants
 * 
 * @author Steve Neverov
 * 
 */
public final class CPSOnHoldAccountConstants {

	// Fields
	public static final String CUSTOMER_NUMBER = "customerNumber";

	public static final String IS_ON_HOLD = "isOnHold";

}
