package com.cps.integrations.onholdaccount;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;

import atg.core.util.StringUtils;
import atg.service.perfmonitor.PerformanceMonitor;
import jp5503b2.bssv.e1.columbiapipe.com.*;
import vsg.exception.WebServiceException;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSOnHoldAccountClient
 * <p>
 * <h4>Description</h4>
 * <p>
 * <h4>Notes</h4>
 *
 * @author Steve Neverov
 */
public class CPSOnHoldAccountClient extends WebServiceClient {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/integrations/onholdaccount/CPSOnHoldAccountClient";

    private String mTestValue;

    public String getTestValue() {
        return mTestValue;
    }

    public void setTestValue(String pTestValue) {
        mTestValue = pTestValue;
    }

    /**
     * This method will return a on hold status details
     *
     * @param pParams
     * @return
     * @throws WebServiceException
     */
    public Map<String, Object> requestOnHoldStatus(Map<String, Object> pParams) throws WebServiceException {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOnHoldStatus").append(" - start").toString());
        }

        Map<String, Object> reply = new HashMap<String, Object>();

        final String operationName = COMPONENT_NAME + ".requestOnHoldStatus";
        final String operationParam = "customerNumber: " + pParams.get(CPSOnHoldAccountConstants.CUSTOMER_NUMBER);
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        WebServiceConfig config = getWebServiceConfig();
        try {
            OnHoldAccountManagerServiceStub service = new OnHoldAccountManagerServiceStub(config.getServiceAddress());
            service._getServiceClient().addHeader(createSecurityHeader(config.getUserId(), config.getPassword()));
            // service._getServiceClient().getOptions().setTimeOutInMilliSeconds(getWebServiceConfig().getTimeOut());
            service._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, config.getTimeOut());
            service._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getSoTimeOut());

            GetOnHoldAccountDocument request = GetOnHoldAccountDocument.Factory.newInstance();
            OnHoldAccountVO onHoldAccountVO = OnHoldAccountVO.Factory.newInstance();
            if (!StringUtils.isBlank(getTestValue())) {
                onHoldAccountVO.setCustomerNumber(Integer.valueOf(getTestValue()));
            } else {
                onHoldAccountVO.setCustomerNumber(Integer.valueOf((String) pParams.get(CPSOnHoldAccountConstants.CUSTOMER_NUMBER)));
            }
            request.setGetOnHoldAccount(onHoldAccountVO);

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request: ").append(request.toString()).toString());
            }

            GetOnHoldAccountResponseDocument response = service.getOnHoldAccount(request);
            if (!StringUtils.isBlank(getTestValue())) {
                throw new BusinessServiceExceptionException("test error");
            }

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Response: ").append(response).toString());
            }

            if (response != null) {
                ShowOnHoldAccountVO result = response.getGetOnHoldAccountResponse();
                reply.put(CPSOnHoldAccountConstants.IS_ON_HOLD, Boolean.valueOf(result.getIsAccountOnHold()));
            }

        } catch (AxisFault af) {
            if (isLoggingError()) {
                if (af.getDetail() != null) {
                    String faultString = af.getDetail().toString();
                    logError(faultString);
                }
                logError(af);
            }
            success = false;
            throw new WebServiceException(af.getMessage());
        } catch (RemoteException | BusinessServiceExceptionException e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e.getMessage());
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOnHoldStatus").append(" - exit").toString());
        }
        return reply;

    }

    public void testRequestOnHoldStatus() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestOnHoldStatus").append(" - start").toString());
        }

        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(CPSOnHoldAccountConstants.CUSTOMER_NUMBER, "100002");

            Map<String, Object> reply = requestOnHoldStatus(params);
            Boolean status = (Boolean) reply.get(CPSOnHoldAccountConstants.IS_ON_HOLD);
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Returned status: ").append(status).toString());
            }

        } catch (WebServiceException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestOnHoldStatus").append(" - exit").toString());
        }

    }

}
