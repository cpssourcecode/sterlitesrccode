package com.cps.integrations.orderstatus;

import java.util.HashMap;

import vsg.webservices.DummyClient;

/**
 * CPSDummyOrderStatusClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author David Mednikov
 *
 */
public class CPSDummyOrderStatusClient extends DummyClient {

    /** Test Price property */
    private String mTestOrderStatus;

    /**
     * 
     * @return
     */
    public HashMap<String, Object> getOrderStatus(int orderNumber) {
        return new HashMap<String, Object>();
    }

    /**
     * 
     * @param pTestOrderStatus
     */
    public void setTestOrderStatus(String pTestOrderStatus) {
        mTestOrderStatus = pTestOrderStatus;
    }

    public String getTestOrderStatus() {
        return mTestOrderStatus;
    }
}
