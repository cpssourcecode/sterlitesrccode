package com.cps.integrations.orderstatus;

import java.math.BigDecimal;

/**
 * CPSOrderStatusConstants
 * 
 * <h4>Description</h4> This class is used to hold constants for Pricing
 * integration.
 * 
 * <h4>Notes</h4>
 * <p>
 * Note before using this one have a look at /atg/userprofiling/PropertyManager
 * an instance of atg.commerce.profile.CommercePropertyManager.
 * <p>
 * Constants class identifying item descriptor names and property names.
 * <p>
 * A naming convention is used to aid the developer in finding the particular
 * property or item descriptor name. All item descriptor names will be prefixed
 * with <code>IN_</code>. All property names will be prefixed with
 * <code>PN_${ItemDescriptorName}</code> where
 * <code>${ItemDescriptorName}</code> is the name of the item descriptor whose
 * property name you are accessing. The item descriptor name in this case may be
 * abbreviated to keep the variable names reasonably short. Just be sure to use
 * the same abbreviated name for all property names of that item descriptor.
 * 
 * @author David Mednikov
 * 
 */
public final class CPSOrderStatusConstants {

	/**
	 * 
	 */
	private CPSOrderStatusConstants() {
		// restrict instantiation
	}
	
	// Fields
	public static final String ORDER_DATE="orderDate";
	public static final String NUMBER_OF_ITEMS="numberOfItems";
	public static final String TAX_TOTAL_AMOUNT="taxTotalAmount";
	public static final String ITEM_TOTAL_AMOUNT="itemTotalAmount";
	public static final String SHIPPING_HANDLING_TOTAL_AMOUNT="shippingHandlingTotalAmount";
	public static final String ORDER_TOTAL_AMOUNT="orderTotalAmount";
	
	//Ship To Fields
	public static final String SHIP_TO_ADDRESS_LINE_1="addressLine1";
	public static final String SHIP_TO_ADDRESS_LINE_2="addressLine2";
	public static final String SHIP_TO_ADDRESS_LINE_3="addressLine3";
	public static final String SHIP_TO_ADDRESS_LINE_4="addressLine4";
	public static final String SHIP_TO_CITY="city";
	public static final String SHIP_TO_COUNTRY_CODE="countryCode";
	public static final String SHIP_TO_COUNTY_CODE="countyCode";
	public static final String SHIP_TO_MAILING_NAME="mailingName";
	public static final String SHIP_TO_POSTAL_CODE="postalCode";
	public static final String SHIP_TO_STATE_CODE="stateCode";
	
	//Line Details
	public static final String AMOUNT_EXTENDED_PRICE="amountExtendedPrice";
	public static final String AMOUNT_PRICE_PER_UNIT="amountPricePerUnit";
	public static final String ATTACHMENT_DETAIL="attachmentDetail";
	public static final String IDENTIFIER_SHORT_TERM="identifierShortTerm";
	public static final String IDENTIFIER_2ND_ITEM="identifier2NdItem";
	public static final String IDENTIFIER_3RD_ITEM="identified3RdItem";
	public static final String ITEM_DESCRIPTION="itemDescription";
	public static final String LINE_NUMBER="lineNumber";
	public static final String LINE_STATUS_DESCRIPTION="lineStatusDescription";
	public static final String LINE_TYPE="linetype";
	public static final String RELATED_KIT="relatedKit";
	public static final String TRANSACTION_QUANTITY="transactionQuantity";

	//Custom properties
	public static final String DISABLED="disabled";
}
