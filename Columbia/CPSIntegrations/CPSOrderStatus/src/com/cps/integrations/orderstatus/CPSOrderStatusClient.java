package com.cps.integrations.orderstatus;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;

import atg.core.util.StringUtils;
import atg.repository.Repository;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.service.perfmonitor.PerformanceMonitor;
import jp554201.bssv.e1.columbiapipe.com.*;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSOrderStatusClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author David Mednikov
 *
 */
public class CPSOrderStatusClient extends WebServiceClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/orderstatus/CPSOrderStatusClient";
    private final static String W_LINETYPE = "W";
    private final static String D_LINETYPE = "D";
    private final static String K_STOCKING_TYPE = "K";
    private final static String O_STOCKING_TYPE = "O";
    private final static String PRODUCT = "product";
    private final static String RQL_SELECT_BY_ID = "id = ?0";
    private static final String PARAMETER_STOCKING_TYPE = "stockingType";

    private Repository mRepository;

    private void processOrderDetails(Map<String, Object> results, ShowOrderStatusHeaderVO outputObject) {

        Calendar orderDate = outputObject.getOrderDate();
        Integer numberOfItems = outputObject.getNumberOfItems();
        BigDecimal itemTotalAmount = outputObject.getItemTotalAmount();
        BigDecimal shippingHandlingTotalAmount = outputObject.getShippingHandlingTotalAmount();
        BigDecimal taxTotalAmount = outputObject.getTaxTotalAmount();
        BigDecimal orderTotalAmount = outputObject.getOrderTotalAmount();

        if (orderDate != null) {
            results.put(CPSOrderStatusConstants.ORDER_DATE, orderDate.getTime());
        }
        if (numberOfItems != null) {
            results.put(CPSOrderStatusConstants.NUMBER_OF_ITEMS, numberOfItems);
        }
        if (itemTotalAmount != null) {
            results.put(CPSOrderStatusConstants.ITEM_TOTAL_AMOUNT, itemTotalAmount);
        }
        if (shippingHandlingTotalAmount != null) {
            results.put(CPSOrderStatusConstants.SHIPPING_HANDLING_TOTAL_AMOUNT, shippingHandlingTotalAmount);
        }
        if (taxTotalAmount != null) {
            results.put(CPSOrderStatusConstants.TAX_TOTAL_AMOUNT, taxTotalAmount);
        }
        if (orderTotalAmount != null) {
            results.put(CPSOrderStatusConstants.ORDER_TOTAL_AMOUNT, orderTotalAmount);
        }

    }

    private void processLineItems(Map<String, Object> results, ShowOrderStatusHeaderVO outputObject) {

        List<Map<String, Object>> lineDetails = new ArrayList<Map<String, Object>>();
        ShowOrderStatusDetailVO[] orderDetails = outputObject.getDetailArray();
        if (orderDetails != null && orderDetails.length > 0) {
            for (int i = 0; i < orderDetails.length; i++) {
                Map<String, Object> lineItem = new HashMap<String, Object>();
                ShowOrderStatusDetailVO detail = orderDetails[i];
                BigDecimal amountExtendedPrice = detail.getAmountExtendedPrice();
                BigDecimal amountPricePerUnit = detail.getAmountPricePerUnit();
                String attachmentDetail = detail.getAttachmentDetail().trim();
                int identifierShortTerm = detail.getIdentifierShortItem();
                String identifier2NdItem = detail.getIdentifier2NdItem().trim();
                String identifier3RdItem = detail.getIdentifier3RdItem().trim();
                String itemDescription = detail.getItemDescription().trim();
                BigDecimal lineNumber = detail.getLineNumber();
                String lineStatusDescription = detail.getLineStatusDescription().trim();
                String linetype = detail.getLinetype().trim();
                String relatedKit = detail.getRelatedKit();
                int transactionQuantity = detail.getTransactionQuantity();
                if (amountExtendedPrice != null) {
                    lineItem.put(CPSOrderStatusConstants.AMOUNT_EXTENDED_PRICE, amountExtendedPrice);
                }
                if (amountPricePerUnit != null) {
                    lineItem.put(CPSOrderStatusConstants.AMOUNT_PRICE_PER_UNIT, amountPricePerUnit);
                }
                if (attachmentDetail != null) {
                    lineItem.put(CPSOrderStatusConstants.ATTACHMENT_DETAIL, attachmentDetail);
                }
                lineItem.put(CPSOrderStatusConstants.IDENTIFIER_SHORT_TERM, identifierShortTerm);
                if (identifier2NdItem != null) {
                    lineItem.put(CPSOrderStatusConstants.IDENTIFIER_2ND_ITEM, identifier2NdItem);
                }
                if (identifier3RdItem != null) {
                    lineItem.put(CPSOrderStatusConstants.IDENTIFIER_3RD_ITEM, identifier3RdItem);
                }
                if (itemDescription != null) {
                    lineItem.put(CPSOrderStatusConstants.ITEM_DESCRIPTION, itemDescription);
                }
                if (lineNumber != null) {
                    lineItem.put(CPSOrderStatusConstants.LINE_NUMBER, lineNumber);
                }
                if (lineStatusDescription != null) {
                    lineItem.put(CPSOrderStatusConstants.LINE_STATUS_DESCRIPTION, lineStatusDescription);
                }
                if (linetype != null) {
                    lineItem.put(CPSOrderStatusConstants.LINE_TYPE, linetype);
                }
                if (relatedKit != null) {
                    lineItem.put(CPSOrderStatusConstants.RELATED_KIT, relatedKit);
                }
                lineItem.put(CPSOrderStatusConstants.TRANSACTION_QUANTITY, transactionQuantity);
                lineDetails.add(lineItem);
            }
        }

        lineDetails.sort(Comparator.comparingDouble(m -> ((BigDecimal) m.get(CPSOrderStatusConstants.LINE_NUMBER)).doubleValue()));
        disableItems(lineDetails);
        deleteChildPrice(lineDetails);
        results.put("lineDetails", lineDetails);
    }

    private void disableItems(List<Map<String, Object>> lineDetails) {
        // Disable D linetype
        for (Map<String, Object> line : lineDetails) {
            String linetype = (String) line.get(CPSOrderStatusConstants.LINE_TYPE);
            if (linetype.equalsIgnoreCase(D_LINETYPE)) {
                line.put(CPSOrderStatusConstants.DISABLED, true);
            }
        }

        // Disable W linetype
        for (Map<String, Object> line : lineDetails) {
            BigDecimal lineNumber = (BigDecimal) line.get(CPSOrderStatusConstants.LINE_NUMBER);
            String linetype = (String) line.get(CPSOrderStatusConstants.LINE_TYPE);
            if (lineNumber.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0 && W_LINETYPE.equalsIgnoreCase(linetype)) {
                disableChildLines(lineDetails, line, lineNumber);
            }
        }

        // Disable K stocking types
        for (Map<String, Object> line : lineDetails) {
            BigDecimal lineNumber = (BigDecimal) line.get(CPSOrderStatusConstants.LINE_NUMBER);
            String stockingType = getStockingType(String.valueOf(line.get(CPSOrderStatusConstants.IDENTIFIER_SHORT_TERM)));
            if (lineNumber.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0
                            && (K_STOCKING_TYPE.equalsIgnoreCase(stockingType) || O_STOCKING_TYPE.equalsIgnoreCase(stockingType))) {
                disableChildLines(lineDetails, line, lineNumber);
            }
        }
    }

    private String getStockingType(String itemId) {
        try {
            RepositoryView repositoryView = getRepository().getView(PRODUCT);
            RqlStatement statement = RqlStatement.parseRqlStatement(RQL_SELECT_BY_ID);
            Object params[] = { itemId };
            RepositoryItem[] items = statement.executeQuery(repositoryView, params);

            if (items != null && items.length > 0) {
                return (String) items[0].getPropertyValue(PARAMETER_STOCKING_TYPE);
            }
        } catch (Exception e) {
            logError("Error occurred during quering Stocking Type by Product id", e);
        }
        return null;
    }

    private void disableChildLines(List<Map<String, Object>> lineDetails, Map<String, Object> line, BigDecimal lineNumber) {
        for (Map<String, Object> childLine : lineDetails) {
            BigDecimal childLineNumber = (BigDecimal) childLine.get(CPSOrderStatusConstants.LINE_NUMBER);
            int parentId = (int) line.get(CPSOrderStatusConstants.IDENTIFIER_SHORT_TERM);
            int childRelatedItemId = getRelatedKit((String) childLine.get(CPSOrderStatusConstants.RELATED_KIT));
            if (childLineNumber.intValue() == lineNumber.intValue() || parentId == childRelatedItemId) {
                childLine.put(CPSOrderStatusConstants.DISABLED, true);
            }
        }
    }

    private void deleteChildPrice(List<Map<String, Object>> lineDetails) {
        for (Map<String, Object> line : lineDetails) {
            String relatedKit = (String) line.get(CPSOrderStatusConstants.RELATED_KIT);
            if (StringUtils.isNotBlank(relatedKit)) {
                line.put(CPSOrderStatusConstants.AMOUNT_PRICE_PER_UNIT, null);
                line.put(CPSOrderStatusConstants.AMOUNT_EXTENDED_PRICE, null);
            }
        }
    }

    private int getRelatedKit(String relatedKit) {
        int value = 0;

        try {
            value = Integer.parseInt(relatedKit.trim());
        } catch (Exception e) {
            vlogDebug("Empty Related Kit string", e);
        }

        return value;
    }

    private void processShippingDetails(Map<String, Object> results, ShowOrderStatusHeaderVO outputObject) {
        ShowSalesShipTo shipToObject = outputObject.getShipTo();
        String addressLine1 = shipToObject.getAddressLine1().trim();
        String addressLine2 = shipToObject.getAddressLine2().trim();
        String addressLine3 = shipToObject.getAddressLine3().trim();
        String addressLine4 = shipToObject.getAddressLine4().trim();
        String city = shipToObject.getCity().trim();
        String countryCode = shipToObject.getCountryCode().trim();
        String countyCode = shipToObject.getCountyCode().trim();
        String mailingName = shipToObject.getMailingName().trim();
        String postalCode = shipToObject.getPostalCode().trim();
        String stateCode = shipToObject.getStateCode().trim();

        if (addressLine1 != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_1, addressLine1);
        }
        if (addressLine2 != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_2, addressLine2);
        }
        if (addressLine3 != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_3, addressLine3);
        }
        if (addressLine4 != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_ADDRESS_LINE_4, addressLine4);
        }
        if (city != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_CITY, city);
        }
        if (countryCode != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_COUNTRY_CODE, countryCode);
        }
        if (countyCode != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_COUNTY_CODE, countyCode);
        }
        if (mailingName != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_MAILING_NAME, mailingName);
        }
        if (postalCode != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_POSTAL_CODE, postalCode);
        }
        if (stateCode != null) {
            results.put(CPSOrderStatusConstants.SHIP_TO_STATE_CODE, stateCode);
        }
    }

    public Map<String, Object> getOrderStatus(int pOrderNumber) {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".getOrderStatus").append(" - start").toString());
        }

        final String operationName = COMPONENT_NAME + ".getOrderStatus";
        final String operationParam = "OrderNumber: " + pOrderNumber;
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        OrderStatusManagerServiceStub serviceStub;
        Map<String, Object> results = new HashMap<String, Object>();
        WebServiceConfig config = getWebServiceConfig();
        try {
            serviceStub = new OrderStatusManagerServiceStub(config.getServiceAddress());
            // serviceStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(config.getTimeOut());
            serviceStub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, config.getTimeOut());
            serviceStub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getSoTimeOut());
            serviceStub._getServiceClient().getOptions().setProperty(HTTPConstants.CHUNKED, Boolean.FALSE);

            // Setting the WSSE header. reusing from OrderExportClient
            OMElement securityHeader = createSecurityHeader(config.getUserId(), config.getPassword());
            serviceStub._getServiceClient().addHeader(securityHeader);

            OrderStatusVO orderInput = OrderStatusVO.Factory.newInstance();
            orderInput.setOrderNumber(pOrderNumber);
            GetOrderStatusDocument input = GetOrderStatusDocument.Factory.newInstance();
            input.setGetOrderStatus(orderInput);

            GetOrderStatusResponseDocument output = serviceStub.getOrderStatus(input);

            ShowOrderStatusHeaderVO outputObject = output.getGetOrderStatusResponse();

            processOrderDetails(results, outputObject);
            processLineItems(results, outputObject);
            processShippingDetails(results, outputObject);

        } catch (AxisFault af) {
            if (isLoggingError()) {
                if (af.getDetail() != null) {
                    String faultString = af.getDetail().toString();
                    logError(faultString);
                }
                logError(af);
            }
            success = false;
            results.put("CONNECTION_ERROR1", true);
        } catch (RemoteException e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            results.put("CONNECTION_ERROR2", true);
        } catch (BusinessServiceExceptionException e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            results.put("rawData", "connection error");
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".getOrderStatus").append(" - exit").toString());
        }

        return results;
    }

    public Repository getRepository() {
        return mRepository;
    }

    public void setRepository(Repository pRepository) {
        this.mRepository = pRepository;
    }
}
