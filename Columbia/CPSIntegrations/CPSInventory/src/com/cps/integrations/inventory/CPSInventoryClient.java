package com.cps.integrations.inventory;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.lang.StringUtils;

import com.cps.inventory.InventoryAvailabilityInfo;

import atg.service.perfmonitor.PerformanceMonitor;
import jp554101.bssv.e1.columbiapipe.com.AvailabilityVO;
import jp554101.bssv.e1.columbiapipe.com.BranchVO;
import jp554101.bssv.e1.columbiapipe.com.BusinessServiceExceptionException;
import jp554101.bssv.e1.columbiapipe.com.GetItemAvailabilityDocument;
import jp554101.bssv.e1.columbiapipe.com.GetItemAvailabilityResponseDocument;
import jp554101.bssv.e1.columbiapipe.com.ItemAvailabilityManagerServiceStub;
import jp554101.bssv.e1.columbiapipe.com.ItemAvailabilityVO;
import jp554101.bssv.e1.columbiapipe.com.ItemGroupCustomer;
import jp554101.bssv.e1.columbiapipe.com.ProductIn;
import jp554101.bssv.e1.columbiapipe.com.ShowItemAvailabilityVO;
import vsg.exception.WebServiceException;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSInventoryClient
 * <p/>
 * <h4>Description</h4>
 * <p/>
 * <h4>Notes</h4>
 *
 * @author Steve Neverov
 */
public class CPSInventoryClient extends WebServiceClient {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/integrations/pricing/CPSInventoryClient";

    /**
     * This method will return a quantity from the integration
     *
     * @param pSkuId
     * @return
     * @throws WebServiceException
     */
    public List<InventoryAvailabilityInfo> requestInventory(String pCustomerNumber, String pSkuId, boolean pDisplayPickUpLocations, long pOrderedQty,
                    String pBranchId) throws WebServiceException {

        final String operationName = COMPONENT_NAME + ".requestInventory";
        final String operationParam = pCustomerNumber + "; " + pSkuId + "; " + pOrderedQty;
        long startTime = System.currentTimeMillis();
        vlogDebug("Inventory Client Start Time : {0}", new Date(startTime));
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - start").toString());
        }
        
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        List<InventoryAvailabilityInfo> infos = new ArrayList<InventoryAvailabilityInfo>();
        WebServiceConfig config = getWebServiceConfig();
        try {
            ItemAvailabilityManagerServiceStub service = new ItemAvailabilityManagerServiceStub(config.getServiceAddress());
            service._getServiceClient().addHeader(createSecurityHeader(config.getUserId(), config.getPassword()));
            // service._getServiceClient().getOptions().setTimeOutInMilliSeconds(getWebServiceConfig().getTimeOut());
            service._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, config.getTimeOut());
            service._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getSoTimeOut());

            GetItemAvailabilityDocument request = GetItemAvailabilityDocument.Factory.newInstance();
            ItemAvailabilityVO itemAvailabilityVO = ItemAvailabilityVO.Factory.newInstance();
            ProductIn product = ProductIn.Factory.newInstance();
            ItemGroupCustomer item = ItemGroupCustomer.Factory.newInstance();
            item.setItemId(Integer.valueOf(pSkuId));
            product.setItem(item);
            product.setQuantityOrdered(BigDecimal.valueOf(pOrderedQty));
            itemAvailabilityVO.setProduct(product);
            String displayPickUpLocations = pDisplayPickUpLocations ? "1" : "0";
            itemAvailabilityVO.setDisplayPickUpLocations(displayPickUpLocations);
            itemAvailabilityVO.setCustomerNumber(Integer.valueOf(pCustomerNumber));
            if (StringUtils.isNotBlank(pBranchId)) {
                itemAvailabilityVO.setBranchPlant("1");
            }
            request.setGetItemAvailability(itemAvailabilityVO);

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request: ").append(request.toString()).toString());
            }

            GetItemAvailabilityResponseDocument response = service.getItemAvailability(request);

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Response: ").append(response.toString()).toString());
            }

            ShowItemAvailabilityVO showItemAvailabilityVO = response.getGetItemAvailabilityResponse();

            AvailabilityVO[] results = showItemAvailabilityVO.getAvailabilityArray();
            if (results != null && results.length > 0) {
                for (AvailabilityVO availabilityVO : results) {
                    InventoryAvailabilityInfo info = new InventoryAvailabilityInfo();
                    info.setProductId(pSkuId);
                    BigDecimal qty = availabilityVO.getQuantityAvailable();
                    if (qty != null) {
                        info.setQuantity(qty.longValue());
                    }
                    BranchVO branch = availabilityVO.getBranch();
                    if (branch != null) {
                        info.setBranchDefault(branch.getIsDefaultBranch());
                        info.setBranchId(branch.getBranchPlant().trim());
                        if(StringUtils.isNotBlank(pBranchId) && pBranchId.equals(branch.getBranchPlant().trim())) {
                            info.setBranchDefault(true);
                        }
                    }
                    info.setLeadTime(availabilityVO.getLeadTime());
                    infos.add(info);
                }
            }
        } catch (AxisFault af) {
            if (isLoggingError()) {
                if (af.getDetail() != null) {
                    String faultString = af.getDetail().toString();
                    logError(faultString);
                }
                logError(af);
            }
            success = false;
            throw new WebServiceException(af.getMessage());
        } catch (RemoteException | BusinessServiceExceptionException e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e.getMessage());
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - exit").toString());
        }
        long endTime = System.currentTimeMillis();
        long timeInSecs = (endTime - startTime)/1000;
        vlogDebug("End Time : {0} ", new Date(endTime));
        vlogDebug("Total time difference between start and end time {0} seconds",timeInSecs);
        return infos;
    }

    /**
     * This method will return a map of quantities from a list of skus based on the branch looking for inventory
     *
     * @param pSkuIds
     * @param pStoreId
     * @return
     * @throws WebServiceException
     */
    public Map<String, Integer> requestInventory(List<String> pSkuIds, String pStoreId) throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - start").toString());
        }
        Map<String, Integer> inventoryMap = testData(pSkuIds);
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - exit").toString());
        }
        return inventoryMap;
    }

    private Map<String, Integer> testData(List<String> pSkuIds) {
        HashMap<String, Integer> testData = new HashMap<String, Integer>();
        Random rand = new Random();
        for (String sku : pSkuIds) {
            testData.put(sku, rand.nextInt(2));
        }
        return testData;
    }

    public void testRequestInventory() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestInventory").append(" - start").toString());
        }
        try {
            List<InventoryAvailabilityInfo> infos = requestInventory("1000", "19", true, 1, null);
            if (infos != null) {
                for (InventoryAvailabilityInfo info : infos) {
                    info.toString();
                }
            }

        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestInventory").append(" - exit").toString());
        }

    }

}
