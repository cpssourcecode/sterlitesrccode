package com.cps.integrations.inventory;

import java.util.Collection;
import java.util.HashMap;

import vsg.exception.WebServiceException;
import vsg.webservices.DummyClient;

/**
 * CPSDummyInventoryClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Steve Neverov
 *
 */
public class CPSDummyInventoryClient extends DummyClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/inventory/CPSDummyInventoryClient";

    /** Test Quantity property */
    private int mTestQuantity;

    /**
     * 
     * @return
     */
    public int getTestQuantity() {
        return mTestQuantity;
    }

    /**
     * 
     * @param pTestQuantity
     */
    public void setTestQuantity(int pTestQuantity) {
        mTestQuantity = pTestQuantity;
    }

    /**
     * This method will return a quantity just to return a value
     * 
     * @param pSkuId
     * @return
     * @throws WebServiceException
     */
    public double requestInventory(String pSkuId) throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - start").toString());
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - exit").toString());
        }
        return getTestQuantity();
    }

    /**
     * This method will return a map of quantities from a list of skus at a specific location
     * 
     * @param pSkuIds
     * @param pStoreId
     * @return
     * @throws WebServiceException
     */
    public HashMap<String, Integer> requestInventory(Collection<String> pSkuIds, String pStoreId) throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - start").toString());
        }
        HashMap<String, Integer> inventoryMap = null;
        if (pSkuIds != null && pSkuIds.size() > 0) {
            inventoryMap = new HashMap<String, Integer>();
            for (String skuId : pSkuIds) {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append("Adding quantity:").append(getTestQuantity()).append(" to map for skuId:").append(skuId).toString());
                }
                inventoryMap.put(skuId, getTestQuantity());
            }
        }
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestInventory").append(" - exit").toString());
        }
        return inventoryMap;
    }
}
