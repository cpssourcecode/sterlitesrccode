package com.cps.integrations.inventory;

/**
 * CPSInventoryConstants
 * 
 * <h4>Description</h4> This class is used to hold constants for Inventory (Will Call)
 * integration.
 * 
 * @author Steve Neverov
 * 
 */
public final class CPSInventoryConstants {

	/**
	 * 
	 */
	private CPSInventoryConstants() {
		// restrict instantiation
	}
	
	// Fields
	public static final String TEMP_CONSTANT = "TimIsGreat";
}
