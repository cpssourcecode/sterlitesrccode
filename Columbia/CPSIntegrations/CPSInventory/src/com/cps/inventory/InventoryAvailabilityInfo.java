package com.cps.inventory;

/**
 * Bean that represents information returned from inventory WS
 *
 * @author Steve Neverov
 */
public class InventoryAvailabilityInfo {

	private String mProductId;

	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}

	public String getProductId() {
		return mProductId;
	}

	private long mQuantity;

	public void setQuantity(long pQuantity) {
		mQuantity = pQuantity;
	}

	public long getQuantity() {
		return mQuantity;
	}

	private String mBranchId;

	public void setBranchId(String pBranchId) {
		mBranchId = pBranchId;
	}

	public String getBranchId() {
		return mBranchId;
	}

	private boolean mBranchDefault;

	public void setBranchDefault(boolean pBranchDefault) {
		mBranchDefault = pBranchDefault;
	}

	public boolean isBranchDefault() {
		return mBranchDefault;
	}

	private int mLeadTime;

	public void setLeadTime(int pLeadTime) {
		mLeadTime = pLeadTime;
	}

	public int getLeadTime() {
		return mLeadTime;
	}

	public String toString() {

		StringBuilder result = new StringBuilder();
		result.append("ProductId: ").append(getProductId()).append("; ")
				.append("Quantity: ").append(getQuantity()).append("; ")
				.append("BranchId: ").append(getBranchId()).append("; ")
				.append("Lead Time: ").append(getLeadTime()).append("; ")
				.append("Is default: ").append(isBranchDefault());
		return result.toString();

	}

}
