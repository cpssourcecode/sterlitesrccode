package com.cps.integrations.pricing;

import static com.cps.integrations.pricing.CPSPricingConstants.CALL_ERROR;
import static com.cps.integrations.pricing.CPSPricingConstants.CALL_RESPONDED;
import static com.cps.integrations.pricing.CPSPricingConstants.CALL_SENT;
import static com.cps.integrations.pricing.CPSPricingConstants.LOCATION_DEFAULT_BRANCH_ID;
import static com.cps.integrations.pricing.CPSPricingConstants.PRICE_EXTENDED_DOMESTIC_CLOSE;
import static com.cps.integrations.pricing.CPSPricingConstants.PRICE_EXTENDED_DOMESTIC_OPEN;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.service.perfmonitor.PerformanceMonitor;
import vsg.webservices.WebServiceClient;

//import vsg.util.AsyncHttpClientUtils;

/**
 * CPSPricingClient
 * <p/>
 * <h4>Description</h4>
 * <p/>
 * <h4>Notes</h4>
 *
 * @author Andy Porter
 */
public class CPSPricingClientMultiStatic extends WebServiceClient implements PricingClientMulti {

    private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(CPSPricingClientMultiStatic.class);

    private int mMaxCalls = 30;

    private static final Double DEFAULT_PRICE = 30.0;

    private String mTestJDEAddressId = "102585";
    private int mDebugLevel = 2;
    private String mCacertsPath = "/usr/java/jdk1.7.0_97/jre/lib/security/cacerts";
    private String mCacertsPassword;

    private boolean mIsJdeCallsEnabled;
    private double mDefaultPrice;
    private boolean testMode;

    private final String[] mProductsArray = new String[] { "627289", "608080", "608089", "608081", "608090", "608082", "655797", "608091", "619637", "621701",
            "608083", "655796", "608094", "608092", "608079", "627176", "607426", "608093", "608078", "557165" };

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/integrations/pricing/CPSPricingClientMulti";
    private boolean enableWebservice;

    /**
     * @param pBusinessUnit
     *            BranchId
     * @param pEntityId
     *            jdeAddressNumberCS
     * @param pItemId
     *            productId
     * @return
     */
    private static String getBody(String pBusinessUnit, String pEntityId, String pItemId, String pUserId, String pPassword) {
        if (null == pBusinessUnit) {
            pBusinessUnit = "100";
        }
        if (null == pEntityId) {
            pEntityId = "102585";
        }
        StringBuilder stringBuilder = new StringBuilder()
                        .append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:orac=\"http://oracle.e1.bssv.JP420000/\">\n")
                        .append("   <soapenv:Header>\n")
                        .append("      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">\n")
                        .append("         <wsse:UsernameToken>\n").append("            <wsse:Username>").append(pUserId).append("</wsse:Username>\n")
                        .append("            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">")
                        .append(pPassword).append("</wsse:Password>\n").append("         </wsse:UsernameToken>\n").append("      </wsse:Security>\n")
                        .append("   </soapenv:Header>\n").append("   <soapenv:Body>\n").append("      <orac:getCustomerItemPrice>\n")
                        .append("         <businessUnit>").append(pBusinessUnit).append("</businessUnit>\n").append("         <product>\n")
                        .append("            <item>\n").append("               <itemId>").append(pItemId).append("</itemId>\n").append("            </item>\n")
                        .append("         </product>\n").append("         <transactionQuantity>1.0</transactionQuantity>\n").append("         <customer>\n")
                        .append("            <shipTo>\n").append("               <entityId>").append(pEntityId).append("</entityId>\n")
                        .append("            </shipTo>\n").append("         </customer>\n").append("      </orac:getCustomerItemPrice>\n")
                        .append("   </soapenv:Body>\n").append("</soapenv:Envelope>");
        return stringBuilder.toString();
    }

    private static Request createRequest(String pBusinessUnit, String pEntityId, String pItemId, String pServiceAddress, String pUserId, String pPassword,
                    boolean isLoggingDebug) {
        String body = getBody(pBusinessUnit, pEntityId, pItemId, pUserId, pPassword);
        if (isLoggingDebug) {
            mLogging.logDebug(new StringBuilder().append("Request for pBusinessUnit=").append(pBusinessUnit).append(", jdeAddress=").append(pEntityId)
                            .append(", productId=").append(pItemId).append(" sent.").toString());
            mLogging.logDebug(new StringBuilder().append("body_").append(pItemId).append(":").append(body).toString());
        }

        byte[] requestbody = body.getBytes();
        final Request request = new RequestBuilder("POST").setUrl(pServiceAddress).setContentLength(requestbody.length).setBody(requestbody)
                        .addHeader("Accept-Encoding", "gzip,deflate").addHeader("Content-Type", "text/xml;charset=UTF-8")
                        .addHeader("SOAPAction", "\"http://oracle.e1.bssv.JP420000//getCustomerItemPrice\"").build();
        return request;
    }

    @Override
    public Map<String, Double> getCustomerItemPriceAsync(Set<String> pProducts, String pProfileId, long pQuantity, String pBranch, boolean isLoggingDebug)
                    throws InterruptedException, ExecutionException, IOException {
        Map<String, Double> results;
        // TODO: Test mode enabled for fetching the prices from ATG
        if (isJdeCallsEnabled()) {
            final int maxCallsLocal = getMaxCalls();
            String serviceAddress = getWebServiceConfig().getServiceAddress();
            String userId = getWebServiceConfig().getUserId();
            String password = getWebServiceConfig().getPassword();
            String cacertsPath = getCacertsPath();
            String cacertsPassword = getCacertsPassword();

            results = getCustomerItemPriceAsync(pProducts, pProfileId, pBranch, maxCallsLocal, serviceAddress, userId, password, cacertsPath, cacertsPassword,
                            isLoggingDebug);
        } else {
            results = new HashMap<>();
            for (String product : pProducts) {
                results.put(product, getDefaultPrice());
            }
        }

        return results;
    }

    public static Map<String, Double> getCustomerItemPriceAsync(Set<String> pProducts, String pProfileId, String pBranch, int pMaxCalls, String pServiceAddress,
                    String pUserId, String pPassword, String pCacertsPath, String pCacertsPassword, boolean isLoggingDebug)
                    throws IOException, ExecutionException, InterruptedException {

        long startTimeMethod = System.currentTimeMillis();

        HashMap<String, Double> results = new HashMap<String, Double>();
        final int maxCallsLocal = pMaxCalls;
        if (0 == maxCallsLocal) {
            results = new HashMap<String, Double>();
            for (String product : pProducts) {
                results.put(product, DEFAULT_PRICE);
            }
        } else {
            final int productsListSize = pProducts.size();

            HashMap<Future<Response>, String> futureMap = new HashMap<Future<Response>, String>();
            ConcurrentHashMap<String, WsPriceResponse> priceResponsesMap = new ConcurrentHashMap<String, WsPriceResponse>();

            long startTimeRound = System.currentTimeMillis();

            final String operationName = COMPONENT_NAME + ".getCustomerItemPriceAsync";
            PerformanceMonitor.startOperation(operationName);
            try (AsyncHttpClient asyncHttpClient = new AsyncHttpClient();) {

                List<Future<Response>> futureCalled = new ArrayList<Future<Response>>(maxCallsLocal);

                int productsRoundCalled = 0;
                int productsCalled = 0;
                for (String productId : pProducts) {
                    {
                        final Request request = createRequest(pBranch, pProfileId, productId, pServiceAddress, pUserId, pPassword, isLoggingDebug);
                        Future<Response> future = asyncHttpClient.executeRequest(request);
                        futureCalled.add(future);
                        futureMap.put(future, productId);

                        WsPriceResponse wsPriceResponse = new WsPriceResponse(productId, CALL_SENT);
                        long startTime = System.currentTimeMillis();
                        wsPriceResponse.setStartTime(startTime);
                        priceResponsesMap.put(productId, wsPriceResponse);

                        productsRoundCalled++;
                        productsCalled++;
                    }
                    // vlogDebug("Request sent productId={0}, callNumber={1}, packSize={2}, threadId={3}, profileId={4}", productId, productsRoundCalled,
                    // maxCallsLocal, threadId, pProfileId);

                    if (productsRoundCalled == maxCallsLocal || productsCalled == productsListSize) {
                        // vlogDebug("Request pack sent: productsRoundCalled={0}, productsCalled={1} packSize={2}, threadId={3}, profileId={4}",
                        // productsRoundCalled, productsCalled, maxCallsLocal, threadId, pProfileId);

                        for (Future<Response> futureCall : futureCalled) {
                            String prodIdCall = futureMap.get(futureCall);
                            WsPriceResponse wsPriceResponseCall = priceResponsesMap.get(prodIdCall);

                            Response response = futureCall.get();
                            long endTime = System.currentTimeMillis();
                            String respBody = response.getResponseBody().toString();
                            if (isLoggingDebug) {
                                mLogging.logDebug(new StringBuilder().append("RESPONSE_").append(prodIdCall).append(":").append(respBody).toString());
                            }
                            Double priceValue = getPriceFromBody(respBody, prodIdCall, isLoggingDebug);
                            if (null == priceValue) {
                                wsPriceResponseCall.setStatus(CALL_ERROR);
                                wsPriceResponseCall.setEndTime(endTime);
                            } else {
                                wsPriceResponseCall.setStatus(CALL_RESPONDED);
                                wsPriceResponseCall.setEndTime(endTime);
                                wsPriceResponseCall.setPriceValue(priceValue);
                            }
                        }
                        futureCalled = new ArrayList<Future<Response>>(maxCallsLocal);
                    }

                }
                long endTimeTimeRound = System.currentTimeMillis();
                long totalTimeRound = endTimeTimeRound - startTimeRound;
                if (isLoggingDebug) {
                    mLogging.logDebug(new StringBuilder().append("Completed getCustomerItemPriceAsync method ").append(" time={").append(totalTimeRound)
                                    .append("}(ms)").toString());
                }

                Set<String> prodResultSet = priceResponsesMap.keySet();
                for (String prodId : prodResultSet) {
                    WsPriceResponse priceResponse = priceResponsesMap.get(prodId);

                    double priceValue = priceResponse.getPriceValue();
                    results.put(prodId, priceValue);

                    long totalTime = priceResponse.getEndTime() - priceResponse.getStartTime();

                    if (isLoggingDebug) {
                        mLogging.logDebug(new StringBuilder().append("Completed productId={").append(prodId).append("}, priceValue={").append(priceValue)
                                        .append("}, startTime={").append(priceResponse.getStartTime()).append("}, time={").append(totalTime).append("}(ms)")
                                        .toString());
                    }
                }

                long endTimeTimeMethod = System.currentTimeMillis();
                long totalTimeMethod = endTimeTimeMethod - startTimeMethod;
                if (isLoggingDebug) {
                    mLogging.logDebug(new StringBuilder().append("Completed getCustomerItemPriceAsync method ").append(" time={").append(totalTimeMethod)
                                    .append("}(ms)").toString());
                }
            } finally {
                PerformanceMonitor.endOperation(operationName);
            }
        }
        return results;
    }

    private static Double getPriceFromBody(String pBody, String pProductId, boolean isLoggingDebug) {
        Double priceValue = null;

        int indexStart = pBody.indexOf(PRICE_EXTENDED_DOMESTIC_OPEN);
        int indexEnd = pBody.indexOf(PRICE_EXTENDED_DOMESTIC_CLOSE);
        int length = PRICE_EXTENDED_DOMESTIC_OPEN.length();

        if (indexStart != -1 && indexEnd != -1) {
            String priceStr = pBody.substring(indexStart + length, indexEnd);
            if (isLoggingDebug) {
                mLogging.logDebug(new StringBuilder().append("ProductId:").append(pProductId).append(", priceStr:").append(priceStr).toString());
            }

            try {
                priceValue = Double.valueOf(priceStr);
            } catch (NumberFormatException e) {
                mLogging.logError(e);
            }
        }
        return priceValue;
    }

    public void testCallAsynchronous() throws InterruptedException, ExecutionException, IOException {
        final String[] productsArray = new String[] { "627289", "608080", "608089", "608081", "608090", "608082", "655797", "608091", "619637", "621701",
                "608083", "655796", "608094", "608092", "608079", "627176", "607426", "608093", "608078", "557165" };
        Set<String> products = new HashSet<String>(Arrays.asList(productsArray));

        String userId = "bssvdvatg1";
        String serviceAddress = "https://172.18.0.13:9087/PY900/SalesOrderManager";
        String cacertsPath = "/usr/java/jdk1.7.0_97/jre/lib/security/cacerts";
        String cacertsPassword = getCacertsPassword();

        getCustomerItemPriceAsync(products, "102585", LOCATION_DEFAULT_BRANCH_ID, 30, serviceAddress, userId, serviceAddress, cacertsPath, cacertsPassword,
                        true);
    }

    /*
     * public static void main(String[] args) throws IOException, InterruptedException, ExecutionException { testCallAsynchronous(); }
     */

    public boolean isEnableWebservice() {
        return enableWebservice;
    }

    public void setEnableWebservice(boolean enableWebservice) {
        this.enableWebservice = enableWebservice;
    }

    /**
     * Gets mMaxCalls.
     *
     * @return Value of mMaxCalls.
     */
    public int getMaxCalls() {
        return mMaxCalls;
    }

    /**
     * Sets new mMaxCalls.
     *
     * @param pMaxCalls
     *            New value of mMaxCalls.
     */
    public void setMaxCalls(int pMaxCalls) {
        mMaxCalls = pMaxCalls;
    }

    public String getTestJDEAddressId() {
        return mTestJDEAddressId;
    }

    public void setTestJDEAddressId(String pTestJDEAddressId) {
        mTestJDEAddressId = pTestJDEAddressId;
    }

    public int getDebugLevel() {
        return mDebugLevel;
    }

    public void setDebugLevel(int pDebugLevel) {
        mDebugLevel = pDebugLevel;
    }

    public String getCacertsPath() {
        return mCacertsPath;
    }

    public void setCacertsPath(String pCacertsPath) {
        mCacertsPath = pCacertsPath;
    }

    public String getCacertsPassword() {
        return mCacertsPassword;
    }

    public void setCacertsPassword(String pCacertsPassword) {
        mCacertsPassword = pCacertsPassword;
    }

    public boolean isJdeCallsEnabled() {
        return mIsJdeCallsEnabled;
    }

    public void setJdeCallsEnabled(boolean pJdeCallsEnabled) {
        mIsJdeCallsEnabled = pJdeCallsEnabled;
    }

    public double getDefaultPrice() {
        return mDefaultPrice;
    }

    public void setDefaultPrice(double pDefaultPrice) {
        mDefaultPrice = pDefaultPrice;
    }

    /**
     * @return the testMode
     */
    public boolean isTestMode() {
        return testMode;
    }

    /**
     * @param testMode
     *            the testMode to set
     */
    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

}
