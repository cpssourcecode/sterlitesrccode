package com.cps.integrations.pricing;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * @author Andy Porter
 */
public interface PricingClientMulti {
	Map<String, Double> getCustomerItemPriceAsync(Set<String> pProducts, String pProfileId, long pQuantity, String pBranch, boolean isLoggingDebug) throws InterruptedException, ExecutionException, IOException;
}
