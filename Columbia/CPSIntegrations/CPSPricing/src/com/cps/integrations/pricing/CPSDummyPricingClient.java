package com.cps.integrations.pricing;

import java.util.HashMap;

import vsg.webservices.DummyClient;

/**
 * CPSDummyPricingClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author David Mednikov
 *
 */
public class CPSDummyPricingClient extends DummyClient {

    /** Test Price property */
    private double mTestPrice;

    /**
     * 
     * @return
     */
    public double getTestPrice() {
        return mTestPrice;
    }

    /**
     * 
     * @param pTestPrice
     */
    public void setTestPrice(double pTestPrice) {
        mTestPrice = pTestPrice;
    }

    public HashMap<String, Object> getCustomerItemPrice(String productId, String customerId, int quantity) {
        return null;
    }
}
