package com.cps.integrations.pricing;

/**
 * CPSPricingConstants
 * <p/>
 * <h4>Description</h4> This class is used to hold constants for Pricing                                                                       N
 * integration.
 * <p/>
 * <h4>Notes</h4>
 * <p/>
 * Note before using this one have a look at /atg/userprofiling/PropertyManager
 * an instance of atg.commerce.profile.CommercePropertyManager.
 * <p/>
 * Constants class identifying item descriptor names and property names.
 * <p/>
 * A naming convention is used to aid the developer in finding the particular
 * property or item descriptor name. All item descriptor names will be prefixed
 * with <code>IN_</code>. All property names will be prefixed with
 * <code>PN_${ItemDescriptorName}</code> where
 * <code>${ItemDescriptorName}</code> is the name of the item descriptor whose
 * property name you are accessing. The item descriptor name in this case may be
 * abbreviated to keep the variable names reasonably short. Just be sure to use
 * the same abbreviated name for all property names of that item descriptor.
 *
 * @author David Mednikov
 */
public final class CPSPricingConstants {

	/**
	 *
	 */
	private CPSPricingConstants() {
		// restrict instantiation
	}

	// Fields
	public static final String COST_EXTENDED_DOMESTIC = "costExtendedDomestic";
	public static final String COST_EXTENDED_FOREIGN = "costExtendedForeign";
	public static final String COST_UNIT_DOMESTIC = "costUnitDomestic";
	public static final String COST_UNIT_FOREIGN = "costUnitForeign";
	public static final String CURRENCY_CODE = "currencyCode";
	public static final String CURRENCY_MODE_CODE = "currencyModeCode";
	public static final String DATE_PRICE_EFFECTIVE = "datePriceEffective";
	public static final String PRICE_EXTENDED_DOMESTIC = "priceExtendedDomestic";
	public static final String PRICE_EXTENDED_FOREIGN = "priceExtendedForeign";
	public static final String PRICE_LIST_DOMESTIC = "priceListDomestic";
	public static final String PRICE_LIST_FOREIGN = "priceListForeign";
	public static final String PRICE_UNIT_DOMESTIC = "priceUnitDomestic";
	public static final String PRICE_UNIT_FOREIGN = "priceUnitForeign";
	public static final String QUANTITY_ORDERED = "quantityOrdered";
	public static final String UNIT_OF_MEASURE_CODE_PRICING = "unitOfMeasureCodePricing";
	public static final String UNIT_OF_MEASURE_CODE_TRANSACTION = "unitOfMeasureCodeTransaction";
	public static final long DEFAULT_QTY = 1L;
	public static final String LOCATION_DEFAULT_BRANCH_ID = "100";

	public static final int CALL_INITIAL = -1;
	public static final int CALL_SENT = 0;
	public static final int CALL_ERROR = 1;
	public static final int CALL_RESPONDED = 2;

	public static final String PRICE_EXTENDED_DOMESTIC_OPEN = "<priceUnitDomestic>";
	public static final String PRICE_EXTENDED_DOMESTIC_CLOSE = "</priceUnitDomestic>";

    public static final String ANONYMOUS_USER_PRICE_MATRIX = "lp005";
    public static final String DEFAULT_LISTPRICE_LIST = "defaultListPriceList";

}
