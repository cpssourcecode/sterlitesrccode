package com.cps.integrations.pricing;

import static com.cps.integrations.pricing.CPSPricingConstants.CALL_INITIAL;

/**
 * @author Andy Porter
 */
public class WsPriceResponse {
	public WsPriceResponse(String pProductId, int pStatus) {
		mStatus = pStatus;
		mProductId = pProductId;
		mPriceValue = 0.0d;
	}

	private int mStatus = CALL_INITIAL;
	private String mProductId = null;
	private Double mPriceValue = 0.0d; // get from.PRICE_EXTENDED_DOMESTIC;
	private long mStartTime;
	private long mEndTime;

	public int getStatus() {
		return mStatus;
	}

	public void setStatus(int pStatus) {
		mStatus = pStatus;
	}

	public String getProductId() {
		return mProductId;
	}

	public void setProductId(String pProductId) {
		mProductId = pProductId;
	}

	public Double getPriceValue() {
		return mPriceValue;
	}

	public void setPriceValue(Double pPriceValue) {
		mPriceValue = pPriceValue;
	}

	public long getStartTime() {
		return mStartTime;
	}

	public void setStartTime(long pStartTime) {
		mStartTime = pStartTime;
	}

	public long getEndTime() {
		return mEndTime;
	}

	public void setEndTime(long pEndTime) {
		mEndTime = pEndTime;
	}

}
