package com.cps.integrations.order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import vsg.exception.WebServiceException;
import vsg.webservices.DummyClient;

/**
 * CPSDummyOrderClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class CPSDummyOrderClient extends DummyClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/order/CPSDummyOrderClient";

    /** Test Order Total property */
    private double mTestOrderTotal;

    /** Test Order Link property */
    private String mTestOrderLink;

    /** Test Order Number property */
    private String mTestOrderNumber;

    /** Test Order CS property */
    private String mTestOrderCS;

    /** Test Order Status property */
    private String mTestOrderStatus;

    /**
     * 
     * @return
     */
    public double getTestOrderTotal() {
        return mTestOrderTotal;
    }

    /**
     * 
     * @param pTestOrderTotal
     */
    public void setTestOrderTotal(double pTestOrderTotal) {
        mTestOrderTotal = pTestOrderTotal;
    }

    /**
     * @return
     */
    public String getTestOrderLink() {
        return mTestOrderLink;
    }

    /**
     * @param pTestOrderLink
     */
    public void setTestOrderLink(String pTestOrderLink) {
        mTestOrderLink = pTestOrderLink;
    }

    /**
     * @return
     */
    public String getTestOrderNumber() {
        return mTestOrderNumber;
    }

    /**
     * @param pTestOrderNumber
     */
    public void setTestOrderNumber(String pTestOrderNumber) {
        mTestOrderNumber = pTestOrderNumber;
    }

    /**
     * @return
     */
    public String getTestOrderCS() {
        return mTestOrderCS;
    }

    /**
     * @param pTestOrderCS
     */
    public void setTestOrderCS(String pTestOrderCS) {
        mTestOrderCS = pTestOrderCS;
    }

    /**
     * @return
     */
    public String getTestOrderStatus() {
        return mTestOrderStatus;
    }

    /**
     * @param pTestOrderStatus
     */
    public void setTestOrderStatus(String pTestOrderStatus) {
        mTestOrderStatus = pTestOrderStatus;
    }

    /**
     * This method will return a list containing a map of order data based on a start and end date, CS, and search parameter
     * 
     * @param pStartDate
     * @param pEndDate
     * @param pCustomerSpecificIdentifier
     * @param pSearchParameter
     * @return
     * @throws WebServiceException
     */
    public List<HashMap<String, Object>> requestOrders(Calendar pStartDate, Calendar pEndDate, String pCustomerSpecificIdentifier, String pSearchParameter)
                    throws WebServiceException {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrders").append(" - start").toString());
        }
        ArrayList<HashMap<String, Object>> orders = new ArrayList<HashMap<String, Object>>();
        // build orders for varying dates
        // pending orders
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 5));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 10));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 11));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 20));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 21));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 30));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 31));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 40));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 41));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 50));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PENDING, 51));

        // processing orders
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 5));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 10));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 11));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 20));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 21));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 30));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 31));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 40));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 41));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 50));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_PROCESSING, 51));

        // shipped orders
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 5));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 10));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 11));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 20));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 21));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 30));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 31));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 40));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 41));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 50));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_SHIPPED, 51));

        // declined orders
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 5));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 10));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 11));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 20));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 21));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 30));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 31));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 40));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 41));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 50));
        orders.add(generateMap(CPSOrderConstants.ORDER_STATUS_DECLINED, 51));

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrders").append(" - exit").toString());
        }
        return orders;
    }

    /**
     * @param pStatus
     * @param pDateVariance
     * @return
     */
    private HashMap<String, Object> generateMap(String pStatus, int pDateVariance) {
        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".generateMap").append(" - start").toString());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        HashMap<String, Object> orderMap = new HashMap<String, Object>();
        orderMap.put(CPSOrderConstants.ORDER_TOTAL, getTestOrderTotal() + pDateVariance);
        // orderMap.put(CPSOrderConstants.ORDER_LINK, getTestOrderLink());
        orderMap.put(CPSOrderConstants.ORDER_NUMBER, getTestOrderNumber() + pDateVariance);
        // orderMap.put(CPSOrderConstants.ORDER_CS, getTestOrderCS()+pDateVariance);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, -pDateVariance);
            orderMap.put(CPSOrderConstants.ORDER_DATE, calendar);
        } catch (Exception e) {
            vlogError(e, "Error");
        }
        // orderMap.put(CPSOrderConstants.ORDER_STATUS, pStatus);

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".generateMap").append(" - exit").toString());
        }
        return orderMap;
    }

}
