package com.cps.integrations.order;

import static com.cps.integrations.order.CPSOrderConstants.ACCOUNT_EXTENDED_ADDRESS_NUMBER_CLOSE;
import static com.cps.integrations.order.CPSOrderConstants.ACCOUNT_EXTENDED_ADDRESS_NUMBER_OPEN;
import static com.cps.integrations.order.CPSOrderConstants.ACCOUNT_EXTENDED_BUSINESS_UNIT_CLOSE;
import static com.cps.integrations.order.CPSOrderConstants.ACCOUNT_EXTENDED_BUSINESS_UNIT_OPEN;
import static com.cps.integrations.order.CPSOrderConstants.ADDRESS_NUMBER;
import static com.cps.integrations.order.CPSOrderConstants.BUSINESS_UNIT;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;

import atg.nucleus.logging.ApplicationLogging;
import atg.nucleus.logging.ClassLoggingFactory;
import atg.service.perfmonitor.PerformanceMonitor;
import vsg.webservices.WebServiceClient;

public class CPSGHAccountClient extends WebServiceClient implements GHAccountClient {

    private static ApplicationLogging mLogging = ClassLoggingFactory.getFactory().getLoggerForClass(CPSGHAccountClient.class);
    private int mDebugLevel = 2;
    private boolean enableWebservice;

    private String mTestCity;
    private String mTestState;

    private final static String COMPONENT_NAME = "/cps/integrations/order/CPSGHAccountClient";

    @Override
    public Map<String, String> getEComDetails(String pCity, String pState, boolean isLoggingDebug)
                    throws InterruptedException, ExecutionException, IOException {
        Map<String, String> results = null;
        if (isEnableWebservice()) {
            pCity = correctCity(pCity);
            pState = correctState(pState);
            String serviceAddress = getWebServiceConfig().getServiceAddress();
            String userId = getWebServiceConfig().getUserId();
            String password = getWebServiceConfig().getPassword();
            results = getEComDetailsAsync(pCity, pState, userId, password, serviceAddress, isLoggingDebug);
        }
        return results;
    }

    public String testEComDetails() throws InterruptedException, ExecutionException, IOException {
        Map<String, String> results = null;
        if (isEnableWebservice()) {
            String pCity = correctCity(getTestCity());
            String pState = correctState(getTestState());
            String serviceAddress = getWebServiceConfig().getServiceAddress();
            String userId = getWebServiceConfig().getUserId();
            String password = getWebServiceConfig().getPassword();
            results = getEComDetailsAsync(pCity, pState, userId, password, serviceAddress, true);
        }
        return "Address Number: " + results.get(ADDRESS_NUMBER) + ", Business Unit: " + results.get(BUSINESS_UNIT);
    }

    private Map<String, String> getEComDetailsAsync(String pCity, String pState, String pUserId, String pPassword, String pServiceAddress,
                    boolean isLoggingDebug) throws IOException, ExecutionException, InterruptedException {
        long startTimeMethod = System.currentTimeMillis();
        Map<String, String> result = new HashMap<>();

        final String operationName = COMPONENT_NAME + ".getEComDetailsAsync";
        PerformanceMonitor.startOperation(operationName);
        try (AsyncHttpClient asyncHttpClient = new AsyncHttpClient()) {

            final Request request = createRequest(pCity, pState, pServiceAddress, pUserId, pPassword, isLoggingDebug);
            Future<Response> future = asyncHttpClient.executeRequest(request);

            Response response = future.get();
            String respBody = response.getResponseBody();
            if (isLoggingDebug) {
                mLogging.logDebug(new StringBuilder().append("RESPONSE_").append(pCity).append(":").append(respBody).toString());
            }
            String addressNumberValue = getValueFromBody(respBody, ACCOUNT_EXTENDED_ADDRESS_NUMBER_OPEN, ACCOUNT_EXTENDED_ADDRESS_NUMBER_CLOSE);
            String businessUnitValue = getValueFromBody(respBody, ACCOUNT_EXTENDED_BUSINESS_UNIT_OPEN, ACCOUNT_EXTENDED_BUSINESS_UNIT_CLOSE);

            result.put(ADDRESS_NUMBER, addressNumberValue);
            result.put(BUSINESS_UNIT, businessUnitValue);

            long endTimeTimeMethod = System.currentTimeMillis();
            long totalTimeMethod = endTimeTimeMethod - startTimeMethod;
            if (isLoggingDebug) {
                mLogging.logDebug(new StringBuilder().append("Completed getEComDetailsAsync method ").append(" time={").append(totalTimeMethod).append("}(ms)")
                                .toString());
            }
        } finally {
            PerformanceMonitor.endOperation(operationName);
        }
        return result;
    }

    private String getBody(String pCity, String pState, String pUserId, String pPassword) {

        StringBuilder stringBuilder = new StringBuilder()
                        .append("<soapenv:Envelope  xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:orac=\"http://oracle.e1.bssv.JP554950/\">\n")
                        .append("   <soapenv:Header>\n")
                        .append("      <wsse:Security soapenv:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:env=\"http://schemas.xmlsoap.org/soap/envelope/\">\n")
                        .append("         <wsse:UsernameToken>\n").append("            <wsse:Username>").append(pUserId).append("</wsse:Username>\n")
                        .append("            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">")
                        .append(pPassword).append("</wsse:Password>\n").append("         </wsse:UsernameToken>\n").append("      </wsse:Security>\n")
                        .append("   </soapenv:Header>\n").append("   <soapenv:Body>\n").append("      <orac:retrieveEComDetails>\n").append("      <city>")
                        .append(pCity).append("   </city> ").append("         <state>").append(pState).append("   </state>")
                        .append("      </orac:retrieveEComDetails>\n").append("   </soapenv:Body>\n").append("</soapenv:Envelope>");
        return stringBuilder.toString();
    }

    private Request createRequest(String pCity, String pState, String pServiceAddress, String pUserId, String pPassword, boolean isLoggingDebug) {
        String body = getBody(pCity, pState, pUserId, pPassword);
        if (isLoggingDebug) {
            mLogging.logDebug(new StringBuilder().append("Request for pCity=").append(pCity).append(", pState=").append(pState).append(" sent.").toString());
            mLogging.logDebug(new StringBuilder().append("body_").append(pCity).append(":").append(body).toString());
        }

        byte[] requestBody = body.getBytes();
        return new RequestBuilder("POST").setUrl(pServiceAddress).setContentLength(requestBody.length).setBody(requestBody)
                        .addHeader("Accept-Encoding", "gzip,deflate").addHeader("Content-Type", "text/xml;charset=UTF-8")
                        .addHeader("SOAPAction", "\"http://oracle.e1.bssv.JP554950//retrieveEComDetails\"").build();
    }

    private String getValueFromBody(String pBody, String pOpen, String pClose) {
        String value = null;

        int indexStart = pBody.indexOf(pOpen);
        int indexEnd = pBody.indexOf(pClose);
        int length = pOpen.length();

        if (indexStart != -1 && indexEnd != -1) {
            value = pBody.substring(indexStart + length, indexEnd).trim();
        }
        return value;
    }

    private String correctCity(String pCity) {
        String[] cities = pCity.split(" ");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < cities.length; i++) {
            cities[i] = cities[i].trim();
            cities[i] = cities[i].substring(0, 1).toUpperCase() + cities[i].substring(1);
            if (i > 0) {
                builder.append(" ");
            }
            builder.append(cities[i]);
        }
        return builder.toString();
    }

    private String correctState(String pState) {
        return pState.substring(0, 2).toUpperCase();
    }

    public int getDebugLevel() {
        return mDebugLevel;
    }

    public void setDebugLevel(int pDebugLevel) {
        mDebugLevel = pDebugLevel;
    }

    public boolean isEnableWebservice() {
        return enableWebservice;
    }

    public void setEnableWebservice(boolean enableWebservice) {
        this.enableWebservice = enableWebservice;
    }

    public String getTestState() {
        return mTestState;
    }

    public void setTestState(String pTestState) {
        this.mTestState = pTestState;
    }

    public String getTestCity() {
        return mTestCity;
    }

    public void setTestCity(String pTestCity) {
        this.mTestCity = pTestCity;
    }
}