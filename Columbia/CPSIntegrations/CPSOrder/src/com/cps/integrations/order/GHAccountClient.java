package com.cps.integrations.order;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public interface GHAccountClient {

    Map<String,String> getEComDetails(String pCity, String pState, boolean isLoggingDebug) throws InterruptedException, ExecutionException, IOException;

}
