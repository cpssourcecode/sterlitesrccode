package com.cps.integrations.order;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import com.csp.order.OrderInfo;

import atg.core.util.StringUtils;
import atg.service.perfmonitor.PerformanceMonitor;
import vsg.exception.WebServiceException;
import vsg.util.json.VSGJsonReader;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSOrderClient
 *
 * <h4>Description</h4>
 *
 * <h4>Notes</h4>
 *
 * @author Tim Harshbarger
 *
 */
public class CPSOrderClient extends WebServiceClient {

    /** component name for debugging */
    private final static String COMPONENT_NAME = "/cps/integrations/order/CPSOrderClient";

    /**
     * This method will return a list of orders
     *
     * @return
     * @throws WebServiceException
     */
    public Map<String, Object> requestOrders(String pCustomerNumber, Calendar pStartDate, Calendar pEndDate, String pOrderNumber, String pPONumber,
                    String pSalesOrderNumber, List<String> pShipTo, int pPageSize, int pStartIndex, String pSortField, String pSortOption)
                    throws WebServiceException {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrders").append(" - start").toString());
        }

        Map<String, Object> result = new HashMap<String, Object>();
        List<OrderInfo> infos = new ArrayList<OrderInfo>();

        final String operationName = COMPONENT_NAME + ".requestOrders";
        final String operationParam = "customerNumber: " + pCustomerNumber;
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        WebServiceConfig config = getWebServiceConfig();
        try {
            String url = new StringBuilder(config.getHistoryServiceAddress()).toString();

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add(CPSOrderConstants.PARAM_PAGE_SIZE, pPageSize);
            builder.add(CPSOrderConstants.PARAM_FROM, pStartIndex);
            builder.add(CPSOrderConstants.PARAM_CUSTOMER_ID, pCustomerNumber);
            if (!StringUtils.isBlank(pSortField)) {
                builder.add(CPSOrderConstants.PARAM_SORT_FIELD, pSortField);
            } else {
                builder.add(CPSOrderConstants.PARAM_SORT_FIELD, CPSOrderConstants.ORDER_DATE);
            }
            if (!StringUtils.isBlank(pSortOption)) {
                builder.add(CPSOrderConstants.PARAM_SORT_OPTION, pSortOption);
            } else {
                builder.add(CPSOrderConstants.PARAM_SORT_OPTION, "DESC");
            }

            JsonObjectBuilder queryBuilder = Json.createObjectBuilder();
            if (!StringUtils.isBlank(pOrderNumber)) {
                queryBuilder.add(CPSOrderConstants.PARAM_ORIGINAL_ORDER_NUMBER, Integer.valueOf(pOrderNumber));
            }
            if (!StringUtils.isBlank(pPONumber)) {
                queryBuilder.add(CPSOrderConstants.PARAM_PO_NUMBER, pPONumber);
            }
            if (!StringUtils.isBlank(pSalesOrderNumber)) {
                queryBuilder.add(CPSOrderConstants.PARAM_ORDER_NUMBER, Integer.valueOf(pSalesOrderNumber));
            }
            if (pStartDate != null) {
                queryBuilder.add(CPSOrderConstants.PARAM_DATE_FROM, pStartDate.getTimeInMillis());
            }
            if (pEndDate != null) {
                queryBuilder.add(CPSOrderConstants.PARAM_DATE_TO, pEndDate.getTimeInMillis());
            }

            if (pShipTo != null && pShipTo.size() > 0) {
                JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                for (String shipTo : pShipTo) {
                    arrayBuilder.add(shipTo);
                }
                queryBuilder.add(CPSOrderConstants.PARAM_SHIP_TO_NUMBER, arrayBuilder.build());
            }
            queryBuilder.add(CPSOrderConstants.PARAM_BILL_TO_NUMBER, pCustomerNumber);

            builder.add(CPSOrderConstants.PARAM_QUERY, queryBuilder.build());
            JsonObject params = builder.build();

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request: ").append(url).append(" ; params: ").append(params).toString());
            }

            JsonObject response = VSGJsonReader.readJsonObjectFromUrl(url, params, config.getTimeOut(), config.getSoTimeOut());

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Response: ").append(response).toString());
            }

            if (response != null) {
                Integer pages = response.getInt(CPSOrderConstants.NUM_FOUND);
                result.put(CPSOrderConstants.NUM_FOUND, pages);

                JsonArray results = response.getJsonArray(CPSOrderConstants.RESULTS);
                for (int i = 0; i < results.size(); i++) {
                    JsonObject data = results.getJsonObject(i);
                    OrderInfo info = new OrderInfo();
                    info.setId(Integer.valueOf(data.getInt(CPSOrderConstants.DOCID)).toString());
                    info.setUrl(new StringBuilder().append(config.getDocumentExtractionServiceAddress()).append(data.getString(CPSOrderConstants.URL))
                                    .toString());
                    Long epochDate = data.getJsonNumber(CPSOrderConstants.ORDER_DATE).longValue();
                    info.setEpochDate(epochDate);
                    info.setDate(new Date(epochDate));
                    info.setOrderNumber(Integer.valueOf(data.getInt(CPSOrderConstants.ORDER_NUMBER)).toString());
                    info.setShipTo(Integer.valueOf(data.getInt(CPSOrderConstants.SHIP_TO_NUMBER)).toString());
                    info.setShipToName(data.getString(CPSOrderConstants.SHIP_TO_NAME));
                    info.setAmount(data.getJsonNumber(CPSOrderConstants.ORDER_TOTAL).doubleValue());
                    info.setCustomerPONumber(data.getString(CPSOrderConstants.CUSTOMER_PO_NUMBER));

                    infos.add(info);
                }
                result.put("orders", infos);
            }

        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e);
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestOrders").append(" - exit").toString());
        }

        return result;
    }

    public void testRequestOrders() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestOrders").append(" - start").toString());
        }

        List<String> shipto = new ArrayList<String>();
        shipto.add("100552");

        try {
            Calendar startDate = Calendar.getInstance();
            startDate.setTimeInMillis(1440791762000l);
            Calendar endDate = Calendar.getInstance();
            endDate.setTimeInMillis(1590791762000l);

            Map<String, Object> reply = requestOrders("101010", startDate, endDate, "1162709", "test", null, shipto, 5, 0, null, null);
            List<OrderInfo> orders = (List<OrderInfo>) reply.get("orders");
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Returned orders: ").append(orders).toString());
            }

        } catch (WebServiceException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestOrders").append(" - exit").toString());
        }

    }
}
