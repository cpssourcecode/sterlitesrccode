package com.cps.integrations.statement;

/**
 * CPSStatementConstants
 *
 * @author Steve Neverov
 *
 */
public final class CPSStatementConstants {

    // Fields
    public static final String CUSTOMER_NUMBER = "customerNumber";
    public static final String EMAIL_ADDRESS = "emailAddress";

    public static final String ID = "id";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String OWNER_NAME = "ownerName";
    public static final String URL = "url";
    public static final String MODIFIED = "modified";
    public static final String STATEMENT_DATE = "STATEMENT_DATE";
    public static final String BILL_TO_NAME = "BILL_TO_NAME";
    public static final String DOCID = "DOCID";
    public static final String STATEMENT_TYPE = "Statement";

}
