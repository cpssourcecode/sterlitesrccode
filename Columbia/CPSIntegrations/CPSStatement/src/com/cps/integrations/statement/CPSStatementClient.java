package com.cps.integrations.statement;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.http.HTTPConstants;

import com.cps.statement.StatementInfo;

import atg.core.util.StringUtils;
import atg.json.JSONArray;
import atg.json.JSONObject;
import atg.service.perfmonitor.PerformanceMonitor;
import jp5503b5.bssv.e1.columbiapipe.com.ARStatementsManagerServiceStub;
import jp5503b5.bssv.e1.columbiapipe.com.ArStatementsVO;
import jp5503b5.bssv.e1.columbiapipe.com.BusinessServiceExceptionException;
import jp5503b5.bssv.e1.columbiapipe.com.ProcessARStatementsDocument;
import jp5503b5.bssv.e1.columbiapipe.com.ProcessARStatementsResponseDocument;
import vsg.exception.WebServiceException;
import vsg.util.json.VSGJsonReader;
import vsg.webservices.WebServiceClient;
import vsg.webservices.WebServiceConfig;

/**
 * CPSInvoiceClient
 * <p>
 * <h4>Description</h4>
 * <p>
 * <h4>Notes</h4>
 *
 * @author Steve Neverov
 */
public class CPSStatementClient extends WebServiceClient {

    /**
     * component name for debugging
     */
    private final static String COMPONENT_NAME = "/cps/integrations/statement/CPSStatementClient";
    private int noOfStatementsToFetch;

    /**
     * @return the noOfStatementsToFetch
     */
    public int getNoOfStatementsToFetch() {
        return noOfStatementsToFetch;
    }

    /**
     * @param noOfStatementsToFetch
     *            the noOfStatementsToFetch to set
     */
    public void setNoOfStatementsToFetch(int noOfStatementsToFetch) {
        this.noOfStatementsToFetch = noOfStatementsToFetch;
    }

    /**
     * This method will request statement
     *
     * @param pParams
     * @return
     * @throws WebServiceException
     */
    public Map<String, Object> requestStatementOnDemand(Map<String, Object> pParams) throws WebServiceException {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestStatementOnDemand").append(" - start").toString());
        }

        Map<String, Object> reply = new HashMap<String, Object>();

        final String operationName = COMPONENT_NAME + ".requestStatementOnDemand";
        final String operationParam = "customerNumber: " + pParams.get(CPSStatementConstants.CUSTOMER_NUMBER);
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        WebServiceConfig config = getWebServiceConfig();
        try {
            ARStatementsManagerServiceStub service = new ARStatementsManagerServiceStub(config.getServiceAddress());
            // service._getServiceClient().getOptions().setTimeOutInMilliSeconds(config.getTimeOut());
            service._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT, config.getTimeOut());
            service._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT, config.getSoTimeOut());
            service._getServiceClient().addHeader(createSecurityHeader(config.getUserId(), config.getPassword()));

            ProcessARStatementsDocument request = ProcessARStatementsDocument.Factory.newInstance();

            ArStatementsVO statementsVO = ArStatementsVO.Factory.newInstance();

            statementsVO.setCustomerNumber(Integer.valueOf((String) pParams.get(CPSStatementConstants.CUSTOMER_NUMBER)));
            statementsVO.setEmailAddress((String) pParams.get(CPSStatementConstants.EMAIL_ADDRESS));

            request.setProcessARStatements(statementsVO);

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request: ").append(request.toString()).toString());
            }

            ProcessARStatementsResponseDocument response = service.processARStatements(request);

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Response: ").append(response.toString()).toString());
            }

            ArStatementsVO responseStatementsVO = response.getProcessARStatementsResponse();
            reply.put(CPSStatementConstants.CUSTOMER_NUMBER, responseStatementsVO.getCustomerNumber());

        } catch (AxisFault af) {
            if (isLoggingError()) {
                if (af.getDetail() != null) {
                    String faultString = af.getDetail().toString();
                    logError(faultString);
                }
                logError(af);
            }
            success = false;
            throw new WebServiceException(af.getMessage());
        } catch (RemoteException | BusinessServiceExceptionException e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e.getMessage());
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestStatementOnDemand").append(" - exit").toString());
        }

        return reply;

    }

    /**
     * This method will return a list of statements
     *
     * @param pCustomerNumber
     *            bill to number
     * @param pStartDate
     *            epoch time in milliseconds
     * @param pEndDate
     *            epoch time in milliseconds
     * @return
     * @throws WebServiceException
     */
    public List<StatementInfo> requestStatements(String pCustomerNumber, String pStartDate, String pEndDate) throws WebServiceException {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestStatements").append(" - start").toString());
        }

        List<StatementInfo> infos = new ArrayList<StatementInfo>();

        final String operationName = COMPONENT_NAME + ".requestStatements";
        final String operationParam = "customerNumber: " + pCustomerNumber;
        PerformanceMonitor.startOperation(operationName, operationParam);
        boolean success = true;

        try {
            String url = getWebServiceConfig().getHistoryServiceAddress();

            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Request URL: ").append(url).toString());
            }

            WebServiceConfig config = getWebServiceConfig();

            /*
             * { "pageSize":10, "from": 0, "customerId" : 100109, "sortField" : "DOCID", "sortOrder" : "ASC", "query" : { "statementDateFrom" : 969192080000,
             * "statementDateTo" : 1442491280000 } }
             */

            JsonObjectBuilder builder = Json.createObjectBuilder();
            builder.add("pageSize", getNoOfStatementsToFetch());
            builder.add("from", 0);
            builder.add("customerId", pCustomerNumber);
            builder.add("sortField", "DOCID");
            builder.add("sortOrder", "ASC");
            JsonObjectBuilder query = Json.createObjectBuilder();
            query.add("statementDateFrom", pStartDate);
            query.add("statementDateTo", pEndDate);
            builder.add("query", query);

            JsonObject input = builder.build();
            JSONObject returnVal = VSGJsonReader.readJsonResponseFromUrl(url, input);

            if (returnVal != null) {
                if (isLoggingDebug()) {
                    logDebug(new StringBuilder().append("Response: ").append(returnVal).toString());
                }

                JSONArray jsonArray = returnVal.getJSONArray("results");
                if (jsonArray != null) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Object obj = jsonArray.get(i);
                        if (obj instanceof JSONObject) {
                            JSONObject statement = (JSONObject) obj;
                            StatementInfo info = new StatementInfo();
                            info.setId(statement.getString(CPSStatementConstants.DOCID));
                            info.setType(CPSStatementConstants.STATEMENT_TYPE);
                            info.setName(statement.getString(CPSStatementConstants.BILL_TO_NAME));
                            info.setDescription(CPSStatementConstants.BILL_TO_NAME);
                            info.setOwnerName(statement.getString(CPSStatementConstants.BILL_TO_NAME));
                            info.setUrl(new StringBuilder().append(getWebServiceConfig().getDocumentExtractionServiceAddress())
                                            .append(statement.getString(CPSStatementConstants.URL)).toString());
                            String epochTime = statement.getString(CPSStatementConstants.STATEMENT_DATE);
                            if (!StringUtils.isBlank(epochTime)) {
                                try {
                                    info.setModifiedDate(new Date(Long.parseLong(epochTime)));
                                    info.setEpochModifiedDate(Long.parseLong(epochTime));
                                } catch (Exception e) {
                                    if (isLoggingError()) {
                                        logError(e);
                                    }
                                }
                            }
                            infos.add(info);
                        }
                    }
                }
            }

        } catch (Exception e) {
            if (isLoggingError()) {
                logError(e);
            }
            success = false;
            throw new WebServiceException(e);
        } finally {
            if (success) {
                PerformanceMonitor.endOperation(operationName, operationParam);
            } else {
                PerformanceMonitor.cancelOperation(operationName, operationParam);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".requestStatements").append(" - exit").toString());
        }

        return infos;
    }

    /**
     * This method will request statement
     *
     * @throws WebServiceException
     */
    public void testRequestStatement() {

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestStatement").append(" - start").toString());
        }

        try {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(CPSStatementConstants.CUSTOMER_NUMBER, "37040");
            params.put(CPSStatementConstants.EMAIL_ADDRESS, "test@gmail.com");
            Map<String, Object> reply = requestStatementOnDemand(params);
            Integer number = (Integer) reply.get(CPSStatementConstants.CUSTOMER_NUMBER);
            if (isLoggingDebug()) {
                logDebug(new StringBuilder().append("Returned number: ").append(number).toString());
            }

        } catch (WebServiceException e) {
            if (isLoggingError()) {
                logError(e);
            }
        }

        if (isLoggingDebug()) {
            logDebug(new StringBuilder().append(COMPONENT_NAME).append(".testRequestStatement").append(" - exit").toString());
        }

    }

}
