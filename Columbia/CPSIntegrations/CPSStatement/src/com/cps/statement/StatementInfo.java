package com.cps.statement;

import java.util.Date;

/**
 * Bean that represents information for statement
 *
 * @author Steve Neverov
 */
public class StatementInfo implements Comparable{

	public StatementInfo() {
        super();
    }

	private String mId;

	public void setId(String pId) {
		mId = pId;
	}

	public String getId() {
		return mId;
	}

	private String mIdEncrypted;

	public void setIdEncrypted(String pIdEncrypted) {
		mIdEncrypted = pIdEncrypted;
	}

	public String getIdEncrypted() {
		return mIdEncrypted;
	}

	private String mType;

	public void setType(String pType) {
		mType = pType;
	}

	public String getType() {
		return mType;
	}

	private String mName;

	public void setName(String pName) {
		mName = pName;
	}

	public String getName() {
		return mType;
	}

	private String mDescription;

	public void setDescription(String pDescription) {
		mDescription = pDescription;
	}

	public String getDescription() {
		return mDescription;
	}

	private String mOwnerName;

	public void setOwnerName(String pOwnerName) {
		mOwnerName = pOwnerName;
	}

	public String getOwnerName() {
		return mOwnerName;
	}

	private String mUrl;

	public void setUrl(String pUrl) {
		mUrl = pUrl;
	}

	public String getUrl() {
		return mUrl;
	}

	private Date mModifiedDate;

	public void setModifiedDate(Date pModifiedDate) {
		mModifiedDate = pModifiedDate;
	}

	public Date getModifiedDate() {
		return mModifiedDate;
	}

	private long mEpochModifiedDate;

	public void setEpochModifiedDate(long pEpochModifiedDate) {
		mEpochModifiedDate = pEpochModifiedDate;
	}

	public long getEpochModifiedDate() {
		return mEpochModifiedDate;
	}

//	public String toString() {
//
//		StringBuilder result = new StringBuilder();
//		result.append("Id: ").append(getId()).append("; ");
//		return result.toString();
//
//	}

	public int compareTo(Object pInfo) {
		return Long.compare(((StatementInfo)pInfo).getEpochModifiedDate(), getEpochModifiedDate());
	}

}
